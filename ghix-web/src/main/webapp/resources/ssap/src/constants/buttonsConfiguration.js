import buttonsTypes from './buttonsTypes';
import {buttonsTextsDefaults} from './buttonsTexts';
import EXCEPTIONS from './exceptionList';


const DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME = {
    leftButton: buttonsTypes.CANCEL_BTN,
    middleButton: buttonsTypes.INITIAL_BTN,
    rightButton: buttonsTypes.COMBINED_BTN
};

const ALL_BUTTONS_HIDDEN_SCHEME = {
    leftButton: buttonsTypes.HIDDEN_BTN,
    middleButton: buttonsTypes.HIDDEN_BTN,
    rightButton: buttonsTypes.HIDDEN_BTN,
};


/**************************************************

 # Buttons Schemas

 *************************************************/

export const BUTTONS_SCHEMES = {
    [EXCEPTIONS.EDITING_PRIMARY_HOUSEHOLD_CONTACT]: DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,

    [EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER_ADDRESS]: DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,

    [EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addPersonButton: buttonsTypes.DEFAULT_BTN,
        deleteButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.ADDING_HOUSEHOLD_MEMBER]: DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,

    [EXCEPTIONS.EDITING_INCOMES_AND_DEDUCTIONS]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addIncomeSourceButton: buttonsTypes.DEFAULT_BTN,
        editButton_secondary: buttonsTypes.DEFAULT_BTN,
        removeButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.REVIEW_INCOME_SECTION]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addIncomeSourceButton: buttonsTypes.DEFAULT_BTN,
        editButton_secondary: buttonsTypes.DEFAULT_BTN,
        removeButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.ADDING_HOUSEHOLD_MEMBER_FROM_HOUSEHOLD_SECTION]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addPersonButton: buttonsTypes.DEFAULT_BTN,
        deleteButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.REVIEW_BEFORE_SUBMIT]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addIncomeSourceButton: buttonsTypes.DEFAULT_BTN,
        editButton_secondary: buttonsTypes.DEFAULT_BTN,
        removeButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.EDITING_ADDITIONAL_INFORMATION] : {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        editAdditionalInformationButton: buttonsTypes.DEFAULT_BTN,
        editButton_secondary: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION]: DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,

    [EXCEPTIONS.FIXING_TAX_RELATIONSHIP]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addPersonButton: buttonsTypes.DEFAULT_BTN,
        deleteButton: buttonsTypes.DEFAULT_BTN
    },

    [EXCEPTIONS.REVIEW_AFTER_SIGN]: {
        ...ALL_BUTTONS_HIDDEN_SCHEME,
        editButton_secondary: buttonsTypes.HIDDEN_BTN,
        downloadButton: buttonsTypes.INITIAL_BTN,
        printButton: buttonsTypes.INITIAL_BTN
    },

    [EXCEPTIONS.MANAGE_SPOUSE]: {
        ...DEFAULT_BUTTONS_WITH_CANCEL_BUTTON_SCHEME,
        addPersonButton: buttonsTypes.DEFAULT_BTN,
        deleteButton: buttonsTypes.DEFAULT_BTN
    }
};

const buttonsConfigs = {

    leftButton: {
        [buttonsTypes.HIDDEN_BTN]: {
            className: 'usa-button',
            isShown: false,
            text: buttonsTextsDefaults.NOTHING,
            i18n: 'button.nothing'
        },
        [buttonsTypes.INITIAL_BTN]: {
            className: 'usa-button-secondary btn-secondary',
            isShown: false,
            text: buttonsTextsDefaults.BACK_TXT,
            i18n: 'button.back'
        },

        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.BACK_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.back'
        },

        [buttonsTypes.CANCEL_BTN]: {
            text: buttonsTextsDefaults.CANCEL_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.cancel'
        }
    },

    middleButton: {
        [buttonsTypes.HIDDEN_BTN]: {
            className: 'usa-button',
            isShown: false,
            text: buttonsTextsDefaults.NOTHING,
            i18n: 'button.nothing'
        },
        [buttonsTypes.INITIAL_BTN]: {
            text: buttonsTextsDefaults.SAVE_TXT,
            isShown: false,
            className: 'usa-button',
            i18n: 'button.save'
        },

        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.SAVE_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.save'
        }
    },

    rightButton: {
        [buttonsTypes.COMBINED_BTN]: {
            className: 'usa-button',
            isShown: true,
            text: buttonsTextsDefaults.SAVE_CONTINUE_TXT,
            i18n: 'button.save.continue'
        },
        [buttonsTypes.HIDDEN_BTN]: {
            className: 'usa-button',
            isShown: false,
            text: buttonsTextsDefaults.NOTHING,
            i18n: 'button.save.continue'
        },
        [buttonsTypes.INITIAL_BTN]: {
            text: buttonsTextsDefaults.CONTINUE_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.continue'
        },

        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.CONTINUE_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.continue'
        },

        [buttonsTypes.SWITCHED_TO_SAVE_BUTTON_BTN]: {
            text: buttonsTextsDefaults.SAVE_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.continue'
        },


        [buttonsTypes.SWITCHED_TO_SUBMIT_APPLICATION_BTN]: {
            text: buttonsTextsDefaults.SUBMIT_APPLICATION_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.submit.application'
        }

    },

    addAddress: {
        [buttonsTypes.DEFAULT_BTN]: {
            className: 'usa-button',
            isShown: true,
            text: buttonsTextsDefaults.NOTHING,
            i18n: 'button.edit'

        },
    },

    editButton_secondary: {
        [buttonsTypes.HIDDEN_BTN]: {
            className: 'usa-button',
            isShown: false,
            text: buttonsTextsDefaults.NOTHING,
            i18n: 'button.edit'

        },
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.EDIT_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary btn-small',
            i18n: 'button.edit'
        }
    },

    removeButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.REMOVE_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary btn-small',
            i18n: 'button.remove'
        }
    },

    addPersonButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.ADD_ANOTHER_PERSON_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.add.person'
        }
    },

    deleteButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.DELETE_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.delete'
        }
    },

    addIncomeSourceButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.ADD_INCOME_SOURCE_TXT,
            isShown: true,
            className: 'usa-button btn-primary margin-top-null',
            i18n: 'button.add.income.source'
        }
    },

    addDeductionSourceButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.ADD_DEDUCTION_SOURCE_TXT,
            isShown: true,
            className: 'usa-button btn-primary margin-top-null',
            i18n: 'button.add.deduction.source'
        }
    },

    addEmployerCoverageDetailButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.ADD_EMPLOYER_DETAIL_TXT,
            isShown: true,
            className: 'usa-button',
            i18n: 'button.add.employer.coverage.detail'
        },
    },

    downloadButton: {
        [buttonsTypes.INITIAL_BTN]: {
            text: buttonsTextsDefaults.DOWNLOAD_TXT,
            isShown: true,
            className: 'usa-button btn-primary btn-small',
            i18n: 'button.download'
        }
    },

    fixHouseholdMemberButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.FIX_HOUSEHOLD_MEMBER_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.fix.household.member'
        },
    },

    fixRelationshipButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.FIX_RELATIONSHIP_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.fix.relationship'
        },
        [buttonsTypes.SPOUSE_BTN]: {
            text: buttonsTextsDefaults.SPOUSE_SELECT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.select.spouse'
        }
    },

    updateInformationButton: {
        [buttonsTypes.DEFAULT_BTN]: {
            text: buttonsTextsDefaults.UPDATE_INFORMATION_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.update.information'
        },
        [buttonsTypes.SPOUSE_BTN]: {
            text: buttonsTextsDefaults.SPOUSE_ADD,
            isShown: true,
            className: 'usa-button-secondary btn-secondary',
            i18n: 'button.add.spouse'
        }
    },

    printButton: {
        [buttonsTypes.INITIAL_BTN]: {
            text: buttonsTextsDefaults.PRINT_TXT,
            isShown: true,
            className: 'usa-button-secondary btn-secondary btn-small',
            i18n: 'button.print'
        }
    },
};

export const getButtons = (exception, page, type, adjustedFlow) => {

    const {
        exceptionType,
        exceptionPages
    } = exception;

    const buttonSubType = adjustedFlow[page].page.buttons[type];



    let typeForException;

    if(page === exceptionPages[0]) {
        typeForException = BUTTONS_SCHEMES[exceptionType][type];
    } else {
        typeForException = buttonSubType;
    }

    const btn = exceptionType !== null ? buttonsConfigs[type][typeForException] : buttonsConfigs[type][buttonSubType];

    if(!btn) { // For FallBack UI in Development. Here we return false if we weren't successful to get Button config
        return {
            isButton: false
        };
    }

    return {
        isButton: true,
        className: btn.className,
        isShown: btn.isShown,
        i18n: btn.i18n
    };
};
