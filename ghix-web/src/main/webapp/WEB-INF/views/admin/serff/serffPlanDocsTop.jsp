<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ page language="java" import="java.net.URLDecoder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="../../resources/demos/style.css" /> -->

<head>
<title>Plan Docs Admin</title>

<style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

th.sortable a {
	background-image: url(../img/arrow_off.png);
}

th.order1 a {
	background-image: url(../img/arrow_down.png);
}

th.order2 a {
	background-image: url(../img/arrow_up.png);
}

tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.excel {
	background-image: url(../img/ico_file_excel.png);
}

span.csv {
	background-image: url(../img/ico_file_csv.png);
}

span.xml {
	background-image: url(../img/ico_file_xml.png);
}

span.pdf {
	background-image: url(../img/ico_file_pdf.png);
}

span.rtf {
	background-image: url(../img/ico_file_rtf.png);
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}
</style>

<script type="text/javascript">

$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
	
	});

/* function validateDates(){

	var dateStart = document.getElementById('date1').value.split('/');
	
	if(dateStart != "" && dateStart.length > 0)
	{
		
	var boolean1 = validateDate(dateStart);
	}

	var dateEnd = document.getElementById('date2').value.split('/');
	
	if(dateEnd !=  "" && dateEnd.length > 0)
        {
		var boolean2 = validateDate(dateEnd);
	}
} */

function validateDate(dateGiven){
	
	var today=new Date();

	var dob_mm = dateGiven[0];  
	var dob_dd = dateGiven[1];  
	var dob_yy = dateGiven[2]; 
	
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);

	if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
	//alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if(today.getTime() < birthDate.getTime()){
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}
	
}

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	var topTenSelectClause='';
	var topTenWhereClause='';
	switch (type) {
		case 'B1' : 
			whereClause = 'last_updated between (sysdate-1) and sysdate';
			filterStatement = ' in last 24 hours';
			break;
		case 'B2' : 
			whereClause = 'last_updated between (sysdate-1/2) and sysdate';
			filterStatement = ' in last 12 hours';
			break;
		case 'B3' : 
			whereClause = 'last_updated between (sysdate-1/24) and sysdate';
			filterStatement = ' in last 1 hour';
			break;
		case 'B4' : 
		
		         //Step 1: Get List Box value, if it is not empty
			var fieldName = document.getElementById('field_name').value;
			var fieldValue = document.getElementById('field_value').value;
			if(fieldValue != null && fieldValue.length > 0)
			{
			whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
			filterStatement = ' with ' + fieldName + " = " + fieldValue;
			}
			
			//Step 2: Get the Status Value
			var statuses = document.getElementById('status');
			var status = statuses.options[statuses.selectedIndex].value;
			
			if(status == 'COMPLETED' || status == 'FAILED')
			{
				if(whereClause){
					whereClause = whereClause  + " and status = '" + status + "'";
				}else{
					whereClause = whereClause  + " status = '" + status + "'";
				}
			}
			if(status == 'S'){
				filterStatement = filterStatement+ ' which were successful';
			}
			if(status == 'F'){
			
				filterStatement = filterStatement+ ' which have failed';
			}
			
			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			if(date1){
				if(whereClause){
					whereClause = whereClause+"and to_char(last_updated  ,'mm/dd/yyyy' )='"+date1+"'" ;
					filterStatement = filterStatement + 'start date ' +  date1 ;
				}else{
					whereClause = whereClause+" to_char(last_updated  ,'mm/dd/yyyy' )='"+date1+"'" ;
					filterStatement = filterStatement + 'last update date' +  date1 ;
				}
			}
			
			if(!whereClause){
				whereClause = "last_updated <= sysdate";
			}
			break;
		case 'B5' : 
			whereClause = "last_updated <= sysdate";
			topTenSelectClause=" Select * from ( ";
			topTenWhereClause=" ) where rownum in (1,2,3,4,5,6,7,8,9,10) order by rownum";
			filterStatement = "for latest records";
			break;
	}
	document.getElementById('whereClause').value = escape(whereClause);
	document.getElementById('filterStatement').value = filterStatement;
	document.getElementById('topTenSelectClause').value = topTenSelectClause;
	document.getElementById('topTenWhereClause').value = topTenWhereClause;
}
</script>    
</head>

<body>
<form method="POST" id="frmPlanDocsTopPage" name="frmPlanDocsTopPage" action="getPlanDocsTopPage">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0" width="99%">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<br />
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<br />
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<br />
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td class="width112">Field:</td>
						<td>
							<select size="1" id="field_name" name="field_name">
								<option value="PLAN_ID" selected="">PLAN_ID</option>
								<option value="HIOS_PRODUCT_ID" >HIOS_PRODUCT_ID</option>
								<option value="ISSUER_PLAN_NUMBER">ISSUER_PLAN_NUMBER</option>
								<option value="hios_product_id">HIOS Prod ID</option>						
								<option value="ERROR_MSG">ERROR_MSG</option>						
							</select>
							<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<select size="1" id="status" name="status">
								<option value="A" selected="">All</option>
								<option value="COMPLETED" >Completed</option>
								<option value="FAILED">Failed</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td>Last Updated Date:</td>
									<td>
										<input type="text" id="date1" name="date1" size="10" maxlength="10">
									</td>
									<!-- <td>End Date:</td>
									<td>
										<input type="text" id="date2" name="date2" size="10" maxlength="10" class="input-medium">
									</td> -->
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "last_updated >= sysdate-2";
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %>select ID,PLAN_ID,HIOS_PRODUCT_ID,ISSUER_PLAN_NUMBER,
'<a href="getPlanDocsBottomLeft?attachment='||UCM_ID_NEW||chr(38)||'type='||substr(doc_name_new,INSTR(doc_name_new,'.')+1,length(doc_name_new)) ||'" target="oldDocs_1">New('||DOC_NAME_NEW||')</a>' as new_doc,
'<a href="getPlanDocsBottomRight?attachment='||UCM_ID_OLD||chr(38)||'type='||substr(doc_name_new,INSTR(doc_name_new,'.')+1,length(doc_name_new)) ||'" target="newDocs_1">Old('||DOC_NAME_OLD||')</a>' as old_doc,
STATUS,ERROR_MSG,
decode (upper(DOCUMENT_TYPE), 'B','Brochure','S','SBC') as doc_type,
to_char(LAST_UPDATED,'DD-MON-YY HH24:MM:SS') as LAST_UPDATED1
from serff_Plan_documents_job
where <%=URLDecoder.decode(whereClause, "UTF-8")%>
order by id desc <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF Batch
	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br />

<display:table name="${result1.rows}" requestURI="getPlanDocsTopPage" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">

	<display:column property="id" title="ID" sortable="false" />
	<display:column property="PLAN_ID" title="Plan ID" sortable="false" />
	<display:column property="HIOS_PRODUCT_ID" title="HIOS Product ID" sortable="false" />
	<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="false" />
	<display:column property="new_doc" title="New Doc" sortable="false" />
	<display:column property="old_doc" title="Old Doc" sortable="false" />
	<display:column property="STATUS" title="Status" sortable="false" />
	<display:column property="ERROR_MSG" title="Error Msg" sortable="false" />
	<display:column property="doc_type" title="Doc Type" sortable="false" />
	<display:column property="LAST_UPDATED1" title="Last Updated" sortable="false" />

	<display:setProperty name="export.xml.filename" value="serffPlanDocs.xml"/>
	<display:setProperty name="export.csv.filename" value="serffPlanDocs.csv"/>
	<display:setProperty name="export.excel.filename" value="serffPlanDocs.xls"/>
</display:table>
</form>
</body>