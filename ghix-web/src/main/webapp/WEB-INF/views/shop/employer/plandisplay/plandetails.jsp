<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link href="<c:url value="/resources/css/jquery.multiselect.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/planView-v2.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/planViewStyles-v2.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/variables.css" />" rel="stylesheet"  type="text/css" />

<style type="text/css">
body {
	padding-top: 40px;
    padding-bottom: 0px;
}
.sidebar-nav {
	padding: 9px 0;
}

.plans-section-content {
	position:relative;
}
.plan-0 {
	border:none !important;
}
	
#minEmp, #maxEmp, #minPercentEmp, #maxPercentEmp,
#minDep, #maxDep, #minPercentDep, #maxPercentDep{
    width: 50px;
    text-align: center;
	font-weight:bold;
	font-size:1.2em;
}

.accordion-inner  {
	padding:10px 20px 0 20px;
	
}

.accordion-inner  h3 {
	margin-bottom:15px;
	color:#ccc;
	border-bottom:solid 1px #e1e3e3;	
}

.accordion-inner #sliderEmp, .accordion-inner #sliderDep{
	width:95%;
	margin:50px 20px 0px 20px;
	
}

.table .label-info {
    background-color: #CDDFEE !important;
    color: #326289 !important;
    float: left;
    font-weight: normal !important;
    white-space: normal !important;
}

.plans-section-content{

	border-left:none;
}

</style>

<div class="row-fluid" id="main">
	<div class="gutter10">

	 	<div class="row-fluid" id="titlebar">
		 	<div class="gutter10" >
				<div class="subnav span3">
		        	<ul class="nav nav-pills">
		            	<li class="">
		                	<a href="../showplans">Back</a>
		                </li>
		            </ul>
				</div>
		        <div id="plansbar" class="span9">
		        <div class="pull-left">
					<img src='<c:url value="/resources/img/logo_${plansWithCost[0].issuerText}.png" />' alt="Logo ${plansWithCost[0].issuer}" /> 
				</div>
				<div class="span6">
					<h4 class="planName" id="Nplans">${plansWithCost[0].name}</h4>
					<h4 class="planType ${plansWithCost[0].level}"><small> ${plansWithCost[0].level}</small></h4>
				</div>
				
				
          			<div class="pull-right">
                  		<a class="btn btn-primary" onClick="addToCart('${plansWithCost[0].id}');">Add to Cart</a>
					</div>
				
				</div><!--plansbar-->
			</div><!--gutter10-->
		</div><!--titlebar-->

		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<p class="gutter10"> 
					<!--The plans in your cart on confirmation will be made available to your employees when they login to their exchange accounts.
        			Please review all information before confirming.-->
        		</p>     
			</div><!--sidebar-->      
       		
       		<div class="span9 columns pull-right" id="rightpanel">
        		<div id="planSelect" class="tabbedSections">
		        <!--plan details-->
					<div class="plans-section-content row-fluid" style="position:relative">
                    	<c:choose>  
                    	<c:when test="${plansWithCost[0].empCount < plansWithCost[0].employeeTotalCount}">
							<p class="label label-warning">Available only to ${plansWithCost[0].empCount} of your ${plansWithCost[0].employeeTotalCount} employees.</p>
						</c:when>
						<c:otherwise>
							<p class="label label-info">Available to all of your ${plansWithCost[0].employeeTotalCount} employees.</p>
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${plansWithCost[0].notAffodableCount >= plansWithCost[0].empCount}">
							<p class="label alert-danger">Not affordable to all of your ${plansWithCost[0].empCount} employees.</p>
						</c:when>
						<c:when test="${plansWithCost[0].notAffodableCount > 0}">
							<p class="label label-warning">Not affordable to ${plansWithCost[0].notAffodableCount} employees.</p>
						</c:when>
						<c:when test="${plansWithCost[0].empCount == plansWithCost[0].employeeTotalCount}">
							<p class="label label-info">Affordable to all of your ${plansWithCost[0].empCount} employees.</p>
						</c:when>
						<c:otherwise>
							<p class="label label-info">Affordable to all of your ${plansWithCost[0].empCount} employees.</p>
						</c:otherwise>
						</c:choose>
					                       
						<table class="table plan-details ">
							<tr>
						    	<td class="span4 txt-right">Employer's Total Cost</td>
						        <td><h4>$${plansWithCost[0].employerTotalCost} <small>per month</small></h4></td>
							</tr>
						    <tr>
						    	<td class="txt-right">Contribution towards employees</td>
						        <td><h4>$${plansWithCost[0].employerContrForEmp} <small>per month</small></h4></td>
							</tr>
							<c:choose>
							<c:when test="${plansWithCost[0].taxPenalty > 0}">
								<tr>
					            	<td>Estimated Tax Penalty</td>
					                <td><h4>$${plansWithCost[0].taxPenalty}<small>/month</small></h4></td>
								</tr>
							</c:when>
							<c:when test="${plansWithCost[0].estimatedTaxCredit > 0}">
								<tr>
						           	<td>Estimated Tax Credit (@${plansWithCost[0].taxCredit}%)</td>
					    	        <td><h4>$${plansWithCost[0].estimatedTaxCredit}<small>/month</small></h4></td>
								</tr>
							</c:when>
							</c:choose>
							<tr>
						    	<td class="txt-right">Average Premium per Employee</td>
						        <td><h4>$${plansWithCost[0].avgPremium} <small>per month</small></h4></td>
							</tr>
						    <tr>
						    	<td class="txt-right">Employer Contributes</td>
								<td><h4>$${plansWithCost[0].employerContrForEmpInPreimum } <small>per month</small></h4></td>
							</tr>
						    <tr>
						    	<td class="txt-right">Employee Pays</td>
						    	<td><h4>$
						    	<c:choose>  
                    				<c:when test="${plansWithCost[0].employeePayInPreimum < 0}">
										0
									</c:when>
									<c:otherwise>
										${plansWithCost[0].employeePayInPreimum }
									</c:otherwise>
								</c:choose>
						    	<small> per month</small></h4></td>
							</tr>
						</table>
					</div>
                        
                    <div class="plans-section-header">
                    	<h3>Benefits</h3>
					</div>
                  
                    <div class="plans-section-content row-fluid" style="position:relative">
						<table class="table plan-details ">
					    	<tr>
								<td class="span4 txt-right">Deductible</td>
					            <td><h4>$${plansWithCost[0].deductible}</h4></td>
							</tr>
					        <tr>
								<td class="txt-right">Annual Out-of-Pocket Limit</td>
								<td><h4>$${plansWithCost[0].oopMax}<br/><small>Excl.dedutible</small></h4></td>
							</tr>
							<tr>
								<td class="txt-right">Coninsurance</td>
								<td><h4>${plansWithCost[0].coInsurance}%</h4></td>
							</tr>
							<tr>
								<td class="txt-right">Doctor Visits</td>
								<td><h4>$${plansWithCost[0].officeVisit}</h4></td>
							</tr>
						</table>
					</div><!--plans-section-content--> 
				</div> <!--plan-details-->
			</div><!--rightpanel-->
		</div> <!--row-fluid-->
	</div><!-- container -->
</div><!-- main -->


<script src="<c:url value="/resources/js/bootstrap.js" />"></script> 
<script src="<c:url value="/resources/js/modernizr.custom.js" />"></script> 
<script src="<c:url value="/resources/js/waypoints.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.8.22.custom.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.multiselect.js" />"></script>

<script type="text/javascript">

$(document).ready(function(){
	$('body').css('height',$(window).height()); // set body height to window height

	$(".collapse").collapse();
	
	$("input").each(function(i){ 
		$(this).attr('checked', false);
	});

	$('a.btn').popover({
		placement:'left',
		
	});
});
function addToCart(planId){
	$.ajax({
		type: "PUT",
		url: "../cartItems/"+planId,
		}).done(function( msg ) {
			if(msg == "FAIL"){
				alert("Selected plan is already present in the cart")
			}
		});
}

</script>
