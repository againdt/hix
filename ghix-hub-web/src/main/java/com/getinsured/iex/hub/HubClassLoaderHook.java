package com.getinsured.iex.hub;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.getinsured.iex.platform.util.GIWSModuleAwareClassLoader;
import com.getinsured.iex.platform.util.GIWSModuleContextProvider;


public class HubClassLoaderHook implements ApplicationContextAware {
    private Logger logger = LoggerFactory.getLogger(HubClassLoaderHook.class);
    @Autowired private GIWSModuleAwareClassLoader wsplatformClassLoader;
    @Autowired private GIWSModuleContextProvider moduleContextProvider;
    private ApplicationContext appContext;

    @PostConstruct
    public void register(){
            logger.info("Registering AHBX Class loader with Platform");
            wsplatformClassLoader.registerClassLoader(this.getClass().getClassLoader(),"Ghix-Hub-Web".intern());
            moduleContextProvider.setModuleContext(appContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
            this.appContext = context;

    }
}
