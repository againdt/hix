/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import com.getinsured.hix.batch.enrollment.service.EnrlReconCompareSnapshotService;

/**
 * @author panda_p
 *
 */
public class EnrlReconCompareSnapshotTask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconCompareSnapshotTask.class);
	
	private Long fileId;
	private EnrlReconCompareSnapshotService enrlReconCompareSnapshotService;
	private Job job;
	private JobLauncher jobLauncher;
	
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		
		if(null != enrlReconCompareSnapshotService){
			LOGGER.trace(" EnrlReconCompareSnapshotTask calling enrlReconCompareSnapshotService ");
			
			//Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
			if(fileId!=null){
				enrlReconCompareSnapshotService.compareFileSnapshot(fileId,  job,  jobLauncher);
			}else{
				LOGGER.error(" fileId is null EnrlReconCompareSnapshotTaskin ");
				throw new RuntimeException("fileId is null in EnrlReconCompareSnapshotTask") ;
			}
		}else{
			LOGGER.error(" enrlReconCompareSnapshotService is null ");
			throw new RuntimeException("enrlReconCompareSnapshotService is null") ;
		}
		
		/*if(null != enrollmentCmsOutService){
			LOGGER.trace(" EnrollmentCmsInTask calling EnrollmentCmsOutService ");
			
			Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
			
			enrollmentCmsOutService.processCmsInFile(jobId, jobService);
		}else{
			LOGGER.error(" enrollmentCmsOutService is null ");
			throw new RuntimeException("enrollmentCmsOutService is null") ;
		}*/
		return RepeatStatus.FINISHED;
	}

	public Long getFileId() {
		return fileId;
	}

	@Value("#{jobParameters['fileId']}")
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public EnrlReconCompareSnapshotService getEnrlReconCompareSnapshotService() {
		return enrlReconCompareSnapshotService;
	}

	public void setEnrlReconCompareSnapshotService(EnrlReconCompareSnapshotService enrlReconCompareSnapshotService) {
		this.enrlReconCompareSnapshotService = enrlReconCompareSnapshotService;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}
	
	
}
