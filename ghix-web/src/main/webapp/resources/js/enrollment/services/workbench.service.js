(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .factory('workbenchService', workbenchService);

    workbenchService.$inject = ['$http', '$q'];

    function workbenchService($http, $q) {

        var activityData = {};
        return {
            getIssuersActivity: getIssuersActivity,
            getIssuerActivity: getIssuerActivity,
            setData: setData,
            getSearchResult: getSearchResult,
            getFileLevelSummary: getFileLevelSummary
        };

        function setData(data) {
            activityData = data;
        }


        function getIssuersActivity() {
            return activityData.enrlReconMonthlyActivityDTO.issuerLevelSummaryList;
        }

        function getSearchResult() {
            return activityData;
        }


        function getFileLevelSummary() {
            return activityData.enrlReconMonthlyActivityDTO.fileLevelSummaryList;
        }


        function getIssuerActivity(issuerDetails) {
            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/fetchmonthlyreconactivitydata';

            var promise = $http.post(url, issuerDetails);

            promise.success(function (response) {
                //console.log("success");
                if (response.errCode === 200) {
                    activityData = response;
                    deferred.resolve(response);

                } else {
                    // failed to fetch issue activity
                }

            });

            promise.error(function (response) {
                deferred.reject(response);
            });

            return deferred.promise;

        }
    }
})();
