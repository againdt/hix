package com.getinsured.iex.hub.vlp37.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.hub.vlp37.model.VLP37CaseLog;

@Repository
public interface IVLP37CaseLogRepository  extends JpaRepository<VLP37CaseLog, Long>{
	
	@Query("select log from VLP37CaseLog as log where log.caseNumber=:caseNumber")
	public VLP37CaseLog  getCaseLog(@Param("caseNumber") String caseNumber);
	
	@Query("select log from VLP37CaseLog as log where log.caseNumber=:caseNumber and log.status='STEP2_ACK'")
	public VLP37CaseLog  getStep2Initiated(@Param("caseNumber") String caseNumber);
	
	@Query("select log from VLP37CaseLog as log where log.caseNumber=:caseNumber and log.status='STEP3_ACK'")
	public VLP37CaseLog  getStep3Initiated(@Param("caseNumber") String caseNumber);
	
}
