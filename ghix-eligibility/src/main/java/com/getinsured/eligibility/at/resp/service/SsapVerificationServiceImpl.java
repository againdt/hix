package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapVerification;
import com.getinsured.iex.ssap.model.SsapVerificationGiwspayload;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapVerificationRepository;
import com.getinsured.timeshift.util.TSDate;

@Service("ssapVerificationService")
@DependsOn("dynamicPropertiesUtil")
@Transactional
public class SsapVerificationServiceImpl implements SsapVerificationService {


	private static final String VERIFIED = "VERIFIED";
	private static final String NOT_VERIFIED = "NOT_VERIFIED";
	private static final String NOT_VERIFIED_VARIANT = "NOT VERIFIED";
	private static final String PENDING = "PENDING";
	private static final String NOT_REQUIRED = "NOT_REQUIRED";
	private static final String SETTING_NEW_STATUS_IN_SSAP_APPLICANT_OBJECT = "Setting new status in Ssap Applicant object... ";
	@Autowired private SsapVerificationRepository ssapVerificationRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private EligibilityService eligibilityService;

	private static final Logger LOGGER = Logger.getLogger(SsapVerificationServiceImpl.class);
	private static final String APPLICANT_SEEKING_COVERAGE = "Y";
	private static String stateCode;

	@PostConstruct
	public void createStateContext() {
		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	@Override
	public List<SsapVerification> findBySsapApplicantId(Long ssapApplicantId) {

		SsapApplicant ssapApplicant = ssapApplicantRepository.findOne(ssapApplicantId);
		return ssapVerificationRepository.findBySsapApplicant(ssapApplicant);

	}

	@Override
	public int updateVerificationStatus(Long ssapApplicantId) {
		return ssapApplicantRepository.updateApplicantVerificationStatus(ssapApplicantId);
	}

	@Override
	public List<SsapVerification> saveOrUpdateSsapVerification(Long ssapApplicantId, List<SsapVerification> ssapVerificationList, Integer giWsPayloadId) {


		SsapApplicant ssapApplicant = ssapApplicantRepository.findOne(ssapApplicantId);

		Timestamp creationTimeStamp = new java.sql.Timestamp(new TSDate().getTime());
		for (SsapVerification ssapVerification : ssapVerificationList) {
			ssapVerification.setSsapApplicant(ssapApplicant);
			ssapVerification.setCreationTimestamp(creationTimeStamp);
			ssapVerification.setLastUpdateTimestamp(creationTimeStamp);

			// Link SsapVerification with GiWsPayload object with join table SsapVerificationGiwspayload
			SsapVerificationGiwspayload ssapVerificationGiwspayload = new SsapVerificationGiwspayload();
			ssapVerificationGiwspayload.setSsapVerification(ssapVerification);
			ssapVerificationGiwspayload.setGiWsPayloadId(BigDecimal.valueOf(giWsPayloadId));

			List<SsapVerificationGiwspayload> ssapVerificationGiwspayloadList = new ArrayList<>();
			ssapVerificationGiwspayloadList.add(ssapVerificationGiwspayload);
			ssapVerification.setSsapVerificationGiwspayloads(ssapVerificationGiwspayloadList);

		}

		List<SsapVerification> ssapVerificationLists = ssapVerificationRepository.save(ssapVerificationList);
		updateSsapApplicantVerificationsReferences(ssapApplicant, ssapVerificationLists);

		return ssapVerificationLists;
	}

	private SsapApplicant updateSsapApplicantVerificationsReferences(SsapApplicant ssapApplicant, List<SsapVerification> ssapVerificationList) {

		for (SsapVerification ssapVerification : ssapVerificationList) {
			readVerificationStatus(ssapApplicant, ssapVerification.getVerificationType(), ssapVerification.getVerificationStatus(), ssapVerification.getId());
		}

		return ssapApplicantRepository.save(ssapApplicant);
	}

	private void readVerificationStatus(SsapApplicant ssapApplicant, String type, String status, Long vId){
		LOGGER.debug(SETTING_NEW_STATUS_IN_SSAP_APPLICANT_OBJECT);
		BigDecimal bigVid = BigDecimal.valueOf(vId);
		switch (type) {
		case "SSN":
			ssapApplicant.setSsnVerificationStatus(status);
			ssapApplicant.setSsnVerificationVid(bigVid);
			break;
		case "CITIZENSHIP":
			ssapApplicant.setCitizenshipImmigrationStatus(status);
			ssapApplicant.setCitizenshipImmigrationVid(bigVid);
			break;
		case "ELIGIBLE_IMMIGRATION_STATUS":
			ssapApplicant.setVlpVerificationStatus(status);
			ssapApplicant.setVlpVerificationVid(bigVid);
			break;
		case "ANNUAL_INCOME":
			ssapApplicant.setIncomeVerificationStatus(status);
			ssapApplicant.setIncomeVerificationVid(bigVid);
			break;
		case "CURRENT_INCOME":
			ssapApplicant.setIncomeVerificationStatus(status);
			ssapApplicant.setIncomeVerificationVid(bigVid);
			break;
		case "INCARCERATION_STATUS":
			ssapApplicant.setIncarcerationStatus(status);
			ssapApplicant.setIncarcerationVid(bigVid);
			break;
		case "ESI_MEC":
			ssapApplicant.setMecVerificationStatus(status);
			ssapApplicant.setMecVerificationVid(bigVid);
			break;
		case "NON_ESI_MEC":
			// TODO:
			break;
		default:
			LOGGER.warn(EligibilityConstants.INVALID_VERIFICATION_TYPE_FOUND + type);
			break;
		}

	}

	/**
	 * returns whether applicant is verified.
	 * For Idaho + Financial case, always return true
	 * @throws GIRuntimeException -
	 * 			SsapApplicant object is null, Member information (IncarcerationStatusIndicator) in json is null, Person Id is not matching for SsapApplicant object and Member information in json
	 */
	@Override
	public boolean isApplicantVerified(String isFinancialFlag, SsapApplicant applicant, HouseholdMember member) {

		boolean isVerified = true;
		if (isFinancialFlag != null && "Y".equalsIgnoreCase(isFinancialFlag)) {
			// TODO: handle for NM
			if ("ID".equals(stateCode)) {
				return isVerified;
			}
		}
		
		/* if applicant is not ON Application, implies its verified by default and always returns as true */
		if (applicant != null && "N".equals(applicant.getOnApplication())) {
			return true;
		}

		preValidate(applicant, member);

		if(applicant != null && APPLICANT_SEEKING_COVERAGE.equals(applicant.getApplyingForCoverage())) {
			// Financial and Non-Financial
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getIncarcerationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getCitizenshipImmigrationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getSsnVerificationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getDeathStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getResidencyStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getNativeAmericanVerificationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getVlpVerificationStatus()) ? false : isVerified;

			// Financial Only
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getIncomeVerificationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getNonEsiMecVerificationStatus()) ? false : isVerified;
			isVerified = eligibilityService.checkNotVerifiedStatus(applicant.getMecVerificationStatus()) ? false : isVerified;
		}

		return isVerified;
	}

	private void preValidate(SsapApplicant applicant, HouseholdMember member) {
		if (applicant == null) {
			throw new GIRuntimeException("SsapApplicant object is null ");
		}
		if (member == null) {
			throw new GIRuntimeException(
					"Member information in json is null ");
		}
		if (member.getApplicantGuid() == null
				|| !member.getApplicantGuid().equals(applicant.getApplicantGuid())) {
			throw new GIRuntimeException(
					"Applicant Guid does not match for SsapApplicant object and Member information in json ");
		}
	}

	private boolean checkIncarcerationStatus(HouseholdMember member,
			boolean incarcerationStatus) {
		boolean determinedIncarcerationStatus = false;
		if (incarcerationStatus) { /* if verified then check below */
			if (member.getIncarcerationStatus() == null
					|| member.getIncarcerationStatus()
							.getIncarcerationStatusIndicator() == null) {
				determinedIncarcerationStatus = false; // assume as false
			} else {
				determinedIncarcerationStatus = !member
						.getIncarcerationStatus()
						.getIncarcerationStatusIndicator();
			}
		}
		return determinedIncarcerationStatus;
	}

}
