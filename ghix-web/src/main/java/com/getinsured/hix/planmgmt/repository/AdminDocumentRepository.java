package com.getinsured.hix.planmgmt.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.getinsured.hix.model.AdminDocument;

public interface AdminDocumentRepository extends JpaRepository<AdminDocument, Integer>{
	@Query("SELECT admindoc.id,admindoc.documentType,admindoc.documentName,admindoc.creationTimestamp,admindoc.createdBy,admindoc.lastUpdateTimestamp,admindoc.lastUpdatedBy,admindoc.userID,admindoc.commentID FROM AdminDocument admindoc order by admindoc.id")
	List<AdminDocument> getAllAdminDocumentInfo();
}
