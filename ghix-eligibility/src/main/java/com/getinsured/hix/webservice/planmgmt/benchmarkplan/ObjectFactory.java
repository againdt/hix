
package com.getinsured.hix.webservice.planmgmt.benchmarkplan;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.hix.webservice.planmgmt.benchmarkplan package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.hix.webservice.planmgmt.benchmarkplan
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBenchmarkPlanRequest }
     * 
     */
    public GetBenchmarkPlanRequest createGetBenchmarkPlanRequest() {
        return new GetBenchmarkPlanRequest();
    }

    /**
     * Create an instance of {@link GetBenchmarkPlanRequest.Members }
     * 
     */
    public GetBenchmarkPlanRequest.Members createGetBenchmarkPlanRequestMembers() {
        return new GetBenchmarkPlanRequest.Members();
    }

    /**
     * Create an instance of {@link GetBenchmarkPlanResponse }
     * 
     */
    public GetBenchmarkPlanResponse createGetBenchmarkPlanResponse() {
        return new GetBenchmarkPlanResponse();
    }

    /**
     * Create an instance of {@link GetBenchmarkPlanRequest.Members.Member }
     * 
     */
    public GetBenchmarkPlanRequest.Members.Member createGetBenchmarkPlanRequestMembersMember() {
        return new GetBenchmarkPlanRequest.Members.Member();
    }

}
