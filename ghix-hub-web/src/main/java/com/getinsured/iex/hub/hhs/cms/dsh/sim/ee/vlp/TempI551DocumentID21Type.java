
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TempI551DocumentID21Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TempI551DocumentID21Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AlienNumber"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}PassportCountry" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DocExpirationDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TempI551DocumentID21Type", propOrder = {
    "alienNumber",
    "passportCountry",
    "docExpirationDate"
})
public class TempI551DocumentID21Type {

    @XmlElement(name = "AlienNumber", required = true)
    protected String alienNumber;
    @XmlElement(name = "PassportCountry")
    protected PassportCountryType passportCountry;
    @XmlElement(name = "DocExpirationDate")
    protected XMLGregorianCalendar docExpirationDate;

    /**
     * Gets the value of the alienNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlienNumber() {
        return alienNumber;
    }

    /**
     * Sets the value of the alienNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlienNumber(String value) {
        this.alienNumber = value;
    }

    /**
     * Gets the value of the passportCountry property.
     * 
     * @return
     *     possible object is
     *     {@link PassportCountryType }
     *     
     */
    public PassportCountryType getPassportCountry() {
        return passportCountry;
    }

    /**
     * Sets the value of the passportCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassportCountryType }
     *     
     */
    public void setPassportCountry(PassportCountryType value) {
        this.passportCountry = value;
    }

    /**
     * Gets the value of the docExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDocExpirationDate() {
        return docExpirationDate;
    }

    /**
     * Sets the value of the docExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocExpirationDate(XMLGregorianCalendar value) {
        this.docExpirationDate = value;
    }

}
