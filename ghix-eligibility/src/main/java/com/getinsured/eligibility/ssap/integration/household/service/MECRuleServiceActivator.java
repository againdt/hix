package com.getinsured.eligibility.ssap.integration.household.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.exception.SSAPIntegrationException;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.jayway.jsonpath.JsonPath;

@Component(value = "mecRuleServiceActivator")
public class MECRuleServiceActivator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MECRuleServiceActivator.class);
	
	@SuppressWarnings("unchecked")
	@ServiceActivator
	public Message<Map<String, String>>[] split(Message<String> message) throws IOException, ParseException, SSAPIntegrationException {
		LOGGER.debug(">>>>>>>>>>>>>>>>>> MEC Response:" + message.getPayload());

		List<Message<Map<String, String>>> requests = null;
		String nmecResponse = message.getHeaders().get(SSAPIntegrationConstants.NMEC_RESPONSE_HEADER).toString();
		String mecResponse = message.getPayload().toString();
		String nmecResponseForMember = null;
		String mecResponseForMember = null;
		
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
		JSONParser parser = new JSONParser();
		JSONObject ssapJSONObject = (JSONObject)parser.parse(ssapJSON);
		ssapJSONObject = (JSONObject) ssapJSONObject.get("singleStreamlinedApplication");
		JSONArray taxHouseholds = (JSONArray)ssapJSONObject.get("taxHousehold");
		requests = new ArrayList<>();
		
		// Check if we need to invoke the rule for each member
		for(Object taxHousehold : taxHouseholds ) {
			JSONObject household = (JSONObject) taxHousehold;
			JSONArray householdMembers = (JSONArray) household.get("householdMember");
			for(Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
				JSONObject socialSecurityCard = (JSONObject)member.get("socialSecurityCard"); 
		        Object socialSecurityNumber = socialSecurityCard.get("socialSecurityNumber");
		        if(null != socialSecurityNumber && StringUtils.isNotBlank(socialSecurityNumber.toString())) {
		            String ssn = socialSecurityNumber.toString();
		            if(null != mecResponse && mecResponse.contains("ResponseSet")) {
		                mecResponseForMember = extractMECResponseForMember(ssn, mecResponse, "$.ResponseSet[?(@Person.SSN == '{SSN}')]");
		            } else {
		                mecResponseForMember = null;
		            }
		            
		            if(null != nmecResponse && nmecResponse.contains("IndividualResponseSet")) {
		                nmecResponseForMember = "{\"IndividualResponseType\":[" + extractMECResponseForMember(ssn, nmecResponse, "$.IndividualResponseSet.IndividualResponseType[*].[?(@ApplicantType.PersonSSNIdentification == '{SSN}')]") + "]}";
		            } else {
		                nmecResponseForMember = null;
		            }
		        } else {
		            mecResponseForMember = SSAPIntegrationConstants.NO_SSN;
		            nmecResponseForMember = SSAPIntegrationConstants.NO_SSN;
		        }
		        
				Map<String, String> rulePayload = new HashMap<String, String>();
				rulePayload.put("ssapJson", ssapJSON);
				rulePayload.put("esiJson", mecResponseForMember);
				rulePayload.put("nonEsiJson", nmecResponseForMember);
				rulePayload.put("personId", member.get("personId").toString());
				Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(rulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
				requests.add(ruleMessage);
			}
		}
		return requests.toArray(new Message[requests.size()]);
	}
	
	private String extractMECResponseForMember(String ssn, String payload, String jsonPath) throws SSAPIntegrationException {
	    String mecResponse = null;
		jsonPath = jsonPath.replace("{SSN}", ssn);
		net.minidev.json.JSONArray mecResponses = JsonPath.read(payload,jsonPath);
		mecResponse = ((net.minidev.json.JSONObject)mecResponses.get(0)).toJSONString();
		return mecResponse;
	}
}
