import React from 'react';
import { connect } from 'react-redux';

import { ENVIRONMENT_TYPES } from '../../constants/flowTypes';
import FooterMN from './footers/MN';
import FooterNV from './footers/NV';

function FooterContainer ({footerLinks}) {
    const { stateCode } = footerLinks;

    const UndefinedFooter = () => {
        return null;
    };

    let FooterComponent = UndefinedFooter;

    switch (stateCode) {
    case ENVIRONMENT_TYPES.MINNESOTA:
        FooterComponent =  FooterMN;
        break;
    case ENVIRONMENT_TYPES.NEVADA:
        FooterComponent = FooterNV;
        break;
    case ENVIRONMENT_TYPES.CALIFORNIA:
        // FooterComponent = FooterCA;
        break;
    case ENVIRONMENT_TYPES.IDAHO:
        // FooterComponent = FooterID;
        break;
    default:
        FooterComponent = UndefinedFooter;
        break;
    }

    return (
        <>
            <FooterComponent {...{footerLinks}} />
        </>
    );
}

const mapStateToProps = state => ({
    footerLinks: state.headerLinks
});

export default connect(mapStateToProps)(FooterContainer);
