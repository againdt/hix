package com.getinsured.hix.util;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * Plan Management Constants File
 */
public final class PlanMgmtConstants {
	
	private PlanMgmtConstants() {
	}
	
	/*	Common String variables		*/
	public static final String ID_CODE = "ID";
	public static final String MODULE_NAME= "PM-";
	public static final String EMPTY_STRING = "";
	public static final String PHONE1 = "phone1";
	public static final String PHONE2 = "phone2";
	public static final String PHONE3 = "phone3";
	public static final String PHONE4 = "phone4";
	public static final String PHONE5 = "phone5";
	public static final String PHONE6 = "phone6";
	public static final String PHONE7 = "phone7";
	public static final String PHONE8 = "phone8";
	public static final String PHONE9 = "phone9";
	public static final String DUPLICATE_HIOS = "duplicateHios";
	public static final String ZERO_STR = "0";
	public static final String ONE_STR = "1";
	public static final String TWO_STR = "2";
	public static final String THREE_STR = "3";
	public static final String FOUR_STR = "4";
	public static final String FIVE_STR = "5";
	public static final String CONTENT_TYPE = "text/html";
	public static final String STATECODEFORCA = "CA";
	public static final String UPLOADPATH = GhixConstants.UPLOAD_PATH;
	public static final String UNUSED = "unused";
	public static final String DASH_SIGN_COMPACT = "-";
	public static final String DASH_SIGN = " - ";
	public static final String COMPANY_LOGO = "companyLogo";
    public static final String COMPANY_LOGO_EXIST = "companyLogoExist";
	public static final String ISSUER_ID_DASH = "issuer_Id";
	public static final String LOGO_IMAGE = "logo_image";
	public static final String IS_PAYMENT_GATEWAY_STRING = "financial.IS_PAYMENT_GATEWAY";
	public static final String MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING = "hasPermission(#model, 'MANAGE_FINANCIAL_INFORMATION')";
	public static final String MANAGE_ISSUER_PERMISSION_STRING = "hasPermission(#model, 'MANAGE_ISSUER')";
	public static final String PARTNER_ID = "partnerId";
	public static final String MANAGE_REPRESENTATIVE_REDIRECT_URL = "redirect:/admin/issuer/representative/manage/";
	public static final String COMPANY_PROFILE_REDIRECT_URL = "redirect:/admin/issuer/company/profile/";
	public static final String SUSPEND_REPRESENTATIVE_REDIRECT_URL = "redirect:/planmgmt/issuer/suspend";
	public static final String COMMA_STRING = ",";
	public static final String UNDERSCORE_STRING = "_";
	public static final String COMPLETE = "COMPLETE";
	public static final String IN_PROGRESS = "In Progress";
	public static final String AT_THE_RATE = "@";
	public static final String COMMA = ",";
	public static final String LOCALHOST_IP = "127.0.0.1";
	public static final int PROCESS_IP_LENGTH = 40;
	/*	Constants For Issuer Report Controller	*/
	public static final String ISSUER = "Issuer";
	public static final String ISSUER_PAYMENT = "IssuerPaymentInformation";
	public static final Integer YEAR_DEFAULT_SPAN = 5;
	public static final Integer MONTHLY_DEFAULT_SPAN = 12;
	public static final Integer QUARTERLY_DEFAULT_SPAN = 4;
	public static final String MONTHLY_PERIOD = "Monthly";
	public static final String QUARTERLY_PERIOD = "Quarterly";
	public static final String YEARLY_PERIOD = "Yearly";
	public static final String DENTAL = "DENTAL";
	public static final String HEALTH = "HEALTH";
	public static final String INDIVIDUAL = "INDIVIDUAL";
	public static final String CURRENT_DATE = "CURRENT_DATE";
	public static final String REQUIRED_PREVIOUS_DATE = "REQUIRED_PREVIOUS_DATE";
	public static final String REQUIRED_DATE_FORMAT = "yyyy-MM-dd";
	public static final String REQUIRED_DATE_FORMAT_FOR_REPORT = "MM/dd/yyyy";
	public static final Integer YEARLY_DEC = -5;
	public static final Integer Q1_START = 1;
	public static final Integer Q1_END = 3;
	public static final Integer Q2_START = 4;
	public static final Integer Q2_END = 6;
	public static final Integer Q3_START = 7;
	public static final Integer Q3_END = 9;
	public static final Integer Q4_START = 10;
	public static final Integer Q4_END = 12;
	public static final Integer PAGE_SIZE = 30;
	public static final int IN_CLAUSE_ITEM_COUNT = 500;
	public static final String PLAN_NUMBER_FOR_QHP = "planNumberForQHP";
	public static final String MARKET_NUMBER_FOR_QHP = "marketForQHP";
	public static final String REGION_NAME_FOR_QHP = "rNameForQHP";
	public static final String PLATINUM_CHECK_FOR_QHP = "platinumCheckQHP";
	public static final String GOLD_CHECK_FOR_QHP = "goldCheckQHP";
	public static final String SILVER_CHECK_FOR_QHP = "silverCheckQHP";
	public static final String BRONZE_CHECK_FOR_QHP = "bronzeCheckQHP";
	public static final String CATASTROPHIC_CHECK_FOR_QHP = "catastrophicCheckQHP";
	public static final String PERIOD_ID_FOR_QHP = "periodIDForQHP";
	public static final String PLAN_LEVEL_FOR_QHP = "planLevelForQHP";
	public static final String RESET_QUERYSTRING = "r";
	public static final String PLAN_ID = "planId";
	public static final String MARKET_ID = "marketID";
	public static final String REGION_NAME = "rName";
	public static final String PERIOD_ID = "periodID";
	public static final String PLAN_LEVEL = "planLevel";
	public static final String ALL_PLAN_LEVELS_FOR_QHP = "platinum,gold,silver,bronze,expandedbronze,catastrophic";
	public static final String ALL_PLAN_LEVELS_FOR_SADP = "high,low";
	public static final String ALL_MARKET_TYPES = "INDIVIDUAL,SHOP";
	public static final String PLAN_LIST = "planList";
	public static final String REGION_LIST = "regionList";
	public static final String PLAN_NAME_FOR_SADP = "planNameForSADP";
	public static final String MARKET_NUMBER_FOR_SADP = "marketForSADP";
	public static final String REGION_NAME_FOR_SADP = "rNameForSADP";
	public static final String LOW_CHECK_FOR_SADP = "lowCheckForSADP";
	public static final String HIGH_CHECK_FOR_SADP = "highCheckForSADP";
	public static final String PERIOD_ID_FOR_SADP = "periodIDForSADP";
	public static final String PLAN_LEVEL_FOR_SADP = "planLevelForSADP";
	public static final String TIME_PERIOD = "timePeriod";
	public static final String PLAN_MARKET = "planMarket";
	public static final String ENROLLMENT = "enrollment";
	public static final String LIST_PAGE_DATA= "listPageData";
	public static final String LIST_PAGE_SIZE= "pageSize";
	public static final String HIGH_CHECK= "highCheck";
	public static final String LOW_CHECK= "lowCheck";
	public static final String PLATINUM_CHECK = "platinumCheck";
	public static final String GOLD_CHECK = "goldCheck";
	public static final String SILVER_CHECK = "silverCheck";
	public static final String BRONZE_CHECK = "bronzeCheck";
	public static final String EXPANDED_BRONZE_CHECK = "expandedBronzeCheck";
	public static final String CATASTROPHIC_CHECK = "catastrophicCheck";
	public static final String VIEW_ENROLLMENT_REPORT = "hasPermission(#model, 'VIEW_ENROLLMENT_REPORT')";
	
	/*	Constants For QHPAdminController	*/
	public static final String PAGE_TITLE = "page_title";
	public static final String IS_EDIT = "isEdit";
	public static final String PLAN = "plan";
	public static final String PLAN_OBJ = "plan";
	public static final String PLAN_CLASS = "Plan";
	public static final String NETWORK = "network";
	public static final String COMMENTS = "comments";
	public static final String DATA = "data";
	public static final String SORT_ORDER = "sortOrder";
	public static final String SORT_ORDER_EXCEPTFIRSTCOLUMN = "tempOrder";
	public static final String SORT_BY = "sortBy";
	public static final String STATUS = "status";
	public static final String VERIFIED = "verified";
	public static final String MARKET = "market";
	public static final String RATE_FILE = "rateFile";
	public static final String BENEFIT_FILE = "benefitFile";
	public static final String PLAN_NUMBER = "planNumber";
	public static final String ISSUER_NAME = "issuerName";
	public static final String PLAN_NAME = "planName";
	public static final String ISSUERNAME = "issuername";
	public static final String ISSUER_VERIFICATION = "issuerVerificationStatus";
	public static final String STATES = "state";
	public static final String TRUE_STRING = "true";
	public static final String FALSE_STRING = "false";
	public static final String PAGE_SIZE_VAR = "pageSize";
	public static final String DISPLAYEDITBUTTON = "displayEditButton";
	public static final String DISPLAYPROVIDERNETWORK = "displayProviderNetwork";
	public static final String DISPLAYACTION = "displayAction";
	public static final String BENEFIT_FILE_NAME = "Benefit_";
	public static final String CERTIFICATION_DOC_FOLDER = "CertificationDocument";
	public static final String EX_TYPE = "exType";
	public static final String TENANT = "tenant";
	public static final String SUCCESS = "Success";
	public static final Integer PDF_TABLE_SIZE = 100;
	public static final Integer PDF_PAGE_SIZE = 2;
	public static final String FORMULARY_DRUG_FILE_NAME_SUFFIX = "-DrugList.xls";
	public static final String PERMISSION_EDIT_PLANS = "EDIT_PLANS";
	public static final String HAS_EDIT_PERMISSIONS = "hasEditPermissions";
	public static final String EDIT_PLAN = "hasPermission(#model, 'EDIT_PLANS')";
	

	/*	Constants For Admin Report Controller	*/
	public static final String ISSUER_ID_FOR_QHP = "issuerIdForQHP";
	public static final String ISSUER_ID_FOR_SADP = "issuerIdForSADP";
	public static final String ISSUER_ID = "issuerId";
	public static final String ISSUER_LIST = "issuersList";
	
	public static final String PAYMENT_METHODS_OBJ = "paymentMethodsObj";
	public static final String LIST_EXCHG_PARTNERS_OBJ = "listExchgPartnersObj";
	public static final String ACCOUNT_USER = "AccountUser";
	public static final String STATUS_STRING = "status";
	public static final String DUPLICATE_ISSUER_ERR = "duplicateIssuerErr";	
	public static final String DUPLICATE_EMAIL_ERR = "repDuplicateEmail";
	public static final String ISSUER_OBJ = "issuerObj";
	public static final String STATUS_HISTORY = "statusHistory";
	public static final String STATE_LIST = "statelist";
	public static final String REDIRECT_URL = "redirectUrl";	
	public static final String DEFAULT_SORT = "defaultSort";
	public static final String RESULT_SIZE = "resultSize";
	public static final String REDIRECT_TO = "redirectTo";
	public static final String ISSUER_DETAILS_NOT_SUBMITTED = "issuerDetailsNotSubmitted";
	public static final String ADMIN_LOGIN_PATH = "redirect:/account/user/login?module=admin";
	public static final String MANAGE_ISSUER_PATH = "redirect:/admin/manageissuer";
	public static final String ISSUERQUALITYRATING = "IssuerQualityRating;";
	public static final String QUALITYRATIING_FOLDER = "qualityRating";
	public static final int TOTALCSVCOLUMN = 24;
	public static final String REQUIRED_DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
	public static final String REP_ID = "repId";		
	public static final String UPDATED = "updated";
	public static final String ISSUER_REP_OBJ = "issuerRepObj";
	public static final Integer END_DATE_SPAN = 12;
	public static final Integer DECREMENT_SPAN_FOR_END_DATE = -1;
	public static final String INVALID_HIOS_ISSUER_ID = "HIOS Issuer ID does not match with Issuer name";
	public static final String INVALID_EFFECTIVE_START_DATE = "Effective Start State has to be a future date";
	public static final String CORRECT_DATA_FLAG = "0";
	public static final String INCORRECT_DATA_FLAG = "1";
	public static final String GENERATEACTIVATIONLINKINFO = "generateactivationlinkInfo";
	public static final String MULTIPLE_ISSUERS_FOUND = "multiple";
	public static final String AUTHORITYFILE = "authorityFile";
	public static final String MARKETINGFILE = "marketingFile";
	public static final String DISCLOSURESFILE = "disclosuresFile";
	public static final String FINANCDISCLOSURESFILE = "financeDisclosuresFile";
	public static final String ACCREDITATIONFILES = "accreditationFile";
	public static final String ORGANIZATIONFILES = "organizationFiles";
	public static final String SUCCESSFULLYUPLOADED = "successFullyUploaded";
	public static final String DOCUMENT_DOWNLOAD_URL_START = "<a href='";
	public static final String DOCUMENT_DOWNLOAD_ACC_DOCID = "/hix/admin/document/filedownload?documentId=";
	public static final String DOCUMENT_DOWNLOAD_ACC_FLNAME = "&fileName=";
	public static final String DOCUMENT_DOWNLOAD_CLOSING_HREF = "' target=\"_blank\" >";
	public static final String DOCUMENT_DOWNLOAD_URL_END = "</a>";

	public static final String SHOW_OTHER_NAVIGATION = "showOtherNavigation";
	public static final String ALLOW_REVIEW_AND_SUBMIT = "allowReviewAndSubmit";
	public static final String STATECODEONEXCHANGE = "stateCodeOnExchange";
	public static final String STAECODEFORCA = "CA";
	public static final String IS_PAYMENT_GATEWAY_FLAG = "isPaymentGateway";
	public static final String ROUTING_NUMBER = "routing_number";
	public static final String INVALID_ROUTING_NUMBER = "invalid_routing_number";
	public static final String PAYMENT_METHOD_INFO_OBJ = "paymentMethodsObjInfo";	
	public static final String EXCHANGE_TYPE = "exchangeType";
	public static final String STATE_CODE = "STATE_CODE";
	public static final String IS_EMAIL_ACTIVATION = "IS_EMAIL_ACTIVATION";
	public static final String UPLOADED_FILE_NAME = "uploadedFileName";
	public static final String UPLOADED_FILE_LINK = "uploadedFileLink";
	public static final String USER = "USER";
	
	public static final String AUTHORITY_DOCUMENT_INFO_MAP = "authorityDocumentInfoMap";
	public static final String ACCREDITATION_DOC_INFO_MAP = "accreditationDocumentInfoMap";
	public static final String ORG_DATA_DOC_INFO_MAP = "orgDataDocumentInfoMap";
	public static final String MARKETING_DOC_INFO_MAP = "marketingDocumentInfoMap";
	public static final String FIN_CLOSURES_DOC_INFO_MAP = "finclosuresDocumentInfoMap";
	public static final String CLAIM_PAYPOL_DOC_INFO_MAP = "claimpaypolDocumentInfoMap";
	public static final String RATING_PRACTICES_DOC_INFO_MAP = "ratingPracticesDocumentInfoMap";
	public static final String COST_SHARING_PAY_DOC_INFO_MAP = "costSharingPayDocumentInfoMap";
	
	public static final String ADD_SUPP_DOC_INFO_MAP = "addSupportDocumentInfoMap";
	
	public static final String MULTIPART_ORIGINAL_FILE_NAME = "originalFileName";
	public static final String MULTIPART_CONTENTTYPE = "contentType";
	public static final String MULTIPART_SIZE = "size";
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	public static final String EDIT_PLAN_BENEFITS_PERMISSION = "hasPermission(#model, 'EDIT_PLAN_BENEFITS')";
	public static final String EDIT_PLAN_DETAILS_PERMISSION = "hasPermission(#model, 'EDIT_PLAN_DETAILS')";
	public static final String EDIT_PLAN_RATES_PERMISSION = "hasPermission(#model, 'EDIT_PLAN_RATES')";
	public static final String MANAGE_QHP_PLANS_PERMISSION = "hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')";
	public static final long FUNTIONAL_IDENTIFIER_VAL = 4000000001l;

	public static final String TRANSACTION_TYPE_LIST = "transactionTypeList";
	public static final String ISA_05_VALUES = "isa05Values";
	public static final String ISA_07_VALUES = "isa07Values";
	public static final String GS08_VALUES = "gs08Values";
	public static final String EDI_MAP_NAME = "ediMapNameValues";
	public static final String EDI_VALIDATION_STANDARD = "ediValidationStandardValues";
	public static final String EDI_APF = "ediApfValues";
	public static final String EDI_ROLES = "ediRoles";

	public static final String GS08_834 = "834";
	public static final String GS08_820 = "820";
	public static final String GS08_999 = "999";
	public static final String GS08_TA1 = "TA1";

	public static final String KEY_ISA05 = "ISA05";
	public static final String KEY_ST01 = "ST01";
	public static final String KEY_ISA07 = "ISA07";
	public static final String KEY_EDI_MAP_NAME = "EDI_MAP_NAME";
	public static final String KEY_EDI_VALIDATION_STANDARD = "EDI_VALIDATION_STANDARD";
	public static final String KEY_EDI_APF = "EDI_APF";
	public static final String KEY_ISA_05_DEFAULT_VALUE = "isa05DefaultValue";
	public static final String KEY_ISA_07_DEFAULT_VALUE = "isa07DefaultValue";
	public static final String KEY_ISA_06_DEFAULT_VALUE = "isa06DefaultValue";
	public static final String KEY_ISA_08_DEFAULT_VALUE = "isa08DefaultValue";
	public static final String KEY_GS_02_DEFAULT_VALUE = "gs02DefaultValue";
	public static final String KEY_GS_03_DEFAULT_VALUE = "gs03DefaultValue";
	public static final String PAYMENT_ID = "paymentId";
	public static final String UPDATE = "update";
	public static final String YES = "yes";
	public static final String NO = "no";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String EDIT = "edit";
	public static final String CURRENT_PAYMENT_ID = "curent_payment_id";
	public static final String FLOW = "flow";
	public static final String IS_ISSUER_CREATED = "isIssuerCreated";
	
	public static final String QHP = "QHP";
	public static final String SADP = "SADP";
	
	//for dynamic configuration
	public static final String PLANMGMT_HEALTHBENEFITS = "planManagement.Health";
	public static final String PLANMGMT_DENTALBENEFITS = "planManagement.Dental";
	public static final String PLANMGMT_DUPLICATE_KEY = "2";
	public static final int SIX = 6;
	public static final int TEN = 10;
	public static final Long  FILE_SIZE = 512000L;
	public static final Long COMPANY_LOGO_FILE_SIZE = 512000L;
	
	/*	Common Integer Variables	*/
	public static final int MINUS_ONE = -1;
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int ELEVEN = 11;
	public static final int TWELVE = 12;
	public static final int THIRTEEN = 13;
	public static final int FOURTEEN = 14;
	public static final int FIFTEEN = 15;
	public static final int SIXTEEN = 16;
	public static final int SEVENTEEN = 17;
	public static final int EIGHTEEN = 18;
	public static final int NINTEEN = 19;
	public static final int TWENETY = 20;
	public static final int TWENETYONE = 21;
	public static final int TWENETYTWO = 22;
	public static final int TWENETYTHREE = 23;
	public static final int TWENETYFOUR = 24;
	public static final int TWENETYFIVE = 25;
	public static final int TWENETYSIX = 26;
	public static final int TWENETYSEVEN = 27;
	public static final int TWENETYEIGHT = 28;
	public static final int TWENETYNINE = 29;
	public static final int THIRTY = 30;
	public static final int THIRTYONE = 31;
	public static final int THIRTYTWO = 32;
	public static final int THIRTYTHREE = 33;
	public static final int THIRTYFOUR = 34;
	public static final int THIRTYFIVE = 35;
	public static final int THIRTYSIX = 36;
	public static final int THIRTYSEVEN = 37;
	public static final int THIRTYEIGHT = 38;
	public static final int THIRTYNINE = 39;
	public static final int FOURTY = 40;
	public static final int FOURTYONE = 41;
	public static final int FOURTYSIX = 46;
	public static final int LISTING_NUMBER = 5;
	public static final int COLUMN1=0;
	public static final int COLUMN2=1;
	public static final int COLUMN3=2;
	public static final int RATING_AREA_START=1;
	public static final int RATING_AREA_END=19;
	public static final int VALID_ZIP_CODE_LENGTH=5;
	public static final int TIME_OUT = 1800;
	public static final int HUNDRED = 100;
	
	/*	Generate Plan Report	*/
	public static final String QHP_GENERATE_PALN_REPORT_MESSAGE = "Your Report is being generated for the selected plans. Please look under the 'Reports' tab to find your 'QHP Rate and Benefit Report'";
	public static final String SADP_GENERATE_PALN_REPORT_MESSAGE = "Your Report is being generated for the selected plans. Please look under the 'Reports' tab to find your 'SADP Rate and Benefit Report'";
	public static final String ERROR_WHILE_ADDING_IN_QUEUE = "Error While adding you for Report Generation";
	
	/*	ISSUER CONTROLLER	*/
	public static final String CDN_ISSUER_LOGO_PATH = "issuers/logo/";
	public static final String ISSUER_LOGIN_PATH = "redirect:/account/user/login?module=issuer";
	public static final String ISSUER_LANDING_PATH = "redirect:/planmgmt/issuer/landing/info";
	public static final String SESSION_REPRESENTATIVE_NAME = "sessionRepresentativeName";
	public static final String SESSION_DELEGATION_CODE = "sessionDelegationCode";
	public static final String SESSION_ERROR_INFO = "sessionErrorInfo";
	public static final String PH_BENEFIT_FILE_NAME = "_PH_Benefit_";
	public static final String NETWORK_OBJ = "networkObj";
	public static final String STATES_OBJ = "stateObj";
	public static final String REP_USER = "repUser";
	public static final String ASC = "ASC";
	public static final String ENROLLMENT_AVAIL = "enrollmentAvail";
	public static final String ISSUER_MANAGEQHP = "issuer/manageqhp";
	public static final String REP_DUPLICATE_EMAIL = "repDuplicateEmail";
	public static final String APPLICATIONLIST = "applicationlist";
	public static final String EXCEPTION_STRING_2 = ", Exception : ";
	public static final String ISSUER_MANAGEQDP = "issuer/manageqdp";
	public static final String STATUS_LIST = "statusList";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String CHANGE_ORDER = "changeOrder";
	public static final String STATUS_HISTORY_PAGE_SIZE = "statusHistoryPageSize";
	public static final String LAST_UPDATED = "lastUpdated";
	public static final String DISPLAYPLANLEVEL = "displayPlanLevel";
	public static final String DISPLAYSTARTBUTTON = "displayStartButton";
	public static final String ISSUERID = "issuerid";
	public static final String HIOSISSUERID = "hiosissuerid";
	public static final String APPEND_HTTP = "http://";
	public static final String APPEND_HTTPS = "https://";
	public static final String PLAN_DIS_DATE = "planDisplayDate";
	public static final String FILE_DOWNLOAD_URL_START = "<a href='/hix/admin/document/filedownload?documentId=";
	public static final String ISSUER_FILE_DOWNLOAD_URL_START = "<a href='/hix/download/document?documentId=";
	public static final String FILE_DOWNLOAD_URL_FILE_NAME =  "&fileName=";
	public static final String FILE_DOWNLOAD_URL_TARGET = "' target=\"_blank\">";
	public static final String FILE_DOWNLOAD_URL_END = "</a>";
	public static final String AUTHORITY = "authority";
	public static final String ACCREDITING = "accrediting";
	public static final String ORGDATA = "orgData";
	public static final String MARKETING = "marketing";
	public static final String FINDISCLOSURES = "finDisclosures";
	public static final String ADDSUPPORTDOC = "addSupportDoc";
	public static final String PAYPOLPRACTICES =  "payPolPractices";
	public static final String ENROLLMENTANDDIS =  "enrollmentAndDis";
	public static final String ENROLLMENT_DISENROLLMENT_DOCUMENT_INFO_MAP =  "enrollDisenrollDocumentInfoMap";
	public static final String RATINGPRACTICES = "ratingPractices";
	public static final String COSTSHAREANDPAYMENT = "costSharAndPayment";
	public static final String SORT_ORDER_DESC = "DESC";
	public static final String SORT_ORDER_ASC = "ASC";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String TITLE = "title";
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";
	public static final String VIEW_ISSUER_PROFILE_PERMISSION = "hasPermission(#model, 'VIEW_ISSUER_PROFILE')";
	public static final String IS_ISSUER_REP_ADDED = "isIssuerRepAdded";
	public static final String IS_ISSUER_REP_DUPLICATE = "isIssuerRepDuplicate";
	public static final String IS_USER_NAME_EXIST = "isUserNameExists";
	public static final String DOES_ISSUER_HAVE_REPR = "doesIssuerHaveRepr";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final String STATIC_ACCESS = "static-access";
	public static final String DB_QUERY_ENDING_WITH_PERCENT = "%'))";
	
	public static final String FUTURE_STATUS = "futureStatus";
	public static final String FUTURE_DATE = "futureDate";
	public static final String NOTAVAILABLE = "Not Available";
	public static final String AVAILABLE = "Available";
	public static final String DEPENDENTSONLY = "Dependents Only";
	
	public static final String SERVICE_AREA_ID = "serviceAreaId";
	public static final String SA_FILE_NAME = "ServiceArea.xls";
	
	public static final String PLAN_YEARS_LIST = "planYearsList";
	public static final String PLAN_YEARS_KEY = "planYearsKey";
	public static final String SELECTED_PLAN_YEAR = "selectedPlanYear";
	public static final String FETCH_FOR_PLAN_YEAR = "fetchForPlanYear";
	public static final String GET_SELECTED_PLAN_YEAR = "planYearSelected";
	public static final String UNDO_DECERTIFICATION = "successfully updated decertification effective date";
	public static final String UNDO_DECERTIFICATION_FAILED = "Could not update decertification effective date, please try later";
	
	public static final String MANAGE_QHP_PATH = "redirect:/admin/planmgmt/manageqhpplans";
	public static final String MANAGE_QDP_PATH = "redirect:/admin/planmgmt/manageqdpplans";
	public static final String FROM_EMAILDI_NO_REPLY = "noreply@getinsured.com";
	public static final String SUCCESS_STR = "success";
	public static final String ENROLL_AVAIL = "ENROLL_AVAIL";
	public static final String PENDING_DECERTIFICATION = "PENDING_DECERTIFICATION";
	public static final String VIEW_PLAN_DETAILS = "hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')";
	public static final String VIEW_PLAN_BENEFITS = "hasPermission(#model, 'VIEW_PLAN_BENEFITS')";
	public static final String VERIFY_PLAN = "hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')";
	public static final String EDIT_PLAN_DETAILS = "hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')";  
	public static final String EDIT_PLAN_BENEFITS = "hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')"; 
	public static final String EDIT_PLAN_RATES = "hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')";
	public static final String ADD_NEW_ISSUER = "hasPermission(#model, 'ADD_NEW_ISSUER')";  
	public static final String EDIT_REPRESENTATIVE = "hasPermission(#model, 'EDIT_REPRESENTATIVE')";
	
	public static final String NOTAVAILABLE_LARGE = "NOTAVAILABLE";
	public static final String AVAILABLE_LARGE = "AVAILABLE";
	public static final String DEPENDENTSONLY_LARGE = "DEPENDENTSONLY";
	public static final String ENROLLMENTAVAILABILITY = "enrollmentAvailability";
	public static final String CURR_DATE = "currDate";
	public static final String UPDATE_CERTIFICATION_STATUS = "hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')";
	
	public static final String IS_VERIFIED = "IS_VERIFIED";
	public static final String IS_CERTIFIED = "IS_CERTIFIED";
	public static final String DOWNLOAD_DOC = "downloadDoc";
	
	public static final String PLAN_COUNT = "planCount";
	public static final String PLAN_NUMBERS = "planNumbers";
	public static final String PLAN_ID_LIST = "planIds";
	public static final String IS_BULK_ACTION = "isBulkAction";
	public static final String MODULENAME = "moduleName";
	
	public static final String ERROR = "ERROR";
	public static final String ERROR_MSG = "Error";
	
	public static final String SEPARATORAT = "@";
	public static final String HIGHFEN = "-";
	public static final String  PLANRATES = "_PlanRates_";
	
	public static final String GET_ISSUER_NAMES_FAILED = "failed to get issuer names";
	public static final String SUBMIT_FAILED = "failed to submit";
	
	public static final String  EXCEPTION = "EXCEPTION";
	
	public static final String INSURANCE_EXCHANGE = "insuranceExchange";
	public static final String EXCHANGE_ADDRESS = "exchangeAddress";
	public static final String EXCHANGE_WEBSITE = "exchangeWebsite";
	public static final String ISSUER_MARKETING_NAME = "issuerMarketingName";
	public static final String BROKER_NAME = "brokerName";
	public static final String BROKER_NAME_TO_ADDRESS = "Certified Broker";
	public static final String BROKER_ADDRESS = "brokerAddress";
	public static final String PLAN_CERTIFICATION_STATUS = "planCertificationStatus";
	public static final String PLAN_ENROLLMENT_STATUS = "planEnrollAvailStatus";
	public static final String HEALTHEXCHANGE = "healthExchange";
	public static final String HEALTHEXCHANGE_URL = "exchangeURL";
	public static final String HEALTHEXCHANGE_TEAM = "healthExchangeTeam";
	public static final String HEALTHEXCHANGE_NAME = "exchangeName";
	public static final String HEALTHEXCHANGE_PHONE = "exchangePhone";
	public static final String DECIMAL_POINT = "\\.";
	public static final String DOLLAR = "\\$";
	public static final String PERCENTAGE = "%";
	public static final String SPACE = " ";
	public static final String DOLLARSIGN = "$";
	public static final String UNDERSCORE = "_";
	public static final String HYPHEN = "-";
	public static final String FILE_SEPARATOR = "/";
	public static final String BCC = "Bcc";
	public static final String ID = "id";
	public static final String RECIPIENT = "recipient";
	public static final String MAIL_DATEFORMAT = "MMMMM dd, yyyy";
	public static final String EMSG_PM_RESPONSE = "No response from Plan Management service.";
	public static final String EMSG_ECM_RESPONSE = "Failed to put document to ECM.";
	public static final String UNEXPECTED_GENERAL_ERROR_CODE = "1000";
	public static final String SELECTEDPLANYEARFORBULK = "SelectedPlanYearForBulk";

	// Crosswalk
	public static final String FTP_UPLOAD_CROSSWALK_PATH = "/uploadPlansCrosswalk/";
	public static final String TRANSFER_CROSSWALK = "TRANSFER_CROSSWALK";
	public static final String FTP_CROSSWALK_FILE = "_CROSSWALK_";
	public static final String CROSSWALK_ECM_BASE_PATH = "serff/docs/crosswalk/";

	// Network Transparency
	public static final String FTP_UPLOAD_NW_TRANS_PATH = "/uploadPlansNetworkTransparency/";
	public static final String TRANSFER_NW_TRANS = "TRANSFER_NW_TRANS";
	public static final String FTP_NW_TRANS_FILE = "_NW_TRANS_";
	public static final String NW_TRANS_ECM_BASE_PATH = "serff/docs/NetworkTransparency/";

	public static final String PATH_SEPERATOR = "/";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE = "Unexpected general error.";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_EMSG_ECM_RESPONSE = "Failed to put document to ECM.";
	public static final String EXCHANGE_STATE = "exchangeState";
	public static final String CERTIFIED = "Certified";
	public static final String TIMEZONE = "timezone";
	public static final String RATEDATEFORMAT = "yyyy-MM-dd";
	public static final int BCC_ADDRESS_STRING_MAX_LENGTH = 770;
	
	public static final String RETURN_STRING_AUTHORITY = "returnStringAuthority";
	public static final String RETURN_STRING_ACCREDITATION = "returnStringAccreditation";
	public static final String RETURN_STRING_ORG_DATA = "returnStringOrgData";
	public static final String RETURN_STRING_MARKETING = "returnStringMarketing";
	public static final String RETURN_STRING_FIN_CLOSURES = "returnStringFinClosures";
	public static final String RETURN_STRING_ADD_SUPP_DOC = "returnStringAddSupDoc";
	public static final String RETURN_STRING_CLAIM_PAYPOL_DOC = "returnStringClaimPaypol";
	public static final String RETURN_STRING_ENROLLMENT_DISENROLLMENT = "returnStringEnrDisEnr";
	public static final String RETURN_STRING_RATING_PRACTICES = "returnStringRating";
	public static final String RETURN_STRING_COST_SHARING = "returnStringCostSharing";
	public static final String  ISSUER_ROLE = "ISSUER";
	public static final String  ADMIN_ROLE = "ADMIN";
	public static final String PHIX = "PHIX";
	public static final String MARKETPROFILE = "MarketProfileDTO";
	public static final String  CERTIFICATIONSTATUS_PATTERN = "^CERTIFIED|DECERTIFIED|INCOMPLETE|LOADED$";
	public static final String  ENROLLMENTAVAILABILITY_PATTERN = "^AVAILABLE|DEPENDENTSONLY|NOTAVAILABLE$";
	
	public static final String COMPANY_LOGOUI = "companyLogoUI";
	
	public static final String PLANINFORMATION_UPDATE = "PlanInformationUpdate";
	public static final String PLANINFORMATION_FOR_CERTIFIED_AND_AVAILABLE = "The plan mentioned above is now available in the marketplace for shopping. Please take the above plan into consideration when assisting current and future customers.";
	public static final String PLANINFORMATION_FOR_NONCERTIFIED_OR_CERTIFIED_OR_INCOMPLETE_AND_NOTAVAILABLE = "The plan mentioned above has been pulled off the marketplace. This plan will not be available for enrollment for future enrollments.  We may, subject to conditions, request you to change the enrollments of your customers currently enrolled in this plan. Customers currently enrolled in this plan will be supported by the carrier for the plan year unless we specifically request you to change enrollments. Please take this information into consideration when assisting current and future customers.";
	public static final String PLANINFORMATION_FOR_CERTIFIED_AND_DEPENDENTSONLY = "The plan mentioned above is now available only for dependents of current enrollees. Please take this into consideration when assisting current and future customers.";
	public static final String ON = "ON";
	public static final String DOT = ".";
	
	public static final String PLAN_CERTIFICATION_BULK_UPDATE_NOTIFICATION = "PlanCertificationBulkStatusUpdateEmail";
	public static final String ISSUER_CERTIFICATION_STATUS_CHANGE_NOTIFICATION = "IssuerCertificationStatusChangeEmail";
	public static final String PLAN_CERTIFIED_NOTIFICATION = "IssuerPlanCertificationEmail";
	public static final String PLAN_DE_CERTIFIED_NOTIFICATION = "IssuerPlanDeCertificationEmail";
	public static final String PLAN_ENROLLMENT_UPDATE_NOTIFICATION = "IssuerPlanEnrollmentAvailabilityChangeEmail";
	public static final String ISSUER_ECM_PDF_FILE_PATH = "issuernotification";
	public static final String PLAN_ECM_PDF_FILE_PATH = "plannotification";
	public static final String PDF_EXT = ".pdf";
	public static final String ISSUER_PLAN_DECERTIFICATION = "IssuerPlanDeCertification";
	public static final String ISSUER_PLAN_CERTIFICATION = "IssuerPlanCertification";
	public static final String ISSUER_FINANCIAL_INFORMATION_UPDATE_NOTIFICATION = "IssuerFinancialInformationChangeEmail";
	public static final String SYSTEM_DATE = "systemDate";
	public static final String ISSUER_ADDRESS = "issuerAddress";
	public static final String ISSUER_REPRESENTATIVE = "issuerRepresentative";
	public static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	public static final String HEALTHEXCHANGE_FULL_NAME = "healthExchangeFullName";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipCode";
	public static final String BREAK_LINE = "<br>";
	public static final String  PLAN_EOC_DOCUMENT = "Plan EOC Document";
	public static final String  FUTURE_EFFECTIVE_DATE = "Future Effective Date";
	public static final String  DISPLAY_FIELD = "displayField";
	public static final String  DISPLAY_VAL = "displayVal";
	public static final String  FUTURE_ENROLLMENT_AVAILABILITY = "Future Enrollment Availability";
	public static final String  EFFECTIVE_DATE = "Effective Date";
	public static final String  PLAN_BROCHURE = "Plan Brochure";
	public static final String  ENROLLMENT_AVAILABILITY = "Enrollment Availability";
	public static final String  PLAN_STATUS = "Plan Status";
	public static final String  DECERTIFIED = "De-certified";
	public static final String  INCOMPLETE = "Incomplete";
	public static final String  LOADED = "Loaded";
	public static final String  PLAN_EOC_DOC_ID = "eocDocId";
	public static final String  PLAN_EOC_DOC_NAME = "eocDocName";
	public static final String ECM_REGEX = "^(?i:workspace)(:)(\\/\\/)(?i:SpacesStore)(\\/)([A-Za-z0-9\\-])+(;)([0-9\\.]{1,5})$";
	public static final String UNSECURE_CHARS_REGEX = "[\n\r]"; // Used for HIX-66062 HP-FOD issue of Header Manipulation

	/**
	 * feth timezone from database
	 */
	public static final String PLAN_HISTORY_DATE_FORMAT = "MMM dd, yyyy";
	public static final String LAST_UPDATE_TIMESTAMP = "lastUpdateTimestamp";
	public static final String PLAN_STATUS_HISTORY = "planStatusHistory";
	
	public static final String SELECTED_PLAN_LEVEL = "selectedPlanLevel";
	public static final String SELECTALL_QHP_PLANS = "isSelectAllPlansOnAllPagesForQHP";
	public static final String SELECTALL_QDP_PLANS = "isSelectAllPlansOnAllPagesForQDP";
	public static final String CERTIFICATION_STATUS_UPDATE = "certificationStatusUpdate";
	public static final String ENROLLMENT_AVBL_UPDATE = "enrollmentAvblUpdate";
	public static final String PRIMARY_CONTACT = "primaryContact";
	public static final String INVALID_VALID_PATH_ERROR = "Application trying to reach a blacklisted folder or filename or extension type.";
	
    //public static final String LOGO_BY_ID_URL_PATH = "/planmgmt/issuer/downloadlogobyid/";
	//public static final String ADMIN_LOGO_BY_ID_URL_PATH = "/admin/issuer/downloadlogobyid/";
    public static final String LOGO_BY_HIOSID_URL_PATH = "/planmgmt/issuer/downloadlogo/";
	public static final String ADMIN_LOGO_BY_HIOSID_URL_PATH = "/admin/issuer/downloadlogo/";
	
	public static final String DOCUMENT = "document";
	public static final String DOCUMENT_NAME = "documentName";
	public static final String MIME_CONTENT_TYPE = "contentType";
	public static final String ACTIVE = "ACTIVE";
	public static final String DISP_ACTIVE = "Active";
	public static final String INACTIVE = "INACTIVE";
	public static final String DISP_INACTIVE = "InActive";
	public static final String INACTIVE_DORMAT = "Inactive-dormant";
	public static final String DISP_SUSPENDED = "Suspended";
	
	
	public static final int ENROLLMENT_AVAIL_INT = 1;
	public static final int UPDATE_DATE_INT = 2;
	public static final int EFFECTIVE_DATE_INT = 3;
	public static final int USER_NAME_INT = 4;
	public static final int COMMENT_INT = 5;
	public static final int ISSUERPLANNUMBER_INT = 6;
	public static final int STATUS_INT = 7;
	public static final int STATUS_FILE_INT = 8;
	public static final int FN_ENROLLMENT_AVAIL_VAL_INT = 9;
	public static final int FN_STATUS_VAL_INT = 10;
	public static final int FN_DISPLAYVAL_INT = 11;
	public static final int PROVIDER_NETWORK_NAME_INT = 12;
	public static final int PROVIDER_NETWORK_TYPE_INT = 13;
	public static final int PROVIDER_NETWORK_ID_INT = 14;
	public static final int FN_BROCHURE_VAL_INT = 15;
	public static final int START_DATE_INT = 16;
	public static final int FN_EFFECTIVE_START_DATE_VAL_INT = 17;
	public static final int BROCHURE_UCM_ID_VAL_INT = 18;
	public static final int FUTURE_ENROLLMENT_AVAIL_INT = 19;
	public static final int ISSUER_VERIFICATION_STATUS_VAL_INT = 20;
	public static final int FUTURE_ERL_AVAIL_DATE_COL_VAL_INT = 21;
	public static final int FN_EOC_DOC_VAL_INT = 22;
	public static final int RATEFILE_INT = 1;
	public static final int RATE_EFFECTIVE_DATE_INT = 3;
	public static final int BENEFITFILE_INT = 6;
	public static final int FN_RATEFILE_VAL_INT = 7;
	public static final int FN_BENEFITFILE_VAL_INT = 8;
	public static final int FN_DISPLAYVAL_RATEHISTORY_INT = 9;
	public static final int BENEFIT_EFFECTIVE_DATE_INT = 10;
	public static final int EFFECTIVE_DATE_COMBINED_INT = 11;
	public static final int PLAN_SBC_VAL_INT = 12;
	public static final String EFFECTIVE_DATE_COL = "enrollmentAvailEffDate";
	public static final String ENROLLMENT_AVAIL_COL = "enrollmentAvail";
	public static final String FUTURE_ENROLLMENT_AVAIL_COL = "futureEnrollmentAvail";
	public static final String FUTURE_ERL_AVAIL_DATE_COL = "futureErlAvailEffDate";
	public static final String ISSUER_VERIFICATION_STATUS_COL = "issuerVerificationStatus";
	public static final String ISSUER_VERIFICATION_STATUS = "Verified";
	public static final String START_DATE_COL = "startDate";
	public static final String STATUS_COL = "status";
	public static final String STATUS_FILE_COL = "supportFile";
	public static final String UPDATED_BY_COL = "lastUpdatedBy";
	public static final String UPDATED_DATE_COL = "lastUpdateTimestamp";
	public static final String BROCHURE_COL = "brochure";
	public static final String USER_NAME_COL = "userName";
	public static final String ISSUERPLANNUMBER_COL = "issuerPlanNumber";
	public static final String DISPLAYVAL_COL = "displayVal";
	public static final String DISPLAYFIELD_COL = "displayField";
	public static final String PROVIDER_NETWORK_NAME_COL = "providerName";
	public static final String PROVIDER_NETWORK_TYPE_COL = "providerType";
	public static final String PROVIDER_NETWORK_ID_COL = "providerNetworkId";
	public static final String BROCHURE_UCM_ID_CAL = "brochureUCmId";
	public static final String PENDING_NO = "No";
	public static final String VERIFIED_YES = "Yes";
	public static final String EOC_DOC_NAME_COL = "eocDocName";
	public static final String EOC_DOC_UCM_ID_CAL = "eocDocId";
	public static final String COMMENT_COL = "commentId";
	public static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	public static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>Comment</a>";
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String TIMESTAMP_PATTERN2 = "yyyy-MM-dd";
	public static final String TIMESTAMP_PATTERN3 = "MMM dd, yyyy";
	public static final String DOWNLOAD_FILE_PRETEXT1 = "<a target='_top' href='";
	public static final String DOWNLOAD_FILE_POSTTEXT_BROCHURE = "' target='_blank'>Download File</a>";
	public static final String REPOSITORY_NAME = "IPlanRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Plan";
	//private static final String DOWNLOAD_FILE_PRETEXT1 = "<a href='"; -- used in RatehistoryRendererImpl
	public static final String DOWNLOAD_FILE_PRETEXT2 =  "download/document?documentId=";
	public static final String DOWNLOAD_FILE_POSTTEXT_BENEFITS = "'>Download File</a>";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE1 = "<a href='/hix/admin/planmgmt/dowloadbenefits/";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE2 =	"'>Download File</a>";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE3 = "<a href='/hix/issuer/planmgmt/dowloadbenefits/";
	public static final String CREATE_AND_DOWNLOAD_RATE_FILE1 = "<a href='/hix/issuer/qhp/dowloadqhprates/";
	public static final String CREATE_AND_DOWNLOAD_RATE_FILE2 =	"'>Download File</a>";
	public static final String BENEFITFILE_COL = "benefitFile";
	public static final String PLAN_SBC_COL = "sbcUcmId";
	public static final String RATEFILE_COL = "rateFile";
	public static final String RATE_EFFECTIVE_DATE_COL = "rateEffectiveDate";
	public static final String UPDATE_DATE_COL = "lastUpdateTimestamp";
	public static final String USER_NAME_ALIAS = "lastUpdatedBy";
	public static final String BENEFIT_EFFECTIVE_DATE_COL = "benefitEffectiveDate";
	public static final String EFFECTIVE_DATE_COMB_COL = "effectiveDateCombined";
	public static final String HISTORY_DOCUMENT_DOWNLOAD_URL_START="<a href='#' onClick=\"showdetail('";
	public static final String HISTORY_DOCUMENT_DOWNLOAD_ACC_DOCID= "download/document?documentId="; //"admin/document/filedownload?documentId=";
	public static final String HISTORY_DOCUMENT_DOWNLOAD_CLOSING_HREF="');\">";
	public static final String HISTORY_DOCUMENT_DOWNLOAD_URL_END="Download File </a>";
	public static final String CMSPLAN_ID = "cmsPlanId";
	public static final String COVERAGE_YEAR = "coverageYear";
	public static final String TERM_DATE = "terminationDate";
	public static final String VIEW_QUALITY_RATING_PERMISSION = "hasPermission(#model, 'VIEW_QUALITY_RATING')";
	public static final String MANAGE_PROVIDER_DATA_PERMISSION = "hasPermission(#model, 'MANAGE_PROVIDER_DATA')";

//  AHBX plan attribute constants STARTS
	public static final String AHBX_PLAN_ID = "planId";
	public static final String AHBX_ISSUER_ID = "issuerId";
	public static final String AHBX_PLAN_EFFCT_END_DATE = "planEffectiveEndDate";
	public static final String AHBX_PLAN_EFFCT_START_DATE = "planEffectiveStartDate";
	public static final String AHBX_PLAN_LEVEL = "planLevel";
	public static final String AHBX_PLAN_MARKET = "planMarket";
	public static final String AHBX_PLAN_NAME = "planName";
	public static final String AHBX_PLAN_NETWORK_TYPE = "planNetworkType";
	public static final String AHBX_PLAN_RECORD_INDICATOR = "planRecordIndicator";
	public static final String AHBX_PLAN_TYPE = "planType";
	public static final String AHBX_PLAN_HIOS_ID = "hiosPlanId";
	public static final String AHBX_PLAN_STATUS = "planInfoStatus";	
	public static final String AHBX_PLAN_YEAR = "planYear";	
	public static final SimpleDateFormat AHBX_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	public static final String VALID_FIRST_NAME="validFirstName";
	public static final String VALID_LAST_NAME="validLastName";
	
	public static final String POSTGRES = "POSTGRESQL"; 
	public static final String DATABASE_TYPE_CONFIG = "#{configProp['database.type']}";
	public static final String STATE_CODE_CA = "CA";
	public static final String STATE_CODE_ID = "ID";
	public static final String STATE_CODE_MS = "MS";
	public static final String STATE_CODE_WA = "WA";
	public static final String STATE_CODE_CT = "CT";
	public static final String STATE_CODE_MN = "MN";
	public static final String STR_COMMA = ",";
	public static final String ALL = "ALL";
	public static final String ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS = "hasPermission(#model, 'ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS')";
	public static final String SUSPEND_ENROLLMENT_REP_REDIRECT_URL = "redirect:/planmgmt/issuer/enrollmentrep/suspend";
	public static final String MIME_TYPE_ARRAY = "mimeTypeArray";
	
	public static TimeZone getGlobalTimeZone() {
		return TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE));
	}
	
}
