package com.getinsured.eligibility.at.ref.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.handler.LceCompareHandler;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.transform.CompareApplicationTransformer;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralLCECompareService")
@Scope("singleton")
public class ReferralLCECompareServiceImpl implements ReferralLCECompareService {

	private static final Logger LOGGER = Logger.getLogger(ReferralLCECompareServiceImpl.class);

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("compareApplicationTransformer")
	private CompareApplicationTransformer compareApplicationTransformer;

	@Autowired
	@Qualifier("lceCompareHandler")
	private LceCompareHandler lceCompareHandler;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Override
	@ReferralTransactionAnno
	public CompareMainDTO executeCompare(long currentApplicationId, long enrolledApplicationId) throws Exception {
		return execute(currentApplicationId, enrolledApplicationId, false);
	}

	public CompareMainDTO execute(long currentApplicationId, long enrolledApplicationId, boolean readOnly) throws Exception {
		CompareMainDTO compareMainDTO = null;
		try {
			LOGGER.info("Lce Compare starts for application id - " + currentApplicationId + " with enrolled id - " + enrolledApplicationId);
			final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
			final SsapApplication enrolledApplication = loadEnrolledApplication(enrolledApplicationId);
			compareMainDTO = transformCompareDto(enrolledApplication, currentApplication);
			executeLce(compareMainDTO,readOnly );
			LOGGER.info("Lce Compare Current Referral with Enrolled Application Ends");
		} catch (Exception e) {
			throw e;
		}
		return compareMainDTO;
	}
	
	@Override
	public CompareMainDTO executeCompareReadOnly(long currentApplicationId, long enrolledApplicationId) throws Exception {
		return execute(currentApplicationId, enrolledApplicationId, true);
	}

	private void executeLce(CompareMainDTO compareMainDTO, boolean readOnly) {
		ssapEnrolleeHandler.parseListofEnrollees(compareMainDTO);
		if (compareMainDTO != null && compareMainDTO.getEnrolledApplication() != null && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes() != null
		        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthPlanId() > 0) {
			//ssapEnrolleeHandler.invokeSharedPlanMgnt(compareMainDTO);
			if(StringUtils.isNotBlank(compareMainDTO.getEnrolledApplication().getCsrLevel())) {
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(compareMainDTO.getEnrolledApplication().getCsrLevel());
			}else {
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(ReferralConstants.CS1);	
			}
		} else { //for dental only Cost sharing always will be CS1.
			// sonar fix
			if(compareMainDTO != null && compareMainDTO.getEnrolledApplication() != null && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes() != null) {
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(ReferralConstants.CS1);	
			}
		}
		runComparator(compareMainDTO);
		
		if (!readOnly){
			modifyCurrentApplicants(compareMainDTO);
		}
	}

	private void modifyCurrentApplicants(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		for (CompareApplicantDTO currentApplicant : currentList) {
			persistSSAPApplicantStatus(currentApplicant);
		}
	}

	private void persistSSAPApplicantStatus(CompareApplicantDTO applicantDTO) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicantDTO.getId());
		ssapapplicant.setStatus(applicantDTO.getStatus());
		ssapApplicantRepository.save(ssapapplicant);
	}

	private void runComparator(CompareMainDTO compareMainDTO) {
		lceCompareHandler.processCompare(compareMainDTO);
	}

	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication, SsapApplication currentApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(compareApplicationTransformer.transformApplication(enrolledApplication, false));
		compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setSsapApplicationId(enrolledApplication.getId());
		compareMainDTO.setCurrentApplication(compareApplicationTransformer.transformApplication(currentApplication, false));
		return compareMainDTO;
	}

	private SsapApplication loadEnrolledApplication(long enrolledApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enrolledApplicationId);
	}

	private SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

}
