<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link href="<c:url value="/resources/css/jslider.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/jslider.blue.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/jslider.plastic.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/jslider.round.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/jslider.round.plastic.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/jquery.multiselect.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/planView-v2.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/planViewStyles-v2.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/css/variables.css" />" rel="stylesheet"  type="text/css" />

<style type="text/css">


.accordion-inner  {
	padding:10px 20px 0 20px;
	
}

.accordion-inner  h3 {
	margin-bottom:15px;
	color:#ccc;
	border-bottom:solid 1px #e1e3e3;	
}

.accordion-inner #sliderEmp, .accordion-inner #sliderDep{
	width:95%;
	margin:50px 20px 0px 20px;
	
}

table.table td {
vertical-align:middle !important;
}

h3.green {
margin-top:20px;
}
#rightpanel {
padding-top:0px;
}
</style>

<div class="row-fluid" id="main">
	<div class="gutter10">
    
		<div class="row-fluid" id="titlebar">
			<div class="gutter10" >
		    	<div class="span3">
		        	<div class="subnav">
		            	<ul class="nav nav-pills">
							<li class="dropdown"><a href="<c:url value="/shop/employer/plandisplay/showplans" />">Back</a></li>
						</ul>
					</div>
				</div>
		        <div class="span9" id="plansbar">
		           	<h3 class="pull-left" id="Nplans">Your Cart <small id="detailSummary"></small></h3>
					<div class="pull-right">
		            	<a class="btn-primary btn validateCart" href="<c:url value="/shop/employer/plandisplay/confirm" />">Confirm</a>
					</div>
				</div>
			</div><!--gutter10-->
		</div><!--titlebar-->
		
		<div class="row-fluid">
		   	 <div class="span3" id="sidebar">
		     	<p class=""> 
		     		The plans in your cart on confirmation will be made available to your employees when they login to their exchange accounts.
		        	Please review all information before confirming.
		        </p>     
				</div><!--sidebar-->      
		    
		   		 <div class="span9 columns pull-right" id="rightpanel">
			    	<div id="planSelect">
			    			<div class="gutter10">
			    				<a href="#" class="remove btn" id="remove_<@=id@>">Remove Selected</a> 
			    				<h4 class="planName pull-right">Open Enrollment Start Date : <span class="dark">${openEnrollmentStart}</span></h4>
			    			</div>
			        	
			
			        	<table id="cart" class="table txt-center">
			        		<div id="loadDiv" class="well" style="background:none;text-align:center">												
			        			<h3 class="green">Loading...</h3>
								<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
							</div>
						</table>
			            
			            <div class="form-actions">
							<div class="pull-right">
			                	<a class="btn-primary btn validateCart" href="confirm">Confirm</a>
							</div>
						</div>
			      	</div>
		      	</div>
			</div><!-- row-fluid -->
	</div><!-- container -->
</div><!-- main -->


<script type="text/html" id="tmpCart">
	<tr>
		<td class="span1"><@=srNo@></td>
        <td class="span1">
        	<div class="control-group">
            	<div class="controls">
                	<label class="checkbox inline">
                    	<input type="checkbox" id="item_<@=id@>"/>
                    </label>
				</div>
			</div>
		</td>
        <td class="span1"><img src='<c:url value="/resources/img/logo_<@=issuerText@>.png" />' alt="Logo <@= issuer @>" /></td>
		<td class="span4"> 
        	<h4 class="planName"><@= name @> </h4>
			<h4 class="planType <@=level@>"><small><@=level@></small></h4>
			<@ if (empCount < employeeTotalCount) { @>
				<p class="label label-warning">Available only to <@=empCount@> of your <@=employeeTotalCount@> employees.</p>
			<@ } else{ @>
				<p class="label label-info">Available to all of your <@=employeeTotalCount@> employees.</p>
			<@ } @>

			<@ if (notAffodableCount >= empCount) { @>
				<p class="label label-important">Not affordable to all of your <@=empCount@> employees.</p>
			<@ } else if (notAffodableCount > 0) { @>
				<p class="label label-warning">Not affordable to <@=notAffodableCount@> of your employees.</p>
			<@ } else if (empCount == employeeTotalCount){ @>
				<p class="label label-info">Affordable to all of your <@=empCount@> employees.</p>
			<@ } else{ @>
				<p class="label label-info">Affordable to <@=empCount@> employees.</p>
			<@ } @>
		</td>
        <td class="span3">Employer's Total Cost :  <h4>$<@=employerTotalCost@> <small>per month</small></h4><a id="plan<@=id@>-details" href="#" onClick="toggleDetails(<@=id@>)">View Details</a></td>
	</tr>
	<tr>
    	<td style="padding:0" colspan="5">
        	<div style="display: none;" id="plan<@=id@>">       
            	<div class="plans-section-header">
                	<h3>Costs</h3>
				</div>
				<div style="position:relative" class=" row-fluid">
					<table class="table plan-details ">
                    	<tbody>
							<tr>
                          		<td class="span4 txt-right">Employer's Total Cost</td>
                          		<td class=""><h4>$<@=employerTotalCost@> <small>per month</small></h4></td>
							</tr>
                          	<tr>
                            	<td class="txt-right">Contribution towards employees</td>
                            	<td><h4>$<@=employerContrForEmp@> <small>per month</small></h4></td>
                          	</tr>
                          	<@ if (taxPenalty > 0) { @>
								<tr>
    	                    		<td class="txt-right">Estimated Tax Penalty</td>
        	                    	<td><h4>$<@=taxPenalty@> <small>per month</small></h4></td>
								</tr>
							<@ } else if (estimatedTaxCredit > 0){ @>
								<tr>
    	                    		<td class="txt-right">Estimated Tax Credit</td>
        	                    	<td><h4>$<@=estimatedTaxCredit@> <small>per month</small></h4></td>
								</tr>
							<@ } @>
                          	<tr>
                            	<td class="txt-right">Average Premium per Employee</td>
                            	<td><h4>$<@=avgPremium@> <small>per month</small></h4></td>
                          	</tr>
                          	<tr>
                            	<td class="txt-right">Employer Contributes</td>
                            	<td><h4>$<@=employerContrForEmpInPreimum@> <small>per month</small></h4></td>
                          	</tr>
                          	<tr>
                            	<td class="txt-right">Employee Pays</td>
                            	<td><h4>$<@=employeePayInPreimum@> <small>per month</small></h4></td>
                          	</tr>
						</tbody>
					</table>
				</div>
                    
                        
                <div class="plans-section-header">
                	<h3>Benefits</h3>
				</div>
                  
				<div style="position:relative" class=" row-fluid">
					<table class="table plan-details ">
                    	<tbody>
							<tr>
                            	<td class="span4 txt-right">Deductible</td>
                            	<td><h4>$<@=deductible @></h4></td>
                          	</tr>
                           	<tr>
                         		<td class="txt-right">Annual Out-of-Pocket Limit</td>
                          		<td><h4>$<@=oopMax @> <small>Excl.dedutible</small></h4></td>
                        	</tr>
                          	<tr>
                            	<td class="txt-right">Coninsurance</td>
                            	<td><h4><@=coInsurance @>%</h4></td>
                          	</tr>
                          	<tr>
                           		<td class="txt-right">Doctor Visits</td>
                            	<td><h4>$<@=officeVisit @></h4></td>
                          	</tr>
						</tbody>
					</table>
				</div><!--plans-section-content--> 
			</div> <!--plan-details-->
		</td>
	</tr>
</script>

<script type="text/html" id="tmpLevelCart">
	<tr>	
		<td style="vertical-align:middle"><@=srNo@></td>
		<td style="vertical-align:middle">
        	<div class="control-group">
            	<div class="controls">
                	<label class="checkbox inline">
                    	<input type="checkbox" id="item_<@=id@>"/>
					</label>
				</div>
			</div>
		</td>
        <td style="text-align:center"> <h3><@=level @> Level</h3></td>
        <td style="vertical-align:middle"> <h4 class=""><@=planCount @> Plans</h4></td>
		<td style="vertical-align:middle">Employer's Total Cost Ranges From <h4>$<@=fromEmployerTotalCost @> <small>per month to </small>$<@=toEmployerTotalCost @> <small>per month</small></h4></td>
	</tr>
</script>

<script type="text/javascript">

$(document).ready(function(){
	$.ajaxSetup({ cache: false });
	
	$('body').css('height',$(window).height()); // set body height to window height

	$(".collapse").collapse();
	
	$("input").each(function(i){ 
		$(this).attr('checked', false);
	});

	$('a.btn').popover({
		placement:'left',
		
	});
});

function toggleDetails(id) {
	if( $('#plan'+id+'-details').html()=='View Details'){
		$('#plan'+id).show( 'fade');
    	$('#plan'+id+'-details').html('Hide Details');
	}else{
   	 	$('#plan'+id).hide( 'fade');
    	$('#plan'+id+'-details').html('View Details');
	}
}
</script>
<script src="<c:url value="/resources/js/jshashtable-2.1_src.js" />"></script>
<script src="<c:url value="/resources/js/jquery.numberformatter-1.2.3.js" />"></script>
<script src="<c:url value="/resources/js/tmpl.js" />"></script>
<script src="<c:url value="/resources/js/jquery.dependClass-0.1.js" />"></script>
<script src="<c:url value="/resources/js/draggable-0.1.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.8.22.custom.min.js" />"></script>
    
<script src="<c:url value="/resources/js/underscore-min.js" />"></script>
<script src="<c:url value="/resources/js/backbone-min.js" />"></script>
<script src="<c:url value="/resources/js/json2.js" />"></script>
<script src="<c:url value="/resources/js/plandisplay/showcart.js" />"></script>
<!--Backbone.Cart-->
<script src="<c:url value="/resources/js/backbone-cart.js" />"></script>

<!--Models/Collections-->
<script src="<c:url value="/resources/js/plandisplay/models/cart.js" />"></script>
<script src="<c:url value="/resources/js/plandisplay/collections/CartCollection.js" />"></script>

<!--Views-->
<script src="<c:url value="/resources/js/plandisplay/views/CartView.js" />"></script>
