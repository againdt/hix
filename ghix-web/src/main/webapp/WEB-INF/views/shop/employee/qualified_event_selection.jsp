<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
  <div id="qualified-event" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="myModalLabel">Report Change in Circumstances</h3>
  </div>
  <form class="form=horizontal" id="reportChange-form">                
      <df:csrfToken/>
      <div class="modal-body">
          <h4>Please provide information about qalifying event(s) you wish to report.</h4>
          <p>Based on the information you provide, you may be eligiblie for Special Enrollment in which you can add/remove family members from coverage or change your selected plan.</p>
        <div class="control-group" id="report-change" >
          <div class="controls" id="select-event">
            <fieldset>
              <legend>Event Type</legend>
                  <div class="row-fluid">
                      <label class="checkbox span5" for="marriage">
                        <input type="checkbox" name="reportChange" id="marriage" value="Marriage">
                            Marriage
                      </label>
                      <span data-date-format="mm/dd/yyyy" class="input-append date pull-right hide">
                          <input type="text" id="marriage-date" name="employeeDetails[0].dob" class="span7" aria-label="Date of Birth" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                      </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span5" for="birth">
                      <input type="checkbox" name="reportChange" id="birth" value="Birth of Child">
                      Birth of Child
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="birth-date" name="employeeDetails[0].dob" class="span7" aria-label="Date of Birth" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span5" for="adoption">
                      <input type="checkbox" name="reportChange" id="adoption" value="Adoption">
                      Adoption
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="adoption-date" name="employeeDetails[0].dob" class="span7" aria-label="Adoption" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span5" for="divorce">
                      <input type="checkbox" name="reportChange" id="divorce" value="Divorce">
                      Divorce
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="divorce-date" name="employeeDetails[0].dob" class="span7" aria-label="Divorce" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span5" for="seperation">
                      <input type="checkbox" name="reportChange" id="seperation" value="Legal Seperation">
                      Legal Separation
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="seperation-date" name="employeeDetails[0].dob" class="span7" aria-label="Seperation" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span5" for="death">
                      <input type="checkbox" name="reportChange" id="death" value="Death of Family Member">
                      Death of Family Member
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="expired-date" name="employeeDetails[0].dob" class="span7" aria-label="Death" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                  <label class="checkbox span5" for="gained-eligibility">
                    <input type="checkbox" name="reportChange" id="gained-eligibility" value="Gained Eligibility for other">
                    Gained Eligibility for other
                  </label>
                  <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                    <input type="text" id="g-eligibility-date" name="employeeDetails[0].dob" class="span7" aria-label="Gained Eligibility for other" tabindex="0">
                    <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid">
                    <label class="checkbox span6" for="lost-eligibility">
                      <input type="checkbox" name="reportChange" id="lost-eligibility" value="Lost Eligibility for other Coverage">
                      Lost Eligibility for other Coverage
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="l-eligibility-date" name="employeeDetails[0].dob" class="span7" aria-label="Lost Eligibility for other" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
              </fieldset>
          </div>
        </div> <!-- /waiving-reason -->           
        
      </div><!-- /modal-body -->
      <div class="modal-footer">
          <button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>           
          <button class="btn btn-primary  pull-right" aria-hidden="true" alt="Submit">Submit</button>
      </div>
  </form>
</div><!--/.modal -->

<script src="js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('div[rel=popover]').popover({trigger:'hover'});
    $('div[rel=popover]').popover({trigger:'focus'}); 

    $('input[name=reportChange]').change(function(){ // adds and removes date field
        if( $(this).is(':checked')) {
            $(this).parent().addClass("checked"); //added for pseudo-element to work. Don't remove
            $(this).parent().siblings('span').removeClass("hide");
        } else {
            $(this).parent().removeClass("checked");
            $(this).parent().siblings('span').addClass("hide");
        }
    });
     
}); /* End document.ready() */