
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.String;


/**
 * A unique reference to a living person assigned by the United States Social Security Administration.
 *                 - Cannot be all 0's
 *                 - Cannot start with 000
 *                 - Cannot start with 666
 *                 - Cannot start with 9 (i.e. 900-999*)
 *                 - Cannot contain XXX-00-XXXX (0's in middle piece)
 *                 - Cannot contain XXX-XX-0000 (0's in the end piece)
 *                 TBD: This SSN restriction is likely to be elevated to the global  hixc namespace at some future time. 
 *                 It is understood that some exchange partners use less restrictive SSN patterns.
 *             
 * 
 * <p>Java class for PersonSSNIdentificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonSSNIdentificationType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;http://niem.gov/niem/proxy/xsd/2.0>string">
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonSSNIdentificationType")
public class PersonSSNIdentificationType
    extends String
{


}
