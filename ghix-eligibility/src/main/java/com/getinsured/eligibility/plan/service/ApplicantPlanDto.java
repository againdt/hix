package com.getinsured.eligibility.plan.service;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;

public class ApplicantPlanDto {

	private String memberGUID;
	private Float premium;
	private Integer region;
	private YorN subscriberFlag;
	
	private Long planId;
	private Float planLevelPremiumBeforeCredit;
	private Float planLevelPremiumAfterCredit;
	private Float planLevelMonthlySubsidy;
	private InsuranceType insuranceType;
	
	
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public Integer getRegion() {
		return region;
	}
	public void setRegion(Integer region) {
		this.region = region;
	}
	public YorN getSubscriberFlag() {
		return subscriberFlag;
	}
	public void setSubscriberFlag(YorN subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}
	public Long getPlanId() {
		return planId;
	}
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	public Float getPlanLevelPremiumBeforeCredit() {
		return planLevelPremiumBeforeCredit;
	}
	public void setPlanLevelPremiumBeforeCredit(Float planLevelPremiumBeforeCredit) {
		this.planLevelPremiumBeforeCredit = planLevelPremiumBeforeCredit;
	}
	public Float getPlanLevelPremiumAfterCredit() {
		return planLevelPremiumAfterCredit;
	}
	public void setPlanLevelPremiumAfterCredit(Float planLevelPremiumAfterCredit) {
		this.planLevelPremiumAfterCredit = planLevelPremiumAfterCredit;
	}
	public Float getPlanLevelMonthlySubsidy() {
		return planLevelMonthlySubsidy;
	}
	public void setPlanLevelMonthlySubsidy(Float planLevelMonthlySubsidy) {
		this.planLevelMonthlySubsidy = planLevelMonthlySubsidy;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getMemberGUID() {
		return memberGUID;
	}
	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
}
