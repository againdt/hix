package com.getinsured.iex.hub.ifsv.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

//import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.VerificationDetailType;
import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_core.ResponseMetadataType;


/**
 * Wrapper class for com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.VerificationDetailType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class VerificationDetailTypeWrapper implements JSONAware{
	
	private TaxReturnTypeWrapper taxReturn;
	private List<ResponseMetadataWrapper> responseMetadata;
	
	private static final String TAX_RETURN_KEY = "TaxReturn";
	private static final String RESPONSE_METADATA_KEY = "ResponseMetadata";
	
	public VerificationDetailTypeWrapper(VerificationDetailType verificationDetailType){
		
		this.taxReturn = new TaxReturnTypeWrapper(verificationDetailType.getTaxReturn());
		
		for(ResponseMetadataType responseMetadataRecord : verificationDetailType.getResponseMetadata()){
			ResponseMetadataWrapper responseMetadataWrapper = new ResponseMetadataWrapper(responseMetadataRecord);
			this.getResponseMetadata().add(responseMetadataWrapper);
		}
	}

	public TaxReturnTypeWrapper getTaxReturn() {
		return taxReturn;
	}

	public List<ResponseMetadataWrapper> getResponseMetadata() {
		
		if(this.responseMetadata == null){
			this.responseMetadata = new ArrayList<ResponseMetadataWrapper>();
		}
		
		return responseMetadata;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.taxReturn != null){
			obj.put(TAX_RETURN_KEY, this.taxReturn);
		}
		if(this.responseMetadata != null){
			obj.put(RESPONSE_METADATA_KEY, this.responseMetadata);
		}
		return obj.toJSONString();
	}
	
}
