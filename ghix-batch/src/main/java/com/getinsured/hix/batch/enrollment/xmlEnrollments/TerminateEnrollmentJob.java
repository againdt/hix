package com.getinsured.hix.batch.enrollment.xmlEnrollments;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.task.EnrollmentCancelTask;
import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIException;


public class TerminateEnrollmentJob implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentJob.class);
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private EnrollmentBatchService enrollmentBatchService;
	
	 private String getDateTime(Date date) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
			return df.format(date);
	}
	 
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setErrorMessage("UpdateEnrollmentByXMLJob failed for some or all files.");
		try {
		String timeStamp = getDateTime(chunkContext.getStepContext().getStepExecution().getJobExecution().getCreateTime());
		
		LOGGER.info("UpdateEnrollmentByXMLTask.execute : START");
		String inbound834XMLPath =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUND834XMLPATH);
		
		String failureFilePath = inbound834XMLPath + File.separator+EnrollmentConstants.FAILURE_FOLDER_NAME + File.separator+timeStamp;
		enrollmentBatchEmailDTO.setFileLocation(failureFilePath);
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);
		List<String> failureFiles= enrollmentBatchService.getFileNamesInAFolder(failureFilePath, EnrollmentConstants.FILE_TYPE_XML);
		
		if(failureFiles!=null && failureFiles.size()>0){
			LOGGER.info("UpdateEnrollmentByXMLTask.emailnotification : START");
			LOGGER.error("UpdateEnrollmentByXML  JOB failed for following files ::: "+failureFiles);
			enrollmentBatchEmailDTO.setErrorCode(201);
			enrollmentBatchEmailDTO.setErrorMessage("UpdateEnrollmentByXMLJob failed for some or all files.");
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			LOGGER.info("UpdateEnrollmentByXMLTask.emailnotification : END");
		}
		
		LOGGER.info("UpdateEnrollmentByXMLTask.execute : END");
		
		} catch (Exception e) {
			LOGGER.error("UpdateEnrollmentByXMLTask.execute : Failed"+e.getCause(),e);
			enrollmentBatchEmailDTO.setErrorCode(201);
			if(enrollmentBatchEmailDTO.getErrorMessage()==null){
				enrollmentBatchEmailDTO.setErrorMessage(e.getMessage());
			}
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			throw new GIException("UpdateEnrollmentByXMLTask :: Failed to execute : "+e.getMessage(),e);
		}
		return RepeatStatus.FINISHED;
	}	
	
	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
			return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}
}
