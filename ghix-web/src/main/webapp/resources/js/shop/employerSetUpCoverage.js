
/*  GET PLAN DATA FROM PLAN-MGMT  */
function loadPlanData(){

	
	 $('#missinginfo').modal('show');
	 var url =  '/hix/shop/employer/setupcoverage/getEmployerQuotingPlans';
   $.ajax({
		url:url, 
		type: "POST", 
		dataType:'json',
		success: function(response)
		{
			if(response['FAILURE']){
				$('#errorDiv').show();
				$('#nextbutton').hide();
				$('#dataDiv').hide();
			}else{
				planDataJSON = response['PREMIUMDATAJSON'];
				tierDataJSON = response['TIERDATAJSON'];
				$('#employeeCount').val(response['EmployeeCount']);
				$('#dependentCount').val(response['DependentCount']);
				prepareTierDivs(tierDataJSON, planDataJSON);
			}
			 $('#missinginfo').modal('hide');
		},
		error: function(e){
			$('#errorDiv').show();
			$('#nextbutton').hide();
			$('#dataDiv').hide();
			$('#missinginfo').modal('hide');
		}
	});
}
/**
 * Prepare All Tier divs in Proper order.
 * 
 * @param tierData
 */
function prepareTierDivs(tierDataJSON, planDataJSON){
	var finalString="";
		var tierData = $.parseJSON(tierDataJSON);

		
	var lowestTier = (tierData["BRONZE"] !='') ? "BRONZE" :((tierData['SILVER'] !='') ? "SILVER" : ((tierData["GOLD"] !='') ? "GOLD" : "PLATINUM" ));
	if(selectedTier==''){
		selectedTier = lowestTier;
	}
	
	if(tierData['BRONZE']){
		finalString +="<div class='planChoice pull-left'>";
		finalString +="<input type='radio' name='employeePlanChoice'  id='employeePlanChoice_1' value='BRONZE' class='pull-left' ";
		if('BRONZE' == selectedTier){
			finalString +=" checked='checked'";
		}
		finalString +=" onclick=\"loadTierPlans('BRONZE','"+ tierData['BRONZE']['PLANS'].length +"','');\">"; 
		finalString +="	<label for='employeePlanChoice1'><span class='margin5-l'>BRONZE</span></label>";
		finalString +="	<div class='margin20-l'>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Coverage Level</small><br>";
		finalString +="			<strong>Least Comprehensive</strong>";	
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Premium Range</small><br>";
		finalString +="			<strong>$"+tierData['BRONZE']['RANGE'][0]+"-$"+tierData['BRONZE']['RANGE'][1]+"/mo</strong>";
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Choice:</small><br>";
		finalString +="			<strong><a class='planChoices' href='#' onclick=\"showPlanDetails('BRONZE', tierDataJSON, planDataJSON );\">"+ tierData['BRONZE']['PLANS'].length +" Plans</a></strong>";
		finalString +="		</p>";
		finalString +="	</div>";                       			
		finalString +="</div>";
	}
	if(tierData['SILVER']){
		finalString +="<div class='planChoice pull-left'>";
		finalString +="<input type='radio' name='employeePlanChoice'  id='employeePlanChoice_1' value='SILVER' class='pull-left' ";
		if('SILVER' == selectedTier){
			finalString +=" checked='checked'";
		}

		finalString +=" onclick=\"loadTierPlans('SILVER','"+ tierData['SILVER']['PLANS'].length +"','');\">"; 
		finalString +="	<label for='employeePlanChoice1'><span class='margin5-l'>SILVER</span></label>";
		finalString +="	<div class='margin20-l'>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Coverage Level</small><br>";
		finalString +="			<strong>Somewhat Comprehensive</strong>";	
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Premium Range</small><br>";
		finalString +="			<strong>$"+tierData['SILVER']['RANGE'][0]+"-$"+tierData['SILVER']['RANGE'][1]+"/mo</strong>";
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Choice:</small><br>";
		finalString +="			<strong><a class='planChoices' href='#' onclick=\"showPlanDetails('SILVER', tierDataJSON, planDataJSON );\">"+ tierData['SILVER']['PLANS'].length +" Plans</a></strong>";
		finalString +="		</p>";
		finalString +="	</div>";                       			
		finalString +="</div>";
	}
	if(tierData['GOLD']){
		finalString +="<div class='planChoice pull-left'>";
		finalString +="<input type='radio' name='employeePlanChoice'  id='employeePlanChoice_1' value='GOLD' class='pull-left' ";
		if('GOLD' == selectedTier){
			finalString +=" checked='checked'";
		}

		finalString +=" onclick=\"loadTierPlans('GOLD','"+ tierData['GOLD']['PLANS'].length +"','');\">"; 
		finalString +="	<label for='employeePlanChoice1'><span class='margin5-l'>GOLD</span></label>";
		finalString +="	<div class='margin20-l'>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Coverage Level</small><br>";
		finalString +="			<strong>More Comprehensive</strong>";	
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Premium Range</small><br>";
		finalString +="			<strong>$"+tierData['GOLD']['RANGE'][0]+"-$"+tierData['GOLD']['RANGE'][1]+"/mo</strong>";
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Choice:</small><br>";
		finalString +="			<strong><a class='planChoices' href='#' onclick=\"showPlanDetails('GOLD', tierDataJSON, planDataJSON );\">"+ tierData['GOLD']['PLANS'].length +" Plans</a></strong>";
		finalString +="		</p>";
		finalString +="	</div>";                       			
		finalString +="</div>";
	}
	if(tierData['PLATINUM']){
		finalString +="<div class='planChoice pull-left'>";
		finalString +="<input type='radio' name='employeePlanChoice'  id='employeePlanChoice_1' value='PLATINUM' class='pull-left' ";
		if('PLATINUM' == selectedTier){
			finalString +=" checked='checked'";
		}

		finalString+=" onclick=\"loadTierPlans('PLATINUM','"+ tierData['PLATINUM']['PLANS'].length +"','');\">"; 
		finalString +="	<label for='employeePlanChoice1'><span class='margin5-l'>PLATINUM</span></label>";
		finalString +="	<div class='margin20-l'>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Coverage Level</small><br>";
		finalString +="			<strong>Most Comprehensive</strong>";	
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Premium Range</small><br>";
		finalString +="			<strong>$"+tierData['PLATINUM']['RANGE'][0]+"-$"+tierData['PLATINUM']['RANGE'][1]+"/mo</strong>";
		finalString +="		</p>";
		finalString +="		<p class='font10 margin10-t'>";
		finalString +="			<small>Choice:</small><br>";
		finalString +="			<strong><a class='planChoices' href='#' onclick=\"showPlanDetails('PLATINUM', tierDataJSON, planDataJSON );\"> "+ tierData['PLATINUM']['PLANS'].length +" Plans</a></strong>";
		finalString +="		</p>";
		finalString +="	</div>";                       			
		finalString +="</div>";
	}
	
	
	$('#selectedTierPlanCount').html(tierData[selectedTier]['PLANS'].length);
	$('#tierDataDiv').html(finalString);
	//loadTierPlans(selectedTier,tierData[selectedTier]['PLANS'].length, planSel );
	loadTierPlans(selectedTier,tierData[selectedTier]['PLANS'].length, '' );
}

function showPlanDetails(tierName, tierDataJSON, planDataJSON){
	var coverageDate = $('#coverageDateStart').val();
	coverageDate = new Date(coverageDate);
	
	var date = (coverageDate.getDate() > 9) ? coverageDate.getDate() : "0"+coverageDate.getDate() ;
	var month = (coverageDate.getMonth()+1);
	month = (month > 9) ? month : "0"+month ;
	var year = coverageDate.getFullYear();
	coverageDate = month+"/"+date+"/"+year;
	
	var tierData = $.parseJSON(tierDataJSON);
	var planDataObj = $.parseJSON(planDataJSON);
	var planIds = tierData[tierName]['PLANS'];
	var planJsonStr = '{';
	for(var i=0;i<=planIds.length-1; i++){
		var planData = planDataObj[planIds[i]];
		planJsonStr += '"'+planIds[i]+'":{';
		planJsonStr += '"avgIndiPremium":"'+planData["avgIndiPremium"]+'",';
		planJsonStr += '"planName":"'+planData["planName"]+'",';
		planJsonStr += '"totalDepePremium":"'+planData["totalDepePremium"]+'",';
		planJsonStr += '"totalIndiPremium":"'+planData["totalIndiPremium"]+'"';
		planJsonStr += '},';
	}
	planJsonStr = planJsonStr.substr(0, planJsonStr.length -1) + "}" ;
	$('#TierName').val(tierName);
	$('#CoverageDate').val(coverageDate);
	$('#PlanData').val(planJsonStr);
	
	
	$('#planDetailsFrm').submit();
}


function loadTierPlans(tier,tierSize,planId){
	var planDisplayURL = '${planDisplayUrl}'; 
	$('#planTier').val(tier);
	$('#waitDiv').modal('show');
	var tierData = $.parseJSON(tierDataJSON);
	var planDataObj = $.parseJSON(planDataJSON);
	var opString="";
	var plansObj = tierData[tier]['PLANS'];
	for(var i=0;i<plansObj.length; i++){
		if(i==0 && planId==''){
			$('#benchmarkPlan').val(plansObj[0]);
			getPlanName();
			populateEmployerCost();
		}
		else if(planId!='' && planId==plansObj[i]){
			$('#benchmarkPlan').val(plansObj[i]);
			getPlanName();
			populateEmployerCost();
		}
		var obj = planDataObj[plansObj[i]];
		
		opString +="<li  id="+plansObj[i]+ "";
		if(i==0 && planId==''){
			opString+=" class='planTabs active'";
		}
		else if(planId!='' && planId==plansObj[i]){
			opString+=" class='planTabs active'";
		}else{
			opString+=" class='planTabs'";
		}
		
		//opString +=" onclick='savePlan("+plansObj[i]+");' onmouseover='loadPlanName("+plansObj[i]+");' onmouseout='hidePlanName("+plansObj[i]+");' ";
        //opString+=" ><a href='#'></a></li>" Changed the code for HIX-46965
        
        opString +=" onclick='savePlan("+plansObj[i]+");' ";
        
        opString+=" ></li>";  
	}
	
	$('#costBasisPlans').html(opString);
	$('#waitDiv').modal('hide');
	$('#selectedTierPlanCount').html(tierSize);
	$('.selectedTier').html(tier);
	$('#tierPlanCount').val(plansObj.length);
	$('#tierLink').attr("href",'<c:url value="'+planDisplayURL+tier+'"/>');
	
	//Setting by default value on tier - HIX-46965
    for(var i=0;i<plansObj.length; i++){
        if(i==0 && planId==''){
            savePlan(plansObj[i]);
        }
    }  
}

function savePlan(pId){ 
	var planData = $.parseJSON(planDataJSON);
	$('#benchmarkPlan').val(pId);

	//Trim all the text on li element first and then bind the current value
    $('#costBasisPlans > li').text('');
    $('#'+pId).text('$'+Math.round(planData[pId]["avgIndiPremium"] * 10 * 10)/100);
    
	$('.planTabs').removeClass('active');
	$('#'+pId).addClass("active");
	getPlanName();
	populateEmployerCost();
}

function populateEmployerCost(){
	var planData = $.parseJSON(planDataJSON);
	var benchMarkPlan = $('#benchmarkPlan').val();
	var totalPlanCost = 0;
	var taxCredit =0;
	totalPlanCost = formatCurrency((planData[benchMarkPlan]["totalIndiPremium"] * ($("#contributionTowardsEmployees").val()/100)) + (planData[benchMarkPlan]["totalDepePremium"] * ($("#contributionTowardsDependents").val()/100)));
	if($("#contributionTowardsEmployees").val() < 50){
		taxCredit = formatCurrency(0);
	}
	else{
		taxCredit =formatCurrency(Math.round(((planData[benchMarkPlan]["avgIndiPremium"])* ($("#contributionTowardsEmployees").val()/100) * (taxCreditPercentage/100))));
	}
	
	$("#yourCostAmount").html(totalPlanCost);
	$("#taxCreditAmount").html(taxCredit );
}

function getPlanName(){

	var planData = $.parseJSON(planDataJSON);
	var selectedPlan = $('#benchmarkPlan').val();
	$('#totalIndvPremium').val(planData[selectedPlan]["totalIndiPremium"]);
	$('#totalDepePremium').val(planData[selectedPlan]["totalDepePremium"]);
	$('#avgIndvPremium').val(planData[selectedPlan]["avgIndiPremium"]);
	$('#pricingVar').html(planData[selectedPlan]["planName"]);
	$('#costBasisPlan').html(planData[selectedPlan]["planName"] + ' '+ $('#planTier').val() +''+' Plan');
}

function loadPlanName(pId){
	var planData = $.parseJSON(planDataJSON);
	$('#'+pId).html(Math.round(planData[pId]["avgIndiPremium"] * 10 * 10)/100);
}
function hidePlanName(pId){
	$('#'+pId).html('');
}
