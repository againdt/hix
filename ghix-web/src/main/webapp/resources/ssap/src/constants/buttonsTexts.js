
const ADD_ANOTHER_PERSON_TXT = 'Add Member';
const ADD_DEDUCTION_SOURCE_TXT = 'Add Deduction Source';
const ADD_EMPLOYER_DETAIL_TXT = 'Add Employer Detail';
const ADD_INCOME_SOURCE_TXT = 'Add Income Source';
const ADD_TRIBAL_INCOME = 'Add tribal income';
const BACK_TXT = 'Back';
const CANCEL_TXT = 'Cancel';
const CONTINUE_TXT = 'Continue';
const DELETE_TXT = 'Delete';
const DOWNLOAD_TXT = 'Download';
const EDIT_TXT = 'Edit';
const FIX_HOUSEHOLD_MEMBER_TXT = 'Update household member info';
const FIX_RELATIONSHIP_TXT = 'Update relationship';
const NOTHING = '';
const PRINT_TXT = 'Print';
const REMOVE_TXT = 'Remove';
const SAVE_TXT = 'Save';
const SAVE_CONTINUE_TXT = 'Save & Continue';
const SPOUSE_ADD = 'Add Spouse';
const SPOUSE_SELECT = 'Select Spouse';
const SUBMIT_APPLICATION_TXT = 'Submit Application';
const UPDATE_INFORMATION_TXT = 'Update information';

export const buttonsTextsDefaults = {
    ADD_ANOTHER_PERSON_TXT,
    ADD_EMPLOYER_DETAIL_TXT,
    ADD_DEDUCTION_SOURCE_TXT,
    ADD_INCOME_SOURCE_TXT,
    ADD_TRIBAL_INCOME,
    BACK_TXT,
    CANCEL_TXT,
    CONTINUE_TXT,
    DELETE_TXT,
    DOWNLOAD_TXT,
    EDIT_TXT,
    FIX_HOUSEHOLD_MEMBER_TXT,
    FIX_RELATIONSHIP_TXT,
    NOTHING,
    PRINT_TXT,
    REMOVE_TXT,
    SAVE_TXT,
    SAVE_CONTINUE_TXT,
    SPOUSE_ADD,
    SPOUSE_SELECT,
    SUBMIT_APPLICATION_TXT,
    UPDATE_INFORMATION_TXT
};

export const buttonsTextsIntl = Object.assign(
    {},
    ...Object.values(buttonsTextsDefaults).map(text => ({
        [text]: {
            id: `${text}_btn`,
            defaultMessage: text
        }
    }))
);

