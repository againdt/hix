import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const ValidationErrorMessage = props => {
    const {
        isVisible,
        messageText,
        errorClasses
    } = props;

    return (
        <div className="usa-grid margin-tb-5">
            <span className={`usa-offset-one-third ${isVisible ? 'shown' : 'hidden'} error--msg ${errorClasses ? errorClasses.join(' ') : ''}`}>
                <em>
                    <FontAwesomeIcon icon="exclamation"/>
                </em>
                { messageText }
            </span>
        </div>
    );
};

export default ValidationErrorMessage;
