package com.getinsured.iex.hub.vlp.wrapper.util;

import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.wrapper.Agency3InitVerifRequestTypeWrapper;

/**
 * Utility class for Agency3InitVerifRequestTypeWrapper
 * Mainly used to fix SONAR violations
 * 
 * @author - Nikhil Talreja
 * @since - 15-Apr-2014
 * 
 */
public final class Agency3InitVerifRequestTypeWrapperUtil {
	
	private Agency3InitVerifRequestTypeWrapperUtil(){
		
	}
	
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
	private static final String REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String CATEGORYCODE_KEY = "CategoryCode";
	
	public static void addNameToWrapper(JSONObject obj, Agency3InitVerifRequestTypeWrapper wrapper) throws HubServiceException{
		
		Object param = null;
		param = obj.get(FIRSTNAME_KEY);
		if(param != null){
			wrapper.setFirstName((String)param);
		}
		
		param = obj.get(MIDDLENAME_KEY);
		if(param != null){
			wrapper.setMiddleName((String)param);
		}
		
		param = obj.get(LASTNAME_KEY);
		if(param != null){
			wrapper.setLastName((String)param);
		}
		
		param = obj.get(DATEOFBIRTH_KEY);
		if(param != null){
			try {
				wrapper.setDateOfBirth((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
	}
	
	public static void addIndicatorsToWrapper(JSONObject obj, Agency3InitVerifRequestTypeWrapper wrapper){
		
		Object param = null;
		param = obj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY);
		if(param != null){
			wrapper.setFiveYearBarApplicabilityIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTSPONSORDATAINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestSponsorDataIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTGRANTDATEINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestGrantDateIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTERCOMMENTSFORHUB_KEY);
		if(param != null){
			wrapper.setRequesterCommentsForHub((String)param);
		}
		
		param = obj.get(CATEGORYCODE_KEY);
		if(param != null){
			wrapper.setCategoryCode((String)param);
		}
		
	}
	
}
