package com.getinsured.hix.enrollment.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.dto.enrollment.Enrollment1095DTO;
import com.getinsured.hix.dto.enrollment.Enrollment1095Response;
import com.getinsured.hix.dto.enrollment.Enrollment1095SearchFilter;
import com.getinsured.hix.enrollment.service.Enrollment1095EditService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @author shinde_dh
 *
 */
@Controller
@RequestMapping(value="/enrollment/1095A")
public class Enrollment1095EditController {
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095EditController.class);
	 
	@Autowired private Enrollment1095EditService enrollment1095EditService;
	private static ObjectMapper mapper = new ObjectMapper();
	private static final String ENROLLMENT1095_DATA_PERMISSION = "hasPermission(#model,'A_1095','1095_ENROLLMENT_DATA')";
	private static final String ID = "id";
	private static final String EDIT_TOOL_JSON = "editToolJson";
	
	/*@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		LOGGER.info("Testing Enrollment1095");
		return "Testing Enrollment1095 Successful";
	}*/
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/searchmember", method = RequestMethod.GET)
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	public String search(Model model) {
		return "enrollment1095/searchmember";
	}
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@GiAudit(transactionName = "searchMembers", eventType = EventTypeEnum.ENROLLMENT_1095, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Search Members", notes = "This method will search Enrollment 1095 Members")
	@RequestMapping(value = "/searchmember", method = RequestMethod.POST )
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	@ResponseBody public String search(HttpServletRequest request) {
		LOGGER.info("Search Enrollment1095");
		String requestJson = request.getParameter(EDIT_TOOL_JSON);
		try 
		{
			requestJson = requestJson.replaceAll("&quot;", "\"");
			Enrollment1095SearchFilter enrollment1095SearchFilter = mapper.readValue(requestJson, Enrollment1095SearchFilter.class);
			Map<String , Object> dataMap = enrollment1095EditService.getEnrollment1095SearchResult(enrollment1095SearchFilter);
			Enrollment1095Response response = new Enrollment1095Response();
			//List<Enrollment1095DTO> list = (List<Enrollment1095DTO>) dataMap.get("data");
			response.setData((List<Enrollment1095DTO>) dataMap.get("data"));
			response.setTotalRecords((Integer) dataMap.get("totalRecords"));
			Gson gson = new Gson();
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),
					new GiAuditParameter("searchCriteria", requestJson));
			return gson.toJson(response);
		} 
		catch (Exception e) 
		{
			 GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					 new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Occurred while searching member"));
			LOGGER.error("Error in searchmember " + e.getMessage());
			return null;
		}
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@GiAudit(transactionName = "updateResendFlag", eventType = EventTypeEnum.ENROLLMENT_1095, eventName = EventNameEnum.UPDATE)
	@ApiOperation(value = "Update Resend Flag", notes = "This method will update Enrollment_1095 PDF Resend Flag.")
	@RequestMapping(value = "/resend", method = RequestMethod.POST)
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	@ResponseBody public String updateResendFlag(HttpServletRequest request) {
		LOGGER.info("Search Enrollment1095");
		Integer enrollment1095Id = Integer.valueOf(request.getParameter(ID));
		try 
		{
			boolean chk = enrollment1095EditService.updateResendFlag(enrollment1095Id);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, chk ? GiAuditor.SUCCESS : GiAuditor.FAILURE));
			GiAuditParameterUtil.add(new GiAuditParameter("enrollment1095ID", enrollment1095Id),
                    new GiAuditParameter("updateResendFlagStatus", chk));
			if(chk){
				JsonObject jsonObj = new JsonObject();
				jsonObj.addProperty("message", "SUCCESS");
				return jsonObj.toString();
			}
			else {
				return "FAIL";
			}
		} 
		catch (Exception e) 
		{
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					new GiAuditParameter("enrollment1095ID", enrollment1095Id),
					new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occurred while updating ResendFlag") );
			LOGGER.error("Error in update resend" + e.getMessage());
			return "FAIL";
		}
	}
	
	
	/**
	 * 
	 * @param model
	 * @param enrollment1095Id
	 * @return
	 */
	@GiAudit(transactionName = "getEnrollment1095ById", eventType = EventTypeEnum.ENROLLMENT_1095, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Get Enrollment_1095 By Id", notes = "This method will fetch Enrollment_1095 record by ID.")
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	public String getEnrollment1095ById(Model model , @PathVariable(ID) final Integer enrollment1095Id) {
		LOGGER.info("Inside getById");
		try 
		{
			Enrollment1095DTO enDto = enrollment1095EditService.getDetailsById(enrollment1095Id);
			model.addAttribute("enrollment1095Details", mapper.writeValueAsString(enDto));
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),
					new GiAuditParameter("enrollment1095ID", enrollment1095Id));
			
			return "enrollment1095/details";
		} 
		catch (Exception e) 
		{ 
			LOGGER.error("Error in getById" + e.getMessage());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					new GiAuditParameter("enrollment1095ID", enrollment1095Id),
					new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occurred while fetching Enrollment1095 By ID"));
			return null;
		}
	}
	
	/**
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@GiAudit(transactionName = "findEnrollment1095ById", eventType = EventTypeEnum.ENROLLMENT_1095, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Find Enrollment_1095 By Id", notes = "This method will fetch Enrollment_1095 record by ID.")
	@RequestMapping(value = "/getDetailById", method = RequestMethod.POST)
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	@ResponseBody public String findEnrollment1095ById(Model model , HttpServletRequest request) {
		Integer enrollment1095Id = Integer.valueOf(request.getParameter(ID));
		LOGGER.info("Inside getDetailById");
		try 
		{
			Enrollment1095DTO enDto = enrollment1095EditService.getDetailsById(enrollment1095Id);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),
					new GiAuditParameter("enrollment1095ID", enrollment1095Id));
			return mapper.writeValueAsString(enDto);			
		} 
		catch (Exception e) 
		{ 
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					new GiAuditParameter("enrollment1095ID", enrollment1095Id),
					new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching Enrollment1095 By ID"));
			LOGGER.error("Error in getDetailById" + e.getMessage());
			return null;
		}
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@GiAudit(transactionName = "updateEnrollment1095", eventType = EventTypeEnum.ENROLLMENT_1095, eventName = EventNameEnum.UPDATE)
	@ApiOperation(value = "updateEnrollment1095", notes = "Update enrollment 1095 record")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@PreAuthorize(ENROLLMENT1095_DATA_PERMISSION)
	@ResponseBody public String updateEnrollment1095(HttpServletRequest request) {
		LOGGER.info("Inside updateEnrollment1095");
		try 
		{
			String requestJson = request.getParameter(EDIT_TOOL_JSON);
			requestJson = requestJson.replaceAll("&quot;", "\"");
			Enrollment1095DTO enrollment1095dto = mapper.readValue(requestJson, Enrollment1095DTO.class);
			boolean chk = enrollment1095EditService.updateEnrollment1095(enrollment1095dto);
			
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, chk ? GiAuditor.SUCCESS: GiAuditor.FAILURE),
					new GiAuditParameter("enrollment1095ID", enrollment1095dto.getId()),
                    new GiAuditParameter("updateResendFlagStatus", chk));
			
			if(chk){
				Enrollment1095DTO enDto = enrollment1095EditService.getDetailsById(enrollment1095dto.getId());
				return mapper.writeValueAsString(enDto);
			}
			else{
				return "FAIL";
			}
		} 
		catch (Exception e) 
		{
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Occurred while updating Enrollment1095"));
			LOGGER.error("Error in updateEnrollment1095" + e.getMessage());
			return "FAIL";
		}
	}
}