<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
 
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">
	$(function() {
		$(".newcommentiframe").click(function(e){
	        e.preventDefault();
	        var href = $(this).attr('href');
	        if (href.indexOf('#') != 0) {
	        	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal();
			}
		});
	});
	
	function closeCommentBox() {
		$("#newcommentiframe").remove();
		window.location.reload(true);
	}
	function closeIFrame() {
		$("#newcommentiframe").remove();
		window.location.reload(true);
	}
</script>

<div class="gutter10-lr">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/employers"/>"><spring:message code="label.employers"/></a></li>
			<li><spring:message code="label.brkactive"/></li>
			<li>${employer.name}</li>
			<li><spring:message code="label.agent.employers.Comments"/></li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->
	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	

<!--  Latest UI -->
<div class="gutter10">
	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>
</div>
 <div class="row-fluid">
	    <div id="sidebar" class="span3">
	       <div class="nav nav-list">
		      <h1>${employer.name}</h1>
		   </div>
		</div>
	 </div>
		<%-- <h1>${employer.name} <small>&nbsp;<spring:message code="label.brkcontactname"/>: ${employer.contactFirstName} ${employer.contactLastName}</small></h1>
	 --%>
	 <div class="row-fluid">
			<div id="sidebar" class="span3">
					<div class="header">
						<h4><spring:message code="label.agent.employers.aboutThisEmployer"/></h4>
					</div>
						<ul class="nav nav-list">
	<%-- 			                  <li class="active"><a href="<c:url value="/shop/employer/employercasesummary/${employer.id}"/>">Summary</a></li> --%>
	<%-- 			                  <li><a href="<c:url value="/shop/employer/employercasecontactinfo/${employer.id}"/>">Contact Info</a></li> --%>
							<li><a href="/hix/shop/employer/employercase/${employer.id}"><spring:message code="label.agent.employers.Summary"/></a></li>
		                 	<li><a href="/hix/shop/employer/employercontactinfo/${employer.id}"><spring:message code="label.agent.employers.contactInformation"/></a></li>
		                    <c:if test="${'NM' ne stateCode && 'MS' ne stateCode}">
		                  	<%-- <li>
		                  		<c:url value="/inbox/secureInboxSearch" var="searchURL">
								   <c:param name="searchText" value="${employer.contactFirstName} ${employer.contactLastName}"/>
								</c:url>
								<a href="${searchURL}"><spring:message code="label.agent.employers.Messages"/></a>
							</li> --%>
							</c:if>
		                <!-- <li><a href="#">Notifications</a></li> -->
		                  	<li class="active"><spring:message code="label.agent.employers.Comments"/></li>
		                </ul>
				<br>
					<div class="header">
						<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
					</div>
						<ul class="nav nav-list">
		                  	<c:choose>
								<c:when test="${showSwitchRolePopup == \"N\"}">
									<c:choose>
										<c:when test="${isCACall == true}">
							                  <li><a href="<c:url value="${switchAccUrl}&_pageLabel=employerHomePage&ahbxId=${employer.id}&ahbxUserType=0&recordId=${brokerId}&recordType=2"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
										</c:when>
										<c:otherwise>
							                  	<c:if test="${showPopupInFuture == null}">  
													<li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
												</c:if>
												<c:if test="${showPopupInFuture != null}">
								 					<li><a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
												</c:if>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
					                  <c:choose>
										<c:when test="${isCACall == true}">
											<li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
										</c:when>
										<c:otherwise>
											<c:if test="${showPopupInFuture == null}">
												<li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>  
											</c:if>
											<c:if test="${showPopupInFuture != null}">
								 				<li><a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
		                 	<%-- <li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"><i class="icon-envelope-unread"></i> <spring:message code="label.agent.employers.ComposeMessage"/></a></li>
		                  	 --%>
		                  	 <li><a name="addComment" href="<c:url value="/shop/employer/newcomment?target_id=${employer.id}&target_name=DESIGNATEBROKER&employerName=${employer.name}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.agent.employers.NewComment"/></a></li>
		                  	 <c:if test="${employerActivated == false}">
								<li><a href="<c:url value="/broker/sendActivationLink/${employer.id}"/>" class=""><spring:message code="label.sendActivationLink"/></a></li>
							 </c:if>
		                </ul>
		</div>
		<!-- Modal -->
		<div id="rightpanel" class="span9">
		
			<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST"">
			<df:csrfToken/>
			<div class="header">
			  <h4 class="span11"><spring:message code="label.agent.employers.comments"/></h4>
		    </div>		
					<%-- <thead>
						<tr class="graydrkaction">
							<th scope="col"><h4><spring:message code="label.agent.employers.Comments"/></h4></th>
							<th scope="col" class="txt-right"><a class="btn btn-primary showcomments"><spring:message code="label.brkaddcomments"/></a></th>
						</tr>
					</thead> --%>
					<table class="table table-border-none">
					<tbody>
						<tr>
							<td colspan="2" class="">					
								<comment:view targetId="${employer.id}" targetName="DESIGNATEBROKER">
								</comment:view>
								
								<jsp:include page="../../platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="DESIGNATEBROKER" id="target_name" name="target_name" /> 
								<input type="hidden" value="${employer.id}" id="target_id" name="target_id" />
								
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="employerName" id="employerName" value="${employer.name}" />
			</form>
		</div>
	</div>	
</div>	
		 
		<!-- Modal -->
			<jsp:include page="employerviewmodal.jsp" />
		<!-- Modal end -->
<!--  Latest UI -->

