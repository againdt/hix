package com.getinsured.iex.hub.nmec.wrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ApplicantType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.PersonNameType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.PersonSSNIdentificationType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.RestrictedProperNameType;
import com.getinsured.iex.hub.nmec.niem.niem.fbi._2.SEXCodeSimpleType;
import com.getinsured.iex.hub.nmec.niem.niem.fbi._2.SEXCodeType;
import com.getinsured.iex.hub.nmec.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.nmec.niem.niem.proxy.xsd._2.Date;

public class ApplicantTypeWrapper implements JSONAware{

    private PersonSSNIdentificationType personSSNIdentificationType;
    private DateType personBirthDateType;
    private JAXBElement<SEXCodeType> personSex;
    private ApplicantType applicantType = null;
    
    private static com.getinsured.iex.hub.nmec.niem.niem.niem_core._2.ObjectFactory niemCoreFactory;
	private static com.getinsured.iex.hub.nmec.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	private static com.getinsured.iex.hub.nmec.niem.niem.fbi._2.ObjectFactory fbiFactory;
	private static com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ObjectFactory vnemFactory;
	private PersonNameType personNameType = null;
	private RestrictedProperNameType personGivenNameType = null;
	private RestrictedProperNameType personMiddleNameType = null;
	private RestrictedProperNameType personSurnameType = null;
	private JAXBElement<Date> personBirthDateJAXBElement;
	private SEXCodeType personSexCodeType = null;
	
	private String personBirthDate = null;
	private String personGivenName = null;
	private String personMiddleName = null;
	private String personSurName = null;
	private String personNameSuffixText = null;
	private String personSSNIdentification = null;
	private String personSexCode = null;

	
	
	public void setPersonSexCode(String personSexCode){
		
		if(personSexCodeType == null){
			personSexCodeType = fbiFactory.createSEXCodeType();
		}
		personSexCodeType.setValue(SEXCodeSimpleType.fromValue(personSexCode));
		if(personSex == null)
		{
			personSex = niemCoreFactory.createPersonSexCode(personSexCodeType);
		}
		else
		{
			this.personSex.setValue(personSexCodeType);
		}
		this.personSexCode = personSexCode;
		
		if(this.applicantType != null)
		{
			applicantType.setPersonSex(personSex);
		}
	}
	
	public String getPersonSexCode(){
		if(this.personSexCodeType != null){
			return this.personSexCode;
		}
		return null;
	}
	
	public ApplicantTypeWrapper(){
		niemCoreFactory = new com.getinsured.iex.hub.nmec.niem.niem.niem_core._2.ObjectFactory();
		fbiFactory = new com.getinsured.iex.hub.nmec.niem.niem.fbi._2.ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.nmec.niem.niem.proxy.xsd._2.ObjectFactory();
		vnemFactory = new com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ObjectFactory();
		
		this.personNameType = vnemFactory.createPersonNameType();
		personBirthDateType = niemCoreFactory.createDateType();
		applicantType = vnemFactory.createApplicantType();
	}
	
	public ApplicantTypeWrapper(ApplicantType applicantType){
		if(applicantType != null){
			PersonNameType nameType = applicantType.getPersonName();
			if(nameType != null){
				this.personGivenName = nameType.getPersonGivenName().getValue();
				RestrictedProperNameType middleNameType = nameType.getPersonMiddleName();
				//Middle Name is optional/ check for null;
				if(middleNameType != null){
					this.personMiddleName = middleNameType.getValue();
				}
				this.personSurName = nameType.getPersonSurName().getValue();
				this.personNameSuffixText = nameType.getPersonNameSuffixText();
			}
			Date bDate = (Date) applicantType.getPersonBirthDate().getDateRepresentation().getValue();
			GregorianCalendar calDate = bDate.getValue().toGregorianCalendar(); 
			this.personBirthDate = calDate.getTime().toString();
			SEXCodeType personSCode = (SEXCodeType) applicantType.getPersonSex().getValue();
			this.personSexCode = personSCode.getValue().toString();
			this.personSSNIdentification = applicantType.getPersonSSNIdentification().getValue();
		}
	}
	
	public void setPersonBirthDate(String mmddyyyy) throws DatatypeConfigurationException, ParseException{

		if(mmddyyyy!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			java.util.Date javaDate = sdf.parse(mmddyyyy);

			Date date = proxyFactory.createDate();
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
			date.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED));
			this.personBirthDateJAXBElement = niemCoreFactory.createDate(date);
			personBirthDateType.setDateRepresentation(personBirthDateJAXBElement);
			personBirthDate = mmddyyyy;
			if(this.applicantType != null)
			{
				applicantType.setPersonBirthDate(personBirthDateType);
			}
		}
	}
	
	public String getPersonBirthDate(){
		if(this.personBirthDateType != null){
			
			return personBirthDate;
			
		}
		return null;
	}
	
	public void setPersonGivenName(String personGivenName){
		if(personGivenName != null && personGivenName.trim().length() > 0){
			if(personGivenNameType == null){
				personGivenNameType = vnemFactory.createRestrictedProperNameType();
			}
			
			personGivenNameType.setValue(personGivenName);
			personNameType.setPersonGivenName(personGivenNameType);
			
			this.personGivenName = personGivenName;
			if(this.applicantType != null)
			{
				this.applicantType.setPersonName(personNameType);
			}
		}
	}
	
	public String getPersonGivenName(){
		if(this.personNameType != null){
			return this.personGivenName;
		}
		return null;
	}
	
	public void setPersonMiddleName(String personMiddleName){

		if(personMiddleName != null && personMiddleName.trim().length() > 0){
			
			if(personMiddleNameType == null)
			{
				personMiddleNameType = vnemFactory.createRestrictedProperNameType();
			}
			personMiddleNameType.setValue(personMiddleName);
			personNameType.setPersonMiddleName(personMiddleNameType);
			
			this.personMiddleName = personMiddleName;
			if(this.applicantType != null)
			{
				applicantType.setPersonName(personNameType);
			}
		}
	}
	
	public String getPersonMiddleName(){
		if(this.personNameType != null){
			return this.personMiddleName;
		}
		return null;
	}
	
	public void setPersonSurName(String personSurName){

		if(personSurName != null && personSurName.length() > 0){
			
			if(personSurnameType == null)
			{
				personSurnameType = vnemFactory.createRestrictedProperNameType();
			}
			personSurnameType.setValue(personSurName);
			personNameType.setPersonSurName(personSurnameType);
			
			this.personSurName = personSurName;
			if(this.applicantType != null)
			{
				applicantType.setPersonName(personNameType);
			}
		}
	}
	
	public String getPersonSurName(){
		if(this.personNameType != null){
			return this.personSurName;
		}
		return null;
	}
	
	public String getPersonNameSuffixText() {
		return personNameSuffixText;
	}

	public void setPersonNameSuffixText(String personNameSuffixText) {
		if(personNameSuffixText!=null){
			this.personNameType.setPersonNameSuffixText(personNameSuffixText);
			this.personNameSuffixText = personNameSuffixText;
		}
	}

	public String getPersonSSNIdentification() {
		return personSSNIdentification;
	}

	public void setPersonSSNIdentification(String personSSNIdentification) {
		if(personSSNIdentificationType  == null){
			personSSNIdentificationType = vnemFactory.createPersonSSNIdentificationType();
			
			personSSNIdentificationType.setValue(personSSNIdentification);
			this.personSSNIdentification = personSSNIdentification;			
			if(this.applicantType != null)
			{
				applicantType.setPersonSSNIdentification(personSSNIdentificationType);
			}
		}
	}

	public ApplicantType getSystemRepresentation(){
		return this.applicantType;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PersonSSNIdentification", this.personSSNIdentification);
		obj.put("PersonBirthDate",personBirthDate);
		obj.put("PersonGivenName",personGivenName);
		obj.put("PersonMiddleName", personMiddleName);
		obj.put("PersonSurName",personSurName);
		obj.put("PersonNameSuffixText", personNameSuffixText);
		obj.put("PersonSSNIdentification", personSSNIdentification);
		return obj.toJSONString();
	}
	
}
