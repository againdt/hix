(function (module) {
    'use strict';

    module.controller('mapController', ['$scope', 'Doctors', 'InfowindowModel', function ($scope, Doctors, InfowindowModel) {
        var vm = this;
        vm.mapData = null;
        vm.map = null;
        vm.bounds = new google.maps.LatLngBounds();
        vm.markers = [];
        vm.createMarker = createMarker;
        vm.clear_markers = clear_markers;
        vm.infowindow = new google.maps.InfoWindow();

        $scope.$on('openInfowindow', function (event, data) {
            if (angular.isNumber(data.index) && data.doctor) {
                Doctors.mouseoverDoctor(data.doctor);
                setTimeout(function () {
                    vm.infowindow.setContent(InfowindowModel.getHtmlContent());
                    vm.infowindow.open(vm.map, vm.markers[data.index]);
                });
            }
        });

        function createMarker (data, index) {
            var myLatLng = new google.maps.LatLng(data.getLat(), data.getLon()),
                newIndex = (Doctors.currentPage() - 1) * Doctors.pageSizeRange() + index,
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: vm.map,
                    doctor: data,
                    icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + newIndex + '|FE6256|000000'
                });
            google.maps.event.addListener(marker, 'mouseover', (function (marker) {
                return function () {
                    $scope.$apply(function () {
                        Doctors.mouseoverDoctor(marker.doctor);
                    });
                    vm.infowindow.setContent(InfowindowModel.getHtmlContent());
                    vm.infowindow.open(vm.map, marker);
                }
            })(marker));

            vm.bounds.extend(myLatLng);
            vm.markers.push(marker);
        }


        function clear_markers() {
            for(var i in vm.markers) {
                vm.markers[i].setMap(null);
            }
            vm.markers.length = 0;
            // setting LatLngBounds to null resets the current bounds
            // and allows the new call for zoom in/out to be made directly
            // against the latest markers to be plotted on the map
            vm.bounds = new google.maps.LatLngBounds(null);
        }

    }]);

    module.directive('map', ['Doctors', function (Doctors) {
        var mapElem,
            mapObj;
        return {
            restrict: 'E',
            replace: true,
            controller: 'mapController',
            controllerAs: 'map',
            templateUrl: 'map.html',
            link: link
        };

        //////////

        function link(scope, elem, attrs, ctrl) {
            scope.$on('createMap', function () {
                ctrl.clear_markers();
                _.each(Doctors.getDoctors(), function (data, index) {
                    ctrl.createMarker(data, index + 1);
                });
                ctrl.map.fitBounds(ctrl.bounds);
            });
            if (!ctrl.map) {
                mapElem = $('#google-map-canvas');
                ctrl.map = new google.maps.Map(mapElem[0]);
                ctrl.map.setCenter(new google.maps.LatLng(37.409548, -122.081473));
                mapObj = ctrl.map;
            }

            var calMapWithDebounce = _.debounce(calculateMapWidth, 300);
            $(window).resize(function() {
                mapElem.hide();
                calMapWithDebounce();
            });

            calculateMapWidth();
            $('#map-modal').on('shown.bs.modal', function () {
                calculateMapWidth();
            });
        }

        function calculateMapWidth() {
            var modalWidth = $('.provider-map').width(),
                paginationWidth = 295 + 4; // border of left and right and seperation
            $('.google-map').width(modalWidth - paginationWidth);
            mapElem.show();
            if (mapObj) {
                google.maps.event.trigger( mapObj, 'resize' );
            }
        }
    }]);

}(angular.module('ProviderMap.Directive.map', [
  'ProviderMap.Factory.doctors',
  'ProviderMap.Directive.infowindow'
])));