
package com.getinsured.ahbx.platform.ecm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultElements" type="{http://jaxb.dev.java.net/array}anyTypeArray" minOccurs="0"/>
 *         &lt;element name="estimatedHitCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="query" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchResult", propOrder = {
    "resultElements",
    "estimatedHitCount",
    "query"
})
public class SearchResult {

    protected AnyTypeArray resultElements;
    protected int estimatedHitCount;
    protected String query;

    /**
     * Gets the value of the resultElements property.
     * 
     * @return
     *     possible object is
     *     {@link AnyTypeArray }
     *     
     */
    public AnyTypeArray getResultElements() {
        return resultElements;
    }

    /**
     * Sets the value of the resultElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyTypeArray }
     *     
     */
    public void setResultElements(AnyTypeArray value) {
        this.resultElements = value;
    }

    /**
     * Gets the value of the estimatedHitCount property.
     * 
     */
    public int getEstimatedHitCount() {
        return estimatedHitCount;
    }

    /**
     * Sets the value of the estimatedHitCount property.
     * 
     */
    public void setEstimatedHitCount(int value) {
        this.estimatedHitCount = value;
    }

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuery(String value) {
        this.query = value;
    }

}
