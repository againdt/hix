const initialState = 'en';

import { languageActionTypes } from '../constants/actionTypes';

export default (state = initialState, action) => {
    switch(action.type) {
    case languageActionTypes.CHANGE_LANGUAGE:
        return action.language;
    case languageActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};