
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * Response Status from HUB to Requestor. This is spawned by the HUB
 *             
 * 
 * <p>Java class for ResponseMetadataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseMetadataType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ResponseCode"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ResponseDescriptionText"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}TDSResponseDescriptionText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseMetadataType", propOrder = {
    "responseCode",
    "responseDescriptionText",
    "tdsResponseDescriptionText"
})
public class ResponseMetadataType
    extends ComplexObjectType
{

    @XmlElement(name = "ResponseCode", required = true)
    protected String responseCode;
    @XmlElement(name = "ResponseDescriptionText", required = true)
    protected String responseDescriptionText;
    @XmlElement(name = "TDSResponseDescriptionText")
    protected String tdsResponseDescriptionText;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseDescriptionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDescriptionText() {
        return responseDescriptionText;
    }

    /**
     * Sets the value of the responseDescriptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDescriptionText(String value) {
        this.responseDescriptionText = value;
    }

    /**
     * Gets the value of the tdsResponseDescriptionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTDSResponseDescriptionText() {
        return tdsResponseDescriptionText;
    }

    /**
     * Sets the value of the tdsResponseDescriptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTDSResponseDescriptionText(String value) {
        this.tdsResponseDescriptionText = value;
    }

}
