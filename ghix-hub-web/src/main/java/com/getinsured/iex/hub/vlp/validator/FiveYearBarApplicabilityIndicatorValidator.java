package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for five year bar indicator
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class FiveYearBarApplicabilityIndicatorValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FiveYearBarApplicabilityIndicatorValidator.class);
	
	/** 
	 * Validations for five year bar indicator
	 * Should be "true" or "false"
	 * 
	 * Required : Yes
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String fiveYearBarApplicabilityIndicator = null;
		
		//Value is mandatory
		if(value == null || value.toString().trim().length() == 0){
			throw new InputDataValidationException(VLPServiceConstants.FIVE_YEAR_BAR_REQUIRED_ERROR_MESSAGE);
		}
		else{
			fiveYearBarApplicabilityIndicator = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if (!fiveYearBarApplicabilityIndicator.matches(VLPServiceConstants.FIVE_YEAR_BAR_REGEX)){
			throw new InputDataValidationException(VLPServiceConstants.FIVE_YEAR_BAR_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
