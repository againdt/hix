<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>
<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<div class="gutter10">
	<div class="row-fluid">
		<div class="span10 offset1" id="rightpanel">
				<div class="box-loose txt-center">
					Your account has been locked due to several unsuccessful attempts to change password.<br> <br>
					<a href="<c:url value="/account/user/login"/>"
					class="btn btn-primary">Login</a>
				</div>
		</div>
	</div>
</div>