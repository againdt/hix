package com.getinsured.hix.tkm.notification;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * Helper class to send Notification email in ticket Management flow.
 *
 * @author Satyakam kaul
 *
 */
@Component
@Scope("prototype")
public class TkmTicketNotificationTemplate extends NotificationAgent {
	private Map<String, String> singleData;
	

	private TkmTickets tkmTickets;
	
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	

	@Override
	public Map<String, String> getSingleData() {
		String exchangeName=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		String exchangeUrl = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		Map<String,String> bean = new HashMap<String, String>();

		bean.put("exchangeName", exchangeName );
		bean.put("ticketNumber", tkmTickets.getNumber());
		bean.put("ticketSubject", tkmTickets.getSubject());
		bean.put("ticketStatus",  getTiketStatus(tkmTickets.getStatus()));
		bean.put("exchangeUrl", exchangeUrl);
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
		bean.putAll(singleData);
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		if (singleData != null)
		{
			data.put("To", singleData.get("recipient"));
			data.put("Subject", singleData.get("subject")+" on " + exchangeName);
			data.putAll(singleData);
		}
		return data;
	}

	private String getTiketStatus(String status) {
		if("UnClaimed".equalsIgnoreCase(status)){
			return "created";
		}
		else if("Canceled".equalsIgnoreCase(status)){
			return "cancelled";
		}
		else if("Resolved".equalsIgnoreCase(status)){
			return "resolved";
		}
		else if("Open".equalsIgnoreCase(status)){
			return "opened";
		}
		
		return "";
		
	}

	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}

	public TkmTickets getTkmTickets() {
		return tkmTickets;
	}

	public void setTkmTickets(TkmTickets tkmTickets) {
		this.tkmTickets = tkmTickets;
	}

}