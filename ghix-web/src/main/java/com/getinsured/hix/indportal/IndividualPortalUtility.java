package com.getinsured.hix.indportal;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesOverrideDTO;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesRequestDto;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto;
import com.getinsured.eligibility.ui.dto.EligibilityProgramDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;
import com.getinsured.eligibility.ui.enums.ApplicantEligibilityStatus;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.admin.service.AnnouncementService;
import com.getinsured.hix.admin.util.AnnouncementDTO;
import com.getinsured.hix.admin.util.AnnouncementDTO.ModuleName;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.cap.util.CapPortalConfigurationUtil;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.dto.eligibility.RenewalRequest;
import com.getinsured.hix.dto.eligibility.RenewalResponse;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderRequestDTO;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.MonthlyAPTCAmountDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm.PlanMetalLevel;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.ApplicantEventsDTO;
import com.getinsured.hix.indportal.dto.ApplicationEligibilityDetails;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.CustomGroupingResponse;
import com.getinsured.hix.indportal.dto.DisenrollDialogData;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.indportal.dto.HouseholdEligibilityDetails;
import com.getinsured.hix.indportal.dto.IndAdditionalDetails;
import com.getinsured.hix.indportal.dto.IndAppeal;
import com.getinsured.hix.indportal.dto.IndPortalAddlnRegDetails;
import com.getinsured.hix.indportal.dto.IndPortalApplicantDetails;
import com.getinsured.hix.indportal.dto.IndPortalContactUs;
import com.getinsured.hix.indportal.dto.IndPortalEligibilityDateDetails;
import com.getinsured.hix.indportal.dto.IndPortalSpecialEnrollDetails;
import com.getinsured.hix.indportal.dto.IndPortalValidationDTO;
import com.getinsured.hix.indportal.dto.MemberEligibilityDetails;
import com.getinsured.hix.indportal.dto.MemberEligibilityStatus;
import com.getinsured.hix.indportal.dto.MemberEligibilityStatusComparator;
import com.getinsured.hix.indportal.dto.MyAppealDetails;
import com.getinsured.hix.indportal.dto.MyApplications;
import com.getinsured.hix.indportal.dto.MyApplicationsEntry;
import com.getinsured.hix.indportal.dto.MyApplicationsEntryComparator;
import com.getinsured.hix.indportal.dto.PendingReferralDetails;
import com.getinsured.hix.indportal.dto.PlanDetails;
import com.getinsured.hix.indportal.dto.PlanSummaryDetails;
import com.getinsured.hix.indportal.dto.PlanSummaryMember;
import com.getinsured.hix.indportal.dto.PlanTile;
import com.getinsured.hix.indportal.dto.PlanTileRequest;
import com.getinsured.hix.indportal.dto.PreferenceEnrollResponse;
import com.getinsured.hix.indportal.dto.SEPOverrideDTO;
import com.getinsured.hix.indportal.dto.SEPOverrideSEPDatesDTO;
import com.getinsured.hix.indportal.dto.SEPOverrideSEPEventsDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardApplicantDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardConfigurationDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardEnrolleeDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardEnrolleeDTO.RelationEnum;
import com.getinsured.hix.indportal.dto.dashboard.DashboardEnrollmentDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardPlanDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardPlanDTO.PlanTypeEnum;
import com.getinsured.hix.indportal.dto.dashboard.DashboardPreEligibilityDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardSepEventDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardTabDTO;
import com.getinsured.hix.indportal.enums.DisenrollReasonEnum;
import com.getinsured.hix.indportal.service.IndividualPortalEnrollmentIntegrationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsRequest.IndAppealReason;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentAttributeEnum;
import com.getinsured.hix.model.enrollment.EnrollmentDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.estimator.mini.PreEligibilityResults;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.client.SsapApplicantRequest;
import com.getinsured.iex.client.SsapApplicationAccessor;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.dto.ind19.Ind19ClientResponse;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.indportal.dto.IndDisenroll;
import com.getinsured.iex.indportal.dto.IndDisenroll.TerminationDateChoice;
import com.getinsured.iex.indportal.dto.ReinstateEnrollmentDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TSLocalTime;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;
/**
 *
 * @author Sunil Desu
 *
 */
@Component
public class IndividualPortalUtility extends IndividualPortalCommons{
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalUtility.class);

	@Autowired
	private AccountActivationService accountActivationService;

	@Autowired
	private AnnouncementService announcementService;

	@Autowired
	private TicketMgmtUtils ticketMgmtUtils;

	@Autowired
	private ConsumerPortalUtil consumerUtil;

	@Autowired
	private DesignateService designateService;

	@Autowired
	private BrokerService brokerService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private ResourceCreator resourceCreator;

	@Autowired
	private SsapApplicationAccessor ssapApplicationAccessor;

	@Autowired
	private EERestCallInvoker restClassCommunicator;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	public IndividualPortalEnrollmentIntegrationService individualPortalEnrollmentIntegrationService;

	@Autowired
	private CoverageStartDateService coverageStartDateService;

	@Autowired
    private LookupService lookUpService;

	@Autowired
	public PreferencesService preferencesService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private IndividualPortalEnrollmentIntegrationService enrollmentIntegrationService;

    @Autowired
    private CSROverrideUtility csrOverrideUtility;

    @Autowired
    private PlanDisplayRestClient planDisplayRestClient;

    @Autowired private Gson platformGson;
    @Autowired private IndividualPortalUtil individualPortalUtil;

    @Autowired 
    private CapPortalConfigurationUtil capPortalConfigurationUtil;
    
	public AccountActivationService getAccountActivationService() {
		return accountActivationService;
	}

	public TicketMgmtUtils getTicketMgmtUtils() {
		return ticketMgmtUtils;
	}

	public ConsumerPortalUtil getConsumerUtil() {
		return consumerUtil;
	}

	public DesignateService getDesignateService() {
		return designateService;
	}

	public BrokerService getBrokerService() {
		return brokerService;
	}

	public RoleService getRoleService(){
		return roleService;
	}

	public ResourceCreator getResourceCreator(){
		return resourceCreator;
	}

	public SsapApplicationAccessor getSsapApplicationAccessor(){
		return ssapApplicationAccessor;
	}

	public EERestCallInvoker getEERestCallInvoker() {
		return restClassCommunicator;
	}

	public static final String IND19_LOG_CAP_HISTORY_FOR_ESD_ERROR_CODE = "19000002";

	public static final String IND_19_GENERIC_ERROR = "IND_19_GENERIC_ERROR";
	public static final String IND_19_ESD_ERROR = "IND_19_ESD_ERROR";
	public static List<String> CHILD_RELATIONSHIP_CODES = Arrays.asList("09", "17", "19");
	public static List<String> SPOUSE_RELATIONSHIP_CODES = Arrays.asList("01");
	public static List<String> SELF_RELATIONSHIP_CODES = Arrays.asList("18");
	public static List<String> ACTIVE_ENROLLMENT_STATUS = Arrays.asList("CONFIRM", "PENDING", "TERM");
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	private static final String SEP_OTHER_EVENT = "OTHER";
	private static final String activeRenewalEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ACTIVE_RENEWAL_ENABLED);
    String[] eligibilityTypes = new String[]{"AssessedCHIPEligibilityType","AssessedMedicaidMAGIEligibilityType","AssessedMedicaidNonMAGIEligibilityType"};
    List<String> eligibilityTypeList = Arrays.asList(eligibilityTypes);

	/**
	 * Prepopulates the communication preference page
	 *
	 * @param model
	 * @param household
	 * @throws GIException
	 */
	public void prepopulateCommPrefPage(Model model, int activeModuleId) throws Exception{

        model.addAttribute("dateOfBirth","");
        model.addAttribute("location", null);

        PreferencesDTO communicationPreferenceDTO = getHouseholdPreferences(activeModuleId, false);

		model.addAttribute("prefSpokenLang", communicationPreferenceDTO.getPrefSpokenLang());
		model.addAttribute("prefWrittenLang", communicationPreferenceDTO.getPrefWrittenLang());
		model.addAttribute("prefContactMethod", communicationPreferenceDTO.getPrefCommunication().getMethod());
		model.addAttribute(IndividualPortalConstants.PHONE_NUMBER, communicationPreferenceDTO.getPhoneNumber());
		model.addAttribute("dateOfBirth", communicationPreferenceDTO.getDob());
		model.addAttribute("location", communicationPreferenceDTO.getLocationDto());
		
		String showPaperless1095Option = "OFF";
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREFERENCES_SHOWPAPERLESS1095OPTION))) {
			showPaperless1095Option = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREFERENCES_SHOWPAPERLESS1095OPTION);
		}
		model.addAttribute("showPaperless1095Option", showPaperless1095Option);
	}

	/**
	 * Populates multi year info and returns coverage year
	 * for dashboard
	 *
	 * @param model
	 * @param httpServletRequest
	 * @return
	 * @throws InvalidUserException
	 */
	public int populateMultiYearTabInfo(List<DashboardTabDTO> tabs, Model model, HttpServletRequest httpServletRequest) throws InvalidUserException{

		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

        boolean showPreviousYearTab = true;
        boolean showCurrentYearTab = false;

        String previousYearTabCutOff = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_PREVIOUS_YEAR_TAB);
        String currentYearTabCutOff = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB);
        String currentYearTabCutOffPriviledgeUser=DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB_PRIVILEDGE_USER);
        if(currentYearTabCutOffPriviledgeUser !=null && currentYearTabCutOffPriviledgeUser.trim().length() >0 && !StringUtils.equalsIgnoreCase(userService.getDefaultRole(userService.getLoggedInUser()).getName(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE)){
        	currentYearTabCutOff=currentYearTabCutOffPriviledgeUser;
        }
        String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
        String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
        String exchangeStartYear = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR);

        int prevYear=0;
		if (StringUtils.isNumeric(previousYear)) {
			prevYear = Integer.parseInt(previousYear);
		}

	    int currYear=0;
		if (StringUtils.isNumeric(currentYear)) {
			currYear = Integer.parseInt(currentYear);
		}

		int exchgStartYear = 0;
		if (StringUtils.isNumeric(exchangeStartYear)) {
			exchgStartYear = Integer.parseInt(exchangeStartYear);
		}

	    int year = TSCalendar.getInstance().get(Calendar.YEAR);

	    String coverageYear = httpServletRequest.getParameter("coverageYear");

        try{
        	LocalDate tsCurrentDate = TSLocalTime.getLocalDateInstance();
			if (StringUtils.equalsIgnoreCase(userService.getDefaultRole(userService.getLoggedInUser()).getName(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE)) {
				if (StringUtils.isNotBlank(previousYearTabCutOff)) {
					LocalDate cutOff = new DateTime(formatter.parse(previousYearTabCutOff)).toLocalDate();
					showPreviousYearTab = cutOff.isAfter(tsCurrentDate) || cutOff.equals(tsCurrentDate);
				}
			}
            if(StringUtils.isNotBlank(currentYearTabCutOff)){
                LocalDate cutOff = new DateTime(formatter.parse(currentYearTabCutOff)).toLocalDate();
                showCurrentYearTab = tsCurrentDate.isAfter(cutOff) || cutOff.equals(tsCurrentDate);
                if(showCurrentYearTab){
                	year = currYear;
                }
            }
        }
        catch(ParseException e){
            LOGGER.error("Error while parsing tab cut off dates",e);
        }
        Integer renewalCoverageYear = null;

		if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
            int computedYear = Integer.parseInt(coverageYear);

            if((computedYear == prevYear && showPreviousYearTab)||
                    (computedYear == currYear && showCurrentYearTab)){
                year = computedYear;
            }

        }else{
        	
        	if(showPreviousYearTab && (prevYear >= exchgStartYear)){
        		DashboardTabDTO priviousYearTab = new DashboardTabDTO();
            		priviousYearTab.setApplicationYear(Integer.toString(prevYear));
            		priviousYearTab.setIsInsideOEEnrollmentWindow(false);
            		priviousYearTab.setIsInsideOEWindow(false);
            		tabs.add(priviousYearTab);
            	}
            if(showCurrentYearTab && (currYear >= exchgStartYear)){
            	DashboardTabDTO currentYearTab = new DashboardTabDTO();
            		currentYearTab.setApplicationYear(Integer.toString(currYear));
            		currentYearTab.setIsInsideOEEnrollmentWindow(individualPortalUtil.isInsideOEEnrollmentWindow(currYear));
            		currentYearTab.setIsInsideOEWindow(individualPortalUtil.isInsideOEWindow(currYear));
            		tabs.add(currentYearTab);
            	}
            
            }             

        int currentYearOnServer = TSLocalTime.getLocalDateInstance().getYear();
		if (year == currYear && currentYearOnServer < currYear) {
			 model.addAttribute("notifyCurrentYear", "true");
			 model.addAttribute("displayYear", previousYear);
		}
		if(year == prevYear && showCurrentYearTab){
			 model.addAttribute("notifyPrevYear", "true");
			 model.addAttribute("displayYear", currentYear);
		}

        model.addAttribute("showPreviousYearTab",showPreviousYearTab);
        model.addAttribute("showCurrentYearTab",showCurrentYearTab);

        model.addAttribute("previousCoverageYear", prevYear);
        model.addAttribute("currentCoverageYear", currYear);
        model.addAttribute("renewalCoverageYear", renewalCoverageYear);

        if(tabs.isEmpty()){
        	DashboardTabDTO currentYearTab = new DashboardTabDTO();
        	currentYearTab.setIsInsideOEEnrollmentWindow(individualPortalUtil.isInsideOEEnrollmentWindow(year));
        	currentYearTab.setIsInsideOEWindow(individualPortalUtil.isInsideOEWindow(year));
    		currentYearTab.setApplicationYear(Integer.toString(year));
    		tabs.add(currentYearTab);
        }

        return year;
	}

	
	/**
	 * Populates favorite plan information for dashboard
	 *
	 * @param eligLead
	 * @param model
	 * @param year
	 * @param household
	 */
	public void populateFavoritePlanInfo(EligLead eligLead, Model model, int year, Household household, DashboardPreEligibilityDTO dashboardPreEligibilityDTO, DashboardTabDTO dashboardTabDTO){
	    try {
            if(null != eligLead){
            	String stage = IndividualPortalConstants.INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.get(String.valueOf(eligLead.getStage()));
            	if ((Integer.parseInt(stage) > IndividualPortalConstants.TWO) || ("CA".equalsIgnoreCase(stateCode))) {
                    Map<String, PlanResponse> selectedPlanPMResponse = getFavoritePlanDetails(eligLead.getId(),year);
                    addSelectedPlanDetailsToModel(model, household, selectedPlanPMResponse,eligLead, year, dashboardPreEligibilityDTO, dashboardTabDTO);
                    GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
            	}                
            }
        } catch (Exception ex) {
            LOGGER.error("Error occured while processing selected plan details");
            model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, false);
            GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while processing selected plan details"));

        }
	}

	/**
	 * Populates agent promotion information
	 *
	 * @param designateAssister
	 * @param designateBroker
	 * @param user
	 * @param httpServletRequest
	 */
	public void populateAgentPromoInfo(DesignateAssister designateAssister,
	        DesignateBroker designateBroker, AccountUser user, HttpServletRequest httpServletRequest, boolean isDashboardCall){

	    boolean hasNoAssister =
                designateAssister == null || designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString());

        boolean hasNoBroker =
                designateBroker == null || designateBroker.getStatus().toString().equals(DesignateBroker.Status.InActive.toString());

        boolean isIndividual = false;
        if (StringUtils.equalsIgnoreCase(user.getDefRole().getName(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE)) {
            isIndividual = true;
        }

        if(hasNoAssister && hasNoBroker && isIndividual){
        	if(isDashboardCall)
            {
            httpServletRequest.getSession().setAttribute("showAgentPromotion", true);
            }
        }else{
        	if(isDashboardCall)
            {
            httpServletRequest.getSession().removeAttribute("showAgentPromotion");
            }
        }

	}

	/**
	 * Populates the model attributes for display on Ind Portal
	 * dashboard
	 *
	 * @param model
	 * @param preEligResults
	 * @param household
	 * @param coverageYear
	 * @param userName 
	 * @param tab 
	 * @throws GIException
	 */
	public void populateModelValuesForDisplay(DashboardConfigurationDTO dashboardConfigurationDTO, Model model, PreEligibilityResults preEligResults,
			Household household,int coverageYear, DashboardPreEligibilityDTO dashboardPreEligibilityDTO, String userName, DashboardTabDTO dashboardTabDTO) throws GIException{

		model.addAttribute(IndividualPortalConstants.STAGE,IndividualPortalConstants.INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.get(String.valueOf(preEligResults.getStage())));
		model.addAttribute(IndividualPortalConstants.FIRST_NAME,household.getFirstName());
		model.addAttribute(IndividualPortalConstants.LAST_NAME,household.getLastName());
		if(household.getNameSuffix()!=null && !household.getNameSuffix().isEmpty())
		model.addAttribute("suffix", household.getNameSuffix());
		model.addAttribute("prescreenerComplete",false);
		//long coverageYear = TSCalendar.getInstance().get(Calendar.YEAR);
		if (Integer.parseInt(IndividualPortalConstants.INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.get(String.valueOf(preEligResults.getStage())))
		        <= IndividualPortalConstants.THREE) {
			if(!"WELCOME".equalsIgnoreCase(String.valueOf(preEligResults.getStage()))){
				model.addAttribute("prescreenerComplete",true);
			}
			String aptc = preEligResults.getAptc();
			if(StringUtils.contains(aptc, "$")){
				aptc = StringUtils.join(aptc,"0");
				dashboardTabDTO.setAptc(Float.valueOf(StringUtils.remove(aptc, "$")));
			}else if(NumberUtils.isParsable(aptc)){
				dashboardTabDTO.setAptc(Float.valueOf(aptc));
			}


			//model.addAttribute(IndividualPortalConstants.APTC, eligLead.getAptc()+((eligLead.getAptc()!=null && eligLead.getAptc().contains("$"))?"0":""));
			model.addAttribute(IndividualPortalConstants.APTC,aptc);
			model.addAttribute(IndividualPortalConstants.CSR, preEligResults.getCsr());
			dashboardPreEligibilityDTO.setAptc(aptc);
			dashboardPreEligibilityDTO.setCsrLevel(preEligResults.getCsr());
			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if(preEligResults.getApiOutput()!=null && !preEligResults.getApiOutput().isEmpty()){
			    try{
			        MiniEstimatorResponse apiOutput = mapper.readValue(preEligResults.getApiOutput(), MiniEstimatorResponse.class);
			        model.addAttribute("preEligibilityMedicaid",apiOutput.getMedicaidHousehold().equalsIgnoreCase(IndividualPortalConstants.YES));
			        model.addAttribute("preEligibilityChip", findNoOfMembersForEligibilityType(apiOutput.getMembers(), "CHIP") > 0);
			        model.addAttribute("preEligibilityMedicare", findNoOfMembersForEligibilityType(apiOutput.getMembers(), "Medicare") > 0);
			        
			        setPreEligibilityMembers(apiOutput, dashboardPreEligibilityDTO);
			    }
			    catch(IOException iex){
		            LOGGER.error("IOException in parsing pre eligibility results",iex);
		        }
			}
			dashboardTabDTO.setPreEligibility(dashboardPreEligibilityDTO);
		}
		String applicationStartLabel = "Start Your Application";
		String recentApplicationStatus = "New";
		String recentApplicationId = "XXXXX";
		String enableEnrollButton = IndividualPortalConstants.FALSE;
		String postEligibility = IndividualPortalConstants.FALSE;
		String enrollCaseNumber = "";
		String enableSep = IndividualPortalConstants.FALSE;
		String sepLinkEvents = IndividualPortalConstants.FALSE;
		String sepDisenroll = IndividualPortalConstants.FALSE;
		String sepEventsNumber = "";
		String sepPlanSelection = IndividualPortalConstants.FALSE;
		//HIX-69096 : Make 'Finalize Plan Choice and Enroll" action a configurable parameter
		boolean showFinalizePlanEnrollBtn = true;

		model.addAttribute("enableSep", enableSep);
		model.addAttribute("sepLinkEvents", sepLinkEvents);
		model.addAttribute("sepDisenroll", sepDisenroll);
		model.addAttribute("sepEventsNumber", sepEventsNumber);
		model.addAttribute("daysforSEP", -1);
		model.addAttribute("sepPlanSelection", sepPlanSelection);
		model.addAttribute("enMessage","");

		/*Story HIX-58890 ::
		 *QEP: Dashboard Messaging for QEP Application
		 *Added the below else if condition to enable the QEP
		 */
		String enableQep = IndividualPortalConstants.FALSE;
		String qepLinkEvents = IndividualPortalConstants.FALSE;
		String qepDisenroll = IndividualPortalConstants.FALSE;
		String qepEventsNumber = "";
		String qepPlanSelection = IndividualPortalConstants.FALSE;

		model.addAttribute("enableQep", enableQep);
		model.addAttribute("qepLinkEvents", qepLinkEvents);
		model.addAttribute("qepDisenroll", qepDisenroll);
		model.addAttribute("qepEventsNumber", qepEventsNumber);
		model.addAttribute("daysforQEP", -1);
		model.addAttribute("qepPlanSelection", qepPlanSelection);
		model.addAttribute("qepFlag", true);
		model.addAttribute("isRecTribe", "N");
		model.addAttribute("allowChangePlan", "N");
		model.addAttribute("changePlanInSEP", "N");

		model.addAttribute("sepQepOver", "N");

		String showChangePlanInOE = "N";
		String nativeAmericanHousehold = "N";

		//OTR Flags
		model.addAttribute("enableOtr", false);

		try{
			SsapApplicationResource ssapApplicationResource = getApplicationForCoverageYearForPortalHome(Long.parseLong(Integer.toString(household.getId())),coverageYear);
			if(ssapApplicationResource!=null && ssapApplicationResource.getApplicationStatus()!=null){
				List<DashboardEnrollmentDTO> dashboardEnrollments = new ArrayList<>();
				String applicationStatus = ssapApplicationResource.getApplicationStatus();
				Long ssapApplicationId = ssapApplicationResource.getApplicationId();
				dashboardTabDTO.setSsapApplicationId(ssapApplicationId);
			    dashboardTabDTO.setApplicationStatus(applicationStatus);
			    dashboardTabDTO.setApplicationDentalStatus(ssapApplicationResource.getApplicationDentalStatus());
			    dashboardTabDTO.setApplicationType(ssapApplicationResource.getApplicationType());
			    dashboardTabDTO.setCaseNumber(ssapApplicationResource.getCaseNumber());
			    dashboardTabDTO.setEncyptedCaseNumber(ghixJasyptEncrytorUtil.encryptStringByJasypt(ssapApplicationResource.getCaseNumber()));
			    dashboardTabDTO.setIsFinancial("Y".equalsIgnoreCase(ssapApplicationResource.getFinancialAssistanceFlag()) ? true : false );
			    dashboardTabDTO.setWhereUserLeftOff(ssapApplicationResource.getCurrentPageId());
			    Map<String, PlanSummaryDetails> planDetailsMap = getPlanSummaryDetails(coverageYear, ssapApplicationResource.getCaseNumber(), dashboardEnrollments, new PlanDetails());
			    dashboardTabDTO.setEnrollments(dashboardEnrollments);
				if("OP".equalsIgnoreCase(applicationStatus)){
					applicationStartLabel = "Resume Your Application";
					recentApplicationStatus="In Progress";
				}else if("SG".equalsIgnoreCase(applicationStatus) ||
				        "SU".equalsIgnoreCase(applicationStatus)||
				        "ER".equalsIgnoreCase(applicationStatus) ||
				        "PN".equalsIgnoreCase(applicationStatus) ||
				        "CC".equalsIgnoreCase(applicationStatus) ||
				        "CL".equalsIgnoreCase(applicationStatus) ||
				        "EN".equalsIgnoreCase(applicationStatus)){
					applicationStartLabel = "View Your Application";
					recentApplicationStatus="Completed";
					enableEnrollButton = (("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus) ) &&
					        ssapApplicationResource.getAllowEnrollment()!=null &&
					                "Y".equalsIgnoreCase(ssapApplicationResource.getAllowEnrollment())) ? IndividualPortalConstants.TRUE:IndividualPortalConstants.FALSE;
					/*
					 * HIX-54308
					 * Use the dates and days to display the message on the banner for SEP applications
					 *
					 * @author - Nikhil Talreja
					 */
					Object ssapId = getIdFromCaseNumber(ssapApplicationResource.getCaseNumber());
					Long applicationId = ssapId != null? ((BigDecimal) ssapId).longValue() : null;
					SsapApplicationEvent sepEvent = getSsapApplicationEventById(applicationId);
					
					if(IndividualPortalConstants.TRUE.equalsIgnoreCase(enableEnrollButton)){
						//Get the SSAP event
						//Object ssapId = getIdFromCaseNumber(ssapApplicationResource.getCaseNumber());
						if(sepEvent!=null){
							Timestamp sepStartDate = sepEvent.getEnrollmentStartDate();
							Timestamp sepEndDate = sepEvent.getEnrollmentEndDate();
							model.addAttribute("sepStartDate", dateToString(IndividualPortalConstants.UI_DATE_FORMAT, sepStartDate));
							model.addAttribute("sepEndDate", dateToString(IndividualPortalConstants.UI_DATE_FORMAT, sepEndDate));
							
							//To find days left for SEP
							if (sepStartDate != null) {
								//Using Joda Time library that covers DST changes
								Calendar today = TSCalendar.getInstance();
								today.set(Calendar.HOUR_OF_DAY, 0);
								today.set(Calendar.MINUTE,0);
								today.set(Calendar.SECOND,0);
								today.set(Calendar.MILLISECOND, 0);
								int days = Days
										.daysBetween(
												new DateTime(today.getTime()).toLocalDate(),
												new DateTime(new Date(sepEndDate
														.getTime())).toLocalDate()).getDays();
								model.addAttribute("daysforSEP", days + 1);// Add 1 to cover current day
							}
						}
					}
					if("SG".equalsIgnoreCase(applicationStatus) || "SU".equalsIgnoreCase(applicationStatus)){
						model.addAttribute(IndividualPortalConstants.STAGE,IndividualPortalConstants.SIGNED_STAGE);
					}else if("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)){
						model.addAttribute(IndividualPortalConstants.STAGE,IndividualPortalConstants.ENROLL_STAGE);
					}

					postEligibility = (("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)) || "EN".equalsIgnoreCase(applicationStatus)) ?IndividualPortalConstants.TRUE:IndividualPortalConstants.FALSE;
					ApplicationEligibilityDetails applicationEligibilityDetails = getApplicationEligibilityDetails(ssapApplicationResource.getCaseNumber());
					
					if(applicationEligibilityDetails.getHouseholdEligibilityDetails().isEligibilityConditional()) {
						dashboardTabDTO.setEligibilityConditional(true);
					}

					String stateSubsidy = applicationEligibilityDetails.getHouseholdEligibilityDetails().getStateSubsidy();
					if (StringUtils.isNotBlank(stateSubsidy) && stateSubsidy!=null && !stateSubsidy.equals("null")){
						dashboardTabDTO.setStateSubsidy(Float.valueOf(stateSubsidy));
					}else{
						dashboardTabDTO.setStateSubsidy(null);
					}
					if("ER".equalsIgnoreCase(applicationStatus) &&
					        applicationEligibilityDetails!=null &&
					        applicationEligibilityDetails.getHouseholdEligibilityDetails()!=null &&
					        (StringUtils.isEmpty(applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc())||
					                "null".equalsIgnoreCase(applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc()))){
						model.addAttribute(IndividualPortalConstants.APTC, "N/A");
					}else if("SG".equalsIgnoreCase(applicationStatus) || "SU".equalsIgnoreCase(applicationStatus)){
						String aptc = preEligResults.getAptc();
						if(StringUtils.contains(aptc, "$")){
							aptc = StringUtils.join(aptc,"0");
							dashboardTabDTO.setAptc(Float.valueOf(StringUtils.remove(aptc, "$")));
						}else if(StringUtils.isNotBlank(aptc) && !aptc.equalsIgnoreCase("N/A")){
							dashboardTabDTO.setAptc(Float.valueOf(aptc));
						}
						//model.addAttribute(IndividualPortalConstants.APTC, eligLead.getAptc()+((eligLead.getAptc()!=null && eligLead.getAptc().contains("$"))?"0":""));
						model.addAttribute(IndividualPortalConstants.APTC, aptc);
					}
					else {
						model.addAttribute(IndividualPortalConstants.APTC, "$"+applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc());
						dashboardTabDTO.setAptc(StringUtils.isNotBlank(applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc()) && !applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc().equals("null") ? Float.valueOf(applicationEligibilityDetails.getHouseholdEligibilityDetails().getAptc()) : null);

					}
					if(("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)) || "EN".equalsIgnoreCase(applicationStatus)){
						if(applicationEligibilityDetails.getHouseholdEligibilityDetails().getCsr()!=null && applicationEligibilityDetails.getHouseholdEligibilityDetails().getCsr().trim().length()>0){
							model.addAttribute(IndividualPortalConstants.CSR, applicationEligibilityDetails.getHouseholdEligibilityDetails().getCsr());
						}else{
							model.addAttribute(IndividualPortalConstants.CSR, "N/A");
						}
					}
					if("EN".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)){
						model.addAttribute(IndividualPortalConstants.STAGE,IndividualPortalConstants.ENROLL_COMPLETED_STAGE);
						addEnrolledPlanDetailsToModel(model, household, planDetailsMap);
						/*
						 * HIX-63747 Show change plan button to Native Americans
						 */
						if(verifyChangePlanAction(planDetailsMap)){
							model.addAttribute("allowChangePlan", "Y");
						}

						//HIX-75317
						if (individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear)) {
							showChangePlanInOE = "Y";
						}
								
						//HIX-67563 -  Provide ability to the consumer to "Change Plan and Enroll"
						//for SEP period granted when all the changes are automatically processed and applied						
						Map<String,PlanSummaryDetails> enrollments = getEnrollmentDetails(Long.toString(applicationId), ssapApplicationResource.getCaseNumber());
						if(enrollments != null && !enrollments.isEmpty()){
							if(verifyChangePlanInSEPAction(ssapApplicationResource.getCaseNumber(),applicationId, enrollments) && "N".equalsIgnoreCase(showChangePlanInOE)){
								model.addAttribute("changePlanInSEP", "Y");
							}
						}							
						
					}
					//HIX-59744 Post Enrollment: Allow Native Americans to Enroll
					if(StringUtils.equalsIgnoreCase(ssapApplicationResource.getNativeAmerican(),"Y")){
						nativeAmericanHousehold = "Y";
						model.addAttribute("isRecTribe", "Y");
					}
					
					List<DashboardApplicantDTO> applicants = null;
					if(applicationId != null){
						String ssapApplicantsJson = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICANTS_BY_SSAP_ID+ "/" + applicationId, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class,null).getBody();
					if(StringUtils.isNotBlank(ssapApplicantsJson)){
							applicants = platformGson.fromJson(ssapApplicantsJson, new TypeToken<List<DashboardApplicantDTO>>() {}.getType());
					}
						dashboardTabDTO.setApplicants(applicants);
				}
					
					if("SEP".equalsIgnoreCase(ssapApplicationResource.getApplicationType()) || "QEP".equalsIgnoreCase(ssapApplicationResource.getApplicationType())){
					DashboardSepEventDTO sepEventDTO = new DashboardSepEventDTO();
					if("ER".equalsIgnoreCase(applicationStatus)){
						if(ApplicationValidationStatus.INITIAL.equals(ssapApplicationResource.getValidationStatus()) || 
								ApplicationValidationStatus.PENDING.equals(ssapApplicationResource.getValidationStatus()) || 
								ApplicationValidationStatus.PENDING_INFO.equals(ssapApplicationResource.getValidationStatus()) || 
								ApplicationValidationStatus.HMS_INTRANSIT.equals(ssapApplicationResource.getValidationStatus())){
							sepEventDTO.setIsVerified(false);
						}else{
							sepEventDTO.setIsVerified(true);
						}
					}else{
						sepEventDTO.setIsVerified(true);
					}					
				
					if(sepEvent == null){
						sepEventDTO.setIsIdentified(false);
					}else{
						sepEventDTO.setIsIdentified(true);
							dashboardConfigurationDTO.setSepStartDate(dateToString("MM/dd/yyyy", sepEvent.getEnrollmentStartDate()));
							dashboardConfigurationDTO.setSepEndDate(dateToString("MM/dd/yyyy", sepEvent.getEnrollmentEndDate()));
							Date today = new TSDate();
							boolean isPlanSelectionLogicEnabled = Boolean
									.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
											IEXConfiguration.IEXConfigurationEnum.IEX_PLAN_SELECTION_ENABLED));
							LOGGER.info("Plan selection enabled : {}", isPlanSelectionLogicEnabled);
							if (sepEvent.getEnrollmentStartDate() != null && sepEvent.getEnrollmentEndDate() != null
									&& DateUtil.checkIfInBetween(sepEvent.getEnrollmentStartDate(),
											sepEvent.getEnrollmentEndDate(), today)
									&& (!isPlanSelectionLogicEnabled || isPlanSelectionAllowed(sepEvent))) {
								dashboardTabDTO.setIsSepOpen(true);
							}else {
								dashboardTabDTO.setIsSepOpen(false);
							}
					}
					dashboardTabDTO.setSepEvent(sepEventDTO);
					}
					
					if(applicationId != null && "OE".equalsIgnoreCase(ssapApplicationResource.getApplicationType())&& "Y".equalsIgnoreCase(activeRenewalEnabled)){
						RenewalRequest renewalRequest = new RenewalRequest();
						RenewalResponse renewalResponse = null;
						renewalRequest.setApplicationId(String.valueOf(applicationId));
						String renewalResponseJson = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"renewal/getRenewalStatus", 
								userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(renewalRequest)).getBody();
						if(StringUtils.isNotBlank(renewalResponseJson)){
							renewalResponse = platformGson.fromJson(renewalResponseJson, RenewalResponse.class);
							if(renewalResponse != null && renewalResponse.getStatusCode().equals(200)){
								@SuppressWarnings("unchecked")
								Map<String,Object> renewalApplicationStatus = (Map<String, Object>) renewalResponse.getData();
								if(renewalApplicationStatus != null && !renewalApplicationStatus.isEmpty()){
									dashboardTabDTO.setHealthRenewalStatus((String) renewalApplicationStatus.get("healthRenewalStatus"));									;
									dashboardTabDTO.setDentalRenewalStatus((String) renewalApplicationStatus.get("dentalRenewalStatus"));
								}
							}
						}
					}
					
				}
				String applicationType = ssapApplicationResource.getApplicationType();
				if(("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus))
				        && "SEP".equalsIgnoreCase(applicationType)){
					evaluateSepEvents(model, ssapApplicationResource, enableSep, sepLinkEvents, sepDisenroll, sepPlanSelection);
				}
				/*Story HIX-58890 ::
				 *QEP: Dashboard Messaging for QEP Application
				 *Added the below else if condition to enable the QEP
				 */
				else if (("ER".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)) &&
				        "QEP".equalsIgnoreCase(applicationType)) {
					evaluateQepEvents(model, ssapApplicationResource, enableSep, sepLinkEvents, sepDisenroll, sepPlanSelection);
				}
				//coverageYear = ssapApplicationResource.getCoverageYear();
				enrollCaseNumber = ssapApplicationResource.getCaseNumber();
				model.addAttribute("enableEnrollOnOE", isEnrollWindowActive());

				//HIX-60647 - Show report a change button for EN applications
				model.addAttribute("applicationStatus", "");
				model.addAttribute("applicationValidationStatus", ssapApplicationResource.getValidationStatus());
				dashboardTabDTO.setValidationStatus(ssapApplicationResource.getValidationStatus());
				if("EN".equalsIgnoreCase(applicationStatus)){
					model.addAttribute("applicationStatus", "Enrolled (Or Active)");
				}
				String financialAssitanceFlag = ssapApplicationResource.getFinancialAssistanceFlag();
				if (StringUtils.equalsIgnoreCase(financialAssitanceFlag,"Y")) {
					model.addAttribute("qepFlag", false);
				}

				//OTR Flags
				if(String.valueOf(coverageYear).equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_YEAR))){
					if(!isBeforeOrAfterOTRDates(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_START_DATE),
							DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_END_DATE))){
						model.addAttribute("enableOtr", true);
						model.addAttribute("markedOtr", false);
						if(StringUtils.isNotBlank(ssapApplicationResource.getRenewalStatus()) && RenewalStatus.OTR.toString().equalsIgnoreCase(ssapApplicationResource.getRenewalStatus())){
							model.addAttribute("markedOtr", true);
						}
					}
				}

				//HIX-69096 : Make 'Finalize Plan Choice and Enroll" action a configurable parameter
				showFinalizePlanEnrollBtn = determineFinalizeEnrollButton(ssapApplicationResource);
			}
		}
		catch(Exception ex){
			LOGGER.error("Error accessing SSAP Service",ex);
			throw new GIException("Error accessing SSAP Service",ex);
		}

		Long oeApproachingdays = daysRemainingToStartEnrl(coverageYear);
		dashboardConfigurationDTO.setOeApproachingdays(oeApproachingdays.intValue());

		model.addAttribute("coverageYear", coverageYear);
		model.addAttribute("postEligibility", postEligibility);
		model.addAttribute("applicationStartLabel",applicationStartLabel);
		model.addAttribute("recentApplicationStatus", recentApplicationStatus);
		model.addAttribute("recentApplicationId", recentApplicationId);
		model.addAttribute("enableEnrollment",enableEnrollButton );
		model.addAttribute("enrollCaseNumber",enrollCaseNumber );
		model.addAttribute("daysEndOe", daysRemainingToEndEnrl(coverageYear));
		model.addAttribute("isOeApproaching", isOpenEnrollmentApproaching(coverageYear));
		model.addAttribute("daysTostartOe", oeApproachingdays);
		model.addAttribute("showChangePlanInOE", showChangePlanInOE);

		//HIX-60048
		boolean isInsideOe = individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear);
		model.addAttribute("isInsideOe", isInsideOe);
		if(StringUtils.equalsIgnoreCase(nativeAmericanHousehold,"Y")
				|| isInsideOe || showFinalizePlanEnrollBtn){
			model.addAttribute("showEnrollButton", "Y");
		}
		else{
			model.addAttribute("showEnrollButton", "N");
		}
		model.addAttribute("isInsideQEP",isInsideQEP(coverageYear));
		model.addAttribute("showCurrentYearEnrollments",isAfterCurrentOE());
		
		int noOfDaysRemainToReviewPref = 0;
		if(StringUtils.isBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_REMIND_PREFERENCES_REVIEW_DAYS))) {
			noOfDaysRemainToReviewPref = 180;
		}else if(household.getPreferencesReviewDate() != null) {
			Calendar today = TSCalendar.getInstance();
			int days = Days.daysBetween(
					new DateTime(new Date(household.getPreferencesReviewDate().getTime())).toLocalDate(),
					new DateTime(today.getTime()).toLocalDate()
				   ).getDays();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("currentDate - householdDate = "+days);
			}
			int remindPreferencesReviewDays = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_REMIND_PREFERENCES_REVIEW_DAYS));
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("remindPreferencesReviewDays = "+days);
			}
			
			noOfDaysRemainToReviewPref = remindPreferencesReviewDays - days;		
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("noOfDaysRemainToReviewPref = "+noOfDaysRemainToReviewPref);
		}
		model.addAttribute("noOfDaysRemainToReviewPref", noOfDaysRemainToReviewPref);
		
		String showPaperless1095Option = "OFF";
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREFERENCES_SHOWPAPERLESS1095OPTION))) {
			showPaperless1095Option = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREFERENCES_SHOWPAPERLESS1095OPTION);
		}
		model.addAttribute("showPaperless1095Option", showPaperless1095Option);
		
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR))){
			String exchangeStartYear = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR);
			model.addAttribute("exchangeStartYear", exchangeStartYear);
		}

	}

	public Map<String, PlanSummaryDetails> getPlanSummaryDetails(int coverageYear, String caseNumber, List<DashboardEnrollmentDTO> dashboardEnrollments, PlanDetails planDetails) {
		Map<String, PlanSummaryDetails> planSummaryDetailsMap = new HashMap<String, PlanSummaryDetails>();

		try {
			List<EnrollmentDataDTO> enrollmentDataDTOList = getEnrollmentbyhoByHouseholdcaseids(coverageYear);
			populatePlanSummaryDetails(enrollmentDataDTOList, planSummaryDetailsMap, caseNumber, dashboardEnrollments, planDetails);				
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details for SSAP id in getPlanSummaryDetails [" + coverageYear + "], : ", e);
		}
		return planSummaryDetailsMap;
	
	}
	
	private void populatePlanSummaryDetails(List<EnrollmentDataDTO> enrollmentDataDTOList, Map<String, PlanSummaryDetails> planSummaryDetailsMap, String caseNumber, List<DashboardEnrollmentDTO> dashboardEnrollments, PlanDetails planDetails) throws GIException{
		SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
		List<EnrollmentDataDTO> enrollmentForHealth = new ArrayList<EnrollmentDataDTO>(3);
		List<EnrollmentDataDTO> enrollmentForDental = new ArrayList<EnrollmentDataDTO>(3);
		List<EnrollmentDataDTO> enrollmentList = new ArrayList<EnrollmentDataDTO>(2);
		List<PlanSummaryDetails> activeHealthPlanDetails = new ArrayList<>();
		List<PlanSummaryDetails> activeDentalPlanDetails = new ArrayList<>();
		for (EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOList) {
			if(isEnrollmentActive(enrollmentDataDTO)){
				if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
					enrollmentForHealth.add(enrollmentDataDTO);
				}else if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
					enrollmentForDental.add(enrollmentDataDTO);
				}
			}
			
		}

		if(enrollmentForHealth.size()>1){
			Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getCreationTimestamp().getTime())));
		}
		if(enrollmentForHealth!= null && !enrollmentForHealth.isEmpty()){
			enrollmentList.addAll(enrollmentForHealth);
		}
		if(enrollmentForDental.size()>1){
			Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getCreationTimestamp().getTime())));
		}
		if(enrollmentForDental != null && !enrollmentForDental.isEmpty()){
			enrollmentList.addAll(enrollmentForDental);
		}
		
		for (EnrollmentDataDTO enrollmentDataDTO : enrollmentList) {
			PlanSummaryDetails planSummaryDetails = new PlanSummaryDetails();
			DashboardEnrollmentDTO dashboardEnrollmentDTO = new DashboardEnrollmentDTO();
			List<DashboardEnrolleeDTO> dashboardEnrolleeList = new ArrayList<>();

			List<EnrolleeDataDTO> enrolleeDataDTOList = enrollmentDataDTO.getEnrolleeDataDtoList();
			List<PlanSummaryMember> membersList = new ArrayList<PlanSummaryMember>(0);
			if(enrolleeDataDTOList != null && !enrolleeDataDTOList.isEmpty()){
				planSummaryDetails.setPolicyId(enrolleeDataDTOList.get(0).getCoveragePolicyNumber());				
				for (EnrolleeDataDTO enrolleeMembers : enrolleeDataDTOList) {
					PlanSummaryMember member = new PlanSummaryMember();
					DashboardEnrolleeDTO dashboardEnrolleeDTO = new DashboardEnrolleeDTO();
					dashboardEnrolleeDTO.setFirstName(enrolleeMembers.getFirstName());
					dashboardEnrolleeDTO.setLastName(enrolleeMembers.getLastName());
					dashboardEnrolleeDTO.setMiddleName(enrolleeMembers.getMiddleName() != null ? enrolleeMembers.getMiddleName() : StringUtils.EMPTY);
					dashboardEnrolleeDTO.setSuffix(enrolleeMembers.getSuffix() != null ? enrolleeMembers.getSuffix() : StringUtils.EMPTY);
					dashboardEnrolleeDTO.setGuid(enrolleeMembers.getMemberId());
					dashboardEnrolleeDTO.setMemberId(enrolleeMembers.getMemberId());
					StringBuilder name = new StringBuilder();
					name.append(enrolleeMembers.getFirstName());
					if(null != enrolleeMembers.getMiddleName() && !enrolleeMembers.getMiddleName().isEmpty()) {
						name.append(" ");
						name.append(enrolleeMembers.getMiddleName());
					}
					name.append(" ");
					name.append(enrolleeMembers.getLastName());
									
					if(null != enrolleeMembers.getSuffix() && !enrolleeMembers.getSuffix().isEmpty()) {
						name.append(" ");
						name.append(enrolleeMembers.getSuffix());
					}
					member.setName(name.toString());
					try{
						dashboardEnrolleeDTO.setDob(enrolleeMembers.getBirthDate() != null?dateFormat.format(enrolleeMembers.getBirthDate()):null);					
						member.setCoverageStartDate(dateFormat.format(enrolleeMembers.getEnrolleeEffectiveStartDate()));
						member.setCoverageEndDate(dateFormat.format(enrolleeMembers.getEnrolleeEffectiveEndDate()));
					}catch(Exception e){
						LOGGER.error("Exception occured while date conversion for enrolleeMembers ", e);
					}
					
					membersList.add(member);
					
					if(StringUtils.isNotBlank(enrolleeMembers.getRelationshipToHouseHoldContactCode())){
						dashboardEnrolleeDTO.setRelation(getRelationShip(enrolleeMembers.getRelationshipToHouseHoldContactCode()));
					}	
					dashboardEnrolleeDTO.setStatus(enrolleeMembers.getEnrolleeStatus());
					dashboardEnrolleeList.add(dashboardEnrolleeDTO);				
				}
			}
			
			dashboardEnrollmentDTO.setEnrollees(dashboardEnrolleeList);
			//HIX-63747
			planSummaryDetails.setEnrollmentCreationTimestamp(enrollmentDataDTO.getCreationTimestamp());
			planSummaryDetails.setCoveredMembers(membersList);
			planSummaryDetails.setCaseNumber(caseNumber);

			planSummaryDetails.setPlanName(enrollmentDataDTO.getPlanName());
			
			com.getinsured.hix.indportal.dto.dashboard.DashboardPlanDTO plan = new com.getinsured.hix.indportal.dto.dashboard.DashboardPlanDTO();
			plan.setPlanName(planSummaryDetails.getPlanName());
			if(null != enrollmentDataDTO.getPlanId()){
				planSummaryDetails.setPlanId(enrollmentDataDTO.getPlanId().toString());
				try {
					PlanResponse planResponse  = getPlanDetails(enrollmentDataDTO.getPlanId().toString());
					planSummaryDetails.setPlanType(planResponse.getNetworkType());
					planSummaryDetails.setOfficeVisit(planResponse.getOfficeVisit());
					planSummaryDetails.setGenericMedications(planResponse.getGenericMedications());
					planSummaryDetails.setPlanLogoUrl(planResponse.getIssuerLogo());
					planSummaryDetails.setIssuerContactPhone(planResponse.getIssuerPhone());
					planSummaryDetails.setIssuerContactUrl(planResponse.getIssuerSite());
					planSummaryDetails.setPaymentUrl(planResponse.getPaymentUrl());
					if (planSummaryDetails.getCoveredMembers().size() == 1) {
						if (StringUtils.isNotBlank(planResponse.getDeductible())) {
							planSummaryDetails.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(planResponse.getDeductible())));
						}
						if (StringUtils.isNotBlank(planResponse.getOopMax())) {
							planSummaryDetails.setOopMax(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(planResponse.getOopMax())));
						}
					} else {
						if (StringUtils.isNotBlank(planResponse.getDeductibleFamily())) {
							planSummaryDetails.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(planResponse.getDeductibleFamily())));
						}
						if (StringUtils.isNotBlank(planResponse.getOopMaxFamily())
								&& NumberUtils.isNumber(planResponse.getOopMaxFamily())) {
							planSummaryDetails.setOopMax(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(planResponse.getOopMaxFamily())));
						}
					}
					plan.setPlanId(Long.valueOf(planSummaryDetails.getPlanId()));
					plan.setPlanLevel(planResponse.getPlanLevel());
					plan.setHiosId(planResponse.getHiosIssuerId());
					plan.setIssuerName(planResponse.getIssuerName());
					
				} catch (GIException e) {
					LOGGER.error(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE, e);
				}
			}
			
			try{
			Date coverageStartDate = enrollmentDataDTO.getEnrollmentBenefitEffectiveStartDate();
			planSummaryDetails.setCoverageStartDate(null==coverageStartDate?null:dateFormat.format(coverageStartDate));
			planSummaryDetails.setCoverageEndDate(null == enrollmentDataDTO.getEnrollmentBenefitEffectiveEndDate()?null:dateFormat.format(enrollmentDataDTO.getEnrollmentBenefitEffectiveEndDate()));
			planSummaryDetails.setIsPlanEffectuated(coverageStartDate != null && new TSDate().getTime() >= coverageStartDate.getTime() ? "Y":"N");
			}catch(Exception e){
				LOGGER.error("Exception occured while date conversion from enrollmentDataDTO ", e);
			}			
			
			planSummaryDetails.setPremium(null == enrollmentDataDTO.getGrossPremiumAmt()?null:enrollmentDataDTO.getGrossPremiumAmt().toString());
			

			if(null == enrollmentDataDTO.getAptcAmount()){
				planSummaryDetails.setElectedAptc("None");
			}
			else{
				planSummaryDetails.setElectedAptc(enrollmentDataDTO.getAptcAmount().toString());
			}

			if(enrollmentDataDTO.getStateSubsidyAmount() != null){
				planSummaryDetails.setStateSubsidyAmount(enrollmentDataDTO.getStateSubsidyAmount());
			}

			planSummaryDetails.setPremiumEffDate(enrollmentDataDTO.getPremiumEffDate());
			/*
			 * HIX-57891
			 * Add Max APTC to the planSummaryDetails
			 
			Object[] values = getIdHHidMaxAptc(caseNumber);
			Float maxAptc = null;
			if(values[2] != null){
				maxAptc = Float.valueOf(values[2].toString());
				planSummaryDetails.setMaxAptc(values[2].toString());
			}*/
			
			
			BigDecimal maxAptc = getEnrollmentAptc(enrollmentDataDTO);
			if(maxAptc != null){
				planSummaryDetails.setMaxAptc(maxAptc.toString());
			}
			
			String enrlStatusLabel = IndividualPortalConstants.ENROLLMENT_STATUS.get(enrollmentDataDTO.getEnrollmentStatus());

			planSummaryDetails.setNetPremium(null == enrollmentDataDTO.getNetPremiumAmt()?null:enrollmentDataDTO.getNetPremiumAmt().toString());
			planSummaryDetails.setEnrollmentStatus(enrlStatusLabel);
			//No mapping found for IsPaymentRejected and set is as false.
			planSummaryDetails.setIsPaymentRejected(false);
			planSummaryDetails.setEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentDataDTO.getEnrollmentId().toString()));

			Calendar cal = TSCalendar.getInstance();
		    cal.setTime(enrollmentDataDTO.getEnrollmentBenefitEffectiveStartDate());
		    int coverageYear = cal.get(Calendar.YEAR);
			boolean hideDisenroll = isActiveApplicationDeniedEligibility(Long.valueOf(enrollmentDataDTO.getHouseHoldCaseId()), coverageYear);
			if(planSummaryDetails.getEnrollmentStatus().equalsIgnoreCase("cancelled") || planSummaryDetails.getEnrollmentStatus().equalsIgnoreCase("terminated") || hideDisenroll){
				planSummaryDetails.setDisEnroll(false);
			}else{
				planSummaryDetails.setDisEnroll(true);
			}

			if(!StringUtils.equalsIgnoreCase(enrollmentDataDTO.getPlanLevel(),PlanMetalLevel.CATASTROPHIC.value()) && (enrollmentDataDTO.getAptcAmount() != null) &&
					!StringUtils.equalsIgnoreCase(enrlStatusLabel,"terminated") && !StringUtils.equalsIgnoreCase(enrlStatusLabel,"cancelled")){
				planSummaryDetails.setAllowAptcAdjustment(true);
			}

			try{
			//HIX-74412 - Limit termination date options when date crosses coverage year
			Date endOfCurrentMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.CURRENT_MONTH, null);
			Date endOfNextMonth = null;
			Date endOfMonthAfterNextMonth = null;

			int currentMonth = new LocalDate(endOfCurrentMonth.getTime()).getMonthOfYear();

			//If user terminates plan in November, show only two options
			if(currentMonth == DateTimeConstants.NOVEMBER){
				endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
			}
			//If user terminates plan in December, show only one option
			else if(currentMonth !=	DateTimeConstants.DECEMBER){
				endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
				endOfMonthAfterNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.MONTH_AFTER_NEXT_MONTH, null);
			}

			planSummaryDetails.setEndOfCurrentMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfCurrentMonth));
			planSummaryDetails.setEndOfNextMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfNextMonth));
			planSummaryDetails.setEndOfMonthAfterNext(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfMonthAfterNextMonth));

			}catch(Exception e){
				LOGGER.error("Exception occured while conversing date using UI_DATE_FORMAT ", e);
			}
			

			plan.setGrossPremium(planSummaryDetails.getPremium() != null ? Float.valueOf(planSummaryDetails.getPremium()) : null);
			plan.setNetPremium(planSummaryDetails.getNetPremium() != null ? Float.valueOf(planSummaryDetails.getNetPremium()) : null);
			
			dashboardEnrollmentDTO.setPlan(plan);			
			dashboardEnrollmentDTO.setId(enrollmentDataDTO.getEnrollmentId().toString());
			dashboardEnrollmentDTO.setAptc(enrollmentDataDTO.getAptcAmount());
			dashboardEnrollmentDTO.setCoverageStartDate(planSummaryDetails.getCoverageStartDate());
			dashboardEnrollmentDTO.setCoverageEndDate(planSummaryDetails.getCoverageEndDate());
			dashboardEnrollmentDTO.setCsr(getLast2Characters(enrollmentDataDTO.getCMSPlanID()));	
			dashboardEnrollmentDTO.setEnrollmentStatus(enrollmentDataDTO.getEnrollmentStatus());
			dashboardEnrollmentDTO.setAllowChangePlan("Y".equalsIgnoreCase(enrollmentDataDTO.getAllowChangePlan()) ? true : false);
			dashboardEnrollmentDTO.setEnrollmentCreationDate(enrollmentDataDTO.getCreationTimestamp() != null ? DateUtil.dateToString(enrollmentDataDTO.getCreationTimestamp(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
			
			if("Y".equalsIgnoreCase(enrollmentDataDTO.getRenewalFlag())){
				dashboardEnrollmentDTO.setRenewalEnrollment(true);
			}else{
				dashboardEnrollmentDTO.setRenewalEnrollment(false);
			}
			
			dashboardEnrollmentDTO.setSsapApplicationId(enrollmentDataDTO.getSsapApplicationId());
			dashboardEnrollmentDTO.setPriorSsapApplicationId(enrollmentDataDTO.getPriorSsapApplicationId());
			planSummaryDetails.setSsapApplicationId(enrollmentDataDTO.getSsapApplicationId());
			
			boolean isOE = individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear);
			planSummaryDetails.setInsideOEEnrollmentWindow(isOE);
						
			if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
				planSummaryDetails.setTypeOfPlan(enrollmentDataDTO.getPlanType());
				planSummaryDetailsMap.put(IndividualPortalConstants.HEALTH_PLAN, planSummaryDetails);
				dashboardEnrollmentDTO.setType(PlanTypeEnum.HEALTH);
				activeHealthPlanDetails.add(planSummaryDetails);
			}else if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
				planSummaryDetails.setTypeOfPlan(enrollmentDataDTO.getPlanType());
				planSummaryDetailsMap.put(IndividualPortalConstants.DENTAL_PLAN, planSummaryDetails);
				dashboardEnrollmentDTO.setType(PlanTypeEnum.DENTAL);
				activeDentalPlanDetails.add(planSummaryDetails);
			}
			
			dashboardEnrollments.add(dashboardEnrollmentDTO);
			planDetails.setActiveHealthPlanDetails(activeHealthPlanDetails);
			planDetails.setActiveDentalPlanDetails(activeDentalPlanDetails);
		}
	}
	
	private boolean isEnrollmentActive(EnrollmentDataDTO enrollmentDataDTO){
		boolean isActive = false;
		if(ACTIVE_ENROLLMENT_STATUS.contains(enrollmentDataDTO.getEnrollmentStatus())){
			isActive = true;
		}
		return isActive;
	}	
	
	private void setPreEligibilityMembers(MiniEstimatorResponse apiOutput , DashboardPreEligibilityDTO preEligibility) {
		if(apiOutput.getMembers() != null && !apiOutput.getMembers().isEmpty()){
			List<com.getinsured.hix.indportal.dto.dashboard.DashboardMemberDTO> preEligibilityMembers = new ArrayList<>();
			for(Member member : apiOutput.getMembers()){
				if(member != null){
					com.getinsured.hix.indportal.dto.dashboard.DashboardMemberDTO preEligiblilityMember = new com.getinsured.hix.indportal.dto.dashboard.DashboardMemberDTO();
					preEligiblilityMember.setDob(member.getDateOfBirth());
					preEligiblilityMember.setRelation(member.getRelationshipToPrimary()!=null?member.getRelationshipToPrimary().toString():null);
					if(StringUtils.isNotBlank(member.getIsSeekingCoverage()) && member.getIsSeekingCoverage().equalsIgnoreCase("Y")){
						preEligiblilityMember.setSeekingCoverage(true);
					}else{
						preEligiblilityMember.setSeekingCoverage(false);
					}
					
					if(StringUtils.isNotBlank(member.getIsNativeAmerican()) && member.getIsNativeAmerican().equalsIgnoreCase("Y")){
						preEligiblilityMember.setNativeAmerican(true);
					}else{
						preEligiblilityMember.setNativeAmerican(false);
					}
					
					
					preEligibilityMembers.add(preEligiblilityMember);
				}
			}
			preEligibility.setMembers(preEligibilityMembers);
			
			if(StringUtils.isNotBlank(apiOutput.getCsrEligible()) && apiOutput.getCsrEligible().equalsIgnoreCase("Y")){
				preEligibility.setCsrEligible(true);
			}else{
				preEligibility.setCsrEligible(false);
			}
			
			if(StringUtils.isNotBlank(apiOutput.getAptcEligible()) && apiOutput.getAptcEligible().equalsIgnoreCase("Y")){
				preEligibility.setAptcEligible(true);
			}else{
				preEligibility.setAptcEligible(false);
			}
			
		}
	}

	public MiniEstimatorResponse getAPIOutputForLead(PdHouseholdDTO pdHouseholdDTO) {
		MiniEstimatorResponse apiOutput = new MiniEstimatorResponse();
		List<Member> members = new ArrayList<Member>();
		
		List<PdPersonDTO> personDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		if(personDTOList != null && personDTOList.size() > 0) {
			for(PdPersonDTO pdPersonDTO : personDTOList) {
				Member member = new Member();
				member.setDateOfBirth(DateUtil.dateToString(pdPersonDTO.getBirthDay(), GhixConstants.REQUIRED_DATE_FORMAT));
				member.setRelationshipToPrimary(MemberRelationships.valueOf(pdPersonDTO.getRelationship().name()));
				member.setIsSeekingCoverage("Y");
				members.add(member);
			}
		}
		apiOutput.setMembers(members);
		if(pdHouseholdDTO.getCostSharing() != null && !CostSharing.CS1.equals(pdHouseholdDTO.getCostSharing())) {
			apiOutput.setCsrEligible("Y");
		}
		if(pdHouseholdDTO.getMaxSubsidy() != null) {
			apiOutput.setAptcEligible("Y");
		}
		
		return apiOutput;
	}

	/**
	 * Make 'Finalize Plan Choice and Enroll" action a configurable parameter
	 * @param ssapApplicationResource
	 * @return
	 */
	private boolean determineFinalizeEnrollButton(SsapApplicationResource ssapApplicationResource) {

		boolean showFinalizePlanEnrollBtn = true;

		if (("ER".equalsIgnoreCase(ssapApplicationResource.getApplicationStatus()) ||  "PN".equalsIgnoreCase(ssapApplicationResource.getApplicationStatus()))
				&& "OE".equalsIgnoreCase(ssapApplicationResource.getApplicationType())) {
			String enrolledStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
			String enrolledEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);
			if(StringUtils.isNotBlank(enrolledStartDate) && StringUtils.isNotBlank(enrolledEndDate)){
				try{
					SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
					LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
					LocalDate enrolledBeginDate = new DateTime(formatter.parseObject(enrolledStartDate)).toLocalDate();
					LocalDate enrolledLastDate = new DateTime(formatter.parseObject(enrolledEndDate)).toLocalDate();
					if (todaysDate.isBefore(enrolledBeginDate) || todaysDate.isAfter(enrolledLastDate)) {
						showFinalizePlanEnrollBtn = false;
					}
				}
				catch(Exception ex){
					LOGGER.error("Error while parsing enroll date",ex);
				}
			}
		}

		return showFinalizePlanEnrollBtn;
	}

	public List<AnnouncementDTO> getActiveAnnouncementList() {
		List<Announcement> announcementList = announcementService.getActiveAnnoucements(ModuleName.INDIVIDUAL_PORTAL.getValue());
		List<AnnouncementDTO> listOfAnnouncement = new ArrayList<AnnouncementDTO>();
		for (Announcement announcement : announcementList) {
			AnnouncementDTO announcementDTO = new AnnouncementDTO();
			announcementDTO.setId(announcement.getId());
			announcementDTO.setAnnouncementName(announcement.getName());
			announcementDTO.setAnnouncementText(announcement.getAnnouncementText());
			announcementDTO.setModuleName(announcement.getModuleName());
			announcementDTO.setStatus(announcement.getStatus().toString());
			SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			announcementDTO.setEffectiveStartDate(dateFormat.format(announcement.getEffectiveStartDate()));
			announcementDTO.setEffectiveEndDate(dateFormat.format(announcement.getEffectiveEndDate()));
			announcementDTO.setUpdatedDate(dateFormat.format(announcement.getUpdatedDate()));
			listOfAnnouncement.add(announcementDTO);
		}
		return listOfAnnouncement;
	}

    /**
     * Method to retrieve Assister designation detail by individual identifier.
     *
     * @param assisterRequestDTO
     *            The AssisterRequestDTO instance.
     * @return assisterResponseDTO The AssisterResponseDTO instance.
     * @throws GIException
     * @throws Exception
     */
    public AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
            AssisterRequestDTO assisterRequestDTO) throws GIException {
        AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
        LOGGER.debug("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
        try {
            assisterDesignateResponseDTO = this.getEERestCallInvoker()
                    .retrieveAssisterDesignationDetailByIndividualId(
                            assisterRequestDTO);

            if (assisterDesignateResponseDTO != null
                    && assisterDesignateResponseDTO.getResponseCode() == EntityConstants.RESPONSE_CODE_FAILURE) {
                throw new GIException();
            }

            LOGGER.debug("REST call Ends to the GHIX-ENITY module."
                    + assisterDesignateResponseDTO);
        } catch (Exception ex) {
            LOGGER.error("Exception in retrieving Designated Assister details, REST response code is: "
                    + ((assisterDesignateResponseDTO != null) ? assisterDesignateResponseDTO
                            .getResponseCode()
                            : "assisterDesignateResponseDTO is null"));
            persistToGiMonitor(ex);
            throw new GIException(
                    "Exception in retrieving designatedAssister details."
                            + ex.getMessage());
        }
        return assisterDesignateResponseDTO;
    }

	public void addEnrolledPlanDetailsToModel(Model model,Household household, Map<String, PlanSummaryDetails> planDetails) throws ParseException {
		String coverageEndDate = null;
		if (planDetails != null) {

			//Enrolled health plan
			PlanSummaryDetails planDetailsMap = planDetails.get(IndividualPortalConstants.HEALTH_PLAN);

			if(planDetailsMap != null){
				PlanTile healthPlanTile = createPlanTile(planDetailsMap);

				model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, true);
				model.addAttribute("healthPlanId", planDetailsMap.getPlanId());
				model.addAttribute("healthCoverageDate",planDetailsMap.getCoverageStartDate().replaceAll("/", ""));

				//Currently unused
				model.addAttribute("coverageStartDate",planDetailsMap.getCoverageStartDate());
				coverageEndDate = planDetailsMap.getCoverageEndDate();
				model.addAttribute("issuerContactNo", planDetailsMap.getIssuerContactPhone());
				model.addAttribute("healthPlanTile", healthPlanTile);

				if(IndividualPortalConstants.IND_DSPL_STATUS.containsKey(planDetailsMap.getEnrollmentStatus())){
					 model.addAttribute("healthEnMessage",IndividualPortalConstants.IND_DSPL_STATUS.get(planDetailsMap.getEnrollmentStatus()));
					 if(!planDetailsMap.getIsPaymentRejected() && "EN_CN".equalsIgnoreCase(IndividualPortalConstants.IND_DSPL_STATUS.get(planDetailsMap.getEnrollmentStatus()))){
						 model.addAttribute("healthEnMessage", null);
					 }
				}

			}
			else {
				model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, false);
			}

			//Enrolled dental plan
			planDetailsMap = planDetails.get(IndividualPortalConstants.DENTAL_PLAN);

			if(planDetailsMap != null){
				PlanTile dentalPlanTile = createPlanTile(planDetailsMap);

				model.addAttribute(IndividualPortalConstants.FAVORITE_DENTAL_PLAN_SELECTED, true);
				model.addAttribute("dentalPlanId", planDetailsMap.getPlanId());
				model.addAttribute("dentalCoverageDate",planDetailsMap.getCoverageStartDate().replaceAll("/", ""));
				if(null!=coverageEndDate && coverageEndDate.trim().length()>0){
					SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
					LocalDate healthEndDate = new DateTime(mmddyyyy.parseObject(coverageEndDate)).toLocalDate();
					LocalDate dentalEndDate = new DateTime(mmddyyyy.parseObject(planDetailsMap.getCoverageEndDate())).toLocalDate();
					if(dentalEndDate.isAfter(healthEndDate)){
						coverageEndDate = planDetailsMap.getCoverageEndDate();
					}
				}else if(null ==coverageEndDate || coverageEndDate.trim().length()==0){
					coverageEndDate = planDetailsMap.getCoverageEndDate();
				}


				model.addAttribute("dentalPlanTile", dentalPlanTile);

				if(IndividualPortalConstants.IND_DSPL_STATUS.containsKey(planDetailsMap.getEnrollmentStatus())){
					 model.addAttribute("dentalEnMessage",IndividualPortalConstants.IND_DSPL_STATUS.get(planDetailsMap.getEnrollmentStatus()));
					 if(!planDetailsMap.getIsPaymentRejected() && "EN_CN".equalsIgnoreCase(IndividualPortalConstants.IND_DSPL_STATUS.get(planDetailsMap.getEnrollmentStatus()))){
						 model.addAttribute("dentalEnMessage", null);
					 }
				}
			}
			else {
				model.addAttribute(IndividualPortalConstants.FAVORITE_DENTAL_PLAN_SELECTED, false);
			}

			//HIX-60300 Integrate with enrollment api to check if there is a valid enrollment
			model.addAttribute("isPlanEffective", checkForHealthAndDentalTermination(planDetails));

		} else {
			model.addAttribute(IndividualPortalConstants.FAVORITE_DENTAL_PLAN_SELECTED, false);
			model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, false);
		}
		model.addAttribute("coverageEndDate", coverageEndDate);
		
	}

	//Adds favorite plan to dashboard
	public void addSelectedPlanDetailsToModel(Model model,Household household, Map<String, PlanResponse> selectedPlanPMResponse, EligLead eligLead, int coverageYear, DashboardPreEligibilityDTO dashboardPreEligibilityDTO, DashboardTabDTO dashboardTabDTO) {
		List<DashboardPlanDTO> dashboardPreEligiblityPlans = new ArrayList<>(2);
		String aptc = "0";
		Float healthAptc = 0.0f;
		Float dentalAptc = 0.0f;

		if (null != eligLead) {
			List<PreEligibilityResults> preEligibilityResults = eligLead.getPreEligibilityResults();
			for(PreEligibilityResults preEligibilityResult: preEligibilityResults){
				if(preEligibilityResult.getCoverageYear()==coverageYear){
					aptc = preEligibilityResult.getAptc();
					aptc = StringUtils.replace(aptc, "$", "");
					if(NumberUtils.isNumber(aptc)){
						healthAptc = Float.parseFloat(aptc);
						dentalAptc = Float.parseFloat(aptc);
					}
					break;
				}
			}
		}

		//Favorite health plan
		if (selectedPlanPMResponse != null && selectedPlanPMResponse.get(IndividualPortalConstants.HEALTH_PLAN) != null) {
			PlanResponse healthPlan = selectedPlanPMResponse.get(IndividualPortalConstants.HEALTH_PLAN);
			PlanTile healthPlanTile = createPlanTile(healthPlan,eligLead,household, healthAptc);

			model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, true);

			model.addAttribute("healthPlanId", healthPlan.getPlanId());

			//HIX-58605 Compute the coverageStartDate using 15 day rule
			Date coverageDate = eligLead.getCoverageStartDate();
			if(coverageDate == null){
				coverageDate = computeEffectiveDate().getTime();
			}
			SimpleDateFormat df = new SimpleDateFormat("MMddyyyy");
			model.addAttribute("healthCoverageDate",df.format(coverageDate));

			model.addAttribute("healthPlanTile", healthPlanTile);

			if(healthPlan.getPlanPremium()<healthAptc){
				dentalAptc = healthAptc - healthPlan.getPlanPremium();
			}else{
				dentalAptc= 0.0f;
			}
			DashboardPlanDTO dashboardPlanDTO = new DashboardPlanDTO();
			dashboardPlanDTO.setPlanId(Long.valueOf(healthPlan.getPlanId()));
			dashboardPlanDTO.setType(PlanTypeEnum.HEALTH);
			dashboardPlanDTO.setPlanName(healthPlanTile.getPlanName());
			dashboardPlanDTO.setPlanLevel(healthPlan.getPlanLevel());
			dashboardPlanDTO.setGrossPremium(healthPlanTile.getMonthlyPremium());
			dashboardPlanDTO.setNetPremium(healthPlanTile.getNetPremium());
			dashboardPlanDTO.setIssuerName(healthPlan.getIssuerName());
			dashboardPlanDTO.setHiosId(healthPlan.getHiosIssuerId());
			dashboardPreEligiblityPlans.add(dashboardPlanDTO);
		} else {
			model.addAttribute(IndividualPortalConstants.FAVORITE_HEALTH_PLAN_SELECTED, false);
		}

		//Favorite dental plan
		if (selectedPlanPMResponse != null && selectedPlanPMResponse.get(IndividualPortalConstants.DENTAL_PLAN) != null) {
			PlanResponse dentalPlan = selectedPlanPMResponse.get(IndividualPortalConstants.DENTAL_PLAN);
			PlanTile dentalPlanTile = createPlanTile(dentalPlan, eligLead,household, dentalAptc);

			model.addAttribute(IndividualPortalConstants.FAVORITE_DENTAL_PLAN_SELECTED,true);

			model.addAttribute("dentalPlanId", dentalPlan.getPlanId());

			// HIX-58605 Compute the coverageStartDate using 15 day rule
			Date coverageDate = eligLead.getCoverageStartDate();
			if(coverageDate == null){
				coverageDate = computeEffectiveDate().getTime();
			}
			SimpleDateFormat df = new SimpleDateFormat("MMddyyyy");
			model.addAttribute("dentalCoverageDate", df.format(coverageDate));

			model.addAttribute("dentalPlanTile", dentalPlanTile);
			
			DashboardPlanDTO dashboardPlanDTO = new DashboardPlanDTO();
			dashboardPlanDTO.setPlanId(Long.valueOf(dentalPlan.getPlanId()));
			dashboardPlanDTO.setType(PlanTypeEnum.DENTAL);
			dashboardPlanDTO.setPlanName(dentalPlan.getPlanName());
			dashboardPlanDTO.setPlanLevel(dentalPlan.getPlanLevel());
			dashboardPlanDTO.setGrossPremium(dentalPlanTile.getMonthlyPremium());
			dashboardPlanDTO.setNetPremium(dentalPlanTile.getNetPremium());
			dashboardPlanDTO.setIssuerName(dentalPlan.getIssuerName());
			dashboardPlanDTO.setHiosId(dentalPlan.getHiosIssuerId());
			dashboardPreEligiblityPlans.add(dashboardPlanDTO);
			
		} else {
			model.addAttribute(IndividualPortalConstants.FAVORITE_DENTAL_PLAN_SELECTED,false);
		}
		dashboardPreEligibilityDTO.setPlans(dashboardPreEligiblityPlans);
		dashboardTabDTO.setPreEligibility(dashboardPreEligibilityDTO);

	}

	/**
	 * Creates a PlanTile DTO for dashboard
	 *
	 * @author Nikhil Talreja
	 * @param eligLead
	 *
	 */
	private PlanTile createPlanTile(PlanSummaryDetails plan){

		PlanTile planTile = new PlanTile();
		planTile.setPlanName(plan.getPlanName());
		planTile.setMonthlyPremium(Float.parseFloat(plan.getPremium()));

		planTile.setNetPremium(Float.parseFloat(plan.getNetPremium()));
		planTile.setPlanType(plan.getPlanType());

		planTile.setOfficeVisit(plan.getOfficeVisit());
		planTile.setGenericMedication(plan.getGenericMedications());
		planTile.setDeductible(plan.getDeductible());
		planTile.setOutOfPocket(plan.getOopMax());

		planTile.setIssuerLogo(plan.getPlanLogoUrl());

		return planTile;
	}

	/**
	 * Creates a PlanTile DTO for dashboard
	 *
	 * @author Nikhil Talreja
	 * @param eligLead
	 *
	 */
	private PlanTile createPlanTile (PlanResponse plan, EligLead eligLead,Household household, Float aptc){

		PlanTile planTile = new PlanTile();
		planTile.setPlanName(plan.getPlanName());
		planTile.setMonthlyPremium(plan.getPlanPremium());

		planTile.setNetPremium(plan.getPlanPremium());
		if(null!=aptc){
			float netPremium = plan.getPlanPremium() - aptc;
			if(netPremium<=0){
				netPremium=0;
			}
			planTile.setNetPremium(netPremium);
		}

		planTile.setPlanType(plan.getNetworkType());

		planTile.setOfficeVisit(plan.getOfficeVisit());
		planTile.setGenericMedication(plan.getGenericMedications());
		if (isSingleMemberEligibleHousehold(household)) {
			if (plan.getDeductible() != null) {
				planTile.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL+
						String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(plan.getDeductible())));
			}
			if (plan.getOopMax() != null) {
				planTile.setOutOfPocket(IndividualPortalConstants.DOLLAR_SYMBOL+
						String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(plan.getOopMax())));
			}

		} else {
			if (plan.getDeductibleFamily() != null) {
				planTile.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL	+
						String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(plan.getDeductibleFamily())));
			}
			if (plan.getOopMaxFamily() != null) {
				planTile.setOutOfPocket(IndividualPortalConstants.DOLLAR_SYMBOL+
						String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(plan.getOopMaxFamily())));
			}
		}
		planTile.setIssuerLogo(plan.getIssuerLogo());

		return planTile;
	}

	public Map<String,PlanResponse> getFavoritePlanDetails(Long eligLeadId,int coverageYear) throws GIException {
		//String response;
		PdResponse  pldOrderResp = null;
		PlanResponse planResponse = null;
		Map<String,PlanResponse> favoritePlans = new HashMap<String, PlanResponse>();
		Gson gson = new Gson();
		//ObjectMapper mapper;
		AccountUser user = null;
		String userName = null;
		try {
			user = this.userService.getLoggedInUser();

			//To fix HP-FOD issue for null dereference
			if(user == null){
				userName = IndividualPortalConstants.EXCHANGE_ADMIN;
			}
			else{
				userName = user.getUsername();
			}

			//mapper = new ObjectMapper();
			//response = hIXHTTPClient.getGETData(GhixEndPoints.PlandisplayEndpoints.FIND_FAV_PLAN_BY_ELIG_LEAD_ID+ "/" + eligLeadId);
			ResponseEntity<PdResponse> response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_FAV_PLAN_BY_ELIG_LEAD_ID+ "/" + eligLeadId + "/" + coverageYear, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, PdResponse.class, null);
			if(response != null && response.getStatusCode() == HttpStatus.OK){
				pldOrderResp = response.getBody();
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching plan by Lead Id (PlanDisplay): ", e);
			persistToGiMonitor(e);
			throw new GIException("Exception occured while fetching plan by Lead Id (PlanDisplay)",e);
		}
		if (pldOrderResp != null
				&& pldOrderResp.getIndividualPlanList() != null) {
			for(IndividualPlan plan : pldOrderResp.getIndividualPlanList()){
				try {

					//mapper = new ObjectMapper();
					PlanRequest planReq = new PlanRequest();
					planReq.setPlanId(Integer.toString(plan.getPlanId()));
					//response = hIXHTTPClient.getPOSTData(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL,mapper.writeValueAsString(planReq),IndividualPortalConstants.APPLICATION_JSON);
					//planResponse = gson.fromJson(response, PlanResponse.class);
					ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, planReq);
					if(response != null && response.getStatusCode() == HttpStatus.OK){
						planResponse = gson.fromJson(response.getBody(), PlanResponse.class);
					}
					planResponse.setPlanPremium(plan.getPremiumBeforeCredit());
					favoritePlans.put(plan.getPlanType().toLowerCase(),planResponse);
				} catch (Exception e) {
					LOGGER.error(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE, e);
					persistToGiMonitor(e);
					throw new GIException(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE,e);
				}
			}
		}
		return favoritePlans;
	}

	public boolean isSingleMemberEligibleHousehold(Household household) {
		ObjectMapper mapper = new ObjectMapper();
		MiniEstimatorResponse miniEstimatorResponse = null;
		List<com.getinsured.hix.model.estimator.mini.Member> members = null;
		int totalEligibleMembers = 0;
		boolean isSingleMemberEligible = true;
		if(StringUtils.isNotBlank(household.getEligLead().getApiOutput())){
		try {
			miniEstimatorResponse = mapper.readValue(household.getEligLead().getApiOutput(),new TypeReference<MiniEstimatorResponse>() {});
			members = miniEstimatorResponse.getMembers();
		} catch (JsonParseException e) {
			persistToGiMonitor(e);
			LOGGER.error("JsonParseException while reading APIOutput from lead of a household: ",e);
		} catch (JsonMappingException e) {
			persistToGiMonitor(e);
			LOGGER.error("JsonMappingException while reading APIOutput from lead of a household: ",e);
		} catch (IOException e) {
			persistToGiMonitor(e);
			LOGGER.error("Error occured while reading APIOutput from lead of a household: ",e);
		}
		}
		
		if(members != null){
			for (com.getinsured.hix.model.estimator.mini.Member member : members) {
				if (member.getIsSeekingCoverage().equals(IndividualPortalConstants.YES) && IndividualPortalConstants.MEMBER_ELIGIBILITY_FOR_COVERAGE.contains(member.getMemberEligibility())) {
						totalEligibleMembers++;
				}
				if (totalEligibleMembers > 1) {
					return false;
				}
			}
		}
		return isSingleMemberEligible;
	}

	public void submitComplaintToCreateTicket(IndPortalContactUs indPortalContactUs){
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
		List<String> documentLinks = indPortalContactUs.getFileList();
		Integer userId=null;
		try {
			AccountUser user =userService.getLoggedInUser();
			userId=user.getId();

			tkmRequest.setCreatedBy(userId);
			tkmRequest.setLastUpdatedBy(userId);
			String category= IndividualPortalConstants.REQUEST_CATEGRY_TO_TKM_MAPPING.get(indPortalContactUs.getTypeOfRequest()).get(IndividualPortalConstants.CATEGORY);
			tkmRequest.setCategory(category);
			String reason = indPortalContactUs.getReason();
			tkmRequest.setType((reason!=null&&!reason.trim().isEmpty())?IndividualPortalConstants.REQUEST_CATEGRY_TO_TKM_MAPPING.get(indPortalContactUs.getTypeOfRequest()).get(reason):IndividualPortalConstants.REQUEST_CATEGRY_TO_TKM_MAPPING.get(indPortalContactUs.getTypeOfRequest()).get(category));
			tkmRequest.setSubject("Ticket submission for :"+category);
			tkmRequest.setComment("Ticket submission for: "+category);
			tkmRequest.setDescription(indPortalContactUs.getRequestDescription() != null?indPortalContactUs.getRequestDescription() :"");
			tkmRequest.setUserRoleId(roleService.findRoleByName(IndividualPortalConstants.INDIVIDUAL_ROLE).getId());

			tkmRequest.setModuleId(user.getActiveModuleId());


			tkmRequest.setRequester(getRequesterId(user));

			//tkmRequest.setRequester(userId);



		} catch (InvalidUserException e) {
			LOGGER.error("Exception accessing User object", e);
		} catch (GIException e) {
			LOGGER.error("", e);
		}
		tkmRequest.setDocumentLinks(documentLinks);

		TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
		LOGGER.debug("Ticket ID is "+(tkmResponse!=null?tkmResponse.getNumber():"null"));
	}


	private int getRequesterId(AccountUser user) throws GIException {
		int userId = user.getId();
		ConsumerResponse consumerResponse = null;
		// Get the user-id from the activemoduleid if logged in user is not 'INDIVIDUAL'
		if (!(IndividualPortalConstants.INDIVIDUAL_ROLE.equalsIgnoreCase(user.getDefRole().getName()))){

			String postParams = "\"" + user.getActiveModuleId() + "\"";
			LOGGER.debug("Fetching pldHousehold  : " + postParams);
			XStream xstream = GhixUtils.getXStreamStaxObject();
			//String postResp = restTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID,postParams, String.class);
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, postParams);
			if(response != null){
				consumerResponse = (ConsumerResponse) xstream.fromXML(response.getBody());
			}
			if (null != consumerResponse) {
				AccountUser individualUser = consumerResponse.getHousehold().getUser();
				userId = (individualUser != null) ?  individualUser.getId() : userId;
			} else {
				// REST API call failed
				throw new GIException("Unable to fetch the consumer information.");
			}
		}
		return userId;
	}

	public void populateHouseholdFromEligLead(Household household, EligLead eligLead, AccountUser user){
		household.setEligLead(eligLead);

		household.setZipCode(eligLead.getZipCode());
		household.setCountyCode(eligLead.getCountyCode());
		household.setFamilySize(eligLead.getFamilySize());
		household.setHouseHoldIncome(eligLead.getHouseholdIncome());
		household.setEmail(user.getEmail());
		household.setPhoneNumber(user.getPhone());
		household.setFirstName(user.getFirstName());
		household.setLastName(user.getLastName());
		household.setNumberOfApplicants(eligLead.getNoOfApplicants());
		household.setPremium(eligLead.getPremium());
		household.setStage(String.valueOf(eligLead.getStage()));
		household.setStatus(String.valueOf(eligLead.getStatus()));
		household.setUser(user);
		String aptc = eligLead.getAptc();
		if(StringUtils.contains(aptc, "$")){
			aptc = StringUtils.join(aptc,"0");
		}
		//model.addAttribute(IndividualPortalConstants.APTC, eligLead.getAptc()+((eligLead.getAptc()!=null && eligLead.getAptc().contains("$"))?"0":""));
		household.setAptc(aptc);
		household.setCsr(eligLead.getCsr());
		household.setCreated(new TSDate(System.currentTimeMillis()));
		household.setCreatedBy(user.getId());
	}

	public void populateHouseholdFromEmptyLead(Household household, EligLead eligLead, AccountUser user, HttpServletRequest request){
		eligLead.setEmailAddress(user.getEmail());
		eligLead.setPhoneNumber(user.getPhone());
		eligLead.setStage(EligLead.STAGE.WELCOME);
		eligLead.setCreatedBy(user.getId());
		eligLead = consumerUtil.createEligLeadRecord(eligLead);
		if (request.getSession() != null) {
			request.getSession().setAttribute(IndividualPortalConstants.LEAD_ID, eligLead.getId());
		}
		household.setEligLead(eligLead);
		household.setFirstName(user.getFirstName());
		household.setLastName(user.getLastName());
		household.setEmail(user.getEmail());
		household.setPhoneNumber(user.getPhone());
		household.setStage(EligLead.STAGE.WELCOME.toString());
		household.setCreatedBy(user.getId());
		household.setUser(user);

	}

	public SsapApplicationResource getApplicationsForHousehold(Long cmrHouseholdId) throws GIException{
		SsapApplicationResource ssapApplicationResource = null;
		try {
			ssapApplicationResource = resourceCreator.getSsapApplicationByCmrHouseoldId(cmrHouseholdId);
		} catch (JsonParseException e) {
			persistToGiMonitor(e);
			throw new GIException("JsonParseException Error accessing SSAP Service",e);
		} catch (JsonMappingException e) {
			persistToGiMonitor(e);
			throw new GIException("JsonMappingException Error accessing SSAP Service",e);
		} catch (IOException e) {
			persistToGiMonitor(e);
			throw new GIException("IOException Error accessing SSAP Service",e);
		}
		return ssapApplicationResource;
	}

	public String invokeInd19(Ind19ClientRequest ind19ClientRequest) throws GIException{
		//String shoppingId = IndividualPortalConstants.FAILURE;
		AppGroupResponse appGroupResponse = new AppGroupResponse();
		createValidationMessage(400, "Error invoking invokeInd19 method.", appGroupResponse);
		try{
			//ObjectMapper mapper = new ObjectMapper();
			Ind19ClientResponse ind19ClientResponse = null;

			//String ind19ResponseString = hIXHTTPClient.getPOSTData(GhixEndPoints.ELIGIBILITY_URL+"postToPlanSelection", mapper.writeValueAsString(ind19ClientRequest), IndividualPortalConstants.APPLICATION_JSON);
			AccountUser user = this.userService.getLoggedInUser();

			//To fix HP-FOD issue for null dereference
			String userName = user == null ? IndividualPortalConstants.EXCHANGE_ADMIN  : user.getUsername();

			ResponseEntity<Ind19ClientResponse> response =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"postToPlanSelection",
					userName, HttpMethod.POST, MediaType.APPLICATION_JSON, Ind19ClientResponse.class, ind19ClientRequest);
			
			if(response != null && response.getBody() != null){
				ind19ClientResponse = response.getBody();
				if(ind19ClientResponse.isInd19CallSuccess() && ind19ClientResponse.getPlanShoppingId() != null && StringUtils.length(ind19ClientResponse.getPlanShoppingId()) > 0){
					return ind19ClientResponse.getPlanShoppingId();
					}else{
						if(ind19ClientResponse.getErrorCodesAndMessages() != null){
						String errMsg = null;
							for (Map.Entry<String,String> entry : ind19ClientResponse.getErrorCodesAndMessages().entrySet()) { 
								errMsg = entry.getValue();
							}
						createValidationMessage(400, errMsg, appGroupResponse);
						return platformGson.toJson(appGroupResponse);
						//return new StringBuilder(IndividualPortalConstants.FAILURE).append(":").append(errMsg).toString();
					}				
				}
			}
			LOGGER.error("IND19 returned failure: "+ind19ClientResponse);
			
		}catch(Exception ex){
			LOGGER.error("Error invoking IND19 client", ex);
			//persistToGiMonitor(ex);
			//shoppingId = "enrollFailure";
			createValidationMessage(500, "Error invoking IND19 client.", appGroupResponse);
		}
		//return shoppingId;			
		return platformGson.toJson(appGroupResponse);
	}

	public int findNoOfMembersForEligibilityType(List<Member> members,String type) {
		if(members != null && members.isEmpty()){
			return 0;
		}
		int count = 0;
		if(members != null){
			for(Member member : members){
				if(StringUtils.equalsIgnoreCase(member.getMemberEligibility(), type)){
					count++;
				}
			}
		}
		return count;
	}

	public MyApplications getApplicationsForHousehold(long consumerHouseholdId, boolean isIndividual, String selectedCoverageYr) throws IOException{

		List<SsapApplicationResource> ssapApplications = null;

		if(StringUtils.isNotBlank(selectedCoverageYr)) {
			ssapApplications = ssapApplicationAccessor.getSsapApplicationsForCoverageYear(consumerHouseholdId, Long.parseLong(selectedCoverageYr));
		} else{
			ssapApplications = ssapApplicationAccessor.getSsapApplications(consumerHouseholdId);
		}
		try {
			return transformApplicationsForDisplay(ssapApplications, isIndividual, selectedCoverageYr);
			
		} catch (InvalidUserException e) {
			LOGGER.error("invalidUserError: " + e);
		}
		return null;
	}

	private MyApplications transformApplicationsForDisplay(List<SsapApplicationResource> ssapApplications,
			boolean isIndividual, String selectedCoverageYr) throws InvalidUserException {

		MyApplications myApplications = new MyApplications();
		myApplications.setInsideOE(individualPortalUtil.isInsideOEEnrollmentWindow(Integer.parseInt(selectedCoverageYr)));
		List<MyApplicationsEntry> myCurrentApplications = new ArrayList<MyApplicationsEntry>(0);
		List<MyApplicationsEntry> myPastApplications = new ArrayList<MyApplicationsEntry>(0);
		List<MyApplicationsEntry> myQueuedApplications = new ArrayList<MyApplicationsEntry>(0);
		for (SsapApplicationResource ssapApplicationResource : ssapApplications) {
			MyApplicationsEntry myApplicationsEntry = new MyApplicationsEntry(ssapApplicationResource, isIndividual);

			if (myApplicationsEntry.getIsApplicationCurrent()) {
				//showEnroll is only populated for current applications
				// since UI currently uses for the same only and to avoid unnecessary
				// computation
				if(StringUtils.equalsIgnoreCase(myApplicationsEntry.getShowEnrollButton(), "N")
						&& (myApplications.isInsideOE() || determineFinalizeEnrollButton(ssapApplicationResource))) {
					myApplicationsEntry.setShowEnrollButton("Y");
				}

				myCurrentApplications.add(myApplicationsEntry);
			} else if (myApplicationsEntry.getIsApplicationQueued()) {
				myQueuedApplications.add(myApplicationsEntry);
			} else {
				myPastApplications.add(myApplicationsEntry);
			}
		}

		//HIX-58447
		Collections.sort(myCurrentApplications,new MyApplicationsEntryComparator());
		Collections.sort(myPastApplications,new MyApplicationsEntryComparator());
		Collections.sort(myQueuedApplications, new MyApplicationsEntryComparator());

    for (MyApplicationsEntry entry : myCurrentApplications) {
      String applicationStatus = entry.getApplicationStatus();
		  if (applicationStatus.equals("Open") || applicationStatus.equals("Unclaimed") || applicationStatus.equals("Submitted") || applicationStatus.equals("Signed")) {
		    break;
      } else if (applicationStatus.equals("Eligibility Received")) {
        entry.setEditable(true);
        break;
      } else if (applicationStatus.equals("Enrolled (Or Active)") || applicationStatus.equals("Partially Enrolled")) {
        entry.setEditable(true);
      }
    }

		myApplications.setCurrentApplications(myCurrentApplications);
		myApplications.setPastApplications(myPastApplications);
		myApplications.setQueuedApplications(myQueuedApplications);
		myApplications.setInsideQEP(isInsideQEP(Integer.parseInt(selectedCoverageYr)));
		
		AccountUser acountUser = userService.getLoggedInUser();
		List<String> userPermissionList = new ArrayList<String>();
		userPermissionList.addAll(capPortalConfigurationUtil.getUserPermissions(null,acountUser));
		myApplications.setUserPermissionList(userPermissionList);

		return myApplications;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Object getIdFromCaseNumber(String caseNumber){
		EntityManager em = emf.createEntityManager();
		Object result = null;
		try{
			Query query = em.createNativeQuery("select id from ssap_applications where case_number=:caseNumber");
			query.setParameter(IndividualPortalConstants.CASE_NUMBER, caseNumber);
			List<Object[]> resultSet=query.getResultList();
			result = resultSet.get(0);
		}catch(Exception ex){
			LOGGER.debug("Exception while getIdFromCaseNumber ", ex);
		}finally{
			em.close();
		}

		return result;
	}

	public boolean updateApplicationStatus(String caseNumber,String status){
		boolean updateStatus = false;
		try{
			AccountUser user = this.userService.getLoggedInUser();
			SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
			ssapApplicationRequest.setCaseNumber(caseNumber);
			ssapApplicationRequest.setApplicationStatus(status);
			ssapApplicationRequest.setUserId(new BigDecimal(user.getId()));

			ResponseEntity<Void> response =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"application/update",
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, Void.class, ssapApplicationRequest);
			if(response != null && response.getStatusCode().equals(HttpStatus.OK)){
				updateStatus = true;
			}
		}catch(Exception ex){
			LOGGER.error("Update to cancel application failed. Returning false, since this exception cannot be handled here.");
			persistToGiMonitor(ex);
		}
			return updateStatus;
	}

	/**
	 * Extract application based on the STATUS. OP, SG, SU, ER, EN is the order of priority
	 *
	 * @author Sunil Desu
	 * @param consumerHouseholdId
	 * @return ssapApplication
	 * @throws IOException
	 */
	public SsapApplicationResource getApplicationForCoverageYearForPortalHome(long consumerHouseholdId, int coverageYear) throws IOException{

		SsapApplicationResource ssapApplicationResource = null;

		List<SsapApplicationResource> ssapApplications = ssapApplicationAccessor.getSsapApplicationsForCoverageYearNoAppData(consumerHouseholdId, coverageYear);
		Map<String, Integer> statusPriorityIndex = new HashMap<String, Integer>();
		if(ssapApplications.size() == 1 &&
				!(ssapApplications.get(0).getApplicationStatus().equals(ApplicationStatus.CLOSED.getApplicationStatusCode())
						|| ssapApplications.get(0).getApplicationStatus().equals(ApplicationStatus.CANCELLED.getApplicationStatusCode())
				)){
			return ssapApplications.get(0);
		}
		for(int i=0; i<ssapApplications.size();i++){
			if(ssapApplications.get(i).getApplicationStatus().trim().toLowerCase().equals(IndividualPortalConstants.SSAPStatus.CL.getValue())) {
				// it should not override the value in the map
				if(null != statusPriorityIndex.keySet() && !statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.CL.getValue())) {
					statusPriorityIndex.put(ssapApplications.get(i).getApplicationStatus().trim().toLowerCase(), i);	
				}
			}else {
				statusPriorityIndex.put(ssapApplications.get(i).getApplicationStatus().trim().toLowerCase(), i);	
			}
				
			
		}
		if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.OP.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.OP.getValue()));
		}else if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.SG.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.SG.getValue()));
		}else if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.SU.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.SU.getValue()));
		}else if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.ER.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.ER.getValue()));
		}else if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.PN.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.PN.getValue()));
		}else if(statusPriorityIndex.keySet().contains(IndividualPortalConstants.SSAPStatus.EN.getValue())){
			ssapApplicationResource = ssapApplications.get(statusPriorityIndex.get(IndividualPortalConstants.SSAPStatus.EN.getValue()));
		}

		return ssapApplicationResource;
	}

	public ApplicationEligibilityDetails getApplicationEligibilityDetails(String caseNumber) throws GIException{
		ApplicationEligibilityDetails applicationEligibilityDetails = null;
		boolean appealSubmission = false;
		try{
			HouseholdEligibilityDetails householdEligibilityDetails = null;
			SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = null;
			AccountUser user = this.userService.getLoggedInUser();

			String userName = user == null ? IndividualPortalConstants.EXCHANGE_ADMIN : user.getUsername();

			ResponseEntity<SsapApplicationEligibilityDTO> response =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"eligibility/application/"+caseNumber,
					userName, HttpMethod.GET, MediaType.APPLICATION_JSON, SsapApplicationEligibilityDTO.class,null);
			if(response != null){
				ssapApplicationEligibilityDTO = response.getBody();
			}
			if(ssapApplicationEligibilityDTO != null){
				householdEligibilityDetails = new HouseholdEligibilityDetails();
				householdEligibilityDetails.setAptc(String.valueOf(ssapApplicationEligibilityDTO.getMaxAPTCAmount()));
				householdEligibilityDetails.setStateSubsidy(String.valueOf(ssapApplicationEligibilityDTO.getMaxStateSubsidy()));
				householdEligibilityDetails.setCsr(ssapApplicationEligibilityDTO.getCsrLevel());
				householdEligibilityDetails.setEligibilityStatus(ssapApplicationEligibilityDTO.getEligibilityStatus().getDescription());
				if("DE".equalsIgnoreCase(ssapApplicationEligibilityDTO.getEligibilityStatus().toString()) && getEligibilityDateCompare(ssapApplicationEligibilityDTO.getEligibilityReceivedDate())){
					appealSubmission= true;

				}
				if("CAE".equalsIgnoreCase(ssapApplicationEligibilityDTO.getEligibilityStatus().toString())){
					householdEligibilityDetails.setEligibilityConditional(true);
				}
				householdEligibilityDetails.setEnableAppealsSubmission(appealSubmission);
				deriveEligibilitProgramForDisplay(householdEligibilityDetails, ssapApplicationEligibilityDTO.getExchangeEligibilityStatus().getDescription(), ssapApplicationEligibilityDTO);

				String showMedicaidPlan = DynamicPropertiesUtil.getPropertyValue("iex.eligibility.showMedicaid");
				List<ApplicantEligibilityStatus> medicAidEligibilities = new ArrayList<ApplicantEligibilityStatus>(Arrays.asList(ApplicantEligibilityStatus.MEDICAID, 
						ApplicantEligibilityStatus.ASSESSED_MEDICAID, ApplicantEligibilityStatus.CHIP, ApplicantEligibilityStatus.ASSESSED_CHIP));
				
				List<MemberEligibilityDetails> memberEligibilityDetailsList = new ArrayList<MemberEligibilityDetails>(0);
				for(SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO:ssapApplicationEligibilityDTO.getSsapApplicantList()){
					MemberEligibilityDetails memberEligibilityDetails = new MemberEligibilityDetails();
					TreeSet<MemberEligibilityStatus> eligibleMemberSet = new TreeSet<MemberEligibilityStatus>(new MemberEligibilityStatusComparator());	  
					for(EligibilityProgramDTO epd: ssapApplicantEligibilityDTO.getEligibilities()){
						//HIX-118379 Add only eligibilities for which member is eligible
						if("TRUE".equalsIgnoreCase(epd.getEligibilityIndicator())) {
							if(epd.getEligibilityStatus()!=null) {
								if(!"FALSE".equalsIgnoreCase(showMedicaidPlan) || !medicAidEligibilities.contains(epd.getEligibilityStatus())) {
									MemberEligibilityStatus mes = new MemberEligibilityStatus();
									if(StringUtils.isNotEmpty(epd.getEligibilityStatus().getDescription()) && StringUtils.isNotEmpty(ApplicantEligibilityStatus.QHP.getDescription()) && ApplicantEligibilityStatus.QHP.getDescription().equalsIgnoreCase(epd.getEligibilityStatus().getDescription())){
										mes.setEligibilityStatus("Medical and Dental Insurance");
									}else {
										mes.setEligibilityStatus(epd.getEligibilityStatus().getDescription());
									}
							eligibleMemberSet.add(mes);
						}
							
						}
							
						}
					}
					
					memberEligibilityDetails.setMemberEligibilities(eligibleMemberSet);
					//memberEligibilityDetails.setMemberEligibilities(ssapApplicantEligibilityDTO.getEligibilities()); // qhp
					memberEligibilityDetails.setFirstName(ssapApplicantEligibilityDTO.getFirstName());
					memberEligibilityDetails.setMiddleName(ssapApplicantEligibilityDTO.getMiddleName());
					memberEligibilityDetails.setLastName(ssapApplicantEligibilityDTO.getLastName());
					memberEligibilityDetails.setSuffix(ssapApplicantEligibilityDTO.getSuffix());
					memberEligibilityDetails.setPersonId(ssapApplicantEligibilityDTO.getPersonId());
					memberEligibilityDetails.setEligibilityStatus(ssapApplicantEligibilityDTO.isVerificationCompleted());
					memberEligibilityDetailsList.add(memberEligibilityDetails);
				}

				Collections.sort(memberEligibilityDetailsList, (member1, member2) -> (int)(member1.getPersonId() - member2.getPersonId()));


				applicationEligibilityDetails = new ApplicationEligibilityDetails();
				applicationEligibilityDetails.setHouseholdEligibilityDetails(householdEligibilityDetails);
				applicationEligibilityDetails.setMembersEligibilityDetails(memberEligibilityDetailsList);
			}
		}catch(Exception ex){
			LOGGER.error("Exception while retrieving eligibilities",ex);
			persistToGiMonitor(ex);
			throw new GIException("Exception while retrieving eligibilities",ex);
		}
		return applicationEligibilityDetails;
	}

	private void deriveEligibilitProgramForDisplay(HouseholdEligibilityDetails householdEligibilityDetails, String exchangeEligibilityStatus, SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO){
		if(exchangeEligibilityStatus.equalsIgnoreCase("APTC/CSR")){
			householdEligibilityDetails.setEligibleProgram("Qualified Health Plan (QHP)");
			householdEligibilityDetails.setAptcEligible(true);
			householdEligibilityDetails.setCsrEligible(true);
		}else if(exchangeEligibilityStatus.equalsIgnoreCase("APTC")){
			householdEligibilityDetails.setEligibleProgram("Qualified Health Plan (QHP)");
			householdEligibilityDetails.setAptcEligible(true);
			householdEligibilityDetails.setCsrEligible(false);
		}else if(exchangeEligibilityStatus.equalsIgnoreCase("QHP")){
			householdEligibilityDetails.setEligibleProgram("Qualified Health Plan (QHP)");
			householdEligibilityDetails.setAptcEligible(false);
			householdEligibilityDetails.setCsrEligible(false);
		}else{
			householdEligibilityDetails.setEligibleProgram("Public Program");
			householdEligibilityDetails.setAptcEligible(false);
			householdEligibilityDetails.setCsrEligible(false);
		}
		for(SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO:ssapApplicationEligibilityDTO.getSsapApplicantEligibleEligibilityDTO()){
			for(EligibilityProgramDTO epd: ssapApplicantEligibilityDTO.getEligibilities()){
				if(eligibilityTypeList.contains(epd.getEligibilityType()) && epd.getEligibilityIndicator().equalsIgnoreCase("TRUE")){
					householdEligibilityDetails.setMedicaidEligible(true);
					break;
				}
			}
			if(householdEligibilityDetails.isMedicaidEligible()){
				break;
			}
		}
	}

	public void submitAppealRequest(IndAppeal indAppeal){
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
		List<String> fileList = new ArrayList<String>(0);
		Integer userId=null;
		try {
			AccountUser user =userService.getLoggedInUser();
			userId=user.getId();

			tkmRequest.setCreatedBy(userId);
			tkmRequest.setLastUpdatedBy(userId);
			tkmRequest.setCategory("Request");
			tkmRequest.setType("Individual Appeal");
			tkmRequest.setSubject("Ticket submission for: Appeal");
			tkmRequest.setComment("Ticket submission for: Appeal");
			tkmRequest.setRequester(getRequesterId(user));
			tkmRequest.setUserRoleId(roleService.findRoleByName(IndividualPortalConstants.INDIVIDUAL_ROLE).getId());
			tkmRequest.setModuleId(user.getActiveModuleId());

			tkmRequest.setIndAppealReason(IndAppealReason.valueOf(indAppeal.getAppealReason()).toString());
		} catch (InvalidUserException e) {
			LOGGER.error("Exception accessing User object",e);
		}catch (GIException e) {
			LOGGER.error("", e);
		}
		fileList.add(indAppeal.getFileId());
		tkmRequest.setDocumentLinks(fileList);
		TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
		LOGGER.debug("Ticket ID is "+(tkmResponse!=null?tkmResponse.getNumber():"null"));
	}

	/* compare the eligibility date with current data
	 * input date
	 * @return boolean
	 */
	public boolean  getEligibilityDateCompare(Date date){
		boolean value = false;
		Date  currentDate = new TSDate();
		if((null == date)||((currentDate.getTime()-date.getTime()) <= IndividualPortalConstants.SIXTY*(IndividualPortalConstants.NO_OF_HOURS_IN_DAY * IndividualPortalConstants.SIXTY * IndividualPortalConstants.SIXTY * IndividualPortalConstants.THOUSAND))){
			value = true;
		}
		return value;
	}


		/**
		 * 1. Fetch application id from caseNumber
		 * 2. getSsapApplicantsByApplicationId - retrieve list of SsapApplicants
		 * 3. Transform to list of IndPortalApplicantDetails
		 */
	public IndAdditionalDetails getApplicantDetails(String caseNumber, String isChangePlanFlowFlag, String changeSEPPlanFlow){
		Object applicationId = getIdFromCaseNumber(caseNumber);
		List<SsapApplicantResource> ssapApplicantResourceList =  new ArrayList<SsapApplicantResource>();
		IndAdditionalDetails indAdditionalDetails = new IndAdditionalDetails();
		Map <Long,SsapApplicantResource> ssapApplicantResourceMap = null;
		SsapApplicationResource ssapApplicationResource=null;
		SsapApplicantResource ssapApplicants= null;
		if( null != applicationId){
			try {
				ssapApplicationResource = ssapApplicationAccessor.getSsapApplications(caseNumber);
				int coverageYr = (int) ssapApplicationResource.getCoverageYear();

				//HIX-63511
				verifyEnrollAction(caseNumber, ssapApplicationResource, isChangePlanFlowFlag,changeSEPPlanFlow, coverageYr);
				//HIX-102830
				verifyESDDetails(caseNumber, ssapApplicationResource, coverageYr, indAdditionalDetails);
				if(StringUtils.isNotBlank(indAdditionalDetails.getErrCode()))
					return indAdditionalDetails;

				ssapApplicantResourceMap =	resourceCreator.getApplicantsByApplicationId(Long.parseLong(applicationId.toString()));
				for (Map.Entry<Long, SsapApplicantResource> applicant : ssapApplicantResourceMap.entrySet()) {
					ssapApplicants = ssapApplicantResourceMap.get(applicant.getKey());
					ssapApplicantResourceList.add(ssapApplicants);
				}
				if(ssapApplicationResource.getApplicationType() != null && ssapApplicationResource.getApplicationType().equalsIgnoreCase("sep")){
					indAdditionalDetails.setIsSep(true);
				}
				transformToIndPortalApplicantsDetails(ssapApplicantResourceList,caseNumber,indAdditionalDetails,applicationId,isChangePlanFlowFlag,changeSEPPlanFlow, ssapApplicationResource.getCoverageYear());
				if(null != ssapApplicationResource.getExemptHouseHold() && ssapApplicationResource.getExemptHouseHold().equalsIgnoreCase("Y")){
					indAdditionalDetails.setHouseholdExempt(true);
				}else {
					indAdditionalDetails.setHouseholdExempt(false);
				}

				if(StringUtils.isNotBlank(ssapApplicationResource.getApplicationStatus())) {
					indAdditionalDetails.setApplicationStatus(ssapApplicationResource.getApplicationStatus());
				}
			} catch (Exception e) {
				LOGGER.error("Exception accessing ssap application details",e);
				persistToGiMonitor(e);
				indAdditionalDetails.setErrCode(IND_19_GENERIC_ERROR);
			}
		}
		return indAdditionalDetails;
	}

	/**
	 * Verify SEP SSAP for ESD validation
	 *
	 * @param caseNumber
	 * @param ssapApplicationResource
	 * @param coverageYear
	 * @param indAdditionalDetails
	 * @return
	 * @throws Exception
	 */
	protected Date verifyESDDetails(String caseNumber, SsapApplicationResource ssapApplicationResource,
			int coverageYear, IndAdditionalDetails indAdditionalDetails) throws Exception {

		Date financialEffDate = null;

		if(null == ssapApplicationResource)
			ssapApplicationResource = ssapApplicationAccessor.getSsapApplications(caseNumber);

		//HIX-102830
		String applicationType = ssapApplicationResource.getApplicationType();
		String applicationStatus = ssapApplicationResource.getApplicationStatus();
		String financialFlag = ssapApplicationResource.getFinancialAssistanceFlag();

		if(StringUtils.equalsIgnoreCase(applicationType, "SEP")
				&& (StringUtils.equalsIgnoreCase(applicationStatus, "ER") || StringUtils.equalsIgnoreCase(applicationStatus, "PN"))
				&& StringUtils.equalsIgnoreCase(financialFlag, "Y")) {

			SsapApplication enSsap  = ssapApplicationRepository.findEnPnSsapApplicationForCoverageYear(ssapApplicationResource.getCmrHouseoldId(), coverageYear);
			if(enSsap == null) {
					enSsap = getErAppWithOnlyDentalEnrl(ssapApplicationResource.getCmrHouseoldId(), Long.valueOf(coverageYear));
			}
			
			if(enSsap != null){
				EligibilityDatesRequestDto eligibilityDatesRequestDto = new EligibilityDatesRequestDto();
				
				if(indAdditionalDetails.getGroup() == null || StringUtils.isEmpty(indAdditionalDetails.getGroup().getGroupEnrollmentId()))
				{
					// Assuming that when there is no group coming in .. thats normal scenario and there will be only one application of EN state--- 
	
				Map<String, EnrollmentShopDTO> enrollments = getEnrollmentDetails(enSsap.getId());
				if(enrollments==null || enrollments.isEmpty()){
					SsapApplication currentErSsap  = ssapApplicationRepository.findErAfterLatestEnPnSsapApplicationForCoverageYear(ssapApplicationResource.getCmrHouseoldId(), coverageYear);
					if(currentErSsap == null){
						throw new GIException("No existing enrolled SSAP found for application case number " + caseNumber);
					}
					enrollments = getEnrollmentDetails(currentErSsap.getId());
					if(enrollments==null || enrollments.isEmpty()){
						throw new GIException("Unable to get Enrollment Plan Details for the SEP application with casenumber "+currentErSsap.getCaseNumber());
					}
				}
	
				boolean dentalOnly = false;
				EnrollmentShopDTO enrollmentShopDTO = null;
				if(enrollments.containsKey(IndividualPortalConstants.HEALTH_PLAN)){
					enrollmentShopDTO = enrollments.get(IndividualPortalConstants.HEALTH_PLAN);
				}else{
					enrollmentShopDTO = enrollments.get(IndividualPortalConstants.DENTAL_PLAN);
					dentalOnly = true;
				}
				eligibilityDatesRequestDto.setEnrollmentStartDate(enrollmentShopDTO.getCoverageStartDate());
				eligibilityDatesRequestDto.setEnrollmentEndDate(enrollmentShopDTO.getCoverageEndDate());
				}
				else
				{
					Map<String,Date> enrollmentDates = getEnrollmentAttributesById(indAdditionalDetails.getGroup().getGroupEnrollmentId());
	
					eligibilityDatesRequestDto.setEnrollmentStartDate(enrollmentDates.get("coverageStartDate"));
					eligibilityDatesRequestDto.setEnrollmentEndDate(enrollmentDates.get("coverageEndDate"));
				}
				eligibilityDatesRequestDto.setCmrHouseholdId(ssapApplicationResource.getCmrHouseoldId().longValue());
				eligibilityDatesRequestDto.setEnAppId(enSsap.getId());
				eligibilityDatesRequestDto.setErAppId(((BigDecimal)getIdFromCaseNumber(caseNumber)).longValue());
				//eligibilityDatesRequestDto.setDentalOnly(dentalOnly);
	
				LOGGER.debug("Calling ESD validate API for: " + eligibilityDatesRequestDto);
				ResponseEntity<EligibilityDatesResponseDto> responseVal = null;
				try {
					AccountUser user = userService.getLoggedInUser();
					responseVal =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/dates/validate",
						user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, EligibilityDatesResponseDto.class, platformGson.toJson(eligibilityDatesRequestDto));
				} catch(Exception ex) {
					
				}
	
				if(responseVal != null && responseVal.getStatusCode().equals(HttpStatus.OK) && responseVal.getBody() != null ){
					EligibilityDatesResponseDto eligibilityDatesResponseDto = responseVal.getBody();
	
					switch(eligibilityDatesResponseDto.getStatus()) {
						case REQUIRED:
							if(eligibilityDatesResponseDto.isUpdated()) {
	
								EligibilityDatesOverrideDTO datesOverrideDTO = new EligibilityDatesOverrideDTO();
								datesOverrideDTO.setFinancialEffectiveDate(eligibilityDatesResponseDto.getFinancialEffectiveDate());
								datesOverrideDTO.setStatus(eligibilityDatesResponseDto.getStatus());
								datesOverrideDTO.setEnrollmentStartDate(eligibilityDatesRequestDto.getEnrollmentStartDate());
								datesOverrideDTO.setEnrollmentEndDate(eligibilityDatesRequestDto.getEnrollmentEndDate());
	
								Map<String, Object> params = null;
								SsapApplicationEvent sepEvent = getSsapApplicationEventById(eligibilityDatesRequestDto.getErAppId()
										.longValue());
	
								if (sepEvent != null) {
									params = new HashMap<>(2);
									if(sepEvent.getEnrollmentStartDate() != null)
										params.put(IndividualPortalConstants.ESD_SEP_START_DATE, sepEvent.getEnrollmentStartDate());
									if(sepEvent.getEnrollmentEndDate() != null)
										params.put(IndividualPortalConstants.ESD_SEP_END_DATE, sepEvent.getEnrollmentEndDate());
								}
	
								logAppEventForValidateESDDetails(IndividualPortalConstants.APPEVENT_ELIGIBILITY_INFORMATION,
										IndividualPortalConstants.APPEVENT_ESD_OVERRIDE_REQUIRED, caseNumber, datesOverrideDTO, params, null);
							}
							indAdditionalDetails.setErrCode(IND_19_ESD_ERROR);
						break;
						case OVERRIDDEN:
						case VALID:
							financialEffDate = eligibilityDatesResponseDto.getFinancialEffectiveDate();
							break;
						default:
							break;
					}
				}
			}
		}

		return financialEffDate;
	}

	private Map<String, Date> getEnrollmentAttributesById(String groupEnrollmentId) {
		Map<String, Date> enrollmentDates = new HashMap<String, Date>(); 
		try {

				AccountUser user = this.userService.getLoggedInUser();

				String userName = user == null ? IndividualPortalConstants.EXCHANGE_ADMIN  : user.getUsername();
				
				EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
				List<EnrollmentAttributeEnum> attributes = new ArrayList<EnrollmentAttributeEnum>();
				attributes.add(EnrollmentAttributeEnum.id);
				attributes.add(EnrollmentAttributeEnum.benefitEffectiveDate);
				attributes.add(EnrollmentAttributeEnum.benefitEndDate);
				enrollmentRequest.setEnrollmentId(new Integer(groupEnrollmentId));
				enrollmentRequest.getEnrollmentAttributeList().addAll(attributes);
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_ATRIBUTE_BY_ID,
						userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(enrollmentRequest)).getBody();

				EnrollmentDTO enrollmentDTO = null;
				if (null != response){
					EnrollmentResponse enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
					if (enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
		    			enrollmentDTO = enrollmentResponse.getEnrollmentDTO();
		    		} else {
		    			throw new GIException("Error fetching Enrollment details by attribute for enrollment id [" + groupEnrollmentId + "],"
								+ "Error Code: " + enrollmentResponse.getErrCode() + " Error message: " + enrollmentResponse.getErrMsg());
		    		}
					if(enrollmentDTO != null){
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
							enrollmentDates.put("coverageStartDate", dateFormat.parse(enrollmentDTO.getEnrollmentStartDate()));
							enrollmentDates.put("coverageEndDate", dateFormat.parse(enrollmentDTO.getEnrollmentEndDate()));

						} catch (ParseException e) {
							throw new GIException("Error parsing Enrollment Coverage End date",e);
						}
					}
					
				}else{
					throw new GIException("No enrollment found for given Enrollment id [" + groupEnrollmentId + "]");
				}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details by attribute: ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(IndividualPortalConstants.ENROLLMENT_EXCEPTION_MESSAGE);
		}
	
		return enrollmentDates;
	}

	/**
	 * HIX-63511
	 * Verifies if user can proceed with enrollment.
	 *
	 * Restricts action if user tries to tamper with UI Actions
	 * @throws InvalidUserException
	 *
	 */
	public void verifyEnrollAction(String caseNumber, SsapApplicationResource ssapApplicationResource, String isChangePlanFlowFlag,
			String changeSEPPlanFlow,int coverageYear) throws GIException, InvalidUserException{

		String applicationType = ssapApplicationResource.getApplicationType();
		
		//OE
		if(StringUtils.equalsIgnoreCase(applicationType, "OE")
				&& !individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear)){
			String enrolledStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
			String enrolledEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);
			if(StringUtils.isNotBlank(enrolledStartDate) && StringUtils.isNotBlank(enrolledEndDate)){
				try{
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
					LocalDate enrolledBeginDate = new DateTime(formatter.parseObject(enrolledStartDate)).toLocalDate();
					LocalDate enrolledLastDate = new DateTime(formatter.parseObject(enrolledEndDate)).toLocalDate();
					if (todaysDate.isBefore(enrolledBeginDate) || todaysDate.isAfter(enrolledLastDate)) {
						throw new GIException("Open enrollment period has ended");
					}
				}
				catch(Exception ex){
					LOGGER.error("Error while parsing enroll date",ex);
				}
			}

		}
		//SEP or QEP
		if(StringUtils.equalsIgnoreCase(applicationType, "SEP") ||
				StringUtils.equalsIgnoreCase(applicationType, "QEP")){
			Integer noOfDaystoEnroll = isInsideEnrollmentWindow(caseNumber);
			if(noOfDaystoEnroll == null || noOfDaystoEnroll < 0){
				throw new GIException("SEP/QEP enrollment period has ended");
			}
		}
	}

	/**
	 * HIX-63511
	 * Verifies if user can proceed with cancel application.
	 *
	 * Restricts action if user tries to tamper with UI Actions
	 *
	 */
	public void verifyCancelAction(String caseNumber) throws Exception{

		SsapApplicationResource ssapApplicationResource = ssapApplicationAccessor.getSsapApplications(caseNumber);
		String applicationStatus = ssapApplicationResource.getApplicationStatus();

		//Application cannot be cancelled
		if(!IndividualPortalConstants.STATUS_FOR_CANCELLING.contains(applicationStatus)
		        && !"EN".equalsIgnoreCase(applicationStatus)){
			throw new GIException("Application cannot be cancelled, its in " + applicationStatus + " status");
		}
	}

	/**
	 * HIX-63511
	 * Verifies if user can proceed with enrollment from tobacco page.
	 *
	 * Restricts action if user tries to tamper with UI Actions
	 * @param coverageYear
	 * @param activeModuleId
	 *
	 */
	public void verifySaveAdditionalInfoAction(String caseNumber, boolean isChangePlanFlowFlag, boolean changeSEPPlanFlow,
			int coverageYear, int activeModuleId) throws Exception{

		SsapApplicationResource ssapApplicationResource = ssapApplicationAccessor.getSsapApplications(caseNumber);
		String applicationStatus = ssapApplicationResource.getApplicationStatus();
		String applicationType = ssapApplicationResource.getApplicationType();
		String nativeAmerican = ssapApplicationResource.getNativeAmerican();

		//OE
		if(StringUtils.equalsIgnoreCase(applicationType, "OE") && !individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear)){
			String enrolledStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
			String enrolledEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);
			if(StringUtils.isNotBlank(enrolledStartDate) && StringUtils.isNotBlank(enrolledEndDate)){
				try{
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
					LocalDate enrolledBeginDate = new DateTime(formatter.parseObject(enrolledStartDate)).toLocalDate();
					LocalDate enrolledLastDate = new DateTime(formatter.parseObject(enrolledEndDate)).toLocalDate();
					if (todaysDate.isBefore(enrolledBeginDate) || todaysDate.isAfter(enrolledLastDate)) {
						throw new GIException("Open enrollment period has ended");
					}
				}
				catch(Exception ex){
					LOGGER.error("Error while parsing enroll date",ex);
				}
			}
		}
		//SEP or QEP
		if(StringUtils.equalsIgnoreCase(applicationType, "SEP") ||
				StringUtils.equalsIgnoreCase(applicationType, "QEP")){
			Integer noOfDaystoEnroll = isInsideEnrollmentWindow(caseNumber);
			if(noOfDaystoEnroll == null || noOfDaystoEnroll < 0){
				throw new GIException("SEP/QEP enrollment period has ended");
			}
		}
	}

	/**
	 * HIX-63747 Allow change plan for Native Americans
	 * only once month
	 * @throws ParseException
	 */
	public boolean verifyChangePlanAction(Map<String, PlanSummaryDetails> planDetailsMap) throws ParseException{

	    PlanSummaryDetails planToCompare = chooseHealthOrDentalEnrollment(planDetailsMap);

	    if(planToCompare == null){
	        return false;
	    }

		LocalDate enrollmentCreationDate = new LocalDate(planToCompare.getEnrollmentCreationTimestamp());
		LocalDate today = TSLocalTime.getLocalDateInstance();

		return enrollmentCreationDate.getMonthOfYear() != today.getMonthOfYear()
				|| enrollmentCreationDate.getYear() != today.getYear();
	}

	public boolean verifyChangePlanInSEPAction(String caseNumber, Long applicationId, Map<String,PlanSummaryDetails> enrollments)
			throws GIException, ParseException {

		//Checking if household is dis-enrolled
		PlanSummaryDetails selectedPlan = chooseHealthOrDentalEnrollment(enrollments);
		boolean hasActiveEnrollment =  isEnrollmentActive(selectedPlan);

		if (hasActiveEnrollment) {
			// Get the SSAP event
			SsapApplicationEvent sepEvent = getSsapApplicationEventById(applicationId);
			if (sepEvent != null
					&& sepEvent.getEventType().equals(SsapApplicationEventTypeEnum.SEP)
					&& StringUtils.equalsIgnoreCase(sepEvent.getChangePlan(),"Y")) {
				Timestamp sepEndDate = sepEvent.getEnrollmentEndDate();
				return new DateTime(sepEndDate.getTime()).isAfterNow();
			}
		}

		return false;
	}


	/**
	 * Story HIX-53164 (Coverage date logic) Dashboard - Report Changes, Proceed
	 * to Plan Selection and compute coverage start date for valid event when
	 * change referral is received
	 *
	 * Sub-task HIX-55450 API to filter event types and compute coverage start date for event type1
	 *
	 * Gets the count for the SEP Events types for an SSAP
	 *
	 * @author Nikhil Talreja
	 *
	 * @param applicantEvents - List of SSAP Applicant Events
	 * @return Map<String, Integer> containing counts
	 */
	public Map<Integer, Integer> getSEPEventTypeCount(List<SsapApplicantEvent> applicantEvents){

		LOGGER.debug("Getting Event counts: for SSAP Applicant events");
		Map<Integer, Integer> eventCounts = null;

		if(applicantEvents == null || applicantEvents.isEmpty()){
			return eventCounts;
		}

		int type0Count = 0;
		int type1Count = 0;
		int type2Count = 0;
		int type3Count = 0;
		int type4Count = 0;

		for (SsapApplicantEvent event : applicantEvents) {

			SepEvents sepEvent = event.getSepEvents();
			if (sepEvent != null) {
				switch (sepEvent.getType()) {
				case IndividualPortalConstants.TYPE_0_EVENT:
					type0Count++;
					break;
				case IndividualPortalConstants.TYPE_1_EVENT:
					type1Count++;
					break;
				case IndividualPortalConstants.TYPE_2_EVENT:
					type2Count++;
					break;
				case IndividualPortalConstants.TYPE_3_EVENT:
					type3Count++;
					break;
				case IndividualPortalConstants.TYPE_4_EVENT:
					type4Count++;
					break;
				default:
					LOGGER.warn("Invalid Event Type");
				}
			}
		}
		eventCounts = new HashMap<Integer, Integer>();
		eventCounts.put(IndividualPortalConstants.TYPE_0_EVENT, type0Count);
		eventCounts.put(IndividualPortalConstants.TYPE_1_EVENT, type1Count);
		eventCounts.put(IndividualPortalConstants.TYPE_2_EVENT, type2Count);
		eventCounts.put(IndividualPortalConstants.TYPE_3_EVENT, type3Count);
		eventCounts.put(IndividualPortalConstants.TYPE_4_EVENT, type4Count);

		LOGGER.debug("Event counts: " + eventCounts);
		return eventCounts;
	}

	private void transformToIndPortalApplicantsDetails(List<SsapApplicantResource> ssapApplicants,
			String caseNumber,IndAdditionalDetails indAdditionalDetails,Object applicationId,
			String isChangePlanFlowFlag,
			String changeSEPPlanFlow,long coverageYear) throws GIException, InvalidUserException{

		Map<String, List<IndPortalApplicantDetails>> indPortalApplicantsDetails = new HashMap<String, List<IndPortalApplicantDetails>>();
		List<IndPortalApplicantDetails> indPortalApplicant = new ArrayList<IndPortalApplicantDetails>();// eligibile or inelgible
		List<IndPortalApplicantDetails> eligibleApplicantDetails =new ArrayList<IndPortalApplicantDetails>();
		List<IndPortalApplicantDetails> ineligibleApplicantDetails =new ArrayList<IndPortalApplicantDetails>();

		/*
		 * Story HIX-53164
		 * Compute Coverage Start Date
		 */
		Long priorApplicationId = null;
		Map<String, EnrollmentShopDTO> enrollments = null;
		if(indAdditionalDetails.getIsSep() != null && indAdditionalDetails.getIsSep()){
			AccountUser applicant = this.userService.getLoggedInUser();
			SsapApplication application = getEnrolledSsapIdForCoverageYearAndHousehold(new BigDecimal(applicant.getActiveModuleId()), coverageYear);
			//enrollments = getEnrollmentDetails(application.getId());
			if(application != null){
				priorApplicationId = application.getId();
			}			
		}

		boolean isChangePlanFlow = false;
		boolean isChangeSEPPlanFlow = false;
		if(StringUtils.equalsIgnoreCase(isChangePlanFlowFlag, IndividualPortalConstants.TRUE)){
			isChangePlanFlow = true;
		}
		if(StringUtils.equalsIgnoreCase(changeSEPPlanFlow, IndividualPortalConstants.TRUE)){
			isChangeSEPPlanFlow = true;
		}
		Timestamp coverageStartDate = coverageStartDateService.computeCoverageStartDateIncludingTermMembers(new BigDecimal(applicationId.toString()), isChangePlanFlow, isChangeSEPPlanFlow, priorApplicationId);
		
		LOGGER.debug("Coverage start date " + coverageStartDate);

		//HIX-72082
		LocalDate coverageDate = new LocalDate(coverageStartDate.getTime());
		LocalDate latestCoverageEndDate = getLatestCoverageEndDate(enrollments);
		if((coverageDate.getYear() > coverageYear) || (latestCoverageEndDate != null &&
				coverageDate.isAfter(latestCoverageEndDate))){
			indAdditionalDetails.setCoverageStartDate(coverageStartDate);
			indAdditionalDetails.setCoverageYearOver(true);
		}

		for (SsapApplicantResource ssapApplicant : ssapApplicants) {
			if(null != ssapApplicant.getApplicantGuid() && !ssapApplicant.getApplicantGuid().isEmpty()){
				boolean isTobacco = false;
				boolean isCovered = false;
				boolean isNativeAmerican = false;
				StringBuilder name = new StringBuilder();
				name.append(ssapApplicant.getFirstName());
				name.append(" ");
				if(null != ssapApplicant.getMiddleName() && !ssapApplicant.getMiddleName().isEmpty()){
					name.append(ssapApplicant.getMiddleName());
					name.append(" ");
				}
				name.append(ssapApplicant.getLastName());
				if(null != ssapApplicant.getNameSuffix() && !ssapApplicant.getNameSuffix().isEmpty()){
					name.append(" ");
					name.append(ssapApplicant.getNameSuffix());					
				}
				IndPortalApplicantDetails indPortalApplicantDetails	 = new IndPortalApplicantDetails();
				indPortalApplicantDetails.setApplicantGuid(ssapApplicant.getApplicantGuid());
				if(null != ssapApplicant.getTobaccouser() && ssapApplicant.getTobaccouser().equalsIgnoreCase("Y")){
					indPortalApplicantDetails.setTobaccoUser(true);
				} else if(null != ssapApplicant.getTobaccouser() && ssapApplicant.getTobaccouser().equalsIgnoreCase("N")) {
					indPortalApplicantDetails.setTobaccoUser(false);
				} else {
					indPortalApplicantDetails.setTobaccoUser(null);
				}
				
				indPortalApplicantDetails.setPersonId(ssapApplicant.getPersonId());
				indPortalApplicantDetails.setName(name.toString());
				indPortalApplicantDetails.setCaseNumber(caseNumber);
				indPortalApplicantDetails.setExemptionNumber(ssapApplicant.getEcnNumber());
				if(null != ssapApplicant.getApplyingForCoverage() && ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")){
					isCovered= true;
				}
				indPortalApplicantDetails.setApplyingForCoverage(isCovered);
				/*
				 * Story HIX-53164
				 * Check if applicant is adult and set tobacco enabled flag
				 */
				indPortalApplicantDetails.setEnableTobacco(checkIfAdult(coverageStartDate,ssapApplicant.getBirthDate()));
				
				if(null != ssapApplicant.getNativeAmericanFlag() && ssapApplicant.getNativeAmericanFlag().equalsIgnoreCase("Y")){
					isNativeAmerican= true;
				}
				indPortalApplicantDetails.setNativeAmericanFlag(isNativeAmerican);
				indPortalApplicant.add(indPortalApplicantDetails);

			}else{
				throw new GIRuntimeException("Exception occured in getting ssap applicant details");
			}

		}
		Collections.sort(indPortalApplicant, (member1, member2) -> (int)(member1.getPersonId() - member2.getPersonId()));
		try {
			Set<Long> personIdSet = new HashSet<Long>();
			ApplicationEligibilityDetails applicationEligibilityDetails = null;
			applicationEligibilityDetails	= getApplicationEligibilityDetails(caseNumber);
			if(applicationEligibilityDetails != null){
				List<MemberEligibilityDetails> memberEligibilityDetailsList = applicationEligibilityDetails.getMembersEligibilityDetails();
				for (MemberEligibilityDetails memberEligibilityDetails : memberEligibilityDetailsList) {
					Set<MemberEligibilityStatus> eligibilityStatuses = memberEligibilityDetails.getMemberEligibilities();
					if(eligibilityStatuses != null){
						for(MemberEligibilityStatus status: eligibilityStatuses){
							if(IndividualPortalConstants.ELIGIBLE_MEMBER_STATUS.contains(status.getEligibilityStatus())){
								personIdSet.add(memberEligibilityDetails.getPersonId());
								break;
							}
						}
					}
				}
				for (IndPortalApplicantDetails indPortalApplicantDetails : indPortalApplicant) {
					if(personIdSet.contains(indPortalApplicantDetails.getPersonId())){
						eligibleApplicantDetails.add(indPortalApplicantDetails);
					}else{
						ineligibleApplicantDetails.add(indPortalApplicantDetails);
					}
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured in getting ssap application details",e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured in getting ssap application details", e);
		}


		//
	indPortalApplicantsDetails.put("eligiblitydetails", eligibleApplicantDetails);
	indPortalApplicantsDetails.put("inEligibilitydetails", ineligibleApplicantDetails);
	indAdditionalDetails.setApplicantDetails(indPortalApplicantsDetails);

	}
	/**
	 * 1.  list of the IndPortalApplicantDetails
	 * 2. updates the (tobacco details) for each applicants details
	 * @param indPortalApplicants
	 * @return
	 */
	public String updateSsapApplicantsAdditionalInfo(IndAdditionalDetails indAdditionalDetails,String caseNumber){
		String message = "success";
		List<IndPortalApplicantDetails> indPortalApplicants= null;
		try {
			SsapApplicationRequest  ssapApplicationRequest = new SsapApplicationRequest();
			ssapApplicationRequest.setCaseNumber(caseNumber);
			ssapApplicationRequest.setExemptFlag(indAdditionalDetails.getHouseholdExempt().equals(true)?"Y":"N");
			boolean result = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+ "ssapapplicant/updateHouseholdExemptflag",ssapApplicationRequest, Boolean.class);
			if(result){
				indPortalApplicants = indAdditionalDetails.getApplicantDetails().get("eligiblitydetails");
				for (IndPortalApplicantDetails indPortalApplicant : indPortalApplicants) {
					String tobaccoUsage = "N";
					SsapApplicantRequest ssapApplicantRequest = new SsapApplicantRequest();
					ssapApplicantRequest.setCaseNumber(caseNumber);
					if (null != indPortalApplicant.getTobaccoUser()&& indPortalApplicant.getTobaccoUser()) {
						tobaccoUsage = "Y";
						ssapApplicantRequest.setTobaccoUsage(tobaccoUsage);
					} else {
						ssapApplicantRequest.setTobaccoUsage(tobaccoUsage);
					}
					if(null != indPortalApplicant.getExemptionNumber()){
						ssapApplicantRequest.setEcnNumber(indPortalApplicant.getExemptionNumber());
						ssapApplicantRequest.setHardshipExempt("Y");
					}
					ssapApplicantRequest.setApplicantGuid(indPortalApplicant.getApplicantGuid());
					boolean isResponse = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+ "ssapapplicant/updateapplicantflags",ssapApplicantRequest, Boolean.class);
					if (!isResponse) {
						message = IndividualPortalConstants.FAILURE;
						break;
					}
				}
			}else{
				message = IndividualPortalConstants.FAILURE;
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in update ssap application details",e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured in update ssap application details", e);
		}
		return message;
	}

	//Used for coverage start date computation
	public Map<String, EnrollmentShopDTO> getEnrollmentDetails(long applicationId){

		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		Map<String, EnrollmentShopDTO> enrollments = null;

		try {

				AccountUser user = this.userService.getLoggedInUser();

				String userName = user == null ? IndividualPortalConstants.EXCHANGE_ADMIN  : user.getUsername();

				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(applicationId);

				

				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON,
						//ENROLLEE_BY_APPLICATION_ID_JSON,
						userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(enrolleeRequest)).getBody();

				if (null != response){
					enrolleeResponse = platformGson.fromJson(response, EnrolleeResponse.class);
					if(enrolleeResponse != null
						&& enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						enrollments = new HashMap<String, EnrollmentShopDTO>();
						enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
						List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
						List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();

						for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
							if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
								enrollmentForHealth.add(enrollmentShopDto);
							}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
								enrollmentForDental.add(enrollmentShopDto);
							}
						}

						if(enrollmentForHealth.size()>1){
							Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
						}
						if(!enrollmentForHealth.isEmpty()){
							enrollments.put(IndividualPortalConstants.HEALTH_PLAN, enrollmentForHealth.get(0));
						}
						if(enrollmentForDental.size()>1){
							Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
						}
						if(!enrollmentForDental.isEmpty()){
							enrollments.put(IndividualPortalConstants.DENTAL_PLAN, enrollmentForDental.get(0));
						}

					}
					
				}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(IndividualPortalConstants.ENROLLMENT_EXCEPTION_MESSAGE);
		}
		return enrollments;
	}

	public Map<String, PlanSummaryDetails> getEnrollmentDetails(String applicationId, String caseNumber){

		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		Map<String, PlanSummaryDetails> planSummaryDetailsMap = new HashMap<String, PlanSummaryDetails>();

		try {
			if(null  != applicationId){

				AccountUser user = this.userService.getLoggedInUser();

				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(Long.parseLong(applicationId));
				LOGGER.debug("Fetching enrollment details for ssap Id: " + applicationId);

				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON,
						user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrolleeRequest));

				String errMsg = "";
				boolean respInvalid = true;
				
				if (null != response && null != response.getBody() && !response.getBody().isEmpty()){
					Gson gson = new Gson();
					enrolleeResponse = gson.fromJson(response.getBody(), EnrolleeResponse.class);
					if(enrolleeResponse != null) {
						if(enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
							enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
							extractEnrollments(enrollmentShopDTOs, planSummaryDetailsMap, caseNumber);
							respInvalid = false;
						} else {
							errMsg = " Error Code: " + enrolleeResponse.getErrCode() + " Error message: " + enrolleeResponse.getErrMsg();
						}
					}
				}
				
				if(respInvalid) {
					//HIX-90443
					throw new GIException("Unable to get Enrollment Plan Details for SSAP id [" + applicationId + "]" + errMsg);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details for SSAP id [" + applicationId + "], : ", e);
			persistToGiMonitor(e);
		}
		return planSummaryDetailsMap;
	}

	public void extractEnrollments(List<EnrollmentShopDTO> enrollmentShopDTOs, Map<String, PlanSummaryDetails> planSummaryDetailsMap, String caseNumber) throws GIException{
		SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
		List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
		List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
		List<EnrollmentShopDTO> enrollmentList = new ArrayList<EnrollmentShopDTO>();
		for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
			if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
				enrollmentForHealth.add(enrollmentShopDto);
			}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
				enrollmentForDental.add(enrollmentShopDto);
			}
		}

		if(enrollmentForHealth.size()>1){
			Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
		if(enrollmentForHealth!= null && !enrollmentForHealth.isEmpty()){
			enrollmentList.add(enrollmentForHealth.get(0));
		}
		if(enrollmentForDental.size()>1){
			Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
		if(enrollmentForDental != null && !enrollmentForDental.isEmpty()){
			enrollmentList.add(enrollmentForDental.get(0));
		}

		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentList) {
			PlanSummaryDetails planSummaryDetails = new PlanSummaryDetails();

			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
			List<PlanSummaryMember> membersList = new ArrayList<PlanSummaryMember>(0);
			SimpleDateFormat format = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
				PlanSummaryMember member = new PlanSummaryMember();
				member.setName(enrolleeMembers.getFirstName()+" "+enrolleeMembers.getLastName());
				member.setCoverageStartDate(format.format(enrolleeMembers.getEffectiveStartDate()));
				member.setCoverageEndDate(format.format(enrolleeMembers.getEffectiveEndDate()));
				membersList.add(member);
			}
			//HIX-63747
			planSummaryDetails.setEnrollmentCreationTimestamp(enrollmentShopDTO.getEnrollmentCreationTimestamp());
			planSummaryDetails.setCoveredMembers(membersList);
			planSummaryDetails.setCaseNumber(caseNumber);

			planSummaryDetails.setPlanName(enrollmentShopDTO.getPlanName());
			if(null != enrollmentShopDTO.getPlanId()){
				planSummaryDetails.setPlanId(enrollmentShopDTO.getPlanId().toString());
				try {
					PlanResponse planResponse  = getPlanDetails(enrollmentShopDTO.getPlanId().toString());
					planSummaryDetails.setPlanType(planResponse.getNetworkType());
					planSummaryDetails.setOfficeVisit(planResponse.getOfficeVisit());
					planSummaryDetails.setGenericMedications(planResponse.getGenericMedications());
					planSummaryDetails.setPlanLogoUrl(planResponse.getIssuerLogo());
					planSummaryDetails.setIssuerContactPhone(planResponse.getIssuerPhone());
					planSummaryDetails.setIssuerContactUrl(planResponse.getIssuerSite());
					planSummaryDetails.setPaymentUrl(planResponse.getPaymentUrl());
					if (planSummaryDetails.getCoveredMembers().size() == 1) {
						if (StringUtils.isNotBlank(planResponse.getDeductible())) {
							planSummaryDetails.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(planResponse.getDeductible())));
						}
						if (StringUtils.isNotBlank(planResponse.getOopMax())) {
							planSummaryDetails.setOopMax(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(planResponse.getOopMax())));
						}
					} else {
						if (StringUtils.isNotBlank(planResponse.getDeductibleFamily())) {
							planSummaryDetails.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(planResponse.getDeductibleFamily())));
						}
						if (StringUtils.isNotBlank(planResponse.getOopMaxFamily())
								&& NumberUtils.isNumber(planResponse.getOopMaxFamily())) {
							planSummaryDetails.setOopMax(IndividualPortalConstants.DOLLAR_SYMBOL +
									String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(planResponse.getOopMaxFamily())));
						}
					}
				} catch (GIException e) {
					persistToGiMonitor(e);
					LOGGER.error(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE, e);
				}
			}

			Date coverageStartDate = enrollmentShopDTO.getCoverageStartDate();
			planSummaryDetails.setPolicyId(enrollmentShopDTO.getCoveragePolicyNumber());
			planSummaryDetails.setCoverageStartDate(null==coverageStartDate?null:dateFormat.format(coverageStartDate));
			planSummaryDetails.setCoverageEndDate(null == enrollmentShopDTO.getCoverageEndDate()?null:dateFormat.format(enrollmentShopDTO.getCoverageEndDate()));
			planSummaryDetails.setPremium(null == enrollmentShopDTO.getMonthlyPremium()?null:enrollmentShopDTO.getMonthlyPremium().toString());
			planSummaryDetails.setIsPlanEffectuated(coverageStartDate != null && new TSDate().getTime() >= coverageStartDate.getTime() ? "Y":"N");

			if(null == enrollmentShopDTO.getAptcAmt()){
				planSummaryDetails.setElectedAptc("None");
			}
			else{
				planSummaryDetails.setElectedAptc(enrollmentShopDTO.getAptcAmt().toString());
			}
			planSummaryDetails.setPremiumEffDate(enrollmentShopDTO.getPremiumEffDate());
			/*
			 * HIX-57891
			 * Add Max APTC to the planSummaryDetails
			 */
			Object[] values = getIdHHidMaxAptc(caseNumber);
			float maxAptc = 0;
			if(values[2] != null){
				maxAptc = Float.valueOf(values[2].toString());
			}
			String enrlStatusLabel = enrollmentShopDTO.getEnrollmentStatusLabel();

			planSummaryDetails.setMaxAptc(String.valueOf(maxAptc));
			planSummaryDetails.setNetPremium(null == enrollmentShopDTO.getNetPremiumAmt()?null:enrollmentShopDTO.getNetPremiumAmt().toString());
			planSummaryDetails.setEnrollmentStatus(enrlStatusLabel);
			planSummaryDetails.setIsPaymentRejected(enrollmentShopDTO.isEnrollmentCancelTermForNonPayment());
			planSummaryDetails.setEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentShopDTO.getEnrollmentId().toString()));

			Calendar cal = TSCalendar.getInstance();
		    cal.setTime(enrollmentShopDTO.getCoverageStartDate());
		    int coverageYear = cal.get(Calendar.YEAR);
			boolean hideDisenroll = isActiveApplicationDeniedEligibility(Long.valueOf(enrollmentShopDTO.getHouseHoldCaseId()), coverageYear);
			if(planSummaryDetails.getEnrollmentStatus().equalsIgnoreCase("cancelled") || planSummaryDetails.getEnrollmentStatus().equalsIgnoreCase("terminated") || hideDisenroll){
				planSummaryDetails.setDisEnroll(false);
			}else{
				planSummaryDetails.setDisEnroll(true);
			}

			if(!StringUtils.equalsIgnoreCase(enrollmentShopDTO.getPlanLevel(),PlanMetalLevel.CATASTROPHIC.value()) && maxAptc > 0 &&
					!StringUtils.equalsIgnoreCase(enrlStatusLabel,"terminated") && !StringUtils.equalsIgnoreCase(enrlStatusLabel,"cancelled")){
				planSummaryDetails.setAllowAptcAdjustment(true);
			}

			//HIX-74412 - Limit termination date options when date crosses coverage year
			Date endOfCurrentMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.CURRENT_MONTH, null);
			Date endOfNextMonth = null;
			Date endOfMonthAfterNextMonth = null;

			int currentMonth = new LocalDate(endOfCurrentMonth.getTime()).getMonthOfYear();

			//If user terminates plan in November, show only two options
			if(currentMonth == DateTimeConstants.NOVEMBER){
				endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
			}
			//If user terminates plan in December, show only one option
			else if(currentMonth !=	DateTimeConstants.DECEMBER){
				endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
				endOfMonthAfterNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.MONTH_AFTER_NEXT_MONTH, null);
			}

			planSummaryDetails.setEndOfCurrentMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfCurrentMonth));
			planSummaryDetails.setEndOfNextMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfNextMonth));
			planSummaryDetails.setEndOfMonthAfterNext(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfMonthAfterNextMonth));

			if(enrollmentShopDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
				planSummaryDetails.setTypeOfPlan(enrollmentShopDTO.getPlanType());
				planSummaryDetailsMap.put(IndividualPortalConstants.HEALTH_PLAN, planSummaryDetails);
			}else if(enrollmentShopDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
				planSummaryDetails.setTypeOfPlan(enrollmentShopDTO.getPlanType());
				planSummaryDetailsMap.put(IndividualPortalConstants.DENTAL_PLAN, planSummaryDetails);
			}

		}
	}
	public PlanResponse getPlanDetails(String planId) throws GIException {
		PlanResponse planResponse = null;
		Gson gson = new Gson();
		if (planId != null ) {
			try {
				AccountUser user = this.userService.getLoggedInUser();
				PlanRequest planReq = new PlanRequest();
				planReq.setPlanId(planId);
				planReq.setMarketType(Plan.PlanMarket.INDIVIDUAL.toString());
				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, planReq);
				if(response != null){
					planResponse = gson.fromJson(response.getBody(), PlanResponse.class);
				}
			} catch (Exception e) {
				LOGGER.error(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE, e);
				persistToGiMonitor(e);
				throw new GIException(IndividualPortalConstants.PLAN_MGMT_EXCEPTION_MESSAGE,e);
			}
		}
		return planResponse;
	}

	/**
	 * Checks if the OE for current year has started
	 *
	 * @return true/false
	 *
	 * @author Nikhil Talreja
	 */
	private boolean isAfterCurrentOE() {

        boolean status = true;
        Date oeStart = null;

        try {

            oeStart = new SimpleDateFormat(
                    IndividualPortalConstants.SHORT_DATE_FORMAT)
                    .parse(DynamicPropertiesUtil
                            .getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));

            LocalDate currentDate = TSLocalTime.getLocalDateInstance();

            if (currentDate.isBefore(new LocalDate(oeStart))) {
                status = false;
            }

        } catch (ParseException e) {
            LOGGER.error(
                    "Exception occured while checking open enrollment start date : ",
                    e);
            persistToGiMonitor(e);
            status = false;
            throw new GIRuntimeException(
                    "Exception occured while checking open enrollment start date :");
        }
        return status;
    }


	//Check if QEP period is on
	private boolean isInsideQEP(int coverageYear) {
		boolean status = false;
		Date oeEnd = null;
		int prevYear=0;
		int currYear=0;
		int renYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			String renewalYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
			if(StringUtils.isNumeric(renewalYear)){
				renYear = Integer.parseInt(renewalYear);
			}
			if(coverageYear == renYear){
				return false;
			}
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
				if(coverageYear<prevYear){
					return false;
				}
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if(coverageYear == prevYear){
				oeEnd = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE));
			}
			if(coverageYear == currYear){
				oeEnd = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE));
			}
			if (TSLocalTime.getLocalDateInstance().isAfter(new LocalDate(oeEnd.getTime()))) {
				status = true;
			}
		} catch (ParseException e) {
			LOGGER.error(
					"Exception occured while checking the current with QEP start date : ",
					e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(
					"Exception occured while checking the current with QEP start date :");
		}
		return status;
	}

	public Map<String, List<MyAppealDetails>> getAppealsDetails(){
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
		String xmlResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse  ticketsResponse=null;
		Map<String, List<MyAppealDetails>> appeals = new HashMap<String, List<MyAppealDetails>>();
		try {
			AccountUser user =userService.getLoggedInUser();
			tkmRequest.setUserRoleId(roleService.findRoleByName(IndividualPortalConstants.INDIVIDUAL_ROLE).getId());
			tkmRequest.setModuleId(user.getActiveModuleId());
			tkmRequest.setCategory("Request");
			tkmRequest.setType("Individual Appeal");
			xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.TicketMgmtEndPoints.GETTICKETLIST, user.getUsername(),HttpMethod.POST,MediaType.APPLICATION_JSON, String.class, tkmRequest)).getBody();
			ticketsResponse = (TkmTicketsResponse)xstream.fromXML(xmlResponse);
			if (null != ticketsResponse && ticketsResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				List<TkmTickets> ticketList = ticketsResponse.getTkmTicketsList();
				transformTOAppealDetails(ticketList, appeals);
			}else{
				LOGGER.error("Unable to get appeals Details. Error Details:");
				throw new GIRuntimeException("Unable to get appeals Details.");
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get appeals Details. Error Details:",e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Unable to get appeals Details.");
		}
		return appeals;
	}

	private void transformTOAppealDetails(List<TkmTickets> ticketList,Map<String, List<MyAppealDetails>> appeals){
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyy");
		List<MyAppealDetails> appealDetails = new ArrayList<MyAppealDetails>();
		for (TkmTickets tkmTicket : ticketList) {
			//Code is commented as part of the JIRA HIX-49018 and uncommented because jira is reopened
			String xmlResponse = null;
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmTicketsResponse  ticketsResponse=null;
			MyAppealDetails myAppealDetails = new MyAppealDetails();
			try {
				AccountUser user =userService.getLoggedInUser();
				TkmTicketsRequest tkmTicketRequest = new TkmTicketsRequest();
				tkmTicketRequest.setId(tkmTicket.getId());
				xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.TicketMgmtEndPoints.GETTICKETSTATUS, user.getUserName(),HttpMethod.POST,MediaType.APPLICATION_JSON, String.class, tkmTicketRequest)).getBody();
				ticketsResponse = (TkmTicketsResponse)xstream.fromXML(xmlResponse);
				if (null != ticketsResponse && ticketsResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					myAppealDetails.setCurrentStatus(ticketsResponse.getTicketStatus());
					myAppealDetails.setAppealId(tkmTicket.getNumber());
					myAppealDetails.setAppealDescription(tkmTicket.getDescription());
					myAppealDetails.setCreatedDate(null == tkmTicket.getCreated()?null : format.format(tkmTicket.getCreated()));
					myAppealDetails.setAppealClosedDate(null == tkmTicket.getUpdated()? null :format.format(tkmTicket.getUpdated()));
				}

			} catch (Exception e) {
				LOGGER.error("Unable to get appeals Details. Error Details:",e);
				persistToGiMonitor(e);
				throw new GIRuntimeException("Unable to get appeals Details. Error Details: ",e.getMessage());
			}
			appealDetails.add(myAppealDetails);
		}
		appeals.put("appeals", appealDetails);
	}


	@SuppressWarnings("unchecked")
	public Map<String, List<PendingReferralDetails>> getPendingReferral(){

		List<PendingReferralDTO>  pendingReferralDTO = null;
		Map<String, List<PendingReferralDetails>> pendingReferrals = new HashMap<String, List<PendingReferralDetails>>();
		try {
			AccountUser user =userService.getLoggedInUser();
			pendingReferralDTO = (ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_PENDING_CMR,user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, ArrayList.class, user.getActiveModuleId())).getBody();
			if(null != pendingReferralDTO){
				transformPendingReferral(pendingReferralDTO,pendingReferrals);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get pending referrals. Error Details:",e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Unable to get pending referrals. Error Details: " );
		}
		return pendingReferrals;

	}

	@SuppressWarnings("unchecked")
	private void transformPendingReferral(List<PendingReferralDTO>  pendingReferralDTO,Map<String, List<PendingReferralDetails>> pendingReferrals){
		List<PendingReferralDetails> pendingRefList = new ArrayList<PendingReferralDetails>();
		for(int i=0;i<pendingReferralDTO.size();i++){
			Map<String,Object> referralmap=(Map<String, Object>) pendingReferralDTO.get(i);
			PendingReferralDetails pendingReferralDetails = new PendingReferralDetails();
			pendingReferralDetails.setReferralId(ghixJasyptEncrytorUtil.encryptStringByJasypt(referralmap.get("id").toString()));
			pendingReferralDetails.setCaseNumber(referralmap.get(IndividualPortalConstants.CASE_NUMBER).toString());
			pendingReferralDetails.setSsapApplicationId(referralmap.get("ssapApplicationId").toString());
			pendingRefList.add(pendingReferralDetails);
		}
		pendingReferrals.put("referrals", pendingRefList);

	}

	public List<EnrollmentShopDTO> getEnrollmentShopDto(String applicationId){
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xmlResponse = null;
		try {
			if(null  != applicationId){
				AccountUser user = userService.getLoggedInUser();
				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(Long.parseLong(applicationId));
				xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xstream.toXML(enrolleeRequest))).getBody();
				enrolleeResponse = (EnrolleeResponse)xstream.fromXML(xmlResponse);
				if (null != enrolleeResponse && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				}else{
					throw new GIException("Unable to get Enrollment Plan Details.");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(IndividualPortalConstants.ENROLLMENT_EXCEPTION_MESSAGE);
		}
		return enrollmentShopDTOs;
	}

	public Boolean getReEnrollmentFlag(Date coverageStartDate, Date disenrollDate, Date applicationCreationTime)
			throws ParseException {

		boolean isInsideOe = individualPortalUtil.isInsideOEEnrollmentWindow(new LocalDate(coverageStartDate).getYear());
		boolean isAppCreatedInOEP = individualPortalUtil.isDateInsideOEWindow(applicationCreationTime);
		if (isInsideOe && isAppCreatedInOEP) {
			return true;
		} else {
			return false;
		}
		
	}

	public EnrollmentPaymentDTO getEnrollmentPaymentDto(PlanSummaryDetails planSummaryDetails){
		EnrollmentPaymentDTO enrollmentPaymentDTO = null;
		EnrollmentResponse enrollmentResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xmlResponse = null;
		String enrollmentId = null;
		try {
			enrollmentId = planSummaryDetails.getEnrollmentId();
			if(null  != enrollmentId){
				AccountUser user = userService.getLoggedInUser();
				final Map<String, String> request = new HashMap<String,String>();
				request.put("id", ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentId));
				xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.ENROLLMENT_URL+"/enrollment/getEnrollmentPaymentInfoByID/{id}", user.getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, String.class,request)).getBody();
				enrollmentResponse = (EnrollmentResponse)xstream.fromXML(xmlResponse);
				if (null != enrollmentResponse && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					enrollmentPaymentDTO = enrollmentResponse.getEnrollmentPaymentDTO();
				}else{
					throw new GIException("Unable to get enrollment payment Dto.");
				}
			}
		}catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment payment details : ", e);

			throw new GIRuntimeException("Exception occured while fetching enrollment  payment details :");
		}
		return enrollmentPaymentDTO;
	}

	public Integer isInsideEnrollmentWindow(String caseNumber) throws GIException{
		Date currentDate = new TSDate();
		Integer days = null;
		try {
			Object ssapId = getIdFromCaseNumber(caseNumber);
			if (ssapId == null) {
				throw new GIException("Invalid Case Number");
			}
			BigDecimal ssap = (BigDecimal) ssapId;
			SsapApplicationEvent sepEvent = getSsapApplicationEventById(ssap.longValue());
			if (sepEvent == null) {
				throw new GIException("SSAP has no event");
			}
			if(sepEvent.getEnrollmentEndDate() != null &&
				currentDate.getTime() <= sepEvent.getEnrollmentEndDate().getTime()){
				days=Days.daysBetween(new DateTime(currentDate.getTime()).toLocalDate(), new DateTime(sepEvent.getEnrollmentEndDate().getTime()).toLocalDate()).getDays();
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching no of days to start enrollment ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured while fetching no of days to start enrollment :");
		}
		return days;
	}

	/**
	 * Fetches the SSAP Event based on SSAP ID
	 *
	 * @author Nikhil Talreja
	 *
	 * @param applicationId - for which SSAP Event is needed
	 * @return SsapApplicationEvent for the SSAP
	 * @throws GIException if the record cannot be fetched for some reason
	 */
	protected SsapApplicationEvent getSsapApplicationEventById(Long applicationId) throws GIException{

		SsapApplicationEvent ssapApplicationEvent = null;
		try {
			List<SsapApplicationEvent> events = ssapApplicationEventRepository.findEventBySsapApplicationId(applicationId);
			if(events != null && !events.isEmpty()){
				ssapApplicationEvent =  events.get(0);
			}
		} catch (Exception e) {
			persistToGiMonitor(e);
		}

		return ssapApplicationEvent;
	}

	/**
	 * Story HIX-41023 - CSR Overrides: CSR can choose to open a special
	 * enrollment period for the household
	 *
	 * Sub-Task HIX-54283 - Create getSepDetails API
	 *
	 * Gets the SEP details and populates IndPortalSpecialEnrollDetails
	 * DTO.
	 *
	 * @author Nikhil Talreja
	 *
	 * @param caseNumber - SSAP Case number for which SEP details need to be fetched
	 * @throws InvalidUserException
	 *
	 */
	public IndPortalSpecialEnrollDetails getSepDetails(final String caseNumber)
			throws GIException, InvalidUserException {

		IndPortalSpecialEnrollDetails response = null;

		LOGGER.debug("Getting SEP details");
		// Fetch SEP details using case number
		Object ssapId = getIdFromCaseNumber(caseNumber);
		if (ssapId == null) {
			throw new GIException("Invalid Case Number");
		}
		//HIX-57896
		//validateActiveModuleId(caseNumber, -1);
		BigDecimal ssap = (BigDecimal) ssapId;
		SsapApplicationEvent sepEvent = getSsapApplicationEventById(ssap
				.longValue());
		if (sepEvent == null) {
			throw new GIException("SSAP has no event");
		}

		response = new IndPortalSpecialEnrollDetails();
		// SSAP Event Id will be encrypted and sent to front end
		response.setEventUpdateId(ghixJasyptEncrytorUtil
				.encryptStringByJasypt(Long.toString((sepEvent.getId()))));
		response.setCaseNumber(caseNumber);

		// Set the dates
		response.setSepStartDate(dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,
				sepEvent.getEnrollmentStartDate()));
		response.setSepEndDate(dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,
				sepEvent.getEnrollmentEndDate()));
		response.setCovStartDate(dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,
				sepEvent.getCoverageStartDate()));
		response.setEnableEditing(true);
		LOGGER.debug("Successfully fetched SEP details");

		return response;
	}

	/**
	 * Validates the inputs for saving SEP or Coverage Dates
	 * for CSR over ride
	 *
	 * @author Nikhil Talreja
	 *
	 * @param IndPortalSpecialEnrollDetails - DTO to be verified
	 * @return boolean - true/false
	 */
	private boolean validateSEPInputs(IndPortalSpecialEnrollDetails indPortalSpecialEnrollDetails){

		/*
		 *  Validate Inputs
		 *
		 *  If SEP dates are being over-ridden,
		 *  SEP start and end date cannot be blank
		 *
		 *  If Coverage date is being over-ridden,
		 *  Coverage start date cannot be blank
		 *
		 */
		if(indPortalSpecialEnrollDetails.isEditSep()){
			if((StringUtils.isBlank(indPortalSpecialEnrollDetails.getSepStartDate())
				|| StringUtils.isBlank(indPortalSpecialEnrollDetails.getSepEndDate()))){
				LOGGER.error("Invalid inputs to saveSEPDetails. SEP start date and/or SEP end date were null");
				return false;
			}
		}
		else{
			if(StringUtils.isBlank(indPortalSpecialEnrollDetails.getCovStartDate())) {
				LOGGER.error("Invalid inputs to saveSEPDetails. Coverage start date was null");
				return false;
			}
		}

		return true;
	}

	/**
	 * Saves the SSAP Event using IndPortalSpecialEnrollDetails DTO.
	 *
	 * @author Nikhil Talreja
	 *
	 * @param DTO from which details should be saved
	 * @return String - success/failure
	 */
	public String saveSEPDTO(final IndPortalSpecialEnrollDetails indPortalSpecialEnrollDetails) {

		String response = IndividualPortalConstants.SUCCESS;

		try {

			LOGGER.debug("Saving SEP details");

			//Validate Inputs
			if(!validateSEPInputs(indPortalSpecialEnrollDetails)){
				return IndividualPortalConstants.FAILURE;
			}

			//Call API to save SSAP Event in DB
			long id = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(indPortalSpecialEnrollDetails.getEventUpdateId()));
			SsapApplicationEvent event = ssapApplicationEventRepository.findOne(id);
			event.setCoverageStartDate(stringToDate(IndividualPortalConstants.SHORT_DATE_FORMAT,indPortalSpecialEnrollDetails.getCovStartDate(),false));
			event.setEnrollmentStartDate(stringToDate(IndividualPortalConstants.SHORT_DATE_FORMAT,indPortalSpecialEnrollDetails.getSepStartDate(),false));
			event.setEnrollmentEndDate(stringToDate(IndividualPortalConstants.SHORT_DATE_FORMAT,indPortalSpecialEnrollDetails.getSepEndDate(),true));
			ssapApplicationEventRepository.save(event);
			LOGGER.debug("Successfully saved SEP details");

		} catch (Exception e) {
			LOGGER.error("Error while saving the special enrollment details", e);
			persistToGiMonitor(e);
			response = IndividualPortalConstants.FAILURE;
		}

		return response;

	}

	private void evaluateSepEvents(Model model, SsapApplicationResource ssapApplicationResource, String enableSep, String sepLinkEvents, String sepDisenroll, String sepPlanSelection) throws GIException{
		LOGGER.debug("SEP Application is being processed with case number = "+ssapApplicationResource.getCaseNumber());
		Object ssapId = getIdFromCaseNumber(ssapApplicationResource.getCaseNumber());
		if (ssapId == null) {
			throw new GIException("Invalid Case Number");
		}
		if(null!=ssapApplicationResource.getEligibilityStatus() && ssapApplicationResource.getEligibilityStatus().equalsIgnoreCase("DE")
				 && null != getEnrolledSsapIdForCoverageYearAndHousehold(ssapApplicationResource.getCmrHouseoldId(), ssapApplicationResource.getCoverageYear())){
			LOGGER.debug("SEP applicaiton has been received and all applicants are Denied eligiblity. Action needs to be taken to disenroll the previous enrollment and close the current application.");
			enableSep = IndividualPortalConstants.TRUE;
			sepDisenroll = IndividualPortalConstants.TRUE;
			model.addAttribute("enableSep", enableSep);
			model.addAttribute("sepDisenroll", sepDisenroll);
			model.addAttribute("sepEventsNumber", ssapApplicationResource.getCaseNumber());
			return;
		}
		BigDecimal ssap = (BigDecimal) ssapId;
		SsapApplicationEvent sepEvent = getSsapApplicationEventById(ssap.longValue());
		if(null!=sepEvent){
			LOGGER.debug("SSAP Application Event exists");
			enableSep = IndividualPortalConstants.TRUE;
			sepLinkEvents=IndividualPortalConstants.TRUE;
			if(null!=sepEvent.filteredPreferredSsapApplicantEvents() && !sepEvent.filteredPreferredSsapApplicantEvents().isEmpty()){
				LOGGER.debug("SSAP Applicant Events are already captured");
				enableSep = IndividualPortalConstants.TRUE;
				sepLinkEvents=IndividualPortalConstants.FALSE;
				sepPlanSelection=IndividualPortalConstants.TRUE;
			}
		}else {
			LOGGER.debug("SSAP Application Event is null. Event is not created");
			enableSep = IndividualPortalConstants.TRUE;
			sepLinkEvents=IndividualPortalConstants.TRUE;
		}
		if(IndividualPortalConstants.TRUE.equalsIgnoreCase(enableSep)){
			model.addAttribute("sepEventsNumber", ssapApplicationResource.getCaseNumber());
			LocalDate today = TSLocalTime.getLocalDateInstance();
			if(null!=sepEvent && sepEvent.getEnrollmentEndDate() != null) {
                if (today.isAfter(new LocalDate(sepEvent.getEnrollmentEndDate().getTime()))) {
                    model.addAttribute("sepQepOver", "Y");
                }
            }
		}
		model.addAttribute("enableSep", enableSep);
		model.addAttribute("sepLinkEvents", sepLinkEvents);
		model.addAttribute("sepPlanSelection", sepPlanSelection);
		LOGGER.debug("enableSep:"+enableSep);
		LOGGER.debug("sepLinkEvents:"+sepLinkEvents);
		LOGGER.debug("sepEventsNumber:"+ssapApplicationResource.getCaseNumber());
	}

	/**
	 * Story HIX-58890 :: QEP: Dashboard Messaging for QEP Application
	 *
	 * @param model
	 * @param ssapApplicationResource
	 * @param enableQep
	 * @param qepLinkEvents
	 * @param qepDisenroll
	 * @param qepPlanSelection
	 *
	 * @author Sunil Sahoo
	 */
	private void evaluateQepEvents(Model model, SsapApplicationResource ssapApplicationResource, String enableQep, String qepLinkEvents, String qepDisenroll, String qepPlanSelection) throws GIException{
		LOGGER.debug("QEP Application is being processed with case number = "+ssapApplicationResource.getCaseNumber());
		Object ssapId = getIdFromCaseNumber(ssapApplicationResource.getCaseNumber());
		if (ssapId == null) {
			throw new GIException("Invalid Case Number");
		}
		BigDecimal ssap = (BigDecimal) ssapId;
		SsapApplicationEvent qepEvent = getSsapApplicationEventById(ssap.longValue());
		if(null!=qepEvent){
			LOGGER.debug("SSAP Application Event exists");
			enableQep = IndividualPortalConstants.TRUE;
			qepLinkEvents=IndividualPortalConstants.TRUE;
			if(null!=qepEvent.filteredPreferredSsapApplicantEvents() && !qepEvent.filteredPreferredSsapApplicantEvents().isEmpty()){
				LOGGER.debug("SSAP Applicant Events are already captured");
				enableQep = IndividualPortalConstants.TRUE;
				qepLinkEvents=IndividualPortalConstants.FALSE;
				qepPlanSelection=IndividualPortalConstants.TRUE;
			}
		}else {
			LOGGER.debug("SSAP Application Event is null. Event is not created");
			enableQep = IndividualPortalConstants.TRUE;
			qepLinkEvents=IndividualPortalConstants.TRUE;
		}
		if(IndividualPortalConstants.TRUE.equalsIgnoreCase(enableQep)){
			model.addAttribute("qepEventsNumber", ssapApplicationResource.getCaseNumber());
			LocalDate today = TSLocalTime.getLocalDateInstance();
			if(null!=qepEvent && qepEvent.getEnrollmentEndDate() != null) {
                if (today.isAfter(new LocalDate(qepEvent.getEnrollmentEndDate().getTime()))) {
                    model.addAttribute("sepQepOver", "Y");
                }
            }
		}

		model.addAttribute("enableQep", enableQep);
		model.addAttribute("qepLinkEvents", qepLinkEvents);
		model.addAttribute("qepPlanSelection", qepPlanSelection);

		LOGGER.debug("enableQep:"+enableQep);
		LOGGER.debug("qepLinkEvents:"+qepLinkEvents);
		LOGGER.debug("qepPlanSelection:"+ssapApplicationResource.getCaseNumber());
	}


	public SsapApplication getEnrolledSsapIdForCoverageYearAndHousehold(BigDecimal consumerHouseholdId, long coverageYear) throws GIException {
		SsapApplication enSsap = ssapApplicationRepository.findEnPnSsapApplicationForCoverageYear(consumerHouseholdId,coverageYear);
		if(enSsap == null) {
			 enSsap = getErAppWithOnlyDentalEnrl(consumerHouseholdId, coverageYear);
		}
		return enSsap;		
	}

	public boolean getAddressValidation(PreferencesDTO communicationPreferenceDTO){
		boolean isInvalid = true;
		if(communicationPreferenceDTO != null
				&& communicationPreferenceDTO.getLocationDto() != null
				&& StringUtils.isNotBlank(communicationPreferenceDTO.getLocationDto().getAddressLine1())
				&& StringUtils.isNotBlank(communicationPreferenceDTO.getLocationDto().getCity())
				&& StringUtils.isNotBlank(communicationPreferenceDTO.getLocationDto().getState())
				&& StringUtils.isNotBlank(communicationPreferenceDTO.getLocationDto().getZipcode())) {
			isInvalid = false;
		}
		return isInvalid;
	}

	@SuppressWarnings("unchecked")
	public Object[] getIdAndApplicationTypeFromCaseNumber(String caseNumber){
		EntityManager em = emf.createEntityManager();
		Object[] result = null;
		try{
			Query query = em.createNativeQuery("select id, application_type, NATIVE_AMERICAN_FLAG, creation_timestamp from ssap_applications where case_number=:caseNumber");
			query.setParameter(IndividualPortalConstants.CASE_NUMBER, caseNumber);
			List<Object[]> resultSet=query.getResultList();
			result = resultSet.get(0);
		}catch(Exception ex){
			LOGGER.debug("Exception while getIdFromCaseNumber ", ex);
		}finally{
			em.close();
		}

		return result;
	}

	/**
	 * Story HIX-48237
	 * As an Exchange, allow consumer (House hold) to
	 * change the Elected APTC Amount
	 *
	 * Sub-task HIX-57890 Consume API to update APTC
	 *
	 * Fetches the ID, CMR_HOUSEHOLD_ID and MAX_APTC
	 * for a SSAP
	 *
	 * @param caseNumber - Case number for the SSAP
	 * @return Object[]
	 *
	 * @author Nikhil Talreja
	 */
	@SuppressWarnings("unchecked")
	public Object[] getIdHHidMaxAptc(String caseNumber){

		EntityManager em = emf.createEntityManager();
		Object[] result = null;
		try{
			Query query = em.createNativeQuery("select ID, CMR_HOUSEOLD_ID, MAXIMUM_APTC from ssap_applications where case_number=:caseNumber");
			query.setParameter(IndividualPortalConstants.CASE_NUMBER, caseNumber);
			List<Object[]> resultSet=query.getResultList();
			result = resultSet.get(0);
		}catch(Exception ex){
			LOGGER.debug("Exception while getIdFromCaseNumber ", ex);
		}finally{
			em.close();
		}

		return result;
	}

	/**
	 * Story HIX-48237
	 * As an Exchange, allow consumer (House hold) to
	 * change the Elected APTC Amount
	 *
	 * Sub-task HIX-57890
	 * Create api to accept enrollment id, elected aptc
	 * from plan summary page
	 *
	 * Makes the API call to change the elected APTC
	 * for the enrollment
	 *
	 * @author Nikhil Talreja
	 * @return Success/Failure
	 */
	public String changeElectedAPTC(AptcUpdate aptcUpdate, AccountUser user) throws GIException{

		try {

			//De-crypt enrollment Ids before calling API
			String healthId = aptcUpdate.getHealthEnrollmentId();
			if(StringUtils.isNotEmpty(healthId)){
				healthId = ghixJasyptEncrytorUtil.decryptStringByJasypt(healthId);
				aptcUpdate.setHealthEnrollmentId(healthId);
			}
			String dentalId = aptcUpdate.getDentalEnrollmentId();
			if(StringUtils.isNotEmpty(dentalId)){
				dentalId = ghixJasyptEncrytorUtil.decryptStringByJasypt(dentalId);
				aptcUpdate.setDentalEnrollmentId(dentalId);
			}
			if(null!=user && StringUtils.isNotBlank(user.getEmail())){
				aptcUpdate.setUserName(user.getEmail());
			}

			SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			if(StringUtils.isNotBlank(aptcUpdate.getAptcPremiumEffectiveDate()))  {
				aptcUpdate.setAptcEffectiveDate(formatter.parse(aptcUpdate.getAptcPremiumEffectiveDate()));
			}
			//HIX-66712 - Calling ghix-eligibility API to update APTC
			ObjectMapper mapper = new ObjectMapper();
			String response = (ghixRestTemplate.exchange(
					GhixEndPoints.EligibilityEndPoints.UPDATE_APTC_URL,
					user.getUserName(), HttpMethod.POST,
					MediaType.APPLICATION_JSON, String.class,
					mapper.writeValueAsString(aptcUpdate))).getBody();
			if (StringUtils.equalsIgnoreCase(response, GhixConstants.RESPONSE_SUCCESS)) {
				return IndividualPortalConstants.SUCCESS;
			} else {
				return IndividualPortalConstants.FAILURE;
			}

		}
		catch(Exception e){
			LOGGER.error("Error occured while calling enrollment API  to change elected APTC: ",e);
			persistToGiMonitor(e);
			return IndividualPortalConstants.FAILURE;
		}

	}

	/**
	 * Sub-task HIX-59136
	 * Create API to retrieve favorite plan details or enrolled plan details.<br>
	 *
	 * Makes the respective API call to show the selected
	 * favorite plan or enrolled plan on the basis of isEnrolled Flag.
	 *
	 * <li>If isEnrolled=true : View the Enrolled plan details on Individual Dash board.</li>
	 * <li>If isEnrolled=false : View the Favorite plan details on Individual Dash board.</li>
	 *
	 * @author Sunil Sahoo
	 *
	 * @return PlanTile
	 */
	public PlanTile setSelectedPlanDetails(PlanTileRequest planTileRequest, EligLead eligLead, Household household, boolean isEnrolled) throws GIException{
		PlanTile planTile = null;
		if (isEnrolled) {
			planTile = addEnrolledPlanDetailsToModel(planTileRequest);
		}else {
			planTile = addSelectedPlanDetailsToModel(eligLead, household);
		}
		return planTile;
	}

	/**
	 * Sub-task HIX-59136
	 * Create API to retrieve favorite plan details or enrolled plan details.<br>
	 *
	 * Populate the PlanTile object with all required fields to view Favorite Plan details
	 *
	 * @author Sunil Sahoo
	 *
	 * @return PlanTile
	 */
	private PlanTile addSelectedPlanDetailsToModel(EligLead eligLead, Household household) throws GIException {
		Map<String, PlanResponse> selectedPlans = getFavoritePlanDetails(Long.valueOf(eligLead.getId()),2015);
		PlanResponse selectedPlan = selectedPlans.get(IndividualPortalConstants.HEALTH_PLAN);
		PlanTile planTile = new PlanTile();
		if (selectedPlan != null) {
			planTile.setPlanName(selectedPlan.getPlanName());
			planTile.setMonthlyPremium(selectedPlan.getPlanPremium());
			planTile.setNetPremium(selectedPlan.getPlanPremium());
			String aptc = eligLead.getAptc();
			aptc = StringUtils.replace(aptc, "$", "");
			if(NumberUtils.isNumber(aptc)){
				double netPremium = (double)selectedPlan.getPlanPremium() - (double)Float.parseFloat(aptc);
				planTile.setNetPremium(getFloatValue(String.valueOf(Math.round(netPremium*IndividualPortalConstants.HUNDRED)/IndividualPortalConstants.HUNDRED)));
			}
			planTile.setPlanType(selectedPlan.getNetworkType());

			planTile.setOfficeVisit(selectedPlan.getOfficeVisit());
			planTile.setGenericMedication(selectedPlan.getGenericMedications());
			if (isSingleMemberEligibleHousehold(household)) {
				if (selectedPlan.getDeductible() != null) {
					planTile.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL+ String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(selectedPlan.getDeductible())));
				}
				if (selectedPlan.getOopMax() != null) {
					planTile.setOutOfPocket(IndividualPortalConstants.DOLLAR_SYMBOL+ String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(selectedPlan.getOopMax())));
				}

			} else {
				if (selectedPlan.getDeductibleFamily() != null) {
					planTile.setDeductible(IndividualPortalConstants.DOLLAR_SYMBOL	+ String.format(IndividualPortalConstants.NUMBER_FORMAT,getFloatValue(selectedPlan.getDeductibleFamily())));
				}
				if (selectedPlan.getOopMaxFamily() != null) {
					planTile.setOutOfPocket(IndividualPortalConstants.DOLLAR_SYMBOL+ String.format(IndividualPortalConstants.NUMBER_FORMAT,	getFloatValue(selectedPlan.getOopMaxFamily())));
				}
			}
			planTile.setIssuerLogo(selectedPlan.getIssuerLogo());
			planTile.setFavoritePlanSelected(true);
			planTile.setPlanId(selectedPlan.getPlanId());
			Date coverageDate = computeEffectiveDate().getTime();
			SimpleDateFormat df = new SimpleDateFormat("MMddyyyy");
			planTile.setCoverageDate(df.format(coverageDate));

		} else {
			planTile.setFavoritePlanSelected(false);
		}
		return planTile;
	}

	/**
	 * Sub-task HIX-59136
	 * Create API to retrieve favorite plan details or enrolled plan details.<br>
	 *
	 * Populate the PlanTile object with all required fields to view Enrolled Plan details
	 *
	 * @author Sunil Sahoo
	 *
	 * @return PlanTile
	 */
	private PlanTile addEnrolledPlanDetailsToModel(PlanTileRequest planTileRequest) {
		PlanTile planTile = new PlanTile();
		Map<String, PlanSummaryDetails> planDetailsMap = getEnrollmentDetails(getIdFromCaseNumber(planTileRequest.getCaseNumber()).toString(), planTileRequest.getCaseNumber());
		PlanSummaryDetails selectedPlan = (planDetailsMap != null && !planDetailsMap.isEmpty()) ? planDetailsMap.get(IndividualPortalConstants.HEALTH_PLAN) : null;
		if (selectedPlan != null) {
			planTile.setPlanName(selectedPlan.getPlanName());
			planTile.setMonthlyPremium(getFloatValue(selectedPlan.getPremium()));
			planTile.setNetPremium(getFloatValue(selectedPlan.getNetPremium()));
			planTile.setPlanType(selectedPlan.getPlanType());
			planTile.setOfficeVisit(selectedPlan.getOfficeVisit());
			planTile.setGenericMedication(selectedPlan.getGenericMedications());
			planTile.setDeductible(selectedPlan.getDeductible());
			planTile.setOutOfPocket(selectedPlan.getOopMax());
			planTile.setIssuerLogo(selectedPlan.getPlanLogoUrl());
			planTile.setFavoritePlanSelected(true);
			planTile.setPlanId(Integer.valueOf(selectedPlan.getPlanId()));
			planTile.setCoverageDate(selectedPlan.getCoverageStartDate().replaceAll("/", ""));
			planTile.setCoverageStartDate(selectedPlan.getCoverageStartDate());
			planTile.setCoverageEndDate(selectedPlan.getCoverageEndDate());
			planTile.setIssuerContactNo(selectedPlan.getIssuerContactPhone());

			if(IndividualPortalConstants.IND_DSPL_STATUS.containsKey(selectedPlan.getEnrollmentStatus())){
				planTile.setEnMessage(IndividualPortalConstants.IND_DSPL_STATUS.get(selectedPlan.getEnrollmentStatus()));
			}
		} else {
			planTile.setFavoritePlanSelected(false);
		}
		return planTile;
	}

	/**
	 * HIX-59752
	 * Prevent Disenroll Action on an enrolled application
	 * if there is any another application in
	 * Eligibility Received status
	 *
	 * Checks if SSAP exists based on CMR_HOUSEHOLD_ID, Coverage year and
	 * application status
	 *
	 * @author Nikhil Talreja
	 */
	public boolean getSsapByStatusAndCoverageYear(int cmrHouseholdId, String coverageYear){

		try{
			List<Long> appliationIdList = ssapApplicationRepository.getErAppWithNoDentalEnrl(new BigDecimal(cmrHouseholdId), Long.valueOf(coverageYear));
			if(appliationIdList != null && !appliationIdList.isEmpty()){
				return true;
				}
			
		}catch(Exception ex){
			LOGGER.debug("Exception while getSsapByStatusAndCoverageYear ", ex);
		}

		return false;
	}

	public Map<String, String> getEnrollmentsForReinstatement(long applicationId, String caseNumber){
		Map<String, String> enrollmentInformation = new HashMap<String, String>(0);
		Map<String, PlanSummaryDetails> enrollments = getEnrollmentDetails(String.valueOf(applicationId), caseNumber);
		if(enrollments!=null && !enrollments.isEmpty()){

			PlanSummaryDetails healthPlan = enrollments.get("health");
			if(healthPlan!=null && (healthPlan.getEnrollmentStatus().equalsIgnoreCase("Cancelled") || healthPlan.getEnrollmentStatus().equalsIgnoreCase("Terminated"))){
				enrollmentInformation.put("Health", healthPlan.getEnrollmentId());
			}

			PlanSummaryDetails dentalPlan = enrollments.get("dental");
			if(dentalPlan!=null && (dentalPlan.getEnrollmentStatus().equalsIgnoreCase("Cancelled") || dentalPlan.getEnrollmentStatus().equalsIgnoreCase("Terminated"))){
				enrollmentInformation.put("Dental", dentalPlan.getEnrollmentId());
			}
		}
		return enrollmentInformation;
	}

	/**
	 * Checks if there is any Pending/Cancelled enrollment
	 *
	 * @param applicationId
	 * @param caseNumber
	 * @return
	 * @throws ParseException
	 */
	public boolean checkEnrollmentsForCancellation(long applicationId, String caseNumber) throws ParseException{

        boolean hasActiveEnrollments = false;

        Map<String, PlanSummaryDetails> enrollments = getEnrollmentDetails(String.valueOf(applicationId), caseNumber);

        if(enrollments!=null && !enrollments.isEmpty()){

            PlanSummaryDetails healthPlan = enrollments.get("health");
            boolean isHealthActive = isEnrollmentActive(healthPlan);

            PlanSummaryDetails dentalPlan = enrollments.get("dental");
            boolean isDentalActive = isEnrollmentActive(dentalPlan);

            hasActiveEnrollments = isHealthActive || isDentalActive;
        }

        return hasActiveEnrollments;
    }

	/**
	 * HIX-57896 Refactor controller methods to validate
	 * household Id with active module Id
	 *
	 * Validates the logged in user's active module Id
	 * with the case number being updated
	 * 
	 * Changes to handle multiple exceptions as per HIX-101736
	 *
	 * @author Nikhil Talreja
	 *
	 */
	public void validateActiveModuleId(String caseNumber, int activeModuleId) throws AccessDeniedException{

		try {
			if(StringUtils.isAnyBlank(caseNumber)) {
				throw new Exception("Case Number cannot be null/empty");
			}
		
			Object[] values = getIdHHidMaxAptc(caseNumber);	
			if(null == values || null == values[1] || StringUtils.isAnyBlank(values[1].toString())) {
				throw new Exception("CMR household Id does not exist for the given Case number [" + caseNumber + "]");
			}

			if(activeModuleId == -1){
					AccountUser user = this.userService.getLoggedInUser();
					activeModuleId = user.getActiveModuleId();
			}
	
			int cmrHouseholdId = new BigDecimal(values[1].toString()).intValue();
			if(activeModuleId != cmrHouseholdId) {
				throw new Exception("CMR household Id [" + cmrHouseholdId + "] does not match active module Id [" + activeModuleId + "]");
			}
		} catch(Exception ex) {
			//persistToGiMonitor(ex);
			throw new AccessDeniedException(ex.getMessage(), ex);
		}
	}

	/**
	 * <h1>Improvement : HIX-62422</h1>
	 *
	 * This method helps to calculate the termination date or benifit effective
	 * end date, while household disenrolling from Health or Dental enrollments.
	 *
	 * <li>If the current date is within 1-15 of a month, then set termination to end of the month.</li>
	 * <li>If the current date is within 16-End of a month, then set termination to end of the next month.</li>
	 *
	 * <h1>Story : HIX-62535</h1>
	 *
	 * The switch case used to return appropriate termination date
	 * while user select the CURRENT_MONTH, NEXT_MONTH and MONTH_AFTER_NEXT_MONTH
	 *
	 * <li>CURRENT_MONTH : Returns current month end date as termination date</li>
	 * <li>NEXT_MONTH : Returns next month end date as termination date</li>
	 * <li>MONTH_AFTER_NEXT_MONTH : Returns end date of month after next month as termination date</li>
	 *
	 * @author Sunil Sahoo
	 * @param coverageStartDate
	 * @throws GIException
	 *
	 */
	public Date getTerminationDateOnDisenrollment (TerminationDateChoice dateChoice, Date coverageStartDate) throws GIException{

		LocalDate currentDate = TSLocalTime.getLocalDateInstance();
		LocalDate terminationDate = null;

		if (dateChoice != null) {

			switch (dateChoice) {

			case CURRENT_MONTH:
				terminationDate = currentDate.dayOfMonth().withMaximumValue();
				break;
			case NEXT_MONTH:
				terminationDate = currentDate.plusMonths(IndividualPortalConstants.ONE).dayOfMonth().withMaximumValue();
				break;
			case MONTH_AFTER_NEXT_MONTH:
				terminationDate = currentDate.plusMonths(IndividualPortalConstants.TWO).dayOfMonth().withMaximumValue();
				break;
			default:
				throw new GIException("Invalid choice for termination date");
			}
		}else {
			if (coverageStartDate != null && coverageStartDate.after(new TSDate())) {
				// Fix for HIX-93506
				terminationDate = new LocalDate(coverageStartDate);
			}else {
				if (currentDate.getDayOfMonth() <= IndividualPortalConstants.FIFTEEN
						|| currentDate.getMonthOfYear() ==	DateTimeConstants.DECEMBER) {
					terminationDate = currentDate.dayOfMonth().withMaximumValue();
				}else {
					terminationDate = currentDate.plusMonths(IndividualPortalConstants.ONE).dayOfMonth().withMaximumValue();
				}
			}
		}
		return terminationDate.toDate();
	}


	/**
	 * Utitlity method to update the application status to 'ER'
	 * if application type are 'OE'/'QEP' and present in OEP period.
	 *
	 * @param indDisenroll
	 * @param applicationType
	 * @return updateFlag
	 */
	public boolean updateReEnrollment(IndDisenroll indDisenroll, String applicationType, String applicationStatus, Date applicationCreationTime) {

		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
		boolean updateFlag = true;

		try {
			if (StringUtils.equalsIgnoreCase("OE", applicationType) || StringUtils.equalsIgnoreCase("SEP", applicationType)) {
					Date coverageStartDate = formatter.parse(indDisenroll.getCoverageStartDate());
					LOGGER.debug("Disenrolling the application is complete");
					Boolean reEnroll = getReEnrollmentFlag(coverageStartDate, formatter.parse(indDisenroll.getTerminationDate()), applicationCreationTime);
					if(reEnroll){
						LOGGER.debug("Resetting the app status to ER");
						updateFlag = updateApplicationStatus(indDisenroll.getCaseNumber(), applicationStatus);
					}
				}
		} catch (Exception e) {
			LOGGER.error("Error while updating enrollment status to ER : ",e);
		}
		return updateFlag;
	}

	/**
	 * Gets the pre eligibility results for a lead for a particular year
	 *
	 * @author Nikhil Talreja
	 *
	 * @param lead
	 * @param newResult
	 * @param coverageYear
	 * @return
	 */
	protected PreEligibilityResults getPreEligResultForCoverageYear(EligLead lead, int coverageYear){

		if(lead != null){
			List<PreEligibilityResults> results = lead.getPreEligibilityResults();
			if(results != null && !results.isEmpty()){
				for(PreEligibilityResults result: results){
					if(result.getCoverageYear() == coverageYear){
						return result;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Calculates the termination date when a HH loses eligibility
	 * during SEP
	 *
	 * Coverage End = End of month except when primary dies
	 * Coverage End when Primary dies = Date of Death
	 *
	 * @author Nikhil Talreja
	 *
	 * @param caseNumber
	 */
	protected LocalDate calculateTerminationDateForSEP(String caseNumber, Object applicationId) throws GIException{

		//HIX-68967 - Household loses eligibility - End of month except primary death
		LocalDate terminationDate = TSLocalTime.getLocalDateInstance().dayOfMonth().withMaximumValue();

		//Applicant events for SEP
		BigDecimal ssapId = (BigDecimal)applicationId;
		SsapApplicationEvent ssapEvent = getSsapApplicationEventById(ssapId.longValue());
		List<SsapApplicantEvent> applicantEvents = ssapEvent.getSsapApplicantEvents();

		//HIX-68967 - Death date of primary is termination death
		for(SsapApplicantEvent applicantEvent : applicantEvents){
			SsapApplicant applicant = applicantEvent.getSsapApplicant();
			if(applicant.getPersonId() == 1){
				SepEvents sepEvent = applicantEvent.getSepEvents();
				if(sepEvent != null && StringUtils.equalsIgnoreCase(sepEvent.getName(),
						LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH)){
					terminationDate = new LocalDate(applicantEvent.getEventDate().getTime());
					break;
				}
			}
		}

		return terminationDate;
	}

	/**
	 * Update the dental APTC amount while Reinstate Health enrollment
	 * If the dental enrollment is in active or pending status
	 *
	 * @author Sunil Sahoo
	 *
	 * @param reinstateEnrollmentDTO
	 * @param user
	 * @param caseNumber
	 *
	 */
	public void updateAptcOnReinstate(ReinstateEnrollmentDTO reinstateEnrollmentDTO, AccountUser user, String caseNumber) {

		try {

			AptcUpdate aptcUpdate = new AptcUpdate();
			String applicationId =getIdFromCaseNumber(caseNumber).toString();
			Map<String, PlanSummaryDetails> enrollments = getEnrollmentDetails(applicationId, caseNumber);
			if (enrollments!=null && !enrollments.isEmpty()) {
				PlanSummaryDetails healthPlan = enrollments.get("health");
				PlanSummaryDetails dentalPlan = enrollments.get("dental");
				if (null != healthPlan && null != healthPlan.getMaxAptc() &&
						null != dentalPlan &&
						Float.valueOf(healthPlan.getMaxAptc()) > 0.0) {
					aptcUpdate.setCaseNumber(caseNumber);
					aptcUpdate.setHealthEnrollmentId(healthPlan.getEnrollmentId());
					aptcUpdate.setAptcAmt(Float.valueOf(healthPlan.getMaxAptc()));
					aptcUpdate.setDentalEnrollmentId(dentalPlan.getEnrollmentId());
					changeElectedAPTC(aptcUpdate, user);
				}
			}
		}catch (GIException e) {
			LOGGER.error("Aptc Update while Reinstatement Enrollment is not successful. ", e);
			persistToGiMonitor(e);
		}
	}


	public boolean isOEEnrollmentOpen() {
		String enrolledStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
		String enrolledEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);
		if(StringUtils.isNotBlank(enrolledStartDate) && StringUtils.isNotBlank(enrolledEndDate)){
			try{
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
				LocalDate enrolledBeginDate = new DateTime(formatter.parseObject(enrolledStartDate)).toLocalDate();
				LocalDate enrolledLastDate = new DateTime(formatter.parseObject(enrolledEndDate)).toLocalDate();
				if (todaysDate.isBefore(enrolledBeginDate) || todaysDate.isAfter(enrolledLastDate)) {
					return false;
				}
			}
			catch(Exception ex){
				LOGGER.error("Error while parsing enroll date",ex);
			}
		}
		return true;
	}

	public String reverseSepQepDenialApp(String caseNumber, AccountUser user, String enrollmentEndDate){

		String responseVal = IndividualPortalConstants.FAILURE;

		try {

			SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			LocalDate enrollEndDate = new LocalDate(dateFormat.parse(enrollmentEndDate).getTime());
			String enrollmentGracePeriod = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
			LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
			LocalDate sepQepDenialDate = todaysDate.plusDays(IndividualPortalConstants.SIXTY);
			
			if (null != enrollmentGracePeriod) {
				sepQepDenialDate = sepQepDenialDate.plusDays(Integer.valueOf(enrollmentGracePeriod));
			}

			if (enrollEndDate.isBefore(TSLocalTime.getLocalDateInstance()) || enrollEndDate.isAfter(sepQepDenialDate)) {
				return responseVal = IndividualPortalConstants.FAILURE;
			}

			Long ssapId = ((BigDecimal)getIdFromCaseNumber(caseNumber)).longValue();

			SsapApplicationEvent ssapEvent = getSsapApplicationEventById(ssapId);
			if(null == ssapEvent) {
				throw new GIException("No SSAP Application Event found for application id [" + ssapId +"]");
			} else {
				restoreSepDenialEvents(ssapEvent, user, enrollEndDate);
				responseVal = IndividualPortalConstants.SUCCESS;
			}
			
		} catch (GIException e) {
			LOGGER.error("SSAP Status update while SEP/QEP denied. ", e);
			persistToGiMonitor(e);
			responseVal = IndividualPortalConstants.FAILURE;
		} catch (ParseException e) {
			LOGGER.error("Enrollment end date parsing error while SEP/QEP denied. ", e);
			persistToGiMonitor(e);
			responseVal = IndividualPortalConstants.FAILURE;
		}

		return responseVal;
	}

	private void restoreSepDenialEvents(SsapApplicationEvent ssapApplicationEvent, AccountUser user, LocalDate enrollEndDate) {

		SsapApplication ssapApplication = ssapApplicationEvent.getSsapApplication();

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();

		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if (ApplicantEventValidationStatus.CANCELLED == ssapApplicantEvent.getValidationStatus()){
					if (ssapApplicantEvent.getSepEvents().getGated() == Gated.Y){
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING);
					} else {
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
					}
					ssapApplicantEvent.setLastUpdatedBy(user.getId());
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
				}
			}


			boolean allEventsVerified = areEventsVerified(applicantEvents);
			if (allEventsVerified) {
				ssapApplication.setValidationStatus(ApplicationValidationStatus.VERIFIED);
			} else {
				/* handle exit path */
				if (ssapApplication.getValidationStatus() != ApplicationValidationStatus.PENDING) {
					ssapApplication.setValidationStatus(ApplicationValidationStatus.PENDING);
				}
			}
		}

		ssapApplication.setAllowEnrollment(ReferralConstants.Y);
		ssapApplication.setSepQepDenied(null);
		ssapApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		ssapApplication.setLastUpdatedBy(BigDecimal.valueOf(user.getId()));
		ssapApplication.setLastUpdateTimestamp(currentTimestamp);


		ssapApplicationEvent.setEnrollmentStartDate(currentTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(enrollEndDate.toDate().getTime()));
		ssapApplicationEvent.setLastUpdatedBy(user.getId());
		ssapApplicationEvent.setLastUpdateTimestamp(currentTimestamp);
		ssapApplicationEventRepository.save(ssapApplicationEvent);
	}

	private boolean areEventsVerified(List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents) {

		boolean allEventsVerified = true;
		if (applicantEvents != null){
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if (OPEN_EVENT_VALIDATION_STATUS.contains(ssapApplicantEvent.getValidationStatus())) {
					allEventsVerified = false;
					break;
				}
			}
		}
		return allEventsVerified;
	}

	private static final Set<ApplicantEventValidationStatus> OPEN_EVENT_VALIDATION_STATUS = EnumSet.of(
			ApplicantEventValidationStatus.INITIAL, ApplicantEventValidationStatus.PENDING,
			ApplicantEventValidationStatus.SUBMITTED, ApplicantEventValidationStatus.REJECTED);

	public long checkForActiveApplication(int cmrHouseoldId, String coverageYear) {

		long ssapCount = ssapApplicationRepository.countOfSsapApplicationsByStatusAndCoverageYear(new BigDecimal(cmrHouseoldId), Long.valueOf(coverageYear));

		return ssapCount;
	}

	public boolean isActiveApplicationDeniedEligibility(long cmrHouseoldId, int coverageYear) {

		try {
			SsapApplicationResource ssapApplicationResource = getApplicationForCoverageYearForPortalHome(cmrHouseoldId, coverageYear);
			if (ssapApplicationResource!=null && StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationType(), "SEP")
					&& (StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationStatus(), "ER") || StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationStatus(), "PN"))
					&& StringUtils.equalsIgnoreCase(ssapApplicationResource.getEligibilityStatus(), "DE")) {
				return true;
			}
		} catch (IOException e) {
			LOGGER.error("Failed to fetch the application details for portal home.", e);
			persistToGiMonitor(e);
		}
		return false;
	}

	protected void populateHouseholdFromPreference(PreferencesDTO communicationPreferenceDTO, Household household, AccountUser user) throws ParseException, GIException {
		final String language = "LANGUAGE";
		final String communicationMode = "PreferredContactMode";

		final Map<String,String> languageMap = new HashMap<>(2);
		languageMap.put("SP-ENG", "English");
		languageMap.put("SP-SPA", "Spanish");

		if(communicationPreferenceDTO.isContactPreferencesUpdated()) {
	        //Preferred spoken language
	        String prefSpokenLang = communicationPreferenceDTO.getPrefSpokenLang();
	        LookupValue lookupForSpokenLang = lookUpService.getlookupValueByTypeANDLookupValueCode(language, languageMap.get(prefSpokenLang));
	        household.setPrefSpokenLang(lookupForSpokenLang.getLookupValueId());

	        //Preferred written language
	        String prefWrittenLang = communicationPreferenceDTO.getPrefWrittenLang();
	        LookupValue lookupForWrittenLang = lookUpService.getlookupValueByTypeANDLookupValueCode(language, languageMap.get(prefWrittenLang));
	        household.setPrefWrittenLang(lookupForWrittenLang.getLookupValueId());

	        //Preferred contact method
	        GhixNoticeCommunicationMethod prefCommunication = communicationPreferenceDTO.getPrefCommunication();
	        LookupValue lookupForCommun = lookUpService.getlookupValueByTypeANDLookupValueCode(communicationMode, prefCommunication.getMethod());
            household.setPrefContactMethod(lookupForCommun.getLookupValueId());

	        //Phone number
	        String phoneNumber = communicationPreferenceDTO.getPhoneNumber();
	        household.setPhoneNumber(phoneNumber.trim());
		}

		if(communicationPreferenceDTO.isMailingAddressUpdated()) {
			//Address updation
			Location location = populateLocationForCommPref(communicationPreferenceDTO, user);
	        household.setZipCode(communicationPreferenceDTO.getLocationDto().getZipcode().trim());
	        household.setPrefContactLocation(location);
		}

		if(!communicationPreferenceDTO.isEditMode()) {
			SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			Date dob = formatter.parse(communicationPreferenceDTO.getDob());
	        household.setBirthDate(dob);
		}
		household.setTextMe(communicationPreferenceDTO.getTextMe());
		household.setMobileNumberVerified(communicationPreferenceDTO.getMobileNumberVerified());
		
		if(household.getOptInPaperless1095() != communicationPreferenceDTO.getOptInPaperless1095()) {
			household.setOptInPaperless1095(communicationPreferenceDTO.getOptInPaperless1095());
			household.setPaperless1095Date(new TSDate());
			household.setPaperless1095User(user.getId());
		}
	}

	/**
	 * Call Enrollment API and creates Location object with updated data
	 * @param householdId
	 * @return
	 * @throws GIException
	 */
	private Location populateLocationForCommPref(PreferencesDTO communicationPreferenceDTO, AccountUser user) throws GIException {
		boolean isResponseValid = false;
		String errmsg = "Enrollment Updation Failure";

		ResponseEntity<PreferenceEnrollResponse> response = null;
		Location location = new Location();

		//Populate Location data with Preference details
		location.setAddress1(communicationPreferenceDTO.getLocationDto().getAddressLine1().trim());
		if (StringUtils.isNotBlank(communicationPreferenceDTO.getLocationDto().getAddressLine2())) {
			location.setAddress2(communicationPreferenceDTO.getLocationDto().getAddressLine2().trim());
		}
		location.setCity(communicationPreferenceDTO.getLocationDto().getCity().trim());
		location.setCounty(communicationPreferenceDTO.getLocationDto().getCountyName());
		location.setCountycode(communicationPreferenceDTO.getLocationDto().getCountyCode());
		location.setState(communicationPreferenceDTO.getLocationDto().getState().trim());
		location.setZip(communicationPreferenceDTO.getLocationDto().getZipcode().trim());

		location = locationService.save(location);

		final Map<String, String> request = new HashMap<String, String>(2);
		request.put("household_case_id", String.valueOf(user.getActiveModuleId()));
		request.put("location_id", String.valueOf(location.getId()));

		try {
			// Call Enrollment API to update Preferences details
			Gson gson = new Gson();
			
			LOGGER.debug("Calling enrollment api updateMailByHouseHoldId with details: [" + request + "]");
			response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_PREFERENCES_ENROLLMENT,
					user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON,
					PreferenceEnrollResponse.class, gson.toJson(request));

		} catch (Exception ex) {
			LOGGER.error("Exception occured while updating Preference details to enrollment with details: [" + request + "] : ", ex);
			throw new GIException("Exception occured while populating Location from Preference details ",ex);
		}


		if(response != null) {
			PreferenceEnrollResponse enrollmentResponse = response.getBody();
			if(enrollmentResponse != null
					&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)
					&& 200 == enrollmentResponse.getErrCode()) {
				isResponseValid = true;
			} else {
				if(enrollmentResponse != null) {
					errmsg += " " + enrollmentResponse.getErrMsg();
				}
			}
		}

		if(!isResponseValid) {
			LOGGER.error("Exception occured while updating Preference details to enrollment with details: [" + request + "] : ", errmsg);
			throw new GIException(errmsg);
		}
		return location;
	}

	protected PreferencesDTO convertToContactPreferenceDTO(IndPortalAddlnRegDetails indPortalAddlnRegDetails) {

		PreferencesDTO communicationPreferenceDTO = new PreferencesDTO();

        communicationPreferenceDTO.setPrefSpokenLang(indPortalAddlnRegDetails.getPrefSpokenLang());
        communicationPreferenceDTO.setPrefWrittenLang(indPortalAddlnRegDetails.getPrefWrittenLang());
        try {
        	communicationPreferenceDTO.setPrefCommunication(GhixNoticeCommunicationMethod.valueOf(indPortalAddlnRegDetails.getPrefCommunication()));
		} catch (IllegalArgumentException | NullPointerException ex) {
			LOGGER.error("Invalid Preferred Communication Method" + indPortalAddlnRegDetails.getPrefCommunication()
							+ " Valid values " + Arrays.asList(GhixNoticeCommunicationMethod.values()));
		}
        communicationPreferenceDTO.setPhoneNumber(indPortalAddlnRegDetails.getPhoneNumber());
        communicationPreferenceDTO.setDob(indPortalAddlnRegDetails.getDob());

        LocationDTO locationDto = new LocationDTO();
        locationDto.setAddressLine1(indPortalAddlnRegDetails.getAddressLine1());
        locationDto.setAddressLine2(indPortalAddlnRegDetails.getAddressLine2());
        locationDto.setCity(indPortalAddlnRegDetails.getCity());
        locationDto.setState(indPortalAddlnRegDetails.getState());
        locationDto.setZipcode(indPortalAddlnRegDetails.getZipcode());
        communicationPreferenceDTO.setLocationDto(locationDto);

        communicationPreferenceDTO.setContactPreferencesUpdated(true);
        communicationPreferenceDTO.setMailingAddressUpdated(true);

        return communicationPreferenceDTO;
    }

	/**
	 *
	 * @param household
	 * @param editMode
	 * @return
	 * @throws Exception
	 */
	protected PreferencesDTO getHouseholdPreferences(int activeModuleId, boolean editMode) throws GIException {

		PreferencesDTO communicationPreferenceDTO = preferencesService.getPreferences(activeModuleId, editMode);

		if(null == communicationPreferenceDTO) {
			throw new GIException("Error fetching Communication Preference details");
		}

		populateActiveEnrollmentDetails(activeModuleId, communicationPreferenceDTO);

		/* HIX-85108 */
		SsapApplication application = hasActiveApplication(activeModuleId, Arrays.asList("ER", "SG", "SU"));
		communicationPreferenceDTO.setActiveApplication(null != application);

        return communicationPreferenceDTO;
	}

	protected IndPortalValidationDTO validateCommunicationPrefData(PreferencesDTO communicationPreferenceDTO) throws GIException, ParseException {
		IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
		indPortalValidationDTO.setStatus(true);
		
		final Set<String> language = new HashSet<String>(2);
		language.add("SP-ENG");
		language.add("SP-SPA");

		if(communicationPreferenceDTO.isContactPreferencesUpdated()) {
	        //Preferred spoken language
	        String prefSpokenLang = communicationPreferenceDTO.getPrefSpokenLang();
	        if(!language.contains(prefSpokenLang)){
	        	//HIX-68352 - Security fix
	        	//throw new GIException("Invalid Spoken Lang Preference");
	        	setValidateError(indPortalValidationDTO, "Invalid Spoken Lang Preference");	        	
	        }

	        //Preferred written language
	        String prefWrittenLang = communicationPreferenceDTO.getPrefWrittenLang();
	        if(!language.contains(prefWrittenLang)){
	        	//HIX-68352 - Security fix
	        	//throw new GIException("Invalid Written Lang Preference");
	        	setValidateError(indPortalValidationDTO, "Invalid Written Lang Preference");
	        }

	        //Preferred contact method
	        /*try{
	        	@SuppressWarnings("unused")
				GhixNoticeCommunicationMethod prefCommunication = communicationPreferenceDTO.getPrefCommunication();
	        } catch(NullPointerException ex) {
	        	throw new GIException("Communication Method Preference is mandatory", ex);
	        }*/

	        String phoneNumber = communicationPreferenceDTO.getPhoneNumber();
	        if(StringUtils.isBlank(phoneNumber)){
	        	//throw new GIException("Phone number is mandatory");
	        	setValidateError(indPortalValidationDTO, "Phone number is mandatory");
	        } else if(!StringUtils.isNumeric(phoneNumber)) {
	        	//throw new GIException("Phone number is not numeric");
	        	setValidateError(indPortalValidationDTO, "Phone number is not numeric");
	        }
		}

		if(communicationPreferenceDTO.isMailingAddressUpdated()) {
			//Address validation
	        if(getAddressValidation(communicationPreferenceDTO)){
	        	//throw new GIException("Mandatory Address fields are not entered");
	        	setValidateError(indPortalValidationDTO, "Mandatory Address fields are not entered");
	        }
		}

		if(!communicationPreferenceDTO.isEditMode()) {
			String dateOfBirth = communicationPreferenceDTO.getDob();
	        //validations for not null
	        if(StringUtils.isEmpty(dateOfBirth)){
	            //throw new GIException("Date Of Birth is mandatory");
	        	setValidateError(indPortalValidationDTO, "Date Of Birth is mandatory");
	        }else{
	        	SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
		        formatter.setLenient(false);
		        //validations for DOB should not be greater than 104 and DOB should not be in future.
		        Date dob = formatter.parse(dateOfBirth);
		        Period period = new Period(dob.getTime(), new TSDate().getTime(),PeriodType.years());
		        int calculatedAge = period.getYears();
		        if (dob.after(new TSDate()) || calculatedAge > IndividualPortalConstants.MAX_AGE) {
		            //throw new GIException("Invalid Date Of Birth");
		        	setValidateError(indPortalValidationDTO, "Invalid Date Of Birth");
		        }
	        }	        
		}		
		return indPortalValidationDTO;
	}
	
	private void setValidateError(IndPortalValidationDTO indPortalValidationDTO, String errorMsg) {
		indPortalValidationDTO.setStatus(false);
		List<String> errors = indPortalValidationDTO.getErrors();
		errors = errors != null ? errors : new ArrayList<String>();
		errors.add(errorMsg);
		indPortalValidationDTO.setErrors(errors);
		
	}


	/**
	 * Check User has Active enrollment or not
	 * @param householdId
	 * @return
	 * @throws GIException
	 */
	private void populateActiveEnrollmentDetails(int householdId, PreferencesDTO preferencesDTO) throws GIException {

    	// If User has active enrollment
    	SsapApplication application  = hasActiveApplication(householdId, Arrays.asList("EN"));

        if(null != application && enrollmentIntegrationService.hasActiveEnrollment(application.getCaseNumber())) {
        	preferencesDTO.setHasActiveEnrollment(true);
        	preferencesDTO.setActiveEnrollmentYear(String.valueOf(application.getCoverageYear()));

        	String financialFlag = application.getFinancialAssistanceFlag();
        	if(StringUtils.isNotBlank(financialFlag) && "N".equalsIgnoreCase(financialFlag)){
        		preferencesDTO.setNonFinancial(true);
        	}
        	
			if (individualPortalUtil.isInsideOEWindow((int) application.getCoverageYear())) {
				preferencesDTO.setInsideOEEnrollementWindow(true);
			}
        }

	}

	 /**
     * Check if User has active enrollment for given coverage year
     * @param householdId
     * @param currentYear
     * @return
     */
    private List<SsapApplication> getApplicationByStatusAndCoverageYear(int householdId, String currentYear, List<String> applicationStatus) {

		int coverageYear = 0;
		if(StringUtils.isNumeric(currentYear)) {
			coverageYear = Integer.parseInt(currentYear);
		}

		//Current Enrolled application in EN state
        List<SsapApplication> application = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYearAndApplicationStatusIn(new BigDecimal(householdId),
        		coverageYear, applicationStatus);

        return application;
    }

	private List<SsapApplication> getApplicationByStatus(int householdId, List<String> applicationStatus) {

		//Current Enrolled application in EN state
		List<SsapApplication> application = ssapApplicationRepository.findByCmrHouseoldIdAndApplicationStatusIn(new BigDecimal(householdId),
				applicationStatus);

		return application;
	}

    /**
     * Check if any existing Application exists with specified application status for current or previous coverage year
     * @param householdId
     * @return
     * @throws GIException
     */
    protected SsapApplication hasActiveApplication(int householdId, List<String> applicationStatus) throws GIException {

    	SsapApplication application = null;

    	// If User has application with ER status
    	String coverageYr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
    	List<SsapApplication> applicationList  = getApplicationByStatusAndCoverageYear(householdId, coverageYr, applicationStatus);

        if(null == applicationList || applicationList.isEmpty()) {
        	// No ssap found for current coverage year
        	SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
        	String previousYearTabCutOff = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_PREVIOUS_YEAR_TAB);

        	if(StringUtils.isNotBlank(previousYearTabCutOff)){
				try {
					// Check if previous year tab cutoff is past
					LocalDate cutOff = new DateTime(formatter.parse(previousYearTabCutOff)).toLocalDate();
					LocalDate now = TSLocalTime.getLocalDateInstance();
	                boolean isPreviousCutoffValid = cutOff.isAfter(now) || cutOff.equals(now);

	                if(isPreviousCutoffValid) {
	                	coverageYr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
	                	applicationList = getApplicationByStatusAndCoverageYear(householdId, coverageYr, applicationStatus);
	                }

				} catch (ParseException e) {
					LOGGER.error("Error while checking Active Enrollment ",e);
				}
            }
        }

        if(applicationList != null && !applicationList.isEmpty()) {
        	if(applicationList.size() > 1) {
        		throw new GIException("More than one application with "+ applicationStatus + " status found for coverage year " + coverageYr);
        	}
        	application = applicationList.get(0);
        }

        return application;
    }

    /**
     * Validates Coverage Year request param for My Applications page.
     * @param selectedCoverageYr
     * @throws GIException
     */
    public IndPortalValidationDTO validateCoverageYrForApplications(String selectedCoverageYr) throws GIException {
    	IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
    	indPortalValidationDTO.setStatus(true);
		
   	 // If coverage year is selected
		if(StringUtils.isNotBlank(selectedCoverageYr)) {

			// If Year is not Numeric
			if(!StringUtils.isNumeric(selectedCoverageYr)) {
				//throw new GIException("Coverage Year should be numeric");
				setValidateError(indPortalValidationDTO, "Coverage Year should be numeric");
			}else{
				if(Long.parseLong(selectedCoverageYr) > Long.parseLong(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR))) {
					//throw new GIException("Selected Coverage Year cannot be greater than current System Year");
					setValidateError(indPortalValidationDTO, "Selected Coverage Year cannot be greater than current System Year");
				}
			}			
		}
		return indPortalValidationDTO;
	}

    protected String updateSSAPApplicationForOTR(String caseNumber) throws GIException {

    	String responseVal = IndividualPortalConstants.FAILURE;

    	try{
    		// Call eligibility API to update Renewal status to OTR
			AccountUser user = this.userService.getLoggedInUser();

			SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
			ssapApplicationRequest.setCaseNumber(caseNumber);
			ssapApplicationRequest.setUserId(new BigDecimal(user.getId()));

			ResponseEntity<String> response =  ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_SSAP_RENEWAL_STATUS,
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, ssapApplicationRequest);

			if(response != null && response.getStatusCode().equals(HttpStatus.OK)
					&& StringUtils.equalsIgnoreCase(response.getBody(), GhixConstants.RESPONSE_SUCCESS)) {
				responseVal = IndividualPortalConstants.SUCCESS;
			}
		}catch(Exception ex){
			LOGGER.error("Renewal Status Update of application to OTR  failed.", ex);
			persistToGiMonitor(ex);
			throw new GIException("Application Renewal Status updation to OTR failed ", ex);
		}
		return responseVal;
	}

    protected IndPortalValidationDTO validateIndDisenrollForTerminationDate(IndDisenroll indDisenroll) throws GIException {
    	IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
    	indPortalValidationDTO.setStatus(true);
    	
		if(StringUtils.isBlank(indDisenroll.getHealthEnrollmentId())
				&& StringUtils.isBlank(indDisenroll.getDentalEnrollmentId())) {
			//throw new GIException("Either Health or Dental Enrollment Id is required");
			setValidateError(indPortalValidationDTO, "Either Health or Dental Enrollment Id is required");
		}

		if(StringUtils.isBlank(indDisenroll.getTerminationDate())) {
			//throw new GIException("Termination date cannot be null");
			setValidateError(indPortalValidationDTO, "Termination date cannot be null");
		}else{
			SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			try {
				@SuppressWarnings("unused")
				LocalDate enrollCovEndDate = new DateTime(dateFormat.parseObject(indDisenroll.getTerminationDate())).toLocalDate();
			} catch (ParseException e) {
				//throw new GIException("Error parsing Termination Coverage End date");
				setValidateError(indPortalValidationDTO, "Error parsing Termination Coverage End date");
			}
		}
		

		/*if(StringUtils.isBlank(indDisenroll.getCaseNumber())) {
			//throw new GIException("Application case number cannot be null");
			setValidateError(indPortalValidationDTO, "Application case number cannot be null");
		}*/

		if(StringUtils.isBlank(indDisenroll.getReasonCode())) {
			//throw new GIException("Termination Reason code cannot be null");
			setValidateError(indPortalValidationDTO, "Termination Reason code cannot be null");
		}else{
			try{
				DisenrollReasonEnum.valueOf(indDisenroll.getReasonCode());
			} catch (IllegalArgumentException ex) {
				//throw new GIException("Invalid Termination Reason code entered", ex);
				setValidateError(indPortalValidationDTO, "Invalid Termination Reason code entered");
			}
		}
		
		return indPortalValidationDTO;
    }

    protected String updateEnrollmentTerminationDate(IndDisenroll indDisenroll, AccountUser user) throws GIException {

    	String response = IndividualPortalConstants.FAILURE;
    	String enrollId = null;

    	if(StringUtils.isNotBlank(indDisenroll.getHealthEnrollmentId())) {
    		enrollId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getHealthEnrollmentId());
    		indDisenroll.setHealthEnrollmentId(enrollId);
    	} else {
    		enrollId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getDentalEnrollmentId());
    		indDisenroll.setDentalEnrollmentId(enrollId);
    	}

    	EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
    	enrollmentRequest.setEnrollmentId(Integer.valueOf(enrollId));

    	List<EnrollmentAttributeEnum> attributeEnums = new ArrayList<EnrollmentAttributeEnum>();
    	attributeEnums.add(EnrollmentAttributeEnum.id);
    	attributeEnums.add(EnrollmentAttributeEnum.enrollmentStatusCode);
    	attributeEnums.add(EnrollmentAttributeEnum.benefitEndDate);
    	enrollmentRequest.getEnrollmentAttributeList().addAll(attributeEnums);

    	EnrollmentDTO enrollmentDTO = null;
    	try{
    		LOGGER.debug("Calling enrollment api getenrollmentatributebyid with enrollemnt id: [" + enrollId + "]");
	    	ResponseEntity<EnrollmentResponse> responseVal = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_ATRIBUTE_BY_ID,
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, EnrollmentResponse.class, enrollmentRequest);

	    	if(null == responseVal || null == responseVal.getBody()) {
	    		LOGGER.error("No enrollment found for given Enrollment id [" + enrollId + "]");
	    	} else {
	    		EnrollmentResponse enrollmentResponse = responseVal.getBody();
	    		if (enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
	    			enrollmentDTO = enrollmentResponse.getEnrollmentDTO();
	    		} else {
	    			throw new GIException("Error fetching Enrollment details for Termination date update for enrollment id [" + enrollId + "],"
							+ "Error Code: " + enrollmentResponse.getErrCode() + " Error message: " + enrollmentResponse.getErrMsg());
	    		}
	    	}
    	} catch(Exception ex) {
    		throw new GIException("Error fetching Enrollment details for Termination date update having enrollment id ["+ enrollId +"]",ex);
    	}

    	if(enrollmentDTO != null
    			&& StringUtils.equalsIgnoreCase(EnrollmentStatus.TERM.toString(), enrollmentDTO.getEnrlStatusCode())) {

    		SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
    		LocalDate reqTerminationEndDate = null;
    		try {
    			Object terminationDate = dateFormat.parseObject(indDisenroll.getTerminationDate());
    			reqTerminationEndDate = new DateTime(terminationDate).toLocalDate();
			} catch (ParseException e) {
				throw new GIException("Error parsing Termination Coverage End date",e);
			}

    		LocalDate enrollmentEndDate = null;
			try {
				enrollmentEndDate = new DateTime(dateFormat.parseObject(enrollmentDTO.getEnrollmentEndDate())).toLocalDate();
			} catch (ParseException e) {
				throw new GIException("Error parsing Enrollment Coverage End date",e);
			}

    		if(reqTerminationEndDate != null && enrollmentEndDate != null && reqTerminationEndDate.isBefore(enrollmentEndDate)) {
    			indDisenroll.setUserName(user.getUserName());
    			indDisenroll.setReasonCode(DisenrollReasonEnum.valueOf(indDisenroll.getReasonCode()).value());

    			if(StringUtils.isNotBlank(indDisenroll.getHealthEnrollmentId())) {
    				response = csrOverrideUtility.invokeHealthDisenrollment(indDisenroll);
    			} else  {
    				response = csrOverrideUtility.invokeDentalDisenrollment(indDisenroll);
    			}

    		} else {
    			LOGGER.error("Enrollment Termination End date is before or equal to new Termination Date set");
    		}
    	}

    	return response;
	}

    protected IndPortalValidationDTO validateAptcEffectiveDateData(AptcUpdate aptcUpdate) throws GIException {
    	IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
    	indPortalValidationDTO.setStatus(true);
		if(null == aptcUpdate) {
			//throw new GIException("AptcUpdate DTO cannot ne null");
			setValidateError(indPortalValidationDTO, "AptcUpdate DTO cannot ne null");
		}else{
	
	    	if(StringUtils.isBlank(aptcUpdate.getHealthEnrollmentId())
					&& StringUtils.isBlank(aptcUpdate.getDentalEnrollmentId())) {
				//throw new GIException("Either Health or Dental Enrollment Id is required");
				setValidateError(indPortalValidationDTO, "Either Health or Dental Enrollment Id is required");
			}
	
	    	if(StringUtils.isBlank(aptcUpdate.getCaseNumber())) {
				//throw new GIException("Application case number cannot be null");
				setValidateError(indPortalValidationDTO, "Application case number cannot be null");
			}else{
				validateActiveModuleId(aptcUpdate.getCaseNumber(), -1);
			}	
	    	
		}
    	
    	return indPortalValidationDTO;
	}

        /**
     * Call Enrollment API to populate APTC next month amount and start date
     * @param aptcUpdate
     * @throws GIException
     */
    protected void populateAPTCPremiumEffDate(AptcUpdate aptcUpdate) throws GIException {

    	Gson gson = new Gson();
		try {

			EnrollmentResponse enrollResp = null;
			EnrollmentRequest enrollReq = new EnrollmentRequest();
			List<Integer> idList = new ArrayList<Integer>();

			//De-crypt enrollment Ids before calling API
			if(StringUtils.isNotEmpty(aptcUpdate.getHealthEnrollmentId())){
				String healthId = ghixJasyptEncrytorUtil.decryptStringByJasypt(aptcUpdate.getHealthEnrollmentId());
				aptcUpdate.setHealthEnrollmentId(healthId);
				idList.add(Integer.valueOf(healthId));
			}

			if(StringUtils.isNotEmpty(aptcUpdate.getDentalEnrollmentId())){
				String dentalId = ghixJasyptEncrytorUtil.decryptStringByJasypt(aptcUpdate.getDentalEnrollmentId());
				aptcUpdate.setDentalEnrollmentId(dentalId);
				idList.add(Integer.valueOf(dentalId));
			}
			enrollReq.setIdList(idList);

			AccountUser user = userService.getLoggedInUser();
			if(user != null && StringUtils.isNotBlank(user.getEmail())){
				aptcUpdate.setUserName(user.getEmail());
			}

			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_MONTHLY_APTC_AMOUNT,
					user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollReq));

			if(response != null && response.getStatusCode() == HttpStatus.OK && StringUtils.isNotBlank(response.getBody())) {
				enrollResp = gson.fromJson(response.getBody(), EnrollmentResponse.class);
				if(enrollResp != null && enrollResp.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					//Populate APTC next month start date and amount
					populateAPTCDetailsFromEnrollment(aptcUpdate, enrollResp);
				}
			}

			if(null == enrollResp || enrollResp.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				String msg = "Unable to get Enrollment Plan Details. Error Details: ";
				if(enrollResp != null){
					msg += enrollResp.getErrCode() + ":" + enrollResp.getErrMsg();
		}
				throw new GIException(msg);
			}

		} catch(Exception e){
			LOGGER.error("Error occured while calling enrollment API  to fetch APTC effective date: ",e);
			throw new GIException(e);
		}
    }

    /**
     * Populate APTC next month start date and amount based on enrollment API response
     * @param aptcUpdate
     * @param enrollResp
     */
	private void populateAPTCDetailsFromEnrollment(AptcUpdate aptcUpdate, EnrollmentResponse enrollResp) {

		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		List<MonthlyAPTCAmountDTO> aptcAmountDTOs = enrollResp.getMonthlyAPTCAmountDTOList();
		if(aptcAmountDTOs != null && !aptcAmountDTOs.isEmpty()) {

			for(MonthlyAPTCAmountDTO  aptcAmountDTO : aptcAmountDTOs) {

				if((StringUtils.isNotBlank(aptcUpdate.getHealthEnrollmentId()) && Integer.valueOf(aptcUpdate.getHealthEnrollmentId()).equals(aptcAmountDTO.getEnrollment_id())) || 
						(StringUtils.isNotBlank(aptcUpdate.getDentalEnrollmentId()) && Integer.valueOf(aptcUpdate.getDentalEnrollmentId()).equals(aptcAmountDTO.getEnrollment_id()))) {
					Float nextMonthAPTCAmount = 0F;
					if (aptcAmountDTO.getNextMonthAPTCAmount() != null) {
						nextMonthAPTCAmount = aptcAmountDTO.getNextMonthAPTCAmount();
					}
					aptcUpdate.setAptcAmt(nextMonthAPTCAmount);
					aptcUpdate.setAptcPremiumEffectiveDate(formatter.format(aptcAmountDTO.getNextMonthStartDate()));
					aptcUpdate.setResponse(IndividualPortalConstants.SUCCESS);
				}
			}
		}

    }

	protected IndPortalValidationDTO validateOverrideSEPDetails(SEPOverrideDTO sepOverrideDTO) throws GIException {
		IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
		indPortalValidationDTO.setStatus(true);
		
		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		if(null == sepOverrideDTO) {
			//throw new GIException("SEPOverrideDTO cannot ne null");
			setValidateError(indPortalValidationDTO, "SEPOverrideDTO cannot ne null");
			return indPortalValidationDTO;
		}

		try {
			if(StringUtils.isBlank(sepOverrideDTO.getSepStartDate())
					|| StringUtils.isBlank(sepOverrideDTO.getSepEndDate())
					|| null == formatter.parse(sepOverrideDTO.getSepStartDate())
					|| null == formatter.parse(sepOverrideDTO.getSepEndDate())) {
				//throw new GIException("SEP Start  and SEP End date cannot ne null");
				setValidateError(indPortalValidationDTO, "SEP Start  and SEP End date cannot ne null");
			}
		} catch (ParseException e) {
			throw new GIException(e);
		}

		if(StringUtils.isBlank(sepOverrideDTO.getOverrideCommentText())) {
			//throw new GIException("SEP Override Comment cannot ne null");
			setValidateError(indPortalValidationDTO, "SEP Override Comment cannot ne null");
		}

		if(StringUtils.isBlank(sepOverrideDTO.getCaseNumber())) {
			//throw new GIException("SSAP case number is mandatory");
			setValidateError(indPortalValidationDTO, "SSAP case number is mandatory");
		}



		if(sepOverrideDTO.getApplicantEvents() != null
					&& !sepOverrideDTO.getApplicantEvents().isEmpty()) {
			for(ApplicantEventsDTO applicantEventsDTO : sepOverrideDTO.getApplicantEvents()) {
				try {
					if(null == applicantEventsDTO.getEventId()
							|| null == applicantEventsDTO.getApplicantEventId()
							|| StringUtils.isBlank(applicantEventsDTO.getEventDate())
							|| null == formatter.parse(applicantEventsDTO.getEventDate())
							|| StringUtils.isBlank(applicantEventsDTO.getFirstName())
							|| StringUtils.isBlank(applicantEventsDTO.getLastName())
							|| StringUtils.isBlank(applicantEventsDTO.getEventNameLabel())) {
						//throw new GIException("Applicant Events DTO details are mandatory");
						setValidateError(indPortalValidationDTO, "Applicant Events DTO details are mandatory");
					}
				} catch (ParseException e) {
					throw new GIException(e);
				}
			}
		}

		if(null == sepOverrideDTO.getCoverageYear()) {
			//throw new GIException("Application Coverage year is mandatory");
			setValidateError(indPortalValidationDTO, "Application Coverage year is mandatory");
		}

		if(StringUtils.isBlank(sepOverrideDTO.getApplicationType())
				|| !(SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(sepOverrideDTO.getApplicationType())
						|| SsapApplicationEventTypeEnum.SEP.name().equalsIgnoreCase(sepOverrideDTO.getApplicationType()))) {
			//throw new GIException("Application Type is mandatory with values: "	+ SsapApplicationEventTypeEnum.QEP.toString() + " or " + SsapApplicationEventTypeEnum.SEP.toString());
			setValidateError(indPortalValidationDTO, "Application Type is mandatory with values: "+ SsapApplicationEventTypeEnum.QEP.toString() + " or " + SsapApplicationEventTypeEnum.SEP.toString());
		}

		if(null == sepOverrideDTO.getIsFinancial()) {
			//throw new GIException("Application Financial flag is mandatory");
			setValidateError(indPortalValidationDTO, "Application Financial flag is mandatory");
		}

		if(StringUtils.isNotBlank(sepOverrideDTO.getCaseNumber())){
			validateActiveModuleId(sepOverrideDTO.getCaseNumber(), -1);
		}		

		return indPortalValidationDTO;
	}

	protected String saveOverrideSEPDetails(SEPOverrideDTO sepOverrideDTO) {

    	String response = IndividualPortalConstants.FAILURE;

    	try {
	    	AccountUser user = this.userService.getLoggedInUser();
	    	sepOverrideDTO.setUserId(user.getId());

			ResponseEntity<String> responseval =  ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_OVERRIDE_SEP_DETAILS,
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, sepOverrideDTO);

			if(responseval != null && responseval.getStatusCode().equals(HttpStatus.OK)
					&& StringUtils.equalsIgnoreCase(responseval.getBody(), GhixConstants.RESPONSE_SUCCESS)) {
				response = saveComments(getIdFromCaseNumber(sepOverrideDTO.getCaseNumber()).toString(),
						IndividualPortalConstants.SSAP_APPLICATION, sepOverrideDTO.getOverrideCommentText());
			}
		}catch(Exception ex){
			LOGGER.error("Error while updating Override SEP details:", ex.getMessage());
			persistToGiMonitor(ex);
		}
		return response;
	}

	protected IndPortalValidationDTO validateFetchSEPEvents(SEPOverrideSEPEventsDTO sepOverrideSEPEventsDTO) throws GIException {
		IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();
		indPortalValidationDTO.setStatus(true);
		if(null == sepOverrideSEPEventsDTO) {
			//throw new GIException("SEPOverrideSEPEventsDTO cannot ne null");
			setValidateError(indPortalValidationDTO, "SEPOverrideSEPEventsDTO cannot be null");
			return indPortalValidationDTO;
		}

		if(StringUtils.isBlank(sepOverrideSEPEventsDTO.getIsFinancial())
				|| !(SepEvents.Financial.Y.name().equalsIgnoreCase(sepOverrideSEPEventsDTO.getIsFinancial())
						|| SepEvents.Financial.N.name().equalsIgnoreCase(sepOverrideSEPEventsDTO.getIsFinancial()))) {
			//throw new GIException("Financial Flag is mandatory with values: " + SepEvents.Financial.Y.toString() + " or " + SepEvents.Financial.N.toString());
			setValidateError(indPortalValidationDTO, "Financial Flag is mandatory with values: " + SepEvents.Financial.Y.toString() + " or " + SepEvents.Financial.N.toString());
		}
		
		if(StringUtils.isBlank(sepOverrideSEPEventsDTO.getDisplayOnUI())
				|| !("Y".equalsIgnoreCase(sepOverrideSEPEventsDTO.getDisplayOnUI())
						|| "N".equalsIgnoreCase(sepOverrideSEPEventsDTO.getDisplayOnUI()))) {
			//throw new GIException("Financial Flag is mandatory with values: " + SepEvents.Financial.Y.toString() + " or " + SepEvents.Financial.N.toString());
			setValidateError(indPortalValidationDTO, "Display On UI Flag is mandatory with values: Y or N");
		}

		boolean isChangeTypeInvalid = false;
		if(StringUtils.isBlank(sepOverrideSEPEventsDTO.getChangeType())) {
			isChangeTypeInvalid = true;
		} else {
			isChangeTypeInvalid = true;
			for(SepEvents.ChangeType changeType: SepEvents.ChangeType.values()) {
				if(changeType.name().equalsIgnoreCase(sepOverrideSEPEventsDTO.getChangeType())) {
					isChangeTypeInvalid = false;
					break;
				}
			}
		}

		if(isChangeTypeInvalid) {
			//throw new GIException("Change Type is mandatory with values: " + SepEvents.ChangeType.values().toString());
			setValidateError(indPortalValidationDTO, "Change Type is mandatory with values: " + SepEvents.ChangeType.values().toString());
		}

		//validateActiveModuleId(sepOverrideDTO.getCaseNumber(), -1);
		return indPortalValidationDTO;
	}


	protected String fetchSEPEventsBasedOnApplicationType(SEPOverrideSEPEventsDTO sepOverrideSEPEventsDTO) {

		String sepEvents = null;

		try {
			sepEvents = resourceCreator.findByEventChangeTypeAndFinancial(
	    			sepOverrideSEPEventsDTO.getChangeType(), SepEvents.Financial.valueOf(sepOverrideSEPEventsDTO.getIsFinancial()),
	    			sepOverrideSEPEventsDTO.getDisplayOnUI());
		}catch(Exception ex){
			LOGGER.error("Error while fetching SEP Events:", ex.getMessage());
			persistToGiMonitor(ex);
		}

		return sepEvents;
	}

	/**
	 * Fetches Special Enrollment Period dates for given application
	 * @param caseNumber
	 * @return
	 */
	protected SEPOverrideSEPDatesDTO fetchSEPPeriodDatesForApplication(String caseNumber) throws GIException{

		LOGGER.debug("Getting SEP Period details");
		SEPOverrideSEPDatesDTO sepOverrideSEPDatesDTO = null;

		// Fetch SEP details using case number
		BigDecimal ssap = (BigDecimal) getIdFromCaseNumber(caseNumber);
		SsapApplicationEvent ssapEvent = getSsapApplicationEventById(ssap.longValue());
		if (ssapEvent == null) {
			LOGGER.error("SSAP application with case number [" + caseNumber + "] has no event");
		} else {

			// Set the dates
			sepOverrideSEPDatesDTO = new SEPOverrideSEPDatesDTO();
			sepOverrideSEPDatesDTO.setSepStartDate(dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,
					ssapEvent.getEnrollmentStartDate()));
			sepOverrideSEPDatesDTO.setSepEndDate(dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,
					ssapEvent.getEnrollmentEndDate()));
			LOGGER.debug("Successfully fetched SEP Period details");

		}

		return sepOverrideSEPDatesDTO;
	}

	/**
	 * Returns Preference Communication literal to be shown in CAP History
	 * @param prefCommunication
	 * @return
	 */
	protected String fetchPrefCommunicationLiteral(GhixNoticeCommunicationMethod prefCommunication) {

		String prefCommunicationLiteral = null;

        switch(prefCommunication){
        case EmailAndMail:
        	prefCommunicationLiteral = IndividualPortalConstants.PREF_COMMUNICATION_EMAIL_AND_MAIL;
        	break;
        case Mail:
        	prefCommunicationLiteral = IndividualPortalConstants.PREF_COMMUNICATION_MAIL;
        	break;
        default:
        	prefCommunicationLiteral = prefCommunication.getMethod();
        }

        return prefCommunicationLiteral;
	}

	/**
	 * Fetch existing Enrollment and APTC Effective date details for an application
	 * @param caseNumber
	 * @param year 
	 * @return
	 * @throws GIException
	 */
	protected IndPortalEligibilityDateDetails fetchEligibilityDetailsForAppln(String caseNumber, int year) throws GIException {

		LOGGER.debug("Fetching existing Enrollment and APTC Effective date details for application "+ caseNumber);

		IndPortalEligibilityDateDetails indPortalEligibilityDateDetails = null;
		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		try {
			AccountUser user = userService.getLoggedInUser();

			indPortalEligibilityDateDetails = new IndPortalEligibilityDateDetails();

			populateSSAPDetailsForESD(user, caseNumber, year, indPortalEligibilityDateDetails);

			Long enrolledAppId =  Long.valueOf(indPortalEligibilityDateDetails.getEnrolledAppId());
			Map<String, EnrollmentShopDTO> enrollments = getEnrollmentDetails(enrolledAppId);
			if(enrollments==null || enrollments.isEmpty()){
				throw new GIException("Unable to get Enrollment Plan Details for the application "+enrolledAppId);
			}
			
			boolean dentalOnly = false;
			EnrollmentShopDTO enrollmentShopDTO = null;
			if(enrollments.containsKey(IndividualPortalConstants.HEALTH_PLAN)){
				enrollmentShopDTO = enrollments.get(IndividualPortalConstants.HEALTH_PLAN);
			}else{
				enrollmentShopDTO = enrollments.get(IndividualPortalConstants.DENTAL_PLAN);
				dentalOnly = true;
			}

			EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO = fetchFinEffDateFroromELigibilty(user, enrollmentShopDTO, indPortalEligibilityDateDetails, dentalOnly);

			indPortalEligibilityDateDetails.setCmrHouseholdId(String.valueOf(user.getActiveModuleId()));
			indPortalEligibilityDateDetails.setEnrollmentStartDate(formatter.format(enrollmentShopDTO.getCoverageStartDate()));
			indPortalEligibilityDateDetails.setEnrollmentEndDate(formatter.format(enrollmentShopDTO.getCoverageEndDate()));
			indPortalEligibilityDateDetails.setFinancialEffectiveDate(formatter.format(eligibilityDatesOverrideDTO.getFinancialEffectiveDate()));
			indPortalEligibilityDateDetails.setStatus(eligibilityDatesOverrideDTO.getStatus());

		} catch(Exception e) {
			LOGGER.error("Error fetching existing Enrollment and APTC Effective date details for application "+ caseNumber, e);
			throw new GIException(e);
		}
		return indPortalEligibilityDateDetails;
	}

	private void populateSSAPDetailsForESD(AccountUser user, String caseNumber, int year, IndPortalEligibilityDateDetails indPortalEligibilityDateDetails) throws GIException {

		String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);

		int activeModuleId = user.getActiveModuleId();
		List<Object[]> ssapIdToAppStatusList = ssapApplicationRepository
												.findSsapByCmrAppStatusAndCoverageYr(new BigDecimal(activeModuleId), year, Arrays.asList("ER", "EN", "PN"));
		if(ssapIdToAppStatusList == null || ssapIdToAppStatusList.isEmpty() || ssapIdToAppStatusList.size() != 2) {
			throw new GIException("Required data not found for SSAP application with CMR Household Id: " + activeModuleId + " and Case number: " + caseNumber);
		}

		for(Object[] ssapIdToAppStatus : ssapIdToAppStatusList) {

			String appStatus = ssapIdToAppStatus[1].toString();
			switch(appStatus) {
				case "EN":
					indPortalEligibilityDateDetails.setEnrolledAppId(ssapIdToAppStatus[0].toString());
					break;
				case "PN":
					indPortalEligibilityDateDetails.setEnrolledAppId(ssapIdToAppStatus[0].toString());
					break;
				case "ER":
					indPortalEligibilityDateDetails.setSepAppId(ssapIdToAppStatus[0].toString());
					break;
				default:
					throw new GIException("Invalid SSAP application found with CMR Household Id: : " + activeModuleId
							+ ", Case number: " + caseNumber + " and Application Status: " + appStatus);
			}
		}

	}

	private EligibilityDatesOverrideDTO fetchFinEffDateFroromELigibilty(AccountUser user, EnrollmentShopDTO enrollmentShopDTO,
			IndPortalEligibilityDateDetails indPortalEligibilityDateDetails, boolean dentalOnly) throws GIException {

		EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO = null;

		try {

			eligibilityDatesOverrideDTO = new EligibilityDatesOverrideDTO();
			eligibilityDatesOverrideDTO.setEditMode(false);
			eligibilityDatesOverrideDTO.setCmrHouseholdId(Long.valueOf(user.getActiveModuleId()));
			eligibilityDatesOverrideDTO.setEnrollmentStartDate(enrollmentShopDTO.getCoverageStartDate());
			eligibilityDatesOverrideDTO.setEnrollmentEndDate(enrollmentShopDTO.getCoverageEndDate());
			eligibilityDatesOverrideDTO.setEnAppId(Long.valueOf(indPortalEligibilityDateDetails.getEnrolledAppId()));
			eligibilityDatesOverrideDTO.setErAppId(Long.valueOf(indPortalEligibilityDateDetails.getSepAppId()));
			eligibilityDatesOverrideDTO.setDentalOnly(dentalOnly);

			LOGGER.debug("Getting Financial Effective Date details from Eligibility for: " + eligibilityDatesOverrideDTO);

			ResponseEntity<EligibilityDatesOverrideDTO> response =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/dates/override",
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, EligibilityDatesOverrideDTO.class, eligibilityDatesOverrideDTO);

			if(response != null && response.getStatusCode().equals(HttpStatus.OK)){
				eligibilityDatesOverrideDTO = response.getBody();
			}

		} catch(Exception e) {
			LOGGER.error("Error fetching Financial Effective Date details from Eligibility", e);
			throw new GIException(e);
		}

		return eligibilityDatesOverrideDTO;
	}

	protected IndPortalValidationDTO validateESDInputParameters(IndPortalEligibilityDateDetails indPortalEligibilityDateDetails) throws GIException {
		IndPortalValidationDTO indPortalValidationDTO = new IndPortalValidationDTO();		
		indPortalValidationDTO.setStatus(true);
		
		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		if(!StringUtils.isNumeric(indPortalEligibilityDateDetails.getCmrHouseholdId())) {
			//throw new GIException("CMR Household Id cannot be null and must be numeric");
			setValidateError(indPortalValidationDTO, "CMR Household Id cannot be null and must be numeric");
		}

		if(!StringUtils.isNumeric(indPortalEligibilityDateDetails.getEnrolledAppId())) {
			//throw new GIException("Enrolled SSAP Id cannot be null and must be numeric");
			setValidateError(indPortalValidationDTO, "Enrolled SSAP Id cannot be null and must be numeric");
		}

		if(!StringUtils.isNumeric(indPortalEligibilityDateDetails.getSepAppId())) {
			//throw new GIException("Current SEP SSAP Id cannot be null and must be numeric");
			setValidateError(indPortalValidationDTO, "Current SEP SSAP Id cannot be null and must be numeric");
		}
		
		if(StringUtils.isBlank(indPortalEligibilityDateDetails.getComments())) {
			//throw new GIException("Comment can not be blank");
			setValidateError(indPortalValidationDTO, "Comment can not be blank");
		}

		try {

			Date enStartDate = null;
			Date enEndDate = null;

			if(StringUtils.isBlank(indPortalEligibilityDateDetails.getEnrollmentStartDate())
					|| null == (enStartDate = formatter.parse(indPortalEligibilityDateDetails.getEnrollmentStartDate()))) {
				//throw new GIException("Enrollment Start date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
				setValidateError(indPortalValidationDTO, "Enrollment Start date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
			}

			if(StringUtils.isBlank(indPortalEligibilityDateDetails.getEnrollmentEndDate())
					|| null == (enEndDate = formatter.parse(indPortalEligibilityDateDetails.getEnrollmentEndDate()))) {
				//throw new GIException("Enrollment End date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
				setValidateError(indPortalValidationDTO, "Enrollment End date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
			}

			if(StringUtils.isBlank(indPortalEligibilityDateDetails.getFinancialEffectiveDate())
					|| null == formatter.parse(indPortalEligibilityDateDetails.getFinancialEffectiveDate())) {
				//throw new GIException("Financial Effective date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
				setValidateError(indPortalValidationDTO, "Financial Effective date cannot be null and should be in format: " + IndividualPortalConstants.SHORT_DATE_FORMAT);
			}

			if(enStartDate != null && enEndDate != null){
				LocalDate enrolledStartDate = new LocalDate(enStartDate);
				LocalDate enrolledEndDate = new LocalDate(enEndDate);

				if(enrolledEndDate.isBefore(enrolledStartDate)) {
					//throw new GIException("Enrollment End date cannot be before Enrollment Start Date");
					setValidateError(indPortalValidationDTO, "Enrollment End date cannot be before Enrollment Start Date");
				}
			}			

		} catch (ParseException e) {
			throw new GIException(e);
		}
		
		return indPortalValidationDTO;
	}

	protected String saveESDDetailsForAppln(String caseNumber, IndPortalEligibilityDateDetails indPortalEligibilityDateDetails) throws GIException {

		EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO = null;

    	String response = IndividualPortalConstants.FAILURE;
		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		try {

			AccountUser user = userService.getLoggedInUser();

			eligibilityDatesOverrideDTO = new EligibilityDatesOverrideDTO();
			eligibilityDatesOverrideDTO.setEditMode(true);
			eligibilityDatesOverrideDTO.setStatus(indPortalEligibilityDateDetails.getStatus());
			eligibilityDatesOverrideDTO.setCmrHouseholdId(Long.valueOf(user.getActiveModuleId()));
			eligibilityDatesOverrideDTO.setEnAppId(Long.valueOf(indPortalEligibilityDateDetails.getEnrolledAppId()));
			eligibilityDatesOverrideDTO.setErAppId(Long.valueOf(indPortalEligibilityDateDetails.getSepAppId()));
			eligibilityDatesOverrideDTO.setEnrollmentStartDate(formatter.parse(indPortalEligibilityDateDetails.getEnrollmentStartDate()));
			eligibilityDatesOverrideDTO.setEnrollmentEndDate(formatter.parse(indPortalEligibilityDateDetails.getEnrollmentEndDate()));
			eligibilityDatesOverrideDTO.setFinancialEffectiveDate(formatter.parse(indPortalEligibilityDateDetails.getFinancialEffectiveDate()));

			LOGGER.debug("Calling ESD save API for: " + eligibilityDatesOverrideDTO);

			ResponseEntity<EligibilityDatesOverrideDTO> responseVal =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/dates/override",
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, EligibilityDatesOverrideDTO.class, eligibilityDatesOverrideDTO);

			if(responseVal != null && responseVal.getStatusCode().equals(HttpStatus.OK) && responseVal.getBody() != null){
				response = IndividualPortalConstants.SUCCESS;
				EligibilityDatesOverrideDTO eligibilityDatesOverrideRes = responseVal.getBody();
				Map<String, Object> params = null;
				SsapApplicationEvent sepEvent = getSsapApplicationEventById(eligibilityDatesOverrideDTO.getErAppId()
						.longValue());
				eligibilityDatesOverrideDTO.setStatus(eligibilityDatesOverrideRes.getStatus());
				eligibilityDatesOverrideDTO.setFinancialEffectiveDate(eligibilityDatesOverrideRes.getFinancialEffectiveDate());
				if (sepEvent != null) {
					params = new HashMap<>(2);
					if(sepEvent.getEnrollmentStartDate() != null)
						params.put(IndividualPortalConstants.ESD_SEP_START_DATE, sepEvent.getEnrollmentStartDate());
					if(sepEvent.getEnrollmentEndDate() != null)
						params.put(IndividualPortalConstants.ESD_SEP_END_DATE, sepEvent.getEnrollmentEndDate());
				}
				logAppEventForValidateESDDetails(IndividualPortalConstants.APPEVENT_ELIGIBILITY_INFORMATION,
						IndividualPortalConstants.APPEVENT_ESD_OVERRIDDEN, caseNumber, eligibilityDatesOverrideDTO, params,indPortalEligibilityDateDetails.getComments());
			}
		} catch(Exception e) {
			LOGGER.error("Error saving ESD details to Eligibility", e);
			throw new GIException(e);
		}

		return response;
	}


	public RenewalStatusNotToConsiderResponseDTO updateAppNotToConsider(Long applicationId,String type){	
		 RenewalStatusNotToConsiderResponseDTO response = null;
		 RenewalStatusNotToConsiderRequestDTO requestDTO = new RenewalStatusNotToConsiderRequestDTO();
		 requestDTO.setSsapApplicationId(applicationId);
		 requestDTO.setPlanType(type);
		try{
			response = 	ghixRestTemplate.putForObject(GhixEndPoints.ELIGIBILITY_URL+ "renewal/setNotToConsiderRenewalStatus", requestDTO, RenewalStatusNotToConsiderResponseDTO.class);					
		}catch(Exception e){
			LOGGER.error("Exception occured for calling getAppGroups api",e);
		}
		return response;		
	}
	
	
	public String getAppGroups(AppGroupRequest appGroupRequest){	
		String appGroupResponseJson = null;
		try{
			appGroupResponseJson = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+ "eligibility/api/getAppGroups/",appGroupRequest, String.class);			
		}catch(Exception e){
			LOGGER.error("Exception occured for calling getAppGroups api",e);
			AppGroupResponse appGroupResponse = new AppGroupResponse();
			appGroupResponse.setStatus(500);
			appGroupResponse.setMessage("Exception occured for calling getAppGroups api");
			return platformGson.toJson(appGroupResponse); 
		}
		return appGroupResponseJson;		
	}
	
	
	
	public String formInd19Request(int activeModuleId, String caseNumber, IndAdditionalDetails indAdditionalDetails, int year) throws Exception{
    	String response=IndividualPortalConstants.FAILURE;
    	    	
    	Date financialEffectiveDate = verifyESDDetails(caseNumber, null, year, indAdditionalDetails);
		if(StringUtils.isNotBlank(indAdditionalDetails.getErrCode())) {
			return IndividualPortalConstants.FAILURE;
		}
		
         Integer brokerId = null;
         Integer assisterId = null;
         
         DesignateBroker designateBroker = this.getDesignateService().findBrokerByIndividualId(activeModuleId);
         if(designateBroker!=null && DesignateBroker.Status.Active.equals(designateBroker.getStatus())){
             brokerId = designateBroker.getBrokerId();
         }
         
         if(brokerId==null || brokerId==0){
             AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
             assisterRequestDTO.setIndividualId(activeModuleId);
             AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

             if(assisterDesignateResponseDTO!=null && assisterDesignateResponseDTO.getAssister()!=null 
                     && DesignateAssister.Status.Active.equals(assisterDesignateResponseDTO.getDesignateAssister().getStatus())){
                 assisterId = assisterDesignateResponseDTO.getAssister().getId();
             }
         }
         /*
          * HIX-58645 Send Primary Contact Phone number in IND 19
          * This number will be used from SSAP.
          * If no number is present in SSAP, use the one from CMR_HOUSE_HOLD
          */
         Household household = this.getConsumerUtil().getHouseholdRecord(activeModuleId);
         Ind19ClientRequest ind19ClientRequest = new Ind19ClientRequest();
         ind19ClientRequest.setCaseNumber(caseNumber);
         ind19ClientRequest.setHouseHoldId((long)activeModuleId);
         ind19ClientRequest.setAssisterId(assisterId);
         ind19ClientRequest.setBrokerId(brokerId);
         ind19ClientRequest.setPhoneNumber(household.getPhoneNumber());
         ind19ClientRequest.setIsChangePlanFlow(indAdditionalDetails.isChangePlanFlow());
         ind19ClientRequest.setIsSEPPlanFlow(indAdditionalDetails.isSEPPlanFlow());
         ind19ClientRequest.setCoverageYear(year);
         ind19ClientRequest.setFinancialEffectiveDate(financialEffectiveDate);
         ind19ClientRequest.setGroup(indAdditionalDetails.getGroup());
         response = invokeInd19(ind19ClientRequest);
         
    	return response;
    }
	
	 public int getValidCoverageYear(String coverageYear) {
	    	int year = TSCalendar.getInstance().get(Calendar.YEAR);
	    	if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
	            year = Integer.parseInt(coverageYear);
	        }
			return year;
		}
	 
	 public String  getLast2Characters(String inputString){
		 if(StringUtils.isNotBlank(inputString) && inputString.length() == 16){
			 int length = inputString.length();
			 int startIndex = length-2;
			 return inputString.substring(startIndex);
		 }
		 return null;
	 }	
	 
	 private RelationEnum getRelationShip(String relationId) {
		 RelationEnum relationShipCode = null;			
		if (SELF_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.SELF;
		} else if (SPOUSE_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.SPOUSE;
		} else if (CHILD_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.CHILD;
		} else {
			relationShipCode = RelationEnum.DEPENDENT;
		}
		return relationShipCode;
	 }

	 public String getEligibleApplicantsBySsapId(AppGroupRequest appGroupRequest){	
			String appGroupResponseJson = null;
			try{
				appGroupResponseJson = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+ "eligibility/api/getAppGroups/",appGroupRequest, String.class);			
			}catch(Exception e){
				LOGGER.error("Exception occured for calling getAppGroups api",e);
				appGroupResponseJson = "Error occoured while forming groups";
			}
			return appGroupResponseJson;		
		}
	 
	 public Long getApplicationId(SsapApplicationResource ssapApplicationResource) {
		 Long ssapId = null;			
		 Object links = ssapApplicationResource.get_links();
		 JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
		 Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
		 String selfHref= (self.get("href")==null)?"": self.get("href");
		 if("".equals(selfHref)) {
			 return ssapId;
			 }
		 else {
			 URI myUri = URI.create(selfHref);
			 ssapId = Long.parseLong(myUri.getPath().replace(GhixEndPoints.SsapEndPoints.RELATTIVE_SSAP_APPLICATION_DATA_URL, ""));
		 }
		 return ssapId;
	 }
	 
	 public SsapApplicationResource getApplicationByCaseNumber(String caseNumber)  throws GIException{
		 try {
			 SsapApplicationResource application = ssapApplicationAccessor.getSsapApplications(caseNumber);
			 return application;
		 } catch (Exception ex) {
			 throw new GIException("No Application found for caseNumber : "+caseNumber);
		 }
	 }
	 
	 public String computeCoverageStartDate(SsapApplicationResource application) throws Exception{
		 Timestamp covgDate = null;
 
		 if(application != null) {
			 Long applicationId =  getApplicationId(application);
			 if(applicationId != null) {
				 Long priorApplicationId = null;
				 if("SEP".equalsIgnoreCase(application.getApplicationType())){
					 SsapApplication enSsap  = ssapApplicationRepository.findEnPnSsapApplicationForCoverageYear(application.getCmrHouseoldId(), application.getCoverageYear());
					 if(enSsap == null) {
						 enSsap = getErAppWithOnlyDentalEnrl(application.getCmrHouseoldId(), application.getCoverageYear());
					 }
					 
					 if(enSsap != null){
						 priorApplicationId = enSsap.getId();
					 }
					 
				 }
				 boolean isChangePlanFlow = checkChangePlanFlow(applicationId, priorApplicationId, application);
				 covgDate = coverageStartDateService.computeCoverageStartDateIncludingTermMembers(new BigDecimal(applicationId.toString()), isChangePlanFlow, false, priorApplicationId);
			 }
		 }
		 
		 if(covgDate == null){
			 throw new GIException("CoverageStartDate should not be null.");
		 }
		 String coverageStartDate = new SimpleDateFormat("MM/dd/yyyy").format(covgDate.getTime());	     
		 return coverageStartDate;
	 }
	 
	 public boolean computeChangeSEPPlanFlowFlag(Long coverageYear, String applicationStatus, String caseNumber, Long applicationId) throws Exception{
		 boolean changeSEPPlanFlow = false;
		 if("EN".equalsIgnoreCase(applicationStatus) || "PN".equalsIgnoreCase(applicationStatus)){
			 Map<String,PlanSummaryDetails> enrollments = getEnrollmentDetails(applicationId.toString(), caseNumber);
			 if (!individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear.intValue())) {
				 if(verifyChangePlanInSEPAction(caseNumber, applicationId, enrollments)){
					 return true;
				 }
			 }else{
				 if(verifyChangePlanAction(enrollments)){
                     return true;
                 }
			 }
		 }		 
		 return changeSEPPlanFlow;
	 }
	 
	 public boolean computeChangePlanFlowFlag(Long coverageYear, String applicationStatus, String caseNumber, boolean isChangeSEPPlanFlow) throws Exception{
		 boolean isChangePlanFlowFlag = false;
		 SsapApplicationResource ssapApplicationResource = ssapApplicationAccessor.getSsapApplications(caseNumber);
		 if(!"OP".equalsIgnoreCase(applicationStatus) && 
				 (ssapApplicationResource != null && StringUtils.equalsIgnoreCase(ssapApplicationResource.getNativeAmerican(),"Y"))){
			 if (!individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear.intValue()) && !isChangeSEPPlanFlow){
				return true;
			 }
		 }
		 return isChangePlanFlowFlag;
	 }
	 
	 public BigDecimal getEnrollmentAptc(EnrollmentDataDTO enrollmentDTO) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(enrollmentDTO.getEnrollmentBenefitEffectiveStartDate());
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			BigDecimal maxAPTC = null;
			List<EnrollmentPremiumDTO> monthlyPrmiums = enrollmentDTO.getEnrollmentPremiumDtoList();
			if(monthlyPrmiums != null && !monthlyPrmiums.isEmpty()) {
				for(EnrollmentPremiumDTO premiumDTO : monthlyPrmiums)
				{
					if(premiumDTO.getMonth().equals(month) && premiumDTO.getYear().equals(year) && premiumDTO.getMaxAptc() != null)	
					{
						maxAPTC = new BigDecimal(Float.toString(premiumDTO.getMaxAptc())).setScale(2, BigDecimal.ROUND_HALF_UP);
						break;
					}
				}
			}
			return maxAPTC;
		}
	 
	 public void createValidationMessage(Integer status, String msg, AppGroupResponse appGroupResponse){
			appGroupResponse.setStatus(status);
			appGroupResponse.setMessage(msg);
	 }
	 
	 public void setDisenrollDialogData(CustomGroupingResponse appGroupResponse) throws GIException{
		 List<DisenrollDialogData> list = new ArrayList<>();
		 for(Group group : appGroupResponse.getGroupList()){
			 if(group.getGroupEnrollmentId() != null){
				 DisenrollDialogData disenrollDialogData = new DisenrollDialogData();
				 disenrollDialogData.setGroupEnrollmentId(group.getEncryptedGroupEnrollmentId());
				 Date coverageStartDate = DateUtil.StringToDate(group.getCoverageDate(), "MM/dd/yyyy");			
				 disenrollDialogData.setIsPlanEffectuated(coverageStartDate != null && new TSDate().getTime() >= coverageStartDate.getTime() ? "Y":"N");
				 disenrollDialogData.setHiosIssuerId(group.getHiosIssuerId());
				 disenrollDialogData.setIssuerName(group.getIssuerName());
				 disenrollDialogData.setPlanName(group.getPlanName());
				 Date endOfCurrentMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.CURRENT_MONTH, null);
				 Date endOfNextMonth = null;
				 Date endOfMonthAfterNextMonth = null;
				 int currentMonth = new LocalDate(endOfCurrentMonth.getTime()).getMonthOfYear();
				 //If user terminates plan in November, show only two options
				 if(currentMonth == DateTimeConstants.NOVEMBER){
					 endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
				 }
				 //If user terminates plan in December, show only one option
				 else if(currentMonth !=	DateTimeConstants.DECEMBER){
					 endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
					 endOfMonthAfterNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.MONTH_AFTER_NEXT_MONTH, null);
				 }
				 disenrollDialogData.setEndOfCurrentMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfCurrentMonth));
				 disenrollDialogData.setEndOfNextMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfNextMonth));
				 disenrollDialogData.setEndOfMonthAfterNext(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfMonthAfterNextMonth));
				 list.add(disenrollDialogData);
			 }
		 }
		 appGroupResponse.setDisenrollDialogDataList(list);
	 }
	 
	 public List<EnrollmentDataDTO> getEnrollmentbyhoByHouseholdcaseids(int coverageYear) {
		 try {
			 AccountUser user = this.userService.getLoggedInUser();	
				String activeModuleId = String.valueOf(user.getActiveModuleId());
				
				List<String> houseHoldCaseIdList = new ArrayList<String>();
				houseHoldCaseIdList.add(activeModuleId);
				
				List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
				enrollmentStatusList.add(EnrollmentStatus.TERM);
				enrollmentStatusList.add(EnrollmentStatus.CANCEL);
				enrollmentStatusList.add(EnrollmentStatus.PENDING);
				enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
				
				EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
				enrollmentRequest.setCoverageYear(String.valueOf(coverageYear));
				enrollmentRequest.setHouseHoldCaseIdList(houseHoldCaseIdList);
				enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
				enrollmentRequest.setPremiumDataRequired(true);
				
				
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, 
						user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();

				if (null != response ){
					EnrollmentResponse enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);					
					if(enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						return enrollmentResponse.getEnrollmentDataDtoListMap().get(activeModuleId);													
					}					
				}
		 } catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment by calling GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS API : ", e);
		}
		return Collections.emptyList();
	 }
	 
	 @SuppressWarnings("unchecked")
	public Map<String,String> fetchLatestAPTCAmount(Map<String, String> inputData) throws Exception{	
		 String response = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.FETCH_LATEST_APTC_AMOUT,
				 this.userService.getLoggedInUser().getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(inputData)).getBody();
		 if(null != response){
			 return platformGson.fromJson(response, Map.class);
		 }
		 return Collections.emptyMap();		
	 }
	
	 public PdHouseholdDTO findHouseholdByShoppingId(String shoppingId, String userName) throws GIRuntimeException {	
		 try{
			 String response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_HOUSEHOLD_BY_SHOPPING_ID+"?shoppingId="+shoppingId, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null).getBody();
			 if (null != response ){
				 ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
				 PdResponse pdResponse = objectReader.readValue(response);				
				 if(pdResponse != null && pdResponse.getPdHouseholdDTO() != null){
					 return pdResponse.getPdHouseholdDTO();
				 }			
			 }
		 }catch (Exception e) {
			 LOGGER.error("Exception occured while fetching PdHouseholdDTO by calling FIND_HOUSEHOLD_BY_SHOPPING_ID API : ", e);
		 }		 
		 return null;
	 }
	
	 public PdHouseholdDTO findPdHouseholdByLeadIdAndYear(String leadId, String year, String userName) {	
		 try{
			 String response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_PLDHOUSEHOLD_BY_LEADID_AND_YEAR+"?leadId="+leadId + "&year="+ year, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null).getBody();
			 if (null != response ){
				 ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
				 PdResponse pdResponse = objectReader.readValue(response);				
				 if(pdResponse != null && pdResponse.getPdHouseholdDTO() != null){
					 return pdResponse.getPdHouseholdDTO();
				 }			
			 }
		 }catch (Exception e) {
			 LOGGER.error("Exception occured while fetching PdHouseholdDTO by calling FIND_PLDHOUSEHOLD_BY_LEADID_AND_YEAR API : ", e);
		 }
		 return null;
	 } 
	
	 public boolean isShowPopup(int householdId) {
		 List<String> applicationStatus = Arrays.asList(
		   ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
       ApplicationStatus.SIGNED.getApplicationStatusCode(),
       ApplicationStatus.SUBMITTED.getApplicationStatusCode()
     );

		 List<SsapApplication> applicationList  = getApplicationByStatus(householdId, applicationStatus);
		 if(applicationList != null && !applicationList.isEmpty()) { 
			 Timestamp serverDate = new TSTimestamp();
			 LOGGER.info("serverDate = "+serverDate.toString());
			 for(SsapApplication application : applicationList){
				 if(application != null){ 
					 Timestamp applicationDate = application.getLastUpdateTimestamp();
					 long differenceInSec = (serverDate.getTime() - applicationDate.getTime())/1000;
					 LOGGER.debug("applicationDate = "+applicationDate.toString());
					 LOGGER.info("differenceInSec = "+differenceInSec);
					 if(differenceInSec >= 0 && differenceInSec < 60){
						if("SEP".equalsIgnoreCase(application.getApplicationType()) || ("QEP".equalsIgnoreCase(application.getApplicationType()) 
								&& ApplicationValidationStatus.HMS_INTRANSIT.equals(application.getValidationStatus()))) {
							return true;
						}else if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equals(application.getApplicationStatus()) 
								&& ("SEP".equalsIgnoreCase(application.getApplicationType()) || "QEP".equalsIgnoreCase(application.getApplicationType()))) {
							return true;
						}
						else if(ApplicationStatus.SIGNED.getApplicationStatusCode().equalsIgnoreCase(application.getApplicationStatus())
              || ApplicationStatus.SUBMITTED.getApplicationStatusCode().equals(application.getApplicationStatus())) {
							return true;
						}
					 }
				 }
			 }			 
		 }
		 return false;
    } 
	
	 public boolean checkChangePlanFlow(Long applicationId, Long priorApplicationId, SsapApplicationResource application) throws Exception{
		 int covgYear = (int) application.getCoverageYear();
		 if(individualPortalUtil.isInsideOEEnrollmentWindow(covgYear)) {
			 if("OE".equalsIgnoreCase(application.getApplicationType())){
				 return true;
			 }else if("SEP".equalsIgnoreCase(application.getApplicationType()) && (applicationId.equals(priorApplicationId))){
				 return true;
			 }
		 }
		 
		 if(!individualPortalUtil.isInsideOEEnrollmentWindow(covgYear)) {	
			 boolean allowChangePlan = false;
			 if("EN".equalsIgnoreCase(application.getApplicationStatus()) || "PN".equalsIgnoreCase(application.getApplicationStatus())){
				 Map<String,PlanSummaryDetails> planDetailsMap = getEnrollmentDetails(applicationId.toString(), application.getCaseNumber());
				 allowChangePlan = verifyChangePlanAction(planDetailsMap);
			 }
			 boolean isNativeAmerican = StringUtils.equalsIgnoreCase(application.getNativeAmerican(),"Y");
			 if(isNativeAmerican && allowChangePlan){
				 return true;
			 } else if("SEP".equalsIgnoreCase(application.getApplicationType())){
				 if("EN".equalsIgnoreCase(application.getApplicationStatus())){				 
					 return true;
				 }else{
					 SsapApplicationEvent sepEvent = getSsapApplicationEventById(applicationId);
					 Date today = new TSDate();
					 if(sepEvent.getEnrollmentEndDate().before(today)) {
						 return true;
					 }
				 }
			 }	
		 }
		 return false;
	 }

	private boolean isPlanSelectionAllowed(final SsapApplicationEvent sepEvent) {
		LOGGER.info("Check plan selection availability for application : {}", sepEvent.getSsapApplication().getId());
		boolean allowPlanSelection = false;
		if (null != sepEvent && !CollectionUtils.isEmpty(sepEvent.getSsapApplicantEvents())) {
			List<SepEvents> sepEnrollmentEvents = sepEvent.getSsapApplicantEvents().stream().map(e -> e.getSepEvents())
					.collect(Collectors.toList());
			allowPlanSelection = sepEnrollmentEvents.stream().filter(e -> !SEP_OTHER_EVENT.equalsIgnoreCase(e.getName()))
					.anyMatch(e -> e.isPlanSelectionAllowed());
			LOGGER.info("Plan selection availability : {}", allowPlanSelection);
		}
		return allowPlanSelection;
	}
	
	private SsapApplication getErAppWithOnlyDentalEnrl(BigDecimal cmrHouseoldId, long coverageYear) {
		SsapApplication ssap = null;
		 List<SsapApplication> appliationList = ssapApplicationRepository.getErAppWithOnlyDentalEnrl(cmrHouseoldId, coverageYear);
		 if(appliationList != null && !appliationList.isEmpty()){
			 ssap = appliationList.get(0);
		 }
		return ssap;
	}
	
	/**
	 * Jira Id: HIX-119036 untilit to call eligibility API to update dental
	 * applicaiton status to null
	 * 
	 * @author Anoop
	 * @since 04th Oct 2019
	 * @param caseNumber
	 * @return boolean
	 */
	public boolean updateDentalEnrollmentStatus(String caseNumber, Date coverageStartDate, Date terminationDate, String applicationType, Date applicationCreationTime) {

		boolean updateFlag = true;

		try {	
			if (StringUtils.equalsIgnoreCase("OE", applicationType) || StringUtils.equalsIgnoreCase("SEP", applicationType)) {
				Boolean reEnroll = getReEnrollmentFlag(coverageStartDate, terminationDate, applicationCreationTime);
				if (reEnroll) {
					if (caseNumber != null) {
						AccountUser user = this.userService.getLoggedInUser();
						SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
						ssapApplicationRequest.setCaseNumber(caseNumber);
						ssapApplicationRequest.setUserId(new BigDecimal(user.getId()));
						ssapApplicationRequest.setApplicationDentalStatusUpdate(true);
						ResponseEntity<Void> response = ghixRestTemplate.exchange(
								GhixEndPoints.ELIGIBILITY_URL + "application/update", user.getUsername(), HttpMethod.POST,
								MediaType.APPLICATION_JSON, Void.class, ssapApplicationRequest);
						if (response != null && response.getStatusCode().equals(HttpStatus.OK)) {
							updateFlag = true;
						} else {
							updateFlag = false;
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating ssapApplicationDental status to null : ", e);
			updateFlag =false;
		}
		return updateFlag;
	}
	
}
