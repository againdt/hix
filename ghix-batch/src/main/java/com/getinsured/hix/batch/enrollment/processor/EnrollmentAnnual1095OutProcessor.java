package com.getinsured.hix.batch.enrollment.processor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnual1095BatchService;

@Component("enrollmentAnnual1095OutProcessor")
public class EnrollmentAnnual1095OutProcessor  implements ItemProcessor< Integer, Integer>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnual1095OutProcessor.class);
	
	private EnrollmentAnnual1095BatchService enrollmentAnnual1095BatchService;
	/*private IEnrollment1095Repository iEnrollment1095Repository;
	private Enrollment1095Out enrollment1095Out;
	*/
	/*@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			applicableYear=ec.getInt("year",0);
		}
		
	}*/

	@Override
	public Integer process(Integer enrollment1095Id) throws Exception {
		// TODO Auto-generated method stub
		if(enrollment1095Id != null ){
			try{
			enrollmentAnnual1095BatchService.process1095Enrollment(enrollment1095Id);
			
			}catch(Exception e){
				
				LOGGER.error("EnrollmentAnnual1095OutProcessor ERROR: " + e.getMessage(), e);
			}
			
		}
		return enrollment1095Id;
	}

	public EnrollmentAnnual1095BatchService getEnrollmentAnnual1095BatchService() {
		return enrollmentAnnual1095BatchService;
	}

	public void setEnrollmentAnnual1095BatchService(
			EnrollmentAnnual1095BatchService enrollmentAnnual1095BatchService) {
		this.enrollmentAnnual1095BatchService = enrollmentAnnual1095BatchService;
	}

	/*public IEnrollment1095Repository getiEnrollment1095Repository() {
		return iEnrollment1095Repository;
	}

	public void setiEnrollment1095Repository(
			IEnrollment1095Repository iEnrollment1095Repository) {
		this.iEnrollment1095Repository = iEnrollment1095Repository;
	}

	public Enrollment1095Out getEnrollment1095Out() {
		return enrollment1095Out;
	}

	public void setEnrollment1095Out(Enrollment1095Out enrollment1095Out) {
		this.enrollment1095Out = enrollment1095Out;
	}
	*/
	
	
	
}
