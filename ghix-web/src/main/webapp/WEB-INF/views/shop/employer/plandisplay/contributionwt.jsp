<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.9.2.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />

<style>
.line {
border-right: 1px solid #e3e3e3 !important;
}

#contribution_error label{margin: -12px 0 30px 142px;}

.display{display: block !important;}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#missinginfo').modal('show');
	getEmployerQuotingPlans();
});

function getEmployerQuotingPlans() {
	var subUrl = '<c:url value="/shop/employer/plandisplay/getEmployerQuotingPlans">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		success : function(response,xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			 if(response.toString().indexOf('Success') == -1){
				 $('#missinginfo').modal('hide');
				 $("#frmcontributionwt :input").attr("disabled", true);
				 window.location.href = 'contribution';
			 }else{
				 $('#missinginfo').modal('show');
			 }
		},error : function(e) {
			alert("Failed to get plans for employer");
		}
	});
}
</script>

<div class="gutter10">
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<div class="accordion graysidebar" id="accordion2">
			<util:nav pageName="${page_name}"></util:nav>
		</div>
		<c:if test="${errorMsg == '' }">
			<util:selection pageName="${page_name}"></util:selection>
		</c:if>
	</div>
	<!--sidebar-->
	<div class="span9" id="rightpanel">
	<h4 class="graydrkbg gutter10" id="skip">
			<a href="#" class="btn"> <spring:message code = "label.back"/></a>
            <span class="offset1"><spring:message code = "label.proposeContribution"/></span>
	</h4>		
		<!--titlebar-->
		<form id="frmcontributionwt" name="frmcontributionwt" action="frmcontributionwt" method="post">
			<df:csrfToken/>
			<div class="gutter10">
				<!-- <p>To help you narrow down to the right choices for your
					business, start by entering the percentage you think you'd like to
					contribute towards the premiums for your employees. Don't worry, you 
					can change these percentages later as
					you narrow down your choices further.</p>-->
					<p><spring:message code = "label.slideBarInfo"/></p>
					
				<div class="well">
			        <div style="margin:15px 0;">
			              <p class="pull-left paddingT5" style="width:105px;"><strong><spring:message code = "label.employees"/></strong> (${totalEmp})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="employeeSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		            </div>
		            <div id="contribution_error"></div>
		            <div style="margin:30px 0; clear:both;">
			              <p class="pull-left paddingT5" style="width:105px;"><strong><spring:message code = "label.dependent"/> </strong> (${totalDependents})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="dependentSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		            </div>
					<table class="table table-border-none table-condensed costs">
						<tr>
							<td colspan="2" class="line"><strong><spring:message code = "label.emplCosts"/></strong> 
							<a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.monthlyCosts"/>" class="info"><i
									class="icon-question-sign"></i></a></td>
							<td>&nbsp;</td>
							<td><strong><spring:message code = "label.potentialTaxCredit"/></strong> <a href="javascript:void(0)"
								rel="tooltip" data-placement="top"
								data-original-title="<spring:message code = "label.taxAdvice"/>" class="info"><i class="icon-question-sign"></i></a></td>
						</tr>
						<tr>
							<td style="width: 27%;"><spring:message code = "label.costFor"/> ${totalEmp} <spring:message code = "label.employees"/></td>
							<td id="employee_range"  class="line">$0-$0 / <spring:message code = "label.month"/></td>
							<td>&nbsp;</td>
							<td id="taxCreditRange">$0-$0 / <spring:message code = "label.month"/></td>
						</tr>
						<tr>
							<td><spring:message code = "label.costFor"/> ${totalDependents} <spring:message code = "label.dependent"/></td>
							<td id="dependent_range" class="line">$0-$0 / <spring:message code = "label.month"/></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><b><spring:message code = "label.total"/></b></td>
							<td id="total"><b>$0-$0 / <spring:message code = "label.month"/></b></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
		</form>
		<!--gutter10-->
	</div>
	<!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<div id="missinginfo" class="modal hide fade" tabindex="-1"  data-backdrop="static" modal="true" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
	<div class="modal-header"></div>
	       <div class="modal-body">
	       	<h4 class="green">Please wait while we quote your group. This may take a few minutes.</h4>
	       	<p class="txt-center"><img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots"/></p>
	      <div class="modal-footer"></div>
	</div>
</div>
<!--main-->
<script type="text/javascript">
$('.info').tooltip();
$(document).ready(function(){
	empMinContribution = 50;
	var employeeTooltip = function(event, ui) {
		if(ui.value == undefined){ 
			ui.value = 50;
		}
        if(ui.value < empMinContribution){
        	ui.value = 50;
        	//$('#contribution_error').html("<label class='error' generated='true'><span><em class='excl'>!</em>here Employer contribution should be 50% or more than that.</span></label>");
            return false;
        }
		$('#employee').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em; width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='employee'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#employee').remove();
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#employeeContribution").val(ui.value);
	};
	
	var dependentTooltip = function(event, ui) {
		
		if(ui.value == undefined){ 
			ui.value = 0;
		}
		$('#dependent').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em; width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='dependent'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#dependent').remove();
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#dependentContribution").val(ui.value);
	};
	
	$( "#employeeSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: 50 ,
	    create: employeeTooltip,
		slide:  employeeTooltip,
	});
	$( "#dependentSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: 0 ,
	    create: dependentTooltip,
		slide:  dependentTooltip,
	});
	
});
</script>