package com.getinsured.hix.plandisplay.prescriptionsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.plandisplay.DrugDosageDTO;
import com.getinsured.hix.dto.plandisplay.DrugSearchDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.plandisplay.util.PrescriptionUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;

@Controller
public class PrescriptionSearchController {
	
	@Autowired private PrescriptionUtil prescriptionUtil;
	@Autowired private Gson platformGson;
	@Autowired private PlanDisplayRestClient planDisplayRestClient;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrescriptionSearchController.class);
	
	@RequestMapping(value = "/private/searchDrugByName", method = RequestMethod.GET)
	@ResponseBody
	public String searchByDrugName(@RequestParam(value="term", required=false) String drugName, Model model, HttpServletRequest request){
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----searchByDrugName started----", null, false);
		
		List<DrugSearchDTO> response = null;
		if(StringUtils.isNotEmpty(drugName)){
			response = prescriptionUtil.drugSearch(drugName);
		}
		
		Map<String, List<DrugSearchDTO>> drugMap = new HashMap<String, List<DrugSearchDTO>>();
		if(response != null && !response.isEmpty()) {
			drugMap.put("drugs", response);
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "searchByDrugName ended in " + (endTime - startTime) + " ms", null, false);
		
		return platformGson.toJson(drugMap);
	}
	
	@RequestMapping(value = "/private/searchByDrugId", method = RequestMethod.GET)
	@ResponseBody
	public String searchByDrugId(@RequestParam(value="drugId", required=false) String drugId, Model model, HttpServletRequest request){
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----searchByDrugId started----", null, false);
		
		List<DrugDosageDTO> response = new ArrayList<DrugDosageDTO>();
		if(StringUtils.isNotEmpty(drugId)){
			response = prescriptionUtil.drugSearchById(drugId);
		}
		
		Map<String, List<DrugDosageDTO>> drugDosageMap = new HashMap<String, List<DrugDosageDTO>>();
		if(response != null && !response.isEmpty()) {
			drugDosageMap.put("drugDosages", response);
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "searchByDrugId ended in " + (endTime - startTime) + " ms", null, false);
		
		return platformGson.toJson(drugDosageMap);
	}
	
	
	@RequestMapping(value = "/private/getFairPriceOfDrug", method = RequestMethod.GET)
	@ResponseBody
	public String getFairPriceResponseOfDrug(@RequestParam(value="ndc", required=false) String ndc, Model model, HttpServletRequest request){
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getFairPriceResponseOfDrug started----", null, false);
		
		String response = null;
		if(StringUtils.isNotEmpty(ndc)){
			response = prescriptionUtil.getFairPriceResponse(ndc);
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "getFairPriceResponseOfDrug ended in " + (endTime - startTime) + " ms", null, false);
		
		return response;
	}
	
	@RequestMapping(value = "/private/genericRxcui", method = RequestMethod.GET)
	@ResponseBody
	public String getGenericRxcui(Model model, HttpServletRequest request){
		String drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
		if("GI".equalsIgnoreCase(drugSearchAPIConfig)){
			return platformGson.toJson("");
		}
		long startTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----genericRxcui started----", null, false);
		String name = (String) request.getParameter("name");
		String strength = (String) request.getParameter("strength");
		String response = null;
		if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(strength)){
			response = prescriptionUtil.getGenericRxCuiResponse(name, strength);
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "genericRxcui ended in " + (endTime - startTime) + " ms", null, false);
		
		return response;
	}
	
	@RequestMapping(value="/private/getRxcuiByNdc/{ndc}", method = RequestMethod.GET)
	public @ResponseBody String getRxcuiByNdc(@PathVariable("ndc") String ndc, HttpServletResponse response)
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "----getRxcuiByNdc started----", null, false);
		return planDisplayRestClient.getRxcuiByNdc(ndc);
	}
}
