const sortByObjectValues = Symbol.for('sort-by-object-values');

Array.prototype[sortByObjectValues] = function() {
    return [...this].sort(function(a,b) {
        const [ name ] = Object.values(a);
        const [ _name ] = Object.values(b);
        return name > _name ? 1 : name < _name ? -1 : 0;
    });
};

const analyzeNumber = Symbol.for('analyze-number');

String.prototype[analyzeNumber] = function() {

    // Calculating qty of '.' and '-'

    let counter1 = 0;
    let counter2 = 0;

    [...this].forEach(char => {
        char === '.' && counter1++;
        char === '-' && counter2++;
    });

    // If there is a float part except providing more than 2 decimals

    if(counter1 === 1) {
        if (this.split('.')[1].length > 2) {
            return false;
        }
    }

    // Preventing providing minus sign on positions except the first one

    if(counter2 === 1) {
        if(this.indexOf('-') !== 0) {
            return false;
        }
    }

    return counter1 < 2 && counter2 <= 1;
};

const cloneFunction = Symbol.for('clone-function');

Function.prototype[cloneFunction] = function() {
    const func = this;
    const clone = function() {
        return func.apply(this, arguments);
    };
    clone.prototype = func.prototype;
    for (let prop in func) {
        if (func.hasOwnProperty(prop) && prop !== 'prototype') {
            clone[prop] = func[prop];
        }
    }
    return clone;
};