var jobList=new vega("jobListApp");
//jobList.showPageLoading = true;
jobList._rawData={
    type: "singlePage",
    pageData: [
        {
            type: "form",
            name: "jobListAppForm",
            css: "form-horizontal",
            schema: {},
            fields: [
					{
                     	type: "grid", name: "gridJob", sortable: true,  dataSource: {
                         pageSize: 10,
							 serverPaging: false,
							 serverSorting: false,
							 serverFiltering: false,
                         transport: {
                             read: {
                                 url: gridService,
								 type: "GET"

                             }
                         },
				
					     schema: {
                             model: {
                                 fields: {
									 ServerTime: {type: "string"},
									 JobName: { editable: false,type: "string" },
									 CurrentSchedule: { editable: false,type: "string" },
									 JobDescription: { editable: false,type: "string" },
									 NewSchedule: { type: "string" }
                                }
                             },
                             data: "GridJobData",
                             total: "TotalCount"						
                         },

                     },

						 pageable: true, //enable paging
						 filterable: {mode:"row"},
						
                        sortable: {
                            mode: "single",
                            allowUnsort: true
                        },
                     columns: [
						 { field: "ServerTime", hidden: true, title: "Server Time", headerAttributes: { "class": "gridHeader" } },
                         { field: "JobName", filterable: {cell:{showOperators:false}}, title: "Job Name", headerAttributes: { "class": "gridHeader" } },
						 { field: "CurrentSchedule", filterable: false,title: "Current Schedule", headerAttributes: { "class": "gridHeader" } },
					     { field: "JobDescription", filterable: false, title: "Schedule Description", headerAttributes: { "class": "gridHeader" } },
						 { field: "NewSchedule", filterable: false,title: "Change Schedule", 
						 template:"<input data-bind='value: NewSchedule' >",
						 
					 headerAttributes: { "class": "gridHeader" } },
						 { command: [ {
										name: "Update", 
										click: function(e){   
											var tr = $(e.target).closest("tr"); 
											var data = this.dataItem(tr); 
											newSchedule = data.get("NewSchedule"); 
											jobName=data.get("JobName");

											if(newSchedule != null && newSchedule.trim().length > 0){
												var csrfToken = $("#csrftoken").val();
												var action = '/hix/admin/scheduleJob';	
												$.post(action,{"jobName":jobName,"cronExp":newSchedule , "csrftoken":csrfToken} , function(response) {
												  $("#refreshGridButton").trigger('click');
												 }, "json").fail(function() {
												 })
											}else{
													 alert("Invalid schedule.");
												 }
										}                              
									}
								],
							editable:true
						}					

                     ],
						 dataBound: function() {
						var rows = this.tbody.children();
						var dataItems = this.dataSource.view();
						for (var i = 0; i < dataItems.length; i++)  {
						  kendo.bind(rows[i], dataItems[i]);
						}
						$("#serverTime").val($("span[ng-bind= 'dataItem.ServerTime']")[0].innerText);
					  }
                 } ,
				{
					type:"button",
					caption:"Refresh Grid",
					name:"refreshGridButton",
					id:"refreshGridButton",
					click:"refreshGrid()",
					css:"margin10-t"
				 }
			]
        }

    ]
};
jobList.jobListAppController = function ($scope, $compile, $http) {
	
    //add additional functionalities
	$scope.refreshGrid = function(){
		$scope.gridJobOptions.dataSource.read();
	};
	

	this.refreshNewGrid = $scope.refreshGrid;
};
jobList.ngController("jobListAppController", ["$scope", "$compile", "$http"], jobList.jobListAppController);