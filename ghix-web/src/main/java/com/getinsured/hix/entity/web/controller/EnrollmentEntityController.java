package com.getinsured.hix.entity.web.controller;

import java.io.IOException;
import java.text.Collator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.helper.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.hix.broker.enums.AEEBookOfBusinessEnums.RECORD_TYPE;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.ApplicationStatus;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.finance.PaymentMethodDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.entity.service.EntityService;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.individual.service.ExternalIndividualService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.PopulationData;
import com.getinsured.hix.model.entity.PopulationEthnicity;
import com.getinsured.hix.model.entity.PopulationIndustry;
import com.getinsured.hix.model.entity.PopulationLanguage;
import com.getinsured.hix.model.entity.PopulationServedWrapper;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.thoughtworks.xstream.XStream;

/**
 * Handles requests for the enrollment entity pages.
 */
@SuppressWarnings("unused")
@Controller
public class EnrollmentEntityController {

	private static final String CLOSED = "Closed";

	private static final String PAYMENT_METHOD = "Payment method";

	private static final String PAYMENT_METHODS = "paymentMethods";

	private static final String POPULATION_INDUSTRY = "populationIndustry";

	private static final String POPULATION_LANGUAGE = "populationLanguage";

	private static final String POPULATION_ETHNICITY = "populationEthnicity";

	private static final String ENTITY_ID = "entityId";

	//private static final String STATUS = "status";
	
	private static final String UPLOAD_DOCUMENT = "uploadedDocument";
	
	private static final String HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY = "hasPermission(#model, 'ENTITY_VIEW_ENTITY')";

	private static final String HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY = "hasPermission(#model, 'ENTITY_EDIT_ENTITY')";

	private static final String DISPLAY_ADD_ENTITY_INFORMATION_END = "displayAddEntityInformation: END";

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityController.class);
	
	@Autowired private Gson platformGson;
	@Autowired 
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private LookupService lookupService;

	@Autowired
	private EERestCallInvoker restClassCommunicator;

	@Autowired
	private UserService userService;
	
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	
	@Autowired
	private EntityUtils entityUtils;

	@Autowired
	private SmartValidator validator;
	
	@Autowired
	private MessageSource messageSource;  
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private EntityService entityService;

	@Autowired 
	private BrokerIndTriggerService brokerIndTriggerService;
	
	@Autowired
	private ExternalIndividualService externalIndividualService;
	
	private static final String PAGE_TITLE = "page_title";
	private static final String LOGGED_USER = "loggedUser";
	private static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	private static final String ENTITY_SITE_HOURS_TIME = "ENTITY_SITE_HOURS_TIME";
	private static final String HOURSOFOPERATIONFROMTIME = "HOURSOFOPERATION_FROMTIME";
	private static final String HOURSOFOPERATIONTOTIME = "HOURSOFOPERATION_TOTIME";
	private static final String CA_STATECODE = "CA";
	private static final String DAYSOFOPERATION = "DAYSOFOPERATION";
	private static final String DAYSLIST = "daysList";
	private static final String DAYSLISTNAMES = "daysListNames";
	private static final String FROMTIMELIST = "fromTimelist";
	private static final String TOTIMELIST = "toTimelist";
	private static final String STATELIST = "statelist";
	private static final String SITE = "site";
	private static final String SITELANGUAGES = "siteLanguages";
	private static final String SITELOCATIONHOURSMAP = "siteLocationHoursMap";
	private static final String ENROLLMENT_ENTITY = "enrollmentEntity";
	private static final String CANCEL = "cancel";
	private static final String UPDATEPROFILE = "updatedProfile";
	private static final String PAYMENT_METHODS_OBJ = "paymentMethodsObj";
	private static final String REGISTRATION_STATUS = "registrationStatus";
	private static final String STATUS_HISTORY = "entityRegStatusHistory";
	private static final String LIST_OF_STATES = "statelist";
	private static final String LANGUAGES_LOOKUP_LIST = "languagesLookupList";
	private static final String ETHNICITIES_LOOKUP_LIST = "ethnicitiesLookupList";
	private static final String INDUSTRIES_LOOKUP_LIST = "industriesLookupList";
	private static final String POPULATION_SERVED_WRAPPER = "populationServedWrapper";
	private static final String LANGUAGE_LOOKUP_TYPE = "ENTITY_LANGUAGE";
	private static final String ETHNICITY_LOOKUP_TYPE = "ENTITY_ETHNICITY";
	public static final String INDUSTRY_LOOKUP_TYPE = "ENTITY_INDUSTRY";
	public static final String PRIMARYSITE = "PRIMARY";
	public static final String SUBSITE = "SUBSITE";
	public static final String ISPRIMARYSITE = "isPrimarySite";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SITELIST = "siteList";
	public static final String SITENUMBER = "siteNumber";
	public static final String SUBSITENUMBER = "siteNumber";
	public static final String ISEDIT = "isEdit";
	public static final String LISTOFLANGUAGES = "listOflanguages";
	public static final String LISTOFLANGUAGENAMES = "listOflanguageNames";
	public static final String QUOTE = "&quot;";
	private static final String IS_EDIT = "isEdit";
	private static final String OTHER_SPOKEN_LANGUAGE = "otherSpokenLanguage";
	private static final String OTHER_WRITTEN_LANGUAGE = "otherWrittenLanguage";
	private static final String RETRIEVING_EE_DETAIL_REST_CALL = "Retrieving Enrollment Entity Detail REST Call Ends";
	private static final String RETRIEVING_EE_DETAIL_REST_CALL_STARTS = "Retrieving Enrollment Entity Detail REST Call Starts";
	private static final String ENTITY_CONTACT = "ENTITY_CONTACT";
	private static final String SITE_ID = "siteId";
	private static final String COUNTIES_SERVED_LIST = "countiesServedlist";
	private static final String LOCATIONHOURSMAPWITHLIST = "LOCATIONHOURSMAPWITHLIST";
	private static final String SITELANGUAGEMAP = "SITELANGUAGEMAP";
	private static final String ADDNEWSUBSITE = "addNewSubSite";
	private static final String LOCATIONMATCHING = "locationMatching";
	private static final String ENROLLMENTENTITY = "enrollmentEntity";
	private static final String ENROLLMENT_ENTITY_USER = "enrollmentEntity";
	private static final String ENTITY_LANGUAGE_SPOKEN = "ENTITY_LANGUAGE_SPOKEN";
	private static final String ENTITY_LANGUAGE_WRITTEN = "ENTITY_LANGUAGE_WRITTEN";
	public static final String LISTOFLANGUAGESWRITTEN = "listOfLanguagesWritten";
	private static final String IS_SHOW = "isShow";
	private List<String> languageList = null;
	private static final String NEXTREGISTRATIONSTEP = "nextRegistrationStep";
	private static final String NO = "no";
	private static final int ZERO = 0;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final int TWOZEROONE = 201;
	private static final int HUNDRED = 100;
	private static final long UPLOAD_DOCUMENT_ALLOWED_SIZE = 5242880;
	private static final String INDIVIDUALLIST = "individualList";
	private static final String RESULTSIZE = "resultSize";
	private static final String CURRENTPAGE = "currentPage";
	private static final String LANGLIST = "langList";
	private static final String ETHNICITYLIST = "ethnicityList";
	private static final String INDUSTRYLIST = "industryList";
	private static final String PRIMARYPHONE1 = "primaryPhone1";
	private static final String PRIMARYPHONE2 = "primaryPhone2";
	private static final String PRIMARYPHONE3 = "primaryPhone3";
	private static final String SECONDARYPHONE1 = "secondaryPhone1";
	private static final String SECONDARYPHONE2 = "secondaryPhone2";
	private static final String SECONDARYPHONE3 = "secondaryPhone3";
	private static final String FAXNUMBER1 = "faxNumber1";
	private static final String FAXNUMBER2 = "faxNumber2";
	private static final String FAXNUMBER3 = "faxNumber3";
	private static final String PRICONTACTPRIMARYPHONENUMBER1 = "priContactPrimaryPhoneNumber1";
	private static final String PRICONTACTPRIMARYPHONENUMBER2 = "priContactPrimaryPhoneNumber2";
	private static final String PRICONTACTPRIMARYPHONENUMBER3 = "priContactPrimaryPhoneNumber3";
	private static final String PRICONTACTSECONDARYPHONENUMBER1 = "priContactSecondaryPhoneNumber1";
	private static final String PRICONTACTSECONDARYPHONENUMBER2 = "priContactSecondaryPhoneNumber2";
	private static final String PRICONTACTSECONDARYPHONENUMBER3 = "priContactSecondaryPhoneNumber3";
	private static final String FIN_PRIMARY_PHONE_NUMBER1 = "finPrimaryPhoneNumber1";
	private static final String FIN_FAX_NUMBER3 = "finFaxNumber3";

	private static final String FIN_FAX_NUMBER2 = "finFaxNumber2";

	private static final String FIN_FAX_NUMBER1 = "finFaxNumber1";

	private static final String FIN_PRIMARY_PHONE_NUMBER3 = "finPrimaryPhoneNumber3";

	private static final String FIN_PRIMARY_PHONE_NUMBER2 = "finPrimaryPhoneNumber2";
	private static final String APP_JSON_CONTENT = "application/json";
	private static final String LANGUAGES_LIST = "languagesList";
	private static final String LANGUAGES_LISTS = "languagesLists";
	private static final String DESIG_STATUS = "desigStatus";
	private static final String PAYMENT_INFO = "PAYMENT_INFO";
	private static final String IS_ADMIN = "isAdmin";
	private static final Integer PAGE_SIZE = 10;
	private static final String INCOMPLETE = "Incomplete";
	private static final String PENDING = "Pending";
	private static final String LIST_OF_PRIMARY_SITES = "primarySitelist";	
	private static final String CEC_FIRST_OR_LAST_NAME = "cecFirstOrLastName";
	private static final String CEC_EMAIL_ADDRESS = "cecEmailAddress";
	private static final String PRIMARY_SITE = "primarySite";
	private static final String INDLIST = "indIDList";
	private static final String SELECTEDCEC = "selectCEC";
	private static final String IND_PAGETITLE="Getinsured Health Exchange: Individuals";
	private static final String Y = "Y";
	private static final String CECLIST = "CECList";
	private static final String SHOWCECTABLE = "showCECtable";
	private static final String PAGESIZE = "pageSize";
	private static final String REQURI = "reqURI";
	private static final String REASSIGNINDIVIDUALS = "reassignIndividuals";
	private static final String PAGENUMBER = "pageNumber";
	
	private static final String FAIL_RECORD_COUNT = "failRecordCount";
 	private static final String SUCCESS_RECORD_COUNT = "successRecordCount";
 	private static final String RECORDS_FOR_MIGRATION = "recordsForMigration";
 	private static final String MIGRATION_STATUS = "migrationStatus";
 	private static final String FAILURE_RESPONSE_LIST = "failureResponseList";
 	private static final String MIGRATION_FAILURE_MESSAGE = "migrationFailureMessage";
 	private static final String MIGRACTION_SUCCESS = "Migraction Success";
 	private static final String ACTIVE = "Active";
 	private static final String ENTITYRECORDID = "entityRecordId";
 	
 	
	
	/**
	 * Handles the Enrollment Entity Registration
	 * 
	 * @param request
	 * @throws Entity information page 
	 * @return URL for navigation to Entity Information page - step 1
	 */
	@RequestMapping(value = "/enrollmententity/registration", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String registrationConfirm(HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("registrationConfirm: START");

		try {
			userService.enableRoleMenu(request, GhixRole.ENROLLMENTENTITY.getRoleName());

			LOGGER.info("registrationConfirm: END");

			return "redirect:/entity/enrollmententity/entityinformation";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while entity registration : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while entity registration in registrationConfirm method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the Enrollment Entity Registration - Step 1
	 * 
	 * @param model
	 * @param request
	 * @param isLeftNav
	 * @param editEntity
	 * @return URL for navigation to Entity Information page - step 1
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/entityinformation", method = RequestMethod.GET)
	@GiAudit(transactionName = "EntityRegistration", eventType = EventTypeEnum.ENTITY_ADMIN, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String displayAddEntityInformation(Model model, HttpServletRequest request,
			@RequestParam(value = "isLeftNav", required = false) String isLeftNav,
			@RequestParam(value = "editEntity", required = false) String editEntity) throws InvalidUserException {

		LOGGER.info("displayAddEntityInformation: START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");
		AccountUser user = null;
	
		String urlForNextStep = null;
		String lastCompletedStep = null;
		boolean isEditEntity = isEntityEditiable(editEntity);
		request.getSession().setAttribute(LOGGED_USER, ENROLLMENT_ENTITY_USER);
		if(EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE)) {
			request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE1, EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE));
		}else {
			request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE, EntityUtils.checkStateCode(EntityConstants.ID_STATE_CODE));
		}
		
		EnrollmentEntity enrollEntity = null;
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		
		try {
			user = userService.getLoggedInUser();
			request.getSession().setAttribute("userActiveRoleName",userService.getDefaultRole(user).getName());
			
			enrollmentEntityRequestDTO.setUserId(user.getId());

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				LOGGER.debug("Setting Record Id in session" + user.getRecordId());
				request.getSession().setAttribute(ENTITYRECORDID, user.getRecordId());
				LOGGER.debug("Setting Record Id in Request DTO");
				enrollmentEntityRequestDTO.setRecordId(user.getRecordId());
			}
			
			String enrollEntityResponseJSON = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			// Retrieve and add EE in model attribute
			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponseJSON);
			String registrationStatus = enrollEntity.getRegistrationStatus();
			
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter("isEditEntity", isEditEntity), new GiAuditParameter("registrationStatus", registrationStatus));
		
			// To show menu individual / employer menu items only if enrollment entity status is Active
			if (isStatusActive(registrationStatus, isEditEntity)) {

				LOGGER.info(DISPLAY_ADD_ENTITY_INFORMATION_END);
				request.getSession().setAttribute("showCECLeftnavigation", TRUE);
				return "redirect:/entity/entityadmin/displayassisters";
			}
						
			// To show hide menu items and display only Entity left navigation if Status is not Active
			if (isStatusNotActive(registrationStatus)) {

				LOGGER.info(DISPLAY_ADD_ENTITY_INFORMATION_END);

				return "redirect:/entity/enrollmententity/registrationstatus";
			}
			// check if entity record is partial or not,if partial,then create a
			// new map
			// added to avoid null pointer exception in case of partial record
			if (enrollEntity.getId() != 0 && enrollEntity.getEntityName() != null) {
				getContactNumber(model, enrollEntity);
				//map = getLeftNavigationMenuMap(enrollEntity);
				// retrieve last completed registration step
				lastCompletedStep = getlastCompletedStep(enrollEntity,map);
				if (lastCompletedStep != null) {
					// retrieve URL for next step from the last
					// completed step
					urlForNextStep = getNextStepURL(lastCompletedStep);
				}
			} 
			request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
					EntityUtils.getNextRegistrationStep(lastCompletedStep));
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATION_STATUS, enrollEntity.getRegistrationStatus());

			model.addAttribute("organizationtypelist", getOrganizationTypes());
			model.addAttribute(COUNTIES_SERVED_LIST, entityUtils.loadCountiesServedData()); 
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));

			// isLeftNavVal identifies whether entity has clicked on Entity
			// Information on left navigation
			String isLeftNavVal = isLeftNav != null ? isLeftNav : FALSE;
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return displayAddEntityInformationSub(urlForNextStep,isEditEntity,isLeftNavVal);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching Entity Information : ", giexception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching Entity Information "));
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Entity Information indisplayAddEntityInformation method : ", exception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error fetching Entity Information "));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	private boolean isStatusNotActive(String registrationStatus) {
		return !EntityUtils.isEmpty(registrationStatus) && !ACTIVE.equals(registrationStatus) && !INCOMPLETE.equals(registrationStatus);
	}

	private boolean isStatusActive(String registrationStatus, boolean isEditEntity) {
		return !EntityUtils.isEmpty(registrationStatus) && (ACTIVE.equals(registrationStatus)) && !isEditEntity;
	}
	
	private String displayAddEntityInformationSub(String urlForNextStep,boolean isEditEntity,String isLeftNavVal)
	{
		if (urlForNextStep == null || isEditEntity || TRUE.equals(isLeftNavVal)) {
			LOGGER.info(DISPLAY_ADD_ENTITY_INFORMATION_END);
			return "entity/enrollmententity/entityinformation";
		} else {
			LOGGER.info(DISPLAY_ADD_ENTITY_INFORMATION_END);
			return urlForNextStep;
		}
	}
	
	private boolean isEntityEditiable(String editEntity) {
		boolean isEditEntity = false;
		if(!EntityUtils.isEmpty(editEntity) && editEntity.equalsIgnoreCase(TRUE)){
			isEditEntity = true;
		}
		return isEditEntity;
	}
	
	private boolean isStatusPending(String registrationStatus) {
		return !EntityUtils.isEmpty(registrationStatus) && PENDING.equals(registrationStatus);
	}

	private boolean isStatusIncompleteOrPending(String registrationStatus, boolean isEditEntity) {
		return !EntityUtils.isEmpty(registrationStatus) && !(INCOMPLETE.equals(registrationStatus))
				&& !(PENDING.equals(registrationStatus)) && !isEditEntity;
	}

	@RequestMapping(value = "/entity/enrollmententity/entityinformation", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String entityInformationSubmit(@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntityData,
			BindingResult bindingResult, Model model, HttpServletRequest request,
			@RequestParam(value = "userId", required = false) Integer userId,
			@RequestParam(value = "entityId", required = false) Integer entityId) throws GIException, InvalidUserException {

		String educationGrantReq = request.getParameter("educationGrant");
		if(enrollmentEntityData == null){
			throw new GIException("Entity Information is Empty");
		}
		//validate Form request
		validateEntityInformation(enrollmentEntityData, bindingResult,educationGrantReq);
		
		try {
			
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			AccountUser user = userService.getLoggedInUser();
			boolean isEntityAdmin = isEntityAdmin();
			EnrollmentEntity enrollEntity = null;
			if (isEntityAdmin) {
				AccountUser accountUser = userService.findById(userId);
				enrollmentEntityData.setUser(accountUser);
				enrollEntity = getEnrollmentEntityByModuleId(entityId);
			} else {
				enrollmentEntityData.setUser(user);
				enrollEntity = getEnrollmentEntityByUserId(request);
			}

			//unescape business legal name 
			if(enrollmentEntityData.getBusinessLegalName() != null) {
				enrollmentEntityData.setBusinessLegalName(HtmlUtils.htmlUnescape(enrollmentEntityData.getBusinessLegalName()));
			}
			//unescape entity name
			if(enrollmentEntityData.getEntityName() != null) {
				enrollmentEntityData.setEntityName(HtmlUtils.htmlUnescape(enrollmentEntityData.getEntityName()));
			}

			if (enrollEntity != null) {
				if (EntityUtils.isEmpty(enrollEntity.getRegistrationStatus())) {
					enrollEntity.setRegistrationStatus(INCOMPLETE);
					enrollmentEntityData.setRegistrationStatus(INCOMPLETE);
				}
				enrollEntity.setEntityType(enrollmentEntityData.getEntityType());
				enrollEntity.setEntityName(enrollmentEntityData.getEntityName());
				enrollEntity.setBusinessLegalName(enrollmentEntityData.getBusinessLegalName());
				enrollEntity.setPrimaryEmailAddress(enrollmentEntityData.getPrimaryEmailAddress());
				enrollEntity.setPrimaryPhoneNumber(enrollmentEntityData.getPrimaryPhoneNumber());
				enrollEntity.setSecondaryPhoneNumber(enrollmentEntityData.getSecondaryPhoneNumber());
				enrollEntity.setFaxNumber(enrollmentEntityData.getFaxNumber());
				enrollEntity.setCommunicationPreference(enrollmentEntityData.getCommunicationPreference());
				enrollEntity.setFederalTaxID(enrollmentEntityData.getFederalTaxID());
				enrollEntity.setStateTaxID(enrollmentEntityData.getStateTaxID());
				enrollEntity.setOrgType(enrollmentEntityData.getOrgType());
				enrollEntity.setUpdatedBy(user.getId());
				setCountiesServed(enrollmentEntityData, enrollmentEntityData.getCountiesServed(), enrollEntity);
				setEducationalGrant(enrollmentEntityData, educationGrantReq, enrollEntity);
				enrollmentEntityRequestDTO.setEnrollmentEntity(enrollEntity);
			} else {
				enrollmentEntityData.setCreatedBy(user.getId());
				enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityData);
			}
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);
			// Save enrollment Entity
			String strEnrollmentEntityResponse = restClassCommunicator.saveEnrollmentEntityInfo(requestJSON);

			// Retrieve and add EE in model attribute
			enrollEntity = getAndSetEnrollmentEntity(model, strEnrollmentEntityResponse);

			// 5. Set left navigation menu
			if (!isEntityAdmin) {
				
				//create entry in module user table if not present
				ModuleUser userModule = userService.findModuleUser(enrollEntity.getId(), ModuleUserService.ENROLLMENTENTITY_MODULE, user);
				if (userModule == null)
				{
					try {
						userService.createModuleUser(enrollEntity.getId(), ModuleUserService.ENROLLMENTENTITY_MODULE, user, true);
					} catch (GIException e) {
						LOGGER.error("Exception occured while creating module user: ", e);
						throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
					}
				}
				setLeftNavigationMenu(request, enrollEntity);
				if(!enrollEntity.getRegistrationStatus().equalsIgnoreCase(INCOMPLETE)){
					return "entity/enrollmententity/viewentityinformation";
				}
			}else {
				return "redirect:/entity/entityadmin/viewentityinformation/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(userId));
			}

			return "redirect:/entity/enrollmententity/getPopulationServed";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving entity information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving entity information inentityInformationSubmit method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	private void validateEntityInformation(EnrollmentEntity enrollmentEntityData, BindingResult bindingResult,String educationGrantReq) {
		//validate the form request
		List<Object> validationGroups = new ArrayList<Object>();
		//if communication preferences is Fax then validate Fax Number 		
		if(!EntityUtils.isEmpty(enrollmentEntityData.getCommunicationPreference()) && "Fax".equalsIgnoreCase(enrollmentEntityData.getCommunicationPreference())){
			validationGroups.add(EnrollmentEntity.EntityInformationFaxNumber.class);
		}
		//if education grant is yes add grant group for validation
		if(!EntityUtils.isEmpty(educationGrantReq) && "yes".equalsIgnoreCase(educationGrantReq)){
			validationGroups.add(EnrollmentEntity.EntityInformationGrant.class);
		}
		//entity information group will be validation for entity information
		validationGroups.add(EnrollmentEntity.EntityInformation.class);
		validator.validate(enrollmentEntityData,bindingResult,validationGroups.toArray());

		if(bindingResult.hasErrors()){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION); 	
	     }
	}
	/**
	 * Handles the Enrollment Entity Registration - Step 1 view page
	 * 
	 * @param model
	 *            ,
	 * @param request
	 * @return URL for navigation to View Entity Information page - step 1
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewentityinformation", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String viewEntityInformation(Model model, HttpServletRequest request) {

		LOGGER.info("viewEntityInformation: START");

		LOGGER.info("View Entity Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");
		AccountUser user = null;
		Map<String, Boolean> map = null;
		
		try {
			user = userService.getLoggedInUser();
			
			EnrollmentEntity enrollEntity = null;
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setUserId(user.getId());
			String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);

			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATION_STATUS, enrollEntity.getRegistrationStatus());
			model.addAttribute("organizationtypelist", getOrganizationTypes());
			model.addAttribute(COUNTIES_SERVED_LIST, getCountiesServed());
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			LOGGER.info("viewEntityInformation: END");

			return "entity/enrollmententity/viewentityinformation";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing entity information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing entity information inviewEntityInformation method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private EnrollmentEntity getAndSetEnrollmentEntity(Model model, String enrollEntityResponse) throws JsonProcessingException, IOException {

		LOGGER.info("getAndSetEnrollmentEntity: START");

		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
		enrollEntity = enrollmentEntityResponseDTO.getEnrollmentEntity();

		if (enrollEntity == null) {
			LOGGER.debug("Enrollment Entity is null. Creating new entity object");

			enrollEntity = new EnrollmentEntity();
		}

		LOGGER.debug("Adding Enrollment Entity into model");

		model.addAttribute(ENROLLMENT_ENTITY, enrollEntity);

		LOGGER.info("getAndSetEnrollmentEntity: END");

		return enrollEntity;
	}

	private Set<String> getOrganizationTypes() {

		LOGGER.info("getOrganizationTypes: START");

		Set<String> organizationTypes = null;
		organizationTypes = new HashSet<String>();
		organizationTypes.add("School");
		organizationTypes.add("Faith-based organization");
		organizationTypes.add("Community Clinic");
		organizationTypes.add("Community-Based organization");
		organizationTypes.add("Public Partnership");
		organizationTypes.add("Private Partnership");

		LOGGER.info("getOrganizationTypes: END");

		return organizationTypes;
	}

	private void getContactNumber(Model model, EnrollmentEntity enrollEntity) {

		LOGGER.info("getContactNumber: START");

		if (enrollEntity.getPrimaryPhoneNumber() != null) {
			model.addAttribute(PRIMARYPHONE1, enrollEntity.getPrimaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(PRIMARYPHONE2, enrollEntity.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(PRIMARYPHONE3, enrollEntity.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getSecondaryPhoneNumber() != null && (!"".equals(enrollEntity.getSecondaryPhoneNumber()))) {
			model.addAttribute(SECONDARYPHONE1, enrollEntity.getSecondaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(SECONDARYPHONE2, enrollEntity.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(SECONDARYPHONE3, enrollEntity.getSecondaryPhoneNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getFaxNumber() != null && (!"".equals(enrollEntity.getFaxNumber()))) {
			model.addAttribute(FAXNUMBER1, enrollEntity.getFaxNumber().substring(ZERO, THREE));
			model.addAttribute(FAXNUMBER2, enrollEntity.getFaxNumber().substring(THREE, SIX));
			model.addAttribute(FAXNUMBER3, enrollEntity.getFaxNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getPriContactPrimaryPhoneNumber() != null && (!"".equals(enrollEntity.getPriContactPrimaryPhoneNumber()))) {
			model.addAttribute(PRICONTACTPRIMARYPHONENUMBER1,
					enrollEntity.getPriContactPrimaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(PRICONTACTPRIMARYPHONENUMBER2,
					enrollEntity.getPriContactPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(PRICONTACTPRIMARYPHONENUMBER3,
					enrollEntity.getPriContactPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if ((enrollEntity.getPriContactSecondaryPhoneNumber() != null && (!"".equals(enrollEntity.getPriContactSecondaryPhoneNumber())))
				&& (!"".equals(enrollEntity.getPriContactSecondaryPhoneNumber()))) {
			model.addAttribute(PRICONTACTSECONDARYPHONENUMBER1, enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(ZERO, THREE));
			model.addAttribute(PRICONTACTSECONDARYPHONENUMBER2, enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(THREE, SIX));
			model.addAttribute(PRICONTACTSECONDARYPHONENUMBER3, enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(SIX, TEN));
		}
		if (enrollEntity.getFinPrimaryPhoneNumber() != null && (!"".equals(enrollEntity.getFinPrimaryPhoneNumber()))) {
			model.addAttribute(FIN_PRIMARY_PHONE_NUMBER1, enrollEntity.getFinPrimaryPhoneNumber()
					.substring(ZERO, THREE));
			model.addAttribute(FIN_PRIMARY_PHONE_NUMBER2, enrollEntity.getFinPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(FIN_PRIMARY_PHONE_NUMBER3, enrollEntity.getFinPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if ((enrollEntity.getFinFaxNumber() != null) && (!"".equals(enrollEntity.getFinFaxNumber()))) {
			model.addAttribute(FIN_FAX_NUMBER1, enrollEntity.getFinFaxNumber().substring(ZERO, THREE));
			model.addAttribute(FIN_FAX_NUMBER2, enrollEntity.getFinFaxNumber().substring(THREE, SIX));
			model.addAttribute(FIN_FAX_NUMBER3, enrollEntity.getFinFaxNumber().substring(SIX, TEN));
		}

		LOGGER.info("getContactNumber: END");

	}

	/**
	 * Handles the primarysite registration
	 * 
	 * @param model
	 * @param entityId
	 * @return URL for navigation to primarysite page
	 */
	@RequestMapping(value = "/entity/enrollmententity/primarysite", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String primarySiteRegistration(Model model, @RequestParam(value=ENTITY_ID,required=false)String entityId) {

		try {
			return processPrimarySitedata(model,entityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching Primary Site Information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Primary Site Information for entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	/**
	 * Handles the primary site registration for admin
	 * 
	 * @param model
	 * @param entityId
	 * @return URL for navigation to primarysite page
	 */
	@RequestMapping(value = "/entity/enrollmententity/primarysite/{entityId}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getPrimarySiteRegistrationForAdmin(Model model, @PathVariable(ENTITY_ID)String entityId) {
		
		try {
			String decryptEntityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityId);
			return processPrimarySitedata(model,decryptEntityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching Primary-Site Information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Primary Site Information of entity: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private String processPrimarySitedata(Model model, String entityId) throws Exception {
		LOGGER.info("primarySiteRegistration: START");

		LOGGER.info("Primary Site Registration ");
		model.addAttribute(PAGE_TITLE, "Enrollment Entity: Site Registration");
		setModelAttributesForSite(model);
		//EnrollmentEntity entity = null;
		int userId = 0;
		AccountUser user = null;
		String enrollEntityResponse = null;
		// check if logged in user is EnrollmentEntity and not EntityAdmin
		try {
			if (!isEntityAdmin()) {

				user = userService.getLoggedInUser();
				if (user != null) {
					userId = user.getId();
				}
			}
			else{
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					LOGGER.info(" Inside Primary Site Registration ");
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
				}
			}
			getPrimarySite(model, userId);
			
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			model.addAttribute(LANGUAGES_LISTS, languageDetails);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch (Exception exception) {
			LOGGER.error("Exception occured in primarySiteRegistration", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting Primary Site Registration");

		LOGGER.info("primarySiteRegistration: END");

		return "entity/enrollmententity/primarysite";
	}

	/**
	 * Handles the site view
	 * 
	 * @param model
	 * @param request
	 * @return URL for navigation to site view page
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewsite", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String viewSiteData(Model model, HttpServletRequest request) {

		LOGGER.info("viewSiteData: START");

		LOGGER.info("Entering viewSiteData method ");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		EnrollmentEntity enrollEntity = null;
		AccountUser user = null;
		Map<String, Boolean> map = null;

		model.addAttribute(PAGE_TITLE, "Enrollment Entity: Sites View");
		
		try {
			
			setModelAttributesForSite(model);
			getAllSites(model);
			
			enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			user = userService.getLoggedInUser();
			if (user != null) {
				enrollmentEntityRequestDTO.setUserId(user.getId());
				String enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);
				enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);
			}
			
			if (enrollEntity != null) {
				map = getLeftNavigationMenuMap(enrollEntity);
				request.getSession().setAttribute(REGISTRATION_STATUS, enrollEntity.getRegistrationStatus());
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			LOGGER.info("Exiting viewSiteData method");

			LOGGER.info("viewSiteData: END");

			return "entity/enrollmententity/viewsite";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching Site Information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Site Information of entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the add subsite functionality on viewsite page
	 * 
	 * @param model
	 * @return URL for navigation to addsubsite page
	 */
	@RequestMapping(value = "/entity/enrollmententity/addsubsite", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String addSubSite(Model model) {

		LOGGER.info("addSubSite: START");

		LOGGER.info("Entering addSubSite method ");
		
		try {
			setModelAttributesForSite(model);
			getAllSites(model);
			// Flag to identify on view page whether an add operation or edit for
			// subsite.
			model.addAttribute(ADDNEWSUBSITE, TRUE);
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			model.addAttribute(LANGUAGES_LISTS, languageDetails);
			LOGGER.info("addSubSite: END");

			return "entity/enrollmententity/viewsite";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while adding Sub-site : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while adding Sub-site for entity: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the primary site save
	 * 
	 * @param  sitelanguages
	 * @param  siteResult
	 * @param  site,
	 * @param  result,
	 * @param  model,
	 * @param request
	 * @param siteLocationHoursString
	 * @param siteLocationHoursHidden,
	 * @param siteIdval,
	 * @param siteLanguageId
	 * @param physicalAddressCheckFlag
	 * @param entityId
	 * @return URL for navigation to site page
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/primarysite", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String submitprimarySite(@ModelAttribute(SITE) @Validated Site site,BindingResult siteResult,
			@ModelAttribute(SITELANGUAGES)@Validated SiteLanguages sitelanguages,BindingResult result,
			Model model, HttpServletRequest request,
			@RequestParam(value = "siteLocationHoursHidden", required = false) String siteLocationHoursString,
			@RequestParam(value = SITE_ID, required = false) String siteIdval,
			@RequestParam(value = "siteLanguageId", required = false) String siteLanguageId,
	        @RequestParam(value = "physicalAddressCheckFlag", required = false) String physicalAddressCheckFlag,
	        @RequestParam(value=ENTITY_ID,required=false)String entityId) throws InvalidUserException{
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			validatePrimarySite(result,siteResult);
				// check if logged in user is EnrollmentEntity and not EntityAdmin
			AccountUser user = userService.getLoggedInUser();
			if (!isEntityAdmin()) {
				enrollmentEntityRequestDTO.setUserId(user.getId());
			} else {
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO));
				
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {				
					enrollmentEntityRequestDTO.setUserId(enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId());
				}			
				
			}
	
			populateEnrollmentEntityRequestDTO(site, sitelanguages, siteLocationHoursString, siteIdval, siteLanguageId,
					physicalAddressCheckFlag, enrollmentEntityRequestDTO);
			
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = saveSiteDetails(enrollmentEntityRequestDTO);
			if(enrollmentEntityResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE && "PrimarySiteExist".equals(enrollmentEntityResponseDTO.getResponseDescription())){
				//redirect user to primary site page and display error message
				model.addAttribute("pme","true");
				return redirecttoPriSite(entityId);				
			}
		
			return redirectToViewSite(request,enrollmentEntityResponseDTO);
			
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving primary site information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving primary site information insubmitprimarySite method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	private void validatePrimarySite(BindingResult bindingResult,BindingResult siteResult){
		if(bindingResult.hasErrors() || siteResult.hasErrors()){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	
	private String redirecttoPriSite(String entityId) throws InvalidUserException
	{
		if(!isEntityAdmin()){
			return "redirect:/entity/enrollmententity/primarysite";
		}
		else{
			return "redirect:/entity/enrollmententity/primarysite/"+ghixJasyptEncrytorUtil.encryptStringByJasypt(entityId);
		}
	}
	
	
	private String redirectToViewSite(HttpServletRequest request,EnrollmentEntityResponseDTO enrollmentEntityResponseDTO) throws InvalidUserException
	{
		if (!isEntityAdmin()) {
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
			navigationMenuMap.put("LOCATION_HOURS", true);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
			request.getSession().setAttribute(REGISTRATION_STATUS,
					enrollmentEntityResponseDTO.getEnrollmentEntity().getRegistrationStatus());
			request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
					EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.PRIMARY.toString()));
			String status = enrollmentEntityResponseDTO.getEnrollmentEntity().getRegistrationStatus();
			if(!status.equalsIgnoreCase(INCOMPLETE) && !status.equalsIgnoreCase(PENDING)){
				return "redirect:/entity/enrollmententity/viewsite";
			}
		}	
		else {

			LOGGER.info("submitprimarySite: END");

			return "redirect:/entity/entityadmin/viewsite/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId()));
		}
		
		return "redirect:/entity/enrollmententity/subsite"; 
	}
	
	

	/**
	 * @param site
	 * @param sitelanguages
	 * @param siteLocationHoursString
	 * @param siteIdval
	 * @param siteLanguageId
	 * @param physicalAddressCheckFlag
	 * @param enrollmentEntityRequestDTO
	 * @param user
	 * @throws GIException
	 * @throws InvalidUserException 
	 */
	private void populateEnrollmentEntityRequestDTO(Site site,
			SiteLanguages sitelanguages, String siteLocationHoursString,
			String siteIdval, String siteLanguageId,
			String physicalAddressCheckFlag,
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) throws GIException, InvalidUserException {
		List<LookupValue> listOfdays;
		
		try {
			if (!EntityUtils.isEmpty(siteIdval)) {
				site.setId(Integer.parseInt(siteIdval));
				//update site
				site.setUpdatedBy(Long.valueOf(userService.getLoggedInUser().getId()));
			}
			else{
				//new site record
				site.setCreatedBy( Long.valueOf(userService.getLoggedInUser().getId()));
			}
			
			site.setSiteType(PRIMARYSITE);
			if("1".equals(physicalAddressCheckFlag) && EntityUtils.isEmpty(site.getPhysicalLocation().getState()) ){
					site.getPhysicalLocation().setState(site.getMailingLocation().getState());
			}	
			enrollmentEntityRequestDTO.setSite(site);
	
			sitelanguages.setSite(site);
			if (!EntityUtils.isEmpty(siteLanguageId)) {
				sitelanguages.setId(Integer.parseInt(siteLanguageId));
			}
			enrollmentEntityRequestDTO.setSiteLanguages(sitelanguages);
			listOfdays = lookupService.getLookupValueList(DAYSOFOPERATION);
			enrollmentEntityRequestDTO.setSiteLocationHoursList(getSiteLocationHoursList(site, siteLocationHoursString));
		}
		catch(NumberFormatException exNF){
			LOGGER.error("Exception occurrend while populating EnrollmentEntityRequestDTO. Exception:", exNF);
			throw new GIRuntimeException(exNF, ExceptionUtils.getFullStackTrace(exNF), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Method to save Site details of an ENmrollment Entity.
	 * 
	 * @param enrollmentEntityRequestDTO The current EnrollmentEntityRequestDTO instance.
	 * @return enrollmentEntityResponseDTO The current EnrollmentEntityResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private EnrollmentEntityResponseDTO saveSiteDetails(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) throws JsonProcessingException, IOException {
		LOGGER.info("saveSiteDetails: START");
		String strResponse = restClassCommunicator.saveSiteDetails(enrollmentEntityRequestDTO);
		LOGGER.info("Message after the restCall regarding Success/failure in db transaction");
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(strResponse);
		LOGGER.info("saveSiteDetails: END");
		return enrollmentEntityResponseDTO;
	}

	/**
	 * Handles the Sub Site Registration
	 * 
	 * @param model
	 * @param String encryptedsiteIdval
	 * @param String encryptedId
	 * @return URL for navigation to subsite page
	 */
	@RequestMapping(value = "/entity/enrollmententity/subsite", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String subSiteRegistration(Model model, @RequestParam(value = SITE_ID, required = false) String encryptedsiteIdval,
			@RequestParam(value=ENTITY_ID,required=false)String encryptedId) {
		String entityId = "";
		String siteIdval = "";
		
		try {
			if(null != encryptedId && !"".equals(encryptedId)){
				entityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			}
			if(null != encryptedsiteIdval && !"".equals(encryptedsiteIdval)){
				siteIdval = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedsiteIdval);
			}
			
			return processSubSiteData(model, siteIdval, entityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching sub-site information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching sub-site info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	/**
	 * Handles the Sub Site Registration
	 * 
	 * @param model
	 * @param String encryptedSiteId
	 * @param String entityId
	 * @return URL for navigation to subsite page
	 */
	@RequestMapping(value = "/entity/enrollmententity/subsite/{entityId}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String subSiteRegistrationForAdmin(Model model, 
			@RequestParam(value = SITE_ID, required = false) String encryptedSiteId, @PathVariable(ENTITY_ID)String entityId) {
		String decryptEntityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityId);
		
		try {
			String decryptSiteId = EntityUtils.isEmpty(encryptedSiteId)?null:ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedSiteId);
			return processSubSiteData(model, decryptSiteId, decryptEntityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching sub site information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching sub site info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private String processSubSiteData(Model model, String siteIdval,
			String entityId) throws Exception {
		LOGGER.info("subSiteRegistration: START");

		int userId = 0;
		AccountUser user = null;
		//EnrollmentEntity entityFromSession = null;
		LOGGER.info("Subsite Registration ");
		model.addAttribute(PAGE_TITLE, "Enrollment Entity: SubSite Registration");
		setModelAttributesForSite(model);
		String enrollEntityResponse=null;
		
		try {
			if (!EntityUtils.isEmpty(siteIdval)) {
				// Setting up flag isEdit to true so that subsite page opens up in
				// edit mode for already exisiting site
				model.addAttribute(IS_EDIT, TRUE);
				siteDataById(model, siteIdval);
			}
			if (!isEntityAdmin()) {

				user = userService.getLoggedInUser();
				if (user != null) {
					userId = user.getId();
				}
			}

			else {
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					LOGGER.info(" Inside Primary Site Registration ");
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
				}
			
			}
			getSubSite(model, userId);
		
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			model.addAttribute(LANGUAGES_LISTS, languageDetails);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch (Exception exception) {
			LOGGER.error("Exception occured in subSiteRegistration", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting Site Registration");

		LOGGER.info("subSiteRegistration: END");

		return "entity/enrollmententity/subsite";
	}

	/**
	 * Handles the subsite save
	 * 
	 * @param request
	 * @param @ModelAttribute(SITELANGUAGES) SiteLanguages sitelanguages
	 * @param @ModelAttribute(SITE) Site site,
	 * @param siteLocationHoursString
	 * @param siteLanguageId
	 * @param physicalAddressCheckFlag
	 * @param siteIdVal
	 * @param entityId
	 * @return URL for navigation to entity contact page
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/subsite", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String subSiteSubmit(@ModelAttribute(SITE) @Validated Site site,BindingResult siteResult,
			@ModelAttribute(SITELANGUAGES) @Validated SiteLanguages sitelanguages,BindingResult siteLangaugeResult,
			HttpServletRequest request,
			@RequestParam(value = "siteLocationHoursHidden", required = false) String siteLocationHoursString,
			@RequestParam(value = SITE_ID, required = false) String siteIdval,
			@RequestParam(value = "siteLanguageId", required = false) String siteLanguageId,
			@RequestParam(value = "physicalAddressCheckFlag", required = false) String physicalAddressCheckFlag,
			@RequestParam(value = ENTITY_ID, required = false) String entityId) throws InvalidUserException{

		LOGGER.info("subSiteSubmit: START");

		LOGGER.info("Subsite Submit");
		int userId = 0;
		AccountUser user = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		String enrollEntityResponse= null;
		
		validateSubSite(siteLangaugeResult,siteResult);
		
		try {
			// check whether the logged in user is EntityAdmin or
			// EnrollmentEntity
			user = userService.getLoggedInUser();
			if (!isEntityAdmin()) {
				enrollmentEntityRequestDTO.setUserId(user.getId());
			} else {
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
					enrollmentEntityRequestDTO.setUserId(userId);
				}
			}

			if (!EntityUtils.isEmpty(siteIdval)) {
				site.setId(Integer.parseInt(siteIdval));
				site.setUpdatedBy(Long.valueOf(user.getId()));
			}else{
				//new subsite
				site.setCreatedBy( Long.valueOf(user.getId()));
			}
			site.setSiteType(SUBSITE);
			if("1".equals(physicalAddressCheckFlag) && EntityUtils.isEmpty(site.getPhysicalLocation().getState())){
					site.getPhysicalLocation().setState(site.getMailingLocation().getState());
			}
			enrollmentEntityRequestDTO.setSite(site);
			if (!EntityUtils.isEmpty(siteLanguageId)) {
				sitelanguages.setId(Integer.parseInt(siteLanguageId));
			}
			enrollmentEntityRequestDTO.setSiteLanguages(sitelanguages);
					
			enrollmentEntityRequestDTO.setSiteLocationHoursList(getSiteLocationHoursList(site, siteLocationHoursString));
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = saveSiteDetails(enrollmentEntityRequestDTO);

			if (!isEntityAdmin()) {
				Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(
						NAVIGATION_MENU_MAP);
				navigationMenuMap.put("LOCATION_HOURS", true);
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
				request.getSession().setAttribute(REGISTRATION_STATUS,
						enrollmentEntityResponseDTO.getEnrollmentEntity().getRegistrationStatus());
				request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
						EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.SUBSITE.toString()));
				String status = enrollmentEntityResponseDTO.getEnrollmentEntity().getRegistrationStatus();
				if(isEntityIncompleteOrPending(status)){
					return "redirect:/entity/enrollmententity/viewsite";
				}
			}
			
			if (isEntityAdmin()) {
				return "redirect:/entity/entityadmin/viewsite/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(userId));
			}

			LOGGER.info("subSiteSubmit: END");

			return "redirect:/entity/enrollmententity/subsite";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving sub site information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving sub site information in subSiteSubmit : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private void validateSubSite(BindingResult bindingResult,BindingResult siteResult){
		if(bindingResult.hasErrors() || siteResult.hasErrors()){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	
	/**
	 * Handles the view payment information of Entity
	 * 
	 * @param model
	 * @param request
	 * @param paymentId
	 * @return URL for navigation to appropriate Entity page
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewpaymentinfo", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String viewpaymentinfo(Model model, HttpServletRequest request) {

		LOGGER.info("viewpaymentinfo: START");

		LOGGER.info("View Entity Payment Info ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Entity Payment Info");
		EnrollmentEntity enrollmentEntityObj = null;
		PaymentMethods paymentMethodsObj = new PaymentMethods();
		Map<String, Boolean> navigationMenuMap = null;
		try {
			enrollmentEntityObj = getEnrollmentEntityByUserId();

			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			if (!StringUtils.isBlank(request.getParameter(CANCEL))
					&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE)) {
				model.addAttribute(UPDATEPROFILE, false);
			}
			request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			paymentMethodsObj = getEntityPaymentMethod(enrollmentEntityObj);
			model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodsObj);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			if (!isEntityAdmin()) {
				navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(
						NAVIGATION_MENU_MAP);
				if (enrollmentEntityObj != null && navigationMenuMap==null ) {
					navigationMenuMap = getLeftNavigationMenuMap(enrollmentEntityObj);
				}
				navigationMenuMap.put("PAYMENT_INFO", true);
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
				request.getSession().setAttribute(REGISTRATION_STATUS, enrollmentEntityObj.getRegistrationStatus());
			}
			
			if (paymentMethodsObj != null | !enrollmentEntityObj.getRegistrationStatus().equalsIgnoreCase(INCOMPLETE)) {
				model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodsObj);

				LOGGER.info("viewpaymentinfo: END");

				return "entity/enrollmententity/viewpaymentinfo";
			} else {

				LOGGER.info("viewpaymentinfo: END");

				return "redirect:/entity/enrollmententity/getpaymentinfo";
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing payment information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing payment info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the Submit payment information of Entity
	 * 
	 * @param paymentMethod
	 * @param model
	 * @param enrollmentEntity
	 * @param paymentId
	 * @param request
	 * @return String
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/paymentinfo", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEntityPaymentInfo(Model model, @ModelAttribute(PAYMENT_METHODS_OBJ) PaymentMethods paymentMethod,
			@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("saveEntityPaymentInfo : START");
		LOGGER.info("Post Entity Payment Info");
		List<ObjectError> errorMessages = new ArrayList<ObjectError>();

		// Checking for XSS attack
		checkXSSAttack(paymentMethod);

		request.getSession().setAttribute(UPDATEPROFILE, FALSE);
		boolean isFinancialInfoAdded = false;
	
		PaymentMethods paymentMethodsObj = null;
		
		EnrollmentEntity enrollmentEntityObj = null;
		PaymentMethods paymentMethods = null;

		validatePaymentMethods(request,paymentMethod,enrollmentEntity,errorMessages);
		if(!errorMessages.isEmpty()){
			 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		
		try {
			// check if logged in user is EnrollmentEntity and not EntityAdmin
			if (!isEntityAdmin()) {
				enrollmentEntityObj = getEnrollmentEntityByUserId();
			} else {
				// in case of entity admin logging in,retrieve enrollment entity
				// object from session
				//enrollmentEntityObj = (EnrollmentEntity) request.getSession().getAttribute(ENROLLMENTENTITY);
				String entityId = request.getParameter("entityId");
				
				String enrollEntityResponse= null;
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				enrollmentEntityObj = enrollmentEntityResponseDTO.getEnrollmentEntity();
				
			}

			enrollmentEntityObj.setReceivedPayments(enrollmentEntity.getReceivedPayments());
			PaymentMethods storedPaymentMethodsObj = getEntityPaymentMethod(enrollmentEntityObj);
 	 	 	if("0".equals(enrollmentEntityObj.getReceivedPayments())){
 	 	 		if ("Incomplete".equals(enrollmentEntityObj.getRegistrationStatus())) {
 	 	 			enrollmentEntityObj.setRegistrationStatus("Pending");
 	 	 			enrollmentEntityObj.setStatusChangeDate(new TSDate());
 	 			}
 	 	 	enrollmentEntityObj = updateEnrollmentEntity(enrollmentEntityObj);
 	 	 	
 	 	 	}else if("1".equals(enrollmentEntityObj.getReceivedPayments())){
	 	 	 	paymentMethodsObj = getPaymentMethodObj(storedPaymentMethodsObj,paymentMethod, enrollmentEntityObj, enrollmentEntityObj.getUser().getId());

			if (paymentMethodsObj.getStatus() == null) {
				paymentMethodsObj.setStatus(PaymentStatus.Active);
			}
			paymentMethodsObj.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
			paymentMethodsObj.setModuleId(enrollmentEntityObj.getId());
			paymentMethodsObj.setModuleName(PaymentMethods.ModuleName.ASSISTERENROLLMENTENTITY);
			
			//enrollmentEntityObj = updateEnrollmentEntity(enrollmentEntityObj);

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setPaymentMethods(paymentMethodsObj);
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityObj);
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);
			LOGGER.info("Retrieving Save Payment Detail REST Call Starts");
			String paymentMethodsStr = restClassCommunicator.savePaymentDetails(requestJSON);
			LOGGER.info("Retrieving Save Payment Detail REST Call Ends");
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(paymentMethodsStr);

			if(enrollmentEntityResponseDTO != null){ 
				if(GhixConstants.RESPONSE_FAILURE.equalsIgnoreCase(enrollmentEntityResponseDTO.getStatus())){
					return enrollmentEntityResponseDTO.getErrMsg();
				}
				else{
					paymentMethods = enrollmentEntityResponseDTO.getPaymentMethods();
					enrollmentEntityObj = enrollmentEntityResponseDTO.getEnrollmentEntity();
				}
			}

			if (storedPaymentMethodsObj != null
						&& (!storedPaymentMethodsObj.getPaymentType().equals(paymentMethodsObj.getPaymentType()))) {
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setId(storedPaymentMethodsObj.getId());
				paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.InActive);
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS,
								paymentMethodRequestDTO, String.class);
			}
			}

			// Called to update the status from Incomplete to Pending

			// saving the PaymentMethods info into the payment_methods table
			LOGGER.info("Financial Information is saved");
			isFinancialInfoAdded = (paymentMethods != null) ? true : false;
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
			navigationMenuMap.put(PAYMENT_INFO, true);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
			request.getSession().setAttribute(REGISTRATION_STATUS, enrollmentEntity.getRegistrationStatus());
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethods);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			LOGGER.info("saveEntityPaymentInfo : END");
			return "success";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving payment information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving payment info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private void validatePaymentMethods(HttpServletRequest request,PaymentMethods paymentMethod,EnrollmentEntity enrollmentEntity,List<ObjectError> errorMessages) throws InvalidUserException {
		Locale locale = request.getLocale();
		if(!"1".equals(enrollmentEntity.getReceivedPayments())){
			return;
		}
		
		if(paymentMethod.getPaymentType() == null){
			errorMessages.add(new FieldError(PAYMENT_METHODS, PAYMENT_METHOD, messageSource.getMessage("label.validatePleaseSelectPaymentType", null, locale)));
		}
		else{
			
			Pattern numberPattern = Pattern.compile("[0-9]*");
			if(paymentMethod.getPaymentType() == PaymentMethods.PaymentType.CHECK){
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getAddress1())){
					errorMessages.add(new FieldError(PAYMENT_METHODS, "address", messageSource.getMessage("label.validatePleaseEnterAddress", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getCity())){
					errorMessages.add(new FieldError(PAYMENT_METHODS, PAYMENT_METHOD,messageSource.getMessage("label.validatePleaseEnterCity", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getState())){
					errorMessages.add(new FieldError(PAYMENT_METHODS, PAYMENT_METHOD,messageSource.getMessage("label.validatePleaseSelectState", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getZipcode())){
					errorMessages.add(new FieldError(PAYMENT_METHODS, PAYMENT_METHOD, messageSource.getMessage("label.validatePleaseEnterZipCode", null, locale)));
				}else if(!numberPattern.matcher(paymentMethod.getFinancialInfo().getZipcode()).matches() ||  !(Integer.parseInt(paymentMethod.getFinancialInfo().getZipcode())>0) ){
					errorMessages.add(new FieldError(PAYMENT_METHODS, PAYMENT_METHOD, messageSource.getMessage("label.validateZipSyntax", null, locale)));
				}
			}
			if(paymentMethod.getPaymentType() == PaymentMethods.PaymentType.EFT){
				
				/*if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount())){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", messageSource.getMessage("label.validatePleaseEnterAccountName", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getBankInfo().getBankName())){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", messageSource.getMessage("label.validatePleaseEnterBankName", null, locale)));
				}else if(!Pattern.compile("[a-z0-9\\\\s]+").matcher(paymentMethod.getFinancialInfo().getBankInfo().getBankName()).matches()){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", messageSource.getMessage("label.validateEnterValidBankName", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber())){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", messageSource.getMessage("label.validatePleaseEnterRoutingNumber", null, locale)));
				}else if(!numberPattern.matcher(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber()).matches() ||  !(Integer.parseInt(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber()) > 0)){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", messageSource.getMessage("label.validatePleaseEnterRoutingNumber", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber())){
					errorMessages.add(new FieldError("paymentMethods", "address", messageSource.getMessage("label.validatePleaseEnterAccountNumber", null, locale)));
				}else if(!numberPattern.matcher(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber()).matches() ||  paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber().length() < 9){
					errorMessages.add(new FieldError("paymentMethods", "address", messageSource.getMessage("label.validatePleaseEnterValidAccountNumber", null, locale)));
				}
				if(EntityUtils.isEmpty(paymentMethod.getFinancialInfo().getBankInfo().getAccountType())){
					errorMessages.add(new FieldError("paymentMethods", "Payment method", "Please select account type"));
				}*/
			}
		}
		
	}
	/**
	 * Handles the add payment information of Entity
	 * 
	 * @param model
	 * @param request
	 * @param encryptedEntityId
	 * @return URL for navigation to appropriate Entity Payment page
	 * @throws InvalidUserException 
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/getpaymentinfo", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String paymentInfo(Model model, HttpServletRequest request,@RequestParam(value = ENTITY_ID, required = false) String encryptedEntityId) throws  GIException, InvalidUserException {

		LOGGER.info("paymentInfo : START");
		LOGGER.info("Get Entity Payment Information ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Entity Payment Information");
		//model.addAttribute(ISDLFLAG, TRUE);
		String entityId = null;
		if(null != encryptedEntityId && !"".equals(encryptedEntityId)){
			entityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedEntityId);
		}
	
		int userId = 0;
		EnrollmentEntity enrollmentEntityObj = new EnrollmentEntity();
		Map<String, Boolean> map = null;
		try {
			// check if logged in user is EnrollmentEntity and not EntityAdmin
			if (!isEntityAdmin()) {
				enrollmentEntityObj = getEnrollmentEntityByUserId();				
			} else {
				Validate.notNull(entityId);
				enrollmentEntityObj = getEnrollmentEntityByModuleId(Integer.parseInt(entityId));
				Validate.notNull(enrollmentEntityObj);
				userId = enrollmentEntityObj.getUser().getId();
			}

			if (!StringUtils.isBlank(request.getParameter(CANCEL))
					&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE)) {
				model.addAttribute(UPDATEPROFILE, false);
			}
			PaymentMethods paymentMethodObj = null;
			paymentMethodObj = getEntityPaymentMethod(enrollmentEntityObj);
			
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodObj);
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);

			// get left navigation info
			if (enrollmentEntityObj != null) {
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
			} else {
				map = new HashMap<String, Boolean>();
			}
			
			if ("1".equals(enrollmentEntityObj.getReceivedPayments())) {
				model.addAttribute(IS_SHOW, TRUE);
				if (paymentMethodObj != null) {
					setPaymentType(model, paymentMethodObj);
				}
			} else {
				model.addAttribute(IS_SHOW, FALSE);
			}

			if (isEntityAdmin()) {
				model.addAttribute(IS_ADMIN, TRUE);
			} else {
				model.addAttribute(IS_ADMIN, FALSE);
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			LOGGER.info("paymentInfo : END");
			return "entity/enrollmententity/paymentinfo";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching/saving payment information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching/saving payment info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the GET request for Enrollment Entity's Registration Status
	 * 
	 * @param request
	 * @param model
	 * @return URL for navigation to Registration status page.
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/registrationstatus", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String registrationstatus(Model model, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("registrationstatus : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Registration Status");
		Map<String, Boolean> map = null;
		
		try {
			EnrollmentEntity entityObj = getEnrollmentEntityByUserId();
			if (entityObj != null) {
				model.addAttribute(REGISTRATION_STATUS, entityObj.getRegistrationStatus());
			}
			map = getLeftNavigationMenuMap(entityObj);
			model.addAttribute(ENROLLMENT_ENTITY, entityObj);

			// get the broker details from broker audit table
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setModuleId(entityObj.getId());
			LOGGER.info("Retrieving Enrollment Entity History Detail REST Call starts");
			String paymentMethodsStr = restClassCommunicator
					.retrieveEnrollmentEntityHistoryByUserId(enrollmentEntityRequestDTO);
			LOGGER.info("Retrieving Enrollment Entity History Detail REST Call Ends");
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(paymentMethodsStr);
			List<Map<String, Object>> entityCertStatusHistory = enrollmentEntityResponseDTO
					.getEnrollmentEntityHistory();
			model.addAttribute(STATUS_HISTORY, entityCertStatusHistory);

			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATION_STATUS, entityObj.getRegistrationStatus());
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));

			LOGGER.info("registrationstatus : END");
			return "entity/enrollmententity/registrationstatus";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching registration status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching registration status of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * View population served information
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to population served page
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewPopulationServed", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String viewPopulationServed(@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity,
			Model model, HttpServletRequest request) {

		LOGGER.info("viewPopulationServed: START");

		Map<String, Boolean> map = null;
		LOGGER.info("View Population Served Details");
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			// enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
			enrollmentEntity.setUser(userService.getLoggedInUser());
			enrollmentEntityRequestDTO.setUserId(userService.getLoggedInUser() != null ? userService.getLoggedInUser()
					.getId() : null);

			String strEnrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			String populationServedDetails = restClassCommunicator
					.getPopulationServedDetails(strEnrollmentEntityRequestDTO);

			getPopulationServedInformationAsList(populationServedDetails, model);
			LOGGER.info("Received population served information");
			EnrollmentEntity ee = (EnrollmentEntity) model.asMap().get(ENROLLMENT_ENTITY);
			if (ee != null) {
				map = getLeftNavigationMenuMap(ee);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			
			if(null != ee) {
				request.getSession().setAttribute(REGISTRATION_STATUS, ee.getRegistrationStatus());
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching population served information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching population served information of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewPopulationServed: END");

		return "entity/enrollmententity/viewpopulationserved";
	}

	/**
	 * Retrieve and set population served information as model attributes
	 * 
	 * @param populationServedDetails
	 * @param model
	 * @return {@link Model}
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private void getPopulationServedInformationAsList(String populationServedDetails, Model model) throws JsonProcessingException, IOException {

		LOGGER.info("getPopulationServedInformationAsList: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(populationServedDetails);

		LOGGER.info("Returning from REST call : " + enrollmentEntityResponseDTO.getResponseCode());

	

		Map<String, PopulationLanguage> languagesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationLanguages();
		List<PopulationLanguage> langList = extractLangListFromMap(languagesMap);
		model.addAttribute(LANGLIST, langList);

		Map<String, PopulationEthnicity> ethnicitiesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationEthnicities();
		List<PopulationEthnicity> ethnicities = extractEthnicityListFromMap(ethnicitiesMap);
		model.addAttribute(ETHNICITYLIST, ethnicities);

		Map<String, PopulationIndustry> industriesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationIndustries();
		List<PopulationIndustry> industries = extractIndustryListFromMap(industriesMap);
		model.addAttribute(INDUSTRYLIST, industries);

		model.addAttribute(POPULATION_SERVED_WRAPPER, enrollmentEntityResponseDTO.getPopulationServedWrapper());

		model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityResponseDTO.getEnrollmentEntity());

		LOGGER.info("getPopulationServedInformationAsList: END");

	}

	/**
	 * Extracts Population Industries list associated with EE from industries
	 * map
	 * 
	 * @param industriesMap
	 * @return {@link List} of {@link PopulationIndustry}
	 */
	private List<PopulationIndustry> extractIndustryListFromMap(Map<String, PopulationIndustry> industriesMap) {

		LOGGER.info("extractIndustryListFromMap: START");

		List<PopulationIndustry> industries = new ArrayList<PopulationIndustry>();
		if (industriesMap != null) {
			Set<String> industryKeys = industriesMap.keySet();
			Iterator<String> industryItr = industryKeys.iterator();
			String industryKey;
			PopulationIndustry industryValue;
			while (industryItr.hasNext()) {
				industryKey = industryItr.next();
				industryValue = industriesMap.get(industryKey);
				industries.add(industryValue);
			}
		}

		LOGGER.info("extractIndustryListFromMap: END");

		return industries;
	}

	/**
	 * Extracts Population Ethnicities list associated with EE from ethnicites
	 * map
	 * 
	 * @param ethnicitiesMap
	 * @return {@link List} of {@link PopulationEthnicity}
	 */
	private List<PopulationEthnicity> extractEthnicityListFromMap(Map<String, PopulationEthnicity> ethnicitiesMap) {

		LOGGER.info("extractEthnicityListFromMap: START");

		List<PopulationEthnicity> ethnicities = new ArrayList<PopulationEthnicity>();
		if (ethnicitiesMap != null) {
			Set<String> ethnicityKeys = ethnicitiesMap.keySet();
			Iterator<String> ethnicityItr = ethnicityKeys.iterator();
			String ethnicityKey;
			PopulationEthnicity ethnicityValue;
			while (ethnicityItr.hasNext()) {
				ethnicityKey = ethnicityItr.next();
				ethnicityValue = ethnicitiesMap.get(ethnicityKey);
				ethnicities.add(ethnicityValue);
			}
		}

		LOGGER.info("extractEthnicityListFromMap: END");

		return ethnicities;
	}

	/**
	 * Retrieves population served information
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param model
	 *            Model object
	 * @param encryptedId
	 * @return URL for navigation to population served page
	 */
	@RequestMapping(value = "/entity/enrollmententity/getPopulationServed", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getPopulationServed(@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity,
			Model model, @RequestParam(value=ENTITY_ID,required=false)String encryptedId) {

		LOGGER.info("getPopulationServed: START");

		LOGGER.info("Retrieve population served information");
		String entityId = "0";
		//if(encryptedId != null && !(encryptedId.equals("") || (encryptedId.equals("0"))))
		if(encryptedId != null && !"".equals(encryptedId)){
			entityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
		}
		String enrollEntityResponse= null;
	
		int userId = 0;
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		String populationServedDetails = null;
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);

			if (!isEntityAdmin()) {
				enrollmentEntity.setUser(userService.getLoggedInUser());
				enrollmentEntityRequestDTO.setUserId(userService.getLoggedInUser() != null ? userService.getLoggedInUser()
						.getId() : null);
				String strEnrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);
				populationServedDetails = restClassCommunicator
						.getPopulationServedDetails(strEnrollmentEntityRequestDTO);
			}
			else
			{
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
					
				}
				enrollmentEntityRequestDTO.setUserId(userId);
				String strEnrollmentEntityRequestDTO = GhixUtils.xStreamHibernateXmlMashaling().toXML(
						enrollmentEntityRequestDTO);
				populationServedDetails = restClassCommunicator
						.getPopulationServedDetails(strEnrollmentEntityRequestDTO);
			}
			
		
			getPopulationServedInformation(populationServedDetails, model);
			LOGGER.info("Received population served information");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching population served info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching population served information of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Display population served information");

		LOGGER.info("getPopulationServed: END");

		return "entity/enrollmententity/populationserved";

	}

	/**
	 * Saves population served information into database
	 * 
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param enrollmentEntityId
	 *            Entity Id
	 * @param languages
	 *            list of languages to be added
	 * @param ethnicities
	 *            list of ethnicities to be added
	 * @param industries
	 *            list of industries to be added
	 * @return URL for navigation to primary site page if primary site is not
	 *         yet added by entity else view population served information page
	 */
	@RequestMapping(value = "/entity/enrollmententity/populationServed", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String submitPopulationServed(Model model, HttpServletRequest request,
			@RequestParam(value = "languagesData", required = false) String languages,
			@RequestParam(value = "ethnicitiesData", required = false) String ethnicities,
			@RequestParam(value = "industriesData", required = false) String industries,
			@RequestParam(value = "userId", required = false) Integer userId) {

		LOGGER.info("submitPopulationServed: START");

		try {
			Map<String, PopulationLanguage> populationLanguagesMap = new HashMap<String, PopulationLanguage>();
			Map<String, PopulationEthnicity> populationEthnicitiesMap = new HashMap<String, PopulationEthnicity>();
			Map<String, PopulationIndustry> populationIndustriesMap = new HashMap<String, PopulationIndustry>();
			List<ObjectError> errorMessages = new ArrayList<ObjectError>();

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			PopulationServedWrapper populationServedWrapper = new PopulationServedWrapper();

			// 1. Split and get map of associated population served data
			splitLanguages(languages, populationLanguagesMap,errorMessages);
			splitEthnicities(ethnicities, populationEthnicitiesMap,errorMessages);
			splitIndustries(industries, populationIndustriesMap,errorMessages);
			validatePopulationServed(errorMessages);

			// 2. Add maps into wrapper class
			if (isNotEmpty(populationLanguagesMap)) {
				populationServedWrapper.setPopulationLanguages(populationLanguagesMap);
			}
			if (isNotEmpty(populationEthnicitiesMap)) {
				populationServedWrapper.setPopulationEthnicities(populationEthnicitiesMap);
			}
			if (isNotEmpty(populationIndustriesMap)) {
				populationServedWrapper.setPopulationIndustries(populationIndustriesMap);
			}

			// 3. Add wrapper into DTO and send to rest invoker for saving into
			// the database
			enrollmentEntityRequestDTO.setPopulationServedWrapper(populationServedWrapper);

			boolean isEntityAdmin = isEntityAdmin();
			
			if (isEntityAdmin) {
				enrollmentEntityRequestDTO.setUserId(userId);
			} else {
				enrollmentEntityRequestDTO.setUserId(userService.getLoggedInUser() != null ? userService
						.getLoggedInUser().getId() : null);
			}

			String strEnrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.info("REST Call to ghix-entity to save population served");

			String populationServedDetails = restClassCommunicator.savePopulationServedDetails(strEnrollmentEntityRequestDTO);

			LOGGER.info("Saved Population Served information successfully");

			// 4. Retrieve and display newly associated data
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(populationServedDetails);
			
			LOGGER.info("Returning from REST call : " + enrollmentEntityResponseDTO.getResponseCode());
			
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponseDTO.getEnrollmentEntity();
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			LOGGER.info("Received Population Served information");

			// 5. Set left navigation menu
			if (isEntityAdmin) {
				return "redirect:/entity/entityadmin/viewPopulationServed/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(userId));
			}
			else {
				// Adding following code to enable add/edit page access using
				// left Navigation
				LOGGER.info("Retrieving EnrollmentEntity Object from enrollmentEntityResponse Object");
				request.getSession().setAttribute(REGISTRATION_STATUS, enrollmentEntityObj.getRegistrationStatus());
				
				Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
				navigationMenuMap.put("POPULATION_SERVED", true);
				
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
				request.getSession().setAttribute(NEXTREGISTRATIONSTEP,	EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.POPULATION_SERVED.toString()));
				
				if(isEntityIncompleteOrPending(enrollmentEntityObj.getRegistrationStatus())){
					LOGGER.info("submitPopulationServed: END");
					return "redirect:/entity/enrollmententity/viewPopulationServed";
				}
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			LOGGER.info("Displaying population served information");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving population served information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving population served info of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("submitPopulationServed: END");
		}

		return "redirect:/entity/enrollmententity/primarysite";
	}

	private void validatePopulationServed(List<ObjectError> errorMessages) {
		
		if(!errorMessages.isEmpty()){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	
	private boolean isEntityIncompleteOrPending(String status){
		if(StringUtils.isNotBlank(status) && !status.equalsIgnoreCase(INCOMPLETE) && !status.equalsIgnoreCase(PENDING)){
			return true;
		}
		return false;
	}
	/**
	 * Verifies if logged in user is with role ENTITY_ADMIN
	 * 
	 * @return boolean true/false
	 * @throws InvalidUserException
	 *             exception
	 */
	private boolean isEntityAdmin() throws InvalidUserException {

		return userService.hasUserRole(userService.getLoggedInUser(), RoleService.ENTITY_ADMIN_ROLE);
	}

	/**
	 * Verifies if logged in user is with role ENROLLMENT_ENTITY
	 * 
	 * @return boolean true/false
	 * @throws InvalidUserException
	 */
	private boolean isEnrollmentEntity() throws InvalidUserException {

		return userService.hasUserRole(userService.getLoggedInUser(), RoleService.ENROLLMENT_ENTITY);
	}
	
	/**
	 * Get population served information from entity response DTO
	 * 
	 * @param populationServedDetails
	 *            String returned from ghix-entity
	 * @param model
	 *            Model object
	 * @return Model object
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private void getPopulationServedInformation(String populationServedDetails, Model model) throws JsonProcessingException, IOException {

		LOGGER.info("getPopulationServedInformation: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(populationServedDetails);

		LOGGER.info("Returning from REST call : " + enrollmentEntityResponseDTO.getResponseCode());

		//EnrollmentEntity enrollmentEntity = enrollmentEntityResponseDTO.getEnrollmentEntity();
		
		PopulationServedWrapper populationServedWrapper = enrollmentEntityResponseDTO.getPopulationServedWrapper();

		List<LookupValue> lookupLanguages = lookupService.getLookupValueList("ENTITY_LANGUAGE");
		Collections.sort(lookupLanguages);
		Collections.reverse(lookupLanguages);
		List<LookupValue> lookupEthnicities = lookupService.getLookupValueList("ENTITY_ETHNICITY");

		List<LookupValue> lookupIndustries = lookupService.getLookupValueList("ENTITY_INDUSTRY");

		getOtherPopulationServedData(populationServedWrapper, lookupLanguages, lookupEthnicities, lookupIndustries,
				model);

		model.addAttribute(LANGUAGES_LOOKUP_LIST, lookupLanguages);
		model.addAttribute(ETHNICITIES_LOOKUP_LIST, lookupEthnicities);
		model.addAttribute(INDUSTRIES_LOOKUP_LIST, lookupIndustries);
		model.addAttribute(POPULATION_SERVED_WRAPPER, enrollmentEntityResponseDTO.getPopulationServedWrapper());

		model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityResponseDTO.getEnrollmentEntity());
		model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		
		LOGGER.info("getPopulationServedInformation: END");

	}

	/**
	 * Get other data saved for population served
	 * 
	 * @param populationServedWrapper
	 * @param lookupLanguages
	 * @param lookupEthnicities
	 * @param lookupIndustries
	 * @param model
	 */
	private void getOtherPopulationServedData(PopulationServedWrapper populationServedWrapper,
			List<LookupValue> lookupLanguages, List<LookupValue> lookupEthnicities, List<LookupValue> lookupIndustries,
			Model model) {

		LOGGER.info("getOtherPopulationServedData: START");

		// Creating array of 4 objects to add other population languages
		PopulationData[] languages = getLookUpData(populationServedWrapper.getPopulationLanguages(), lookupLanguages, new PopulationLanguage(), FOUR);
		// Creating array of 2 objects to add other population ethnicities
		PopulationData[] ethnicities = getLookUpData(populationServedWrapper.getPopulationEthnicities(), lookupEthnicities, new PopulationEthnicity(), TWO);
		// Creating array of 2 objects to add other population industries
		PopulationData[] industries = getLookUpData(populationServedWrapper.getPopulationIndustries(), lookupIndustries, new PopulationIndustry(), TWO);

		// model.addAttribute("langList", languages);
		model.addAttribute("langList", languages);
		model.addAttribute("ethnicityList", ethnicities);
		model.addAttribute("industryList", industries);

		LOGGER.info("getOtherPopulationServedData: END");
	}

	/**
	 * Splits string industries added by user and add it to map
	 * 
	 * @param industries
	 *            String from UI
	 * @param populationIndustriesMap
	 *            {@link Map} of {@link PopulationIndustry}
	 */

	private void splitIndustries(String industries, Map<String, PopulationIndustry> populationIndustriesMap,List<ObjectError> errorMessages) {

		LOGGER.info("splitIndustries: START");
		int sum = 0;
		if (StringUtils.isNotBlank(industries)) {
			String industryValue;
			List<String> properties;
			PopulationIndustry populationIndustry;
			List<String> selectedIndustriesList = EntityUtils.splitStringValues(industries, "~");

			for (String selectedIndustry: selectedIndustriesList) {
				properties = Arrays.asList(selectedIndustry.split(","));
				if(properties.size() != TWO){
					errorMessages.add(new ObjectError(POPULATION_INDUSTRY, "{err.enterestimatedpercent}"));
					return;
				}
				populationIndustry = new PopulationIndustry();
				industryValue = lookupService.getLookupValueLabel(INDUSTRY_LOOKUP_TYPE, properties.get(0));
				if (industryValue == null) {
					populationIndustry.setIndustry(properties.get(0));
				} else {
					populationIndustry.setIndustry(industryValue);
				}
				populationIndustry.setIndustryPercentage(Long.valueOf(properties.get(1)));
				populationIndustriesMap.put(populationIndustry.getIndustry(), populationIndustry);
				sum+=populationIndustry.getIndustryPercentage();
			}
		}
		if(sum !=HUNDRED){
			errorMessages.add(new ObjectError(POPULATION_INDUSTRY, "Please select at least one Ethnicity"));
		}
		LOGGER.info("splitIndustries: END");
	}

	/**
	 * Splits string ethnicities added by user and add it to map
	 * 
	 * @param ethnicities
	 * @param populationEthnicitiesMap
	 */
	private void splitEthnicities(String ethnicities, Map<String, PopulationEthnicity> populationEthnicitiesMap,List<ObjectError> errorMessages) {

		LOGGER.info("splitEthnicities: START");
		int sum = 0;
		if (StringUtils.isNotBlank(ethnicities)) {
			String ethnicityValue;
			List<String> properties;
			PopulationEthnicity populationEthnicity;
			List<String> selectedEthnicitiesList = EntityUtils.splitStringValues(ethnicities, "~");

			for (String selectedEthnicity: selectedEthnicitiesList) {
				properties = Arrays.asList(selectedEthnicity.split(","));
				if(properties.size() != TWO){
					errorMessages.add(new ObjectError(POPULATION_ETHNICITY, "{err.enterestimatedpercent}"));
					return;
				}
				populationEthnicity = new PopulationEthnicity();
				ethnicityValue = lookupService.getLookupValueLabel(ETHNICITY_LOOKUP_TYPE, properties.get(0));

				if (ethnicityValue == null) {
					populationEthnicity.setEthnicity(properties.get(0));
				} else {
					populationEthnicity.setEthnicity(ethnicityValue);
				}
				populationEthnicity.setEthnicityPercentage(Long.valueOf(properties.get(1)));
				populationEthnicitiesMap.put(populationEthnicity.getEthnicity(), populationEthnicity);
				sum+=populationEthnicity.getEthnicityPercentage();
			}
		}
		if(sum != HUNDRED){
			errorMessages.add(new ObjectError(POPULATION_ETHNICITY, "{err.totalEthnicitiesServed}"));
		}
		LOGGER.info("splitEthnicities: END");
	}

	/**
	 * Splits string languages added by user and add it to map
	 * 
	 * @param languages
	 * @param populationLanguagesMap
	 */
	private void splitLanguages(String languages, Map<String, PopulationLanguage> populationLanguagesMap,List<ObjectError> errorMessages) {

		LOGGER.info("splitLanguages: START");
		long sum = 0;

		if (StringUtils.isNotBlank(languages)) {
			String langValue;
			List<String> properties;
			PopulationLanguage populationLanguage;
			List<String> selectedLanguagesList = EntityUtils.splitStringValues(languages, "~");

			for (String selectedLanguage: selectedLanguagesList) {
				properties = Arrays.asList(selectedLanguage.split(","));
				if(properties.size() != THREE){
					errorMessages.add(new ObjectError(POPULATION_LANGUAGE, "{err.langPercentAndStaffRequired}"));
					return;
				}
				populationLanguage = new PopulationLanguage();
				langValue = lookupService.getLookupValueLabel(LANGUAGE_LOOKUP_TYPE, properties.get(0));
				if (langValue == null) {
					populationLanguage.setLanguage(properties.get(0));
				} else {
					populationLanguage.setLanguage(langValue);
				}
				populationLanguage.setLanguagePercentage(Long.valueOf(properties.get(1)));
				populationLanguage.setNoOfStaffSpeakLanguage(Integer.valueOf(properties.get(TWO)));
				populationLanguagesMap.put(populationLanguage.getLanguage(), populationLanguage);
				sum+= populationLanguage.getLanguagePercentage();
			}
			}
		if(sum != HUNDRED){
			errorMessages.add(new ObjectError(POPULATION_LANGUAGE, "{err.totalLangAssistance}"));
		}

		LOGGER.info("splitLanguages: END");
	}

	/**
	 * Sets site contact information
	 * 
	 * @param model
	 * @param site
	 */
	private void getSiteContactNumber(Model model, Site site) {

		LOGGER.info("getSiteContactNumber: START");

		if (site.getPrimaryPhoneNumber() != null && (!"".equals(site.getPrimaryPhoneNumber()))) {
			model.addAttribute("primaryPhone1", site.getPrimaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute("primaryPhone2", site.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("primaryPhone3", site.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (site.getSecondaryPhoneNumber() != null && (!"".equals(site.getSecondaryPhoneNumber()))) {
			model.addAttribute("secondaryPhone1", site.getSecondaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute("secondaryPhone2", site.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("secondaryPhone3", site.getSecondaryPhoneNumber().substring(SIX, TEN));
		}

		LOGGER.info("getSiteContactNumber: END");
	}

	/**
	 * set required model attributes for primary/subsite for an enrollment
	 * entity
	 * 
	 * @param model
	 */
	private void setModelAttributesForSite(Model model) {

		LOGGER.info("setModelAttributesForSite: START");
		
		List<LookupValue> fromTimeListOfHours =null;
		if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
			fromTimeListOfHours = lookupService.getLookupValueListForHoursOfOperation(HOURSOFOPERATIONFROMTIME);
		}else{
			fromTimeListOfHours = lookupService.getLookupValueListForHoursOfOperation(ENTITY_SITE_HOURS_TIME);	
		}
		model.addAttribute(FROMTIMELIST, fromTimeListOfHours);
		
		List<LookupValue> toTimeListOfHours = null;
		if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
			toTimeListOfHours = lookupService.getLookupValueListForHoursOfOperation(HOURSOFOPERATIONTOTIME);
		}else{
			toTimeListOfHours = lookupService.getLookupValueListForHoursOfOperation(ENTITY_SITE_HOURS_TIME);
		}
		 
		model.addAttribute(TOTIMELIST, toTimeListOfHours);
		List<LookupValue> listOfdays = lookupService.getLookupValueList(DAYSOFOPERATION);
		model.addAttribute(DAYSLIST, listOfdays);
		List<String> listOfDayNames = new ArrayList<String>();
		for (LookupValue l : listOfdays) {
			String dayName = l.getLookupValueLabel();
			listOfDayNames.add(dayName);
		}
		model.addAttribute(DAYSLISTNAMES, listOfDayNames);
		LookupValue spkLanguage=null;
		List<LookupValue> listOflanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE_SPOKEN);
		for(LookupValue value:listOflanguages){
			if("English".equalsIgnoreCase(value.getLookupValueLabel())){
				spkLanguage=value;
				listOflanguages.remove(value);
				break;
			}
		}
		Collections.sort(listOflanguages);
		Collections.reverse(listOflanguages);
		if(spkLanguage!=null){
			listOflanguages.add(0, spkLanguage);
		}
		model.addAttribute(LISTOFLANGUAGES, listOflanguages);
		List<String> listOfLanguageNames = new ArrayList<String>();
		for (LookupValue l : listOflanguages) {
			String languageName = l.getLookupValueLabel();
			listOfLanguageNames.add(languageName);
		}
		Collections.sort(listOfLanguageNames, Collator.getInstance());
		LookupValue langWritten=null;
		List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
		for(LookupValue lookupValue:listOfLanguagesWritten){
			if("English".equalsIgnoreCase(lookupValue.getLookupValueLabel())){
				langWritten=lookupValue;
				listOfLanguagesWritten.remove(lookupValue);
				break;
			}
		}
		Collections.sort(listOfLanguagesWritten);
		Collections.reverse(listOfLanguagesWritten);
		if(langWritten!=null){
			listOfLanguagesWritten.add(0, langWritten);
		}
		model.addAttribute(LISTOFLANGUAGESWRITTEN, listOfLanguagesWritten);
		List<String> listOfLanguageWritten = new ArrayList<String>();
		for (LookupValue l : listOfLanguagesWritten) {
			String languageName = l.getLookupValueLabel();
			listOfLanguageWritten.add(languageName);
		}

		model.addAttribute(LISTOFLANGUAGENAMES, listOfLanguageNames);
		model.addAttribute(STATELIST, new StateHelper().getAllStates());

		LOGGER.info("setModelAttributesForSite: END");
	}

	

	/**
	 * retrieves site related data based on site Id and sets it in the Model.
	 * 
	 * @param model
	 * @param siteId
	 */
	private void siteDataById(Model model, String siteId) {

		LOGGER.info("siteDataById: START");

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		int comparisonResult;
		boolean isPhysicalLocationNotNull, isMailingLocationNotNull;
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		
		try {
			Site siteById = new Site();
			String siteIds = siteId.replace(",", "");
			int siteIdVal = Integer.valueOf(siteIds);
			siteById.setId(siteIdVal);
			enrollmentEntityRequestDTO.setSite(siteById);
			LOGGER.info("Before Rest Call in site registration");
			String siteRetrived = restClassCommunicator.retieveSiteById(enrollmentEntityRequestDTO);
			LOGGER.info("Rest Call completed.");
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(siteRetrived);
			Site siteObj = enrollmentEntityResponseDTO.getSite();
			model.addAttribute(SITE, siteObj);

			// check whether physical Location attibutes are null.
			isPhysicalLocationNotNull = checkForNullLocationAttributes(siteObj.getPhysicalLocation());

			// check whether mailing Location attibutes are null.
			isMailingLocationNotNull = checkForNullLocationAttributes(siteObj.getMailingLocation());

			// check whether mailing location and physical location
			// are same so that "Same as Mailing Address" Checkbox
			// is checked
			if (isPhysicalLocationNotNull && isMailingLocationNotNull) {
				comparisonResult = siteObj.getPhysicalLocation().compareTo(siteObj.getMailingLocation());
				model.addAttribute(LOCATIONMATCHING, comparisonResult == 0 ? "Y" : "N");
			}
			// retrieves contact number for the site and assigns them to form
			// fields
			getSiteContactNumber(model, siteObj);
			SiteLanguages siteLanguages = enrollmentEntityResponseDTO.getSiteLanguages();
			model.addAttribute(SITELANGUAGES, siteLanguages);
			// retrieves languages entered as Others by user so that they can be
			// displayed on the form in Others category for spoken and written
			// language
			setOtherLanguage(siteLanguages, model);
			List<SiteLocationHours> siteLocationHoursList = enrollmentEntityResponseDTO.getSiteLocationHoursList();
			Map<String, SiteLocationHours> siteLocationHoursMap = new HashMap<String, SiteLocationHours>();
			if (siteLocationHoursList != null) {
				for (SiteLocationHours sitelocationhrs : siteLocationHoursList) {
					// siteLocationHoursMap created with Day as Key so that
					// values can be retrieved on the Jsp form by iterating
					// through list of days.
					siteLocationHoursMap.put(sitelocationhrs.getDay(), sitelocationhrs);
				}
				model.addAttribute(SITELOCATIONHOURSMAP, siteLocationHoursMap);
			}
		} catch (Exception exception) {
			LOGGER.error("Error in siteDataById", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("siteDataById: END");
	}

	/**
	 * Handles the retrieval of primary site
	 * 
	 * @param model
	 * @param isPrimary
	 *            isPrimary (whether the site is a primary site or not)
	 */
	private void getPrimarySite(Model model, int id) {

		LOGGER.info("getPrimarySite: START");

		LOGGER.info("Inside getPrimarySite method");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		int comparisonResult;
		boolean isPhysicalLocationNotNull, isMailingLocationNotNull;
	//	AccountUser user = null;
		enrollmentEntityRequestDTO.setUserId(id);
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		
		try {
			LOGGER.info("Rest Call to ghix-entity to retrieve site details");
			String siteDetails = restClassCommunicator.retrieveSiteDetails(enrollmentEntityRequestDTO);
			LOGGER.info("Rest Call to ghix-entity Successful");
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(siteDetails);
			
			LOGGER.info("EnrollmentEntityResponseDTO ResponseCode" + enrollmentEntityResponseDTO.getResponseCode());
			if (enrollmentEntityResponseDTO.getResponseCode() != TWOZEROONE) {
				List<Site> sites = enrollmentEntityResponseDTO.getSiteList();
			//	List<Site> subSites = new ArrayList<Site>();
				LOGGER.debug("Number of Sites" + sites.size());
				for (Site site : sites) {
					LOGGER.info("Iterating through list of sites");
					if (StringUtils.equalsIgnoreCase(site.getSiteType(), PRIMARYSITE)) {
						model.addAttribute(SITE, site);

						// check whether physical Location attibutes are null.
						isPhysicalLocationNotNull = checkForNullLocationAttributes(site.getPhysicalLocation());

						// check whether mailing Location attibutes are null.
						isMailingLocationNotNull = checkForNullLocationAttributes(site.getMailingLocation());

						// check whether mailing location and physical location
						// are same so that "Same as Mailing Address" Checkbox
						// is checked
						if (isPhysicalLocationNotNull && isMailingLocationNotNull) {
							comparisonResult = site.getPhysicalLocation().compareTo(site.getMailingLocation());
							model.addAttribute(LOCATIONMATCHING, comparisonResult == 0 ? "Y" : "N");
						}
						// retrieves contact number for the site and assigns
						// them to form fields
						getSiteContactNumber(model, site);
						LOGGER.info("Retrieving Site Languages from enrollmentEntityResponseDTO");
						SiteLanguages siteLanguages = enrollmentEntityResponseDTO.getSiteLanguages();
						model.addAttribute(SITELANGUAGES, siteLanguages);
						// retrieves languages entered as Others by user so that
						// they can be displayed on the form in Others category
						// for spoken and written language
						setOtherLanguage(siteLanguages, model);
						LOGGER.info("Retrieving SiteLocation Hours List from enrollmentEntityResponseDTO");
						List<SiteLocationHours> siteLocationHoursList = enrollmentEntityResponseDTO
								.getSiteLocationHoursList();
						Map<String, SiteLocationHours> siteLocationHoursMap = new HashMap<String, SiteLocationHours>();
						if (siteLocationHoursList != null) {
							for (SiteLocationHours sitelocationhrs : siteLocationHoursList) {
								// siteLocationHoursMap created with Day as Key
								// so that values can be retrieved on the Jsp
								// form by iterating through list of days.
								siteLocationHoursMap.put(sitelocationhrs.getDay(), sitelocationhrs);
							}
							model.addAttribute(SITELOCATIONHOURSMAP, siteLocationHoursMap);
						}
					}
				}

				LOGGER.info("Exiting getPrimarySite method");

			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getPrimarySite method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getPrimarySite: END");
	}

	/**
	 * retrieves subsite data from db based on subsite id.
	 * 
	 * @param model
	 * @param id
	 */
	public void getSubSite(Model model, int id) {

		LOGGER.info("getSubSite: START");

		LOGGER.info("Inside getSubSite method");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		
		try {
			enrollmentEntityRequestDTO.setUserId(id);

			String siteDetails = restClassCommunicator.retrieveSiteDetails(enrollmentEntityRequestDTO);

			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(siteDetails);

			if (enrollmentEntityResponseDTO.getResponseCode() != TWOZEROONE) {
				List<Site> sites = enrollmentEntityResponseDTO.getSiteList();
				List<Site> subSites = new ArrayList<Site>();

				for (Site site : sites) {
					if (StringUtils.equalsIgnoreCase(site.getSiteType(), SUBSITE)) {

						subSites.add(site);
					}
				}
				if (subSites.size() != 0) {
					model.addAttribute(SITELIST, subSites);
					int subsiteno = subSites.size();
					// siteNumber to be added as a model attribute so that site
					// table shows up on Jsp only when siteNumber>0
					model.addAttribute(SUBSITENUMBER, subsiteno);
				}
			}

			LOGGER.info("Exiting getSubSite");

		} catch (Exception exception) {
			LOGGER.error("Exception occured in getSiteData method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSubSite: END");
	}

	

	/**
	 * retrieves list of written language names
	 * 
	 * @return list of written languages
	 */
	private List<String> getlistOfWrittenLanguageNames() {

		LOGGER.info("getlistOfWrittenLanguageNames: START");

		List<LookupValue> listOfWrittenlanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
		List<String> listOfWrittenLanguageNames = new ArrayList<String>();
		for (LookupValue l : listOfWrittenlanguages) {
			String languageName = l.getLookupValueLabel();
			listOfWrittenLanguageNames.add(languageName);
		}

		LOGGER.info("getlistOfWrittenLanguageNames: END");

		return listOfWrittenLanguageNames;
	}

	/**
	 * creates the left Navigation Menu Map
	 * 
	 * @param enrollmentEntity
	 * @return leftMenu navigation Map
	 */
	private Map<String, Boolean> getLeftNavigationMenuMap(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("getLeftNavigationMenuMap: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		Map<String, Boolean> map = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts : EnrollmentEntityRequestDTO in Request : "
					+ enrollmentEntityRequestDTO);
			String response = restClassCommunicator.populateNavigationMenuMap(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(response);
			map = enrollmentEntityResponseDTO.getNavigationMenuMap();
			LOGGER.debug("Populate Left Navigation Map REST Call Ends");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Populating Left Navigation Map : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		if(map == null) {
			map = new HashMap<String, Boolean>();
		}

		LOGGER.info("getLeftNavigationMenuMap: END");

		return map;
	}

	/**
	 * retrieves a list of counties served
	 * 
	 * @return List of county names
	 */
	private List<String> getCountiesServed() {

		LOGGER.info("getCountiesServed: START");

		List<String> countiesServed = new ArrayList<String>();
		List<LookupValue> lookupCountiesServed = lookupService.getLookupValueList("ENTITY_COUNTIESSERVED");
		for (LookupValue l : lookupCountiesServed) {
			countiesServed.add(QUOTE + l.getLookupValueLabel() + QUOTE);
		}

		LOGGER.info("getCountiesServed: END");

		return countiesServed;
	}

	/**
	 * 
	 * @param paymentMethod
	 * @param enrollmentEntityObj
	 * @param userId
	 * @return
	 */
	private PaymentMethods getPaymentMethodObj(PaymentMethods paymentMethodsObj, PaymentMethods paymentMethod, EnrollmentEntity enrollmentEntityObj,
			Integer userId) {

		LOGGER.info("getPaymentMethodObj : START");
		//PaymentMethods paymentMethodsObj = null;
		FinancialInfo financialInfo = null;
		BankInfo bankInfo = null;
		//paymentMethodsObj = getEntityPaymentMethod(enrollmentEntityObj);

		if(paymentMethodsObj == null || 
				(!paymentMethod.getPaymentType().equals(paymentMethodsObj.getPaymentType()))){
				paymentMethodsObj = new PaymentMethods();
		}
		
		if ("1".equals(enrollmentEntityObj.getReceivedPayments())) {
/*			paymentMethodsObj = (enrollmentEntityResponseDTO.getPaymentMethods() != null) ? enrollmentEntityResponseDTO
					.getPaymentMethods() : new PaymentMethods();*/
			financialInfo = (paymentMethodsObj.getFinancialInfo() != null) ? paymentMethodsObj.getFinancialInfo()
					: new FinancialInfo();
			
			Validate.notNull(financialInfo);
			
			bankInfo = (financialInfo.getBankInfo() != null) ? financialInfo.getBankInfo() : new BankInfo();

			if ("EFT".equals(paymentMethod.getPaymentType().toString())) {
				// adding the bank related information to the BankInfo object
				bankInfo.setAccountNumber(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber());
				bankInfo.setBankName(paymentMethod.getFinancialInfo().getBankInfo().getBankName());
				bankInfo.setRoutingNumber(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber());
				bankInfo.setAccountType(paymentMethod.getFinancialInfo().getBankInfo().getAccountType());
				bankInfo.setNameOnAccount(paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount());
				financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.EFT);

				financialInfo.setAddress1(null);
				financialInfo.setAddress2(null);
				financialInfo.setCity(null);
				financialInfo.setState(null);
				financialInfo.setZipcode(null);
				getAddressForFinancialInfo(enrollmentEntityObj,financialInfo);
				// setting the BankInfo object to the FinancialInfo object
				financialInfo.setBankInfo(bankInfo);
				paymentMethodsObj.setPaymentType(PaymentMethods.PaymentType.EFT);
				paymentMethodsObj.setPaymentMethodName(PaymentMethods.PaymentType.EFT.toString());

				Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				Set<ConstraintViolation<BankInfo>> violations = null;
				String accountNumber = paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber();
				if(accountNumber != null && accountNumber.equalsIgnoreCase(bankInfo.getAccountNumber())){
						violations = validator.validate(bankInfo, BankInfo.EditPaymentMethod.class);
				}else{
						bankInfo.setAccountNumber(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber());
						violations = validator.validate(bankInfo, BankInfo.EditPaymentMethodBankInfo.class);
				}
				if (violations != null && !violations.isEmpty()) {
						// throw exception in case client side validation breached
						throw new GIRuntimeException(
										GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
			}

			if ("CHECK".equals(paymentMethod.getPaymentType().toString())) {
				financialInfo.setAddress1(paymentMethod.getFinancialInfo().getAddress1());
				financialInfo.setAddress2(paymentMethod.getFinancialInfo().getAddress2());
				financialInfo.setCity(paymentMethod.getFinancialInfo().getCity());
				financialInfo.setState(paymentMethod.getFinancialInfo().getState());
				financialInfo.setZipcode(paymentMethod.getFinancialInfo().getZipcode());
				financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.CHECK);

				// setting the BankInfo object to the FinancialInfo object
				financialInfo.setBankInfo(new BankInfo());
				paymentMethodsObj.setPaymentType(PaymentMethods.PaymentType.CHECK);
 	 	 	 	paymentMethodsObj.setPaymentMethodName(PaymentMethods.PaymentType.CHECK.toString());
			}
			if(null != financialInfo) {
				if(null == financialInfo.getFirstName() || financialInfo.getFirstName().trim().isEmpty()) {
						financialInfo.setFirstName(enrollmentEntityObj.getUser().getFirstName());
				}

				if(null == financialInfo.getLastName() || financialInfo.getLastName().trim().isEmpty()) {
						financialInfo.setLastName(enrollmentEntityObj.getUser().getLastName()); 
				}

				if(null == financialInfo.getEmail() || financialInfo.getEmail().trim().isEmpty()) {
						financialInfo.setEmail(enrollmentEntityObj.getUser().getEmail());
				}

				if(null == financialInfo.getContactNumber() || financialInfo.getContactNumber().isEmpty()) {
						financialInfo.setContactNumber(enrollmentEntityObj.getUser().getPhone());
				}
			}
			paymentMethodsObj.setFinancialInfo(financialInfo);
		}
		
		LOGGER.info("getPaymentMethodObj : END");
		return paymentMethodsObj;
	}

	private void getAddressForFinancialInfo(EnrollmentEntity enrollmentEntity, FinancialInfo financialInfo){
		List<Site> listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,Site.site_type.PRIMARY.toString());
		if(listOfPrimarySites != null && !listOfPrimarySites.isEmpty()){
				Site primarySite = listOfPrimarySites.get(0);
				Location location = new Location();
				location.setAddress1(primarySite.getMailingLocation().getAddress1());
				location.setAddress2(primarySite.getMailingLocation().getAddress2());
				location.setCity(primarySite.getMailingLocation().getCity());
				location.setState(primarySite.getMailingLocation().getState());
				location.setZip(primarySite.getMailingLocation().getZip().toString());
				financialInfo.setLocation(location);
		}
	}
	
	@RequestMapping(value = "/entity/enrollmententity/entitycontactinfo/{entityId}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEntityContactInfoAdmin(Model model, @PathVariable(ENTITY_ID)String entityId) {
		try {
			String decryptEntityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityId);
			return processEntityContactInfo(model, decryptEntityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching entity-contact information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching entity contact info of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	/**
	 * Handles the GET request for Enrollment Entity's Contacts Information
	 * 
	 * @param model
	 * @param entityId
	 * @return
	 */
	@RequestMapping(value = "/entity/enrollmententity/entitycontactinfo", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEntityContactInfo(Model model, @RequestParam(value=ENTITY_ID,required=false)String entityId) {

		try {
			return processEntityContactInfo(model, entityId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching entity contact information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching entity contact info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private String processEntityContactInfo(Model model, String entityId) throws Exception {
		LOGGER.info("getEntityContactInfo: START");

		int userId = 0;
		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		String enrollEntityResponse = null;
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Contact Information");
		LOGGER.info("Entity Contact Information page");
		AccountUser user = null;
		
		try {
			// check whether logged in user is an entityadmin or enrollment
			// entity
			if (isEnrollmentEntity()) {

				user = userService.getLoggedInUser();
				if (user != null) {
					userId = user.getId();
				}
				
			}
			else
			{
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
				}
			}
			enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setUserId(userId);
			// retrieve enrollment entity from database based on userId of
			// logged in entity
			enrollEntityResponse = restClassCommunicator
					.getEnrollmentEntityContactDetails(enrollmentEntityRequestDTO);
			// Retrieve and add EE in model attribute
			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);
			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				// Adding this flag for showing different buttons for Add and
				// Edit
				/*
				 * if (EntityUtils.isEmpty(enrollEntity.getFinFaxNumber())) {
				 * model.addAttribute(ISEDIT, false); } else {
				 * model.addAttribute(ISEDIT, true); }
				 */
				if (!EntityUtils.isEmpty(enrollEntity.getFinFaxNumber()) || isEntityAdmin()) {
					model.addAttribute(ISEDIT, true);
				} else {
					model.addAttribute(ISEDIT, false);
				}
				if(isEntityIncompleteState(enrollEntity.getRegistrationStatus())){
					model.addAttribute(ISEDIT, true);
				}
			}
			model.addAttribute("ENROLLMENT_ENTITY", enrollEntity);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getEntityContactInfo", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getEntityContactInfo: END");

		return "entity/enrollmententity/entitycontactinfo";
	}

	private boolean isEntityIncompleteState(String status) throws InvalidUserException{
		if(!isEntityAdmin() && !EntityUtils.isEmpty(status) && !status.equalsIgnoreCase(INCOMPLETE)){
			return true;
		}
		return false;
	}
	/**
	 * Handles the Submit Entity Contact Information
	 * 
	 * @param enrollmentEntityData
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param entityId
	 * @return
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/enrollmententity/entitycontactinfo", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEntityContactInfo(@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntityData,
			BindingResult bindingResult, Model model, HttpServletRequest request,@RequestParam(value=ENTITY_ID,required=false)String entityId) throws GIException {

		LOGGER.info("saveEntityContactInfo: START");
		//AccountUser user = null;
		//String enrollEntityResponse= null;
		EnrollmentEntity enrollmentEntityObj = null;
		int userId = 0;
		//validate Entity Information
		validateEntityContactInformation(bindingResult,enrollmentEntityData);
		try {
			//EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			// check if logged in user is EnrollmentEntity and not EntityAdmin
			if (isEnrollmentEntity()) {
				enrollmentEntityObj = getEnrollmentEntityByUserId();
			} else {
				if(entityId != null) {
					enrollmentEntityObj = getEnrollmentEntityByModuleId(Integer.parseInt(entityId));
				}
				else {
					throw new GIException("Entity Id cannot be null");
				}
			}
			
			Validate.notNull(enrollmentEntityObj);
			
			enrollmentEntityObj.setPriContactName(enrollmentEntityData.getPriContactName());
			enrollmentEntityObj.setPriContactEmailAddress(enrollmentEntityData.getPriContactEmailAddress());
			enrollmentEntityObj.setPriContactPrimaryPhoneNumber(enrollmentEntityData.getPriContactPrimaryPhoneNumber());
			enrollmentEntityObj.setPriContactSecondaryPhoneNumber(enrollmentEntityData
					.getPriContactSecondaryPhoneNumber());
			enrollmentEntityObj.setPriCommunicationPreference(enrollmentEntityData.getPriCommunicationPreference());
			enrollmentEntityObj.setFinContactName(enrollmentEntityData.getFinContactName());
			enrollmentEntityObj.setFinEmailAddress(enrollmentEntityData.getFinEmailAddress());
			enrollmentEntityObj.setFinPrimaryPhoneNumber(enrollmentEntityData.getFinPrimaryPhoneNumber());
			enrollmentEntityObj.setFinFaxNumber(enrollmentEntityData.getFinFaxNumber());
			enrollmentEntityObj.setFinCommunicationPreference(enrollmentEntityData.getFinCommunicationPreference());

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityObj);
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			// Save enrollment Entity
			restClassCommunicator.saveEnrollmentEntityContactDetails(requestJSON);

			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);

			getContactNumber(model, enrollmentEntityObj);
			
			if (isEnrollmentEntity()) {
				Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
				navigationMenuMap.put(ENTITY_CONTACT, true);
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
				request.getSession().setAttribute(REGISTRATION_STATUS, enrollmentEntityObj.getRegistrationStatus());
				request.getSession().setAttribute(
						NEXTREGISTRATIONSTEP,
						EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.ENTITY_CONTACT
								.toString()));
				if(!enrollmentEntityObj.getRegistrationStatus().equalsIgnoreCase(INCOMPLETE)){
					return "redirect:/entity/enrollmententity/viewentitycontactinfo";
				}
			}
			if (!isEnrollmentEntity()) {
				request.getSession().setAttribute(ENROLLMENTENTITY, enrollmentEntityObj);
				userId = enrollmentEntityObj.getUser().getId();
				return "redirect:/entity/entityadmin/viewentitycontactinfo/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(userId));
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving entity contact information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving entity contact info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveEntityContactInfo: END");
		return "redirect:/entity/assister/registration";
	}

	private void validateEntityContactInformation(BindingResult bindingResult,EnrollmentEntity enrollmentEntityData){
		List<Object> validationGroups = new ArrayList<Object>();
		
		if(!EntityUtils.isEmpty(enrollmentEntityData.getPriCommunicationPreference()) && "Secondary Phone".equals(enrollmentEntityData.getPriCommunicationPreference())){
			validationGroups.add(EnrollmentEntity.EntityContactPrimaryContactSecondaryPhoneNumber.class);
		}
		if(!EntityUtils.isEmpty(enrollmentEntityData.getFinCommunicationPreference()) && "Secondary Phone".equals(enrollmentEntityData.getFinCommunicationPreference())){
			validationGroups.add(EnrollmentEntity.EntityContactFinancialContactSecondaryPhoneNumber.class);
		}
		validationGroups.add(EnrollmentEntity.EntityContactInformation.class);
		validator.validate(enrollmentEntityData,bindingResult,validationGroups.toArray());
		
		if(bindingResult.hasErrors()){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	/**
	 * Handles the view entity contact information of Entity
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewentitycontactinfo", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String viewEntityContactInfo(Model model, HttpServletRequest request,@RequestParam(value=ENTITY_ID,required=false)String entityId) {

		LOGGER.info("viewEntityContactInfo: START");
		LOGGER.info("View Entity Contact Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");
		AccountUser user = null;
		int userId = 0;
	
		Map<String, Boolean> map = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		String enrollEntityResponse = null;
		
		try {
			if (!isEntityAdmin()) {
				user = userService.getLoggedInUser();
				userId = user.getId();

			} else {
				//if role is admin then retirve the user id from rest call
				//GET_ENROLLMENTENTITY_DETAILS_BY_ID
				String decryptEntityId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityId);
				
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(decryptEntityId));
				enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
				}
			}

			EnrollmentEntity enrollEntity = null;
			enrollmentEntityRequestDTO.setUserId(userId);
			enrollEntityResponse = restClassCommunicator
					.getEnrollmentEntityContactDetails(enrollmentEntityRequestDTO);

			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);

			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			model.addAttribute("ENROLLMENT_ENTITY", enrollEntity);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			request.getSession().setAttribute(REGISTRATION_STATUS, enrollEntity.getRegistrationStatus());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing entity contact information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing entity contact info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewEntityContactInfo: END");

		return "entity/enrollmententity/viewentitycontactinfo";
	}

	@RequestMapping(value = "entity/enrollmententity/entityinformation/getcountiesServedList", method = RequestMethod.GET, headers = "Accept=*/*")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getCountiesServedList(@RequestParam(COUNTIES_SERVED_LIST) String name,
			HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("getCountiesServedList : START");
		String jsonData = null;
		List<String> countiesServedlist = new ArrayList<String>();

		response.setContentType(APP_JSON_CONTENT);
		
		try {
			/* countiesServedlist= lookupService.getLookupValueListForEntity(name); */
			for (LookupValue lookupValue : lookupService.getLookupValueListForCountiesServed(name)) {
				countiesServedlist.add(lookupValue.getLookupValueLabel());
			}
			request.getSession().setAttribute(COUNTIES_SERVED_LIST, countiesServedlist);
			LOGGER.info("getCountiesServedList : END");
			jsonData = platformGson.toJson(countiesServedlist);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching counties served information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching counties served info of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return jsonData;

	}
		
	/**
	 * Method to fetch all sites, sitelanguages and sitelocationhours associated
	 * with the logged in entity
	 * 
	 * @param model
	 * @throws Exception 
	 */
	private void getAllSites(Model model) throws Exception {

		LOGGER.info("getAllSites: START");

		LOGGER.info("Inside getAllSites method");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		List<Site> sitesList = null;
		List<Site> siteListByType = new ArrayList<Site>();
		Site primary = null;

		try {
			AccountUser user = userService.getLoggedInUser();
			enrollmentEntityRequestDTO.setUserId(user.getId());
			// rest Call to fetch list of sites and associated siteLocation
			// Hours and siteLanguages
			String siteDetails = restClassCommunicator.getAllSites(enrollmentEntityRequestDTO);
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(siteDetails);

			if(null == enrollmentEntityResponseDTO) {
				LOGGER.warn("Invalid Enrollment Entity Response DTO instance encountered.");
			}
			else {
				// retrieves list of sites from enrollmententityresponseDTO
				sitesList = enrollmentEntityResponseDTO.getSiteList();
	
				// creates a map of siteLocationHours objects
				Map<Integer, List<SiteLocationHours>> siteLocationHrsMap = enrollmentEntityResponseDTO
						.getSiteLocationHoursMap();
	
				// adds siteLocationHoursMap to a model attribute
				model.addAttribute(LOCATIONHOURSMAPWITHLIST, siteLocationHrsMap);
	
				// creates a map of SiteLanguages objects
				Map<Integer, SiteLanguages> siteLanguagesMap = enrollmentEntityResponseDTO.getSiteLanguagesMap();
				// adds siteLanguageMap to model attribute
				model.addAttribute(SITELANGUAGEMAP, siteLanguagesMap);

				for (Site site : sitesList) {
	
					// retrieves contact number for the site and assigns them to
					// form fields
					getSiteContactNumber(model, site);
	
					// put all subsites together in list
					if (!PRIMARYSITE.equalsIgnoreCase(site.getSiteType())) {
						siteListByType.add(site);
	
					} else {
	
						// assign primary site to a temporary object
						primary = site;
					}
				}
			
				// add primary site at 0th position in the list
				siteListByType.add(0, primary);
	
				// adds siteList to model attribute
				model.addAttribute(SITELIST, siteListByType);
			}
			
			LOGGER.info("Exiting getAllSites method");

		} catch (Exception exception) {
			LOGGER.error("Exception occured in getAllSites method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getAllSites: END");
	}

	// Checks for null physical Location and mailing location attributes
	private boolean checkForNullLocationAttributes(EntityLocation location) {

		LOGGER.info("checkForNullLocationAttributes: START");

		String address1 = null;
		String city = null;
		String state = null;
		String zipCode = null;
		address1 = location.getAddress1();
		city = location.getCity();
		state = location.getState();
		zipCode = location.getZip();
		if (address1 != null && city != null && state != null && zipCode != null) {

			LOGGER.info("checkForNullLocationAttributes: END");

			return true;
		}

		LOGGER.info("checkForNullLocationAttributes: END");

		return false;
	}

	/**
	 * Handles the POST request for view document
	 * 
	 * @param documentId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/entity/enrollmententity/viewAttachment", method = RequestMethod.GET)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public void viewAttachment(@RequestParam("documentId") String encryptedId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		LOGGER.info("viewAttachment: START");
		//decrypt the query parameter
		Integer documentId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		EntityDocuments entityDocuments = null;
		LOGGER.info("In View Attachment ");
		
		try {
			if (documentId != null) {
				// get broker Document object
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setModuleId(documentId);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				String entityDocumentsStr = restClassCommunicator
						.retrieveEntityDocumentsObjectById(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

				
				EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocumentsStr);
				
				if(null != enrollmentEntityResponse) {
					entityDocuments = enrollmentEntityResponse.getEntityDocuments();
				}
			}
			byte[] attachment = null;

			if (entityDocuments != null) {
				// get document in byte[] from ECM server
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setEntityType(entityDocuments.getDocumentName());
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				String attachmentStr = restClassCommunicator.retrieveDocumentsfromECMbyId(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

				
				EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(attachmentStr);
				attachment = enrollmentEntityResponse.getAttachment();
			} else {
				attachment = "No Document is attached".getBytes();
			}

			if (entityDocuments != null) {
				response.setContentType(entityDocuments.getMimeType().replaceAll("[\n\r]", ""));
				response.addHeader("Content-Disposition",
						"attachment; filename=" + entityDocuments.getOrgDocumentName().replaceAll("[\n\r]", ""));
			} else {
				response.setContentType("text/plain");
				response.setContentLength(attachment.length);
			}
			FileCopyUtils.copy(attachment, response.getOutputStream());

		} catch (IOException iOException) {
			LOGGER.error("Unable to show attachment", iOException);
			FileCopyUtils.copy("UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes(), response.getOutputStream());
		}

		LOGGER.info("viewAttachment: END");
	}

	/**
	 * uploading the files into their specific folders
	 * 
	 * @param fileToUpload
	 * @param folderName
	 * @param file
	 * @param entityId
	 * @return documentId
	 * @throws Exception 
	 */
	private int uploadFile(String fileToUpload, String folderName, MultipartFile file, int entityId) throws Exception {

		LOGGER.info("uploadFile: START");

		LOGGER.info("In upload File ");
		//EnrollmentEntity enrollmentEntityObj = null;
		int documentId1 = 0;
		
		try {
			String documentId = null;

			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			// upload file to DMS

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setAttachment(file.getBytes());
			enrollmentEntityRequestDTO.setModuleId(entityId);
			enrollmentEntityRequestDTO.setEntityType(modifiedFileName);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String documentIdStr = restClassCommunicator.saveDocumentsOnECM(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(documentIdStr);
			documentId = enrollmentEntityResponse.getDocument();
			if (documentId == null) {

				LOGGER.info("uploadFile: END");

				return documentId1;
			}
			String createdBy = userService.getLoggedInUser().getEmail();

			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			String mimeType = mimeTypesMap.getContentType(file.getOriginalFilename());
			EntityDocuments entityDocs = new EntityDocuments();
			entityDocs.setCreatedBy(createdBy);
			entityDocs.setCreatedDate(new TSDate());
			entityDocs.setDocumentName(documentId);
			entityDocs.setOrgDocumentName(file.getOriginalFilename());
			entityDocs.setMimeType(mimeType);
			enrollmentEntityRequestDTO.setEntityDocuments(entityDocs);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// save document info in broker document
			String entityDocsStr = restClassCommunicator.saveDocument(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

			
			enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
			EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();
			documentId1 = entityDocuments.getID();

		} catch (Exception ex) {
			LOGGER.error("Fail to uplaod Entity Document file. Ex: ", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("uploadFile: END");

		return documentId1;
	}

	/**
	 * Handles the Submit Document of Entity
	 * 
	 * @param addSupportDoc
	 * @param request
	 * @param fileToUpload
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/entity/enrollmententity/uploadsubmit", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEntityWithDocument(Model model, HttpServletRequest request,
			@RequestParam(value = "fileInput", required = false) MultipartFile addSupportDoc,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload) {

		LOGGER.info("saveEntityWithDocument: START");
		final long uploadDocumentAllowedSize = UPLOAD_DOCUMENT_ALLOWED_SIZE;
		
		LOGGER.info("Post Entity Document Upload Info");
		if((addSupportDoc==null)||(addSupportDoc.getSize() > uploadDocumentAllowedSize)){
			request.getSession().setAttribute(UPLOAD_DOCUMENT, "SIZE_FAILURE");
			return "redirect:/entity/enrollmententity/documententity";
		}
		Integer documentId = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		
		try {
			// get logged user
			AccountUser user = userService.getLoggedInUser();
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequest.setModuleId(user.getId());
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// get enrollment entity details
			String enrollmentEntityObjStr = restClassCommunicator
					.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

			 
			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			if ((fileToUpload != null) && ("fileInput".equalsIgnoreCase(fileToUpload))) {
				// call uploadFile method to upload document on ECM server
				documentId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", addSupportDoc, enrollmentEntityObj.getId());
			}
			enrollmentEntityObj.setDocumentIdEE(documentId);
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityObj);
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			// Save enrollment Entity
			String strEnrollmentEntityResponse = restClassCommunicator.saveEnrollmentEntityWithDocument(requestJSON);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(strEnrollmentEntityResponse);
			EnrollmentEntity enrollmentEntityNew = enrollmentEntityResponseDTO.getEnrollmentEntity();
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(
					NAVIGATION_MENU_MAP);
			navigationMenuMap.put("DOCUMENT_UPLOAD", true);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
			request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
					EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.DOCUMENT_UPLOAD.toString()));
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityNew);
			request.getSession().setAttribute(UPLOAD_DOCUMENT, String.valueOf(documentId));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving document : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving document of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveEntityWithDocument: END");

		return "redirect:/entity/enrollmententity/documententity";
	}

	/**
	 * Handles the request for Document upload of Entity
	 * 
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Document upload of Entity
	 */
	@RequestMapping(value = "/entity/enrollmententity/documententity", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String documentEE(Model model, HttpServletRequest request) {

		LOGGER.info("documentEE: START");

		LOGGER.info("Get Entity Document Information ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Entity Document Information");
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);

		try {
			Map<String, Boolean> map = null;
			AccountUser user = userService.getLoggedInUser();
			// get enrollment entity details
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequest.setModuleId(user.getId());
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
			
			EnrollmentEntityResponseDTO enrollmentEntityResponse = reader.readValue(entityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			if (enrollmentEntityObj == null) {
				enrollmentEntityObj = new EnrollmentEntity();
				enrollmentEntityObj.setUser(user);
			}

			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			List<Map<String, Object>> entityCertStatusHistory = null;
			if (enrollmentEntityObj != null) {
				// get history of documents for enrollment entity
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
				LOGGER.info("Retrieving Enrollment Entity History Detail REST Call starts");
				String enrollmentEntityHistoryStr = restClassCommunicator
						.retrieveEnrollmentEntityDocumentHistoryById(enrollmentEntityRequestDTO);
				LOGGER.info("Retrieving Enrollment Entity History Detail REST Call Ends");
				
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(enrollmentEntityHistoryStr);
				entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			model.addAttribute("enrollmentRegisterStatusHistory", entityCertStatusHistory);
			model.addAttribute("STATE_CODE", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			if (!EntityUtils.isEmpty((String) request.getSession().getAttribute(UPLOAD_DOCUMENT))) {
				model.addAttribute(UPLOAD_DOCUMENT, request.getSession().getAttribute(UPLOAD_DOCUMENT));
				request.getSession().removeAttribute(UPLOAD_DOCUMENT);
			}

		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching document : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching document of enrollmententity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("documentEE: END");

		return "entity/enrollmententity/documententity";
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param encryptedId
	 * @return URL for navigation to document upload
	 * @throws ContentManagementServiceException
	 */
	@RequestMapping(value = "/entity/enrollmententity/remove", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String removeDocument(@RequestParam("documentId") String encryptedId, Model model, HttpServletRequest request) throws ContentManagementServiceException {

		LOGGER.info("removeDocument: START");

		LOGGER.info("In document remove ");
		//decrypt the query parameter
		Integer documentId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		
		try {
			if (documentId != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setModuleId(documentId);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				// delete document from broker document
				restClassCommunicator.deleteDocument(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
				Map<String, Boolean> map = null;
				AccountUser user = userService.getLoggedInUser();
				enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setModuleId(user.getId());
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				// get enrollment entity deatil
				String entityObjStr = restClassCommunicator
						.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
				
				EnrollmentEntityResponseDTO enrollmentEntityResponse = reader.readValue(entityObjStr);
				EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
				if (enrollmentEntityObj == null) {
					enrollmentEntityObj = new EnrollmentEntity();
					enrollmentEntityObj.setUser(user);
				}
				model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
				List<Map<String, Object>> entityCertStatusHistory = null;
				if (enrollmentEntityObj != null) {
					EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call starts");
					// get document history of enrollment entity
					String enrollmentEntityHistoryStr = restClassCommunicator
							.retrieveEnrollmentEntityDocumentHistoryById(enrollmentEntityRequestDTO);
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call Ends");
					EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(enrollmentEntityHistoryStr);
					entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
					map = getLeftNavigationMenuMap(enrollmentEntityObj);
				} else {
					map = new HashMap<String, Boolean>();
				}
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
				model.addAttribute("enrollmentRegisterStatusHistory", entityCertStatusHistory);
				model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));

			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while deleting document : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while deleting document : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("removeDocument: END");

		return "entity/enrollmententity/documententity";
	}

	@RequestMapping(value = "/entity/enrollmententity/directToSite", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String directToSite(@RequestParam("siteType") String encSiteType, @RequestParam String siteId, 
			@RequestParam(value=ENTITY_ID,required=false)String entityId) {

		LOGGER.info("directToSite: START");
		String siteType =ghixJasyptEncrytorUtil.decryptStringByJasypt(encSiteType);
		if (siteType != null && siteType.equalsIgnoreCase(PRIMARYSITE)) {

			LOGGER.info("directToSite: END");
			return "redirect:/entity/enrollmententity/primarysite/"+ entityId +"?siteId=" + siteId;
		} else {

			LOGGER.info("directToSite: END");
			return "redirect:/entity/enrollmententity/subsite/" + entityId + "?siteId=" + siteId;
		}
	}

	/**
	 * Retrieves list of designated individuals
	 * 
	 * @param desigStatus
	 *            Status of designated individual to search for
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/enrollmententity/individuals", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ENTITY_VIEW_INDIVIDUAL')")
	public String getIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus,
			Model model, HttpServletRequest request) {

		LOGGER.info("EnrollmentEntityController.getIndividuals: START");
		
		Integer pageSize = PAGE_SIZE;
		request.setAttribute("pageSize", pageSize);
		request.setAttribute("reqURI", "individuals");
		
		String param = request.getParameter("pageNumber");
	//	EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		Integer startRecord = 0;
		Integer iResultCt = null;
		int iResultCount = -1;
		int currentPage = 1;
		
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
		}
		
		try {
			Map<String, Object> searchCriteria;
			Map<String, Object> map = new HashMap<>();
			BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
			AccountUser user = userService.getLoggedInUser();
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequest.setModuleId(user.getId());
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// get enrollment entity deatil
			String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			if(EntityUtils.isEnrollmentEntityCertifiedAndActive(enrollmentEntityObj)){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}
			
			
			// populate search criteria			
			searchCriteria = this.createSearchCriteriaForIndividuals(request, enrollmentEntityObj.getId(), desigStatus,brokerBOBSearchParamsDTO);
			
			Map<String, Object> houseHoldMap = null;
			String previousSortBy = request.getParameter("previousSortBy");
			String previousSortOrder = request.getParameter("previousSortOrder");
			boolean isSortChanged = false;
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				List<Long> individualsId = null;
				List<Long> nextIndividualIds = null; 
				brokerBOBSearchParamsDTO.setDesignationStatus(desigStatus);
				brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				//sort IND72 feature
				if(brokerBOBSearchParamsDTO !=null && previousSortBy == null && previousSortOrder == null){
					previousSortBy = brokerBOBSearchParamsDTO.getSortBy();
					previousSortOrder = brokerBOBSearchParamsDTO.getSortOder();
				}else if(brokerBOBSearchParamsDTO!=null && brokerBOBSearchParamsDTO.getSortBy() != null && brokerBOBSearchParamsDTO.getSortOder() != null 
						&&  !brokerBOBSearchParamsDTO.getSortBy().equalsIgnoreCase(previousSortBy) &&  !brokerBOBSearchParamsDTO.getSortOder().equalsIgnoreCase(previousSortOrder)){
					isSortChanged = true;
					currentPage = 1;
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				}
				
				if((currentPage%PAGE_SIZE)!=1 && request.getParameterValues("individualsIdAtClient") !=null  && request.getParameter("landingFromPrev") == null){
					nextIndividualIds = new ArrayList<Long>();
					individualsId= new ArrayList<Long>();
					for(String tmpStrId:request.getParameterValues("individualsIdAtClient")){
						individualsId.add(Long.parseLong(tmpStrId));
					}
					
					iResultCount = request.getParameter("totalCountAtClient")!= null ? !request.getParameter("totalCountAtClient").isEmpty()? Integer.parseInt(request.getParameter("totalCountAtClient")) :0:0;
					
					if(individualsId.size() > 0 && !isSortChanged){
						int size = individualsId.size();
						if(size <= PAGE_SIZE){
							nextIndividualIds = individualsId;
						} else if(size > ((currentPage-1)%PAGE_SIZE)*PAGE_SIZE){
							nextIndividualIds = individualsId.subList(((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
						}else{
							nextIndividualIds = individualsId.subList(((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
						}
						brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);	
					}else if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") )){
						brokerBOBSearchParamsDTO.setPageNumber(currentPage-PAGE_SIZE+1);
					}
				}
				
				BrokerBobResponseDTO brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				
				if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") ) && brokerBobResponseDTO!=null &&
						brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
					
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
					 
					individualsId = brokerBobResponseDTO.getIndividualsId();
					iResultCount = brokerBobResponseDTO.getCount();
					int size = individualsId.size();
					if(size <= PAGE_SIZE){
						nextIndividualIds = individualsId;
					} else if(size > ((currentPage-1)%PAGE_SIZE)  *PAGE_SIZE){
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
					}else{
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
					}
					 
					brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);
					
					brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				}
				
				if(brokerBobResponseDTO!=null){
					List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO = brokerBobResponseDTO.getBrokerBobDetailsDTO();
					
					if( brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
						individualsId = brokerBobResponseDTO.getIndividualsId();
					}
					
					if((currentPage%PAGE_SIZE)==1){
						iResultCount = brokerBobResponseDTO.getCount();
					}
					
					if(null != individualsId && individualsId.size() > 0){
						model.addAttribute("individualsId", individualsId);
					}
					
					houseHoldObjList = getConsumerCompositList(brokerBOBDetailsDTO) ;
					
				}
				
				model.addAttribute(BrokerConstants.CURRENT_STATUS_LIST, getLookupValues(lookupService.getLookupValueList("CURRENT_STATUS")));
				model.addAttribute(BrokerConstants.ENROLLMENT_STATUS_LIST, getLookupValues(lookupService.getLookupValueList("ENROLLMENT_STATUS_TYPE")));
			}else{
				searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
				searchCriteria.put(BrokerConstants.STATUS, desigStatus);
				map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
				houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
				
				model.addAttribute(BrokerConstants.APPLICATION_STATUS_LIST,  ApplicationStatus.values());
				model.addAttribute(BrokerConstants.ELIGIBILITY_STATUS_LIST,  EligibilityStatus.values());
				
				if(houseHoldMap!=null && !houseHoldMap.isEmpty()){
					houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					iResultCt = (Integer) Integer.parseInt((String)houseHoldMap.get(BrokerConstants.TOTAL_RECORD_COUNT));
				}	
				
				if (iResultCt != null) {
					iResultCount = iResultCt.intValue();
				}
				
			}
			
			//Set in model
			model.addAttribute("previousSortOrder", previousSortOrder);
			model.addAttribute("previousSortBy", previousSortBy);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
			model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
			
			model.addAttribute(INDIVIDUALLIST, houseHoldObjList);
			model.addAttribute(RESULTSIZE, iResultCount);
			
			model.addAttribute("searchCriteria", searchCriteria);
			model.addAttribute(BrokerConstants.SORT_ORDER, (String) searchCriteria.get(BrokerConstants.SORT_ORDER));
			model.addAttribute(PAGE_TITLE, IND_PAGETITLE);
			model.addAttribute(DESIG_STATUS, desigStatus);
			model.addAttribute(CURRENTPAGE, currentPage);
			
		}catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching pending individuals : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching pending individuals list : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("EnrollmentEntityController.getIndividuals: END");

		return "entity/enrollmententity/individuals";
	}
	
	
	private List<String> getLookupValues(List<LookupValue> lookupValues){
		List<String> labels = new ArrayList<>();
		
		if(lookupValues!=null){
			for(LookupValue value:lookupValues){
				labels.add(value.getLookupValueLabel());
			}
		}
		
		return labels;
	}

	/**
	 * Handles the reset all functionality
	 * 
	 * @param desigStatus
	 * @param model
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/enrollmententity/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ENTITY_MANAGE_ENTITY')")
	public String resetAll(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model) {

		LOGGER.info("resetAll: START");

		model.addAttribute(PAGE_TITLE, IND_PAGETITLE);

		LOGGER.info("resetAll: END");

		return "redirect:/entity/enrollmententity/individuals?desigStatus="+desigStatus;

	}
	
	/**
	 * Creates search criteria for Individuals
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            broker id
	 * @param brokerBOBSearchParamsDTO 
	 * @return {@link Map} of search parameter with value
	 */
	private Map<String, Object> createSearchCriteriaForIndividuals(HttpServletRequest request, int id, String desigStatus, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {

		LOGGER.info("EnrollmentEntityController.createSearchCriteriaForIndividuals : START");

		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		String sortBy = null;
		
		if(desigStatus!=null){
				if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.Active.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
				}
				else if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.Pending.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.REQUESTSENT : request.getParameter(BrokerConstants.SORT_BY);
				}
				else if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.InActive.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.INACTIVESINCE : request.getParameter(BrokerConstants.SORT_BY);
				}
				else{
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
				}
		}
		else{
			LOGGER.info("EnrollmentEntityController.createSearchCriteriaForIndividuals - Designation Status is Null.");
			sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
		}
		
		String sortOrder = determineSortOrderForCriteria(request);
		
		searchCriteria.put(BrokerConstants.SORT_BY, sortBy);
		searchCriteria.put(BrokerConstants.SORT_ORDER, sortOrder);
		
		brokerBOBSearchParamsDTO.setSortBy(sortBy);
		brokerBOBSearchParamsDTO.setSortOder(sortOrder);

		String assisterId = String.valueOf(id);
		searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ENTITY);
		brokerBOBSearchParamsDTO.setRecordType("Entity");
		
		String indFirstOrLastName = (request.getParameter(BrokerConstants.INDIVIDUAL_FIRST_OR_LAST_NAME) == null) ? "" : request.getParameter(BrokerConstants.INDIVIDUAL_FIRST_OR_LAST_NAME);
		String cecFirstOrLastName = (request.getParameter(BrokerConstants.CEC_FIRST_OR_LAST_NAME) == null) ? "" : request.getParameter(BrokerConstants.CEC_FIRST_OR_LAST_NAME);
		
		if (!"".equals(assisterId)) {
			searchCriteria.put(BrokerConstants.MODULE_ID, assisterId);
			brokerBOBSearchParamsDTO.setRecordId(Long.parseLong(assisterId));
		}
		if (!"".equals(indFirstOrLastName)) {
			searchCriteria.put(BrokerConstants.INDIVIDUAL_FIRST_OR_LAST_NAME, indFirstOrLastName);
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				String[] array = indFirstOrLastName.split(" ");
				brokerBOBSearchParamsDTO.setFirstName(array[0]);
				if(array.length>1){
					brokerBOBSearchParamsDTO.setLastName(array[1]);
				} 
			}
		}
		if (!"".equals(cecFirstOrLastName)) {
			searchCriteria.put(BrokerConstants.CEC_FIRST_OR_LAST_NAME, cecFirstOrLastName);
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				String[] array = cecFirstOrLastName.split(" ");
				brokerBOBSearchParamsDTO.setCounselorFirstName(array[0]);
				if(array.length>1){
					brokerBOBSearchParamsDTO.setCounselorLastName(array[1]);
				} 
			}
		}
		
		String applicationStatus = (request.getParameter(BrokerConstants.APPLICATION_STATUS) == null) ? "" : request.getParameter(BrokerConstants.APPLICATION_STATUS);
		if(!"".equals(applicationStatus)){
			searchCriteria.put(BrokerConstants.APPLICATION_STATUS, applicationStatus);
			brokerBOBSearchParamsDTO.setAplicationStatus(applicationStatus);
		}
		
		String eligibilityStatus = (request.getParameter(BrokerConstants.ELIGIBILITY_STATUS) == null) ? "" : request.getParameter(BrokerConstants.ELIGIBILITY_STATUS);
		if(!"".equals(eligibilityStatus)){
			searchCriteria.put(BrokerConstants.ELIGIBILITY_STATUS, eligibilityStatus);
			brokerBOBSearchParamsDTO.setEligibilityStatus(eligibilityStatus);
		}
		
		String enrollmentStatus = (request.getParameter(BrokerConstants.ENROLLMENT_STATUS) == null) ? "" : request.getParameter(BrokerConstants.ENROLLMENT_STATUS);
		if(!"".equals(enrollmentStatus)){
			searchCriteria.put(BrokerConstants.ENROLLMENT_STATUS, enrollmentStatus);
			brokerBOBSearchParamsDTO.setEnrollmentStatus(enrollmentStatus);
		}
		
		String currentStatus = (request.getParameter(BrokerConstants.CURRENT_STATUS) == null) ? "" : request.getParameter(BrokerConstants.CURRENT_STATUS);
		if(!"".equals(currentStatus)){
			searchCriteria.put(BrokerConstants.CURRENT_STATUS, currentStatus);
			brokerBOBSearchParamsDTO.setCurrentStatus(currentStatus);
		}
		
		setDates(searchCriteria, request,brokerBOBSearchParamsDTO);

		LOGGER.info("EnrollmentEntityController.createSearchCriteriaForIndividuals : END");

		return searchCriteria;
	}
	
	private void setFirstAndLastName(String name,BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO){
		String[] array = name.split(" ");
		brokerBOBSearchParamsDTO.setFirstName(array[0]);
		if(array.length>1){
			brokerBOBSearchParamsDTO.setLastName(array[1]);
		} 
	}
	
	/**
	 * Method to determine the Sort Order for Criteria. 
	 * 
	 * @param request The HttpServletRequest instance.
	 * @return sortOrder The sort order value.
	 */
	private String determineSortOrderForCriteria(HttpServletRequest request) {
		String sortOrder =  (request.getParameter(BrokerConstants.SORT_ORDER) == null) ?  BrokerConstants.ASC : request.getParameter(BrokerConstants.SORT_ORDER);
		sortOrder = (sortOrder.equals("")) ? BrokerConstants.ASC : sortOrder;
		String changeOrder =  (request.getParameter(BrokerConstants.CHANGE_ORDER) == null) ?  FALSE : request.getParameter(BrokerConstants.CHANGE_ORDER);
		sortOrder = (changeOrder.equalsIgnoreCase(TRUE) ) ? ((sortOrder.equals(BrokerConstants.DESC)) ? BrokerConstants.ASC : BrokerConstants.DESC ): sortOrder;	
		return sortOrder;
	}

	/*private void addToSearchCriteria(Map<String, Object> searchCriteria, String key, 
			String value) {
		if (isValueBlank(value)) {
			searchCriteria.put(key, value);
		}
	}*/
	
	/**
	 * @param value
	 * @return
	 */
	/*private boolean isValueBlank(String value) {
		return !value.equals("");
	}*/

	/**
	 * Sets dates in search criteria
	 * 
	 * @param searchCriteria
	 *            {@link Map} of search criteria
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param brokerBOBSearchParamsDTO 
	 */
	private void setDates(Map<String, Object> searchCriteria, HttpServletRequest request, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {
		
		LOGGER.info("setDates : START");

		String requestSentFrom = (request.getParameter(BrokerConstants.REQUEST_SENT_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_FROM);
		String requestSentTo = (request.getParameter(BrokerConstants.REQUEST_SENT_TO) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_TO);
		String inactiveDateFrom = (request.getParameter(BrokerConstants.INACTIVE_DATE_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_FROM);
		String inactiveDateTo = (request.getParameter(BrokerConstants.INACTIVE_DATE_TO) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_TO);
		String activeDateTo = (request.getParameter(BrokerConstants.ACTIVE_DATE_TO) == null) ? "" : request
				.getParameter(BrokerConstants.ACTIVE_DATE_TO);
		String activeDateFrom = (request.getParameter(BrokerConstants.ACTIVE_DATE_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.ACTIVE_DATE_FROM);

		if (!"".equals(requestSentFrom)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_FROM, requestSentFrom);
			brokerBOBSearchParamsDTO.setRequestSentFrmDt(requestSentFrom);
		}
		if (!"".equals(requestSentTo)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_TO, requestSentTo);
			brokerBOBSearchParamsDTO.setRequestSentToDt(requestSentTo);
		}
		if (!"".equals(inactiveDateFrom)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_FROM, inactiveDateFrom);
			brokerBOBSearchParamsDTO.setInactiveSinceFrmDt(inactiveDateFrom);
		}
		if (!"".equals(inactiveDateTo)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_TO, inactiveDateTo);
			brokerBOBSearchParamsDTO.setInactiveSinceToDt(inactiveDateTo);
		}
		if (!"".equals(activeDateFrom)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_FROM, activeDateFrom);
			brokerBOBSearchParamsDTO.setActiveSinceFromDate(activeDateFrom);
		}
		if (!"".equals(activeDateTo)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_TO, activeDateTo);
			brokerBOBSearchParamsDTO.setActiveSinceToDate(activeDateTo);
		}

		LOGGER.info("setDates : END");
	}

	/**
	 * method for getting autocomplete language list on site, subsite and
	 * enrollment counselor pages
	 * @param query
	 * 
	 * */
	@RequestMapping(value = "/getOtherLanaguageList", method = RequestMethod.GET, headers = "Accept=*/*")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public List<String> getOtherLanaguageList(@RequestParam("term") String query) {

		LOGGER.info("getOtherLanaguageList: START");

		List<String> otherLanguageList = new ArrayList<String>();
		LOGGER.info("spoken language autocomplete start here");

		try {
			otherLanguageList = lookupService.populateLanguageNames(query);

			if (null == otherLanguageList) {
				languageList = new ArrayList<String>();
			} else {
				languageList = otherLanguageList;
			}

			LOGGER.info("getOtherLanaguageList: END");

			return getOtherLanguageList(query);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching other language list : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching other language list of entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * retrieves a list of languages other than ones populated from lookup
	 * 
	 * @param query
	 * @return list of other languages
	 */
	private List<String> getOtherLanguageList(String query) {

		LOGGER.info("getOtherLanguageList: START");
		List<String> matched = new ArrayList<String>();
		String country = null;
		String queryLowerCase = null;
		if (query != null) {
			queryLowerCase = query.toLowerCase();
			for (int i = 0; i < languageList.size(); i++) {
				country = languageList.get(i).toLowerCase();
				if (country.startsWith(queryLowerCase)) {
					matched.add(languageList.get(i));
				}
			}
		}
		LOGGER.info("getOtherLanguageList: END");

		return matched;
	}

	/**
	 * method to validate counties served entered only from drop down
	 * 
	 * @param countiesServed
	 * @return Boolean <code>true</code> if County served is valid else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/enrollmententity/entityinformation/checkCountiesServed")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public Boolean checkCountiesServed(@RequestParam String countiesServed) {

		LOGGER.info("checkCountiesServed: START");

		LOGGER.info("Counties served validation start here");
		List<String> countiesServedlist = new ArrayList<String>();
		List<LookupValue> lookupCountiesServed = lookupService.getLookupValueList("ENTITY_COUNTIESSERVED");
		for (LookupValue l : lookupCountiesServed) {
			countiesServedlist.add(l.getLookupValueLabel());
		}
		String countiesServedObj = countiesServed.trim();
		List<String> countiesStrList = Arrays.asList(countiesServedObj.split(","));
		for (String countiesStr : countiesStrList) {
			if (!countiesServedlist.contains(countiesStr.trim())) {

				LOGGER.info("checkCountiesServed: END");

				return false;
			}
		}

		LOGGER.info("checkCountiesServed: END");

		return true;
	}

	/**
	 * This method retrieves the URLfor the next step after the last completed
	 * step.
	 * 
	 * @param String
	 *            lastCompletedStep
	 * @return String url for next Step
	 */
	private String getNextStepURL(String lastCompletedStep) {

		LOGGER.info("getNextStepURL: START");

		String step = null;
		//EnrollmentEntity.registrationSteps nextStep = null;
		String nextStepName = null;
		String stepURL = null;
		boolean isCAStateCode = EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE);
		// create an Enum map of all the steps with the steps with their
		// respective URLs.
		Map<EnrollmentEntity.registrationSteps, String> stepMap = new EnumMap<EnrollmentEntity.registrationSteps, String>(
				EnrollmentEntity.registrationSteps.class);
		stepMap.put(EnrollmentEntity.registrationSteps.ENTITY_INFO,
				"redirect:/entity/enrollmententity/entityinformation");
		stepMap.put(EnrollmentEntity.registrationSteps.POPULATION_SERVED,
				"redirect:/entity/enrollmententity/getPopulationServed");
		stepMap.put(EnrollmentEntity.registrationSteps.PRIMARY, "redirect:/entity/enrollmententity/primarysite");
		stepMap.put(EnrollmentEntity.registrationSteps.SUBSITE, "redirect:/entity/enrollmententity/subsite");
		stepMap.put(EnrollmentEntity.registrationSteps.ENTITY_CONTACT,
				"redirect:/entity/enrollmententity/entitycontactinfo");
		stepMap.put(EnrollmentEntity.registrationSteps.ASSISTER, "redirect:/entity/assister/registration");
		stepMap.put(EnrollmentEntity.registrationSteps.DOCUMENT_UPLOAD,
				"redirect:/entity/enrollmententity/documententity");
		if(!isCAStateCode){
			stepMap.put(EnrollmentEntity.registrationSteps.PAYMENT_INFO, "redirect:/entity/enrollmententity/getpaymentinfo");
		}
		stepMap.put(EnrollmentEntity.registrationSteps.REGISTRATION_STATUS,
				"redirect:/entity/enrollmententity/registrationstatus");
		// step URL set for entityinformation page so that in case of exceptions
		// entity is directed to entityinfo page
		stepURL = stepMap.get(EnrollmentEntity.registrationSteps.ENTITY_INFO);
	//	int index = 0;
		try {
			// get the name of last completed step from the list
			step = lastCompletedStep != null ? lastCompletedStep : "ENTITY_INFO";
			// get the name of the step next to last completed one
			nextStepName = EntityUtils.getNextRegistrationStep(step);
			EnrollmentEntity.registrationSteps[] regstep = EnrollmentEntity.registrationSteps.values();
			for (int i = 0; i < EnrollmentEntity.registrationSteps.values().length; i++) {
				// get the URL for the next step from the Enum map
				if (regstep[i].toString().equals(nextStepName)) {
					stepURL = stepMap.get(regstep[i]);
				}
			}

		} catch (Exception exception) {
			LOGGER.error("Exception occured in getNextStepURL method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getNextStepURL: END");

		return stepURL;
	}

	/**
	 * Retrieves last completed step in registration process
	 * 
	 * @param enrollmentEntity
	 * @return String lastCompleted Step
	 */
	private String getlastCompletedStep(EnrollmentEntity enrollmentEntity,Map<String, Boolean> map) {

		LOGGER.info("getlastCompletedStep: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		List<String> orderedMapKeys = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		String lastCompletedStep = null;
		int size = 0;
		
		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts for getting all the steps entity has finished.");
			String response = restClassCommunicator.populateNavigationMenuMap(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(response);
			if(enrollmentEntityResponseDTO!=null) { 
				if( enrollmentEntityResponseDTO.getNavigationMenuMap()!=null)
				{
					map.putAll(enrollmentEntityResponseDTO.getNavigationMenuMap());
				}			
				orderedMapKeys = enrollmentEntityResponseDTO.getSortedListOfMapKeys();
			}
			
			Validate.notNull(orderedMapKeys);
			
			size = orderedMapKeys.size();

			// Added Fix for HIX-18412
			if (orderedMapKeys != null && !orderedMapKeys.isEmpty()) {
				lastCompletedStep = orderedMapKeys.get(size - 1);
			}

			LOGGER.debug("Populate Left Navigation Map REST Call Ends for getting all the steps entity has finished.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while getting all the steps entity has finished : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getlastCompletedStep: END");

		return lastCompletedStep;
	}

	/**
	 * Method to validate languages entered by AEE
	 * 
	 * @param otherSpokenLanguage
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/enrollmententity/primarysite/checkLanguagesSpokenForSite")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public Boolean checkLanguagesSpoken(@RequestParam String otherSpokenLanguage) {

		LOGGER.info("checkLanguagesSpoken: START");

		List<String> countiesServedlist = new ArrayList<String>();
		String otherSpokenLanguageWithoutSpace = null;

		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			otherSpokenLanguageWithoutSpace = otherSpokenLanguage.trim();
			countiesServedlist = Arrays.asList(otherSpokenLanguageWithoutSpace.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesSpoken: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking entity spoken languages : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking spoken languages fo entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesSpoken: END");

		return true;

	}

	/**
	 * Method to validate languages entered by AEE
	 * 
	 * @param otherWrittenLanguage
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/enrollmententity/primarysite/checkLanguagesWrittenForSite")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public Boolean checkLanguagesWritten(@RequestParam String otherWrittenLanguage) {

		LOGGER.info("checkLanguagesWritten: START");

		LOGGER.info("spoken language validation start here");

		List<String> countiesServedlist = new ArrayList<String>();
		String otherWrittenLanguageWithoutSpace = null;
		
		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			otherWrittenLanguageWithoutSpace = otherWrittenLanguage.trim();
			countiesServedlist = Arrays.asList(otherWrittenLanguageWithoutSpace.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesWritten: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking entity written languages : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking written languages fo entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesWritten: END");

		return true;
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param otherSpokenLanguage
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/assister/registration/checkLanguagesSpokenForAsister")
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'ENTITY_VIEW_ASSISTER')")
	public Boolean checkLanguagesSpokenForAssister(@RequestParam String otherSpokenLanguage) {

		LOGGER.info("checkLanguagesSpokenForAssister: START");

		LOGGER.info("spoken language validation start here");

		List<String> countiesServedlist = new ArrayList<String>();

		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			String otherSpokenLang = otherSpokenLanguage.trim();
			countiesServedlist = Arrays.asList(otherSpokenLang.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesSpokenForAssister: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking spoken languages of entity: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking spoken languages of entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesSpokenForAssister: END");

		return true;
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param otherWrittenLanguage
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/assister/registration/checkLanguagesWrittenForAssister")
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'ENTITY_VIEW_ASSISTER')")
	public Boolean checkLanguagesWrittenForAssister(@RequestParam String otherWrittenLanguage) {

		LOGGER.info("checkLanguagesWrittenForAssister: START");

		LOGGER.info("spoken language validation start here");

		List<String> countiesServedlist = new ArrayList<String>();

		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			String otherWrittenLang = otherWrittenLanguage.trim();
			countiesServedlist = Arrays.asList(otherWrittenLang.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesWrittenForAssister: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking written languages for assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking written languages : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesWrittenForAssister: END");

		return true;
	}
	
	
	private String loadLanguages(){
		JsonArray languageJsonArr = null;
        LOGGER.info("getLanguageList : START");
        String jsonLangData = "";
	    List languageSpokenList = null;
	    
		try{ 
		    languageSpokenList = lookupService.populateLanguageNames("");
		    languageJsonArr = new JsonArray();
		    for(int i=0;i<languageSpokenList.size();i++)
		    {
		    	String lan = EntityUtils.initializeAndUnproxy(languageSpokenList.get(i));
		    	if(lan!=null){
		    		languageJsonArr.add(new JsonPrimitive(lan));
		    	}
		    }
		    jsonLangData = platformGson.toJson(languageJsonArr);
		}
		catch(Exception exception)
		{
			LOGGER.error("Exception occurred while loading Languages : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
        LOGGER.info("getLanguageList : END");
        return jsonLangData;
	}
	
	private void setPaymentType(Model model, PaymentMethods paymentMethodObj) {
		if (PaymentMethods.PaymentType.CHECK.toString().equalsIgnoreCase(paymentMethodObj.getPaymentType().toString())) {
			model.addAttribute("paymentType", "CHECK");
		} else if (PaymentMethods.PaymentType.EFT.toString().equalsIgnoreCase(paymentMethodObj.getPaymentType().toString())) {
			model.addAttribute("paymentType", "EFT");
		} else {
			model.addAttribute("paymentType", "NULL");
		}
	}

	private EnrollmentEntity getEnrollmentEntityByUserId()
			throws InvalidUserException {
		AccountUser user;
		int userId;
		EnrollmentEntity enrollmentEntityObj;
		user = userService.getLoggedInUser();
		userId = user.getId();
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(userId);
		LOGGER.info("Retrieving Enrollment Entity Detail REST Call Starts");
		String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
		LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
		EnrollmentEntityResponseDTO enrollmentEntityResponse;
		try {
			enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
		} catch ( Exception exception) {
			LOGGER.error("Exception occured in getEnrollmentEntityByUserId in ghix-web." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
		if (enrollmentEntityObj == null) {
			enrollmentEntityObj = new EnrollmentEntity();
			enrollmentEntityObj.setUser(user);
		}
		return enrollmentEntityObj;
	}  
	
	private EnrollmentEntity getEnrollmentEntityByUserId(HttpServletRequest request)
			throws InvalidUserException, JsonProcessingException, IOException {
		AccountUser user;
		int userId;
		EnrollmentEntity enrollmentEntityObj;
		user = userService.getLoggedInUser();
		userId = user.getId();
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(userId);
		LOGGER.info("Retrieving Enrollment Entity Detail REST Call Starts");
		String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
		LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
		EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
		enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
		if (enrollmentEntityObj == null) {
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				String recordId = (String)request.getSession().getAttribute(ENTITYRECORDID);
				LOGGER.info("Setting Entity Id in Request DTO : " + recordId);
				enrollmentEntityRequest.setUserId(user.getId());
				enrollmentEntityRequest.setRecordId(recordId);
	            
	            String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequest);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				enrollmentEntityObj = enrollmentEntityResponseDTO.getEnrollmentEntity();
				if(enrollmentEntityObj==null){
				     enrollmentEntityObj = new EnrollmentEntity();
				}
				enrollmentEntityObj.setUser(user);
			} else {
				enrollmentEntityObj = new EnrollmentEntity();
				enrollmentEntityObj.setUser(user);
			}
		}
	
		return enrollmentEntityObj;
	}  

	/**
	 * Checking for XSS Attack
	 * 
	 * @param paymentMethod
	 *            {@link PaymentMethods}
	 */
	private void checkXSSAttack(PaymentMethods paymentMethod) {
		if (paymentMethod.getFinancialInfo() != null) {
			paymentMethod.getFinancialInfo().setAddress1(
					XssHelper.stripXSS(paymentMethod.getFinancialInfo().getAddress1()));
			paymentMethod.getFinancialInfo().setAddress2(
					XssHelper.stripXSS(paymentMethod.getFinancialInfo().getAddress2()));
			paymentMethod.getFinancialInfo().setCity(XssHelper.stripXSS(paymentMethod.getFinancialInfo().getCity()));
			paymentMethod.getFinancialInfo().setState(XssHelper.stripXSS(paymentMethod.getFinancialInfo().getState()));
			paymentMethod.getFinancialInfo().setZipcode(
					XssHelper.stripXSS(paymentMethod.getFinancialInfo().getZipcode()));

			BankInfo bankInfo = paymentMethod.getFinancialInfo().getBankInfo();
			if (bankInfo != null) {
				bankInfo.setAccountNumber(XssHelper.stripXSS(bankInfo.getAccountNumber()));
				bankInfo.setAccountType(XssHelper.stripXSS(bankInfo.getAccountType()));
				bankInfo.setBankName(XssHelper.stripXSS(bankInfo.getBankName()));
				bankInfo.setNameOnAccount(XssHelper.stripXSS(bankInfo.getNameOnAccount()));
				bankInfo.setRoutingNumber(XssHelper.stripXSS(bankInfo.getRoutingNumber()));
			}
		}
	}	
	
	private EnrollmentEntity updateEnrollmentEntity(
			EnrollmentEntity enrollmentEntityObj) throws JsonProcessingException, IOException {
		EnrollmentEntity enrollmentEntity = enrollmentEntityObj;
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();

		//if ("1".equals(enrollmentEntity.getReceivedPayments())) {
		enrollmentEntityRequest.setModuleId(enrollmentEntity.getId());
		LOGGER.info("Deleting Payment Method by Module Id : Rest Call Starts");
		restClassCommunicator.deletePaymentMethod(enrollmentEntityRequest);
		LOGGER.info("Deleting Payment Method by Module Id : Rest Call Ends");

		enrollmentEntityRequest.setEnrollmentEntity(enrollmentEntity);

		LOGGER.info("Updating Enrollment Entity Received Payment : Rest Call Starts");
		String resp = restClassCommunicator.updateEnrollmentEntity(JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequest));
		LOGGER.info("Updating Enrollment Entity Received Payment : Rest Call Ends");
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO dto = reader.readValue(resp);
		enrollmentEntity = dto.getEnrollmentEntity();
		
		return enrollmentEntity;
	}	
	
	private PaymentMethods getEntityPaymentMethod(
			EnrollmentEntity enrollmentEntityObj) throws JsonProcessingException, IOException {
	
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
		enrollmentEntityRequestDTO.setEntityType(ModuleUserService.ENROLLMENTENTITY_MODULE);
		LOGGER.info("Retrieving Payment Detail REST Call Starts");
		String paymentMethodsStr = restClassCommunicator.retrievePaymentDetails(enrollmentEntityRequestDTO);
		LOGGER.info("Retrieving Payment Detail REST Call Ends");
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(paymentMethodsStr);
		return enrollmentEntityResponseDTO.getPaymentMethods();
	}	
	
	private EnrollmentEntity getEnrollmentEntityByModuleId(int entityId) throws JsonProcessingException, IOException {
		EnrollmentEntity enrollmentEntityObj;
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(entityId);

		String enrollmentEntityObjStr = restClassCommunicator
				.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);

		EnrollmentEntityResponseDTO enrollmentEntityResponse =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
		enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
		return enrollmentEntityObj;
	}	

	private void setLeftNavigationMenu(HttpServletRequest request,
			EnrollmentEntity enrollEntity) {
		Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
		navigationMenuMap.put("ENTITY_INFO", true);
		request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
		request.getSession().setAttribute("assisterEnrollmentEntity", enrollEntity);
		request.getSession().setAttribute(REGISTRATION_STATUS, enrollEntity.getRegistrationStatus());
		request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
				EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.ENTITY_INFO.toString()));
	}

	private void setCountiesServed(EnrollmentEntity enrollmentEntityData,
			String countiesServed, EnrollmentEntity enrollEntity) {
		if (countiesServed != null) {
			// Fix for issue HIX-16689: if countiesServed contains ','
			// at the end, remove it else keep as it is
			String countiesInput = stripCountiesServed(countiesServed);
			enrollmentEntityData.setCountiesServed(countiesInput);
			enrollEntity.setCountiesServed(countiesInput);
		}
	}

	private void setEducationalGrant(EnrollmentEntity enrollmentEntityData,
			String educationGrantReq, EnrollmentEntity enrollEntity) {
		// If Education Grant is set to N, reset any prior parameters
		if (educationGrantReq != null && NO.equals(educationGrantReq)) {
			enrollEntity.setGrantAwardAmount(null);
			enrollEntity.setGrantContractNo(null);
		} else {
			enrollEntity.setGrantAwardAmount(enrollmentEntityData.getGrantAwardAmount());
			enrollEntity.setGrantContractNo(enrollmentEntityData.getGrantContractNo());
		}
	}	
	
	/**
	 * @param countiesServed
	 * @return
	 */
	private String stripCountiesServed(String countiesServed) {
		String countiesInput = countiesServed.indexOf(',') != -1 ? countiesServed.lastIndexOf(',') == (countiesServed
				.length() - 1) ? countiesServed.substring(0, countiesServed.lastIndexOf(','))
				: countiesServed : countiesServed;
		LOGGER.debug("Counties Served:" + countiesInput);
		return countiesInput;
	}
	
	private List<SiteLocationHours> getSiteLocationHoursList(Site site,
			String siteLocationHoursString) {
		List<String> siteLocationHoursStringSplit;
		List<SiteLocationHours> siteLocationHoursList = new ArrayList<SiteLocationHours>();
		if (!EntityUtils.isEmpty(siteLocationHoursString)) {
			// Split siteLocation Hours coming as request Parameter by ~
			siteLocationHoursStringSplit = EntityUtils.splitStringValues(siteLocationHoursString, "~");

			for (Iterator<String> it = siteLocationHoursStringSplit.iterator(); it.hasNext();) {
				List<String> properties = null;
				properties = Arrays.asList(it.next().split(","));
				if(properties.size() != THREE){
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					//return siteLocationHoursList;
				}
				SiteLocationHours siteLocationHrs = new SiteLocationHours();
				siteLocationHrs.setSite(site);
				siteLocationHrs.setDay(properties.get(0));
				
				if("closed".equalsIgnoreCase(properties.get(1).trim()) ^ "closed".equalsIgnoreCase(properties.get(TWO).trim())){
					//One is closed and other is not closed
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				if(!"closed".equalsIgnoreCase(properties.get(1).trim())){
					DateFormat df= null;
					if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
						df= new SimpleDateFormat("HH:mm");
					}else{
						df= new SimpleDateFormat("hh:mm a");
					}
					Date fromDate;
					Date toDate;
					try {
						fromDate = df.parse(properties.get(1).trim());
						toDate=df.parse(properties.get(TWO).trim());
						if(fromDate.getTime()>=toDate.getTime()){
							//from date should be small
							throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
						}
					} catch (ParseException e) {
						//Given values are not as per required format.
						throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					}
				}

				
				siteLocationHrs.setFromTime(properties.get(1));
				if (properties.size() < THREE) {
					siteLocationHrs.setToTime(null);
				} else {
					siteLocationHrs.setToTime(properties.get(TWO));
				}
				siteLocationHoursList.add(siteLocationHrs);
			}
		}
		return siteLocationHoursList;
	}

	private void validationSiteLocationHours(SiteLocationHours siteLocationHrs) {
		boolean isValid = true;
		Date fromTime = null;
		Date toTime = null;
		SimpleDateFormat sdf = null;
		
		//site location hours should not be same
		if(EntityUtils.isEmpty(siteLocationHrs.getDay()) || EntityUtils.isEmpty(siteLocationHrs.getFromTime()) || EntityUtils.isEmpty(siteLocationHrs.getToTime())){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		
		if((siteLocationHrs.getFromTime().equals(siteLocationHrs.getToTime()) && !(CLOSED.equalsIgnoreCase(siteLocationHrs.getFromTime()) || CLOSED.equalsIgnoreCase(siteLocationHrs.getToTime())))){
			isValid = false;
		}
		
		if((CLOSED.equalsIgnoreCase(siteLocationHrs.getFromTime()) && !CLOSED.equalsIgnoreCase(siteLocationHrs.getToTime())) || (!CLOSED.equalsIgnoreCase(siteLocationHrs.getFromTime()) && CLOSED.equalsIgnoreCase(siteLocationHrs.getToTime()))){
			isValid = false;
		}
		
		if(!(CLOSED.equalsIgnoreCase(siteLocationHrs.getFromTime()) || CLOSED.equalsIgnoreCase(siteLocationHrs.getToTime()))){
			//find if time is 12hr or 24 hr
			boolean is24HrTime = true;
			Pattern patterAmPm = Pattern.compile("(am|pm)"); 
			if(patterAmPm.matcher(siteLocationHrs.getFromTime()).find()){
				is24HrTime = false;
			}
	
			//create date formatter from  
			if(is24HrTime){
				sdf = new SimpleDateFormat("HH:mm");
			}else{
				sdf = new SimpleDateFormat("hh:mm a");
			}
			
			try {
				fromTime =  sdf.parse(siteLocationHrs.getFromTime());
				toTime =  sdf.parse(siteLocationHrs.getToTime());
			} catch (ParseException ex) {
				LOGGER.error("Error occured: Invalid site location hours, validation failed", ex);
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			
			if(fromTime.getTime() >= toTime.getTime()){
				isValid =  false;
			}
		}
		
		if(!isValid){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}

	private <E> boolean isNotEmpty(
			Map<String, E> map) {
		return map != null && !map.isEmpty();
	}
	
	/**
	 * @param <E>
	 * @param <F>
	 * @param <G>
	 * @param map
	 * @param lookupLanguages
	 * @return
	 */
	private <E extends PopulationData> PopulationData[] getLookUpData(
			Map<String, E> map,
			List<LookupValue> lookupLanguages, E e, int arraySize) {
		// Creating array of 4 objects to add other population languages
		PopulationData[] languages = new PopulationData[arraySize];
		List<E> associatedLanguages = extractLangListFromMap(map);

		// Creating list of languages available in lookup table
		List<String> langLookupValueList = new ArrayList<String>();
		for (LookupValue lookupLang : lookupLanguages) {
			langLookupValueList.add(lookupLang.getLookupValueLabel());
		}

		// If language other than lookup table is available, then add it into
		// array else add new object to avoid null pointer exception
		int i = 0;
		for (E lang : associatedLanguages) {
			if (i < arraySize) {
				if (!langLookupValueList.contains(lang.getValue())) {
					languages[i] = lang;
					i++;
				} else {
					languages[i] = e;
				}
			}
		}
		return languages;
	}
	
	/**
	 * Extracts Population Languages list associated with EE from languages map
	 * @param <E>
	 * 
	 * @param languagesMap
	 * @return {@link List} of {@link PopulationLanguage}
	 */
	private <E extends PopulationData> List<E> extractLangListFromMap(Map<String, E> languagesMap) {

		LOGGER.info("extractLangListFromMap: START");

		List<E> languages = new ArrayList<E>();
		if (languagesMap != null) {
			Set<String> langKeys = languagesMap.keySet();
			Iterator<String> langItr = langKeys.iterator();
			String langKey;
			E langValue;
			while (langItr.hasNext()) {
				langKey = langItr.next();
				langValue = languagesMap.get(langKey);
				languages.add(langValue);
			}
		}

		LOGGER.info("extractLangListFromMap: END");

		return languages;
	}	

	/**
	 * sets languages entered by entity in Others Textbox in Model
	 * 
	 * @param siteLanguages
	 * @param model
	 */
	private void setOtherLanguage(SiteLanguages siteLanguages, Model model) {

		LOGGER.info("setOtherLanguage: START");

		setLanguages(siteLanguages.getSpokenLanguages(), model, OTHER_SPOKEN_LANGUAGE);
		setLanguages(siteLanguages.getWrittenLanguages(), model, OTHER_WRITTEN_LANGUAGE);

		LOGGER.info("setOtherLanguage: END");
	}

	private void setLanguages(String languages, Model model, String otherWrittenLanguage) {
		String[] writtenLanguages;
		List<String> listOfNames;
		// Null check for written language to prevent Null Pointer Exception
		if (languages != null) {
			writtenLanguages = languages.split(",");
			listOfNames = getlistOfWrittenLanguageNames();
			StringBuilder strWritten = new StringBuilder();
			for (String language : writtenLanguages) {
				if (!"".equals(language) && !listOfNames.contains(language)) {
					if (strWritten.length() == 0) {
						strWritten.append(language);
					} else {
						strWritten.append(",");
						strWritten.append(language);
					}
				}
			}
			strWritten = strWritten.length() == 0 ? null : strWritten;
			model.addAttribute(otherWrittenLanguage, strWritten);
		}
	}
	
	/**
	 * Retrieves individuals details.
	 * 
	 * @param id
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/individualcase/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public String getIndividualCaseDetails(@PathVariable("id") String encryptedId, Model model) throws InvalidUserException{

		LOGGER.info("individual contact info page ");
		
		try {
			AccountUser user = userService.getLoggedInUser();
			int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
			model.addAttribute("page_title", "Getinsured Health Exchange: Individuals Details Page");				
			getIndividualDetails(id, model);
			model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
			model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			model.addAttribute("userActiveRoleName",userService.getDefaultRole(user).getName());
			
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while fetching individual detail : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching individuals detail : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
				
		return "individual/individualdetails";
	}
	
	/**
	 * Retrieves individual details and set it to model object
	 * 
	 * @param id
	 *            Individual Identification Number
	 * @param model
	 *            Model
	 */
	private void getIndividualDetails(Integer individualId, Model model) {
		
		LOGGER.info("getIndividualDetails : STARTS");		
		ConsumerComposite hhIndividual =  new ConsumerComposite(); 
		Integer startRecord = 0;
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
			EnrollmentEntity enrollmentEntity = null;
			try {
				enrollmentEntity = getEnrollmentEntityByUserId();
			} catch (InvalidUserException e) {
				LOGGER.error("Exception occured in fetching enrollment entity by userID", e);
				throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			Map<String, Object> map = new HashMap<>();
			Map<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ENTITY);
			searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(individualId));
			searchCriteria.put(BrokerConstants.MODULE_ID, enrollmentEntity.getId());
			searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
			map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
			List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
			Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
			if(houseHoldMap!=null && !houseHoldMap.isEmpty()) {
				individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
				
				hhIndividual = individualList.get(0);
			}	
		} else {
			populateIndividualDetails(hhIndividual, individualId);
		}
		
		model.addAttribute("individualUser", hhIndividual);		
		LOGGER.info("getIndividualDetails : ENDS");
	}
	
	private void populateIndividualDetails(ConsumerComposite hhIndividual, Integer individualId){
		ExternalIndividual externalIndividual = externalIndividualService.findByExternalIndividualID(individualId);
		
		if(externalIndividual!=null){
			hhIndividual.setId(externalIndividual.getId());
			hhIndividual.setFirstName(externalIndividual.getFirstName());
			hhIndividual.setLastName(externalIndividual.getLastName());
			hhIndividual.setEmailAddress(externalIndividual.getEmail());
			hhIndividual.setPhoneNumber(String.valueOf(externalIndividual.getPhone()));
			hhIndividual.setAddress1(externalIndividual.getAddress1());
			hhIndividual.setAddress2(externalIndividual.getAddress2());
			hhIndividual.setCity(externalIndividual.getCity());
			hhIndividual.setState(externalIndividual.getState());
			hhIndividual.setZip(externalIndividual.getZip());
			hhIndividual.setEligibilityStatus(externalIndividual.getEligibilityStatus());
		}
	}
	
	private List<Site> getListOfEnrollmentEntitySitesBySiteType(EnrollmentEntity enrollmentEntity, String siteType) {

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		enrollmentEntityRequestDTO.setSiteType(siteType);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Starts : EnrollmentEntityRequestDTO in Request : "
					+ enrollmentEntityRequestDTO);
			String listOfSitesResponse = restClassCommunicator.getListOfEnrollmentEntitySitesBySiteType(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			Validate.notNull(enrollmentEntityResponseDTO);
			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Enrollment Entity List of Sites : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: END");

		return enrollmentEntityResponseDTO.getSiteList();
	}
	
	/**
	 * Handles the re-assign individuals to councelors.
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/enrollmententity/reassignindividualsToCEC", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String reassignIndividualsToCEC(Model model, HttpServletRequest request,
			@RequestParam(value = SELECTEDCEC, required = false) String selectedCEC) throws InvalidUserException, GIException
	{

		LOGGER.info("reassignIndividualsToCEC: START");

		model.addAttribute(PAGE_TITLE, IND_PAGETITLE);

		LOGGER.info("reassignIndividualsToCEC: END");
		
		try {
			AccountUser user = userService.getLoggedInUser();
			
			EnrollmentEntity enrollmentEntity =  getEnrollmentEntityByUserId();
			List<Integer> individualIdsList = new ArrayList<Integer>();		
			individualIdsList = (List<Integer>)request.getSession().getAttribute(INDLIST);
			
			AssisterRequestDTO assisterRequestDTO =  new AssisterRequestDTO();
			assisterRequestDTO.setListOfIndividualsToReassign(individualIdsList);
			assisterRequestDTO.setEsignName(user.getFirstName() +" "+user.getLastName());
			assisterRequestDTO.setId(Integer.parseInt(selectedCEC));
			assisterRequestDTO.setEnrollmentEntityId(enrollmentEntity.getId());
			
			
			// get enrollment entity details
			String entityResponseDTOStr  = restClassCommunicator.reassignIndividuals(assisterRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);
			
			EntityResponseDTO entityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EntityResponseDTO.class).readValue(entityResponseDTOStr);
			int count = entityResponseDTO.getCount();
			
			//Get the name of CEC to whom Individual is transfereed
			
			String assisterObjStr = null;
//			// Retrieving Assister Details
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setId(Integer.valueOf(selectedCEC));
			assisterObjStr = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
//			
			AssisterResponseDTO assisterResponse = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterObjStr);
			String cecName = assisterResponse.getAssister().getFirstName() + " " + assisterResponse.getAssister().getLastName();
			
			return new Integer(count)+","+cecName;
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while re-assigning individuals to Assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while re-assignement of individuals to Assister : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	/**
	 * Handles the re-assign individuals to councellors.
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/enrollmententity/getindividualstoassign", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String getIndividuals(Model model, HttpServletRequest request,
			@RequestParam(value = INDLIST, required = false) String indIDList) throws InvalidUserException, GIException {

		LOGGER.info("reassignIndividuals: START");

		model.addAttribute(PAGE_TITLE, IND_PAGETITLE);

		LOGGER.info("reassignIndividuals: END");

		if(indIDList != null) 
		{
			String indIDJson = StringEscapeUtils.unescapeHtml(indIDList);
			List<Integer> individualIdsList = new ArrayList<Integer>(); 
			indIDJson = indIDJson.replaceAll(" ", "");		
			if(indIDJson.indexOf(',') > -1) {
				
				String indIds[] = indIDJson.split(",");
				
				for(String indId : indIds) {
					
					if(indId.trim().length() > 0 && StringUtils.isNumeric(indId) && Integer.parseInt(indId) > 0) {
						individualIdsList.add(Integer.parseInt(indId));
					}
				}				
			}
			request.getSession().setAttribute(INDLIST, individualIdsList);			
		}
		return "success";
	}
	
	
	
	/**
	 * Handles the re-assign individuals to councellors.
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/enrollmententity/reassignIndividuals/{showCECtable}",  method = RequestMethod.GET)
	public String reassignIndividuals(Model model, @PathVariable(SHOWCECTABLE)String showCECTable)  {

		LOGGER.info("reassignIndividuals: START");		
		model.addAttribute(PAGE_TITLE, IND_PAGETITLE);

		LOGGER.info("reassignIndividuals: END");
		try {					
			model.addAttribute(SHOWCECTABLE,showCECTable);
			EnrollmentEntity enrollmentEntity =  getEnrollmentEntityByUserId();
			List<Site> listOfPrimarySites = new ArrayList<Site>();	
			List<Site> listOfSubSites = new ArrayList<Site>();
			listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
					Site.site_type.PRIMARY.toString());
			
			listOfSubSites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
					Site.site_type.SUBSITE.toString());
			
			if(listOfSubSites!=null){
				listOfPrimarySites.addAll(listOfSubSites);
			}
			
			model.addAttribute(LIST_OF_PRIMARY_SITES, listOfPrimarySites);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while re-assigning individuals to Assister in reassignIndividuals: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while re-assigning individuals to Assister : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return "entity/enrollmententity/reassignIndividuals";
	}
		
	/**
	 * Retrieves list of designated individuals
	 * 
	 * @param cecEmailAddress
	 * @param primarySite
	 * @param cecName
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/enrollmententity/reassignIndividuals", method = { RequestMethod.GET, RequestMethod.POST })
	public String getCEC(@RequestParam(value = CEC_EMAIL_ADDRESS, required = false) String cecEmailAddress,
			@RequestParam(value = PRIMARY_SITE, required = false) String primarySite,
			@RequestParam(value = CEC_FIRST_OR_LAST_NAME, required = false) String cecName,
			Model model, HttpServletRequest request) {	
		
		LOGGER.info("EnrollmentEntityController.getIndividuals: START");	
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		Integer pageSize = PAGE_SIZE;
		Integer startRecord = 0;
		int currentPage = 1;
		String pageNumber = request.getParameter(PAGENUMBER);				
		
		if (pageNumber != null) {
			startRecord = (Integer.parseInt(pageNumber) - 1) * pageSize;
			currentPage = Integer.parseInt(pageNumber);
		}		
		request.setAttribute(PAGESIZE, pageSize);
		request.setAttribute(REQURI, REASSIGNINDIVIDUALS);	
		
		try {			
			EnrollmentEntity enrollmentEntity =  getEnrollmentEntityByUserId();
			List<Site> listOfPrimarySites = new ArrayList<Site>();
			List<Site> listOfSubSites = new ArrayList<Site>();
			listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,Site.site_type.PRIMARY.toString());
			model.addAttribute(LIST_OF_PRIMARY_SITES, listOfPrimarySites);		
			
			listOfSubSites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
					Site.site_type.SUBSITE.toString());
			
			if(listOfSubSites!=null){
				listOfPrimarySites.addAll(listOfSubSites);
			}
			
			AccountUser user = userService.getLoggedInUser();
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequest.setModuleId(user.getId());			
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// get enrollment entity details
			String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL);

			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			
			// populate search criteria								
			searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
			if ( (cecName != null) && (!"".equals(cecName)) ) {
				searchCriteria.put(BrokerConstants.CEC_FIRST_OR_LAST_NAME, cecName);
			}
			if ( (cecEmailAddress != null) && (!"".equals(cecEmailAddress)) ) {
				searchCriteria.put(CEC_EMAIL_ADDRESS, cecEmailAddress);
				
			}
			if ( (primarySite != null) && (!"".equals(primarySite)) ) {
				searchCriteria.put(PRIMARY_SITE, Integer.valueOf(primarySite));				
			}
			if (enrollmentEntityObj.getId() != 0) {
				searchCriteria.put(BrokerConstants.MODULE_ID, enrollmentEntityObj.getId());
			}
			searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ENTITY);
			enrollmentEntityRequest.setPageSize(pageSize);
			
			enrollmentEntityRequest.setSearchCriteria(searchCriteria);
			LOGGER.info("Calling ghix-entity to retrieve list of Assisters for Enrollment Entity");
			
			String assisterResponse = restClassCommunicator.retrieveListOfCEC(enrollmentEntityRequest);
			AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(assisterResponse);	
			List<Assister> assisterList = assisterResponseDTO.getListOfAssisters();
			int totalRecords = assisterResponseDTO.getTotalAssisters();
			
			if( assisterList != null && !assisterList.isEmpty() )
			{						
				model.addAttribute(CECLIST,assisterList);										
				model.addAttribute(RESULTSIZE, totalRecords);
				model.addAttribute(CURRENTPAGE, currentPage);	
				model.addAttribute(PAGESIZE, pageSize);			
			}
			model.addAttribute(SHOWCECTABLE,Y);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while searching Assisters : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while searching Assisters in getCEC : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}		
		LOGGER.info("EnrollmentEntityController.getIndividuals: END");		
		return "entity/enrollmententity/reassignIndividuals";
	}
	
	@RequestMapping(value = "/entity/enrollmententity/migratepaymentmethod", method = { RequestMethod.GET})
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String getMigratepaymentmethod() {
			
			return "entity/enrollmententity/migratepaymentmethod";
			
	}
	 	 	 	 		        
	@RequestMapping(value = "/entity/enrollmententity/migratepaymentmethod", method = { RequestMethod.POST})
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String doMigratepaymentmethod(@RequestParam(value = "pageSize", required = true) Integer pageSize, Model model) {
		LOGGER.info("do Migrate Paymentmethod : START");
		if(pageSize == null ||pageSize > GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT){
			pageSize = GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT;
		}
		List<PaymentMethodDTO> paymentMethodDTOList = null;
		try{
			paymentMethodDTOList = entityService.getPaymentMethodMigrationData(pageSize);
			if(paymentMethodDTOList == null || paymentMethodDTOList.isEmpty() ){
					LOGGER.info("No Data found for Migraction : START");
					model.addAttribute("MIGRATION_STATUS", "No Data found for Migraction");
					return "entity/enrollmententity/migratepaymentmethod";
			}
			
			PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
			paymentMethodRequestDTO.setPaymentMethodDTOList(paymentMethodDTOList);
			LOGGER.info("MIGRATE_PAYMENT_METHOD_PCI rest call : START");
			String paymentMethodMigractionResponse = restClassCommunicator.migrateEntityPaymentDetails(paymentMethodRequestDTO);
			LOGGER.info("MIGRATE_PAYMENT_METHOD_PCI rest call : END");
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodMigractionResponse);
			String migrationStatus = "";
 	 	 	Integer successRecordCount  = null;
 	 	 	Integer failRecordCount = null;
 	 	 	if(null != paymentMethodResponse) {
				if(GhixConstants.RESPONSE_SUCCESS.toString().equalsIgnoreCase(paymentMethodResponse.getStatus())){
					migrationStatus = MIGRACTION_SUCCESS;
					successRecordCount = paymentMethodDTOList.size();
					failRecordCount = 0;
				}else if(GhixConstants.RESPONSE_PARTIAL_SUCCESS.toString().equalsIgnoreCase(paymentMethodResponse.getStatus())){
					migrationStatus = paymentMethodResponse.getStatus();
					model.addAttribute(MIGRATION_FAILURE_MESSAGE, paymentMethodResponse.getErrMsg());
					successRecordCount  = migrationSuccessRecords(paymentMethodResponse.getPaymentMethodResponseList(),paymentMethodDTOList);
					failRecordCount = paymentMethodDTOList.size() - successRecordCount;
					model.addAttribute(FAILURE_RESPONSE_LIST, paymentMethodResponse.getPaymentMethodResponseList());
				}else{
					migrationStatus = paymentMethodResponse.getStatus();
					model.addAttribute(MIGRATION_FAILURE_MESSAGE, paymentMethodResponse.getErrMsg());
					successRecordCount  = migrationSuccessRecords(paymentMethodResponse.getPaymentMethodResponseList(),paymentMethodDTOList);
					failRecordCount = paymentMethodDTOList.size() - successRecordCount;
					model.addAttribute(FAILURE_RESPONSE_LIST, paymentMethodResponse.getPaymentMethodResponseList());
	
				}
 	 	 	}
		model.addAttribute(MIGRATION_STATUS, migrationStatus);
		model.addAttribute(RECORDS_FOR_MIGRATION, paymentMethodDTOList.size());
		model.addAttribute(SUCCESS_RECORD_COUNT, successRecordCount);
		model.addAttribute(FAIL_RECORD_COUNT, failRecordCount);
				
		}catch(Exception exception){
			LOGGER.error("Exception occured while migratepaymentmethod ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("doMigratepaymentmethod : END");
		return "entity/enrollmententity/paymentMethodMigrationResponse";
	}
	
	private Integer migrationSuccessRecords(
			List<Map<Integer, Object>> paymentMethodResponseList,
			List<PaymentMethodDTO> paymentMethodDTOList) {
	if (paymentMethodResponseList != null
					&& !paymentMethodResponseList.isEmpty()) {
			Map<Integer, Object> exceptionMap = paymentMethodResponseList
							.get(0);
			if (exceptionMap != null && !exceptionMap.isEmpty()) {
					return paymentMethodDTOList.size() - exceptionMap.size();
			}
	}

	return 0;

}
	
	private List<ConsumerComposite> getConsumerCompositList(List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO) throws GIException {
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");//BrokerConstants.DATE_FORMAT  TODO: Need to get confirmation from AHBX about date format.
		ConsumerComposite tmpConsumerComposite=null;
		
		if(brokerBOBDetailsDTO!=null){
			for(BrokerBOBDetailsDTO tmpBrokerBOBDetailsDTO:brokerBOBDetailsDTO){
				tmpConsumerComposite= new ConsumerComposite();
				
				tmpConsumerComposite.setId(tmpBrokerBOBDetailsDTO.getIndividualId());
				
				tmpConsumerComposite.setFirstName(tmpBrokerBOBDetailsDTO.getFirstName());
				tmpConsumerComposite.setLastName(tmpBrokerBOBDetailsDTO.getLastName());
				
				tmpConsumerComposite.setEmailAddress(tmpBrokerBOBDetailsDTO.getEmailAddress());
				tmpConsumerComposite.setPhoneNumber(tmpBrokerBOBDetailsDTO.getPhoneNumber());
				
				tmpConsumerComposite.setAddress1(tmpBrokerBOBDetailsDTO.getAddress1());
				tmpConsumerComposite.setAddress2(tmpBrokerBOBDetailsDTO.getAddress2());
				tmpConsumerComposite.setCity(tmpBrokerBOBDetailsDTO.getCity());
				tmpConsumerComposite.setState(tmpBrokerBOBDetailsDTO.getState());
				tmpConsumerComposite.setZip(tmpBrokerBOBDetailsDTO.getZipCode());
				
				tmpConsumerComposite.setEnrollmentStatus(tmpBrokerBOBDetailsDTO.getEnrollmentStatus());
				tmpConsumerComposite.setCurrentStatus(tmpBrokerBOBDetailsDTO.getCurrentStatus());
				
				tmpConsumerComposite.setFamilySize(tmpBrokerBOBDetailsDTO.getNoOfHouseholdMembers()); 
				
				if(null != tmpBrokerBOBDetailsDTO.getRequestSentDt() && !tmpBrokerBOBDetailsDTO.getRequestSentDt().isEmpty()){
					try {
						tmpConsumerComposite.setRequestSent(  sdf.parse(tmpBrokerBOBDetailsDTO.getRequestSentDt())  );
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing RequestSentDt from IND72 response: ", e);
					} 				
				}
				if(null!= tmpBrokerBOBDetailsDTO.getActiveSinceFrmDt() && !tmpBrokerBOBDetailsDTO.getActiveSinceFrmDt().isEmpty() ){
					try {
						tmpConsumerComposite.setActiveSince(sdf.parse(tmpBrokerBOBDetailsDTO.getActiveSinceFrmDt()));
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing ActiveSince from IND72 response: ", e);
					}
				}
				if(null!= tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt() && !tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt().isEmpty() ){
					try {
						tmpConsumerComposite.setInActiveSince(sdf.parse(tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt()));
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing InActiveSince from IND72 response: ", e);
					}
				}
				
				tmpConsumerComposite.setCecId(tmpBrokerBOBDetailsDTO.getCecId());
				tmpConsumerComposite.setCecFirstName(tmpBrokerBOBDetailsDTO.getEcFirstName());
				tmpConsumerComposite.setCecLastName(tmpBrokerBOBDetailsDTO.getEcLastName());
				
				houseHoldObjList.add(tmpConsumerComposite);
			}
		}
		
		return houseHoldObjList;
	}
	
	private DesignateAssister retrieveAssisterDesignationDetailByIndividualId(
			int individualId) throws GIException {
		DesignateAssister designateAssister = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setIndividualId(individualId);
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		LOGGER.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
		
		try {
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			
			if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){				
					throw new Exception();
			}	
			designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
			
			LOGGER.debug("REST call Ends to the GHIX-ENITY module.");
		} catch(Exception ex){
			LOGGER.error("Exception in retrieving designatedAssister details, REST response code is: "+
					((assisterDesignateResponseDTO!=null)?assisterDesignateResponseDTO.getResponseCode():"assisterDesignateResponseDTO is null"));
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return designateAssister;
	}
	
	/**
	 * Handles the Submit of Entity information
	 * 
	 */
	@RequestMapping(value = "/entity/enrollmententity/registerentity", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEntityInfo(Model model, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("saveEntityInfo : START");
		LOGGER.info("Post Entity Info");
		List<ObjectError> errorMessages = new ArrayList<ObjectError>();

		request.getSession().setAttribute(UPDATEPROFILE, FALSE);
	
		EnrollmentEntity enrollmentEntityObj = null;

		try {
			// check if logged in user is EnrollmentEntity and not EntityAdmin
			if (!isEntityAdmin()) {
				enrollmentEntityObj = getEnrollmentEntityByUserId();
			} else {
				// in case of entity admin logging in,retrieve enrollment entity
				// object from session
				//enrollmentEntityObj = (EnrollmentEntity) request.getSession().getAttribute(ENROLLMENTENTITY);
				String entityId = request.getParameter("entityId");
				
				String enrollEntityResponse= null;
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				enrollmentEntityObj = enrollmentEntityResponseDTO.getEnrollmentEntity();
				
			}

//			enrollmentEntityObj.setReceivedPayments(enrollmentEntity.getReceivedPayments());
 	 	 	
 	 		if ("Incomplete".equals(enrollmentEntityObj.getRegistrationStatus())) {
 	 			enrollmentEntityObj.setRegistrationStatus("Pending");
 	 			enrollmentEntityObj.setStatusChangeDate(new TSDate());
 			}
 	 	 	enrollmentEntityObj = updateEnrollmentEntity(enrollmentEntityObj);

			// Called to update the status from Incomplete to Pending

			// saving the PaymentMethods info into the payment_methods table
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
			navigationMenuMap.put(PAYMENT_INFO, true);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
			request.getSession().setAttribute(REGISTRATION_STATUS, enrollmentEntityObj.getRegistrationStatus());
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			
			LOGGER.info("saveEntityInfo : END");
			return "success";
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving entity information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving entity info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
}
