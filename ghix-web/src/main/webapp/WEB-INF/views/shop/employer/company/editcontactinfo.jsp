<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<style type="text/css">
input.btn.btn-small[type="submit"] {
    height: 22px;
}
</style>


<script type="text/javascript">
/* function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
} */


jQuery.validator.addMethod("phonecheck", function(value, element, param) {
phone1 = $("#phone1").val();
phone2 = $("#phone2").val();
phone3 = $("#phone3").val();
	var intPhone1= parseInt( phone1);
	if( 
			(phone1 == "" || phone2 == "" || phone3 == "")  || 
			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
		return false; 
	}else if(intPhone1<100){
		return false;
	} 
	
var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
var phone = $("#phone1").val()+ "-" +$("#phone2").val()+ "-" +$("#phone3").val();
$("#contactnumber").val(phone);	
var phoneno = /^\d{10}$/;
  if(phonenumber.match(phoneno))      {
    return true;
  }
return false;
});



function validateForm(){
	$("#frmeditcontact").submit();
}

</script>
<!-- HIX-20763-->
<div class="gutter10">
	<div class="row-fluid">
		<div class="span3" id="sidebar"></div>	
		<!-- add id for skip side bar -->	
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmeditcontact" name="frmeditcontact" action="editcontactinfo" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="contactNumber" id="contactnumber" value="">	
				<div class="header">
					 <h4 class="pull-left">Account Contact Information</h4>
					 	<span class="pull-right">
					 		<a href="/hix/shop/employer/dashboard" title="Cancel" class="btn btn-small" role="button">Cancel</a>
					 		<input type="submit" name="submitBtn" id="submitBtn" onClick="javascript:validateForm();"  Value="<spring:message  code='label.save'/>" title="<spring:message  code='label.next'/>" class="btn btn-small btn-primary">					 		
					 	</span>
					 </div><br>
					<p>Use this page to edit your contact information. To transfer your account to someone else or to change your login email, please contact ${exchangeName} at ${exchangePhone}.</p>
					<div class="profile">
						<div class="control-group">
							<label for="contactFirstName" class="control-label required"><spring:message  code="label.emplFirstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">		
								<input type="text" name="contactFirstName" id="contactFirstName" value="${employerOBJ.contactFirstName}"  class="input-xlarge">
								<div id="contactFirstName_error"></div>								
							</div>
						</div>
						<div class="control-group">
							<label for="contactLastName" class="control-label required"><spring:message  code="label.emplLastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="contactLastName" id="contactLastName" value="${employerOBJ.contactLastName}"  class="input-xlarge">
								<div id="contactLastName_error"></div>					
							</div>
						</div>
						<div class="control-group">
							<c:choose>
								<c:when test="${employerOBJ.contactNumber != null && employerOBJ.contactNumber != ''}">
									<c:set var="phone" value="${employerOBJ.contactNumber}" />
									<c:set var="phoneParts" value="${fn:split(phone,'-')}" />
	
									<c:set var="phone1" value="${phoneParts[0]}" />
									<c:set var="phone2" value="${phoneParts[1]}" />
									<c:set var="phone3" value="${phoneParts[2]}" />
								</c:when>
							</c:choose>
							
							<label for="phone_bci" class="control-label required"><spring:message  code="label.employerphone"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="phone1" id="phone1" size="3" maxlength="3" class="input-mini" value="${phone1}">
								<input type="text" name="phone2" id="phone2" size="3" maxlength="3" class="input-mini" value="${phone2}">
								<input type="text" name="phone3" id="phone3" size="4" maxlength="4" class="input-mini" value="${phone3}">
								<div id="phone3_error"></div>
							</div>
						</div>
						<div class="control-group">
							<label for="contactEmail" class="control-label required"><spring:message  code="label.emplEmail"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" value="${employerOBJ.contactEmail}" name="contactEmail" id="contactEmail" class="input-xlarge" size="30" />
								<small class="block">Editing this will not change the email use to log in.</small>
								<div id="contactEmail_error"></div>			
							</div>
						</div>
						<div class="control-group">
							<label for="location" class="control-label required"><spring:message  code="label.employerworksiteaddr"/> </label>
							<div class="controls">
								<select name="location" id="location">
								</select>
								&nbsp;&nbsp;
								<small>
									<a href="javascript:void(0)" id="worksite_ModalLink" role="button" class="" data-toggle="modal">
									<i class="icon-plus-sign"></i>
									<spring:message  code="label.employeraddworksiteaddr"/></a>
								</small>
							</div>
						</div>
					</div>
					<!-- profile -->
					
					<input type="hidden" name="jsonStateValue" id="jsonStateValue" value='${doctorsJSON}' />
					<input type="hidden" name="defaultStateValue" id="defaultStateValue" value='${defaultStateCode}' />
					<input type="hidden" name="employerId" id="employerId" value="${employerOBJ.id}"/>
			</form>
		</div>
	</div>
</div>




<script type="text/javascript">
	var empDetailsJSON = ${empDetailsJSON};
	var contactlocationid = ${contactLocationId},
		empid = '${employerOBJ.id}';
	
	populateLocations(empDetailsJSON, contactlocationid);
	
	
	function populateLocations(empDetailsJSON, contactlocationid){
		application.populate_location_on_parent_page(empDetailsJSON, contactlocationid,'LOCATIONID');
	}

		
	function validateLocation(){
		if( $("#frmworksites").validate().form() ) {
			$.ajax(
					{
		        		type: "POST",
		       			url: "<c:url value='/shop/employer/company/addWorksites'/>",
		        		data: $("#frmworksites").serialize(),
		        		dataType:'json',
		        		success: function(response,xhr){
							if(isInvalidCSRFToken(xhr))	    				
								return;
		        			populateLocations(response);
		        			application.remove_modal();
		        		},
		       			error: function(requestObject, error, errorThrown){
		        			
		        		}
		        });
		}
	}
	
	
	var validator = $("#frmeditcontact").validate({ 
		rules : {
			contactFirstName : {
				required : true
			},
			contactLastName : {
				required : true
			},
			phone3 : {phonecheck : true},
			contactEmail : { required : true, email: true}
		},
		messages : {
			contactFirstName : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>"
			},
			contactLastName : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>"
			},
			phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"},
			contactEmail : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>", 
				email: "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
	function getCountyList(eIndex, zip, county) {
		var subUrl =  '<c:url value="/shop/employer/company/addCounties"/>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
				if(isInvalidCSRFToken(xhr))	    				
					return;
				populateCounties(response, eIndex, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, eIndex,county){
		
		$('#county'+eIndex).html('');
		var optionsstring = '<option value="">Select County...</option>';
		var i =0;
		$.each(response, function(key, value) {
			var optionVal = key+'#'+value;
			var selected = (county == key) ? 'selected' : '';
			var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$('#county'+eIndex).html(optionsstring);
	}
	
	$(document).ready(function(){
		$(document).on('onGetCountyList',getCountyList);
	});
	
</script>
<!-- PAGE FUNCTION -->