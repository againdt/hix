package com.getinsured.hix.indportal.dto;

public class HouseholdEligibilityDetails {

	private String eligibleProgram;
	private String eligibilityStatus;
	private String aptc;
	private String stateSubsidy;
	private String csr;
	private boolean isAptcEligible;
	private boolean isCsrEligible;
	private boolean isMedicaidEligible;
	private boolean enableAppealsSubmission;
	private boolean isEligibilityConditional;
	
	public String getEligibleProgram() {
		return eligibleProgram;
	}

	public void setEligibleProgram(String eligibleProgram) {
		this.eligibleProgram = eligibleProgram;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public boolean isAptcEligible() {
		return isAptcEligible;
	}

	public void setAptcEligible(boolean isAptcEligible) {
		this.isAptcEligible = isAptcEligible;
	}

	public boolean isCsrEligible() {
		return isCsrEligible;
	}

	public void setCsrEligible(boolean isCsrEligible) {
		this.isCsrEligible = isCsrEligible;
	}
	
	public boolean isMedicaidEligible() {
		return isMedicaidEligible;
	}

	public void setMedicaidEligible(boolean isMedicaidEligible) {
		this.isMedicaidEligible = isMedicaidEligible;
	}

	public String getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(String stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HouseholdEligibilityDetails [eligibleProgram=");
		builder.append(eligibleProgram);
		builder.append(", eligibilityStatus=");
		builder.append(eligibilityStatus);
		builder.append(", aptc=");
		builder.append(aptc);
		builder.append(", csr=");
		builder.append(csr);
		builder.append(", isAptcEligible=");
		builder.append(isAptcEligible);
		builder.append(", isCsrEligible=");
		builder.append(isCsrEligible);
		builder.append("]");
		return builder.toString();
	}

	public boolean isEnableAppealsSubmission() {
		return enableAppealsSubmission;
	}

	public void setEnableAppealsSubmission(boolean enableAppealsSubmission) {
		this.enableAppealsSubmission = enableAppealsSubmission;
	}

	public boolean isEligibilityConditional() {
		return isEligibilityConditional;
	}

	public void setEligibilityConditional(boolean isEligibilityConditional) {
		this.isEligibilityConditional = isEligibilityConditional;
	}
	
	

}
