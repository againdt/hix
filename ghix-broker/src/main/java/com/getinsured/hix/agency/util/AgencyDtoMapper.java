package com.getinsured.hix.agency.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.agency.AgencyAssistantApprovalDto;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusDto;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencyLocationDto;
import com.getinsured.hix.dto.agency.AgencyManagerProfileDto;
import com.getinsured.hix.dto.agency.AgencyProfileDto;
import com.getinsured.hix.dto.agency.AgencySiteDto;
import com.getinsured.hix.dto.agency.AgencySiteHourDto;
import com.getinsured.hix.dto.agency.AgencySiteLocationDto;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.model.agency.AgencySiteHour;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
public class AgencyDtoMapper {
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private ZipCodeService zipCodeService;
	
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	
	public <T> T convert(Class<T> type,Object source,Object... otherSources){
		T targetType = null;
		if(source == null || type == null){
			throw new GIRuntimeException("null args not supported");
		}//agency to AgencyInformationDto
		else if(source instanceof Agency && type.equals(AgencyInformationDto.class)){
			targetType = type.cast(toAgencyInformationDto((Agency) source));
		}//AgencyInformationDto to Agency
		else if(source instanceof AgencyInformationDto && type.equals(Agency.class)){
			targetType = type.cast(toAgency((AgencyInformationDto) source));
		}else if(source instanceof AgencySite && type.equals(AgencySiteDto.class)){
			targetType = type.cast(toAgencySiteDto((AgencySite) source));
		}else if(source instanceof AgencySiteDto && type.equals(AgencySite.class)){
			targetType = type.cast(toAgencySite((AgencySiteDto) source));
		}else if(source instanceof Location && type.equals(AgencyLocationDto.class)){
			targetType = type.cast(toAgencyLocationDto((Location) source));
		}else if(source instanceof AgencyLocationDto && type.equals(Location.class)){
			targetType = type.cast(toLocation((AgencyLocationDto) source));
		}else if(source instanceof AgencySiteHourDto && type.equals(AgencySiteHour.class)){
			targetType = type.cast(toAgencySiteHour((AgencySiteHourDto) source));
		}else if(source instanceof AgencySiteHour && type.equals(AgencySiteHourDto.class)){
			targetType = type.cast(toAgencySiteHourDto((AgencySiteHour) source));
		}else if(source instanceof Agency && otherSources !=null && otherSources.length >0 && otherSources[0] instanceof Broker && type.equals(AgencyProfileDto.class)){
			targetType = type.cast(toAgencyProfileDto((Agency) source, (Broker)  otherSources[0]));
		}else if(source instanceof Agency && otherSources.length==0 && type.equals(AgencyProfileDto.class)){
			targetType = type.cast(toAgencyProfileDto((Agency) source));
		}else if(source instanceof AgencyAssistant && type.equals(AgencyAssistantInfoDto.class)){
			targetType = type.cast(toAssistantInformationDto((AgencyAssistant) source));
		}else if(source instanceof AgencyAssistantInfoDto && type.equals(AgencyAssistant.class)){
			targetType = type.cast(toAgencyAssistant((AgencyAssistantInfoDto) source));
		}else if(source instanceof AgencyAssistant && type.equals(AgencyAssistantApprovalDto.class)){
			targetType = type.cast(toAssistantApprovalDto((AgencyAssistant) source));
		}else if(source instanceof AgencyAssistant && type.equals(AgencyAssistantStatusDto.class)){
			targetType = type.cast(toAssistantStatusDto((AgencyAssistant) source));
		}else if(source instanceof Broker && type.equals(AgencyManagerProfileDto.class)){
			targetType = type.cast(toAgencyManagerProfileDto((Broker) source));
		}
		else{
			throw new GIRuntimeException("Conversion not supported from "+source.getClass()+" to" +type.getClass().getName());
		}
		return targetType;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> convertCollection(Class<T> type,Collection<?> source){
		//This test could be skipped if you trust the callers, but it wouldn't be safe then.
	    if(source!=null && !source.isEmpty() ) {
	    	Class<?> collectionClass =  source.iterator().next().getClass();
	    	if(collectionClass.equals(AgencySiteDto.class) && type.equals(AgencySite.class)){
	    			return (List<T>) toAgencySites((Collection<AgencySiteDto>)source);
	    	}else if(collectionClass.equals(AgencySite.class) && type.equals(AgencySiteDto.class)){
    			return (List<T>) toAgencySiteDtos((Collection<AgencySite>)source);
	    	}else if(collectionClass.equals(AgencySite.class) && type.equals(AgencySiteLocationDto.class)){
				return (List<T>) toAgencySiteLocationDtos((Collection<AgencySite>)source);
	    	}
	    	else{
		    	throw new GIRuntimeException("Conversion not supported from "+source.getClass()+" to" +type.getClass().getName());
		    }
	    	
	    }
		
		return null;
	}
	
	private AgencyManagerProfileDto toAgencyManagerProfileDto(Broker broker){
		if(broker == null){
			return null;
		}
		AgencyManagerProfileDto agencyManagerProfileDto = new AgencyManagerProfileDto();
		agencyManagerProfileDto.setBrokerId(String.valueOf(broker.getId()));
		agencyManagerProfileDto.setAgencyContactEmailId(broker.getYourPublicEmail());
		agencyManagerProfileDto.setAgencyContactPhoneNumber(broker.getContactNumber());
		agencyManagerProfileDto.setAgencyManagerFirstName(broker.getUser() !=null?broker.getUser().getFirstName():broker.getFirstName());
		agencyManagerProfileDto.setAgencyManagerLastName(broker.getUser() !=null?broker.getUser().getLastName():broker.getLastName());
		agencyManagerProfileDto.setAgencyManager(true);
		return agencyManagerProfileDto;
	}
	
	private List<AgencySite> toAgencySites(Collection<AgencySiteDto> agencySiteDtos) {
		List<AgencySite> agencySites = new ArrayList<AgencySite>();
		for(AgencySiteDto agencySiteDto : agencySiteDtos){
			agencySites.add(toAgencySite(agencySiteDto));
		}
		return agencySites;
		
	}
	
	private List<AgencySiteDto> toAgencySiteDtos(Collection<AgencySite> agencySites) {
		List<AgencySiteDto> agencySiteDtos = new ArrayList<AgencySiteDto>();
		for(AgencySite agencySite : agencySites){
			agencySiteDtos.add(toAgencySiteDto(agencySite));
		}
		return agencySiteDtos;
		
	}

	private AgencyInformationDto toAgencyInformationDto(Agency agency){
		AgencyInformationDto agencyInformationDto = new AgencyInformationDto();
		agencyInformationDto.setAgencyName(agency.getAgencyName());
		agencyInformationDto.setBusinessLegalName(agency.getBusinessLegalName());
		agencyInformationDto.setCertificationStatus(agency.getCertificationStatus());
		agencyInformationDto.setFederalTaxId(agency.getFederalTaxId());
		agencyInformationDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agency.getId())));
		agencyInformationDto.setLicenseNumber(agency.getLicenseNumber());
		return agencyInformationDto;
	}
	
	private Agency toAgency(AgencyInformationDto agencyInformationDto){
		Agency agency = new Agency();
		agency.setAgencyName(agencyInformationDto.getAgencyName());
		agency.setBusinessLegalName(agencyInformationDto.getBusinessLegalName());
		agency.setCertificationStatus(agencyInformationDto.getCertificationStatus());
		agency.setFederalTaxId(agencyInformationDto.getFederalTaxId());
		if(agencyInformationDto.getId() != null && !agencyInformationDto.getId().trim().isEmpty()){
			agency.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyInformationDto.getId())));
		}
		agency.setLicenseNumber(agencyInformationDto.getLicenseNumber());
		return agency;
	}
	
	private AgencySiteDto toAgencySiteDto(AgencySite agencySite){
		AgencySiteDto agencySiteDto = new AgencySiteDto();
		if(agencySite.getAgency() != null ){
			if(agencySite.getAgency().getId() !=null){
				agencySiteDto.setAgencyId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencySite.getAgency().getId())));
			}
			if(agencySite.getAgency().getCertificationStatus() !=null){
				agencySiteDto.setCertificationStatus(agencySite.getAgency().getCertificationStatus().getValue());
			}
		}
		
		agencySiteDto.setEmailAddress(agencySite.getEmailAddress());
		agencySiteDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencySite.getId())));
		agencySiteDto.setLocation(toAgencyLocationDto(agencySite.getLocation()));
		agencySiteDto.setPhoneNumber(agencySite.getPhoneNumber());
		agencySiteDto.setSiteLocationHours(toAgencySiteHourDtos(agencySite.getAgencySiteHours()));
		agencySiteDto.setSiteLocationName(agencySite.getSiteLocationName());
		agencySiteDto.setSiteType(agencySite.getSiteType());
		return agencySiteDto;
	}
	
	
	private AgencySite toAgencySite(AgencySiteDto agencySiteDto){
		AgencySite agencySite = new AgencySite();
		if(agencySiteDto.getAgencyId() !=null && !agencySiteDto.getAgencyId().trim().isEmpty()){
			Agency agency = new Agency();
			agency.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencySiteDto.getAgencyId())));
			agencySite.setAgency(agency);
		}
		agencySite.setEmailAddress(agencySiteDto.getEmailAddress());
		if(agencySiteDto.getId() !=null && !agencySiteDto.getId().trim().isEmpty()){
			agencySite.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencySiteDto.getId())));
		}
		
		agencySite.setLocation(toLocation(agencySiteDto.getLocation()));
		agencySite.setPhoneNumber(agencySiteDto.getPhoneNumber());
		agencySite.setAgencySiteHours(toAgencySiteHours(agencySiteDto.getSiteLocationHours()));
		agencySite.setSiteLocationName(agencySiteDto.getSiteLocationName());
		agencySite.setSiteType(agencySiteDto.getSiteType());
		return agencySite;
	}
	
	
	
	
	private AgencyLocationDto toAgencyLocationDto(Location location){
		if(location == null){
			return null;
		}
		AgencyLocationDto agencyLocationDto = new AgencyLocationDto();
		if(location.getId() !=0 ){
			agencyLocationDto.setId(String.valueOf(location.getId()));
		}
		agencyLocationDto.setAddress1(location.getAddress1());
		agencyLocationDto.setAddress2(location.getAddress2());
		agencyLocationDto.setCity(location.getCity());
		
		agencyLocationDto.setLat(location.getLat());
		agencyLocationDto.setLon(location.getLon());
		agencyLocationDto.setState(location.getState());
		agencyLocationDto.setZip(location.getZip());
		return agencyLocationDto;
	}
	
	private Location toLocation(AgencyLocationDto agencyLocationDto){
		if(agencyLocationDto == null){
			return null;
		}
		Location location = new Location();
		location.setAddress1(agencyLocationDto.getAddress1());
		location.setAddress2(agencyLocationDto.getAddress2());
		location.setCity(agencyLocationDto.getCity());
		if(agencyLocationDto.getId() !=null && !agencyLocationDto.getId().trim().isEmpty()){ 
			location.setId(Integer.parseInt(agencyLocationDto.getId()));
		}
		//setting lattitude and longitude
		ZipCode zipCode=zipCodeService.findByZipCode(!StringUtils.isEmpty(agencyLocationDto.getZip()) ? agencyLocationDto.getZip() : null); 
		if(zipCode!=null){
			location.setLat(zipCode.getLat());
			location.setLon(zipCode.getLon());
		}
		location.setState(agencyLocationDto.getState());
		location.setZip(agencyLocationDto.getZip());
		return location;
	}
	
	private List<AgencySiteHourDto> toAgencySiteHourDtos(Collection<AgencySiteHour> agencySiteHours){
		if(agencySiteHours == null){
			return null;
		}
		List<AgencySiteHourDto> agencySiteHourDtos = new ArrayList<AgencySiteHourDto>();
		for(AgencySiteHour agencySiteHour : agencySiteHours){
			agencySiteHourDtos.add(toAgencySiteHourDto(agencySiteHour));
		}
		return agencySiteHourDtos;
	}
	
	private Set<AgencySiteHour> toAgencySiteHours(Collection<AgencySiteHourDto> agencySiteHourDtos){
		if(agencySiteHourDtos == null){
			return null;
		}
		Set<AgencySiteHour> agencySiteHours = new TreeSet<AgencySiteHour>();
		for(AgencySiteHourDto agencySiteHourDto : agencySiteHourDtos){
			agencySiteHours.add(toAgencySiteHour(agencySiteHourDto));
		}
		return agencySiteHours;
	}
	
	private AgencySiteHourDto toAgencySiteHourDto(AgencySiteHour agencySiteHour){
		if(agencySiteHour == null){
			return null;
		}
		AgencySiteHourDto agencySiteHourDto = new AgencySiteHourDto();
		agencySiteHourDto.setDay(agencySiteHour.getDay());
		agencySiteHourDto.setFromTime(agencySiteHour.getFromTime());
		if(agencySiteHour.getId() != null){
			agencySiteHourDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencySiteHour.getId())));
		}
		agencySiteHourDto.setToTime(agencySiteHour.getToTime());
		return agencySiteHourDto;
	}
	
	private AgencySiteHour toAgencySiteHour(AgencySiteHourDto agencySiteHourDto){
		if(agencySiteHourDto == null){
			return null;
		}
		AgencySiteHour agencySiteHour = new AgencySiteHour();
		agencySiteHour.setDay(agencySiteHourDto.getDay());
		agencySiteHour.setFromTime(agencySiteHourDto.getFromTime());
		if(agencySiteHourDto.getId() !=null && !agencySiteHourDto.getId().trim().isEmpty()){ 
			agencySiteHour.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt((agencySiteHourDto.getId()))));
		}
		agencySiteHour.setToTime(agencySiteHourDto.getToTime());
		return agencySiteHour;
	}
	
	
	private AgencyProfileDto toAgencyProfileDto(Agency agency, Broker broker){
		if(agency == null || broker == null){
			return null;
		}
		AgencyProfileDto agencyProfileDto = new AgencyProfileDto();
		agencyProfileDto.setAgencyBusinessLegalName(agency.getBusinessLegalName());
		agencyProfileDto.setAgencyName(agency.getAgencyName());
		
		return agencyProfileDto;
	}
	
	private AgencyProfileDto toAgencyProfileDto(Agency agency){
		if(agency == null){
			return null;
		}
		AgencyProfileDto agencyProfileDto = new AgencyProfileDto();
		agencyProfileDto.setAgencyBusinessLegalName(agency.getBusinessLegalName());
		agencyProfileDto.setAgencyName(agency.getAgencyName());
		return agencyProfileDto;
	}
	
	private AgencyAssistantInfoDto toAssistantInformationDto(AgencyAssistant assistant){
		AgencyAssistantInfoDto assistantInfoDto = new AgencyAssistantInfoDto();
		assistantInfoDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(assistant.getId())));
		if(assistant.getUser()!=null && assistant.getUser().getFirstName()!=null){
			assistantInfoDto.setFirstName(assistant.getUser().getFirstName());
		}else{
			assistantInfoDto.setFirstName(assistant.getFirstName());
		}
		if(assistant.getUser()!=null && assistant.getUser().getLastName()!=null){
			assistantInfoDto.setLastName(assistant.getUser().getLastName());
		}else{
			assistantInfoDto.setLastName(assistant.getLastName());
		}
		assistantInfoDto.setPrimaryContactNumber(assistant.getPrimaryContactNumber());
		assistantInfoDto.setBusinessContactNumber(assistant.getBusinessContactNumber());
		assistantInfoDto.setPersonalEmailAddress(assistant.getPersonalEmailAddress());
		assistantInfoDto.setBusinessEmailAddress(assistant.getBusinessEmailAddress());
		assistantInfoDto.setCommunicationPref(assistant.getCommunicationPreference());
		assistantInfoDto.setBusinessLegalName(assistant.getBusinessLegalName());
		assistantInfoDto.setBusinessAddress(toAgencyLocationDto(assistant.getBusinessAddress()));
		assistantInfoDto.setCorrespondenceAddress(toAgencyLocationDto(assistant.getCorrespondenceAddress()));
		assistantInfoDto.setAssistantRole(assistant.getAssistantRole());
		assistantInfoDto.setApprovalStatus(assistant.getApprovalStatus());
		assistantInfoDto.setStatus(assistant.getStatus());
		if(assistant.getAgency() != null && assistant.getAgency().getId() !=null){
			assistantInfoDto.setAgencyId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(assistant.getAgency().getId())));
		}
		return assistantInfoDto;
	}
	
	private AgencyAssistant toAgencyAssistant(AgencyAssistantInfoDto assistantInformationDto){
		AgencyAssistant assistant = new AgencyAssistant();
		if(assistantInformationDto.getId() != null){
			assistant.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(assistantInformationDto.getId())));
		}
		assistant.setFirstName(assistantInformationDto.getFirstName());
		assistant.setLastName(assistantInformationDto.getLastName());
		assistant.setPrimaryContactNumber(assistantInformationDto.getPrimaryContactNumber());
		assistant.setBusinessContactNumber(assistantInformationDto.getBusinessContactNumber());
		assistant.setPersonalEmailAddress(assistantInformationDto.getPersonalEmailAddress());
		assistant.setBusinessEmailAddress(assistantInformationDto.getBusinessEmailAddress());
		assistant.setCommunicationPreference(assistantInformationDto.getCommunicationPref());
		assistant.setBusinessLegalName(assistantInformationDto.getBusinessLegalName());
		assistant.setAssistantRole(assistantInformationDto.getAssistantRole());
		assistant.setCorrespondenceAddress(toLocation(assistantInformationDto.getCorrespondenceAddress()));
		if(assistantInformationDto.getAgencyId() !=null && !assistantInformationDto.getAgencyId().trim().isEmpty()){
			Agency agency = new Agency();
			agency.setId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(assistantInformationDto.getAgencyId())));
			assistant.setAgency(agency);
		}
		return assistant;
	}
	
	private AgencyAssistantApprovalDto toAssistantApprovalDto(AgencyAssistant assistant){
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		AgencyAssistantApprovalDto assistantApprovalDto = new AgencyAssistantApprovalDto();
		assistantApprovalDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(assistant.getId())));
		if(assistant.getCreationDate() !=null){
			assistantApprovalDto.setApplicationSubmissionDate(dateFormat.format(assistant.getCreationDate()));
		}
		assistantApprovalDto.setAssistantNumber(assistant.getAssistantNumber());
		if(assistant.getApprovalDate()!=null){
			assistantApprovalDto.setApprovalDate(dateFormat.format(assistant.getApprovalDate()));
		}
		if(assistant.getApprovalStatus() != null){
			assistantApprovalDto.setApprovalStatus(assistant.getApprovalStatus().getValue());
		}
		assistantApprovalDto.setStatus(assistant.getStatus());
		return assistantApprovalDto;
	}
	
	private AgencyAssistantStatusDto toAssistantStatusDto(AgencyAssistant assistant){
		AgencyAssistantStatusDto assistantStatusDto = new AgencyAssistantStatusDto();
		assistantStatusDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(assistant.getId())));
		assistantStatusDto.setStatus(assistant.getStatus());
		if(assistant.getApprovalStatus() != null){
			assistantStatusDto.setApprovalStatus(assistant.getApprovalStatus().getValue());
		}
		
		return assistantStatusDto;
	}
	
	private List<AgencySiteLocationDto> toAgencySiteLocationDtos(Collection<AgencySite> agencySites) {
		List<AgencySiteLocationDto> agencySiteLocDtos = new ArrayList<AgencySiteLocationDto>();
		for(AgencySite agencySite : agencySites){
			agencySiteLocDtos.add(toAgencySiteLocationDto(agencySite));
		}
		return agencySiteLocDtos;
		
	}
	
	private AgencySiteLocationDto toAgencySiteLocationDto(AgencySite agencySite){
		AgencySiteLocationDto agencySiteLocDto = new AgencySiteLocationDto();
		if(agencySite != null){
			agencySiteLocDto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencySite.getId())));
			agencySiteLocDto.setLocation(toAgencyLocationDto(agencySite.getLocation()));
			agencySiteLocDto.setSiteLocationName(agencySite.getSiteLocationName());
		}
		return agencySiteLocDto;
	}
	

}
