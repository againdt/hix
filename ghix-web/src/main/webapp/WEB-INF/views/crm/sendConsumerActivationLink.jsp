<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<input type="hidden" id="householdId" name="householdId" value="<encryptor:enc value='${param.householdId}'/>" />
<input type="hidden" id="householdId" name="householdId" value="<encryptor:enc value='${param.householdEmail}'/>" />
<input type="hidden" id="householdPhone" name="householdPhone" value="<encryptor:enc value='${param.householdPhone}'/>" /> 

<!-- Confirmation dialog  -->
<form name="activationDlgFrm" id="activationDlgFrm" method="POST"  action="<c:url value="/cap/consumer/sendActivationLink" />" novalidate="novalidate" class="form-horizontal noborder">
<div id="confirmActivationDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-header">
            <h4 class="margin0 pull-left">Send Activation Link</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button">x</button>
        </div>

  
  <div class="modal-body">
		<div class="control-group txt-center">
	    	<h4 class="margin0"> Send activation email ? </h4>
		</div>
	    <div class="control-group"
		    	<c:if test="${householdEmail != ''}" >
		    		style="display:none"
				</c:if>
	    	>
				<label for="email" class="required control-label"><spring:message code="label.userEmail" /> 
					<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="Required!" /> </label>
				<!-- end of label -->
				<div class="controls">
					<input type="email" name="email" id="email"	class="input-xlarge" size="30" value="${param.householdEmail}" />
					<div id="email_error"></div>
				</div>
				<!-- end of controls-->
			</div>
		<div class="control-group"
		    	<c:if test="${householdPhone != ''}" >
		    		style="display:none"
				</c:if>
	    	>
				<label for="email" class="required control-label"><spring:message code="label.columnHeaderPhone" /> 
					<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="Required!" /> </label>
				<!-- end of label -->
				<div class="controls">
					<input type="text" maxlength=10 minlength=10 name="phone" id="phone"	class="input-xlarge" size="30" value="${param.householdPhone}" />
					<div id="phone_error"></div>
				</div>
				<!-- end of controls-->
			</div>
  </div>
  <div class="modal-footer clearfix">
    <button class="btn pull-left" data-dismiss="modal" aria-hidden="true" ><spring:message code="label.cancelButton" /></button>
    <button class="btn btn-primary" onclick="sendActivationEmail(); return false;"><spring:message code="label.sendButton" /></button>
  </div>
  
  <div id="ajaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>
</form>

<script type="text/javascript">
var validator = $("#activationDlgFrm").validate({ 
	rules : {
		    email : { required : true, validEmailCheck : true },
		    phone:{required:true,validPhone:true}
	},
	messages : {
		email : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
			validEmailCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"
		},
		phone:{
			required:"<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>",
			validPhone : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"
		}
	},
	errorClass: "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		if($("#" + elementId + "_error").html() != null)
		{
			$("#" + elementId + "_error").html('');
		}
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error")
				.attr('class', 'error');
	}
	
	
});


jQuery.validator.addMethod("validEmailCheck", function(value, element, param) {
	password = $("#email").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (!emailReg.test(password)) {
		return false;
	} else {
		return true;
	}
	return false;
});
jQuery.validator.addMethod("validPhone", function(value, element, param) {
	phone = $("#phone").val();
	var phoneReg = /\d{10}/;
	if (!phoneReg.test(phone)) {
		return false;
	} else {
		return true;
	}
	return false;
});

function sendActivationEmail() {
	
	if($("#activationDlgFrm").valid()) {
		
		$("#ajaxloader").show();
		
		var householdId = $("#householdId").val();
		var email = $("#email").val();
		
		var validateUrl = '<c:url value="/cap/consumer/sendActivationLink"></c:url>';

		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				householdId : householdId,
				email : email,
				phone:$("#phone").val()
			},
			success: function(response,xhr) {
			    if(isInvalidCSRFToken(xhr))
			 	  return;
				$("#ajaxloader").hide();
				$("#confirmActivationDlg").modal('hide');
			},
			error : function(error) {
				$("#ajaxloader").hide();
				$("#confirmActivationDlg").modal('hide');
				alert("Unable to send an activation");				
			}
		});
	}

}
function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
	rv = true;
	}
	return rv;
}	


</script>

<!-- Confirmation dialog ends -->