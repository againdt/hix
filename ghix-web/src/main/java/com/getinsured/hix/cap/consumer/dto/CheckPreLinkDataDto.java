package com.getinsured.hix.cap.consumer.dto;

import com.getinsured.hix.cap.util.ValidationUtility;

public class CheckPreLinkDataDto {

	private String REF_LCE_CMR_ID;
	private String CMR_ENROLLED_FLG;
	private String REF_LCE_STATUS_FLG;
	
	
	public String getREF_LCE_CMR_ID() {
		return REF_LCE_CMR_ID;
	}
	public boolean isValid() {
		return
				!ValidationUtility.isNullOrEmpty(REF_LCE_CMR_ID)
				&& !ValidationUtility.isNullOrEmpty(CMR_ENROLLED_FLG)
				&& ValidationUtility.isValidPhoneNumber(REF_LCE_STATUS_FLG);
			
	}
	
	public void setREF_LCE_CMR_ID(String rEF_LCE_CMR_ID) {
		REF_LCE_CMR_ID = rEF_LCE_CMR_ID;
	}
	public String getCMR_ENROLLED_FLG() {
		return CMR_ENROLLED_FLG;
	}
	public void setCMR_ENROLLED_FLG(String cMR_ENROLLED_FLG) {
		CMR_ENROLLED_FLG = cMR_ENROLLED_FLG;
	}
	public String getREF_LCE_STATUS_FLG() {
		return REF_LCE_STATUS_FLG;
	}
	public void setREF_LCE_STATUS_FLG(String rEF_LCE_STATUS_FLG) {
		REF_LCE_STATUS_FLG = rEF_LCE_STATUS_FLG;
	}
	
}
