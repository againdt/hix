package com.getinsured.hix.batch.bulkusers.partitioning;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.hix.batch.bulkusers.common.CsvHugeFileSplit;
import com.getinsured.hix.batch.bulkusers.config.RestTemplateConfig;

public class BulkUsersPartitioners implements Partitioner {

	private static final Logger logger = LoggerFactory.getLogger(BulkUsersPartitioners.class);

	private CsvHugeFileSplit csvHugeFileSplit;
	private String bulkUsersCsvFilePath;
	private String bulkUsersFileDirPath;
	private String csvSplitFilesDirectory;
	private String csvJobsOutputFileNameExpr;
	private String csvSplitFileNameExpr;
	private RestTemplateConfig restTemplateConfig;
	private String msUsersRestAPIUrl;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		
		String exceptionAsString = "";
		Map<String, ExecutionContext> partitionData = new HashMap<String, ExecutionContext>();
		checkBulkUsersFileDirAndPermissions();
		checkUsermanagementServiceStatus();
		try {

			logger.info("Calling CsvHugeFileSplit.splitIntoCsvFiles() method.");

			csvHugeFileSplit.splitIntoCsvFiles();

			ArrayList<String> fileNames = null;

			fileNames = CommonUtil.getListOfFileNames(csvSplitFilesDirectory, csvSplitFileNameExpr);

			if (fileNames != null && fileNames.size() > 0) {
				for (int i = 0; i < fileNames.size(); i++) {

					ExecutionContext executionContext = new ExecutionContext();

					executionContext.putString("inputFileName", fileNames.get(i));
					String jobsOutputFileName = csvJobsOutputFileNameExpr + i + " " + CommonUtil.dateToString();
					executionContext.put("outputFileName", jobsOutputFileName);

					executionContext.putString("threadName", "[Thread - " + i + "]");
					partitionData.put("partition: " + i, executionContext);

					logger.info("Split CSV File Name : " + fileNames.get(i));
				}
			}

		} catch (IOException e) {
			exceptionAsString = CommonUtil.convertExceptionToString(e);
			logger.error("Exception Details : " + exceptionAsString);
		} catch (Exception e) {
			exceptionAsString = CommonUtil.convertExceptionToString(e);
			logger.error("Exception Details : " + exceptionAsString);
		}

		return partitionData;
	}

	

	public void checkUsermanagementServiceStatus() throws RuntimeException {
		try {
			restTemplateConfig.setRestApiTimeout(1);
			ResponseEntity<String> responseEntity = restTemplateConfig.restTemplate().getForEntity(msUsersRestAPIUrl,
					String.class);
			String response = responseEntity.getBody();
			if (response != null && response.contains("Bulk users creation service is up and running.")) {
				HttpStatus errorCode = responseEntity.getStatusCode();
			}
		} catch (RestClientException | KeyManagementException | UnrecoverableKeyException | NoSuchAlgorithmException
				| KeyStoreException | CertificateException | IOException e1) {
			e1.printStackTrace();
			throw new RuntimeException("\"Not able to connect ms-user-management service url "+msUsersRestAPIUrl+"\"", e1);
		}
	}
	
	public void checkBulkUsersFileDirAndPermissions() throws RuntimeException {
		String dirPaths []= "csvhugedatafile,csvsplitfiles,csvjobsoutput,csvconsolidateddata".split(",");
		List <String> errors = new ArrayList<>();
		boolean errorsFlag = false;
		for(int i=0;i<dirPaths.length;i++) {
			File fdir = new File(this.bulkUsersFileDirPath+File.separator+dirPaths[i]);
			
			if(fdir.isDirectory()) {
				if(Files.isWritable(fdir.toPath()) && Files.isReadable(fdir.toPath())) {
					logger.info(fdir.getAbsolutePath()+" directory is exists. Read/Write permissions are granted to folder.");
				}
				else {
					errorsFlag = true;
					errors.add("\""+fdir.getAbsolutePath()+" directory is exists. Please check Read/Write folder permissions.\"");
					logger.info(fdir.getAbsolutePath()+" directory is exists. Please check Read/Write folder permissions.");
				}
			}
			else {
				errorsFlag = true;
				errors.add("\""+fdir.getAbsolutePath()+" directory is not exists.\"");
				logger.info(fdir.getAbsolutePath()+" directory is not exists.");
			}
		}
		
		File file = new File(this.bulkUsersCsvFilePath);
		if(file.exists()) {
			logger.info("\n\n"+file.getAbsolutePath()+" file exists.");
		}
		else {
			errorsFlag = true;
			errors.add("\""+file.getAbsolutePath()+" file not exists.\"");
			logger.info(file.getAbsolutePath()+" file not exists.");
		}
		if(errorsFlag) {
			throw new RuntimeException("Failed to start bulk users process job. "+errors.toString());
		}
	}

	public CsvHugeFileSplit getCsvHugeFileSplit() {
		return csvHugeFileSplit;
	}

	public void setCsvHugeFileSplit(CsvHugeFileSplit csvHugeFileSplit) {
		this.csvHugeFileSplit = csvHugeFileSplit;
	}

	public String getCsvSplitFilesDirectory() {
		return csvSplitFilesDirectory;
	}

	public void setCsvSplitFilesDirectory(String csvSplitFilesDirectory) {
		this.csvSplitFilesDirectory = csvSplitFilesDirectory;
	}

	public String getCsvJobsOutputFileNameExpr() {
		return csvJobsOutputFileNameExpr;
	}

	public void setCsvJobsOutputFileNameExpr(String csvJobsOutputFileNameExpr) {
		this.csvJobsOutputFileNameExpr = csvJobsOutputFileNameExpr;
	}

	public String getCsvSplitFileNameExpr() {
		return csvSplitFileNameExpr;
	}

	public void setCsvSplitFileNameExpr(String csvSplitFileNameExpr) {
		this.csvSplitFileNameExpr = csvSplitFileNameExpr;
	}

	public String getBulkUsersFileDirPath() {
		return bulkUsersFileDirPath;
	}

	public void setBulkUsersFileDirPath(String bulkUsersFileDirPath) {
		this.bulkUsersFileDirPath = bulkUsersFileDirPath;
	}
	
	public String getBulkUsersCsvFilePath() {
		return bulkUsersCsvFilePath;
	}

	public void setBulkUsersCsvFilePath(String bulkUsersCsvFilePath) {
		this.bulkUsersCsvFilePath = bulkUsersCsvFilePath;
	}



	public String getMsUsersRestAPIUrl() {
		return msUsersRestAPIUrl;
	}

	public void setMsUsersRestAPIUrl(String msUsersRestAPIUrl) {
		this.msUsersRestAPIUrl = msUsersRestAPIUrl;
	}

	public RestTemplateConfig getRestTemplateConfig() {
		return restTemplateConfig;
	}

	public void setRestTemplateConfig(RestTemplateConfig restTemplateConfig) {
		this.restTemplateConfig = restTemplateConfig;
	}

}
