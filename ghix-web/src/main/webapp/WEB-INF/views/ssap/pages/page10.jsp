<%@ include file="../include.jsp" %>
<div id="appscr60" style="display: none;" >
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page10.tellUsAboutHousehold" text="Tell Us About your Household" /></h5>
		</div>
		
		<form id="appscr60Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
			</div>
			<div class="gutter10">
			<div id="houseHoldQuestion">	
				<div class="control-group">
					<fieldset>
					<legend>
						<spring:message code="ssap.does" text="Does" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page10.planToFile" text="plan to file a federal income tax return for"/>
						<span class='coverageYear'></span>?&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</legend>
					<label class="margin5-l radio" for="planToFileFTRIndicatorYes">
						<input type="radio" name="planToFileFTRIndicator" id="planToFileFTRIndicatorYes" onclick="planToFileYes()" value="yes" data-parsley-errors-container="#planToFileFTRIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>						
						<spring:message code="ssap.page10.planToFileYes" text="Yes" />
					</label>
					<label class="margin5-l radio" for="planToFileFTRIndicatorNo">
						<input type="radio" name="planToFileFTRIndicator" id="planToFileFTRIndicatorNo" onclick="planToFileNo()" value="no" />
						<spring:message code="ssap.page10.planToFileNo" text="No" />
					</label>
					<div id="planToFileFTRIndicatorYesErrorDiv"></div>
				</fieldset>
				</div>
			</div>
			
			<!-- <div id="appscrIsChildDiv"> -->
				<div class="control-group hide" id="marriedIndicatorDiv">
					<fieldset>
					<legend>
						<spring:message code="ssap.page10.is" text="Is" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page10.married" text="married?" />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</legend>
					<label class="margin5-l radio" for="marriedIndicatorYes">
						<input type="radio" name="marriedIndicator" id="marriedIndicatorYes" value="yes" onclick="isMarried()" data-parsley-errors-container="#marriedIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
						<spring:message code="ssap.page10.planToFileYes" text="Yes" />
					</label>
					<label class="margin5-l radio" for="marriedIndicatorNo">
						<input type="radio" name="marriedIndicator" id="marriedIndicatorNo" value="no" onclick="isNotMarried()" />
						<spring:message code="ssap.page10.planToFileNo" text="No" />
					</label>
					<div id="marriedIndicatorYesErrorDiv"></div>
				</fieldset>
				</div><!-- control-group -->
				
				<div id="householdSpouseMainDiv" class="hide">
					<div class="control-group" id="planOnFilingJointFTRDiv">
						<fieldset>
						<legend>
							<spring:message code="ssap.does" text="Does" />
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page10.planToFileWith" text="plan to file a joint federal income tax return with" />
							<strong class="onDependantHisHer"></strong>
							<spring:message code="ssap.page10.spouseFor" text="spouse for" />
							<span class='coverageYear'></span>?&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</legend>
						<label class="margin5-l radio" for="planOnFilingJointFTRIndicatorYes">
							<input type="radio"  name="planOnFilingJointFTRIndicator" id="planOnFilingJointFTRIndicatorYes" value="yes"  onclick="householdPayJointly()" data-parsley-errors-container="#planOnFilingJointFTRIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
							<spring:message code="ssap.page10.planToFileYes" text="Yes" />
						</label>
						<label class="margin5-l radio" for="planOnFilingJointFTRIndicatorNo">
							<input type="radio" name="planOnFilingJointFTRIndicator" id="planOnFilingJointFTRIndicatorNo" value="no" onclick="householdNotPayJointly()" />
							<spring:message code="ssap.page10.planToFileNo" text="No" />
						</label>
						<div id="planOnFilingJointFTRIndicatorYesErrorDiv"></div>
						</fieldset>
					</div><!-- control-group -->
					
					<div class="control-group">
						<p>
							<spring:message code="ssap.page10.whoIs" text="Who is" />  
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page10.isSpouse" text="'s spouse?" />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<div  id="householdSpouseDiv"></div>
						
						<div id="appscr60_spouseDiv" class="gutter10 hide">
							<div class="control-group">
								
								<label class="required control-label" for="spouseFirstName">
									<spring:message code="ssap.page31B.spouse" text="Spouse" />&nbsp;<spring:message code="ssap.firstName" text="First Name" />
									<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
									<span aria-label="Required!"></span>
								</label><!-- end of label -->
								<div class="controls">
									<input type="text" id="spouseFirstName" name="spouseFirstName" class="input-large" placeholder="<spring:message code='ssap.firstName' text='First Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>"/>
								</div><!-- end of controls-->
								
							</div><!-- control-group ends -->
														
							<div class="control-group">
								
								<label class="control-label" for="spouseMiddleName">
									<spring:message code="ssap.page31B.spouse" text="Spouse" />&nbsp;<spring:message code='ssap.middleName' text='Middle Name' />
								</label><!-- end of label -->
								<div class="controls">
									<input type="text" id="spouseMiddleName" name="spouseMiddleName" class="input-large" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
								</div><!-- end of controls-->
								
							</div><!-- control-group ends -->
							
							<div class="control-group">
								
								<label class="required control-label" for="spouseLastName">
									<spring:message code="ssap.page31B.spouse" text="Spouse" />&nbsp;<spring:message code='ssap.lastName' text='Last Name' />
									<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
									<span aria-label="Required!"></span>
								</label><!-- end of label -->
								<div class="controls">
									<input type="text" id="spouseLastName" name="spouseLastName" class="input-large" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>"/>
								</div><!-- end of controls-->
								
							</div><!-- control-group ends -->
							
							<div class="control-group">
								
								<label class="control-label" for="spouseGetSuffixSElection">
									<spring:message code="ssap.suffix" text="Suffix" />
								</label><!-- end of label -->
								<div class="controls">
									<select id="spouseGetSuffixSElection" class="input-large">
										<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
										<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
										<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
										<option value="III"><spring:message code="ssap.III" text="III" /></option>
										<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
									</select>
									<div class="help-inline" id="suffix_error"></div>
								</div><!-- end of controls-->
								
							</div><!-- control-group ends -->
							
							<div class="control-group">
								<label class="required control-label" for="spouseDOB">
									<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
									<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
									<span aria-label="Required!"></span>
								</label><!-- end of label -->
								<div class="controls">
									<div class="input-append date date-picker" data-date="">
										<input type="text" id="spouseDOB" name="spouseDOB" class="input-medium dob" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#spouseDOBErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.dobRequired'/>"/>
										<span  class="add-on dateSpan"><i class="icon-calendar"></i></span>
										<div id="spouseDOBErrorDiv"></div>
									</div>
								</div><!-- end of controls-->
							</div><!-- control-group ends -->
						</div><!-- appscr60_spouseDiv ends -->
					</div><!-- control-group -->
					
					<div class="control-group">
						<fieldset>
						<legend>
							<spring:message code="ssap.does" text="Does" />
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page10.liveWithSpouse" text="live with spouse?" />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</legend>
						<label class="margin5-l radio" for="liveWithSpouseIndicatorYes">
							<input type="radio" name="liveWithSpouseIndicator" id="liveWithSpouseIndicatorYes" value="yes" data-parsley-errors-container="#liveWithSpouseIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
							<spring:message code="ssap.page10.planToFileYes" text="Yes" />
						</label>
						<label class="margin5-l radio" for="liveWithSpouseIndicatorNo">
							<input type="radio" name="liveWithSpouseIndicator" id="liveWithSpouseIndicatorNo" value="no"  />
							<spring:message code="ssap.page10.planToFileNo" text="No" />
						</label>
						<div id="liveWithSpouseIndicatorYesErrorDiv"></div>
						</fieldset>
					</div><!-- control-group -->
					
				</div><!-- householdSpouseMainDiv ends -->
			<!-- </div> --><!-- appscrIsChildDiv ends -->
			
			
			
			<div class="control-group hide" id="willClaimDependents">
				<fieldset>
				<legend>
					<spring:message code="ssap.page10.will" text="Will" />
					<strong class="nameOfHouseHold"></strong>
					<span class="householdContactSpouseName"></span>
					<spring:message code="ssap.page10.claimDependents" text="claim any dependents on" />
					<strong class="onDependantHisHer"></strong>
					<spring:message code="ssap.page10.federalIncomeReturn" text="federal income tax return for" />
					<span class='coverageYear'></span>?&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<label class="margin5-l radio" for="householdHasDependantYes">
					<input type="radio" id="householdHasDependantYes"  name="householdHasDependant" onchange="changeDependentDivView60A()" value="yes" data-parsley-errors-container="#householdHasDependantYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
					<spring:message code="ssap.page10.planToFileYes" text="Yes" />
				</label>
				<label class="margin5-l radio" for="householdHasDependantNo">
					<input type="radio" name="householdHasDependant" id='householdHasDependantNo' onchange="changeDependentDivView60A()" value="no" />
					<spring:message code="ssap.page10.planToFileNo" text="No" />
				</label>
				<div id="householdHasDependantYesErrorDiv"></div>
				</fieldset>
			</div><!-- control-group and willClaimDependents ends -->
			
			<div class="control-group hide" id="householdDependentMainDiv">
				<div id="householdDependantDiv"></div>
			</div><!-- householdDependentMainDiv ends -->
			
			<div class="hide" id="dependentDetailDiv_inputFields">
				<div class="control-group">
					<label class="required control-label" for="dependentFirstName">
						<spring:message code="ssap.page31B.Dependent" text="Dependent" />&nbsp;<spring:message code="ssap.firstName" text="First Name" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="dependentFirstName" name="dependentFirstName" class="input-large" placeholder="<spring:message code='ssap.firstName' text='First Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="dependentMiddleName">
						<spring:message code="ssap.page31B.Dependent" text="Dependent" />&nbsp;<spring:message code='ssap.middleName' text='Middle Name' />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="dependentMiddleName" name="dependentMiddleName" class="input-large" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="required control-label" for="dependentLastName">
						<spring:message code="ssap.page31B.Dependent" text="Dependent" />&nbsp;<spring:message code='ssap.lastName' text='Last Name' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="dependentLastName" name="dependentLastName" class="input-large" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="dependentGetSuffixSElection">
						<spring:message code="ssap.suffix" text="Suffix" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="input-large" id="dependentGetSuffixSElection">
							<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
							<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
							<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
							<option value="III"><spring:message code="ssap.III" text="III" /></option>
							<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
						</select>
						<div class="help-inline" id="suffix1_error"></div>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="required control-label" for="dependentDOB">
						<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<div class="input-append date date-picker" data-date="">
							<input type="text" id="dependentDOB" name="dependentDOB" class="input-medium" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#dependentDOBErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.dobRequired'/>" />
							<span  class="add-on dateSpan"><i class="icon-calendar"></i></span>
							<div id="dependentDOBErrorDiv"></div>
						</div>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->				
			</div><!-- dependentDetailDiv_inputFields ends -->
			
			<div class="control-group hide" id="willBeClaimed">
				<fieldset>
				<legend>
					<spring:message code="ssap.page10.will" text="Will" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page10.beClaimed" text="be claimed as a dependent on someone else&#39;s federal income tax return for" />
					<span class="coverageYear"></span>?&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<label class="margin5-l radio" for="doesApplicantHasTaxFillerYes">
					<input type="radio"  id='doesApplicantHasTaxFillerYes' onchange="doesFilerHasTaxFiller()" value="yes" name="doesApplicantHasTaxFiller" data-parsley-errors-container="#doesApplicantHasTaxFillerYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
					<spring:message code="ssap.page10.planToFileYes" text="Yes" />
				</label>
				<label class="margin5-l radio" for="doesApplicantHasTaxFillerNo">
					<input type="radio" id='doesApplicantHasTaxFillerNo' onchange="doesFilerHasTaxFiller()" value="no" name="doesApplicantHasTaxFiller" />
					<spring:message code="ssap.page10.planToFileNo" text="No" />
				</label>
				<div id="doesApplicantHasTaxFillerYesErrorDiv"></div>
				</fieldset>
			</div><!-- control-group and willBeClaimed ends -->
			
			<div id="appscr60ClaimedFilerDiv" class="hide">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page10.whoIsTaxFiler" text="Who is the tax filer that will claim " />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page10.onTheirIncome" text="on their income tax return? If " />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page10.isClaimedBy" text="is claimed by a married couple filing a joint tax return, select either spouse below" />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<div id="householdFilerDiv" ></div>
				</div>
				<div class="hide"  id="appscr60_taxFilerDiv1">
					<div class="control-group">
						<label class="required control-label" for="taxFilerFirstName">
							<spring:message code="ssap.tax.filer" text="Tax Filer" />&nbsp;<spring:message code="ssap.firstName" text="First Name" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							<span aria-label="Required!"></span>
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="taxFilerFirstName" name="taxFilerFirstName" class="input-large" placeholder="<spring:message code='ssap.firstName' text='First Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="taxFilerMiddleName">
							<spring:message code="ssap.tax.filer" text="Tax Filer" />&nbsp;<spring:message code='ssap.middleName' text='Middle Name' />
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="taxFilerMiddleName" name="taxFilerMiddleName" class="input-large" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="required control-label" for="taxFilerLastName">
							<spring:message code="ssap.tax.filer" text="Tax Filer" />&nbsp;<spring:message code='ssap.lastName' text='Last Name' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							<span aria-label="Required!"></span>
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="taxFilerLastName" name="taxFilerLastName" class="input-large" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr60TaxFilerSuffix">
							<spring:message code="ssap.suffix" text="Suffix" />
						</label><!-- end of label -->
						<div class="controls">
							<select class="input-large" id="appscr60TaxFilerSuffix" class="appscr60TaxFilerSuffix">
								<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
								<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
								<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
								<option value="III"><spring:message code="ssap.III" text="III" /></option>
								<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
							</select>
							<div class="help-inline" id="suffix2_error"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="required control-label" for="taxFilerDob">
							<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							<span aria-label="Required!"></span>
						</label><!-- end of label -->
						<div class="controls">
							<div class="input-append date date-picker" data-date="">
								<input type="text" id="taxFilerDob" name="taxFilerDob" class="input-medium" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#taxFilerDobErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.dobRequired'/>"/>
								<span  class="add-on dateSpan"><i class="icon-calendar"></i></span>
								<div id="taxFilerDobErrorDiv"></div>
							</div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				</div><!-- appscr60_taxFilerDiv1 ends -->
			</div>
			</div>
		</form>
	</div>         
</div>

<div id="notSeekingCoverageAlert" class="modal hide fade" tabindex="-1" role="dialog">
  <div class="modal-header">
  </div>
  <div class="modal-body">
    <p class="hide PCNotEligible"><spring:message code="ssap.notSeekingCoverageAlert.PCNotEligible1"/> <strong class="PCName"></strong> <spring:message code="ssap.notSeekingCoverageAlert.PCNotEligible2"/><p>  	
    <div class="dependentNotEligible hide">
	    <p class="hide singular"><spring:message code="ssap.notSeekingCoverageAlert.singular"/><p>
	    <p class="hide plural"><spring:message code="ssap.notSeekingCoverageAlert.plural"/><p>
	    <div id="notSeekingCoverageList"></div>
	    <p class="margin10-t"><spring:message code="ssap.notSeekingCoverageAlert.message"/></p>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" onclick="next()"><spring:message code="ssap.notSeekingCoverageAlert.continue"/></button>
  </div>
</div>