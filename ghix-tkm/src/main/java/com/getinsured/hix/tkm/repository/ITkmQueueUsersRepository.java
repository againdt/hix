/**
 * 
 */
package com.getinsured.hix.tkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmQueueUser;
import com.getinsured.hix.model.TkmQueues;

/**
 * @author hardas_d
 *
 */
public interface ITkmQueueUsersRepository extends JpaRepository<TkmQueueUser, Integer>{

	/**
	 * @author hardas_d
	 * @return List<TkmQueueUser>
	 * @param queueId
	 * @return
	 */
	@Query("SELECT tqu.userId FROM TkmQueueUser tqu WHERE tqu.groupId= :queueId")
	List<Integer> findUsersByGroupId(@Param("queueId") Integer queueId);

	/**
	 * @author hardas_d
	 * @return void
	 * @param queueId
	 * @param userList
	 */
	@Transactional
	@Modifying
	@Query("DELETE FROM TkmQueueUser tqu WHERE tqu.groupId= :queueId AND tqu.userId IN (:userList)")
	void deleteByGroupAndUsers(@Param("queueId") Integer queueId, @Param("userList") List<Integer> userList);
	
	/**
	 * @author kaul_s
	 * @return List<TkmQueue Id>
	 * @param userId
	 * @return
	 */
	@Query("SELECT tqu.groupId FROM TkmQueueUser tqu WHERE tqu.userId= :userId")
	List<Integer> findGroupsByUserId(@Param("userId") Integer userId);
	
	@Query("from AccountUser as u where u.id IN( select qu.userId from TkmQueueUser as qu where qu.groupId ="
			+ "(select id from TkmQueues where name=:queuename))")
	List<AccountUser> getAccountUsersByQueueName(@Param("queuename") String queuename);
	
	@Query("select distinct(groupId) from TkmQueueUser a where a.userId=:userId)")
	List<Integer> getQueueByUserId(@Param("userId") int userId);

	@Query("from TkmQueues tq WHERE tq.id IN (select a.groupId from TkmQueueUser a where a.userId=:userId)")
	List<TkmQueues> getQueueNameByUserId(@Param("userId") int userId);
	
	@Query("Select u.id,concat(u.firstName,' ',u.lastName) as fullName,r.description from AccountUser as u,UserRole ur, Role r where u.id IN( select qu.userId from TkmQueueUser as qu where qu.groupId ="
			+ "(select id from TkmQueues where name=:queuename)) and u.id=ur.user.id and ur.role.id=r.id ORDER BY u.lastName ASC")
	List<Object[]> getAccountUsersWithRoleByQueueName(@Param("queuename") String queuename);

	@Query("Select u.id,concat(u.firstName,' ',u.lastName) as fullName from AccountUser as u where u.id IN( select qu.userId from TkmQueueUser as qu where qu.groupId ="
			+ "(select id from TkmQueues where name=:queuename)) ORDER BY u.lastName ASC")
	List<Object[]> getAccountUsersByQueueNameForReassign(@Param("queuename") String queuename);
}
