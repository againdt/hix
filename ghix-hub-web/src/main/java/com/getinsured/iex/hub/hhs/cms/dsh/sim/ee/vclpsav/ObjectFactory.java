
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.hhs.cms.dsh.sim.ee.vclpsav package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RequesterCommentsForHub_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "RequesterCommentsForHub");
    private final static QName _NonCitCoaCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitCoaCode");
    private final static QName _NonCitAdmittedToText_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitAdmittedToText");
    private final static QName _QualifiedNonCitizenCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "QualifiedNonCitizenCode");
    private final static QName _FiveYearBarApplyCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "FiveYearBarApplyCode");
    private final static QName _FiveYearBarApplicabilityIndicator_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "FiveYearBarApplicabilityIndicator");
    private final static QName _NonCitMiddleName_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitMiddleName");
    private final static QName _ResponseDescriptionText_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "ResponseDescriptionText");
    private final static QName _NonCitBirthDate_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitBirthDate");
    private final static QName _DSHAutoTriggerStepTwo_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "DSHAutoTriggerStepTwo");
    private final static QName _CaseNumber_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "CaseNumber");
    private final static QName _NonCitFirstName_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitFirstName");
    private final static QName _NonCitAdmittedToDate_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitAdmittedToDate");
    private final static QName _NonCitLastName_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitLastName");
    private final static QName _NonCitEntryDate_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitEntryDate");
    private final static QName _ResponseMetadata_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "ResponseMetadata");
    private final static QName _EligStatementTxt_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "EligStatementTxt");
    private final static QName _USCitizenCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "USCitizenCode");
    private final static QName _NonCitCountryCitCd_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitCountryCitCd");
    private final static QName _SubmitAgency3DHSResubResponse_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "SubmitAgency3DHSResubResponse");
    private final static QName _ResponseCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "ResponseCode");
    private final static QName _SEVISID_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "SEVISID");
    private final static QName _LawfulPresenceVerifiedCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "LawfulPresenceVerifiedCode");
    private final static QName _EligStatementCd_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "EligStatementCd");
    private final static QName _NonCitCountryBirthCd_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "NonCitCountryBirthCd");
    private final static QName _TDSResponseDescriptionText_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "TDSResponseDescriptionText");
    private final static QName _CategoryCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "CategoryCode");
    private final static QName _SubmitAgency3DHSResubRequest_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "SubmitAgency3DHSResubRequest");
    private final static QName _FiveYearBarMetCode_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "FiveYearBarMetCode");
    private final static QName _RequestedCoverageStartDate_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "RequestedCoverageStartDate");
    private final static QName _WebServSftwrVer_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "WebServSftwrVer");
    private final static QName _SubmitAgency3DHSResubResponseSet_QNAME = new QName("http://vclpsav.ee.sim.dsh.cms.hhs.gov", "SubmitAgency3DHSResubResponseSet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.hhs.cms.dsh.sim.ee.vclpsav
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponseMetadataType }
     * 
     */
    public ResponseMetadataType createResponseMetadataType() {
        return new ResponseMetadataType();
    }

    /**
     * Create an instance of {@link SubmitAgency3DHSResubResponseSetType }
     * 
     */
    public SubmitAgency3DHSResubResponseSetType createSubmitAgency3DHSResubResponseSetType() {
        return new SubmitAgency3DHSResubResponseSetType();
    }

    /**
     * Create an instance of {@link SubmitAgency3DHSResubResponseType }
     * 
     */
    public SubmitAgency3DHSResubResponseType createSubmitAgency3DHSResubResponseType() {
        return new SubmitAgency3DHSResubResponseType();
    }

    /**
     * Create an instance of {@link SubmitAgency3DHSResubRequestType }
     * 
     */
    public SubmitAgency3DHSResubRequestType createSubmitAgency3DHSResubRequestType() {
        return new SubmitAgency3DHSResubRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "RequesterCommentsForHub")
    public JAXBElement<String> createRequesterCommentsForHub(String value) {
        return new JAXBElement<String>(_RequesterCommentsForHub_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitCoaCode")
    public JAXBElement<String> createNonCitCoaCode(String value) {
        return new JAXBElement<String>(_NonCitCoaCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitAdmittedToText")
    public JAXBElement<String> createNonCitAdmittedToText(String value) {
        return new JAXBElement<String>(_NonCitAdmittedToText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "QualifiedNonCitizenCode")
    public JAXBElement<String> createQualifiedNonCitizenCode(String value) {
        return new JAXBElement<String>(_QualifiedNonCitizenCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarApplyCode")
    public JAXBElement<String> createFiveYearBarApplyCode(String value) {
        return new JAXBElement<String>(_FiveYearBarApplyCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarApplicabilityIndicator")
    public JAXBElement<Boolean> createFiveYearBarApplicabilityIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_FiveYearBarApplicabilityIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitMiddleName")
    public JAXBElement<String> createNonCitMiddleName(String value) {
        return new JAXBElement<String>(_NonCitMiddleName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "ResponseDescriptionText")
    public JAXBElement<String> createResponseDescriptionText(String value) {
        return new JAXBElement<String>(_ResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitBirthDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitBirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitBirthDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "DSHAutoTriggerStepTwo")
    public JAXBElement<Boolean> createDSHAutoTriggerStepTwo(Boolean value) {
        return new JAXBElement<Boolean>(_DSHAutoTriggerStepTwo_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "CaseNumber")
    public JAXBElement<String> createCaseNumber(String value) {
        return new JAXBElement<String>(_CaseNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitFirstName")
    public JAXBElement<String> createNonCitFirstName(String value) {
        return new JAXBElement<String>(_NonCitFirstName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitAdmittedToDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitAdmittedToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitAdmittedToDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitLastName")
    public JAXBElement<String> createNonCitLastName(String value) {
        return new JAXBElement<String>(_NonCitLastName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitEntryDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitEntryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitEntryDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "ResponseMetadata")
    public JAXBElement<ResponseMetadataType> createResponseMetadata(ResponseMetadataType value) {
        return new JAXBElement<ResponseMetadataType>(_ResponseMetadata_QNAME, ResponseMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "EligStatementTxt")
    public JAXBElement<String> createEligStatementTxt(String value) {
        return new JAXBElement<String>(_EligStatementTxt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "USCitizenCode")
    public JAXBElement<String> createUSCitizenCode(String value) {
        return new JAXBElement<String>(_USCitizenCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitCountryCitCd")
    public JAXBElement<String> createNonCitCountryCitCd(String value) {
        return new JAXBElement<String>(_NonCitCountryCitCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitAgency3DHSResubResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "SubmitAgency3DHSResubResponse")
    public JAXBElement<SubmitAgency3DHSResubResponseType> createSubmitAgency3DHSResubResponse(SubmitAgency3DHSResubResponseType value) {
        return new JAXBElement<SubmitAgency3DHSResubResponseType>(_SubmitAgency3DHSResubResponse_QNAME, SubmitAgency3DHSResubResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "ResponseCode")
    public JAXBElement<String> createResponseCode(String value) {
        return new JAXBElement<String>(_ResponseCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "SEVISID")
    public JAXBElement<String> createSEVISID(String value) {
        return new JAXBElement<String>(_SEVISID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "LawfulPresenceVerifiedCode")
    public JAXBElement<String> createLawfulPresenceVerifiedCode(String value) {
        return new JAXBElement<String>(_LawfulPresenceVerifiedCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "EligStatementCd")
    public JAXBElement<BigInteger> createEligStatementCd(BigInteger value) {
        return new JAXBElement<BigInteger>(_EligStatementCd_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "NonCitCountryBirthCd")
    public JAXBElement<String> createNonCitCountryBirthCd(String value) {
        return new JAXBElement<String>(_NonCitCountryBirthCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "TDSResponseDescriptionText")
    public JAXBElement<String> createTDSResponseDescriptionText(String value) {
        return new JAXBElement<String>(_TDSResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "CategoryCode")
    public JAXBElement<String> createCategoryCode(String value) {
        return new JAXBElement<String>(_CategoryCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitAgency3DHSResubRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "SubmitAgency3DHSResubRequest")
    public JAXBElement<SubmitAgency3DHSResubRequestType> createSubmitAgency3DHSResubRequest(SubmitAgency3DHSResubRequestType value) {
        return new JAXBElement<SubmitAgency3DHSResubRequestType>(_SubmitAgency3DHSResubRequest_QNAME, SubmitAgency3DHSResubRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarMetCode")
    public JAXBElement<String> createFiveYearBarMetCode(String value) {
        return new JAXBElement<String>(_FiveYearBarMetCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "RequestedCoverageStartDate")
    public JAXBElement<XMLGregorianCalendar> createRequestedCoverageStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RequestedCoverageStartDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "WebServSftwrVer")
    public JAXBElement<String> createWebServSftwrVer(String value) {
        return new JAXBElement<String>(_WebServSftwrVer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitAgency3DHSResubResponseSetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", name = "SubmitAgency3DHSResubResponseSet")
    public JAXBElement<SubmitAgency3DHSResubResponseSetType> createSubmitAgency3DHSResubResponseSet(SubmitAgency3DHSResubResponseSetType value) {
        return new JAXBElement<SubmitAgency3DHSResubResponseSetType>(_SubmitAgency3DHSResubResponseSet_QNAME, SubmitAgency3DHSResubResponseSetType.class, null, value);
    }

}
