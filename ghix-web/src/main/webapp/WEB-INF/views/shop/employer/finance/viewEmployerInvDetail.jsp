<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript">
function validate(employerId)
{
  $("form").attr("action", "../../../broker/approve/"+employerId + "/employers");
  $("form").submit();
};

function closeIFrame() {
	$("#emodal").remove();
	parent.location.reload();
	window.parent.closeIFrame();
}


</script>

<div class="gray">
	<h4>${employer.name}</h4>
</div>

<form class="form-vertical" id="frmviewEmployerDetail"
	name="frmviewEmployerDetail" method="GET" target="_parent">
	<df:csrfToken/>
	<table class="table table-border-none">
		<tbody>
			<tr>
				<td class=""><spring:message code="label.agent.employers.NumberofEmployees"/>: <strong>${totalEmp}</strong></td>
			</tr>
			<tr>
				<td class=""><spring:message code="label.brkcontactname"/>: <strong>${employer.contactFirstName}
						${employer.contactLastName}</strong></td>
			</tr>
			<tr>
				<td class=""><spring:message code="label.brkPhoneNumber"/>: 
				<c:if test= "${employer.contactNumber ne null || employer.contactNumber ne ''}">
					<strong>${employer.contactNumber}</strong>
				</c:if>
				</td>
			</tr>
			<tr>
				<td class=""><spring:message code="label.brkEmail"/>: <strong>${employer.contactEmail}</strong></td>
			</tr>
		</tbody>
	</table>
	<c:if test="${(status != 'InActive')}">
		<div class="form-actions">
			<span class="pull-right">
				<a class="btn btn-primary btn-small" onClick="return validate(${employer.id})"><spring:message code="label.brkaccept"/></a>
				<a class="btn btn-small" href="#" onClick="window.parent.closeMe(${employer.id}, '${desigStatus}', '${employer.name}')"><spring:message code="label.brkdecline"/></a>
			</span>
		</div>
	</c:if>
</form>
