 <%@ include file="../include.jsp" %>
 
 <%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
 <%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
 
 <%
	 String stateCode=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
 %>
  
  <c:set var="stateCode" value="<%=stateCode%>" />

 <div id="appscr86" style='display: none;'>

			<div class="Main_ui_design">
			<div class="titleCSSForPages">
				<label class="titleForPages" class="labelML15"><span class="nameOfHouseHold"></span><spring:message code="ssap.page36.requiredDocs" text="'s Required Documents"/></label>
		      	<div class="Hr_ui_design"><div class="Hr_ui_design1"></div> </div>
			</div> 

				<form>
					<div class="value61"><spring:message code="ssap.page36.systemISNotAbleSubmitFollowing" text="System was unable to verify your
						information. Please submit the following documents."/></div>

					<div class="value61"><spring:message code="ssap.page36.submissionThroughAccountPage" text="You can also proceed and submit the
						document later through My Account Page."/></div>



<div class="docuploaddiv">
		
	<c:if test="${stateCode != 'CA'}">
		<table class="tableML34">
			<tbody>
				<tr>
				<th class="blackColorLbl"><spring:message code="ssap.page36.docCategory " text="Select Document Category"/></th>
				<th class="blackColorLbl"><spring:message code="ssap.page36.docType " text="Select Document Type"/> </th>
				<th></th>
				</tr>
				<tr>
					<td class="tdPL0W280">
						<select class="doctype1 selectPL0MR0" aria-label="Document categories">
							<option><spring:message code="ssap.page36.documentCategory " text="Document Category"/></option>
							<option value="tribalMember"><spring:message code="ssap.page36.proofOFTribalMembership" text="Proof of tribal membership"/></option>
							<option value="immigrationStatus"><spring:message code="ssap.page36.proofOFEligibleImmigration" text="Proof of eligible immigration status"/></option>
							<option value="citizenship"><spring:message code="ssap.page36.proofOFCitizenShip" text="proof of citizenship"/></option>
							<option value="other"><spring:message code="ssap.page36.proofOFOther " text="Other"/></option>
						</select>
					</td>
					<td class="tdPL0W280">
					<select class="doctype2" aria-label="Document types">
							<option><spring:message code="ssap.page36.documentType" text="Document Type"/></option>
							<option value="tribalMember"><spring:message code="ssap.page36.proofOFTribalMembership " text="Proof of tribal membership"/></option>
							<option value="immigrationStatus"><spring:message code="ssap.page36.proofOFEligibleImmigration " text="Proof of eligible immigration status"/></option>
							<option value="citizenship"><spring:message code="ssap.page36.proofOFCitizenShip" text="proof of citizenship"/></option>
							<option value="other"><spring:message code="ssap.page36.proofOFOther" text="Other"/></option>
						</select>
					</td>
					<td class="upload_btn"><input type="button" value="upload"></td>
				</tr>
			</tbody>
		</table>
	</c:if>
</div>



				</form>
         
         </div></div>