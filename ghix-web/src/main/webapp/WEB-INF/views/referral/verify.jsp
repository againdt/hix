<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.springframework.validation.FieldError"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript"
	src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
	
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.js" />"></script>
<div class="gutter10" onload="javascript:displayEnrolledModal()">
	<div class="row-fluid" id="titlebar">
		<h1><spring:message code="label.referral.pageheader" /></h1>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><spring:message code="label.referral.sideheader" /></h4>
			</div>
			<ul class="nav nav-list">
				<li class="active"><spring:message code="label.referral.securityquestions" />
				</li>
				<li><spring:message code="label.referral.linktoapplication" />
				</li>
			</ul>
		</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4><spring:message code="label.referral.securityquestions" /></h4>
			</div>
			<c:if test="${requestScope.failure != '1'}">
		<div>
		<ul>
		 <li> <spring:message code="msg.referral.verify.content1" />  <br></li>
		 <li> <spring:message code="msg.referral.verify.content2" /><br></li>
		 <li> <spring:message code="msg.referral.verify.content3" /></li>
		 		
		 </ul>
		</div>
		
	<br>
	        <spring:message code="msg.referral.verify.content6" /><br>
	         <br>
	         </c:if>
	         <% 
			 Object fieldErrorObj = session.getAttribute("fieldErrors");
			 if(fieldErrorObj != null)
	         {
	        	 %>
	        	 <c:forEach items="${fieldErrors}" var="msg">
						<font color="red"><spring:message code="${msg.code}" /></font><br/>
				</c:forEach>
				<%         	 
	         	session.removeAttribute("fieldErrors");
	         }
	         %>
	         <br>
			<form:form id="frmReferralSsapVerification" 
				modelAttribute="referralVerification"
				name="frmReferralSsapVerification" method="POST"
				class="form-horizontal gutter10">
				<form:hidden path="referralActivation.ssapApplicationId" />
				<form:hidden path="referralActivation.cmrHouseholdId" />
				<form:hidden path="referralActivation.id" />
 				<df:csrfToken/>
				<c:if test="${requestScope.failure == '1'}">
					<div id='msgRetry'>
						<div class="gutter10 center-text">
							<p style="font-size: 16px">
								<spring:message code="msg.referral.invalid1" /></p>
								<br>
							<p>	<spring:message code="msg.referral.invalid2"/>
							</p>
						</div>
						<div class="form-actions">
							<button class="btn btn-primary pull-right" id="btnRetry">Retry</button>
						</div>
					</div>
				</c:if>

				<c:choose>
					<c:when test="${requestScope.failure == '1'}">
						<div class="hide" id="mainForm">
					</c:when>
					<c:otherwise>
						<div>
					</c:otherwise>
				</c:choose>
				
				<div class="control-group">
					<label class="control-label"><spring:message
							code="${referralVerification.verificationQuestions[0].text}" /><img
						src="<c:url value="/resources/images/requiredAsterix.png" />"
						width="10" height="10" alt="Required!" /> <form:hidden
							path="verificationQuestions[0].code" /> </label>
					<div class="controls">
						<form:input path="verificationQuestions[0].answer"  
							id="verificationQuestions[0].answer" class="input-large" required="required"/>
						<div id="verificationQuestions[0].answer_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message
							code="${referralVerification.verificationQuestions[1].text}" /><img
						src="<c:url value="/resources/images/requiredAsterix.png" />"
						width="10" height="10" alt="Required!" /> <form:hidden
							path="verificationQuestions[1].code" /> </label>
					<div class="controls">
						<form:input path="verificationQuestions[1].answer"
							 id="verificationQuestions[1].answer" required="required"/>
						<div id="verificationQuestions[1].answer_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message
							code="${referralVerification.verificationQuestions[2].text}" /><img
						src="<c:url value="/resources/images/requiredAsterix.png" />"
						width="10" height="10" alt="Required!" /> <form:hidden
							path="verificationQuestions[2].code" /> </label>
					<div class="controls">
						<div id="date" data-provide="datepicker"
							class="input-append date date-picker">
							<form:input path="verificationQuestions[2].answer"
								  id="verificationQuestions[2].answer"
								aria-label=" Sample text.  M M/ D D/ Y Y Y Y" required="required"
								class="dateOfBirth input-medium" maxlength="10" autocomplete="off" />
							<span class="add-on" id="dateSpan"><i
								class="icon-calendar"></i> </span>

						</div>
						<div id="verificationQuestions[2].answer_error"></div>
					</div>
				</div>
				<div class="form-actions">
					<c:if test="${sessionScope.enrolledAppSess == 'N' or sessionScope.ActiveAppPresent == 'N'}">
					<input type="submit"
						value="<spring:message code="label.submit-button"/>"
						class="btn btn-primary pull-right">
					</c:if>				
				</div>
		</div>


		</form:form>
	</div>
</div>
</div>
<div id="confirm" class="modal hide fade modelcntr">
  <div class="modal-body">
<c:if test="${sessionScope.enrolledAppSess == 'Y'}">
  <ul>
    <li> <spring:message code="msg.referral.verify.content7" /></li>
    <li> <spring:message code="msg.referral.verify.content8" />  </li>
    </ul><br>
    <spring:message code="msg.referral.verify.content9" /><br>
    <spring:message code="msg.referral.verify.content10" /><br>
    <spring:message code="msg.referral.verify.content11" /><br>
    <spring:message code="msg.referral.verify.content12" /><br>
    <spring:message code="msg.referral.verify.content13" /><br>
    <spring:message code="msg.referral.verify.content14" /><br>
     <spring:message code="msg.referral.verify.content15" /><br>
  </c:if>
   <c:if test="${sessionScope.ActiveAppPresent == 'Y'}">
    <ul>
    <li> <spring:message code="msg.referral.verify.nonfinancialcontent1" /></li>
    <li> <spring:message code="msg.referral.verify.content8" />  </li>
    </ul><br>
    <spring:message code="msg.referral.verify.content9" /><br>
    <spring:message code="msg.referral.verify.content10" /><br>
    <spring:message code="msg.referral.verify.content11" /><br>
    <spring:message code="msg.referral.verify.content12" /><br>
    <spring:message code="msg.referral.verify.content13" /><br>
    <spring:message code="msg.referral.verify.content14" /><br>
     <spring:message code="msg.referral.verify.content15" /><br>
    </c:if> 
  </div>
  <div class="modal-footer">
    <a href="<c:url value='/indportal' />" class="btn btn-primary">Go To Dashboard</a>
  </div>
</div> 


<style type="text/css">
body,
.modal-open .page-container,
.modal-open .page-container .navbar-fixed-top,
.modal-open .modal-container {
	overflow-y: scroll;
}

@media (max-width: 979px) {
	.modal-open .page-container .navbar-fixed-top{
		overflow-y: visible;
	}
}

#confirm{
top:35%;
}

</style>


<script type="text/javascript">
	<c:if test="${requestScope.failure == '1'}">
	var onRetry = function() {
		$("#mainForm").show();
		$("#msgRetry").hide();
		return false;
	};
	$(function() {
		$("#btnRetry").bind("click", onRetry);
	});
	</c:if>
	
		<c:if test="${sessionScope.enrolledAppSess == 'Y' or sessionScope.ActiveAppPresent =='Y'}">
	$( document ).ready(function() {
		   $('#confirm')
		        .modal({ backdrop: 'static', keyboard: false })
		        .one('click', '[data-value]', function (e) {
		            if($(this).data('value')) {
		                alert('confirmed');
		            } else {
		                alert('canceled');
		            }
		        });
		});
	</c:if>
  $(document).ready(function() {
    $('.dateOfBirth').mask("00/00/0000");
  })
</script>