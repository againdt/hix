package com.getinsured.eligibility.rrv.service;

import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface SingleStreamlinedApplicationService {

    SingleStreamlinedApplication getSingleStreamlinedApplication(long applicationId);

    SingleStreamlinedApplication getSingleStreamlinedApplication(SsapApplication ssapApplication);

    void saveSingleStreamlinedApplication(SsapApplication ssapApplication, SingleStreamlinedApplication singleStreamlinedApplication);
}
