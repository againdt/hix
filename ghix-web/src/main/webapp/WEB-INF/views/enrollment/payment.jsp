<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
</style>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">
$("input[name=paymentType]").live('click', function(){
   	var optionVal = $("input[name=paymentType]:checked").val();
   	if(optionVal == 'Bank'){
   		$('#bankDataForm').show();
   		$('#creditCardDataForm').hide();
   	}else{
   		$('#creditCardDataForm').show();
   		$('#bankDataForm').hide();
   	}
});

function validateForm(){
	if( $("#frmpayment").validate().form() ) {
		 if( $("input[name=paymentType]:checked").val() == undefined ){
			 alert('Please Select Payment Type');
			 return false;
		 }else  if( $("input[name=paymentType]:checked").val() == 'CreditCard' ){
			 validateCreditInfo();
		 }else{
			 $("#frmpayment").submit();
		 }
	 }
}


function validateCreditInfo() {
    var validateUrl = 'validatecreditinfo';
 	var frm = $(document.frmpayment);
    var fields = frm.serializeArray();
 	   
 	var result = new Object();
    jQuery.each(fields, function(i, field){
  	   result[field.name] = field.value;
    });
    
    var dat = result;
 	
    //alert('AJAX Call starts here');
 	
     $.ajax({
		url:validateUrl, 
		type: "POST", 
		data: dat, 
		success: function(response)
		{
			//alert('ajax call response : '+response);
			if(response == 'REJECT'){
				error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please provide valid creditcard Information.</span></label>";
				$('#creditInfoAuth_error').html(error);
			 	return false;
			}else{
			 	$("#frmpayment").submit();
			}
		},
 });
}

</script>
    
<div class="breadcrumb-bar">
	<ul class="breadcrumb">
		<li><a href="<c:url value="/plandisplay/anonymous/" />"><span class="dot">1</span> <em>Household</em></a></li>
		<li><a href="<c:url value="/plandisplay/preferences/" />"><span class="dot">2</span> <em>Preferences</em></a></li>
		<li><a href="<c:url value="/plandisplay/planselection/" />"><span class="dot">3</span> <em>Plan Selection</em></a></li>
		<li><a href="<c:url value="/plandisplay/showcart/" />"><span class="dot">4</span> <em><strong>Cart</strong></em></a></li>
		<li class="active"><a href="<c:url value="/enrollment/payment?cart_id="/>${cartId}"><span class="dot">5</span> <em> Payment</em></a></li>
		<li><span class="dot">6</span><em> E-signature</em></li>
		<li><span class="dot">7</span><em> Order Confirmation</em></li>
	</ul>
</div>
<!--breadcrumb-bar-->
		<div class="row-fluid">	
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p><p/>
					  <strong><c:out value="${errorMsg}"></c:out></strong>
 	 	 	 		          <p> Going back will result to lost of cart content.</p> 
 	 	 	 	              <p>If you wish to proceed to make a new selection of plans <a href="<c:url value="/plandisplay/preferences" />"><Strong> Click Here</Strong></a> </p>
 	 	 	 		          <p>If you wish to continue with your current order to enrollment <a href="<c:url value="/enrollment/esignature?cart_id=" />${cartId}"> <Strong>Click Here<Strong></a></p>
 	 	 	    </c:if>
				<br>
			</div>
		</div>

	<c:if test="${errorMsg == ''}">	
		<div class="row-fluid" id="titlebar">
	<div class="gutter10">
		<div class="span3">
			<div class="subnav">
				<ul class="nav nav-pills">
					 <li class=""><a href="<c:url value="/plandisplay/showcart" />">Back </a></li>
				</ul>
			</div>
		</div>
		<div class="span9">
			<h3 class="pull-left" id="Nplans">Payment Information</h3>
		</div>
	</div>
</div> <!--titlebar-->
		
<div class="row-fluid">
    
     <div class="span3" id="sidebar">
    	 <div class="gutter10">
        	<p>Please provide us with your billing information.</p>
     	 </div>
       </div><!--sidebar-->  
           
       	<div class="span9 columns pull-right" id="rightpanel">
             <div class="gutter10"> 
                <form method="post" action="createpayment" class="form-horizontal" name="frmpayment" id="frmpayment">
                	<input type="hidden" name="country" id="country" value="US"/>
                	<input type="hidden" name="cart_id" id="cart_id" value="${cartId}"/>
                     <div id="family_info_data">
                     	<fieldset id="family_info_1">
                  
                            <div class="control-group">
                                 <label for="firstName" class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
                                	<input type="text" class="input-xlarge" id="firstName" name="firstName" size="30">
                                	<div class="" id="firstName_error"></div>
                            	</div>
                            	
                            </div>
                            <div class="control-group">
                                <label for="LastName" class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
	                                <input type="text" class="input-xlarge" id="lastName" name="lastName" size="30">
	                                <div class="" id="lastName_error"></div>
                                </div>
                            </div>
                                   <div class="control-group">
                                <label for="email" class="control-label"><spring:message  code="label.email"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
	                                <input type="text" class="input-xlarge" id="email" name="email" size="30">
	                                <div class="" id="email_error"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="address1" class="control-label"><spring:message  code="label.address1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
	                                <input type="text" class="input-xlarge" id="address1" name="address1" size="30">
	                                <div class="" id="address1_error"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="address2" class="control-label"><spring:message  code="label.address2"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
	                                <input type="text" class="input-xlarge" id="address2" name="address2" size="30">
	                                <div class="" id="address2_error"></div>
                                </div>
                            </div>
                                
                            <div class="control-group">
                              <label for="zipcode" class="control-label"><spring:message  code="label.zipcode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                              <div class="controls">
                                 <input class="input-small" onblur="populateCityNState(this);" type="text" placeholder="Zip Code"  maxlength="5"  id="zipcode" name="zipcode">
                                 <div id="zipcode_error"></div>
                              </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="city" class="control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
                                	<input type="text" class="input-xlarge" id="city" name="city" size="30">
                                	<div class="" id="city_error"></div>
                                </div>
                            </div>
                            
                            <div class="control-group">
                               	 <label for="state" class="control-label"><spring:message  code="label.state"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                             <div class="controls">
									<select size="1"  id="state" name="state">
										 <option value="">Select</option>
										
										 	<option <c:if test="${stateCode == locationObj.state}"> SELECTED </c:if> value="<c:out value="${stateCode}" />">
	                                            <c:out value="${stateName}" />
	                                        </option>
										
									</select>
									<div id="state_error"></div>
								</div>
                            </div>
                            <h4>Total Monthly Premium for Plans Selected:$ ${premium} <br /> <small>(advanced subsidies have been applied if you qualified)</small></h4>
                            <br />

                            
                         <section class="row-fluid">

                        <legend>Select Payment Type:</legend>
                       <div id="accordion-paytype" class="accordion">
                            	
                               <div class="accordion-group">
									<div class="accordion-heading">
                                          <div class="gutter10">
                                            <input type="radio" name="paymentType" id="paymentType" value="Bank"> Bank Account Information / e-Check &nbsp;
                                        </div>
                                    </div>
                                    
                                 	<div id="collapse1"> <!-- collapse1 -->
                                	<div id="bankDataForm" style="display:none;" class="accordion-inner">
                                        <div class="control-group">
                                            <label for="bankName" class="control-label"><spring:message  code="label.bankName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<input type="text" class="input-xlarge" id="bankName" name="bankInfo.bankName" size="30">
                                            	<div id="bankName_error"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <label for="nameOnAccount" class="control-label"><spring:message  code="label.nameOnAccount"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<input type="text" class="input-xlarge" id="nameOnAccount" name="bankInfo.nameOnAccount" size="30">
                                            	<div id="nameOnAccount_error"></div>
                                            </div>
                                        </div>
                                        
                                         <div class="control-group">
                                            <label for="accountType" class="control-label"><spring:message  code="label.accountType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                               <select name="bankInfo.accountType" id="accountType">
													<option value="">Select</option>
													<option value="C">Checking</option>
													<option value="S">Saving</option>
													<option value="X">Corporate Checking</option>
												</select>
												<div id="accountType_error"></div>
                                            </div>
                                        </div>
		                                        
                                        <div class="control-group">
                                             <label for="routingNumber" class="control-label"><spring:message  code="label.routingNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<input type="text" class="input-xlarge" id="routingNumber" name="bankInfo.routingNumber"  maxlength="9" size="30">
                                            	<div id="routingNumber_error"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <label for="accountNumber" class="control-label"><spring:message  code="label.accountNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<input type="text" class="input-xlarge" id="accountNumber" name="bankInfo.accountNumber" maxlength="20" size="30">
                                            	<div class="" id="accountNumber_error"></div>
                                            </div>
                                        </div>
                                    </div>  <!-- End accordian-inner -->
                                </div> <!-- End collapse1 -->
							</div>
                                 
                                 <div class="accordion-group">
										<div class="accordion-heading">
                                          <div class="gutter10">
                              			   <input type="radio" name="paymentType" id="paymentType" value="CreditCard">Credit / Debit Card &nbsp;                        				</div>
                                       </div>
                         			<div id="collapse2"> <!-- collapse2 -->
                                    <div style="display:none;" id="creditCardDataForm" class="accordion-inner">
                                        <div class="control-group">
                                            <label for="nameOnCard" class="control-label"><spring:message  code="label.nameOnCard"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                           		<input type="text" class="input-xlarge" id="nameOnCard" name="creditCardInfo.nameOnCard" size="30">
                                           		<div id="nameOnCard_error"></div>
                                            </div>
                                        </div>
            
                                         <div class="control-group">
                                            <label for="cardType" class="control-label"><spring:message  code="label.cardType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<select  id="cardType" name="creditCardInfo.cardType">
													<option value="">Select</option>
													<option value="mastercard">Master Card</option>
													<option value="visa">Visa</option>
													<option value="americanexpress">American Express</option>
												</select>
												<div id="cardType_error"></div>
                                            </div>
                                       </div>
                                         
                                        <div class="control-group">
                                            <label for="cardNumber" class="control-label"><spring:message  code="label.cardNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<input type="text" class="input-xlarge" id="cardNumber" name="creditCardInfo.cardNumber" maxlength="16" size="30">
                                            	 <div class="" id="cardNumber_error"></div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <label for="expirationDate" class="control-label"><spring:message  code="label.expirationDate"/><small> MM/YYYY</small>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                            <div class="controls">
                                            	<span>
                                            		<select class="span2" name="creditCardInfo.expirationMonth" id="expirationMonth">
														<option value="">Select</option>
														<option value="01">Jan</option>
														<option value="02">Feb</option>
														<option value="03">Mar</option>
														<option value="04">Apr</option>
														<option value="05">May</option>
														<option value="06">Jun</option>
														<option value="07">Jul</option>
														<option value="08">Aug</option>
														<option value="09">Sep</option>
														<option value="10">Oct</option>
														<option value="11">Nov</option>
														<option value="12">Dec</option>
													</select>
                                            	</span>
												<span class="input-mini">
                                            		<select class="span2" id="expirationYear" name="creditCardInfo.expirationYear">
														<option value="">Select</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
													</select>
                                            	</span> 
                                            	<div id="creditInfoAuth_error"></div>
                                            </div>
                                        </div>
                                        
                                    </div>  <!-- End accordian-inner -->
                                   
                             	</div> <!-- End collapse2 -->
							</div>
                        </div>  <!-- End accordian_paytype -->
                        
                         <div class="form-actions" style="padding-left:20px">
                        		 <a href="#" class="btn">Apply for Payment Exemption</a>
                             <div class="pull-right">
                                  <a href="#" class="btn">Cancel</a>
                                  <input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" class="btn btn-primary" value="Next &rarr; eSignature" title="Next &rarr; eSignature"/>
                              </div>
                            </div>
                           </div>
                         </section>
                        </fieldset>
                        </div> <!-- End family_info_data  -->
          			</form>  <!-- End Form -->
             </div><!--gutter10-->
		</div><!--rightpanel-->
         
      </div> <!--row-fluid-->
</c:if>  
    
<script type="text/javascript">

jQuery.validator.addMethod("expirationDateCheck", function(value, element, param) {
	expirationMonth = $("#expirationMonth").val(); 
  	expirationYear  = $("#expirationYear").val(); 
  	if(expirationMonth == "" || expirationYear == ""){ 
  		return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("alphabetsOnly", function(value, element) {
	  return  /^([a-zA-Z][^\s])[a-zA-Z ']+$/i.test(value);
});

function populateCityNState(e){
	var validateUrl = 'getCityNState';
	var startindex = (e.id).indexOf("_");
	var index = (e.id).substring(startindex,(e.id).length);
	$.ajax({
		url:validateUrl, 
		type: "POST", 
		data: {zipcode: $("#"+e.id).val()}, 
		success: function(response)
		{
			if(response != null || response != 'null'){
				//console.log('test response '+response.city);
				$('#city').val(response.city);
				$('#state'+' option[value="'+response.state+'"]').attr('selected','selected');
			}
		},
    });
}

var validator = $("#frmpayment").validate({ 
	rules : {
		    'bankInfo.accountType' : {required: true},
		    'bankInfo.bankName' : {required: true, alphabetsOnly : true},
		    'bankInfo.nameOnAccount' : {required: true, alphabetsOnly : true},
			'bankInfo.routingNumber' : {required: true, number : true, minlength: 9},	
			'bankInfo.accountNumber' : { required: true, number: true},
			'creditCardInfo.cardType' : { required : true},
			'creditCardInfo.nameOnCard' : { required : true, alphabetsOnly : true},
			'creditCardInfo.cardNumber' : { required: true, number:  true, minlength: 16 },
			'creditCardInfo.expirationYear' : { expirationDateCheck: true },
			firstName : { required: true, alphabetsOnly : true },
			lastName : { required: true, alphabetsOnly : true },
			email: { required : true, email: true},
			address1 : { required: true },
			city : { required: true },
			state : { required: true },
			zipcode : { required: true, minlength : 5, number : true}
	},
	messages : {
		'bankInfo.accountType': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountType' javaScriptEscape='true'/></span>"},
		'bankInfo.bankName': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBankName' javaScriptEscape='true'/></span>",
							   alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"							
		},
		'bankInfo.nameOnAccount': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateNameOnAccount' javaScriptEscape='true'/></span>",
									alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"
		},
		'bankInfo.routingNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumber' javaScriptEscape='true'/></span>",
						 number : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberFormat' javaScriptEscape='true'/></span>",
						 minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberLength' javaScriptEscape='true'/></span>"
					   },
		'bankInfo.accountNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumber' javaScriptEscape='true'/></span>",
						 number : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberFormat' javaScriptEscape='true'/></span>"
						},
		'creditCardInfo.cardType': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardType' javaScriptEscape='true'/></span>"},
		'creditCardInfo.cardNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumber' javaScriptEscape='true'/></span>" ,
					  number : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberFormat' javaScriptEscape='true'/></span>",
					  minlength: "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberLength' javaScriptEscape='true'/></span>"
					},
		'creditCardInfo.nameOnCard':{required : "<span> <em class='excl'>!</em><spring:message code='label.validateNameOnCard' javaScriptEscape='true'/></span>",
									 alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"
		},			
		'creditCardInfo.expirationYear' : { expirationDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateExpirationDate' javaScriptEscape='true'/></span>"},
		'creditCardInfo.nameOnCard' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateNameOnCard' javaScriptEscape='true'/></span>",
										alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"	
		},
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>",
					 alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"
		},
		lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>",
					alphabetsOnly : "<span> <em class='excl'>!</em>Please enter only alphabets.</span>"	
		},
		email: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
	     		email : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailFormat' javaScriptEscape='true'/></span>"
	    },
		address1: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress1' javaScriptEscape='true'/></span>"},
		city: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		state: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		zipcode: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
			   number : "<span> <em class='excl'>!</em><spring:message code='label.validateZipNumber' javaScriptEscape='true'/></span>",
			   minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipLength' javaScriptEscape='true'/></span>"
			 }
	},
	highlight: function (element, errorClass) {
		/*shows the error message on invalid input*/
		var elementId = $(element).attr('id');
		$('#'+elementId).parent().find('#'+elementId+'_error').show();
    },
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	},
	success:function(error,element){
		/*hides the error message on a valid input*/
		var elementId = $(element).attr('id');
		$('#'+elementId).parent().find('#'+elementId+'_error').hide();
	}
});
</script>