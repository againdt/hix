package com.getinsured.hix.batch.indportal.reader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class BrokerDataFieldSetMapper implements FieldSetMapper<BrokerData>{
 
    @Override
    public BrokerData mapFieldSet(FieldSet fieldSet) throws BindException {
    	BrokerData borkerData = new BrokerData();
    	borkerData.setQualificationType(fieldSet.readString(0));
    	borkerData.setLicense(fieldSet.readString(1));
    	borkerData.setNpn(fieldSet.readString(2));
    	borkerData.setName(fieldSet.readString(3));
    	borkerData.setAddress(fieldSet.readString(4));
    	borkerData.setCityName(fieldSet.readString(5));
    	borkerData.setState(fieldSet.readString(6));
    	borkerData.setZip(fieldSet.readString(7));
    	borkerData.setPhone(fieldSet.readString(8));
    	borkerData.setEmail(fieldSet.readString(9));
    	borkerData.setOriginalIssueDate(fieldSet.readString(10));
    	borkerData.setLicenseExpiryDate(fieldSet.readString(11));
        return borkerData;
    }
 
}
