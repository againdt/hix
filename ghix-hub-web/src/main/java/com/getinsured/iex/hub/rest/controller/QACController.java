package com.getinsured.iex.hub.rest.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Controller class to handle synchronous request to QAC module
 * 
 * @author Vishaka Tharani
 * @since 28-Mar-2014
 * 
 */
@Controller
public class QACController extends MessageProcessor {

	
	@Autowired
	private WebServiceTemplate qacServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(QACController.class);

	@RequestMapping(value = "/invokeQAC", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeQAC(@RequestBody String qacRequestJSON){
		
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge nesiMecBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
			handler = nesiMecBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(qacRequestJSON);
			String qacInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			handler.setJsonInput(qacInputJSON);
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("QAC");
			requestRecord.setEndpointOperationName("QAC");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			if(objInputJson.get("applicationId") != null){
				requestRecord.setSsapApplicationId((Long) objInputJson.get("applicationId"));
			}
			response = this.executeRequest(requestRecord, handler, qacServiceTemplate);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
