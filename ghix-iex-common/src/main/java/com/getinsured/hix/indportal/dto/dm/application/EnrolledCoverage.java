package com.getinsured.hix.indportal.dto.dm.application;

public class EnrolledCoverage {
	 private String insuranceMarketType;
	 private String insurancePolicyMemberId;
	 private String insurancePolicyNumber;
	 private String insurancePlanName;


	 // Getter Methods 

	 public String getInsuranceMarketType() {
	  return insuranceMarketType;
	 }

	 public String getInsurancePolicyMemberId() {
	  return insurancePolicyMemberId;
	 }

	 public String getInsurancePolicyNumber() {
	  return insurancePolicyNumber;
	 }

	 public String getInsurancePlanName() {
	  return insurancePlanName;
	 }

	 // Setter Methods 

	 public void setInsuranceMarketType(String insuranceMarketType) {
	  this.insuranceMarketType = insuranceMarketType;
	 }

	 public void setInsurancePolicyMemberId(String insurancePolicyMemberId) {
	  this.insurancePolicyMemberId = insurancePolicyMemberId;
	 }

	 public void setInsurancePolicyNumber(String insurancePolicyNumber) {
	  this.insurancePolicyNumber = insurancePolicyNumber;
	 }

	 public void setInsurancePlanName(String insurancePlanName) {
	  this.insurancePlanName = insurancePlanName;
	 }
	}
