package com.getinsured.hix.indportal.dto.dm.application;

import java.util.List;
import java.util.Map;

public class InsuranceCoverage {
	 private List<EnrolledCoverage> enrolledCoverages;
	 private String offeredEmployeeCoverage;
	 private Map<String,EscOffer> employerSponsoredCoverageOffers;
	 //EmployerSponsoredCoverageOffers employerSponsoredCoverageOffers;


	 // Getter Methods 

	 public String getOfferedEmployeeCoverage() {
	  return offeredEmployeeCoverage;
	 }

	 // Setter Methods 

	 public void setOfferedEmployeeCoverage(String offeredEmployeeCoverage) {
	  this.offeredEmployeeCoverage = offeredEmployeeCoverage;
	 }

	public List<EnrolledCoverage> getEnrolledCoverages() {
		return enrolledCoverages;
	}

	public void setEnrolledCoverages(List<EnrolledCoverage> enrolledCoverages) {
		this.enrolledCoverages = enrolledCoverages;
	}

	public Map<String, EscOffer> getEmployerSponsoredCoverageOffers() {
		return employerSponsoredCoverageOffers;
	}

	public void setEmployerSponsoredCoverageOffers(Map<String, EscOffer> employerSponsoredCoverageOffers) {
		this.employerSponsoredCoverageOffers = employerSponsoredCoverageOffers;
	}
	}
