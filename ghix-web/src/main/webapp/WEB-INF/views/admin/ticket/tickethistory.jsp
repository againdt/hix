<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>

<!-- Cancel ticket JSP to add confirm dialog and cancel ticket functionality  -->
<%-- <jsp:include page="cancelTicket.jsp">
	<jsp:param value="${ticket.id}" name="ticketId"/>
</jsp:include> --%>
<!-- Cancel ticket JSP ends -->
<style>
    input[type="radio"], input[type="checkbox"] {
        margin: 0 0 0 4px;
    }
</style>
<div class="gutter10">
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/ticketmgmt/ticket/ticketlist/" />"><spring:message  code="lbl.viewTickets"/></a></li>
            <li><spring:message  code="link_tkt.tktHistory"/></li>
        </ul><!--page-breadcrumb ends-->
        <%-- 	<h1>&nbsp;<small>${ticket.subject}</small></h1> --%>
        <div class="row-fluid">
            <h1>${ticket.subject}</h1>
            <div class="row-fluid">
                <jsp:include page="ticketQuickActionBar.jsp">
                    <jsp:param value="${ticket.id}" name="ticketId"/>
                    <jsp:param value="${ticket.tkmWorkflows.type}" name="ticketType"/>
                    <jsp:param value="${ticket.tkmWorkflows.category}" name="ticketCategory"/>
                </jsp:include>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <c:set var="tkmTicketsObj" value="${ticket}" scope="request" />
            <jsp:include page="ticketsummary.jsp">
                <jsp:param value="${ticket.id}" name="ticketId"/>
                <jsp:param value="../ticketlist?pageNumber=${gotoPageNumber}" name="backButtonUrl"/>
            </jsp:include>
        </div>

        <div class="span9" id="rightpanel">
            <div style="font-size: 14px; color: red" id="tkmErrorDiv">
                <c:if test="${errorMsg != null && errorMsg != ''}">
                    <p><c:out value="${errorMsg}"></c:out><p/>
                    <c:remove var="errorMsg"/>
                </c:if>
            </div>
            <input type="hidden" name="isDetailedView" value="${isDetailedView}">
            <table id="userlistTbl" name="userlistTbl" class="table table-striped">
                <thead>
                <tr class="header">
                    <th scope="col" style="width:100px;">Date</th>
                    <th scope="col" style="width:140px;">Ticket/Task</th>
                    <th style="width:100px;">Status</th>
                    <th scope="col" style="width:100px;">Assignee</th>
                    <th scope="col" style="width:100px;">Workgroup</th>
                    <th scope="col" style="width:20px;">
								<span class="dropdown pull-right">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown"><i
                                            class="icon-cog"></i><i class="caret"></i><span class="hide">Action</span></a>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
										<input type="checkbox" value="${isDetailedView}" id="detailedViewChkBox" onClick="switchHistoryView();"/> <spring:message code="lbl.showDetailView"/>
									</ul>
								</span>
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${fn:length(ticketHistoryList) > 0}">
                        <!-- format="{0,date,MMM dd, yyyy}"  -->
                        <c:forEach items="${ticketHistoryList}" var="ticketHistory" varStatus="vs">
                            <tr>
                                <td>${ticketHistory.disDate}</td>
                                <td>${ticketHistory.description}</td>
                                <td>${ticketHistory.status}</td>
                                <td>${ticketHistory.name}</td>
                                <td>${ticketHistory.assignee}</td>
                                <td>&nbsp;</td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <h4 class="alert alert-info">
                            <spring:message code='label.norecords' />
                        </h4>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
            <div class="pagination txt-center">
                <dl:paginate resultSize="${resultSize + 0}" pageSize="${PageSize + 0}"/>
            </div>
        </div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->



<script type="text/javascript">
    $(document).ready(function() {
        $("#tickethistory").addClass("active navmenu");
        $('#ticketHistoryList').find('.header th:first-child').addClass('width128');

        var checkStatus = ${isDetailedView};
        $("#detailedViewChkBox").attr('checked', checkStatus);
    });

    function switchHistoryView() {
        var isDetailedView = $("#detailedViewChkBox").is(":checked");

        var ticketId = '<encryptor:enc value="${ticket.id}"/>';
        var validateUrl ='<c:url value="/ticketmgmt/ticket/tickethistory"></c:url>';

        $.ajax({
            url : validateUrl,
            type : "POST",
            data : {
        ${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
            encTicketId : ticketId,
            isDetailedView : isDetailedView
    },
        success: function(response,xhr)
        {
            if(isInvalidCSRFToken(xhr))
                return;
            if(!isDetailedView){
                window.location.search = '?pageNumber=1';
            }else{
                window.location.reload(true);
            }
        },
        error: function(e){
            alert("Failed to fetch history");
        },
    });
    }

    function isInvalidCSRFToken(xhr){
        var rv = false;
        if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
            alert($('Session is invalid').text());
            rv = true;
        }
        return rv;
    }
</script>
