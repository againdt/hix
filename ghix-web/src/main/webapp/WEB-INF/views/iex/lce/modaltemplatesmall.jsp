<script type="text/ng-template" id="modaltemplatesmall.jsp">
  <div class='ng-modal' ng-show='show'>
    <div class='ng-modal-overlay'></div>
    <div class='ng-modal-dialog' ng-style='dialogStyle' style="height:400px">
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' ng-click='hideModal()'>
          <span aria-hidden='true'><i class='icon-remove'></i></span>
          <span class='aria-hidden'>close</span>
        </button>
        <h3> <spring:message code="label.lce.reportChange"/></h3>
      </div>
      <div class='modal-body' ng-transclude></div>
      <div class='hidden'>
        <a class='btn btn-primary ng-modal-close' ng-click='hideModal()'><spring:message code="lce.cancelbutton"/></a>
        <a class='btn btn-primary ng-modal-close' ng-click='hideModal()'><spring:message code="label.lce.save"/></a>
      </div>
    </div>
  </div>
</script>