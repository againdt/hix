/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.converter;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author chopra_s
 * 
 */
@Component("dozzerBooleanConverter")
@Scope("singleton")
public class GlobalBooleanConverter
		extends
		DozerConverter<com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean, Boolean> {

	public GlobalBooleanConverter() {
		super(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean.class,
				Boolean.class);
	}

	@Override
	public com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean convertFrom(
			Boolean arg0,
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean arg1) {
		return null;
	}

	@Override
	public Boolean convertTo(
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean source,
			Boolean dest) {
		if (source != null) {
			return source.isValue();
		}
		return null;
	}

}
