package com.getinsured.iex.hub.ridp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationQuestionType;


public class VerificationQuestionWrapper implements JSONAware{

	private String question = null;
	private List<AnswerChoice> answers = new ArrayList<AnswerChoice>();
	private int questionNumber;
	
	public VerificationQuestionWrapper(VerificationQuestionType xmlRep){
		com.getinsured.iex.hub.ridp.niem.niem.proxy.xsd._2.String nativeStr = xmlRep.getVerificationQuestionText();
		this.question = nativeStr.getValue();	
		List<com.getinsured.iex.hub.ridp.niem.niem.proxy.xsd._2.String> ansList = xmlRep.getVerificationAnswerChoiceText();
		AnswerChoice choice;
		int choiceIndex = 0;
		for(com.getinsured.iex.hub.ridp.niem.niem.proxy.xsd._2.String ans: ansList){
			choiceIndex++;
			choice = new AnswerChoice(choiceIndex, ans.getValue());
			this.answers.add(choice);
		}
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("question", this.question);
		obj.put("questionNumber", this.questionNumber);
		JSONArray answerChoices = new JSONArray();
		for(AnswerChoice ansChoice:this.answers){
			answerChoices.add(ansChoice);
		}
		obj.put("choices", answerChoices);
		return obj.toJSONString();
	}

	public void setQuestionNumber(int questionNo) {
		this.questionNumber = questionNo;
	}
	
	private class AnswerChoice implements JSONAware{
		private int choiceIndex;
		private String choiceText;

		public AnswerChoice(int choiceIndex, String choiceText) {
			this.choiceIndex = choiceIndex;
			this.choiceText = choiceText;
		}

		@SuppressWarnings("unchecked")
		@Override
		public String toJSONString() {
			JSONObject obj = new JSONObject();
			obj.put("choiceIndex", this.choiceIndex);
			obj.put("choiceText", this.choiceText);
			
			JSONObject choiceObject = new JSONObject();
			choiceObject.put("answerChoice", obj);
			return choiceObject.toJSONString();
		}
	}
}


