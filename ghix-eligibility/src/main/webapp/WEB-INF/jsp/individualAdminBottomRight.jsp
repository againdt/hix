<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="oracle.sql.CLOB"%>
<%@include file="datasource.jsp"%>

<html>

<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">

</head>

<body>
	Content of Request / Response, Attachments Content
	<br>
	<%
		Map<String, String[]> parameters = request.getParameterMap();
		try {

			Enumeration parameterList = request.getParameterNames();
			while (parameterList.hasMoreElements()) {
				String sName = parameterList.nextElement().toString();

				if ("request".equalsIgnoreCase(sName)) {
					out.println("Request for ID = "
							+ request.getParameter(sName) + "<br>");
	%>
	<sql:transaction dataSource="${jspDataSource}">
		<sql:query var="specsql">
	select request_payload from gi_ws_payload where gi_ws_payload_id = <%=request.getParameter("request")%>
		</sql:query>
		<c:set value="${specsql.rows[0].request_payload}" var="spec"
			scope="request" />
		<%
			// ugly hack, not my fault.
			oracle.sql.CLOB clob = (oracle.sql.CLOB) request
					.getAttribute("spec");
			long size = clob.length();
			String spec = clob.getSubString(1, (int) size);
			request.setAttribute("spec1", spec);
		%>
	</sql:transaction>

	<textarea name="spec" rows="10" cols="72">
		<c:out value="${spec1}" />
	</textarea>

	<%
		} else if ("response".equalsIgnoreCase(sName)) {
					out.println("Response for ID = "
							+ request.getParameter(sName) + "<br>");
	%>
	<sql:transaction dataSource="${jspDataSource}">
		<sql:query var="specsql">
		select response_payload from gi_ws_payload where gi_ws_payload_id = <%=request.getParameter("response")%>
		</sql:query>
		<c:set value="${specsql.rows[0].response_payload}" var="spec"
			scope="request" />
		<%	
		out.println("Response for ID = "
				+ request.getAttribute("spec") + "<br>");
			// ugly hack, not my fault.
			oracle.sql.CLOB clob=(oracle.sql.CLOB) request.getAttribute("spec");
			long size = clob.length();
			String spec = clob.getSubString(1, (int) size);
			request.setAttribute("spec2", spec);
		%>
	</sql:transaction>

	<textarea name="spec" rows="10" cols="72">
		<c:out value="${spec2}" />
	</textarea>

	<%
		} else if ("exception".equalsIgnoreCase(sName)) {
					out.println("Exception for ID = "
							+ request.getParameter(sName) + "<br>");
	%>
	<sql:transaction dataSource="${jspDataSource}">
		<sql:query var="specsql">
		select EXCEPTION_MESSAGE from gi_ws_payload where gi_ws_payload_id = <%=request.getParameter("exception")%>
		</sql:query>
		<c:set value="${specsql.rows[0].EXCEPTION_MESSAGE}" var="spec"
			scope="request" />
		<%
			// ugly hack, not my fault.
							oracle.sql.CLOB clob = (oracle.sql.CLOB) request
									.getAttribute("spec");
							long size = clob.length();
							String spec = clob.getSubString(1, (int) size);
							request.setAttribute("spec3", spec);
		%>
	</sql:transaction>

	<textarea name="spec" rows="10" cols="72">
		<c:out value="${spec3}" />
	</textarea>

	<%
		} else {
					//out.println( "Not handled "+sName + " = " + request.getParameter( sName ) + "<br>" );
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
	%>

</body>

</html>
