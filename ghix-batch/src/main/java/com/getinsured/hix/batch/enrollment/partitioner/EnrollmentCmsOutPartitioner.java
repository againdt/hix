package com.getinsured.hix.batch.enrollment.partitioner;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrollmentCmsOutPartitioner")
@Scope("step")
public class EnrollmentCmsOutPartitioner implements Partitioner {
	private EnrollmentCmsOutService enrollmentCmsOutService;
	private String hiosIssuerId;
	private String year;
	private String month;
	private String correctionFlag = null;
	List<Integer> applicableYear = null;	
	private int applicableMonth = 0;
	private int partitionSeq = 1;
	private List<Integer> issuerList = null;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCmsOutPartitioner.class);

	@Override
	public Map<String, ExecutionContext> partition(int gridZise) {
//		List<BatchJobExecution> batchExecutionList = enrollmentCmsOutService.getRunningBatchList("enrollmentCmsOutJob");
//		if(batchExecutionList != null && batchExecutionList.size() == 1){
			cleanWipFolder();
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			Date batchStartDate = new Date();
			try{
				setApplicableMonthYear();
				Integer batchYear = LocalDate.now().getYear();
				if("Y".equalsIgnoreCase(correctionFlag)){
					Integer currentYear =  LocalDate.now().getYear();
					Integer currentMonth = LocalDate.now().getMonthValue();
					issuerList = enrollmentCmsOutService.findIssuersForCorrection(currentYear, currentMonth);
					setValuesToPartition(issuerList, partitionMap, currentYear,currentYear, currentMonth, batchStartDate.getTime());
				}else if(null == issuerList){
					for (Integer year : applicableYear) {
						issuerList = enrollmentCmsOutService.findIssuerByYear(Integer.toString(year));
						setValuesToPartition(issuerList, partitionMap, year,batchYear, applicableMonth, batchStartDate.getTime());
					}
				}else{
					for (Integer year : applicableYear) {
						setValuesToPartition(issuerList, partitionMap, year,batchYear, applicableMonth, batchStartDate.getTime());
					}
				}
			}catch(Exception e){
				LOGGER.error("EnrollmentCmsOutPartitioner failed to execute : "+ e.getMessage(), e);
				throw new RuntimeException("EnrollmentCmsOutPartitioner failed to execute : " + e.getMessage()) ;
			}

			return partitionMap;

//		}
//		return null;
	}

	private void cleanWipFolder() {
		StringBuilder cmsXMLFileNameBuilder = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT)
				.append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME);
		//cmsXMLFileNameBuilder.append("C://jobfolder");
		EnrollmentUtils.cleanDirectory(cmsXMLFileNameBuilder.toString());
	}

	/**
	 * Set values to partition map
	 * @param issuerList
	 * @param partitionMap
	 * @param year applicable year
	 * @param batchYear year from UI
	 * @param month
	 * @param batchStartTime 
	 */
	private void setValuesToPartition(List<Integer> issuerList, Map<String, ExecutionContext> partitionMap,
			Integer year, Integer batchYear, Integer month, long batchStartTime) {
		if(null != issuerList && !issuerList.isEmpty()){
			ExecutionContext value = null;
			for (int i = 0;i< issuerList.size(); i++, partitionSeq++) {
				value = new ExecutionContext();
				value.putInt("issuerId", issuerList.get(i));
				value.putInt("year", year);
				value.putInt("partition",  partitionSeq);
				value.putInt("batchYear", batchYear);
				value.putInt("month", month);
				value.putLong("batchStartTime", batchStartTime);
				partitionMap.put("partition - "+issuerList.get(i)+" | "+year+" | "+ partitionSeq, value);
				LOGGER.debug("EnrollmentCmsOutPartitioner : partition():: partition map entry end for partition :: "+i);
			}
		}
	}

	public String getYear() {
		return year;
	}
	
	@Value("#{jobParameters['Year']}")
	public void setYear(String year) {
		if (StringUtils.isNotBlank(year)) {
			this.year = year.trim();
		} else {
			this.year ="-1";
		}
	
	}
	
	private void setApplicableMonthYear() throws GIException{
		int intMonth = 0;
		int intYear = 0;
		String monthCsv = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_PREVIOUS_YEAR_REPORTING_MONTHS);
		List<Integer> configMonthList = new ArrayList<Integer>();
		if(null != monthCsv){
			String[] monthArray = monthCsv.split(",");
			for(String monthStr : monthArray){
				if(NumberUtils.isNumber(monthStr) && Integer.valueOf(monthStr)>0 && Integer.valueOf(monthStr)<=12){
					configMonthList.add(Integer.valueOf(monthStr));
				}
			}
		}
		LocalDate currentDate = LocalDate.now();
		int currentYear = currentDate.getYear();
		int currentMonth = currentDate.getMonthValue();
		
		applicableMonth = currentMonth;
		applicableYear = new ArrayList<Integer>();
		
		if(NumberUtils.isNumber(year)){
			intYear = Integer.parseInt(year);
			if(intYear != -1){
				applicableYear.add(Integer.parseInt(year));
			}else{
				applicableYear.add(currentYear);
				//Config Check
				applicableYear.add(currentYear+1);
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_PREVIOUS_YEAR_REPORTING_REQUIRED)) && configMonthList.contains(applicableMonth)){
					applicableYear.add(currentYear-1);
				}
			}
		}else{
			throw new GIException("Please provide valid year(yyyy)");
		}

	}

	@Value("#{jobParameters['hiosIssuerId']}")
	public void setHiosIssuerId(String hiosIssuerId) {
		if(StringUtils.isNotBlank(hiosIssuerId)){
			this.hiosIssuerId = hiosIssuerId.trim();
			getIssuerList(this.hiosIssuerId);
		}
	}

	private void getIssuerList(String hiosIdList) {
		String[] hiosIdArr = hiosIdList.split("[|]");
		List<Integer> issuerIdList = new ArrayList<Integer>();
		
		for (String hiosId : hiosIdArr) {
			Integer issuerId = enrollmentCmsOutService.getIssuerIdByHiosIssuerId(hiosId.trim());
			if(issuerId != null && issuerId != EnrollmentConstants.ZERO){
				issuerIdList.add(issuerId);
			}
		}
		
		issuerList = new ArrayList<Integer>();
		if(issuerIdList!=null && !issuerIdList.isEmpty()){
			issuerList.addAll(issuerIdList);
		}
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		if (StringUtils.isNotBlank(month)) {
			this.month = month.trim();
		} else {
			this.month = "-1";
		}
	}

	/**
	 * @return the correctionFlag
	 */
	public String getCorrectionFlag() {
		return correctionFlag;
	}

	/**
	 * @param correctionFlag the correctionFlag to set
	 */
	public void setCorrectionFlag(String correctionFlag) {
		if(StringUtils.isNotBlank(correctionFlag)){
			this.correctionFlag = correctionFlag.trim();
		}
	}

	public EnrollmentCmsOutService getEnrollmentCmsOutService() {
		return enrollmentCmsOutService;
	}

	public void setEnrollmentCmsOutService(
			EnrollmentCmsOutService enrollmentCmsOutService) {
		this.enrollmentCmsOutService = enrollmentCmsOutService;
	}
}
