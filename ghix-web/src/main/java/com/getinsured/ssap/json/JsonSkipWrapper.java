package com.getinsured.ssap.json;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Skipping given wrapper property.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonDeserialize(using = JsonSkipWrapperDeserializer.class)
public @interface JsonSkipWrapper {
}
