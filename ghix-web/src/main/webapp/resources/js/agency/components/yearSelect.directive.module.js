(function() {
    'use strict';

    angular.module('yearSelectDirectiveModule', [])
    .directive('yearSelect', yearSelect);
    	
	function yearSelect(){
        return {
            restrict: 'E',
            replace: true,
            scope: '=',
            template: '<select id="yearSelect" class="input-full-width"><option value="">Select Year</option><option ng-repeat="year in years" value="{{year}}">{{year}}</option></select>',
        }
    }



})();
