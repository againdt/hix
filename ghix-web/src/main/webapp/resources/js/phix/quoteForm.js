/**
 * @author Sunil Desu, Felix Cheng
 */
var benefitsApp = angular.module('benefitsApp', ['ngRoute', 'ui.mask', 'ngResource', 'ui.bootstrap','spring-security-csrf-token-interceptor']);

benefitsApp.run(function($rootScope){
  $rootScope.requiredNotice = false;
});

benefitsApp.config(function($interpolateProvider){
        $interpolateProvider.startSymbol('@@').endSymbol('@@');
    });

benefitsApp.config(['$routeProvider', '$tooltipProvider', 
  function($routeProvider, $tooltipProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'pp1',
        controller: 'benefitsControl'
      }).
      when('/results', {
        templateUrl: 'pp2',
        controller: 'resultsControl'
      }).
      when('/doctorVisits', {
        //templateUrl: 'pp3',
        //controller: 'page3'
    	  redirectTo: '/hix/private/saveIndividualPHIX'
      }).
      when('/prescriptions', {
        templateUrl: 'pp4',
        controller: 'page4'
      }).
      otherwise({
        redirectTo: '/'
      });
    
    $tooltipProvider.setTriggers({
	    'blur': 'never',
	    'keyup': 'never',
	    'keypress': 'never',
	    'keydown': 'never'
	  });
  }]);


benefitsApp.constant("FormInfo", {
	zipValid: false,
	incomeValid: false,
	zipWarning: false,
	coverageYear:2016
});

benefitsApp.controller('benefitsControl', ['$rootScope','$scope', '$http', '$location', '$q', '$window', 'FormInfo', 'ajaxFty', function($rootScope, $scope, $http, $location, $q, $window, FormInfo, ajaxFty) {
 
  $rootScope.requiredNotice = true;
  
//	$("#zipCode").popover({
//		trigger: "manual"
//	});
  
  $scope.getParam = function(name){
	  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  
  $scope.checkDateInput = function(){
	  for (var i=0; i < $scope.formInfo.family.length; i++){
		  if ($scope.formInfo.family[i].errorMsg || $scope.formInfo.family[i].birthday ===""){
			  return false;
		  }
	  }
		return true;  
  	}
	  

  	$scope.ageErrorArr = [];
  	$scope.ageErrorTrack = 0; 
	/*$(window).resize(function() {
		$("*").popover('hide');
	});*/
	
	$scope.showIncomeSection = true;
	
	$scope.initPrescreen = function(condition,lead){
	    
	    $scope.formInfo = FormInfo;
	    $scope.formInfo.children = 0; 
	    $scope.formInfo.spouse = 0;
	    $scope.formInfo.seekingCoverage = 0; 
	   
	    if ($scope.formInfo.family){
	      angular.forEach($scope.formInfo.family, function (val, key){
	        if (val.birthday) {
	          val.preloaded = "preloaded";
	          if (val.birthday===""){
	        	  $scope.ageErrorArr[key]= true;
	        	  $scope.ageErrorTrack++;
	          }
	        };
	        
	        if(val.seekingCoverage) {
	          $scope.formInfo.seekingCoverage ++;
	        };
	        
	        if (val.type === "SPOUSE"){
	          $scope.formInfo.spouse++;
	        } else if (val.type === "CHILD"){
	          $scope.formInfo.children++;
	        }
	      });
	      return; 
	    }
	    
	    $('#coverageYear').html($scope.formInfo.coverageYear);
	    
	    if (condition && lead){
	      var response = $http.get('./iex/initpreeligibility?coverageYear='+$scope.getParam('coverageYear'));
	      response.success(function(data, status, headers, config) {

	        $scope.prescreenRequest = data;
	        
	        //Auto-fill if it's a returned user
	        if ($scope.prescreenRequest && !$scope.formInfo.family){
	          var infoObj = $scope.prescreenRequest;
	          
	          if(!$("#gCoverageYear").val()){
	        	  $('#coverageYear').html(infoObj.coverageYear);
	        	  $scope.formInfo.coverageYear = infoObj.coverageYear;
	          }
	          
	          if (!$scope.formInfo.zipValid) {
	            $scope.formInfo.zipCode = infoObj.zipCode;
	          };
	          
	          if($scope.formInfo.zipCode){
	        	  $scope.prepopulatedCounty = infoObj.countyCode;
	              $scope.submitZip();
	          }
	          
	          if(infoObj.householdIncome > 0){
	            $scope.formInfo.income = infoObj.householdIncome;
	          } else if (infoObj.householdIncome === 0 && infoObj.familySize > 0){
	            $scope.formInfo.income = 0;
	          };
	          
	          if(!$scope.formInfo.family && typeof(infoObj)!= 'string'){
	            $scope.formInfo.family = [];
	            var applicantFam = $scope.formInfo.family;
	            if (infoObj.members != [] && infoObj.members.length > 0){

	              angular.forEach(infoObj.members, function(value, key){
	                var individInfo = {};
	                individInfo.id = key;
	                individInfo.type = value.relationshipToPrimary;
	                
	                if (individInfo.type === 'SPOUSE'){
	                  $scope.formInfo.spouse++;
	                } else if (individInfo.type === 'CHILD'){
	                  $scope.formInfo.children++;
	                };
	                
	                if (value.dateOfBirth) {
	                  individInfo.birthday = value.dateOfBirth;
	                  individInfo.preloaded = "preloaded";
	                } else {
	                  individInfo.birthday = "";
    	        	  $scope.ageErrorArr[key]= true;
    	        	  $scope.ageErrorTrack++;
	                }
	                
//	                individInfo.birthday = value.dateOfBirth || "";
	                individInfo.tobaccoUse = (value.isTobaccoUser === "Y"? true : false);
	                individInfo.dobValid = false;
	                
	                individInfo.seekingCoverage = 
	                  (value.isSeekingCoverage === "Y"? (function(){
	                    $scope.formInfo.seekingCoverage ++;
	                    return true;
	                  })() : false);
	                
	                individInfo.removable = (key === 0? false : true);
	                individInfo.nativeAmerican = (value.isNativeAmerican === "Y"? true : false);
	                applicantFam.push(individInfo);
	              });
	            } else if ($scope.formInfo.family.length === 0){
	              $scope.formInfo.family = [{ id: 1, type: "SELF", birthday: "", tobaccoUse: false, dobValid: false, seekingCoverage: true, removable: false, nativeAmerican: false }];
	              $scope.formInfo.seekingCoverage ++;
	        	  $scope.ageErrorArr[0]= true;
	        	  $scope.ageErrorTrack++;
	            }
	          } else {
	            $scope.formInfo.family = [{ id: 1, type: "SELF", birthday: "", tobaccoUse: false, dobValid: false, seekingCoverage: true, removable: false, nativeAmerican: false }];
	            $scope.formInfo.seekingCoverage ++;
	        	  $scope.ageErrorArr[0]= true;
	        	  $scope.ageErrorTrack++;
	          }     
	          $scope.showIncomeSection = $scope.prescreenRequest.showIncomeSection;
//	        } else {
//	          angular.forEach($scope.formInfo.family, function (val, key){
//	            if (val.birthday) {
//	              val.preloaded = "preloaded";
//	            };
//	            
//	            if(val.seekingCoverage) {
//	              $scope.formInfo.seekingCoverage ++;
//	            };
//	            
//	            if (val.type === "SPOUSE"){
//	              $scope.formInfo.spouse++;
//	            } else if (val.type === "CHILD"){
//	              $scope.formInfo.children++;
//	            }
//	          });
	        }
	      });
	      
	      response.error(function(data, status, headers, config) {
	        $scope.formInfo.family = [{ id: 1, type: "SELF", birthday: "", tobaccoUse: false, dobValid: false, seekingCoverage: true, removable: false, nativeAmerican: false }];
	        $scope.formInfo.seekingCoverage ++;
	      });
	    } else {
	      $scope.formInfo.family = [{ id: 1, type: "SELF", birthday: "", tobaccoUse: false, dobValid: false, seekingCoverage: true, removable: false, nativeAmerican: false }];
	      $scope.formInfo.seekingCoverage ++;
	    }
	};
	
	$scope.coverageYearChange = function() {
		$('#coverageYear').html($scope.formInfo.coverageYear);
	};	
	
	$scope.formInfo = FormInfo;
	
	$scope.coveragedateValid = function() {
		$scope.formInfo.covgDateValid = false;
		$scope.covgDateWarning = false;
		$scope.covgDateWarning1 = false;
		$scope.covgDateWarning2 = false;
		var coverageDate1 = $scope.formInfo.coverageDate;
		$scope.formInfo.coverageDate = coverageDate1.slice(0, 2) + "/" + coverageDate1.slice(2, 4) + "/" + coverageDate1.slice(4,8);
		res1 = datemmddyyyy();
		if(!res1){
			$scope.covgDateWarning = true;
			return false;
		}
		res2 = pastDate();
		if(!res2){
			$scope.covgDateWarning1 = true;
			return false;
		}
		res3 = endOfMonth();
		if(!res3){
			$scope.covgDateWarning2 = true;
			return false;
		}
		$scope.formInfo.covgDateValid = true;		    
		return true;
		
	};
	
	datemmddyyyy = function() {
		//alert("date format");
		dob_arr = $scope.formInfo.coverageDate.split("/");
		dob_mm = dob_arr[0];
		dob_dd = dob_arr[1];
		dob_yy = dob_arr[2];

		var birthDate = new Date(),
	        covgDate = new Date($scope.formInfo.coverageDate);
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);
		
		if ((dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear())) {
			return false;
		}
	    if (!(covgDate instanceof Date) || dob_mm.length !== 2 || dob_dd.length !== 2 || dob_yy.length !== 4) {
	        return false;
	    }
	    return true;
		
	};
	
	pastDate = function() {
		//alert("past date");
		var today = new Date();
		var covDateArr = $scope.formInfo.coverageDate.split("/");
		var covDate = new Date(covDateArr[2], covDateArr[0]-1, covDateArr[1]);	
		//alert("covdate" + covDate );
		//alert("today" + today );
		//alert("covDate > today " + (covDate > today) );
		return (covDate > today);
	};
	endOfMonth = function(){
		//alert("end of month");
		dateCompare = 0;
		var oneMonthFromcurrent = new Date();
		 oneMonthFromcurrent.setMonth(oneMonthFromcurrent.getMonth()+1);
		var covDateArr = $scope.formInfo.coverageDate.split("/");
		var covDate = new Date(covDateArr[2], covDateArr[0]-1, covDateArr[1]);
		//alert("covdate" + covDate );
		//alert("oneMonthFromcurrent" + oneMonthFromcurrent );
		if(covDate>oneMonthFromcurrent){
			dateCompare = 1;
		}
		//alert(dateCompare);
		return (dateCompare!=1);
	};

	$scope.submitZip = function() {
		var zipRegEx = /^\d{5}$/g;
		var testResult = zipRegEx.test($scope.formInfo.zipCode);
		if(!testResult) {
//			$("#zipCode").addClass('redBorder');
			$scope.zipWarning = true;
			$scope.formInfo.zipValid = false;
			$scope.formInfo.showMultipleCounties = false;
			$scope.formInfo.showOneCounty = false;
			if($scope.delay) {
				$scope.delay.resolve();
			}
			return false;
		}
//		else {
//			$("#zipCode").popover('hide');
//			$("#zipCode").removeClass('redBorder');
//		}

		var postData = 'zip=' + $scope.formInfo.zipCode;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		};

		var promise = $http.post('/hix/iex/validateZip', postData, config);

		promise.success(function(data, status, headers, config) {
			$scope.handleZipSuccess(data);
		});
		promise.error(function() { 
			$scope.openZipErr = true; 
		});

	};

	$scope.multiCountyChange = function() {
		if(!$scope.formInfo.selectedCounty) {
			$scope.countyNotSelected = true;
		}
		else {
			$scope.countyNotSelected = false;
		}
	};

	$scope.handleZipSuccess = function(data) {
		$scope.formInfo.countyInfo = [];

		for (key in data) {
			$scope.formInfo.countyInfo.push({ 
				name: key, 
				code: data[key].countyCode, 
				state: data[key].stateCode, 
				nameCombined: key + " (" + data[key].stateCode + ")", 
				codeCombined: data[key].countyCode+" "+data[key].stateCode+" "+key}
			);
			if($scope.prepopulatedCounty == data[key].countyCode){
				$scope.formInfo.selectedCounty = data[key].countyCode+" "+data[key].stateCode+" "+key;
			}
		}

		var validStateZip = true;

		if($scope.formInfo.countyInfo.length == 1 && validStateZip) {
			$scope.formInfo.showOneCounty = true;
			$scope.formInfo.showMultipleCounties = false;
			$scope.formInfo.selectedCounty = null;
			$scope.formInfo.zipValid = true;
			$scope.zipWarning = false;

			$scope.formInfo.countyCode = $scope.formInfo.countyInfo[0].code;
			$scope.formInfo.stateCode = $scope.formInfo.countyInfo[0].state;
			$scope.formInfo.countyName =  $scope.formInfo.countyInfo[0].name;

//			$("#zipCode").popover('hide');
//			$("#zipCode").removeClass('redBorder');
			if($scope.delay) {
				$scope.delay.resolve();
			}
			
		}
		else if($scope.formInfo.countyInfo.length > 1 && validStateZip) {
			$scope.formInfo.showMultipleCounties = true;
			$scope.formInfo.showOneCounty = false;
			$scope.formInfo.zipValid = true;
			$scope.zipWarning = false;
//			$("#zipCode").popover('hide');
//			$("#zipCode").removeClass('redBorder');
			if($scope.delay) {
				$scope.delay.resolve();
			}
			else {
				//$scope.formInfo.selectedCounty = null;
//				$scope.countyNotSelected = true;
			}
		}
		else {
			$scope.formInfo.zipValid = false;
			$scope.zipWarning = true;
			$scope.formInfo.showMultipleCounties = false;
			$scope.formInfo.showOneCounty = false;
//			$("#zipCode").addClass('redBorder');
			if($scope.delay) {
				$scope.delay.resolve();
			}
		}
	};

	$scope.seekCoverage= function(ind){
	  if($scope.formInfo.family[ind].seekingCoverage){
	    $scope.formInfo.seekingCoverage--;
	  } else {
	    $scope.formInfo.seekingCoverage++;
	  };
	};
	
	$scope.addSpouse = function() {
		if($scope.formInfo.spouse == 0 && $scope.formInfo.children <= 5) {
			$scope.formInfo.spouse = 1;
			var spouseId = $scope.formInfo.family.length;
			$scope.formInfo.family.push({ id: $scope.formInfo.family.length+1, type: "SPOUSE", birthday: "", dobValid: false, tobaccoUse: false, seekingCoverage: true, removable: true, dobValid: false, nativeAmerican: false });
			$scope.formInfo.seekingCoverage++;
		} 
		else if ($scope.formInfo.spouse != 1){
			$scope.openDependLimit = true;
		}
	};

	$scope.addChild = function() {
		if(($scope.formInfo.children + $scope.formInfo.spouse) <= 5) {
//			$('#incomeEntry *').popover('hide');
			$scope.formInfo.children += 1;
			var childId = $scope.formInfo.family.length+1;
			$scope.formInfo.family.push({ id: childId, type: "CHILD", birthday: "", dobValid: false, tobaccoUse: false, seekingCoverage: true, removable: true, dobValid: false, nativeAmerican: false });
			$scope.formInfo.seekingCoverage++;
		} else {
			$scope.openDependLimit = true;
		};
	};

	$scope.remove = function(index, memberType) {
//		$('#incomeEntry *').popover('hide');
//		$('#family-members-container input.member-birthday').popover('hide');
		if($scope.formInfo.family[index].seekingCoverage){
		    $scope.formInfo.seekingCoverage--;
		  };		
		
		if(memberType == "SPOUSE") {
			$scope.formInfo.spouse = 0;
		} else {
			$scope.formInfo.children -= 1;
		};
		$scope.formInfo.family.splice(index, 1);
	};


	$scope.submitPrescreen = function(submitType) {
	  
	if (!$scope.formInfo.seekingCoverage){
		$scope.seekingCoverageM = true;
		return;
	}; 
	  
    if($scope.formInfo.showOneCounty) {

      $scope.formInfo.countyCode = $scope.formInfo.countyInfo[0].code;
      $scope.formInfo.stateCode = $scope.formInfo.countyInfo[0].state;
      $scope.formInfo.countyName =  $scope.formInfo.countyInfo[0].name;
      
      $scope.formInfo.aptcAllPlansNoAptcCountyName = $scope.formInfo.countyInfo[0].name;
      $scope.formInfo.aptcAllPlansNoAptcStateName = $scope.formInfo.countyInfo[0].state;
    }
    else if($scope.formInfo.showMultipleCounties) {
	  if(!$scope.formInfo.selectedCounty) {
			$scope.countyNotSelected = true;
//			$location.hash("zipAnchor");
//			$anchorScroll();
			$window.scrollTo(0,0);
			return;
      }
      var county_state_split = $scope.formInfo.selectedCounty.split(" ");
      $scope.formInfo.countyCode = county_state_split[0];
      $scope.formInfo.stateCode  = county_state_split[1];
      $scope.formInfo.countyName = county_state_split[2];

      $scope.formInfo.aptcAllPlansNoAptcStateName = $scope.formInfo.stateCode;
      $scope.formInfo.aptcAllPlansNoAptcCountyName = $scope.formInfo.countyName;
    }
    
    ajaxFty.setCounty($scope.formInfo.countyName);
    
		var familySize = $scope.formInfo.family.length;
		
		var income = $scope.formInfo.income || null;
		var zipCode = $scope.formInfo.zipCode;
		var countyCode = $scope.formInfo.countyCode;
		var stateCode = $scope.formInfo.stateCode;
		var coverageStartDate = $scope.formInfo.coverageDate;

		var membersToSubmit = [];

		for (var i = 0; i < $scope.formInfo.family.length; i++) {
			if ($scope.formInfo.family[i].birthday.length === 8){
				var dateOfBirth = $scope.formInfo.family[i].birthday.slice(0, 2) + "/" + $scope.formInfo.family[i].birthday.slice(2, 4) + "/" + $scope.formInfo.family[i].birthday.slice(4,8);
			} else {
				var dateOfBirth = $scope.formInfo.family[i].birthday;
			}

			var isTobaccoUser = ($scope.formInfo.family[i].tobaccoUse) ? "Y" : "N";
			var isSeekingCoverage = ($scope.formInfo.family[i].seekingCoverage) ? "Y" : "N";
			var nativeAmerican = ($scope.formInfo.family[i].nativeAmerican) ? "Y" : "N";
			
			var relationshipToPrimary = $scope.formInfo.family[i].type;
	
			var memberObj = { dateOfBirth: dateOfBirth, isTobaccoUser: isTobaccoUser, isSeekingCoverage: isSeekingCoverage, isNativeAmerican:nativeAmerican,  relationshipToPrimary: relationshipToPrimary };

			membersToSubmit.push(memberObj);
		}

		var JSONtoSubmit = {
			countyCode: countyCode,
			familySize: familySize,
			householdIncome: income,
			members: membersToSubmit,
			stateCode: stateCode,
			zipCode: zipCode,
			callToApiRequired:'Y',
			showIncomeSection:$scope.showIncomeSection,
			coverageYear:$scope.formInfo.coverageYear,
			coverageStartDate:coverageStartDate
		};
			
		var promise = ajaxFty.sendPrescreen(JSONtoSubmit, submitType);
		
		promise.then(function(data){
			  $window.location.href="./preferences";  
		}, function(){
			$scope.openSubmitFail = true;
		});

	};

}]);

benefitsApp.controller('resultsControl', ['$rootScope','$scope', '$http', '$location', '$q', 'FormInfo', 'ajaxFty', function($rootScope, $scope, $http, $location, $q, FormInfo, ajaxFty) {
	$rootScope.requiredNotice = false;

	$scope.handleSavingsData = function() {

		var res = ajaxFty.getPresrResult('result');

		$scope.formInfo = res;
				
		if (res == undefined){
			$location.path('/');
			return;
		}
		
		var maxAptc = res.aptc;
		var aptcVal = parseInt(maxAptc.replace(/\$/g, ''),10);
        var aptcEligible = res.aptcEligible;
        var expectedMonthlyPremium = res.expectedMonthlyPremium;
        var expectedPremium = parseInt(expectedMonthlyPremium.replace(/\$/g, ''),10);
        var lspPremiumAfterAptc = res.lspPremiumAfterAptc;
        var premiumAfterAptc = parseInt(lspPremiumAfterAptc.replace(/\$/g, ''),10);
        var noOfAptcCsr = res.noOfAptcCsr;
        var medicaidEligible = res.medicaidEligible;
        var medicareEligible = res.medicareEligible;
        var chipEligible = res.chipEligible;
        var chipHousehold = res.chipHousehold;
       	var medicareHousehold = res.medicareHousehold;
        var csr = res.csrLevel;
        var extAffHhId = res.affiliateHouseholdId;
        var affiliateId = res.affiliateId;
        var affid = res.affFlowCustomField1;
        var legacySiteUrl = res.legacySiteUrl;
       	var planInfoAvailable = res.planInfoAvailable;
        var callUs = res.callUs;

        $scope.formInfo.aptcAllPlansNoAptcStateName = res.stateName;
        $scope.formInfo.aptcAllPlansNoAptcCountyName = ajaxFty.getPresrResult('countyName');
        $scope.formInfo.expectedPremium=expectedPremium;        
        $scope.formInfo.lspPremiumAfterAptc = premiumAfterAptc;
        if (isNaN($scope.formInfo.lspPremiumAfterAptc)) {
        	$scope.formInfo.lspPremiumAfterAptc = false;
        }

        $scope.count = [];

        //Check for CSR
        $scope.csrResults = false;
        if (csr == 'CS2'||csr == 'CS3'||csr == 'CS4' || csr == 'CS5' || csr == 'CS6') {
            $scope.csrResults = true;
            $scope.count.push("CSR");
        }

        //CHIP
        if (chipEligible == 'Y') {

            $scope.chipResults = true;
            if(res.noOfChip == 1){
                $scope.chipTextOne = true;
                $scope.chipHeaderOne = true;
            }
            else{
                $scope.chipTextTwo = true;
                $scope.chipHeaderTwo = true;
            }

            $scope.count.push("CHIP");

        }
        else {
        	 $scope.chipResults = false;
        }

        //Medicare
        if (medicareEligible == 'Y') {

            $scope.medicareResults = true;
            if(res.noOfMedicare == 1){
                $scope.medicareTextOne = true;
            }
            else{
                $scope.medicareTextTwo = true;
            }

            $scope.count.push("Medicare");

        }

        //Medicaid
        if (medicaidEligible == 'Y') {

            $scope.medicaidResults = true;
            $scope.count.push("Medicaid");

        }

        //APTC with sufficient data i.e APTC > $0
        if(aptcEligible == 'Y' && !isNaN(aptcVal) && aptcVal > 0) {

        	$scope.aptcEligible = true;
        	$scope.showZeroAptc = false;

            if($scope.formInfo.members.length > 1) {
            	$scope.familySizeText = "your family is";

            }else {
       			$scope.familySizeText = "you are";
            }

            //Show max aptc (monthly and yearly)
            $scope.maxAptc = '$' + aptcVal;
            $scope.maxAptcYearly = aptcVal * 12;

			$scope.count.push("APTC>0");
         }
         else if(!isNaN(aptcVal) && aptcVal <= 0) {
         	//alert("APTC Y zero")
         	$scope.aptcEligible = false;
         	$scope.showZeroAptc = true;

        	if($scope.formInfo.members.length > 1) {
            	$scope.familySizeText = "a family";

            }else {
       			$scope.familySizeText = "an individual";
            }
         } else if(aptcEligible=='Y' && maxAptc=='N/A'){
      		$scope.showZeroAptc = true;
     		$scope.showexppremium = true;
     	}


         if(!$scope.aptcEligible) {
         	if($scope.medicaidResults) {
         		$scope.showZeroAptc = false;
         		//Show Medicaid and don't show anything about premiums and tax credits.
         	}
         	if($scope.medicareResults) {
         		var SomebodyUnder65 = 0;
         		if ($scope.formInfo.family){
             		for(var i = 0; i < $scope.formInfo.family.length; i++) {
             			if($scope.formInfo.family[i].age < 65 ) {
             				SomebodyUnder65++;
             			}
             		}
         		}

         		// If you're eligible for Medicare, and nobody is under 65, don't show anything about premiums and tax credits.
         		//if(!SomebodyUnder65) $scope.showZeroAptc = false;
         	}
         	if(aptcEligible=='N' && maxAptc=='N/A' && !($scope.medicaidResults)){
         		$scope.highincome = true;         	
         	}
         }

        if($scope.count.length == 0 && $scope.formInfo.familySize == 1) {
        	$scope.eligiRes = 1; 
        }
        else if($scope.count.length == 0 && $scope.formInfo.familySize > 1) {
        	$scope.eligiRes = 2;
        }
       	else if($scope.count.length > 0 && $scope.formInfo.familySize == 1) {
       		$scope.eligiRes = 3;
        }
        else if($scope.count.length > 0 && $scope.formInfo.familySize > 1) {
        	$scope.eligiRes = 4;
        }

	};

	$scope.back = function() {
		$location.path('/');
	};

	$scope.next = function() {
		//$location.path('/doctorVisits');
		var params = {
				"coverageYear":$scope.formInfo.coverageYear
		};
		var config = {
			params:params
		};
		var promise = $http.post('./iex/updateStageInEligLeadRecord?coverageYear='+$scope.formInfo.coverageYear, config);
		promise.success(function(data, status, headers, config) {
			document.location.href="./private/saveIndividualPHIX";
		});
		promise.error(function() { alert("Please try again"); });
	};

}]);

benefitsApp.controller('page3', ['$scope', '$http', '$location', '$q', 'FormInfo', function($scope, $http, $location, $q, FormInfo) {

	$scope.formInfo = FormInfo;

	$scope.back = function() {
		$location.path('/results');
	};

	$scope.next = function() {
		$location.path('/prescriptions');
	};

	$scope.isChecked = function(value) {
		if($scope.formInfo.frequencyDoctor == value) {
			return true;
		}
		else {
			return false;
		};
	};

}]);

benefitsApp.controller('page4', ['$scope', '$http', '$location', '$q', 'FormInfo', function($scope, $http, $location, $q, FormInfo) {

	$scope.formInfo = FormInfo;

	$scope.back = function() {
		$location.path('/doctorVisits');
	};

	$scope.next = function() {
		alert("not developed yet!");
	};

	$scope.isChecked = function(value) {
		if($scope.formInfo.prescriptions == value) {
			return true;
		}
		else {
			return false;
		};
	};

}]);


benefitsApp.factory('ajaxFty', ['$q', '$http', function($q, $http){
	var storage ={};

	return {
		sendPrescreen: function (infoObj, submitType){
			var deferred = $q.defer();
			var jsonString = JSON.stringify(infoObj);
			var config = {
				headers: { 'Content-Type': 'application/json' },
			};
			
			  var promise = $http.post('./saveAnonymous', jsonString, config);
		
			promise.success(function(data, status, headers, config) {
				if (submitType === "check"){
					result = data;
					storage.result = data;
				};

				if (data.status === "failure") {
					deferred.reject();
				} else {
					deferred.resolve();
				};				
			});
		
			promise.error(function(data, status, headers, config) { 
				deferred.reject();
				});
			
			return deferred.promise;
		},
		
		getPresrResult: function(prop){
			return storage[prop]; 
		},
		
		setCounty: function(name){
			storage.countyName = name;
		},
		
		
	};
}]);

benefitsApp.directive("modalShow", function ($parse) {
	return {
	    restrict: "A",
	    link: function (scope, element, attrs) {

	        //Hide or show the modal
	        scope.showModal = function (visible, elem) {
	            if (!elem)
	                elem = element;

	            if (visible)
	                $(elem).modal("show");                     
	            else
	                $(elem).modal("hide");
	        };

	        //Watch for changes to the modal-visible attribute
	        scope.$watch(attrs.modalShow, function (newValue, oldValue) {
	          scope.showModal(newValue, attrs.$$element);
	        }, true);

	        //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
	        $(element).bind("hide.bs.modal", function () {
	          $parse(attrs.modalShow).assign(scope, false);
	          if (!scope.$$phase && !scope.$root.$$phase)
	              scope.$apply();
	        });
	    }

	};
});


benefitsApp.directive("dateCheck", function () {
	return {
	    restrict: "A",
	    require: "ngModel",
	    link: function (scope, element, attrs, ctrl) {
	    	var calculateAge = function(){	
	    		var dateArr = ctrl.$viewValue.split("/");
	    		var today = new Date();
	    		var cutOff = new Date(2015, 10, 15, 23, 59);

	    		
	    		var getAge= function(dob, effectiveDate){
	    			
	    			//Check for future DOB, irrespective of effective date
	    			var now = new Date();
	    			now.setHours(0,0,0,0);
	    			if(dob > now){
	    				return -1;
	    			}
	    			
	    			effectiveDate = effectiveDate || new Date();
	    			var diff = effectiveDate.getTime() - dob.getTime();
	    			var age = (diff / (1000 * 60 * 60 * 24 * 365.25));
	    			return age
    			};
	    		
//	    		cutOff.setFullYear(2014);
//	    		cutOff.setMonth(10);
//	    		cutOff.setDate(15);
//	    		cutOff.setHours(23);
//	    		cutOff.setMinutes(59);
	    		
	    		if (scope.formInfo.coverageYear == 2016 && cutOff > today){
	    			var effective = new Date(2016, 0, 1, 12, 0);
	    		} else {
		    		var effective = new Date(today);
					//Current Date is on or before 15 of a month
					if(today.getDate() <= 15){
						effective.setMonth(today.getMonth() + 1);
					}
					//Current date is after 15th of the month
					else{
						effective.setMonth(today.getMonth() + 2);
					};	  
					
					effective.setDate(1);
					effective.setHours(0);
					effective.setMinutes(0);
					effective.setSeconds(0);
	    		}
	    		
	    		//HIX-64221 Support for 2016 Issuer Plan Preview
	    		if(!scope.showIncomeSection){
	    			effective = new Date(2016, 0, 1, 12, 0);
	    		}
	    		
				var birthD = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
				birthD.setHours(0); 
				var diffInYr = effective.getFullYear() - birthD.getFullYear()
				
				if (diffInYr == 18){
					if (effective.getMonth() === birthD.getMonth() && effective.getDate() === birthD.getDate()){
						return 18.001;
					}
				} else if (diffInYr == 26){
					if (effective.getMonth() === birthD.getMonth() && effective.getDate() === birthD.getDate()){
						return 26.001;
					}
				}; 
				
				var age = getAge(new Date(birthD), new Date(effective));

				return age;
	    	};
	    	
	    	var validateAge = function(){
	    		var index = attrs.order;
	    		
	    		if (!ctrl.$viewValue){
	    			return;
	    		};
	    		
	    		var dateArr = ctrl.$viewValue.split("/");
	    		
	    		var daysInMo;
	    		
	    		switch(dateArr[0]){
	    		  case "02":
	    		    if (dateArr[2] % 4 === 0){
	    		      daysInMo = 29;
	    		    } else {
	    		      daysInMo = 28;
	    		    };
	    		    break;
	    		  case "04" : case "06" : case "09" : case "11":
	    		    daysInMo = 30;
	    		    break;
	    		  default:
	    		    daysInMo = 31;
	    		}
	    		
	    		if (dateArr[0] > 12 || dateArr[1] > daysInMo || dateArr[0] < 1 || dateArr[1] < 1){
	    			scope.formInfo.family[index].errorMsg = "Please enter valid date";
	    			return false;
	    		}
	    		
	    		var age = calculateAge();
	    		scope.formInfo.family[index].age = age;
	    		
	    		if(attrs.mtype == "SELF") {
					if(age < 18 || age > 120) {
						if(age < 18) {
							scope.formInfo.family[index].errorMsg = "You must be 18 or older.";
							return false;
						}
						else if(age > 120) {
							scope.formInfo.family[index].errorMsg = "Please enter an age below 120.";
							return false;
						}
					}
				} 
				else if(attrs.mtype == "SPOUSE") {
					if(age < 18|| age > 120) {
						if(age < 18) {
							scope.formInfo.family[index].errorMsg = "There are no plans for a spouse of this age.";
							return false;
						}
						else if(age > 120) {
							scope.formInfo.family[index].errorMsg = "Please enter an age below 120.";
							return false;
						};
					}
				} // close Spouse
				else if(attrs.mtype == "CHILD") {
					if(age < 0 || age > 26) {
						if(age > 26) {
							scope.formInfo.family[index].errorMsg = "Children must be under age 26."
							return false;
						}
						else if(age < 0) {
							scope.formInfo.family[index].errorMsg = "Quotes cannot be generated for anyone with a future birth date.";
							return false;
						}

					} 	
					else if (age < 18.0005) {
						scope.formInfo.family[index].tobaccoUse = false;
						scope.formInfo.family[index].disableTobacco = true;
					}
					else {
						scope.formInfo.family[index].disableTobacco = false;
					}
				} 
	    		scope.formInfo.family[index].errorMsg = null;
				return true;
	    	};
 	 		    	
	        scope.$watch(validateAge, function (newValue) {
	          ctrl.$setValidity('age', newValue);
	        });

	    }

	};
});

benefitsApp.directive('popOver', function () {
	  return {
		    restrict: 'A',
		    scope: {
		      shown: '=',
		    },
		    link: function(scope, element) {
		      scope.$watch('shown', function(shown) {
		        if (shown) {
		          element.popover('show');
		        } else {
		          element.popover('hide');
		        }
		      });
		    }
	  };
});

benefitsApp.directive('triggerClick', function ($timeout) {
	  return {
		    restrict: 'A',
		    link: function(scope, element, attrs) {
		    	var condition = function (){
		    		return attrs.triggerClick;
		    	};
	    	
		    	scope.$watch(condition, function(nV, oV){
		    		if (nV) {
		    			$timeout(function(){
		    				element.focus();
		    			}, 0 , false);
			   		} else if (nV === oV){
			   			element.mouseleave();
			   		}
		    	});
		    	
		    }
  	  };
});

(function() {
	var numOnly = function() {
		return {
			restrict : "A",
			require : "ngModel",
			link : function(scope, ele, attrs, ngM) {
				ele.bind("keydown", function(event) {
					if (event.which === 96 || event.which === 48) {
						if (event.target.value === "0"){
							event.preventDefault();
						}
					};
					
					if(event.ctrlKey || event.metaKey) {
						if (event.which === 86 || event.which === 67){
							return;
						}
					}
					
					if (event.which < 8 || event.which > 57) {
						if (event.which >= 96 && event.which <=105){
							return;
						}
						event.preventDefault();
					} else {
						if (event.shiftKey){
							event.preventDefault();
						}
					}
				})
		  }
		}
    };

	angular.module('benefitsApp')
		.directive("numOnly", numOnly)
})();

(function(){
	var numMask = function($filter){
		return {
			require: '?ngModel',
	        link: function (scope, elem, attrs, ctrl) {
	            if (!ctrl) return;
				           
	            ctrl.$formatters.unshift(function (a) {
	            	var temp = $filter(attrs.numMask)(ctrl.$modelValue);
	                return temp;
	            });

	            ctrl.$parsers.unshift(function (viewValue) {
	                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
	                elem.val($filter('number')(plainNumber));
	                if (plainNumber === "" && elem.val() ==="0"){
	                	elem.val("");
	                } else if (elem.val() ==="0" || elem.val() ==="00"){
	                	plainNumber = "0"
	                }
	                return plainNumber;
	            });
	        }
		}
	};
	
	
	angular.module('benefitsApp')
	.directive("numMask", numMask)
})();


(function(){
	var testCheck = function(){
		return{
			require: 'ngModel',
			link: function(scope, ele, attrs, ctrl){
				ele.on('blur', function(){
					//console.log(scope, ele, attrs, ctrl);
					var checker; 
					var ind = attrs.testCheck; 
					var retBool = function(){
						return checker; 
					}
					if (this.value === "mm/dd/yyyy"){
						  scope.formInfo.family[ind].errorMsg = "Please enter valid date as 'mm/dd/yyyy'";
						  checker= false; 
					  } else if (scope.formInfo.family[ind].errorMsg === "Please enter valid date as 'mm/dd/yyyy'"){
						  scope.formInfo.family[ind].errorMsg = null;
						  checker = true;
					  };

				})
			}
		}
	};
	
	angular.module('benefitsApp')
	.directive('testCheck', testCheck)
})();
 
