package com.getinsured.hix.batch.ssap.redetermination.task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicantService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * Spring batch Tasklet to execute Auto renewal of SSAP Applications. 
 * 
 * @author Sahay_B
 *  
 * GI_APP_CONFIG Properties used (iex.ssap.autorenewal.parentappyear, iex.ssap.autorenewal.renewalyear, global.open.enrollment.start.date
 * and global.open.enrollment.end.date)
 * 
 * This batch is expected to run once and leave all {iex.ssap.autorenewal.parentappyear} applications with an  {iex.ssap.autorenewal.renewalyear} 
 * Application in Conditionally eligible open state with cloned applicants and application events. 
 */
@DependsOn("dynamicPropertiesUtil")
public class Process extends StepExecutionListenerSupport implements Tasklet {
	
	private static final String RENEWAL_PROCESS_BATCH = "RENEWAL_PROCESS_BATCH";
	private static final Logger LOGGER = Logger.getLogger(Process.class);
	private static final String FAILURE = "failure";
	private static final String SUCCESS = "success";
	private SsapCloneApplicationService ssapCloneApplicationService;
	private SsapCloneApplicantService ssapCloneApplicantService;
	private SsapApplicationRepository ssapApplicationRepository;
	private SsapApplicantRepository ssapApplicantRepository;
	private GhixRestTemplate ghixRestTemplate;
	private ThreadPoolTaskExecutor taskExecutor;
	private ActiveEnrollmentService activeEnrollmentService;
	private SsapJsonBuilder ssapJsonBuilder;
	private GIMonitorService giMonitorService;
	private EligibilityProgramRepository eligibilityProgramRepository;
	private UserService userService;
	
	private String childRelations;
	private String activeEnrollmentStatusList;
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		long batchSize = new Long(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE));
		activeEnrollmentStatusList = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_ACTIVEENROLLMENTSTATUSLIST);
		childRelations = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_CHILDRELATIONCODE);
		
		LOGGER.info("Ssap Auto Renewal Batch job to Process new cloned Application for " + renewalYear);
		batchSize = chunkContext.getStepContext().getStepExecution().getJobParameters().getLong("BATCH_SIZE",batchSize);
		
		if(batchSize == 0) {
			throw new GIException("Invalid Batch Size");
		}

		List<SsapApplication> renewedApplications = new ArrayList<SsapApplication>();
		try {
			//renewedApplications = ssapCloneApplicationService.getRenewedSsapApplicationsByCoverageYear(renewalYear,batchSize);
		} catch (Exception giExceotion) {
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50007", new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null, giExceotion.getStackTrace().toString(), GIRuntimeException.Component.BATCH.getComponent(), null);
		}
		
		if(renewedApplications != null && renewedApplications.size() == 0) {
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50008", new Date(), this.getClass().getName(), "No cloned application found to renew.", null, "No cloned application found to renew.", GIRuntimeException.Component.BATCH.getComponent(), null);
			return RepeatStatus.FINISHED;
		}
		
		Set<Future<Map<String,String>>> tasks = new HashSet<Future<Map<String,String>>>(renewedApplications.size());
		for (SsapApplication ssapApplication : renewedApplications) {
			LOGGER.info("Processing Parent Application " + ssapApplication.getCaseNumber() + "for renewal year " + renewalYear);
			try {
				tasks.add(taskExecutor.submit(new SsapRedeterminationProcessor(ssapApplication, renewalYear)));
				//new SsapApplicationProcessor(ssapApplication, renewalYear).cloneAndProcessSsapApplication(ssapApplication, renewalYear);
			} catch (Exception giExceotion) {
				try{
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50009", new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null, giExceotion.getStackTrace().toString(), GIRuntimeException.Component.BATCH.getComponent(), null);
				}catch(Exception ex){
						LOGGER.error("Error Processing application - "+ssapApplication.getId(), ex);
				}
			}
		}
		
		waitForAllTaskToComplete(tasks);
		ssapCloneApplicationService.logProcessData(tasks, RENEWAL_PROCESS_BATCH);
		
		return RepeatStatus.FINISHED;

	}


	private void waitForAllTaskToComplete(Set<Future<Map<String,String>>> tasks) {
		boolean batchIsNotCompleted = true; 
		long currentNanoTime = System.nanoTime();
		long elapsedNanoTime = System.nanoTime();
		
		while(batchIsNotCompleted || (elapsedNanoTime/1000000000 >  60 * 15)) {
			boolean isAllTaskCompleted = true;
			for (Future<Map<String,String>> future : tasks) {
				if(!future.isDone()) {
					isAllTaskCompleted = false;
					break;
				}
			}
			batchIsNotCompleted = !isAllTaskCompleted;
			elapsedNanoTime = System.nanoTime() - currentNanoTime;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				LOGGER.error("Thread interrupted from sleep.");
			}
		}
	}
	
	private class SsapRedeterminationProcessor implements Callable<Map<String,String>> {
		
		private static final String EXCEPTION = "exception";
		private static final String PROCESSING_RESULT = "processingResult";
		private SsapApplication currentApplication = null;
		Long renewalYear = null;

		
		public SsapRedeterminationProcessor(SsapApplication currentApplication, Long renewalYear) {
			this.currentApplication = currentApplication;
			this.renewalYear = renewalYear;
		}


		@Override
		public Map<String,String> call() throws Exception {
			return processSsap(currentApplication, renewalYear);
		}

		private Map<String,String> processSsap(SsapApplication currentApplication, Long renewalYear) throws Exception, GIException {
			String processingResult = "ERROR";
			Map<String,String> result = new HashMap<String,String>();
			try {
				LOGGER.info("Started Processing application " + currentApplication.getCaseNumber());
				result.put("applicationId", String.valueOf(currentApplication.getId()));
				long previousEnrolledApplication =  ssapCloneApplicationService.findPreivousYearEnrolledApplication(currentApplication.getCmrHouseoldId());
				if(applicationStatusEquals(RenewalStatus.STARTED)) {
					LOGGER.info("Pending Voluntary Termination check started");				
					boolean isConfirmedEnrollment = isConfirmedEnrollment(previousEnrolledApplication);
					
					if(!isConfirmedEnrollment) {
						currentApplication.setRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
						ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
						//throw new GIException(50005, "No Confirmed Enrollment for Application " + currentApplication.getId() , "High");
					}
					else {
						currentApplication.setRenewalStatus(RenewalStatus.TERMINATION_CHECK_DONE);
						ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
					}
				}
				
				List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(currentApplication);
				
				if(applicationStatusEquals(RenewalStatus.TERMINATION_CHECK_DONE)) {
					if(currentApplication.getFinancialAssistanceFlag().equals("N")){
						boolean isAnyoneSeekingCoverage = dependentAging26Check(currentApplication,ssapApplicants);
						if(isAnyoneSeekingCoverage) {
							currentApplication.setRenewalStatus(RenewalStatus.DEPENDENT_AGE_CHECK_DONE);
						}
						else {
							currentApplication.setRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
							currentApplication.setDentalRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
							ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
							processingResult = "SUCCESS";
							throw new GIException(50005, "Dependent Age Check Failed due to NO ONE SEEKING COVERAGE for Application " + currentApplication.getId() , "High");
						}
					}else{
						currentApplication.setRenewalStatus(RenewalStatus.TO_CONSIDER);
					}
					ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
				}
				//dental enrollment check starts
				Set<String> dentalApplicants =  ssapCloneApplicationService.dependentDentalAgeCheck(currentApplication, ssapApplicants, previousEnrolledApplication, renewalYear);
				if(dentalApplicants != null && !dentalApplicants.isEmpty()){
					//currentApplication.setRenewalStatus(RenewalStatus.DEPENDENT_AGE_CHECK_DONE);
					if(currentApplication.getFinancialAssistanceFlag().equals("N")){
						currentApplication.setDentalRenewalStatus(RenewalStatus.DEPENDENT_AGE_CHECK_DONE);
					}else{
						currentApplication.setDentalRenewalStatus(RenewalStatus.TO_CONSIDER);
					}
				}else{
					currentApplication.setDentalRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
					
				}
				ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
				
				if((applicationStatusEquals(RenewalStatus.DEPENDENT_AGE_CHECK_DONE) || RenewalStatus.DEPENDENT_AGE_CHECK_DONE.equals(currentApplication.getDentalRenewalStatus()))
						&& currentApplication.getFinancialAssistanceFlag().equals("N")){
					
					if(currentApplication.getApplicationStatus().equals(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode())){
							if(RenewalStatus.DEPENDENT_AGE_CHECK_DONE.equals(currentApplication.getDentalRenewalStatus())){
								currentApplication.setDentalRenewalStatus(RenewalStatus.TO_CONSIDER);
							}
							if(RenewalStatus.DEPENDENT_AGE_CHECK_DONE.equals(currentApplication.getRenewalStatus())){
								currentApplication.setRenewalStatus(RenewalStatus.TO_CONSIDER);
							}
							ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
							LOGGER.info("Application is in ER, updating renewal status to TO_CONSIDER " + currentApplication.getCaseNumber());
					}else{
					if(rerunEligibilityForRenewalYear(currentApplication)) {
						LOGGER.info("Waiting for Eligibility Re-run for Application " + currentApplication.getCaseNumber());
					}
				}
				}else{
					
				}
				LOGGER.info("Task completed for application " + currentApplication.getCaseNumber());
				processingResult = "SUCCESS";
			} catch (GIException e) {
				//currentApplication.setRenewalStatus(RenewalStatus.ERROR);
				//ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
				result.put(EXCEPTION, ExceptionUtils.getFullStackTrace(e));
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + e.getErrorCode(), new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				//throw new GIRuntimeException("Error Processing application case number" + currentApplication.getCaseNumber() + " after step " + currentApplication.getRenewalStatus(), e);
			} catch (Exception e) {
				currentApplication.setRenewalStatus(RenewalStatus.ERROR);
				currentApplication.setDentalRenewalStatus(RenewalStatus.ERROR);
				ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001", new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				result.put(EXCEPTION, ExceptionUtils.getFullStackTrace(e));
				processingResult = RenewalStatus.ERROR.toString();
				//throw new GIRuntimeException("Error Processing application case number" + currentApplication.getCaseNumber() + " after step " + currentApplication.getRenewalStatus(), e);
			}
			
			result.put(PROCESSING_RESULT,processingResult);
			return result;
		}


		private boolean applicationStatusEquals(RenewalStatus renewalStatus) {			
			return currentApplication != null && currentApplication.getRenewalStatus().equals(renewalStatus);
		}			
		
		
		private boolean isConfirmedEnrollment(Long parentApplicationId) throws GIException, RestClientException, JsonProcessingException {
			List<String> validActiveEnrollmentStatus = new ArrayList<String>();

			String[] activeEnrollmentStatusArr = activeEnrollmentStatusList.split(",");
			
			for (String activeEnrollmentStatus : activeEnrollmentStatusArr) {
				validActiveEnrollmentStatus.add(activeEnrollmentStatus);
			}
		
			if(validActiveEnrollmentStatus.size() == 0) {
				validActiveEnrollmentStatus.add("CONFIRM");
				validActiveEnrollmentStatus.add("PENDING");
			}
			
			return activeEnrollmentService.isConfirmedEnrollment(parentApplicationId, validActiveEnrollmentStatus, "exadmin@ghix.com");
		}


		private boolean dependentAging26Check(SsapApplication currentApplication,List<SsapApplicant> ssapApplicants) throws Exception {
			List<String> bloodRelations = new ArrayList<String>();
			String[] childRelationArr = childRelations.split(",");
			
			for (String childRelation : childRelationArr) {
				bloodRelations.add(childRelation);
			}
		
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			
			List<BloodRelationship> relationShip = members.get(0).getBloodRelationship();
			
			
/*			if(isPrimaryNotSeekingCoverage(ssapApplicants)  && isChildOnlyPlan(relationShip, bloodRelations)) {
				return true;
			}*/
			
			Date coverageStartDate = getCoverageStartDate() ; 
			
			for (BloodRelationship bloodRelationship : relationShip) {
				if (bloodRelationship.getRelatedPersonId() != null && Integer.parseInt(bloodRelationship.getRelatedPersonId()) == 1) {
					if(bloodRelations.contains(bloodRelationship.getRelation())){	
						for (SsapApplicant ssapApplicant : ssapApplicants) {
							if(ssapApplicant.getPersonId() == Integer.parseInt(bloodRelationship.getIndividualPersonId())) {
								if(checkForAge26(ssapApplicant.getBirthDate(), coverageStartDate)) {
									ssapApplicant.setApplyingForCoverage("N");
									for (HouseholdMember householdMember : members) {
										if(householdMember.getPersonId() == ssapApplicant.getPersonId()) {
											householdMember.setApplyingForCoverageIndicator(false);
											ssapApplicant = ssapCloneApplicantService.saveSsapApplicant(ssapApplicant);
											createProgramEligibilityForQhp(ssapApplicant, "FALSE");											
										}
									}
								}
							}
						}
					}
				}
			}
			
			currentApplication.setApplicationData(ssapJsonBuilder.transformToJson(applicationData));
			ssapCloneApplicationService.saveSsapSepApplication(currentApplication, true);
			
			return isAnyoneSeekingCoverage(ssapApplicants);
		}
		
		private boolean isAnyoneSeekingCoverage(
				List<SsapApplicant> ssapApplicants) {
			boolean isAnyoneSeekingCoverage = false;
			
			for (SsapApplicant ssapApplicant : ssapApplicants) {
				if(ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")) {
					isAnyoneSeekingCoverage = true;
				}
			}
			
			return isAnyoneSeekingCoverage;
		}

		private void createProgramEligibilityForQhp(SsapApplicant ssapApplicant, String indicatorVal) {

			Date currentDate = new Date();
			EligibilityProgram eligibilityProgramDO = new EligibilityProgram();
			eligibilityProgramDO.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
			eligibilityProgramDO.setEligibilityIndicator(indicatorVal);
			eligibilityProgramDO.setEligibilityStartDate(currentDate);
			//DateTime date = new DateTime().withTime(23, 59, 59, 999);
			eligibilityProgramDO.setEligibilityEndDate(new Date());
			eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
			eligibilityProgramDO.setSsapApplicant(ssapApplicant);
			eligibilityProgramDO.setIneligibleReason("Not Seeking Coverage due to Aging Out");
			eligibilityProgramRepository.save(eligibilityProgramDO);
		}


		private Date getCoverageStartDate() throws GIException {
			return ssapCloneApplicationService.getCoverageStartDate(currentApplication);
		}


		private boolean checkForAge26(Date birthDate, Date coverageStartDate) {
			Calendar dob = Calendar.getInstance();  
			dob.setTime(birthDate);  
			Calendar today = Calendar.getInstance();  
			today.setTime(coverageStartDate);
			int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
			if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			  age--;  
			} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
			    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			  age--;  
			}
			
			return (age >= 26);
		}
		
	}
	
	public String invokeSSAPIntegration(long applicationId) {
		String responseMessage=FAILURE;
		 LOGGER.debug("Invoking the SSAP orchestration flow");
		 try {
			 ghixRestTemplate.exchange(GhixEndPoints.SsapIntegrationEndpoints.RENEWAL_INTEGRATION_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_XML, String.class, String.valueOf(applicationId));
			 //restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL,applicationId, String.class);
			 responseMessage = SUCCESS;
		 }catch(Exception e) {
			 LOGGER.error("Exception in invokeSSAPIntegration", e);
			 responseMessage = FAILURE;
		 }	     
	     return responseMessage;
	 }	


	private boolean rerunEligibilityForRenewalYear(SsapApplication currentApplication) throws Exception {
		currentApplication.setApplicationStatus(ApplicationStatus.SIGNED.getApplicationStatusCode());
		ssapCloneApplicationService.saveSsapSepApplication(currentApplication, false);
		
		invokeSSAPIntegration(currentApplication.getId());
		return false;
		
	}	
	
	private boolean isPrimaryNotSeekingCoverage(
			List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if(ssapApplicant.getPersonId() == 1 && ssapApplicant.getApplyingForCoverage().equals("N")) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean isChildOnlyPlan(List<BloodRelationship> relationShip,
			List<String> childRelations) {
		boolean isChildOnlyPlan = true;
		for (BloodRelationship bloodRelationship : relationShip) {
			if (Integer.parseInt(bloodRelationship.getIndividualPersonId()) == 1) {
				if(!childRelations.contains(bloodRelationship.getRelation()) && !wardRelation(bloodRelationship.getRelation())){
					return false;
				}
			}
		}
		return isChildOnlyPlan;
	}	

	private boolean wardRelation(String relation) {
		if("15".equals(relation)) {
			return true;
		}
		return false;
	}


	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}


	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}


	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}


	public SsapApplicantRepository getSsapApplicantRepository() {
		return ssapApplicantRepository;
	}


	public void setSsapApplicantRepository(
			SsapApplicantRepository ssapApplicantRepository) {
		this.ssapApplicantRepository = ssapApplicantRepository;
	}


	public ActiveEnrollmentService getActiveEnrollmentService() {
		return activeEnrollmentService;
	}


	public void setActiveEnrollmentService(
			ActiveEnrollmentService activeEnrollmentService) {
		this.activeEnrollmentService = activeEnrollmentService;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	public GIMonitorService getGiMonitorService() {
		return giMonitorService;
	}

	public void setGiMonitorService(GIMonitorService giMonitorService) {
		this.giMonitorService = giMonitorService;
	}

	public EligibilityProgramRepository getEligibilityProgramRepository() {
		return eligibilityProgramRepository;
	}

	public void setEligibilityProgramRepository(
			EligibilityProgramRepository eligibilityProgramRepository) {
		this.eligibilityProgramRepository = eligibilityProgramRepository;
	}


	public SsapCloneApplicationService getSsapCloneApplicationService() {
		return ssapCloneApplicationService;
	}


	public void setSsapCloneApplicationService(
			SsapCloneApplicationService ssapCloneApplicationService) {
		this.ssapCloneApplicationService = ssapCloneApplicationService;
	}


	public SsapCloneApplicantService getSsapCloneApplicantService() {
		return ssapCloneApplicantService;
	}


	public void setSsapCloneApplicantService(
			SsapCloneApplicantService ssapCloneApplicantService) {
		this.ssapCloneApplicantService = ssapCloneApplicantService;
	}


	public UserService getUserService() {
		return userService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	
}
