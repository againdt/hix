package com.getinsured.eligibility.ssap.integration.notices;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TimeShifterUtil;

import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.PassiveEnrollmentRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;

@Service("agedOutDisenrollmentNoticeService")
public class AgedOutDisenrollmentNoticeServiceImpl implements	AgedOutDisenrollmentNoticeService {

	private static final Logger lOGGER = LoggerFactory.getLogger(AgedOutDisenrollmentNoticeServiceImpl.class);
	private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);


	@Autowired private Gson platformGson;
	@Autowired SsapApplicationRepository ssapApplicationRepository;
	//@PersistenceUnit private EntityManagerFactory emf;
	@Autowired	private NoticeService noticeService;
	@Autowired	private LookupService lookupService;
	@Autowired	private  PassiveEnrollmentRepository passiveEnrollmentRepository;
	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	private static String dentalAgeoutAge = "19";
	private static String healthAgeoutAge = "26";
		
	public String processAgeOutDependents(String caseNumber) {
		
		String ecmId=null;
	
		
		DateTime runDate = DateTime.now();
		DateTime expiryDate = runDate.plusMonths(2);
		
			SsapApplication application = getApplicationData(caseNumber);
			lOGGER.debug("processing caseNumber="+ caseNumber );
			AgeOutNoticeDTO data = prepareNoticeData(application, runDate, expiryDate);
			if (data != null){
				try {
					ecmId=generate(data, application, "EE005QHPAgeOutNotice");
					if (data != null && data.getHealthDependants() != null
							&& data.getHealthDependants().size() > 0) {
						for (AgeOutDependant ageOutDependant : data
								.getHealthDependants()) {
							String depAgeOutEcmId=generateDependant(ageOutDependant,data, application,
									"EE023QHPAgeOutDependantNotice");
							lOGGER.info("Dependent Age out notice sent for " +caseNumber +" at " +  depAgeOutEcmId);
						}
					}
				
				} catch (NoticeServiceException e) {
					lOGGER.info("Age out Notiifcation not generated for " +caseNumber);
				}
			}return ecmId;
		}

	
	@SuppressWarnings("unused")
	private String generateDependant(AgeOutDependant ageOutDependant,
			AgeOutNoticeDTO ageOutNoticeDTO, SsapApplication application, String noticeTemplateName) throws NoticeServiceException {
		int moduleId = ageOutNoticeDTO.getHouseholdId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName();
		Location location = fetchPrimarySsapApplicantAddress(application.getSsapApplicants());

		String relativePath = "cmr/" + moduleId + "/ssap/" + application.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId  + (new TSDate().getTime()) + ".pdf";
		String emailId = ageOutNoticeDTO.getEmailId();
		List<String> validEmails = emailId != null ? Arrays.asList(emailId): null;
		Notice notice = null;
		notice = noticeService
				.createModuleNotice(
						noticeTemplateName,
						GhixLanguage.US_EN,
						getReplaceableObjectData(ageOutNoticeDTO,application,ageOutDependant),
						relativePath,
						ecmFileName,
						moduleName,
						moduleId,
						validEmails,
						DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
						fullName, location, GhixNoticeCommunicationMethod.Mail);

		return notice.getEcmId();
		
	}

	private String  getLookupValueId(){
		  
		String lookUpValueids =lookupService.getlookupValueIdByTypeANDLookupValueCode("ENROLLMENT_STATUS", "CONFIRM")+ ","
				+ lookupService.getlookupValueIdByTypeANDLookupValueCode("ENROLLMENT_STATUS","PENDING")+ ","
				+ lookupService.getlookupValueIdByTypeANDLookupValueCode("ENROLLMENT_STATUS","APPROVED");
		return lookUpValueids;
	}
	
	private String generate(AgeOutNoticeDTO ageOutNoticeDTO, SsapApplication ssapApplication,String noticeTemplateName) throws NoticeServiceException {

		int moduleId = ageOutNoticeDTO.getHouseholdId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName();
		Location location = fetchPrimarySsapApplicantAddress(ssapApplication.getSsapApplicants());

		String relativePath = "cmr/" + moduleId + "/ssap/" + ssapApplication.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId  + (new TSDate().getTime()) + ".pdf";
		String emailId = ageOutNoticeDTO.getEmailId();
		List<String> validEmails = emailId != null ? Arrays.asList(emailId): null;
		Notice notice = null;
		notice = noticeService
				.createModuleNotice(
						noticeTemplateName,
						GhixLanguage.US_EN,
						getReplaceableObjectData(ageOutNoticeDTO,ssapApplication,null),
						relativePath,
						ecmFileName,
						moduleName,
						moduleId,
						validEmails,
						DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
						fullName, location, GhixNoticeCommunicationMethod.Mail);

		return notice.getEcmId();
	}
	
	private Map<String, Object> getReplaceableObjectData(AgeOutNoticeDTO ageOutNoticeDTO,
		SsapApplication ssapApplication, AgeOutDependant ageOutDependant) throws NoticeServiceException {
			Map<String, Object> tokens = new HashMap<String, Object>();
			tokens.put("primaryApplicantName", ageOutNoticeDTO.getPrimaryFirstName()+ " " +ageOutNoticeDTO.getPrimaryLastName());
			tokens.put("ApplicationID", ssapApplication.getCaseNumber());
			tokens.put("Date", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
			tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			tokens.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
			tokens.put("exgCityName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
			tokens.put("exgStateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
			tokens.put("zip",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
			tokens.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			tokens.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
			tokens.put("exchangeAddressEmail",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			tokens.put(TemplateTokens.EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			tokens.put("ageOutDependents",ageOutNoticeDTO);
			SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
			tokens.put("spanishDate", formatter.format(new TSDate()));
			tokens.put("caseNumber", ssapApplication.getCaseNumber());
			tokens.put("ssapApplicationId", ssapApplication.getId());
			if(ageOutDependant!=null)
			{
				tokens.put("ageOutDependentName",ageOutDependant.getFirstName()+" " +ageOutDependant.getLastName());
			}
			
			return tokens;
	}
	
	/*private List<String> getCaseNumbers() {
		
		List<String> rsObj = new ArrayList<>();
		String lookupValueIds = getLookupValueId();
		if(StringUtils.isBlank(lookupValueIds.replace(",",""))){
			rsObj.add("NoLookupFound");
			return  rsObj;
		}
		
		EntityManager em = null;
		
		try {
			String query ="select distinct a.case_number  from ssap_applicants p , ssap_applications a "
					+ ", enrollment en "
					+ " where p.ssap_application_id=a.id "//and a.id='188072'"
					+ " and p.birth_date is not null"
					+ " and a.application_status='EN'"
					+ " and en.ssap_application_id=a.id"
					+ " and en.ENROLLMENT_STATUS_LKP  in ("
					+ lookupValueIds + ")";
			lOGGER.debug("query starting  at   "+ new Timestamp(TimeShifterUtil.currentTimeMillis()));
			rsObj = emf.createEntityManager().createNativeQuery(query).getResultList();
			lOGGER.debug("query ending  at   "+ new Timestamp(TimeShifterUtil.currentTimeMillis()));
		} catch (Exception e) {
			lOGGER.debug(e);
		}
		return rsObj;
	}*/
    
	Date getBirthDateGivenYearsBack(int givenYears) {
		return DateTime.now().plusMonths(2).minusYears(givenYears).toDate();
	}
	
	private SsapApplication getApplicationData(String caseNumber) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		return ssapApplications.get(0);
	}

	private SsapApplication getApplication(long appid) {
		return ssapApplicationRepository.findOne(appid);
	}
	
	private static final String CHILD = "child";
	private static final String SSAP_TAX_HOUSEHOLD_PATH = "$.singleStreamlinedApplication.taxHousehold[*]";

	@SuppressWarnings("serial")
	private static final Map<String , String> BLOOD_RELATIONS = new HashMap<String , String>() {{
		put("14", CHILD);
		put("03",CHILD);
		put("16",CHILD);
	}};

	
	private class HouseholdMemberComparator implements Comparator<HouseholdMember>{

		@Override
		public int compare(HouseholdMember o1, HouseholdMember o2) {
			return o1.getPersonId().compareTo(o2.getPersonId());
		}
	}
	
	private AgeOutNoticeDTO prepareNoticeData(SsapApplication application, DateTime runDate, DateTime expiryDate) {
		long time = System.currentTimeMillis();
		List<Enrollment> enrollments = passiveEnrollmentRepository.findActiveEnrollmentBySsapApplicationId(application.getId());
		if(lOGGER.isDebugEnabled()){
			lOGGER.debug("passiveEnrollmentRepository took {} ms  ", (System.currentTimeMillis()- time));
		}
		
		String healthPlanName = getPlanName("HLT",enrollments);
		String dentalPlanName = getPlanName("DEN",enrollments);
		
		return prepareNotice(application, runDate, expiryDate, getEnrolleesByPlan("HLT",enrollments), getEnrolleesByPlan("DEN",enrollments), healthPlanName , dentalPlanName);
	}
	
	private String getPlanName(String planType, List<Enrollment> enrollments) {
		String planName="";
		for(Enrollment enrollment :enrollments){
			if (planType.equals(enrollment.getInsuranceTypeLkp().getLookupValueCode())){
				planName= enrollment.getPlanName();
			}
		}
		return planName;
	}


	private Set<String> getEnrolleesByPlan(String planType,List<Enrollment> enrollments){
		Set<String> enrollees = new HashSet<String>();
		for(Enrollment enrollment :enrollments){
			if (planType.equals(enrollment.getInsuranceTypeLkp().getLookupValueCode())){
				for(Enrollee enrollee: enrollment.getEnrollees()){
					enrollees.add(enrollee.getExchgIndivIdentifier());
					//healthPlanName = enrollment.getPlanName();
				}
			}
		}
		return enrollees;
	}
	
	private AgeOutNoticeDTO prepareNotice(SsapApplication application, DateTime runDate, DateTime expiryDate,Set<String> healthEnrollees, Set<String> dentalEnrollees, String healthPlanName, String dentalPlanName){ 
		String ssapJson = application.getApplicationData();  
		String caseNumber = application.getCaseNumber();
		TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>> token = new TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>>(){};
		String taxhh = JsonPath.read(ssapJson,SSAP_TAX_HOUSEHOLD_PATH).toString();
		lOGGER.debug(" taxhh: "+taxhh);
		List<com.getinsured.iex.ssap.TaxHousehold> thhList = platformGson.fromJson(taxhh, token.getType());
		if (thhList == null || thhList.size() == 0){
			throw new GIRuntimeException();
		}

		List<HouseholdMember> txMembers = thhList.get(0).getHouseholdMember();
		lOGGER.debug("house hold members "+ txMembers);
		if (txMembers == null || txMembers.size() == 0){
			throw new GIRuntimeException();
		}

		java.util.Collections.sort(txMembers, new HouseholdMemberComparator());
		
		List<BloodRelationship> bloodRelations = txMembers.get(0).getBloodRelationship();
		
		if (txMembers.get(0).getHouseholdContact() == null || txMembers.get(0).getHouseholdContact().getHomeAddress() == null
				|| StringUtils.isEmpty(txMembers.get(0).getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode())
				|| StringUtils.isEmpty(txMembers.get(0).getHouseholdContact().getHomeAddress().getPostalCode())){
			throw new GIRuntimeException();
		}
		
		List<AgeOutDependant> healthDepandants = new ArrayList<AgeOutDependant>();
		List<AgeOutDependant> dentalDepandants = new ArrayList<AgeOutDependant>();
		String primaryFirstName= txMembers.get(0).getName().getFirstName();
		String lastFirstName   = txMembers.get(0).getName().getLastName();
		for (HouseholdMember householdMember : txMembers) {

			BloodRelationship brss = bloodRelations.get(bloodRelations.indexOf(BloodRelationship.build("1", householdMember.getPersonId().toString())));
			if (BLOOD_RELATIONS.containsKey((brss.getRelation()))){
				DateTime memberDoB =new DateTime(householdMember.getDateOfBirth());
				lOGGER.debug(" memberDoB : "+ memberDoB);
				/*health*/
				DateTime twentySixYearDoB = memberDoB.plusYears(26);
				if (healthEnrollees.contains(householdMember.getApplicantGuid()) &&	twentySixYearDoB.isAfter(runDate) && twentySixYearDoB.isBefore(expiryDate)) {
					healthDepandants.add( createAgeoutDependantsList(healthPlanName,householdMember, twentySixYearDoB, this.healthAgeoutAge));
				}
				DateTime ninteenYearDoB = memberDoB.plusYears(19);
				if (dentalEnrollees.contains(householdMember.getApplicantGuid()) && ninteenYearDoB.isBefore(expiryDate) && ninteenYearDoB.isAfter(runDate)) {
					dentalDepandants.add( createAgeoutDependantsList(dentalPlanName,householdMember, ninteenYearDoB, this.dentalAgeoutAge));
				}
			}
		}
		return populateAgeOutDto(healthDepandants,dentalDepandants,caseNumber,primaryFirstName,lastFirstName , getEmailId(application), application.getCmrHouseoldId());
	}
	
	
	private AgeOutNoticeDTO populateAgeOutDto( List<AgeOutDependant> healthDepandants,List<AgeOutDependant> dentalDepandants,String caseNumber,String primaryFirstName,String lastFirstName,String emailAddress, BigDecimal cmrId){
		AgeOutNoticeDTO result = null;
		if (!(healthDepandants.isEmpty() &&  dentalDepandants.isEmpty())){
			result = new AgeOutNoticeDTO();
			result.setApplicationID(caseNumber);
			result.setPrimaryFirstName(primaryFirstName);
			result.setPrimaryLastName(lastFirstName);
			result.setEmailId(emailAddress);
			result.setHouseholdId(cmrId == null ? 0 : cmrId.intValue());
			if(!(healthDepandants.isEmpty())){
				result.setHealthDependants(healthDepandants);
			}
			if(!(dentalDepandants.isEmpty())){
				result.setDentalDependants(dentalDepandants);
			}
		}
		return result;
	}

	private AgeOutDependant createAgeoutDependantsList(String planName, HouseholdMember householdMember, DateTime currentDoB,String age) {
			String dateFormat = "MMMM dd, YYYY";
			if ("MN".equalsIgnoreCase(STATE_CODE)) {
				dateFormat = "MMMM d, YYYY";
			}
			AgeOutDependant healthDependant = new AgeOutDependant();
			healthDependant.setFirstName(householdMember.getName().getFirstName()!= null?householdMember.getName().getFirstName():" ");
			healthDependant.setLastName(householdMember.getName().getLastName()!= null?householdMember.getName().getLastName():" ");
			DateTime dropoutDate = currentDoB.plusMonths(1).withDayOfMonth(1).minusDays(1);	
			healthDependant.setDropOutDate(DateUtil.dateToString(dropoutDate.toDate(), dateFormat));
			healthDependant.setPlanId(planName);
			healthDependant.setDropOutReasonText(householdMember.getName().getFirstName()+ " will be " + age + " years old on %s",
					DateUtil.dateToString(currentDoB.toDate(), dateFormat));
			lOGGER.debug(" dependant  added : "+ healthDependant.getFirstName() + " " +healthDependant.getLastName());
			return healthDependant;
	}
	
	private Location fetchPrimarySsapApplicantAddress(
			List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return fetchPrimarySsapApplicantAddress(ssapApplicant);
			}
		}
		return null;

	}
	
	private Location fetchPrimarySsapApplicantAddress(SsapApplicant ssapApplicant) {
		int locationid = ssapApplicant.getMailiingLocationId() != null ? ssapApplicant.getMailiingLocationId().intValue() : 0;
		if (locationid == 0) {
			locationid = ssapApplicant.getOtherLocationId() != null ? ssapApplicant.getOtherLocationId().intValue() : 0;
		}
		return iLocationRepository.findOne(locationid);
	}
	
	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}
		return null;
	}
}
