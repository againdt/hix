package com.getinsured.hix.admin.web;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;

public class AdminControllerTest extends BaseTest {

	@Autowired NoticeTypeRepository NoticeTypeRepo;
	
	@Test
	public void testNoticeTypeList() {
		List<NoticeType> noticeType = NoticeTypeRepo.findAll();
		assertNotNull(noticeType);
		assert noticeType.size()>0;
	}

	@Test
	public void testEditNoticeType() {
		Integer id = 1;
		NoticeType noticeType =  NoticeTypeRepo.findById(id);
		assertNotNull(noticeType);
		//assertEquals(noticeType.getId(),1);
	}

	

}
