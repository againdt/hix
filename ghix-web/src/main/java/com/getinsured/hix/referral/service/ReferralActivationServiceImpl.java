package com.getinsured.hix.referral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.eligibility.referral.ui.dto.VerificationQuestion;
import com.getinsured.hix.dto.broker.BrokerRequestDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.referral.exception.ReferralServiceException;
import com.getinsured.hix.referral.repository.ReferralActivationDocumentRepository;
import com.getinsured.hix.referral.util.WebReferralConstants;
import com.getinsured.hix.referral.util.WebReferralUtil;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.referral.ReferralLinkingOverride;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@DependsOn("dynamicPropertiesUtil")
@Component("referralActivationService")
@Scope("singleton")
public class ReferralActivationServiceImpl implements ReferralActivationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralActivationServiceImpl.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	private ContentManagementService ecmService;

	@Autowired
	@Qualifier("referralActivationDocumentRepository")
	private ReferralActivationDocumentRepository referralActivationDocumentRepository;

	@Autowired
	private TicketMgmtUtils ticketMgmtUtils;

	@Autowired
	private RoleService roleService;

	private static final int CSR_VERIFICATION_PENDING = 1;

	@Override
	public int bindUserCmrToReferralActivation(int ssap, int cmr, int user) {
		final int referralActivation = restCallToBindUser(ssap, cmr, user);
		return referralActivation;
	}

	private int restCallToBindUser(final int ssap, final int cmr, final int user) {
		int returnValue = WebReferralConstants.FAILURE;
		ResponseEntity<Long> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_ACTIVATION_LINK_CMR, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Long.class, new int[] { user, cmr, ssap });

		if (responseEntity == null || responseEntity.getBody().equals(WebReferralConstants.FAILURE)) {
			LOGGER.warn("restCallToBindUser failure ");
		} else {
			returnValue = responseEntity.getBody().intValue();
		}
		return returnValue;
	}

	@Override
	public ReferralActivation fetchReferralActivationObject(final int id) {
		ReferralActivation referralActivation = null;

		@SuppressWarnings("serial")
		final Map<String, Integer> request = new HashMap<String, Integer>() {
			{
				put("id", id);
			}
		};

		final ResponseEntity<ReferralActivation> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_ACTIVATION, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, ReferralActivation.class, request);
		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.warn("fetchReferralActivationObject failure ");
		} else {
			referralActivation = responseEntity.getBody();
		}
		return referralActivation;
	}

	@Override
	public ReferralVerification populateReferralVerification(final ReferralActivation referralActivation) {
		ReferralVerification referralVerification = new ReferralVerification();
		referralVerification.setReferralActivation(referralActivation);

		referralVerification.getVerificationQuestions().add(QuestionHelper.getQuestion1());
		referralVerification.getVerificationQuestions().add(QuestionHelper.getQuestion2());
		referralVerification.getVerificationQuestions().add(QuestionHelper.getQuestion3());

		return referralVerification;
	}

	private static class QuestionHelper {
		static VerificationQuestion getQuestion1() {
			return new VerificationQuestion("firstName", "label.referral.quesFirstName");
		}

		static VerificationQuestion getQuestion2() {
			return new VerificationQuestion("lastName", "label.referral.quesLastName");
		}

		static VerificationQuestion getQuestion3() {
			return new VerificationQuestion("dob", "label.referral.quesDateOfBirth");
		}
	}

	@Override
	public int validateSecurityQuestions(final ReferralVerification referralVerification) {
		int returnValue = WebReferralConstants.FAILURE;
		
		ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.VALIDATE_REFERRAL_ANSWERS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, referralVerification);
		if (responseEntity == null || !responseEntity.getBody().equals(1)) {
			LOGGER.warn("validateSecurityQuestions failure ");
			if(null != responseEntity) {
			returnValue = responseEntity.getBody();
			}
		} else {
			returnValue = WebReferralConstants.SUCCESS;
		}
		return returnValue;
	}

	@Override
	public int uploadDocumentForVerification(CommonsMultipartFile documentFile, String documentType, int id) throws Exception {
		int returnStatus = WebReferralConstants.DOCUMENT_FAILURE;
		String ecmDocumentId = null;
		String fileName = null;
		try {
			final AccountUser user = getLoggedInUser();
			final String relativePath = user.getId() + "/" + WebReferralConstants.REFERRAL_ACTIVATION_DOCUMENTS;
			if (documentFile != null) {
				fileName = documentFile.getOriginalFilename();
				// Upload file to ECM using the Content Management service
				ecmDocumentId = ecmService.createContent(relativePath, WebReferralUtil.generateFileName(documentFile.getOriginalFilename()), documentFile.getBytes());
				final ConsumerDocument consumerDocument = createConsumerDocument(user, ecmDocumentId, fileName, documentType, id);
				createNewReferralTicket(consumerDocument, user, id);
				returnStatus = restCallToUpdateReferralStatus(id);
			}
		} catch (Exception e) {
			LOGGER.error("error while uploading document for" + id + " " + e);
		}

		return returnStatus;
	}

	private int restCallToUpdateReferralStatus(int id) {
		int returnValue = WebReferralConstants.FAILURE;
		ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_ACTIVATION_STATUS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, new int[] { id,
		        CSR_VERIFICATION_PENDING });

		if (responseEntity == null || !responseEntity.getBody().equals(1)) {
			LOGGER.warn("restCallToUpdateReferralStatus failure ");
		} else {
			returnValue = WebReferralConstants.SUCCESS;
		}
		return returnValue;
	}

	private ConsumerDocument createConsumerDocument(AccountUser user, String ecmDocumentId, String fileName, String documentType, final int referralActivationId) {
		ConsumerDocument consumerDocument = new ConsumerDocument();
		consumerDocument.setCreatedBy(Long.valueOf(user.getId()));
		consumerDocument.setDocumentCategory(WebReferralConstants.REFERRAL_TKM_CATEGORY);
		consumerDocument.setDocumentName(fileName);
		consumerDocument.setDocumentType(documentType);
		consumerDocument.setTargetName(WebReferralConstants.REFERRAL_TKM_MODULE_NAME);
		consumerDocument.setTargetId(new Long(referralActivationId));
		consumerDocument.setEcmDocumentId(ecmDocumentId);
		consumerDocument = referralActivationDocumentRepository.save(consumerDocument);
		return consumerDocument;
	}

	private TkmTickets createNewReferralTicket(final ConsumerDocument consumerDocument, final AccountUser user, final int referralActivationId) throws GIException {
		TkmTickets tkmResponse = null;
		try {
			final TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
			final List<String> docLinks = new ArrayList<String>();
			docLinks.add(consumerDocument.getEcmDocumentId());
			final Integer userId = user.getId();
			tkmRequest.setCreatedBy(userId);
			tkmRequest.setLastUpdatedBy(userId);
			tkmRequest.setSubject("Ticket submission for: " + WebReferralConstants.REFERRAL_TKM_CATEGORY);
			tkmRequest.setComment("Ticket submission for: " + WebReferralConstants.REFERRAL_TKM_TYPE);
			tkmRequest.setCategory(WebReferralConstants.REFERRAL_TKM_CATEGORY);
			tkmRequest.setType(WebReferralConstants.REFERRAL_TKM_TYPE);
			tkmRequest.setUserRoleId(roleService.findRoleByName(WebReferralConstants.INDIVIDUAL_ROLE).getId());
			tkmRequest.setRequester(userId);
			tkmRequest.setModuleId(referralActivationId);
			tkmRequest.setModuleName(WebReferralConstants.REFERRAL_TKM_MODULE_NAME);
			tkmRequest.setDocumentLinks(docLinks);
			tkmRequest.setCaseNumber(StringUtils.EMPTY);
			tkmRequest.setRedirectUrl(StringUtils.EMPTY);

			final Map<String, String> ticketFormPropertiesMap = new HashMap<String, String>();
			ticketFormPropertiesMap.put("docId", consumerDocument.getId().toString()); // This will be used to update the status of the document later when the document is approved or rejected
			tkmRequest.setTicketFormPropertiesMap(ticketFormPropertiesMap);

			tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
			LOGGER.debug("Referral Activation Document Verification ticket ID is " + (tkmResponse != null ? tkmResponse.getNumber() : "null"));
		} catch (Exception e) {
			final String errorMessage = "Critical Error occurred while creating a ticket for document Id " + consumerDocument.getEcmDocumentId();
			LOGGER.error(errorMessage, e);
			throw new GIException(errorMessage, e);
		}
		return tkmResponse;
	}

	@Override
	public List<ConsumerDocument> fetchConsumerDocuments(long referralActivation) {
		return referralActivationDocumentRepository.getDocumentsForReferralActivationId(referralActivation);
	}

	@SuppressWarnings("serial")
	@Override
	public List<Integer> fetchCoverageYear() {
		final List<Integer> coverageYear = new ArrayList<Integer>() {
			{
				add(2015);
			}
		};

		return coverageYear;
	}

	@Override
	public int persistCoverageYear(int referralId, int coverageYear) {
		int returnValue = WebReferralConstants.FAILURE;
		ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_COVERAGE_YEAR, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, new int[] { referralId, coverageYear });

		if (responseEntity == null || responseEntity.getBody().equals(WebReferralConstants.FAILURE)) {
			
			LOGGER.warn("persistCoverageYear failure ");
		} else {
			returnValue = responseEntity.getBody().intValue();
		}
		return returnValue;
	}

	@Override
	public int processRidpCheck(final int referralVerification, final int userId) {

		int returnValue = WebReferralConstants.FAILURE;
		final ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_ACTIVATION_RIDP, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, new int[] {
		        referralVerification, userId });

		if (responseEntity == null || !responseEntity.getBody().equals(1)) {
			LOGGER.warn("processRidpCheck failure ");
			if(null != responseEntity ) {
			returnValue = responseEntity.getBody();
			}
		} else {
			returnValue = WebReferralConstants.SUCCESS;
		}
		return returnValue;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
	public Map<String, String> processRidpLinkCheck(final int referralVerification, final int userId, final int householdId) {

		Map<String, String> returnValue = null;
		ResponseEntity<Map> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_ACTIVATION_RIDP, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, new int[] { referralVerification, userId,
		        householdId });

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.warn("processRidpCheck failure ");
		} else {
			returnValue = responseEntity.getBody();
		}
		return returnValue;
	}

	private AccountUser getLoggedInUser() throws ReferralServiceException {
		AccountUser currentUser;
		String errorMessage;
		try {
			currentUser = userService.getLoggedInUser();
		} catch (InvalidUserException invalidUserException) {
			errorMessage = "Error fetching currently logged in user details";
			LOGGER.error(errorMessage, invalidUserException);
			throw new ReferralServiceException(errorMessage, invalidUserException);
		}
		return currentUser;
	}

	private String getUserName() {
		String userName = ReferralConstants.EXADMIN_USERNAME;
		try {
			AccountUser currentUser = getLoggedInUser();
			if (currentUser != null) {
				userName = currentUser.getUserName();
			}
		} catch (ReferralServiceException e) {
			LOGGER.error("Error fetching currently logged in user details", e);
		}
		return userName;
	}

	@Override
	public ReferralActivation fetchReferralActivationForSsap(final int ssapApplication) {
		ReferralActivation referralActivation = null;
		
		@SuppressWarnings("serial")
		final Map<String, Integer> request = new HashMap<String, Integer>() {
			{
				put("id", ssapApplication);
			}
		};
		final ResponseEntity<ReferralActivation> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.REFERRAL_ACTIVATION_SSAP, null, HttpMethod.GET, MediaType.APPLICATION_JSON, ReferralActivation.class, request);
		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.warn("fetchReferralActivationObject failure ");
		} else {
			referralActivation = responseEntity.getBody();
		}

		return referralActivation;
	}

	@Override
	public void sendAdditionalVerificationRequiredNotification(long referralActivationId) {
		try {
			ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.SEND_ADDITIONAL_VERIFICATION_DOC_NOTICE, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class,
			        referralActivationId);

			if (responseEntity == null) {
				LOGGER.error("Notice to upload document was not sent to referralActivationId" + referralActivationId);
			}
		} catch (Exception e) {
			LOGGER.error("Notice to upload document was not sent to referralActivationId" + referralActivationId);
		}
	}

	/**
	 * -pravin to get applicants to display in LCE
	 */
	@Override
	public LceActivityDTO fetchApplicantsforLceEvents(String caseNumber) {
		LceActivityDTO lceActivityDTO = null;
		try {
			final ResponseEntity<LceActivityDTO> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.FETCH_LCE_APPLICANTS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, LceActivityDTO.class, caseNumber);
			if (responseEntity == null) {
				LOGGER.error("Unable to fetch applicants to report lce for casenumber" + caseNumber);
			} else {
				lceActivityDTO = responseEntity.getBody();
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Unable to fetch applicants to report lce for casenumber -" + caseNumber);
		}
		return lceActivityDTO;
	}

	/**
	 * -Pravin to save ssapapplicantevent for selected sep event
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, String> updateSepEventsforApplicantandApplications(LceActivityDTO lceActivityDTO) {

		final ResponseEntity<Map> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_SSAP_SEP_EVENTS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, lceActivityDTO);
		if (responseEntity == null) {
			LOGGER.error("Not able to update Sep Events for " + lceActivityDTO.getCaseNumber());
			throw new GIRuntimeException("Not able to update Sep Events - Please contact admin.");
		} else {
			return responseEntity.getBody();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, List<SepEvents>> fetchSepAndQEPEvents() {

		Map<String, List<SepEvents>> sepEventMap = null;
		ParameterizedTypeReference<Map<String, List<SepEvents>>> typeRef = new ParameterizedTypeReference<Map<String, List<SepEvents>>>() {
		};
		final ResponseEntity<Map<String, List<SepEvents>>> responseEntity = ghixRestTemplate.exchangeParameterized(GhixEndPoints.EligibilityEndPoints.DISPLAY_SEP_QEP_EVENTS, ReferralConstants.EXADMIN_USERNAME, HttpMethod.GET, typeRef);
		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to fetch SEP Events from sep lookup table - Please contact admin.");
			throw new GIRuntimeException("Unable to fetch SEP Events from sep lookup table - Please contact admin.");
		} else {
			sepEventMap = responseEntity.getBody();
		}

		return sepEventMap;
	}

	@Override
	public long getActiveApplicationCountForCMR(final int cmrHouseHoldId, final Integer ssapApplicationId) {
		long count = 0;

		@SuppressWarnings("serial")
		final Map<String, Integer> request = new HashMap<String, Integer>() {
			{
				put("cmrHouseHoldId", cmrHouseHoldId);
				put("ssapApplicationId", ssapApplicationId);
			}
		};
		try {
			final ResponseEntity<Long> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.COUNT_SSAP_CMRHOUSEHOLD, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, Long.class, request);
			if (responseEntity == null) {
				LOGGER.error("Not able to get active App count for " + cmrHouseHoldId);
			} else {
				count = responseEntity.getBody();
			}
		} catch (Exception e) {
			LOGGER.error("Not able to get active App count for" + cmrHouseHoldId + e);
		}
		return count;
	}

	@SuppressWarnings("unchecked")
    @Override
	public Map<String, String> checkforEnrolledApplication(int cmrId, int ssapApplicationId) {

		Map<String, String> returnValue = null;

		String url = String.format(GhixEndPoints.EligibilityEndPoints.ENROLLED_APP_BY_CMRHOUSEHOLD, ssapApplicationId, cmrId);
		@SuppressWarnings("rawtypes")
		final ResponseEntity<Map> responseEntity = ghixRestTemplate.exchange(url, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, Map.class, null);
		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.warn("");
			return returnValue;

		} else {
			returnValue = responseEntity.getBody();
			return returnValue;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<Integer, String> getSsapApplicant(final String accessCode, final String isCsr) {
		Map<Integer, String> ssapMap = null;

		final ResponseEntity<Map> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICANT + accessCode + "/" + isCsr, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, Map.class, null);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to fetch security questions - Please contact admin.");
			throw new GIRuntimeException("Unable to fetch security questions - Please contact admin.");
		} else {
			ssapMap = responseEntity.getBody();
		}

		return ssapMap;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<Integer, String> getSsapApplicantfromHome(final String accessCode, final String isCsr) {
		Map<Integer, String> ssapMap = null;

		final ResponseEntity<Map> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICANT_FROM_HOME + accessCode + "/" + isCsr, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, Map.class, null);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to fetch security questions - Please contact admin.");
			throw new GIRuntimeException("Unable to fetch security questions - Please contact admin.");
		} else {
			ssapMap = responseEntity.getBody();
		}

		return ssapMap;
	}
	
	

	@Override
	public String getSsapApplicantGuid(final String ssapApplicationId) {
		String ssapApplicantGuid = null;

		final ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICANT_GUID + ssapApplicationId, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to fetch ssap applicant guid - Please contact admin.");
			throw new GIRuntimeException("Unable to fetch ssap applicant guid - Please contact admin.");
		} else {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("VALIDATE ACCESS CODE TEMPLATE RSP: " + responseEntity.getBody());
			}
			ssapApplicantGuid = responseEntity.getBody();
		}

		return ssapApplicantGuid;
	}

	@Override
	public int checkForExistingHouseholdFromSsnDoB(final String ssapApplicationId) {
		int householdId = 0;

		@SuppressWarnings("serial")
		final Map<String, String> request = new HashMap<String, String>() {
			{
				put("ssapApplicationId", ssapApplicationId);
			}
		};

		final ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.FIND_HH_SSN_DOB + ssapApplicationId, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, Integer.class, null);

		if (responseEntity == null || responseEntity.getBody() == null) {
			throw new GIRuntimeException("Unable to fetch/create household - Please contact admin.");
		} else {

			householdId = responseEntity.getBody().intValue();
		}

		return householdId;
	}

	@Override
	public ReferralLinkingOverride compareHouseholdAndPrimary(final String ssapApplicationId, final String householdId) {
		ReferralLinkingOverride referralLinkingOverride = null;

		@SuppressWarnings("serial")
		final Map<String, String> request = new HashMap<String, String>() {
			{
				put("ssapApplicationId", ssapApplicationId);
				put("householdId", householdId);
			}
		};

		final ResponseEntity<ReferralLinkingOverride> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.COMPARE_HOUSEHOLD_PRIMARY_REFERRAL, getUserName(), HttpMethod.GET, MediaType.APPLICATION_JSON, ReferralLinkingOverride.class, request);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to compare primary and household - Please contact admin.");
			throw new GIRuntimeException("Unable to compare primary and household - Please contact admin.");
		} else {
			referralLinkingOverride = responseEntity.getBody();
		}

		return referralLinkingOverride;
	}

	@Override
	public Integer updateReferralActivationCount(final String referralId) {
		Integer count = -1;

		final ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_REFERRAL_COUNT, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, referralId);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to update referral count - Please contact admin.");
			throw new GIRuntimeException("Unable to update referral count - Please contact admin.");
		} else {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("UPDATE REFERRAL CNT: " + responseEntity.getBody());
			}
			count = responseEntity.getBody();
		}

		return count;

	}
	
	@Override
	public Integer updateReferralActivationSuccessCount(final String referralId) {
		Integer count = -1;
		final ResponseEntity<Integer> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.EligibilityEndPoints.UPDATE_REFERRAL_SUCCESS_COUNT, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Integer.class, referralId);

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to update referral success count - Please contact admin.");
			throw new GIRuntimeException("Unable to update referral success count - Please contact admin.");
		} else {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("UPDATE REFERRAL SUCCESS CNT: " + responseEntity.getBody());
			}
			count = responseEntity.getBody();
		}

		return count;

	}

	@Override
	public void triggerDesignation(int householdId) {

		try {
			AccountUser loggedInUser = userService.getLoggedInUser();
			boolean isAssister = userService.hasUserRole(loggedInUser, RoleService.ASSISTER_ROLE);
			boolean isBroker = userService.hasUserRole(loggedInUser, RoleService.BROKER_ROLE);

			if (isAssister) { // We have separate module deployed for Broker and // Assister so we need to have this if/else
				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setIndividualId(householdId);
				assisterRequestDTO.setUserId(loggedInUser.getId());
				assisterRequestDTO.setEsignName(loggedInUser.getFullName());
				assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Active);
				String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(assisterRequestDTO);
				// This goes to ghix-entity
				String response = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEntityEndPoints.DESIGNATE_ASSISTER, requestXML, String.class); // Rest call for
				                                                                                                                                        // designation
				AssisterResponseDTO assisterResponseDTO = (AssisterResponseDTO) GhixUtils.xStreamHibernateXmlMashaling().fromXML(response);
				LOGGER.info("Result of Assister-Designation for user:" + SecurityUtil.sanitizeForLogging("" + loggedInUser.getId()) + " is:" + SecurityUtil.sanitizeForLogging("" + assisterResponseDTO.getResponseCode()) + ", Desc:"
				        + SecurityUtil.sanitizeForLogging(assisterResponseDTO.getResponseDescription()));

			} else if (isBroker) {
				// Simillar code here that will go to ghix-broker
				BrokerRequestDTO brokerRequestDTO = new BrokerRequestDTO();
				brokerRequestDTO.setIndividualId(householdId);
				brokerRequestDTO.setUserId(userService.getLoggedInUser().getId());
				brokerRequestDTO.setEsignName(userService.getLoggedInUser().getFullName());
				brokerRequestDTO.setDesigStatus(DesignateBroker.Status.Active);
				String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(brokerRequestDTO);
				String response = ghixRestTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.BROKER_DESIGNATE_INDIVIDUAL, requestXML, String.class);
				BrokerResponse brokerResponseDTO = (BrokerResponse) GhixUtils.xStreamHibernateXmlMashaling().fromXML(response);

				LOGGER.info("Result of Broker-Designation for user:" + SecurityUtil.sanitizeForLogging("" + loggedInUser.getId()) + " is:" + SecurityUtil.sanitizeForLogging("" + brokerResponseDTO.getResponseCode()) + ", Desc:"
				        + SecurityUtil.sanitizeForLogging(brokerResponseDTO.getResponseDescription()));
			} else {
				LOGGER.info("Not triggering designation flow for User :" + SecurityUtil.sanitizeForLogging("" + loggedInUser.getId()) + " since does not have appropriate role.");
			}
		} catch (Exception ex) {
			LOGGER.error("Ignoring error during triggerDesignation. Error:" + ex.getMessage());
			LOGGER.error(ex.getStackTrace().toString());
		}
	}

}