package com.getinsured.eligibility.at.ref.service;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("referralRidpService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralRidpServiceImpl implements ReferralRidpService {

	private static final Logger LOGGER = Logger.getLogger(ReferralRidpServiceImpl.class);

	private static final String REFERRAL = "REFERRAL";

	

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;

	
	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;

	// @Value("#{configProp['ridp.required'] ?: 'N'}")
	// private String ridpCheck = "N";

	@Autowired
	private UserService userService;

	@Override
	@Transactional
	public int executeRidp(SsapApplicant ssapApplicant, int userId, Household household) {
		 int result = ReferralConstants.ERROR;
		 
		 updateCmrHousehold(ssapApplicant, userId, household);
		 
		 result = ReferralConstants.SUCCESS;
		
		LOGGER.debug("executeRidp ends with status " + result);
		return result;
	}


	private void updateCmrHousehold(SsapApplicant ssapApplicant,  int userId, Household household) {
		
		
		if (userId == ReferralConstants.NONE) {
			userId = userService.getSuperUserId(ReferralConstants.EXADMIN_USERNAME);
		}

		
		final Location location = populateLocation(ssapApplicant);
		household.setFirstName(WordUtils.capitalizeFully(ssapApplicant.getFirstName()));
		household.setMiddleName(WordUtils.capitalizeFully(ssapApplicant.getMiddleName()));
		household.setLastName(WordUtils.capitalizeFully(ssapApplicant.getLastName()));
		household.setBirthDate(ssapApplicant.getBirthDate());
		household.setLocation(location);
		household.setPrefContactLocation(populateMailingLocation(ssapApplicant));
		household.setZipCode(location.getZip());
		household.setPhoneNumber(ssapApplicant.getPhoneNumber());
		household.setUpdated(new Timestamp(new TSDate().getTime()));
		household.setUpdatedBy(userId);
		if(ssapApplicant.getSsn()!=null) {
			household.setSsn(ssapApplicant.getSsn());
		}
		
		/* update RIDP flags if required */
		if (!RIDP_VERIFIED_YES.equals(ReferralUtil.checkAndValidString(household.getRidpVerified()))) {
			household.setRidpVerified(RIDP_VERIFIED_YES);
			household.setRidpDate(new TSDate());
			household.setRidpSource(REFERRAL);
		}
		
		if (household.getPrefContactMethod() == null) {
			Integer prefCommMethodLookup = readDefaultPrefCommMethod(household);
			household.setPrefContactMethod(prefCommMethodLookup);
		}
		
		cmrHouseholdRepository.save(household);
	}
	
	@Autowired 
	private LookupService lookUpService;

	private static final String COMMUNICATION_MODE = "PreferredContactMode";
	
	private Integer readDefaultPrefCommMethod(final Household household) {
		LookupValue prefCommMethodLookup = null;
		GhixNoticeCommunicationMethod prefCommunication = null;
		if (StringUtils.isNotBlank(household.getEmail())){
			prefCommunication = GhixNoticeCommunicationMethod.Email;
		} else {
			prefCommunication = GhixNoticeCommunicationMethod.Mail;
		}
		prefCommMethodLookup = lookUpService.getlookupValueByTypeANDLookupValueCode(COMMUNICATION_MODE, prefCommunication.getMethod());
		
		return prefCommMethodLookup != null ? prefCommMethodLookup.getLookupValueId() : null;
	}
	
	private Location populateMailingLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else {
			return null;
		}
	}

	private Location populateLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else {
			return null;
		}
	}

}
