package com.getinsured.hix.batch.agent;

public class Notices {
	
	private int noticeSerialNumber;
	private String noticeName;
	private String noticeEmailSubject;
	
	public int getNoticeSerialNumber() {
		return noticeSerialNumber;
	}
	public void setNoticeSerialNumber(int noticeSerialNo) {
		this.noticeSerialNumber = noticeSerialNo;
	}
	public String getNoticeName() {
		return noticeName;
	}
	public void setNoticeName(String noticeName) {
		this.noticeName = noticeName;
	}
	public String getNoticeEmailSubject() {
		return noticeEmailSubject;
	}
	public void setNoticeEmailSubject(String noticeEmailSubject) {
		this.noticeEmailSubject = noticeEmailSubject;
	}
	
	
}
