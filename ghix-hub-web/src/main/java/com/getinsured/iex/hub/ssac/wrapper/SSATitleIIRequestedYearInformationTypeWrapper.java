package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSATitleIIRequestedYearInformationType;

/**
 * Wrapper class for SSATitleIIRequestedYearInformationType
 * 
 * @author Nikhil Talreja
 * @since 05-Mar-2014
 */
public class SSATitleIIRequestedYearInformationTypeWrapper implements JSONAware {
	
	private String incomeDate;
    private String yearlyIncomeAmount;
	
	public SSATitleIIRequestedYearInformationTypeWrapper(SSATitleIIRequestedYearInformationType ssaTitleIIRequestedYearInformationType){
		
		if(ssaTitleIIRequestedYearInformationType == null){
			return;
		}
		
		this.incomeDate = ssaTitleIIRequestedYearInformationType.getIncomeDate();
		if(ssaTitleIIRequestedYearInformationType.getYearlyIncomeAmount() != null){
			this.yearlyIncomeAmount = ssaTitleIIRequestedYearInformationType.getYearlyIncomeAmount().toPlainString();
		}
		
	}
	
	public String getIncomeDate() {
		return incomeDate;
	}

	public String getYearlyIncomeAmount() {
		return yearlyIncomeAmount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("IncomeDate", this.incomeDate);
		obj.put("YearlyIncomeAmount", this.yearlyIncomeAmount);
		return obj.toJSONString();
	}
	
}
