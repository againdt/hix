package com.getinsured.hix.batch.tkm.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.tkm.service.TicketListService;

public class TKMReportTask implements Tasklet{

	private static final Logger LOGGER = LoggerFactory.getLogger(TKMReportTask.class);
	private TicketListService ticketListService;
	
	public TicketListService getTicketListService() {
		return ticketListService;
	}




	public void setTicketListService(TicketListService ticketListService) {
		this.ticketListService = ticketListService;
	}




	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) 
	{
		LOGGER.info("=============== TKMReportTask Started ============== ");
		try {
			ticketListService.generateCsvForTickets();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		LOGGER.info("=============== TKMReportTask COMPLETED ============== ");
	
		return RepeatStatus.FINISHED;
	}
	
	
}
