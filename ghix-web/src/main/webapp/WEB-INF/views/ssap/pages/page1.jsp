 <%@ include file="../include.jsp" %>
         <div id="appscr51" style='display: none;'>
         	<div class="row-fluid">
	         	<%-- <h1>
	         		<spring:message code="ssap.page1.startYourApplication" text="Start Your Application" />
	         	</h1> --%>
	         	
	         	<div class="Main_contant_ui_work">
			        <div class="Main_contant_ui_work_title margin0 padding0 noBorder">
			        	<div class="header">
			        		<h4><spring:message code="ssap.page1.privacyOfYourInformation" text="Privacy of your information" /></h4>
			        	</div>
			          	<div class="Print_button hide"><a href="#"><spring:message code="ssap.page1.print" text="Print" /></a></div>
			          	<div class="Download_button hide"><a href="#"><spring:message code="ssap.page1.download" text="Download" /></a></div>
		            </div>
	            
		             <div class="main_page_contant_message gutter10">
			              <p>
				             <spring:message code="ssap.page1.privacyText1" text="We&#39;ll leave your information private as required by law." />
				             <spring:message code="ssap.page1.privacyText2" text="Your answers on this form will only be used to determine eligibility for health insurance or help paying for health insurance." /> <spring:message code="ssap.page1.privacyText3" text="We&#39;ll check your answers using information in our electronic databases and the databases of partner agencies." /> <spring:message code="ssap.page1.privacyText4" text="If the information doesn&#39;t match, we may ask you to send us proof." /> <spring:message code="ssap.page1.privacyText5" text="This application does not ask any questions about your medical history." />
				          </p>
				          <p>
				             <spring:message code="ssap.page1.important" text="Important:" />
				          </p> 
				          <p>
				            <spring:message code="ssap.page1.privacyText7" text="As part of the application process we may need to retrieve your information from other government agencies like IRS, Social Security Administration, and the Department of Homeland Security." />  <spring:message code="ssap.page1.privacyText8" text="We need this information to check your eligibility for health insurance or help paying for health insurance, if you choose to apply, and give you the best service possible." /> 
				          </p>
		             </div>
	             
	              
	              	<input type="hidden" id="currentDate" class="hide" />
	              
	             
		            <div class="main_page_contant_message1 gutter10">
		              
		              	<div class="content-link gutter10">
		              	 	<ul class="unstyled">
		              	 		<li class="non_financial_hide_id">
		              	 			<a href="#" target="_blank" class="headingColour"><spring:message code="ssap.page1.privacyText9" text="Learn more about your data" /> </a>
		              	 		</li>
		              	 		<li>
		              	 			<spring:message code="ssap.page1.privacyText10"/>
		              	 		</li>
		              	 	</ul>
		              	</div>
			            <div class="gutter10 clearfix">
							<label for="acceptanceCB" class="pull-left capitalize-none checkbox" for="acceptanceCB">
								<input type="checkbox" value="" name="" id="acceptanceCB"/><spring:message code="ssap.page1.privacyText11" text="I agree to allow my information to be used and retrieved from data sources for this application." /> <spring:message code="ssap.page1.privacyText12" text="I have consent for all people I will list on the application that allows their information to be retrieved and used from data sources for this application." /> 								
							</label>
							
			             	
			             	<ul id="parsley-terms-checkbox" class="parsley-error-list hide">
								<li class="custom-error-message" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>Please select the terms and condition checkbox</span>
								</li>
							</ul>
			             </div>
		            </div>
	        
	             </div>
          	</div>
                   
              
            
         </div>
         
         
          <script>
         	$('#tab1').addClass('active');
         </script>