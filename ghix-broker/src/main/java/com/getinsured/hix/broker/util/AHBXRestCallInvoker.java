package com.getinsured.hix.broker.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.broker.EmployerIndividualBOBDTO;
import com.getinsured.hix.dto.broker.IndividualDTO;
import com.getinsured.hix.model.EmployersBOBResponse;
import com.getinsured.hix.model.IndividualBOBresponse;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * Used to call ghix-ahbx using REST template, which in turn will call AHBX
 * web-services.
 */
@Component
public class AHBXRestCallInvoker {
	private static final Logger LOGGER = LoggerFactory.getLogger(AHBXRestCallInvoker.class);

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Calls AHBX service to get External Employer BOB Details and saves in GI
	 * Database in AHBX layer This method just initiates the retrieving detail
	 * process and expect success/failure status
	 * 
	 * @param id
	 *            external employer id
	 * @return employerResponse response object returned from AHBX
	 * @throws Exception 
	 */
	public EmployersBOBResponse retrieveAndSaveExternalEmployerBOBDetail(long id) throws GIException {
		EmployersBOBResponse employersBOBResponse = null;
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Employer Id: " + id);
		strResponse.append("\nResponse is ");

		try {
			EmployerIndividualBOBDTO dto = new EmployerIndividualBOBDTO();
			dto.setId(id);

			LOGGER.info("Sending External Employer BOB details to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.GET_EXTERNAL_EMPLOYER_BOB_DETAIL, dto, String.class);
			
			if(!BrokerUtils.isEmpty(wsResponse)){
				employersBOBResponse = (EmployersBOBResponse) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
			}

			strResponse.append(wsResponse);

/*			LOGGER.info("Successfully sent External Employer BOB details over Rest call to Ghix-Ahbx. Received following response:"
					+ strResponse.toString());
*/
		} catch (Exception exception) {
			LOGGER.error("Send External Employer BOB details Web Service failed. Exception is :", exception);
			throw new GIException("Send External Employer BOB details Web Service failed. Exception", exception);
		}
		
		return employersBOBResponse;
	}

	/**
	 * Calls AHBX service to get External Individual BOB Details and saves in GI
	 * Database in AHBX layer This method just initiates the retrieving detail
	 * process and expect success/failure status
	 * 
	 * @param id
	 *            external individual id
	 * @return individualResponse response object returned from AHBX
	 * @throws Exception 
	 */
	public IndividualBOBresponse retrieveAndSaveExternalIndividualDetail(long id) throws GIException {
		IndividualBOBresponse individualBOBresponse = null;
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Individual Id: " + id);
		strResponse.append("\nResponse is ");

		try {
			IndividualDTO dto = new IndividualDTO();
			dto.setIndividualid(id);

			LOGGER.info("IND52:Sending External Individual BOB Details to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.GET_EXTERNAL_INDIVIDUAL_BOB_DETAIL, dto, String.class);

			
			if(!BrokerUtils.isEmpty(wsResponse)){
				individualBOBresponse = (IndividualBOBresponse) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
			}
			
			strResponse.append(wsResponse);

/*			LOGGER.info("IND52:Successfully sent External Individual BOB Details over Rest call to Ghix-Ahbx. Received following response:"
					+ strResponse.toString());
*/
		} catch (Exception exception) {
			LOGGER.error("IND52:Send External Individual BOB Detail Web Service failed. Exception is :", exception);
			throw new GIException("IND52:Send External Individual BOB Detail Web Service failed", exception);
		}

		return individualBOBresponse;
	}
}
