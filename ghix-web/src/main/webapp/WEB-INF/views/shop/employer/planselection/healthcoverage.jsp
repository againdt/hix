<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<link rel="stylesheet" href="<c:url value='/resources/css/magnific-popup.css' />">
<script>


	var employerIndvContribution = ${employerIndvContribution}; 
	var employerDepeContribution = ${employerDepeContribution};
	var defaultEmployerIndvContribution = ${defaultEmployerIndvContribution}; 
	var minDentalPremium = 0;
	var maxDentalPremium = 0;
	var planDataJSON = '';
	var tierDataJSON = '';
	var taxCreditPercentage = ${taxCredit};
	var selectedTier = "${selectedTier}";
	var planSel = '${selectedPlan}';
</script>

<script type="text/javascript" src="<c:url value='/resources/js/shop/eps-common.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/shop/employerPlanSelection.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.magnific-popup.min.js' />"></script>

<script type="text/javascript">
function showPlanDetailsLink(){
	showPlanDetails($('#planTier').val(),  tierDataJSON, planDataJSON);
}</script>
<div class="gutter10">  	
	<div class="row-fluid margin30-t">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage Setup</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/shop/employer/planselection/gettingstarted' />"><i class="icon-ok"></i> Getting Started</a></li>
	      		<li class="active"><a href="#">2 Health Coverage Setup</a></li>
	      		<c:if test="${isDentalAvailable=='YES' }"><li><a href="#">3 Dental Coverage Setup</a></li></c:if>
	      		<li><a href="#">4 Review and Confirm</a></li>
	      	</ul>

	      	<div class="header margin30-t">
	      		<h4>Quick Links</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/broker/search' />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
	      	</ul>
	    </div><!-- #sidebar -->
    
	    <div class="span9" id="rightpanel">
	    	<div class="header">
	    		<h4 class="pull-left">Health Coverage Setup</h4>
	    		
				<!-- NEW COVERAGE SETUP Button. To open/hide #coverageSetupChoice section - do not show for the RENEWAL -->	    		
	    		<a href="#" class="btn btn-small btn-primary pull-right healthCoverageSetupAction hide">Next</a>
	    		<a href="#" class="btn btn-small pull-right healthCoverageSetupActionBack hide">Back</a>
				<!-- NEW COVERAGE SETUP Button ends -->	    
		
	    		<a href="#" id="nextbutton"  onclick="submitForm();" class="btn btn-small btn-primary pull-right healthCoverageSetupActionNext">Next</a>	    		
	    		<a href="<c:url value="/shop/employer/planselection/gettingstarted" />" class="btn btn-small pull-right healthCoverageSetupActionBackReturn">Back</a>
	    	</div>
			<div class="row-fluid" id="errorDiv" style="display:none;"> 
					<div style="font-size: 15px; color: red">
							&nbsp;<p id="errorMsg">No plans found for the selected criteria<p/>
						
						<br>
					</div>
			</div>
			<div class="row-fluid" id="dataDiv">
	    	<div class="gutter10">
					<div class="gutter10" id="coverageSetupSuggestions">
						<div class="row-fluid margin20-b">
							<p class="span8">Based on your objectives, we have made some initial setup suggestions. You can make changes and see how it affects your monthly cost.</p>
							<div class="span2 img-polaroid" id="yourCostWrapper">
								<ul>
									<li>Your Cost</li>
									<li class="loadingImg"><img src="<c:url value="/resources/img/loadingGif.gif"/>" alt=""></li>
									<li class="amount"><span id="yourCostAmount">${employerCost}</span> monthly</li>
								</ul>
							</div>
							<div class="span2 img-polaroid" id="taxCreditWrapper">
								<ul>
									<li><a href="#" rel="tooltip" data-toggle="tooltip" class="ttclass" data-placement="top" title="" data-original-title="Your company may be eligible to receive a tax credit for offering health coverage to your employees if your company: 
										- has fewer than 25 full-time employees
										- offers coverage to all full-time employees
										- has an average annual salary per employee of less than $50,000
										- contributes at least 50% of premium costs for employee plans
										Contact your tax advisor for more information or see the <a href='http://www.irs.gov/uac/Small-Business-Health-Care-Tax-Credit-for-Small-Employers\'>IRS site</a> for more information.">Tax Credit</a></li>
									<li class="loadingImg"><img src="<c:url value="/resources/img/loadingGif.gif"/>" alt=""></li>
									<li class="amount"><span id="taxCreditAmount"></span> monthly</li>
								</ul>
							</div>
						</div><!-- .row-fluid -->

						<div class="accordion" id="coverageSetupAccordion">	
												
<!-- FIRST ACCORDION FOR NEW COVERAGE SETUP - do not show for the renewal setup -->							
							<div class="accordion-group margin10-b ">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Coverage Effective Date</span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseOneNewSetup" data-text-swap="Close">Change</a>
						      		<span class="pull-right" id="coverageEffectiveDateTitle">${selectedDate}</span>
						    	</div>
						    	<div id="collapseOneNewSetup" class="accordion-body collapse">
						      		<div class="accordion-inner">
						      			<div class="effectiveDate">
						      			<c:forEach items="${coverageDates}" var="coverageDate"  varStatus="counter">
                        					<div class="enrollmentPeriodDate pull-left">
												<input type="radio" name="coverageDateStart" id="coverageEffectiveDate1" value="${coverageDate[0]}" class="pull-left" <c:if test="${coverageDate[0] == selectedDate}"> checked="checked" </c:if> onclick="setCoverageStartDate(this.value);"  />
												<label for="coverageEffectiveDate1"><span class="margin7-l">${coverageDate[0]}</span></label>
                    							<div class="margin20-l">
                    								<p class="font10 margin10-t">
                    									<small>Open Enrollment</small><br>
                    									<strong>${coverageDate[1]}</strong>
                    								</p>
                    								<p class="font10 margin10-t">
                    									<small>First Payment Due</small><br>
                    									<strong>${coverageDate[2]}</strong>
                    								</p>
                    							</div>                       			
                        					</div>
										</c:forEach>	
                        				</div>
						      		</div>
						    	</div>
						  	</div>
<!-- FIRST ACCORDION FOR NEW COVERAGE SETUP ENDS -->

							<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Your Contribution % Towards Employees</span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseTwo" data-text-swap="Close">Change</a>
						      		<span class="pull-right contributionTowardsEmployeesResult">${employerIndvContribution}</span>
						    	</div>
						    	<div id="collapseTwo" class="accordion-body collapse margin20-b">
						      		<div class="accordion-inner">
						        		<p>Select the percentage of your employee's health insurance premiums you are willing to pay.  The portion you don't cover will be paid by your employees.</p>
						        		
						        		<div class="slider-home row-fluid">
											<div class="sliderTowardsEmployees span9"></div>
    										<div class="span3 contributionTowardsEmployeesResult">${employerIndvContribution}</div>  
    										<input type="hidden" id="contributionTowardsEmployees" name="contributionTowardsEmployees" value="${employerIndvContribution}" />
										</div>
										<div id="empContribution_error" style="font-size: 15px; color: red"></div>		
						      		</div>
						      		
						    	</div>
						  	</div>

						  	<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Your Contribution % Towards Dependents</span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseThree" data-text-swap="Close">Change</a>
						      		<span class="pull-right contributionTowardsDependentsResult">${employerDepeContribution}</span>
						    	</div>
						    	<div id="collapseThree" class="accordion-body collapse margin20-b">
						      		<div class="accordion-inner">
						        		<p>Please select the percentage of your employee's dependent's health insurance premiums you are willing to pay.  The portion you don't cover will be paid by your employees.</p>

						        		<div class="slider-home row-fluid">
											<div class="sliderTowardsDependents span9"></div>
    										<div class="span3 contributionTowardsDependentsResult">${employerDepeContribution}</div>  
    										<input type="hidden" id="contributionTowardsDependents" name="contributionTowardsDependents"  value="${employerDepeContribution}"/>
										</div>
						      		</div>
						    	</div>
						  	</div>

						  	<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Employee Plan Choice <a tabindex ="-1" class="ttclass" rel="tooltip" data-toggle="tooltip" data-original-title ="All plans available on <%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%> offer the essential health benefits required by the Affordable Care Act.  As the employer, you select a level of plans to offer to your employees and each employee can select the specific plan within that level that best meets his or her needs." href="#"><i class="icon-question-sign"></i></a></span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseFour" data-text-swap="Close" title="" data-original-title="All plans available on ${exchangeName} offer the essential health benefits required by the Affordable Care Act.  As the employer, you select a level of plans to offer to your employees and each employee can select the specific plan within that level that best meets his or her needs.">Change</a>
						      		<span class="pull-right"><a class='planChoices' href='#' onclick="showPlanDetailsLink();"><span id="selectedTierPlanCount"></span><span class="selectedTier"></span> Plans</a></span>
						    	</div>
						    	<div id="collapseFour" class="accordion-body collapse margin20-b">
						      		<div class="accordion-inner">
						        		<p>The set of plans your employees can choose from is determined by the tier you select. The tiers differ in terms of the number of plans available, benefit comprehensiveness and cost of the plans.</p>
						        		<div class="effectiveDate" id="tierDataDiv">
                        						<!-- TIER DATA DIV - TOBE GENERATED BY JSCRIPT  -->
                        				</div>
						      		</div>
						    	</div>
						  	</div>

						  	<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Cost Basis Plan <a tabindex="-1" class="ttclass" rel="tooltip" data-toggle="tooltip" data-original-title="The Cost Basis Plan is used only for calculating your contributions.  For example, let's say you selected a 50% contribution to employees and plan A as your Cost Basis Plan.  If the premium for plan A for employee Jane who is 30 is $300, then your contribution will be $150 (300 x .50), no matter which plan Jane chooses. Jane will pay any difference between the $150 and the cost of the plan she chooses.
 	 	 	 		                <br/><br/>The Cost Basis Plan is not a recommendation you're making to your employees, and they are not told which plan you are using as the Cost Basis Plan." rel="tooltip" href="#"><i class="icon-question-sign"></i></a></span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseFive" data-text-swap="Close">Change</a>
						      		<span class="pull-right" id="costBasisPlan"><span class="selectedTier"></span> Plan</span>
						    	</div>
						    	<div id="collapseFive" class="accordion-body collapse margin20-b">
						      		<div class="accordion-inner">
						        		<p>Select a Cost Basis plan to lock in your costs regardless of what plans your employees eventually select.</p>
						        		<div class="row-fluid">
						        			<span class="lowestPricePlan">Lowest Price <span class="selectedTier"></span> Plan </span>
						        			<div class="horizontalSliderWrapper">
								        		<ul class="inline costBasisView horizontalSlider"  id="costBasisPlans">
									        		
		 							        	</ul>
		 							        	
 							        		</div>
 							        		<span class="highestPricePlan">Highest Price <span class="selectedTier"></span> Plan</span>
 							        		<div class="alert">
 							        			<p class="margin10-t">You have chosen the <span id="pricingVar"></span> <span class="selectedTier"></span> Plan as your Cost Basis plan.</p>
 							        			<p>Your contribution to each employee will be <span id="employeeContributionValue">${employerIndvContribution}</span>% of the premium of this plan.  Your employees will pay the difference between your contribution and the cost of the <span class="selectedTier"></span> plan they select. </p>
 							        		</div>
						        		</div>
						      		</div>
						    	</div>
						  	</div>
						</div><!-- #accordion -->
					</div><!-- #coverageSetupSuggestions -->

					<div class="form-actions">
						<form action="healthcoverage" id="healthcoverageform" name="healthcoverageform" method="post">
							<df:csrfToken/>
							<input type="hidden" name="benchmarkPlan" id="benchmarkPlan" value="${selectedPlan}"/>
							<input type="hidden" name="totalIndvPremium" id="totalIndvPremium" value=""/>
							<input type="hidden" name="totalDepePremium" id="totalDepePremium" value=""/>
							<input type="hidden" name="avgIndvPremium" id="avgIndvPremium" value=""/>
							<input type="hidden" name="benchmarkPlanName" id="benchmarkPlanName" value=""/>
							<input type="hidden" name="planTier" id="planTier" value="${selectedTier}"/>
							<input type="hidden" name="tierPlanCount" id="tierPlanCount" value="${selectedTierPlanCount}"/>
							<input type="hidden" name="employeeContribution" id="employeeContribution" value="${employerIndvContribution}"/>
							<input type="hidden" name="dependentContribution" id="dependentContribution" value="${employerDepeContribution}"/>
							<input type="hidden" name="employeeCount" id="employeeCount" value=""/>
							<input type="hidden" name="dependentCount" id="dependentCount" value=""/>
							<a href="#" class="btn btn-primary btn-large healthCoverageSetupActionNext" onclick="submitForm();">These look good to me &gt;&gt;</a>
						</form>
					</div>
				
	    	</div>
	    	</div>
		</div><!-- .span9 #rightpanel -->
	</div><!-- .row-fluid -->
	
	<form name="planDetailsFrm" id="planDetailsFrm" action="<c:url value='${planDisplayUrl}'/>" method="POST" target="_blank"> 
			<df:csrfToken/>
			<input type="hidden" id="TierName" name="TierName" value="" />
			<input type="hidden" id="CoverageDate" name="CoverageDate" value="" />
			<input type="hidden" id="PlanData" name="PlanData" value="" />
					
	</form>
</div><!-- .gutter10 -->

<div id="waitDiv" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="waitingDiv" aria-hidden="true">
     <div class="modal-header"></div>
     <div class="modal-body">
       <h3 class="green"><spring:message code = "label.processingMsg"/></h3>
<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
     </div>
     <div class="modal-footer"></div>
</div>

<div id="missinginfo" class="modal hide fade" tabindex="-1"  data-backdrop="static" modal="true" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
	<div class="modal-header"></div>
	       <div class="modal-body">
	       	<h4 class="green">Please wait while we quote your group. This may take a few minutes.</h4>
	       	<p class="txt-center"><img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots"/></p>
	      <div class="modal-footer"></div>
	</div>
</div>
<script>
$('.ttclass').tooltip();
$(document).ready(function(){
	$('.ui-slider-handle').on('click', function(){
		$('li.amount').hide();
		$('li.loadingImg').show().delay(1000).hide(0);
		$('li.amount').delay(1000).show(0);
	});
	
	loadPlanData();
	


});	


function submitForm(){
	var planData = $.parseJSON(planDataJSON);
	var selectedPlan = $('#benchmarkPlan').val();
	$('#totalIndvPremium').val(planData[selectedPlan]["totalIndiPremium"]);	
	if($('#contributionTowardsDependents').val() > 0){
		$('#totalDepePremium').val(planData[selectedPlan]["totalDepePremium"]);	
	}
	else{
		$('#totalDepePremium').val(0);
	}
	$('#avgIndvPremium').val(planData[selectedPlan]["avgIndiPremium"]);	
	$('#benchmarkPlanName').val(planData[selectedPlan]["planName"]);	
	
	$('#healthcoverageform').submit();
}


function openFindBrokerModal(){
	$('#findbroker').modal('show');
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
});


</script>