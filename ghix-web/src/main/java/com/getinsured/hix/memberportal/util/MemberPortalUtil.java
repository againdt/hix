package com.getinsured.hix.memberportal.util;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;

/**
 * Utility class for PHIX Member Portal
 * @author Nikhil Talreja
 * @since 20 August, 2013 
 * 
 */
@Component
public class MemberPortalUtil {
	
	private static final Logger LOGGER = Logger.getLogger(MemberPortalUtil.class);

	@Autowired private HIXHTTPClient hIXHTTPClient;

	private static ObjectMapper mapper = new ObjectMapper();
	
	
	public static final String CONTENT_TYPE = "application/json";
	
	/**
	 * Method to find a record from ELIG_LEAD table based on lead Id
	 *
	 * @author Nikhil Talreja
	 * @since 30 August, 2013
	 * 
	 * @param long leadId - lead id for which record needs to be found 
	 * @return EligLead - EligLead object
	 */
	/*
	public EligLead fetchEligLeadRecord(long leadId){
		
		try{
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.GET_LEAD, "\""+leadId+"\"", CONTENT_TYPE);
			return mapper.readValue(response, EligLead.class);
		}
		catch(Exception e){
			LOGGER.error("Exception occured while fetching record from ELIG_LEAD table",e);
			return null;
		}
	}*/
}
