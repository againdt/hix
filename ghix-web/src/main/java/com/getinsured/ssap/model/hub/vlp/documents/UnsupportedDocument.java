package com.getinsured.ssap.model.hub.vlp.documents;

import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Handling all unsupported document types as well as 'None of These' option.
 *
 * @author Yevgen Golubenko
 * @since 11/4/19
 */
public class UnsupportedDocument extends DhsDocument {
  public UnsupportedDocument()
  {
    super.setDocumentType(DocumentType.UNSUPPORTED);
  }

  /**
   * Unsupported document type will always be valid, so it will
   * pass pre-checks.
   * @return true
   */
  @Override
  public boolean isValid() {
    return true;
  }

  @Override
  public DhsDocument from(CitizenshipDocument citizenshipDocument) {
    return new UnsupportedDocument();
  }
}
