//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.01.23 at 01:51:54 PM PST 
//


package com.getinsured.iex.hub.ridp.cms.hix._0_1.hix_core;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ResponseCodeType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.TextType;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.iex.hub.ridp.cms.hix._0_1.hix_core package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PersonPreferredLanguage_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "PersonPreferredLanguage");
    private final static QName _ResponseMetadata_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "ResponseMetadata");
    private final static QName _PersonAugmentation_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "PersonAugmentation");
    private final static QName _ResponseDescriptionText_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "ResponseDescriptionText");
    private final static QName _Response_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "Response");
    private final static QName _ResponseCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-core", "ResponseCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.iex.hub.ridp.cms.hix._0_1.hix_core
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponseMetadataType }
     * 
     */
    public ResponseMetadataType createResponseMetadataType() {
        return new ResponseMetadataType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link PersonAugmentationType }
     * 
     */
    public PersonAugmentationType createPersonAugmentationType() {
        return new PersonAugmentationType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonLanguageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "PersonPreferredLanguage")
    public JAXBElement<PersonLanguageType> createPersonPreferredLanguage(PersonLanguageType value) {
        return new JAXBElement<PersonLanguageType>(_PersonPreferredLanguage_QNAME, PersonLanguageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "ResponseMetadata")
    public JAXBElement<ResponseMetadataType> createResponseMetadata(ResponseMetadataType value) {
        return new JAXBElement<ResponseMetadataType>(_ResponseMetadata_QNAME, ResponseMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonAugmentationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "PersonAugmentation", substitutionHeadNamespace = "http://niem.gov/niem/structures/2.0", substitutionHeadName = "Augmentation")
    public JAXBElement<PersonAugmentationType> createPersonAugmentation(PersonAugmentationType value) {
        return new JAXBElement<PersonAugmentationType>(_PersonAugmentation_QNAME, PersonAugmentationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "ResponseDescriptionText", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-core", substitutionHeadName = "Response")
    public JAXBElement<TextType> createResponseDescriptionText(TextType value) {
        return new JAXBElement<TextType>(_ResponseDescriptionText_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "Response")
    public JAXBElement<Object> createResponse(Object value) {
        return new JAXBElement<Object>(_Response_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-core", name = "ResponseCode", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-core", substitutionHeadName = "Response")
    public JAXBElement<ResponseCodeType> createResponseCode(ResponseCodeType value) {
        return new JAXBElement<ResponseCodeType>(_ResponseCode_QNAME, ResponseCodeType.class, null, value);
    }

}
