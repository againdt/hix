package com.getinsured.eligibility.ind71G.client;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GHouseHoldContact;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GMember;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GRace;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GRelationship;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71GResponsiblePerson;
import com.getinsured.hix.dto.plandisplay.ind71g.IND71Response;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse.GroupType;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.indportal.dto.Member;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;
import com.google.gson.Gson;

@Component("ind71GClientAdapter")
public class Ind71GClientAdapter {

	private static final Logger LOGGER = Logger.getLogger(Ind71GClientAdapter.class);
	
	private static final String EXCEPTION = "Exception - ";
	private static final String NEW_LINE = "\n";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION = "No Application Events found for application!";
	private static final String EMPTY_RESPONSE_FROM_IND71G_API = "Empty response from IND71G API";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";
	
	private static final String THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR = "There are no enrolled SSAPs for this household for coverage year - ";
	private static final String EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER = "Existing medical enrollment could not be retrieved for the application with casenumber ";
	private static final String GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION = "Getting enrollment details for linked enrolled application - ";
	private static final String ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION = "Error computing coverage start date current application - ";

	private static final String RESPONSE_CODE = "Response Code";
	
	private static final String US_CITIZEN = "1";
	private static final String NON_US_CITIZEN = "3";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private CoverageStartDateService coverageStartDateService;

	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private IntegrationLogService integrationLogService;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired private Gson platformGson;
	
	@Autowired private SsapUtil ssapUtil;
	
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	
	private List<String> csPriorityList = new ArrayList<>(Arrays.asList("CS1", "CS4", "CS5", "CS6", "CS3", "CS2"));

	public String execute(Long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse, String isChangePlan) {

		StringBuilder finalPayload = new StringBuilder();

		String status = GhixConstants.RESPONSE_FAILURE;
		Ind71GRequest request = null;
		String ind71gResponseJson = null;
		IND71Response iND71Response = null;
		try {
			request = formRequest(ssapApplicationId, aptcUpdate,enrollmentResponse, isChangePlan, null);
			LOGGER.info("ind71g request "+request);
			try{
				/*
        		 * this had to be changed as when called from web the logged in user is lost
        		 * this is done same as when called from LCE auto flow we pass exadmin user   
        		 */
				ind71gResponseJson =  ghixRestTemplate.exchange(GhixEndPoints.PLANDISPLAY_URL+ "/api/ind71g/", ReferralConstants.EXADMIN_USERNAME, 
						HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,request).getBody();
    		}catch(Exception e){
    			LOGGER.error("Exception occured for calling IND71G api",e);
    			throw new GIRuntimeException("Error occoured while calling IND71G : " + ind71gResponseJson);
    		}
        	if (ind71gResponseJson == null) {
				finalPayload.append(EMPTY_RESPONSE_FROM_IND71G_API);
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_IND71G_API);
			}else {
				try{
	        		iND71Response = platformGson.fromJson(ind71gResponseJson, IND71Response.class);
	        	}catch(Exception e){
	    			throw new GIRuntimeException("Invalid response in IND71G for application id : " + ssapApplicationId);
	    		}
				if ("200".equalsIgnoreCase(iND71Response.getResponseCode())) {
					status = GhixConstants.RESPONSE_SUCCESS;
				} else {
					/* Donot throw exception as we have persisted events */			
				}
			}
			finalPayload.append(NEW_LINE).append(RESPONSE_CODE).append(iND71Response.getResponseCode());
			
		} catch (Exception e) {
			if (request != null){
				try {
					String requestJson = platformGson.toJson(request);
					finalPayload.append(NEW_LINE).append("Request - ").append(requestJson);
				} catch (Exception e1) {
					/* Donot throw exception as we have persisted events. This is only for logging invalid request */
				}
			}
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}

		return status;
	}


	private Ind71GRequest formRequest(Long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse, String isChangePlan, Map<String, Set<String>> disenrollMap) {

		SsapApplication currentApplication = validate(ssapApplicationId);

		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = getEnrollmentDetails(currentApplication);
		Date coverageDate = computeCoverageStartDate(currentApplication.getId(), enrollmentsByTypeMap);

		EnrollmentShopDTO healthEnrollmentShopDTO = enrollmentsByTypeMap.get(HEALTH);

		if(healthEnrollmentShopDTO != null && ReferralUtil.isCoverageDatePastEnrollmentDate(coverageDate, healthEnrollmentShopDTO.getCoverageEndDate())) {
			throw new GIRuntimeException(ReferralConstants.COVERAGE_DATE_IS_PAST_ENROLLMENT_DATE + ReferralConstants.COVERAGE_DATE_STR_VAL + coverageDate + ReferralConstants.CURRENT_ENROLLMENT_END_DATE_STR_VAL + healthEnrollmentShopDTO.getCoverageEndDate());
		}

		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		SsapApplicant responsiblePersonApplicantDB = ssapUtil.getPrimaryTaxFilerApplicantFromApplication(currentApplication);
		HouseholdMember responsiblePersonApplicantJson = ssapUtil.getApplicantJson(members, responsiblePersonApplicantDB);

		SsapApplicant householdContactApplicantDB = ssapUtil.getPrimaryContactApplicantFromApplication(currentApplication);
		HouseholdMember householdContactApplicantJson = ssapUtil.getApplicantJson(members, householdContactApplicantDB);
		
		if (responsiblePersonApplicantDB == null || responsiblePersonApplicantJson == null) {
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}

		Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		if (ssapApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}

		// populate applicant event map to draw enrollment ADD, TERM, OTHER sections
		Map<String, SsapApplicantEvent> ssapApplicantEventMap = new HashMap<String, SsapApplicantEvent>();
		List<SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent ssapApplicantEvent : applicantEvents) { 
			ssapApplicantEventMap.put(ssapApplicantEvent.getSsapApplicant().getApplicantGuid(), ssapApplicantEvent);
		}

		Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && 
					((ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) 
							&& ChangeType.OTHER.name().equals(applicantEvent.getSepEvents().getChangeType())) 
							|| (ChangeType.ADD.name().equals(applicantEvent.getSepEvents().getChangeType())))) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (((!ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus())) || ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())))
					&& ChangeType.REMOVE.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyIneigibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyIneligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		/**
		 * HIX-112981 Handle age out, add age out member to the ineligibleApplicant list.
		 * The disenroll map contains the list of members to be aged out per insurance type.
		 * Adding the members in the disenroll map to the ineligible applicant list.
		 */
		if (null != disenrollMap) {
			Set<String> ageOutApplicantGuid = disenrollMap.values().stream().flatMap(Set::stream)
					.collect(Collectors.toSet());
			for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
				if (ageOutApplicantGuid.contains(applicant.getApplicantGuid())) {
					for (HouseholdMember householdMember : members) {
						if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
							newlyIneigibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
							break;
						}
					}
					newlyIneligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
				}
			}
		}

		Map<String, SsapApplicant> changeEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.OTHER.name().equals(applicantEvent.getSepEvents().getChangeType())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						changeEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				changeEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		// HLT
		Ind71GRequest request;
		try {
			request = populateHealthEnrollment(newlyEligibleApplicantDBRowMap, newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, 
					newlyEligibleApplicantJsonRowMap, newlyIneigibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap,  
					responsiblePersonApplicantJson, ssapApplicantEventMap,  household, currentApplication, healthEnrollmentShopDTO, 
					coverageDate, enrollmentsByTypeMap, aptcUpdate, enrollmentResponse,isChangePlan,householdContactApplicantJson, disenrollMap);
		} catch (GIException | ParseException e) {
			throw new GIRuntimeException("Error forming AutoRenewalRequest object" , ExceptionUtils.getStackTrace(e));
		}


		return request;
	}

	private Ind71GRequest populateHealthEnrollment(Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap,
			Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap,
			Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap,
			Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap,
			Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, HouseholdMember responsiblePersonApplicantJson,
			Map<String, SsapApplicantEvent> applicantEventMap, Household household, 
			SsapApplication currentApplication, EnrollmentShopDTO healthEnrollmentShopDTO, Date coverageDate, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap,
			AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse, String isChangePlan,HouseholdMember householdContactApplicantJson, Map<String, Set<String>> disenrollMap) throws GIException, ParseException {
		
		Ind71GRequest ind71GRequest = new Ind71GRequest();
		ind71GRequest.setApplicationId(String.valueOf(currentApplication.getId()));
		ind71GRequest.setEnrollmentType("S");
		ind71GRequest.setHouseholdCaseId(String.valueOf(currentApplication.getCmrHouseoldId()));
		ind71GRequest.setAllowChangePlan(isChangePlan);
		
		AptcHistory aptcHistory = aptcHistoryRepository.findLatestSlcspAmount(currentApplication.getId()); 
		if(aptcHistory != null && aptcHistory.getSlcspAmount() != null) {
			ind71GRequest.setEhbAmount(new Float(aptcHistory.getSlcspAmount().toString()));
			}
		
		if (responsiblePersonApplicantJson == null){
			throw new GIRuntimeException("responsiblePersonApplicantJson cannot be null!");
		}
		
		/* responsiblePerson section */
		IND71GResponsiblePerson responsiblePerson = getResponsiblePerson(responsiblePersonApplicantJson);
		ind71GRequest.setResponsiblePerson(responsiblePerson);

		/* houseHoldContact section */
		IND71GHouseHoldContact houseHoldContact = getHouseHoldContact(householdContactApplicantJson);
		ind71GRequest.setHouseHoldContact(houseHoldContact);
		
		/* enrollment section */
		List<IND71GEnrollment> enrollments = getEnrollments(newlyEligibleApplicantDBRowMap,
				otherEligibleApplicantDBRowMap, newlyEligibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap,
				responsiblePersonApplicantJson, applicantEventMap, household, currentApplication,newlyIneligibleApplicantDBRowMap, newlyIneigibleApplicantJsonRowMap, coverageDate,
				aptcUpdate, enrollmentResponse, disenrollMap);
		
		ind71GRequest.setEnrollments(enrollments);
		
		return ind71GRequest;
	}
	
	private List<IND71GEnrollment> getEnrollments( Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap,
			Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap,
			Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, HouseholdMember responsiblePersonApplicantJson,
			Map<String, SsapApplicantEvent> applicantEventMap, Household household, 
			SsapApplication currentApplication, Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap, Date coverageDate, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse, Map<String, Set<String>> disenrollMap) {
		
		LOGGER.info("getEnrollments: aptcUpdate: "+aptcUpdate + " newlyEligibleApplicantDBRowMap: "+newlyEligibleApplicantDBRowMap);
		List<IND71GEnrollment> enrollments = new ArrayList<IND71GEnrollment>();
		Map<String,Object> enrollmentDetails = getEnrollmentsInApplication(enrollmentResponse); 
		List<Integer> enrollmentId = null;
		Integer dentalEnrollmentId = null;
		if(enrollmentDetails!=null && !enrollmentDetails.isEmpty()) {
			dentalEnrollmentId = (Integer)enrollmentDetails.get("Dental");
			enrollmentId = (List<Integer>) enrollmentDetails.get("Enrollment");
		}
		LOGGER.info("getEnrollments: enrollmentDetails: "+enrollmentDetails);
		List<String> addMemberGuid = new ArrayList<String>();
		List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			ApplicantStatusEnum temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE == temp || ApplicantStatusEnum.ADD_NEW_ELIGIBILE == temp ) {
				addMemberGuid.add(ssapApplicant.getApplicantGuid());
			}
		}
	
		Map<String,Group> enrollmentAptc =  getEnrollmentAPTC(aptcUpdate);
		LOGGER.info("getEnrollments: enrollmentAptc: "+enrollmentAptc);
		
		String insuranceType = "Health";
		Map<Integer,List<String>> enrollmentWiseMember = getEnrollmentWiseMember(enrollmentResponse);
		HashSet<String> enrollmentWiseCSLevels = null;
		
		Map<String, String> personToGuidMap = populatePersonToGuidMap(currentApplication);
		if(!CollectionUtils.isEmpty(enrollmentId)){
			for (Integer enrollmentid : enrollmentId) {
				IND71GEnrollment iND71GEnrollment = new IND71GEnrollment();
				List<IND71GMember> members = new ArrayList<IND71GMember>();
				enrollmentWiseCSLevels = new HashSet<String>();
				Set<String> removedMembers = newlyIneligibleApplicantDBRowMap.keySet();
				int removeCase = !removedMembers.isEmpty()? removedMembers.size():0;
				insuranceType = "Health";
				iND71GEnrollment.setEnrollmentId(String.valueOf(enrollmentid));
				if(dentalEnrollmentId!=null && dentalEnrollmentId.equals(enrollmentid)) {
					insuranceType = "Dental";
				}
				
				iND71GEnrollment.setInsuranceType(insuranceType);
				Group group = null;
				if(enrollmentAptc!=null && !enrollmentAptc.isEmpty()) {
					group=enrollmentAptc.get(String.valueOf(enrollmentid));
				}
				if(group!=null) {
					LOGGER.info("getEnrollments: group: "+group);
					BigDecimal maxAptc = group.getMaxAptc();
					BigDecimal maxStateSubsidy = group.getMaxStateSubsidy();
					Set<String> keys = enrollmentAptc.keySet();
					LOGGER.info("getEnrollments: keys: "+keys + " maxAptc: "+maxAptc + " maxStateSubsidy: " + maxStateSubsidy);
					int i = 0;
					for(String key:keys) {
						if(key.contains("H-")) {
							if(enrollmentAptc.get("H-"+i)!=null) {
								Group subGroup = enrollmentAptc.get("H-"+i);
								List<Member> memberList = subGroup.getMemberList();
								LOGGER.info("getEnrollments: memberList: "+memberList);
								if(memberList!=null) {
									for(Member member:memberList) {
										if(addMemberGuid.contains(member.getId())){
											if(member.getAptc()!=null) {
												if(maxAptc != null) {
													maxAptc = maxAptc.add(member.getAptc());	
												}												
											}
											if(member.getStateSubsidy()!=null) {
												if(maxStateSubsidy != null) {
													maxStateSubsidy = maxStateSubsidy.add(member.getStateSubsidy());
												}
											}
										}
									}
								}
							}
							i++;
						}
					}
					if(maxAptc!=null) {
						iND71GEnrollment.setAptc(maxAptc.floatValue());
					}
					if(maxStateSubsidy!=null) {
						iND71GEnrollment.setStateSubsidy(maxStateSubsidy);
					}
				}
								
				iND71GEnrollment.setCsr(currentApplication.getCsrLevel());
				iND71GEnrollment.setCoverageDate(formWSDate(aptcUpdate.getAptcEffectiveDate() != null ? aptcUpdate.getAptcEffectiveDate() :coverageDate));
				iND71GEnrollment.setFinancialEffectiveDate(formWSDate(aptcUpdate.getAptcEffectiveDate() != null ? aptcUpdate.getAptcEffectiveDate() :coverageDate));
				List<String> enrolledMemberList =  enrollmentWiseMember.get(enrollmentid);
				
				for (String othereligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
					if((enrollmentWiseMember.get(enrollmentid)).contains(othereligibleApplicantGuid)) {
						SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(othereligibleApplicantGuid);
						HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(othereligibleApplicantGuid);
						SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(othereligibleApplicantGuid);
						
						IND71GMember iND71GMember = populateMember(memberDB, memberJson, 
								responsiblePersonApplicantJson, ssapApplicantEvent, household, 
								"NOCHANGE", personToGuidMap,aptcUpdate.getAptcEffectiveDate(),enrolledMemberList,removeCase,insuranceType, disenrollMap);
						members.add(iND71GMember);
						enrollmentWiseCSLevels.add(memberDB.getCsrLevel() != null ? memberDB.getCsrLevel() : "CS1");
					}
				}
				for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
					if((enrollmentWiseMember.get(enrollmentid)).contains(newIneligibleApplicantGuid)) {
						SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
						HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
						SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
						
						IND71GMember iND71GMember = populateMember(memberDB, memberJson, 
								responsiblePersonApplicantJson, ssapApplicantEvent, household, 
								"REMOVE", personToGuidMap,aptcUpdate.getAptcEffectiveDate(),enrolledMemberList,removeCase,insuranceType, disenrollMap);
						members.add(iND71GMember);
					}
				}
				for (String newApplicantGuid : newlyEligibleApplicantDBRowMap.keySet()) {
					/**
					 * The enrollmentId size condition (!CollectionUtils.isEmpty(enrollmentId) && enrollmentId.size()==1) 
					 *  is removed as we are going support health and dental both
					 * and we have a condition for checking auto add in SEP depending on one flag 
					 *  but we need to check the count if this is called independently from third party 
					 */					 
					if(!CollectionUtils.isEmpty(enrollmentId)) {
						SsapApplicant memberDB = newlyEligibleApplicantDBRowMap.get(newApplicantGuid);
						HouseholdMember memberJson = newlyEligibleApplicantJsonRowMap.get(newApplicantGuid);
						SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newApplicantGuid);
						
						IND71GMember iND71GMember = populateMember(memberDB, memberJson, 
								responsiblePersonApplicantJson, ssapApplicantEvent, household, 
								"ADD", personToGuidMap,aptcUpdate.getAptcEffectiveDate(),enrolledMemberList,removeCase,insuranceType, disenrollMap);
						members.add(iND71GMember);
						enrollmentWiseCSLevels.add(memberDB.getCsrLevel() != null ? memberDB.getCsrLevel() : "CS1");
					}
				}
				iND71GEnrollment.setMembers(members);
				if(!enrollmentWiseCSLevels.isEmpty()) {
					iND71GEnrollment.setCsr(getGroupCSLevel(enrollmentWiseCSLevels));
				}
				enrollments.add(iND71GEnrollment);
			}
		}
		return enrollments;
	}
	
	private Map<String,Object> getEnrollmentsInApplication(EnrollmentResponse enrollmentResponse){
		Map<String,Object> enrollmentDetails = new HashMap<String,Object>();
		List<Integer> enrollmentId = new ArrayList<Integer>();
		Integer dentalEnrollmentId = null;
		if(enrollmentResponse != null) {
			Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();
			if(enrolleeData != null) {
				for(String ssapApplicantGuid : enrolleeData.keySet()) {
					List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicantGuid);
					if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {
						for(EnrollmentMemberDataDTO enrollmentDTO : enrollmentDTOList) {
							enrollmentId.add(enrollmentDTO.getEnrollmentID());
							if("Dental".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
								dentalEnrollmentId = enrollmentDTO.getEnrollmentID();
							}
						}
					}
				}
			}
		}
		if(!enrollmentId.isEmpty()) {
			Set<Integer> enrollmentSet = new HashSet<Integer>();
			enrollmentId.removeIf(p -> !enrollmentSet.add(p));
        }
		enrollmentDetails.put("Enrollment", enrollmentId);
	    if(dentalEnrollmentId!=null) {
	    	enrollmentDetails.put("Dental", dentalEnrollmentId);
	    }
		return enrollmentDetails;
	}
	
	private Map<String,Group> getEnrollmentAPTC(AptcUpdate aptcUpdate){
		Map<String,Group> enrollmentAPTCDetails = new HashMap<String,Group>();
		if(aptcUpdate!= null && aptcUpdate.getAppGroupResponse()!=null && !CollectionUtils.isEmpty(aptcUpdate.getAppGroupResponse().getGroupList())){
			int i = 0;
			for(Group group:aptcUpdate.getAppGroupResponse().getGroupList()) {
				if(StringUtils.isNotEmpty(group.getGroupEnrollmentId())) {
					
					enrollmentAPTCDetails.put(group.getGroupEnrollmentId(), group);
				}else if((group.getGroupType()).equals(GroupType.HEALTH)){
					enrollmentAPTCDetails.put("H-"+i, group);
					i++;
				}
			}
		}
		
		return enrollmentAPTCDetails;
	}

	private Map<Integer,List<String>> getEnrollmentWiseMember(EnrollmentResponse enrollmentResponse){
		Map<Integer,List<String>> memberDetails = new HashMap<Integer,List<String>>();
		if(enrollmentResponse != null) {
			Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();
			if(enrolleeData != null) {
				for(String ssapApplicantGuid : enrolleeData.keySet()) {
					List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicantGuid);
					if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {
						for(EnrollmentMemberDataDTO enrollmentDTO : enrollmentDTOList) {
							List<String> memberId = memberDetails.get(enrollmentDTO.getEnrollmentID());
							if(memberId==null) {
								memberId = new ArrayList<String>();
								memberId.add(ssapApplicantGuid);
								memberDetails.put(enrollmentDTO.getEnrollmentID(), memberId);
							}
							else {
								memberId.add(ssapApplicantGuid);
								memberDetails.put(enrollmentDTO.getEnrollmentID(), memberId);
							}
						}
					}
				}
			}
		}
		return memberDetails;
	}
	
	
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;


	
	private Map<String, String> populatePersonToGuidMap(SsapApplication currentApplication) {
		
		Map<String, String> personToGuidMap = new HashMap<>();
		List<SsapApplicant> applicants = currentApplication.getSsapApplicants();
		
		for (SsapApplicant ssapApplicant : applicants) {
			personToGuidMap.put(String.valueOf(ssapApplicant.getPersonId()), ssapApplicant.getApplicantGuid());
		}
		return personToGuidMap;
	}


	private IND71GMember populateMember(SsapApplicant memberDB, HouseholdMember memberJson,
			HouseholdMember responsiblePersonApplicantJson, SsapApplicantEvent ssapApplicantEvent,
			Household household, String type, Map<String, String> personToGuidMap,Date coverageDate, List<String> enrolledMemberList, int removeCase,String insuranceType, Map<String, Set<String>> disenrollMap) {

		IND71GMember member = new IND71GMember();
		boolean isAgeOutScenario = null != disenrollMap && !disenrollMap.isEmpty();

		member.setMemberId(memberDB.getApplicantGuid());
		member.setMaintenanceReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());
		
		member.setNewPersonFlag("N");
		if (StringUtils.equalsIgnoreCase("ADD", type)) {
			member.setNewPersonFlag("Y"); 
		}
		member.setDisenrollMemberFlag("N");
		
		boolean disEnrollFlag = false;
		// for CA the config would be false and we want to disenroll from dental of the elig is NONE
		// for other states config would be true and we want to disenroll from dental if elig is not QHP
		final String disenrollFromDental =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DISENROLL_FROM_DENTAL);
		if(!isAgeOutScenario && StringUtils.equalsIgnoreCase("REMOVE", type) && ("Health".equalsIgnoreCase(insuranceType) || 
				("Dental".equalsIgnoreCase(insuranceType) && ((disenrollFromDental.equalsIgnoreCase(Boolean.TRUE.toString()) 
						&& !StringUtils.equalsIgnoreCase(ReferralConstants.QHP, memberDB.getEligibilityStatus())) ||
					(disenrollFromDental.equalsIgnoreCase(Boolean.FALSE.toString()) 
					&& StringUtils.equalsIgnoreCase(ReferralConstants.ELIGIBLE_NONE, memberDB.getEligibilityStatus())))						
				))) {
			disEnrollFlag = true;
		}
		//HIX-112981 Condition for age out removal
		//Set disenrollFlag as true for members present in the disenroll map for the respective insurance type
		else if(isAgeOutScenario && StringUtils.equalsIgnoreCase("REMOVE", type)) {
			for(String key : disenrollMap.keySet()) {
				if(key.toLowerCase().contains(insuranceType.toLowerCase())){
					disEnrollFlag = disenrollMap.get(key).contains(memberDB.getApplicantGuid());	
				}
			}
		}
		
		//will remove member from health always, remove from dental only if elig status is none and config is false or elig status is not QHP and config is true
		if (disEnrollFlag) {
			member.setDisenrollMemberFlag("Y"); 
			DateTime terminationDate = new DateTime(coverageDate);
			if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {
				terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
			}
			member.setTerminationDate(formWSDate(new Date(terminationDate.getMillis())));
			
			if ("DEATH".equals(ssapApplicantEvent.getSepEvents().getName())){
				member.setDeathDate(formWSDate(ssapApplicantEvent.getEventDate()));
			}
			member.setTerminationReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());
		}
		
		member.setFirstName(memberDB.getFirstName());
		member.setLastName(memberDB.getLastName());
		member.setMiddleName(memberDB.getMiddleName());
		member.setSuffix(memberDB.getNameSuffix());
		
		member.setDob(formWSDate(memberDB.getBirthDate()));
		member.setSsn(memberDB.getSsn());
		
		populateTobaccoUsageDetails( member, memberDB);
		
		String primaryPhone = null, secondaryPhone = null;
		if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) && !StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
			secondaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		}
		
		if (StringUtils.isEmpty(primaryPhone)) {
			primaryPhone = household.getPhoneNumber();
		}
		member.setPrimaryPhone(primaryPhone);
		member.setSecondaryPhone(secondaryPhone);
		if(!StringUtils.equalsIgnoreCase("REMOVE", type) || (StringUtils.equalsIgnoreCase("REMOVE", type) && !disEnrollFlag)){
			populateLocationDetails(member, memberJson, responsiblePersonApplicantJson);
			populateGenderDetails(member, memberDB);
			
			populateMaritalStatus(member, memberDB);
			
			if(memberJson.getCitizenshipImmigrationStatus()!=null) {
				if(memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null && memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
					member.setCitizenshipStatusCode(US_CITIZEN);
				}else{
					member.setCitizenshipStatusCode(NON_US_CITIZEN);
				}
			}
			
			//member.setCitizenshipStatusCode(value);
			
			String languageSpokenLkpStr = languageSpoken(memberJson);
			member.setSpokenLanguageCode(languageSpokenLkpStr);
			
			String languageWrittenLkpStr = languageWritten(memberJson);
			member.setWrittenLanguageCode(languageWrittenLkpStr);
			
			populateMailingAddress(memberJson, member);
			
			
			
			member.setResponsiblePersonRelationship(populateRelationshipToHCP(memberJson, "" + memberDB.getPersonId(), responsiblePersonApplicantJson));
			
			if (memberDB.getPersonId() == 1){
				if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
						&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
					member.setPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
				}
				if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
						&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
						&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null ) {
					member.setPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
				}
			}
			
			populateRelationship(memberDB, memberJson, responsiblePersonApplicantJson, member, personToGuidMap, enrolledMemberList,removeCase);
	
			populateRaces(memberJson, member);
			
			member.setFinancialHardshipExemption("Y".equals(memberDB.getHardshipExempt()) ? "Y" : "N");
			
			member.setCatastrophicEligible("N");
			
			//member.setChildOnlyPlanEligibile(YesNoVal.N);
			
			member.setExternalMemberId(memberDB.getExternalApplicantId());
		}
		return member;

	}


	private void populateRaces(HouseholdMember memberJson, IND71GMember member) {
		if(memberJson.getEthnicityAndRace() != null) {
			//List<Ethnicity> ethnicities = memberJson.getEthnicityAndRace().getEthnicity();
			List<Race> races = memberJson.getEthnicityAndRace().getRace();

			if(races != null) {
				for (Race race : races) {
					IND71GRace ind71GRace = new IND71GRace();
					/*if(race.getCode().equalsIgnoreCase("0000-0")){
						continue;
					}*/
					if(race.getCode().equalsIgnoreCase("2131-1")){
						ind71GRace.setDescription(race.getOtherLabel());
					}
					ind71GRace.setRaceEthnicityCode(race.getCode());
					if(member.getRace()!=null){
						member.getRace().add(ind71GRace);
					}else {
						List<IND71GRace> racesList = new ArrayList<IND71GRace>();
						racesList.add(ind71GRace);
						member.setRace(racesList);
					}
				}
			}
			
			/*if(ethnicities != null) {
				for (Ethnicity race : ethnicities) {
					IND71GRace ind71GRace = new IND71GRace();
					if(race.getCode().equalsIgnoreCase("0000-0")){
						continue;
					}
					if(race.getCode().equalsIgnoreCase("2131-1")){
						ind71GRace.setDescription(race.getOtherLabel());
					}
					ind71GRace.setRaceEthnicityCode(race.getCode());
					if(member.getRace()!=null){
						member.getRace().add(ind71GRace);
					}else {
						List<IND71GRace> racesList = new ArrayList<IND71GRace>();
						racesList.add(ind71GRace);
						member.setRace(racesList);
					}
				}
			}*/
		}
	}


	private void populateRelationship(SsapApplicant memberDB, HouseholdMember memberJson,
			HouseholdMember responsiblePersonApplicantJson, IND71GMember member, Map<String, String> personToGuidMap, List<String> enrolledMemberList,int removeCase) {
		List<BloodRelationship> bloodRelatioshipList = responsiblePersonApplicantJson.getBloodRelationship();
		String personId = String.valueOf(memberDB.getPersonId());
		
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId())) {
				String relatedMemberId = personToGuidMap.get(bloodRelationship.getRelatedPersonId());
				if((removeCase>0 && enrolledMemberList.contains(relatedMemberId)) || removeCase==0) {
					IND71GRelationship ind71GRelationship = new IND71GRelationship();
					
					ind71GRelationship.setMemberId(relatedMemberId);
					ind71GRelationship.setRelationshipCode(bloodRelationship.getRelation());
					
					if(member.getRelationships()!=null){
					    member.getRelationships().add(ind71GRelationship);
					}else {
						List<IND71GRelationship> relationships = new ArrayList<IND71GRelationship>();
						relationships.add(ind71GRelationship);
						member.setRelationships(relationships);
					}
				}
			}
		}
		
	}


	private void populateMailingAddress(HouseholdMember memberJson, IND71GMember member) {
		Address mailingAddress = memberJson.getHouseholdContact().getMailingAddress();

		member.setMailingAddress1(mailingAddress.getStreetAddress1());
		member.setMailingAddress2(mailingAddress.getStreetAddress2());
		member.setMailingCity(mailingAddress.getCity());
		member.setMailingState(mailingAddress.getState());
		member.setMailingZip(ssapUtil.getFiveDigitZipCode(mailingAddress.getPostalCode()));
	}


	private void populateMaritalStatus(IND71GMember member, SsapApplicant memberDB) {
		String maritalStatusLkpStr = memberDB.getMarried();
		if ("Yes".equalsIgnoreCase(memberDB.getMarried()) || "true".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "M";
		}else if("No".equalsIgnoreCase(memberDB.getMarried()) || "false".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "R";
		}
		member.setMaritalStatusCode(maritalStatusLkpStr);
	}

	private static final String HEALTH = "health";
	private static final String DENTAL = "dental";

	

	@SuppressWarnings("unused")
	private String getPlanId(EnrollmentShopDTO enrollment, String enrollmentType) {
		return enrollment != null ? String.valueOf(enrollment.getPlanId()) : null;
	}

	private Map<String, EnrollmentShopDTO> getEnrollmentDetails(SsapApplication currentApplication) {

		SsapApplication enrolledApplication = getEnrolledSsapForCoverageYearAndHousehold(currentApplication);

		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		try {
			enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveMedicalAndDentalEnrollments(enrolledApplication.getId());
		} catch (GIException e) {
			throw new GIRuntimeException(GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION + enrolledApplication.getCaseNumber());
		}
		if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || (null == enrollmentsByTypeMap.get(HEALTH) && null == enrollmentsByTypeMap.get(DENTAL))){			
			SsapApplication currentErSsap  = ssapApplicationRepository.findErAfterLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
			if(currentErSsap == null){
				throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());
			}			
			try {
				enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveMedicalAndDentalEnrollments(currentErSsap.getId());
			} catch (GIException e) {
				throw new GIRuntimeException(GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION + enrolledApplication.getCaseNumber());
			}
			if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || null == enrollmentsByTypeMap.get(HEALTH)){
				throw new GIRuntimeException(EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER+ currentErSsap.getCaseNumber());
			}			
		}
		return enrollmentsByTypeMap;
	}
	
	/**
	 * This method will look for EN/PN application first. If not found will look for only dental enrolled application. 
	 * 
	 * */

	public SsapApplication getEnrolledSsapForCoverageYearAndHousehold(SsapApplication currentApplication) {
		SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		if(enrolledApplication == null){
			// If EN/PN application is not found check if only dental enrolled application is found.
			enrolledApplication = ssapApplicationRepository.findOnlyDentalEnrolledSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear(),currentApplication.getId());
			if(enrolledApplication == null){
				throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());	
			}
			
		}
		return enrolledApplication;
	}

	private Timestamp computeCoverageStartDate(Long currentApplicationId, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap) {
		try {
			return coverageStartDateService.computeCoverageStartDate(BigDecimal.valueOf(currentApplicationId), 
					enrollmentsByTypeMap, 
					false,false);
		} catch (GIException e) {
			throw new GIRuntimeException(ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION + currentApplicationId);
		}
	}


	private IND71GResponsiblePerson getResponsiblePerson(HouseholdMember responsiblePersonApplicantJson) {
		
		IND71GResponsiblePerson responsiblePerson = new IND71GResponsiblePerson();

		responsiblePerson.setResponsiblePersonId(responsiblePersonApplicantJson.getApplicantGuid());
		
		responsiblePerson.setResponsiblePersonFirstName(responsiblePersonApplicantJson.getName().getFirstName());
		responsiblePerson.setResponsiblePersonLastName(responsiblePersonApplicantJson.getName().getLastName());
		responsiblePerson.setResponsiblePersonMiddleName(responsiblePersonApplicantJson.getName().getMiddleName());
		responsiblePerson.setResponsiblePersonSuffix(responsiblePersonApplicantJson.getName().getSuffix());
		responsiblePerson.setResponsiblePersonSsn(responsiblePersonApplicantJson.getSocialSecurityCard().getSocialSecurityNumber());
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			responsiblePerson.setResponsiblePersonPrimaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) != null ? responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone() != null) {
			responsiblePerson.setResponsiblePersonSecondaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
			responsiblePerson.setResponsiblePersonPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null  
				&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
				&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			responsiblePerson.setResponsiblePersonPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress() != null) { 
			responsiblePerson.setResponsiblePersonHomeAddress1(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress1()));
			responsiblePerson.setResponsiblePersonHomeAddress2(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress2()));
			responsiblePerson.setResponsiblePersonHomeCity(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getCity()));
			responsiblePerson.setResponsiblePersonHomeState(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getState()));
			responsiblePerson.setResponsiblePersonHomeZip(ssapUtil.getFiveDigitZipCode(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode()));
		}

		responsiblePerson.setResponsiblePersonType("QD"); // ???

		return responsiblePerson;
	}

	private String nullCheckedValue(String value) {
		if(StringUtils.isBlank(value)) {
			return null;
		}
		return value;
	}
	private IND71GHouseHoldContact getHouseHoldContact(HouseholdMember responsiblePersonApplicantJson) {

		IND71GHouseHoldContact houseHoldContact = new IND71GHouseHoldContact();
		
		houseHoldContact.setHouseHoldContactId(responsiblePersonApplicantJson.getApplicantGuid());
		
		houseHoldContact.setHouseHoldContactFirstName(responsiblePersonApplicantJson.getName().getFirstName());
		houseHoldContact.setHouseHoldContactLastName(responsiblePersonApplicantJson.getName().getLastName());
		houseHoldContact.setHouseHoldContactMiddleName(nullCheckedValue(responsiblePersonApplicantJson.getName().getMiddleName()));
		houseHoldContact.setHouseHoldContactSuffix(responsiblePersonApplicantJson.getName().getSuffix());

		if(responsiblePersonApplicantJson.getSocialSecurityCard() != null) {
			houseHoldContact.setHouseHoldContactFederalTaxIdNumber(responsiblePersonApplicantJson.getSocialSecurityCard().getSocialSecurityNumber());
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			houseHoldContact.setHouseHoldContactPrimaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) != null ? responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone() != null) {
			houseHoldContact.setHouseHoldContactSecondaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
			houseHoldContact.setHouseHoldContactPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
				&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			houseHoldContact.setHouseHoldContactPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
		}
		
			
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress() != null) {
			houseHoldContact.setHouseHoldContactHomeAddress1(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress1()));
			houseHoldContact.setHouseHoldContactHomeAddress2(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress2()));
			houseHoldContact.setHouseHoldContactHomeCity(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getCity()));
			houseHoldContact.setHouseHoldContactHomeState(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getState()));
			houseHoldContact.setHouseHoldContactHomeZip(ssapUtil.getFiveDigitZipCode(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode()));
		}

		return houseHoldContact;
	}

	private void populateLocationDetails(IND71GMember member, HouseholdMember memberJson, HouseholdMember responsiblePersonApplicantJson) {

		Address homeAddress = memberJson.getHouseholdContact().getHomeAddress();

		member.setCountyCode(getCounty(memberJson, responsiblePersonApplicantJson));
		member.setHomeAddress1(nullCheckedValue(homeAddress.getStreetAddress1()));
		member.setHomeAddress2(nullCheckedValue(homeAddress.getStreetAddress2()));
		member.setHomeCity(nullCheckedValue(homeAddress.getCity()));
		member.setHomeState(nullCheckedValue(homeAddress.getState()));
		member.setHomeZip(ssapUtil.getFiveDigitZipCode(homeAddress.getPostalCode()));
	}
	
	private String getCounty(HouseholdMember applicantJsonRow, HouseholdMember responsiblePersonApplicantJson) {
		String countyCode = responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}

	private void populateGenderDetails(IND71GMember member, SsapApplicant memberDB) {
		String genderLkpStr = "Male".equalsIgnoreCase(memberDB.getGender()) ? "M" : "F";
		member.setGenderCode(genderLkpStr);
	}

	private String populateRelationshipToHCP(HouseholdMember memberJson, String personId, HouseholdMember responsiblePersonApplicantJson) {
		String relatioshipToHCPStr = null;

		List<BloodRelationship> bloodRelatioshipList = responsiblePersonApplicantJson.getBloodRelationship();
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId()) && String.valueOf(PRIMARY_PERSON_ID).equalsIgnoreCase(bloodRelationship.getRelatedPersonId())) {
				relatioshipToHCPStr = bloodRelationship.getRelation();
			}
		}
		return relatioshipToHCPStr;
	}

	private String languageSpoken(HouseholdMember memberJson) {
		String languageSpoken = null;
		String languageSpokenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageSpoken = memberJson.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
		}

		if(StringUtils.isNotBlank(languageSpoken)) {
			languageSpokenCode = LanguageEnum.getCode(languageSpoken);								
		}
		return languageSpokenCode;
	}

	private String languageWritten(HouseholdMember memberJson) {
		String languageWritten = null;
		String languageWrittenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageWritten = memberJson.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
		}

		if(StringUtils.isNotBlank(languageWritten)) {
			languageWrittenCode = LanguageEnum.getCode(languageWritten);								
		}
		return languageWrittenCode;
	}

	private void populateTobaccoUsageDetails(IND71GMember member, SsapApplicant memberDB) {
		String tobaccoUsage = "N";
		if (memberDB.getTobaccouser() != null) {
			tobaccoUsage = StringUtils.equalsIgnoreCase(ReferralConstants.Y, memberDB.getTobaccouser()) ? "Y" : "N";
		}
		else{
			tobaccoUsage = null;
		}
		member.setTobacco(tobaccoUsage);
	}
	

	private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);

		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);

		if (currentApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}

	private static final String WS_DATE_FORMAT = "MM/dd/yyyy";

	private static final String IND71G = "IND71G";

	private static String formWSDate(Date date) {
		if(date != null){
			SimpleDateFormat WS_SIMPLE_DATE_FORMATTER = new SimpleDateFormat(WS_DATE_FORMAT);
			return WS_SIMPLE_DATE_FORMATTER.format(date);	
		}
		else{
			return "";
		}
	}
	
	
	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, IND71G, status, ssapApplicationId, null);
	}


	/**
	 * Overloaded execute method called by the age out service
	 * @param ssapApplicationId
	 * @param aptcUpdate
	 * @param enrollmentResponse
	 * @param isChangePlan
	 * @param disenrollMap
	 * @return
	 */
	public String execute(long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse,
			String isChangePlan, Map<String, Set<String>> disenrollMap) {
		StringBuilder finalPayload = new StringBuilder();

		String status = GhixConstants.RESPONSE_FAILURE;
		Ind71GRequest request = null;
		String ind71gResponseJson = null;
		IND71Response iND71Response = null;
		try {
			request = formRequest(ssapApplicationId, aptcUpdate,enrollmentResponse, isChangePlan, disenrollMap);
			LOGGER.info("ind71g request "+request);
			try{
				/*
        		 * this had to be changed as when called from web the logged in user is lost
        		 * this is done same as when called from LCE auto flow we pass exadmin user   
        		 */
				ind71gResponseJson =  ghixRestTemplate.exchange(GhixEndPoints.PLANDISPLAY_URL+ "/api/ind71g/", ReferralConstants.EXADMIN_USERNAME, 
						HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,request).getBody();
    		}catch(Exception e){
    			LOGGER.error("Exception occured for calling IND71G api",e);
    			throw new GIRuntimeException("Error occoured while calling IND71G : " + ind71gResponseJson);
    		}
        	if (ind71gResponseJson == null) {
				finalPayload.append(EMPTY_RESPONSE_FROM_IND71G_API);
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_IND71G_API);
			}else {
				try{
	        		iND71Response = platformGson.fromJson(ind71gResponseJson, IND71Response.class);
	        	}catch(Exception e){
	    			throw new GIRuntimeException("Invalid response in IND71G for application id : " + ssapApplicationId);
	    		}
				if ("200".equalsIgnoreCase(iND71Response.getResponseCode())) {
					status = GhixConstants.RESPONSE_SUCCESS;
				} else {
					/* Donot throw exception as we have persisted events */			
				}
			}
			finalPayload.append(NEW_LINE).append(RESPONSE_CODE).append(iND71Response.getResponseCode());
			
		} catch (Exception e) {
			if (request != null){
				try {
					String requestJson = platformGson.toJson(request);
					finalPayload.append(NEW_LINE).append("Request - ").append(requestJson);
				} catch (Exception e1) {
					/* Donot throw exception as we have persisted events. This is only for logging invalid request */
				}
			}
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}

		return status;
	}
	
	private String getGroupCSLevel(HashSet<String> enrollmentCSLevels){
		String CSLevel = null;
		Integer lowestIndex = null;
		Integer currentIndex = null;
		if(CollectionUtils.isNotEmpty(enrollmentCSLevels)) {
			for(String memberCS : enrollmentCSLevels) {
				currentIndex = csPriorityList.indexOf(memberCS);
				if(lowestIndex == null || lowestIndex>currentIndex) {
					lowestIndex = currentIndex;
				}
			}
			CSLevel = (lowestIndex != null && lowestIndex != -1) ? csPriorityList.get(lowestIndex) : "CS1" ;
		}
		return CSLevel;
	}

}
