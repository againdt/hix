package com.getinsured.eligibility.at.ref.si;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.AssisterTypeEnum;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ExternalAssisterEndPoints;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;

/**
 * This service is used to invoke External Assister Designate/De-designate api for storing Assister information
 * @author sahasrabuddhe_n
 *
 */
@Component("externalAssisterService")
@Scope("singleton")
public class ExternalAssisterService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalAssisterService.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private Gson platformGson;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	
	/**
	 * This makes a call to external assister api by passing the requried parameters 
	 * @param assisterInfo
	 * @param householdId
	 */
	public void callExternalAssisterDesignateAPI(AssisterType assisterInfo, int householdId) {
		LOGGER.info("Calling External Assister Designate/De-Designate API for householdID : "+householdId);
		try {

			DesignateAssisterRequest request = new DesignateAssisterRequest();
			request.setHouseholdId(householdId);
			// set the request object from Assisterinfo
			request.setAssisterName(
					assisterInfo.getRolePlayedByPerson().getPersonName().get(0).getPersonGivenName().getValue());
			String assisterType = assisterInfo.getRolePlayedByPerson().getPersonAugmentation()
					.getPersonOrganizationAssociation().getOrganization().getOrganizationName().getValue();
			AssisterTypeEnum[] enums = AssisterTypeEnum.values();
			for(int i=0;i<enums.length;i++) {
				if(enums[i].assisterTypeValue.equals(assisterType)){
					assisterType =enums[i].assisterTypeCode ;
					break;
				}
			}
			request.setAssisterType(DesignateAssisterRequest.ASSISTER_TYPE
					.valueOf(assisterType));
			final int isize = assisterInfo.getRolePlayedByPerson().getPersonAugmentation().getPersonIdentification()
					.size();
			IdentificationType temp;
			for (int i = 0; i < isize; i++) {
				temp = assisterInfo.getRolePlayedByPerson().getPersonAugmentation().getPersonIdentification().get(i);
				if (ReferralUtil.isValidString(ReferralUtil.getValue(temp.getIdentificationCategoryText())) && temp
						.getIdentificationCategoryText().getValue().equalsIgnoreCase(ReferralProcessingConstants.ASSISTER_EXTERNALID)) {
					if (null != temp.getIdentificationID() && null != temp.getIdentificationID().getValue()) {
						request.setExternalAssisterId(temp.getIdentificationID().getValue());
					}

				}
				if (ReferralUtil.isValidString(ReferralUtil.getValue(temp.getIdentificationCategoryText()))
						&& temp.getIdentificationCategoryText().getValue().equalsIgnoreCase(ReferralProcessingConstants.BROKER_NPN)) {
					if (null != temp.getIdentificationID() && null != temp.getIdentificationID().getValue()) {
						request.setAgentNpn(temp.getIdentificationID().getValue());
					}

				}
			}
			if (null != assisterInfo.getRolePlayedByPerson().getPersonAugmentation().getPersonOrganizationAssociation()
					.getOrganization().getOrganizationIdentification()) {
				String federalTaxId = assisterInfo.getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification()
						.getIdentificationID().getValue();
				request.setFederalTaxId(federalTaxId);
			}
			
			boolean assisterUpdate = false;			
			DesignateAssisterResponse designateAssisterResponse = getExternalBrokerDetails(Integer.valueOf(request.getHouseholdId()).toString());
			
			if (designateAssisterResponse != null ) {

				if (designateAssisterResponse.getAssisterName() != null && request.getAssisterName() != null && !designateAssisterResponse.getAssisterName().equalsIgnoreCase(request.getAssisterName())) {
					assisterUpdate = true;
				}
				if (designateAssisterResponse.getAgentNpn() != null && request.getAgentNpn() != null && !designateAssisterResponse.getAgentNpn().equalsIgnoreCase(request.getAgentNpn())) {
					assisterUpdate = true;
				}
				if (designateAssisterResponse.getFederalTaxId() != null && request.getFederalTaxId() != null && !designateAssisterResponse.getFederalTaxId().equalsIgnoreCase(request.getFederalTaxId())) {
					assisterUpdate = true;
				}
				if (designateAssisterResponse.getAssisterType() != null && request.getAssisterType() != null && !designateAssisterResponse.getAssisterType().equalsIgnoreCase(request.getAssisterType())) {
					assisterUpdate = true;
				}
				if (designateAssisterResponse.getExternalAssisterId() != null && request.getExternalAssisterId() != null && !designateAssisterResponse.getExternalAssisterId().equalsIgnoreCase(request.getExternalAssisterId())) {
					assisterUpdate = true;
				}
				
				Integer userId = ssapApplicationRepository.getUserIdForExternalAssisterId(designateAssisterResponse.getExternalAssisterId());

				if ((userId != null && designateAssisterResponse.getAssisterUserId() != 0 && designateAssisterResponse.getAssisterUserId() != userId) || (userId != null && designateAssisterResponse.getAssisterUserId() == 0)) {
					assisterUpdate = true;
				}
				
			}
			
			ResponseEntity<String> res = null;
			if(designateAssisterResponse.getExternalAssisterId() == null || designateAssisterResponse!=null && assisterUpdate) { 
			res = ghixRestTemplate.exchange(
					GhixEndPoints.ExternalAssisterEndPoints.DESIG_REDESIG_DEDESIG_ASSISTER,
					ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
					platformGson.toJson(request));
			}
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("ExternalAssisterService callExternalAssisterAPI ends - Response is - {}",res);
			}

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder()
					.append(ReferralProcessingConstants.ERROR_CALLING_EXTERNAL_ASSISTER)
					.append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}

	}
	
	public void callExternalAssisterDeDesignateAPI(int householdId) {
		DesignateAssisterRequest request = new DesignateAssisterRequest();
		request.setHouseholdId(householdId);
		
		ResponseEntity<String> res = ghixRestTemplate.exchange(
				GhixEndPoints.ExternalAssisterEndPoints.DESIG_REDESIG_DEDESIG_ASSISTER,
				ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
				platformGson.toJson(request));		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("ExternalAssisterService callExternalDeDesignateAPI ends - Response is - {}",res);
		}
	}
	
	public DesignateAssisterResponse getExternalBrokerDetails(String cmrHouseholdId) {
		String designateAssisterResponse = null;
		if (cmrHouseholdId != null) {
			int householdId = Integer.valueOf(cmrHouseholdId);
			designateAssisterResponse = ghixRestTemplate.getForObject(ExternalAssisterEndPoints.GET_ASSISTER_DETAIL_FOR_HOUSEHOLD + "/" + householdId ,String.class);
			if(designateAssisterResponse==null){
				LOGGER.error("No external Broker data found : got respose as null "+ExternalAssisterEndPoints.GET_ASSISTER_DETAIL_FOR_HOUSEHOLD + cmrHouseholdId);
			}else{
				return platformGson.fromJson(designateAssisterResponse, DesignateAssisterResponse.class);
			}
		}
		return null;
	}
}
