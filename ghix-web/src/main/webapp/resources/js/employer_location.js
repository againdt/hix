var imgpath = '/hix/resources/images/requiredAsterix.png';

//for accessibility
var additionWorksite = '<span class="aria-hidden">An Additional Worksite is added, please tab and complete it.</span>';

var employer_location = {
	line_index: 1,
	location_row:
		'<div id="employer_location_FID" class="addressBlock">\n' +
		'<input type="hidden" name="locations[FID].primaryLocation" id="primaryLocation_FID" value="NO">' +
		'<input type="hidden" name="locations[FID].location.id" id="id_FID" value="0">' +
		'<input type="hidden" name="locations[FID].id" id="employer_location_id_FID" value="0">' +
		'<input type="hidden" name="locations[FID].location.lat" id="lat_FID" value="0.0" >' +
		'<input type="hidden" name="locations[FID].location.lon" id="lon_FID" value="0.0" >' +
		'<input type="hidden" id="rdi_FID" name="rdi_FID" value="" />' +
		
		'<h4> Additional Worksite <div class="pull-right"><a class="pull-right btn-small btn-danger" href="javascript:employer_location.delete_line(\'FID\')" id="remove">Remove <span class="aria-hidden">Additional Worksite</span></a></div> </h4>\n' +   
		'<div class="profile">\n' +
		'<div class="control-group">\n' +
		                '<label for="address1_FID" class="control-label required">'+ additionWorksite +' Address 1 <img src=\"' + imgpath + '\" alt="required"></label>\n' +
		                '<div class="controls">\n' +
		                                '<input type="text" name="locations[FID].location.address1" id="address1_FID"  class="input-xlarge" size="30">\n' +
		                               '<div id="address1_FID_error"  class=""></div>\n' +
		                               '<input type="hidden" id="address1_FID_hidden" name="address1_FID_hidden" >\n' +
		               '</div>\n' +
		'</div>\n' +
		'<div class="control-group">\n' +
		               '<label for="address2_FID" class="control-label required">Address 2</label>\n' +
		                '<div class="controls">\n' +
		                                '<input type="text"  name="locations[FID].location.address2" id="address2_FID" class="input-xlarge" size="30">\n' +
		                                '<div id="address2_FID_error"  class=""></div>\n' +
		                                '<input type="hidden"  id="address2_FID_hidden" name="address2_FID_hidden">\n'+
		               '</div>\n' +
		'</div>\n' +
		'<div class="control-group">\n' +
		                '<label for="city_FID" class="control-label required">City  <img src=\"' + imgpath + '\" alt="required"></label>\n' +
		                '<div class="controls">\n' +
		                                '<input type="text" name="locations[FID].location.city" id="city_FID"  class="input-medium">\n' +
		                                '<div id="city_FID_error"  class=""></div>\n' +
		                                '<input type="hidden" id="city_FID_hidden" name="city_FID_hidden" >\n' +	
		                '</div>\n' +
		'</div>\n' +
		'<div class="control-group">\n' +
		                '<label for="state_FID" class="control-label required">State <img src=\"' + imgpath + '\" alt="required"></label>\n' +
		                '<div class="controls">\n' +
		                '<select size="1" class="input-medium" name="locations[FID].location.state" id="state_FID" >\n' +
		                 	'<option value="">Select</option>\n' + populateStateList() +
						'</select>\n'+
		                  '<div id="state_FID_error"  class=""></div>\n' +
		                  '<input type="hidden" id="state_FID_hidden" name="state_FID_hidden" >\n'+	
		               '</div>\n' +
		'</div>\n' +
		'<div class="control-group">\n' +
	                '<label for="zip_FID" class="control-label required">Zip Code  <img src=\"' + imgpath + '\" alt="required"></label>\n' +
	                '<div class="controls">\n' +
	                                '<input type="text" name="locations[FID].location.zip" id="zip_FID" maxlength="5" class="input-small zipCode" >\n' +
	                                '<div id="zip_FID_error"  class=""></div>\n' +
	                                '<input type="hidden" id="zip_FID_hidden" name="zip_FID_hidden" >\n' +
	                '</div>\n' +
        '</div>\n'+
	    '<div class="control-group">\n'+
			'<label for="county_FID" class="control-label required">County <img src=\"' + imgpath + '\" alt="required"></label>\n'+
			'<div class="controls">\n'+
				'<select size="1" class="input-large" name="locations[FID].location.county" id="county_FID">\n'+
				'<option value="">Select County...</option>\n'+
				'</select>\n'+
				'<div id="county_FID_error"></div>\n'+		
			'</div>\n'+
		'</div>\n',
     add_worksite: function(empLocationOBJ, defaultStCode){
		var row = this.location_row;
		row = row.replace(/FID/g, this.line_index);
		$('#locationsBox').append(row);
		
		//for accessibility		
		$("#address1_"+this.line_index).focus();
		$("#locationsBox label[for^=address1_]").html("Address 1 <img src='/hix/resources/images/requiredAsterix.png' alt='required'>");
		//end accessibility	
			
		if(empLocationOBJ != ""){
			$("#id_"+this.line_index).val(empLocationOBJ['id']);
			$("#employer_location_id_"+this.line_index).val(empLocationOBJ['employer_location_id']);
			$("#primaryLocation_"+this.line_index).val(empLocationOBJ['primary']);
			$("#address1_"+this.line_index).val(empLocationOBJ['address1']);
			$("#address2_"+this.line_index).val(empLocationOBJ['address2']);
			$("#city_"+this.line_index).val(empLocationOBJ['city']);
			$("#zip_"+this.line_index).val(empLocationOBJ['zip']);
			$("#county_"+this.line_index).val(empLocationOBJ['county']);
			$("#state_"+this.line_index).val(empLocationOBJ['state']);
		}else{
			$("#state_"+this.line_index).val(defaultStCode);
		}
		
		jQuery("#address1_"+this.line_index).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em>Please enter address.</span>" } } );
		jQuery("#city_"+this.line_index).rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em>Please enter City.</span>" } } );
		jQuery("#zip_"+this.line_index).rules("add", { number: true, required: true, zipcheck:true, messages: { required:"<span> <em class='excl'>!</em>Please enter zip.</span>" , number: "<span> <em class='excl'>!</em>Please enter valid zip.</span>", zipcheck : "<span><em class='excl'>!</em>Please enter a valid zip.</span>" } } );
		jQuery("#state_"+this.line_index).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em>Please Select State.</span>" } } );
		jQuery("#county_"+this.line_index).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em>Please Select County.</span>" } } );
		
		$("#zip_"+this.line_index).bind("focusout",function (event) {
			var startindex = (event.target.id).indexOf("_");
			var index = (event.target.id).substring(startindex+1,(event.target.id).length);
			validateAddress(index);});
			this.line_index++;
     },
	
	delete_line: function(index){
		if(confirm('Are you sure you want to delete this record ?')){
			deleteEmployerLocation( $('#employer_location_id_'+index).val(), index);
		}
	}

};
