<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<!-- NOTE: Test page as a placeholder to test document upload. To-be-deleted as soon as UI is ready to test in an integrated way -->
<body>
	<form:form method="POST" action="manualVerificationFileUpload"
		enctype="multipart/form-data">
		<form:errors path="*" cssClass="errorblock" element="div" />
		<input type="text" name="doctype" value="passport" />
Please select a file to upload : <input type="file"
			name="manualVerificationFile" />
		<input type="submit" value="upload" />
		<span><form:errors path="file" cssClass="error" /></span>
	</form:form>

</body>
</html>