(function() {
	"use strict";
	angular.module("communicationPreferencesApp", ['communicationPreferencesCtrlApp', 'communicationPreferencesDirectiveApp', 'addressValidationModule', 'ui.mask','spring-security-csrf-token-interceptor'])
	.value('isEditMode', false);
})();