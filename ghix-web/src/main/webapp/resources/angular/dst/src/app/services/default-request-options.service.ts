import { Injectable } from '@angular/core';
import { BaseRequestOptions, RequestOptions } from '@angular/http';

@Injectable()
export class DefaultRequestOptions extends BaseRequestOptions {

    constructor(
    ) {
        super();
        // Set the default 'Authorization' header

        // Update: You need to subscribe to an observable that will update the JWT. Otherwise, it will run only once.
        // Something like:
        // AuthService.user.subscribe(user => this.headers.set('Authorization', `Bearer ${user.token}`);)
    }
}
