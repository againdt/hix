<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
        String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
%>
<c:set var="stateCode" value="<%=stateCode%>" />
<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1 data-original-title=""><spring:message code="label.sep.heading"/></h1>
		<h1 data-original-title=""><spring:message code="label.lce.mainjsp.alertp3qualifyinglifeevent"/></h1>
	</div>
	<div class="row-fluid">
	<div id="sidebar" class="span3">
				<div class="header">
					<h4>
						<spring:message code="label.sep.help"/>
					</h4>
				</div>
					<ul class="nav nav-list">
	      		<li><a href="/hix/indportal#/contactus"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a></li>
	      	</ul>
			</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4><spring:message code="label.sep.denial.header"/></h4>
			</div>
			<div class="gutter20">
				<div class="alert alert-info margin30-b">
					<spring:message code="label.sep.denial.msg1"/>
					<ul><li><spring:message code="label.sep.denial.reason"/></li></ul>
					<p><spring:message code="label.sep.denial.msg2"/></p>
					
					<c:if test="${stateCode != 'MN'}">
						<p><spring:message code="label.sep.denial.msg3"/></p>
					
						<p><spring:message code="label.sep.denial.msg4"/></p>
						<ul style="list-style-type: disc;">
						<li><spring:message code="label.lce.sep.denial.list1"/></li>
						<li><spring:message code="label.lce.sep.denial.list2"/></li>
						<li><spring:message code="label.lce.sep.denial.list3"/></li>
						</ul>
					</c:if>
				</div>
				<form:form>	<df:csrfToken />
				<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
				</form:form>
			</div>
		</div>

	</div>
</div>
