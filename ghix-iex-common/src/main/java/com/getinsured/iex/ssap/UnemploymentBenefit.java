
package com.getinsured.iex.ssap;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class UnemploymentBenefit {
    @Expose
    private String incomeFrequency;
    @Expose
    private String stateGovernmentName;
    @Expose
    private Double benefitAmount;
    @Expose
    private Date unemploymentDate;

    public String getIncomeFrequency() {
        return incomeFrequency;
    }

    public void setIncomeFrequency(String incomeFrequency) {
        this.incomeFrequency = incomeFrequency;
    }

    public String getStateGovernmentName() {
        return stateGovernmentName;
    }

    public void setStateGovernmentName(String stateGovernmentName) {
        this.stateGovernmentName = stateGovernmentName;
    }

    public Double getBenefitAmount() {
        return benefitAmount;
    }

    public void setBenefitAmount(Double benefitAmount) {
        this.benefitAmount = benefitAmount;
    }

    public Date getUnemploymentDate() {
        return unemploymentDate;
    }

    public void setUnemploymentDate(Date unemploymentDate) {
        this.unemploymentDate = unemploymentDate;
    }

}
