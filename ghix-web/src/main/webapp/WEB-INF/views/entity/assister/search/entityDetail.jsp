<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>

<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
	<style>
		body {background-image:none;}
		.table {border: none;}
		table#entitySearchResult{table-layout: fixed;width: 100%;}
	</style>
 <c:set var="encrytedAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
 <c:set var="encrytedPrimaryAssisterSiteId" ><encryptor:enc value="${assister.primaryAssisterSite.id}" isurl="true"/> </c:set>
 <c:set var="encrytedEnrollmentEntityId" ><encryptor:enc value="${enrollmentEntityDetail.enrollmentEntity.id}" isurl="true"/> </c:set>
 <c:set var="encrytedenrollmentEntityDetailsiteId" ><encryptor:enc value="${enrollmentEntityDetail.site.id}" isurl="true"/> </c:set>



<div class="gutter10">
<div class="row-fluid">
	<h1>
		${enrollmentEntityDetail.enrollmentEntity.entityName}
		<a class="btn btn-primary pull-right" href="/hix/entity/locateassister/entitylist?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}"><spring:message code="label.assister.backbtn"/></a>
	</h1>
</div>
<div class="row-fluid">
	<div class="gutter10">
		<div id="profile-info" class="span6">
			<table id="display-info" class="table table-border-none">
				<tbody>
					<tr>
						<td colspan="3">
							<p>
								<c:set var="entityAddress" value=""/>
								<c:if test="${enrollmentEntityDetail.entityLocation!=null}">
									<c:if test="${not empty enrollmentEntityDetail.entityLocation.address1}">
										<c:set var="entityAddress" value="${enrollmentEntityDetail.entityLocation.address1},"/>
									</c:if>
									<c:if test="${not empty enrollmentEntityDetail.entityLocation.address2}">
										<c:set var="entityAddress" value="${entityAddress} ${enrollmentEntityDetail.entityLocation.address2},"/>
									</c:if>
									<c:if test="${not empty enrollmentEntityDetail.entityLocation.city}">
										<c:set var="entityAddress" value="${entityAddress} ${enrollmentEntityDetail.entityLocation.city},"/>
									</c:if>
									<c:if test="${not empty enrollmentEntityDetail.entityLocation.state}">
										<c:set var="entityAddress" value="${entityAddress} ${enrollmentEntityDetail.entityLocation.state},"/>
									</c:if>
									<c:if test="${not empty enrollmentEntityDetail.entityLocation.zip}">
										<c:set var="entityAddress" value="${entityAddress} ${enrollmentEntityDetail.entityLocation.zip}"/>
									</c:if>
								</c:if>
								<c:choose>
									<c:when test="${not empty entityAddress}">
										<c:out value="${entityAddress}" />
									</c:when>
									<c:otherwise>
										<spring:message code="label.assister.noAddress"/>
									</c:otherwise>
								</c:choose>
								<c:choose>
								<c:when test="${enrollmentEntityDetail.enrollmentEntity.primaryPhoneNumber != null && not empty enrollmentEntityDetail.enrollmentEntity.primaryPhoneNumber}">
									<c:set var="unformattedPhoneNbr" value="${enrollmentEntityDetail.enrollmentEntity.primaryPhoneNumber}"/>
								<c:set var="formattedPhoneNbr" value="${fn:substring(unformattedPhoneNbr, 0, 3)}-${fn:substring(unformattedPhoneNbr, 3, 6)}-${fn:substring(unformattedPhoneNbr, 6, 10)}" />
								<br/><c:out value="${formattedPhoneNbr}" />
								</c:when>
								<c:otherwise>
									<br/><c:out value="${enrollmentEntityDetail.enrollmentEntity.primaryPhoneNumber}" />
								</c:otherwise>
								</c:choose>
								<br />
								${enrollmentEntityDetail.enrollmentEntity.primaryEmailAddress}
							<p>
						</td>
					</tr>
					<tr>
						<td ><spring:message code="label.assister.languagespoken"/></td>
						<td><strong>${enrollmentEntityDetail.siteLanguages.spokenLanguages}</strong></td>
						<td></td>
					</tr>
					<tr>
						<td ><spring:message code="label.assister.languagewritten"/></td>
						<td><strong>${enrollmentEntityDetail.siteLanguages.writtenLanguages}</strong></td>
						<td></td>
					</tr>
					<tr>
						<td><spring:message code="label.assister.hoursofoperation"/></td>
						<td>
							<strong>
								<c:forEach items="${enrollmentEntityDetail.listOfSiteLocationHours}" var="locationhours">
								    <c:choose>
										<c:when test="${locationhours.fromTime == 'Closed'  && locationhours.toTime == 'Closed'}">
										${locationhours.day} ${locationhours.fromTime} <br>
										</c:when>
										<c:otherwise>
										${locationhours.day} ${locationhours.fromTime}-${locationhours.toTime} <br>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</strong>
						</td>
						<td></td>
					</tr>
					<tr>
						<c:choose>
						<c:when test="${noOfAssisters > 0}">
							<td colspan="3">
								<strong>"${enrollmentEntityDetail.enrollmentEntity.entityName}"</strong> <spring:message code="label.assister.has"/>
								<c:choose>
								 	<c:when test="${noOfAssisters>1}">
								    	${noOfAssisters} <spring:message code="label.assister.certifiedenrollcouncelors"/>
								  	</c:when>
								  	<c:otherwise>
								  		${noOfAssisters} <spring:message code="label.assister.certifiedenrollcouncelor"/>
								  	</c:otherwise>
								</c:choose>
								<p><a href="<c:url value="/entity/locateassister/viewassisterlist/${encrytedEnrollmentEntityId}/${encrytedenrollmentEntityDetailsiteId}?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}"/>">
									<spring:message code="label.assister.showcertifiedcounselors"/>
								</a></p>
							</td>
						</c:when>
						<c:otherwise>
							<td colspan="3">
								<spring:message code="label.assister.nocertifiedcounselors"/>
							</td>
						</c:otherwise>
					</c:choose>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="span6">
			<iframe width="300" height="270" frameborder="0" scrolling="no"
				marginheight="0" marginwidth="0" class="thumbnail"
				src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${enrollmentEntityDetail.entityLocation.address1}+${enrollmentEntityDetail.entityLocation.city},+${enrollmentEntityDetail.entityLocation.state}+${enrollmentEntityDetail.entityLocation.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${enrollmentEntityDetail.entityLocation.address1}+${enrollmentEntityDetail.entityLocation.city},+${enrollmentEntityDetail.entityLocation.state}+${enrollmentEntityDetail.entityLocation.zip}&amp;gl=us&amp;t=m&amp;ll=${enrollmentEntityDetail.entityLocation.lat},${enrollmentEntityDetail.entityLocation.lon}&amp;z=13&amp;iwloc=A&amp;output=embed"></iframe>
		</div>
	</div>
</div>

	<div id="assisterlist" style="display: none" class="container-fluid" >
		<form id="entityListfrm" name="entityListfrm" action="searchentities" method="post">
				<input type="hidden" name="currentpage" id="currentpage" value="">
				<c:choose>
					<c:when test="${fn:length(listOfAssistersWithLanguages) > 0}">
						<table id="entitySearchResult" class="table table-condense">
							<thead>
								<tr class="header list-header">
									<th><spring:message code="label.assister.assistername"/></th>
									<th><spring:message code="label.assister.contactinfo"/></th>
									<th><spring:message code="label.assister.languages"/></th>
								</tr>
							</thead>
							<c:forEach var="assisterMap" items="${listOfAssistersWithLanguages}">
								<c:set var="assister" value="${assisterMap.key}"/>
								<c:set var="assisterLanguages" value="${assisterMap.value}"/>
								<c:set var="encrytedAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
 								<c:set var="encrytedPrimaryAssisterSiteId" ><encryptor:enc value="${assister.primaryAssisterSite.id}" isurl="true"/> </c:set>
							    <tr>
									<td class="break-word">
										<a href="<c:url value="/entity/locateassister/assisterdetail/${encrytedAssisterId}/${encrytedPrimaryAssisterSiteId}?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}"/>">${assister.firstName} ${assister.lastName}</a>
									</td>
									<td class="break-word">
										<c:if test="${assister.mailingLocation!=null}">
											<c:if test="${assister.mailingLocation.zip!=null}">
												<c:if test="${assister.mailingLocation.address1 != null && assister.mailingLocation.address1 != \"\"}">
													${assister.mailingLocation.address1}
												</c:if>
												<c:if test="${assister.mailingLocation.address2 != null && assister.mailingLocation.address2 != \"\"}">
													<br>${assister.mailingLocation.address2}
												</c:if>
												  	<br>${assister.mailingLocation.city}&nbsp;&nbsp;&nbsp;${assister.mailingLocation.state}
												<br><c:out value="${assister.mailingLocation.zip}" />
											</c:if>
										</c:if>
										<c:choose>
										<c:when test="${assister.primaryPhoneNumber != null}">
														<c:set var="unformattedPhoneNbr" value="${assister.primaryPhoneNumber}"/>
										<c:set var="formattedPhoneNbr" value="${fn:substring(unformattedPhoneNbr, 0, 3)}-${fn:substring(unformattedPhoneNbr, 3, 6)}-${fn:substring(unformattedPhoneNbr, 6, 10)}" />
										<br/><c:out value="${formattedPhoneNbr}" />
										</c:when>
										<c:otherwise>
														<br/><c:out value="${assister.primaryPhoneNumber}" />
										</c:otherwise>
										</c:choose>
										<br/><c:out value="${assister.emailAddress}" />
									</td>
									<td class="comma">${assisterLanguages.spokenLanguages}</td>
								</tr>
							</c:forEach>
						</table>
						<div class="center">
							<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
						</div>
					</c:when>
					<c:otherwise>
						<b><spring:message code="label.assister.recordsnotfound"/></b>
					</c:otherwise>
				</c:choose>
			</form>
		</div>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->
<script type="text/javascript">
	$(document).ready(function() {
	  	function ie8Trim(){
			if(typeof String.prototype.trim !== 'function') {
	            String.prototype.trim = function() {
	            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	            };
			}
		}
		var isCertified ='${showAssisterList}';
		if(isCertified=="true") {
			$("#assisterlist").show();
		}  else {
			$("#assisterlist").hide();
		}
		$(".comma").text(function(i, val) {
		    return val.replace(/,/g, ", ");
		});
	});
</script>
