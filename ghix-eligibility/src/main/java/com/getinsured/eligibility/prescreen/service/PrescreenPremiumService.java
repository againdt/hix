package com.getinsured.eligibility.prescreen.service;

import java.util.List;

import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.prescreen.PrescreenPremiumData;

/**
 * 
 * @author bhatia_s
 *
 */
public interface PrescreenPremiumService {

	/**
	 * 
	 * @param memberList
	 * @param countyCode
	 * @param zipCode
	 * @param stateName
	 * @return
	 */
	 PrescreenPremiumData calculatePremium(List<Member> memberList, 
			String countyCode, String zipCode, String stateName);
	
}
