package com.getinsured.eligibility.at.ref.service.nonfinancial;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralRenNFConversionService")
@Scope("singleton")
public class ReferralRenNFConversionServiceImpl extends LceProcessHandlerBaseService implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralRenNFConversionServiceImpl.class);

	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;

	@Autowired
	@Qualifier("referralNFCompareService")
	private ReferralNFCompareService referralNFCompareService;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Override
	@ReferralTransactionAnno
	public boolean execute(NFProcessDTO nfProcessRequestDTO) throws Exception {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("ReferralRenNFConversionServiceImpl starts for - " + nfProcessRequestDTO);
		}
		final SsapApplication currentApplication = loadCurrentApplication(nfProcessRequestDTO.getCurrentApplicationId());

		if (currentApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
		}

		final SsapApplication nonFinancialApplication = loadCurrentApplication(nfProcessRequestDTO.getNfApplicationId());

		if (nonFinancialApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getNfApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getNfApplicationId());
		}

		final CompareMainDTO compareMainDTO = referralNFCompareService.executeRenewalCompare(currentApplication, nonFinancialApplication);

		/*final boolean blnEnrollmentActive = checkConfirmedEnrollment(nonFinancialApplication);
		if (blnEnrollmentActive) {
			conversionToConsider(compareMainDTO, currentApplication, nonFinancialApplication);
		} else {
			conversionNotToConsider(compareMainDTO, currentApplication, nonFinancialApplication);
		}*/
		
		conversionToStarted(compareMainDTO, currentApplication, nonFinancialApplication);
		
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("ReferralRenNFConversionServiceImpl ends for - " + nfProcessRequestDTO);
		}
		return true;
	}
	
	private void conversionToStarted(CompareMainDTO compareMainDTO, SsapApplication currentApplication, SsapApplication nonFinancialApplication) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("conversionToConsider starts for - " + currentApplication.getId());
		}
		updateCurrentAppToER(currentApplication);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);
		currentApplication.setAllowEnrollment(ReferralConstants.Y);
		currentApplication.setRenewalStatus(RenewalStatus.STARTED);
		updateSsapApplication(currentApplication);
	}

	/*private void conversionNotToConsider(CompareMainDTO compareMainDTO, SsapApplication currentApplication, SsapApplication nonFinancialApplication) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("conversionNotToConsider starts for - " + currentApplication.getId());
		}
		updateCurrentAppToER(currentApplication);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);
		currentApplication.setAllowEnrollment(ReferralConstants.Y);
		currentApplication.setRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
		updateSsapApplication(currentApplication);
	}

	private void conversionToConsider(CompareMainDTO compareMainDTO, SsapApplication currentApplication, SsapApplication nonFinancialApplication) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("conversionToConsider starts for - " + currentApplication.getId());
		}
		updateCurrentAppToER(currentApplication);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);
		currentApplication.setAllowEnrollment(ReferralConstants.Y);
		currentApplication.setRenewalStatus(RenewalStatus.STARTED);
		updateSsapApplication(currentApplication);
	}

	private boolean checkConfirmedEnrollment(SsapApplication enrolledApplication) {
		boolean isConfirmedEnrollment = false;
		try {
			isConfirmedEnrollment = activeEnrollmentService.isConfirmedEnrollment(enrolledApplication.getId(), ReferralConstants.EXADMIN_USERNAME);
		} catch (GIException e) {
			throw new GIRuntimeException("Error while checking termination status for Application ID " + enrolledApplication.getId());
		}
		return isConfirmedEnrollment;
	}*/

	private void manageCurrentApplicationApplicants(SsapApplication currentApplication, CompareMainDTO compareMainDTO) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("manageCurrentApplicationApplicants  for application - " + currentApplication.getId());
		}
		final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(compareMainDTO.getCurrentApplication().getApplicationData());
		for (CompareApplicantDTO applicantDTO : compareMainDTO.getCurrentApplication().getApplicants()) {
			updateApplicantGuid(ssappln, applicantDTO);
		}
		final String ssapJson = ssapJsonBuilder.transformToJson(ssappln);
		updateSSAPJSON(currentApplication, ssapJson);

		modifyCurrentApplicants(compareMainDTO);
	}

	private void updateSSAPJSON(SsapApplication currentApplication, String ssapJson) {
		currentApplication.setApplicationData(ssapJson);
		updateSsapApplication(currentApplication);
	}

	private void modifyCurrentApplicants(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		for (CompareApplicantDTO currentApplicant : currentList) {
			persistSSAPApplicantGuid(currentApplicant);
		}
	}

	private void persistSSAPApplicantGuid(CompareApplicantDTO applicantDTO) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicantDTO.getId());
		ssapapplicant.setApplicantGuid(applicantDTO.getApplicantGuid());
		ssapApplicantRepository.save(ssapapplicant);
	}

	private void updateApplicantGuid(SingleStreamlinedApplication ssappln, CompareApplicantDTO applicantDTO) {
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == applicantDTO.getPersonId()) {
				houseHoldMember.setApplicantGuid(applicantDTO.getApplicantGuid());
				break;
			}
		}
	}

}
