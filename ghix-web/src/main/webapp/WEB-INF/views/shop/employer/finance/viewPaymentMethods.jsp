<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>


<%@ page isELIgnored="false"%>


<div class="row-fluid">
	<div class="gutter10">
	<ul class="breadcrumb txt-left">
	<li><a href="<c:url value="/shop/employer/" />">Employer Portal </a> <span class="divider">/</span></li>
	<li><a href="<c:url value="/shop/employer/viewpayment" />">Payments </a> <span class="divider">/</span></li>
	<li class="active">Manage Payment Methods</li>
	</ul>
		<div class="">
			<h1>Manage Payment Methods <a href="#" rel="tooltip" data-placement="top"
			data-original-title="You can manage your methods of payment here by editing them, making them active or 
			inactive or setting one of them to be your default. The gear icon on the far right helps perform such 
			operations." class="info"><i class="icon-question-sign customMargin"></i></a></h1>

		</div>
	</div>
</div>

<div class="row-fluid">

	<div class="span3" id="sidebar">
      	<div class="header">
			<h4 class="margin0">Refine Results</h4>
		</div>
				<div class="gray graybg">
					<form method="post"  class="form-vertical gutter10">
						<df:csrfToken/>

						<div class="control-group">
							<label class="control-label" for="paymentMethodName">Name</label>
							<div class="controls">
								<input class="span11" placeholder="Payment Method Name" name="paymentMethodName" id="paymentMethodName" value="${searchCriteria.paymentMethodName}">
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="updatedDate">Date</label>
							<div class="input-append date date-picker" id="date">
								<input class="input-medium" type="text" name="updatedDate" id="updatedDate" value="${searchCriteria.updatedDate}" pattern="mm-dd-yyyy" onChange="setdate();" /> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<!-- <input title="MM/dd/yyyy" class="datepick span10" 
							name="updatedDate" id="updatedDate" value="" onChange="setdate();"/> -->
						</div>
						<div class="control-group">
							<%--<label class="control-label" for="inputPassword">Type</label> --%>
							<div class="controls">

								<%--<label class="control-label"> <select
									class="input-medium" name="paymentTypes" id="paymentTypes">

										<option value="" <c:if test="${searchCriteria.paymentType == ''}"> selected="selected" </c:if>>Any</option>
										<option value="BANK" <c:if test="${searchCriteria.paymentType == 'BANK'}"> selected="selected" </c:if>>BANK</option>
										 <option value="CREDITCARD" <c:if test="${searchCriteria.paymentType == 'CREDITCARD'}"> selected="selected" </c:if>>CREDITCARD</option>
										<option value="MANUAL" <c:if test="${searchCriteria.paymentType == 'MANUAL'}"> selected="selected" </c:if>>MANUAL</option>
								</select>
								</label> --%>
								<div class="txt-center">
									<button type="submit" class="btn">Go</button>
								</div>
							</div>
						</div>
					</form>

				</div>
				<div class="gutter10">
				<a class="btn input-medium"
							href="<c:url value="/shop/employer/addpayment"/>"><i class="icon-plus-sign"></i>  Add New</a>
					<!-- <button class="btn input-medium" type="button">
						<i class="icon-plus-sign"></i> Add New
					</button>-->
				</div>



	</div>





	<div class="span9" id="rightpanel">


		<form class="form-horizontal" id="frmfinancialinfolist"
			name="frmfinancialinfolist" action="paymentmethods" method="POST">
			<df:csrfToken/>
			<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
			<c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(paymentTypelist) > 0}">
							
								<table class="table table-striped">
								
									<thead>
										<tr class="graydrkbg ">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<th class="sortable" scope="col"><dl:sort
													title="Payment Method Name" sortBy="paymentMethodName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Type" sortBy="paymentType"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Date Modified"
													sortBy="updatedOn"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Status" sortBy="status"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th>Default</th>
											<th>&nbsp;</th>
										</tr>
									</thead>

									<c:forEach items="${paymentTypelist}" var="paymentMethods" varStatus="vs">
											
											<input type="hidden" value="<encryptor:enc value = "${paymentMethods.id}"/>" name="paymentid" id="paymentid_${vs.index}" /> 
												
											<input type="hidden" value="${paymentMethods.isDefault}" name="default" id="default_${vs.index}" />
											<input type="hidden" value="" name="existingpaymentid" id="existingpaymentid" />
										<tr>
											<!-- first row of table -->
											
												<!-- <td><input type="checkbox"> </td> -->
											<td><%--<c:choose>
													<c:when
														test='${not empty paymentMethods.financialInfo.bankInfo}'>
														<span>${paymentMethods.financialInfo.bankInfo.bankName}
															Manual (check) 
															
														</span>
													</c:when>
													<c:when
														test='${not empty paymentMethods.financialInfo.creditCardInfo}'>
														<span>${paymentMethods.financialInfo.creditCardInfo.nameOnCard}</span>
													</c:when>
													<c:otherwise>
														<span></span>
													</c:otherwise>
												</c:choose> --%>
												<c:choose>
													<c:when
														test='${not empty paymentMethods.paymentMethodName}'>
														<span>${paymentMethods.paymentMethodName}</span>
													</c:when>
													<c:otherwise>
														<span></span>
													</c:otherwise>
												</c:choose>
												</td>

											<td>
											${paymentMethods.paymentType}</td>
											<td>
											<fmt:formatDate
													value="${paymentMethods.updatedOn}" pattern="MM/dd/yyyy" /></td>
													
											<td id="status_${vs.index}">${paymentMethods.status}</td>
											<c:if test="${'Y' == paymentMethods.isDefault}">
												<input type="hidden" value="<encryptor:enc value = "${paymentMethods.id}"/>"
													name="defaultpaymentid_${vs.index}" id="defaultpaymentid" />
											</c:if>
											<td class="txt-center" id="defaulticon_${vs.index}"><!-- this icon will indicate whether or not the account is default -->
											          <c:choose>
											               <c:when
														test="${'Y' == paymentMethods.isDefault}">
											                
															   <i class="icon-ok-sign"></i>
															</c:when>
															<c:otherwise>
														<span></span>
													</c:otherwise>
															</c:choose>
															</td>
                                             <td><div class="" style="width:50px">
													<div class="dropdown">
														<a href="/page.html" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"><i
															class="icon-cog"></i><b class="caret"></b>
														</a>
														<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right"><!-- class removed "action-align-l" -->
															
															<li><a href="#" onClick="updatePaymentMethod(this,'edit');" id="edit_${vs.index}"><i class="icon-pencil"></i> Edit</a></li>
															<c:if test="${'InActive' == paymentMethods.status}">
															<li><a href="#"
																	onClick="updatePaymentMethod(this,'Active');" id="changestatus_${vs.index}"><i class="icon-ok"></i> Active</a></li>
															</c:if>
															<c:if test="${'Active' == paymentMethods.status}">
																<li><a href="#" onClick="updatePaymentMethod(this,'InActive');" id="changestatus_${vs.index}"><i class="icon-remove"></i> InActive</a></li>
															</c:if>
															<c:if test="${'N' == paymentMethods.isDefault && 'Active' == paymentMethods.status && 'MANUAL' != paymentMethods.paymentType}">
																<li><a href="#" onClick="updatePaymentMethod(this,'makedefault');" id="lidefault_${vs.index}"><i class="icon-ok-sign"></i> Make Default</a></li>
															</c:if>
															<!-- <li><a class=""
																href="<c:url value="/assister/decline/${assister.employerId}?prevStatus=${empStatus}"/>"
																onClick="return validateDecline()">Mark as Inactive</a>
															</li> -->
														</ul>
													</div>
												</div></td>
												
										</tr>
								
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</div>



<div class="row-fluid">
	<!-- <div class="pagination">
		<ul>
			<li><a href="#">Prev</a></li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">Next</a></li>
		</ul>
	</div>-->



	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<div id="dialog-modal_error" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>
<!-- row-fluid -->

<script type="text/javascript">
$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});

function setdate(){
	$("#updatedDate").val($("#updatedDate").val());
}

	$('.info').tooltip();

if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
{
	$( "#dialog-modal_error" ).dialog({
	      height: 200,
	      width: 250,
	      modal: true,
	      title: "Error: Add/Edit Payment Methods",
	      buttons: {
				Ok: function() {
				$( this ).dialog( "close" );
				}
				}
	    });			
}

	function updatePaymentMethod(e,test) {
		
		if (test == 'makedefault') {
			var didConfirm = confirm("This will make current payment method as default and remove default setting of other payment method. Is it ok?");
			if (didConfirm == true) {

			} else {
				return false;
			}
		}
		if (test != 'edit') {
			var validateUrl = '';
			var parameter = '';
			
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);
			
			var selectedstatus = '';
			if (test == 'makedefault') {
				validateUrl = '<c:url value="/shop/employer/makePaymentDefault"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> </c:param> </c:url>';
				
				parameter = {
					id : $("#paymentid" + index).val(),
					makedefault : test,
					employerid : $("#id").val()
				};
			} else {
				validateUrl = '<c:url value="/shop/employer/changePaymentStatus"> <c:param name="${df:csrfTokenParameter()}"> 	<df:csrfToken plainToken="true" /> 	</c:param> 	</c:url>';
				selectedstatus = test;
				parameter = {
					id : $("#paymentid" + index).val(),
					status : selectedstatus
				};
				if(selectedstatus=='InActive' && ('Y'==($('#default' + index).val()))){
					alert("Default Payment cannot be made inactive, kindly make other payment as default, before making this payment as inactive and then deactivate this payment method");
					return false;
				}
			}

			$
					.ajax({
						url : validateUrl,
						type : "POST",
						data : parameter,
						success : function(response , xhr) {
							if(isInvalidCSRFToken(xhr))	    				
								return;
							if (response != null || response != 'null') {
								if (selectedstatus != '') {
									
									
									$('#status' + index).text(response);
									
									$('#changestatus' + index).remove();
									
									if (response == 'InActive') {
									  $('#action'+ index ).append(
										    $('<li>').append(
										        $('<a>').attr({'href': '#',
										           'onClick': 'updatePaymentMethod(this,\'Active\')',
										           'id': 'changestatus'+index}).append(
										                   $('<i>').attr('class', 'icon-ok')).append('Active')
										                   
							          )); 
									  
									  if ('N'==($('#default' + index).val())){
										  $('#lidefault' + index).remove();
									  }
									}
									 else if (response == 'Active') {
										 $('#action'+ index ).append(
										 $('<li>').append(
											        $('<a>').attr({'href': '#',
											           'onClick': 'updatePaymentMethod(this,\'InActive\')',
											           'id': 'changestatus'+index}).append(
											                   $('<i>').attr('class', 'icon-remove')).append('Inactive')
								         )); 
										 
										 if ('N'==($('#default' + index).val())){
											 $('#action'+ index ).append(
													 $('<li>').append(
														        $('<a>').attr({'href': '#',
														           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
														           'id': 'lidefault'+index}).append(
														                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
											         )); 
										 }
									}
									
								} else {
									if (response == 'Y') {
										$('#status' + index).text(
												$('#status' + index).text());
										$('#defaulticon' + index).text('');
										$('#defaulticon' + index).append($('<i>').attr('class', 'icon-ok-sign'));
										$('#default' + index).val('Y');
										$('#lidefault' + index).remove();
										
										if ($('#defaultpaymentid').length)
										{
												$("#defaultpaymentid").val(
														$("#paymentid" + index).val());
												
												var previousdefaultname = $(
														"#defaultpaymentid").attr(
														'name');
												var newstartindex = previousdefaultname
														.indexOf("_");
												var newindex = (previousdefaultname)
														.substring(
																newstartindex,
																(previousdefaultname).length);
												$('#default' + newindex).val('N');
												$('#defaulticon' + newindex).text('');
												if('Active' == $('#status'+newindex).text()){
												$('#action'+ newindex ).append(
														 $('<li>').append(
															        $('<a>').attr({'href': '#',
															           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
															           'id': 'lidefault'+newindex}).append(
															                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
												         )); 
												}
												$("#defaultpaymentid").attr('name','defaultpaymentid' + index);
										}
										else{
											$('<input>').attr({
											    type: 'hidden',
											    id: 'defaultpaymentid',
											    name: 'defaultpaymentid'+index,
											    value:$("#paymentid" + index).val()
											}).appendTo('form');
										}
										
									}else if(response.indexOf("Error") >=0){
										alert("Error :: Please provide the correct information related to user or Please contact customer support team.");
									}
								}
							}
						},
					});
		} else {
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);
			$("#existingpaymentid").val($("#paymentid" + index).val());
			$("#frmfinancialinfolist").attr("action", "editpayment");
			$("#frmfinancialinfolist").submit();
		}
	}
	
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
			rv = true;
		}
		return rv;
	}
</script>
