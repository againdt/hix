package com.getinsured.eligibility.at.sep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Financial;


@RepositoryRestResource(path="SepEvents")
public interface SepEventsRepository extends JpaRepository<SepEvents, Integer> {

	List<SepEvents> findByFinancialOrderByLabelAsc(Financial financial);
	
	@RestResource(path="findByFinancial")
	List<SepEvents> findByFinancial(@Param("financial") SepEvents.Financial financial);
	
	@RestResource(path="findByEventNameCategoryAndFinancial")
	@Query("select s from SepEvents s where s.name = :name and s.category = :category and s.financial = :financial)")
	SepEvents findByEventNameAndFinancial(@Param("name") String name, @Param("category") String category, @Param("financial") SepEvents.Financial financial);
	
	@RestResource(path="findByEventNameCategoryAndNonFinancial")
	@Query("select s from SepEvents s where s.name = :name and s.category = :category and s.financial = :financial)")
	SepEvents findByEventNameAndNonFinancial(@Param("name") String name, @Param("category") String category, @Param("financial") SepEvents.Financial financial);

	@RestResource(path="findByEventChangeTypeAndFinancial")
	@Query("select s from SepEvents s where s.changeType = :changeType and s.financial = :financial and s.displayOnUI = :displayOnUI)")
	List<SepEvents> findByEventChangeTypeAndFinancial(@Param("changeType") String changeType, @Param("financial") SepEvents.Financial financial, @Param("displayOnUI") String displayOnUI);
	
}
