package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.math.BigDecimal;

public class CoveredInsuredMembers {
	 private String personTrackingNumber;
	 /*private String issuerInsuredMemberIdentifier;
	 private String definingMemberMaritalStatusTypeCodeName;
	 private String definingInsurancePolicyStatusTypeCodeName;
	 private String definingMemberAssociationToSubscriberTypeCodeName;
	 private boolean subscriberIndicator;
	 private String insuredMemberIdentifier;
	 private BigDecimal memberMonthlyPolicyPremiumAmount;
	 private BigDecimal memberEhbPremiumAmount;
	 private BigDecimal memberAllocatedAPTCAmount;
	 private BigDecimal memberIndividualResponsibleAmount;
	 private String memberExchangeRateAreaReference;*/
	 private String identifyingTobaccoUseTypeCodeName;
	 private MemberInfo memberInfo;


	 // Getter Methods 

	 public String getPersonTrackingNumber() {
	  return personTrackingNumber;
	 }
	 public String getIdentifyingTobaccoUseTypeCodeName() {
		  return identifyingTobaccoUseTypeCodeName;
		 }

	/* public String getIssuerInsuredMemberIdentifier() {
	  return issuerInsuredMemberIdentifier;
	 }

	 public String getDefiningMemberMaritalStatusTypeCodeName() {
	  return definingMemberMaritalStatusTypeCodeName;
	 }

	 public String getDefiningInsurancePolicyStatusTypeCodeName() {
	  return definingInsurancePolicyStatusTypeCodeName;
	 }

	 public String getDefiningMemberAssociationToSubscriberTypeCodeName() {
	  return definingMemberAssociationToSubscriberTypeCodeName;
	 }

	 public boolean getSubscriberIndicator() {
	  return subscriberIndicator;
	 }

	 public String getInsuredMemberIdentifier() {
	  return insuredMemberIdentifier;
	 }

	 

	 public BigDecimal getMemberMonthlyPolicyPremiumAmount() {
	  return memberMonthlyPolicyPremiumAmount;
	 }

	 public BigDecimal getMemberEhbPremiumAmount() {
	  return memberEhbPremiumAmount;
	 }

	 public BigDecimal getMemberAllocatedAPTCAmount() {
	  return memberAllocatedAPTCAmount;
	 }

	 public BigDecimal getMemberIndividualResponsibleAmount() {
	  return memberIndividualResponsibleAmount;
	 }

	 public String getMemberExchangeRateAreaReference() {
	  return memberExchangeRateAreaReference;
	 }*/

	 public MemberInfo getMemberInfo() {
	  return memberInfo;
	 }

	 // Setter Methods 

	 public void setPersonTrackingNumber(String personTrackingNumber) {
	  this.personTrackingNumber = personTrackingNumber;
	 }

	/* public void setIssuerInsuredMemberIdentifier(String issuerInsuredMemberIdentifier) {
	  this.issuerInsuredMemberIdentifier = issuerInsuredMemberIdentifier;
	 }

	 public void setDefiningMemberMaritalStatusTypeCodeName(String definingMemberMaritalStatusTypeCodeName) {
	  this.definingMemberMaritalStatusTypeCodeName = definingMemberMaritalStatusTypeCodeName;
	 }

	 public void setDefiningInsurancePolicyStatusTypeCodeName(String definingInsurancePolicyStatusTypeCodeName) {
	  this.definingInsurancePolicyStatusTypeCodeName = definingInsurancePolicyStatusTypeCodeName;
	 }

	 public void setDefiningMemberAssociationToSubscriberTypeCodeName(String definingMemberAssociationToSubscriberTypeCodeName) {
	  this.definingMemberAssociationToSubscriberTypeCodeName = definingMemberAssociationToSubscriberTypeCodeName;
	 }

	 public void setSubscriberIndicator(boolean subscriberIndicator) {
	  this.subscriberIndicator = subscriberIndicator;
	 }

	 public void setInsuredMemberIdentifier(String insuredMemberIdentifier) {
	  this.insuredMemberIdentifier = insuredMemberIdentifier;
	 }

	 public void setMemberMonthlyPolicyPremiumAmount(BigDecimal memberMonthlyPolicyPremiumAmount) {
	  this.memberMonthlyPolicyPremiumAmount = memberMonthlyPolicyPremiumAmount;
	 }

	 public void setMemberEhbPremiumAmount(BigDecimal memberEhbPremiumAmount) {
	  this.memberEhbPremiumAmount = memberEhbPremiumAmount;
	 }

	 public void setMemberAllocatedAPTCAmount(BigDecimal memberAllocatedAPTCAmount) {
	  this.memberAllocatedAPTCAmount = memberAllocatedAPTCAmount;
	 }

	 public void setMemberIndividualResponsibleAmount(BigDecimal memberIndividualResponsibleAmount) {
	  this.memberIndividualResponsibleAmount = memberIndividualResponsibleAmount;
	 }

	 public void setMemberExchangeRateAreaReference(String memberExchangeRateAreaReference) {
	  this.memberExchangeRateAreaReference = memberExchangeRateAreaReference;
	 }*/

	 public void setIdentifyingTobaccoUseTypeCodeName(String identifyingTobaccoUseTypeCodeName) {
	  this.identifyingTobaccoUseTypeCodeName = identifyingTobaccoUseTypeCodeName;
	 }
	 
	 public void setMemberInfo(MemberInfo memberInfo) {
	  this.memberInfo = memberInfo;
	 }
	}
