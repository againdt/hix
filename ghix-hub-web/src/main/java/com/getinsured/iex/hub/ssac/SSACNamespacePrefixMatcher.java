package com.getinsured.iex.hub.ssac;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class SSACNamespacePrefixMatcher extends NamespacePrefixMapper{
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/niem-core/2.0")){
			return "nc";
		}else if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/appinfo/2.0")){
			return "i";
		}else if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/structures/2.0")){
			return "s";
		}
		else if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/usps_states/2.0")){
			return "s";
		}
		
		
		else if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/niem-core/2.0")){
			return "nc";
		}
		
		else if(namespaceUri.equalsIgnoreCase("http://niem.gov/niem/proxy/xsd/2.0")){
			return "niem-xsd";
		}
		
		else if(namespaceUri.equalsIgnoreCase("http://extn.ssac.ee.sim.dsh.cms.hhs.gov")){
			return "ssac";
		}
		
		else if(namespaceUri.equalsIgnoreCase("http://codes.ssac.ee.sim.dsh.cms.hhs.gov")){
			return "ssac-codes";
		}
		
		else if(namespaceUri.equalsIgnoreCase("http://ssac.ee.sim.dsh.cms.hhs.gov")){
			return "exch";
		}
		
		else{
			return suggestion;
		}
	}
}
