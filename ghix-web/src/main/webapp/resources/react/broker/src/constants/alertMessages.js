import React from 'react';
import { FormattedMessage } from 'react-intl';

export const errorRetrieveTitle = (
  <FormattedMessage
    id="broker.error.retrieve.title"
    defaultMessage="Error retrieving information"
  />
);

export const errorRetrieveBody = (
  <FormattedMessage
    id="broker.error.retrieve.body"
    defaultMessage="There was an error retrieving your information. Please try again later."
  />
);

export const errorSubmitTitle = (
  <FormattedMessage
    id="broker.error.submit.title"
    defaultMessage="Error submitting information"
  />
);

export const errorSubmitBody = (
  <FormattedMessage
    id="broker.error.submit.body"
    defaultMessage="There was an error submitting your information. Please try again later."
  />
);

export const errorSubmitAvailabilityTitle = (
  <FormattedMessage
    id="broker.error.submit.availability.title"
    defaultMessage="Error changing availability"
  />
);

export const errorSubmitAvailabilityBody = (
  <FormattedMessage
    id="broker.error.submit.availability.body"
    defaultMessage="There was an error changing your availability status. Please try again later."
  />
);

export const errorNotEnrolledTitle = (
  <FormattedMessage
    id="broker.error.enroll.availability.title"
    defaultMessage="You are currently not enrolled in the broker connect program"
  />
);

export const errorNotEnrolledLink = (
  <FormattedMessage
    id="broker.error.enroll.availability.link"
    defaultMessage="Participation Information"
  />
);

export const errorNotEnrolledBodyBefore = (
  <FormattedMessage
    id="broker.error.enroll.availability.body.before"
    defaultMessage={`Please navigate to `}
  />
);

export const errorNotEnrolledBodyAfter = (
  <FormattedMessage
    id="broker.error.enroll.availability.body.after"
    defaultMessage={` to sign up.`}
  />
);

export const alertRemovalTitle = (
  <FormattedMessage
    id="broker.alert.participation.removal.title"
    defaultMessage="Successfully unenrolled"
  />
);

export const alertRemovalBody = (
  <FormattedMessage
    id="broker.alert.participation.removal.body"
    defaultMessage="Your participation to the broker connect program has been removed with immediate effect. You have the option to join the program again at any time by signing up for the program again."
  />
);

export const alertUpdateHoursTitle = (
  <FormattedMessage
    id="broker.alert.hours.update.title"
    defaultMessage="Availability Hours Updated"
  />
);

export const alertUpdateHoursBody = (
  <FormattedMessage
    id="broker.alert.hours.update.body"
    defaultMessage="Your availability hours have been successfully updated."
  />
);

export const alertUpdatePhoneTitle = (
  <FormattedMessage
    id="broker.alert.phone.update.title"
    defaultMessage="Phone Number Updated"
  />
);

export const alertUpdatePhoneBody = (
  <FormattedMessage
    id="broker.alert.phone.update.body"
    defaultMessage="Your broker connect phone number has been successfully updated."
  />
);

export const alertSuccessEnrolledTitle = (
  <FormattedMessage
    id="broker.alert.participation.enroll.title"
    defaultMessage="Successfully enrolled"
  />
);

export const alertSuccessEnrolledBody = (
  <FormattedMessage
    id="broker.alert.participation.enroll.body"
    defaultMessage="Your participation in the Broker Connect program is confirmed. Please note that you will not start getting calls unless you select the hours during which you are available to take calls below."
  />
);
