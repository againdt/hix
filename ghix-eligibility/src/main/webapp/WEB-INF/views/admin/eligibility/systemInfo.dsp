<%@page import="com.getinsured.hix.platform.startup.*"%>
<%@include file="datasource.jsp"%>
<style>
table{
	border: 1px solid #666;
	width: 80%;	
	margin: 20px 0 20px 0 !important;	
	font-size: 14px;
}
td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

tr:nth-child(odd) {
	background-color: #fff
}

tr:nth-child(even) {
	background-color: #fea
}
</style>
<%!private SystemInfo systemInfo = null;
	private DbPing dbPing = null;%>
<%
	systemInfo = (SystemInfo) ctx.getBean("appStartupBean");
	dbPing = (DbPing) ctx.getBean("dbPingBean");
%>

<%=systemInfo.getSystemInfo("html").replaceAll("<br>", "")%>
<br>
<h4>DB Details</h4>
<%=dbPing.dbPing("html")%>
