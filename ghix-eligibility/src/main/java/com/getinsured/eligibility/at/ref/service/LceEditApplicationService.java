package com.getinsured.eligibility.at.ref.service;

import org.springframework.stereotype.Component;

import com.getinsured.iex.ssap.model.SsapApplication;

@Component("lceEditApplicationService")
public interface LceEditApplicationService {
	
	boolean closeApplications(SsapApplication ssapApplication);

	void editApplicationProcess(Long applicationId) throws Exception;

}
