package com.getinsured.iex.dto;

import java.sql.Timestamp;
import org.springframework.hateoas.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapVerificationResource extends ResourceSupport {

	private Timestamp creationTimestamp;
	private Timestamp effectiveEndDate;
	private Timestamp effectiveStartDate;
	private Timestamp lastUpdateTimestamp;
	private String source;
	private String verficationResults;
	private String verificationStatus;
	private String verificationType;
	@JsonInclude(Include.NON_NULL)
	private String ssapApplicant;
	public Object _links;
	
	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}
	
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Timestamp getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(Timestamp effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public Timestamp getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Timestamp effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getVerficationResults() {
		return verficationResults;
	}
	public void setVerficationResults(String verficationResults) {
		this.verficationResults = verficationResults;
	}
	public String getVerificationStatus() {
		return verificationStatus;
	}
	public void setVerificationStatus(String verificationStatus) {
		this.verificationStatus = verificationStatus;
	}
	public String getVerificationType() {
		return verificationType;
	}
	public void setVerificationType(String verificationType) {
		this.verificationType = verificationType;
	}
	public String getSsapApplicant() {
		return ssapApplicant;
	}
	public void setSsapApplicant(String ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}
}
