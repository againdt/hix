import { errorActionTypes } from '../constants/actionTypes';

export const initialState = {
    errorStatus: false,
    errorMessage: null,
    errorType: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case errorActionTypes.SET_ERROR:

        if(
            Array.isArray(action.errorData) ||
            !(action.errorData instanceof Object) ||
            action.errorData === null ||
            typeof action.errorData === 'function'
        ) {
            throw new Error('Invalid value (not an object) has been passed in errorReducer');
        }

        return {
            errorStatus: true,
            ...action.errorData
        };
    case errorActionTypes.REMOVE_ERROR:
        return initialState;
    case errorActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};
