package com.getinsured.hix.platform.sms.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.platform.sms.SmsResponse;

public class SmsServiceTest extends BaseTest {

	private @Autowired SmsService smsService;

	@Test
	public void testIndiaNumber() {
		SmsResponse status = smsService.send("4084621703", "Test SMS The PIN is <pin>","1223232");
		assertNotNull(status);
		assertTrue("FAILURE".equals(status.getStatus()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyToPhone1(){
		smsService.send(null, "test The PIN is <pin>","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyToPhone2(){
		smsService.send("", "test The PIN is <pin>","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBody1(){
		smsService.send("4084621703", null,"1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBody2(){
		smsService.send("4084621703", "","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidBody2(){
		smsService.send("4084621703", "how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?12345","1223232");

	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat1(){
		SmsResponse status = smsService.send("+16506184604", "This is a vimo junit test message with phone format +16506184604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat2(){
		SmsResponse status = smsService.send("6506184604", "This is a vimo junit test message with phone format 6506184604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat3(){
		SmsResponse status = smsService.send("(650) 618-4604", "This is a vimo junit test message with phone format (650) 618-4604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat4(){
		SmsResponse status = smsService.send("(650)618-4604", "This is a vimo junit test message with phone format (650)618-4604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	public void testValidData() {
		SmsResponse status = smsService.send("+1 (650) 618-4604", "This is a vimo junit test message with phone format +1 (650) 618-4604.<pin>","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

}
