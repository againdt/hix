<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<!DOCTYPE html>
<html>

<head>
<%       String trackingCode= "UA-XXXXX-X";
if(GhixConstants.GOOGLE_ANALYTICS_CODE!= null)
	   trackingCode = GhixConstants.GOOGLE_ANALYTICS_CODE;
request.setAttribute("trackingCode",trackingCode); %>
	 	<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m);
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', '${trackingCode}', '<%=GhixConfiguration.EXCHANGE_URL%>');
	ga('send', 'pageview');
</script>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/> 
<!-- IE8 fixes -->
<!--[if lt IE 9]>
<script type="text/javascript"
	src="<c:url value="/resources/js/custom-phix_ie8.js"/>"></script>
<![endif]-->

<!--  typekit scripts -->
<script type="text/javascript" src="//use.typekit.net/xer8gub.js"></script>
	<script type="text/javascript">
	try {
		Typekit.load();
	} catch (e) {}
	</script>
<link rel="icon" href="<c:url value="/resources/images/favicon.ico"/>" type="image/ico" >	
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.min-phix.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/datepicker-phix.css"/>">
	<!-- end -->
<!--  <link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.min-phix.css"/>"> -->
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/jquery.vegas.css"/>">

	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/nanoscroller-phix.css"/>">


	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/custom4.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/custom_overide.css"/>">
	
	<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/phone.css"/>">

	
<!-- jquery 1.10.2 -->
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.10.2.min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.placeholder.js"/>"></script>	
<script type="text/javascript"
	src="<c:url value="/resources/js/custom-phix-prelaunch.js"/>"></script>
	
	
	<style type="text/css">
	#stateMessage h4 small {
	display:block

	}
	.leftcol, .rightcol {
	text-align:center
	}
	
	.leftcol #stateMessage h4 span {
		color:#222;
	}
	
	.leftcol .alert {
	text-align:center;
	list-style-type:disc;	
	padding-left:20px;
	background: none repeat scroll 0 0 #EDF2F5;
    border: 1px solid #D9EDF7;
    border-radius: 0 4px 4px 0;
    color:#666;
    
    
	}
	
	.leftcol .alert p {
	text-align:center;
	border-bottom:1px solid #fff;
	padding-bottom:10px;
	}
	
	.leftcol .alert  ul {
	border:none;
	padding-right:10px;
	font-weight:bold;
		text-align:left;
	}
	
		.rightcol {
		padding-left:20px;
		}
	
	.rightcol p.span12 {
	margin:0 10px 20px 30px;
	} 
	
	.leftcol ul.alert li {
	padding:10px;
	}
	
	section div.panel.span12 {
    padding-left: 5%;
    width: 100%;
}

section div.panel h4 {
margin:15px 0;
}
	
	</style>
</head>

    <body>
    <div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<c:if test="${sessionScope.FlowPhoto eq 'found'}">
         			<spring:url value="/affiliate/logo/${sessionScope.ClickTrackAffiliateId}/${sessionScope.ClickTrackAffiliateFlowId}"  var="logoUrl"/>
			    	<a class="pull-left brand">
			    	<img src="${logoUrl}" alt="Flow logo" />
			    	<span>Affiliate</span></a>
			</c:if> 
			<c:if test="${sessionScope.FlowPhoto ne 'found'}">
					<a class="pull-left brand"
					href="<c:url value="/"/>"><img
					src="<c:url value="/resources/images/logo-getinsured.png"/>"
					alt="GetInsured.com"><span>GetInsured.com</span></a>
			</c:if> 


			<a class="btn btn-navbar" data-toggle="collapse"
			data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>


		<div class="nav-collapse collapse">
			<ul class="nav pull-right" id="topnav">
				<li class="dropdown">
                            <a href="#" data-toggle="dropdown">GET ASSISTANCE&nbsp;<i class="icon-caret-down"></i></a>
                            <ul class="dropdown-menu getassistance-dd">
                            <c:if test="${sessionScope.ClickTrackIVRNumber==null}">
		                 <li><a href="#">(866) 602 8466</a></li>		
		                </c:if>
		                
		                <c:if test="${sessionScope.ClickTrackIVRNumber!=null}">
		                  <li><a href="#">(<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,0,3)}"/>)&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,3,6)}"/>&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,6,10)}"/></a></li>	
		                </c:if>
<!--                           <li><a href="#">1-855-996-6448</a></li> -->
                          <li><a href="/answers/" target="_blank">Answers</a></li>
                          </ul>
                        </li>
				<li><a href="/answers/" target="_blank">ANSWERS</a>
				</li>
<%-- 				<li><a href="<c:url value="/account/signup/individual"/>">SIGN UP</a> --%>
<!-- 				</li> -->
<%-- 				<li><a href="<c:url value="/account/user/login"/>">LOG IN</a> --%>
<!-- 				</li> -->
			</ul>
		</div>
	</div>
</div>

<!-- <div id="floatpanel" class="stuck"> -->
<!-- <div id="floatpanel-inner"> -->
<!--                                     <p><strong>Your Profile</strong></p> -->
                                    
                  
<!--                                     <ul class="proflist"> -->
<%--                                                       <li><span>Zip Code:&nbsp;</span><c:out value="${phixRequest.zipCode}"/></li> --%>
<%--                                                       <li><span>County:&nbsp;</span><c:out value="${countyName}"/></li> --%>
<!--                                     <li> -->
<!--                                         <span>Seeking coverage:&nbsp;</span> -->
<%--                                         <c:out value="${peopleNeedingCoverage}"/> --%>
<!--                                   </li> -->
<!--                                     <li><span>Income:&nbsp;</span> -->
<%--                                     	<c:if test="${phixRequest.householdIncome == -1}"> --%>
<!--                                     		No response entered. -->
<%--                                     	</c:if> --%>
<%--                                     	<c:if test="${phixRequest.householdIncome > 0}"> --%>
<%--                                     		<fmt:formatNumber var="income"  value="${phixRequest.householdIncome}"  maxFractionDigits="0" groupingUsed=""/> --%>
<%--                                     		$<c:out value="${income}"/> --%>
<%--                                     	</c:if>		 --%>
<!--                                     </li> -->
<!--                                     <li> -->
<!--                                         <span>Financial Help:&nbsp;</span> -->
                                 
<%--                                         	<c:if test="${phixRequest.aptc != null && phixRequest.aptc != '' && phixRequest.aptc != 'N/A' && phixRequest.aptc != '$0.0' && phixRequest.aptc != '0'}"> --%>
<%--                                         		<c:out value="${phixRequest.aptc}"/>/month --%>
<%--                                         	</c:if>	 --%>
<%--                                         	<c:if test="${(phixRequest.csrLevel != null && phixRequest.csrLevel != '' && phixRequest.csrLevel != 'N/A') && (phixRequest.aptc != null && phixRequest.aptc != '' && phixRequest.aptc != 'N/A' && phixRequest.aptc != '$0.0' && phixRequest.aptc != '0')}"> + </c:if> --%>
<%--                                         	<c:if test="${phixRequest.csrLevel != null && phixRequest.csrLevel != '' && phixRequest.csrLevel != 'N/A'}"> --%>
<!--                                         		Plan Upgrade -->
<%--                                         	</c:if>	 --%>
<%--                                         	<c:if test="${(phixRequest.csrLevel == null || phixRequest.csrLevel == '' || phixRequest.csrLevel == 'N/A') && (phixRequest.aptc == null || phixRequest.aptc == '' || phixRequest.aptc == 'N/A' || phixRequest.aptc == '$0.0' || phixRequest.aptc == '0')}"> --%>
<!--                                         		None -->
<%--                                         	</c:if> --%>
<!--                                    </li> -->
                                    
<!--                                     <li> -->
<!--                                         <span>Medical Usage:&nbsp;</span> -->
<%--                                         <c:out value="${usage}"/> --%>
<!--                                         </li> -->
                              	
<!--                                     <li> -->
<!--                                         <span>Plan Benefits:&nbsp;</span> -->
<%--                                         <c:if test="${phixRequest.benefits != null && phixRequest.benefits != '' }"> --%>
<%--                                         	<c:out value="${phixRequest.benefits}"/> --%>
<%--                                         </c:if> --%>
<%--                                         <c:if test="${phixRequest.benefits == null || phixRequest.benefits == '' }"> --%>
<!--                                         	No benefits selected -->
<%--                                         </c:if> --%>
<!--                                     </li> -->
                                    
                                    
<!--                                  </ul>       -->
<!-- </div> -->
<!-- </div> -->
    <section id="prelaunch-main">
	
  <div class="panelwrap row-fluid">

                <!-- Start of Pre-Launch Page -->
                <div class="panel span12">
                	<!-- <div class="row-fluid">
                		<div class="span12">
							<h4>Now that you know that coverage will be affordable next year, you have 2 options.</h4>
						</div>
					</div> -->
					
					<div class="row-fluid">
						<%-- <div class="span4 leftcol">						
               				 <c:if test="${showStateMessage == 'Y'}">
					                <div id="stateMessage">
										<p>Let's pick this conversation over the phone. Because you are resident of <c:out value="${stateName}"></c:out>, the best way for us to get you covered is with our certified support staff.</p>
										<h4 class="">
										Call us:
										<span>
											<c:if test="${sessionScope.ClickTrackIVRNumber==null}">
												(866) 602 8466                         
					                        </c:if>                                              
					                        <c:if test="${sessionScope.ClickTrackIVRNumber!=null}">
					                             (<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,0,3)}"/>)&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,3,6)}"/>&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,6,10)}"/>              
					                        </c:if>
					                        </span>
					                        <small>Mon-Fri 9AM to 10PM EST</small>
					                       </h4>
										</div>
										</c:if>
									<div class="alert alert-info">
									<p class="">Our state-certified agents will:</p>
										<ul class="alert alert-info">
											<li>Answer your questions</li>
											<li> Help you maximize your savings and discounts</li>
											<li> Explain health plan benefits</li>
											<li> Get you enrolled in a health plan that fits your needs!</li>
										</ul>
									</div>
							</div> --%>
	               
				
							<div class=" span8 offset2 rightcol">
							<h4 >Let us tell you when you can come back and shop online:</h4>
								<p class="span12">We're building an awesome online shopping place for you. Give us your info and we'll let you know when the new marketplace is live. Don't worry there's lots of time left in open enrollment.  
							</p>
							
								<div class="row-fluid">
						 			
										<form id="preLaunchForm" class="" action="thankyou" method="post" >
		                          		 <!--  <h3>Sign up now</h3> -->
		                            	<input id="showStateMessage" name="showStateMessage" type="hidden" value="${showStateMessage}">
		                            	<input id="stateName" name="stateName" type="hidden" value="${stateName}">
											<div class="span12">
												
				                                   	 <input id="leadName" name="leadName"  type="text" placeholder="Name" value="${name}" maxlength="100">
				                                   	 <div id="leadName_error" class="preform_error"></div>
			                                 
		                                  	</div>
		                                    <div class="span12">
			                                   
				                                    <input id="leadEmail" name="leadEmail"   type="text" placeholder="Email" value="${email}" maxlength="250">
				                                    <div id="leadEmail_error" class="preform_error"></div>
			                                   
		                                    </div>
		                                
											<%-- <span class="add-on">(</span>
											<input id="leadPhone1" name="leadPhone1" type="text" placeholder="XXX" maxlength="3" value="${fn:substring(phone, 0, 3)}" class="numbersOnlyField span1"><span class="add-on">) -</span>
											<input id="leadPhone2" name="leadPhone2" type="text" placeholder="XXX" maxlength="3" value="${fn:substring(phone, 3, 6 )}" class="numbersOnlyField span1"><span class="add-on">-</span>
											<input id="leadPhone3" name="leadPhone3" type="text" placeholder="XXXX" maxlength="4" value="${fn:substring(phone, 6, 10)}" class="numbersOnlyField span1"> 
											 --%>
											<div class="span12">
												
													<input id="leadPhone" name="leadPhone" type="text" placeholder="Phone Number" value="${phone}" maxlength="10" class="numbersOnlyField"> 
				                                    <div id="leadPhone_error" class="preform_error"></div>
			                                   
		                                    </div>
		                          
			                                <div class="span12">
			                                	<div class="gutter10">
			                                		<button id="updateLead" class="btn btn-large  btn-success" type="button" onclick="javascript:updateLeadRecord();">Submit</button>
			                                	</div>
		                              		 </div>
											<div class="span12">
												<div class="gutter20">
													<%-- <label for="isOkToCall" style="line-height:auto;display:inline-block;font-weight:normal;">
													<input id="isOkToCall" name="isOkToCall" type="checkbox" 
														<c:if test="${isOkToCall == 'Y' }">checked="checked"</c:if>
														style="display:inline;visibility: visible;">Yes, it's okay to call this number.  I agree that this phone number, including my wireless number (if provided), may be used by GetInsured to contact me, including through use of an automatic telephone dialing system.  I know I am not required to provide this consent to make a purchase.  </label>
			                               		 		 <div id="isOkToCall_error" class="preform_error"></div>
			                               
			                              				 <p style="line-height:14px;"><small>By pressing the 'Submit' button, I agree to receive communications from GetInsured.com.  I can unsubscribe or opt out at any time.
                      		 		 					</small>
                      		 							 </p>
                      		 				 		<div id="updateMessage" class="preform_error" style="display:none">
                      		 						 </div> --%>
                     			  					<!-- <button id="updateLead" class="btn btn-large btn-success" type="button" onclick="javascript:updateLeadRecord();">Keep me updated</button> -->
                       				 				<p>By pressing 'Submit',  I agree that this phone number, including my wireless number (if provided), may be used by GetInsured to contact me about insurance products, including through use of an automated dialer. I can unsubscribe or opt out at any time. I know I am not required to provide this consent to make a purchase.</p>
                       				 				<p>Your information is safe with us. <!-- We'll never share it and we won't contact you (unless you ask us to). --></p>
                       				 			</div>
                       						</div>
                       					</div>
                       					</div>
                      	 	 		</form>
                   			 </div>
                   		</div><!-- row-fluid-->
                	</div><!-- panel wrap -->
                 <!-- End of Pre-Launch Page -->
       </section>
	<footer>
			<div class="container-fluid">
                        
                        <div class="row-fluid">  
                            
                            <div class="span3">
                                <img src="/hix/resources/img/getinsured-green.png" alt="GetInsured.com">
                                <ul class="media-icons">
                                    <li><a href="https://twitter.com/GetInsuredcom" target="_blank">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-twitter icon-light"></i>
                                      </span>
                                      </a></li>
                                    <li><a href="https://www.facebook.com/GetInsured" target="_blank">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-facebook icon-light"></i>
                                      </span>
                                    </a></li>
                                    <!--   <li><a href="/answers/contact/">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-envelope icon-light"></i>
                                      </span></a></li> -->
                                </ul>
                                
                            </div>
                              
                            <div class="span5 offset1">
                            	
                                <ul class="footer-links">
                                     <li><a href="/answers/how-to-use/" target="_blank">How to Use This Site</a></li>
                                     <!-- <li><a href="/answers/" target="_blank">Health Insurance - What You Need to Know</a></li> -->
                                     <li><a href="https://quoting.getinsured.com/exchange/home.html" target="_blank">Partner with Us</a></li> 
                                        <li><a href="/answers/blog/" target="_blank">The Latest from our Blog</a></li>
                                  </ul>
                                  
                             </div>
                                <div class="span3">
                                <ul class="footer-links">
                             
                                      <li><a href="/answers/about/" target="_blank">About GetInsured</a></li>
                                     <li><a href="/answers/contact/" target="_blank">Contact Us</a></li>
                                      <li><a href="/answers/privacy-and-legal-notices/" target="_blank">Privacy and Legal Notices</a></li>
                             <!--         <li><a href="/answers/terms-and-conditions/">Terms & Conditions</a></li> -->
                               </ul>
                               
                            </div>

                            

                           <!--  <div class="span6 blog-link">
                                <h5>From the GetInsured Blog:</h5>
                                <h4><a href="#">3 Things Young People Should Know About Obamacare</a></h4>
                                <p>Okay, you've seen the surveys. My generation is not paying attention to Obamacare. We have no idea what it is or why it matters. And just like we procrasitnated on our homework in school, we're putting off...</p>
                                Read the Full Article</a>
                            </div> -->
                            
                        </div>
                        
                        <div class="row-fluid copyright">
                          <span>� 2013 GetInsured.com </span>
                        </div>
              </div>      
</footer>
             <script type="text/javascript">
        		$(document).ready(function(){
            	  //dropdownmenu

				  $('#dropDoenIcon').on('click', function() {
				  $('.nav-collapse').toggleClass('open');
	  
				  if($('div.overlay').length){
					$('div.overlay').remove();
				  }
				  else{
					$('<div class="overlay"></div>').css('opacity','0.5').height($(document).height()).appendTo('body');
				  }
				  return false;
				});
				});
            </script>     
    </body>
</html>