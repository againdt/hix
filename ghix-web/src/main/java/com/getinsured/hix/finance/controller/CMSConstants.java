package com.getinsured.hix.finance.controller;

public class CMSConstants {
	
	public static final String PAYMENT_TRANSACTION_ID = "Payment Transaction ID";
	public static final String MARKET_INDICATOR = "Market Indicator";
	public static final String ASSIGNED_QHP_IDENTIFIER="Assigned QHP Identifier";
	public static final String TOTAL_AMOUNT_OWNED="Total Amount Owned";
	public static final String PREMIUM_AMOUNT_TOTAL="Premium Amount Total";
	public static final String APTC_AMOUNT="APTC Amount";
	public static final String FIRST_NAME="First Name";
	public static final String MIDDLE_NAME="Middle Name";
	public static final String LAST_NAME="Last Name";
	public static final String SUFFIX_NAME="Suffix Name";
	public static final String PARTNER_ASSIGNED_CONSUMER_ID = "Partner Assigned Consumer ID";
	public static final String TIN_IDENTIFICATION="TIN Identification";
	public static final String STREET_NAME_1="Street Name 1";
	public static final String STREET_NAME_2="Street Name 2";
	public static final String STATE="State";
	public static final String ZIPCODE="Zip Code";
	public static final String CONTACT_EMAIL_ADDRESS="Contact Email Address";
	public static final String SUBSCRIBER_IDENTIFIER="Subscriber Identifier";
	public static final String ADDITIONAL_INFORMATION="Additional Information";
 	public static final String CITY_NAME="City Name";
 	public static final String PROPOSED_COVERAGE_EFFECTIVE_DATE="Proposed Coverage Effective Date";
 	public static final String PREFERRED_LANGUAGE_CODE = "Preferred Language Code";
}
