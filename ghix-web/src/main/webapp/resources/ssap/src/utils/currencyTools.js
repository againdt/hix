const options = {
    maximumFractionDigits : 2,
    currency              : 'USD',
    style                 : 'currency',
    currencyDisplay       : 'symbol'
};

const localStringToNumber = string => {
    // console.log(string, typeof string === 'string');
    if(typeof string !== 'string' && typeof string !== 'number') {
        throw new Error('Not a string or number has been passed to currencyTool');
    }
    return (string[0] === '(' ? -1 : 1) * Number(String(string).replace(/[^0-9.-]+/g,''));
};

export default {
    getMoneyFormat: (value, isCents) => {
        if(value && value < 0) {
            return `(${localStringToNumber(isCents ? -1 * value/100 : -1 * value)
                .toLocaleString(undefined, options)})`;
        }
        return value ? localStringToNumber(isCents ? value/100 : value)
            .toLocaleString(undefined, options) :
            '';
    },
    localStringToNumber,
    options
};