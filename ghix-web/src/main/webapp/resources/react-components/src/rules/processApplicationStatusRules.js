/**
 * Created by sellathurai_s on 9/5/18.
 */

import {Engine} from 'json-rules-engine';
import rulesData from './json/applicationStatusRules.json';
import RulesUtil from './rulesUtil';

class ProcessApplicationStatusRules {

    constructor(facts){
        // console.log("Inside Process Application Status Rules.");
        this.engine = new Engine();
        this.facts = facts;
        this.rulesData = rulesData;
        this.rulesUtil = new RulesUtil();
    }

    getDefaultOutput(applicationStatus) {
        return {
            "type": "SINGLE_COL",
            "col1": "",
            "col2": "",
            "col3": "",
        };
    }

    executeRules() {
        return new Promise((resolve, reject) => {

            // console.log("Inside Process Application Status Rules - Execute Rules.");

            for (let i = 0; i < this.rulesData.length; i++) {
                this.engine.addRule({
                    conditions: {
                        any: [{
                            all: this.rulesUtil.createRule(this.rulesData[i].rules)
                        }]
                    },
                    event: this.rulesUtil.createEvent(this.rulesData[i].result)
                });
            }

            let displayData = [];
            this.engine
                .run(this.facts)
                .then(events => { // run() returns events with truthy conditions
                    if (events && events.length > 0) {
                        displayData = events.map(item =>
                            { return item.params }
                        );
                        // console.log("ProcessApplicationStatusRules-" + JSON.stringify(displayData));
                        resolve(displayData);
                    } else {
                        // console.log("ProcessApplicationStatusRules Default State: " + JSON.stringify(this.getDefaultOutput("ER")));
                        displayData.push(this.getDefaultOutput("ER"));
                        // console.log("ProcessApplicationStatusRules No matching records");
                        resolve(displayData);
                    }
                });

        });
    }
};


export { ProcessApplicationStatusRules as default};