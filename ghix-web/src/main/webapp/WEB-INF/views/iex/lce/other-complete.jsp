<script type="text/ng-template" id="other-complete.jsp">
  <div class="gutter10">
    <form class="form-horizontal">
      <div class="gutter10 alert alert-info">
        <p>Thank you for reporting that you had a complex issue that prevented you from completing your enrollment during the Open Enrollment Period. In order to determine if you qualify for a Special Enrollment Period, Your Health Idaho needs some additional information from you. Please call us at 1-855-YH-IDAHO (1-855-944-3246) as soon as possible.</p>

        <p>When you call, the representative will ask for information about your situation to determine if your circumstances qualify you for a special enrollment period. The representative can also help you apply and enroll in coverage.</p>

        <p>If you're already enrolled in a plan and you get a special enrollment period, you can stay in your current plan in most cases, or you may be able to switch plans. In some limited cases, you may qualify for an earlier effective date of coverage. Remember, you must make the first premium payment before your coverage becomes effective.</p>

      </div>
      <div>
        <a class="btn btn-secondary pull-left" href="<c:url value="/indportal" />">Go Back to Dashboard</a>
        <!--<a class="btn btn-primary pull-right" ng-click="next()"><spring:message code="label.lce.next"/></a>-->
      </div>
    </form>
  </div>
</script>