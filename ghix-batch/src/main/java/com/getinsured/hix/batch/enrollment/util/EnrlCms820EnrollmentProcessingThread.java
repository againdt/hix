package com.getinsured.hix.batch.enrollment.util;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.batch.enrollment.service.EnrlCms820Service;

public class EnrlCms820EnrollmentProcessingThread implements Callable<Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlCms820EnrollmentProcessingThread.class);

	private List<Long> enrollmentIdList;
	private Integer summaryId;
	private JobService jobService = null;
	private StepExecution stepExecution = null;
	
	public EnrlCms820EnrollmentProcessingThread(List<Long> enrollmentIdList, Integer summaryId, JobService jobService, StepExecution stepExecution,
			EnrlCms820Service enrlCms820Service) {
		super();
		this.enrollmentIdList = enrollmentIdList;
		this.summaryId = summaryId;
		this.jobService = jobService;
		this.stepExecution = stepExecution;
		this.enrlCms820Service = enrlCms820Service;
	}

	private EnrlCms820Service enrlCms820Service;

	@Override
	public Integer call() throws Exception {
		try {
			return enrlCms820Service.processEnrollmentIdList(enrollmentIdList, summaryId, jobService, stepExecution);
		} catch (Exception e) {
			LOGGER.error("Exception occurred in EnrlCms820SummaryProcessingThread: ", e);
		}
		return null;
	}

}
