package com.getinsured.hix.provider.service.zipcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

//@Service(value = "solrZipCodeService")
public class SolrZipCodeServiceImpl implements SolrZipCodeService{
	private static final Logger logger = LoggerFactory.getLogger(SolrZipCodeServiceImpl.class);
	@Autowired
	@Qualifier("solrZipCodeServer")
	private SolrServer zipCodeRepository;
	private SolrQuery query;	
	private String resultSetfields = "ZipCode,City,State,County,CountyFIPS,StateFIPS,Zip_location";
	
		
	@Override
	@Cacheable("zip_codes")
	public ZipCode findByZipCode(String zip) {
		ZipCode tmpZip = null;
		this.query = new SolrQuery();
		this.query.set("q", "ZipCode:"+zip);
		this.query.set("fl", resultSetfields);
		try {
			QueryResponse response = this.zipCodeRepository.query(query);
			if(response.getResults().getNumFound() > 0){
				tmpZip = new ZipCode();
				ArrayList<SolrDocument> docs = response.getResults();
				SolrDocument tmp = docs.get(0);
				Map<String, Object> fieldMap = tmp.getFieldValueMap();
				tmpZip.setFieldValues(fieldMap);
			}
		} catch (SolrServerException e) {
			logger.error("findByZipCode("+zip+") failed with message \""+e.getMessage()+"\"");
		}
		return tmpZip;
	}

	@Override
	public List<ZipCode> getCityList(String city) {
		ZipCode tmpZip = null;
		ArrayList<ZipCode> retList = null;
		this.query = new SolrQuery();
		this.query.set("q", "City:"+city);
		this.query.set("fl", resultSetfields);
		try {
			QueryResponse response = this.zipCodeRepository.query(query);
			if(response.getResults().getNumFound() > 0){
				tmpZip = new ZipCode();
				ArrayList<SolrDocument> docs = response.getResults();
				retList = new ArrayList<ZipCode>();
				for(SolrDocument tmp: docs){
					tmpZip = new ZipCode();
					Map<String, Object> fieldMap = tmp.getFieldValueMap();
					tmpZip.setFieldValues(fieldMap);
					retList.add(tmpZip);
				}
			}
		} catch (SolrServerException e) {
			logger.error("getCityList("+city+") failed with message \""+e.getMessage()+"\"");
		}
		return retList;
	}

	@Override
	public List<ZipCode> getZipList(String zipCode) {
		ZipCode tmpZip = null;
		ArrayList<ZipCode> retList = null;
		if(this.zipCodeRepository == null){
			logger.error("No repo available for zip code:");
			return null;
		}
		this.query = new SolrQuery();
		this.query.set("q", "ZipCode:"+zipCode);
		this.query.set("fl", resultSetfields);
		try {
			QueryResponse response = this.zipCodeRepository.query(query);
			if(response.getResults().getNumFound() > 0){
				tmpZip = new ZipCode();
				ArrayList<SolrDocument> docs = response.getResults();
				retList = new ArrayList<ZipCode>();
				for(SolrDocument tmp: docs){
					tmpZip = new ZipCode();
					for(String tmpStr: tmp.getFieldNames()){
						tmpZip.setValue(tmpStr,tmp.getFieldValue(tmpStr));
					}
					retList.add(tmpZip);
				}
			}
		} catch (SolrServerException e) {
			logger.error("getZipList("+zipCode+") failed with message \""+e.getMessage()+"\"");
		}
		return retList;
	}
	
	@Override
	public boolean validateZipAndCountyCode(String zipCode, String countyCode){
		this.query = new SolrQuery();
		this.query.set("q", "ZipCode:"+zipCode);
		this.query.setFilterQueries("County:"+countyCode);
		try {
			QueryResponse response = this.zipCodeRepository.query(query);
			if(response.getResults().getNumFound() > 0){
				return true;
			}
		} catch (SolrServerException e) {
			logger.error("validateZipAndCountyCode("+zipCode+","+countyCode+") failed with message \""+e.getMessage()+"\"");
		}
		return false;
	}

	@Override
	public List<ZipCode> findByZip(String zip) {
		return this.getZipList(zip);
	}
	
	@Override
	public List<String> getZipByCountyCodeAndZips(List<String> zips,String countyCode) {
		if(countyCode.length() !=5){
			logger.error("Invalid county code provided(>5)");
			return null;
		}
		ArrayList<String> resultList = null;
		String zipOpts = "";
		String stateFips = countyCode.substring(0,2);
		String countyFips = countyCode.substring(2);
		logger.info("State FIPS:"+stateFips+" County Fips:"+countyFips);
		this.query = new SolrQuery();
		this.query.set("q", "*:*");
		this.query.set("fl", "ZipCode");
		this.query.addFilterQuery("CountyFIPS:"+countyFips);
		this.query.addFilterQuery("StateFIPS:"+stateFips);
		for(String tmp:zips){
			zipOpts += "ZipCode:"+tmp+" OR ";
		}
		if(zipOpts.endsWith("OR ")){
			zipOpts = zipOpts.substring(0, zipOpts.length()-3);
		}
		this.query.addFilterQuery(zipOpts);
		try {
			QueryResponse response = this.zipCodeRepository.query(query);
			if(response.getResults().getNumFound() > 0){
				resultList = new ArrayList<String>();
				ArrayList<SolrDocument> results = response.getResults();
				for(SolrDocument tmp: results){
					resultList.add((String)tmp.getFieldValue("ZipCode"));
				}
			}
		} catch (SolrServerException e) {
			logger.error("getZipByCountyCodeAndZips("+zips+","+countyCode+") failed with message \""+e.getMessage()+"\"");
		}
		return resultList;
	}

	public SolrServer getZipCodeRepository() {
		return zipCodeRepository;
	}
	
	public void setZipCodeRepository(SolrServer zipCodeRepository) {
		if(zipCodeRepository == null){
			logger.error("Received null repo");
		}else{
			logger.info("Repository setup for Solr ZipCode service done!");
		}
		this.zipCodeRepository = zipCodeRepository;
	}

}