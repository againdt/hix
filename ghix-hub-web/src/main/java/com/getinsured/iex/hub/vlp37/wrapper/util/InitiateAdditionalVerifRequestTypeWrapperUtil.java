package com.getinsured.iex.hub.vlp37.wrapper.util;

import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.wrapper.InitiateAdditionalVerifRequestTypeWrapper;


public final class InitiateAdditionalVerifRequestTypeWrapperUtil {

	private static final String REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
	private static final String REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
		
	public static void addIndicatorsToWrapper(JSONObject obj, InitiateAdditionalVerifRequestTypeWrapper wrapper){
		
		Object param = null;
		
		param = obj.get(REQUESTSPONSORDATAINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestSponsorDataIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTGRANTDATEINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestGrantDateIndicator((Boolean) param);
		}
	}
	
}
