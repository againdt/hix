package com.getinsured.hix.batch.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.enrollment.PassiveEnrollment;

@Repository
@Qualifier("passiveEnrollmentDataRepository")
public interface PassiveEnrollmentDataRepository extends JpaRepository<PassiveEnrollment, Integer> {

	@Query("select pe from PassiveEnrollment pe where batchId = :batchId and recordProcessingStatus = :status")
    List<PassiveEnrollment> getAllRecordsForBatchIdAndRecordProcessingStatus(@Param("batchId") Long batchId, @Param("status") String recordingProcessingStatus);
}
