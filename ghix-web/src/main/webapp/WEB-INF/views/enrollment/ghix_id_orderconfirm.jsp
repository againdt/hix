<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="java.util.List, com.getinsured.hix.model.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>


<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
#menu {
	display: none;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}

.no-health-plan {
	background: #f9f7c1;
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.no-dental-plan {
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.cart {
	border: 1px solid #c9c9c9;
}

h3 {
	line-height: 25px;
	margin: 0
}

/* .darkblue {
	background: #0c9bd2;
	color: #fff;
} */

.darkgreen {
	background: #008080;
	color: #fff;
}

.SILVER {
	background: url(../resources/img/silver-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.GOLD {
	background: url(../resources/img/gold-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.BRONZE {
	background: url(../resources/img/bronze-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.CATASTROPHIC {
	background: url(../resources/img/catastrophic-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.PLATINUM {
	background: url(../resources/img/platinum-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.NOLABEL {
	height: 50px;
}
.planimg {
	margin-top: 20px;
	margin-left: 20px;
	padding-bottom: 10px;
}

.closeout {
	position: relative;
	top: 24px;
	right: 10px;
	margin: -19px 0 0 0;
}

.mini-cart {
	width: 380px;
	border: 4px solid #4f4f4f;
	background: #fff;
	position: absolute;
	z-index: 10;
}

.mini-cart h3 {
	color: #fff;
}

.mini-cart .header {
	background: #4f4f4f;
}
#container-wrap .container {
	margin-top:0px!important;
}
.view-broker {
	top: 8px!important;
	z-index: 10;
}

</style>
<script type="text/javascript">

	$(document).ready(function() {

		setPageTitle('<spring:message code="enroll.order.confirmation"/>')

		// Variables for dataLayer
		var healthPlanInfo = $("#healthPlanInsurer").val() + ' | ' + $("#healthPlanName").val() + ' | ' + $("#healthPlanLevel").val();
		var dentalPlanInfo = $("#dentalPlanInsurer").val() + ' | ' + $("#dentalPlanName").val() + ' | ' + $("#dentalPlanLevel").val();
		var healthPlanPresent = $("#healthPlanPresent").val();
		var dentalPlanPresent = $("#dentalPlanPresent").val();
		var planInfo = '';

		if(healthPlanPresent && dentalPlanPresent) {
			planInfo = healthPlanInfo + ' :: ' + dentalPlanInfo;
		}
		if(healthPlanPresent && !dentalPlanPresent) {
			planInfo = healthPlanInfo;
		}
		if(!healthPlanPresent && dentalPlanPresent) {
			planInfo = dentalPlanInfo;
		}

		// Data layer variable to be pushed at the end of shell.jsp
    	dl_pageCategory = 'Order Confirmation';

    	// Checkout Step 5
		dl_eventStack.push({
			'event': 'checkout',
			'eventCategory': 'Plan Selection - ecommerce',
			'eventAction': 'Confirmation Pageview',
			'eventLabel': planInfo,
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 5}
				}
			}
		});


		$('.info').tooltip();

		$('.remove').hide();
		$('.close').hover(function() {
			$(this).prev('div').toggle();
		});
	});

	function gotoparent() {

		window.top.location.href ='/hix/indportal';
	}

	function payHealthExternalDlEvent() {
		// Checkout Step 6
		window.dataLayer.push({
			'event': 'checkout',
			'eventCategory': 'CalHEERS - ecommerce',
			'eventAction': 'Pay Now Button Click',
			'eventLabel': 'Order Confirmation Page',
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 6}
				}
			}
		});
	}
</script>


<div class="gutter10">
<c:choose>
	<c:when test="${PGRM_TYPE == 'Shop'}">
		<div class="topnav">
			<ul class="inline">
				<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="">Verify Information</a></li>
			  	<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="">Update Dependents</a></li>
			 	<li ><a href="#" rel="tooltip" data-placement="bottom" data-original-title="">Select Plan</a></li>
			   	<li class="active"><a href="#" rel="tooltip" data-placement="bottom" data-original-title="" class="last">Enroll</a></li>
		   	</ul>
		</div>
	 </c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
	<c:if test="${errorMsg != ''}">
		<div class="row-fluid">
			<div style="font-size: 14px; color: red">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</div>
		</div>
	</c:if>

<c:if test="${errorMsg == ''}">
	<div class="row-fluid" id="titlebar">
		<h1><spring:message code='enroll.orderConfirm.confirm' /></h1>
	</div>

	<div class="row-fluid">
			<div class="span12" id="rightpanel">
								<div class="alert alert-info hide-for-nm">
									<spring:message code='enroll.ms.orderConfirm.confirm.content.value' />
								</div>
								<div class="alert alert-info hide-for-ms">
									<c:choose>
										<c:when test="${enrollment_type == enrollment_type_individual}">
												<% if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equals("MN")){
												%>
													<p><strong><spring:message code='enroll.nm.orderConfirm.confirm.payment.content.value1'/></strong></p>
													<p><strong><spring:message code='enroll.nm.orderConfirm.confirm.payment.content.value2'/></strong></p>
													<p><strong><spring:message code='enroll.nm.orderConfirm.confirm.payment.content.value3'/></strong></p>
												<%} %>
												<% if(!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equals("MN")){
													%>
														<p><strong><spring:message code='enroll.nm.orderConfirm.confirm.payment.content.value'/></strong></p>
														<p><strong>FURTHER ACTION REQUIRED:</strong> <br>You must pay your first month's premium before your enrollment can be finalized. This health/dental insurance is not yet in force.</p>
													<%} %>
										</c:when>
										<c:otherwise>
											<spring:message code='enroll.nm.orderConfirm.confirm.content.value' />
										</c:otherwise>
									</c:choose>
								</div>
						<form name="frmorderconfirm" id="frmorderconfirm" action="orderconfirmsubmit" method="post">
							<df:csrfToken/>
							<input type="hidden" name="cart_id" id="cart_id" value="${cartId}">
							<input type="hidden" name="redirectLink" id="redirectLink" value="${redirectLink}">

							<c:if test="${fn:length(enrollmentMap) gt 0}">
								<c:forEach var="enrollmentMapEntry" items="${enrollmentMap}">
									<c:set var="enrollmentlist" value="${enrollmentMapEntry.value}"></c:set>
									<c:if test="${fn:length(enrollmentlist) gt 0}">
										<div class="cart">
											<div class="header">
												<h4> <spring:message code='enroll.orderConfirm.${enrollmentMapEntry.key}' /></h4>
											</div>
											<c:forEach items="${enrollmentlist}" var="enrollmentObj">
												<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentObj.benefitEffectiveDate}" var="effDate" />
												<div class="gutter10">
													<div class="health-plan">
														<p>
															<strong>${enrolleeNameMap[enrollmentObj.id]}</strong>
																 <span
																	class="pull-right">
																	<c:if test="${enrollmentObj.disableEffectiveDate != 'YES'}">
																	  <spring:message code='enroll.orderConfirm.effDate' /> &nbsp;
																		 <strong>${effDate}</strong>
																	</c:if>
																</span>
														</p>
															<c:choose>
																<c:when test="${enrollmentMapEntry.key == 'HEALTH'}">
																	<input type="hidden" name="healthPlanInsurer" id="healthPlanInsurer" value="${enrollmentObj.insurerName}" />
																	<input type="hidden" name="healthPlanName" id="healthPlanName" value="${enrollmentObj.planName}" />
																	<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value="${enrollmentObj.planLevel}" />
																	<input type="hidden" name="healthPlanPresent" id="healthPlanPresent" value="true"/>
																</c:when>
																<c:when test="${enrollmentMapEntry.key == 'DENTAL'}">
																	<input type="hidden" name="dentalPlanInsurer" id="dentalPlanInsurer" value="${enrollmentObj.insurerName}" />
																	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value="${enrollmentObj.planName}" />
																	<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value="${enrollmentObj.planLevel}" />
																	<input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="true"/>
																</c:when>
																<c:otherwise>
																	<input type="hidden" name="healthPlanInsurer" id="healthPlanInsurer" value=""/>
																	<input type="hidden" name="healthPlanName" id="healthPlanName" value=""/>
																	<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value=""/>

																	<input type="hidden" name="dentalPlanInsurer" id="dentalPlanInsurer" value=""/>
																	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value=""/>
																	<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value=""/>

																	<input type="hidden" name="healthPlanPresent" id="healthPlanPresent" value="false"/>
																	<input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="false"/>
																</c:otherwise>
															</c:choose>
                                                        <div class="div_bordered">
                                                            <div class="row-fluid">
                                                                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 txt-center">
                                                                    <div class="planimg">
                                                                    <c:set var="issuerIdVar" ><encryptor:enc value="${enrollmentObj.issuerId}" isurl="true"/> </c:set>
                                                                        <img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage(this)" />
                                                                    <br>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 txt-center">
                                                                    <strong>${enrollmentObj.insurerName}</strong><br>
                                                                    <fmt:formatDate pattern="MMddyyyy" value="${enrollmentObj.benefitEffectiveDate}" var="coverageDate" />
                                                                    <c:url value="/private/planinfo/${coverageDate}/${enrollmentObj.planId}" var="planDetailsURL"/>
                                                                    <a href='${planDetailsURL}' target ='_blank'>${enrollmentObj.planName}</a>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 txt-center">
                                                                    <spring:message code='enroll.orderConfirm.mnthlyPremium' /><br>
                                                                    <c:if test="${enrollmentObj.aptcAmt != null && (enrollmentObj.aptcAmt > 0.00)}">
                                                                        <spring:message code='enroll.orderConfirm.taxCredit' />
                                                                    </c:if>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 txt-center">
                                                                    <strong>
                                                                        <span class="block">$ <fmt:formatNumber value="${enrollmentObj.grossPremiumAmt}" minFractionDigits="2" /></span>
                                                                        <c:if test="${enrollmentObj.aptcAmt != null && (enrollmentObj.aptcAmt > 0.00)}">
                                                                        <span class="block">-$ <fmt:formatNumber value="${enrollmentObj.aptcAmt}" minFractionDigits="2" /></span>
                                                                        </c:if>
                                                                    </strong>
                                                                </div>
                                                            </div>
                                                        <c:if test="${fn:length(enrollmentMap) gt 0 || fn:length(enrollmentlist) gt 0}">
                                                        <div class="row-fluid">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <c:choose>
                                                                <c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key != 'DENTAL'}">
                                                                    <c:if test="${ShowHealthPaymentButton == 'true'}">
                                                                        <c:set var="isPayButton" value="true"/>
                                                                            <div class="row-fluid">
                                                                                <a id ="healthPaymentBtn" class="btn btn-primary btn_save jsPaymentBtn pay_health_google_tracking pull-right" href="<c:url value="/finance/paymentRedirect?finEnrlDataKey=finEnrlHltDataKey"/>" onclick="payHealthExternalDlEvent()"></i><spring:message code='enroll.orderConfirm.health.makePayment'/></a>
                                                                            </div>
                                                                    </c:if>
                                                                    <c:if test="${finEnrlHltDataKey == null || finEnrlHltDataKey == ''}">
                                                                        <div class="row-fluid">
                                                                            <span class="pull-left"><spring:message code='	enroll.nm.payment.noButton.message.value'/></span>
                                                                        </div>
                                                                    </c:if>
                                                                <c:if test="${ShowHealthPaymentButton == 'false'}">
                                                                    <div class="row-fluid">
                                                                        <div class="alert alert-info hide-for-ms">
                                                                            <spring:message code='enroll.nm.payment.offline.message_3.value'/>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                </c:when>

                                                                <c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key == 'DENTAL'}">
                                                                    <c:if test="${ShowDentalPaymentButton == 'true'}">
                                                                        <c:set var="isPayButton" value="true"/>
                                                                        <div class="row-fluid">
                                                                            <a id = "dentalPaymentBtn" class="btn btn-primary btn_save pull-right jsPaymentBtn pay_dental_google_tracking" href="<c:url value="/finance/paymentRedirect?finEnrlDataKey=finEnrlDntDataKey"/>" onclick="payHealthExternalDlEvent()"></i><spring:message code='enroll.orderConfirm.dental.makePayment'/></a>
                                                                        </div>
                                                                    </c:if>
                                                                    <c:if test="${finEnrlDntDataKey == null || finEnrlDntDataKey == ''}">
                                                                        <div class="row-fluid">
                                                                            <span class="pull-left"><spring:message code='enroll.nm.payment.noButton.message.value'/></span>
                                                                        </div>
                                                                    </c:if>
                                                                    <c:if test="${ShowDentalPaymentButton == 'false'}">
                                                                        <div class="row-fluid">
                                                                            <div class="alert alert-info hide-for-ms">
                                                                                <spring:message code='enroll.nm.payment.offline.message_3.value'/>
                                                                            </div>
                                                                        </div>
                                                                    </c:if>

                                                                </c:when>
                                                            </c:choose>
                                                            </div>
                                                        </div>
                                                        </c:if>
                                                        </div>
                                                    </div>
												</div>
											</c:forEach>
										</div>
									</c:if>
								</c:forEach>

								<c:choose>
                                <%--  Totals Table for Individual  --%>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<div class="total_table">
											<c:if test="${enrollmentObj.aptcAmt != null && (enrollmentObj.aptcAmt > 0.00)}">
											<div class="row-fluid">
												<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 txt-right"><spring:message code='enroll.orderConfirm.totalMonthlyPremium' /></div>
												<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 txt-center"><p class="lead margin0-b">$<fmt:formatNumber value="${totalMonthlyPremium}"	minFractionDigits="2" /></p></div>
											</div>
											<div class="row-fluid">
												<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 txt-right"><spring:message code='enroll.orderConfirm.taxCredit1'/></div>
												<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 txt-center"><p class="lead margin0-b">-$<fmt:formatNumber value="${totalAptcContribution}" minFractionDigits="2" /></p></div>
											</div>
											</c:if>
											<div class="row-fluid">
												<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 txt-right"><spring:message code='enroll.orderConfirm.yourMonthlyPayment' /></div>
												<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 txt-center"><p class="lead margin0-b"> $<fmt:formatNumber value="${totalCost}" minFractionDigits="2" /></p></div>
											</div>
										</div>
									</c:when>
								</c:choose>
							</c:if>
							<br>
							<c:if test="${showChngPlans==true}">
								<h4 class="graydrkbg gutter10 marginTop20"><spring:message code='enroll.orderConfirm.mngPlanChng' /></h4>
								<div class="gutter10">
								  <p><spring:message code='enroll.nm.makingChangesToPlan.value' /></p>
								</div>
							</c:if>
							<c:if test="${fn:length(disEnrollmentMap) gt 0}">
								<h4 class="graydrkbg gutter10 marginTop20"><spring:message code='enroll.orderConfirm.disenrollmentsFromPlanTitle' /></h4>
								<div class="gutter10">
									<p><spring:message code='enroll.orderConfirm.disenrollmentsFromPlan' /></p>
									<p>
										<c:forEach var="disEnrollment" items="${disEnrollmentMap}">
	  										&nbsp<b><c:out value="${disEnrollment.key}"/>: &nbsp <c:out value="${disEnrollment.value}"/></b><br>
										</c:forEach>
									</p>
								</div>
							</c:if>
							<c:if test="${showDisc==true}">
								<h4 class="graydrkbg gutter10 marginTop20"><spring:message code='enroll.orderConfirm.disclaimer' /></h4>
								<div class="gutter10">
									<p><spring:message code='enroll.nm.orderConfirm.disclaimer.value' /></p>
								</div>
							</c:if>
							<% if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("NM")){
 	 	 	 	              %>
							<c:if test="${fn:length(adultDentalPlansLink) gt 0}">
								<div>
									<spring:message code='enroll.nm.orderConfirm.adultDentalPlans_1'/>
									<a href="${adultDentalPlansLink}" target="_blank"> <u>here</u></a>
									<spring:message code='enroll.nm.orderConfirm.adultDentalPlans_2' arguments="${stateName}"/>
								</div>
							</c:if>
							<%} %>
							<div class="form-actions">
								<div class="controls">
									<a onclick="gotoparent()" href="#" class="btn btn-primary btn-small btn_save pull-right goto_dashboard_google_tracking"><spring:message code='go.to.dashboard'/></a>
									<a href="#" class="btn btn-small btn_save pull-right print_google_tracking" onclick="window.print()"> <i class="icon-print"></i><spring:message code='enroll.orderConfirm.print' /></a>
									<a href="/hix/indportal#/customGrouping" class="btn btn-primary btn_save btn-small pull-right goto_grouping_google_tracking"><spring:message code='enroll.orderConfirm.shopForMoreMembers' text="Shop For More Members" /></a>
								</div>
							</div>

							<% if(!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("NM")){
								%>
							<c:if test="${isPayButton == 'true'}">
							  <div class="alert">
							  	<p class="txt-right">
							   		<c:url value="/indportal" var="exitPayOffLineURL"/>
						        	<a href='${exitPayOffLineURL}' class="btn btn-primary btn_save"><spring:message code='enroll.orderConfirm.exit.pay.offline'/></a>
							  	</p>
							  </div>
							</c:if>
							<%} %>
						</form>
			</div>
		</div>
	</c:if>
</div>

<!-- Code Starts for Payment Redirection-->
<script type="text/javascript">
$(function(){
  $('.jsPaymentBtn').on('click',function(e){
	e.preventDefault();

    me = $(this);

	//Make the button as disable state
	me.attr('disabled',true);
	me.unbind('click');

	var curURL = me.attr('href');

	//Open URL in new window
	window.open(curURL,"_blank", 'CarrierRedirection',"width:1000px,height:680px");

	//Reset href for disable state:
	me.attr('href','javascript:void(0)');

  });
});

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}
</script>
<!-- Code End for Payment Redirection-->

<script type="text/javascript">

function resizeimage() {
	$(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;

		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }

		// Check if current height is larger than max
		if(height > maxHeight){
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio
		}

		if($(this).parents('td')){
    		$(this).parents('td').width(maxWidth);
    	}


		$(this).css('display','block');
    });
}
</script>
