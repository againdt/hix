<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"
	prefix="util"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />

<script>
var ctx = "<%=request.getContextPath()%>";
</script>

<style>

h1.nextprev {
    color: #ffffff;
    font-size: 2.8em;
    line-height: 2.8em;
}

.scroll-left-arrow-wrapper, .scroll-right-arrow-wrapper {
	width: 25px;
	background: #f3f3f3;
	padding: 82px 4px;
	cursor: pointer;
}
.scroll-left-arrow-wrapper {
	float: left;
	margin-left: -11px;
}
.scroll-right-arrow-wrapper {
	float: left;
	margin-right: -11px;
	border-left: solid 1px #e2e2e2;
}
.view-plans {
	width: 90.9%;
	margin: 0 32px;
}
.plans-wrap {
	float: left;
	background: url(/hix/resources/img/teeth.png) repeat-x left bottom;
	height: 240px;
	position:relative;
}

.plans-top-wrapper {
	z-index: 2;
	text-align:center;
	width:699px;
}
.plans-section-content .plan {
	float: left;
	width: 212px; /*22.64%;*/
	position: relative;
}
.plans-container {
	margin-top: -10px;
	border-right: 1px solid #E2E2E2;
}
#planSelect div.plan:nth-child(4n) {
	border-right: 0px !important;
}
div.plans-top-wrapper.span6 {
	width:48.317948718%;
}
.scroll-left-arrow-wrapper {
	margin-left:0px;
}
.plans-wrap {
	width: 90.2%;
}
.plans-section-content .plan {
	float: left;
	width: 203px;
	position: relative;
}
.table th, .table td, .view-plans .table td {
	border:0px;
	vertical-align:bottom;
}
.dedictible .table td {
	border-bottom: 1px solid #E2E2E2;
}
div.view-plans .plan:nth-child(2n) {
	border-left: 1px solid #E2E2E2;
	border-right: 1px solid #E2E2E2;
}
div.plans-section-content .plan {
	border-left: 1px solid #E2E2E2;
	
}

.highcharts-legend,.highcharts-axi {
	display: none !important;
	
}
div#plans-header.plans-header.plans-section.plans-wrap .plan {padding: 30px 0px;}

.provider-logo {
    height: 45px;
}
</style>

<div class="gutter10" id="tierdetails">
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
		<h4 class="graydrkbg gutter10">
			 <a href="<c:url value="/shop/employer/plandisplay/plantier" />" class="btn"> <spring:message  code="label.back"/></a>
            <span class="offset1"><spring:message code = "label.viewPlanDetails"/></span>
           <!--  <a href="#" class="btn btn-primary setBenchmarkPlan pull-right"> Next </a> -->
            	</h4>
		<div class="plans-top-wrapper">	
			<div id="planSelect" class="tabbedSections">				
					<div class="row-fluid" id="titlebar">
						<div class="gutter10">
							<div id="plansbar">
								<h3 class="pull-left" id="Nplans">
									<div id="totalPlanCount">${planCount}</div>
									<spring:message code = "label.plans"/>
								</h3>
								<!--PAGINATION  -->
								<div class="pagination pull-left">
									<ul id="pagination">
									</ul>
								</div>
								<!--SORT  -->
								<!-- <span id="sort"> -->
								<!-- </span> &nbsp; &nbsp;  -->
								<!-- pull-right -->
							</div>
							<!--plansbar-->
						</div>
						<!--gutter10-->
					</div>
					<!--titlebar-->

					<div class="scroll-left-arrow-wrapper">
						<div class="scroll-left-arrow">
							<h1 id="prevItem" class="nextprev"><i class="icon-chevron-left"></i></h1>
						</div>
					</div>
					<!-- MAIN PLAN SUMMARY SECTION  -->
					<div class="plans-header plans-section plans-wrap"
						id="plans-header">
						<div class="plans-header-content-wrapper">
							<div class="plans-section-content">
								<input type="hidden" name="benchmarkPlan" id="benchmarkPlan" value="${benchmarkPlan}" />
								<input type="hidden" name="maxEmployerCost" id="maxEmployerCost" value="" />
              					<input type="hidden" name="maxEmployeeCost" id="maxEmployeeCost" value="" />
              					
								<div id="summary" class="��well" style="background: none;"></div>
								<div id="summary_err" class="��well"
									style="background: none; display: none;">
									<h3 class="box-gutter10 label-info"
										style="vertical-align: center; margin: 20px 10px; width: 95%">
										<spring:message code = "label.changePreferances"/></h3>
								</div>
							</div>
							<!-- plans-section-content-->
						</div>
						<!--plans-header-content-wrapper-->
					</div>
					<!--plans-section plans-header-->
					<!-- PLAN NAVIGATION  -->
					<div class="scroll-right-arrow-wrapper">
						<div class="scroll-right-arrow">
							<h1 id="nextItem" class="nextprev"><i class="icon-chevron-right"></i></h1>
						</div>
					</div>
				</div>
				</div>
				<!-- End of top bar -->
				<table class="view-plans">
					<tbody>
						<tr>
							<td class="center-container row-fluid">
								<div class="plans-container">
									<div class="plans-section row-fluid" id="plans-cost">
										<div class="plans-section-header">
											<h4 data-toggle="collapse" data-target="#row1" class="collapsetoggle">
											<i class="icon-chevron-right"></i>
												<spring:message code = "label.monthlyCostToYou"/>  <small>(<spring:message code = "label.basedOnContri"/> ${emplrContributionEmployee}% <spring:message code = "label.and"/> ${emplrContributionDependent}%  )</small>
													<a href="#" rel="tooltip" data-placement="top"
													data-original-title="<spring:message code = "label.rangeOfCosts"/>"></a>
											</h4>
										</div>
									</div>
										<!-- <div class="plans-section-info"></div> -->
										<div style="height: auto;" class="plans-section-content row-fluid in collapse" id="row1">
											<div id="employerCost"></div>
										</div>
										<!--plans-section-content-->
									</div>

									<!--plans-section row-fluid-->
									<div class="plans-section  row-fluid" id="plans-cost">
										<div class="plans-section-header">
											<h4 data-toggle="collapse" data-target="#row2" class="collapsetoggle">
											<i class="icon-chevron-right"></i>
												<spring:message code = "label.costToEachEmp"/> <small>(<spring:message code = "label.basedOnContri"/> ${emplrContributionEmployee}% <spring:message code = "label.towardsEmp"/>)</small>
												<a href="#" rel="tooltip" data-placement="top"
									data-original-title="<spring:message code = "label.rangeOfCosts"/>" 
									class="info"><div class="icon-question-sign"></div></a> 
											</h4>
										</div>
										<!-- <div class="plans-section-info"></div> -->
										<div style="height: auto;" class="plans-section-content row-fluid in collapse" id="row2">
											<div id="employeeCost"></div>
											<!--plans-section-content-->
										</div>
									</div>
									<!--plans-section row-fluid-->
									<div class="plans-section dedictible row-fluid">
										<div class="plans-section-header">
										<h4 data-toggle="collapse" data-target="#row3" class="collapsetoggle">
									    </h4>

										<!--plans-section-content-->
									</div>
									<!--plans-section row-fluid-->
								</div> <!--plans-container-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--gutter10-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
<!--main-->

<!--sample template for result item-->
<script type="text/html" id="resultItemTemplateSummary">

			<div class="plan plan-<@=planId@>">
				<div style="height:100px" tabindex="0"><issuerLogo:view issuerId="<@= issuerId @>" /><span class="aria-hidden"><@= name @> plan <spring:message code = "label.avgEmpPrem"/> $<@=indvPremium @></span></div>
				<table class="planNameWrap">
					<tr>
						<td class="planName planNameWrap" >
							
							<@= name @>
						</td>
					</tr>
				</table>
				<h4 class="premium"><small><spring:message code = "label.avgEmpPrem"/></small>$<@=indvPremium @></h4>

            </div>			
</script>
<script type="text/html" id="resultItemTemplateEmployerCost">
<div class="plan plan-<@=planId @>">
    <table class="table ">
      <tbody>
        <tr>
          <td>
				<div id="Employer_<@=planId@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
	  	  </td>
        </tr>
      </tbody>
    </table>
</div>
</script>
<script type="text/html" id="resultItemTemplateEmployeeCost">
<div class="plan plan-<@=planId @>">
    <table class="table ">
      <tbody>
        <tr>
          <td>
				<div id="Employee_<@=planId@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
	 	  </td>
        </tr>
      </tbody>
    </table>
</div>
</script>
<!--sample template for pagination UI-->

<script type="text/html" id="tmpPagination">
		
			<@ if (page != 1) { @>
				<li><a href="#" class="prev" data-original-title=""><span aria-hidden="true">&#8249;</span> <span class="aria-hidden">Previous page</span></a></li>
			<@ } @>
			<@ _.each (pageSet, function (p) { @>
				<@ if (page == p) { @>
					<li class="active"><a href="#" class="page" data-original-title=""><span class='aria-hidden'>Page </span> <@= p @> <span class='aria-hidden'> Current Page </span></a></li>
				<@ } else { @>
					<li><a href="#" class="page" data-original-title="" id="<@=p@>"><span class='aria-hidden'>Page </span> <@= p @></a></li>
				<@ } @>
			<@ }); @>
			<@ if (lastPage != page) { @>
				<li><a href="#" class="next" data-original-title=""><span aria-hidden="true">&#8250;</span> <span class="aria-hidden">Next page</span></a></li>
			<@ } @>
		
</script>

<script type="text/html" id="tmpSort">
<div class="dropdown">
                 <a href="#" data-target="#" data-toggle="dropdown-menu" role="button" id="sort_link" class="btn btn-small dropdown-toggle" data-original-title="" onclick="toggleSort();">
                           <spring:message code = "label.sortBy"/>
	                  <b class="caret"></b>
                 </a>
                 <ul class="dropdown-menu" id="sort_dropdown">
                     <li><a href="#" class="sort_premium_asc" id="premium_asc"><spring:message code = "label.priceLowHigh"/></a></li>
                     <li><a href="#" class="sort_premium_desc" id="premium_desc"><spring:message code = "label.priceHighLow"/></a></li>
                     <li><a href="#" class="sort_issuer" id="issuerText"><spring:message code = "label.insurer"/><span id="issuerTextOrder"><span></a></li>
                 </ul>
 </div>
</script>
	

<script src="<c:url value="/resources/js/waypoints.min.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/jquery-ui-1.8.22.custom.min.js" />">
	
</script>
<script src="<c:url value="/resources/js/jquery.multiselect.js" />">
	
</script>

<script src="<c:url value="/resources/js/underscore-min.js" />">
	
</script>
<script src="<c:url value="/resources/js/backbone-min.js" />">
	
</script>
<script src="<c:url value="/resources/js/json2.js" />">
	
</script>
<script src="<c:url value="/resources/js/plandisplay/showplans.js" />">
	
</script>

<!--Backbone.Paginator-->
<script src="<c:url value="/resources/js/backbone-pagination.js" />">
	
</script>

<!--Models/Collections-->
<script src="<c:url value="/resources/js/plandisplay/models/plan.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/collections/PaginatedCollection.js" />">
	
</script>

<!--Views-->
<script
	src="<c:url value="/resources/js/plandisplay/views/PlanView.js" />"></script>
<script
	src="<c:url value="/resources/js/plandisplay/views/PaginationView.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/views/SortView.js" />">
</script>
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/exporting.js" />"></script> 
	
<script type="text/javascript">
	$(window).load(function() {
		$('.view-plans').animate({
			opacity : 1,
			right : 0
		}, 650);
	});

	$(document).ready(function() {
/* 		$('.navbar .dropdown-toggle').click(function() {
			$('.navbar .dropdown').addClass('open');
		}); */

		$.ajaxSetup({
			cache : false
		});

		$('#planSelect a').tooltip({
			placement : 'top'
		});

				
		$('div#summary.well').find('.plans').css({
			"font-style" : "italic",
			"font-weight" : "bolder"
		});
		
			$('.info').tooltip();
			$('.plans-section-header').on(
					'click show hide',
					function(e) {
						$(e.target).find('i').toggleClass('icon-chevron-right icon-chevron-down', 200);
					});
	});	
    
	function toggleSort() {
		$('#planSelect .dropdown').addClass('open');
	}
	
	function drawEmployerBar(barId, dependentCost,EmployeeCost,maxVal){
		chart = new Highcharts.Chart({
            chart: {renderTo: barId,
	            		type: 'column', 
	            		spacingBottom: 0, 
	            		marginBottom: 20, 
	            		marginTop: 5,
	            		events: {
		    	            load: function(event) {
		    	            	var id = $(event.target.container).attr('id');
		    	            	$('.plan .table').each(function(){
		    	            		var firstValue = $('#'+id).find('.highcharts-data-labels:first').find('text').find('tspan').text();
		    	            		if (firstValue == '$0'){
		    	            			$('#'+id).find('.highcharts-data-labels:first').find('text').find('tspan').text('');
		    	            		}
		    	            	});
		    	            }
		    	        }  
            		},
			credits: { enabled: false },
			title: { text: '' },
			legend:  { enabled: false },
			exporting:{ buttons: {
                printButton:{ enabled:false },
                exportButton: { enabled:false }
              }
         	},
        	xAxis: { categories: ['Monthly Cost'] },
			colors: [ '#99CC00','#74B33C' ],
            yAxis: {min: 0, max: maxVal,  title: { text: '' }, gridLineWidth: 0, gridLineColor: '#ffffff', labels: { enabled: false },
            	stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                formatter: function() {
                	return formatCurrency(this.total);
                }
            }},
           
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br />'+
                        this.series.name +': <br />'+ formatCurrency(this.y);
                }
            },
            plotOptions: {
                column: {stacking: 'normal', borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        formatter: function() {
                        	return formatCurrency(this.y);
                        }
                    }
                }
            },
            series: [{
                name: 'Dependents', shadow: false,
                data: [dependentCost]
            },  {
                name: 'Employee', shadow: false,
                data: [EmployeeCost]
            }]
        });
		
		//for accessibility		
		var planName= $(".plan-"+barId.replace(/^\D+/g,'')).find("td.planNameWrap").text();  
		var HighChartSpan = $("#"+barId).find(".highcharts-stack-labels tspan");
		HighChartSpan.prepend("<span class='aria-hidden'>"+ planName +" Plan monthly costs you " + HighChartSpan.text() +", " + dependentCost + " for depentents and "+ EmployeeCost +"for employee</span>"); 
	}
	
	function drawEmployeeBar(barId, val,maxVal){
		  
		  chart = new Highcharts.Chart({
		    chart:   { 
		    			renderTo: barId, 
		    			type: 'column', 
		    			spacingBottom: 0, 
		    			marginBottom: 20, 
		    			marginTop: 5,
		    			
		    		},
		    credits: { enabled: false },
		    legend:  { enabled: false },
		    exporting:{ buttons: {
		                 printButton:{ enabled:false },
		                 exportButton: { enabled:false }
		               }
		          },
		    title: { text: '' },
		    xAxis: { categories: ['Employee Cost'] },
		    colors: ['#67c2c2'],
		    yAxis: { min: 0, max: maxVal, title: { text: '' }, gridLineWidth: 0, gridLineColor: '#ffffff', labels: { enabled: false } },
		    plotOptions: { 
		      column: { pointPadding: 0.5, borderWidth: 0, pointWidth: 56, colorByPoint: true, color: '#009999' }
		    },
		    tooltip: {
                formatter: function() {
                    return this.series.name +': <br />'+ formatCurrency(this.y);
                }
            },
		    series: [{name: 'EmployeeCost', shadow: false, color: '#009999', data: [val], dataLabels: {
		    enabled: true,
		    formatter: function() {
		       return formatCurrency(this.y);
		    }
		 }    }]
		  });
		  
			//for accessibility
		  var planName= $(".plan-"+barId.replace(/^\D+/g,'')).find("td.planNameWrap").text();   
		  var HighChartSpan = $("#"+barId).find(".highcharts-data-labels tspan");
		  HighChartSpan.prepend("<span class='aria-hidden'>"+ planName +" Plan monthly costs each employee " + HighChartSpan.text() + "</span>"); 
		}
	
	function formatCurrency(num) {  
		num = parseFloat(num).toFixed(0);
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '$' + num);
	}
</script>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>