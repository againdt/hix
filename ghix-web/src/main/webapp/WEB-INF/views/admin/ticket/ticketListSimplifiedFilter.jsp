<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page isELIgnored="false"%>
<style>
	  #ticketQueues .ui-selecting { background: #BBBBBB; }
  #ticketQueues .ui-selected { background: #3B7FC4; color: white; }
  #ticketQueues { list-style-type: none; }
  #ticketQueues li {width:auto;float:left;margin:5px 5px;padding:5px ;border-radius:5px;}
  
  .selected{background: #3B7FC4; color: white; }
    .unselected{background: #BBBBBB; color: black; }
</style>
<script>

$('document').ready(function(){
	var result = $( "#select-result" ).empty();
	$('.ui-widget-content').click(function(){
		var index =  $("#ticketQueues li").index(this) +1;
		var checkifSelected = $(this).hasClass("selected");
		if(checkifSelected){
			 $(this).removeClass('selected');
		}		  
		else{
			 $(this).addClass('selected');
		}
	});
	  
}); 
</script>

<div class="span12">
	<div class="control-group"> 
 
		
		<div class="span10">
			<ul id="ticketQueues" name="ticketQueues" >
		
				<c:forEach var="assigned" items="${ticketQueues}">
					<li class="ui-widget-content tktqueue" name="${assigned.name}"
						<c:if test="${searchCriteria.ticketQueues == assigned.name}"> selected="selected" </c:if>>${assigned.name}</li>
				</c:forEach>
				<li class="ui-widget-content tktArray" name="unClaimedTicket"><spring:message code="only_tktStatus.unclaimed"/></li>
				<li class="ui-widget-content tktArray"  name="assignedToMe"> <spring:message code="simplified_view.assignedToMe"/></li>
				<c:if test="${isArchivedAllowed == true }"> 
				 <li class="ui-widget-content tktArray"  name="archiveFlagValue"> <spring:message code="simplified_view.archivedTicket"/></li>
				</c:if>
				<li><input type="submit" class="btn btn-primary pull-right"
						value="Search" title="Click to Search"
						style="padding: 4px 58px; margin-right: -30px;"> </li>
			</ul></br><br>
		
		</div>     	
	</div>	
</div>

<!-- span2 margin20-lr -->

