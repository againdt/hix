$(function(){

	/* #########################################################################
	#########################################################################
	##########################PROVIDER SEARCH###################################
	#########################################################################
	######################################################################### */
	var providerSearchConfig = $('#providerSearchConfig').val();
	var prescriptionSearchConfig = $('#prescriptionSearchConfig').val();
	var benefitQuestionsConfig = $('#benefitQuestionsConfig').val();
	var currentPage = 1;

	var isBottomHit = false;
	var hasMoreProvider = true;
	var isProviderTermChanged = false;

	var prescriptionSearchRequest = [];

	var providerDataArray = [];
	var providerSearchType = 'doctor';
	var defaultProviderCount = 100;
	var totalSlides = 5;
	var currentSlide = 1;

	if($('#stateCode').val() === 'CT'){
		defaultProviderCount = 500;
	}

	findMaxBoxHeight();

	if(providerSearchConfig === 'OFF') {
		totalSlides--;
		$('#providerSearchItem').remove();
		$('#medicalServiceItem').addClass('active');

		if($('#stateCode').val() === 'ID') {
			$('#medicalServiceBack').hide();
		}

	}else {
		$('#providerSearchItem').addClass('active');
	}

	if(prescriptionSearchConfig === 'OFF') {
		totalSlides--;
		$('#prescriptionSearchItem').remove();
	}

	if(benefitQuestionsConfig === 'OFF') {
		totalSlides--;
		$('#benefitQuestionsItem').remove();

		if(prescriptionSearchConfig === 'ON') {
			$('#prescriptionSearchSubmit').show();
			$('#prescriptionSearchNext').hide();
		}else {
			$('#prescriptionSubmit').show();
			$('#prescriptionNext').hide();
		}
	}


	$('.totalSlides').text(totalSlides);

	$('a[data-slide=next]').click(function() {
		$('.currentSlide').text(++currentSlide);
	})

	$('a[data-slide=prev]').click(function() {
		$('.currentSlide').text(--currentSlide);
	})

	function capitalizeWord(type){
		var re = /(\b[a-z](?!\s))/g;
		var newType = type.toLowerCase().replace(re, function(x){return x.toUpperCase();});
		return newType;
	}


	/*#############################providerTypeDropdown start#############################*/
	$('#providerTypeDropdown li').on('click touchstart', function() {
		$('i.icon-check-sign').hide();

		var thisElement = $(this);
		thisElement.find('i.icon-check-sign').show();

		$('#providerTypeDropdown li .aria-hidden').text('Please press enter to select');
		thisElement.find('.aria-hidden').text('selected');

		$('#providerType').text(thisElement.find('.providerType').text());

		//$('input[name="providerType"]').val(thisElement.attr('data-itfem'));
		providerSearchType = thisElement.find('.providerType').attr('data-provider-search-type');
		setDefaultRadius(0);
		//$('#providerDistance').val('5');


		if(providerSearchType == 'doctor') {
			$('#providerName').attr('placeholder', plan_display['pd.label.providerName_1']).attr('aria-label', plan_display['pd.label.providerName_1']);
		}else  if(providerSearchType == 'dentist') {
			$('#providerName').attr('placeholder', plan_display['pd.label.providerName_2']).attr('aria-label', plan_display['pd.label.providerName_2']);
		}else{
			$('#providerName').attr('placeholder', plan_display['pd.label.providerName_3']).attr('aria-label', plan_display['pd.label.providerName_3']);
		}


	});
	/*#############################providerTypeDropdown end#############################*/

	 $("body").on("click keyup",function(){
	  		var providerListLength = $("#providerSelectedListUl li").length;
		    if(providerListLength >= 5){
		    	$("#providerName, #providerZipcode, #providerDistance, #providerTypeId").prop('disabled', true);
		    } else {
		    	$("#providerName, #providerZipcode, #providerDistance, #providerTypeId").prop('disabled', false);

		    };

	  });

	 $('form').bind("keypress", function(e) {
		if(e.keyCode == 13 && e.target.type !== 'submit') {
			e.preventDefault();
			return false;
		}
	});

	$("#providerName").autocomplete({
		minLength: 2,
		source: sourceFn,
		open: openFn,
		select: selectFn,
		focus: focusFn
	});

	if(providerSearchConfig === 'ON'){
		setDefaultRadius(0);
	}
	var zipcode_org = zipcode_new = $('#providerZipcode').val();
	$('#providerZipcode').change(function() {
		zipcode_new = $(this).val();
		if (zipcode_org != zipcode_new) {
			window.dataLayer.push({
				'includedZipCode': 'Yes'
			});
		}
		if($(this).val().length === 5){
			setDefaultRadius(0);
		}
	});
	if (zipcode_org == zipcode_new) {
		window.dataLayer.push({
			'includedZipCode': 'No'
		});
	}

	$(window).resize(findMaxBoxHeight);
	function setDefaultRadius(providerDistanceArrayIndex){
		var providerDistanceArray = [20,30,50,100];
		if ($('#stateCode').val() === 'ID'){
			providerDistanceArray = [100];
		}

		var requestData = {
				'searchKey': '',
				'userZip': $('#providerZipcode').val(),
				'currentPage': 1,
				'pageSize': 100,
				'otherDistance': providerDistanceArray[providerDistanceArrayIndex],
				'searchType': providerSearchType
		};

		$.ajax({
			url: '/hix/provider/doctors/search',
			dataType: 'json',
			data: requestData,
			async: false,
			success: function (responseText){
				if (responseText && responseText !== null) {
					if(responseText.providers && responseText.total_hits < defaultProviderCount && providerDistanceArrayIndex < (providerDistanceArray.length-1)){
						providerDistanceArrayIndex++;
						setDefaultRadius(providerDistanceArrayIndex);
					}else{
						$('#providerDistance').val(providerDistanceArray[providerDistanceArrayIndex]);
					}
				}
			}
		});
	}
	function sourceFn(request, response){
		//Removing special characters
		request.term = $('#providerName').val().replace(/[^a-zA-Z0-9'`./+~\s-]/gi, '');
		$('#providerName').val(request.term);

		var zipCode = $('#providerZipcode').val();
		var otherDistance = $('#providerDistance').val();

		if(request.term.length < 2){
			return false;
		}else{
			var requestData = {
				'searchKey': request.term,
				'userZip': zipCode,
				'currentPage': currentPage,
				'pageSize': 10,
				'otherDistance': otherDistance,
				'searchType': providerSearchType
			};

			$.ajax({
				url: '/hix/provider/doctors/search',
				dataType: 'json',
				data: requestData,
				async: false,
				success: function (responseText){
					if (responseText && responseText !== null) {
						isBottomHit = false;

						currentPage++;
						var providers = responseText.providers;
						//var data = [];
						var tempData = {};
						if(providers) {
							for(var i=0; i<providers.length;i++){
								var specialty = providers[i].specialty.replace(/[\[\]]/g,"");
								var prefix = '';
								var phone = ''
								var phoneFormat = '';

								if (providers[i].practicePhones != '') {
									var phoneString = providers[i].practicePhones[0].replace(/[\[\]]/g,"");

									phone = phoneString.substring(0,3) + '-' + phoneString.substring(3,6) + '-' + phoneString.substring(6,10);
									phoneFormat = '<br>' + phone;
								}

							var address = providers[i].providerAddress[0];
							var street = address.addline1 + (address.addline2 ? ' ' + address.addline2 : '');
							var providerName = capitalizeWord(providers[i].name.toLowerCase());

							var prefix = '';
					        if (providerSearchType === 'doctor' && providerName.indexOf('Dr.') !== 0 && $('#stateCode').val() === 'CA'){
					                prefix = plan_display['pd.label.doctor.title'] + ' ';
					        }
					        else {
					                prefix = '';
					        }
							//var prefix = providerSearchType === 'hospital' || 'dentist'? '' : plan_display['pd.label.doctor.title']+' ';
							var postfix = providerSearchType === 'dentist'? ' '+plan_display['pd.label.doctor.dmd']+' ' : '';


							tempData = {
								id: providers[i].strenuus_id,
								label: '<span>' + prefix + providerName + '</span><div class="ps-dropdown-list-style">'+ specialty + phoneFormat  + '<br>' + street + '<br>' + address.city + ' ' + address.state + ', ' + address.zip + '</div>',
								value: prefix + ' ' + providerName,
								name: providerName,
								networkId: providers[i].networkTierId ? providers[i].networkTierId.replace(/[\[\]]/g,"") : providers[i].networkTierId,
								networkTier: providers[i].networkTierId ? providers[i].networkTierId.replace(/[\[\]]/g,"") : providers[i].networkTierId,
								specialty: specialty,
								phone: phone,
								address: street,
								city: address.city,
								state: address.state,
								zip: address.zip,
								groupKey: providers[i].groupKey,
								providerType: providerSearchType
							};
							providerDataArray.push(tempData);
						}

						if(providers.length < 10){
							hasMoreProvider = false;

							if(providerDataArray.length === 0){
								tempData = {
									id : plan_display['pd.label.noResults'],
									label : plan_display['pd.label.noResultsFound'],
									value : plan_display['pd.label.noResultsFound']
								};
								$('body').append('<span class="aria-hidden" aria-live="rude">' + plan_display['pd.label.noResultsFound'] + '</span>');
								$('.supporting_err_msg').show();

								providerDataArray.push(tempData);
							}

						}

						response(providerDataArray);
					}
					}
					else {
						window.dataLayer.push ({
							'event': 'providersearchEvent',
							'eventCategory': 'Plan Selection - Provider Search',
							'eventAction': 'Provider Search',
							'eventLabel': 'Unsuccessful'
						});
						tempData = {
									id : plan_display['pd.label.noResults'],
									label : plan_display['pd.label.noResultsFound'],
									value : plan_display['pd.label.noResultsFound']
								};
						$('body').append('<span class="aria-hidden" aria-live="rude">' + plan_display['pd.label.noResultsFound'] + '</span>');
						$('.supporting_err_msg').show();
						providerDataArray.push(tempData);
						response(providerDataArray);
					}

				},
				error: function (xhr, ajaxOptions, thrownError){
					console.log(xhr);
				}
			});
		}
	}



	function openFn(){
		var acData = $(this).data('autocomplete');
        acData.term = acData.term.replace(/[^a-zA-Z0-9'`./+~\s-]/gi, '');
        var reg = new RegExp(acData.term, 'gi');

		acData.menu.element.find('a').each(function(){
			var me = $(this);
			var text =me.text();
			var data =text.substring(text.indexOf(">")+1, text.indexOf("</span>"));
			var repdata = data.replace(reg, function(str) {return '<strong>'+str+'</strong>';});
			me.html(me.text().replace(data, repdata));
        });

	}


	function selectFn(event, ui){
		if(ui.item.id === plan_display['pd.label.noResults']){
	        ui.item.value='';
        }else{
        	if(document.getElementById(ui.item.id)==null){
        		var providerData = {
        			id: ui.item.id,
        			name: ui.item.name,
	          		networkId: ui.item.networkId,
	          		networkTier: ui.item.networkTier,
	          		specialty: ui.item.specialty,
	          		address: ui.item.address,
	          		city: ui.item.city,
	          		state: ui.item.state,
	          		phone: ui.item.phone,
	          		zip: ui.item.zip,
	          		groupKey: ui.item.groupKey,
	          		providerType: ui.item.providerType
	          	};

	          	selectProvider(providerData);
	          	findMaxBoxHeight();
	        }

        	ui.item.value='';
        }
	}

	function focusFn(event, ui){
		if (ui.item && ui.item.value === plan_display['pd.label.noResultsFound']) {
			return false;
		}
	}

	//add provider
	function selectProvider(newProviderData){
		var providersJson = $('#providersJson').val();
		var existingProvidersJson= [];
		var prefix = '';
        if (providerSearchType === 'doctor' && newProviderData.name.indexOf('Dr') !== 0 && $('#stateCode').val() === 'CA'){
                prefix = plan_display['pd.label.doctor.title'];
        }
        else {
                prefix = '';
        }
		var postfix = providerSearchType === 'dentist'? ' '+plan_display['pd.label.doctor.dmd'] : '';

		if(providersJson!== ''){
			existingProvidersJson = JSON.parse(providersJson);
		}

		//update provider hidden input
		existingProvidersJson.push(newProviderData);
		$('#providersJson').val(JSON.stringify(existingProvidersJson));

		if(newProviderData.providerType === 'doctor'){
			newProviderData.providerType = plan_display['pd.label.preferences.providerDoctor'];
		}else if(newProviderData.providerType === 'dentist'){
			newProviderData.providerType = plan_display['pd.label.preferences.providerDentist'];
		}else{
			newProviderData.providerType = plan_display['pd.label.preferences.providerHospital'];
		}

		if(newProviderData.address !== undefined || newProviderData.city !== undefined || newProviderData.state !== undefined || newProviderData.zip !== undefined){

		// } else {
		//update provider selected HTML
			var providerSelectedHtml = "<li id=" + newProviderData.id + " class='search-choice'>"
					+ "<div class='chzn-header'><span class='aria-hidden'>selected</span>"+newProviderData.providerType+"<a class='search-choice-close' href='javascript:void(0)' aria-label='" + plan_display['pd.label.preferences.remove'] + " " + newProviderData.providerType + newProviderData.name + "'>x</a></div>"
					+ "<div class='chzn-details'><span class='aria-hidden'>Name</span><b>" + prefix + " <span class='txt-capitalize'>"+newProviderData.name+"</span>"+ "</b><br><span class='aria-hidden'>Specialty</span>"
					+ newProviderData.specialty+ "<br>";

			if(newProviderData.phone){
				providerSelectedHtml +="<span class='aria-hidden'>Phone number</span>" + newProviderData.phone + "<br>"
			}

			providerSelectedHtml += "<span class='aria-hidden'>Address</span>"+ newProviderData.address + "<br>" + newProviderData.city + ", " + newProviderData.state + " " + newProviderData.zip + "</div></li>";

			if($('#providerSelectedList ul li').length == 4){
				providerSelectedHtml += '<span class="aria-hidden">You already added 5 providers.</span>'
			}

			$("#providerSelectedList ul").append(providerSelectedHtml);

			if($('#providerSelectedList ul li').length == 5){
				$('#providerSelectedList li:first').focus();
			}
		}
	}


	//remove selected provider
	$('#providerSelectedList').on('click', '.search-choice-close', function (event) {
		clearData();
		var providerId = $(event.currentTarget).closest('li').attr('id');
		//remove from UI
		$('#'+providerId).remove();

		//remove from hidden input
		var existingProvidersJson = JSON.parse($('#providersJson').val());
		var newProvidersJson = [];

	    for (var i = 0; i < existingProvidersJson.length; i++) {
	      if (parseInt(existingProvidersJson[i].id) !== parseInt(providerId))
	    	  newProvidersJson.push(existingProvidersJson[i]);
	    }

	    $('#providersJson').val(JSON.stringify(newProvidersJson));

	    setTimeout(function(){
	    	$('#providerName').focus();
	    }, 100);
	});

	$('#ui-id-1').scroll(function () {

		if(isProviderTermChanged ===true){
			$("#ui-id-1").animate({
				scrollTop: 0
			}, 0);

			isProviderTermChanged = false;
			return;
		}

		if ($(this)[0].scrollHeight - $(this).scrollTop() <= $(this).outerHeight() && isBottomHit === false && hasMoreProvider === true) {
			// You can perform as you want
			isBottomHit = true;

		    $("#providerName").autocomplete('search');
		}
	});

	$('#providerName').on('input blur', function(){
		isProviderTermChanged = true;
		clearData();
	});

	function clearData(){
		currentPage = 1;
		providerDataArray = [];
		$('.supporting_err_msg').hide();
		$('#ui-id-1').empty();

		hasMoreProvider = true;

	}

	function findMaxBoxHeight() {
		var highestbox = null;
		var ht = 0;
		$('.search-choice').each(function(){
				var h = $(this).find(".chzn-details").height();
	  		if(h > ht){
	    		ht = h;
	    		highestTile = $(this);
	  		}
		});
		ht = ht + 49;
		$('.search-choice').css('height',ht);
	}


	/* #########################################################################
	#########################################################################
	##########################PROVIDER SEARCH END##############################
	#########################################################################
	######################################################################### */


	/* #########################################################################
	#########################################################################
	##########################PRESCRIPTION SEARCH START##############################
	#########################################################################
	######################################################################### */

	if($('#prescriptionSearchRequest').val() !== ''){
		prescriptionSearchRequest = JSON.parse($('#prescriptionSearchRequest').val());
		addSelectedDrugUI(prescriptionSearchRequest);
	}



	$("#drugName").autocomplete({
		minLength: 2,
		source: prescriptionSourceFn,
		select: prescriptionSelectFn
	});

	function prescriptionSourceFn(request, response){

		var drugName = $('#drugName').val();
		if(drugName.length >= 2){
			$.ajax({
				url: '/hix/private/searchDrugByName?term=' + drugName,
				success: function (res){
					$('#searchLoading').hide();
					var drugs = JSON.parse(res).drugs;

					if(drugs){
						drugs.forEach(function(drug){
							drug.drugName = capitalizeWord(drug.drugName);
							drug.label = drug.drugName;
						})
					}else{
						drugs =[{
							label : plan_display['pd.label.noResultsFoundDrugs'],
							value : ''
						}];
						$('body').append('<span class="aria-hidden" aria-live="rude">' + plan_display['pd.label.noResultsFoundDrugs'] + '</span>');
					}
					response(drugs);
				},
				error: function (xhr, status, error){
					$('#searchLoading').hide();
					console.log('error');
				}
			})
		}
	}

	function prescriptionSelectFn(event, ui){
		if(ui.item.value === '') return;

		$.ajax({
			url: '/hix/private/searchByDrugId?drugId=' + encodeURIComponent(ui.item.drugID),
			success: function (res){
				if(res === '{}'){
					$('#drugDosagesContainer').html('<p class="alert alert-error inline-block" role="alert">' + plan_display['pd.label.noResultsFound'] + '.</p>');
					return;
				}

				var drugDosages = JSON.parse(res).drugDosages;

				var html = '<div id="drugDosages" automation-id="drug-dosages" class="margin10-b">'
						+ '<div class="drugDosagesHeader">'+plan_display['pd.label.preferences.availableAs']+' <a id="removeDrugDosages" automation-id="drug-dosages-remove" href="javascript:void(0)" class="pull-right" aria-label="' + plan_display['pd.label.preferences.removeDosageFor'] + ' ' + ui.item.value + '">'
						+ '<span aria-hidden="true">x</span></a></div><div class="gutter10">';

				drugDosages.forEach(function(drugDosage){
					drugDosage.drugDosageName = capitalizeWord(drugDosage.drugDosageName);
					drugDosage.drugDosage = drugDosage.drugDosageName;
					drugDosage.drugRxCode = drugDosage.drugDosageID;
					if(drugDosage.genericDosageID != null && drugDosage.genericDosageID != ""){
						drugDosage.genericID = drugDosage.genericDosageName;
						drugDosage.genericRxCode = drugDosage.genericDosageID;
						drugDosage.genericName = capitalizeWord(drugDosage.genericDosageName);
						drugDosage.genericDosage = capitalizeWord(drugDosage.genericDosageName);
				    }

					$.extend(ui.item, drugDosage);

					html += '<label class="radio margin10-b"><input type="radio" name="drugDosage" value="' + encodeURIComponent(JSON.stringify(ui.item)) + '">' + drugDosage.drugDosage + ' <small class="drugDosageDay">('+plan_display['pd.label.preferences.thirtyDaySupply']+')</small></label>';
				})

				html += '<button class="btn btn-primary" id="dosageSelection" automation-id="drug-dosage-selection">' + plan_display['pd.label.preferences.selectDosage'] + '</button><div class="error help-inline" id="drugDosages_error"></div></div></div>';


				$('#drugDosagesContainer').html(html);
			},
			error: function (xhr, status, error){
				console.log('error');
			}
		})
	}


	$('body').on('click','#dosageSelection',function(event){
		event.preventDefault();

		var alreadySelected = false;
		var selectedDrug = $('[name=drugDosage]:checked').val();

		if(selectedDrug === undefined){
			return;
		}

		selectedDrug = JSON.parse(decodeURIComponent(selectedDrug));

		//clear value
		$('#drugName').val('');

		for(var i=0; i<prescriptionSearchRequest.length; i++ ){
			if(prescriptionSearchRequest[i].drugID === selectedDrug.drugID){
				alreadySelected = true;
				break;
			}
		}

		if(alreadySelected){
			$('#drugDosagesContainer').html('<p class="alert alert-error inline-block">' + plan_display['pd.label.preferences.drugAlreadySelected'] + '</p>');
			$('#drugName').focus();
			return;
		}


		$("#drugDosagesContainer").html('');
		$('#ariaDrugDosagesContainer').html('');
		addSelectedDrugUI(selectedDrug);

	});

	function addSelectedDrugUI(item){
		var selectedDrugs = [];

		if(Array.isArray(item)){
			selectedDrugs = item;
		}else{
			if(item.genericRxCode == null){
				$.ajax({
					url: '/hix/private/genericRxcui?name=' + item.drugName + '&strength=' + escape(item.strength),
					async: false,
					success: function (res){
					  if(res != null && res != ""){
						var genericData = JSON.parse(res);
						$.extend(item, genericData);
						item.genericID = item.genericName;
						item.genericRxCode = item.genericRxcui;

						if(item.genericName){
							item.genericName = capitalizeWord(item.genericName);
						}

						if(item.genericDosage){
							item.genericDosage = capitalizeWord(item.genericDosage);
						}
					  }
					},
					error: function (xhr, status, error){
						console.log('error');
					}
				})
			}
			prescriptionSearchRequest.push(item);
			$('#prescriptionSearchRequest').val(JSON.stringify(prescriptionSearchRequest));

			selectedDrugs.push(item);
		}

		selectedDrugs.forEach(function(selectedDrug){
			var html = '<span class="aria-hidden">selected' + selectedDrug.drugDosage + '</span>';
			html += '<div class="selectedDrug span6" automation-id="' + selectedDrug.drugID + '" id="' + selectedDrug.drugID + '">'
				+ '<div class="selectedDrugHeader"><a class="pull-right removeSelectedDrug" automation-id="drug-dosage-selected-remove" href="javascript:void(0)" aria-label="' + plan_display['pd.label.preferences.remove'] + ' ' + selectedDrug.drugDosage + '">'
				+ '<span aria-hidden="true">x</span></a>'+ selectedDrug.drugDosage +'</div>';

			if(selectedDrug.genericDosage){
				html += '<div class="selectedDrugGenericName">' + selectedDrug.genericName + ' (' + plan_display['pd.label.preferences.genericName'] + ')</div>' ;
			}

			html += '</div>';

			if(prescriptionSearchRequest.length >= 5){
				html += '<span class="aria-hidden">' + plan_display['pd.label.preferences.fiveDosageAdded'] + '</span>';
			}

			$('#selectedDrugsContainer').append(html);
		})


		if(prescriptionSearchRequest.length >= 5){
			$('#drugName').prop('disabled', true);
			$('#selectedDrugsContainer .selectedDrug:first').focus();
		}else{
			$('#drugName').focus();
		}
	}


	$("#drugDosagesContainer").on('click', '#removeDrugDosages', function(){
		$('#ariaDrugDosagesContainer').html('');
		$("#drugDosagesContainer").html('');
		$('#drugName').val('').focus();
		$('body').append("<span class='aria-hidden' role='alert'>" + plan_display['pd.label.preferences.dosageSelectionRemoved'] + "</span>");
	})

	$("#selectedDrugsContainer").on('click', '.removeSelectedDrug', function(){
		var selectedDrug = $(this).closest('.selectedDrug');

		prescriptionSearchRequest = prescriptionSearchRequest.filter(function(drug){
			return selectedDrug.attr('id') != drug.drugID
		})

		$('#prescriptionSearchRequest').val(JSON.stringify(prescriptionSearchRequest));
		selectedDrug.remove();

		$('#drugName').prop('disabled', false);
		$('#drugName').focus();
	})


//	$('#preferencesForm').validate({
//		rules: {
//			drugDosage: 'required'
//		},
//		messages: {
//			drugDosage: '<span> <em class="excl">!</em>You must select a prescription dosage</span>'
//		},
//		onkeyup : false,
//		errorClass : "error",
//		errorPlacement : function(error, element) {
//			error.appendTo($("#drugDosages_error"));
//		}
//	});
	/* #########################################################################
	#########################################################################
	##########################PRESCRIPTION SEARCH END##############################
	#########################################################################
	######################################################################### */

//	$('body').tooltip({
//		selector : '[rel=tooltip]',
//		trigger: 'manual'
//	})

	$('[rel=tooltip]').tooltip({
		trigger: 'manual'
	}).focus(function(){
		$(this).tooltip('show');
	}).blur(function(){
		$(this).tooltip('hide');
	}).hover(function(){
		$(this).tooltip('show');
	}, function(){
		$(this).tooltip('hide');
	});


	$('.reset').click(function(event) {
		event.preventDefault();

		$(':input', '#preferencesForm').not(
				':button, :submit, :reset').removeAttr(
				'checked').removeAttr('selected');
		$('.childrens-dental').hide();

		if($('#stateCode').val() === 'CT' || $('#stateCode').val() === 'MN'){
			$('#threemonths').prop('checked', true);
			$('#onetofour').prop('checked', true);
		}
		/*provider search*/
		$('#providerName').val('');
		$('#providerDistance').val('5');
		if($('#stateCode').val() === 'CT'){
			$('#providerDistance').val('20');
		}
		//$('#providerZipcode').val('');
		$('#providerSelectedListUl').html('');
		$('#providersJson').val('');

		/*drug search*/
		$('#selectedDrugsContainer, #drugDosagesContainer').html('');
		$('#prescriptionSearchRequest, #drugName').val('');
		prescriptionSearchRequest = [];
		$('#drugName').prop('disabled', false);

	});
	/*carousel */

	var selectedBenefits = [];
	$('.benefits input:checkbox').change(function() {
		if ($('input:checkbox:checked').length > 4) {
			if($('input:checkbox:checked').prop( "checked")) {
				selectedBenefits.push($(this).data("filter-label"));
			}
			$('.compare').off("change");
			$(this).prop('checked', false);
			$('.benefits-error').slideDown().fadeIn();
			setTimeout(function() {
				$('.benefits-error').slideUp().fadeOut();
			}, 3000);
		} else if ($('input:checkbox:checked').length < 4) {
			if($('input:checkbox:checked').prop( "checked")) {
				selectedBenefits.push($(this).data("filter-label"));
			}
			$('.benefits-error').hide();
		}
		if(selectedBenefits.length > 1) {
			selectedBenefits.join(" , ");
		}
	});


	$('.skip-to-view').on('click', function(){
		var googleTrackingPage = $('.item.active').attr('data-google-tracking-page');
	})
	$('#benefitSubmit').on('click', function(){
		window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Benefits', 'eventAction': $.trim($(this).text()), 'eventLabel': selectedBenefits});
	});

	$('#providerSearchNext').on('click', function(){
		var providersJson = $('#providersJson').val();
		var numberOfDoctors = 0, numberOfHospitals = 0, numberOfDentists = 0;

		if(providersJson){
			providersJson = JSON.parse(providersJson);

			providersJson.forEach(function(provider){
				if(provider.providerType === 'doctor'){
					numberOfDoctors++;
				}else if(provider.providerType === 'hospital'){
					numberOfHospitals++;
				}else{
					numberOfDentists++;
				}
			});
		}
		var selectedProviders = 'Doctor | ' + numberOfDoctors + ', Dentists | ' + numberOfDentists + ', Hospitals | ' + numberOfHospitals;
		//window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Provider Search', 'eventAction': 'Navigational Button Click', 'eventLabel': $(this).text()});
		window.dataLayer.push({'event': 'providersearchEvent', 'eventCategory': 'Plan Selection - Provider Search ', 'eventAction': $.trim($(this).text()), 'eventLabel': selectedProviders});
	})
	$('#medicalServiceBack, #medicalUsageNext').on('click', function(){
		window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Medical Service', 'eventAction': 'Navigational Button Click', 'eventLabel': $.trim($(this).text())});
	})
	$('.gtm_prescription_drug_back').on('click', function(){
		window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Drug', 'eventAction': 'Navigational Button Click', 'eventLabel': $.trim($(this).text())});
	})
	$('#prescriptionNext , #prescriptionSubmit').on('click', function(){
		window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Drug', 'eventAction': 'Navigational Button Click', 'eventLabel': $.trim($(this).text())});
	})
	$('#prescriptionSearchSubmit, #prescriptionSearchNext').on('click', function(){
		var prescriptionSearchJson = $('#prescriptionSearchRequest').val();
		var numberOfDrugs = 0;
		if(prescriptionSearchJson){
			prescriptionSearchJson = JSON.parse(prescriptionSearchJson);
			numberOfDrugs = prescriptionSearchJson.length;
		}
		window.dataLayer.push({'event': 'prescriptionsearchEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Search ', 'eventAction': 'Navigational Button Click', 'eventLabel': 'Prescription |' + numberOfDrugs});
	})


//	$('.slideJump').on('click', function(e){
//
//		if($(this).hasClass('toSlide0')){
//			$('#preferenceSection').text('for medical service');
//		}else if($(this).hasClass('toSlide1')){
//			$('#preferenceSection').text('for prescription drug');
//		}else if($(this).hasClass('toSlide2')){
//			$('#preferenceSection').text('for provider search');
//		}else if($(this).hasClass('toSlide3')){
//			$('#preferenceSection').text('for prescription search');
//		}
//		$('#preferenceHeader').focus();
//	})

});
