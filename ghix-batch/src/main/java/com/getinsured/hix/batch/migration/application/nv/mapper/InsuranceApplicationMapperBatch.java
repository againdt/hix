package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.indportal.dto.dm.application.ApplicationDTO;
import com.getinsured.hix.indportal.dto.dm.application.AttestationApplication;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactPreferenceCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.GYear;



@Component(value="insuranceApplicationMapperBatch")
public class InsuranceApplicationMapperBatch {
	private static final String SBM = "SBM";
	private static final String CASEID = "HouseholdCaseId";
	@Autowired private InsuranceApplicantMapperBatch insuranceApplicantMapper;
	
	public InsuranceApplicationType mapInsuranceApplication(ApplicationDTO applicationDTO,Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas, Map<String, InsuranceApplicantType> applicantsByMemberId) throws  GIException{
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType insuranceApplicationType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicationType();

		/*com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType appIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
		appIdentificationType.setIdentificationID(AccountTransferUtil.createString(Integer.toString(applicationDTO.getResult().getInsuranceApplicationIdentifier())));
		// Set the application identification category as "SBM (State Based Marketplace)"
		appIdentificationType.setIdentificationCategoryText(AccountTransferUtil.createTextType(SBM));
		insuranceApplicationType.getApplicationIdentification().add(appIdentificationType);*/
		
		com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType appIdentificationTypeCaseId = AccountTransferUtil.niemCoreFactory.createIdentificationType();
		appIdentificationTypeCaseId.setIdentificationID(AccountTransferUtil.createString(applicationDTO.getResult().getInsuranceApplicationIdentifier().toString()));
		// Set the application identification category as "SBM (State Based Marketplace)"
		appIdentificationTypeCaseId.setIdentificationCategoryText(AccountTransferUtil.createTextType(CASEID));
		insuranceApplicationType.getApplicationIdentification().add(appIdentificationTypeCaseId);

		//--- Application Creation Starts ---
    	com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ActivityType applicationCreationActivity= AccountTransferUtil.niemCoreFactory.createActivityType();
    	DateType applicationCreationDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		Date applicationCreationDate = AccountTransferUtil.basicFactory.createDate();
		applicationCreationDate.setValue(AccountTransferUtil.getSystemDate());
		applicationCreationDateType.setDate(applicationCreationDate);
		applicationCreationActivity.setActivityDate(applicationCreationDateType);
    	insuranceApplicationType.setApplicationCreation(applicationCreationActivity);
    	//--- Application Creation Ends ---
    	
    	//--- Application Submission Starts ---
    	com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ActivityType submisionActivity= AccountTransferUtil.niemCoreFactory.createActivityType();
		DateType applicationSubmisstionDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		//gov.niem.niem.proxy.xsd._2.Date submisstionDate = basicFactory.createDate();
		
		applicationSubmisstionDateType.setDate(applicationCreationDate);
		submisionActivity.setActivityDate(applicationSubmisstionDateType);
		insuranceApplicationType.setApplicationSubmission(submisionActivity);	
    	//--- Application Submission Ends ---			

		//--- Application Update Starts ---
    	com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ActivityType applicationUpdateActivity= AccountTransferUtil.niemCoreFactory.createActivityType();
    	DateType applicationUpdateDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		Date applicationUpdateDate = AccountTransferUtil.basicFactory.createDate();
		applicationUpdateDate.setValue(AccountTransferUtil.getSystemDate());
		applicationUpdateDateType.setDate(applicationUpdateDate);
		applicationUpdateActivity.setActivityDate(applicationUpdateDateType);
		if(insuranceApplicationType.getApplicationUpdate() != null)
		{
			insuranceApplicationType.getApplicationUpdate().add(applicationCreationActivity);
		}
    	//--- Application Update Ends ---
		
		// applyingForFinancialAssistanceIndicator
		insuranceApplicationType.setInsuranceApplicationRequestingFinancialAssistanceIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getRequestingFinancialAssistanceIndicator()));
		for (Entry<String, AttestationsMember> entryMember : applicationDTO.getResult().getAttestations().getMembers().entrySet()) {
			boolean isPrimary = false;
			String contactPersonId = AccountTransferUtil.getPersonTrackingNumber(applicationDTO.getResult().getAttestations().getApplication().getContactMemberIdentifier(), applicationDTO.getResult().getComputed().getMembers());
			String personId = AccountTransferUtil.getPersonTrackingNumber(entryMember.getKey(), applicationDTO.getResult().getComputed().getMembers());
			if (personId.equals(contactPersonId)) {
				isPrimary = true;
			}
			InsuranceApplicantType applicant = insuranceApplicantMapper.createInsuranceApplicants(entryMember.getValue(), AccountTransferUtil.getPersonTrackingNumber(entryMember.getKey(),applicationDTO.getResult().getComputed().getMembers()), verificationMetadatas,applicationDTO.getResult().getComputed().getMembers().get(entryMember.getKey()),applicationDTO.getResult().getAttestations(),applicationDTO.getResult().getComputed().getTaxHouseholds(),isPrimary);
			if(applicant != null)
			{
			applicantsByMemberId.put(entryMember.getKey(), applicant);
			insuranceApplicationType.getInsuranceApplicant().add(applicant);
			}
		}
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType applicationExtensionType = AccountTransferUtil.insuranceApplicationObjFactory.createApplicationExtensionType();
		GYear gyear = AccountTransferUtil.basicFactory.createGYear();
		XMLGregorianCalendar coverageYear = AccountTransferUtil.getSystemDateYear();
		coverageYear.setYear(2019);
		gyear.setValue(coverageYear);
		
		applicationExtensionType.setCoverageYear(gyear);
		insuranceApplicationType.setApplicationExtension(applicationExtensionType);

		insuranceApplicationType.setSSFPrimaryContact(this.addSSFPrimaryContactType(applicationDTO.getResult().getAttestations().getApplication()));
		insuranceApplicationType.setSSFSigner(this.createSSFSignerType(applicationDTO));
		// InsuranceApplicationRequestingMedicaidIndicator
		insuranceApplicationType.setInsuranceApplicationRequestingMedicaidIndicator(AccountTransferUtil.addBoolean(false));
		//returnObj.setInsuranceApplication(insuranceApplicationType);
		
		if(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations() != null)
		{
			insuranceApplicationType.setInsuranceApplicationTaxReturnAccessIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getRenewalAgreementIndicator()));
			
			if(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getRenewEligibilityYearQuantity() != null) {
				QuantityType coverageRenewalYearQuantity =AccountTransferUtil.niemCoreFactory.createQuantityType();
				coverageRenewalYearQuantity.setValue(new BigDecimal(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getRenewEligibilityYearQuantity()));
				insuranceApplicationType.setInsuranceApplicationCoverageRenewalYearQuantity(coverageRenewalYearQuantity);
			}else {
				QuantityType coverageRenewalYearQuantity =AccountTransferUtil.niemCoreFactory.createQuantityType();
				coverageRenewalYearQuantity.setValue(BigDecimal.ZERO);
				insuranceApplicationType.setInsuranceApplicationCoverageRenewalYearQuantity(coverageRenewalYearQuantity);
			}
		}
		return insuranceApplicationType;
		
	}
	

	private com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFSignerType createSSFSignerType(ApplicationDTO applicationDTO) {
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFSignerType sSFSigner = AccountTransferUtil.insuranceApplicationObjFactory.createSSFSignerType();
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.SignatureType signatureType = AccountTransferUtil.hixCoreFactory.createSignatureType();
		
		com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		if(!CollectionUtils.isEmpty(applicationDTO.getResult().getAttestations().getApplication().getApplicationSignatures()) && applicationDTO.getResult().getAttestations().getApplication().getApplicationSignatures().size()>0)
		{
			sigDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(applicationDTO.getResult().getAttestations().getApplication().getApplicationSignatures().get(0).getApplicationSignatureDate()));
		}
		else
		{
			sigDate.setValue(AccountTransferUtil.getSystemDate());
		}
		sigDateType.setDate(sigDate);
		signatureType.setSignatureDate(sigDateType);
		sSFSigner.setSignature(signatureType);
		if(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations() != null  )
		{
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFAttestationType sSFAttestation = AccountTransferUtil.insuranceApplicationObjFactory.createSSFAttestationType();
		//TO:DO - we need to check this value in DB = setSSFAttestationNonPerjuryIndicator, setSSFAttestationCollectionsAgreementIndicator
		sSFAttestation.setSSFAttestationCollectionsAgreementIndicator(AccountTransferUtil.addBoolean(true));
		
		sSFAttestation.setSSFAttestationMedicaidObligationsIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getMedicaidRequirementAgreementIndicator()));
		sSFAttestation.setSSFAttestationNonPerjuryIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getPenaltyOfPerjuryAgreementIndicator()));
		if(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getNonIncarcerationAgreementIndicator() != null)
		{
			sSFAttestation.getSSFAttestationNotIncarceratedIndicator().add(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getNonIncarcerationAgreementIndicator()));
		}
		else
		{
			sSFAttestation.getSSFAttestationNotIncarceratedIndicator().add(AccountTransferUtil.addBoolean(true));
		}
		sSFAttestation.setSSFAttestationApplicationTermsIndicator(AccountTransferUtil.addBoolean(false));
		sSFAttestation.setSSFAttestationPendingChargesIndicator(AccountTransferUtil.addBoolean(false));
		sSFAttestation.setSSFAttestationInformationChangesIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getChangeInformationAgreementIndicator()));
		sSFSigner.setSSFAttestation(sSFAttestation);
		}
		return sSFSigner;
	}
	
	private com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFPrimaryContactType addSSFPrimaryContactType(AttestationApplication application){
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFPrimaryContactType sSFPrimaryContactType = AccountTransferUtil.insuranceApplicationObjFactory.createSSFPrimaryContactType();
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactPreferenceCodeType contactPreferenceCodeType = AccountTransferUtil.hixTypeFactory.createContactPreferenceCodeType();

			if (application.getContactMethod() != null && application.getContactMethod().size() >0 && application.getContactMethod().contains("EMAIL")) {
				contactPreferenceCodeType.setValue(ContactPreferenceCodeSimpleType.fromValue("Email"));
			} else {
				contactPreferenceCodeType.setValue(ContactPreferenceCodeSimpleType.fromValue("Mail"));
			}
			sSFPrimaryContactType.setSSFPrimaryContactPreferenceCode(contactPreferenceCodeType);
		return sSFPrimaryContactType;
	} 
}
