<%@page import="org.apache.commons.dbcp.BasicDataSource"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page import = "java.io.OutputStream" %> 
<%@ page import="com.getinsured.hix.platform.ecm.*"%>
<%@include file="datasource.jsp" %>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
 <link rel="stylesheet" href="/resources/demos/style.css" />
 
 <style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

th.sortable a {
	background-image: url(../img/arrow_off.png);
}

th.order1 a {
	background-image: url(../img/arrow_down.png);
}

th.order2 a {
	background-image: url(../img/arrow_up.png);
}

tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.excel {
	background-image: url(../img/ico_file_excel.png);
}

span.csv {
	background-image: url(../img/ico_file_csv.png);
}

span.xml {
	background-image: url(../img/ico_file_xml.png);
}

span.pdf {
	background-image: url(../img/ico_file_pdf.png);
}

span.rtf {
	background-image: url(../img/ico_file_rtf.png);
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}

</style>
</head>

<body>
Content of Request / Response, Attachments Content
</body>

</html>
 

<c:if test="${param.request != ''}">
<sql:query dataSource="${jspDataSource}" var="result1">
select f.FINAL_FACTS_MAP, f.FINAL_STATUS_MAP, f.ACTION_ID_LIST, f.FINAL_STATUS, 
to_char(f.START_TIME,'DD-MON-YY HH24:MM:SS') as START_TIME, 
to_char(f.END_TIME,'DD-MON-YY HH24:MM:SS') as END_TIME,
t.ACTION_ID,t.ACTION_NAME,t.ACTION_STATUS,t.ACTION_ERROR_CODE,t.ACTION_EXCEPTION,
to_char(t.START_TIME,'DD-MON-YY HH24:MM:SS') as A_START_TIME,
t.RETRY_COUNT,
to_char(t.END_TIME,'DD-MON-YY HH24:MM:SS') as A_END_TIME
from SSAP_ERP_ACTION_MAPPER_FACTS f, SSAP_ERP_ACTION_MAPPER_tasks t
where f.id=t.ssap_erp_am_facts_id and f.ssap_application_id='<%=request.getParameter("request")%>' 
order by t.ACTION_ID
</sql:query>

<display:table name="${result1.rows}" requestURI="/admin/factsBottomRight" id="table1" style="white-space: pre-wrap;" export="false" pagesize="10">
	<display:column property="FINAL_FACTS_MAP" title="FINAL FACTS MAP" sortable="false" />
	<display:column property="FINAL_STATUS_MAP" title="FINAL STATUS MAP" sortable="false" />
	<display:column property="ACTION_ID_LIST" title="ACTION ID LIST" sortable="false" />
	<display:column property="FINAL_STATUS" title="FINAL STATUS" sortable="false" />
	<display:column property="START_TIME" title="START TIME" sortable="false" />
	<display:column property="END_TIME" title="END TIME" sortable="false" />
	<display:column property="ACTION_ID" title="ACTION ID" sortable="false" />
	<display:column property="ACTION_NAME" title="ACTION NAME" sortable="false" />
	<display:column property="ACTION_STATUS" title="ACTION STATUS" sortable="false" />
	<display:column property="ACTION_ERROR_CODE" title="ACTION ERROR_CODE" sortable="false" />
	<display:column property="ACTION_EXCEPTION" title="ACTION EXCEPTION" sortable="false" />
	<display:column property="A_START_TIME" title="ACTION START_TIME" sortable="false" />
	<display:column property="A_END_TIME" title="ACTION END TIME" sortable="false" />
</display:table>
</c:if>
<c:if test="${param.request == ''}">
	<br/>No Data in Application Id
</c:if>

