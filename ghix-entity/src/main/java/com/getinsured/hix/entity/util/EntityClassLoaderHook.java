package com.getinsured.hix.entity.util;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.getinsured.hix.platform.util.ModuleAwareClassLoader;
import com.getinsured.hix.platform.util.ModuleContextProvider;

public class EntityClassLoaderHook implements ApplicationContextAware {
	private Logger logger = LoggerFactory.getLogger(EntityClassLoaderHook.class);
	@Autowired
	private ModuleAwareClassLoader wsplatformClassLoader;
	@Autowired
	private ModuleContextProvider moduleContextProvider;
	private ApplicationContext appContext;

	@PostConstruct
	public void register() {
		logger.info("Registering Entity Class loader with Platform");
		wsplatformClassLoader.registerClassLoader(this.getClass().getClassLoader(), "Entity Class Loader".intern());
		moduleContextProvider.setModuleContext(appContext);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.appContext = context;

	}
}
