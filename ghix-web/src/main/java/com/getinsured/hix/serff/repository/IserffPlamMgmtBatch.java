/**
 * 
 */
package com.getinsured.hix.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.BATCH_STATUS;

/**
 * Interface is used provide JpaRepository of table SERFF_PLAN_MGMT_BATCH.
 * @author Vani Sharma
 *
 */
@Repository
public interface IserffPlamMgmtBatch extends JpaRepository< SerffPlanMgmtBatch, Long> {
    @Query("SELECT b FROM SerffPlanMgmtBatch b WHERE b.batchStatus=:batchStatus and b.isDeleted='N' and rownum<=1 ")
    SerffPlanMgmtBatch getPlanByBatchStatus(@Param("batchStatus") BATCH_STATUS batchStatus);


	SerffPlanMgmtBatch findById(long id);
}
