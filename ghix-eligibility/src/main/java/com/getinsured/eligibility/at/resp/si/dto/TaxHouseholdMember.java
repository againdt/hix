package com.getinsured.eligibility.at.resp.si.dto;

import java.util.Map;

public class TaxHouseholdMember {

	private PersonInfo personInfo;

	private Map<String, Verification> verificationMap;

	private Map<String, EligibilityProgram> eligibilityProgramMap;

	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	public Map<String, EligibilityProgram> getEligibilityProgramMap() {
		return eligibilityProgramMap;
	}

	public void setEligibilityProgramMap(Map<String, EligibilityProgram> eligibilityProgramMap) {
		this.eligibilityProgramMap = eligibilityProgramMap;
	}

	public Map<String, Verification> getVerificationMap() {
		return verificationMap;
	}

	public void setVerificationMap(Map<String, Verification> verificationMap) {
		this.verificationMap = verificationMap;
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
}
