//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a representation of an identity.
 * 
 * <p>Java class for IdentificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}IdentificationID"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}IdentificationCategoryText" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}IdentificationJurisdictionISO3166Alpha3Code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationType", propOrder = {
    "identificationID",
    "identificationCategoryText",
    "identificationJurisdictionISO3166Alpha3Code"
})
public class IdentificationType
    extends ComplexObjectType
{

    @XmlElement(name = "IdentificationID", required = true)
    protected String identificationID;
    @XmlElement(name = "IdentificationCategoryText")
    protected TextType identificationCategoryText;
    @XmlElement(name = "IdentificationJurisdictionISO3166Alpha3Code")
    protected String identificationJurisdictionISO3166Alpha3Code;

    /**
     * Gets the value of the identificationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationID() {
        return identificationID;
    }

    /**
     * Sets the value of the identificationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationID(String value) {
        this.identificationID = value;
    }

    /**
     * Gets the value of the identificationCategoryText property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getIdentificationCategoryText() {
        return identificationCategoryText;
    }

    /**
     * Sets the value of the identificationCategoryText property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setIdentificationCategoryText(TextType value) {
        this.identificationCategoryText = value;
    }

    /**
     * Gets the value of the identificationJurisdictionISO3166Alpha3Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationJurisdictionISO3166Alpha3Code() {
        return identificationJurisdictionISO3166Alpha3Code;
    }

    /**
     * Sets the value of the identificationJurisdictionISO3166Alpha3Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationJurisdictionISO3166Alpha3Code(String value) {
        this.identificationJurisdictionISO3166Alpha3Code = value;
    }

}
