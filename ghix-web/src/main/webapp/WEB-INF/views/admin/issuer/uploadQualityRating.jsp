<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
        		<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.issuerTitle"/></a></li>
        		<li><a href="<c:url value="/admin/qualityrating/view"/>"><spring:message  code="pgheader.qualityrating"/></a></li>
        		<li><spring:message  code="pgheader.uploadNewFile"/></li>
            </ul><!--page-breadcrumb ends-->
            <h1><spring:message  code="pgheader.qualityrating"/></h1>
		</div>
	
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.aboutthisfile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li>
                    	<spring:message  code="label.ratingReaderInfo"/>
                    </li>
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="span8"><b><spring:message  code="pgheader.uploadNewQualityRating"/></b></h4>
                        <a class="btn btn-small" href="<c:url value="/admin/qualityrating/view"/>"><spring:message  code="label.cancel"/></a>
						<a class="btn btn-primary btn-small" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                        
                    </div>
                    <div class="gutter10">
                    <form class="form-horizontal" id="frmQualityRating" name="frmQualityRating" method="POST" enctype="multipart/form-data" action="<c:url value="/admin/issuer/qualityRating/document/save"/>">
                    	<df:csrfToken/>
						<label class="control-label" for="qualityRatingFile"><spring:message  code="label.qualityRatingFile"/>
							<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message  code='label.uploadQualityRatingFile'/>"><i class="icon-question-sign"></i></a>
						</label>
						<div class="controls">
							<input type="file" id="qualityRatingFile" name="qualityRatingFile"  class="input-file" onchange="return validateFileExtension(this)"/>
							&nbsp; <button type="submit" class="btn" id="btn_quality_rating" name="btn_quality_rating"><spring:message  code="label.upload"/></button>
							<div id="qualityRating_display"></div>
							<div id="qualityRatingFile_error" class=""></div>
							<input type="hidden" id="hdnQualityRating" name="hdnQualityRating" value=""/>
						</div>
						
						<br />
						<label class="control-label" for="comment_text"><spring:message  code="label.addComment"/></label>
						<div class="controls paddingT5">
							<textarea class="input-xlarge" name="comment_text" id="comment_text" rows="4" cols="40" style="resize: none;" 
							maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>
							<input type="hidden" id="hdnCommentText" name="hdnCommentText" value=""/>
							<div id="comment_text_error"  class="help-inline"></div>
							<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message  code="label.characterLeft"/></b></div>
						</div>
						
						<div id="invalidInfo" style="color:red;">
							<table>
							    <c:forEach var="errorMsg" items="${invalidHiosIds}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							    <c:forEach var="errorMsg" items="${invalidPlanHOISIds}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							    <c:forEach var="errorMsg" items="${dataMissMatches}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							    <c:forEach var="errorMsg" items="${invalidEffectiveDates}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							    <c:forEach var="errorMsg" items="${effectiveDateValidationErrors}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							     <c:forEach var="errorMsg" items="${invalidGlobalRatings}" >  
									<tr><td><c:out value="${errorMsg}" /></td></tr>
							    </c:forEach>
							</table>
							<%
								String fileEmpty = request.getParameter("fileEmpty");
								String badInput = request.getParameter("badInput");
							%>
							<% if(StringUtils.isNotBlank(fileEmpty)) { %>
						
								<spring:message code="label.norecords"/><br>
							
							<% } if(StringUtils.isNotBlank(badInput)) { %>
								
								<spring:message code="label.badInputFile"/><br>
							<% } %>
						</div>
						
                    </form>
                </div>
            </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
	
	<script type="text/javascript">
	var csrValue = $("#csrftoken").val();
	function validateFileExtension(fld) {
		if(!fld.value || !/(\.csv)$/i.test(fld.value)) {
			$.alert("Invalid File Selected to upload", "Invalid Selection");
			fld.form.reset();
			fld.focus();        
			return false;   
		}   
		return true; 
	}

	function submitForm(){
		 $("#frmQualityRating").submit();
	 } 
		  
	  $(document).ready(function(){
		  $("#btn_quality_rating").click(function(){

			  if (validateFileExtension(document.getElementById("qualityRatingFile"))) {
				$('#frmQualityRating').ajaxSubmit({
					url: "<c:url value='/admin/issuer/qualityratingfile/upload'/>",
					data: {"csrftoken":csrValue},
					success: function(responseText){
							if(responseText.value=='undefined'){
								return false;
							}
						    var val = responseText.split("|");
						    $("#qualityRating_display").text(val[1]);
							$("#hdnQualityRating").val(val[2]);
							//alert(val[1] +':::'+ val[2]);
			       	}
				});
			  }
			  return false; 
			});	// button click close
		  });
  
	$('.ttclass').tooltip();
	var validator = $("#frmQualityRating").validate({ 
		rules : {
			qualityRatingFile : {required : true},
		},
		messages : {
			qualityRatingFile: { required : "<span><em class='excl'>!</em><spring:message  code='err.qualityRating' javaScriptEscape='true'/></span>"},
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
	function updateCharCount(){
		var commentMaxLen=500;
		var currentLen = $.trim(document.getElementById("comment_text").value).length;
		 var test=document.getElementById("comment_text").value;
		 $("#hdnCommentText").val(test);
		var charLeft = commentMaxLen - currentLen;
		$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
	}
	
	$.extend({ alert: function (message, title) {
		  $("<div></div>").dialog( {
			buttons: { "Ok": function () { $(this).dialog("close"); } },
			close: function (event, ui) { $(this).remove(); },
			resizable: false,
			title: title,
			modal: true
		  }).text(message);
		}
	});
	
</script>