package com.getinsured.hix.indportal.dto;

import java.util.List;

/*
 * DTO used for to update enrollment status
 * @author Sunil Sahoo
 */
public class EnrollmentStatusUpdate {
	
	private String overrideCommentText;
	private String issuerAssignedPolicyId;
	private String enrollmentId;
	private String caseNumber;
	private String lastPremiumPaidDate;
	private List<CoveredFamilyMember> familyMembers;
	private String houseHoldId;
	
	public String getOverrideCommentText() {
		return overrideCommentText;
	}
	public void setOverrideCommentText(String overrideCommentText) {
		this.overrideCommentText = overrideCommentText;
	}
	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}
	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public List<CoveredFamilyMember> getFamilyMembers() {
		return familyMembers;
	}
	public void setFamilyMembers(List<CoveredFamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}
	public String getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}
	public void setLastPremiumPaidDate(String lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}
	public String getHouseHoldId() {
		return houseHoldId;
	}
	public void setHouseHoldId(String houseHoldId) {
		this.houseHoldId = houseHoldId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EnrollmentStatusUpdate [overrideCommentText=");
		builder.append(overrideCommentText);
		builder.append(", issuerAssignedPolicyId=");
		builder.append(issuerAssignedPolicyId);
		builder.append(", enrollmentId=");
		builder.append(enrollmentId);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", lastPremiumPaidDate=");
		builder.append(lastPremiumPaidDate);
		builder.append(", familyMembers=");
		builder.append(familyMembers);
		builder.append("]");
		return builder.toString();
	}

}
