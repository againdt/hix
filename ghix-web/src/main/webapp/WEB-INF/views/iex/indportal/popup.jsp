<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

 <div id="portalDelayPopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 id="failedSumissionModal">Application is in progress...</h3>
        </div>
        <div class="modal-body gutter20">
          <p>The information received in your application is being processed. The page will load once the application processing is complete.</p>
        </div>
        <div style="display:none" class="modal-footer refreshIt">
        	<strong style="display:block">Your application is still in queue. Please refresh manually.</strong>
        	 <a href="javascript:void(0);" onclick="goHome()" class="btn btn-primary" data-dismiss="modal">Refresh</a>
        </div>
    </div> <!-- /.modal-content-->
   </div><!-- /.modal-dialog-->
</div>
<div style="display:none" class="showdashboard alert alert-block">
	<%@ include file="portalHome_CA.jsp" %>
</div>

<script >
function goHome(){
	location.href='./indportal';
}

$(document).ready(function() {
	var count = 0;
	var timeout = setTimeout(countdown, 1000);
	function countdown() {	
		count = count+1;
		if (count > 5 ){
			$.ajax({
				  method: "GET",
				  url: "indportal/showDelayPopup",
				  cache: false
				}).done(function( response ) { 
				    if(response == false){
				    	clearTimeout(timeout);
				    	goHome();
					}else{
						count = 0;
						timeout = setTimeout(countdown, 1000);
						$('#portalDelayPopup').modal('show');
					}					    
				  });
		}else{
			timeout = setTimeout(countdown, 1000);
			$('#portalDelayPopup').modal('show');	
		}		
			
	}	
});

</script>




 

 


