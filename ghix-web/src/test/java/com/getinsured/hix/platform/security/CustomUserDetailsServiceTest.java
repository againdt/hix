package com.getinsured.hix.platform.security;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.repository.UserRepository;


public class CustomUserDetailsServiceTest extends BaseTest{

	@Autowired	CustomUserDetailsService customUserDetailsService;
	@Autowired	UserRepository userRepository;  
	
	AccountUser usr;
	boolean insertFlag;
	//private final String _USER= "saypratap@yahoo.co.in";
	
	@Before
	public void init() {
		System.out.println("Calling init()....");
		
		usr  = userRepository.findByUserName("saypratap@yahoo.co.in");
		
		if(usr == null){
			System.out.println("user not found");

			usr = new AccountUser();
			usr.setUserName("saypratap@yahoo.co.in");
			usr.setEmail("saypratap@yahoo.co.in");
			userRepository.save(usr);
			insertFlag=true;
		}else{
			System.out.println("user found .....");
			System.out.println("user name found : "+ usr.getUsername());
		}
		
	}
	
	
	@Test
	public void testLoadUserByUsername() {
		AccountUser domainUser = new AccountUser();


		domainUser = (AccountUser) customUserDetailsService
				.loadUserByUsername("saypratap@yahoo.co.in");

		try {
			assertTrue(
					"Test 1: Default values are wrong.",
					domainUser != null
							&& domainUser.getUsername().equalsIgnoreCase(
									"saypratap@yahoo.co.in"));

			System.out.println("Test 1 completed successfully.");
		} catch (AssertionError e) {
			fail(e.getMessage());
		}
	}

	
	@After
	public void endTest() {
		System.out.println("Calling end()....");
		if(insertFlag){
			
			System.out.println("Deleting User ....");
			
			usr  = userRepository.findByUserName("saypratap@yahoo.co.in");
			userRepository.delete(usr);	
		}
	}

}
