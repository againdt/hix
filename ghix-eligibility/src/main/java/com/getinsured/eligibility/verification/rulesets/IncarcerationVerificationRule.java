package com.getinsured.eligibility.verification.rulesets;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


/**
 * Encapsulates Death Verification Rule methods
 */
@Component
public class IncarcerationVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(IncarcerationVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) {
		
		IncarcerationVerificationRule incarcerationVerificationRule = new IncarcerationVerificationRule();
		//incarcerationVerificationRule.verifyIncarceration(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadHUBJSON());
	}

	/**
	 * This rule will verify Incarceration Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @throws Exception 
	 */
	public boolean verifyIncarceration(String ssapJson, String hubResponseJson, String pPersonId) throws Exception {
		LOGGER.info("Verify Incarceration Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonArray householdMembers;
		JsonObject householdMember;
		String personId;
		JsonObject incarcerationStatus;
		boolean incarcerationStatusIndicator;
		boolean personChecked = false;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				try {
					if(personId.equals(pPersonId)){
						incarcerationStatus = householdMember.get(SSAPConstants.INCARCERATION_STATUS).getAsJsonObject();
						incarcerationStatusIndicator = incarcerationStatus.get(SSAPConstants.INCARCERATION_STATUS_INDICATOR).getAsBoolean();
						
						if(incarcerationStatusIndicator){
							status = SSAPConstants.VERIFIED;
						} else {
							if(!SSAPUtils.isEmpty(hubResponseJson)){
								if(!SSAPConstants.NO_SSN.equalsIgnoreCase(hubResponseJson)){
									String personIncarcerationInformationIndicator = SSAPUtils.searchHubResponseJson(
											SSAPConstants.PERSON_INCARCERATION_INFORMATION_INDICATOR, hubResponseJson);
									if(!SSAPUtils.isEmpty(personIncarcerationInformationIndicator) && SSAPConstants.TRUE.equals(personIncarcerationInformationIndicator)){
										String prisonerConfinementDate = SSAPUtils.searchHubResponseJson( 
												SSAPConstants.PRISONER_CONFINEMENT_DATE, hubResponseJson);
										if(!SSAPUtils.isEmpty(prisonerConfinementDate) && SSAPUtils.isDateGreaterThanCurrentDateInMillis(prisonerConfinementDate)){
											status = SSAPConstants.VERIFIED;
										} else {
											status = SSAPConstants.NOT_VERIFIED;
										}
									} else {
										status = SSAPConstants.VERIFIED;
									}
								} else {
									status = SSAPConstants.NOT_VERIFIED;
								}
							} else {
								status = SSAPConstants.PENDING;
							}
						}
						personChecked = true;
					}
					
					if(personChecked)break;
				} catch(Exception exception){
					LOGGER.error("Exception occurred while Verifying Incarceration : Person : "+pPersonId, exception);
					String rule = SSAPConstants.RULES.INCARCERATION_VERIFICATION_RULE.toString();
					giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
							hubResponseJson, null, null, null, rule), rule));
				}
			}
			
			if(!SSAPUtils.isEmpty(status)){
				verificationStatus = verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
						Long.valueOf(pPersonId), status, SSAPConstants.INCARCERATION_VERIFICATION_RULE);
				verificationStatus = true;
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Incarceration : " , exception);
			throw exception;
		}

		LOGGER.info("Verify Incarceration Rule : Ends ");
		
		return verificationStatus;
	}
}
