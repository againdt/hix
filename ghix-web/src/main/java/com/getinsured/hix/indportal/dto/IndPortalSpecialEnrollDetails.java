package com.getinsured.hix.indportal.dto;

public class IndPortalSpecialEnrollDetails {
	
	private String sepStartDate;
	private String sepEndDate;
	private String covStartDate;
	private String caseNumber;
	//Flag to indicate, if CSR is allowed to edit SEP details
	private boolean enableEditing;
	//Maps the SSAP Application Event Id
	private String eventUpdateId;
	//The coverage year for which CSR is overriding the details
	private long coverageYear;
	//Flag to indicate if CMR_HOUSHOLD.SEP_EDITED_YEAR should be updated
	private boolean editSep;
	
	public String getSepStartDate() {
		return sepStartDate;
	}
	public void setSepStartDate(String sepStartDate) {
		this.sepStartDate = sepStartDate;
	}
	public String getCovStartDate() {
		return covStartDate;
	}
	public void setCovStartDate(String covStartDate) {
		this.covStartDate = covStartDate;
	}
	public String getSepEndDate() {
		return sepEndDate;
	}
	public void setSepEndDate(String sepEndDate) {
		this.sepEndDate = sepEndDate;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public boolean isEnableEditing() {
		return enableEditing;
	}
	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
	public String getEventUpdateId() {
		return eventUpdateId;
	}
	public void setEventUpdateId(String eventUpdateId) {
		this.eventUpdateId = eventUpdateId;
	}
	public long getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	public boolean isEditSep() {
		return editSep;
	}
	public void setEditSep(boolean editSep) {
		this.editSep = editSep;
	}
	

}
