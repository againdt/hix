package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CouchMigrationUtil {
	private CouchMigrationUtil() {

	}

	public static boolean isValidString(String s) {
		return !(s == null || s.trim().equals(""));
	}

	public static String validString(String s) {
		return s.trim();
	}
	
	public static int mapSize(Map<?, ?> data) {
		return data != null ? data.size() : 0;
	}
	
	public static long convertToLong(String s) {
		return (s != null) ? Long.parseLong(s) : 0;
	}
	
	public static int convertToInt(String s) {
		return (s != null) ? Integer.parseInt(s) : 0;
	}
	
	public static int listSize(List<?> data) {
		return data != null ? data.size() : 0;
	}

	public static int collectionSize(Collection<?> data) {
		return data != null ? data.size() : 0;
	}

}
