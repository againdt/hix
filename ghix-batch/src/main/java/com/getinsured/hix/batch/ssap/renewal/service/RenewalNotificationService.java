package com.getinsured.hix.batch.ssap.renewal.service;

import com.getinsured.hix.batch.ssap.renewal.dto.GenerateNoticeEnum;

public interface RenewalNotificationService {
	void processRenewalApplicationData(GenerateNoticeEnum generateNoticeFor, Long batchSize, String stateCode);
}