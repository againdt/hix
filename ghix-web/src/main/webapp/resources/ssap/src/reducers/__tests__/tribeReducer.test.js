import tribeReducer, { initialState } from '../tribeReducer';
import { tribesActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: 23
};

const clearAction = {
    type: tribesActionTypes.CLEAR_STATE
};

const setTribesAction = {
    type: tribesActionTypes.SET_TRIBES,
    tribes: {
        name: 'Nevada',
        code: 'NV',
        tribes: [
            {
                tribeName: 'Some native americans from Nevada',
                tribeCode: 'NV010001'
            }
        ]
    }
};

describe('Test for tribeReducer: ', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(
            tribeReducer(
                initialState,
                anotherAction
            )
        ).toEqual(initialState);
    });

    it('Should set new tribes', () => {
        expect(
            tribeReducer(
                initialState,
                setTribesAction
            )
        ).toEqual(setTribesAction.tribes);
    });

    it('Should clear state', () => {
        expect(
            tribeReducer(
                initialState,
                clearAction
            )
        ).toEqual(initialState);
    });
});

