package com.getinsured.eligibility.at.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.ref.common.AssisterTypeEnum;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.resp.si.enums.NativeAmericanCSREnum;
import com.getinsured.eligibility.at.resp.si.enums.NonNativeAmericanCSREnum;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSREligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilitySpansType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantEventType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantNonQHPType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.common.base.Enums;

/**
 * This call has been copied from IdValidator.
 *
 */
@Component("customValidatorImpl")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class CustomValidatorImpl extends BaseCustomValidator {

	private static final List<String> CSR_LIST = new ArrayList<String>();
	private static final String IDAHO = "ID";
	@Autowired private IUserRepository iUserRepository;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private ATSpanProcessingRepository atSpanProcessingRepository;
	
	static {
		CSR_LIST.add("OpenToIndiansBelow300PercentFPL");
		CSR_LIST.add("OpenToIndiansAbove300PercentFPL");
		CSR_LIST.add("73PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_LIST.add("87PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_LIST.add("94PercentActuarialVarianceLevelSilverPlanCSR");
	}

	private void validateMedicaidCHIPId(AccountTransferRequestPayloadType request, List<String> errorList) {
		String identificationId = null;
		List<PersonType> personList = request.getPerson();
		for (PersonType personType : personList) {
			identificationId = null;
			if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
				identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
			}

			if (identificationId == null) {
				if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID().getValue();
				}
			}
			if (identificationId == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_IDENTIFICATION_ID, new Object[] { personType.getId() }));
			}
		}

	}

	@Override
	public List<String> validate(AccountTransferRequestPayloadType request) {
		final List<String> errorList = super.validate(request);

		validateMedicaidCHIPId(request, errorList);
		//validateFinancialApplication(request, errorList);
		validateTaxHouseHold(request, errorList);
		validateEligibilities(request, errorList);
		validateExchangeEligibilities(request, errorList);
		validateBloodRelationshipAvailableForAll(request, errorList);
		validateApplicationExtension(request, errorList);
		validateHousholdCaseId(request, errorList);
		
		if(GhixPlatformConstants.TIMESHIFT_ENABLED){
			validateDateYearAge(request, errorList);	
		}
		validateAuthRepsOrgaIdenId(request, errorList);
		
		// validate the spans added for capturing the current and total span information from AT
		validateEligibilitySpans(request,errorList);
		
		// validate the assister information if passed - this is only for MN
		validateAssisterInformation(request,errorList);
		
		//validate if coverage seeking member is also sent as an insurance applicant
		validateCoverageSeekingApplicants(request,errorList);
		return errorList;
	}
	
	/**
	 * This method is used to validate the assister information if passed in case of MN
	 * @param request
	 * @param errorList
	 */
	private void validateAssisterInformation(AccountTransferRequestPayloadType accountTransferRequestPayloadType, List<String> errorList) {
		
		String isAssisterValidationEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
		if(isAssisterValidationEnabled.equalsIgnoreCase("true")) {
			// validate the Assister for MN
			String assisterType = null;
			boolean isvalidOrgName= false;
			if (null != accountTransferRequestPayloadType.getAssister() && null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson())
			{
				
				if (null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
						&& null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
								.getPersonOrganizationAssociation()
						&& null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
								.getPersonOrganizationAssociation().getOrganization()
						&& null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
								.getPersonOrganizationAssociation().getOrganization().getOrganizationName()
						&& null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
								.getPersonOrganizationAssociation().getOrganization().getOrganizationName().getValue()) {	
					// validate the organization name
					assisterType = accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation()
							.getPersonOrganizationAssociation().getOrganization().getOrganizationName().getValue();		
					
					AssisterTypeEnum[] enums = AssisterTypeEnum.values();
					for(int i=0;i<enums.length;i++) {
						// case sensitive comparison for the assister type
						if(enums[i].assisterTypeValue.equals(assisterType)){
							isvalidOrgName = true;
							break;
						}
					}					
					if(!isvalidOrgName) {
						// error the value of assiter type must be 
						errorList.add(ReferralUtil.formatMessage(MSG_INVALID_ASSISTER_TYPE_ORG_NAME, new Object[]{}));
					}				
				} else {
					// organization name is mandatory 
					errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Assister:OrganizationName" }));
				}
				
				if(null != accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation() 
					&& accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation().getPersonIdentification() != null 
				    && ReferralUtil.listSize(accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson().getPersonAugmentation().getPersonIdentification()) != 0) 
				{
					boolean isExternalAssisterIdPassed = false;
					boolean isNpnPassed = false;
					final int isize = accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson()
							.getPersonAugmentation().getPersonIdentification().size();
					IdentificationType temp;
					for (int i = 0; i < isize; i++) {
						temp = accountTransferRequestPayloadType.getAssister().getRolePlayedByPerson()
								.getPersonAugmentation().getPersonIdentification().get(i);
						if (ReferralUtil.isValidString(ReferralUtil.getValue(temp.getIdentificationCategoryText())) && temp
								.getIdentificationCategoryText().getValue().equals(ReferralProcessingConstants.ASSISTER_EXTERNALID)) {
							if(null != temp.getIdentificationID() && null != temp.getIdentificationID().getValue()) {
								isExternalAssisterIdPassed = true;	
							}
							
						}
						if (ReferralUtil.isValidString(ReferralUtil.getValue(temp.getIdentificationCategoryText())) && temp
								.getIdentificationCategoryText().getValue().equals(ReferralProcessingConstants.BROKER_NPN)) {
							if(null != temp.getIdentificationID() && null != temp.getIdentificationID().getValue()) {
								isNpnPassed = true;	
							}
							
						} 
					}
					
					// if externalAssisterid is not passed then throw error
					if(!isExternalAssisterIdPassed) {
						errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Assister:PersonIdentification Identification ID for text Assister ExternalID" }));	
					}
					
					// if the assiterType is broker then npn is mandatory
					if(isvalidOrgName && null != assisterType && assisterType.equalsIgnoreCase(AssisterTypeEnum.BROKER.toString())) {
						// check npn 
						if(!isNpnPassed) {
							errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Assister:PersonIdentification Identification ID for text Broker NPN" }));	
						}
					}
					
				} else {
					errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Assister:PersonIdentification" }));
				}
			}
			
		}
	}

	/**
	 * This method is used to validate the current span and total span information from the AT
	 * This is added for MN for Multiple Eligibility Spans (MES)
	 * @param request
	 * @param errorList
	 */

	private void validateEligibilitySpans(AccountTransferRequestPayloadType request, List<String> errorList) {
		
		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		
		if(mutipleEligibiltySpanEnabled.equalsIgnoreCase("true")){
			// get the eligibility spans from the insurance application
			EligibilitySpansType eligibilitySpansType = request.getInsuranceApplication().getApplicationExtension().getEligibilitySpans();
			if(null == eligibilitySpansType) {
				// EligibilitySpans is not passed
				errorList.add(ReferralUtil.formatMessage(MSG_NO_OR_INVALID_ELIGIBILITY_SPANS, new Object[] { "CurrentEligibilitySpan and TotalEligibilitySpan" }));
			} else if(null == eligibilitySpansType.getCurrentEligibilitySpan() || null == eligibilitySpansType.getTotalEligibilitySpan()) {
				// EligibilitySpans is passed but CurrentEligibilitySpan and TotalEligibilitySpan is not passed
				errorList.add(ReferralUtil.formatMessage(MSG_NO_OR_INVALID_ELIGIBILITY_SPANS, new Object[] { "CurrentEligibilitySpan and TotalEligibilitySpan" }));
			} else if(null == eligibilitySpansType.getCurrentEligibilitySpan().getValue() || null == eligibilitySpansType.getTotalEligibilitySpan().getValue()) {
				// EligibilitySpans is passed but CurrentEligibilitySpan and TotalEligibilitySpan is null or not an integer
				errorList.add(ReferralUtil.formatMessage(MSG_NO_OR_INVALID_ELIGIBILITY_SPANS, new Object[] { "CurrentEligibilitySpan and TotalEligibilitySpan" }));
			} else {
				// validate them for the values they contain
				if (!(eligibilitySpansType.getCurrentEligibilitySpan().getValue().compareTo(BigDecimal.ZERO) > 0) 
						|| !(eligibilitySpansType.getTotalEligibilitySpan().getValue().compareTo(BigDecimal.ZERO) > 0) ) {
					// EligibilitySpans is passed but CurrentEligibilitySpan and TotalEligibilitySpan is 0 or less than 0 
					errorList.add(ReferralUtil.formatMessage(MSG_NO_OR_INVALID_ELIGIBILITY_SPANS, new Object[] { "CurrentEligibilitySpan and TotalEligibilitySpan" }));	
				}
				else{
					// If valid current and total span are found validate the sequence of spans.
					// Commenting following line as we not inserting data into at_span_info yet. 
					validateEligibilitySpansSequence(request,eligibilitySpansType.getCurrentEligibilitySpan().getValue(),eligibilitySpansType.getTotalEligibilitySpan().getValue(),errorList);
				}
			}	
		}		
	}

	/**
	 * This method is used to validate if spans are sent in sequence.
	 * MN should send spans in order of 1/x,2/x and so on.
	 * @param request
	 * @param errorList
	 */
	private void validateEligibilitySpansSequence(AccountTransferRequestPayloadType request,BigDecimal atCurrentSpan,BigDecimal atTotalSpans,List<String> errorList) {
		// if current span == 1, no sequence validation is required.
		if(atCurrentSpan.intValue() == 1){
			return;
		}
		else{
			Set<BigDecimal> cmrHouseHolds;
			// fetch HH from primary applicant minsure id
			String primaryContactExternalId = this.extractPrimaryContactExternalApplicantId(request);
			if(StringUtils.isNotEmpty(primaryContactExternalId)){
				if(primaryContactExternalId != null && !primaryContactExternalId.isEmpty()){// if HH found perform validation.
					// fetch latest span from span table
					String receivedSpans = "";
						List<AccountTransferSpanInfo> dbATSpanList = new ArrayList<AccountTransferSpanInfo>();
						ApplicationExtensionType applicationExtension = request.getInsuranceApplication().getApplicationExtension();
						int coverageYear = applicationExtension.getCoverageYear().getValue().getYear(); // Null check is not added on purpose as flow can't proceed without coverage year.
						dbATSpanList = atSpanProcessingRepository.findByExternalApplicantIdAndCoverageYear(primaryContactExternalId, coverageYear);
						if(dbATSpanList != null && !dbATSpanList.isEmpty()){
							BigDecimal latestDBSpan = dbATSpanList.get(0).getCurrentSpan();
							int sliceIndex=0;
							if(atCurrentSpan.intValue() - latestDBSpan.intValue() != 1){
								for (AccountTransferSpanInfo accountTransferSpanInfo : dbATSpanList) {
									sliceIndex++;
									if(accountTransferSpanInfo.getCurrentSpan().intValue() == 1){
										break;
									}
								}
								
								// If we have MES groups were sent multiple times, we need to compare only with the last sent group. 
								
								List<AccountTransferSpanInfo> slicedAtSpanList = new ArrayList<AccountTransferSpanInfo>();
								slicedAtSpanList =	dbATSpanList.subList(0, sliceIndex);
								if(slicedAtSpanList != null && !slicedAtSpanList.isEmpty()){
									Collections.reverse(slicedAtSpanList);
									for (AccountTransferSpanInfo accountTransferSpanInfo : slicedAtSpanList) {
										receivedSpans =  receivedSpans + accountTransferSpanInfo.getCurrentSpan() + "/" + accountTransferSpanInfo.getTotalSpan() + ",";
									}
									receivedSpans = receivedSpans.substring(0, receivedSpans.length()-1);// Remove last comma
									errorList.add(ReferralUtil.formatMessage(MSG_MISSING_ELIGIBILITY_SPANS, new Object[] {receivedSpans}));// If other spans are received before 1st span is not received
								}
								else{
									errorList.add(ReferralUtil.formatMessage(MSG_MISSING_ELIGIBILITY_SPANS, new Object[] { "None." }));// If other spans are received before 1st span is not received
								}
							}
						}
						else{
							errorList.add(ReferralUtil.formatMessage(MSG_MISSING_ELIGIBILITY_SPANS, new Object[] { "None." }));// If other spans are received before 1st span is not received
						}
					
				}
				else{ // HH by applicant external id not found, return error. 
					errorList.add("Can not validate spans. No household found.");
				}
			}
			else{ // primary applicant's external id not found, return error
				errorList.add("Can not validate spans. Applicant's medicaid id not found.");
			}
		}
		
	}

	private void validateDateYearAge(AccountTransferRequestPayloadType request, List<String> errorList) {
		 
		List<PersonType> personTypeList = request.getPerson();
		TSDate currentDate = new TSDate();
		for(PersonType person:personTypeList) {
			java.util.Date dob = ReferralUtil.extractDate(person.getPersonBirthDate());
			if(null != dob && dob.getYear() > currentDate.getYear()) {
				errorList.add("The year in this nc:Date must be greater than 1912 and less than or equal to the current year.");
			} 
			 
		}
		
	}

	private void validateHousholdCaseId(AccountTransferRequestPayloadType request, List<String> errorList) {
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =request.getInsuranceApplication().getApplicationIdentification();
			boolean hhCaseIdPresent = false;
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType :iTypes) {
				if( null != tIdentificationType.getIdentificationCategoryText( ) && null != tIdentificationType.getIdentificationCategoryText().getValue() && 
						null !=tIdentificationType.getIdentificationID()  && null != tIdentificationType.getIdentificationID().getValue() && 
						!tIdentificationType.getIdentificationID().getValue().isEmpty() && 
						ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
					hhCaseIdPresent = true;
					break;
				}
			}
			
			if(!hhCaseIdPresent) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ApplicationIdentification:IdentificationCategoryText  household Case id" }));
			}
			
		}
		
	}

	private void validateApplicationExtension(AccountTransferRequestPayloadType request, List<String> errorList) {
		final Set<Integer> coverageYears = ReferralUtil.fetchCoverageYears(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.ACCOUNT_TRANSFER_COVERAGE_YEARS));

		ApplicationExtensionType applicationExtension = request.getInsuranceApplication().getApplicationExtension();
		if (applicationExtension == null) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ApplicationExtension" }));
		} else {
			String captureCoverageYear = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.AT_CAPTURE_COVERAGE_YEAR);
			if("TRUE".equalsIgnoreCase(captureCoverageYear)){
				if (applicationExtension.getCoverageYear() == null || applicationExtension.getCoverageYear().getValue() == null) {
					errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "CoverageYear" }));
				} else if (!coverageYears.contains(applicationExtension.getCoverageYear().getValue().getYear())) {
					errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE, new Object[] { "CoverageYear" }));
				}	
			}
			if (ReferralUtil.listSize(applicationExtension.getExtendedApplicant()) != ReferralConstants.NONE) {
				validateApplicationExtensionApplicant(applicationExtension.getExtendedApplicant(), errorList);
			}
		}
	}

	private void validateApplicationExtensionApplicant(List<ExtendedApplicantType> extendedApplicantList, List<String> errorList) {
		String applicantID;
		for (ExtendedApplicantType applicant : extendedApplicantList) {
			if (applicant.getExtendedApplicantMemberIdentification() == null || applicant.getExtendedApplicantMemberIdentification().getIdentificationID() == null
			        || !ReferralUtil.isNotNullAndEmpty(applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue())) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ExtendedApplicantMemberIdentification" }));
			} else {
				applicantID = applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue();
				final List<ExtendedApplicantEventType> extendedApplicantEventList = applicant.getExtendedApplicantEvent();
				final List<ExtendedApplicantNonQHPType> extendedApplicantNonQHPList = applicant.getExtendedApplicantNonQHP();

				if ((ReferralUtil.listSize(extendedApplicantEventList) == ReferralConstants.NONE) && (ReferralUtil.listSize(extendedApplicantNonQHPList) == ReferralConstants.NONE)) {
					errorList.add(ReferralUtil.formatMessage(MSG_EVENT_QHP_MANDATORY, new Object[] { applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue() }));
				}
				if (ReferralUtil.listSize(extendedApplicantEventList) != ReferralConstants.NONE) {
					validateExtendedApplicantEvent(extendedApplicantEventList, errorList, applicantID);
				}
				if (ReferralUtil.listSize(extendedApplicantNonQHPList) != ReferralConstants.NONE) {
					validateExtendedApplicantNonQHP(extendedApplicantNonQHPList, errorList, applicantID);
				}
			}
		}
	}

	private void validateExtendedApplicantEvent(List<ExtendedApplicantEventType> extendedApplicantEventList, List<String> errorList, String applicantId) {
		// Set<String> eventCodes = new HashSet<String>();
		for (ExtendedApplicantEventType extendedApplicantEventType : extendedApplicantEventList) {

			if (extendedApplicantEventType.getExtendedApplicantEventCode() == null || extendedApplicantEventType.getExtendedApplicantEventCode().getValue() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantEventCode", applicantId }));
			} /*
			 * else if (!eventCodes.add(extendedApplicantEventType.getExtendedApplicantEventCode().getValue().value())) { errorList.add(ReferralUtil.formatMessage(MSG_CODE_DUPLICATE, new Object[] { "ExtendedApplicantEventCode", applicantId })); }
			 */
			if (extendedApplicantEventType.getExtendedApplicantEventDate() == null || extendedApplicantEventType.getExtendedApplicantEventDate().getDate() == null) {

				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantEventDate", applicantId }));
			} else if (!isValidDate(extendedApplicantEventType.getExtendedApplicantEventDate().getDate())) {

				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant EventDate", applicantId }));
			} else if (!validationForFutureDate(extendedApplicantEventType.getExtendedApplicantEventDate().getDate(), extendedApplicantEventType.getExtendedApplicantEventCode())) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "ExtendedApplicant EventDate", applicantId }));
			}

			if (extendedApplicantEventType.getExtendedApplicantReportDate() == null || extendedApplicantEventType.getExtendedApplicantReportDate().getDate() == null) {

				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantReportDate", applicantId }));
			} else if (!isValidDate(extendedApplicantEventType.getExtendedApplicantReportDate().getDate()) || isFutureDate(extendedApplicantEventType.getExtendedApplicantReportDate().getDate())) {

				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant ReportDate", applicantId }));
			}

		}
	}

	private boolean validationForFutureDate(Date date, ExtendedApplicantEventCodeType extendedApplicantEventCodeType) {
		if (!ApplicationExtensionEventUtil.FUTURE_DATED_LCE_EVENTS.contains(extendedApplicantEventCodeType.getValue().value()) && isFutureDate(date)) {
			return false;
		}
		return true;

	}

	private void validateExtendedApplicantNonQHP(List<ExtendedApplicantNonQHPType> extendedApplicantNonQHPList, List<String> errorList, String applicantId) {
		// Set<String> eventCodes = new HashSet<String>();

		for (ExtendedApplicantNonQHPType extendedApplicantNonQHPType : extendedApplicantNonQHPList) {
			if (extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode() == null || extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode().getValue() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantNonQHPCode", applicantId }));
			} /*
			 * else if (!eventCodes.add(extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode().getValue().value())) { errorList.add(ReferralUtil.formatMessage(MSG_CODE_DUPLICATE, new Object[] { "ExtendedApplicantNonQHPCode", applicantId })); }
			 */
			if (extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate() == null || extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate().getDate() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantNonQHPDate", applicantId }));
			} else if (!isValidDate(extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate().getDate())) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant NonQHPDate", applicantId }));
			}
		}
	}

	/**
	 * Added by pravin to check blood relationship available for all members (1-to-1) except self.
	 *
	 * @param request
	 * @param errorList
	 */
	private void validateBloodRelationshipAvailableForAll(AccountTransferRequestPayloadType request, List<String> errorList) {

		List<PersonType> personList = request.getPerson();
		boolean relationshiperr = false;
		int familySize = ReferralUtil.listSize(personList);
		for (PersonType personType : personList) {
			List<PersonAssociationType> personAssociationType = personType.getPersonAugmentation() != null ? personType.getPersonAugmentation().getPersonAssociation() : null;

			int assocMemsize = ReferralUtil.listSize(personAssociationType);
			String indPersonId = personType != null ? personType.getId() != null ? personType.getId() : null : null;

			if (relationshiperr || assocMemsize != familySize - 1 || StringUtils.isEmpty(indPersonId)) {
				relationshiperr = true;
				break;
			}

			/* Now check 1-to-1 mapping */
			List<String> associatedPersonId = new ArrayList<String>();
			for (int i = 0; i < assocMemsize; i++) {
				boolean personRefIdPresent = personAssociationType.get(i) != null && personAssociationType.get(i).getPersonReference() != null && personAssociationType.get(i).getPersonReference().size() > 0
				        && personAssociationType.get(i).getPersonReference().get(0) != null && personAssociationType.get(i).getPersonReference().get(0).getRef() != null;

				String personRefId = personRefIdPresent ? ((PersonType) personAssociationType.get(i).getPersonReference().get(0).getRef()).getId() : null;

				String relationshipCode = personRefIdPresent ? (personAssociationType.get(i).getFamilyRelationshipCode() != null ? personAssociationType.get(i).getFamilyRelationshipCode().getValue() : null) : null;

				if (!StringUtils.isEmpty(personRefId) && !associatedPersonId.contains(personRefId) && !StringUtils.isEmpty(relationshipCode) && !indPersonId.equalsIgnoreCase(personRefId)) {
					associatedPersonId.add(personRefId);
				} else {
					relationshiperr = true;
					break;
				}
			}

		}

		if (relationshiperr) {
			errorList.add(MSG_MISSING_RELATIONSHIP);
		}
	}

	private void validateFinancialApplication(AccountTransferRequestPayloadType request, List<String> errorList) {
		boolean blnFlag = false;
		if (request.getInsuranceApplication().getInsuranceApplicationRequestingFinancialAssistanceIndicator() != null && request.getInsuranceApplication().getInsuranceApplicationRequestingFinancialAssistanceIndicator().isValue()) {
			blnFlag = true;
		}
		if (!blnFlag) {
			errorList.add(MSG_FINANCIAL_APP);
		}
	}

	private void validateTaxHouseHold(AccountTransferRequestPayloadType request, List<String> errorList) {

		if (request.getTaxReturn() != null && ReferralUtil.listSize(request.getTaxReturn()) > 1) {
			errorList.add(MSG_TAX_RETURN);
		}
	}

	private void validateEligibilities(AccountTransferRequestPayloadType request, List<String> errorList) {
		List<InsuranceApplicantType> InsuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();
		boolean isEligibilityMissing = false;
		for (InsuranceApplicantType insuranceApplicantType : InsuranceApplicantTypeList) {
			List<EligibilityType> eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
			boolean isAPTC = false, isCSR = false;
			for (EligibilityType eligibilityType : eligibilityTypeList) {
				String eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);

				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)) {
					isAPTC = true;
				}

				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)) {
					isCSR = true;
				}
			}

			if (!isAPTC || !isCSR) {
				isEligibilityMissing = true;
				break;
			}
		}

		if (isEligibilityMissing) {
			errorList.add(MSG_APTC_CSR_MISSING);
		}
	}
	
	private void validateExchangeEligibilities(AccountTransferRequestPayloadType request, List<String> errorList) {
		String exchangeEligibilityEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE);
		
		if(exchangeEligibilityEnabled.equalsIgnoreCase("true")){
			List<InsuranceApplicantType> InsuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();
			boolean isEligibilityMissing = false;
			for (InsuranceApplicantType insuranceApplicantType : InsuranceApplicantTypeList) {
				List<EligibilityType> eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
				boolean isExchangeEligibility = false;
				for (EligibilityType eligibilityType : eligibilityTypeList) {
					String eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);

					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE)) {
						isExchangeEligibility = true;
					}
				}

				if (!isExchangeEligibility) {
					isEligibilityMissing = true;
					break;
				}
			}

			if (isEligibilityMissing) {
				errorList.add(MSG_EXCHANGE_ELIG_MISSING_CA);
			}
	
		}
		
	}

	@Override
	void validateEligibilityDetails(AccountTransferRequestPayloadType request, List<String> errorList) {
		final String primaryTaxFilerApplicantId = extractPrimaryTaxFilerReferenceId(request);
		final String primaryContactApplicantId = extractPrimaryContactReferenceId(request);
		String exchangeEligibilityEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE);
		final List<InsuranceApplicantType> insuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();
		List<EligibilityType> eligibilityTypeList = null;
		String eligibilityIndicatorType = null;
		String applicantId = null;
		boolean aptcCheck[] = new boolean[] { false, false };
		boolean csrCheck[] = new boolean[] { false, false };
		boolean isPrimaryTaxFiler = false;
		boolean isPrimaryContact = false;
		Set<String> reasonCode = new HashSet<String>();
		Set<DateTime> aptcEligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> csrEligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> eligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> eligibilityEndDates = new HashSet<DateTime>();
		for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypeList) {
			applicantId = ((PersonType) insuranceApplicantType.getRoleOfPersonReference().getRef()).getId();
			eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
			isPrimaryTaxFiler = StringUtils.equalsIgnoreCase(primaryTaxFilerApplicantId, applicantId);
			isPrimaryContact = StringUtils.equalsIgnoreCase(primaryContactApplicantId, applicantId);
			reasonCode.clear();
			if (eligibilityTypeList != null) {
				Boolean exchangeEligible = null;
				Boolean aptcEligible = null;
				Boolean csrEligible = null;
				Boolean medicaidEligible = null;
				Boolean chipEligible = null;
				EligibilityType aptcEligibilityType = null;
				EligibilityType csrEligibilityType = null;
				
				for (EligibilityType eligibilityType : eligibilityTypeList) {
					eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)) {
						aptcEligibilityType = eligibilityType;
						//aptcEligible = validateAptcDetails(eligibilityType, applicantId, isPrimaryTaxFiler,isPrimaryContact, errorList, aptcCheck, reasonCode, eligibilityStartDates, eligibilityEndDates);
					}

					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)) {
						csrEligibilityType = eligibilityType;
						//csrEligible = validateCsrDetails(eligibilityType, applicantId, errorList, csrCheck, eligibilityStartDates, eligibilityEndDates);
						//Removing validation for restricting multiple native or non native CS level as per requirement HIX-111247
						//validateCSRAlphaCategoryCodes(eligibilityType, errorList, nativeAmericancsrAplhaCategoryCode, nonNativeAmericancsrAplhaCategoryCode, invalidNACsrCategoryCodes, invalidNonNACsrCategoryCodes);
					}
					
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE)) {
						exchangeEligible = isEligible(eligibilityType);
					}
					
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.MEDICAID_MAGI_ELIGIBILITY_TYPE)) {
						medicaidEligible = isEligible(eligibilityType);
					}
					
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CHIP_ELIGIBILITY_TYPE)) {
						chipEligible = isEligible(eligibilityType);
					}
				}
				//below if condition is for both ID and other states
				if(exchangeEligibilityEnabled.equalsIgnoreCase("false") || exchangeEligible!=null && exchangeEligible){
				    aptcEligible = validateAptcDetails(aptcEligibilityType, applicantId, isPrimaryTaxFiler,isPrimaryContact, errorList, aptcCheck, reasonCode, aptcEligibilityStartDates, eligibilityEndDates);
					csrEligible = validateCsrDetails(csrEligibilityType, applicantId, errorList, csrCheck, csrEligibilityStartDates, eligibilityEndDates);
				}
				if (aptcCheck[1] == false && (isPrimaryTaxFiler || isPrimaryContact)) {
					final APTCEligibilityType aptc = (APTCEligibilityType) aptcEligibilityType;
				
					final BigDecimal maxAPTCAmount = aptc != null ? aptc.getAPTC() != null ? aptc.getAPTC().getAPTCMaximumAmount() != null ? aptc.getAPTC().getAPTCMaximumAmount().getValue() : null : null : null;
					if (maxAPTCAmount != null && maxAPTCAmount.doubleValue() >= 0) {
						aptcCheck[1] = true;
					}
				}
				if (exchangeEligible!=null && !exchangeEligible && aptcEligible!=null && !aptcEligible && 
						csrEligible != null && csrEligible && medicaidEligible != null && medicaidEligible) {
					errorList.add(ReferralUtil.formatMessage(MSG_CSR_MEDICAID_INVALID, new Object[] { applicantId }));
				}
				
				if (exchangeEligible!=null && !exchangeEligible && aptcEligible!=null && !aptcEligible && 
						csrEligible != null && csrEligible && chipEligible != null && chipEligible) {
					errorList.add(ReferralUtil.formatMessage(MSG_CSR_CHIP_INVALID, new Object[] { applicantId }));
				}
			}
		}

		
		if (aptcEligibilityStartDates.size() > 1){
			errorList.add(MSG_MULTIPLE_APTC_ELIGIBILITY_START_DATES_FOUND);
		}
		if (csrEligibilityStartDates.size() > 1){
			errorList.add(MSG_MULTIPLE_CSR_ELIGIBILITY_START_DATES_FOUND);
		}
		
		//HIX-111936 - Remove eligibility end date validations in the AT processing
		/*
		if (eligibilityEndDates.size() > 1){
			errorList.add(MSG_MULTIPLE_APTC_AND_CSR_ELIGIBILITY_END_DATES_FOUND);
		}*/

		//HIX-112466 Remove 1st of month validation for aptc csr eligibility dates.
/*		for (DateTime dateTime : eligibilityStartDates) {
			if (dateTime.getDayOfMonth() != 1){
				errorList.add(MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_NOT_FIRST_OF_A_MONTH);
				break;
			}
		}
*/
		eligibilityStartDates.addAll(aptcEligibilityStartDates);
		eligibilityStartDates.addAll(csrEligibilityStartDates);
		ApplicationExtensionType applicationExtension = request.getInsuranceApplication().getApplicationExtension();
		if (applicationExtension != null
				&& applicationExtension.getCoverageYear() != null
					&& applicationExtension.getCoverageYear().getValue() != null) {
			int coverageYear = applicationExtension.getCoverageYear().getValue().getYear();

			for (DateTime dateTime : eligibilityStartDates) {
				if (dateTime.getYear() != coverageYear){
					errorList.add(ReferralUtil.formatMessage(MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_OUTSIDE_COVERAGE_YEAR, new Object[] { String.valueOf(coverageYear) }));
					break;
				}
			}

			//HIX-111936 - Remove eligibility end date validations in the AT processing
			/*for (DateTime dateTime : eligibilityEndDates) {
				if (dateTime.getYear() != coverageYear){
					errorList.add(ReferralUtil.formatMessage(MSG_APTC_AND_CSR_ELIGIBILITY_END_DATE_IS_OUTSIDE_COVERAGE_YEAR, new Object[] { String.valueOf(coverageYear) }));
					break;
				}
			}*/
		}

		if (aptcCheck[0] && !aptcCheck[1]) {
			errorList.add(MSG_APTC_FINAL_AMOUNT);
		}

		if (csrCheck[0] && !csrCheck[1]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "CSR Level" }));
		}
		
		/*if (invalidNACsrCategoryCodes != null && invalidNACsrCategoryCodes.size() > 0) {
			errorList.add(MSG_NATIVE_AMR_CSR + nativeAmericancsrAplhaCategoryCode.toString() + " " + invalidNACsrCategoryCodes.toString());
		}

		if (invalidNonNACsrCategoryCodes != null && invalidNonNACsrCategoryCodes.size() > 0) {
			errorList.add(MSG_NON_NATIVE_AMR_CSR + nonNativeAmericancsrAplhaCategoryCode.toString() + " " + invalidNonNACsrCategoryCodes.toString());
		}*/
	}

	private PersonType extractSignerApplicantId(AccountTransferRequestPayloadType request) {
		return (PersonType) request.getInsuranceApplication().getSSFSigner().getRoleOfPersonReference().getRef();
	}

	private boolean isEligible(EligibilityType eligibilityType) {
		return (eligibilityType.getEligibilityIndicator() != null && eligibilityType.getEligibilityIndicator().isValue());
	}

	private boolean eligibilityDateRange(EligibilityType eligibilityType,
			Set<DateTime> eligibilityStartDates, Set<DateTime> eligibilityEndDates, boolean blnEligible) {
		boolean blnDateRange = false;
		if (eligibilityType.getEligibilityDateRange() != null) {
			blnDateRange = eligibilityType.getEligibilityDateRange().getStartDate() != null
					&& isValidDate(eligibilityType.getEligibilityDateRange().getStartDate().getDate());

			if (blnDateRange && blnEligible){
				eligibilityStartDates.add(getDate(eligibilityType.getEligibilityDateRange().getStartDate().getDate()));
			}
			blnDateRange = blnDateRange && eligibilityType.getEligibilityDateRange().getEndDate() != null && isValidDate(eligibilityType.getEligibilityDateRange().getEndDate().getDate());
			if (blnDateRange){
				eligibilityEndDates.add(getDate(eligibilityType.getEligibilityDateRange().getEndDate().getDate()));
			}
			blnDateRange = blnDateRange && (ReferralUtil.compareDate(ReferralUtil.extractDate(eligibilityType.getEligibilityDateRange().getStartDate()), ReferralUtil.extractDate(eligibilityType.getEligibilityDateRange().getEndDate())) >= 0);
		}

		return blnDateRange;
	}

	private boolean validateAptcDetails(EligibilityType eligibilityType, String applicantId, boolean isPrimaryTaxFiler, boolean isPrimaryContact, List<String> errorList, boolean aptcCheck[], Set<String> reasonCode,
			Set<DateTime> aptcEligibilityStartDates, Set<DateTime> eligibilityEndDates) {
		final boolean blnEligible = isEligible(eligibilityType);

		if (blnEligible) {
			aptcCheck[0] = true;
		}

		final boolean blnDateRange = eligibilityDateRange(eligibilityType, aptcEligibilityStartDates, eligibilityEndDates,blnEligible);

		if (!blnDateRange) {
			errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "APTC Eligibility Date Range", applicantId }));
		}

		if (eligibilityType.getEligibilityReasonText() != null && eligibilityType.getEligibilityReasonText().getValue() != null) {
			reasonCode.add(eligibilityType.getEligibilityReasonText().getValue());
		} else {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "APTC Eligibility Reason Text", applicantId }));
		}

		return blnEligible;
	}

	private void validateCSRAlphaCategoryCodes(EligibilityType eligibilityType, List<String> errorList, Set<String> nativeAmericancsrAplhaCategoryCode, Set<String> nonNativeAmericancsrAplhaCategoryCode, List<String> invalidCsrCategoryCodes,
	        List<String> invalidNonNACsrCategoryCodes) {

		CSREligibilityType csr = (CSREligibilityType) eligibilityType;
		final String csrLevel = csr != null ? csr.getCSRAdvancePayment() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue() : null : null : null;
		if (csrLevel != null) {
			if (NativeAmericanCSREnum.value(csrLevel) != null) {
				if (nativeAmericancsrAplhaCategoryCode.size() == 0) {
					nativeAmericancsrAplhaCategoryCode.add(csrLevel);
				} else if (!(nativeAmericancsrAplhaCategoryCode.contains(csrLevel))) {

					invalidCsrCategoryCodes.add(csrLevel);
				}
			}

			if (NonNativeAmericanCSREnum.value(csrLevel) != null) {
				if (nonNativeAmericancsrAplhaCategoryCode.size() == 0) {
					nonNativeAmericancsrAplhaCategoryCode.add(csrLevel);
				} else if (!(nonNativeAmericancsrAplhaCategoryCode.contains(csrLevel))) {
					invalidNonNACsrCategoryCodes.add(csrLevel);
				}
			}

		}
	}

	private boolean validateCsrDetails(EligibilityType eligibilityType, String applicantId, List<String> errorList, boolean csrCheck[],
			Set<DateTime> eligibilityStartDates, Set<DateTime> eligibilityEndDates) {
		final boolean blnEligible = isEligible(eligibilityType);
		final CSREligibilityType csr = (CSREligibilityType) eligibilityType;
		if (blnEligible) {
			final String csrLevel = csr != null ? csr.getCSRAdvancePayment() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue() : null : null : null;
			csrCheck[0] = true;
			if (csrLevel != null) {
				csrCheck[1] = true;
				if (CSR_LIST.indexOf(csrLevel) == -1) {
					errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "CSR Level", applicantId }));
				}
			}
			/* For CSR, Date check for all members only if their eligibility is true, as DHW is not sending this info for in-eligible members */
			final boolean blnDateRange = eligibilityDateRange(eligibilityType, eligibilityStartDates, eligibilityEndDates,blnEligible);

			if (!blnDateRange) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "CSR Eligibility Date Range", applicantId }));
			}
		}

		return blnEligible;
	}

	@Override
	boolean isZipValidationApplicable(String state) {
		return StringUtils.equalsIgnoreCase(state, IDAHO);
	}

	/**
	 * validate Authorized Representative Organization Identification ID.
	 */
	private void validateAuthRepsOrgaIdenId(AccountTransferRequestPayloadType accountTransferRequestPayloadType, List<String> errorList) {

		final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
		if (StringUtils.isBlank(hasLinkUserStr) || !"true".equalsIgnoreCase(hasLinkUserStr)) {
			return;
		}

		String identificationID = null;

		if (null != accountTransferRequestPayloadType.getAuthorizedRepresentative()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID().getValue()) {

			identificationID = accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
					.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID().getValue().trim();
		}

		if (StringUtils.isBlank(identificationID)) {
			// HIX-112624-relax-the-validation-rule-on Auth Rep
			//errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "AuthorizedRepresentative:OrganizationIdentification Identification ID" }));
		}
		else {
			AccountUser linkedUser = iUserRepository.findByExtnAppUserId(identificationID);

			if (null == linkedUser) {
				errorList.add(ReferralUtil.formatMessage(MSG_NO_PROVISION_AUTO_REP, new Object[] { "AuthorizedRepresentative:OrganizationIdentification Identification ID", identificationID }));
			}
		}
	}
	
	private String extractPrimaryContactExternalApplicantId(AccountTransferRequestPayloadType request) {
		PersonType primaryContact = (PersonType) request.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
		List<PersonType> personList = request.getPerson();
		String identificationId = null;
		
		for (PersonType personType : personList) {
			if(personType.getId().equalsIgnoreCase(primaryContact.getId())){
				if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
					break;
				}
			}
		}
		return identificationId;
	}
	
	private String extractPrimaryTaxFilerReferenceId(AccountTransferRequestPayloadType request) {
		String primaryTaxFilerReferenceId="";
		final int size = ReferralUtil.listSize(request.getTaxReturn());
		if (size != 0) {
			final TaxReturnType taxReturnType = request.getTaxReturn().get(0);
			if (taxReturnType.getTaxHousehold() == null) {
				return "";
			}
			final TaxHouseholdType taxHouseholdType = taxReturnType.getTaxHousehold();


			final List<?> taxFilerList = taxHouseholdType.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent();
			final int tfSize = ReferralUtil.listSize(taxFilerList);
			JAXBElement<?> taxFilerType = null;

			// Populate Primary Taxfiler & Dependents
			for (int i = 0; i < tfSize; i++) {
				taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
				if (taxFilerType != null) {
					if (taxFilerType.getName().getLocalPart().equals(PRIMARYTAXFILER)) {
						JAXBElement<TaxFilerType> primaryTaxFiler = (JAXBElement<TaxFilerType>)taxFilerType;
						if (primaryTaxFiler.getValue() != null && primaryTaxFiler.getValue().getRoleOfPersonReference() != null) {
							primaryTaxFilerReferenceId = ((PersonType) primaryTaxFiler.getValue().getRoleOfPersonReference().getRef()).getId();	
						}
					}
				}
			}
		}
		return primaryTaxFilerReferenceId;
	}

	private String extractPrimaryContactReferenceId(AccountTransferRequestPayloadType request) {
		return ((PersonType) request.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef()).getId();
		
	}
	
	private void validateCoverageSeekingApplicants(AccountTransferRequestPayloadType request, List<String> errorList) {
		final List<String> insuranceApplicants = extractInsuranceApplicantIds(request);
		final List<PersonType> listPersonType = request.getPerson();
		PersonType person = null;
		final int size = ReferralUtil.listSize(listPersonType);
		for (int i = 0; i < size; i++) {
			person = listPersonType.get(i);
			if (person == null) {
				continue;
			}
			if (null != person.getPersonSeekingCoverageIndicator() && person.getPersonSeekingCoverageIndicator().isValue()
					&& insuranceApplicants.indexOf(person.getId()) == -1) {
				errorList.add(ReferralUtil.formatMessage(MSG_MISSING_COVERAGE_SEEKING_INSURANCE_APPLICANT, new Object[]{person.getId()}));
			}
		}
	}
	
	private List<String> extractInsuranceApplicantIds(AccountTransferRequestPayloadType source) {
		List<String> insuranceApplicantIdsList = new ArrayList<String>();
		final List<InsuranceApplicantType> insuranceApplicantList = source.getInsuranceApplication().getInsuranceApplicant();
		final int size = ReferralUtil.listSize(insuranceApplicantList);
		InsuranceApplicantType insuranceApplicant;
		PersonType personType;

		for (int i = 0; i < size; i++) {
			insuranceApplicant = insuranceApplicantList.get(i);
			if (insuranceApplicant.getRoleOfPersonReference() != null) {
				if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {
					personType = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
					insuranceApplicantIdsList.add(personType.getId());
				}
			}
		}

		return insuranceApplicantIdsList;
	}
}
