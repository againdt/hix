package com.getinsured.hix.ssap.repository;

import java.util.List;

import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmDocuments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ConsumerDocument;

public interface SsapApplicantDocumentRepository extends JpaRepository<ConsumerDocument, Long> {
	
	@Query("select doc from ConsumerDocument doc where targetId = :applicantID and targetName = 'SSAP_APPLICANTS'")
	List<ConsumerDocument> getDocumentsForSsapApplicantId(@Param("applicantID") Long applicantID);
	
	@Query("select doc from ConsumerDocument doc where targetId = :applicantID and documentCategory = :documentCategory and targetName = 'SSAP_APPLICANTS'")
	List<ConsumerDocument> getDocumentsForSsapApplicantIdAndDocumentCategory(@Param("applicantID") Long applicantID,@Param("documentCategory") String documentCategory);

	@Query("SELECT td FROM TkmDocuments td WHERE td.documentPath = :ecmDocumentID")
	TkmDocuments findTkmDocumentForConsumerDocumentID(@Param("ecmDocumentID") String ecmDocumentID);
}
