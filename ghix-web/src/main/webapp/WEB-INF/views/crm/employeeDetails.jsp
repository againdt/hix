<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">
 
</script>
<!--start page-breadcrumb -->
<div class="gutter10">
<div class="row-fluid">	
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employee/searchemployee"/>">Employees</a></li>
			<li>Basic Informations</li>
		</ul> 
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
		<h1>
		${employee.name} <small class="margin20-l">Contact Name:
			${employeeDetail.firstName} ${employeeDetail.lastName} </small>
	</h1>
	</div>
	<!--page-breadcrumb ends-->
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
			<h4>About this Employee</h4>
			</div>
			<ul class="nav nav-list">

				<li class="active navmenu" >Basic Information</li>
				<li ><a href="/hix/crm/crmemployee/comments/${employeeId}">Comments</a></li>
				<li class="navmenu"><a href="/hix/crm/crmemployee/ticket/history/${employeeId}">Ticket History</a></li>
				<li class="navmenu"><a href="/hix/crm/crmemployee/securityQuestions/${employeeId}">Security Questions</a></li>
	
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> Actions</h4>
			</div>
			<ul class="nav nav-list">
			<c:choose>
			<c:when test="${employee.status == 'TERMINATED'}">
			<li class="navmenu disabled"><a href="#" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:when>
			<c:otherwise>
			<c:if test="${checkDilogEmp == null}"> 
			<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<c:if test="${checkDilogEmp != null}">  
			<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employee&switchToModuleId=${employeeId}&switchToResourceName=${employee.name}" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<li class="navmenu"><a href="<c:url value="/crm/crmemployee/passwordreset"/>/${employeeId}?sourcePage=details" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
			</c:otherwise>
			</c:choose>
			
				
                <!-- <li class="navmenu disabled"><a href="#" ><i class="icon-envelope"></i>Compose Message</a></li>
                <li class="navmenu disabled"><a href="#"><i class="icon-comment"></i>New Comment</a></li> -->
            </ul>    
		</div>
		
		<div id="rightpanel" class="span9">
			<div class="header">
                 <h4>Basic Information</h4>
            </div>
            <div class="gutter10">
			<form class="form-horizontal" id="" name="" action="" method="POST">
		              <df:csrfToken/>
				<table class="table table-border-none">
					<!-- <thead>
						<tr class="graydrkbg">
							<th class="span3"><strong>Summary</strong></th>
							<th></th>
						</tr>
					</thead> -->
					<tbody>
						<tr>
							<td class="txt-right span4">Name</td>
							<td><strong>${employee.name}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Address</td>
							<td><strong>
											${employeeDetail.location.address1} <br />
											${employeeDetail.location.address2}
											${employeeDetail.location.city}
											${employeeDetail.location.state}
											${employeeDetail.location.zip}
									</strong></td>
						</tr>
		
						
	     				<tr>
							<td class="txt-right">Gender</td>
							<td><strong>${employeeDetail.gender}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Date Of Birth</td>
							<td><strong>${employeeDetail.dob}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">SSN</td>
							<td><strong><c:if
	                            test="${not empty employeeDetail.ssn}">
	                            <c:set var="myValue">${employeeDetail.ssn}</c:set>
	                            <c:set var="ssnLength">${fn:length(myValue)}</c:set>
	                            <c:set var="replacessn">${fn:substring(employeeDetail.ssn, 0, 6)}</c:set>
	                           ${fn:replace(employeeDetail.ssn,replacessn, '***-**')} 
	                       </c:if>
	                       </strong>
							</td>
						</tr>
						<tr>
							<td class="txt-right">Email Address</td>
							<td><strong>${employeeDetail.email}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Phone Number</td>
							<td><strong>${employeeDetail.contactNumber}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Tobacco User</td>
							<td><strong>${employeeDetail.smoker}</strong></td>
						</tr>
						
						<tr>
							<td class="txt-right">Native American</td>
							<td><strong>${employeeDetail.nativeAmr}</strong></td>
						</tr>
						<tr>
									<td class="txt-right vertical-align-top">Worksite
										</td>
									<td><strong>
											${employee.employer.locations[0].location.address1} <br />
											${employee.employer.locations[0].location.address2}
											${employee.employer.locations[0].location.city}
											${employee.employer.locations[0].location.state}
											${employee.employer.locations[0].location.zip}
									</strong></td>
								</tr>
						
						<%-- <tr class="mshide">
							<td class="txt-right">Estimated Annual Salary</td>
							<td><strong>${employee.yearlyIncome}</strong></td>
						</tr> --%>
					</tbody>
				</table>
				<input type="hidden" name="employeeName" id="employeeName" 
				value="${employee.name}" />
			</form>
			</div>
		</div>
		

	</div>
	<!-- row-fluid -->
</div>
<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal -->
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employee/home" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="pull-left">View Employee Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">&times;</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employee View" will take to the Employee's portal for ${employee.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employee.<br/>
					Proceed to Employee view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployeeView" name="checkEmployeeView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary" type="submit">Employee View</button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employee" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employeeId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employee.name}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>