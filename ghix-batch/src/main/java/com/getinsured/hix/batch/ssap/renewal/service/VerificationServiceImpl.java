package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.util.TSDate;

/**
 * Class is used to provide services for Verification Job.
 * 
 * @since August 12, 2019
 */
@Service("verificationService")
public class VerificationServiceImpl implements VerificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationServiceImpl.class);
	private static final String FAILURE = "failure";
	private static final String SUCCESS = "success";

	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GIMonitorService giMonitorService;

	/**
	 * Method is use to save and throws Verification Job Error.
	 */
	@Override
	public synchronized void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog(ERROR_CODE, new TSDate(), this.getClass().getName(),
				errorMessage, null, errorMessage, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}

	/**
	 * Method is use to save and throws Verification Job Error.
	 */
	@Override
	public synchronized Integer logToGIMonitor(Exception e, String caseNumber) {

		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog(ERROR_CODE, new Date(), this.getClass().getName(),
				e.getLocalizedMessage() + "\n" + e.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(e), null,
				caseNumber, GIRuntimeException.Component.BATCH.getComponent(), null);

		if (giMonitor != null) {
			giMonitorId = giMonitor.getId();
		}
		return giMonitorId;
	}

	/**
	 * Method is used to get SSAP Applications by Coverage year, Status and Batch size.
	 */
	@Override
	public synchronized List<Long> getSsapApplicationsByCoverageYearAndStatus(long coverageYear, List<String> applicationStatusList, Long batchSize) {
		return ssapCloneApplicationService.getSsapApplicationsByCoverageYearAndApplicationStatusList(coverageYear, applicationStatusList, batchSize);
	}

	/**
	 * Verification method of SSAP application.
	 */
	@Override
	public synchronized String verificationSsap(Long applicationId) {

		String responseMessage = FAILURE;
		LOGGER.debug("Invoking the SSAP orchestration flow");

		try {
			ghixRestTemplate.exchange(GhixEndPoints.SsapIntegrationEndpoints.RENEWAL_INTEGRATION_URL,
					"exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_XML, String.class,
					String.valueOf(applicationId));
			responseMessage = SUCCESS;
		}
		catch (Exception e) {
			LOGGER.error("Exception in invoke SSAP Integration", e);
			logToGIMonitor(e, Long.toString(applicationId));
			responseMessage = FAILURE;
		}
		return responseMessage;
	}
}
