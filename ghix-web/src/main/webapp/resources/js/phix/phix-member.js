/*
 * Custom script for PHIX home page
 */

$(function(){
	
	//Eligibility Home Member model
	PhixMemberModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			dateOfBirth: "",
			isTobaccoUser : "N",
			isSeekingCoverage : "Y",
      	}
		
	});
	
	//Members Collection
	PhixMemberCollection = Backbone.Collection.extend({
		model : PhixMemberModel
	});
	
	
	
	//Creating Collection
	phixHouseholdMembers = new PhixMemberCollection();
	
});