<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<script type="text/javascript">
var dataToPopulate="${applicationJson}";
var showPermissionToCall = "${showPermissionToCall}";

var theUrlForAddconsumersubmit = '<c:url value="/cap/consumer/addconsumersubmit"></c:url>';
var csrf='${df:csrfTokenParameter()}';
var csrfValue='<df:csrfToken plainToken="true" />';
function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
	  alert($('Session is invalid').text());
	  rv = true;
	 }
	return rv;
}
</script>
<df:csrfToken/>
<div ng-app="capApp">
 	<div ng-controller="mainController">
  		<div spinning-loader="loader"></div>
		<div class="gutter10"  id="addConsumer">
		    <div class="row-fluid">
		        <h1 id="skip">Create Individual Record</h1>
		    </div>
		    <div class="row-fluid">
		        <div class="span3" id="sidebar">
		            <div class="header">
		                <h4> About Individual </h4>
		            </div>
		            <div class="gutter10">
		                <p>Enter information for the individual to create a record prior to acting on the individual's behalf.</p>    
		            </div>
		      	</div>
				<div class="span9" id="rightpanel">
			    	<div class="row-fluid">
			            <div class="header margin20-b">
			                <h4>Individual Information</h4>
			            </div>
	            		<div class="gutter10">
	            		<input type="hidden" name="csrReferralFlow" id="csrReferralFlow" value="${csrReferralFlow}"/>
			            <input type ="hidden" name="showPermissionToCall" id="showPermissionToCall" value="${showPermissionToCall}" >
			            	<ng-include src="'<c:url value="/resources/html/cap/partials/addConsumer.html "/>'"> </ng-include>
			            	
			            </div>
			        </div>
			   	</div>
			</div>
		</div>
		
      	
      	
 	</div>
 </div>

<script src="<gi:cdnurl value="/resources/angular/mask.js"/>"></script> 
<script src="<gi:cdnurl value="/resources/js/cap/cap.js"/>"></script> 
<script src="<gi:cdnurl value="/resources/js/parsley.2.0.js" />"></script>


<script>
window.ParsleyConfig = {
 successClass: "has-success",
 errorClass: "has-error",
 classHandler: function (el) {
   return el.$element.closest(".control-group");
 },
 errorsContainer: function (el) {
   return el.$element.closest(".control-group");
 },
 errorsWrapper: "<div class='error'></div>",
 errorTemplate: "<div></div>"
};

</script>


<script>
$(window).load(function(){
	$("#frmaddconsumer").parsley();
	$('#mainSubmitButton[disabled]').on('click', function(event){
		event.preventDefault();
	});

});	

</script>