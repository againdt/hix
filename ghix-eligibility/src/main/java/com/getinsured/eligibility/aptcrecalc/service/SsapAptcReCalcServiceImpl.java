package com.getinsured.eligibility.aptcrecalc.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.aptcrecalc.dto.Request;
import com.getinsured.eligibility.aptcrecalc.dto.Response;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

@Service("ssapaptcrecalcservice")
public class SsapAptcReCalcServiceImpl implements SsapAptcReCalcService {

	//private static final GhixLogger LOGGER = GhixLogFactory.getLogger(SsapAptcReCalcServiceImpl.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapAptcReCalcServiceImpl.class);
	
	@Autowired private Gson platformGson;	
	private static final String INPUT_COVERAGE_START_DATE = "coverageStartDate";
	private static final String INPUT_EXTERNAL_CASE_ID = "externalCaseId";
	private static final String INPUT_ENROLLMENT_YEAR_KEY = "enrollmentYear";
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private RestTemplate restTemplate;
	@Autowired public GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Override
	public Response callAhbxReCalcApi(Map<String,String> inputData,SsapApplication latestSsapApplication) throws Exception{
		try{
			Response response = null;
			String requestJson = this.formReCalcRequst(inputData,latestSsapApplication);
			String responseJson = callAhbxReCalcApi(requestJson);
			if(responseJson != null){
				response = platformGson.fromJson(responseJson, Response.class);
			}
			return response;	
		}
		catch(Exception e){
			throw e;
		}
	}
	
	private String formReCalcRequst(Map<String,String> inputData,SsapApplication latestSsapApplication) throws Exception {
		Request ahbxRequest = new Request();
		String encryptHouseholdCaseId =  ghixJasyptEncrytorUtil.encryptStringByJasypt(inputData.get(INPUT_EXTERNAL_CASE_ID));
		ahbxRequest.setAhbxCaseId(encryptHouseholdCaseId);	
		String coverageStartDate = null;	
		try{
			DateFormat yymmddFormat = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat mmddyyyyFormat = new SimpleDateFormat("MM/dd/yyyy");
			coverageStartDate = yymmddFormat.format(mmddyyyyFormat.parse(inputData.get(INPUT_COVERAGE_START_DATE))); 
		}catch(Exception e){
			throw new Exception("Date conversion failed from dd-MM-yyyy to yyyy-MM-dd");
		}
		       
		
		ahbxRequest.setCoverageStartDate(coverageStartDate);
		ahbxRequest.setEnrlYear(inputData.get(INPUT_ENROLLMENT_YEAR_KEY));
		ahbxRequest.setEnrollmentDataDtoListMap(this.getEnrollmentData(inputData.get(INPUT_EXTERNAL_CASE_ID), inputData.get(INPUT_ENROLLMENT_YEAR_KEY)));
		
		return platformGson.toJson(ahbxRequest);
	}
	
	private Map<String, List<EnrollmentDataDTO>> getEnrollmentData(String externalCaseId, String coverageYear){
		
		Map<String, List<EnrollmentDataDTO>> enrollmentDataListMap = null;
		List<String> externalCaseIdList = new ArrayList<String>();
		externalCaseIdList.add(externalCaseId);
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setExternalCaseIdList(externalCaseIdList);
		enrollmentRequest.setPremiumDataRequired(true);
		enrollmentRequest.setCoverageYear(coverageYear);
		ResponseEntity<String> res = ghixRestTemplate.exchange(
				GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_EXTERNAL_CASE_ID,
				ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
				platformGson.toJson(enrollmentRequest));
		if (res != null && res.getBody() != null) {
			EnrollmentResponse response = platformGson.fromJson(res.getBody(), EnrollmentResponse.class);					
			 if(response != null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response.getStatus())) {
				 if(response.getEnrollmentDataDtoListMap() != null){
					 enrollmentDataListMap = response.getEnrollmentDataDtoListMap();
				 }				 
			 }
		}
		return enrollmentDataListMap;
	}

		
	private String callAhbxReCalcApi(String requestJson){
		HttpHeaders headers = new HttpHeaders();
		String responseJson = "";
		String aptcRecalc = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_AHBX_APTC_RECALC_ENABLE);
		if("ON".equalsIgnoreCase(aptcRecalc)){
			//String url="http://localhost:8080/ghix-eligibility/ssapaptcrecalc/testAhbx";
			String url=GhixConstants.AHBX_RECALC_API_URL;
			LOGGER.info("AHBX_RECALC_API_URL = {}", url);
			LOGGER.info("requestJson = {}", requestJson);
		   // headers.set("X-APP-ID", "U0eZ2Wmh_2Qyj4Eeomw2bqTCddIulEexivj_aFqrbtQ");
			headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<String> requestEntity = new HttpEntity<String>(requestJson, headers);
		    LOGGER.info("requestEntity = {}", requestEntity);
		    
		    HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);
		    LOGGER.info("response = " + response);
		    if(response != null && response.getBody() != null){
		    	responseJson = response.getBody();	
		    }
		    LOGGER.info("responseJson {} ", responseJson);
		}
		else{
			Response response = new Response();
			response.setStatus("SUCCESS");
			responseJson = platformGson.toJson(response);
		}
		return responseJson;
	}

}
