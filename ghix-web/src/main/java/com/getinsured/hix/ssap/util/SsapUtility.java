package com.getinsured.hix.ssap.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.getinsured.hix.ssap.models.AmericanAlaskaTribe;


public class SsapUtility {
	
	private static final int _12 = 12;

	private static final int _30 = 30;

	private static final int _3 = 3;

	private static final int _40 = 40;

	private static final int _8 = 8;

	private static final int _23 = 23;

	private static final int _19 = 19;

	private static final int _6 = 6;

	private static final int _7 = 7;

	private static final int _9 = 9;

	private static final int _13 = 13;

	private static final int _4 = 4;

	private static final int _2 = 2;

	private static final int _120 = 120;

	private static final int _21 = 21;

	private static final int _260 = 260;

	private static final int _1 = 1;
	
	private static final int _5 = 5;

	private static Logger ssapUtilityLogger = Logger.getLogger(SsapUtility.class);
	
	private static Map<String,List<AmericanAlaskaTribe>> americanAlaskaStateTribeMap = new HashMap<String,List<AmericanAlaskaTribe>>();
	
	private SsapUtility(){
		
	}

static{
		
		List<AmericanAlaskaTribe> alabamaStateList = new ArrayList<AmericanAlaskaTribe>(_1);
		alabamaStateList.add(new AmericanAlaskaTribe("AL010001","Poarch Band of Creek Indians of Alabama")); 
		americanAlaskaStateTribeMap.put("AL", alabamaStateList);
		
		List<AmericanAlaskaTribe> alaska = new ArrayList<AmericanAlaskaTribe>(_260);
		alaska.add(new AmericanAlaskaTribe("AK020002","Native Village of Afognak (formerly the Village of Afognak)"));
		alaska.add(new AmericanAlaskaTribe("AK020003","Agdaagux Tribe of King Cove"));
		alaska.add(new AmericanAlaskaTribe("AK020004","Native Village of Akhiok"));
		alaska.add(new AmericanAlaskaTribe("AK020005","Akiachak Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020006","Akiak Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020007","Native Village of Akutan"));
		alaska.add(new AmericanAlaskaTribe("AK020008","Village of Alakanuk"));
		alaska.add(new AmericanAlaskaTribe("AK020009","Alatna Village"));
		alaska.add(new AmericanAlaskaTribe("AK020010","Native Village of Aleknagik"));
		alaska.add(new AmericanAlaskaTribe("AK020011","Algaaciq Native Village (St. Mary's)"));
		alaska.add(new AmericanAlaskaTribe("AK020012","Allakaket Village"));
		alaska.add(new AmericanAlaskaTribe("AK020013","Native Village of Ambler"));
		alaska.add(new AmericanAlaskaTribe("AK020014","Village of Anaktuvuk Pass"));
		alaska.add(new AmericanAlaskaTribe("AK020015","Yupiit of Andreafski"));
		alaska.add(new AmericanAlaskaTribe("AK020016","Angoon Community Association"));
		alaska.add(new AmericanAlaskaTribe("AK020017","Village of Aniak"));
		alaska.add(new AmericanAlaskaTribe("AK020018","Anvik Village"));
		alaska.add(new AmericanAlaskaTribe("AK020019","Arctic Village (See Native Village of Venetie Tribal Government)"));
		alaska.add(new AmericanAlaskaTribe("AK020020","Asa'carsarmiut Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020021","Native Village of Atka"));
		alaska.add(new AmericanAlaskaTribe("AK020022","Village of Atmautluak"));
		alaska.add(new AmericanAlaskaTribe("AK020023","Atqasuk Village (Atkasook)"));
		alaska.add(new AmericanAlaskaTribe("AK020024","Native Village of Barrow Inupiat Traditional Government"));
		alaska.add(new AmericanAlaskaTribe("AK020025","Beaver Village"));
		alaska.add(new AmericanAlaskaTribe("AK020026","Native Village of Belkofski"));
		alaska.add(new AmericanAlaskaTribe("AK020027","Village of Bill Moore's Slough"));
		alaska.add(new AmericanAlaskaTribe("AK020028","Birch Creek Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020029","Native Village of Brevig Mission"));
		alaska.add(new AmericanAlaskaTribe("AK020030","Native Village of Buckland"));
		alaska.add(new AmericanAlaskaTribe("AK020031","Native Village of Cantwell"));
		alaska.add(new AmericanAlaskaTribe("AK020032","Native Village of Chenega (aka Chanega)"));
		alaska.add(new AmericanAlaskaTribe("AK020033","Chalkyitsik Village"));
		alaska.add(new AmericanAlaskaTribe("AK020034","Cheesh-Na Tribe (formerly the Native Village of Chistochina)"));
		alaska.add(new AmericanAlaskaTribe("AK020035","Village of Chefornak"));
		alaska.add(new AmericanAlaskaTribe("AK020036","Chevak Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020037","Chickaloon Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020038","Chignik Bay Tribal Council (formerly the Native Village of Chignik)"));
		alaska.add(new AmericanAlaskaTribe("AK020039","Native Village of Chignik Lagoon"));
		alaska.add(new AmericanAlaskaTribe("AK020040","Chignik Lake Village"));
		alaska.add(new AmericanAlaskaTribe("AK020041","Chilkat Indian Village (Klukwan)"));
		alaska.add(new AmericanAlaskaTribe("AK020042","Chilkoot Indian Association (Haines)"));
		alaska.add(new AmericanAlaskaTribe("AK020043","Chinik Eskimo Community (Golovin)"));
		alaska.add(new AmericanAlaskaTribe("AK020044","Native Village of Chitina"));
		alaska.add(new AmericanAlaskaTribe("AK020045","Native Village of Chuathbaluk (Russian Mission, Kuskokwim)"));
		alaska.add(new AmericanAlaskaTribe("AK020046","Chuloonawick Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020047","Circle Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020048","Village of Clarks Point"));
		alaska.add(new AmericanAlaskaTribe("AK020049","Native Village of Council"));
		alaska.add(new AmericanAlaskaTribe("AK020050","Craig Community Association"));
		alaska.add(new AmericanAlaskaTribe("AK020051","Village of Crooked Creek"));
		alaska.add(new AmericanAlaskaTribe("AK020052","Curyung Tribal Council"));
		alaska.add(new AmericanAlaskaTribe("AK020053","Native Village of Deering"));
		alaska.add(new AmericanAlaskaTribe("AK020054","Native Village of Diomede (aka Inalik)"));
		alaska.add(new AmericanAlaskaTribe("AK020055","Village of Dot Lake"));
		alaska.add(new AmericanAlaskaTribe("AK020056","Douglas Indian Association"));
		alaska.add(new AmericanAlaskaTribe("AK020057","Native Village of Eagle"));
		alaska.add(new AmericanAlaskaTribe("AK020058","Native Village of Eek"));
		alaska.add(new AmericanAlaskaTribe("AK020059","Egegik Village"));
		alaska.add(new AmericanAlaskaTribe("AK020060","Eklutna Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020061","Native Village of Ekuk"));
		alaska.add(new AmericanAlaskaTribe("AK020062","Ekwok Village"));
		alaska.add(new AmericanAlaskaTribe("AK020063","Native Village of Elim"));
		alaska.add(new AmericanAlaskaTribe("AK020064","Emmonak Village"));
		alaska.add(new AmericanAlaskaTribe("AK020065","Evansville Village (aka Bettles Field)"));
		alaska.add(new AmericanAlaskaTribe("AK020066","Native Village of Eyak (Cordova)"));
		alaska.add(new AmericanAlaskaTribe("AK020067","Native Village of False Pass"));
		alaska.add(new AmericanAlaskaTribe("AK020068","Native Village of Fort Yukon"));
		alaska.add(new AmericanAlaskaTribe("AK020069","Native Village of Gakona"));
		alaska.add(new AmericanAlaskaTribe("AK020070","Galena Village (aka Louden Village)"));
		alaska.add(new AmericanAlaskaTribe("AK020071","Native Village of Gambell"));
		alaska.add(new AmericanAlaskaTribe("AK020072","Native Village of Georgetown"));
		alaska.add(new AmericanAlaskaTribe("AK020073","Native Village of Goodnews Bay"));
		alaska.add(new AmericanAlaskaTribe("AK020074","Organized Village of Grayling (aka Holikachuk)"));
		alaska.add(new AmericanAlaskaTribe("AK020075","Gulkana Village"));
		alaska.add(new AmericanAlaskaTribe("AK020076","Native Village of Hamilton"));
		alaska.add(new AmericanAlaskaTribe("AK020077","Healy Lake Village"));
		alaska.add(new AmericanAlaskaTribe("AK020078","Holy Cross Village"));
		alaska.add(new AmericanAlaskaTribe("AK020079","Hoonah Indian Association"));
		alaska.add(new AmericanAlaskaTribe("AK020080","Native Village of Hooper Bay"));
		alaska.add(new AmericanAlaskaTribe("AK020081","Hughes Village"));
		alaska.add(new AmericanAlaskaTribe("AK020082","Huslia Village"));
		alaska.add(new AmericanAlaskaTribe("AK020083","Hydaburg Cooperative Association"));
		alaska.add(new AmericanAlaskaTribe("AK020084","Igiugig Village"));
		alaska.add(new AmericanAlaskaTribe("AK020085","Village of Iliamna"));
		alaska.add(new AmericanAlaskaTribe("AK020086","Inupiat Community of the Arctic Slope"));
		alaska.add(new AmericanAlaskaTribe("AK020087","Iqurmuit Traditional Council (formerly the Native Village of Russian Mission)"));
		alaska.add(new AmericanAlaskaTribe("AK020088","Ivanoff Bay Village"));
		alaska.add(new AmericanAlaskaTribe("AK020089","Kaguyak Village"));
		alaska.add(new AmericanAlaskaTribe("AK020090","Organized Village of Kake"));
		alaska.add(new AmericanAlaskaTribe("AK020091","Kaktovik Village (aka Barter Island)"));
		alaska.add(new AmericanAlaskaTribe("AK020092","Village of Kalskag"));
		alaska.add(new AmericanAlaskaTribe("AK020093","Village of Kaltag"));
		alaska.add(new AmericanAlaskaTribe("AK020094","Native Village of Kanatak"));
		alaska.add(new AmericanAlaskaTribe("AK020095","Native Village of Karluk"));
		alaska.add(new AmericanAlaskaTribe("AK020096","Organized Village of Kasaan"));
		alaska.add(new AmericanAlaskaTribe("AK020097","Kasigluk Traditional Elders Council (formerly the Native Village of Kasigluk)"));
		alaska.add(new AmericanAlaskaTribe("AK020098","Kenaitze Indian Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020099","Ketchikan Indian Corporation"));
		alaska.add(new AmericanAlaskaTribe("AK020100","Native Village of Kiana"));
		alaska.add(new AmericanAlaskaTribe("AK020101","King Island Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020102","King Salmon Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020103","Native Village of Kipnuk"));
		alaska.add(new AmericanAlaskaTribe("AK020104","Native Village of Kivalina"));
		alaska.add(new AmericanAlaskaTribe("AK020105","Klawock Cooperative Association"));
		alaska.add(new AmericanAlaskaTribe("AK020106","Native Village of Kluti Kaah (aka Copper Center)"));
		alaska.add(new AmericanAlaskaTribe("AK020107","Knik Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020108","Native Village of Kobuk"));
		alaska.add(new AmericanAlaskaTribe("AK020109","Kokhanok Village"));
		alaska.add(new AmericanAlaskaTribe("AK020110","Native Village of Kongiganak"));
		alaska.add(new AmericanAlaskaTribe("AK020111","Village of Kotlik"));
		alaska.add(new AmericanAlaskaTribe("AK020112","Native Village of Kotzebue"));
		alaska.add(new AmericanAlaskaTribe("AK020113","Native Village of Koyuk"));
		alaska.add(new AmericanAlaskaTribe("AK020114","Koyukuk Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020115","Organized Village of Kwethluk"));
		alaska.add(new AmericanAlaskaTribe("AK020116","Native Village of Kwigillingok"));
		alaska.add(new AmericanAlaskaTribe("AK020117","Native Village of Kwinhagak (aka Quinhagak)"));
		alaska.add(new AmericanAlaskaTribe("AK020118","Native Village of Larsen Bay"));
		alaska.add(new AmericanAlaskaTribe("AK020119","Levelock Village"));
		alaska.add(new AmericanAlaskaTribe("AK020120","Lime Village"));
		alaska.add(new AmericanAlaskaTribe("AK020121","Village of Lower Kalskag"));
		alaska.add(new AmericanAlaskaTribe("AK020122","Manley Hot Springs Village"));
		alaska.add(new AmericanAlaskaTribe("AK020123","Manokotak Village"));
		alaska.add(new AmericanAlaskaTribe("AK020124","Native Village of Marshall (aka Fortuna Ledge)"));
		alaska.add(new AmericanAlaskaTribe("AK020125","Native Village of Mary's Igloo"));
		alaska.add(new AmericanAlaskaTribe("AK020126","McGrath Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020127","Native Village of Mekoryuk"));
		alaska.add(new AmericanAlaskaTribe("AK020128","Mentasta Traditional Council"));
		alaska.add(new AmericanAlaskaTribe("AK020129","Metlakatla Indian Community, Annette Island Reserve"));
		alaska.add(new AmericanAlaskaTribe("AK020130","Native Village of Minto"));
		alaska.add(new AmericanAlaskaTribe("AK020131","Naknek Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020132","Native Village of Nanwalek (aka English Bay)"));
		alaska.add(new AmericanAlaskaTribe("AK020133","Native Village of Napaimute"));
		alaska.add(new AmericanAlaskaTribe("AK020134","Native Village of Napakiak"));
		alaska.add(new AmericanAlaskaTribe("AK020135","Native Village of Napaskiak"));
		alaska.add(new AmericanAlaskaTribe("AK020136","Native Village of Nelson Lagoon"));
		alaska.add(new AmericanAlaskaTribe("AK020137","Nenana Native Association"));
		alaska.add(new AmericanAlaskaTribe("AK020138","New Koliganek Village Council"));
		alaska.add(new AmericanAlaskaTribe("AK020139","New Stuyahok Village"));
		alaska.add(new AmericanAlaskaTribe("AK020140","Newhalen Village"));
		alaska.add(new AmericanAlaskaTribe("AK020141","Newtok Village"));
		alaska.add(new AmericanAlaskaTribe("AK020142","Native Village of Nightmute"));
		alaska.add(new AmericanAlaskaTribe("AK020143","Nikolai Village"));
		alaska.add(new AmericanAlaskaTribe("AK020144","Native Village of Nikolski"));
		alaska.add(new AmericanAlaskaTribe("AK020145","Ninilchik Village"));
		alaska.add(new AmericanAlaskaTribe("AK020146","Native Village of Noatak"));
		alaska.add(new AmericanAlaskaTribe("AK020147","Nome Eskimo Community"));
		alaska.add(new AmericanAlaskaTribe("AK020148","Nondalton Village"));
		alaska.add(new AmericanAlaskaTribe("AK020149","Noorvik Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020150","Northway Village"));
		alaska.add(new AmericanAlaskaTribe("AK020151","Native Village of Nuiqsut (aka Nooiksut)"));
		alaska.add(new AmericanAlaskaTribe("AK020152","Nulato Village"));
		alaska.add(new AmericanAlaskaTribe("AK020153","Nunakauyarmiut Tribe (formerly the Native Village of Toksook Bay)"));
		alaska.add(new AmericanAlaskaTribe("AK020154","Native Village of Nunam Iqua (formerly the Native Village of Sheldon's Point)"));
		alaska.add(new AmericanAlaskaTribe("AK020155","Native Village of Nunapitchuk"));
		alaska.add(new AmericanAlaskaTribe("AK020156","Village of Ohogamiut"));
		alaska.add(new AmericanAlaskaTribe("AK020157","Village of Old Harbor"));
		alaska.add(new AmericanAlaskaTribe("AK020158","Orutsararmuit Native Village (aka Bethel)"));
		alaska.add(new AmericanAlaskaTribe("AK020159","Oscarville Traditional Village"));
		alaska.add(new AmericanAlaskaTribe("AK020160","Native Village of Ouzinkie"));
		alaska.add(new AmericanAlaskaTribe("AK020161","Native Village of Paimiut"));
		alaska.add(new AmericanAlaskaTribe("AK020162","Pauloff Harbor Village"));
		alaska.add(new AmericanAlaskaTribe("AK020163","Pedro Bay Village"));
		alaska.add(new AmericanAlaskaTribe("AK020164","Native Village of Perryville"));
		alaska.add(new AmericanAlaskaTribe("AK020165","Petersburg Indian Association"));
		alaska.add(new AmericanAlaskaTribe("AK020166","Native Village of Pilot Point"));
		alaska.add(new AmericanAlaskaTribe("AK020167","Pilot Station Traditional Village"));
		alaska.add(new AmericanAlaskaTribe("AK020168","Native Village of Pitka's Point"));
		alaska.add(new AmericanAlaskaTribe("AK020169","Platinum Traditional Village"));
		alaska.add(new AmericanAlaskaTribe("AK020170","Native Village of Point Hope"));
		alaska.add(new AmericanAlaskaTribe("AK020171","Native Village of Point Lay"));
		alaska.add(new AmericanAlaskaTribe("AK020172","Native Village of Port Graham"));
		alaska.add(new AmericanAlaskaTribe("AK020173","Native Village of Port Heiden"));
		alaska.add(new AmericanAlaskaTribe("AK020174","Native Village of Port Lions"));
		alaska.add(new AmericanAlaskaTribe("AK020175","Portage Creek Village (aka Ohgsenakale)"));
		alaska.add(new AmericanAlaskaTribe("AK020176","Pribilof Islands Aleut Communities of St. Paul & St. George Islands"));
		alaska.add(new AmericanAlaskaTribe("AK020177","Qagan Tayagungin Tribe of Sand Point Village"));
		alaska.add(new AmericanAlaskaTribe("AK020178","Qawalangin Tribe of Unalaska"));
		alaska.add(new AmericanAlaskaTribe("AK020179","Rampart Village"));
		alaska.add(new AmericanAlaskaTribe("AK020180","Village of Red Devil"));
		alaska.add(new AmericanAlaskaTribe("AK020181","Native Village of Ruby"));
		alaska.add(new AmericanAlaskaTribe("AK020182","Saint George Island (See Pribilof Islands Aleut Communities of St. Paul & St. George Islands)"));
		alaska.add(new AmericanAlaskaTribe("AK020183","Native Village of Saint Michael"));
		alaska.add(new AmericanAlaskaTribe("AK020184","Saint Paul Island (See Pribilof Islands Aleut Communities of St. Paul & St. George Islands)"));
		alaska.add(new AmericanAlaskaTribe("AK020185","Village of Salamatoff"));
		alaska.add(new AmericanAlaskaTribe("AK020186","Native Village of Savoonga"));
		alaska.add(new AmericanAlaskaTribe("AK020187","Organized Village of Saxman"));
		alaska.add(new AmericanAlaskaTribe("AK020188","Native Village of Scammon Bay"));
		alaska.add(new AmericanAlaskaTribe("AK020189","Native Village of Selawik"));
		alaska.add(new AmericanAlaskaTribe("AK020190","Seldovia Village Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020191","Shageluk Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020192","Native Village of Shaktoolik"));
		alaska.add(new AmericanAlaskaTribe("AK020193","Native Village of Shishmaref"));
		alaska.add(new AmericanAlaskaTribe("AK020194","Native Village of Shungnak"));
		alaska.add(new AmericanAlaskaTribe("AK020195","Sitka Tribe of Alaska"));
		alaska.add(new AmericanAlaskaTribe("AK020196","Skagway Village"));
		alaska.add(new AmericanAlaskaTribe("AK020197","Village of Sleetmute"));
		alaska.add(new AmericanAlaskaTribe("AK020198","Village of Solomon"));
		alaska.add(new AmericanAlaskaTribe("AK020199","South Naknek Village"));
		alaska.add(new AmericanAlaskaTribe("AK020200","Stebbins Community Association"));
		alaska.add(new AmericanAlaskaTribe("AK020201","Native Village of Stevens"));
		alaska.add(new AmericanAlaskaTribe("AK020202","Village of Stony River"));
		alaska.add(new AmericanAlaskaTribe("AK020203","Sun'aq Tribe of Kodiak (formerly the Shoonaq' Tribe of Kodiak)"));
		alaska.add(new AmericanAlaskaTribe("AK020204","Takotna Village"));
		alaska.add(new AmericanAlaskaTribe("AK020205","Native Village of Tanacross"));
		alaska.add(new AmericanAlaskaTribe("AK020206","Native Village of Tanana"));
		alaska.add(new AmericanAlaskaTribe("AK020207","Tangirnaq Native Village (formerly Lesnoi Village)"));
		alaska.add(new AmericanAlaskaTribe("AK020208","Native Village of Tatitlek"));
		alaska.add(new AmericanAlaskaTribe("AK020209","Native Village of Tazlina"));
		alaska.add(new AmericanAlaskaTribe("AK020210","Telida Village"));
		alaska.add(new AmericanAlaskaTribe("AK020211","Native Village of Teller"));
		alaska.add(new AmericanAlaskaTribe("AK020212","Native Village of Tetlin"));
		alaska.add(new AmericanAlaskaTribe("AK020213","Central Council of the Tlingit & Haida Indian Tribes"));
		alaska.add(new AmericanAlaskaTribe("AK020214","Traditional Village of Togiak"));
		alaska.add(new AmericanAlaskaTribe("AK020215","Tuluksak Native Community"));
		alaska.add(new AmericanAlaskaTribe("AK020216","Native Village of Tuntutuliak"));
		alaska.add(new AmericanAlaskaTribe("AK020217","Native Village of Tununak"));
		alaska.add(new AmericanAlaskaTribe("AK020218","Twin Hills Village"));
		alaska.add(new AmericanAlaskaTribe("AK020219","Native Village of Tyonek"));
		alaska.add(new AmericanAlaskaTribe("AK020220","Ugashik Village"));
		alaska.add(new AmericanAlaskaTribe("AK020221","Umkumiute Native Village"));
		alaska.add(new AmericanAlaskaTribe("AK020222","Native Village of Unalakleet"));
		alaska.add(new AmericanAlaskaTribe("AK020223","Native Village of Unga"));
		alaska.add(new AmericanAlaskaTribe("AK020224","Village of Venetie (See Native Village of Venetie Tribal Government)"));
		alaska.add(new AmericanAlaskaTribe("AK020225","Native Village of Venetie Tribal Government (Arctic Village and Village of Venetie)"));
		alaska.add(new AmericanAlaskaTribe("AK020226","Village of Wainwright"));
		alaska.add(new AmericanAlaskaTribe("AK020227","Native Village of Wales"));
		alaska.add(new AmericanAlaskaTribe("AK020228","Native Village of White Mountain"));
		alaska.add(new AmericanAlaskaTribe("AK020229","Wrangell Cooperative Association"));
		alaska.add(new AmericanAlaskaTribe("AK020230","Yakutat Tlingit Tribe"));
		alaska.add(new AmericanAlaskaTribe("AK020231","Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020232","Ak Chin Indian Community of the Maricopa (Ak Chin) Indian Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020233","Cocopah Tribe of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020234","Colorado River Indian Tribes of the Colorado River Indian Reservation (Arizona and California)"));
		alaska.add(new AmericanAlaskaTribe("AK020235","Fort McDowell Yavapai Nation"));
		alaska.add(new AmericanAlaskaTribe("AK020236","Fort Mojave Indian Tribe (Arizona, California and Nevada)"));
		alaska.add(new AmericanAlaskaTribe("AK020237","Gila River Indian Community of the Gila River Indian Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020238","Havasupai Tribe of the Havasupai Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020239","Hopi Tribe of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020240","Hualapai Indian Tribe of the Hualapai Indian Tribe Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020241","Kaibab Band of Paiute Indians of the Kaibab Indian Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020242","Navajo Nation (Arizona, New Mexico and Utah)"));
		alaska.add(new AmericanAlaskaTribe("AK020243","Pascua Yaqui Tribe of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020244","Quechan Tribe of the Fort Yuma Indian Reservation (Arizona and California)"));
		alaska.add(new AmericanAlaskaTribe("AK020245","Salt River Pima-Maricopa Indian Community of the Salt River Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020246","San Carlos Apache Tribe of the San Carlos Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020247","San Juan Southern Paiute Tribe of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020248","Tohono O'odham Nation of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020249","Tonto Apache Tribe of Arizona"));
		alaska.add(new AmericanAlaskaTribe("AK020250","White Mountain Apache Tribe of the Fort Apache Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020251","Yavapai-Apache Nation of the Camp Verde Indian Reservation"));
		alaska.add(new AmericanAlaskaTribe("AK020252","Yavapai-Prescott Tribe of the Yavapai Reservation"));
		americanAlaskaStateTribeMap.put("AK", alaska);
		
		List<AmericanAlaskaTribe> arizona = new ArrayList<AmericanAlaskaTribe>(_21);
		arizona.add(new AmericanAlaskaTribe("AZ040591","Ak Chin Indian Community of the Maricopa (Ak Chin) Indian Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040592","Cocopah Tribe of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040593","Colorado River Indian Tribes of the Colorado River Indian Reservation (Arizona and California)"));
		arizona.add(new AmericanAlaskaTribe("AZ040594","Fort McDowell Yavapai Nation"));
		arizona.add(new AmericanAlaskaTribe("AZ040595","Fort Mojave Indian Tribe (Arizona, California and Nevada)"));
		arizona.add(new AmericanAlaskaTribe("AZ040596","Gila River Indian Community of the Gila River Indian Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040597","Havasupai Tribe of the Havasupai Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040598","Hopi Tribe of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040599","Hualapai Indian Tribe of the Hualapai Indian Tribe Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040600","Kaibab Band of Paiute Indians of the Kaibab Indian Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040601","Navajo Nation (Arizona, New Mexico and Utah)"));
		arizona.add(new AmericanAlaskaTribe("AZ040602","Pascua Yaqui Tribe of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040603","Quechan Tribe of the Fort Yuma Indian Reservation (Arizona and California)"));
		arizona.add(new AmericanAlaskaTribe("AZ040604","Salt River Pima-Maricopa Indian Community of the Salt River Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040605","San Carlos Apache Tribe of the San Carlos Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040606","San Juan Southern Paiute Tribe of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040607","Tohono O'odham Nation of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040608","Tonto Apache Tribe of Arizona"));
		arizona.add(new AmericanAlaskaTribe("AZ040609","White Mountain Apache Tribe of the Fort Apache Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040610","Yavapai-Apache Nation of the Camp Verde Indian Reservation"));
		arizona.add(new AmericanAlaskaTribe("AZ040611","Yavapai-Prescott Tribe of the Yavapai Reservation"));
		americanAlaskaStateTribeMap.put("AZ", arizona);
		
		
		List<AmericanAlaskaTribe> california = new ArrayList<AmericanAlaskaTribe>(_120);
		california.add(new AmericanAlaskaTribe("CA060253","Agua Caliente Band of Cahuilla Indians of the Agua Caliente Indian Reservation"));
		california.add(new AmericanAlaskaTribe("CA060254","Alturas Indian Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060255","Augustine Band of Cahuilla Indians (formerly the Augustine Band of Cahuilla Mission Indians of the Augustine Reservation)"));
		california.add(new AmericanAlaskaTribe("CA060256","Bear River Band of the Rohnerville Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060257","Berry Creek Rancheria of Maidu Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060258","Big Lagoon Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060259","Big Pine Band of Owens Valley Paiute Shoshone Indians of the Big Pine Reservation"));
		california.add(new AmericanAlaskaTribe("CA060260","Big Sandy Rancheria of Mono Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060261","Big Valley Band of Pomo Indians of the Big Valley Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060262","Blue Lake Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060263","Bridgeport Paiute Indian Colony of California"));
		california.add(new AmericanAlaskaTribe("CA060264","Buena Vista Rancheria of Me-Wuk Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060265","Cabazon Band of  Mission Indians"));
		california.add(new AmericanAlaskaTribe("CA060266","Cachil DeHe Band of Wintun Indians of the Colusa Indian Community of the Colusa Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060267","Cahuilla Band of Mission Indians of the Cahuilla Reservation"));
		california.add(new AmericanAlaskaTribe("CA060268","Cahto Indian Tribe of the Laytonville Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060269","California Valley Miwok Tribe (formerly the Sheep Ranch Rancheria of Me-Wuk Indians of California)"));
		california.add(new AmericanAlaskaTribe("CA060270","Campo Band of Diegueño Mission Indians of the Campo Indian Reservation"));
		california.add(new AmericanAlaskaTribe("CA060271","Capitan Grande Band of Diegueño Mission Indians of California:"));
		california.add(new AmericanAlaskaTribe("CA060272","Barona Group of Capitan Grande Band of Mission Indians of the Barona Reservation"));
		california.add(new AmericanAlaskaTribe("CA060273","Viejas (Baron Long) Group of Capitan Grande Band of Mission Indians of the Viejas Reservation Cedarville Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060274","Chemehuevi Indian Tribe of the Chemehuevi Reservation"));
		california.add(new AmericanAlaskaTribe("CA060275","Cher-Ae Heights Indian Community of the Trinidad Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060276","Chicken Ranch Rancheria of Me-Wuk Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060277","Cloverdale Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060278","Cold Springs Rancheria of Mono Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060279","Colorado River Indian Tribes of the Colorado River Indian Reservation (california and California)"));
		california.add(new AmericanAlaskaTribe("CA060280","Cortina Indian Rancheria of Wintun Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060281","Coyote Valley Band of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060282","Death Valley Timbi-Sha Shoshone Band of California"));
		california.add(new AmericanAlaskaTribe("CA060283","Dry Creek Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060284","Elem Indian Colony of Pomo Indians of the Sulphur Bank Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060285","Elk Valley Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060286","Enterprise Rancheria of Maidu Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060287","Ewiiaapaayp Band of Kumeyaay Indians"));
		california.add(new AmericanAlaskaTribe("CA060288","Federated Indians of Graton Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060289","Fort Bidwell Indian Community of the Fort Bidwell Reservation of California"));
		california.add(new AmericanAlaskaTribe("CA060290","Fort Independence Indian Community of Paiute Indians of the Fort Independence Reservation"));
		california.add(new AmericanAlaskaTribe("CA060291","Fort Mojave Indian Tribe (california, California and Nevada)"));
		california.add(new AmericanAlaskaTribe("CA060292","Greenville Rancheria of Maidu Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060293","Grindstone Indian Rancheria of Wintun-Wailaki Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060294","Guidiville Rancheria of California"));
		california.add(new AmericanAlaskaTribe("CA060295","Habematolel Pomo of Upper Lake (formerly the Upper Lake Band of Pomo Indians of Upper Lake Rancheria of California)"));
		california.add(new AmericanAlaskaTribe("CA060296","Hoopa Valley Tribe"));
		california.add(new AmericanAlaskaTribe("CA060297","Hopland Band of Pomo Indians of the Hopland Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060298","Inaja Band of Diegueño Mission Indians of the Inaja and Cosmit Reservation"));
		california.add(new AmericanAlaskaTribe("CA060299","Ione Band of Miwok Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060300","Jackson Rancheria of Me-Wuk Indians of  California"));
		california.add(new AmericanAlaskaTribe("CA060301","Jamul Indian Village of California"));
		california.add(new AmericanAlaskaTribe("CA060302","Karuk Tribe of California"));
		california.add(new AmericanAlaskaTribe("CA060303","Kashia Band of Pomo Indians of the Stewart's Point Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060304","La Jolla Band of Luiseño Mission Indians of the La Jolla Reservation"));
		california.add(new AmericanAlaskaTribe("CA060305","La Posta Band of Diegueño Mission Indians of the La Posta Indian Reservation"));
		california.add(new AmericanAlaskaTribe("CA060306","Los Coyotes Band of Cahuilla & Cupeno Indians of the Los Coyotes Reservation"));
		california.add(new AmericanAlaskaTribe("CA060307","Lower Lake Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060308","Lytton Rancheria of California"));
		california.add(new AmericanAlaskaTribe("CA060309","Manchester Band of Pomo Indians of the Manchester-Point Arena Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060310","Manzanita Band of Diegueño Mission Indians of the Manzanita Reservation"));
		california.add(new AmericanAlaskaTribe("CA060311","Mechoopda Indian Tribe of Chico Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060312","Mesa Grande Band of Diegueño Mission Indians of the Mesa Grande Reservation"));
		california.add(new AmericanAlaskaTribe("CA060313","Middletown Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060314","Mooretown Rancheria of Maidu Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060315","Morongo Band of Cahuilla Mission Indians of the Morongo Reservation"));
		california.add(new AmericanAlaskaTribe("CA060316","Northfork Rancheria of Mono Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060317","Paiute-Shoshone Indians of the Bishop Community of the Bishop Colony"));
		california.add(new AmericanAlaskaTribe("CA060318","Paiute-Shoshone Indians of the Lone Pine Community of the Lone Pine Reservation"));
		california.add(new AmericanAlaskaTribe("CA060319","Pala Band of Luiseño Mission Indians of the Pala Reservation"));
		california.add(new AmericanAlaskaTribe("CA060320","Paskenta Band of Nomlaki Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060321","Pauma Band of Luiseño Mission Indians of the Pauma & Yuima Reservation"));
		california.add(new AmericanAlaskaTribe("CA060322","Pechanga Band of Luiseño Mission Indians of the Pechanga Reservation"));
		california.add(new AmericanAlaskaTribe("CA060323","Picayune Rancheria of Chukchansi Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060324","Pinoleville Pomo Nation (formerly the Pinoleville Rancheria of Pomo Indians of California)"));
		california.add(new AmericanAlaskaTribe("CA060325","Pit River Tribe (includes XL Ranch, Big Bend, Likely, Lookout, Montgomery Creek and Roaring Creek Rancherias)"));
		california.add(new AmericanAlaskaTribe("CA060326","Potter Valley Tribe (formerly the Potter Valley Rancheria of Pomo Indians of California)"));
		california.add(new AmericanAlaskaTribe("CA060327","Quartz Valley Indian Community of the Quartz Valley Reservation of California"));
		california.add(new AmericanAlaskaTribe("CA060328","Quechan Tribe of the Fort Yuma Indian Reservation (california and California)"));
		california.add(new AmericanAlaskaTribe("CA060329","Ramona Band or Village of Cahuilla Mission Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060330","Redding Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060331","Redwood Valley Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060332","Resighini Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060333","Rincon Band of Luiseño Mission Indians of the Rincon Reservation"));
		california.add(new AmericanAlaskaTribe("CA060334","Robinson Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060335","Round Valley Indian Tribes of the Round Valley Reservation"));
		california.add(new AmericanAlaskaTribe("CA060336","Rumsey Indian Rancheria of Wintun Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060337","San Manual Band of Serrano Mission Indians of the San Maual Reservation"));
		california.add(new AmericanAlaskaTribe("CA060338","San Pasqual Band of Diegueño Mission Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060339","Santa Rosa Indian Community of the Santa Rosa Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060340","Santa Rosa Band of Cahuilla Indians (formerly the Santa Rosa Band of Cahuilla Mission Indians of the Santa Rosa Reservation)"));
		california.add(new AmericanAlaskaTribe("CA060341","Santa Ynez Band of Chumash Mission Indians of the Santa Ynez Reservation"));
		california.add(new AmericanAlaskaTribe("CA060342","Santa Ysabel Band of Diegueño Mission Indians of the Santa Ysabel Reservation"));
		california.add(new AmericanAlaskaTribe("CA060343","Scotts Valley Band of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060344","Sheep Ranch Rancheria of Me-Wuk Indians"));
		california.add(new AmericanAlaskaTribe("CA060345","Sherwood Valley Rancheria of Pomo Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060346","Shingle Springs Band of Miwok Indians, Shingle Springs Rancheria (Verona Tract)"));
		california.add(new AmericanAlaskaTribe("CA060347","Smith River Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060348","Soboba Band of Luiseño Indians"));
		california.add(new AmericanAlaskaTribe("CA060349","Susanville Indian Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060350","Sycuan Band of the Kumeyaay Nation (formerly the Sycuan Band of Diegueno Mission Indians of California)"));
		california.add(new AmericanAlaskaTribe("CA060351","Table Bluff Reservation-Wiyot Tribe"));
		california.add(new AmericanAlaskaTribe("CA060352","Table Mountain Rancheria of California"));
		california.add(new AmericanAlaskaTribe("CA060353","Torres-Martinez Desert Cahuilla Indians (formerly the Torres-Martinez Band of Cahuilla Mission Indians of California)"));
		california.add(new AmericanAlaskaTribe("CA060354","Tule River Indian Tribe of the Tule River Reservation"));
		california.add(new AmericanAlaskaTribe("CA060355","Tuolumne Band of Me-Wuk Indians of the Tuolumne Rancheria of California"));
		california.add(new AmericanAlaskaTribe("CA060356","Twenty-Nine Palms Band of Mission Indians of California"));
		california.add(new AmericanAlaskaTribe("CA060357","United Auburn Indian Community of the Auburn Rancheria of California"));
		california.add(new AmericanAlaskaTribe("CA060358","Upper Lake Band of Pomo Indians"));
		california.add(new AmericanAlaskaTribe("CA060359","Utu Utu Gwaitu Paiute Tribe of the Benton Paiute Reservation"));
		california.add(new AmericanAlaskaTribe("CA060360","Washoe Tribe (Carson Colony, Dresslerville Colony, Woodfords Community, Stewart Community and Washoe Ranches) (California and Nevada)"));
		california.add(new AmericanAlaskaTribe("CA060361","Wilton Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060362","Wiyot Tribe (formerly the Table Bluff Reservation-Wiyot Tribe)"));
		california.add(new AmericanAlaskaTribe("CA060363","Yurok Tribe of the Yurok Reservation"));
		california.add(new AmericanAlaskaTribe("CA060364", "Cedarville Rancheria"));
		california.add(new AmericanAlaskaTribe("CA060365", "Koi Nation of Northern California"));
		california.add(new AmericanAlaskaTribe("CA060366", "Lipay Nation of Santa Ysebel"));
		california.add(new AmericanAlaskaTribe("CA060367", "Tejon Indian Tribe"));
		california.add(new AmericanAlaskaTribe("CA060368", "Tolowa Dee-nl' Nation"));
		california.add(new AmericanAlaskaTribe("CA060369", "Yocha Dehe Wintun Nation"));
		americanAlaskaStateTribeMap.put("CA", california);
		
		
		List<AmericanAlaskaTribe> colorado = new ArrayList<AmericanAlaskaTribe>(_2);
		colorado.add(new AmericanAlaskaTribe("CO080364","Southern Ute Indian Tribe of the Southern Ute Reservation"));
		colorado.add(new AmericanAlaskaTribe("CO080365","Ute Mountain Tribe of the Ute Mountain Reservation (Colorado, New Mexico and Utah)"));
		americanAlaskaStateTribeMap.put("CO", colorado);
		
		
		List<AmericanAlaskaTribe> connecticut = new ArrayList<AmericanAlaskaTribe>(_2);
		connecticut.add(new AmericanAlaskaTribe("CT090366","Mashantucket Pequot Tribe of Connecticut"));
		connecticut.add(new AmericanAlaskaTribe("CT090367","Mohegan Indian Tribe of Connecticut"));
		americanAlaskaStateTribeMap.put("CT", connecticut);
		
		
		List<AmericanAlaskaTribe> florida = new ArrayList<AmericanAlaskaTribe>(_2);
		florida.add(new AmericanAlaskaTribe("FL120368","Miccosukee Tribe of Indians of Florida"));
		florida.add(new AmericanAlaskaTribe("FL120369","Seminole Tribe of Florida (Dania, Big Cypress, Brighton, Hollywood and Tampa Reservations)"));
		americanAlaskaStateTribeMap.put("FL", florida);
		
		
		List<AmericanAlaskaTribe> idaho = new ArrayList<AmericanAlaskaTribe>(_5);
		idaho.add(new AmericanAlaskaTribe("ID160370","Coeur D'Alene Tribe of the Coeur D'Alene Reservation"));
		idaho.add(new AmericanAlaskaTribe("ID160371","Kootenai Tribe of Idaho"));
		idaho.add(new AmericanAlaskaTribe("ID160372","Nez Perce Tribe of Idaho"));
		idaho.add(new AmericanAlaskaTribe("ID160373","Shoshone-Bannock Tribes of the Fort Hall Reservation of Idaho"));
		idaho.add(new AmericanAlaskaTribe("NV320435","Shoshone-Paiute Tribes of the Duck Valley Reservation"));
		americanAlaskaStateTribeMap.put("ID", idaho);
		
		
		List<AmericanAlaskaTribe> iowa = new ArrayList<AmericanAlaskaTribe>(_1);
		iowa.add(new AmericanAlaskaTribe("IA190374","Sac & Fox Tribe of the Mississippi in Iowa"));
		americanAlaskaStateTribeMap.put("IA", iowa);
		
		List<AmericanAlaskaTribe> kansas = new ArrayList<AmericanAlaskaTribe>(_4);
		kansas.add(new AmericanAlaskaTribe("KS200375","Iowa Tribe (Kansas and Nebraska)"));
		kansas.add(new AmericanAlaskaTribe("KS200376","Kickapoo Tribe of Indians of the Kickapoo Reservation in Kansas"));
		kansas.add(new AmericanAlaskaTribe("KS200377","Prairie Band of Potawatomi Nation"));
		kansas.add(new AmericanAlaskaTribe("KS200378","Sac and Fox Nation of Missouri (Kansas and Nebraska)"));
		americanAlaskaStateTribeMap.put("KS", kansas);
		
		List<AmericanAlaskaTribe> louisiana = new ArrayList<AmericanAlaskaTribe>(_4);
		louisiana.add(new AmericanAlaskaTribe("LA220379","Chitimacha Tribe of Louisiana"));
		louisiana.add(new AmericanAlaskaTribe("LA220380","Coushatta Tribe of Louisiana"));
		louisiana.add(new AmericanAlaskaTribe("LA220381","Jena Band of Choctaw Indians"));
		louisiana.add(new AmericanAlaskaTribe("LA220382","Tunica-Biloxi Indian Tribe of Louisiana"));
		americanAlaskaStateTribeMap.put("LA", louisiana);
		
		
		List<AmericanAlaskaTribe> maine = new ArrayList<AmericanAlaskaTribe>(_4);
		maine.add(new AmericanAlaskaTribe("ME230383","Aroostook Band of Micmac Indians"));
		maine.add(new AmericanAlaskaTribe("ME230384","Houlton Band of Maliseet Indians of Maine"));
		maine.add(new AmericanAlaskaTribe("ME230385","Passamaquoddy Tribe of Maine"));
		maine.add(new AmericanAlaskaTribe("ME230386","Penobscot Tribe of Maine"));
		americanAlaskaStateTribeMap.put("ME", maine);
		
		List<AmericanAlaskaTribe> massachusetts = new ArrayList<AmericanAlaskaTribe>(_2);
		massachusetts.add(new AmericanAlaskaTribe("MA250387","Mashpee Wampanoag Indian Tribal Council")) ;
		massachusetts.add(new AmericanAlaskaTribe("MA250388","Wampanoag Tribe of Gay Head (Aquinnah) of Massachusetts")) ;
		americanAlaskaStateTribeMap.put("MA", massachusetts);
		
		List<AmericanAlaskaTribe> michigan = new ArrayList<AmericanAlaskaTribe>(_13);
		michigan.add(new AmericanAlaskaTribe("MI260389","Bay Mills Indian Community of the Sault Ste. Marie Band of Chippewa Indians"));
		michigan.add(new AmericanAlaskaTribe("MI260390","Grand Traverse Band of Ottawa and Chippewa Indians"));
		michigan.add(new AmericanAlaskaTribe("MI260391","Hannahville Indian Community"));
		michigan.add(new AmericanAlaskaTribe("MI260392","Huron Potawatomi, Inc."));
		michigan.add(new AmericanAlaskaTribe("MI260393","Keweenaw Bay Indian Community"));
		michigan.add(new AmericanAlaskaTribe("MI260394","Lac Vieux Desert Band of Lake Superior Chippewa Indians"));
		michigan.add(new AmericanAlaskaTribe("MI260395","Little River Band of Ottawa Indians"));
		michigan.add(new AmericanAlaskaTribe("MI260396","Little Traverse Bay Bands of Odawa Indians"));
		michigan.add(new AmericanAlaskaTribe("MI260397","Match-e-be-nash-she-wish Band of Pottawatomi Indians of Michigan"));
		michigan.add(new AmericanAlaskaTribe("MI260398","Pokagon Band of Potawatomi Indians (Michigan and Indiana)"));
		michigan.add(new AmericanAlaskaTribe("MI260399","Saginaw Chippewa Indian Tribe of Michigan"));
		michigan.add(new AmericanAlaskaTribe("MI260400","Sault Ste. Marie Tribe of Chippewa Indians of Michigan"));
		americanAlaskaStateTribeMap.put("MI", michigan);
		
		List<AmericanAlaskaTribe> minnesota = new ArrayList<AmericanAlaskaTribe>(_9);
		minnesota.add(new AmericanAlaskaTribe("MN270401","Lower Sioux Indian Community in the State of Minnesota"));
		minnesota.add(new AmericanAlaskaTribe("MN270402","Mdewakanton Sioux Indians"));
		minnesota.add(new AmericanAlaskaTribe("MN270403","Minnesota Chippewa Tribe (Six component reservations: Bois Forte Band (Nett Lake);"));
		minnesota.add(new AmericanAlaskaTribe("MN270404","Fond du Lac Band; Grand Portage Band; Leech Lake Band; Mille Lacs Ban; White Earth Band)"));
		minnesota.add(new AmericanAlaskaTribe("MN270405","Prairie Island Indian Community in the State of Minnesota"));
		minnesota.add(new AmericanAlaskaTribe("MN270406","Mdewakanton Sioux Indians"));
		minnesota.add(new AmericanAlaskaTribe("MN270407","Red Lake Band of Chippewa Indians"));
		minnesota.add(new AmericanAlaskaTribe("MN270408","Shakopee Mdewakanton Sioux Community of Minnesota"));
		minnesota.add(new AmericanAlaskaTribe("MN270409","Upper Sioux Community"));
		americanAlaskaStateTribeMap.put("MN", minnesota);
		
		List<AmericanAlaskaTribe> mississippi = new ArrayList<AmericanAlaskaTribe>(_1);
		mississippi.add(new AmericanAlaskaTribe("MS280410","Mississippi Band of Choctaw Indians")); 
		americanAlaskaStateTribeMap.put("MS", mississippi);
		
		List<AmericanAlaskaTribe> montana = new ArrayList<AmericanAlaskaTribe>(_7);
		montana.add(new AmericanAlaskaTribe("MT300411","Assiniboine and Sioux Tribes of the Fort Peck Indian Reservation"));
		montana.add(new AmericanAlaskaTribe("MT300412","Blackfeet Tribe of the Blackfeet Indian Reservation of Montana"));
		montana.add(new AmericanAlaskaTribe("MT300413","Chippewa-Cree Indians of the Rocky Boy's Reservation"));
		montana.add(new AmericanAlaskaTribe("MT300414","Confederated Salish and Kootenai Tribes of the Flathead Reservation"));
		montana.add(new AmericanAlaskaTribe("MT300415","Crow Tribe of Montana"));
		montana.add(new AmericanAlaskaTribe("MT300416","Fort Belknap Indian Community of the Fort Belknap Reservation of Montana"));
		montana.add(new AmericanAlaskaTribe("MT300417","Northern Cheyenne Tribe of the Northern Cheyenne Indian Reservation"));
		americanAlaskaStateTribeMap.put("MT", montana);
		
		List<AmericanAlaskaTribe> nebraska = new ArrayList<AmericanAlaskaTribe>(_6);
		nebraska.add(new AmericanAlaskaTribe("NE310418","Iowa Tribe (Kansas and Nebraska)"));
		nebraska.add(new AmericanAlaskaTribe("NE310419","Omaha Tribe of Nebraska"));
		nebraska.add(new AmericanAlaskaTribe("NE310420","Ponca Tribe of Nebraska"));
		nebraska.add(new AmericanAlaskaTribe("NE310421","Sac and Fox Nation of Missouri (Kansas and Nebraska)"));
		nebraska.add(new AmericanAlaskaTribe("NE310422","Santee Sioux Nation"));
		nebraska.add(new AmericanAlaskaTribe("NE310423","Winnebago Tribe of Nebraska"));
		americanAlaskaStateTribeMap.put("NE", nebraska);
		
		List<AmericanAlaskaTribe> nevada = new ArrayList<AmericanAlaskaTribe>(_19);
		nevada.add(new AmericanAlaskaTribe("NV320424","Confederated Tribes of the Goshute Reservation (Nevada and Utah)"));
		nevada.add(new AmericanAlaskaTribe("NV320425","Duckwater Shoshone Tribe of the Duckwater Reservation"));
		nevada.add(new AmericanAlaskaTribe("NV320426","Ely Shoshone Tribe of Nevada"));
		nevada.add(new AmericanAlaskaTribe("NV320427","Fort McDermitt Paiute and Shoshone Tribes of the Fort McDermitt Indian Reservation (Nevada and Oregon)"));
		nevada.add(new AmericanAlaskaTribe("NV320428","Fort Mojave Indian Tribe (Arizona, California and Nevada)"));
		nevada.add(new AmericanAlaskaTribe("NV320429","Las Vegas Tribe of Paiute Indians of the Las Vegas Indian Colony"));
		nevada.add(new AmericanAlaskaTribe("NV320430","Lovelock Paiute Tribe of the Lovelock Indian Colony"));
		nevada.add(new AmericanAlaskaTribe("NV320431","Moapa Band of Paiute Indians of the Moapa River Indian Reservation"));
		nevada.add(new AmericanAlaskaTribe("NV320432","Paiute-Shoshone Tribe of the Fallon Reservation and Colony"));
		nevada.add(new AmericanAlaskaTribe("NV320433","Pyramid Lake Paiute Tribe of the Pyramid Lake Reservation"));
		nevada.add(new AmericanAlaskaTribe("NV320434","Reno-Sparks Indian Colony"));
		nevada.add(new AmericanAlaskaTribe("NV320435","Shoshone-Paiute Tribes of the Duck Valley Reservation"));
		nevada.add(new AmericanAlaskaTribe("NV320436","Summit Lake Paiute Tribe of Nevada"));
		nevada.add(new AmericanAlaskaTribe("NV320437","Te-Moak Tribe of Western Shoshone Indians of Nevada (Four constituent bands: Battle Mountain Band; Elko Band; South Fork Band; Wells Band)"));
		nevada.add(new AmericanAlaskaTribe("NV320438","Walker River Paiute Tribe of the Walker River Reservation"));
		nevada.add(new AmericanAlaskaTribe("NV320439","Washoe Tribe (Nevada and California) (Carson Colony, Dresslerville Colony, Woodfords Community, Stewart Community and Washoe Ranches)"));
		nevada.add(new AmericanAlaskaTribe("NV320440","Winnemucca Indian Colony of Nevada"));
		nevada.add(new AmericanAlaskaTribe("NV320441","Yerington Paiute Tribe of the Yerington Colony & Campbell Ranch"));
		nevada.add(new AmericanAlaskaTribe("NV320442","Yomba Shoshone Tribe of the Yomba Reservation"));
		americanAlaskaStateTribeMap.put("NV", nevada);
		
		List<AmericanAlaskaTribe> newMexico = new ArrayList<AmericanAlaskaTribe>(_23);
		newMexico.add(new AmericanAlaskaTribe("NM350443","Jicarilla Apache Nation"));
		newMexico.add(new AmericanAlaskaTribe("NM350444","Mescalero Apache Tribe of the Mescalero Reservation"));
		newMexico.add(new AmericanAlaskaTribe("NM350445","Navajo Nation (Arizona, New Mexico and Utah)"));
		newMexico.add(new AmericanAlaskaTribe("NM350446","Ohkay Owingeh (formerly the Pueblo of San Juan)"));
		newMexico.add(new AmericanAlaskaTribe("NM350447","Pueblo of Acoma"));
		newMexico.add(new AmericanAlaskaTribe("NM350448","Pueblo of Cochiti"));
		newMexico.add(new AmericanAlaskaTribe("NM350449","Pueblo of Jemez"));
		newMexico.add(new AmericanAlaskaTribe("NM350450","Pueblo of Isleta"));
		newMexico.add(new AmericanAlaskaTribe("NM350451","Pueblo of Laguna"));
		newMexico.add(new AmericanAlaskaTribe("NM350452","Pueblo of Nambe"));
		newMexico.add(new AmericanAlaskaTribe("NM350453","Pueblo of Picuris"));
		newMexico.add(new AmericanAlaskaTribe("NM350454","Pueblo of Pojoaque"));
		newMexico.add(new AmericanAlaskaTribe("NM350455","Pueblo of San Felipe"));
		newMexico.add(new AmericanAlaskaTribe("NM350456","Pueblo of San Ildefonso"));
		newMexico.add(new AmericanAlaskaTribe("NM350457","Pueblo of Sandia"));
		newMexico.add(new AmericanAlaskaTribe("NM350458","Pueblo of Santa Ana"));
		newMexico.add(new AmericanAlaskaTribe("NM350459","Pueblo of Santa Clara"));
		newMexico.add(new AmericanAlaskaTribe("NM350460","Pueblo of Santo Domingo"));
		newMexico.add(new AmericanAlaskaTribe("NM350461","Pueblo of Taos"));
		newMexico.add(new AmericanAlaskaTribe("NM350462","Pueblo of Tesuque"));
		newMexico.add(new AmericanAlaskaTribe("NM350463","Pueblo of Zia"));
		newMexico.add(new AmericanAlaskaTribe("NM350464","Ute Mountain Tribe of the Ute Mountain Reservation (Colorado, New Mexico and Utah)"));
		newMexico.add(new AmericanAlaskaTribe("NM350465","Zuni Tribe of the Zuni Reservation"));
		newMexico.add(new AmericanAlaskaTribe("NM350466","Kewa Pueblo"));

		americanAlaskaStateTribeMap.put("NM", newMexico);

		final List<AmericanAlaskaTribe> virginia = new ArrayList<>(7);
		virginia.add(new AmericanAlaskaTribe("VA000001", "Pamunkey Indian Tribe"));
		virginia.add(new AmericanAlaskaTribe("VA000002", "Chickahominy Indian Tribe"));
		virginia.add(new AmericanAlaskaTribe("VA000003", "Chickahominy Indian Tribe - Eastern Division"));
		virginia.add(new AmericanAlaskaTribe("VA000004", "Upper Mattaponi Tribe"));
		virginia.add(new AmericanAlaskaTribe("VA000005", "Rappahannock Tribe Inc."));
		virginia.add(new AmericanAlaskaTribe("VA000006", "Monacan Indian Nation"));
		virginia.add(new AmericanAlaskaTribe("VA000007", "Nansemond Indian Tribe"));
		americanAlaskaStateTribeMap.put("VA", virginia);

		final List<AmericanAlaskaTribe> indiana = new ArrayList<>(1);
		indiana.add(new AmericanAlaskaTribe("IN000001", "Pokagon Band of Potawatomi Indians (Michigan and Indiana)"));
		americanAlaskaStateTribeMap.put("IN", indiana);
		
		List<AmericanAlaskaTribe> newYork = new ArrayList<>(_8);
		newYork.add(new AmericanAlaskaTribe("NY360466","Cayuga Nation of New York"));
		newYork.add(new AmericanAlaskaTribe("NY360467","Oneida Nation of New York"));
		newYork.add(new AmericanAlaskaTribe("NY360468","Onondaga Nation of New York"));
		newYork.add(new AmericanAlaskaTribe("NY360469","Saint Regis Mohawk Tribe (formerly the St. Regis Band of Mohawk Indians of New York)"));
		newYork.add(new AmericanAlaskaTribe("NY360470","Seneca Nation of New York"));
		newYork.add(new AmericanAlaskaTribe("NY360471","Shinnecock Indian Nation"));
		newYork.add(new AmericanAlaskaTribe("NY360472","Tonawanda Band of Seneca Indians of New York"));
		newYork.add(new AmericanAlaskaTribe("NY360473","Tuscarora Nation of New York"));
		americanAlaskaStateTribeMap.put("NY", newYork);
		
		List<AmericanAlaskaTribe> northCarolina = new ArrayList<AmericanAlaskaTribe>(_1);
		northCarolina.add(new AmericanAlaskaTribe("NC370474","Eastern Band of Cherokee Indians of North Carolina"));
		americanAlaskaStateTribeMap.put("NC", northCarolina);
		
		List<AmericanAlaskaTribe> northDakota = new ArrayList<AmericanAlaskaTribe>(_4);
		northDakota.add(new AmericanAlaskaTribe("ND380475","Spirit Lake Tribe"));
		northDakota.add(new AmericanAlaskaTribe("ND380476","Standing Rock Sioux Tribe (North Dakota and South Dakota)"));
		northDakota.add(new AmericanAlaskaTribe("ND380477","Three Affiliated Tribes of the Fort Berthold Reservation"));
		northDakota.add(new AmericanAlaskaTribe("ND380478","Turtle Mountain Band of Chippewa Indians of North Dakota"));
		americanAlaskaStateTribeMap.put("ND", northDakota);
		
		List<AmericanAlaskaTribe> oklahoma = new ArrayList<AmericanAlaskaTribe>(_40);
		oklahoma.add(new AmericanAlaskaTribe("OK400479","Absentee-Shawnee Tribe of Indians"));
		oklahoma.add(new AmericanAlaskaTribe("OK400480","Alabama-Quassarte Tribal Town"));
		oklahoma.add(new AmericanAlaskaTribe("OK400481","Apache Tribe"));
		oklahoma.add(new AmericanAlaskaTribe("OK400482","Caddo Nation of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400483","Cherokee Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400484","Cheyenne-Arapaho Tribes"));
		oklahoma.add(new AmericanAlaskaTribe("OK400485","Chickasaw Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400486","Choctaw Nation of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400487","Citizen Potawatomi Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400488","Comanche Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400489","Delaware Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400490","Delaware Tribe of Indians"));
		oklahoma.add(new AmericanAlaskaTribe("OK400491","Eastern Shawnee Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400492","Fort Sill Apache Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400493","Iowa Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400494","Kaw Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400495","Kialegee Tribal Town"));
		oklahoma.add(new AmericanAlaskaTribe("OK400496","Kickapoo Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400497","Kiowa Indian Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400498","Miami Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400499","Modoc Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400500","Muscogee (Creek) Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400501","Osage Tribe"));
		oklahoma.add(new AmericanAlaskaTribe("OK400502","Ottawa Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400503","Otoe-Missouria Tribe of Indians"));
		oklahoma.add(new AmericanAlaskaTribe("OK400504","Pawnee Nation of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400505","Peoria Tribe of Indians of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400506","Ponca Tribe of Indians of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400507","Quapaw Tribe of Indians"));
		oklahoma.add(new AmericanAlaskaTribe("OK400508","Sac & Fox Nation"));
		oklahoma.add(new AmericanAlaskaTribe("OK400509","Seminole Nation of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400510","Seneca-Cayuga Tribe of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400511","Shawnee Tribe"));
		oklahoma.add(new AmericanAlaskaTribe("OK400512","Thlopthlocco Tribal Town"));
		oklahoma.add(new AmericanAlaskaTribe("OK400513","Tonkawa Tribe of Indians of Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400514","United Keetoowah Band of Cherokee Indians in Oklahoma"));
		oklahoma.add(new AmericanAlaskaTribe("OK400515","Wichita and Affiliated Tribes (Wichita, Keechi, Waco and Tawakonie)"));
		oklahoma.add(new AmericanAlaskaTribe("OK400516","Wyandotte Nation"));
		americanAlaskaStateTribeMap.put("OK", oklahoma);
		
		List<AmericanAlaskaTribe> oregon = new ArrayList<AmericanAlaskaTribe>();
		oregon.add(new AmericanAlaskaTribe("OR410517","Burns Paiute Tribe"));
		oregon.add(new AmericanAlaskaTribe("OR410518","Confederated Tribes of the Coos, Lower Umpqua and Siuslaw Indians of Oregon"));
		oregon.add(new AmericanAlaskaTribe("OR410519","Confederated Tribes of the Grand Ronde Community of Oregon"));
		oregon.add(new AmericanAlaskaTribe("OR410520","Confederated Tribes of the Siletz Reservation"));
		oregon.add(new AmericanAlaskaTribe("OR410521","Confederated Tribes of the Umatilla Reservation"));
		oregon.add(new AmericanAlaskaTribe("OR410522","Confederated Tribes of the Warm Springs Reservation of Oregon"));
		oregon.add(new AmericanAlaskaTribe("OR410523","Coquille Tribe of Oregon"));
		oregon.add(new AmericanAlaskaTribe("OR410524","Cow Creek Band of Umpqua Indians of Oregon"));
		oregon.add(new AmericanAlaskaTribe("OR410525","Fort McDermitt Paiute and Shoshone Tribes of the Fort McDermitt Indian Reservation (Nevada and Oregon)"));
		oregon.add(new AmericanAlaskaTribe("OR410526","Klamath Tribes (formerly the Klamath Indian Tribe of Oregon)"));
		americanAlaskaStateTribeMap.put("OR", oregon);
		
		List<AmericanAlaskaTribe> rhodeIsland = new ArrayList<AmericanAlaskaTribe>(_1);
		rhodeIsland.add(new AmericanAlaskaTribe("RI440527","Narragansett Indian Tribe of Rhode Island"));
		americanAlaskaStateTribeMap.put("RI", rhodeIsland);
		
		List<AmericanAlaskaTribe> southCarolina = new ArrayList<AmericanAlaskaTribe>(_1);
		southCarolina.add(new AmericanAlaskaTribe("SC450528","Catawba Indian Nation (Catawba Tribe of South Carolina)"));
		americanAlaskaStateTribeMap.put("SC", southCarolina);
		
		List<AmericanAlaskaTribe> southDakota = new ArrayList<AmericanAlaskaTribe>();
		southDakota.add(new AmericanAlaskaTribe("SD460529","Cheyenne River Sioux Tribe of the Cheyenne River Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460530","Crow Creek Sioux Tribe of the Crow Creek Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460531","Flandreau Santee Sioux Tribe of South Dakota"));
		southDakota.add(new AmericanAlaskaTribe("SD460532","Lower Brule Sioux Tribe of the Lower Brule Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460533","Oglala Sioux Tribe of the Pine Ridge Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460534","Rosebud Sioux Tribe of the Rosebud Indian Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460535","Sisseton-Wahpeton Oyate of the Lake Traverse Reservation"));
		southDakota.add(new AmericanAlaskaTribe("SD460536","Standing Rock Sioux Tribe (North Dakota and South Dakota)"));
		southDakota.add(new AmericanAlaskaTribe("SD460537","Yankton Sioux Tribe of South Dakota"));
		americanAlaskaStateTribeMap.put("SD", southDakota);
		
		List<AmericanAlaskaTribe> texas = new ArrayList<AmericanAlaskaTribe>(_3);
		texas.add(new AmericanAlaskaTribe("TX480538","Alabama-Coushatta Tribes"));
		texas.add(new AmericanAlaskaTribe("TX480539","Kickapoo Traditional Tribe of Texas"));
		texas.add(new AmericanAlaskaTribe("TX480540","Ysleta Del Sur Pueblo of Texas"));
		americanAlaskaStateTribeMap.put("TX", texas);
		
		List<AmericanAlaskaTribe> utah = new ArrayList<AmericanAlaskaTribe>(_7);
		utah.add(new AmericanAlaskaTribe("UT490541","Confederated Tribes of the Goshute Reservation (Nevada and Utah)"));
		utah.add(new AmericanAlaskaTribe("UT490542","Navajo Nation (Arizona, New Mexico and Utah)"));
		utah.add(new AmericanAlaskaTribe("UT490543","Northwestern Band of Shoshoni Nation of Utah (Washakie)"));
		utah.add(new AmericanAlaskaTribe("UT490544","Paiute Indian Tribe of Utah  (Cedar City Band of Paiutes, Kanosh Band of Paiutes, Koosharem Band of Paiutes, Indian Peaks Band of Paiutes, and Shivwits Band of Paiutes)"));
		utah.add(new AmericanAlaskaTribe("UT490545","Skull Valley Band of Goshute Indians of Utah"));
		utah.add(new AmericanAlaskaTribe("UT490546","Ute Indian Tribe of the Uintah and Ouray Reservation"));
		utah.add(new AmericanAlaskaTribe("UT490547","Ute Mountain Ute Tribe (Colorado, New Mexico and Utah)"));
		americanAlaskaStateTribeMap.put("UT", utah);
		
		List<AmericanAlaskaTribe> washington = new ArrayList<AmericanAlaskaTribe>(_30);
		washington.add(new AmericanAlaskaTribe("WA530548","Confederated Tribes of the Chehalis Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530549","Confederated Tribes of the Colville Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530550","Confederated Tribes and Bands of the Yakama Nation"));
		washington.add(new AmericanAlaskaTribe("WA530551","Cowlitz Indian Tribe"));
		washington.add(new AmericanAlaskaTribe("WA530552","Hoh Indian Tribe of the Hoh Indian Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530553","Jamestown S'Klallam Tribe of Washington"));
		washington.add(new AmericanAlaskaTribe("WA530554","Kalispel Indian Community of the Kalispel Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530555","Lower Elwha Tribal Community of the Lower Elwha Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530556","Lummi Tribe of the Lummi Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530557","Makah Indian Tribe of the Makah Indian Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530558","Muckleshoot Indian Tribe of the Muckleshoot Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530559","Nisqually Indian Tribe of the Nisqually Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530560","Nooksack Indian Tribe of Washington"));
		washington.add(new AmericanAlaskaTribe("WA530561","Port Gamble Indian Community of the Port Gamble Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530562","Puyallup Tribe of the Puyallup Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530563","Quileute Tribe of the Quileute Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530564","Quinault Tribe of the Quinault Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530565","Samish Indian Tribe"));
		washington.add(new AmericanAlaskaTribe("WA530566","Sauk-Suiattle Indian Tribe of Washington"));
		washington.add(new AmericanAlaskaTribe("WA530567","Shoalwater Bay Tribe of the Shoalwater Bay Indian Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530568","Skokomish Indian Tribe of the Skokomish Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530569","Snoqualmie Tribe"));
		washington.add(new AmericanAlaskaTribe("WA530570","Spokane Tribe of the Spokane Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530571","Squaxin Island Tribe of the Squaxin Island Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530572","Stillaguamish Tribe of Washington"));
		washington.add(new AmericanAlaskaTribe("WA530573","Suquamish Indian Tribe of the Port Madison Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530574","Swinomish Indians of the Swinomish Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530575","Tulalip Tribes of the Tulalip Reservation"));
		washington.add(new AmericanAlaskaTribe("WA530576","Upper Skagit Indian Tribe of Washington"));
		americanAlaskaStateTribeMap.put("WA", washington);
		
		List<AmericanAlaskaTribe> wisconsin = new ArrayList<AmericanAlaskaTribe>(_12);
		wisconsin.add(new AmericanAlaskaTribe("WI550577","Bad River Band of the Lake Superior Tribe of Chippewa Indians"));
		wisconsin.add(new AmericanAlaskaTribe("WI550578","Forest County Potawatomi Community"));
		wisconsin.add(new AmericanAlaskaTribe("WI550579","Ho-Chunk Nation of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550580","Lac Courte Oreilles Band of Lake Superior Chippewa Indians of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550581","Lac du Flambeau Band of Lake Superior Chippewa Indians of the Lac du Flambeau"));
		wisconsin.add(new AmericanAlaskaTribe("WI550582","Reservation of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550583","Menominee Indian Tribe of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550584","Oneida Tribe of Indians of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550585","Red Cliff Band of Lake Superior Chippewa Indians of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550586","St. Croix Chippewa Indians of Wisconsin"));
		wisconsin.add(new AmericanAlaskaTribe("WI550587","Sokaogon Chippewa Community"));
		wisconsin.add(new AmericanAlaskaTribe("WI550588","Stockbridge Munsee Community"));
		americanAlaskaStateTribeMap.put("WI", wisconsin);
		
		List<AmericanAlaskaTribe> wyoming = new ArrayList<AmericanAlaskaTribe>(_2);
		wyoming.add(new AmericanAlaskaTribe("WY560589","Arapahoe Tribe of the Wind River Reservation"));
		wyoming.add(new AmericanAlaskaTribe("WY560590","Shoshone Tribe of the Wind River Reservation"));
		americanAlaskaStateTribeMap.put("WY", wyoming);
		
	}


	public static List<AmericanAlaskaTribe> getAmericanAlaskaStateTribeName(String stateCode) {
		return americanAlaskaStateTribeMap.get(stateCode);
	}

	/**
	 * Returns list map of 2 letter State Code -> List&lt;AmericanAlaskaTribe&gt;.
	 * @return Map of [state code, list of tribes].
	 */
	public static Map<String, List<AmericanAlaskaTribe>> getAlaskaNativeAndTribeList() {
		return americanAlaskaStateTribeMap;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> simpleMapValueSort(final Map<K, V> mapToSort) {
        List<Map.Entry<K, V>> entries = new ArrayList<Map.Entry<K, V>>(mapToSort.size());
 
        entries.addAll(mapToSort.entrySet());
 
        Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(final Map.Entry<K, V> entry1, final Map.Entry<K, V> entry2) {
                return entry1.getValue().compareTo(entry2.getValue());
            }
        });
 
        Map<K, V> sortedCrunchifyMap = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : entries) {
            sortedCrunchifyMap.put(entry.getKey(), entry.getValue());
        }
        return sortedCrunchifyMap;
    }
}
