/**
 * 
 */
package com.getinsured.eligibility.phix.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.prescreen.repository.IEligLeadRepository;
import com.getinsured.eligibility.prescreen.repository.IStateChipMedicaidRepository;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;

/**
 * Service Implementation for PHIX Services
 * 
 * @author Nikhil Talreja
 * @since  August 28, 2013
 */
@Service("phixService")
public class PhixServiceImpl implements PhixService {
	
	
	@Autowired private IEligLeadRepository iEligLeadRepository;
	@Autowired private IStateChipMedicaidRepository iStateChipMedicaidRepository;
	
	@Override
	public EligLead getEligLeadRecord(long id) {
		return iEligLeadRepository.findOne(id);
	}
	
	@Override
	public EligLead saveEligLeadRecord(EligLead record){
		return iEligLeadRepository.save(record);
	}

	@Override
	public StateChipMedicaid getStateChipMedicaidRecordByState(String stateCode) {
		return iStateChipMedicaidRepository.findByStateCode(stateCode);
	}

	@Override
	public List<EligLead> fetchNewRecordsForOutBoundCall(Long batchStage, String api) {
		return iEligLeadRepository.getAllOutboundCallLeads(batchStage, api);
	}
}
