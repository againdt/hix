(function (models) {
	models.Favorite = Backbone.Model.extend({
		
		urlRoot : "getEmployeeFavorites",

		//to specify default attributes for model
		defaults: {
			"id"		: "",
		}
	});
})(App.models);