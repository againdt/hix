package com.getinsured.hix.indportal.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PlanSummaryDetails {

	private String caseNumber;
	private String planLogoUrl;
	private String planType;
	private String planName;
	private String officeVisit;
	private String genericMedications;
	private String deductible;
	private String oopMax;
	private String enrollmentStatus;
	private String planId;
	
	private String policyId;
	private String coverageStartDate;
	private String coverageEndDate;
	
	private String premium;
	private String paymentUrl;
	private String issuerContactUrl;
	private String issuerContactPhone;
	private String enrollmentId;
	private String typeOfPlan;
	private List<PlanSummaryMember> coveredMembers;
	private boolean disEnroll;
	private String electedAptc;
	private BigDecimal stateSubsidyAmount;
	private String netPremium;
	private String maxAptc;
	private Boolean isPaymentRejected;
	//HIX-63747
	private Date enrollmentCreationTimestamp;
	private String isPlanEffectuated = "N" ;
	private String endOfCurrentMonth;
	private String endOfNextMonth;
	private String endOfMonthAfterNext;
	
	private boolean allowAptcAdjustment;
	private Float essEhbPrmAmt;
	
	private String premiumEffDate;
	private Long ssapApplicationId;
	private String allowChangePlan;
	private boolean insideOEEnrollmentWindow;
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getPlanLogoUrl() {
		return planLogoUrl;
	}

	public void setPlanLogoUrl(String planLogoUrl) {
		this.planLogoUrl = planLogoUrl;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}

	public String getGenericMedications() {
		return genericMedications;
	}

	public void setGenericMedications(String genericMedications) {
		this.genericMedications = genericMedications;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getIssuerContactUrl() {
		return issuerContactUrl;
	}

	public void setIssuerContactUrl(String issuerContactUrl) {
		this.issuerContactUrl = issuerContactUrl;
	}

	public String getIssuerContactPhone() {
		return issuerContactPhone;
	}

	public void setIssuerContactPhone(String issuerContactPhone) {
		this.issuerContactPhone = issuerContactPhone;
	}

	public List<PlanSummaryMember> getCoveredMembers() {
		return coveredMembers;
	}

	public void setCoveredMembers(List<PlanSummaryMember> coveredMembers) {
		this.coveredMembers = coveredMembers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PlanSummaryDetails [caseNumber=");
		builder.append(caseNumber);
		builder.append(", planLogoUrl=");
		builder.append(planLogoUrl);
		builder.append(", planType=");
		builder.append(planType);
		builder.append(", planName=");
		builder.append(planName);
		builder.append(", officeVisit=");
		builder.append(officeVisit);
		builder.append(", genericMedications=");
		builder.append(genericMedications);
		builder.append(", deductible=");
		builder.append(deductible);
		builder.append(", oopMax=");
		builder.append(oopMax);
		builder.append(", enrollmentStatus=");
		builder.append(enrollmentStatus);
		builder.append(", planId=");
		builder.append(planId);
		builder.append(", policyId=");
		builder.append(policyId);
		builder.append(", coverageStartDate=");
		builder.append(coverageStartDate);
		builder.append(", coverageEndDate=");
		builder.append(coverageEndDate);
		builder.append(", premium=");
		builder.append(premium);
		builder.append(", paymentUrl=");
		builder.append(paymentUrl);
		builder.append(", issuerContactUrl=");
		builder.append(issuerContactUrl);
		builder.append(", issuerContactPhone=");
		builder.append(issuerContactPhone);
		builder.append(", coveredMembers=");
		builder.append(coveredMembers);
		builder.append(", electedAptc=");
		builder.append(electedAptc);
		builder.append(", netPremium=");
		builder.append(netPremium);
		builder.append("]");
		return builder.toString();
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getTypeOfPlan() {
		return typeOfPlan;
	}

	public void setTypeOfPlan(String typeOfPlan) {
		this.typeOfPlan = typeOfPlan;
	}

	public boolean getDisEnroll() {
		return disEnroll;
	}

	public void setDisEnroll(boolean disEnroll) {
		this.disEnroll = disEnroll;
	}

	public String getElectedAptc() {
		return electedAptc;
	}

	public void setElectedAptc(String electedAptc) {
		this.electedAptc = electedAptc;
	}

	public String getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(String netPremium) {
		this.netPremium = netPremium;
	}

	public String getMaxAptc() {
		return maxAptc;
	}

	public void setMaxAptc(String maxAptc) {
		this.maxAptc = maxAptc;
	}

	public Boolean getIsPaymentRejected() {
		return isPaymentRejected;
	}

	public void setIsPaymentRejected(Boolean isPaymentRejected) {
		this.isPaymentRejected = isPaymentRejected;
	}

	public Date getEnrollmentCreationTimestamp() {
		return enrollmentCreationTimestamp;
	}

	public void setEnrollmentCreationTimestamp(Date enrollmentCreationTimestamp) {
		this.enrollmentCreationTimestamp = enrollmentCreationTimestamp;
	}

	public String getIsPlanEffectuated() {
		return isPlanEffectuated;
	}

	public void setIsPlanEffectuated(String isPlanEffectuated) {
		this.isPlanEffectuated = isPlanEffectuated;
	}

	public String getEndOfCurrentMonth() {
		return endOfCurrentMonth;
	}

	public void setEndOfCurrentMonth(String endOfCurrentMonth) {
		this.endOfCurrentMonth = endOfCurrentMonth;
	}

	public String getEndOfNextMonth() {
		return endOfNextMonth;
	}

	public void setEndOfNextMonth(String endOfNextMonth) {
		this.endOfNextMonth = endOfNextMonth;
	}

	public String getEndOfMonthAfterNext() {
		return endOfMonthAfterNext;
	}

	public void setEndOfMonthAfterNext(String endOfMonthAfterNext) {
		this.endOfMonthAfterNext = endOfMonthAfterNext;
	}

	/**
	 * @return the allowAptcAdjustment
	 */
	public boolean isAllowAptcAdjustment() {
		return allowAptcAdjustment;
	}

	/**
	 * @param allowAptcAdjustment the allowAptcAdjustment to set
	 */
	public void setAllowAptcAdjustment(boolean allowAptcAdjustment) {
		this.allowAptcAdjustment = allowAptcAdjustment;
	}

	public Float getEssEhbPrmAmt() {
		return essEhbPrmAmt;
	}

	public void setEssEhbPrmAmt(Float essEhbPrmAmt) {
		this.essEhbPrmAmt = essEhbPrmAmt;
	}

	/**
	 * @return the premiumEffDate
	 */
	public String getPremiumEffDate() {
		return premiumEffDate;
	}

	/**
	 * @param premiumEffDate the premiumEffDate to set
	 */
	public void setPremiumEffDate(String premiumEffDate) {
		this.premiumEffDate = premiumEffDate;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getAllowChangePlan() {
		return allowChangePlan;
	}

	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}

	public BigDecimal getStateSubsidyAmount() {
		return stateSubsidyAmount;
	}

	public void setStateSubsidyAmount(BigDecimal stateSubsidyAmount) {
		this.stateSubsidyAmount = stateSubsidyAmount;
	}

	public boolean isInsideOEEnrollmentWindow() {
		return insideOEEnrollmentWindow;
	}

	public void setInsideOEEnrollmentWindow(boolean insideOEEnrollmentWindow) {
		this.insideOEEnrollmentWindow = insideOEEnrollmentWindow;
	}
}
