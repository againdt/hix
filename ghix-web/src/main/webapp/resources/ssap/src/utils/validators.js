import moment from 'moment';

import VALIDATION_TYPES from '../constants/validationTypes';

export const TEMP_VALIDATORS_FOR_DATEPICKER = {
    [VALIDATION_TYPES.IS_ALL_NUMERIC]: {
        verifier: value => {
            if(typeof value !== 'string') {
                console.log('Input is not a string in validator!');
                return false;
            }
            const regex = /^[0-9]+$/;
            return !!value.match(regex);
        },
        message: 'Please enter digits only!'
    },
    [VALIDATION_TYPES.IS_NOT_EMPTY]: {
        verifier: value => {
            return !!value;
        },
        message: 'This is empty! Provide some value!'
    }
};

const checkRegex = (value, regex, affordableValues = undefined, reverse = undefined) => {
    if(affordableValues && affordableValues.includes(value)) {
        return true;
    } else {
        if(value === null) {
            return false;
        } else {
            return reverse ?
                !(value && value.match(regex)) :
                !!(value && value.match(regex));
        }
    }
};

const methods = {
    address: ([value]) => checkRegex(value,/^[A-Za-z0-9-.# ]*$/),

    adjustTime: ([value]) => !!moment(value).toDate().getTime(),

    alphanumeric: ([value]) => checkRegex(value, /^[A-Za-z0-9]/),

    alienNumber: ([value]) => checkRegex(value, /^[0-9]/),

    checked: ([value]) => !!value,

    checkIfUnique: ([ value, setOfUniques ]) =>  !setOfUniques.has(value),

    city: ([value]) => checkRegex(value, /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/),

    dateExceed: ([value]) => moment(value).toDate().getTime() > new Date().getTime(),

    dateNotGreaterThanCurrentDate: ([value]) => {
        return moment(value).toDate().getTime() <= new Date().getTime();
    },

    dateNotLessThanCHIPDenialDate: ([value]) => {
        return (moment(value).toDate().getTime() < new Date().getTime() && moment(value).toDate().getTime() > moment().subtract(91, 'days'));
    },

    dateMinimumThreshold: ([value]) => moment(value).toDate().getTime() >= moment('1900-01-01').toDate().getTime(),

    dateNotNull: ([value]) => value !== null,

    digits: ([value]) => checkRegex(String(value), /^[0-9]*$/),

    digitsOrEmpty: ([value]) => checkRegex(value, /^[0-9]*$/, ['', null]),

    // eslint-disable-next-line
    email: ([value]) => checkRegex(value, /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/),

    addressOrEmpty: ([value]) => checkRegex(value,/^[A-Za-z0-9-.# ]*$/, ['', null]),

    exactLength: ([value ,, length]) => value && value.length === length,

    exactMatch: ([value, valuesToMatch]) => valuesToMatch.some(valueToMatch => valueToMatch.toLowerCase() === value.toLowerCase()),

    greenCard: ([value]) => checkRegex(value, /^[a-zA-Z]{3}[0-9]/),

    maxLength: ([value ,, length]) => (value === null || value === '') ? true :  String(value).length <= length,

    middleName: ([value]) => checkRegex(value,/^[a-zA-Z -.']+$/, ['', null]),

    minLength: ([value ,, length]) => (value === null || value === '') ? true :  String(value).length >= length,

    noLetters: ([value]) =>  checkRegex(value, /[a-zA-Z]/, [null], true),

    notEmpty: ([value])  => value !== '' && value !== null,

    notZeroAndNotEmpty: ([value]) => value !== 0 && value !== '' && value !== null,

    personName: ([value]) =>  checkRegex(value,/^[a-zA-Z -.']+$/),

    phoneNumber: ([value]) => {
        if (value === null || value === '') {
            return true;
        } else {
            const numbersQty = typeof value === 'string' && value.split('').filter(c => c.match(/^[0-9]*$/)).join('').length;
            return numbersQty === 10;
        }
    },

    range: ([value, rangeToMatch]) => {
        // For negative income sources amounts: eligible only tribal income is equal to 0
        if(rangeToMatch.length === 0) {
            return value === 0;
        }
        // Common case
        return +value >= rangeToMatch[0] && value <= rangeToMatch[1];
    },

    sevisIDNumber: ([value]) =>  checkRegex(value, /^[0-9]/),

    checkIfSpecified: ([value]) => value !== 'UNSPECIFIED',

    zipCode: ([value]) =>  checkRegex(value,/^\d{5}(-\d{4})?$/),
};

const VALIDATION_CONFIG = [
    {
        validatorName: VALIDATION_TYPES.ADDRESS_LINE1,
        payload: {
            methods: [
                'address',
                ['minLength', 5],
                ['maxLength', 75]
            ],
            messages: [
                'Please enter a valid address.',
                'Please enter AL 1'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.ADDRESS_LINE2,
        payload: {
            methods: [
                'addressOrEmpty',
                ['minLength', 1],
                ['maxLength', 55],
            ],
            messages: [
                'Please enter a valid address.',
                'Please enter address line 2'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.ALIEN_NUMBER,
        payload: {
            methods: [
                'digits',
                ['exactLength', 9]
            ],
            messages: [
                'Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).' +
                'When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. ' +
                'If the number is an older issued A-Number, add leading 0s to ensure nine digits.',
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.ALL_DIGITS,
        payload: {
            methods: [
                'digits',
                ['minLength', 1],
                ['maxLength', 2]
            ],
            messages: [
                'Please enter a valid number between 1 and 19'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.CERTIFICATE_NUMBER,
        payload: {
            methods: [
                'digits',
                ['minLength', 6],
                ['maxLength', 12],
            ],
            messages: ['Please enter your Naturalization number. This will contain 6-12 digits.']
        }
    },
    {
        validatorName: VALIDATION_TYPES.CITIZENSHIP_NUMBER,
        payload: {
            methods: [
                'alphanumeric',
                ['minLength', 6],
                ['maxLength', 12],
            ],
            messages: [
                'Please enter your Citizenship number. This will contain 6-12 digits'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.CHECKED,
        payload: {
            methods: [
                'checked'
            ],
            messages: [
                'Please check this box to continue'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.CHOSEN,
        payload: {
            methods: [
                'notEmpty'
            ],
            messages: [
                'Please provide a response here to continue'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.CITY,
        payload: {
            methods: [
                'city',
                ['minLength', 1],
                ['maxLength', 30]
            ],
            messages: [
                'Please enter a valid city.',
                'Please enter city name'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.DATE,
        payload: {
            methods: [
                'adjustTime',
                'dateMinimumThreshold',
                'dateNotNull'
            ],
            messages: [
                'Please enter a correct date',
                'Please enter a date'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.DATE_FUTURE_ARBITRARY,
        payload: {
            methods: [
                'adjustTime',
                'dateExceed',
                'dateNotNull'
            ],
            messages: [
                'Please enter a correct date',
                'Please enter a date'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.DATE_NOT_GREATER_THAN_CURRENT,
        payload: {
            methods: [
                'adjustTime',
                'dateNotGreaterThanCurrentDate',
                'dateMinimumThreshold',
                'dateNotNull'
            ],
            messages: [
                'Please enter a correct date',
                'Please enter a date'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.DATE_VALID_CHIP_DENIAL_DATE,
        payload: {
            methods: [
                'adjustTime',
                'dateNotLessThanCHIPDenialDate',
                'dateMinimumThreshold',
                'dateNotNull'
            ],
            messages: [
                'Please enter a date within the last 90 days',
                'Please enter a date'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.EMAIL,
        payload: {
            methods: [
                'email',
                ['maxLength', 100]
            ],
            messages: [
                'Please enter a valid email address',
                'Please enter an email'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.I_551,
        payload: {
            methods: [
                'greenCard',
                ['exactLength', 13]
            ],
            messages: [
                'Please enter the Card Number. Card Number is 13 characters long, with the first 3 characters alpha and the remaining 10 characters numeric.'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.I_94,
        payload: {
            methods: [
                'alphanumeric',
                ['exactLength', 11]
            ],
            messages: [
                'Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.IS_SIGNED,
        payload: {
            methods: [
                'exactMatch',
            ],
            messages: [
                'Please enter a valid signature'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.NAME,
        payload: {
            methods: [
                'personName',
                ['minLength', 1],
                ['maxLength', 60]
            ],
            messages: [
                'Please enter a valid name',
                'Please enter a name'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.MIDDLE_NAME,
        payload: {
            methods: [
                'middleName',
                ['minLength', 1],
                ['maxLength', 60]
            ],
            messages: [
                'Please enter a valid name',
                'Please enter a name'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.MONEY,
        payload: {
            methods: [
                'notZeroAndNotEmpty'
            ],
            messages: [
                'Please provide a dollar amount'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.MONEY_NOT_EXCEED,
        payload: {
            methods: [
                'range'
            ],
            messages: [
                'Please provide a dollar amount less than income source amount'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.NATURALIZATION_NUMBER,
        payload: {
            methods: [
                'alphanumeric',
                ['minLength', 6],
                ['maxLength', 12]
            ],
            messages: [
                'Please enter a valid naturalization number',
                'Please enter a naturalization number'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.NOT_EMPTY,
        payload: {
            methods: [
                'notEmpty',
            ],
            messages: [
                'Please provide a response here'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.PASSPORT,
        payload: {
            methods: [
                'alphanumeric',
                ['minLength', 6],
                ['maxLength', 12]
            ],
            messages: [
                'Please enter your passport number. This generally contains 6-12 alphanumeric characters.',
                'Please enter a passport number'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.PHONE_EXTENSION,
        payload: {
            methods: [
                'digitsOrEmpty',
                ['maxLength', 4]
            ],
            messages: [
                'Please enter a valid phone extension',
                'Please enter a phone extension'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.PHONE_NUMBER,
        payload: {
            methods: [
                'phoneNumber',
                'noLetters',
                ['maxLength', 14]
            ],
            messages: [
                'Please enter a valid phone number',
                'Please enter a phone number'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.RANGE,
        payload: {
            methods: [
                'range'
            ],
            messages: [
                'Please enter a valid number',
                'Please enter a number'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.SEVIS_ID,
        payload: {
            methods: [
                'digits',
                ['exactLength', 10]
            ],
            messages: [
                'Please enter the SEVIS ID number. This always starts with an N followed by 10 numeric digits.' +
                ' This is usually located at the top, right-hand side of the document.When entering a SEVIS Number, ' +
                'remove the leading N and enter the 10 digits, including the leading 0s.'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.SPECIFIED,
        payload: {
            methods: [
                'checkIfSpecified'
            ],
            messages: [
                'Please provide a response here to continue'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.SSN,
        payload: {
            methods: [
                'digits',
                ['exactLength', 9]
            ],
            messages: [
                'Please enter a valid SSN',
                'Please enter a SSN'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.VISA,
        payload: {
            methods: [
                'alphanumeric',
                ['exactLength', 8]
            ],
            messages: [
                'Please enter your visa number, usually printed in red at the bottom right side of the document. It contains 8 alphanumeric characters.'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.UNIQUE,
        payload: {
            methods: [
                'checkIfUnique'
            ],
            messages: [
                'Please enter a unique SSN',
                'Please enter a SSN'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.ZIP_CODE,
        payload: {
            methods: [
                'zipCode',
            ],
            messages: [
                'Please enter a valid zip code.',
                'Please enter a zip code'
            ]
        }
    },
    {
        validatorName: VALIDATION_TYPES.DOCUMENT_DESCRIPTION,
        payload: {
            methods: [
                ['minLength', 1],
                ['maxLength', 35]
            ],
            messages: [
                'Please provide a brief document description.',
            ]
        }
    }
];

function * gen(values, type) {
    const methodsForValidation = VALIDATION_CONFIG.find(validator => validator.validatorName === type)
        .payload
        .methods
        .map(methodDescription => {
            if(Array.isArray(methodDescription)) {              // Checking if method has to be run with arguments
                const [ method, ...args ] = methodDescription;
                return methods[method]([...values, ...args]);   // Yes. We pass values for validation and arguments
            } else {
                return methods[methodDescription]([...values]); // No. We pass only values
            }
        });

    for (const m of methodsForValidation) {
        yield m;
    }
}

export default VALIDATION_CONFIG.map(type => ({
    validatorType: VALIDATION_TYPES[type.validatorName],
    verifier: ({value, valuesToMatch, isGenShown}) => {
        const result = [...gen([value, valuesToMatch], VALIDATION_TYPES[type.validatorName])];
        isGenShown && console.log(`${type.validatorName} validation result:`, result);
        return result.every(check => check);
    },
    messages: type.payload.messages
}));

