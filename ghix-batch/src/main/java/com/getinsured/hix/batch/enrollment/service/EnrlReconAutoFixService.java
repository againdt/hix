package com.getinsured.hix.batch.enrollment.service;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;

public interface EnrlReconAutoFixService {
	void processAutoFix(Integer fileId, Job job, JobLauncher jobLauncher);
}
