package com.getinsured.hix.serff.service.networkTransparency;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to persist data in Network Transparency table.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
public class NetworkTransparencyVO {

	private String planYear;
	private String issuerName;
	private String hiosIssuerID;

	private List<NetworkTransparencyDataVO> networkTransData;
	private boolean isNotValid;
	private String errorMessages;

	private Map<String, String> mapCountyFips;

	public class NetworkTransparencyDataVO {

		private String networkID;
		private String planHiosID;
		private String county;
		private String rating;

		public String getNetworkID() {
			return networkID;
		}

		public void setNetworkID(String networkID) {
			this.networkID = networkID;
		}

		public String getPlanHiosID() {
			return planHiosID;
		}

		public void setPlanHiosID(String hiosID) {
			this.planHiosID = hiosID;
		}

		public String getCounty() {
			return county;
		}

		public void setCounty(String county) {
			this.county = county;
		}

		public String getCountyFips() {

			if (StringUtils.isNotBlank(county) && !CollectionUtils.isEmpty(mapCountyFips)) {
				return mapCountyFips.get(county.toLowerCase());
			}
			else {
				return null;
			}
		}

		public String getRating() {
			return rating;
		}

		public void setRating(String rating) {
			this.rating = rating;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder("NetworkTransparencyDataVO");
			sb.append(" [networkID=");
			sb.append(networkID);
			sb.append(", planHiosID=");
			sb.append(planHiosID);
			sb.append(", county=");
			sb.append(county);
			sb.append(", countyFips=");
			sb.append(getCountyFips());
			sb.append(", rating=");
			sb.append(rating);
			sb.append("]");
			return sb.toString();
		}
	}

	public String getPlanYear() {
		return planYear;
	}

	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosIssuerID() {
		return hiosIssuerID;
	}

	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}

	public NetworkTransparencyDataVO getNetworkTransDataVO() {
		return new NetworkTransparencyDataVO();
	}

	public List<NetworkTransparencyDataVO> getNetworkTransData() {
		return networkTransData;
	}

	public void setNetworkTransDataVOInList(NetworkTransparencyDataVO networkTransDataVO) {

		if (CollectionUtils.isEmpty(this.networkTransData)) {
			this.networkTransData = new ArrayList<NetworkTransparencyDataVO>();
		}
		this.networkTransData.add(networkTransDataVO);
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("NetworkTransparencyVO");
		sb.append(" [planYear=");
		sb.append(planYear);
		sb.append(", issuerName=");
		sb.append(issuerName);
		sb.append(", hiosIssuerID=");
		sb.append(hiosIssuerID);
		sb.append(", networkTransData= (");

		if (!CollectionUtils.isEmpty(this.networkTransData)) {
			sb.append(networkTransData.toString());
		}
		else {
			sb.append("NULL");
		}
		sb.append("), isNotValid=");
		sb.append(isNotValid);
		sb.append(", errorMessages=");
		sb.append(errorMessages);
		sb.append("]");
		return sb.toString();
	}

	public void setCountyFipsInMap(String countyName, String countyFips) {

		if (CollectionUtils.isEmpty(this.mapCountyFips)) {
			this.mapCountyFips = new HashMap<String, String>();
		}
		this.mapCountyFips.put(countyName.toLowerCase(), countyFips);
	}
}
