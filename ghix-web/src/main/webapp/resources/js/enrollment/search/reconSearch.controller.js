(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .controller('searchContrl', [ '$scope', '$timeout', '$stateParams', '$state', 'searchService', 'searchNotifyingService', 'dataStorageService', 'searchReconciliation', 'searchPaginationService', 'clearSearchResultsService', function($scope, $timeout, $stateParams, $state, searchService, searchNotifyingService, dataStorageService, searchReconciliation, searchPaginationService, clearSearchResultsService) {
            $scope.searchFilters = {};
            $scope.searchParameters = {};
            $scope.monthList = [ 'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December' ];

            $scope.initializeSearchFilters = function () {
                var searchParams = searchReconciliation.getIssuer();

                // if detail page is refreshed, all the searched params are lost, in that case lets get it from localstorage.
                if(searchParams && !searchParams.reconYear){
                    searchParams = dataStorageService.getItem("issuerData");
                }

                $(document).ready(function() {
                    $('#myTab li:nth-child(2)').trigger( "click" );

                    $(".nav-tabs > li").click(function () {
                        $(".nav-tabs > li").removeClass("active");
                        $(this).addClass("active");
                    });
                });

                searchService.getSearchFilters().then(
                    function(filters) {
                        // $timeout(function() {
                        $scope.searchFilters = filters;
                        $scope.enrlDiscrepancyLookupList = [];
                        $scope.enrlDiscrepancyLookupList.push({"id": -1, "label": "All"});
                        for(var j = 0; j < $scope.searchFilters.enrlSearchDiscrepancyDTO.enrlDiscrepancyLookupList.length; j++) {
                            $scope.enrlDiscrepancyLookupList.push($scope.searchFilters.enrlSearchDiscrepancyDTO.enrlDiscrepancyLookupList[j]);
                        }
                        $scope.discrepancyStatusList = [];
                        $scope.discrepancyStatusList.push("ALL");
                        for(var i = 0; i < $scope.searchFilters.enrlSearchDiscrepancyDTO.discrepancyStatusList.length; i++) {
                            $scope.discrepancyStatusList.push($scope.searchFilters.enrlSearchDiscrepancyDTO.discrepancyStatusList[i]);
                        }

                        $scope.issuerDetailsList = [];
                        $scope.issuerDetailsList.push({ "issuerId": -1, "hiosIssuerId": "-1", "issuerName": "All"});
                        for(var k = 0; k < $scope.searchFilters.enrlSearchDiscrepancyDTO.issuerDetailsList.length; k++) {
                            $scope.issuerDetailsList.push($scope.searchFilters.enrlSearchDiscrepancyDTO.issuerDetailsList[k]);
                        }
                        //$scope.issuerDetailsList = $scope.searchFilters.enrlSearchDiscrepancyDTO.issuerDetailsList;

                        $scope.coverageYearList = [];
                        $scope.coverageYearList.push("ALL");
                        for(var m = 0; m < $scope.searchFilters.enrlSearchDiscrepancyDTO.coverageYearList.length; m++) {
                            $scope.coverageYearList.push($scope.searchFilters.enrlSearchDiscrepancyDTO.coverageYearList[m]);
                        }

                        //$scope.coverageYearList = $scope.searchFilters.enrlSearchDiscrepancyDTO.coverageYearList;

                        $scope.reconYearList = $scope.searchFilters.enrlSearchDiscrepancyDTO.reconYearList;

                        $scope.reconMonthList = $scope.monthList;

                        $timeout(function() {

                            // $("#discrepancyStatus").val($stateParams.issuer.status).change();
                            if($scope.searchObject){
                                if($scope.searchObject.isSearchOnlyDiscrepancy)
                                    $("#isSearchForDiscrepancies").attr('checked','checked');

                                $("#searchIssuer").val($scope.searchObject.searchIssuerID);
                                $("#subscriberID").val($scope.searchObject.subscriberID);
                                $("#reconciliationYear").val($scope.searchObject.reconciliationYear).change();
                                $("#discrepancyTypes").val($scope.searchObject.discrepancyType).change();
                                $("#discrepancyStatus").val($scope.searchObject.discrepancyStatus).change();
                                $("#issuerAssignedPolicyID").val($scope.searchObject.issuerAssignedPolicyID);
                                $("#exchangeAssignedPolicyID").val($scope.searchObject.exchangeAssignedPolicyID);
                                $("#reconciliationMonth").val($scope.monthList[$scope.searchObject.reconciliationMonth - 1]).change();
                                $("#coverageYear").val($scope.searchObject.coverageYear).change();

                            }
                            else {
                                var currentTime = new Date();
                                var month = currentTime.getMonth() + 1;
                                var year = currentTime.getFullYear();

                                if(searchParams) {
                                    $("#reconciliationYear").val(searchParams.reconYear).change();
                                    $("#reconciliationMonth").val($scope.monthList[searchParams.reconMonth - 1]).change();
                                    $("#searchIssuer").val(searchParams.hiosIssuerId).change();
                                }
                                else {
                                    $("#reconciliationYear").val(year).change();
                                    $("#reconciliationMonth").val($scope.monthList[month -1]).change();
                                }
                            }

                            $timeout(function() {
                                $scope.searchParameters = searchParams;
                                if(searchParams === null){
                                    searchParams = dataStorageService.getItem("searchData");
                                }
                                if (searchParams) {
                                    var fileName = searchParams.fileName;
                                    $scope.searchReconData(false);
                                    dataStorageService.setItem("issuerData", $scope.searchParameters);
                                    $("#fileName").text(fileName);
                                }
                            }, 0);
                        }, 0);
                    },
                    function(result) {
                        //console.log("Failed to get Search Filters");
                    });
            };

            $scope.targetField = null;
            $scope.clearResults = function() {

                if($scope.targetField) {
                    $("#fileName").val("");
                    $("#fileNameGroup").hide();
                    clearSearchResultsService.notify();
                }
            };

            $scope.focusCallback = function($event){
                if($event.target === null) {
                    return;
                }
                $scope.targetField = $event.target;
            };

            $scope.isFileNameVisible = function () {
                return searchReconciliation.getIssuer();
            };

            $scope.resetReconSearch = function () {
                $("#issuerAssignedPolicyID").val("");
                $("#exchangeAssignedPolicyID").val("");
                $("#subscriberID").val("");
                $("#fileName").val("");
                searchReconciliation.setIssuer(null);
                var currentTime = new Date();
                var month = currentTime.getMonth() + 1;
                var year = currentTime.getFullYear();
                $("#reconciliationYear").val(year).change();
                $("#reconciliationMonth").val($scope.monthList[month -1]).change();
                $("#discrepancyStatus").val("-1").change();
                $("#searchIssuer").val("-1").change();
                $("#coverageYear").val("-1").change();
                $("#discrepancyTypes").val("-1").change();
                $("#isSearchForDiscrepancies").prop('checked', false);
                dataStorageService.setItem("ReconObj", null);
                clearSearchResultsService.notify();
            };

            $scope.searchReconData = function (isSave, paginationOptions) {
                var searchParams = searchReconciliation.getIssuer();

                if(searchParams === null || !searchParams.hiosIssuerId){
                    searchParams = dataStorageService.getItem("ReconObj");
                }

                var searchIssuerID = $("#searchIssuer :selected").val();
                var subscriberID = $("#subscriberID").val();
                var reconciliationYear = $("#reconciliationYear :selected").val();
                var discrepancyType = $("#discrepancyTypes :selected").val();
                var discrepancyStatus = $("#discrepancyStatus :selected").val();
                var issuerAssignedPolicyID = $("#issuerAssignedPolicyID").val();
                var exchangeAssignedPolicyID = $("#exchangeAssignedPolicyID").val();
                var reconciliationMonth = $scope.monthList.indexOf($("#reconciliationMonth :selected").val()) + 1;
                var coverageYear =  $("#coverageYear :selected").val();
                var isSearchOnlyDiscrepancy = $("#isSearchForDiscrepancies").is(":checked");

                
                var doNotSendFileID = false;
                doNotSendFileID = searchParams && (searchParams.hiosIssuerId && parseInt(searchParams.hiosIssuerId, 10) !== parseInt(searchIssuerID, 10) ||
                        searchParams.coverageYear && parseInt(searchParams.coverageYear, 10) !== parseInt(coverageYear, 10) ||
                        coverageYear !== "ALL" ||
                		searchParams.reconMonth && parseInt(searchParams.reconMonth,10) !== parseInt(reconciliationMonth, 10));
                if(doNotSendFileID === undefined)
                	doNotSendFileID = false;

                var requestJSON = {};

                if(searchIssuerID && searchIssuerID !== "-1") {
                    requestJSON.hiosIssuerId = searchIssuerID;
                }

                if(searchParams && searchParams.fileId && !doNotSendFileID){
                    requestJSON.fileId = searchParams.fileId;
                }
                else {
                	$("#fileNameGroup").hide();
                }
                
                if(subscriberID){
                    requestJSON.subscriberId = subscriberID;
                }

                if(issuerAssignedPolicyID) {
                    requestJSON.issuerEnrollmentId = issuerAssignedPolicyID;
                }

                if(exchangeAssignedPolicyID) {
                    requestJSON.hixEnrollmentId = exchangeAssignedPolicyID;
                }

                if(discrepancyType && discrepancyType !== "-1"){
                    requestJSON.discrepencyType = discrepancyType;
                }

                if(reconciliationMonth)
                    requestJSON.reconMonth = reconciliationMonth;

                if(reconciliationYear)
                    requestJSON.reconYear = reconciliationYear;

                if(coverageYear && coverageYear !== "ALL")
                    requestJSON.coverageYear = coverageYear;

                if(discrepancyStatus && discrepancyStatus !== "ALL")
                    requestJSON.status = discrepancyStatus;

                if(isSearchOnlyDiscrepancy)
                    requestJSON.isSearchOnlyDiscrepancy = true;

                if(paginationOptions) {
                    if (paginationOptions.pageSize)
                    	requestJSON.pageSize = paginationOptions.pageSize;

                    if (paginationOptions.currentPage)
                        requestJSON.pageNumber = paginationOptions.currentPage;

                    if (paginationOptions.sortByField)
                        requestJSON.sortByField = paginationOptions.sortByField;

                    if (paginationOptions.sortOrder)
                        requestJSON.sortOrder = paginationOptions.sortOrder;
                }else{
                	requestJSON.pageSize = 15;
                }

                searchService.getSearchResults(requestJSON).then(
                    function(results) {
                        searchNotifyingService.notify();

                    },
                    function(result) {
                        //console.log("Failed to get Search Filters");
                    });

                if(isSave) {
                    var searchObject = {
                        "searchParams": searchParams,
                        "searchIssuerID": searchIssuerID,
                        "subscriberID": subscriberID,
                        "reconciliationYear": reconciliationYear,
                        "discrepancyType": discrepancyType,
                        "discrepancyStatus": discrepancyStatus,
                        "issuerAssignedPolicyID": issuerAssignedPolicyID,
                        "exchangeAssignedPolicyID": exchangeAssignedPolicyID,
                        "reconciliationMonth": reconciliationMonth,
                        "coverageYear": coverageYear,
                        "isSearchOnlyDiscrepancy": isSearchOnlyDiscrepancy
                    };

                    dataStorageService.setItem("searchData", searchObject);
                }

            };

            searchPaginationService.subscribe($scope, function doSearch(event, paginationOptions) {
                $scope.searchReconData(false, paginationOptions);
            });

            (function () {
                $scope.searchObject = null;
                $scope.issuerData = dataStorageService.getItem("issuerData");
                var searchParams = searchReconciliation.getIssuer();

                if($scope.issuerData && $scope.issuerData.lastUpdateTime && searchParams && searchParams.lastUpdateTime && moment($scope.issuerData.lastUpdateTime).isAfter(moment(searchParams.lastUpdateTime))) {
                    searchReconciliation.setIssuer($scope.issuerData);
                }

                $scope.searchObject = dataStorageService.getItem("searchData");

                $scope.initializeSearchFilters();
            })();

        }]);
})();
