package com.getinsured.ssap.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.hix.ssap.SsapConstants;
import com.getinsured.ssap.model.hub.vlp.VlpIntegrationLog;
import com.getinsured.ssap.repository.VlpIntegrationLogRepository;

/**
 * VLP Status controller that provides status
 * about current SSAP application VLP state.
 *
 * @author Yevgen Golubenko
 * @since 3/7/19
 */
@RestController
public class VlpStatusController
{
  private static final Logger log = LoggerFactory.getLogger(VlpStatusController.class);

  private final VlpIntegrationLogRepository vlpIntegrationLogRepository;

  @Autowired
  public VlpStatusController(final VlpIntegrationLogRepository vlpIntegrationLogRepository)
  {
    this.vlpIntegrationLogRepository = vlpIntegrationLogRepository ;
  }

  @PreAuthorize(SsapConstants.SSAP_VIEW)
  @RequestMapping(value = "/newssap/application/status/{applicationId}", method = RequestMethod.GET)
  public ResponseEntity<?> status(@PathVariable Long applicationId) {
    List<VlpIntegrationLog> integrationLogList = vlpIntegrationLogRepository.findBySsapApplicationId(applicationId);
    log.info("Found {} VlpIntegrationLogs for given application id: {}", integrationLogList.size(), applicationId);
    return ResponseEntity.ok(integrationLogList);
  }

  @PreAuthorize(SsapConstants.SSAP_VIEW)
  @RequestMapping(value = "/newssap/application/status/applicant/{applicantId}", method = RequestMethod.GET)
  public ResponseEntity<?> applicantStatus(@PathVariable Long applicantId) {
    List<VlpIntegrationLog> integrationLogs = vlpIntegrationLogRepository.findBySsapApplicantId(applicantId);

    if(integrationLogs.size() > 0)
    {
      return ResponseEntity.ok(integrationLogs);
    }

    log.warn("Unable to find VlpIntegrationLogs for given applicant id: {}", applicantId);
    return ResponseEntity.notFound().build();
  }
}
