{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "dateOfBirth": "01/25/1970",
            "name": {
              "firstName": "Jonathan",
              "middleName": "C",
              "lastName": "Mault"
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "222222305"
            },
            "gender": "M",
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/25/2014",
              "insurancePolicyExpirationDate": "12/31/2014"
            },
            "organization": [
              {
                "organizationCode": "TRIC"
              }
            ]
          },
          {
            "dateOfBirth": "01/25/1972",
            "name": {
              "firstName": "Maria",
              "middleName": "C",
              "lastName": "Johnson"
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "222222306"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/25/2014",
              "insurancePolicyExpirationDate": "12/31/2014"
            },
            "organization": [
              {
                "organizationCode": "TRIC"
              }
            ]
          },
          {
            "dateOfBirth": "01/25/1970",
            "name": {
              "firstName": "Kevin",
              "middleName": "C",
              "lastName": "Fox",
              "personNameSuffixText": "Jr"
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "222222307"
            },
            "gender": "M",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/25/2014",
              "insurancePolicyExpirationDate": "12/31/2014"
            },
            "organization": [
              {
                "organizationCode": "TRIC"
              }
            ]
          }
        ]
      }
    ]
  }
}