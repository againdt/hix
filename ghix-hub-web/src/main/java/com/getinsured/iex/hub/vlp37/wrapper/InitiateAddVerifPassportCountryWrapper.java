package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.PassportCountryType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper Class for PassportCountryType
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class InitiateAddVerifPassportCountryWrapper implements JSONAware {
	
	private PassportCountryType passportCountryType;
	
	private String passportNumber;
	private String countryOfIssuance;
	
	private static final String PASSPORT_NUMBER_KEY = "PassportNumber";
	private static final String COUNTRY_KEY = "CountryOfIssuance";
	
	public InitiateAddVerifPassportCountryWrapper(){
		ObjectFactory factory = new ObjectFactory();
		this.passportCountryType = factory.createPassportCountryType();
	}
	
	public InitiateAddVerifPassportCountryWrapper(PassportCountryType passportCountry) {
		this.passportCountryType = passportCountry;
		this.passportNumber = passportCountry.getPassportNumber();
		this.countryOfIssuance = passportCountry.getCountryOfIssuance();
	}

	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.passportCountryType.setPassportNumber(passportNumber);
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.passportCountryType.setCountryOfIssuance(countryOfIssuance);
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public PassportCountryType getPassportCountryType(){
		return this.passportCountryType;
	}

	
	/**
	 * Converts the JSON String to PassportCountryWrapper object
	 * 
	 * @param jsonString - String representation for PassportCountryWrapper
	 * @return PassportCountryWrapper Object
	 */
	public static InitiateAddVerifPassportCountryWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create PassportCountryWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		InitiateAddVerifPassportCountryWrapper wrapper = new InitiateAddVerifPassportCountryWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		tmpObj = obj.get(PASSPORT_NUMBER_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create PassportCountryWrapper, No Passport number found");
		}
		wrapper.passportNumber = (String)tmpObj;
		wrapper.passportCountryType.setPassportNumber(wrapper.passportNumber);
		
		tmpObj = obj.get(COUNTRY_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create PassportCountryWrapper, No Country of Issuance found");
		}
		wrapper.countryOfIssuance = (String)tmpObj;
		wrapper.passportCountryType.setCountryOfIssuance(wrapper.countryOfIssuance);
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		return obj.toJSONString();
	}
	
	/**
	 * Converts the JSON Object to PassportCountryWrapper object
	 * 
	 * @param obj - JSON representation for PassportCountryWrapper
	 * @return PassportCountryWrapper Object
	 */
	public static InitiateAddVerifPassportCountryWrapper fromJSONObject(JSONObject obj) throws HubServiceException {
		InitiateAddVerifPassportCountryWrapper wrapper = new InitiateAddVerifPassportCountryWrapper();
		wrapper.setPassportNumber((String)obj.get(PASSPORT_NUMBER_KEY));
		wrapper.setCountryOfIssuance((String)obj.get(COUNTRY_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		return obj;
	}
}
