package com.getinsured.eligibility.at.ref.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

/**
 * @author chopra_s
 * 
 */
@Component("ssapEnrolleeHandler")
@Scope("singleton")
public class SsapEnrolleeHandler {
	
	private static final Logger LOGGER = Logger.getLogger(SsapEnrolleeHandler.class);

	private static final int LATEST_RECORD = 0;

	private final static String SUBSCRIBER = "SUBSCRIBER";

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private GIWSPayloadService giwsPayloadService;
	
	@Autowired
	@Qualifier("zipCodeService")
	private ZipCodeService zipCodeService;
	
    public EnrolleeResponse invokeEnrollmentApi(long applicationId) throws RestClientException, JsonProcessingException {
		
    	EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
    	enrolleeRequest.setSsapApplicationId(applicationId);
    	
		ObjectMapper mapper = new ObjectMapper();
		
		String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON, 
				ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, 
				mapper.writeValueAsString(enrolleeRequest)).getBody();
		
		logRequestAndResponse(applicationId, mapper.writeValueAsString(enrolleeRequest), response);
		
		if (null != response){ 
			Gson gson = new Gson();
			EnrolleeResponse enrolleeResponse = gson.fromJson(response, EnrolleeResponse.class);
			return enrolleeResponse;
		}
		return null;
   	}

    private void logRequestAndResponse(Long applicationId,String request,String response)
    {
    	GIWSPayload giwsPayload = new GIWSPayload();
        giwsPayload.setSsapApplicationId(applicationId);
        giwsPayload.setCreatedTimestamp(new TSDate());
        giwsPayload.setEndpointFunction("FIND_ENROLLEE_BY_APPLICATION_ID_JSON");
        giwsPayload.setEndpointOperationName("invokeEnrollmentApi");
        giwsPayload.setEndpointUrl(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON);
        giwsPayload.setRequestPayload(request);
        giwsPayload.setResponsePayload(response);
        giwsPayloadService.save(giwsPayload);
    }
    
	public Map<String, Object> fetchEnrollmentID(long applicationId) {
		Map<String, Object> enrollmentIdMap = new HashMap<String, Object>();
		try {
			final EnrolleeResponse enrolleeResponse = invokeEnrollmentApi(applicationId);
			if (null != enrolleeResponse && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				final List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				
				final List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
				final List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
				
				extractLatestRecord(enrollmentShopDTOs, enrollmentForHealth, enrollmentForDental);
				
				if(!enrollmentForHealth.isEmpty()){
					enrollmentIdMap.put(enrollmentForHealth.get(LATEST_RECORD).getPlanType(), "" + enrollmentForHealth.get(LATEST_RECORD).getEnrollmentId());
					enrollmentIdMap.put(ReferralConstants.HEALTH_ENROLLMENT_DTO, enrollmentForHealth.get(LATEST_RECORD));
				}
				
				if(!enrollmentForDental.isEmpty()){
					enrollmentIdMap.put(enrollmentForDental.get(LATEST_RECORD).getPlanType(), "" + enrollmentForDental.get(LATEST_RECORD).getEnrollmentId());
					enrollmentIdMap.put(ReferralConstants.DENTAL_ENROLLMENT_DTO, enrollmentForDental.get(LATEST_RECORD));
				}
				
				
				
				/*for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
					enrollmentIdMap.put(enrollmentShopDTO.getPlanType(), "" + enrollmentShopDTO.getEnrollmentId());
					if (ReferralConstants.HEALTH.equalsIgnoreCase(enrollmentShopDTO.getPlanType())) {
						enrollmentIdMap.put(ReferralConstants.HEALTH_ENROLLMENT_DTO, enrollmentShopDTO);
					}
					if (ReferralConstants.DENTAL.equalsIgnoreCase(enrollmentShopDTO.getPlanType())) {
						enrollmentIdMap.put(ReferralConstants.DENTAL_ENROLLMENT_DTO, enrollmentShopDTO);
					}
				}*/
			} else {
				if (enrolleeResponse != null) {
					throw new GIRuntimeException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
				}
			}

		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching enrollment details :", e);
		}
		return enrollmentIdMap;
	}

	
	public Map<String, List<EnrollmentShopDTO>> fetchMultipleEnrollmentID(long applicationId) {
		Map<String, List<EnrollmentShopDTO>> enrollmentIdMap = new HashMap<String, List<EnrollmentShopDTO>>();
		try {
			final EnrolleeResponse enrolleeResponse = invokeEnrollmentApi(applicationId);
			if (null != enrolleeResponse && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				final List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				
				final List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
				final List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
				
				extractLatestRecord(enrollmentShopDTOs, enrollmentForHealth, enrollmentForDental);
				
				
				if(!enrollmentForHealth.isEmpty()){
					enrollmentIdMap.put(ReferralConstants.HEALTH_ENROLLMENT_DTO, enrollmentForHealth);
				}
				
				if(!enrollmentForDental.isEmpty()){
					enrollmentIdMap.put(ReferralConstants.DENTAL_ENROLLMENT_DTO, enrollmentForDental);
				}
				
				
			} else {
				if (enrolleeResponse != null) {
					throw new GIRuntimeException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
				}
			}

		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching enrollment details :", e);
		}
		return enrollmentIdMap;
	}

	private void extractLatestRecord(final List<EnrollmentShopDTO> enrollmentShopDTOs,
			final List<EnrollmentShopDTO> enrollmentForHealth, final List<EnrollmentShopDTO> enrollmentForDental) {
		for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
			if (ReferralConstants.HEALTH.equalsIgnoreCase(enrollmentShopDto.getPlanType())) {
				enrollmentForHealth.add(enrollmentShopDto);
			} else if (ReferralConstants.DENTAL.equalsIgnoreCase(enrollmentShopDto.getPlanType())) {
				enrollmentForDental.add(enrollmentShopDto);
			}
		}
			
		if(enrollmentForHealth.size() > 1){
			Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
		
		if(enrollmentForDental.size() > 1){
			Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
	}

	public void parseListofEnrollees(CompareMainDTO compareMainDTO) {
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		try {
			final long applicationId = compareMainDTO.getEnrolledApplication().getId();
			if (applicationId > 0) {
				enrolleeResponse = invokeEnrollmentApi(applicationId);
				if (null != enrolleeResponse && null != enrolleeResponse.getStatus() && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
					enrollmentShopDTOs = enrollmentShopDTOs.stream()
							   .filter(enrollmentShopDTO -> !enrollmentShopDTO.getEnrollmentStatusValue().equalsIgnoreCase("CANCEL"))
							   .filter(enrollmentShopDTO -> checkTerminationDate(enrollmentShopDTO))
							   .collect(Collectors.toList());
					extractEnrolledData(enrollmentShopDTOs, compareMainDTO);
				} else {
					StringBuilder errorReason = new StringBuilder();
					if(null != enrolleeResponse) {
					errorReason.append(enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
					}
					throw new GIRuntimeException("Unable to get Enrollment Plan Details. Error Details: " + errorReason);
				}
			}
		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching enrollment details :", e);
		}
	}
	
	private boolean checkTerminationDate(EnrollmentShopDTO enrollmentShopDTO) {
		if (enrollmentShopDTO.getEnrollmentStatusValue().equalsIgnoreCase("TERM")) {
				return EligibilityUtils.isEqualOrAfter(new Date(), enrollmentShopDTO.getTerminationDate());
		} else {
			return true;
		}
	}

	private void extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, CompareMainDTO compareMainDTO) {
		
		final List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
		final List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
		
		extractLatestRecord(enrollmentShopDTOs, enrollmentForHealth, enrollmentForDental);
		
		if(!enrollmentForHealth.isEmpty()){
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setHealthPlanId(ReferralUtil.convertToValidLong(enrollmentForHealth.get(LATEST_RECORD).getPlanId()));
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setHealthEnrollmentId(ReferralUtil.convertToValidLong(enrollmentForHealth.get(LATEST_RECORD).getEnrollmentId()));
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setHealthPlanLevel(enrollmentForHealth.get(LATEST_RECORD).getPlanLevel());
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setHealthNetPremiumAmt(enrollmentForHealth.get(LATEST_RECORD).getNetPremiumAmt());
			
			for ( EnrollmentShopDTO enrollmentForHealthObject : enrollmentForHealth) {
				List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentForHealthObject.getEnrolleeShopDTOList();
				
				for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
					for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {
						if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
							enrolledApplicant.setEnrolled(true);
								compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthEnrollees().add(enrolledApplicant.getApplicantGuid());
							
							if (enrolleeMembers.getPersonType().equalsIgnoreCase(SUBSCRIBER)) {
								LOGGER.warn("Setting Enrolled Address data for subscriber for enrollment id - " + enrolleeMembers.getEnrollmentId());
								enrolledApplicant.setEnrolledHomeAddress(address(enrolleeMembers.getHomeAddress1(),enrolleeMembers.getHomeAddress2(),enrolleeMembers.getHomeCity(),enrolleeMembers.getHomeState(),enrolleeMembers.getHomeCounty(),enrolleeMembers.getHomeCountyCode(),enrolleeMembers.getHomeZip()));
								enrolledApplicant.setSubscriber(true);
									compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setHealthSubscriberId(enrolledApplicant.getApplicantGuid());
								}
							break;
						}
					}
				}
				
			}
			
			
		}
		
		if(!enrollmentForDental.isEmpty()){
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setDentalEnrollmentId(ReferralUtil.convertToValidLong(enrollmentForDental.get(LATEST_RECORD).getEnrollmentId()));
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setDentalPlanId(ReferralUtil.convertToValidLong(enrollmentForDental.get(LATEST_RECORD).getPlanId()));
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setDentalNetPremiumAmt(enrollmentForDental.get(LATEST_RECORD).getNetPremiumAmt());
			
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentForDental.get(LATEST_RECORD).getEnrolleeShopDTOList();
			
			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
				for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {
					if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
						enrolledApplicant.setEnrolled(true);
						compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getDentalEnrollees().add(enrolledApplicant.getApplicantGuid());
						
						if (enrolleeMembers.getPersonType().equalsIgnoreCase(SUBSCRIBER)) {
							enrolledApplicant.setEnrolledHomeAddress(address(enrolleeMembers.getHomeAddress1(),enrolleeMembers.getHomeAddress2(),enrolleeMembers.getHomeCity(),enrolleeMembers.getHomeState(),enrolleeMembers.getHomeCounty(),enrolleeMembers.getHomeCountyCode(),enrolleeMembers.getHomeZip()));
							enrolledApplicant.setSubscriber(true);
							compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setDentalSubscriberId(enrolledApplicant.getApplicantGuid());
						}
						break;
					}
				}
			}
		}
	}

	public Date calculateDisenrollmentEndDate(long applicationId) {
		Date endDate = null;
		Map<String, Object> enrollmentMap = fetchEnrollmentID(applicationId);
		if (enrollmentMap.get(ReferralConstants.HEALTH_ENROLLMENT_DTO) != null) {
			EnrollmentShopDTO enrollmentShopDTO = (EnrollmentShopDTO) enrollmentMap.get(ReferralConstants.HEALTH_ENROLLMENT_DTO);
			endDate = enrollmentShopDTO.getCoverageEndDate();
		}
		else if (enrollmentMap.get(ReferralConstants.DENTAL_ENROLLMENT_DTO) != null) {
			EnrollmentShopDTO enrollmentShopDTO = (EnrollmentShopDTO) enrollmentMap.get(ReferralConstants.DENTAL_ENROLLMENT_DTO);
			endDate = enrollmentShopDTO.getCoverageEndDate();
		}
		return endDate;
	}

	public static boolean isEnrolledForHealth(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return enrolledApplicationAttributes.getHealthEnrollmentId() != ReferralConstants.NONE;
	}
	
	
	public void invokeSharedPlanMgnt(CompareMainDTO compareMainDTO) {

		long healthPlanId = compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthPlanId();
		
		PlanRequest planrequest = new PlanRequest();
		planrequest.setPlanId(String.valueOf(healthPlanId));
		planrequest.setMinimizePlanData(true);
		try {
			ResponseEntity<String> xmlPlanResponse = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DETAILS_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, planrequest);
			String costSharing = checkPlanResponse(xmlPlanResponse);
			if (StringUtils.isEmpty(costSharing)){
				throw new GIRuntimeException("Error invoking Plan Management Cost Sharing details for plan for application id - " + compareMainDTO.getEnrolledApplication().getCaseNumber());
			}
			compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(costSharing);
		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching Plan Management Cost Sharing details for plan : ", e);
		}

	}

	private String checkPlanResponse(ResponseEntity<String> xmlPlanResponse) {
	    if (xmlPlanResponse != null && xmlPlanResponse.getBody() != null){
    		Gson gson = new Gson();
    		PlanResponse planresponse = gson.fromJson(xmlPlanResponse.getBody(), PlanResponse.class);
    		if (null != planresponse && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(planresponse.getStatus())) {
    			return planresponse.getCostSharing();
    		} 
	    }
		return null;
    }
	
	private Address address(String homeAddress1, String homeAddress2, String homeCity, String homeState, String homeCounty, String homeCountyCode, String homeZip) {
		LOGGER.warn("Address details got from enrollment : Address 1 " + homeAddress1+" Address 2 "+homeAddress2
	 	 	 	      +" City "+homeCity+" State "+homeState+" County "+homeCounty+" CountyCode "+homeCountyCode+" Zip "+homeZip);
		Address address = new Address();

		if (homeCity != null) {
			address.setCity(homeCity);
		}
		if (homeZip != null) {
			address.setPostalCode(homeZip);
		}
		if (homeState != null) {
			address.setState(homeState);
		}
		if (homeAddress1 != null) {
			address.setStreetAddress1(homeAddress1);
		}
		if (homeAddress2 != null) {
			address.setStreetAddress2(homeAddress2);
		}
		if (homeCounty != null) {
			address.setCounty(homeCounty);
		}
		if (homeCountyCode != null) {
			address.setPrimaryAddressCountyFipsCode(homeCountyCode);
		}
		populateCountyName(address);
		 LOGGER.warn("Address Object created : Address 1 " + address.getStreetAddress1()+" Address 2 "+address.getStreetAddress2()
	 	 	      	+" City "+address.getCity()+" State "+address.getState()+" County "+address.getCounty()
	 	 	        +" CountyCode "+address.getPrimaryAddressCountyFipsCode()+" Zip "+address.getPostalCode());
		return address;
	}
	
	private void populateCountyName(Address address) {

		if (address != null) {
			String countyCode = address.getPrimaryAddressCountyFipsCode();
			if(countyCode.length()==5) {
				countyCode=countyCode.substring(2);
			}

			String zip = address.getPostalCode();
			String state = address.getState();
			String county = address.getCounty();

			if (ReferralUtil.isNotNullAndEmpty(countyCode) && ReferralUtil.isNotNullAndEmpty(zip) && ReferralUtil.isNotNullAndEmpty(state)) {
				if (county == null) {
					String countyName = zipCodeService.findCountyNameByZipStateAndCountyCD(zip, state, countyCode);
					address.setCounty(countyName);
				}
			}
		}

	}
}
