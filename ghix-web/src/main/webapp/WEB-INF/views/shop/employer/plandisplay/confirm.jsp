<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"
	prefix="util"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />
<style>
 #doneButton {
 	margin: -6px;
 }
</style>

<div class="gutter10">
<div class="row-fluid">
	<div class="gutter10">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
		<h4 class="auto-height-header header">
			<span class="pull-left"><spring:message code="label.congratulations"/></span> 
			<c:if test="${respStatus == 'SUCCESS'}">
	            <a href="#" class="btn btn-primary getSelectedTier pull-right" onclick="fnFinish();"> <spring:message code = "label.finish"/> </a>
            </c:if>
            <c:if test="${stateCode !='CA' && respStatus == 'NA'}">
	              <input type="button" name="doneButton" id="doneButton" onClick="javascript:window.location.href ='<c:url value="/shop/employer/dashboard" />'" class="btn btn-primary confirm_btn pull-right" value="<spring:message  code='label.donebtn'/>" />
	        </c:if>
        </h4>
		
			<!--titlebar-->
				<div class="gutter10">
					<c:if test="${respStatus == 'FAILURE'}">
						<p>
							<label class='error' generated='true'><span><em class='excl'>!</em>${errorCode} - ${errorMsg}	</label>
						</p>
					</c:if>
					
					<p><spring:message code = "label.completionLine1"/> 
                    <c:if test="${stateCode !='CA'}"><spring:message code = "label.completionLine2"/> ${openEnrollmentStart}. <spring:message code = "label.completionLine3"/> ${init_openenrollment_period}-<spring:message code = "label.completionLine5"/> ${openEnrollmentEnd}, 
					 <spring:message code = "label.completionLine4"/></c:if>
					<c:if test="${stateCode =='CA'}">
						<br/><b><spring:message code="label.covEffectiveDt"/> : ${coverageDate}</b>
					</c:if>
					</p>
				</div>
				<!--gutter10-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
</div>
<!--main-->

<script type="text/javascript">
function gotoparent(link){
	window.top.location.href = link;
}

function fnFinish(){
	 var  pathURL='<c:url value="/shop/employer/plandisplay/redirectFlow" />';

	$.get(pathURL, function(data) {
		window.top.location.href = data;
		//location.href = data;
	});
}

</script>