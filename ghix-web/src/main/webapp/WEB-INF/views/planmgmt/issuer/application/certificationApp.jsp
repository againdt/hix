<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
		
		<div class="row-fluid">
	
			<div class="span3" id="sidebar">
				
					<!--  beginning of side bar -->
					<ul class="nav nav-list">
					  	<li class="nav-header"><spring:message code="label.issuerCertification"/></li>
					  	<li><a href="#"><spring:message code="label.authorizeRepresentatives"/></a></li>			  	
						<li class="active"><a href="#"><spring:message code="label.submitIssuerApplication"/></a></li>	
						<li><a href="#"><spring:message code="label.reviewAndConfirm"/></a></li>							
					  </ul>
					<!-- end of side bar -->
				
			</div><!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
				
					<div class="profile">
						<div class="page-header">
							<h1><spring:message  code="label.issuerHeader"/></h1>
							<br />
							<p><spring:message  code="label.issuerUploadLabel"/></p>
						</div><!-- end of page-header -->

								<table class="table">
									<thead>
										<tr class="header">
											<td>1.</td>
											<td>
												<div class="control-group">
													<label for="firstName" class="required control-label" for="fileInput"><spring:message  code="label.issuerUploadAuthority"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
												        <div class="controls">
												        <form class="form-stacked" id="frmissuerAuthority" name="frmissuerAuthority" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
															<df:csrfToken/>
															<input type="file" id="authority" name="authority" class="input-file">
															&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
															<input type="hidden" name="fileType" value="authority"/>
															<div id="authorityFileName" class="help-inline"></div>
															<div id="authorityFiles" class="help"></div>
															<div id="authorityFile_error" class=""></div>
														</form>
														</div>	<!-- end of controls-->
												</div>	<!-- end of control-group -->
											</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>2.</td>
											<td>
												<div class="control-group">
													<label for="firstName" class="required control-label" for="fileInput"><spring:message  code="label.issuerUploadMarketingReq"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
												        <div class="controls">
												        <form class="form-stacked" id="frmissuerMarketingReq" name="frmissuerMarketingReq" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
															<df:csrfToken/>
															<input type="file" id="marketingReq" name="marketingReq" class="input-file">
															&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
															<input type="hidden" name="fileType" value="marketingReq"/>
															<div id="marketingReqFileName" class="help-inline"></div>
															<div id="marketingReqFiles" class="help"></div>
															<div id="marketingFile_error" class=""></div>
														</form>
														</div>	<!-- end of controls-->
												</div>	<!-- end of control-group -->
											</td>
										</tr>
										<tr>
											<td>3.</td>
											<td>
												<div class="control-group">
													<label for="firstName" class="required control-label" for="fileInput"><spring:message  code="label.issuerUploadDisclosures"/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
												        <div class="controls">
												        <form class="form-stacked" id="frmissuerDisclosures" name="frmissuerDisclosures" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
															<df:csrfToken/>
															<input type="file" id="disclosures" name="disclosures" class="input-file">
															&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
															<input type="hidden" name="fileType" value="disclosures"/>
															<div id="disclosuresFileName" class="help-inline"></div>
															<div id="disclosuresFiles" class="help"></div>
															<div id="disclosuresFile_error" class=""></div>
														</form>
														</div>	<!-- end of controls-->
												</div>	<!-- end of control-group -->
											</td>
										</tr>
										<tr>
											<td>4.</td>
											<td>																
												<div class="control-group">
													<label for="firstName" class="required control-label"><spring:message  code="label.issuerUploadAccreditation"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
												        <div class="controls">
												        <form class="form-stacked" id="frmissuerAccreditation" name="frmissuerAccreditation" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
															<df:csrfToken/>
															<input type="file" id="accreditation" name="accreditation"  class="input-file" for="fileInput">
															&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
															<input type="hidden" name="fileType" value="accreditation"/>
															<div id="accreditationFileName" class="help-inline"></div>
															<div id="accreditationFiles" class="help"></div>
															<div id="accreditationFile_error" class=""></div>
														</form>
														</div>	<!-- end of controls-->
												</div>	<!-- end of control-group -->	
											</td>
										</tr>
										<tr>
											<td>5.</td>
											<td>							
				                                <div class="control-group">
				                                    <label for="firstName" class="required control-label"><spring:message  code="label.issuerUploadAdditionalInfo"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				                                        <div class="controls">
				                                        <form class="form-stacked" id="frmissuerAddInfo" name="frmissuerAddInfo" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
				                                            <df:csrfToken/>
				                                            <input type="file" id="additionalInfo" name="additionalInfo"  class="input-file" for="fileInput">
				                                            &nbsp; <button class="btn"><spring:message code="label.upload"/></button>
				                                            <input type="hidden" name="fileType" value="additionalInfo"/>
				                                            <div id="additionalInfo" class="help-inline"></div>
															<div id="additionalInfoFiles" class="help"></div>
				                                            <div id="additionalInfo_error" class=""></div>
				                                        </form>
				                                        </div>  <!-- end of controls-->
				                                </div>  <!-- end of control-group -->   
											</td>
										</tr>
	                                </tbody>
                                </table>	
	                                
							</div><!-- end of profile -->
						 <form class="form-vertical gutter10" id="frmbrokerreg" name="frmbrokerreg" action="<c:url value="/planmgmt/issuerapplication" />" method="POST"> 
							<div class="controls">
								<df:csrfToken/>
										<input type="hidden" id="authorityFile" name="authorityFile" value="${authorityFile}"/>
										<input type="hidden" id="marketingFile" name="marketingFile" value="${marketingFile}"/>
										<input type="hidden" id="disclosuresFile" name="disclosuresFile" value="${disclosuresFile}"/>
										<input type="hidden" id="accreditationFile" name="accreditationFile" value="${accreditationFile}"/>
										<input type="hidden" id="addInfoFile" name="addInfoFile" value="${addInfoFile}"/>
									</div>				
						 </form>
						 <div class="form-actions">
								<input type="button" name="submit" id="submit" value="<spring:message  code='label.issuerSubmit'/>" class="btn btn-primary" onclick="formSubmit()" />
							</div><!-- end of form-actions -->
							          					
			</div><!-- end of span9 -->
			<!-- broker registration -->
	
	</div> <!--  end of row-fluid -->
<script type="text/javascript">

$(document).ready(function(){
	var authorityUpload = { 
	        //target:        '#rateFileName',   // target element(s) to be updated with server response 
	        beforeSubmit:  showAuthorityRequest,  // pre-submit callback 
	        success:       showAuthorityResponse  // post-submit callback 
	    }; 
	var marketingUpload = { 
	        //target:        '#benefitFileName',   // target element(s) to be updated with server response 
	        beforeSubmit:  showMarketingRequest,  // pre-submit callback 
	        success:       showMarketingResponse  // post-submit callback 
	    }; 
	 var disclosuresUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  showDisclosuresRequest,  // pre-submit callback 
	        success:       showDisclosuresResponse  // post-submit callback 
	    }; 
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };   
	var addInfoUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAddInfoRequest,  // pre-submit callback 
	        success:       submitAddInfoResponse  // post-submit callback 
	    };  
	 
	    // bind form using 'ajaxForm' 
	    $('#frmissuerAuthority').ajaxForm(authorityUpload); 
	    $('#frmissuerMarketingReq').ajaxForm(marketingUpload); 
	    $('#frmissuerDisclosures').ajaxForm(disclosuresUpload);
	    $('#frmissuerAccreditation').ajaxForm(accreditationUpload);
	    $('#frmissuerAddInfo').ajaxForm(addInfoUpload);
	    
	   // $("#submit").attr("disabled", "disabled");
	    updateFileLists();
});

var validator = $("#frmbrokerreg").validate({ 
	 ignore: "",
	rules : {authorityFile : { required : true},
		marketingFile : { required : true},
		disclosuresFile : { required : true},
		accreditationFile : { required : true}
	}, 
	messages : {
		authorityFile : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateUploadAuthority' javaScriptEscape='true'/></span>"},
		marketingFile : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateUploadMarketingReq' javaScriptEscape='true'/></span>"},
		disclosuresFile : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateUploadDisclosures' javaScriptEscape='true'/></span>"},
		accreditationFile : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateUploadAccreditation' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

function  updateFileLists()
{
	var fileNames = $('#authorityFile').val();
	updateList('#authorityFiles', fileNames, '#authorityFile');
	
	fileNames = $('#marketingFile').val();
	updateList('#marketingReqFiles', fileNames, '#marketingFile');

	fileNames = $('#disclosuresFile').val();
	updateList('#disclosuresFiles', fileNames, '#disclosuresFile');

	fileNames = $('#accreditationFile').val();
	updateList('#accreditationFiles', fileNames, '#accreditationFile');

	fileNames = $('#addInfoFile').val();
	updateList('#additionalInfoFiles', fileNames, '#addInfoFile');
}

function updateList(parentId, fileNames, targetId)
{
	var val = fileNames.split(",");
	$.each(val, function(index, actualFilename) {
		  var fileName = getFileName(val[index]);
		  addFiles(parentId, fileName, actualFilename, targetId);
		});
}

function  getFileName(fileName)
{
	var startIndex = fileName.indexOf('_');
	var len = fileName.lastIndexOf('_') ;
	var mEs = fileName.substring(startIndex+1,len);
	var ext = fileName.substring(fileName.indexOf('.',len));
	
	return mEs+ext;
}

function  updatedSubmit()
{
	if($("#optionsCheckbox").is(":checked"))
	{
		$("#submit").removeAttr("disabled");
	}
	else
	{
		$("#submit").attr("disabled", "disabled");
	}
}
function  formSubmit(){	
	$("#authorityFile_error").html("");
	$("#marketingFile_error").html("");
	$("#disclosuresFile_error").html("");
	$("#accreditationFile_error").html("");
	$("#additionalInfo_error").html("");		
	$("#frmbrokerreg").submit();
	
}
</script>

<script type="text/javascript">

function showAuthorityRequest(formData, jqForm, options) {
	var fileName = $('#authority').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
} 
 
// post-submit callback 
function showAuthorityResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	//$("#authorityFileName").text(val[1]);
	//$("#authorityFile").val(val[2]);
	addNewFiles('#authorityFiles', val[1],val[2],'#authorityFile');
	$("#authorityFile_error").html("");
} 

function showMarketingResponse(responseText, statusText, xhr, $form)  { 
	var val = responseText.split(",");
	//$("#marketingReqFileName").text(val[1]);
	//$("#marketingFile").val(val[2]);
	addNewFiles('#marketingReqFiles', val[1],val[2],'#marketingFile');
	$("#marketingFile_error").html("");
}

function showMarketingRequest(formData, jqForm, options) { 
	var fileName = $('#marketingReq').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function showDisclosuresRequest(formData, jqForm, options) { 
	var fileName = $('#disclosures').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function showDisclosuresResponse(responseText, statusText, xhr, $form)  { 
	var val = responseText.split(",");
	//$("#disclosuresFileName").text(val[1]);
	//$("#disclosuresFile").val(val[2]);
	addNewFiles('#disclosuresFiles', val[1],val[2],'#disclosuresFile');
	$("#disclosuresFile_error").html("");
}

function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	
	var val = responseText.split(",");
	//$("#accreditationFileName").text(val[1]);
	//$("#accreditationFile").val(val[2]);
	addNewFiles('#accreditationFiles', val[1],val[2],'#accreditationFile');
	$("#accreditationFile_error").html("");
}

function submitAccreditationRequest(formData, jqForm, options) { 
	var fileName = $('#accreditation').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function submitAddInfoResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	//$("#accreditationFileName").text(val[1]);
	//$("#accreditationFile").val(val[2]);
	addNewFiles('#additionalInfoFiles', val[1],val[2],'#addInfoFile');
}

function submitAddInfoRequest(formData, jqForm, options) { 
	var fileName = $('#additionalInfo').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}


function addFiles(parentDiv, fileName, actualFilename, targetDivName)
{
	if (fileName == null || fileName.length == 0)
		return;
	var totalDivs = $(parentDiv+" > div").length ;
	var eleId = parentDiv.replace("#", "")+totalDivs;
	var newDivText = "<div id="+eleId+">" + fileName + " <a onclick=\"deleteFile('"+eleId+ "','"+actualFilename+"','"+targetDivName+"')\">Remove File</a></div>";
	$(parentDiv).append(newDivText);
}

function addNewFiles(parentDiv, fileName, actualFilename, targetDivName)
{
	addFiles(parentDiv, fileName, actualFilename, targetDivName);
	var val =$(targetDivName).val();
	if (val != null && val.length > 0)
		val+= ",";
	$(targetDivName).val(val+actualFilename);
}

//--- below function is require to perform Sting.startsWith call which is used in deleteFile().
if (typeof String.prototype.startsWith != 'function') {
	  // see below for better implementation!
	  String.prototype.startsWith = function (str){
	    return this.indexOf(str) == 0;
	  };
	}


function deleteFile(eleId, actualFilename, targetDivName)
{
	var type = '';
	if(eleId.startsWith('authorityFiles')) {
        type='Authority';
    } else if(eleId.startsWith('marketingReqFiles')) {
    	type='Marketing';
    }
    else if(eleId.startsWith('disclosuresFiles')) {
    	type='Disclosures';
    }
    else if(eleId.startsWith('accreditationFiles')) {
    	type='Accreditation';
    }
    else if(eleId.startsWith('additionalInfoFiles')) {
    	type='Additional_info';
    }

    // delete from UI
	var value = $(targetDivName).val();
	var array = value.split(',');
	var str = '';
	var existingValue = '';
	var i;
	for (i=0; i<array.length; i++) { 
		existingValue = array[i];
		if(existingValue != actualFilename)
		{
			str += existingValue+',';
		}
	  }  
	str = str.substring(0, str.length-1);
	//value = value.replace(actualFilename, "");
	$(targetDivName).val(str);
	$("#"+eleId).remove();
	
	//delete from server
	$.ajax({
		  type: 'POST',
		  url: "updateFiles",
		  data: { name: actualFilename, type: type }
		});
}
</script>
