package com.getinsured.eligibility.ssap.integration.household.service;

import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value="verificationStatusServiceActivator")
public class VerificationStatusServiceActivator {

	@SuppressWarnings("rawtypes")
	@ServiceActivator
	public Message receive(Message<String> message) {
		int currentAccountTransferRetryCount = 0;
		Object currentATRetryCountHeaderObj = message.getHeaders().get(SSAPIntegrationConstants.ACCOUNT_TRANSFER_RETRY_COUNT);
		if(null != currentATRetryCountHeaderObj) {
			currentAccountTransferRetryCount = Integer.valueOf(currentATRetryCountHeaderObj.toString());
			currentAccountTransferRetryCount++;
		} 
		/*
		 * The following logic of creating a new message every time had to be done because the ACCOUNT_TRANSFER_RETRY_COUNT header was getting lost in the loop every time after the JPA call is made. 
		 */
		Message newMessage = MessageBuilder.withPayload(message.getPayload()).copyHeaders(message.getHeaders()).setHeader(SSAPIntegrationConstants.ACCOUNT_TRANSFER_RETRY_COUNT, currentAccountTransferRetryCount).build();
		return newMessage;
	}
}
