# cypress

## System Requirements

* macOS 10.9 and above (64-bit only)
* Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64-bit only)
* Windows 7 and above

## Software Requirements

* Node.js with npm
* IDE (Intellij, WebStorm, Visual Studio, etc.)

### Install Cypress via npm:

    $ cd /your/project/path
    
    $ npm install cypress --save-dev

NOTE: for other ways to install refer to Cypress documentation

## Running Tests

### Local with Desktop runner
     
     $ npx cypress open
     
### Local CL run 
    
    $ npx cypress run --browser chrome --spec "cypress/integration/enrollment/medical_and_dental.js"
     
for more options refer to cypress documenation:
    https://docs.cypress.io/guides/guides/command-line.html#How-to-run-commands 
    
    
## Environment configuration

Environment configuration setup is in config directory. Create json file for each environment.
Command to run in a specific environment:
   
       cypress open --env configFile=mn5qa
       
If environment variable is not provided *mnauto* is default. This can be changed in C:\Users\galperina_y\code\iex\qa-automation\cypress-e2e\cypress\plugins\index.js

## Creating Scenarios
[Creating Scenarios](doc/CreatingScenarios.md)

## Project Execution Instructions
[ProjectExecutionInstructions](doc/ProjectExecutionInstructions.md)

## Project Installation Instructions
[ProjectInstallation](doc/ProjectInstallation.md)

## Automated Scenarios
Test Case reference:
https://docs.google.com/spreadsheets/d/1M1Jpn2oWegsxb84cUHZmVEUBifpJQ1mMN_HoZKAo5Yo/edit#gid=0

## Questions?

Cypress documentation: https://docs.cypress.io/api/api/table-of-contents.html

Cypress chatroom on gitter: https://gitter.im/cypress-io/cypress