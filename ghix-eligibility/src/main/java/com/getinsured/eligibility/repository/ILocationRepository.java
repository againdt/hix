package com.getinsured.eligibility.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Location;

@Repository
@Qualifier("iLocationRepository")
public interface ILocationRepository extends JpaRepository<Location, Integer> {

}
