package com.getinsured.eligibility.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO;

/**
 * @author chopra_s
 * 
 */
@Qualifier("referralActivationRepository")
public interface ReferralActivationRepository extends JpaRepository<ReferralActivation, Long> {

	@Query(" from ReferralActivation ra where ra.ssapApplicationId = :ssapApplicationId)")
	ReferralActivation findBySsapApplication(@Param("ssapApplicationId") int ssapApplicationId);

	@Query(" from ReferralActivation ra where ra.userId = :userId and ra.ssapApplicationId=:ssapApplicationId)")
	ReferralActivation findByUserAndSsapApplication(@Param("userId") int userId, @Param("ssapApplicationId") int ssapApplicationId);

	@Query(" from ReferralActivation ra where ra.cmrHouseholdId = :cmrHouseholdId and ra.ssapApplicationId = :ssapApplicationId)")
	ReferralActivation findByCmrHouseholdAndSsapApplication(@Param("cmrHouseholdId") int cmrHouseholdId, @Param("ssapApplicationId") int ssapApplicationId);

	@Query(" from ReferralActivation ra where ra.workflowStatus = :workFlowStatus  and FUNC('TRUNC', ra.createdOn) = FUNC('TRUNC', :createdOn)")
	List<ReferralActivation> findAllPendingReferral(@Param("workFlowStatus") ReferralActivationEnum workFlowStatus, @Param("createdOn") Date createdOn);

	@Query("select new com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO(ra.id,ra.workflowStatus,sa.id,sa.caseNumber) from ReferralActivation ra, SsapApplication sa where ra.cmrHouseholdId = :cmrHouseholdId and ra.ssapApplicationId = sa.id and ra.workflowStatus not in (:statusList))")
	List<PendingReferralDTO> findPendingReferrals(@Param("cmrHouseholdId") int cmrHouseholdId, @Param("statusList") List<ReferralActivationEnum> statusList);

	@Query("select ra.userId from ReferralActivation ra where ra.ssapApplicationId=:ssapApplicationId)")
	long getUserIdFromSsapApplication( @Param("ssapApplicationId") int ssapApplicationId);

	@Query("select ra.cmrHouseholdId from ReferralActivation ra where ra.ssapApplicationId=:ssapApplicationId)")
	long getCmrHouseHoldIdFromSsapApplication( @Param("ssapApplicationId") int ssapApplicationId);

	@Query(" from ReferralActivation ra where ra.workflowStatus = :workFlowStatus  and ra.createdOn >= :startDate and ra.createdOn < :endDate ")
	List<ReferralActivation> findByWorkflowStatusAndCreatedOnBetween(@Param("workFlowStatus") ReferralActivationEnum workFlowStatus, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(" from ReferralActivation ra where ra.workflowStatus = :workFlowStatus ")
	List<ReferralActivation> findByWorkflowStatus(@Param("workFlowStatus") ReferralActivationEnum workFlowStatus);


	@Modifying
	@Transactional
	@Query("update ReferralActivation set  cmrHouseholdId = :cmrHouseholdId, workflowStatus = :workFlowStatus ,lastUpdatedOn = :lastUpdatedOn where id = :id ")
	int updateWorkFlowStatusAndHouseholdId(@Param("cmrHouseholdId") int cmrHouseholdId, @Param("workFlowStatus") ReferralActivationEnum workFlowStatus, @Param("lastUpdatedOn") Date lastUpdatedOn, @Param("id") long id);
}
