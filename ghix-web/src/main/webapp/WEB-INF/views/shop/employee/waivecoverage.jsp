<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="gutter10">
  <div class="row-fluid">
      <div class="row-fluid">
        <div class="span9 gutter10">
          <h1>Waive Employer Coverage</h1>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span3" id="sidebar">
          <h4 class="graydrkbg">You Should Know</h4>
          <div class="gutter10">
            <p>By waiving employer coverage you are forfeiting any employer premium contributions. 
              Waiving your employer provided coverage means you will be unable to enroll in employer 
              coverage until next open enrollment unless you experience a qualified event. You should 
              only waive coverage if you have coverage elsewhere. </p>
          </div>
        </div>
        <!-- /.sidebar-->
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
          
            <h4 class="graydrkbg gutter10">Reason For Waiving Employer Coverage</h4>
            <div class="gutter10">
              <p>Please select the reason for waiving employer sponsored coverage. Once you waive coverage you 
                will be unable to enroll until next open enrollment unless you experience a qualified event.</p>
              <form class="form-horizontal gutter10" name="frmwaivecoverage" id="frmwaivecoverage">
              <df:csrfToken/>
                <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="Covered by another employer's plan">
                    I am covered by another employer's plan</label>
                </div>
                <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="Covered by Individual Health Exchange plan">
                    I am covered by an Individual Health Exchange plan</label>
                </div>
                <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="Covered by government plan (e.g. Medicare)">
                    I am covered by a government plan (ie Medicare, Medicaide etc.) </label>
                </div>
                <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="Covered as dependent on someone else's plan">
                    I am covered as a dependent under someone else's plan </label>
                </div>
                 <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="Not covered by anopther plan and do not wish to enroll">
                    I am not covered by another plan and I choose not to have health insurance </label>
                </div>
                <div class="control-group">
                  <label class="radio">
                    <input type="radio" id="waive_reason" name="waive_reason" value="other">
                    Other </label>
                  <textarea name="waive_reason_other" id="waive_reason_other" rows="3" maxlength="255"></textarea>
                </div>
                <div class="control-group">
                <label class="checkbox"> <input type="checkbox" id="emp_attest"> I attest that I wish to waive my employer provided coverage. </label>
                
   			 </div>
             <div class="control-group">
            <p class="offset4">Employee Name: ${employee.name}</p>
            <label class="control-label" for="inputEmail">Employee's  E-Signature <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	    <div class="controls">
    	  <input type="text" id="employeeSignature" name="employeeSignature" class="input-xlarge"> <span id="employeeSignature_error"></span>
    		</div>
    	<small class="offset3">Type your full name here as your electronic signature.</small>
   			</div>
   			<fmt:formatDate pattern="MM/dd/yyyy" value="${currentDate}" var="today" />
            <p class="span2">Today's Date </p><p class="span3 date">${today}</p>
              <input type="hidden" id="employeeId" name="employeeId" value="${employee.id}" >
              <div class="form-actions margin-top50"> <a class="btn" onclick="javascript:window.history.go(-1)"> &lt; Back </a> 
               <a class="btn btn-primary pull-right" onclick="validateDetails()"> Submit </a> </div>
              </form>
            </div>
            <!--gutter10--> 
            
          </div>
          <!--row-fluid--> 
        </div>
        <!--rightpanel--> 
      </div>
      <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->
<script type="text/javascript">
var waiveOption = "";

function validateDetails(){
	waiveOption = "";
	$(':radio[name="waive_reason"]').each(function() {
		if(this.checked)	 
			waiveOption = this.value;
	});
	
	if(waiveOption !=""){
		var isChecked = $('#emp_attest').attr('checked')?true:false;
		if(isChecked == true){
			if( $("#frmwaivecoverage").validate().form() ) {
				updateWaiveCoverage($("#employeeId").val());
			}
		}else{
			alert("Please select attest.");
			return false;
		}
	}else{
		alert("Please select waive option.");	
		return false;
	}
	return false;
}

function updateWaiveCoverage(elem){
	
	if(waiveOption !=""){
		if(waiveOption == 'other'){
			waiveOption = $('#waive_reason_other').val() + "."; }
		window.location.href ='updatewaivecoverage/'+elem + '&SN='+waiveOption + '&ES=' + $("#employeeSignature").val()+".";
	}
}



$(document).ready( function () {
	$('.info').tooltip();	 
	maxLength = $("textarea#waive_reason_other").attr("maxlength");
        $("textarea#waive_reason_other").after("<div><span id='remainingLengthTempId'>" 
                  + maxLength + "</span> remaining</div>");
 
        $("textarea#waive_reason_other").bind("keyup change", function(){checkMaxLength(this.id,  maxLength); } ) 
    });
 
    function checkMaxLength(textareaID, maxLength){
 
        currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 
		if (currentLengthInTextarea > (maxLength)) { 
 
			// Trim the field current length over the maxlength.
			$("textarea#waive_reason_other").val($("textarea#waive_reason_other").val().slice(0, maxLength));
			$(remainingLengthTempId).text(0);
 
		}
    }
    
    var validator2 = $('#frmwaivecoverage').validate({ 
		onkeyup: false,
		onclick: true,
		onSubmit: true,
		onfocusout: false,
		rules : {
			employeeSignature : { required : true}
		},
		messages : {
			employeeSignature : { required : "<span> <em class='excl'>!</em>Please enter your electronic signature.</span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	}); 
   
</script>