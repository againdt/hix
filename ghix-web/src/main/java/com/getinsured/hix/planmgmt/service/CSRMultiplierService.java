package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.getinsured.hix.model.PlanCsrMultiplier;

public interface CSRMultiplierService {

	List<PlanCsrMultiplier> getCSRMultiplierList();

	void updateCSRMultiplier(String csrMultiplier);

	List<Map<String, Object>> getCSRMultiplierHistory(HttpServletRequest request, Model model);

	List<Map<String, Object>> getCSRMultiplierHistory();

}
