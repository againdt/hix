(function (module) {
    'use strict';

    module.controller('InfowindowCtrl', ['$scope', 'Doctors', function ($scope, Doctors) {
        var vm = this;

        vm.getName = getName;
        vm.getSpecialty = getSpecialty;
        vm.getStreet = getStreet;
        vm.getCityStateZip = getCityStateZip;

        ////////////

        function getName () {
            var doctor = Doctors.mouseoverDoctor();
            if (angular.isDefined(doctor.getName)) {
                return doctor.getName();
            }
        }

        function getSpecialty () {
            var doctor = Doctors.mouseoverDoctor();
            if (angular.isDefined(doctor.getSpecialty)) {
                return doctor.getSpecialty();
            }
        }

        function getStreet () {
            var doctor = Doctors.mouseoverDoctor();
            if (angular.isDefined(doctor.getStreet)) {
                return doctor.getStreet();
            }
        }

        function getCityStateZip () {
            var doctor = Doctors.mouseoverDoctor();
            if (angular.isDefined(doctor.getCityStateZip)) {
                return doctor.getCityStateZip();
            }

        }
    }]);

    module.factory('InfowindowModel', [function () {
        return {
            getHtmlContent: getHtmlContent
        };

        //////////

        function getHtmlContent () {}
    }])

    module.directive('infowindow', ['InfowindowModel', function (InfowindowModel) {
        return {
            restrict: 'E',
            controller: 'InfowindowCtrl',
            controllerAs: 'vm',
            templateUrl: 'infowindow.html',
            link: link
        };

        //////////////

        function link (scope, elem, attrs, ctrl) {
            InfowindowModel.getHtmlContent = (function (elem) {
                return function () {
                    return elem.html();
                }
            })(elem);
        }
    }]);

}(angular.module('ProviderMap.Directive.infowindow', [
  'ProviderMap.Factory.doctors'
])));