(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .factory('searchNotifyingService', searchNotifyingService);

    searchNotifyingService.$inject = ['$rootScope'];

    function searchNotifyingService ($rootScope){
        return {
            subscribe: function(scope, callback) {
                var handler = $rootScope.$on('searchResultsFound', callback);
                scope.$on('$destroy', handler);
            },

            notify: function() {
                $rootScope.$emit('searchResultsFound');
            }
        };
    }
})();