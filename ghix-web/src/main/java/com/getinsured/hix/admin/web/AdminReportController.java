package com.getinsured.hix.admin.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.MarketType;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.repository.RatingAreaRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
//import com.getinsured.hix.util.GhixConfiguration;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class AdminReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminReportController.class);
	@Autowired private IssuerService issuerService;
	@Autowired private RatingAreaRepository ratingAreaRepository;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IPlanHealthRepository iPlanHealthRepository;
	@Autowired private IPlanDentalRepository iPlanDentalRepository;
	@Autowired private PlanMgmtService planMgmtService;

	private final ProcessDataForReport dataForReport = new ProcessDataForReport();
	private Integer timeSpanForGraph = 0;
	private final String splitStringForReport = ",";
	
	@RequestMapping(value = "/admin/reports/qhpenrollmentreport/graph", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.VIEW_ENROLLMENT_REPORT)
	public String qhpEnrollmentReportGraph(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		String planLevelStringForQHPGraph = null;
		Issuer requiredIssuerForQHPGraph = null;
		List<EnrollmentPlanResponseDTO> act_enrollListForQHPGraph = new ArrayList<EnrollmentPlanResponseDTO>();
		;
		List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;
		List<String> goldListForQHPGraph = new ArrayList<String>();
		List<String> plantiumListForQHPGraph = new ArrayList<String>();
		List<String> silverListForQHPGraph = new ArrayList<String>();
		List<String> bronzeListForQHPGraph = new ArrayList<String>();
		List<String> expandedBronzeListForQHPGraph = new ArrayList<String>();
		List<String> catastrophicListForQHPGraph = new ArrayList<String>();
		Integer ratingRegionAreaForQHPGraph = 0;
		List<Issuer> issuersList = issuerService.findAllIssuerForReport();
		Collections.sort(issuersList, new ComparatorClass());
		String marketTypeConcatStringForQHPGraph = null;
		try{
		if (request.getQueryString() != null) {
			if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
				request.getSession().removeAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.PLATINUM_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.GOLD_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.SILVER_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.BRONZE_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);
			}
		}

		String issuerIdForQHP = null;
		String marketForQHP = null;
		String rNameForQHP = null;
		String periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
		String platinumCheckForQHPGraph = null;
		String goldCheckForQHPGraph = null;
		String silverCheckForQHPGraph = null;
		String bronzeCheckForQHPGraph = null;
		String expandedBronzeCheckForQHPGraph = null;
		String catastrophicCheckForQHPGraph = null;

		if (request.getParameter(PlanMgmtConstants.ISSUER_ID) != null) {
			issuerIdForQHP = request.getParameter(PlanMgmtConstants.ISSUER_ID);
			request.getSession().setAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP, issuerIdForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP) != null) {
			issuerIdForQHP = request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
			marketForQHP = request.getParameter(PlanMgmtConstants.MARKET_ID);
			request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP) != null) {
			marketForQHP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
			rNameForQHP = request.getParameter(PlanMgmtConstants.REGION_NAME);
			request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP, rNameForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP) != null) {
			rNameForQHP = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP).toString();
		}

		if (rNameForQHP != null && !rNameForQHP.isEmpty()) {
			ratingRegionAreaForQHPGraph = dataForReport.getRatingRegionValue(rNameForQHP);
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("ratingRegionAreaForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForQHPGraph)));
		}
		}

		if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
			periodIDForQHP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
			request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP) != null) {
			periodIDForQHP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP).toString();
		}

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		List<String> regionListForQHPGraph = ratingAreaRepository.getAllRatingAreaName(stateCode);

		if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
			String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
			for (int i = 0; i < planLevels.length; i++) {
				if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
					platinumCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, platinumCheckForQHPGraph);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
					goldCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, goldCheckForQHPGraph);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
					silverCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, silverCheckForQHPGraph);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
					bronzeCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, bronzeCheckForQHPGraph);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
					expandedBronzeCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, expandedBronzeCheckForQHPGraph);
				}else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
					catastrophicCheckForQHPGraph = planLevels[i];
					planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, catastrophicCheckForQHPGraph);
				}
			}

			model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
			request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
		} else {
			if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP) != null) {
				String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);

				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
						platinumCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, platinumCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
						goldCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, goldCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
						silverCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, silverCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
						bronzeCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, bronzeCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
						expandedBronzeCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, bronzeCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
						catastrophicCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, catastrophicCheckForQHPGraph);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);

			} else {
				planLevelStringForQHPGraph = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_QHP;
				model.addAttribute(PlanMgmtConstants.GOLD_CHECK, Plan.PlanLevel.GOLD);
				model.addAttribute(PlanMgmtConstants.PLATINUM_CHECK, Plan.PlanLevel.PLATINUM);
				model.addAttribute(PlanMgmtConstants.SILVER_CHECK, Plan.PlanLevel.SILVER);
				model.addAttribute(PlanMgmtConstants.BRONZE_CHECK, Plan.PlanLevel.BRONZE);
				model.addAttribute(PlanMgmtConstants.EXPANDED_BRONZE_CHECK, Plan.PlanLevel.EXPANDEDBRONZE);
				model.addAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK, Plan.PlanLevel.CATASTROPHIC);
				periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
				marketForQHP = null;
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);

				String[] planLevels = {
						Plan.PlanLevel.PLATINUM.toString().toLowerCase(),
						Plan.PlanLevel.GOLD.toString().toLowerCase(),
						Plan.PlanLevel.SILVER.toString().toLowerCase(),
						Plan.PlanLevel.BRONZE.toString().toLowerCase(),
						Plan.PlanLevel.EXPANDEDBRONZE.toString().toLowerCase(),
						Plan.PlanLevel.CATASTROPHIC.toString().toLowerCase() };
				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
			}
		}

		if (issuerIdForQHP != null && !issuerIdForQHP.isEmpty()) {
			try {
				requiredIssuerForQHPGraph = issuerService.getIssuerById(Integer.parseInt(issuerIdForQHP));
			} catch (Exception e) {
				LOGGER.error("ERROR IN ISSUER AT REPORT: ", e);
			}
		}
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("planLevelStringForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPGraph)));
		}
		
		/*	iteration depends on the number of plan levels	*/
		if (planLevelStringForQHPGraph != null) {
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("planLevelString for qhpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPGraph.contains(splitStringForReport))));
			}
			boolean hasMultipleString = planLevelStringForQHPGraph.contains(splitStringForReport);
			if (hasMultipleString) {
				String[] planLevelArrForQHPGraph = planLevelStringForQHPGraph.split(splitStringForReport);
				for (int p = 0; p < planLevelArrForQHPGraph.length; p++) {
					List<String> returnList = new ArrayList<String>();
					EnrollmentRequest enrollmentRequestForQHPGraph = new EnrollmentRequest();
					Map<String, String> timeMap = new HashMap<String, String>();
					if(null != periodIDForQHP){
						timeMap = dataForReport.setTimePeriods(periodIDForQHP.toUpperCase());
					}

					if (marketForQHP != null && !(marketForQHP.isEmpty())) {
						if (marketTypeConcatStringForQHPGraph != null
								&& !(marketTypeConcatStringForQHPGraph.isEmpty())
								&& !(marketTypeConcatStringForQHPGraph.contains(marketForQHP))) {
							marketTypeConcatStringForQHPGraph = getConcatedString(marketTypeConcatStringForQHPGraph, marketForQHP);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPGraph)));
							}
						} else {
							marketTypeConcatStringForQHPGraph = marketForQHP;
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPGraph)));
						}
						}
					} else {
						marketTypeConcatStringForQHPGraph = PlanMgmtConstants.ALL_MARKET_TYPES;
					}
					String marketTypeArray[] = marketTypeConcatStringForQHPGraph.split(splitStringForReport);
					if(LOGGER.isDebugEnabled()){
					LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
					}

						for (int market = 0; market < marketTypeArray.length; market++) {

							if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
								enrollmentRequestForQHPGraph.setPlanMarket(MarketType.INDIVIDUAL);
							} else {
								enrollmentRequestForQHPGraph.setPlanMarket(MarketType.SHOP);
							}

							if (requiredIssuerForQHPGraph != null) {
								enrollmentRequestForQHPGraph.setIssuerName(requiredIssuerForQHPGraph.getName());
							} else {
								enrollmentRequestForQHPGraph.setIssuerName(null);
							}
							enrollmentRequestForQHPGraph.setPlanLevel((planLevelArrForQHPGraph[p].toUpperCase()));
							if (ratingRegionAreaForQHPGraph != 0) {
								enrollmentRequestForQHPGraph.setRatingRegion(String.valueOf(ratingRegionAreaForQHPGraph));
							} else {
								enrollmentRequestForQHPGraph.setRatingRegion(null);
							}
							enrollmentRequestForQHPGraph.setStartDate(DateUtil.changeFormat(timeMap.get("REQUIRED_PREVIOUS_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
							enrollmentRequestForQHPGraph.setEndDate(DateUtil.changeFormat(timeMap.get("CURRENT_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));

							XStream xstreamForQHPGraph = GhixUtils.getXStreamStaxObject(); 
							String xmlRequestForQHPGraph = xstreamForQHPGraph.toXML(enrollmentRequestForQHPGraph);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("xmlRequestToSendForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForQHPGraph)));
							}

							String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_ISSUER_URL, xmlRequestForQHPGraph, String.class);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
							}
							temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();

							EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForQHPGraph.fromXML(enrollmentListXML);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
							}
							temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
							if (temp_enrollemntResponse != null
									&& temp_enrollemntResponse.size() > 0) {
								act_enrollListForQHPGraph.addAll(temp_enrollemntResponse);
							}
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("act_enrollListForQHPGraph_test: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForQHPGraph.size())));
							}

							if (temp_enrollemntResponse != null
									&& temp_enrollemntResponse.size() > 0) {
								int enroll_size = act_enrollListForQHPGraph.size();
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("act_enrollList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enroll_size)));
								}

								/* seperate data for graph depend on time period */
								if (periodIDForQHP != null
										&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
									timeSpanForGraph = PlanMgmtConstants.MONTHLY_DEFAULT_SPAN;
								} else if (periodIDForQHP != null
										&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
									timeSpanForGraph = PlanMgmtConstants.QUARTERLY_DEFAULT_SPAN;
								} else if (periodIDForQHP != null
										&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
									timeSpanForGraph = PlanMgmtConstants.YEAR_DEFAULT_SPAN;
								}

								if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
									plantiumListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								} else if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
									goldListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								} else if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
									silverListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								} else if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
									bronzeListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								}else if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
									expandedBronzeListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								}
								else if (planLevelArrForQHPGraph[p] != null
										&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
									catastrophicListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], returnList);
								}

								/*Used for the color Seperation and show the colors on graph*/
								if (periodIDForQHP != null
										&& planLevelArrForQHPGraph[p] != null) {
									dataForReport.setTimePeriodsAndValues(model, periodIDForQHP, act_enrollListForQHPGraph);

									if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, goldListForQHPGraph, planLevelArrForQHPGraph[p]);
									} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, plantiumListForQHPGraph, planLevelArrForQHPGraph[p]);
									} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, silverListForQHPGraph, planLevelArrForQHPGraph[p]);
									} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, bronzeListForQHPGraph, planLevelArrForQHPGraph[p]);
									}else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, expandedBronzeListForQHPGraph, planLevelArrForQHPGraph[p]);
									}
									else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
										dataForReport.setTimePeriodsValues(model, periodIDForQHP, catastrophicListForQHPGraph, planLevelArrForQHPGraph[p]);
									}
								}
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("Enrollment List for QHP Graph: " + SecurityUtil.sanitizeForLogging(String.valueOf(enroll_size)));
								}
							} else {
								LOGGER.info("No Data populated for Enrollment List for QHP Graph");
							}

						}

				}
			}
		}
		model.addAttribute(PlanMgmtConstants.ISSUER_LIST, issuersList);
		model.addAttribute(PlanMgmtConstants.ISSUER_ID, issuerIdForQHP);
		model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForQHP);
		model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForQHP);
		model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForQHP);
		if (regionListForQHPGraph != null) {
			model.addAttribute(PlanMgmtConstants.REGION_LIST, regionListForQHPGraph);
		}
		}catch (Exception e) {						
			LOGGER.error("Exception at enrollment graph for QHP: ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_GRAPH_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/reports/qhpenrollmentreport/graph";
	}

	private String getConcatedString(String planLevelString, String levelString) {
		if (levelString != null) {
			if (planLevelString != null) {
				planLevelString = planLevelString + levelString
						+ splitStringForReport;
			} else {
				planLevelString = levelString + splitStringForReport;
			}
		}
		return planLevelString;
	}

	@RequestMapping(value = "/admin/reports/qhpenrollmentreport/list", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.VIEW_ENROLLMENT_REPORT)
	public String qhpEnrollmentReportList(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		try{
		if (request.getQueryString() != null) {
			if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
				request.getSession().removeAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.PLATINUM_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.GOLD_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.SILVER_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.BRONZE_CHECK_FOR_QHP);
				//request.getSession().removeAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP);
				request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);
			}
		}

		String issuerIdForQHP = null;
		List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
		ProcessDataForReport dataForReport = new ProcessDataForReport();
		List<EnrollmentPlanResponseDTO> act_enrollListForQHPList = new ArrayList<EnrollmentPlanResponseDTO>();
		List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;
		List<HashMap<String, String>> listPageMapData = new ArrayList<HashMap<String, String>>();

		String marketTypeConcatStringForQHPList = null;
		String planLevelStringForQHPList = null;
		String rNameForQHPList = null;
		Integer ratingRegionAreaForQHPList = 0;

		String marketForQHP = null;
		String periodIDForQHP = null;
		String platinumCheckForQHPList = null;
		String goldCheckForQHPList = null;
		String silverCheckForQHPList = null;
		String bronzeCheckForQHPList = null;
		String expandedBronzeCheckForQHPList = null;
		String catastrophicCheckForQHPList = null;
		Issuer requiredIssuerForQHPList = null;

		List<Issuer> issuersList = issuerService.findAllIssuerForReport();
		Collections.sort(issuersList, new ComparatorClass());

		if (request.getParameter(PlanMgmtConstants.ISSUER_ID) != null) {
			issuerIdForQHP = request.getParameter(PlanMgmtConstants.ISSUER_ID);
			request.getSession().setAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP, issuerIdForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP) != null) {
			issuerIdForQHP = request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_QHP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
			marketForQHP = request.getParameter(PlanMgmtConstants.MARKET_ID);
			request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP) != null) {
			marketForQHP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
			periodIDForQHP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
			request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP) != null) {
			periodIDForQHP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP).toString();
		}

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		List<String> regionListForQHPList = ratingAreaRepository.getAllRatingAreaName(stateCode);

		if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
			rNameForQHPList = request.getParameter(PlanMgmtConstants.REGION_NAME);
			request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP, rNameForQHPList);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP) != null) {
			rNameForQHPList = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP).toString();
		}

		if (rNameForQHPList != null && !rNameForQHPList.isEmpty()) {
			ratingRegionAreaForQHPList = dataForReport.getRatingRegionValue(rNameForQHPList);
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("ratingRegionAreaForQHPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForQHPList)));
		}
		}

		if (issuerIdForQHP != null && !issuerIdForQHP.isEmpty()) {
			try {
				requiredIssuerForQHPList = issuerService.getIssuerById(Integer.parseInt(issuerIdForQHP));
			} catch (Exception e) {
				LOGGER.error("ERROR IN REPOER FOR ISSUER : ", e);
			}
		}

		if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
			String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
			for (int i = 0; i < planLevels.length; i++) {
				if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
					platinumCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, platinumCheckForQHPList);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
					goldCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, goldCheckForQHPList);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
					silverCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, silverCheckForQHPList);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
					bronzeCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, bronzeCheckForQHPList);
				}else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
					expandedBronzeCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, expandedBronzeCheckForQHPList);
				}  
				else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
					catastrophicCheckForQHPList = planLevels[i];
					planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, catastrophicCheckForQHPList);
				}
			}

			model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
			request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
		} else {
			if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP) != null) {
				String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);

				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
						platinumCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, platinumCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
						goldCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, goldCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
						silverCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, silverCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
						bronzeCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, bronzeCheckForQHPList);
					}else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
						expandedBronzeCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, expandedBronzeCheckForQHPList);
					}  
					else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
						catastrophicCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, catastrophicCheckForQHPList);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);

			} else {
				planLevelStringForQHPList = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_QHP;
				model.addAttribute(PlanMgmtConstants.GOLD_CHECK, Plan.PlanLevel.GOLD);
				model.addAttribute(PlanMgmtConstants.PLATINUM_CHECK, Plan.PlanLevel.PLATINUM);
				model.addAttribute(PlanMgmtConstants.SILVER_CHECK, Plan.PlanLevel.SILVER);
				model.addAttribute(PlanMgmtConstants.BRONZE_CHECK, Plan.PlanLevel.BRONZE);
				model.addAttribute(PlanMgmtConstants.EXPANDED_BRONZE_CHECK, Plan.PlanLevel.EXPANDEDBRONZE);
				model.addAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK, Plan.PlanLevel.CATASTROPHIC);
				periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
				marketForQHP = null;
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);

				String[] planLevels = {
						Plan.PlanLevel.PLATINUM.toString().toLowerCase(),
						Plan.PlanLevel.GOLD.toString().toLowerCase(),
						Plan.PlanLevel.SILVER.toString().toLowerCase(),
						Plan.PlanLevel.BRONZE.toString().toLowerCase(),
						Plan.PlanLevel.EXPANDEDBRONZE.toString().toLowerCase(),
						Plan.PlanLevel.CATASTROPHIC.toString().toLowerCase() };
				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
			}

		}
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("planLevelStringForQHPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPList)));
		}

		if (planLevelStringForQHPList != null) {
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("planLevelStringForQHPList for qhpList: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPList.contains(splitStringForReport))));
			}
			boolean hasMultipleString = planLevelStringForQHPList.contains(splitStringForReport);
			if (hasMultipleString) {
				String[] planLevelArrForQHPList = planLevelStringForQHPList.split(splitStringForReport);
				if(null != planLevelArrForQHPList){
					for (int p = 0; p < planLevelArrForQHPList.length; p++) {
						EnrollmentRequest enrollmentRequestForQHPList = new EnrollmentRequest();
						Map<String, String> timeMap = new HashMap<String, String>();
	
						if (marketForQHP != null && !(marketForQHP.isEmpty())) {
							if (marketTypeConcatStringForQHPList != null
									&& !(marketTypeConcatStringForQHPList.isEmpty())
									&& !(marketTypeConcatStringForQHPList.contains(marketForQHP))) {
								marketTypeConcatStringForQHPList = getConcatedString(marketTypeConcatStringForQHPList, marketForQHP);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPList)));
								}
							} else {
								marketTypeConcatStringForQHPList = marketForQHP;
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPList)));
							}
							}
						} else {
							marketTypeConcatStringForQHPList = PlanMgmtConstants.ALL_MARKET_TYPES;
						}
						String marketTypeArray[] = marketTypeConcatStringForQHPList.split(splitStringForReport);
						if(null != marketTypeArray){
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
							}
							
							for (int market = 0; market < marketTypeArray.length; market++) {
	
								if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
									enrollmentRequestForQHPList.setPlanMarket(MarketType.INDIVIDUAL);
								} else {
									enrollmentRequestForQHPList.setPlanMarket(MarketType.SHOP);
								}
	
								if (requiredIssuerForQHPList != null) {
									enrollmentRequestForQHPList.setIssuerName(requiredIssuerForQHPList.getName());
								} else {
									enrollmentRequestForQHPList.setIssuerName(null);
								}
	
								timeMap = dataForReport.setTimePeriods(periodIDForQHP.toUpperCase());
								enrollmentRequestForQHPList.setPlanLevel((planLevelArrForQHPList[p].toUpperCase()));
								if (ratingRegionAreaForQHPList != 0) {
									enrollmentRequestForQHPList.setRatingRegion(String.valueOf(ratingRegionAreaForQHPList));
								} else {
									enrollmentRequestForQHPList.setRatingRegion(null);
								}
								enrollmentRequestForQHPList.setStartDate(DateUtil.changeFormat(timeMap.get("REQUIRED_PREVIOUS_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
								enrollmentRequestForQHPList.setEndDate(DateUtil.changeFormat(timeMap.get("CURRENT_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
	
								//HIX-91520
								//XStream xstreamForQHPList = GhixUtils.getXStreamStaxObject();
								XStream xstreamForQHPList = GhixUtils.getXStreamStaxObject(); 
								String xmlRequestForQHPList = xstreamForQHPList.toXML(enrollmentRequestForQHPList);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("enrollmentRequestForQHPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForQHPList)));
								}
	
								String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_ISSUER_URL, xmlRequestForQHPList, String.class);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
								}
	
								if (enrollmentListXML != null) {
									temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
									EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForQHPList.fromXML(enrollmentListXML);
									if(LOGGER.isDebugEnabled()){
									LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
									}
									temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
	
									if (temp_enrollemntResponse != null
											&& temp_enrollemntResponse.size() > 0) {
										act_enrollListForQHPList.addAll(temp_enrollemntResponse);
									}
	
									if (temp_enrollemntResponse != null
											&& temp_enrollemntResponse.size() > 0) {
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("temp_enrollemntResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_enrollemntResponse.size())));
										}
										listPageData = setDataForListPage(planLevelArrForQHPList[p], periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPList.getStartDate(), enrollmentRequestForQHPList.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market]);
	
										List<AdminReportFormBean> formBeans = new ArrayList<AdminReportFormBean>();
										formBeans.addAll(listPageData);
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("formBeans: " + SecurityUtil.sanitizeForLogging(String.valueOf(formBeans.size())));
										}
	
										removeDuplicateInfo(listPageData);
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData.size())));
										}
	
										if (listPageData.size() > 0) {
											for (AdminReportFormBean adminReportFormBean : listPageData) {
												HashMap<String, String> listPageMap = new HashMap<String, String>();
												Integer enrollcount = getEnrollmentCountForOneEnrollment(adminReportFormBean, formBeans);
												if(LOGGER.isDebugEnabled()){
												LOGGER.debug("enrollcount: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollcount)));
												}
												listPageMap.put(PlanMgmtConstants.TIME_PERIOD, adminReportFormBean.getTimePeriod());
												listPageMap.put(PlanMgmtConstants.ISSUER_NAME, adminReportFormBean.getIssuerName());
												listPageMap.put(PlanMgmtConstants.PLAN_NAME, adminReportFormBean.getPlanName());
												listPageMap.put(PlanMgmtConstants.PLAN_NUMBER, adminReportFormBean.getPlanNumber());
												listPageMap.put(PlanMgmtConstants.PLAN_LEVEL, adminReportFormBean.getPlanLevel());
												listPageMap.put(PlanMgmtConstants.PLAN_MARKET, adminReportFormBean.getMarket());
												listPageMap.put(PlanMgmtConstants.ENROLLMENT, String.valueOf(enrollcount));
												listPageMapData.add(listPageMap);
											}
										}
									}
								} else {
									LOGGER.info("No Data populated for Enrollment List for QHP List");
								}
							}
						}
	
					}
				}
			}
		}
		model.addAttribute(PlanMgmtConstants.ISSUER_LIST, issuersList);
		model.addAttribute(PlanMgmtConstants.ISSUER_ID, issuerIdForQHP);
		model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForQHP);
		model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForQHP);
		model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForQHPList);
		// model.addAttribute("listPageData", listPageData);
		model.addAttribute(PlanMgmtConstants.LIST_PAGE_DATA, listPageMapData);
		model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, PlanMgmtConstants.PAGE_SIZE);

		if (regionListForQHPList != null) {
			model.addAttribute(PlanMgmtConstants.REGION_LIST, regionListForQHPList);
		}
		}catch (Exception e) {
			LOGGER.error("Exception at enrollment list for QHP: ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/reports/qhpenrollmentreport/list";
	}

	@RequestMapping(value = "/admin/reports/sadpenrollmentreport/graph", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.VIEW_ENROLLMENT_REPORT)
	public String sadpEnrollmentReportGraph(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		Issuer requiredIssuerForSADPGraph = null;
		try{
		if (request.getQueryString() != null) {
			if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
				request.getSession().removeAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.HIGH_CHECK_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.LOW_CHECK_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);
			}
		}

		String planLevelStringForSADP = null;
		String marketTypeConcatStringForSADPGraph = null;
		String rNameForSADPGraph = null;
		String issuerIDForSADP = null;
		String marketForSADP = null;
		String periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
		String highCheckForSADP = null;
		String lowCheckForSADP = null;
		Integer ratingRegionAreaForSADPGraph = 0;
		List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;
		List<EnrollmentPlanResponseDTO> act_enrollListForSADPGraph = new ArrayList<EnrollmentPlanResponseDTO>();
		List<String> highListForSADPGraph = new ArrayList<String>();
		List<String> lowListForSADPGraph = new ArrayList<String>();

		List<Issuer> issuersList = issuerService.findAllIssuerForReport();
		Collections.sort(issuersList, new ComparatorClass());

		if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
			periodIDForSADP = request.getParameter(PlanMgmtConstants.PERIOD_ID).toString();
			request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP) != null) {
			periodIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.ISSUER_ID) != null) {
			issuerIDForSADP = request.getParameter(PlanMgmtConstants.ISSUER_ID);
			request.getSession().setAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP, issuerIDForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP) != null) {
			issuerIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
			marketForSADP = request.getParameter(PlanMgmtConstants.MARKET_ID);
			request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP, marketForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP) != null) {
			marketForSADP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
			rNameForSADPGraph = request.getParameter(PlanMgmtConstants.REGION_NAME);
			request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP, rNameForSADPGraph);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP) != null) {
			rNameForSADPGraph = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP).toString();
		}

		if (rNameForSADPGraph != null && !rNameForSADPGraph.isEmpty()) {
			ratingRegionAreaForSADPGraph = dataForReport.getRatingRegionValue(rNameForSADPGraph);
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("ratingRegionAreaForSADPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForSADPGraph)));
		}
		}

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		List<String> regionListForSADPGraph = ratingAreaRepository.getAllRatingAreaName(stateCode);

		if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
			String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
			for (int i = 0; i < planLevels.length; i++) {
				if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
					highCheckForSADP = planLevels[i];
					planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
					lowCheckForSADP = planLevels[i];
					planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
				}
			}

			model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
			request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
		} else {
			if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP) != null) {
				String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);

				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
						highCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
						lowCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);

			} else {
				planLevelStringForSADP = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_SADP;
				model.addAttribute(PlanMgmtConstants.HIGH_CHECK, Plan.PlanLevelDental.HIGH);
				model.addAttribute(PlanMgmtConstants.LOW_CHECK, Plan.PlanLevelDental.LOW);
				periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
				marketForSADP = null;
				String[] planLevels = {
						Plan.PlanLevelDental.HIGH.toString().toLowerCase(),
						Plan.PlanLevelDental.LOW.toString().toLowerCase() };
				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
			}
		}

		if (issuerIDForSADP != null && !issuerIDForSADP.isEmpty()) {
			try {
				requiredIssuerForSADPGraph = issuerService.getIssuerById(Integer.parseInt(issuerIDForSADP));
			} catch (Exception e) {
				LOGGER.error("ERROR FOR ISSUER IN REPORT : ", e);
			}
		}

		if (planLevelStringForSADP != null) {
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("planLevelStringForSADP for sadpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForSADP.contains(splitStringForReport))));
			}
			boolean hasMultipleString = planLevelStringForSADP.contains(splitStringForReport);
			if (hasMultipleString == true) {
				String[] planLevelArrForSADPGraph = planLevelStringForSADP.split(splitStringForReport);
				for (int p = 0; p < planLevelArrForSADPGraph.length; p++) {
					List<String> returnList = new ArrayList<String>();
					EnrollmentRequest enrollmentRequestForSADPGraph = new EnrollmentRequest();
					Map<String, String> timeMap = new HashMap<String, String>();

					if (marketForSADP != null && !(marketForSADP.isEmpty())) {
						if (marketTypeConcatStringForSADPGraph != null
								&& !(marketTypeConcatStringForSADPGraph.isEmpty())
								&& !(marketTypeConcatStringForSADPGraph.contains(marketForSADP))) {
							marketTypeConcatStringForSADPGraph = getConcatedString(marketTypeConcatStringForSADPGraph, marketForSADP);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPGraph)));
							}
						} else {
							marketTypeConcatStringForSADPGraph = marketForSADP;
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPGraph)));
						}
						}
					} else {
						marketTypeConcatStringForSADPGraph = PlanMgmtConstants.ALL_MARKET_TYPES;
					}
					String marketTypeArray[] = marketTypeConcatStringForSADPGraph.split(splitStringForReport);
					if(LOGGER.isDebugEnabled()){
					LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
					}

						for (int market = 0; market < marketTypeArray.length; market++) {

							if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
								enrollmentRequestForSADPGraph.setPlanMarket(MarketType.INDIVIDUAL);
							} else {
								enrollmentRequestForSADPGraph.setPlanMarket(MarketType.SHOP);
							}

							if (requiredIssuerForSADPGraph != null) {
								enrollmentRequestForSADPGraph.setIssuerName(requiredIssuerForSADPGraph.getName());
							} else {
								enrollmentRequestForSADPGraph.setIssuerName(null);
							}

							timeMap = dataForReport.setTimePeriods(periodIDForSADP.toUpperCase());
							if (ratingRegionAreaForSADPGraph != 0) {
								enrollmentRequestForSADPGraph.setRatingRegion(String.valueOf(ratingRegionAreaForSADPGraph));
							} else {
								enrollmentRequestForSADPGraph.setRatingRegion(null);
							}

							enrollmentRequestForSADPGraph.setPlanLevel((planLevelArrForSADPGraph[p].toUpperCase()));
							enrollmentRequestForSADPGraph.setStartDate(DateUtil.changeFormat(timeMap.get("REQUIRED_PREVIOUS_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
							enrollmentRequestForSADPGraph.setEndDate(DateUtil.changeFormat(timeMap.get("CURRENT_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));

							//HIX-91520, HIX-93225
							//XStream xstreamForSADPGraph = GhixUtils.getXStreamStaxObject();
							XStream xstreamForSADPGraph = GhixUtils.getXStreamStaxObject(); 
							String xmlRequestForSADPGraph = xstreamForSADPGraph.toXML(enrollmentRequestForSADPGraph);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("xmlRequestForSADPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForSADPGraph)));
							}

							String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_ISSUER_URL, xmlRequestForSADPGraph, String.class);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
							}

							if (enrollmentListXML != null) {
								temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
								EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForSADPGraph.fromXML(enrollmentListXML);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
								}
								temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();

								if (temp_enrollemntResponse != null
										&& temp_enrollemntResponse.size() > 0) {
									act_enrollListForSADPGraph.addAll(temp_enrollemntResponse);
									if(LOGGER.isDebugEnabled()){
									LOGGER.debug("act_enrollListForSADPGraph_test: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPGraph.size())));
								}
								}

								if (act_enrollListForSADPGraph != null
										&& act_enrollListForSADPGraph.size() > 0) {
									if (periodIDForSADP != null
											&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.MONTHLY_DEFAULT_SPAN;
									} else if (periodIDForSADP != null
											&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.QUARTERLY_DEFAULT_SPAN;
									} else if (periodIDForSADP != null
											&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.YEAR_DEFAULT_SPAN;
									}

									if (planLevelArrForSADPGraph[p] != null
											&& planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
										lowListForSADPGraph = setDataForGraph(planLevelArrForSADPGraph[p], timeSpanForGraph, periodIDForSADP, act_enrollListForSADPGraph, enrollmentRequestForSADPGraph.getStartDate(), enrollmentRequestForSADPGraph.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market], returnList);
									} else if (planLevelArrForSADPGraph[p] != null
											&& planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
										highListForSADPGraph = setDataForGraph(planLevelArrForSADPGraph[p], timeSpanForGraph, periodIDForSADP, act_enrollListForSADPGraph, enrollmentRequestForSADPGraph.getStartDate(), enrollmentRequestForSADPGraph.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market], returnList);
									}

									if (periodIDForSADP != null
											&& act_enrollListForSADPGraph != null
											& planLevelStringForSADP != null) {
										dataForReport.setTimePeriodsAndValues(model, periodIDForSADP, act_enrollListForSADPGraph);
										if (planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForSADP, highListForSADPGraph, planLevelArrForSADPGraph[p]);
										} else if (planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForSADP, lowListForSADPGraph, planLevelArrForSADPGraph[p]);
										}
									}
									if(LOGGER.isDebugEnabled()){
									LOGGER.debug("Enrollment List for SADP Graph: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPGraph.size())));
								}
								}
							} else {
								LOGGER.info("No Data populated for Enrollment List for SADP Graph");
							}

						}

				}
			}
		}

		model.addAttribute(PlanMgmtConstants.ISSUER_LIST, issuersList);
		model.addAttribute(PlanMgmtConstants.ISSUER_ID, issuerIDForSADP);
		model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForSADP);
		model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForSADP);
		model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForSADPGraph);
		if (regionListForSADPGraph != null) {
			model.addAttribute(PlanMgmtConstants.REGION_LIST, regionListForSADPGraph);
		}
		}catch (Exception e) {
			LOGGER.error("Exception at enrollment graph for SADP: ",e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SADPENROLLMENT_REPORT_GRAPH_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/reports/sadpenrollmentreport/graph";
	}

	@RequestMapping(value = "/admin/reports/sadpenrollmentreport/list", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.VIEW_ENROLLMENT_REPORT)
	public String sadpEnrollmentReportList(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		try{
		if (request.getQueryString() != null) {
			if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
				request.getSession().removeAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.HIGH_CHECK_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.LOW_CHECK_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);
				request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP);
			}
		}

		Issuer requiredIssuerForSADPList = null;
		ProcessDataForReport dataForReport = new ProcessDataForReport();

		List<HashMap<String, String>> listPageMapData = new ArrayList<HashMap<String, String>>();
		List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
		List<EnrollmentPlanResponseDTO> act_enrollListForSADPList = new ArrayList<EnrollmentPlanResponseDTO>();
		List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;

		String issuerIDForSADP = null;
		String planLevelStringForSADP = null;
		String marketTypeConcatStringForSADPList = null;
		String rNameForSADPList = null;
		String marketForSADP = null;
		String periodIDForSADP = null;
		String lowCheckForSADP = null;
		String highCheckForSADP = null;
		Integer ratingRegionAreaForSADPList = 0;

		List<Issuer> issuersList = issuerService.findAllIssuerForReport();
		Collections.sort(issuersList, new ComparatorClass());

		if (request.getParameter(PlanMgmtConstants.ISSUER_ID) != null) {
			issuerIDForSADP = request.getParameter(PlanMgmtConstants.ISSUER_ID);
			request.getSession().setAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP, issuerIDForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP) != null) {
			issuerIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.ISSUER_ID_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
			marketForSADP = request.getParameter(PlanMgmtConstants.MARKET_ID);
			request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP, marketForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP) != null) {
			marketForSADP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
			periodIDForSADP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
			request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP) != null) {
			periodIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP).toString();
		}

		if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
			rNameForSADPList = request.getParameter(PlanMgmtConstants.REGION_NAME);
			request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP, rNameForSADPList);
		} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP) != null) {
			rNameForSADPList = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP).toString();
		}

		if (rNameForSADPList != null && !rNameForSADPList.isEmpty()) {
			ratingRegionAreaForSADPList = dataForReport.getRatingRegionValue(rNameForSADPList);
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("ratingRegionAreaForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForSADPList)));
		}
		}

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		List<String> regionListForSADPList = ratingAreaRepository.getAllRatingAreaName(stateCode);

		String requiredMarketTypeForSADPList = dataForReport.getRequiredMarketType(marketForSADP);
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("requiredMarketTypeForSADPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(requiredMarketTypeForSADPList)));
		}

		if (issuerIDForSADP != null && !issuerIDForSADP.isEmpty()) {
			try {
				requiredIssuerForSADPList = issuerService.getIssuerById(Integer.parseInt(issuerIDForSADP));
			} catch (Exception e) {
				LOGGER.error("ERROR IN REPORT FOR ISSUER : ", e);
			}
		}

		if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
			String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
			for (int i = 0; i < planLevels.length; i++) {
				if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
					highCheckForSADP = planLevels[i];
					planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
				} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
					lowCheckForSADP = planLevels[i];
					planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
				}
			}

			model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
			request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
		} else {
			if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP) != null) {
				String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);

				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
						highCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
						lowCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);

			} else {
				planLevelStringForSADP = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_SADP;
				model.addAttribute(PlanMgmtConstants.HIGH_CHECK, Plan.PlanLevelDental.HIGH);
				model.addAttribute(PlanMgmtConstants.LOW_CHECK, Plan.PlanLevelDental.LOW);
				periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
				marketForSADP = null;
				String[] planLevels = {
						Plan.PlanLevelDental.HIGH.toString().toLowerCase(),
						Plan.PlanLevelDental.LOW.toString().toLowerCase() };
				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
			}
		}

		if (planLevelStringForSADP != null) {
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("planLevelStringForSADP for sadpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForSADP.contains(splitStringForReport))));
			}
			boolean hasMultipleString = planLevelStringForSADP.contains(splitStringForReport);
			if (hasMultipleString) {
				String[] planLevelArrForSADPList = planLevelStringForSADP.split(splitStringForReport);
				if(null != planLevelArrForSADPList){
					for (int p = 0; p < planLevelArrForSADPList.length; p++) {
						EnrollmentRequest enrollmentRequestForSADPList = new EnrollmentRequest();
						Map<String, String> timeMap = new HashMap<String, String>();
						if (marketForSADP != null && !(marketForSADP.isEmpty())) {
							if (marketTypeConcatStringForSADPList != null
									&& !(marketTypeConcatStringForSADPList.isEmpty())
									&& !(marketTypeConcatStringForSADPList.contains(marketForSADP))) {
								marketTypeConcatStringForSADPList = getConcatedString(marketTypeConcatStringForSADPList, marketForSADP);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)));
								}
							} else {
								marketTypeConcatStringForSADPList = marketForSADP;
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)));
							}
							}
						} else {
							marketTypeConcatStringForSADPList = PlanMgmtConstants.ALL_MARKET_TYPES;
						}
						if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Market_Type_Concated: "+ SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)));
						}
						
						String marketTypeArray[] = marketTypeConcatStringForSADPList.split(splitStringForReport);
						if(null != marketTypeArray){
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
							}
							for (int market = 0; market < marketTypeArray.length; market++) {
	
								if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
									enrollmentRequestForSADPList.setPlanMarket(MarketType.INDIVIDUAL);
								} else {
									enrollmentRequestForSADPList.setPlanMarket(MarketType.SHOP);
								}
	
								if (requiredIssuerForSADPList != null) {
									enrollmentRequestForSADPList.setIssuerName(requiredIssuerForSADPList.getName());
								} else {
									enrollmentRequestForSADPList.setIssuerName("");
								}
	
								timeMap = dataForReport.setTimePeriods(periodIDForSADP.toUpperCase());
	
								if (ratingRegionAreaForSADPList != 0) {
									enrollmentRequestForSADPList.setRatingRegion(String.valueOf(ratingRegionAreaForSADPList));
								} else {
									enrollmentRequestForSADPList.setRatingRegion(null);
								}
	
								enrollmentRequestForSADPList.setPlanLevel((planLevelArrForSADPList[p].toUpperCase()));
								enrollmentRequestForSADPList.setStartDate(DateUtil.changeFormat(timeMap.get("REQUIRED_PREVIOUS_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
								enrollmentRequestForSADPList.setEndDate(DateUtil.changeFormat(timeMap.get("CURRENT_DATE"), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
	
								//HIX-91520, HIX-93225	
								//XStream xstreamForSADPList = GhixUtils.getXStreamStaxObject();
								XStream xstreamForSADPList = GhixUtils.getXStreamStaxObject(); 
								String xmlRequestForSADPList = xstreamForSADPList.toXML(enrollmentRequestForSADPList);
								
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("xmlRequestForSADPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForSADPList)));
								}
	
								String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_ISSUER_URL, xmlRequestForSADPList, String.class);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
								}
	
								if (enrollmentListXML != null) {
									temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
									EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForSADPList.fromXML(enrollmentListXML);
									if(LOGGER.isDebugEnabled()){
									LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
									}
									temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
									if (temp_enrollemntResponse != null
											&& temp_enrollemntResponse.size() > 0) {
										act_enrollListForSADPList.addAll(temp_enrollemntResponse);
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("act_enrollListForSADPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPList.size())));
									}
									}
									if (act_enrollListForSADPList != null
											&& act_enrollListForSADPList.size() > 0) {
										listPageData = setDataForListPage(planLevelArrForSADPList[p], periodIDForSADP, act_enrollListForSADPList, enrollmentRequestForSADPList.getStartDate(), enrollmentRequestForSADPList.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market]);
										List<AdminReportFormBean> formBeans = new ArrayList<AdminReportFormBean>();
										formBeans.addAll(listPageData);
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("formBeans: " + SecurityUtil.sanitizeForLogging(String.valueOf(formBeans.size())));
										}
	
										removeDuplicateInfo(listPageData);
										if(LOGGER.isDebugEnabled()){
										LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData.size())));
										}
	
										if (listPageData.size() > 0) {
											for (AdminReportFormBean adminReportFormBean : listPageData) {
												HashMap<String, String> listPageMap = new HashMap<String, String>();
												Integer enrollcount = getEnrollmentCountForOneEnrollment(adminReportFormBean, formBeans);
												if(LOGGER.isDebugEnabled()){
												LOGGER.debug("enrollcount: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollcount)));
												}
												listPageMap.put(PlanMgmtConstants.TIME_PERIOD, adminReportFormBean.getTimePeriod());
												listPageMap.put(PlanMgmtConstants.ISSUER_NAME, adminReportFormBean.getIssuerName());
												listPageMap.put(PlanMgmtConstants.PLAN_NAME, adminReportFormBean.getPlanName());
												listPageMap.put(PlanMgmtConstants.PLAN_NUMBER, adminReportFormBean.getPlanNumber());
												listPageMap.put(PlanMgmtConstants.PLAN_LEVEL, adminReportFormBean.getPlanLevel());
												listPageMap.put(PlanMgmtConstants.PLAN_MARKET, adminReportFormBean.getMarket());
												listPageMap.put(PlanMgmtConstants.ENROLLMENT, String.valueOf(enrollcount));
												listPageMapData.add(listPageMap);
											}
										}
									}
								} else {
									LOGGER.info("No Data available in list to display");
								}
	
							}
						}
					}
				}
			}
		}

		model.addAttribute(PlanMgmtConstants.ISSUER_LIST, issuersList);
		model.addAttribute(PlanMgmtConstants.ISSUER_ID, issuerIDForSADP);
		model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForSADP);
		model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForSADP);
		model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForSADPList);
		model.addAttribute(PlanMgmtConstants.LIST_PAGE_DATA, listPageMapData);
		model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, PlanMgmtConstants.PAGE_SIZE);
		if (regionListForSADPList != null) {
			model.addAttribute(PlanMgmtConstants.REGION_LIST, regionListForSADPList);
		}

		}catch (Exception e) {
			LOGGER.error("Exception at enrollment list for QHP: ",e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SADPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/reports/sadpenrollmentreport/list";
	}

	@SuppressWarnings("rawtypes")
	public List<String> setDataForGraph(String planLevelString, Integer limitPeriod, String periodID, List<EnrollmentPlanResponseDTO> enrollmentList, String startDate, String endDate, String graphType, String market, List<String> returnList) {
		List<String> tempList = new ArrayList<String>();
		List<String> blankspaceList = new ArrayList<String>();
		List<String> temp_returnList = new ArrayList<String>();
		if (periodID != null
				&& periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
			/*for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
				LOGGER.info("PlanID: " + enrollment.getPlanId() +" and Market:"+ market.toUpperCase());
				//Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
				Plan plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
				
				if(plan!=null){
					LOGGER.info("PlanInfo: " + plan.getId() + plan.getName() +":::"+ plan.getInsuranceType() + plan.getMarket());
					tempList=produceGraphOutPutforMonthly(plan,enrollment,planLevelString,tempList,periodID,graphType);
					
				}
			}*/
			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						if(LOGGER.isDebugEnabled()){
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						}
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanHealth().getPlanLevel())) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanDental().getPlanLevel())) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						}
						break;
					}
				}
			}
			// End changes

			blankspaceList.addAll(tempList);
			if (blankspaceList != null && blankspaceList.size() > 0) {
				if(LOGGER.isDebugEnabled()){
				LOGGER.debug("blankspaceList: " + SecurityUtil.sanitizeForLogging(String.valueOf(blankspaceList)));
				}
				temp_returnList = dataForReport.getResultantGraph(blankspaceList, periodID);
				if (returnList.size() <= 0) {
					returnList.addAll(temp_returnList);
				} else {
					List<String> demo = new ArrayList<String>();
					demo.addAll(returnResultantList(returnList, temp_returnList));
					returnList.clear();
					returnList.addAll(demo);
				}
			}

		} else if (periodID != null
				&& periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
			float fquarter1 = 0.0f;
			float fquarter2 = 0.0f;
			float fquarter3 = 0.0f;
			float fquarter4 = 0.0f;

			/*
			for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
				//Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
				Plan plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
				if(plan!=null){
					LOGGER.info("QuarterlyPlanInfo: " + plan.getName() +":::"+ plan.getInsuranceType());
					tempList=produceGraphOutPutforQuarterly(plan,enrollment,planLevelString,tempList,periodID,graphType);
					
				}
			}*/

			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						if(LOGGER.isDebugEnabled()){
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						}
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanHealth().getPlanLevel())) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanDental().getPlanLevel())) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						}
						break;
					}
				}
			}
			// End changes

			for (int k = 0; k < tempList.size(); k += 4) {
				if (!tempList.get(k).equals("")) {
					fquarter1 = fquarter1 + Float.parseFloat(tempList.get(k));
				}
				if (!tempList.get(k + 1).equals("")) {
					fquarter2 = fquarter2
							+ Float.parseFloat(tempList.get(k + 1));
				}
				if (!tempList.get(k + 2).equals("")) {
					fquarter3 = fquarter3
							+ Float.parseFloat(tempList.get(k + 2));
				}
				if (!tempList.get(k + 3).equals("")) {
					fquarter4 = fquarter4
							+ Float.parseFloat(tempList.get(k + 3));
				}
			}
			returnList.add(String.valueOf(fquarter1));
			returnList.add(String.valueOf(fquarter2));
			returnList.add(String.valueOf(fquarter3));
			returnList.add(String.valueOf(fquarter4));
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("returnList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnList)));
			}
		} else if (periodID != null
				&& periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
			Float pcount;
			Map<String, String> tempMap = new HashMap<String, String>();
			/*for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
				//Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
				Plan plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
				if(plan!=null){
					LOGGER.info("QuarterlyPlanInfo: " + plan.getName() +":::"+ plan.getInsuranceType());
					
					String requiredDate=DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), REQUIRED_DATE_FORMAT), REQUIRED_DATE_FORMAT);
					LocalDate enrollmentDate=new LocalDate(requiredDate);

					pcount=produceGraphOutPutforYearly(plan,enrollment,planLevelString,tempList,periodID,graphType);
					if(tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))){
						if(tempMap.get(String.valueOf(enrollmentDate.getYear()))!=null){
							pcount=pcount+Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
							tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
						}
					}else{
						tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
					}
				}
			}*/

			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						if(LOGGER.isDebugEnabled()){
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						}
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanHealth().getPlanLevel())) {
								String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
								LocalDate enrollmentDate = new LocalDate(requiredDate);

								pcount = produceGraphOutPutforYearly(plan, enrollment, planLevelString, tempList, periodID, graphType);
								if (tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))) {
									if (tempMap.get(String.valueOf(enrollmentDate.getYear())) != null) {
										pcount = pcount
												+ Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
										tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
									}
								} else {
									tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
								}
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if (planLevelString.toUpperCase().equalsIgnoreCase(plan.getPlanDental().getPlanLevel())) {
								String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
								LocalDate enrollmentDate = new LocalDate(requiredDate);

								pcount = produceGraphOutPutforYearly(plan, enrollment, planLevelString, tempList, periodID, graphType);
								if (tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))) {
									if (tempMap.get(String.valueOf(enrollmentDate.getYear())) != null) {
										pcount = pcount
												+ Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
										tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
									}
								} else {
									tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
								}
							}
						}
						break;
					}
				}
			}
			// End changes
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("tempMap: " + SecurityUtil.sanitizeForLogging(String.valueOf(tempMap)) + SecurityUtil.sanitizeForLogging(String.valueOf(tempMap.size())));
			}
			Date currentDate = new TSDate();
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.YEAR, PlanMgmtConstants.YEARLY_DEC);
			Date previousDate = calendar.getTime();
			SimpleDateFormat sdfDestination = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT);
			String newPreviousDate = sdfDestination.format(previousDate);
			LocalDate lastYearDate = new LocalDate(newPreviousDate);

			Iterator it = tempMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				for (int k = 5; k >= 1; k--) {
					if (String.valueOf((lastYearDate.getYear() + k)).equals(pairs.getKey().toString())) {
						if(LOGGER.isDebugEnabled()){
						LOGGER.debug("LastYear = " + SecurityUtil.sanitizeForLogging(String.valueOf((lastYearDate.getYear() + k))));
						LOGGER.debug(SecurityUtil.sanitizeForLogging(String.valueOf(pairs.getKey())) + " = " + SecurityUtil.sanitizeForLogging(String.valueOf(pairs.getValue())));
						}
						returnList.add(pairs.getValue().toString());
					}
				}
			}

		}
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("returnList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnList)) + "::" + SecurityUtil.sanitizeForLogging(String.valueOf(returnList.size())));
		}
		return returnList;
	}

	public List<String> produceGraphOutPutforMonthly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		// List<PlanHealth> planList_health=null;
		// List<PlanDental> planList_dental=null;
		// if(graphType.equalsIgnoreCase("HEALTH")){
		// planList_health=planMgmtService.getPlanHealthInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }else if(graphType.equalsIgnoreCase("DENTAL")){
		// planList_dental=planMgmtService.getPlanDentalInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }
		//
		// if((planList_health!=null && planList_health.size()>0) ||
		// (planList_dental!=null && planList_dental.size()>0)){
		// }
		float plancount = 0;
		int i = 0;
		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("CURRENT_DATE"))));
		LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("REQUIRED_PREVIOUS_DATE"))));
		}

		LocalDate lastYearDate = new LocalDate(timeMap.get("REQUIRED_PREVIOUS_DATE"));
		LocalDate todayYearDate = new LocalDate(timeMap.get("CURRENT_DATE"));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)) + " lastYearDate: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)) + " todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));
		}

		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));
		}

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.getMonthOfYear() == lastYearDate.getMonthOfYear()
					&& enrollmentDate.isBefore(temp_todayYearDate)) {
				plancount++;
				tempList.add(i, String.valueOf(plancount));
			} else {
				tempList.add(i, "");
			}
			i++;
			lastYearDate = lastYearDate.plus(Period.months(1));
		}
		return tempList;
	}

	public List<String> produceGraphOutPutforQuarterly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		// List<PlanHealth> planList_health=null;
		// List<PlanDental> planList_dental=null;
		// if(graphType.equalsIgnoreCase("HEALTH")){
		// planList_health=planMgmtService.getPlanHealthInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }else if(graphType.equalsIgnoreCase("DENTAL")){
		// planList_dental=planMgmtService.getPlanDentalInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }
		//
		// if((planList_health!=null && planList_health.size()>0) ||
		// (planList_dental!=null && planList_dental.size()>0)){
		float plancount = 0;
		float q1 = 0;
		float q2 = 0;
		float q3 = 0;
		float q4 = 0;

		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("CURRENT_DATE"))));
		LOGGER.debug("REQUIRED_PREVIOUS_DATE: "+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("REQUIRED_PREVIOUS_DATE"))));
		}

		LocalDate lastYearDate = new LocalDate(timeMap.get("REQUIRED_PREVIOUS_DATE"));
		LocalDate todayYearDate = new LocalDate(timeMap.get("CURRENT_DATE"));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);
		
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)) + " lastYearDate: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)) + " todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));
		}

		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));
		}

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.getMonthOfYear() == lastYearDate.getMonthOfYear()
					&& enrollmentDate.isBefore(temp_todayYearDate)) {
				if(LOGGER.isDebugEnabled()){
				LOGGER.debug("monthInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate.getMonthOfYear())));
				}
				if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q1_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q1_END) {
					plancount++;
					q1 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q2_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q2_END) {
					plancount++;
					q2 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q3_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q3_END) {
					plancount++;
					q3 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q4_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q4_END) {
					plancount++;
					q4 = plancount;
				}
			}
			lastYearDate = lastYearDate.plus(Period.months(1));
		}
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("q1: " + SecurityUtil.sanitizeForLogging(String.valueOf(q1)) + " q2: " + SecurityUtil.sanitizeForLogging(String.valueOf(q2)) + " q3: " 
						+ SecurityUtil.sanitizeForLogging(String.valueOf(q3)) + " q4: " + SecurityUtil.sanitizeForLogging(String.valueOf(q4)));
		}

		tempList.add(String.valueOf(q1));
		tempList.add(String.valueOf(q2));
		tempList.add(String.valueOf(q3));
		tempList.add(String.valueOf(q4));
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("tempList: " + SecurityUtil.sanitizeForLogging(String.valueOf(tempList)));
		}
		// }
		return tempList;
	}

	public Float produceGraphOutPutforYearly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		float plancount = 0;
		// List<PlanHealth> planList_health=null;
		// List<PlanDental> planList_dental=null;
		// if(graphType.equalsIgnoreCase("HEALTH")){
		// planList_health=planMgmtService.getPlanHealthInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }else if(graphType.equalsIgnoreCase("DENTAL")){
		// planList_dental=planMgmtService.getPlanDentalInfo(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }
		//
		// if((planList_health!=null && planList_health.size()>0) ||
		// (planList_dental!=null && planList_dental.size()>0)){
		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("CURRENT_DATE"))));
		LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("REQUIRED_PREVIOUS_DATE"))));
		}

		LocalDate lastYearDate = new LocalDate(timeMap.get("REQUIRED_PREVIOUS_DATE"));
		LocalDate todayYearDate = new LocalDate(timeMap.get("CURRENT_DATE"));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);

		LocalDate tempNextDate = new LocalDate(lastYearDate);
		tempNextDate = lastYearDate.plus(Period.years(1));

		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)) + " lastYearDate: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)) + " todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));
		}

		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));
		}

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.isAfter(lastYearDate)
					&& enrollmentDate.isBefore(tempNextDate)) {
				plancount++;
			}
			lastYearDate = tempNextDate;
			tempNextDate = tempNextDate.plus(Period.years(1));
		}
		// }
		return plancount;
	}

	public List<AdminReportFormBean> setDataForListPage(String planLevelString, String periodID, List<EnrollmentPlanResponseDTO> enrollmentList, String startDate, String endDate, String graphType, String market) {
		List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
		for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
			// Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
			Map<String, String> timeMap = new HashMap<String, String>();
			timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("CURRENT_DATE"))));
			LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
					+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get("REQUIRED_PREVIOUS_DATE"))));
			}

			LocalDate lastYearDate = new LocalDate(timeMap.get("REQUIRED_PREVIOUS_DATE"));
			LocalDate todayYearDate = new LocalDate(timeMap.get("CURRENT_DATE"));

			String requiredDate_Check = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
			LocalDate enrollmentDate_Check = new LocalDate(requiredDate_Check);

			LocalDate tempNextDate = new LocalDate(lastYearDate);
			tempNextDate = lastYearDate.plus(Period.years(1));

			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("enrollmentDate_Check: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate_Check)));
			LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
			LOGGER.debug("todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));
			}

			LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));
			}

			if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)
					|| periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
				while (lastYearDate.isBefore(temp_todayYearDate)
						&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {
					if (enrollmentDate_Check.getMonthOfYear() == lastYearDate.getMonthOfYear()
							&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {

						getListPageData(planLevelString, enrollment, graphType, market, listPageData, periodID);
					}
					lastYearDate = lastYearDate.plus(Period.months(1));
				}
			} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
				while (lastYearDate.isBefore(temp_todayYearDate)
						&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {
					if (enrollmentDate_Check.isAfter(lastYearDate)
							&& enrollmentDate_Check.isBefore(tempNextDate)) {
						getListPageData(planLevelString, enrollment, graphType, market, listPageData, periodID);
					}
					lastYearDate = tempNextDate;
					tempNextDate = tempNextDate.plus(Period.years(1));
				}
			}

		}
		return listPageData;
	}

	public void getListPageData(String planLevelString, EnrollmentPlanResponseDTO enrollment, String graphType, String market, List<AdminReportFormBean> listPageData, String periodID) {
		List<PlanHealth> planHealth_List = new ArrayList<PlanHealth>();
		List<PlanDental> planDental_List = new ArrayList<PlanDental>();
		Integer enrollmentCount = 0;

		Plan plan = planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
		if (plan != null) {
			if (plan.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.HEALTH)) {
				planHealth_List = iPlanHealthRepository.getPlanHealthInfoList(plan.getId(), planLevelString.toUpperCase());
				if(LOGGER.isDebugEnabled()){
				LOGGER.debug("planHealth_List: " + SecurityUtil.sanitizeForLogging(String.valueOf(planHealth_List)));
				}

				if (planHealth_List != null && planHealth_List.size() > 0) {
					for (PlanHealth planHealth : planHealth_List) {
						if (planHealth.getPlanLevel().equals(planLevelString.toUpperCase())) {
							String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
							LocalDate enrollmentDate = new LocalDate(requiredDate);
							AdminReportFormBean adminReportFormBean = new AdminReportFormBean();

							if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(enrollmentDate.toString("MMM-yyyy"));
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
								String quaterString = getQuarterValue(enrollmentDate);
								adminReportFormBean.setTimePeriod(quaterString);
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(String.valueOf(enrollmentDate.getYear()));
							}

							Issuer issuer = issuerService.getIssuerById(plan.getIssuer().getId());
							adminReportFormBean.setIssuerName(issuer.getName());
							adminReportFormBean.setPlanName(plan.getName());
							adminReportFormBean.setPlanNumber(plan.getIssuerPlanNumber());

							adminReportFormBean.setCostSharing(plan.getPlanHealth().getCostSharing());

							adminReportFormBean.setPlanLevel(planHealth.getPlanLevel());
							adminReportFormBean.setMarket(plan.getMarket());

							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("getPlanID: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollment.getPlanId())));
							}
							enrollmentCount = planMgmtService.getEnrollmentCount(Integer.parseInt(enrollment.getPlanId()));
							adminReportFormBean.setEnrollcount(String.valueOf(enrollmentCount));
							listPageData.add(adminReportFormBean);
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData)));
						}
					}
				}
			}
			}
			if (plan.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.DENTAL)) {
				planDental_List = iPlanDentalRepository.getPlanDentalInfoList(plan.getId(), planLevelString.toUpperCase());
				if(LOGGER.isDebugEnabled()){
				LOGGER.debug("planDental_List: " + SecurityUtil.sanitizeForLogging(String.valueOf(planDental_List)));
				}
				if (planDental_List != null && planDental_List.size() > 0) {
					for (PlanDental planDental : planDental_List) {
						if (planDental.getPlanLevel().equals(planLevelString.toUpperCase())) {
							String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
							if(null != requiredDate){
								LocalDate enrollmentDate = new LocalDate(requiredDate);
								AdminReportFormBean adminReportFormBean = new AdminReportFormBean();
	
								if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
									adminReportFormBean.setTimePeriod(enrollmentDate.toString("MMM-yyyy"));
								} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
									String quaterString = getQuarterValue(enrollmentDate);
									adminReportFormBean.setTimePeriod(quaterString);
								} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
									adminReportFormBean.setTimePeriod(String.valueOf(enrollmentDate.getYear()));
								}

								Issuer issuer = issuerService.getIssuerById(plan.getIssuer().getId());
								adminReportFormBean.setIssuerName(issuer.getName());
								adminReportFormBean.setPlanName(plan.getName());
								adminReportFormBean.setPlanNumber(plan.getIssuerPlanNumber());
								adminReportFormBean.setPlanLevel(planDental.getPlanLevel());
								adminReportFormBean.setMarket(plan.getMarket());
								enrollmentCount = planMgmtService.getEnrollmentCount(Integer.parseInt(enrollment.getPlanId()));
								adminReportFormBean.setEnrollcount(String.valueOf(enrollmentCount));
								listPageData.add(adminReportFormBean);
								if(LOGGER.isDebugEnabled()){
								LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData)));
							}
						}
					}
				}
			}
		}
	}
	}

	public void removeDuplicateInfo(List<AdminReportFormBean> listPageData) {
		for (int i = 0; i < listPageData.size(); i++) {
			String timePeriod = listPageData.get(i).getTimePeriod();
			String level = listPageData.get(i).getPlanLevel();
			String market = listPageData.get(i).getMarket();
			String name = listPageData.get(i).getPlanName();
			for (int j = i + 1; j < listPageData.size(); j++) {
				if (timePeriod.equals(listPageData.get(j).getTimePeriod())
						&& level.equals(listPageData.get(j).getPlanLevel())
						&& market.equals(listPageData.get(j).getMarket())
						&& name.equalsIgnoreCase(listPageData.get(j).getPlanName())) {
					listPageData.remove(j--);
				}
			}
		}
	}

	public Integer getEnrollmentCountForOneEnrollment(AdminReportFormBean adminReportFormBean, List<AdminReportFormBean> formBeans) {
		Integer count = 0;
		for (int k = 0; k < formBeans.size(); k++) {
			if (adminReportFormBean.getTimePeriod().contains(formBeans.get(k).getTimePeriod())
					&& adminReportFormBean.getPlanName().equalsIgnoreCase(formBeans.get(k).getPlanName())) {
				count++;
			}
		}
		return count;
	}

	public String getQuarterValue(LocalDate enrollmentDate) {
		String quaterString = null;
		if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q1_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q1_END) {
			quaterString = "Q1";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q2_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q2_END) {
			quaterString = "Q2";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q3_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q3_END) {
			quaterString = "Q3";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q4_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q4_END) {
			quaterString = "Q4";
		}
		return quaterString;
	}

	private List<String> returnResultantList(List<String> resultant, List<String> temp_resultant) {
		Integer fval = 0;
		Integer sval = 0;
		Integer sum = 0;
		List<String> final_resultList = new ArrayList<String>();
		for (int i = 0; i < resultant.size(); i++) {
			if (resultant.get(i) != null && !(resultant.get(i).isEmpty())) {
				fval = Integer.parseInt(resultant.get(i));
			}
			if (temp_resultant.get(i) != null
					&& !(temp_resultant.get(i).isEmpty())) {
				sval = Integer.parseInt(temp_resultant.get(i));
			}
			if (fval != 0 && sval != 0) {
				sum = fval + sval;
				final_resultList.add(String.valueOf(sum).trim());
				sum = 0;
				fval = 0;
				sval = 0;
			} else if (fval != 0 && sval == 0) {
				final_resultList.add(String.valueOf(fval).trim());
				sum = 0;
				fval = 0;
			} else if (fval == 0 && sval != 0) {
				final_resultList.add(String.valueOf(sval).trim());
				sum = 0;
				sval = 0;
			} else {
				final_resultList.add(String.valueOf("").trim());
			}
		}
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("final_resultList: " + SecurityUtil.sanitizeForLogging(String.valueOf(final_resultList)));
		}
		return final_resultList;
	}

	@SuppressWarnings("unused")
	private AdminReportFormBean createDummyAdminReportFormBean() {
		AdminReportFormBean adminReportFormBean = new AdminReportFormBean();
		adminReportFormBean.setCostSharing("CS1");
		adminReportFormBean.setEnrollcount("EnrlCount");
		adminReportFormBean.setEnrollmentCount("EnrlmtCount");
		adminReportFormBean.setIssuerName("IssuerOne");
		adminReportFormBean.setMarket("Market");
		adminReportFormBean.setPlanLevel("Platinum");
		adminReportFormBean.setPlanName(PlanMgmtConstants.PLAN_NAME);
		adminReportFormBean.setPlanNumber("PLN007");
		adminReportFormBean.setTimePeriod("MONTHLY_PERIOD");

		return adminReportFormBean;
	}

}
