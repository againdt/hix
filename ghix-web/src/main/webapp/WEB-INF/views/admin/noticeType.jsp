<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ page import="com.getinsured.hix.platform.notification.CKEditorConfigHelper" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<div class="gutter10">
<div class="row-fluid">
	<c:choose>
					<c:when test="${Mode =='Add'}">
					<h1><spring:message  code="label.AddNoticeTypeInfo"/></h1> 
					</c:when>
					  <c:otherwise>
                     <h1><spring:message  code="label.EditNoticeTypeInfo"/></h1> 
                     </c:otherwise>
     </c:choose>
</div>

<div class="row-fluid">
	<div class="span3" id="sidebar">        
	</div>
	<div class="span9" id="rightpanel">
	<div class="header">
        <h4 class="span9">Notification Details</h4>
        <a class="btn btn-small" href="<c:url value="/admin/noticetypelist"/>"><spring:message  code="label.cancel"/></a> 
		<a id="save-user" class="btn btn-primary btn-small" ><spring:message  code="label.save"/></a>
    </div>
	<div class="row-fluid">
	<form class="form-horizontal gutter10" id= "frmEditNoticeType" name="frmEditNoticeType" action="<c:url value="/admin/noticetype"/>"   method="post">
	 <input type="hidden" name="id" id="id" value="${id}">
			<df:csrfToken/>
					<div class="control-group">
						<label for="notificationName" class="control-label required"><spring:message  code="label.notificationName"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="notificationName" id="notificationName" value="${noticeType.notificationName}" class="xlarge" size="60">
							<input type="hidden" id="notificationName_hidden" name="notificationName_hidden" value="${noticeType.notificationName}">
							<div id="notificationName_error"></div>
						</div>
						
						
					</div>
				
					<div class="control-group">
						<label for="language" class="control-label"><spring:message  code="label.languageClass"/></label>
						<div class="controls">
						<input type="hidden" id="language_hidden" name="language_hidden" value="${noticeType.language}">
							<select id="language" name="language" class="input-medium">
								<option value="">SELECT</option>
								<c:forEach var="language" items="${languageList}">
									<option <c:if test="${language == noticeType.language}"> SELECTED </c:if> value="${language}">${language.language}</option>
								</c:forEach>
								
							</select>
							
							<div id="language_error"></div>	
						</div>	
										
					</div>
					<!-- end of control-group -->
					
					<div class="control-group">
						<label for="user" class="control-label"><spring:message  code="label.user"/></label>
						<div class="controls">
							<select id="user" name="user" class="input-medium">
								<option value="">SELECT</option>
								<c:forEach var="users" items="${usersList}">
									<option <c:if test="${users == noticeType.user}"> SELECTED </c:if> value="${users}">${users.roleName}</option>
								</c:forEach>
								
							</select>
							<div id="user_error"></div>	
						</div>	
										
					</div>
					<!-- end of control-group -->
					
					
					<div class="control-group">
						<label for="type" class="control-label"><spring:message  code="label.type"/></label>
						<div class="controls">
							<select id="type" name="type" class="input-medium">
								<option value="">SELECT</option>
								<c:forEach var="types" items="${typeStatusList}">
									<option <c:if test="${types == noticeType.type}"> SELECTED </c:if> value="${types}">${types.code}</option>
								</c:forEach>
								
							</select>
							<div id="type_error"></div>	
						</div>	
										
					</div>
					<!-- end of control-group --> 
					
					<div class="control-group">
						<label for="method" class="control-label"><spring:message  code="label.method"/></label>
						<div class="controls">
							<select id="method" name="method" class="input-medium">
								<option value="">SELECT</option>
								<c:forEach var="methods" items="${methodsList}">
									<option <c:if test="${methods == noticeType.method}"> SELECTED </c:if> value="${methods}">${methods.method}</option>
								</c:forEach>
								
							</select>
							<div id="method_error"></div>	
						</div>	
										
					</div>
					<!-- end of control-group --> 
				
					<div class="control-group nmhide">
						<label for="externalId" class="control-label required"><spring:message  code="label.externalId"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="externalId" id="externalId" value="${noticeType.externalId}" class="xlarge pdf_txtbox" size="60">
							<div id="externalId_error"></div>
							<!-- <div id="external_template_err" class="error span10"><label for="templateLocationlabel" generated="true" class="error"><span> <em class="excl">!</em>Please enter either External Id or Template location.</span></label></div> -->
						</div>
					</div>
					
					<div class="control-group">
						<label for="templateLocation" class="control-label required"><spring:message  code="label.templatePath"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="templateLocation" id="templateLocation" value="${noticeType.templateLocation}" class="xlarge" size="60">
							<div id="templateLocation_error"></div>
							<div id="templateLocation_valid" style="display:none;"><label class="error"><span> <em class="excl">!</em><spring:message  code="label.validateTemplateLocationForExtension"/></span></label></div>
							
						</div>	
										
					</div>
					<div class="control-group">
						<label for="emailFrom" class="control-label required"><spring:message  code="label.emailFrom"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="emailFrom" id="emailFrom" value="${noticeType.emailFrom}" class="xlarge email_txtbox" size="60">
							<div class="error help-inline" id="emailFrom_error"></div>
							<div id="emailFrom_valid" style="display:none;"><label class="error"><span> <em class="excl">!</em><spring:message  code="label.validateEmailAddress"/></span></label></div>
						</div>	
											
					</div>
							<div class="control-group">
						<label for="emailTo" class="control-label required"><spring:message  code="label.emailTo"/> </label>
						<div class="controls">
							<input type="text" name="emailTo" id="emailTo" value="${noticeType.emailTo}" class="xlarge email_txtbox" size="60">
							<div class="error help-inline" id="emailTo_error"></div>
							<div id="emailTo_valid" style="display:none;"><label class="error"><span> <em class="excl">!</em><spring:message  code="label.validateEmailAddress"/></span></label></div>
						</div>	
											
					</div>
					<div class="control-group">
						<label for="emailSubject" class="control-label required"><spring:message  code="label.emailSubject"/></label>
						<div class="controls">
							<input type="text" name="emailSubject" id="emailSubject" value="${noticeType.emailSubject}" class="xlarge email_txtbox" size="60">
						</div>	
                        										
					</div>
					
					 <div class="control-group">
                        <label for="effectiveDate" class="required control-label"><spring:message  code="label.effectiveDate"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input title="MM/dd/yyyy" class="datepick input-small"  name="effectiveDate" id="effectiveDate"  value="${effectiveDate}" />
                                <div id="effectiveDate_error" class="help-inline"></div>
                            </div> <!-- end of controls-->
                    </div>
                    <div class="control-group">
                        <label for="terminationDate" class="required control-label"><spring:message  code="label.terminationDate"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input title="MM/dd/yyyy" class="datepick input-small" name="terminationDate" id="terminationDate"  value="${terminationDate}"/>
                                <div id="terminationDate_error" class="help-inline"></div>
                            </div> <!-- end of controls-->
                    </div>
					
		</form>
	</div>
	<div class="notes" style="display:none">
		<div class="row">
			<div class="span">

		      <p>If the Employer chooses to authorize an employee or a Navigator/Broker, 
		      a new user account is being created for this representative.  
		      The Employer may remove any Authorized Representatives using the Account 
		      Administration page accessible from the Employer Dashboard.
		      </p>
		    </div>
	    </div>
	</div>	
</div>
</div>
</div>
<input type="hidden" name="notificationNameCheck" id="notificationNameCheck" value="" />

<div id="dialog-confirm" >
  <p> Changes will be effective from today.</p>
</div>
<script type="text/javascript">





$(function() {
	
	$('.datepick').each(function() {
		var imgpath = "<gi:cdnurl value='/resources/images/calendar.gif' />";
		$(this).datepicker({
			minDate: '0',
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true,
			option : $.datepicker.regional["en-GB"],
			onClose:function(){
				$('#effectiveDate_error').hide();
				
			}
		});
	});
	
	
	 var str = "";
	  $("#type option:selected").each(function () {
	            str = $(this).val();
	            
	             if(str == "EMAIL"){
	            	
	            	$('#emailFrom').addClass('required');
	            	$('#templateLocation').addClass('required');
	            	$('.pdf_txtbox').attr('disabled','true').val('');
	            	$('.email_txtbox').removeAttr('disabled');
	            }
	            else if(str == "PDF"){
	            	$('.pdf_txtbox').removeAttr('disabled');
	            	$('.email_txtbox').attr('disabled','true').val('');
	            	$('#emailFrom_valid').hide();
	            	var noticeEngineType='${noticeEngineType}'	;
	            	if(noticeEngineType=='ADOBE_LIVECYCLE')
	            		{
	            		$('#externalId').addClass('required');
	            		$('#templateLocation').attr('disabled','true').val('');
	            		}
	            	else
	            		{
	            		$('#templateLocation').addClass('required');
	            		$('#externalId').attr('disabled','true').val('');
	            		
	            		}
	            	
	            }
	            else{
	            	
	            	$('#externalId').removeClass('required');
	            	$('#templateLocation').removeClass('required');
	            	$('#emailFrom').removeClass('required');
	            	$('.email_txtbox, .pdf_txtbox').removeAttr('disabled');
	            }
	             
	             var noticeEngineType='${noticeEngineType}'	;
	             if(noticeEngineType=='DEFAULT'){
	            		$('#externalId').attr('disabled','true').val('');
	             }
	  });
	
	
	
	
});


$( "#save-user" ).click(function() {
	var todayDate = currentDate();
	var effectiveDate = $('#effectiveDate').val();
	if(($('#effectiveDate').val() !="")&&(effectiveDate >= todayDate)){
		editNameCheck();
	
		if(!($('#notificationName_error .error').is(':visible')) && !($('#emailFrom_valid .error').is(':visible'))){
			submitForm();
		}
	}
	else{
		$( "#dialog-confirm" ).dialog( "open" );
	}
	
	
});

$( "#dialog-confirm" ).dialog({
	autoOpen: false,
    resizable: false,
    height:120,
    modal: true,
    draggable: false,
    buttons: {
        "OK": function() {
        	var todayDate = currentDate();
        	var effectiveDate = $('#effectiveDate').val();
        	if(effectiveDate < todayDate){
        		$('#effectiveDate').val(todayDate);
        	}
        	editNameCheck();			
          	$( this ).dialog( "close" );
          	submitForm();
        }
      },
      dialogClass: "hide_close_btn"
});
function submitForm(){
	var ext = $('#templateLocation').val().split('.').pop().toLowerCase();
	
	var noticeEngineType='${noticeEngineType}';
	if(noticeEngineType=='ADOBE_LIVECYCLE'){
		if ($('#type').val() == 'PDF'){
			$('#templateLocation_valid, #templateLocation_valid label').hide();
			$("#frmEditNoticeType").submit();
		}else{
			if($.inArray(ext, ['html']) == -1) {	
		    	
			    $('#templateLocation_valid, #templateLocation_valid label').show();
			}
			else {
			
				$('#templateLocation_valid, #templateLocation_valid label').hide();
				$("#frmEditNoticeType").submit(); 
			}
		}
	}else{
		if($.inArray(ext, ['html']) == -1) {	
	    	
		    $('#templateLocation_valid, #templateLocation_valid label').show();
		}
		else {
		
			$('#templateLocation_valid, #templateLocation_valid label').hide();
			$("#frmEditNoticeType").submit(); 
		}
	}	
	
} 


jQuery.validator.addMethod("notificationNameCheck", function(value, element, param) {
	return $("#notificationNameCheck").val() == 0 ? false : true;

});


function checkNameAndLanguage(){
	
	if( ($("#language").val()!="") && ($("#notificationName").val()!=""))
	{
		
		if (($('#notificationName_hidden').val() == $('#notificationName').val()) && ($("#language").val()==$('#language_hidden').val()))
		{
			editNameCheck();
			$('#notificationName_error').hide();
		}
		else
		{
			$.get('noticetype/checkIfExistNameAndLanguage',
					{notificationName: $("#notificationName").val(),
				     language: $("#language").val()},
					function(response){
				    	 $('#notificationName_error').show();
						if(response == "EXIST"){
							$("#notificationNameCheck").val(0);
							$('.blur_error, #notificationName_error .error').remove();
							$("#notificationName_error").append("<label class='error blur_error'><span> <em class='excl'>!</em>Name already exist.</span></label>");							
						}else{
							$("#notificationNameCheck").val(1);
							$("#notificationName_error").html("");
						}
					}
				);
		}
	}
	
}


var validator = $("#frmEditNoticeType").validate({ 
	rules : {
		    notificationName : { required : true ,notificationNameCheck : true},
		    user : { required : true},
		    type : { required : true},
		    method : { required : true},
		    effectiveDate:{required : true, date: true, checkDuration : true, effectiveStartDateCheck : true },
		    terminationDate:{ date: true, checkDuration : true, effectiveEndDateCheck : true},
		 
		    language : { required : true}
		    
		    
	},
	messages : {
		notificationName : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateName' javaScriptEscape='true'/></span>" ,
					notificationNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateNameAndLanguageCheck' javaScriptEscape='true'/></span>"},
		type: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateType' javaScriptEscape='true'/></span>"},
		user: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateUser' javaScriptEscape='true'/></span>"},
		method: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateMethod' javaScriptEscape='true'/></span>"},
		templateLocation: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTemplateLocation' javaScriptEscape='true'/></span>"},
		externalId: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateExternalId' javaScriptEscape='true'/></span>"},
	    emailFrom: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailFrom' javaScriptEscape='true'/></span>"},
	    language: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLanguageClass' javaScriptEscape='true'/></span>"},
	    
	    'effectiveDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveStartDate' javaScriptEscape='true'/></span>",
			effectiveStartDateCheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateStartDateVal' javaScriptEscape='true'/></span>" ,
			checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationStart' javaScriptEscape='true'/></span>",
			date : "<span> <em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>"				
		},
		'terminationDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveEndDate' javaScriptEscape='true'/></span>",
			effectiveEndDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEndDateVal' javaScriptEscape='true'/></span>", 
			checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationEnd' javaScriptEscape='true'/></span>",
			date : "<span><em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>"
	 	}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	},
	success:function(error,element){
		/*hides the error message on a valid input*/
		var elementId = $(element).attr('id');
		$('#'+elementId).parent().find('#'+elementId+'_error').hide();
	}
});

/* $('#submit').click(function(e){
	
	CKEDITOR.instances.template.updateElement();
    var textData = CKEDITOR.instances.template.getData();
    
    if(textData.length>0) { 
    	 $("#template_error").attr('class','');
    	 $("#template_error").html('');
    	return true; 
    }
    
    $("#frmEditNoticeType").validate().form();
    
    $("#template_error").attr('class','error span10');
    $("#template_error").html("<label class='error' for='template' generated='true'><span> <em class='excl'>!</em><spring:message code='label.validateTemplate' javaScriptEscape='true'/></span></label>");
    
    
    return false;
    
}); */


function getCurrentDate(){
	var currentDate = new Date();
    var year = currentDate.getFullYear();
    var currMonth = currentDate.getMonth()+1;
    var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
    var day = currentDate.getDate();

    var today = month + '/' + day + '/' + year ;
    
    return today;
 }
 
jQuery.validator.addMethod("effectiveStartDateCheck", function (value, element, param) {
	
    var effectiveStartDate = $("#effectiveDate").val();
    var today = getCurrentDate() ;
    if (new Date(effectiveStartDate) < new Date(today)) {
        return false;
    }
    return true;
});


jQuery.validator.addMethod("effectiveEndDateCheck", function (value, element, param) {
	var effectiveEndDate = $("#terminationDate").val();
    var today = getCurrentDate();
    if (new Date(effectiveEndDate) <= new Date(today)) {
        return false;
    }
    return true;
});

jQuery.validator.addMethod("checkDuration", function (value, element, param) {
	 var effectiveStartDate = new Date($("#effectiveDate").val());
	 var effectiveEndDate = new Date($("#terminationDate").val());
	 
	 if(new Date(effectiveStartDate) >= new Date(effectiveEndDate)){
		 return false;
	 }else{
		 //return true;
	}
	 return true;
	 
});



$("#type").change(function () {
	  var str = "";
	  $("#type option:selected").each(function () {
	            str = $(this).val();
	            
	             if(str == "EMAIL"){
		            	
	            	$('#emailFrom').addClass('required');
	            	$('#templateLocation').addClass('required');
	            	$('.pdf_txtbox').attr('disabled','true').val('');
	            	$('.email_txtbox, #templateLocation').removeAttr('disabled');
	            }
	            else if(str == "PDF"){
	            	$('.pdf_txtbox').removeAttr('disabled');
	            	$('.email_txtbox').attr('disabled','true').val('');
	            	$('#emailFrom_valid').hide();
	            	var noticeEngineType='${noticeEngineType}'	;
	            	if(noticeEngineType=='ADOBE_LIVECYCLE')
	            		{
	            		$('#externalId').addClass('required');
	            		$('#templateLocation').attr('disabled','true').val('');
	            		}
	            	else
	            		{
	            		$('#templateLocation').addClass('required');
	            		$('#externalId').attr('disabled','true').val('');
	            		
	            		}
	            	
	            }
	            else{
	            	$('#externalId').removeClass('required');
	            	$('#templateLocation').removeClass('required');
	            	$('#emailFrom').removeClass('required');
	            	$('.email_txtbox, .pdf_txtbox').removeAttr('disabled');
	            }
	             
	             var noticeEngineType='${noticeEngineType}'	;
	             if(noticeEngineType=='DEFAULT'){
	            		$('#externalId').attr('disabled','true').val('');
	             }
	  });
});

/* $("#externalId, #templateLocation").change(function () {
	$('#external_template_err').hide();
}); */


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
	    return true;
	}
	else {
	    return false;
	}
}

$('#emailFrom').blur(function() {
    var sEmail = $('#emailFrom').val();
    if ($.trim(sEmail).length == 0) {
    	$('#emailFrom_valid, #emailFrom_valid label').show();
        //e.preventDefault();
    }
    if (validateEmail(sEmail)) {
    	$('#emailFrom_valid, #emailFrom_valid label').hide();
    }
    else {
    	$('#emailFrom_valid, #emailFrom_valid label').show();
      //  e.preventDefault();
    }
});

$('#notificationName, #language').blur(function(){
	checkNameAndLanguage();
});

$('#notificationName').keyup(function(){
	editNameCheck();
	$('.blur_error').remove();
});
function editNameCheck(){
	var settings = $("#frmEditNoticeType").validate().settings;
	for (var i in settings.rules){
	   delete settings.rules[i].notificationNameCheck;
	}
}

function currentDate(){
	var currentDate = new Date();
	var month = currentDate.getMonth()+1;
	var day = currentDate.getDate();

	var todayDate =	((''+month).length<2 ? '0' : '') + month + '/' +
					((''+day).length<2 ? '0' : '') + day + '/'+
					currentDate.getFullYear();
	 return todayDate;
}

</script>
