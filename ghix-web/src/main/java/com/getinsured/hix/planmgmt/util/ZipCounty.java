package com.getinsured.hix.planmgmt.util;

public class ZipCounty {
	
	private String zipCode;
	private String county;
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	

}
