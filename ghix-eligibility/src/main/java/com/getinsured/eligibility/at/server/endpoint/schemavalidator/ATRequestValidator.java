package com.getinsured.eligibility.at.server.endpoint.schemavalidator;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

@Component
public class ATRequestValidator {

	private static final String INSIDE_ACCOUNT_TRANSFER_DATA_VALIDATION = "inside ATRequestValidator ...";

	private static final Logger LOGGER = Logger.getLogger(ATRequestValidator.class);

	@Autowired private SchematronValidator schematronValidator;

	public List<String> validate(AccountTransferRequestPayloadType request, String inputXml) {

		LOGGER.info(INSIDE_ACCOUNT_TRANSFER_DATA_VALIDATION);

		return schematronValidator.runSchematronValidation(inputXml);

	}

}
