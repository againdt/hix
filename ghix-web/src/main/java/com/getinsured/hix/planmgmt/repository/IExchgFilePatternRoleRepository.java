package com.getinsured.hix.planmgmt.repository;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.getinsured.hix.model.ExchgFilePatternRole;

public interface IExchgFilePatternRoleRepository extends JpaRepository<ExchgFilePatternRole, Integer> {
	@Query("SELECT exchg.name FROM ExchgFilePatternRole exchg")
	List<String> findAllNames();
}

