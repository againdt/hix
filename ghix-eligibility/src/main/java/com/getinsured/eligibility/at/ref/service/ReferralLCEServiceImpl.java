package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralLCEService")
@Scope("singleton")
public class ReferralLCEServiceImpl implements ReferralLCEService {

	private static final Logger LOGGER = Logger.getLogger(ReferralLCEServiceImpl.class);

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Override
	public int executeLCE(long referralActivationId) {
		LOGGER.debug("executeLCE starts " + referralActivationId);
		return checkLCEStatus(referralActivationId);
	}

	private int checkLCEStatus(long referralActivationId) {
		int result = ReferralConstants.ERROR;
		final ReferralActivation referralActivation = referralActivationRepository.findOne(referralActivationId);

		if (referralActivation == null) {
			LOGGER.warn(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
			return result;
		}

		if (!referralActivation.isRidpPending() && !referralActivation.isLcePending()) {
			LOGGER.warn(ReferralConstants.WORKFLOW_STATUS_NOT_VALID);
			result = ReferralConstants.STATUS_NOT_VALID;
			return result;
		}

		if (ReferralConstants.IS_LCE.equals(referralActivation.getIsLce())) {
			if (!referralActivation.isLcePending()) {
				referralActivation.setWorkflowStatus(ReferralActivationEnum.LCE_PENDING);
				referralActivation.setLastUpdatedOn(new TSDate());
				referralActivationRepository.save(referralActivation);
			}
			result = ReferralConstants.LCE_FLOW;
		} else {
			result = ReferralConstants.SUCCESS;
		}
		LOGGER.debug("executeLCE ends with status " + result);
		return result;
	}
	
	@Override
	public int processLce(LceActivityDTO lceActivityDTO, boolean openEnrollment) {
		if (validLifeChange(lceActivityDTO)) {
			createSpecialEnrollmentEvent(lceActivityDTO);
			return ReferralConstants.SUCCESS;
		} else {
			if (openEnrollment) {
				updateReferralStatus(lceActivityDTO.getReferralActivationId());
				return ReferralConstants.INVALID_LCE;
			} else {
				return ReferralConstants.OE_CLOSED;
			}
		}
	}

	private void updateReferralStatus(long referralActivationId) {
		LOGGER.debug("updateReferralStatus here");
	}

	private void createSpecialEnrollmentEvent(LceActivityDTO lceActivityDTO) {
		LOGGER.debug("createSpecialEnrollmentEvent here");
	}

	
	//TODO
	//need to change this condition
	private boolean validLifeChange(LceActivityDTO lceActivityDTO) {
		return false ;
	}
}
