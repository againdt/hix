package com.getinsured.eligibility.prescreen.calculator;

import org.springframework.stereotype.Component;

/**
 * This class has the MAGI calculator implementation. MAGI calculation depends
 * on the Household, particularly, the family size of the household, number of
 * dependents and the types of income.
 * 
 * <p>
 * The following are the rules as per the specifications on April 17 2013.
 * <ol>
 * 
 * <li>Do not count the incomes of individual dependents when their total income
 * from Job and Self Employment income is below $5950 for 2014. This rule only
 * applies to dependents and not spouses.</li>
 * 
 * <li>There is no cap on deductions for alimony paid.</li>
 * 
 * <li>There is a $2500 limit per return on deductions for payments toward
 * student loan interest</li>
 * 
 * </ol>
 * </p>
 * 
 * @author Sunil Desu
 * @since April 17 2013
 * 
 */
@Component
public class MagiProductCalculator {

	/*private static Logger LOGGER = Logger.getLogger(MagiProductCalculator.class);

	private static final String CHILD = "child";
	private static final String RELATIVE = "relative";

	private static final String INVALID_HOUSEHOLD_PARAM = "Invalid PrescreenHousehold object passed to MAGI calculator.";
	private static final String INVALID_DEDUCTIONS_PARAM = "Invalid PrescreenDeductions object passed to MAGI calculator.";

	private PrescreenHousehold prescreenHousehold = null;
	private List<PrescreenHouseholdMember> householdMembers = null;
	private List<PrescreenIncome> prescreenIncomes = null;
	private PrescreenDeductions prescreenDeductions = null;
	private double magiValue = 0.0;

	private static double STUDENT_LOAN_CAP = 2500.0;
	private static double DEPENDENT_JOB_AND_SELF_EMP_INCOME_SUM_LIMIT = 5950.0;

	*//**
	 * This is the implementation of the MAGI calculation for a given household
	 * and the corresponding tax deductions that are applicable.
	 * 
	 * @author Sunil Desu
	 * @since April 17 2013
	 * 
	 * @param prescreenHousehold
	 * @param prescreenDeductions
	 * @return magiValue which is the calculated MAGI income value
	 *//*
	public double calculateMagi(PrescreenDTO prescreenDTO) {

		magiValue = 0.0;

		try {
			if (prescreenDTO != null
					&& prescreenDTO.getPrescreenProfile() != null
					&& !prescreenDTO.getPrescreenProfile()
							.getIsIncomePageLatest()) {
				magiValue = prescreenDTO.getPrescreenProfile()
						.getTaxHouseholdIncome();

				if (prescreenDTO.getPrescreenDeductions() != null) {
					prescreenDeductions = prescreenDTO
							.getPrescreenDeductions();
					magiValue -= (prescreenDeductions.getAlimonyPaid()
							+ (prescreenDeductions.getStudentLoans() > STUDENT_LOAN_CAP ? STUDENT_LOAN_CAP
							: prescreenDeductions.getStudentLoans()));
				}

			} else {
				prescreenHousehold = prescreenDTO.getPrescreenHousehold();
				if (prescreenHousehold == null
						|| prescreenHousehold.getHouseholdMembers() == null
						|| prescreenHousehold.getHouseholdMembers().size() == 0) {
					throw new InvalidParameterException(INVALID_HOUSEHOLD_PARAM);
				}

				prescreenIncomes = prescreenDTO.getPrescreenIncomes();
				if (prescreenIncomes == null) {
					throw new InvalidParameterException(
							INVALID_DEDUCTIONS_PARAM);
				}

				prescreenDeductions = prescreenDTO.getPrescreenDeductions();
				if (prescreenDeductions == null) {
					throw new InvalidParameterException(
							INVALID_DEDUCTIONS_PARAM);
				}

				householdMembers = prescreenHousehold.getHouseholdMembers();

				for (PrescreenIncome prescreenIncome : prescreenIncomes) {

					for (PrescreenHouseholdMember householdMember : householdMembers) {
						if (prescreenIncome.getMemberDesc().equalsIgnoreCase(
								householdMember.getmemberDesc())) {

							if (householdMember.getRelationshipWithClaimer() != null
									&& (householdMember
											.getRelationshipWithClaimer()
											.equalsIgnoreCase(CHILD) || householdMember
											.getRelationshipWithClaimer()
											.equalsIgnoreCase(RELATIVE))) {
								if ((prescreenIncome.getJobIncome() + prescreenIncome
										.getSelfEmployment()) > DEPENDENT_JOB_AND_SELF_EMP_INCOME_SUM_LIMIT) {

									magiValue += prescreenIncome
											.getTotalIncome();

								} else {

									magiValue += prescreenIncome
											.getTotalIncome()
											- prescreenIncome.getJobIncome()
											- prescreenIncome
													.getSelfEmployment();

								}
							} else {

								magiValue += prescreenIncome.getTotalIncome();

							}

						}
					}
				}
				magiValue -= (prescreenDeductions.getAlimonyPaid()
						+ (prescreenDeductions.getStudentLoans() > STUDENT_LOAN_CAP ? STUDENT_LOAN_CAP
						: prescreenDeductions.getStudentLoans()));
			}
		} catch (InvalidParameterException ipe) {

			LOGGER.info("Invalid arguments passed to MAGI calculator. "
					+ ipe.getMessage());

		} catch (Exception ex) {

			LOGGER.error("Exception occurred while calculating MAGI."
					+ ex.getMessage());
		}
		LOGGER.info("MAGI value returned by calculator is " + magiValue);
		return magiValue;
	}*/
}
