package com.getinsured.eligibility.renewal.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.RenewalApplicantService;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicantService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.renewal.dto.ToConsiderDataDTO;
import com.getinsured.eligibility.renewal.service.SsapToConsiderService;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentByMemberListRequestDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMembersDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.RenewalApplicant;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.RenewalApplicantRepository;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.timeshift.util.TSDate;


@Service
public class SsapToConsiderServiceImpl implements SsapToConsiderService,RenewalConstants {
	
	@Autowired
	private RenewalApplicationRepository renewalApplicationRepository;

	@Autowired
	private RenewalApplicantRepository renewalApplicantRepository;
	
	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	
	@Autowired
	private SsapCloneApplicantService ssapCloneApplicantService;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	private ActiveEnrollmentService activeEnrollmentService;
	
	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired
	private GIMonitorService giMonitorService;
	
	@Autowired
	private EligibilityProgramRepository eligibilityProgramRepository;
	
	@Autowired
	private RenewalApplicationService renewalApplicationService;
	
	@Autowired
	private RenewalApplicantService renewalApplicantService;
	
	@Autowired
	private LookupService lookupService;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	
	private static final RenewalStatus TO_CONSIDER = RenewalStatus.TO_CONSIDER;
	private static final RenewalStatus NOT_TO_CONSIDER = RenewalStatus.NOT_TO_CONSIDER;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapToConsiderServiceImpl.class);
	
	@Override
	public boolean processErrorApplications(Long renewalYear,Long batchSize) throws Exception
	{
		boolean success = false;
		List<SsapApplication> renewalApplicationsWithErrors = ssapCloneApplicationService.getAutoRenewalApplicationsForNextCoverageYearWithErrors(renewalYear, Long.valueOf(batchSize));
		for(SsapApplication renewalApplicationWithError : renewalApplicationsWithErrors)
		{
			LOGGER.info("Started Error Processing for application id = " + renewalApplicationWithError.getId());
			renewalApplicantRepository.deleteBySsapApplicationId(renewalApplicationWithError.getId());
			renewalApplicationRepository.deleteBySsapApplicationId(renewalApplicationWithError.getId());
			success = true;
		}
		
		return success;
	}
	
	@Override
	public boolean processErrorApplications(Long ssapApplicationId) throws Exception
	{
		boolean success = false;
		LOGGER.info("Deleting renewalApplicants and renewalApplication for application id = " + ssapApplicationId);
		renewalApplicantRepository.deleteBySsapApplicationId(ssapApplicationId);
		renewalApplicationRepository.deleteBySsapApplicationId(ssapApplicationId);
		success = true;
		return success;
	}
	
	public String processSsap(SsapApplication currentApplication, Long renewalYear) throws Exception{
		String processingResult = "ERROR";
		try 
		{
			RenewalApplication ssapRenewalApplication = executeToConsiderLogicForRenewal(currentApplication, renewalYear, null);
			processingResult = "SUCCESS";
		} 
		catch (GIException e) {
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + e.getErrorCode(), new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
		} 
		catch (Exception e) {
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001", new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
			processingResult = RenewalStatus.ERROR.toString();
		}
		
		return processingResult;
	}
	
	private boolean checkAPTCEligilibilityForNewMembers(SsapApplication currentApplication,RenewalApplication ssapRenewalApplication,Map<String,SsapApplicant> atMembers)
	{
		boolean isDentalNotToConsider = false;
		if(StringUtils.contains(currentApplication.getExchangeEligibilityStatus().name(), "APTC"))
		{
			// Get renewal applicants with SSAP_APPLICATION_ID,RENEWAL_STATUS = 'NOT_TO_CONSIDER' and MEMBER_FALLOUT_REASON_CODE=NEW_MEMBER for health
			List<RenewalApplicant> newApplicants = renewalApplicantRepository.findNewMembersByPlanType(NOT_TO_CONSIDER, HEALTH, currentApplication.getId(), getReasonCode(NEW_MEMBER));
			if(CollectionUtils.isNotEmpty(newApplicants))
			{
				for(RenewalApplicant renewalApplicant : newApplicants)
				{
					SsapApplicant ssapApplicant = atMembers.get(String.valueOf(renewalApplicant.getSsapApplicantId()));
					List<EligibilityProgram> memberEligibilities = eligibilityProgramRepository.findBySsapApplicant(ssapApplicant);
					boolean exchangeEligibility = false;
					boolean aptcEligibility = false;
					for(EligibilityProgram program : memberEligibilities){
						if(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE.equalsIgnoreCase(program.getEligibilityType()) && "TRUE".equalsIgnoreCase(program.getEligibilityIndicator())){
							exchangeEligibility = true;
						}
						if(EligibilityConstants.APTC_ELIGIBILITY_TYPE.equalsIgnoreCase(program.getEligibilityType()) && "TRUE".equalsIgnoreCase(program.getEligibilityIndicator())){
							aptcEligibility = true;
						}
					}
					if( exchangeEligibility && aptcEligibility)
					{
						isDentalNotToConsider = true;
						break;
					}
				}
			}
		}
		
		return isDentalNotToConsider;
	}
	
	public RenewalApplication executeToConsiderLogicForRenewal(SsapApplication currentApplication, Long renewalYear, Integer serverName) throws Exception, GIException 
	{
		LOGGER.info("Started Processing application id = " + currentApplication.getId());
		RenewalApplication ssapRenewalApplication = populateRenewalApplication(currentApplication);
		
		try
		{
			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(currentApplication);
			Map<String,SsapApplicant> atMembers = extractATSsapApplicantById(ssapApplicants);
			// Get Enrollment details by invoking API, Get previous year by config
			Long previousCoverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
			Integer cmrHouseholdId = currentApplication.getCmrHouseoldId().intValue();
			List<EnrollmentWithMemberDataDTO> enrolledMembers = getEnrollmentDetails(currentApplication.getId(),ssapApplicants, previousCoverageYear,cmrHouseholdId);
			
			// Apply TO_CONSIDER logic here
			if(CollectionUtils.isNotEmpty(enrolledMembers))
			{
				try 
				{
					ssapRenewalApplication = processHealthEnrollment(currentApplication, enrolledMembers, ssapApplicants,ssapRenewalApplication);
					boolean processDental = true;
					
					// Y for CA and N for MN,NV,ID
					String renewDental = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.RENEW_DENTAL);
					
					if(NOT_TO_CONSIDER.equals(ssapRenewalApplication.getHealthRenewalStatus()))
					{
						if(N.equals(renewDental) && !(ExchangeEligibilityStatus.QHP.equals(currentApplication.getExchangeEligibilityStatus())))
						{
							processDental = false;
						}
					}
					
					
					if(checkIfDentalEnrollmentExists(enrolledMembers))
					{
						if(processDental)
						{
							if(N.equals(renewDental) && checkAPTCEligilibilityForNewMembers(currentApplication, ssapRenewalApplication, atMembers))
							{
								ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
								ssapRenewalApplication.setDentalReasonCode(getReasonCode(NEW_APTC_MEMBER));
								ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
							}
							else
							{
								ssapRenewalApplication = processDentalEnrollment(currentApplication,ssapRenewalApplication, enrolledMembers, ssapApplicants);
							}
						}
						else
						{
							ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
							ssapRenewalApplication.setDentalReasonCode(getReasonCode(DENTAL_NOT_CONSIDERED));
							ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
						}
					}
					else
					{
						ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
						ssapRenewalApplication.setDentalReasonCode(getReasonCode(ENROLLMENT_NOT_AVAILABLE));
						ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
					}
				} 
				catch (Exception e) {
					LOGGER.info("Exception in TO_CONSIDER for application id = " + currentApplication.getId());
					GIMonitor  gIMonitor  = giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_TO_CONSIDER", new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
					ssapRenewalApplication.setGiMonitorId(gIMonitor.getId().longValue());
					ssapRenewalApplication.setHouseholdRenewalStatus(RenewalStatus.ERROR);
					ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
					throw e;
				}
			}
			else
			{
				ssapRenewalApplication.setHouseholdRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
				ssapRenewalApplication.setHealthRenewalStatus(NOT_TO_CONSIDER);
				ssapRenewalApplication.setHealthReasonCode(getReasonCode(ENROLLMENT_NOT_AVAILABLE));
				ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
				ssapRenewalApplication.setDentalReasonCode(getReasonCode(ENROLLMENT_NOT_AVAILABLE));
				ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
				//throw new GIException("No enrollment data found");
			}
			
			LOGGER.info("Renewal Processing completed for application id = " + currentApplication.getId());
		}
		catch(GIException e)
		{
			LOGGER.error("Error in Renewal Processing for application id = " + currentApplication.getId());
			GIMonitor  gIMonitor  = giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_TO_CONSIDER", new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
			ssapRenewalApplication.setGiMonitorId(gIMonitor.getId().longValue());
			ssapRenewalApplication.setHouseholdRenewalStatus(RenewalStatus.ERROR);
			ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
			throw e;
		}
		
		saveServerName(ssapRenewalApplication, serverName);
		
		return ssapRenewalApplication;
	}
	
	private List<EnrollmentMembersDTO> extractMemberIdsForEnrollmentRequest(List<SsapApplicant> ssapApplicants)
	{
		List<EnrollmentMembersDTO> members = new ArrayList<EnrollmentMembersDTO>();
		
		if(CollectionUtils.isNotEmpty(ssapApplicants))
		{
			for( SsapApplicant applicant : ssapApplicants)
			{
				EnrollmentMembersDTO member = new EnrollmentMembersDTO();
				member.setMemberId(applicant.getApplicantGuid());
				member.setExternalMemberId(applicant.getExternalApplicantId());
				members.add(member);
			}
		}
		return members;
	}
	
	private void saveServerName(RenewalApplication renewalApplication, Integer serverName) {

		if (renewalApplication != null && serverName != null) {
			renewalApplication.setToConsiderServerName(serverName);
			renewalApplicationRepository.save(renewalApplication);
		}
	}
	
	private Map<String,SsapApplicant> extractATMembersByMemberId(List<SsapApplicant> ssapApplicants)
	{
		Map<String,SsapApplicant> atMembersGroup = new HashMap<String,SsapApplicant>();
		if(CollectionUtils.isNotEmpty(ssapApplicants))
		{
			String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
			for( SsapApplicant applicant : ssapApplicants)
			{
				atMembersGroup.put(TRUE.equalsIgnoreCase(useExternalMemberId) ? applicant.getExternalApplicantId() : applicant.getApplicantGuid(),applicant);
			}
		}
		return atMembersGroup;
	}
	
	private Map<String,SsapApplicant> extractATSsapApplicantById(List<SsapApplicant> ssapApplicants)
	{
		Map<String,SsapApplicant> atMembersGroup = new HashMap<String,SsapApplicant>();
		if(CollectionUtils.isNotEmpty(ssapApplicants))
		{
			for( SsapApplicant applicant : ssapApplicants)
			{
				atMembersGroup.put(Long.toString(applicant.getId()),applicant);
			}
		}
		return atMembersGroup;
	}
	
	private boolean checkIfDentalEnrollmentExists(List<EnrollmentWithMemberDataDTO> enrollmentData)
	{
		boolean isDentalEnrollmentExists = false;
		
		for(EnrollmentWithMemberDataDTO enrolledMemberDto : enrollmentData)
		{
			if(DENTAL.equalsIgnoreCase(enrolledMemberDto.getInsuranceType()))
			{
				isDentalEnrollmentExists = true;
			}
		}
		
		return isDentalEnrollmentExists;
	}
	
	private String removeLastTwoCharsFromPlanId(String planId) {
	    
		if (StringUtils.isNotBlank(planId)) {
	    	planId.substring(0, planId.length() - 2);
	    }
	    
		return planId;
	}
	
	
	private RenewalApplication processDentalEnrollment(SsapApplication currentApplication,RenewalApplication ssapRenewalApplication,List<EnrollmentWithMemberDataDTO> enrollmentData,List<SsapApplicant> ssapApplicants) throws Exception
	{
		String applicationId = Long.toString(currentApplication.getId());
		List<String> dentalEnrolledMembers = new ArrayList<String>();  
		Map<String,Object> dentalEnrolledMembersMap = new HashMap<String, Object>();
		Map<String,SsapApplicant> dentalATMembers = extractATMembersByMemberId(ssapApplicants);
		String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
		String planId = "";
		String insuranceType = null;
		boolean allMembersMedicaidEligible = false;
		List<RenewalApplicant> toConsiderMembers = new ArrayList<RenewalApplicant>();
		List<RenewalApplicant> medicaidEligibleMembers = new ArrayList<RenewalApplicant>();
		EnrollmentWithMemberDataDTO dentalEnrollmentMemberData = null;
		for(EnrollmentWithMemberDataDTO enrolledMemberDto : enrollmentData)
		{
			if(DENTAL.equalsIgnoreCase(enrolledMemberDto.getInsuranceType()))
			{
				insuranceType = enrolledMemberDto.getInsuranceType();
				planId = enrolledMemberDto.getPlanID();
				dentalEnrollmentMemberData = enrolledMemberDto;
				List<EnrollmentMembersDTO>  members = enrolledMemberDto.getMembers();
				for(EnrollmentMembersDTO memberDto : members)
				{
					String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? memberDto.getExternalMemberId() : memberDto.getMemberId();
					dentalEnrolledMembers.add(memberId);
					dentalEnrolledMembersMap.put(memberId, enrolledMemberDto);
				}
			}
		}
		
		
		for(EnrollmentWithMemberDataDTO enrollmentDataDTO : enrollmentData)
		{
			if(DENTAL.equalsIgnoreCase(enrollmentDataDTO.getInsuranceType()))
			{
				ToConsiderDataDTO toConsiderDataDTO = new ToConsiderDataDTO();
				toConsiderDataDTO.setDentalATMembers(dentalATMembers);
				toConsiderDataDTO.setDentalEnrolledMembersMap(dentalEnrolledMembersMap);
				toConsiderDataDTO.setApplicationId(applicationId);
				toConsiderDataDTO.setPlanId(removeLastTwoCharsFromPlanId(planId));
				
				// Check all members are matching
				// DENTAL_RENEWAL_STATUS = TO_CONSIDER in RENEWAL_APPLICATIONS & RENEWAL_APPLICANTS
				for( String memberId : dentalEnrolledMembers)
				{
					EnrollmentWithMemberDataDTO enrolledMemberDto = (EnrollmentWithMemberDataDTO) dentalEnrolledMembersMap.get(memberId);
					SsapApplicant applicant = dentalATMembers.get(memberId);
					toConsiderDataDTO.setEnrolledMemberDto(enrolledMemberDto);
					toConsiderDataDTO.setMemberId(memberId);
					RenewalApplicant ssapRenewalApplicant = checkDentalEligibility(currentApplication,applicant, toConsiderDataDTO,insuranceType);
					if(toConsiderDataDTO.isAllMembersNotToConsider())
						break;
					ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
					if(TO_CONSIDER.equals(ssapRenewalApplicant.getRenewalStatus()))
					{
						toConsiderMembers.add(ssapRenewalApplicant);
					}
					else
					{
						if(applicant != null && MEDICAID.equalsIgnoreCase(applicant.getEligibilityStatus()))
						{
							medicaidEligibleMembers.add(ssapRenewalApplicant);
						}
					}
					if(medicaidEligibleMembers.size()== dentalEnrolledMembers.size())
					{
						allMembersMedicaidEligible = true;
					}
				}
				
				if(toConsiderDataDTO.isAllMembersNotToConsider()) // This is case of removal in CA(i.e. Member is not QHP eliglible )
				{
					toConsiderMembers.clear();
					Map<String,SsapApplicant> dentalATMembersCopy = new HashMap<String, SsapApplicant>(dentalATMembers);
					// For CA All members will be marked as NOT_TO_CONSIDER
					for(String applicantGuid : dentalEnrolledMembersMap.keySet())
					{
						if(dentalATMembersCopy.containsKey(applicantGuid))
						{
							dentalATMembersCopy.remove(applicantGuid);
							
							EnrollmentWithMemberDataDTO enrolledMemberDto = (EnrollmentWithMemberDataDTO) dentalEnrolledMembersMap.get(applicantGuid);
							RenewalApplicant ssapRenewalApplicant = renewalApplicantRepository.findByMemberIdAndPlanType(applicantGuid,DENTAL,Long.valueOf(applicationId));
							if(ssapRenewalApplicant == null)
							{
								ssapRenewalApplicant = populateRenewalApplicants(applicationId,applicantGuid,enrolledMemberDto,NOT_TO_CONSIDER,DENTAL_IN_ELIGIBLE,insuranceType);
							}
							else
							{
								ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
								ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
							}

							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
						}
						else
						{
							EnrollmentWithMemberDataDTO enrolledMemberDto = (EnrollmentWithMemberDataDTO) dentalEnrolledMembersMap.get(applicantGuid);
							RenewalApplicant ssapRenewalApplicant = renewalApplicantRepository.findByMemberIdAndPlanType(applicantGuid,DENTAL,Long.valueOf(applicationId));
							if(ssapRenewalApplicant == null)
							{
								ssapRenewalApplicant = populateRenewalApplicants(applicationId,applicantGuid,enrolledMemberDto,NOT_TO_CONSIDER,NOT_IN_AT,insuranceType);
							}
							else
							{
								ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(NOT_IN_AT));
								ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
							}

							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
						}
					}
					
					if(!dentalATMembersCopy.isEmpty())
					{
						for(String applicantGuid : dentalATMembersCopy.keySet())
						{
							SsapApplicant ssapApplicant = dentalATMembersCopy.get(applicantGuid);
							RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,applicantGuid,null,NOT_TO_CONSIDER,DENTAL_IN_ELIGIBLE,insuranceType);
							ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
							ssapRenewalApplicant.setGroupReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
						}
					}
					
					ssapRenewalApplication.setDentalReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
					ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
					ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
					return ssapRenewalApplication;
				}
				
				/// New member added in AT
				for(String applicantGuId : dentalATMembers.keySet())
				{
					if(!dentalEnrolledMembersMap.containsKey(applicantGuId))
					{
						SsapApplicant applicant = dentalATMembers.get(applicantGuId);
						toConsiderDataDTO.setMemberId(applicantGuId);
						String memberSource = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_ALL_FROM_AT_OR_ENROLLMENT); // AT for ID and ENROLLMENT for MN,CA,NV
						if(AT.equals(memberSource)){
							toConsiderDataDTO.setEnrolledMemberDto(dentalEnrollmentMemberData);
						}
						else{
							toConsiderDataDTO.setEnrolledMemberDto(null); // No enrollment details populated for new member
						}
						toConsiderDataDTO.setNewAddedMember(true);
						RenewalApplicant ssapRenewalApplicant = checkDentalEligibility(currentApplication,applicant, toConsiderDataDTO,insuranceType);
						ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
					}
				}
			}
		}
		
		if(toConsiderMembers.size() > 0)
		{
			ssapRenewalApplication.setDentalRenewalStatus(TO_CONSIDER);
		}
		else
		{
			if(allMembersMedicaidEligible)
			{
				ssapRenewalApplication.setDentalReasonCode(getReasonCode(MEDICAID_ELIGIBLE_HOUSEHOLD));
				ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
			}
			else
			{
				ssapRenewalApplication.setDentalReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
				ssapRenewalApplication.setDentalRenewalStatus(NOT_TO_CONSIDER);
			}
		}
		
		ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
		
		return ssapRenewalApplication;
	}
	
	
	private RenewalApplicant checkDentalEligibility(SsapApplication currentApplication,SsapApplicant applicant,ToConsiderDataDTO toConsiderDataDTO,String insuranceType) throws Exception
	{
		String medicaidCheck = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.ALLOW_MEDICAID_MEMBERS_IN_DENTAL); // Y for CA and N for ID,MN,NV
		String considerRemainingEligMember = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_REMAINING_ELIG_MEMBER_FOR_DENTAL); // Y for ID,MN,NV and N for CA
		String applicationId = toConsiderDataDTO.getApplicationId();
		String memberId = toConsiderDataDTO.getMemberId();
		EnrollmentWithMemberDataDTO enrolledMemberDto = toConsiderDataDTO.getEnrolledMemberDto();
		RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,null,null,insuranceType);
		
		if(applicant != null && (QHP.equalsIgnoreCase(applicant.getEligibilityStatus()) || (Y.equalsIgnoreCase(medicaidCheck) && MEDICAID.equalsIgnoreCase(applicant.getEligibilityStatus()))))
		{
			// TO_CONSIDER
			toConsiderDataDTO.setToConsider(true);
			toConsiderDataDTO.setSsapApplicant(applicant);
			ssapRenewalApplicant = markMember(currentApplication,toConsiderDataDTO, ssapRenewalApplicant);
			
		}
		else if(N.equalsIgnoreCase(considerRemainingEligMember) && !toConsiderDataDTO.isNewAddedMember()) 
		{
			// For CA All members will be marked as NOT_TO_CONSIDER
			toConsiderDataDTO.setAllMembersNotToConsider(true);
		}
		else
		{
			// NOT_TO_CONSIDER
			toConsiderDataDTO.setToConsider(false);
			if(applicant == null)// Member tag is not in AT
			{
				ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(NOT_IN_AT));
				ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
			}
			else
			{
				if(MEDICAID.equalsIgnoreCase(applicant.getEligibilityStatus()))
				{
					// Member is MEDICAID eligible
					ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(MEDICAID_ELIGIBLE_MEMBER));
					ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
				}
				else
				{
					// Member is not eligible
					ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
					ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
				}
			}
		}
		return ssapRenewalApplicant;
	}
	
	private RenewalApplicant markMember(SsapApplication currentApplication,ToConsiderDataDTO toConsiderDataDTO,RenewalApplicant ssapRenewalApplicant) throws Exception
	{
		String memberId = toConsiderDataDTO.getMemberId();
		boolean isToConsider = toConsiderDataDTO.isToConsider();
		Map<String,SsapApplicant> dentalATMembers = toConsiderDataDTO.getDentalATMembers();
		Map<String,Object> dentalEnrolledMembersMap = toConsiderDataDTO.getDentalEnrolledMembersMap();
		String memberSource = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_ALL_FROM_AT_OR_ENROLLMENT); // AT for ID and ENROLLMENT for MN,CA,NV

		if(isToConsider)
		{
			if(AT.equals(memberSource))
			{
				if(dentalATMembers.containsKey(memberId))
				{
					doDentalAgeCheck(currentApplication,ssapRenewalApplicant,toConsiderDataDTO);
				}
				else
				{
					ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(NOT_IN_AT));
					ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
				}
			}
			else
			{
				if(dentalEnrolledMembersMap.containsKey(memberId))
				{
					doDentalAgeCheck(currentApplication,ssapRenewalApplicant,toConsiderDataDTO);
				}
				else
				{
					ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(NEW_MEMBER));
					ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
				}
			}
		}
		else
		{
			ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(DENTAL_IN_ELIGIBLE));
			ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
		}
		
		return ssapRenewalApplicant;
	}
	
	
	private void doDentalAgeCheck(SsapApplication currentApplication,RenewalApplicant ssapRenewalApplicant,ToConsiderDataDTO toConsiderDataDTO) throws Exception
	{
		String dentalAgeCheck = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DENTAL_AGE_OUT); // // Y for MN,NV,ID and N for CA
		if(Y.equals(dentalAgeCheck))
		{
			Map<String,Object> dentalEnrolledMembersMap = toConsiderDataDTO.getDentalEnrolledMembersMap();
			Set<String> dentalEnrolledMembers = dentalEnrolledMembersMap.keySet();
			boolean dentalAgeCheckPassed = dentalAgeCheck(currentApplication,toConsiderDataDTO.getSsapApplicant(),currentApplication.getCoverageYear(),toConsiderDataDTO.getPlanId(),dentalEnrolledMembers);
			if(dentalAgeCheckPassed)
			{
				ssapRenewalApplicant.setRenewalStatus(TO_CONSIDER);
			}
			else
			{
				ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(DENTAL_AGE_CHECK_FAILED));
				ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
			}
		}
		else
		{
			ssapRenewalApplicant.setRenewalStatus(TO_CONSIDER);
		}
	}
	
	private boolean comapreEligibility(SsapApplicant firstMember,SsapApplicant secondMember) {
		boolean status = false;
		if(firstMember!=null && secondMember!=null){
			List<EligibilityProgram> firstMemberEligibilities = eligibilityProgramRepository.findBySsapApplicant(firstMember);
			List<EligibilityProgram> secondMemberEligibilities = eligibilityProgramRepository.findBySsapApplicant(secondMember);

			if(CollectionUtils.isEmpty(firstMemberEligibilities) || CollectionUtils.isEmpty(secondMemberEligibilities)){
				return false;
			}
			
			if(checkEligibities(firstMemberEligibilities, secondMemberEligibilities, firstMember, secondMember) && checkEligibities(secondMemberEligibilities, firstMemberEligibilities, firstMember, secondMember)){
				status = true;
			}
		}
		
		return status;
	}
	
	private boolean checkEligibities(List<EligibilityProgram> firstMemberEligibilities, List<EligibilityProgram> secondMemberEligibilities, SsapApplicant firstMember,SsapApplicant secondMember){
		boolean status = true;
		for(EligibilityProgram program1:firstMemberEligibilities){
			boolean isElementFound = false;
			for(EligibilityProgram program2:secondMemberEligibilities){
				if(program1.getEligibilityType().equals(program2.getEligibilityType())){
					isElementFound = true;
					if(!program1.getEligibilityIndicator().equals(program2.getEligibilityIndicator())){
						return false;
					}
					if(EligibilityConstants.CSR_ELIGIBILITY_TYPE.equalsIgnoreCase(program1.getEligibilityType()) && "TRUE".equalsIgnoreCase(program1.getEligibilityIndicator())){
						if((firstMember.getCsrLevel()!=null && secondMember.getCsrLevel()==null) || 
								(firstMember.getCsrLevel()==null && secondMember.getCsrLevel()!=null) 
								|| (!StringUtils.equalsIgnoreCase(firstMember.getCsrLevel(), secondMember.getCsrLevel()))){
							return false;
						}
					}
					break;
				}
			}
			
			if(!isElementFound && "true".equalsIgnoreCase(program1.getEligibilityIndicator())){
				status = false;
				break;
			}
		}
		
		return status;
	}
	
	private RenewalApplication processHealthEnrollment(SsapApplication currentApplication,List<EnrollmentWithMemberDataDTO> enrolledMembersData,List<SsapApplicant> ssapApplicants,RenewalApplication ssapRenewalApplication) throws Exception
	{
		String renewInconsistentGroups = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.RENEW_INCONSISTENT_GROUP_HEALTH); // Y for MN and N for ID,CA,NV
		String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
		List<String> healthEnrolledMembers = new ArrayList<String>(); 
		Map<String,SsapApplicant> atMemberMap = extractATMembersByMemberId(ssapApplicants);
		Map<String,SsapApplicant> atApplicantsMap = extractATSsapApplicantById(ssapApplicants);
		Map<String,Object> markedMembersMap = new HashMap<String, Object>();
		String applicationId = Long.toString(currentApplication.getId());
		List<RenewalApplicant> toConsideredMembers = null; 
		List<RenewalApplicant> notToConsideredMembers = null; 
		Map<String,Object> healthEnrolledMembersMap = new HashMap<String, Object>();
		boolean isHealthEnrollmentExists = false;
		boolean isGICExists = false;
		boolean isToConsiderExists = false;
		boolean isMedicaidEligibleGroupExists = false;
		int healthGroupCount = 0;
		EnrollmentWithMemberDataDTO healthEnrollmentMemberData = null;
		String insuranceType = null;
		
		for(EnrollmentWithMemberDataDTO enrolledMemberDto : enrolledMembersData)
		{
			if(HEALTH.equalsIgnoreCase(enrolledMemberDto.getInsuranceType()))
			{
				insuranceType = enrolledMemberDto.getInsuranceType();
				isHealthEnrollmentExists = true;
				healthGroupCount++;
				healthEnrollmentMemberData = enrolledMemberDto;
				List<EnrollmentMembersDTO>  members = enrolledMemberDto.getMembers();
				for(EnrollmentMembersDTO memberDto : members)
				{
					String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? memberDto.getExternalMemberId() : memberDto.getMemberId();
					healthEnrolledMembers.add(memberId);
					healthEnrolledMembersMap.put(memberId, enrolledMemberDto);
				}
			}
		}
		
		if(isHealthEnrollmentExists)
		{
			for(EnrollmentWithMemberDataDTO enrolledMemberDto : enrolledMembersData)
			{
				if(HEALTH.equalsIgnoreCase(enrolledMemberDto.getInsuranceType()))
				{
					toConsideredMembers = new ArrayList<RenewalApplicant>();
					notToConsideredMembers = new ArrayList<RenewalApplicant>();
					List<EnrollmentMembersDTO>  members = enrolledMemberDto.getMembers();
					for(EnrollmentMembersDTO memberDto : members)
					{
						String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? memberDto.getExternalMemberId() : memberDto.getMemberId();
						if(atMemberMap.containsKey(memberId))
						{
							SsapApplicant applicant = atMemberMap.get(memberId);
							markedMembersMap.put(Long.toString(applicant.getId()), applicant);
							
							if(QHP.equalsIgnoreCase(applicant.getEligibilityStatus()))
							{
								// APPLY HEALTH_AGE_CHECK_OUT logic
								boolean healthAgeCheckPassed = healthAgeCheck(currentApplication, applicant);
								if(healthAgeCheckPassed)
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,TO_CONSIDER,null,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									toConsideredMembers.add(ssapRenewalApplicant);
								}
								else
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,NOT_TO_CONSIDER,HEALTH_AGE_CHECK_FAILED,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									notToConsideredMembers.add(ssapRenewalApplicant);
								}
							}
							else
							{
								if(MEDICAID.equalsIgnoreCase(applicant.getEligibilityStatus()))
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,NOT_TO_CONSIDER,MEDICAID_ELIGIBLE_MEMBER,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									notToConsideredMembers.add(ssapRenewalApplicant);
								}
								else
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,NOT_TO_CONSIDER,MEMBER_NOT_ELIGIBLE,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									notToConsideredMembers.add(ssapRenewalApplicant);
								}
							}
						}
						else
						{
							// mark RENWAL_STATUS as NULL as this is removal
							RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,enrolledMemberDto,NOT_TO_CONSIDER,NOT_IN_AT,insuranceType);
							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
							notToConsideredMembers.add(ssapRenewalApplicant);
						}
						
					}
					
					//Does atleast 1 member have TO_CONSIDER?
					if(toConsideredMembers.size()>0)
					{
						if(Y.equals(renewInconsistentGroups))  // Y for MN and N for ID,CA,NV
						{
							// Do not match eligibility of all members in group
							// mark RENWAL_STATUS as TO_CONSIDER for all members in toConsideredMembers list and change Reason code
							
							isToConsiderExists = true;
							for(RenewalApplicant ssapRenewalApplicant : toConsideredMembers)
							{
								ssapRenewalApplicant.setRenewalStatus(TO_CONSIDER);
								ssapRenewalApplicant.setGroupRenewalStatus(TO_CONSIDER);
								ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
							}
						}
						else
						{
							RenewalApplicant ssapRenewalApplicantFirst = toConsideredMembers.get(0);
							String memberOneId = Long.toString(ssapRenewalApplicantFirst.getSsapApplicantId());
							SsapApplicant memberOne = (SsapApplicant) markedMembersMap.get(memberOneId);
							if(toConsideredMembers.size() > 1)
							{
								boolean eligibilityMatched = false;
								for (int i = 1; i < toConsideredMembers.size(); i++) 
								{
									RenewalApplicant nextApplicant = toConsideredMembers.get(i);
									String nextMemberId = Long.toString(nextApplicant.getSsapApplicantId());
									SsapApplicant nextMember = (SsapApplicant) markedMembersMap.get(nextMemberId);
									
									eligibilityMatched = comapreEligibility(memberOne, nextMember);
									if(!eligibilityMatched)
										break; 
								}
								
								if(eligibilityMatched)
								{
									// mark RENWAL_STATUS as TO_CONSIDER for all members in toConsideredMembers list and change Reason code
									isToConsiderExists = true;
									for(RenewalApplicant ssapRenewalApplicant : toConsideredMembers)
									{
										ssapRenewalApplicant.setRenewalStatus(TO_CONSIDER);
										ssapRenewalApplicant.setGroupRenewalStatus(TO_CONSIDER);
										ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									}
									
								}
								else
								{
									isGICExists = true;
									// Since eligibility of all members in group is not matching, so all are NOT_TO_CONSIDER
									// mark RENWAL_STATUS as NOT_TO_CONSIDER for all members in toConsideredMembers list and change Reason code
									for(RenewalApplicant ssapRenewalApplicant : toConsideredMembers)
									{
										ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
										ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
										ssapRenewalApplicant.setGroupReasonCode(getReasonCode(GROUP_INCONSISTENT));
										ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									}
									
								}
							}
							else
							{
								// mark RENWAL_STATUS as TO_CONSIDER as this is only member
								isToConsiderExists = true;
								ssapRenewalApplicantFirst.setRenewalStatus(TO_CONSIDER);
								ssapRenewalApplicantFirst.setGroupRenewalStatus(TO_CONSIDER);
								ssapRenewalApplicantFirst = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicantFirst);
							}
						}
					}
					else
					{
						// Since all members in group are NOT_TO_CONSIDER
						// mark RENWAL_STATUS as NOT_TO_CONSIDER for all members in notToConsideredMembers list and change Reason code
						for(RenewalApplicant ssapRenewalApplicant : notToConsideredMembers)
						{
							if(ssapRenewalApplicant.getMemberFalloutReasonCode().equals(getReasonCode(MEDICAID_ELIGIBLE_MEMBER)))
							{
								ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
								ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
								ssapRenewalApplicant.setGroupReasonCode(getReasonCode(MEDICAID_ELIGIBLE_GROUP));
								isMedicaidEligibleGroupExists = true;
							}
							else
							{
								ssapRenewalApplicant.setRenewalStatus(NOT_TO_CONSIDER);
								ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
								ssapRenewalApplicant.setGroupReasonCode(getReasonCode(GROUP_IN_ELIGIBLE));
							}
							
							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
						}
					}
				}
			}
			
			// Identify Added members which are not marked above and mark RENWAL_STATUS as TO_CONSIDER for all Added members
			for(String applicantId : atApplicantsMap.keySet())
			{
				if(!markedMembersMap.containsKey(applicantId))
				{
					// This is added member and will not have enrollment details.
					String memberSource = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_ALL_FROM_AT_OR_ENROLLMENT); // AT for ID and ENROLLMENT for MN,CA,NV
					SsapApplicant ssapApplicant = (SsapApplicant) atApplicantsMap.get(applicantId);
					String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? ssapApplicant.getExternalApplicantId() : ssapApplicant.getApplicantGuid();
					
					if(AT.equals(memberSource))
					{
						if(healthGroupCount == 1)
						{
							if(QHP.equalsIgnoreCase(ssapApplicant.getEligibilityStatus()))
							{
								// APPLY HEALTH_AGE_CHECK_OUT logic
								boolean healthAgeCheckPassed = healthAgeCheck(currentApplication, ssapApplicant);
								if(healthAgeCheckPassed)
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,healthEnrollmentMemberData,TO_CONSIDER,NEW_MEMBER,insuranceType);
									ssapRenewalApplicant.setGroupRenewalStatus(TO_CONSIDER);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
									isToConsiderExists = true;
								}
								else
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,healthEnrollmentMemberData,NOT_TO_CONSIDER,HEALTH_AGE_CHECK_FAILED,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
								}
							}
							else
							{
								if(MEDICAID.equalsIgnoreCase(ssapApplicant.getEligibilityStatus()))
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,healthEnrollmentMemberData,NOT_TO_CONSIDER,MEDICAID_ELIGIBLE_MEMBER,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
								}
								else
								{
									RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,healthEnrollmentMemberData,NOT_TO_CONSIDER,MEMBER_NOT_ELIGIBLE,insuranceType);
									ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
								}
							}
							
						}
						else
						{
							RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,null,NOT_TO_CONSIDER,MULTIPLE_GROUP,insuranceType);
							ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
							ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
						}
					}
					else
					{
						RenewalApplicant ssapRenewalApplicant = populateRenewalApplicants(applicationId,memberId,null,NOT_TO_CONSIDER,NEW_MEMBER,insuranceType);
						ssapRenewalApplicant.setGroupRenewalStatus(NOT_TO_CONSIDER);
						ssapRenewalApplicant = renewalApplicantService.saveRenewalApplicant(ssapRenewalApplicant);
					}
				}
			}
		
			if(isGICExists || isToConsiderExists)
			{
				if(isGICExists)
				{
					ssapRenewalApplication.setHealthReasonCode(getReasonCode(HEALTH_INCONSISTENT));
					ssapRenewalApplication.setHealthRenewalStatus(NOT_TO_CONSIDER);
				}
				else{
					ssapRenewalApplication.setHealthRenewalStatus(TO_CONSIDER);
				}
			}
			else{
				if(isMedicaidEligibleGroupExists)
				{
					ssapRenewalApplication.setHealthReasonCode(getReasonCode(MEDICAID_ELIGIBLE_HOUSEHOLD));
					ssapRenewalApplication.setHealthRenewalStatus(NOT_TO_CONSIDER);
				}
				else
				{
					ssapRenewalApplication.setHealthReasonCode(getReasonCode(HEALTH_IN_ELIGIBLE));
					ssapRenewalApplication.setHealthRenewalStatus(NOT_TO_CONSIDER);
				}
			}
		}
		else
		{
			// Enrollment not available
			ssapRenewalApplication.setHealthRenewalStatus(NOT_TO_CONSIDER);
			ssapRenewalApplication.setHealthReasonCode(getReasonCode(ENROLLMENT_NOT_AVAILABLE));
		}
		
		ssapRenewalApplication = renewalApplicationService.saveRenewalApplication(ssapRenewalApplication);
		return ssapRenewalApplication;
	}
	
	
	
	private RenewalApplicant populateRenewalApplicants(String applicationId,String memberId,EnrollmentWithMemberDataDTO enrolledMemberDto,RenewalStatus renewalStatus,String groupReasonCode,String planType)
	{
		RenewalApplicant ssapRenewalApplicant = renewalApplicantRepository.findByMemberIdAndPlanType(memberId,StringUtils.upperCase(planType),Long.valueOf(applicationId));
		if( ssapRenewalApplicant == null)
		{
			ssapRenewalApplicant = new RenewalApplicant();
		}
		
		Date date = new TSDate();
		Timestamp timestamp = new Timestamp(date.getTime());
		String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
		boolean externalMemberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? true : false;
		SsapApplicant ssapApplicant = null;
		if(externalMemberId)
		{
			ssapApplicant = ssapApplicantRepository.findByExternalApplicantIdAndSsapApplicationId(memberId, Long.valueOf(applicationId));
		}
		else{
			ssapApplicant = ssapApplicantRepository.findBySsapApplicantGuidAndSsapApplicationId(memberId, Long.valueOf(applicationId));
		}
		
		if( ssapApplicant != null)
		{
			ssapRenewalApplicant.setSsapApplicantId(ssapApplicant.getId());
		}
		
		if( enrolledMemberDto != null)
		{
			ssapRenewalApplicant.setEnrollmentId(Long.valueOf(enrolledMemberDto.getEnrollmentID()));
			ssapRenewalApplicant.setPlanId(enrolledMemberDto.getPlanID());
			//ssapRenewalApplicant.setPlanType(enrolledMemberDto.getInsuranceType());
		}
		if(StringUtils.isNotBlank(groupReasonCode))
		{
			ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(groupReasonCode));
		}
		ssapRenewalApplicant.setPlanType(planType);
		ssapRenewalApplicant.setMemberId(memberId);
		ssapRenewalApplicant.setSsapApplicationId(Long.valueOf(applicationId));
		ssapRenewalApplicant.setRenewalStatus(renewalStatus);
		ssapRenewalApplicant.setCreationTimestamp(timestamp);
		ssapRenewalApplicant.setLastUpdateTimestamp(timestamp);
		return ssapRenewalApplicant;
	}
	
	
	private RenewalApplication populateRenewalApplication(SsapApplication currentApplication)
	{
		RenewalApplication ssapRenewalApplication = new RenewalApplication();
		Date date = new TSDate();
		Timestamp timestamp = new Timestamp(date.getTime());
		ssapRenewalApplication.setSsapApplicationId(currentApplication.getId());
		Integer cmrHouseHoldId = Integer.valueOf(currentApplication.getCmrHouseoldId().toString());
		ssapRenewalApplication.setCmrHouseholdId(cmrHouseHoldId);
		String externalHouseholdCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(cmrHouseHoldId);
		ssapRenewalApplication.setExternalHouseholdCaseId(externalHouseholdCaseId);
		ssapRenewalApplication.setHealthReasonCode(null);
		ssapRenewalApplication.setDentalReasonCode(null);
		ssapRenewalApplication.setCreationTimestamp(timestamp);
		ssapRenewalApplication.setLastUpdateTimestamp(timestamp);
		return ssapRenewalApplication;
	}
	
	private Integer getReasonCode(String lookupValueCode)
	{
		Integer reasonCode =  lookupService.getlookupValueIdByTypeANDLookupValueCode(RENEWALS,lookupValueCode );
		return reasonCode;
	}
	
	private boolean healthAgeCheck(SsapApplication currentApplication,SsapApplicant ssapApplicant) throws Exception
	{
		boolean healthAgeCheckPassed;
		String healthAgeCheckNFApp = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_HEALTH_AGE_OUT_NFA); // Y for NV,ID and N for CA,MN
		String healthAgeCheckFinApp = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_HEALTH_AGE_OUT_FA); // Y for NV and N for CA,MN,ID
		
		if(ExchangeEligibilityStatus .QHP.equals(currentApplication.getExchangeEligibilityStatus())){
			if(Y.equals(healthAgeCheckNFApp))
			{
				boolean isAnyoneSeekingCoverage = dependentAging26Check(currentApplication,ssapApplicant);
				if(isAnyoneSeekingCoverage) {
					healthAgeCheckPassed = true;
				}
				else 
				{
					healthAgeCheckPassed = false;
				}
			}
			else
			{
				healthAgeCheckPassed = true;
			}
		}
		else
		{
			if(Y.equals(healthAgeCheckFinApp))
			{
				boolean isAnyoneSeekingCoverage = dependentAging26Check(currentApplication,ssapApplicant);
				if(isAnyoneSeekingCoverage) {
					healthAgeCheckPassed = true;
				}
				else 
				{
					healthAgeCheckPassed = false;
				}
			}
			else
			{
				healthAgeCheckPassed = true;
			}
		}
		return healthAgeCheckPassed;
	}
	
	
	private boolean dentalAgeCheck(SsapApplication currentApplication,SsapApplicant ssapApplicant,Long renewalYear,String planId,Set<String> dentalEnrolledMembers) throws Exception
	{
		boolean dentalAgeCheckPassed;
		String dentalAgeOutCheck = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DENTAL_AGE_OUT); // // Y for MN,NV,ID and N for CA
		
		//dental enrollment check starts
		
		if(Y.equals(dentalAgeOutCheck))
		{
			Set<String> dentalApplicants =  renewalApplicationService.dependentDentalAgeCheck(currentApplication, ssapApplicant,dentalEnrolledMembers, renewalYear,planId);
			if(dentalApplicants != null && !dentalApplicants.isEmpty()){
				dentalAgeCheckPassed = true;
			}
			else{
				dentalAgeCheckPassed = false;
				
			}
		}
		else
		{
			dentalAgeCheckPassed = true;
		}
		
		return dentalAgeCheckPassed;
	}
	
	private List<EnrollmentWithMemberDataDTO> getEnrollmentDetails(Long applicationId , List<SsapApplicant> members,Long coverageYear,Integer cmrHouseHoldId) throws GIException, RestClientException, JsonProcessingException {
		
		EnrollmentByMemberListRequestDTO requestDto = new EnrollmentByMemberListRequestDTO();
		requestDto.setMembers(extractMemberIdsForEnrollmentRequest(members));
		requestDto.setCoverageYear(Long.toString(coverageYear));
		List<EnrollmentStatus> enrollmentStatus = new ArrayList<EnrollmentStatus>();
		enrollmentStatus.add(EnrollmentStatus.CONFIRM);
		enrollmentStatus.add(EnrollmentStatus.PENDING);
		requestDto.setEnrollmentStatusList(enrollmentStatus);
		requestDto.setHouseholdCaseId(String.valueOf(cmrHouseHoldId));
		return activeEnrollmentService.getEnrollmentByMembers(requestDto, applicationId);
	}
	
	private boolean dependentAging26Check(SsapApplication currentApplication,SsapApplicant ssapApplicant) throws Exception {
		List<String> bloodRelations = new ArrayList<String>();
		String childRelations = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_CHILDRELATIONCODE);//"09,10,17,19";
		String[] childRelationArr = childRelations.split(",");
		
		for (String childRelation : childRelationArr) {
			bloodRelations.add(childRelation);
		}
	
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		List<BloodRelationship> relationShip = members.get(0).getBloodRelationship();
		
		
		/*if(isPrimaryNotSeekingCoverage(ssapApplicants)  && isChildOnlyPlan(relationShip, bloodRelations)) {
			return true;
		}*/
		
		Date coverageStartDate = getCoverageStartDate(currentApplication) ; 
		
		for (BloodRelationship bloodRelationship : relationShip) {
			if (bloodRelationship.getRelatedPersonId() != null && Integer.parseInt(bloodRelationship.getRelatedPersonId()) == 1) {
				if(bloodRelations.contains(bloodRelationship.getRelation())){	
					if(ssapApplicant.getPersonId() == Integer.parseInt(bloodRelationship.getIndividualPersonId())) {
						if(checkForAge26(ssapApplicant.getBirthDate(), coverageStartDate)) {
							ssapApplicant.setApplyingForCoverage("N");
							for (HouseholdMember householdMember : members) {
								if(householdMember.getPersonId() == ssapApplicant.getPersonId()) {
									householdMember.setApplyingForCoverageIndicator(false);
									ssapApplicant = ssapCloneApplicantService.saveSsapApplicant(ssapApplicant);
									createProgramEligibilityForQhp(ssapApplicant, "FALSE");											
								}
							}
						}
					}
				}
			}
		}
		
		currentApplication.setApplicationData(ssapJsonBuilder.transformToJson(applicationData));
		ssapCloneApplicationService.saveSsapSepApplication(currentApplication, true);
		
		return isAnyoneSeekingCoverage(ssapApplicant);
	}
	
	private boolean isAnyoneSeekingCoverage(SsapApplicant ssapApplicant) 
	{
		boolean isAnyoneSeekingCoverage = false;
		
		if(ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")) {
			isAnyoneSeekingCoverage = true;
		}
		
		return isAnyoneSeekingCoverage;
	}

	private void createProgramEligibilityForQhp(SsapApplicant ssapApplicant, String indicatorVal) {

		Date currentDate = new Date();
		EligibilityProgram eligibilityProgramDO = new EligibilityProgram();
		eligibilityProgramDO.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
		eligibilityProgramDO.setEligibilityIndicator(indicatorVal);
		eligibilityProgramDO.setEligibilityStartDate(currentDate);
		//DateTime date = new DateTime().withTime(23, 59, 59, 999);
		eligibilityProgramDO.setEligibilityEndDate(new Date());
		eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
		eligibilityProgramDO.setSsapApplicant(ssapApplicant);
		eligibilityProgramDO.setIneligibleReason("Not Seeking Coverage due to Aging Out");
		eligibilityProgramRepository.save(eligibilityProgramDO);
	}


	private Date getCoverageStartDate(SsapApplication currentApplication) throws GIException {
		return ssapCloneApplicationService.getCoverageStartDate(currentApplication);
	}


	private boolean checkForAge26(Date birthDate, Date coverageStartDate) {
		Calendar dob = Calendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar today = Calendar.getInstance();  
		today.setTime(coverageStartDate);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		  age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
		  age--;  
		}
		
		return (age >= 26);
	}
}
