package com.getinsured.hix.batch.provider.processors;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ITINProcessor implements ProviderDataFieldProcessor {
    private ValidationContext context;
    private int index;
    private Logger logger = LoggerFactory.getLogger(ITINProcessor.class);

    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated)
            throws ProviderValidationException, InvalidOperationException {
        String s;
        int intITIN;
        try {
            s = (String) objToBeValidated;
            intITIN = Integer.valueOf(s);
            String strITIN = String.valueOf(intITIN).trim();
            if (strITIN.length() != 9) {
                throw new ProviderValidationException("Validation of ITIN failed. Length of ITIN is not 9.");
            }
            if (!strITIN.startsWith("9")) {
                throw new ProviderValidationException("Validation of ITIN failed. Given ITIN does not start with 9.");
            }
            int centerDigits = Integer.parseInt(strITIN.substring(3, 5));
            if (centerDigits < 70 || centerDigits > 88) {
                throw new ProviderValidationException("Validation of ITIN failed. 4th and 5th digit should be in the range of 70-88.");
            }
        } catch (NumberFormatException e) {
            throw new ProviderValidationException("Validation of ITIN failed. It seems to contain non-digit characters.");
        }
        return intITIN;
    }
}
