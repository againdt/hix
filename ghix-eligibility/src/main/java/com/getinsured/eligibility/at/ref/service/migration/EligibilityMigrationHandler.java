package com.getinsured.eligibility.at.ref.service.migration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicantEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.MatchHandler;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.QHPApplicationEligibilityHelper;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * EligibilityHandler to compare stored ssapApplicants with the received state's response.
 *
 * @author Ekram
 *
 */
@Component
public class EligibilityMigrationHandler {

	private static final Logger LOGGER = Logger.getLogger(EligibilityMigrationHandler.class);

	@Autowired private ApplicantEligibilityHelper applicantEligibilityHelper;
	@Autowired private ApplicationEligibilityMigrationHelper applicationEligibilityMigrationHelper;
	@Autowired private QHPApplicationEligibilityHelper qHPApplicationEligibilityHelper;
	@Autowired private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;

	private String STATE_CODE;
	
	private List<String> csPriorityList = new ArrayList<>(Arrays.asList("CS1", "CS4", "CS5", "CS6", "CS3", "CS2"));

	@Autowired
	private MigrationUtil migrationUtil;
	
	@PostConstruct
	public void createStateContext() {
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			boolean flag = process(erpResponse);

			if (flag){
				resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.AUTO);
			} else {
				resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.MANUAL);
			}

		} catch (Exception e) {

			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			migrationUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}

	private boolean process(ERPResponse erpResponse){
		//1. Fetch all incoming applicants from AT Response
		List<TaxHouseholdMember> taxHHList =  erpResponse.getTaxHouseholdMemberList();

		//2. Fetch all existing applicants from SSAP_APPLICANT table
		List<TaxHouseholdMember> ssapHHList =  erpResponse.getSsapApplicantList();

		//3. Fetch corresponding giWsPayload object from db for ssapApplicationId..
		Integer giWsPayloadId = erpResponse.getGiWsPayloadId();

		//4. Compare every AT Response applicant with DB..
		Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap = new HashMap<>();
		Map<String, Object> hhLevelEliDetails = new HashMap<>();
		com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram hhAPTCElig = null;
		Set<String> hhCSLevel = new HashSet<String>();
		hhLevelEliDetails.put("HHCSLevel",hhCSLevel);
		for (TaxHouseholdMember atTaxPerson : taxHHList) {

			TaxHouseholdMember ssapApplicant = MatchHandler.findMatchingMember(atTaxPerson, ssapHHList);

			if (ssapApplicant != null){
				// Persist latest eligibility data..
				List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityPrograms = applicantEligibilityHelper.processEligibility(atTaxPerson, ssapApplicant, giWsPayloadId, hhLevelEliDetails);
				eligibilityProgramMap.put(ssapApplicant.getPersonInfo().getPrimaryKey(), eligibilityPrograms);
			} else {
				LOGGER.error(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED);
				throw new GIRuntimeException(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED +
						"AT Person Detail - " + atTaxPerson +
						"ssap HH List - " + ssapHHList);
			}

		}
		Boolean isAPTCElig = Boolean.FALSE;
		if (!hhLevelEliDetails.isEmpty()){
			if (hhLevelEliDetails.containsKey("APTCElig")){
				isAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
			}
			if (hhLevelEliDetails.containsKey("APTCProgram")){
				hhAPTCElig = (com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram) hhLevelEliDetails.get("APTCProgram");
				hhLevelEliDetails.remove("APTCProgram");
			}
			if(hhLevelEliDetails.containsKey("HHCSLevel")) {
				hhCSLevel = (HashSet<String>)hhLevelEliDetails.get("HHCSLevel");
				List<String> hhMemberCSLevels = new ArrayList<String>();
				hhMemberCSLevels.addAll(hhCSLevel);
				String HHLevellowestCS = getLowPriorityCSLevel(hhMemberCSLevels);
				hhLevelEliDetails.put(EligibilityConstants.CSR_LEVEL, HHLevellowestCS);
			}
		}
		//5. Derive Application Eligibility Status and update application
		applicationEligibilityMigrationHelper.processEligibility(eligibilityProgramMap, erpResponse.isFullAssessment(), erpResponse.getApplicationID(), hhLevelEliDetails);

		//6. handle DE and CAM case separately...
		if ("NM".equals(STATE_CODE)){
			qHPApplicationEligibilityHelper.processEligibility(erpResponse);
		}

		//7. determine application is Native American Application...
		nativeAmericanEligibilityHelper.processEligibility(erpResponse.getApplicationID());
		
		if(hhAPTCElig!=null && isAPTCElig.booleanValue()) {
			applicationEligibilityMigrationHelper.insertHHAptc(hhAPTCElig,erpResponse.getApplicationID());
		}

		return true;
	}
	
	private String getLowPriorityCSLevel(List<String> hhMemberCSLevels){
		String CSLevel = null;
		Integer lowestIndex = null;
		Integer currentIndex = null;
		if(CollectionUtils.isNotEmpty(hhMemberCSLevels)) {
			for(String memberCS : hhMemberCSLevels) {
				currentIndex = csPriorityList.indexOf(memberCS);
				if(lowestIndex == null || lowestIndex>currentIndex) {
					lowestIndex = currentIndex;
				}
			}
			CSLevel = csPriorityList.get(lowestIndex);
		}
		return CSLevel;
	}

}
