import React from 'react';

import Impersonation from './Impersonation';

// import CONFIG from '../../../.env';


function MinnesotaHeader ({headerLinks, language, messages, onChangeLanguage}) {
    const { impersonation, impersonationDetails, exchangeName, links, user } = headerLinks;
    const { LOGOUT, MANAGE_ASSISTER } = links;
    const { role } = user;
    const { LOGO: logo } = headerLinks.assets;

    const MNColor = '#018383';
    const headerContainerStyle = {
        background: 'white',
        borderBottom: `8px solid ${MNColor}`
    };

    const menuItemStyle = {
        color: MNColor,
        fontSize: '18px',
        fontWeight: 'bold'
    };

    const getAssisterUrl = () => {
        switch (role) {
        case 'EXTERNAL_ASSISTER':
            return <a id="home" title = "Manage Assister" className="usa-nav-link" style={menuItemStyle} href={`${MANAGE_ASSISTER}/NavigatorS`}>{messages && messages['header.assister.home']}</a>;
        case 'INDIVIDUAL':
            return <a id="home" title = "Manage Assister" className="usa-nav-link" style={menuItemStyle} href={`${MANAGE_ASSISTER}/CitizenPortal/application.do?AssociateAssistor`}>{messages && messages['header.assister.manage']}</a>;
        default:
            return null;
        }
    };
    
    return (
        <>
            <a className="usa-skipnav" href="#main-content">Skip to main content</a>
            <header className="usa-header usa-header-basic" id="masthead" role="banner" style={headerContainerStyle}>
                <div className="usa-nav-container masthead__container">
                    <div className="usa-navbar">
                        <div className="usa-logo logo" id="masthead-logo-wrap">
                            <a href="#" className="masthead__logo__img">
                                <img id="masthead-logo-img_02" className="masthead__logo__img va-middle" src={logo} alt={exchangeName} width="150"/>
                            </a>
                        </div>
                        <div className="usa-menu-btn masthead__nav__btn--collapse">
                            {/*<FontAwesomeIcon icon="bars" />*/} Menu
                        </div>
                    </div>
                    <nav role="navigation" className="usa-nav main__nav">
                        <ul className="usa-nav-primary usa-accordion main__nav__links_container">
                            <li>
                                <a id="backToDashboard" title = "Dashboard" className="usa-nav-link" style={menuItemStyle} href="/hix/indportal">{messages && messages['label.dashboard']}</a>
                            </li>
                            <li>
                                {getAssisterUrl()}
                            </li>
                            <li>
                                <a id="home" title="Learn more" rel="noopener noreferrer" className="usa-nav-link" style={menuItemStyle} href="https://www.mnsure.org/learn-more/index.jsp" target="_blank">{messages && messages['header.learn.more']}</a>
                            </li>
                            <li>
                                <a id="home" title="Get Help" rel="noopener noreferrer" className="usa-nav-link" style={menuItemStyle} href="https://www.mnsure.org/help/" target="_blank">{messages && messages['header.help.get']}</a>
                            </li>
                            <li>
                                <a id="home" title="Sign Out" className="usa-nav-link" style={menuItemStyle} href={LOGOUT}>{messages && messages['header.sign.out']}</a>
                            </li>
                            <li>
                                {language === 'en' &&
                                    <a id="home" title="Change to Spanish" className="usa-nav-link" style={menuItemStyle} href="#" onClick={() => onChangeLanguage('es')}>{messages && messages['label.language.spanish']}</a>
                                }
                                {language === 'es' &&
                                    <a id="home" title="Change to English" className="usa-nav-link" style={menuItemStyle} href="#" onClick={() => onChangeLanguage('en')}>{messages && messages['label.language.english']}</a>
                                }
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
            {impersonation && impersonationDetails && impersonationDetails.name &&
                <Impersonation {...{impersonationDetails}} />
            }
        </>
    );
}

export default MinnesotaHeader;
