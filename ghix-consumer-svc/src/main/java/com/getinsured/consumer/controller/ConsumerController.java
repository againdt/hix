package com.getinsured.consumer.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.iex.util.SsapUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.service.ConsumerHistoryService;
import com.getinsured.consumer.service.ConsumerService;
import com.getinsured.consumer.service.GoodRxService;
import com.getinsured.consumer.service.RelinkHouseholdService;
import com.getinsured.exception.RestfulException;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.consumer.history.ConsumerHistoryView;
import com.getinsured.hix.dto.cap.CapConsumerDto;
import com.getinsured.hix.dto.consumer.ConsumerRequestDTO;
import com.getinsured.hix.dto.consumer.MemberPortalRequestDTO;
import com.getinsured.hix.dto.consumer.MemberPortalResponseDTO;
import com.getinsured.hix.dto.consumer.goodrx.DrugDetailsDTO;
import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;
import com.getinsured.hix.dto.crm.ConsumerHistoryRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.ConsumerCallLog;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.HouseholdEnrollment;
import com.getinsured.hix.model.consumer.Member;
import com.getinsured.hix.model.consumer.Prescription;
import com.getinsured.hix.platform.eventlog.AppEventDto;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * This is the entry point for clients. From Controller we call service to get
 * the things done.
 * 
 * @author Suhasini Goli
 * @since 9/11/2013
 */
@Controller
@RequestMapping("/consumer")
public class ConsumerController {

	private static final String EMPTY = "";

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerController.class);

	public static final String HOUSEHOLD_CASE_ID_SEQUENCE = "CMR_HOUSEHOLD_CASE_ID_SEQ";
	public static final String GLOBAL_STATE_EXCHANGE_ABBREVIATION = "global.exchangeAbbr";

	@Autowired
	private ConsumerService consumerService;
	@Autowired
	private GoodRxService goodRxService;
	@Autowired
	private ConsumerHistoryService consumerHistoryService;
	@Autowired
	private RelinkHouseholdService relinkHouseholdService;
	@Autowired
	private UserService userService;
	@Autowired
	private SsapUtil ssapUtil;

	private static final String ERR_MSG_PARAM_MISSING = "Error: Required parameters are missing.";

	private static final String WELCOME_MESSAGE = "Welcome to GHIX Consumer Service module....";

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {

		LOGGER.info(WELCOME_MESSAGE);
		return WELCOME_MESSAGE;
	}

	/**
	 * This method saves the Member information
	 * 
	 * @author Suhasini Goli
	 * @since Sept 11, 2013
	 * @param Member
	 * @return Member
	 */
	@RequestMapping(value = "/saveMember", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Member>  saveMember(@RequestBody Member member) {
		Member memberObj = null;
		try{
			memberObj = consumerService.saveMember(member);	
		}
		catch(Exception e){
			throw new RestfulException("Error in saving member.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		LOGGER.info("Member created successfully with id: " + memberObj.getId());
		return new ResponseEntity<Member>(memberObj,HttpStatus.OK);
	}

	@RequestMapping(value = "findMemberById/{id}", method = RequestMethod.GET)
	@ApiOperation(value="Finds Member by Id", notes="This operation gives Member for the Id in the parameter")
	@ResponseBody
	public ResponseEntity<Member> findMemberById(@PathVariable int id) {
		Member memberObj = null;
		try{
			memberObj = consumerService.findMemberById(id);
		}
		catch(Exception e){
			throw new RestfulException("Error in finding member.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Member>(memberObj,HttpStatus.OK);
	}

	@RequestMapping(value = "/saveHousehold", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Household>  saveHousehold(@RequestBody Household household) {
		
		boolean householdCreateflag = false; //flag to check if household is already created so to not to trigger Household created event for the member.
		
		if (null != household && household.getId() > 0) {
			householdCreateflag = true;
		}
		Household savedHousehold = null;
		try {
			AccountUser user = household.getUser();
			if(user != null ) {
				int userId = user.getId();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Looking up user received with the saveHousehold request for id {}",user.getId());
				}
				user = this.userService.findById(userId);
				if(user != null) {
					household.setUser(user);
				}
			}

			// If household case id is empty or null: Create sequence and prepend with with exchange abbreviation from GI APP Config
			if (household.getHouseholdCaseId() == null || household.getHouseholdCaseId().isEmpty()) {
				String nextHouseholdCaseId = ssapUtil.getNextSequenceFromDB(HOUSEHOLD_CASE_ID_SEQUENCE);
				String exchangeAbbreviation = DynamicPropertiesUtil.getPropertyValue(GLOBAL_STATE_EXCHANGE_ABBREVIATION);
				household.setHouseholdCaseId(exchangeAbbreviation + nextHouseholdCaseId);
			}

			savedHousehold = consumerService.saveHousehold(household);
		} catch (Exception ex) {
			LOGGER.error("Unable to save the household record", ex);
			throw new RestfulException("Unable to save household.",HttpStatus.NOT_ACCEPTABLE,ex);
		}

		if (!householdCreateflag) {
			try {
				// HIX-38637
				ConsumerEventHistory consumerCreatedEvent = new ConsumerEventHistory(
						household.getCreated());
				consumerCreatedEvent.setEventSource("Consumer");
				consumerCreatedEvent.setEventLevel("Household Created");
				consumerCreatedEvent.setHouseholdId(household.getId());
				consumerCreatedEvent.setDatetime(household.getCreated());
				consumerCreatedEvent.setCreatedBy(household.getCreatedBy());
				consumerHistoryService
						.saveConsumerEverntLog(consumerCreatedEvent);
			} catch (Exception e) {
				LOGGER.info("Exception while creating event account for hosusehold created "
						+ e.getMessage());
			}
		}
		LOGGER.info("Household created successfully with id: {}",savedHousehold.getId());
		
		if (savedHousehold.getUser() != null) {
			AccountUser user = new AccountUser();
			user.setId(savedHousehold.getUser().getId());
			savedHousehold.setUser(user);
		}
		return new ResponseEntity<Household>(savedHousehold, HttpStatus.OK);
	}

	@RequestMapping(value = "/findHouseholdById", method = {
			RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseEntity<String> findHouseholdById(@RequestBody String id) {
		LOGGER.debug("Consumer service findHouseholdById called for input: " + SecurityUtil.sanitizeForLogging(id));
		
		ConsumerResponse consumerResponse = new ConsumerResponse();
		consumerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		consumerResponse.setErrMsg(ERR_MSG_PARAM_MISSING);
		
		id = id.replaceAll("\"", EMPTY);
		if(StringUtils.isNumeric(id)){
			Integer householdId = Integer.parseInt(id);	
			Household household = consumerService.findHouseholdById(householdId);
			if (null != household) {
				LOGGER.info("findHouseholdById returns null");
				consumerResponse.setHousehold(household);
				consumerResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				consumerResponse.setErrMsg(EMPTY);
			}
			else{
				throw new RestfulException("Unable to find household by id = "+householdId,HttpStatus.NOT_FOUND);
			}
		}

		XStream xstream = GhixUtils.getXStreamStaxObject();
		return new ResponseEntity<String>(xstream.toXML(consumerResponse),HttpStatus.OK) ;

	}

	@RequestMapping(value = "/householdbyhouseholdid/{householdId}", method = RequestMethod.GET)
	public ResponseEntity<Household> getHouseholdByHouseholdId(@PathVariable Integer householdId) {
		LOGGER.debug("ghix-consumer-svc::/householdbyhouseholdid/{}", householdId);
		Household household = consumerService.findHouseholdById(householdId);

		if(household == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}

		return ResponseEntity.ok(household);
	}

	@RequestMapping(value = "/manageConsumers", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> searchHousehold(
			@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("Consumer service: searchHousehold invoked");
		Map<String, Object> consumerListAndRecordCount = new HashMap<String,Object>(0);
	//	return consumerService.searchHousehold(searchCriteria);
		try{
			consumerListAndRecordCount = consumerService.searchHouseholdUsingNativeQuery(searchCriteria);
		}
		catch(Exception e){
			throw new RestfulException("Error in search query.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Map<String,Object>>(consumerListAndRecordCount, HttpStatus.OK) ;
	}	
	
	/**
	 * 
	 * @param searchCriteria
	 * @return the list of applicants as per the search criteria
	 */
	@RequestMapping(value = "/manageapplicants", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> searchApplicants(@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("Consumer service: searchApplicants invoked");
		Map<String, Object> applicantListAndRecordCount = new HashMap<String,Object>(0);
		try{
			applicantListAndRecordCount = consumerService.searchApplicants(searchCriteria);
		}
		catch(Exception e){
			throw new RestfulException("Error in search query.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Map<String,Object>>(applicantListAndRecordCount, HttpStatus.OK) ;
	}	
	
	@RequestMapping(value = "/manageLeads", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> searchEligLead(
			@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("Consumer service: searchEligLead invoked");
		return consumerService.searchEligLead(searchCriteria);
	}
	
	

	@RequestMapping(value = "/findMembersByHousehold", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> findMembersByHousehold(@RequestBody String householdStr) {
		
		ConsumerResponse consumerResponse = new ConsumerResponse();
		LOGGER.info("Consumer service: findMembersByHousehold invoked for input: " + SecurityUtil.sanitizeForLogging(householdStr));
		XStream xstream =  GhixUtils.getXStreamStaxObject();
		Household household = (Household) xstream.fromXML(householdStr);
		List<Member> members = null;
		try{
			members = consumerService.findMembersByHousehold(household);	
		}
		catch(Exception e){
			throw new RestfulException("Error in find members by household. Household="+householdStr,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}

		if (members == null){
			members = new ArrayList<Member>();
			LOGGER.info("No member found for this household");
		}
			

		consumerResponse.setMembers(members);

		return new ResponseEntity<String>(xstream.toXML(consumerResponse),HttpStatus.OK) ;

	}

	@RequestMapping(value = "/findHouseholdByUserId", method = {
			RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseEntity<String> findHouseholdByUserId(@RequestBody String id) {
		LOGGER.info("Consumer Service: findHouseholdByUserId invoked for input: " + SecurityUtil.sanitizeForLogging(id));

		Integer userId = Integer.parseInt(id.replaceAll("\"", EMPTY));

		XStream xstream = GhixUtils.getXStreamStaxObject();

		Household household = null;
		try{
			household = consumerService.findHouseholdByUserId(userId);	
		}
		catch(Exception e){
			throw new RestfulException("Error in finding household by user id:"+userId,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		if (household != null) {
			return new ResponseEntity<String>(xstream.toXML(household),HttpStatus.OK) ;
		}
		LOGGER.info("Consumer Service: No Household found for user with id : " + SecurityUtil.sanitizeForLogging(id));
		return new ResponseEntity<String>(StringUtils.EMPTY,HttpStatus.OK) ;
	}

	@RequestMapping(value = "/findHouseholdRecordByUserId", method = {RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseEntity<Household> findHouseholdRecordByUserId(@RequestBody String id) {
		LOGGER.info("Consumer Service: findHouseholdRecordByUserId invoked for input: " + SecurityUtil.sanitizeForLogging(id));

		Integer userId = Integer.parseInt(id.replaceAll("\"", EMPTY));

		Household household = null;
		try{
			household = consumerService.findHouseholdByUserId(userId);	
		}
		catch(Exception e){
			throw new RestfulException("Error in finding household by user id:"+userId,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Household>(household,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findHouseholdByEligLeadId", method = {
			RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseEntity<String> findHouseholdByEligLeadId(@RequestBody String id) {
		LOGGER.info("Consumer Service: findHouseholdByEligLeadId invoked for input: " + SecurityUtil.sanitizeForLogging(id));
		Integer eligLeadId = Integer.parseInt(id.replaceAll("\"", EMPTY));
		XStream xstream = GhixUtils.getXStreamStaxObject();

		Household household = null;
		try{
			household = consumerService.findHouseholdByEligLeadId(eligLeadId);
		}
		catch(Exception e){
			throw new RestfulException("Error in finding household by elig lead id:"+eligLeadId,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		if (household != null) {
			return new ResponseEntity<String>(xstream.toXML(household),HttpStatus.OK) ;
		}

		LOGGER.info("Consumer Service: No Household found for EligLead having id : " + SecurityUtil.sanitizeForLogging(id));
		return new ResponseEntity<String>(StringUtils.EMPTY,HttpStatus.OK) ;
	}
	
	
	
	
	/*  
    @RequestMapping(value = "/saveFFMResponse", method = RequestMethod.POST)
	@ResponseBody
	public Household saveFamilyFFM(@RequestBody FFMHouseholdResponse household) {
		LOGGER.info("Consumer Service: saveFamilyFFM invoked for input: " );
		return consumerService.saveResponseFromFFM(household);
	}
	*/
	
	@RequestMapping(value = "/findHouseholdByGIHouseholdId/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Household> findHouseholdByGIHouseholdId(@PathVariable String id) {
		LOGGER.info("Consumer Service: findHouseholdByGIHouseholdId called for input: " + id);
		Household household = null;
		try{
			household = consumerService.findHouseholdByGIHouseholdId(id);	
		}
		catch(Exception e){
			throw new RestfulException("Error in finding household by gi householdd id:"+id,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Household>(household, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveHouseholdInformation", method = RequestMethod.POST)
	@ResponseBody
	public MemberPortalResponseDTO saveHouseholdInformation(@RequestBody MemberPortalRequestDTO mpRequestDTO) {
		LOGGER.info("Consumer Service: saveHouseholdInformation");
		try {
			return consumerService.saveHouseholdInformation(mpRequestDTO);
		} catch (ConsumerServiceException e) {
			throw new RestfulException("Unable to save household information",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
	}
	
	/**
	 * This method updates the smoker information for a list of members
	 * 
	 * @author Suhasini Goli
	 * @since  Dec 20, 2013
	 * @param  FFMHouseholdRepsonse
	 * @return void
	 * @ 
	 */
	/*
	@RequestMapping(value = "/updateSmokerInfoForMembers", method = RequestMethod.POST)
	@ResponseBody
	void updateSmokerInfoForMembers(@RequestBody FFMHouseholdResponse ffmResponse) {
		LOGGER.debug("Consumer Service: updateSmokerInfoForMembers" );
		consumerService.updateSmokerInfoForMembers(ffmResponse);
		return;
	}
	*/
	
	@RequestMapping(value = "/drugSearch/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<String>> drugSearch(@PathVariable String name){
		LOGGER.info("GoodRx drug search called for input: " + SecurityUtil.sanitizeForLogging(name));
		GoodRxResponseDTO response = null;
		try {
			response = goodRxService.search(name);
		} catch (ConsumerServiceException e) {
			throw new RestfulException("Unable to search drugs.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		if(response!=null && response.isSuccess() && response.getData()!=null){
			return new ResponseEntity<List<String>>(response.getData().getCandidates(),HttpStatus.OK);
		}
		return null;
	}
	
	@RequestMapping(value = "/drugDetails/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<DrugDetailsDTO> drugDetails(@PathVariable String name){
		LOGGER.info("GoodRx drug details called for input: " + SecurityUtil.sanitizeForLogging(name));
		GoodRxResponseDTO response = goodRxService.details(name);
		if(response!=null && response.isSuccess() && response.getData()!=null){
			return new ResponseEntity<DrugDetailsDTO>(response.getData(),HttpStatus.OK);
		}
		return null;
	}
	
	/**
	 * This method saves the Prescription information
	 * 
	 * @author Suhasini Goli
	 * @since  Dec 12, 2013
	 * @param Prescription
	 * @return Prescription
	 */
	@RequestMapping(value = "/savePrescription", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Prescription> savePrescription(@RequestBody Prescription prescription) {
		Prescription prescriptionObj = null;
		try {
			prescriptionObj = consumerService.savePrescription(prescription);
		} catch (ConsumerServiceException e) {
			throw new RestfulException("Unable to save prescription",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		LOGGER.debug("Member created successfully with id: " + prescription.getId());
		return new ResponseEntity<Prescription>(prescriptionObj,HttpStatus.OK);
	}
	
	/**
	 * This method saves a list of Prescriptions
	 * 
	 * @author Suhasini Goli
	 * @since  Dec 12, 2013
	 * @param  List<Prescription>
	 * @return List<Prescription>
	 */
	@RequestMapping(value = "/savePrescriptions", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerResponse> savePrescription(@RequestBody ConsumerResponse consumerResponse) {

		ConsumerResponse response = null;
		try {
			response = consumerService.savePrescriptions(consumerResponse);
		} catch (ConsumerServiceException e) {
			throw new RestfulException("Unable to save Prescriptions",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<ConsumerResponse>(response,HttpStatus.OK);
	}
	
		
	@RequestMapping(value = "/findPrescriptionById", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Prescription>  findPrescriptionById(@RequestBody long id){
		Prescription prescriptionObj = consumerService.findPrescriptionById(id);
		return new ResponseEntity<Prescription>(prescriptionObj,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findPrescriptionsByHousehold", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerResponse> findPrescriptionsByHousehold(@RequestBody String householdStr) {
		ConsumerResponse consumerResponse = null;
		LOGGER.debug("ConsumerController.findMembersByHousehold - invoked");
		try{
			Household household = JacksonUtils.getJacksonObjectReader(Household.class).readValue(householdStr);
			consumerResponse = consumerService.findPrescriptionsByHousehold(household);
		}
		catch(Exception e){
			LOGGER.error(" ERROR in findPrescriptionsByHousehold ", e);
		}
		if (consumerResponse == null) {
			consumerResponse = new ConsumerResponse();
		}
		
		return new ResponseEntity<ConsumerResponse>(consumerResponse,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findPrescriptionsByHouseholdId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerResponse> findPrescriptionsByHouseholdId(@RequestBody int id) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("ConsumerController.findPrescriptionsByHouseholdId " + SecurityUtil.sanitizeForLogging(""+id));
		}
        ConsumerResponse consumerResponse = consumerService.findPrescriptionsByHouseholdId(id);
		
		if (consumerResponse == null) {
			consumerResponse =  new ConsumerResponse();
		}
		
		return new ResponseEntity<ConsumerResponse>(consumerResponse,HttpStatus.OK);

	}
	
	
	@RequestMapping(value = "/findPrescriptionByName", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Prescription> findPrescriptionByName(@RequestBody Prescription prescription){
		Prescription prescriptionObj = consumerService.findPrescriptionByName(prescription);
		return new ResponseEntity<Prescription>(prescriptionObj,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deletePrescriptionById", method = RequestMethod.POST)
	@ResponseBody
	public void deletePrescriptionById(@RequestBody long id){
		consumerService.deletePrescriptionById(id);
		return;
	}
	
	@RequestMapping(value = "/deletePrescription", method = RequestMethod.POST)
	@ResponseBody
	public void deletePrescription(@RequestBody Prescription prescription){
		consumerService.deletePrescription(prescription);
		return;
	}
	
	@RequestMapping(value = "/saveLocation", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Location> saveLocation(@RequestBody Location location) {
		Location savedLocation = consumerService.saveLocation(location);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Location created/updated successfully with id: " + savedLocation.getId());
		}
		return new ResponseEntity<Location>(savedLocation,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveHouseholdEnrollment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<HouseholdEnrollment> saveHouseholdEnrollment(@RequestBody HouseholdEnrollment householdEnrollment) {
		HouseholdEnrollment savedHouseholdEnrollment = consumerService.saveHouseholdEnrollment(householdEnrollment);
		LOGGER.info("HouseholdEnrollment created/updated successfully with id: " + savedHouseholdEnrollment.getId());
		return new ResponseEntity<HouseholdEnrollment>(savedHouseholdEnrollment,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findHouseholdEnrollmentByHouseholdId", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerResponse> findHouseholdEnrollmentByHouseholdId(@RequestBody Integer householdId) {
		List<HouseholdEnrollment> householdEnrollmentList = consumerService.findHouseholdEnrollmentByHouseholdId(householdId);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Returning HouseholdEnrollment List for Household Id {} ",SecurityUtil.sanitizeForLogging(""+householdId));
		}
		ConsumerResponse consumerResponse = new ConsumerResponse();
		consumerResponse.setHouseholdEnrollments(householdEnrollmentList);
		return new ResponseEntity<ConsumerResponse>(consumerResponse,HttpStatus.OK);
	}


	@RequestMapping(value = "/saveStateInHousehold", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Household> saveStateInHousehold(@RequestBody ConsumerRequestDTO consumerRequestDTO) {
		Household household = consumerService.saveStateInHousehold(consumerRequestDTO);
		if(null != household) {
			Location location = household.getLocation();
			if(null != location) {
				location.setState(household.getState());
				consumerService.saveLocation(location);
			}
		}
		else{
			household = new Household();
		}
		
		return new ResponseEntity<Household>(household, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveCallLogs", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerCallLog> saveCallLogs(@RequestBody ConsumerCallLog callLogs) {
		ConsumerCallLog savedCallLogs = consumerService.saveCallLogs(callLogs);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("CallLogs created/updated successfully with id: {}", SecurityUtil.sanitizeForLogging(""+savedCallLogs.getId()));
		}
		return new ResponseEntity<ConsumerCallLog>(savedCallLogs,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getHouseholdList", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getConsumerList(@RequestBody Map<String, Object> searchCriteria) {
		Map<String,Object> consumers = consumerService.getConsumerList(searchCriteria);
		return new ResponseEntity<Map<String, Object>>(consumers, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/getlinkappdata", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getlinkappdata(@RequestBody Map<String, Object> searchCriteria) {
		LOGGER.info("Consumer controller: Get link app data ");
		Map<String, Object> consumerLinkAppData = consumerService.getlinkappdata(searchCriteria); 
		return new ResponseEntity<Map<String, Object>>(consumerLinkAppData, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getConsumerCallLogHistory", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ConsumerHistoryView> getConsumerCallLogHistory(@RequestBody String id) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Consumer service: getHousehold Call log invoked consumer id: "+SecurityUtil.sanitizeForLogging(id));
		}
		Integer householdId = Integer.parseInt(id.replaceAll("\"", EMPTY));
		ConsumerHistoryView consumerHistoryView = null;
		try {
			consumerHistoryView = consumerHistoryService.getConsumerHistory(String.valueOf(householdId));
		} catch (ConsumerServiceException e) {
			throw new RestfulException("Unable to getHousehold Call log history",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return  new ResponseEntity<ConsumerHistoryView>(consumerHistoryView, HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/saveConsumerCallLog", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveConsumerCallLog(
			@RequestBody ConsumerHistoryRequest historyRequest) {
		ConsumerCallLog callLogs = null;
		String status = "false";
		try {
			callLogs = consumerHistoryService
					.populateCallLogObject(historyRequest);
			//callLogs = consumerService.saveCallLogs(callLogs);
			if(null!=callLogs){
				status =  "true";
			}
		} catch (Exception e) {
			LOGGER.error("Error in saveConsumerCallLog while parsing json "
					+ e.getMessage());
			throw new RestfulException("Error in saveConsumerCallLog.",HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		
		LOGGER.info("saveConsumerCallLog was unsuccessful");
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveConsumerCommentLog", method = RequestMethod.POST)
	public ResponseEntity<String> saveConsumerCommentLog(
			@RequestBody ConsumerHistoryRequest historyRequest) {
		List<Comment> comments = null;
		String status = "false";
		try {
			comments = consumerHistoryService
					.saveCommentLogObject(historyRequest);
			if(null!=comments){
				status = "true";
			}
		} catch (Exception e) {
			LOGGER.error("Error in saveConsumerCallLog while parsing json "
					, e);
		}
		
		LOGGER.info("saveConsumerCallLog was unsuccessful");
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveConsumerEventLog", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveConsumerEventLog(
			@RequestBody ConsumerEventHistory consumerEventHistory) {
		String response = null;
		String status = "false";
		try {
			response = consumerHistoryService
					.saveConsumerEverntLog(consumerEventHistory);
			if(null!=response && response.equalsIgnoreCase("success")){
				status = "true";
			}
		} catch (Exception e) {
			LOGGER.error("Exception in saveConsumerEventLog while saving event log. "
					, e);
		}
		
		LOGGER.info("saveConsumerEventLog was successful");
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/checkIfEmailExists", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Boolean> checkIfEmailExists(@RequestBody String email) {
		Boolean flag = consumerService.checkIfEmailExists(email);
		return new ResponseEntity<Boolean>(flag, HttpStatus.OK);
	}

	@RequestMapping(value = "/relinkHouseholdToUser", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public ResponseEntity<Map<String, Object>> relinkHouseholdToUser(@RequestBody Map<String,Integer> reuestMap) {
		LOGGER.info("Consumer controller:relinkHouseholdToUser ");
		Map<String, Object> relinkDetails = null;
		Integer linkHouseholdId = reuestMap.get("linkhouseholdId");
		Integer delinkhouseholdId = reuestMap.get("delinkhouseholdId");
		Integer loggedInUserId = reuestMap.get("loggedInUser");
		Integer userid = reuestMap.get("userid");
		try{
			relinkDetails = relinkHouseholdService.linkNewHousehold(linkHouseholdId,delinkhouseholdId,userid,loggedInUserId);
		}
		catch(Exception e){
			LOGGER.error("Exception occurred in relink household flow." , e);
			relinkDetails.clear();
			relinkDetails.put("Error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(relinkDetails,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(relinkDetails,HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/checkExactHouseholdExists", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Boolean> checkExactHouseholdExists(@RequestBody Household household) {
		LOGGER.info("Consumer controller: checkExactHouseholdExists ");
		return new ResponseEntity<Boolean>(consumerService.checkExactHouseholdExists(household),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/memeber/accesscode", method = {
			RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseEntity<String> findMemberAccessCode(@RequestBody String id) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Consumer service findMemberAccessCode called for input: " + SecurityUtil.sanitizeForLogging(id));
		}
		Integer householdId = Integer.parseInt(id.replaceAll("\"", EMPTY));
		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<String> accessCodes = consumerService.getPrimaryApplicantAccessCodes(householdId);
		return new ResponseEntity<String>( xstream.toXML(accessCodes),HttpStatus.OK);	

	}
	
	@RequestMapping(value = "/searchappevents", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> searchAppEvents(@RequestBody Map<String, Object> searchCriteria) {
		
		LOGGER.info("Searching application events");
		List<AppEventDto> appEventList = new ArrayList<AppEventDto>();
		String response = "";
		try {
			appEventList = consumerHistoryService.searchAppEvents(searchCriteria);
			Type listType = new TypeToken<ArrayList<AppEventDto>>(){}.getType();
			response = GhixUtils.platformGson().toJson(appEventList,listType);
		} catch (Exception e) {
			LOGGER.error("Error while searching application event", e);
			return new ResponseEntity<String>("Error sarching applicant events "+e.getMessage()
					,HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateEditMemberDetails", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateEditMemberDetails(@RequestBody CapConsumerDto capConsumerDto) {
		
		Household household = consumerService.findHouseholdById(Integer.parseInt(capConsumerDto.getIdEnc()));
		household.setFirstName(capConsumerDto.getFirstName());
		household.setLastName(capConsumerDto.getLastName());
		household.setEmail(capConsumerDto.getEmail());
		household.setPhoneNumber(capConsumerDto.getContactNumber());
		household.setUpdatedBy(Integer.parseInt(capConsumerDto.getLastUpdated()));
		household.setBirthDate(capConsumerDto.getDob());
		if(StringUtils.isEmpty(capConsumerDto.getSsn())){
			household.setSsn(null);
		}
		household.setSsnNotProvidedReason(capConsumerDto.getSsnNotProvidedReason());
		Household savedHousehold = consumerService.saveHousehold(household);
		String householdValue = null;
		try { 
			householdValue = JacksonUtils.getJacksonObjectWriter(Household.class).writeValueAsString(savedHousehold);
		} catch (JsonProcessingException e) {
			LOGGER.error("ERROR serializing HouseHold Object", e);
			throw new RestfulException("ERROR serializing HouseHold Object",HttpStatus.INTERNAL_SERVER_ERROR, e);
			
		} 
		return new ResponseEntity<String>(householdValue,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/household/caseId/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Household> findHouseholdByCaseId(@PathVariable String id) {
		LOGGER.info("Consumer Service: findHouseholdByCaseId called for input: " + id);
		Household household = null;
		try{
			household = consumerService.findHouseholdByCaseId(id);	
		}
		catch(Exception e){
			throw new RestfulException("Error in finding household by case id:"+id,HttpStatus.INTERNAL_SERVER_ERROR,e);
		}
		return new ResponseEntity<Household>(household, HttpStatus.OK);
	}
	
}

