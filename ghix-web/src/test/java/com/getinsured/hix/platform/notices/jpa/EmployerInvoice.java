package com.getinsured.hix.platform.notices.jpa;

import java.util.Date;

public class EmployerInvoice 
{
	private String employerName;
	
	private String addressLine1;
	
	private String addressLine2;
	
	private int caseId;
	
	private String invoiceNumber;
	
	private Date paymentDate;
	
	private Date statementDate;
	
	private String periodCovered;
	
	private float amountDueFromLastInvoice;
	
	private float totalPayentReceived;
	
	private float premiumThisPeriod;
	
	private float adjustments;
	
	private float exchangeFees; 

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public int getCaseId() {
		return caseId;
	}

	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public float getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}

	public void setAmountDueFromLastInvoice(float amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}

	public float getTotalPayentReceived() {
		return totalPayentReceived;
	}

	public void setTotalPayentReceived(float totalPayentReceived) {
		this.totalPayentReceived = totalPayentReceived;
	}

	public float getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(float premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public float getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(float adjustments) {
		this.adjustments = adjustments;
	}

	public float getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(float exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
}
