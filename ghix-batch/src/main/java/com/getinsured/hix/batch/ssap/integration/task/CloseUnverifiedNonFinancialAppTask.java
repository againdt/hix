package com.getinsured.hix.batch.ssap.integration.task;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.NoticeTmplHelper;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.thoughtworks.xstream.XStream;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class CloseUnverifiedNonFinancialAppTask implements Tasklet {
    
    private static final String VERIFIED = "VERIFIED";

	private static final String NON_COMPLIANCE_FOR_VERIFICATIONS = "Non-compliance for verifications.";

    private static final Logger LOGGER = Logger.getLogger(CloseUnverifiedNonFinancialAppTask.class);
    
    private static final String ELIBILITY_INDICATOR_FALSE = "FALSE";
    
    private SsapApplicationRepository ssapApplicationRepository;
    private SsapApplicantRepository ssapApplicantRepository;
    
    private EligibilityProgramRepository eligibilityProgramRepository;
    
    
    private RestTemplate restTemplate;

    private int maxAllowedDaysForVerification;
    
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private NoticeService noticeService;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	
	private NoticeTmplHelper noticeTmplHelper;
	
	private PreferencesService preferencesService;
	
	private AppEventService appEventService;
	private LookupService lookupService;
	
	private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
	private static final String APPLICATION_ELIGIBILITY_DENIED = "APPLICATION_ELIGIBILITY_DENIED";
	private static final String APPLICATION_ID  = "Application ID";
	private static final String APPLICATION_TYPE  = "Application Type";
	private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";	
	private static final String STATE_NAME = "exgStateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_NAME = "exgName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_ADDRESS_TWO = "exchangeAddress2";
	private static final String EXCHANGE_CITY_NAME = "exgCityName";
	private static final String EXCHANGE_ZIP = "zip";
	private static final String PRIMARY_APPLICANT = "primaryContact";
	private static final String APPLICANTS = "applicants";
	private static final String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	private static final String USER_NAME = "userName";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "SpecialEnrollmentEndDate";
	private static final String DATE_FORMAT_FOR_NOTICE ="MMM dd, yyyy";
	private static final String EMPTY_STRING="";
	private static final String ES = "ES";
	private static final String LOCAL_ES = "es";
	private static final String DATE_FORMAT = "MMMM dd, YYYY";
	private static final String SPANISH_DATE = "spanishDate";
	private static final String ENGLISH_DATE = "englishDate";
	private static final int SEP_DENIAL_NO_OF_DAYS_TO_APPEAL = 90;
	private static final String NUMBER_OF_DAYS = "Numberofdays";
	public static final String APPEALS_CONTENT_LOCATION="notificationTemplate/eligibilityAppealsStaticContent.html";


    private final List<String> openApplicationStatuses = Arrays.asList(
            //ApplicationStatus.SIGNED.getApplicationStatusCode(),
            //ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
            ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
    
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        LOGGER.info("CloseUnverifiedNonFinancialAppTask started on " + new Timestamp(System.currentTimeMillis()));
        DateTime expiryDateForCompletingVerifications = DateTime.now().minusDays(maxAllowedDaysForVerification);
        try {
            List<SsapApplication> unverifiedApplications = ssapApplicationRepository.getNonFinancialAppsWithExchangeEligStatusAsCAEAndEsignDateLessThan(openApplicationStatuses, expiryDateForCompletingVerifications.toDate()); 
            //SsapApplication application = unverifiedApplications.get(2);
            for(SsapApplication application : unverifiedApplications) {
                boolean householdVerified = true;
                boolean allMembersAreNotVerified = true;
                for(SsapApplicant applicant : application.getSsapApplicants()) {
                    if(StringUtils.isNotBlank(applicant.getApplyingForCoverage()) && "Y".equals(applicant.getApplyingForCoverage())) {
                        boolean residencyStatus = StringUtils.isNotBlank(applicant.getResidencyStatus()) && VERIFIED.equals(applicant.getResidencyStatus());
                        boolean citizenshipStatus = StringUtils.isNotBlank(applicant.getCitizenshipImmigrationStatus()) && VERIFIED.equals(applicant.getCitizenshipImmigrationStatus());
                        boolean vlpVerificationStatus = StringUtils.isNotBlank(applicant.getVlpVerificationStatus()) && VERIFIED.equals(applicant.getVlpVerificationStatus());
    
                        if(!residencyStatus || !(vlpVerificationStatus || citizenshipStatus)) {
                            householdVerified = false;
                            applicant.setEligibilityStatus("NONE");  
                            ssapApplicantRepository.saveAndFlush(applicant);
                        }
                        else {
                        	allMembersAreNotVerified = false;
                        }
                    }
                }
                
                if(allMembersAreNotVerified) {
                	
                	String isDisEnrolled = triggerDisenrollFlow(application);
                	if(isDisEnrolled != null && isDisEnrolled.equals(GhixConstants.RESPONSE_SUCCESS)) {
                		closeApplicationAndSendNotification(application);
                		triggerDisenrollNotification(application);                		
                	}                	
                } else if(!householdVerified) {                	
                	markApplicationEligibleAndSendNotification(application);
                	triggerDisenrollNotification(application);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error in CloseUnverifiedNonFinancialAppTask", e);
        }
        LOGGER.info("CloseUnverifiedNonFinancialAppTask finished on " + new Timestamp(System.currentTimeMillis()));
        return RepeatStatus.FINISHED;
    }
    private void logIndividualAppEvent(String eventName, String eventType,SsapApplication ssapApplication){
	       
		try {

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(APPLICATION_ID,String.valueOf(ssapApplication.getId()));
			mapEventParam.put(APPLICATION_TYPE,NON_FINANCIAL_APPLICATION);
			
			if(null !=ssapApplication.getCmrHouseoldId()){
				eventInfoDto.setModuleId(ssapApplication.getCmrHouseoldId().intValue());	
			}
			
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}
    private void triggerDisenrollNotification(SsapApplication application) throws NoticeServiceException {
    	try {
			sendNotification("SsapFinalTerminationNotification_", "SsapFinalTerminationNotification", "SsapFinalTerminationNotification", new Date(), application, cmrHouseholdRepository.findOne(application.getCmrHouseoldId().intValue()));
		} catch (Exception e) {
			LOGGER.error("Error generating Final Termination Notice");
		}		
	}
   	    
	private String triggerDisenrollFlow(SsapApplication application) {
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();
		
		disEnrollDTO.setSsapApplicationid(application.getId());
		
		SimpleDateFormat sm = new SimpleDateFormat("MM/dd/yyyy");
	   	
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE)); ;
		
		disEnrollDTO.setTerminationDate(sm.format(calendar.getTime()));
		disEnrollDTO.setTerminationReasonCode("AI");		
		
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		
		LOGGER.debug("Invoking  ind disenrollement Service");
		
		String responseMsg = "failure";
		
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			ResponseEntity<String> get_resp=  ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xStream.toXML(enrollmentRequest));
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(get_resp.getBody());
			if(null != get_resp && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode() == 200){
				responseMsg=GhixConstants.RESPONSE_SUCCESS;	
			}else{
				responseMsg=GhixConstants.RESPONSE_FAILURE;
			}
		}catch(Exception e) {
			LOGGER.error("Exception in invokeIndDisenrollment", e);
			throw new GIRuntimeException("Exception occurred while invoking ",e);
		}	
	
		return responseMsg;		
	}

	private void closeApplicationAndSendNotification(SsapApplication application) {
        application.setEligibilityStatus(EligibilityStatus.DE);
        application.setEligibilityReceivedDate(new Date());
        //As we are keeping enrollment open till last day of month we should keep application EN till last day of month
        //HIX-106694
        Calendar calendar = Calendar.getInstance();
        if(calendar.getActualMaximum(Calendar.DATE) == calendar.get(Calendar.DATE)) {
        	application.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
        }
        
        ssapApplicationRepository.saveAndFlush(application);
        logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_DENIED, application);
        List<EligibilityProgram> programs = eligibilityProgramRepository.getEligibilitiesForApplication(application.getId());
      
        for(EligibilityProgram program : programs) {
            program.setEligibilityIndicator(ELIBILITY_INDICATOR_FALSE);
            program.setIneligibleReason(NON_COMPLIANCE_FOR_VERIFICATIONS);
            eligibilityProgramRepository.save(program); 
        }
        
        // Call Eligibility notification service to send notifications.
        try {
            restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_NOTIFICATION_UPDATED_URL + application.getCaseNumber(), String.class);
        } catch (Exception exception) {
            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
        }
        
    }
	
	private void markApplicationEligibleAndSendNotification(SsapApplication application) {
        application.setEligibilityStatus(EligibilityStatus.AE);
        application.setEligibilityReceivedDate(new Date());
        ssapApplicationRepository.saveAndFlush(application);
        Map<Long, String> applicantEligibilityStatus = getApplicantEligibilityMap(application.getSsapApplicants());
        List<EligibilityProgram> programs = eligibilityProgramRepository.getEligibilitiesForApplication(application.getId());
        
        for(EligibilityProgram program : programs) {
        	if(applicantEligibilityStatus.get(program.getSsapApplicant().getId()) != null && applicantEligibilityStatus.get(program.getSsapApplicant().getId()).equals("NONE")) {
	            program.setEligibilityIndicator(ELIBILITY_INDICATOR_FALSE);
	            program.setIneligibleReason(NON_COMPLIANCE_FOR_VERIFICATIONS);
	            eligibilityProgramRepository.save(program); 
        	}
        }
        // Call Eligibility notification service to send notifications.
        try {
            restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_NOTIFICATION_UPDATED_URL + application.getCaseNumber(), String.class);
        } catch (Exception exception) {
            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
        }
        
    }

    private Map<Long, String> getApplicantEligibilityMap(
			List<SsapApplicant> ssapApplicants) {
    	Map<Long, String> applicantEligibilityStatus = new HashMap<Long, String>();
    	for (SsapApplicant ssapApplicant : ssapApplicants) {
    		applicantEligibilityStatus.put(ssapApplicant.getId(), ssapApplicant.getEligibilityStatus());
		}
		return applicantEligibilityStatus;
	}
    
	public void sendNotification(String fileName, String templateName, String relativePath, Date sepEndDate,SsapApplication ssapApplication, Household household) throws Exception {
		LOGGER.info("Sending Notification : Starts");
		
		Map<String, Object> individualTemplateData = new HashMap<String, Object>();
		List<String> sendToEmailList = new LinkedList<String>();;
		DateFormat dateFormat = null;
		DateFormat dateFormatForSpanish = null;
		try {
			
			for(SsapApplicant tmpSsapApplicant:ssapApplication.getSsapApplicants()){
				if(tmpSsapApplicant.getEligibilityStatus()==null){
					tmpSsapApplicant.setEligibilityStatus("");
				}
			}
			
			individualTemplateData.put(LifeChangeEventConstant.NOTIFICATION_SEND_DATE, new Date());
			individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			individualTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			individualTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			individualTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);			
			individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			
			individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			individualTemplateData.put(EXCHANGE_ADDRESS_TWO , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
			individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			individualTemplateData.put(EXCHANGE_ZIP , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			//set data for SEP Denial Notification
			individualTemplateData.put(NUMBER_OF_DAYS,SEP_DENIAL_NO_OF_DAYS_TO_APPEAL);
			
			//initialize the date formatter
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
			dateFormatForSpanish = new SimpleDateFormat(DATE_FORMAT,new Locale(LOCAL_ES, ES));
			//set English and Spanish date into template data
			Date todaysDate = new Date(); 
			individualTemplateData.put(ENGLISH_DATE ,dateFormat.format(todaysDate));			
			individualTemplateData.put(SPANISH_DATE ,dateFormatForSpanish.format(todaysDate));			
			
			//get name of primary applicant
			String name = household.getFirstName() + " " + household.getLastName();//LifeChangeEventUtil.getNameOfPrimaryApplicantFromSsapApplicant(ssapApplicants);
			individualTemplateData.put(PRIMARY_APPLICANT , name);
			individualTemplateData.put(USER_NAME , name);
			individualTemplateData.put(EXCHANGE_ADDRESS_EMAIL , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			individualTemplateData.put(APPLICANTS , ssapApplication.getSsapApplicants());
			if(sepEndDate!=null){
				DateFormat df = new SimpleDateFormat(DATE_FORMAT_FOR_NOTICE);
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, df.format(sepEndDate));
			}else{
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, EMPTY_STRING);
			}
			
			PreferencesDTO preferencesDTO = preferencesService.getPreferences(household.getId(), false);
			
			 
			Location location = getLocation(preferencesDTO);
			
			GhixNoticeCommunicationMethod commPref = preferencesDTO.getPrefCommunication();
			if(commPref == null){
				commPref = GhixNoticeCommunicationMethod.Mail;
			}
			
			sendToEmailList.add(preferencesDTO.getEmailAddress());
			 
			individualTemplateData.put("houseHoldAddressLine1" , location == null ? "" : location.getAddress1());
			individualTemplateData.put("houseHoldAddressLine2" , location == null ? "" : location.getAddress2() == null ? "" : location.getAddress2());
			individualTemplateData.put("houseHoldCity" , location == null ? "" : location.getCity());
			individualTemplateData.put("houseHoldState" ,location == null ? "" : location.getState());
			individualTemplateData.put("houseHoldZip" ,  location == null ? "" : location.getZip());
			
			individualTemplateData.put("eligibilityAppeals", getStaticTemplate(individualTemplateData, APPEALS_CONTENT_LOCATION));
			individualTemplateData.put("exchangeEligibilityStatus", "QHP");
			individualTemplateData.put("exchangeFAX", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put("caseNumber", ssapApplication.getCaseNumber());
			individualTemplateData.put("denielReason", NON_COMPLIANCE_FOR_VERIFICATIONS);
			individualTemplateData.put("ssapApplicationId", ssapApplication.getId());
			
			
			fileName = fileName + System.currentTimeMillis() + LifeChangeEventConstant.PDF;
			String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, individualTemplateData, relativePath, fileName,
					LifeChangeEventConstant.INDIVIDUAL, household.getId(), sendToEmailList, exchangeName, name ,location,commPref);
		} catch(Exception exception){
			LOGGER.error("Error sending notification for " + templateName);
		}
		
		LOGGER.info("Sending Notification : Ends");
	}    
	
	private String getStaticTemplate(Map<String, Object> individualTemplateData, String templateLocation) throws NoticeServiceException {

		InputStream inputStreamUrl = null;

		try {
			if("Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))){
				inputStreamUrl = getTemplateFromECM(templateLocation);
			}
			else{
				inputStreamUrl = this.getClass().getClassLoader().getResourceAsStream(templateLocation);
			}
			return populateStaticTemplateContent( individualTemplateData,  IOUtils.toString(inputStreamUrl, "UTF-8"));
		} catch (FileNotFoundException fnfe) {
			//LOGGER.error("Error in reading template from:",templateLocation);
			throw new NoticeServiceException(fnfe);
		} catch (IOException ioe) {
			//LOGGER.error("Error in reading template from:",templateLocation);
			throw new NoticeServiceException(ioe);
		}
		catch (Exception e) {
			//LOGGER.error("Error while populating the template:",e);
			throw new NoticeServiceException(e);
		}
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
		}

	}
	
	
	
	private InputStream getTemplateFromECM(String location) throws ContentManagementServiceException  {
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
			 	+ location;
			
		return new ByteArrayInputStream(noticeTmplHelper.readBytesByPath(ecmTemplateFolderPath));
		//return new ByteArrayInputStream(ecmService.getContentDataByPath(ecmTemplateFolderPath + location));
	}
	
	private String populateStaticTemplateContent(Map<String, Object> individualTemplateData, String templateContent) throws NoticeServiceException
	{
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(individualTemplateData, sw);

		} catch (Exception e) {
			LOGGER.error("Exception found while populating Static Template", e);
			throw new NoticeServiceException(e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}

		return sw.toString();
	}
		
	private Location getLocation(PreferencesDTO preferencesDTO) {
		
		if(preferencesDTO.getLocationDto() == null){
			return null;
		}
		
		Location returnLocation=new Location();
		
		returnLocation.setAddress1(preferencesDTO.getLocationDto().getAddressLine1());
		returnLocation.setAddress2(preferencesDTO.getLocationDto().getAddressLine2());
		returnLocation.setCity(preferencesDTO.getLocationDto().getCity());
		returnLocation.setCounty(preferencesDTO.getLocationDto().getCountyName());
		returnLocation.setCountycode(preferencesDTO.getLocationDto().getCountyCode());
		returnLocation.setState(preferencesDTO.getLocationDto().getState());
		returnLocation.setZip(preferencesDTO.getLocationDto().getZipcode());

		return returnLocation;
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
        return ssapApplicationRepository;
    }

    public void setSsapApplicationRepository(SsapApplicationRepository ssapApplicationRepository) {
        this.ssapApplicationRepository = ssapApplicationRepository;
    }
    
    public int getMaxAllowedDaysForVerification() {
        return maxAllowedDaysForVerification;
    }

    public void setMaxAllowedDaysForVerification(int maxAllowedDaysForVerification) {
        this.maxAllowedDaysForVerification = maxAllowedDaysForVerification;
    }
    
    public EligibilityProgramRepository getEligibilityProgramRepository() {
        return eligibilityProgramRepository;
    }

    public void setEligibilityProgramRepository(
            EligibilityProgramRepository eligibilityProgramRepository) {
        this.eligibilityProgramRepository = eligibilityProgramRepository;
    }
    
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}

	public NoticeService getNoticeService() {
		return noticeService;
	}

	public void setNoticeService(NoticeService noticeService) {
		this.noticeService = noticeService;
	}

	public CmrHouseholdRepository getCmrHouseholdRepository() {
		return cmrHouseholdRepository;
	}

	public void setCmrHouseholdRepository(
			CmrHouseholdRepository cmrHouseholdRepository) {
		this.cmrHouseholdRepository = cmrHouseholdRepository;
	}

	public SsapApplicantRepository getSsapApplicantRepository() {
		return ssapApplicantRepository;
	}

	public void setSsapApplicantRepository(
			SsapApplicantRepository ssapApplicantRepository) {
		this.ssapApplicantRepository = ssapApplicantRepository;
	}
	public AppEventService getAppEventService() {
		return appEventService;
	}
	public void setAppEventService(AppEventService appEventService) {
		this.appEventService = appEventService;
	}
	public LookupService getLookupService() {
		return lookupService;
	}
	public void setLookupService(LookupService lookupService) {
		this.lookupService = lookupService;
	}
	public PreferencesService getPreferencesService() {
		return preferencesService;
	}
	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}
	 
	public NoticeTmplHelper getNoticeTmplHelper() {
		return noticeTmplHelper;
	}
	public void setNoticeTmplHelper(NoticeTmplHelper noticeTmplHelper) {
		this.noticeTmplHelper = noticeTmplHelper;
	}
    
}