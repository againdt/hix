package com.getinsured.hix.indportal.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.hix.indportal.IndividualPortalCommons;
import com.getinsured.hix.indportal.service.IndividualPortalEnrollmentIntegrationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * 
 * @author Sunil Desu
 *
 */
@Service
public class IndividualPortalEnrollmentIntegrationServiceImpl extends IndividualPortalCommons implements IndividualPortalEnrollmentIntegrationService{

	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalEnrollmentIntegrationServiceImpl.class);

	public boolean hasActiveEnrollment(String caseNumber){
		Object applicationId = getIdFromCaseNumber(caseNumber);
		boolean hasActiveEnrollment = false;
		try {
			AccountUser user =userService.getLoggedInUser(); 
			hasActiveEnrollment = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID, 
					user.getUsername(),HttpMethod.POST,MediaType.APPLICATION_JSON, Boolean.class, Long.parseLong(applicationId.toString()))).getBody();
			LOGGER.debug("Does the application have an active enrollment? - "+hasActiveEnrollment);
		} catch (Exception e) {
			LOGGER.error("Unable to get active enrollments",e);
			persistToGiMonitor(e);
		}
		return hasActiveEnrollment;
	}

	
}
