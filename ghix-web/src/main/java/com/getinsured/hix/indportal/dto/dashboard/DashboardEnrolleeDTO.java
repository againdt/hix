package com.getinsured.hix.indportal.dto.dashboard;

public class DashboardEnrolleeDTO {

	public enum RelationEnum {
		SELF, SPOUSE, CHILD, DEPENDENT
	}

	private String memberId;
	private String guid;
	private String firstName;
	private String lastName;
	private String middleName;
	private String suffix;
	private RelationEnum relation;
	private String dob;
	private String status;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public RelationEnum getRelation() {
		return relation;
	}

	public void setRelation(RelationEnum relation) {
		this.relation = relation;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

}
