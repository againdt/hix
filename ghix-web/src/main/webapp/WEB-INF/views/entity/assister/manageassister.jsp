<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script> --%>


<style>
.small-caps label,.small-caps p {
	text-transform: uppercase;
	font-size: 11px;
}

input.checkbox {
	margin-top: 0px;
}

#assissterlist.table tr td.datecolwidth {
width:80px
}
</style>

<!-- end of secondary navbar -->
<div class="gutter10-lr">
	<div class="l-page-breadcrumb hide">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.assister.back" /></a></li>
				<li><a
					href="<c:url value="/entity/assisteradmin/manageassister"/>"><spring:message
							code="label.assister.assisters" /></a></li>
				<li><spring:message code="label.assister.manage"/></li>
			</ul>
			<!--page-breadcrumb ends-->
		</div>
		<!-- end of .row-fluid -->
	</div>
	<!--l-page-breadcrumb ends-->

	<div class="row-fluid">
		<h1>
			<spring:message code="label.assister.assisters" />
			<small>
				<c:choose>
					<c:when test="${totalassistersbysearch>1}">
					        ${totalassistersbysearch} <spring:message code="label.assister.matchingCounselors"/>
					</c:when>
					<c:otherwise>
					        ${totalassistersbysearch} <spring:message code="label.assister.assMatchingCertifiedEnrollmentCounselor"/>
					</c:otherwise>
				</c:choose>
			</small>
			<c:if test="${CA_STATE_CODE}">
				<a href="#disclaimerPopup" role="button" data-toggle="modal" style=" font-size: 13px;" class="pull-right"><spring:message code="label.assister.exportList"/></a>
			</c:if>
		</h1>
	</div>
	<!-- end of .row-fluid -->

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><spring:message code="label.assister.refineResultsBy"/><a class="pull-right" href="#" onclick="resetAll()"><spring:message code="label.assister.resetAll" /></a></h4>
			</div>
			<div class="lightgray">
				<form method="post" id="assistersearch" name="assistersearch"
					class="form-vertical gutter10" novalidate="novalidate"
					action="<c:url value="/entity/assisteradmin/manageassister/list"/>">
					<df:csrfToken/>
					
					<input type="hidden" id="sortBy" name="sortBy" >
					<input type="hidden" id="sortOrder" name="sortOrder" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="pageSize" name="pageSize" value="${pageSize + 0}">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					
					<div class="control-group">
						<label for="firstName" class="control-label"><spring:message
								code="label.assister.assisterFirstName" /></label>
						<div class="controls">
							<input type="text" name="firstName" id="firstName"
								value="${assisterSearchParameters.firstName}" class="input-medium">
						</div>
						<!-- end of controls -->
					</div>
					<div class="control-group">
						<label for="lastName" class="control-label"><spring:message
								code="label.assister.assisterLastName" /></label>
						<div class="controls">
							<input type="text" name="lastName" id="lastName"
								value="${assisterSearchParameters.lastName}" class="input-medium">
						</div>
						<!-- end of controls -->
					</div>
					<!-- end of control-group -->
					<c:if test="${CA_STATE_CODE}">
						<div class="control-group">
							<label for="assisterNumber" class="control-label"><spring:message
									code="label.assister.counselorNumber" /></label>
							<div class="controls">
								<input type="text" name="assisterNumber" id="assisterNumber" 
									value="${assisterSearchParameters.assisterNumber}" class="input-medium" maxlength="19">
									<div id="assisterNumber_error"></div>
							</div>
							<!-- end of controls -->
						</div>					
					</c:if>
					<div class="control-group">
						<label for="entity.entityName" class="control-label"><spring:message
								code="label.assister.entityName" /></label>
						<div class="controls">
							<input type="text" name="entityName"
								id="entityName"
								value="${assisterSearchParameters.entityName}"
								class="input-medium">
						</div>
						<!-- end of controls -->
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<label class="control-label" for="certificationStatus"><spring:message
								code="label.assister.status" /></label> 
						<div class="controls">
							<label class="checkbox">
								<c:choose>
									<c:when test="${assisterSearchParameters.statusActive == 'Active'}">
										<input type="checkbox" name="statusActive" id="statusActive" value="Active" checked />
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="statusActive" id="statusActive" value="Active"/>
									</c:otherwise>
								</c:choose> 
								<spring:message code="label.assister.active"/>		
							</label>
							<label class="checkbox">
								<c:choose>
									<c:when test="${assisterSearchParameters.statusInActive == 'InActive'}">
										<input type="checkbox" name="statusInActive" id=statusInActive value="InActive" checked />
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="statusInActive" id="statusInActive" value="InActive"/>
									</c:otherwise>
								</c:choose> 
								<spring:message code="label.assister.in-Active"/>	
							</label>
							</div>
						</div>
					
					<div class="control-group">
					<label class="control-label" for="certificationStatus"> <spring:message	code="label.assister.certificationStatus" /></label>
						<select id="certificationStatus"
							name="certificationStatus" class="input-medium">
							<option value=""><spring:message code="label.assister.select"/></option>
							<c:forEach items="${statusList}" var="value">
								<option value="${value}"
									<c:if test="${assisterSearchParameters.certificationStatus == value}"> SELECTED </c:if>>${value}</option>
							</c:forEach>
						</select>
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<fieldset>
							<legend class="control-label"><strong><spring:message
								code="label.assister.certificationRenewalDate" /></strong></legend>

							<label for="fromDate"><spring:message code="label.assister.from"/></label>
							<div class="controls">
								<div class="input-append date date-picker" data-date="">
									<input class="span10" type="text" name="fromDate" id="fromDate"
										value="${assisterSearchParameters.fromDate}"> <span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
								</div>
								<div id="fromDate_error"></div>								
							</div>
							<label for="toDate"><spring:message code="label.assister.to"/></label>
							<div class="controls">
								<div class="input-append date date-picker" data-date="">
									<input class="span10" type="text" name="toDate" id="toDate"
										value="${assisterSearchParameters.toDate}"> <span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
								</div>
								<div id="toDate_error"></div>
							</div>
						</fieldset>
					</div>
					<!-- end of control-group -->
					<div class="gutter10">
						<input type="submit" value="<spring:message code="label.assister.indGo"/>" aria-label="Go" class="btn input-small offset3"
							id="submit" name="submit">
					</div>
				</form>
			</div>
			<!-- end of .lightgray -->
		</div>
		<!-- end of .span3 -->

		<div id="rightpanel" class="span9">
			<form method="post" id="frmassisterList" name="frmassisterList"
				class="form-vertical" novalidate="novalidate"
				action="<c:url value="/entity/assisteradmin/manageassister/listforpagination"/>">
				<df:csrfToken/>
<!-- 				<input type="hidden" name="id" id="id" value=""> -->
<!-- 				<input type="hidden" name="pFirstName" id="pFirstName" value=""> -->
<!-- 				<input type="hidden" name="pLastName" id="pLastName" value=""> -->
<!-- 				<input type="hidden" name="pEntityName" id="pEntityName" value=""> -->
<!-- 				<input type="hidden" name="pFromDate" id="pFromDate" value=""> -->
<!-- 				<input type="hidden" name="pToDate" id="pToDate" value=""> -->
<!-- 				<input type="hidden" name="pCertificationStatus" id="pCertificationStatus" value=""> -->
<!-- 				<input type="hidden" name="pStatusActive" id="pStatusActive" value=""> -->
<!-- 				<input type="hidden" name="pStatusInactive" id="pStatusInactive" value=""> -->
<!-- 				<input type="hidden" id="pageNumber" name="pageNumber" value=""> -->
				<c:choose>
					<c:when test="${fn:length(assisterList) > 0}">
						<table class="table" id="assisterlist" name="assisterlist">
							<thead>
								<tr class="header">					
									<spring:message code="label.assister.name" var="nameVal"/>
									<c:if test="${CA_STATE_CODE}">
										<spring:message code="label.assister.counselorNumber" var="assisterNumberVal"/>
									</c:if>
									<spring:message code="label.assister.entityName" var="entityNameVal"/>
									<spring:message code="label.assister.certificationRenewal" var="reCertificationDateVal"/>
									<spring:message code="label.assister.status" var="statusVal"/>
									<spring:message code="label.assister.certificationStatus" var="certificationStatusVal"/>
									<c:if test="${CA_STATE_CODE}">
										<c:set var="assisterNumberSort" value="${sortOrder}" />
									</c:if>
									<c:set var="entitySort" value="${sortOrder}" />
                                    <c:set var="reCertDateSort" value="${sortOrder}" />
                                    <c:set var="statusSort" value="${sortOrder}" /> 
                                    <c:set var="certSort" value="${sortOrder}" />
                                   	<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${sortOrder}"></dl:sort></th>
                                    <c:if test="${CA_STATE_CODE}">
										<th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${assisterNumberVal}" sortBy="ASSISTER_NUMBER" sortOrder="${assisterNumberSort}"></dl:sort></th>
									</c:if>
								    <th class="sortable" scope="col" style="width: 95px;"><dl:sort customFunctionName="traverse" title="${entityNameVal}" sortBy="ENTITY_NAME" sortOrder="${entitySort}"></dl:sort></th>
								    <th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${reCertificationDateVal}" sortBy="RECERTIFICATION_DATE" sortOrder="${reCertDateSort}"></dl:sort></th>
								    <th class="sortable" scope="col" style="width:55px"><dl:sort customFunctionName="traverse" title="${statusVal}" sortBy="STATUS" sortOrder="${statusSort}"></dl:sort></th>
								    <th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${certificationStatusVal}" sortBy="CERTIFICATION_STATUS" sortOrder="${certSort}"></dl:sort></th>
								    <th class="sortable" scope="col" style="width:45px"><spring:message code="label.brkAction"/></th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${assisterList}" var="assister">
								<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true" /> </c:set>
								<tr>
									<td><a
										href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${assisterId}"/>">
											<c:out value="${assister.firstName} ${assister.lastName}"></c:out>
									</a></td>
									<c:if test="${CA_STATE_CODE}">
										<td>
											<c:choose>
												<c:when test = "${assister.assisterNumber == 0}">
												</c:when>
												<c:otherwise>
													<c:out value="${assister.assisterNumber}"></c:out>
												</c:otherwise>
											</c:choose>
											</td>
									</c:if>
									<td class="wordBreak"><c:out value="${assister.entity.entityName}"></c:out></td>
									
									<td class="datecolwidth"><fmt:formatDate value="${assister.reCertificationDate}"
													pattern="MM/dd/yyyy"/></td> 
									<td><c:out value="${assister.status}"></c:out></td>
									<td><c:out value="${assister.certificationStatus}"></c:out></td>
									<td>
				                 	   <div class="dropdown pull-right width50">
				                 		   <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
										   <ul class="dropdown-menu" role="menu"><li role="menuitem"><a id="editId" href="<c:url value="/entity/assisteradmin/certificationstatus?assisterId=${assisterId}"/>"class="offset1">
										   <i class="icon-pencil"></i><spring:message code="label.assister.edit"/></a></li></ul>										
										</div>									
									</td>
									
								</tr>
							</c:forEach>
							</tbody>

						</table>

						<div class="pagination center">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<dl:paginate resultSize="${totalassistersbysearch + 0}"
									pageSize="${pageSize + 0}" hideTotal="true" showDynamicPageSize="true" />
								</c:when>
								<c:otherwise>
									<dl:paginate resultSize="${totalassistersbysearch + 0}"
									pageSize="${pageSize + 0}" hideTotal="true" />
								</c:otherwise>
							</c:choose>
							
						</div>
					</c:when>
					<c:otherwise>
						<b><spring:message code="label.assister.noMatchRecFound"/></b>
					</c:otherwise>
				</c:choose>
			</form>
			
			<c:if test="${CA_STATE_CODE}">
				<div id="disclaimerPopup" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
					<form id="exportAssisterListAsExcel" name="exportAssisterListAsExcel" action="<c:url value="/entity/assisteradmin/exportAssisterList"/>" method="GET" novalidate>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3><spring:message code='label.assister.exportList.disclaimerTitle' javaScriptEscape='true'/></h3>
						</div>
						<div class="modal-body">
							<p><spring:message  code="label.assister.exportList.disclaimerMesg1"/> ${exchangeName} <spring:message  code="label.assister.exportList.disclaimerMesg2"/></p>
							<p><spring:message  code="label.assister.exportList.disclaimerMesg3"/></p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.assister.No"/></button>
		  					<input id="submitbtn" type="submit" class="btn btn-primary" value='<spring:message code="label.assister.Yes"/>'/>
						</div>
					</form>
				</div>
				</c:if>
		</div>
		<!-- end of .span9 -->
	</div>
	<!-- end of .row-fluid -->
</div>
<!-- end of .gutter10-lr -->

<script type="text/javascript">
					$(function(){$('.datepick').each(function(){$(this).datepicker({showOn:"button",buttonImage:"../resources/images/calendar.gif",buttonImageOnly:true});});});function onUpdate(){}
									$(document).ready(function(){$('table[name="brokerlist"]').tablesorter();});$('input[name="check_allActive"]').click(function(){if($('input[name="check_allActive"]').is(":checked"))
									{$('input[name="indCheckboxActive"]').each(function(){$(this).attr("checked",true);});}else{$('input[name="indCheckboxActive"]').each(function(){$(this).attr("checked",false);});}});$('input[name="indCheckboxActive"]').bind('change',function(){if($('input[name="indCheckboxActive"]').filter(':not(:checked)').length==0){$('input[name="check_allActive"]').attr("checked",true);}else{$('input[name="check_allActive"]').attr("checked",false);}});function countChecked(){var n=$("input[name='indCheckboxActive']:checked").length;var itemsSelected="<small>&#40; "+n+(n===1?" Item":" Items")+" Selected &#41;&nbsp;<\/small>";$("#counter").html(itemsSelected);if(n>0){$("#counter").show();}else{$("#counter").show();}}
									countChecked();$(":checkbox").click(countChecked);
									
					function traverse(url){
						var queryString = {};
						url.replace(
							    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
							    function($0, $1, $2, $3) { queryString[$1] = $3; }
							);
						 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
						 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
						 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
						 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
						$("#assistersearch input[type=submit]").click();
					}							
										
				</script>
<script type="text/javascript">
			$(function(){$("#getAssistance").click(function(e){e.preventDefault();var href=$(this).attr('href');if(href.indexOf('#')!=0){$('<div id="searchBox" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�<\/button><\/div><div class=""><iframe id="search" src="'+href+'" class="searchModal-body"><\/iframe><\/div><\/div>').modal();}});});function closeSearchLightBox(){$("#searchBox").remove();window.location.reload();}
					function closeSearchLightBoxOnCancel(){$("#searchBox").remove();}
					$('.date-picker').datepicker({
    	        autoclose: true,
    	        format: 'mm-dd-yyyy'
    		});
					
					jQuery.validator.addMethod("assisterNumberLength", function(value, element, param) {
						var value = $("#assisterNumber").val();
						if(value.length == 0 || $.isNumeric(value)){
							return true;
						}
						return false; 
					});
					
					var validator = $("#assistersearch").validate({ 
						onkeyup: true,
						onclick: true,
						onblur: true,
						 rules : {
							 wrapper: "li",
							 fromDate : {chkDateFormat : true},
							 toDate : {chckDate : true, chkDateFormat : true},
							 assisterNumber : {assisterNumberLength : true}
						 },
						 messages : {
							fromDate : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"},
						 	toDate : { chckDate : "<span><em class='excl'>!</em><spring:message  code='label.validateFromAndToDate' javaScriptEscape='true'/></span>",
						 		chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"},
						 		assisterNumber : { assisterNumberLength : "<span> <em class='excl'>!</em><spring:message code='err.validassisternumber' javaScriptEscape='true'/></span>" }
						 },
						 errorClass: "error",
						 errorPlacement: function(error, element) {
						 var elementId = element.attr('id');		
						 error.appendTo( $("#" + elementId + "_error"));
						}
					});	
					 
					jQuery.validator.addMethod("chckDate", function(value, element, param) {
						var fromDate=document.getElementById('fromDate').value;
						var toDate=document.getElementById('toDate').value;
						 
						fromDate = fromDate.replace(/-/g, '/');
						toDate = toDate.replace(/-/g, '/');
						 
						if((fromDate != null) &&(toDate !=null)){
							if(new Date(fromDate).getTime()  > new Date(toDate).getTime() ){
								return false;
							}  
						}
						return true;
					 });
					
					jQuery.validator.addMethod("chkDateFormat", function(value, element, param) {
						// if license renewal date is not added do not validate anything
						if(value == '') {
							return true; 
						} 
						if(value != '') {
							return /^(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$/.test(value);
						}	
					});
						
					function resetAll() {
						 var  contextPath =  "<%=request.getContextPath()%>";
		var documentUrl = contextPath + "/entity/assisteradmin/resetall";
		window
				.open(
						documentUrl,
						"_self",
						"directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
	}
		
	$("#exportAssisterListAsExcel").submit(function(){
		$("#disclaimerPopup").modal('hide');
	}); 
</script>