
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page import="java.util.Calendar"%>


<%--<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>--%>
<%--<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>--%>
<link rel="stylesheet"  type="text/css" href="../../resources/css/ghixcustom.css"/>

<script type="text/javascript">
	var app = angular.module('uploadDrugsApp', []);
	app.controller("validateCtrl",function($scope) {
		$scope.xmlConst = '<spring:message code="label.loadDrugs.fileType.xml"/>';
		$scope.jsonConst = '<spring:message code="label.loadDrugs.fileType.json"/>';
		$scope.xmlConst=$scope.xmlConst.trim();
		$scope.jsonConst=$scope.jsonConst.trim();
		$scope.invalidCarrier = true;
		$scope.invalidFile = true;
		angular.element('#fileToUpload').prop('disabled', true);

		$scope.resetForm =  function() {
			$scope.invalidCarrier = true;
			$scope.invalidFile = true;
			$scope.frmUploadDrugs.$dirty = false;
			$scope.frmUploadDrugs.$pristine = true;
			$scope.frmUploadDrugs.$submitted = false;
			angular.element('#fileToUpload').prop('disabled', true);
		}

		$scope.enableFileNode = function() {
			angular.element("#fileToUpload").prop('disabled', false);
			angular.element("#fileToUpload").val(null);
			$scope.invalidFile = true;
			$scope.frmUploadDrugs.fileToUpload.$dirty = false;
		}

		$scope.hasValidFile =  function(file) {
			var hasValid = false;

			if (file) {

				var fileTypeVal = $scope.frmUploadDrugs.fileType.$modelValue;

				if ($scope.xmlConst == fileTypeVal && file.type == "text/xml") {
					hasValid = true;
				}
				else if ($scope.jsonConst == fileTypeVal) {
					var regexJsonFile = new RegExp(".json$", "i");
					hasValid = regexJsonFile.test(file.name);
				}
			}
			return hasValid;
		}
	});

	var INTEGER_REGEXP = /^-?\d+$/;
	app.directive('carrierValidation', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attr, mCtrl) {
				function carrierValidation(value) {
					var hasValidValue = false;
					scope.invalidCarrier = true;
//					debugger;
					if (INTEGER_REGEXP.test(value)) {
						hasValidValue = true;
						scope.invalidCarrier = false;
					}
					else if (!value) {
						hasValidValue = true;
					}
					mCtrl.$setValidity('carrierValidation', hasValidValue);
					return value;
				}
				mCtrl.$parsers.push(carrierValidation);
			}
		};
	});

	app.directive('filesValidation', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attr, mCtrl) {

				element.bind('change', function() {
//					debugger;
					mCtrl.$dirty = true;
					angular.element("#fileValid").html("");
					scope.invalidFile = false;
					var errorMessage = null;

					if (this.files.length == 0) {
						errorMessage = '<spring:message code="label.loadDrugs.selectDrugFile.required"/>';
						scope.invalidFile = true;
					}
					else if (!scope.hasValidFile(this.files[0])) {

						var fileTypeVal = scope.frmUploadDrugs.fileType.$modelValue;

						if (scope.xmlConst == fileTypeVal) {
							errorMessage = '<spring:message code="label.loadDrugs.selectDrugFile.requiredXML"/>';
						}
						else if (scope.jsonConst == fileTypeVal) {
							errorMessage = '<spring:message code="label.loadDrugs.selectDrugFile.requiredJSON"/>';
						}
						else {
							errorMessage = '<spring:message code="label.loadDrugs.selectDrugFile.requiredFILE"/>';
						}
						scope.invalidFile = true;
					}

					if (scope.invalidFile) {
						angular.element("#fileValid").html(errorMessage);
					}
					mCtrl.$valid = !scope.invalidFile;
					mCtrl.$setValidity('filesValidation', !scope.invalidFile);

					scope.$apply(function () {
						mCtrl.$render();
					});
				});
			}
		};
	});
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>

<div class="row-fluid" id="main">
	<div class="gutter10">
		<div class="row-fluid">
			<h1 id=""><spring:message code="label.loadDrugs.header"/></h1>
		</div><!-- end of .row-fluid -->

		<div class="row-fluid">
			<div id="sidebar" class="span3">
				<div class="header"><h4 class="margin0"><spring:message code="label.loadDrugs.header.process"/></h4></div>
				<ul class="unstyled">
					<li><b><spring:message code="label.step_1"/>: </b><spring:message code="label.loadDrugs.step1"/></li>
					<li><b><spring:message code="label.step_2"/>: </b><spring:message code="label.loadDrugs.step2"/></li>
					<li><b><spring:message code="label.step_3"/>: </b><spring:message code="label.loadDrugs.step3"/></li>
					<li><b><spring:message code="label.step_4"/>: </b><spring:message code="label.loadDrugs.step4"/></li>
					<li><b><spring:message code="label.step_5"/>: </b><spring:message code="label.loadDrugs.step5"/></li>
				</ul>
			</div><!-- end of .span3 -->

			<div class="span9" id="rightpanel" ng-app="uploadDrugsApp" ng-controller="validateCtrl">
				<div class="header" style="height:40px"></div>
				<form name="frmUploadDrugs" ng-model="frmUploadDrugs" action="${pageContext.request.contextPath}/admin/serff/uploadDrugs" enctype="multipart/form-data" method="POST">
					<df:csrfToken />
					<div class="control-group">
						<c:if test="${uploadStatus != null}">
							<div style="color: red">${uploadStatus}<br /><br /></div>
						</c:if>
						<c:if test="${uploadSucess != null}">
							<div style="color: green">${uploadSucess}<br /><br /></div>
						</c:if>

						<label for="carrier" class="control-label"><b><spring:message code="label.step_1"/>:</b> <spring:message code="label.loadDrugs.selectIssuer"/>&nbsp;<span style="color:red">*</span>&nbsp;&nbsp;</label>
						<select name="carrier" ng-model="carrier" onchange="$('#carrierText').val(this.options[this.selectedIndex].text);" required carrier-validation>
							<option value="" selected="selected"><spring:message code="label.selectOption"/></option>
							<c:if test="${issuerList != null}">
								<c:forEach var="issuer" items="${issuerList}">
									<c:if test="${issuer.hiosIssuerId != null}">
										<option value="${issuer.hiosIssuerId}">[${issuer.hiosIssuerId}] ${issuer.shortName}</option>
									</c:if>
								</c:forEach>
							</c:if>
						</select>
						<span style="color:red" ng-show="frmUploadDrugs.carrier.$dirty && frmUploadDrugs.carrier.$invalid">
							<span ng-show="frmUploadDrugs.carrier.$error.required"><spring:message code="label.loadDrugs.selectIssuer.required"/></span>
							<span ng-show="frmUploadDrugs.carrier.$error.carrierValidation"><spring:message code="label.loadDrugs.selectIssuer.carrierValidation"/></span>
						</span>
						<input type="hidden" id="carrierText" name="carrierText" />
					</div>

					<div class="control-group">
						<c:set var="currentYear"><%=Calendar.getInstance().get(Calendar.YEAR)%></c:set>
						<label for="selectYear" class="control-label"><b><spring:message code="label.step_2"/>:</b> <spring:message code="label.loadDrugs.selectYear"/>&nbsp;<span style="color:red">*</span>&nbsp;&nbsp;</label>
						<select size="1" id="selectYear" name="selectYear" required>
							<option value="${currentYear - 1}">${currentYear - 1}</option>
							<option value="${currentYear}" selected>${currentYear}</option>
							<option value="${currentYear + 1}">${currentYear + 1}</option>
						</select>
						<span style="color:red" ng-show="frmUploadDrugs.selectYear.$dirty && frmUploadDrugs.selectYear.$invalid">
							<span ng-show="frmUploadDrugs.selectYear.$error.required"><spring:message code="label.loadDrugs.selectYear.required"/></span>
						</span>
					</div>

					<div class="control-group">
						<div>
							<label class="pull-left" for="fileType"><b><spring:message code="label.step_3"/>:</b> <spring:message code="label.loadDrugs.fileType"/>&nbsp;</label><span style="color:red">*</span></label>
							&nbsp; &nbsp;
							<input type="radio" ng-model="fileType" name="fileType" value="XML" ng-click="enableFileNode()" required><spring:message code="label.loadDrugs.fileType.xml"/>
							<input type="radio" ng-model="fileType" name="fileType" value="JSON" ng-click="enableFileNode()" required><spring:message code="label.loadDrugs.fileType.json"/>
							<span style="color:red" ng-show="frmUploadDrugs.fileType.$dirty && frmUploadDrugs.fileType.$invalid">
								<span ng-show="frmUploadDrugs.fileType.$error.required"><spring:message code="label.loadDrugs.fileType.required"/></span>
							</span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="fileToUpload"><b><spring:message code="label.step_4"/>:</b> <spring:message code="label.loadDrugs.selectDrugFile"/>&nbsp;<span style="color:red">*</span></label>
						<div class="controls">
							<input type="file" class="filestyle" id="fileToUpload" ng-model="fileToUpload" name="fileToUpload" data-rule-required="true" accept=".xml,.json" data-classButton="btn" required files-validation />
							<br /><span style="color:red" ng-show="frmUploadDrugs.fileToUpload.$dirty && frmUploadDrugs.fileToUpload.$invalid">
								<span id="fileValid" name="fileValid" ng-show="frmUploadDrugs.fileToUpload.$error.filesValidation"></span>
							</span>
						</div>
					</div>

					<div class="control-group">
						<div class="span2">
							<input class="btn offset2" type="reset" ng-click="resetForm()" value="Cancel" />
						</div>
						<div class="span2">
							<input class="btn btn-primary offset1" type="submit" ng-model="saveBtn" name="saveBtn" value="Save" ng-disabled="invalidCarrier || invalidFile" />
						</div>
					</div>
				</form>
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
	</div> <!-- end of .gutter10 -->
</div><!--end of row-fluid Main-->

