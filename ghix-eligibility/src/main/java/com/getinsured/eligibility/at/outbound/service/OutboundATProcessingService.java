package com.getinsured.eligibility.at.outbound.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.iex.ssap.model.OutboundATApplicant;
import com.getinsured.iex.ssap.model.OutboundATApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;

public interface OutboundATProcessingService {
	
	void logOutboundAccountTransfer(Long applicationId,Map<String,Object> outboundAtDetailsMap, Integer giWsPayloadId, String status);
	
	public Map<String,Object> prepareOutboundAtDBLoggingDetails(Long applicationId);
	
	public GIWSPayload populateGiWSPayload(Long ssapAppId,Map<String,Object> outboundAtDetailsMap);
	
	public OutboundATApplication populateOutboundATApplication(Long applicationId,String caseNumber,Integer giWsPayloadId,String status);
	
	public List<OutboundATApplicant> populateOutboundAtApplicants(Long applicationId,List<SsapApplicant> applicants);
	
	public void performOutboundAtDBLogging(OutboundATApplication application,List<OutboundATApplicant> applicants);

	boolean compareNewAndEnrolledApplication(Long currentApplicationId);

}
