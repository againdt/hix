package com.getinsured.iex.util;

import java.io.IOException;

import com.getinsured.iex.erp.org.nmhix.ssa.person.GenderEnum;
import com.getinsured.iex.erp.org.nmhix.ssa.person.SuffixEnum;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * @author chopra_s
 * 
 */
public class IexEnumAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> type) {
		Class<? super T> rawType = type.getRawType();
		if (rawType == SuffixEnum.class) {
			return new NameSuffixTypeAdapter<T>();
		}
		else if (rawType == GenderEnum.class) {
			return new GenderTypeAdapter<T>();
		}
		return null;
	}

	public class NameSuffixTypeAdapter<T> extends TypeAdapter<T> {
		public void write(JsonWriter out, T value) throws IOException {
			if (value == null) {
				out.nullValue();
				return;
			}
			SuffixEnum obj = (SuffixEnum) value;
			out.value(obj.value());
		}

		public T read(JsonReader in) throws IOException {
			return null;
		}
	}
	
	public class GenderTypeAdapter<T> extends TypeAdapter<T> {
		public void write(JsonWriter out, T value) throws IOException {
			if (value == null) {
				out.nullValue();
				return;
			}
			GenderEnum obj = (GenderEnum) value;
			out.value(obj.value());
		}

		public T read(JsonReader in) throws IOException {
			return null;
		}
	}
}
