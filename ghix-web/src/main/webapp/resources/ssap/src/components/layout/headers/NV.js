import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Impersonation  from './Impersonation';
import CONFIG from '../../../.env';

function NevadaHeader ({headerLinks, handleOpeningModal, refs}) {
    const { anonymous, impersonation, impersonationDetails, exchangeName } = headerLinks;
    //const {activeModuleName} = headerLinks.user
    const {
        SETTINGS: accountSettings,
        ASSISTANCE: assistance,
        DASHBOARD: dashboard,
        // HELP: help,
        // HOME: home,
        LOGIN: login,
        LOGOUT: logout,
        CONTACT_US: contactUs,
        HELP: help,
        UNREAD_MESSAGES: unReadMsg,
    } = headerLinks.links;

    const { LOGO: logo } = headerLinks.assets;
    // const {
    //     GOOGLE_ANALYTICS_CODE: trackingCode,
    //     GOOGLE_ANALYTICS_CONTAINER_ID: gtmContainerID
    // } = headerLinks.properties;

    const { btnRef, btn2Ref, navRef, ulRef, ul2Ref } = refs;

    const logoForDevelopment = '../../../assets/images/logo_nv.png';

    return(
        <>
            <a className="usa-skipnav" href="#main-content">Skip to main content</a>
            <header className="usa-header usa-header-basic" id="masthead" role="banner">
                <div className="usa-nav-container masthead__container">
                    <div className="usa-navbar">
                        <div className="usa-logo logo" id="masthead-logo-wrap">
                            <a href="#" className="">
                                <img id="masthead-logo-img_02" className="masthead__logo__img" src={CONFIG.ENV.DEVELOPMENT ? logoForDevelopment : logo} alt={exchangeName} width="150"/>
                            </a>
                        </div>
                        <div className="usa-menu-btn masthead__nav__btn--collapse">
                            {/*<FontAwesomeIcon icon="bars" />*/}
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </div>
                    </div>
                    <nav role="navigation" ref={navRef} className="usa-nav main__nav">
                        <button className="usa-nav-close usa-nav__close">
                            <img src="../resources/ssap/assets/images/close.svg" alt="close" />
                        </button>
                        {
                            anonymous ?
                                <ul id="navbar-links-container" className="usa-nav-primary usa-nav__primary usa-accordion main__nav__links_container">
                                    <li className="usa-nav__primary-item"><a id="authentication_01" href={login}>Login</a></li>
                                    <li className="usa-nav__primary-item">
                                        <button id="get-assistance_01" ref={btnRef} className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--01" aria-controls="basic-nav-section-one" aria-expanded="false">
                                            <span>Help & Support</span>
                                        </button>
                                        <ul id="basic-nav-section-one" ref={ulRef} className="usa-nav-submenu main__nav__submenu" aria-hidden="true">
                                            <li className="usa-nav__submenu-item">
                                                <a href={assistance} id="find-assistance_01" onClick={handleOpeningModal}>Find Local Assistance</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={help} id="phone_01">Frequently Asked Questions</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={contactUs} id="phone_01">Contact Us</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                : <ul className="usa-nav-primary usa-nav__primary usa-accordion main__nav__links_container">
                                    <li className="usa-nav__primary-item"><a id="home" className="usa-nav-link" aria-label="Home" href={dashboard}>
                                        <FontAwesomeIcon icon="home" />
                                    </a></li>
                                    <li className="usa-nav__primary-item"><a id="secured-mail_01" className="usa-nav-link secured-mail" aria-label="Inbox" href={unReadMsg}>
                                        <FontAwesomeIcon className="lock" icon="lock" /><FontAwesomeIcon className="envelope" icon="envelope" />
                                    </a></li>
                                    <li className="usa-nav__primary-item">
                                        <button id="get-assistance_02" ref={btnRef} className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--01" aria-expanded="false" aria-controls="basic-nav-section-one">
                                            <span>Help & Support</span>
                                        </button>
                                        <ul id="basic-nav-section-one" ref={ulRef} className="usa-nav-submenu main__nav__submenu" aria-hidden="true">
                                            <li className="usa-nav__submenu-item">
                                                <a href={assistance} id="find-assistance_02" onClick={handleOpeningModal}>Find Local Assistance</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={help} id="phone_01">Frequently Asked Questions</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={contactUs} id="phone_01">Contact Us</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="usa-nav__primary-item">
                                        <button id="my-account" ref={btn2Ref} className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--02" aria-expanded="false" aria-controls="basic-nav-section-two">
                                            <span>My Account</span>
                                        </button>
                                        <ul id="basic-nav-section-two" ref={ul2Ref} className="usa-nav-submenu  main__nav__submenu" aria-hidden="true">
                                            <li className="usa-nav__submenu-item">
                                                <a href={accountSettings} id="account-settings_01">Account Settings</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={dashboard} id="dashboard_01">Dashboard</a>
                                            </li>
                                            <li className="usa-nav__submenu-item">
                                                <a href={logout} id="logout_01">Logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                        }
                    </nav>
                </div>
            </header>
            {impersonation && impersonationDetails && impersonationDetails.name &&
                <Impersonation {...{impersonationDetails}} />
            }
        </>
    );
}

export default NevadaHeader;
