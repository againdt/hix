/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.converter;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.iex.ssap.enums.SuffixEnum;

/**
 * @author chopra_s
 * 
 */
@Component("dozzerNameSuffixConverter")
@Scope("singleton")
public class NameSuffixConverter extends DozerConverter<com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String, String> {

	public NameSuffixConverter() {
		super(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String.class, String.class);
	}

	@Override
	public com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String convertFrom(String arg0, com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String arg1) {
		return null;
	}

	@Override
	public String convertTo(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String source, String dest) {
		if (source != null) {
			String data = source.getValue();
			for (SuffixEnum c : SuffixEnum.values()) {
				if (c.value().equals(data)) {
					return c.value();
				}
			}
		}
		return null;
	}

}
