package com.getinsured.hix.entity.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.repository.IAssisterRepository;
import com.getinsured.hix.model.entity.Assister;

/**
 * Implements {@link AssisterService} to perform Assister related operations
 * like generate record id etc.
 * 
 */
@Service("assisterService")
@Transactional
public class AssisterServiceImpl implements AssisterService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterServiceImpl.class);
	
	@Autowired
	private IAssisterRepository assisterRepository;

	/**
	 * @see AssisterService#generateAssisterRecordId(Assister)
	 */
	@Override
	@Transactional
	public Integer generateAssisterRecordId(Assister assister) {
		
		LOGGER.info("generateAssisterRecordId: START");
		
		Assister newAssister = null;
		Integer assisterId = null;
		if (assister != null) {
			newAssister = assisterRepository.save(assister);
			assisterId = newAssister.getId();
		}
		
		LOGGER.info("generateAssisterRecordId: END");
		
		return assisterId;
	}

	/**
	 * @see AssisterService#findById(int)
	 */
	@Override
	public Assister findById(int assisterId) {
		
		LOGGER.info("findById: START");
		LOGGER.info("findById: END");
		
		return assisterRepository.findOne(assisterId);
	}

}