package com.getinsured.eligibility.at.ref.service;

import java.util.Map;

public class ReferralNoticeRequest {

	private String noticeName;
	private String caseNumber;
	private long enrolledApplicationId;
	
	private Map<String, String> noticeData;
	public String getNoticeName() {
		return noticeName;
	}
	public void setNoticeName(String noticeName) {
		this.noticeName = noticeName;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public long getEnrolledApplicationId() {
		return enrolledApplicationId;
	}
	public void setEnrolledApplicationId(long enrolledApplicationId) {
		this.enrolledApplicationId = enrolledApplicationId;
	}
	public Map<String, String> getNoticeData() {
		return noticeData;
	}
	public void setNoticeData(Map<String, String> noticeData) {
		this.noticeData = noticeData;
	}
}
