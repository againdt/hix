/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;


import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.enrollment._1095.cy_2018.treasury.irs.common.BatchCategoryCodeType;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095Factory;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095Inbound;
import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentIn1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentOut1095Repository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.ZipHelper;
import com.getinsured.hix.enrollment1095.service.Enrollment1095NotificationService;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentIn1095;
import com.getinsured.hix.model.enrollment.EnrollmentInDtl1095;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;



/**
 * @author negi_s
 *
 */
@Service("enrollment1095AInboundBatchService")
public class Enrollment1095AInboundBatchServiceImpl implements Enrollment1095AInboundBatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095AInboundBatchServiceImpl.class);
	private static final String ANNUAL_NACK_REPSONSE = "EOYNAK";
	private static final String ANNUAL_ACK_REPSONSE = "EOYACK";
	private static final String ANNUAL_OUT_REPSONSE = "EOYOUT";

	private static final String ANNUAL_NACK_FOLDER = "NACK_1095";
	private static final String ANNUAL_ACK_FOLDER = "ACK_1095";
	private static final String ANNUAL_OUT_FOLDER = "OUT_1095";
	private static final String OUT = ".OUT";

	private static final String ACK = "ack";
	private static final String NACK = "nack";
	private static final String MANIFEST = "manifest";


	private static final String DO_NOTHING = "DO_NOTHING";
	private static final String MARK_ACK ="MARK_ACK";
	private static final String MARK_SUCCESS = "MARK_SUCCESS";
	private static final String MANUAL_CORRECTION = "MANUAL_CORRECTION";
	private static final String RESUB_XML = "RESUB_XML";
	private static final String REGEN_XML = "REGEN_XML";
	private static final String REGEN_ZIP = "REGEN_ZIP";
	private static final String REGEN_CORRECTED = "REGEN_CORRECTED";
	private static final String SUCCESS = "SUCCESS";
	private static final String ACKFLAG = "ACK";


	@Autowired private IEnrollmentIn1095Repository enrollmentIn1095Repository;
	@Autowired private IEnrollmentOut1095Repository enrollmentOut1095Repository;
	@Autowired private IEnrollment1095Repository enrollment1095Repository;
	@Autowired private Enrollment1095NotificationService enrollment1095NotificationService;
	@Autowired private EnrollmentAnnualIrsBatchService enrollmentAnnualIrsBatchService;
	@Autowired private Enrollment1095Factory enrollment1095Factory;

	@Override
	public void processInboundResponse(Long jobExecutionId) throws GIException {
		// Move File to WIP Folder
		/*
		 * IRS_1095_RESPONSE ( Main folder )
		 * 		WIP_<TimeStamp>
		 * 			ACK_1095
		 * 			NACK_1095
		 * 			OUT_1095
		 */
		String irs1095ResponsePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN).toString();
		//String irs1095ResponsePath = "D:\\vino\\D2C\\TestIRS";

		File[] allWipDirForDelete = getWipDirList(irs1095ResponsePath,OUT);

		if(allWipDirForDelete != null && allWipDirForDelete.length > 0){
			for (File wipDir : allWipDirForDelete) {
				deleteWipFolder(wipDir);
			}
		}

		File[] allInFileForMail = EnrollmentUtils.getFilesInAFolderByName(irs1095ResponsePath, OUT);

		if(allInFileForMail != null && allInFileForMail.length > 0){
			sendenrollment1095InEmail(allInFileForMail);
		}



		String wIPFolder = "WIP_"+new Date().getTime();
		String irs1095ResponseWipPath = irs1095ResponsePath+ File.separator + wIPFolder;

		//Filter Files
		File[] irsAckResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irs1095ResponsePath, ANNUAL_ACK_REPSONSE);
		File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irs1095ResponsePath, ANNUAL_NACK_REPSONSE );
		File[] irsOutResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irs1095ResponsePath, ANNUAL_OUT_REPSONSE);

		try{
			//Move file to WIP folder
			if(irsAckResponseFiles != null && irsAckResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irs1095ResponseWipPath + File.separator + ANNUAL_ACK_FOLDER, irsAckResponseFiles);
			}

			if(irsNackResponseFiles != null && irsNackResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irs1095ResponseWipPath + File.separator + ANNUAL_NACK_FOLDER, irsNackResponseFiles);
			}

			if(irsOutResponseFiles != null && irsOutResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irs1095ResponseWipPath + File.separator + ANNUAL_OUT_FOLDER, irsOutResponseFiles);
			}

		}catch(Exception e){
			throw new GIException("Error in moving response files" , e);
		}

		try{

			//Process Files
			processInboundAckResponse(irs1095ResponseWipPath + File.separator + ANNUAL_ACK_FOLDER, jobExecutionId);
			processInboundNackResponse(irs1095ResponseWipPath + File.separator + ANNUAL_NACK_FOLDER, jobExecutionId);
			processInboundOutResponse(irs1095ResponseWipPath + File.separator + ANNUAL_OUT_FOLDER, jobExecutionId);

		}catch(Exception e){
			throw new GIException("Error in moving response files" , e);
		}finally{
			deleteWipFolder(new File(irs1095ResponseWipPath));
		}

		List<EnrollmentIn1095> processList = reSubmittFileToIrs(jobExecutionId);

		if(processList != null && processList.size() > 0){
			sendenrollment1095InProcessEmail(processList);
		}
	}

	private void deleteWipFolder(File irs1095ResponseWipPath) {
		try{
			//FileUtils.deleteQuietly(new File(irs1095ResponseWipPath));
			if (irs1095ResponseWipPath.isDirectory()) {
				File[] files = irs1095ResponseWipPath.listFiles();
				if (files != null && files.length > 0) {
					for (File aFile : files) {
						deleteWipFolder(aFile);
					}
				}
				irs1095ResponseWipPath.delete();
			}else {
				irs1095ResponseWipPath.delete();
			}
		}catch(Exception ex){
			LOGGER.error("Error Occurred while deleting WIP folder", ex);
		}
	}

	private List<EnrollmentIn1095> reSubmittFileToIrs(Long jobExecutionId) throws GIException{

		List<EnrollmentIn1095> in1095AllList = enrollmentIn1095Repository.getbyJobExecutionId(String.valueOf(jobExecutionId));
		List<EnrollmentIn1095> in1095ReSubXMLList = new ArrayList<>();
		if(in1095AllList != null && in1095AllList.size() > 0 ){
			for (EnrollmentIn1095 enrollmentIn1095 : in1095AllList) {
				String giAction = enrollmentIn1095.getErrorCodeLkp().getLookupValueLabel();

				switch (giAction) {
				case DO_NOTHING:
					doNothing(enrollmentIn1095);
					break;
				case MARK_ACK:
					updateEnrollment1095(enrollmentIn1095, ACKFLAG);
					break;
				case MARK_SUCCESS:
					updateEnrollment1095(enrollmentIn1095, SUCCESS);
					break;
				case MANUAL_CORRECTION:
					updateEnrollment1095ForManualCorrection(enrollmentIn1095, MANUAL_CORRECTION);
					break;
				case RESUB_XML:
					addToResubXML(in1095ReSubXMLList,enrollmentIn1095);
					break;
				case REGEN_XML:
					updateEnrollment1095(enrollmentIn1095, REGEN_XML);
					break;
				case REGEN_ZIP:
					updateEnrollment1095(enrollmentIn1095, REGEN_ZIP);
					break;
				case REGEN_CORRECTED :
					updateEnrollment1095(enrollmentIn1095, REGEN_CORRECTED);
					break;
				}
			}
		}
		resubXML(in1095ReSubXMLList);
		// FOR Process Mail
		List<EnrollmentIn1095> in1095List = new ArrayList<>();
		in1095List.addAll(in1095AllList);

		return in1095List;
	}

	private void updateEnrollment1095(EnrollmentIn1095 enrollmentIn1095, String update) {
		String batchId = enrollmentIn1095.getBatchId();
		String documentId = enrollmentIn1095.getDocumentSeqId();
		String enrollmentIds = null;

		try{
			if(batchId != null && documentId != null && !REGEN_ZIP.equalsIgnoreCase(update)){
				if(documentId.contains("|")){
					String documentIds [] = documentId.split("\\|");
					StringBuilder enrollmentIdBuilder = new StringBuilder();
					for (String docId : documentIds) {
						enrollmentIdBuilder.append(enrollmentOut1095Repository.getEnrollmentIds(batchId, docId)).append(",");
					}
					enrollmentIds = enrollmentIdBuilder.deleteCharAt(enrollmentIdBuilder.length()-1).toString();
				}else{
					enrollmentIds = enrollmentOut1095Repository.getEnrollmentIds(batchId, documentId);
				}

			}else if(batchId != null && documentId == null && REGEN_ZIP.equalsIgnoreCase(update)){
				List<String> enrollmentIdList = enrollmentOut1095Repository.getEnrollmentIdsByBatchId(batchId);
				StringBuilder enrollmentIdBuilder = new StringBuilder();
				for(String enrollmentIdClob : enrollmentIdList){
					enrollmentIdBuilder.append(enrollmentIdClob).append(",");
				}
				enrollmentIds = enrollmentIdBuilder.deleteCharAt(enrollmentIdBuilder.length()-1).toString();
			}else{
				throw new GIException("Error spliting BatchId = "+ batchId +" or Document Seq Id =  "+documentId );
			}
		}catch(Exception exception){
			LOGGER.error("Error spliting BatchId = "+ batchId +" or Document Seq Id =  "+documentId , exception);
		}


		if(enrollmentIds != null){
			String[] enrollmentArray = enrollmentIds.split(",");

			if(enrollmentArray != null && enrollmentArray.length > 0){
				List<Integer> enrollmentList = new ArrayList<>();
				for (String string : enrollmentArray) {
					enrollmentList.add(Integer.valueOf(string));
				}
				List<Enrollment1095> enrollment1095List = new ArrayList<Enrollment1095>();
				int size = enrollmentList.size();
				if (size > 1000) {
					int numberOfPartitions = size / 1000;
					if (size % 1000 != 0) {
						numberOfPartitions++;
					}
					int firstIndex = 0;
					int lastIndex = 0;
					for (int i = 0; i < numberOfPartitions; i++) {
						firstIndex = i * 1000;
						lastIndex = (i + 1) * 1000;
						if (lastIndex > size) {
							lastIndex = size;
						}
						enrollment1095List.addAll(enrollment1095Repository.getEnrollment1095ByExchgAsignedPolicyId(enrollmentList.subList(firstIndex, lastIndex)));
					}
				} else {
					enrollment1095List = enrollment1095Repository.getEnrollment1095ByExchgAsignedPolicyId(enrollmentList);
				}

				for (Enrollment1095 enrollment1095 : enrollment1095List) {
					enrollment1095.setInboundBatchCategoryCode(enrollmentIn1095.getBatchCategoryCode());
					enrollment1095.setEnrollmentIn1095Id(enrollmentIn1095.getId());
					enrollment1095.setInboundAction(update);

					enrollment1095Repository.save(enrollment1095);
				}

				enrollmentIn1095.setIsProcessedFlag(EnrollmentConstants.Y);
				enrollmentIn1095Repository.saveAndFlush(enrollmentIn1095);

			}
		}}

	private void addToResubXML(List<EnrollmentIn1095> in1095ReSubXMLList, EnrollmentIn1095 enrollmentIn1095){
		updateEnrollment1095(enrollmentIn1095,RESUB_XML);
		in1095ReSubXMLList.add(enrollmentIn1095);
	}

	private void resubXML(List<EnrollmentIn1095> in1095ReSubXMLList) {
		/*
		 * 	1. Pull XML file from Archive
			2. Package REGEN XMLs into one of many packages based on their Original Batch ID and Batch Category Code. For example REGEN XMLs belonging to a Batch Category Codes IRS_EOY_REQ should be seperated from IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ and IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ ones in seperate package. Similarly REGEN XMLs belonging to seperate Original Batch ID should be packaged into seperate packages.
			3. Generate new Batch ID for each package. 
			4. Generate new Manifest for each package. Only include REGEN XMLs and do not include any new transactions, or mix with corrections or void transactions.
			5. Use IRS_EOY_RESUBMIT_ REJECTED_OR_MISSING_FILE_REQ as the Batch Category Code for the package.
			5. Reference Original Batch ID
			6. Resubmit & Alert Email
			7. Correction Indicators should NOT be set in the XML.
		 */
		Map<String,List<String>> batchIdMap = new HashMap<>();
		Map<String,List<EnrollmentIn1095>> enr1095IdMap = new HashMap<>();
		if(in1095ReSubXMLList != null && in1095ReSubXMLList.size() > 0 ){
			for (EnrollmentIn1095 enrollmentIn1095 : in1095ReSubXMLList) {
				String batchId = enrollmentIn1095.getBatchId();
				String previousBatchId = enrollmentIn1095.getPreviousBatchId();
				if(previousBatchId == null){
					previousBatchId = getPreviousBatchId(batchId, enrollmentIn1095.getDocumentSeqId());
				}
				String key = null;

				if(previousBatchId != null){
					key = batchId+"|"+previousBatchId+"|"+enrollmentIn1095.getBatchCategoryCode();
				}else{
					key = batchId+"|"+enrollmentIn1095.getBatchCategoryCode();
				}

				if(batchIdMap.containsKey(key)){
					List<String> tempList = batchIdMap.get(key);
					tempList.add(enrollmentIn1095.getDocumentSeqId());

					List<EnrollmentIn1095> tempIdList = enr1095IdMap.get(key);
					tempIdList.add(enrollmentIn1095);

				}else{
					List<String> tempList = new ArrayList<>();
					tempList.add(enrollmentIn1095.getDocumentSeqId());

					batchIdMap.put(key, tempList);

					List<EnrollmentIn1095> tempInList = new ArrayList<>();
					tempInList.add((enrollmentIn1095));

					enr1095IdMap.put(key, tempInList);
				}
			}

			for (Map.Entry<String,List<String>> entry : batchIdMap.entrySet()){
				String batchIdAndCode = entry.getKey();
				List<String> documentList = entry.getValue();

				// Split Documnet Seq Id as it may contains "|"
				List<String> newDocumentList = new ArrayList<>();
				for (String docId : documentList) {
					if(docId.contains("|")){
						newDocumentList.addAll(Arrays.asList(docId.split("\\|")));
					}else{
						newDocumentList.add(docId);
					}
				}

				resendXML(batchIdAndCode, newDocumentList, enr1095IdMap.get(batchIdAndCode));
			}
		}
	}

	private String getPreviousBatchId(String batchId, String documentSeqId) {
		String enrollmentIds = enrollmentOut1095Repository.getEnrollmentIds(batchId, documentSeqId);
		String originalBatchId = null;
		if(enrollmentIds != null){
			String[] enrollmentArray = enrollmentIds.split(",");
			if(enrollmentArray != null && enrollmentArray.length > 0){
				originalBatchId = enrollment1095Repository.findOriginalBatchId(Integer.valueOf(enrollmentArray[0]));
			}
		}
		return originalBatchId;
	}

	private void updateEnrollment1095ForManualCorrection(EnrollmentIn1095 enrollmentIn1095, String update) throws GIException {
		List<EnrollmentInDtl1095> enrlDtlList = enrollmentIn1095.getEnrollmentInDtl1095List();
		List<Integer> enrollmentList = new ArrayList<>();

		for (EnrollmentInDtl1095 enrollmentInDtl1095 : enrlDtlList) {
			enrollmentList.add(enrollmentInDtl1095.getEnrollmentId());
		}

		if(enrollmentList.size() > 0){
			List<Enrollment1095> enrollment1095List = enrollment1095Repository.getEnrollment1095ByExchgAsignedPolicyId(enrollmentList);

			for (Enrollment1095 enrollment1095 : enrollment1095List) {
				if(enrollment1095.getEnrollmentIn1095Id() != null 
						&& !BatchCategoryCodeType.IRS_EOY_REQ.name().equalsIgnoreCase(enrollment1095.getBatchCategoryCode()) 
						&& !enrollment1095.getInboundAction().equalsIgnoreCase("SUCCESS")){

					enrollment1095.setResubCorrectionInd(Enrollment1095.YorNFlagIndicator.Y.toString());
				}

				enrollment1095.setInboundBatchCategoryCode(enrollmentIn1095.getBatchCategoryCode());
				enrollment1095.setEnrollmentIn1095Id(enrollmentIn1095.getId());
				enrollment1095.setInboundAction(update);

				enrollment1095Repository.save(enrollment1095);
			}
		}

		String batchId = enrollmentIn1095.getBatchId();
		String documentSeqId = enrollmentIn1095.getDocumentSeqId();
		String enrollmentIds = null;
		if(batchId != null && documentSeqId != null){
			enrollmentIds = enrollmentOut1095Repository.getEnrollmentIds(batchId, documentSeqId);
		}else{
			LOGGER.error("Error spliting BatchId : "+ batchId);
		}

		if(enrollmentIds != null){
			String[] enrollmentArray = enrollmentIds.split(",");

			if(enrollmentArray != null && enrollmentArray.length > 0){

				List<Integer> bigEnrollmentList = new ArrayList<>();
				for (String string : enrollmentArray) {
					bigEnrollmentList.add(Integer.valueOf(string));
				}

				bigEnrollmentList.removeAll(enrollmentList);
				if(bigEnrollmentList != null && !bigEnrollmentList.isEmpty()) {
					List<Enrollment1095> enrollment1095List = enrollment1095Repository.getEnrollment1095ByExchgAsignedPolicyId(bigEnrollmentList);

					for (Enrollment1095 enrollment1095 : enrollment1095List) {
						enrollment1095.setResubCorrectionInd(null);
						enrollment1095.setInboundBatchCategoryCode(enrollmentIn1095.getBatchCategoryCode());
						enrollment1095.setEnrollmentIn1095Id(enrollmentIn1095.getId());
						enrollment1095.setInboundAction(SUCCESS);

						enrollment1095Repository.save(enrollment1095);
					}
				}
			}
		}
		enrollmentIn1095.setIsProcessedFlag(EnrollmentConstants.Y);
		enrollmentIn1095Repository.saveAndFlush(enrollmentIn1095);
	}

	private void doNothing(EnrollmentIn1095 enrollmentIn1095) {
		updateEnrollment1095(enrollmentIn1095, DO_NOTHING );
		sendDoNothingEmail(enrollmentIn1095);
	}

	private void resendXML(String batchIdAndCode, List<String> documentList, List<EnrollmentIn1095> InList) {
		// copy documents to temp folder
		// create manifest file
		// set to hub

		String irs_annual_report_archive_folderpath = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT)
				.append(File.separator).append(EnrollmentConstants.ARCHIVE_FOLDER).toString();

		String batchId_bcc_array [] = batchIdAndCode.split("\\|");
		String batchId = null;
		String previousBatchId = null;

		if(batchId_bcc_array != null){
			if(batchId_bcc_array.length == 3){
				batchId = batchId_bcc_array[0];
				previousBatchId = batchId_bcc_array[1];
			}else{
				batchId = batchId_bcc_array[0];
				previousBatchId = null;
			}
		}

		String folderName = batchId.replace(":", "-");

		String directoryTobeZipped = irs_annual_report_archive_folderpath + File.separator + folderName;

		String irs1095ResponsePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN).toString();
		//String irs1095ResponsePath = "D:\\vino\\D2C\\TestIRS";
		String wIPFolder = "WIP_"+new Date().getTime();
		String irs1095ResponseWipPath = irs1095ResponsePath+ File.separator + wIPFolder;

		moveXMLToResend(directoryTobeZipped, irs1095ResponseWipPath, documentList);
		String processError = null;
		String year = enrollmentOut1095Repository.getCoverageYearFromBatchId(batchId);
		try {
			boolean isGenerartedXMLsValid = enrollmentAnnualIrsBatchService.validateGeneratedXMLs(irs1095ResponseWipPath, Integer.valueOf(year));
			enrollmentAnnualIrsBatchService.generateManifestXML(Integer.parseInt(year),
					BatchCategoryCodeType.IRS_EOY_RESUBMIT_REJECTED_OR_MISSING_FILE_REQ.toString(), 
					irs1095ResponseWipPath, 
					false, 
					previousBatchId, 
					batchId, 
					null, 
					null,
					true, isGenerartedXMLsValid);
		} catch (NumberFormatException | GIException e) {
			processError = EnrollmentUtils.shortenedStackTrace(e, 3);
			LOGGER.error("generateManifestXML()::"+e.getMessage(),e);
		}

		if(null == processError){
			for (EnrollmentIn1095 enrollmentIn1095 : InList) {
				enrollmentIn1095.setIsProcessedFlag(EnrollmentConstants.Y);
				enrollmentIn1095Repository.saveAndFlush(enrollmentIn1095);
			}
		}else{
			for (EnrollmentIn1095 enrollmentIn1095 : InList) {
				enrollmentIn1095.setIsProcessedFlag(EnrollmentConstants.N);
				enrollmentIn1095.setProcessError(processError);
				enrollmentIn1095Repository.saveAndFlush(enrollmentIn1095);
			}
		}
	}

	private void moveXMLToResend(String directoryTobeZipped, String irs1095ResponseWipPath, List<String> documentList) {
		for (String documentId : documentList) {
			File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(directoryTobeZipped, documentId );
			EnrollmentUtils.copyZipFiles(irs1095ResponseWipPath , irsNackResponseFiles);
		}
	}

	private void processInboundAckResponse(String ackResponsePath, Long jobExecutionId) {
		File ackfilePath = new File(ackResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(ackfilePath.exists()){
			renameOutFilesToZip(ackfilePath, ackResponsePath);
			String[] ZipFileNames = ackfilePath.list();
			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(ackResponsePath + File.separator + responseFileName);

				cleanDir(ackResponsePath);

				zippy.extract(SourceZipFile, ackfilePath);
				try{
					processAckFile(ackResponsePath, responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :"+ e.getMessage());
				}

				moveToArchive(SourceZipFile);
			}
		}
	}

	private void processInboundNackResponse(String nackResponsePath, Long jobExecutionId) {
		File nackfilePath = new File(nackResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(nackfilePath.exists()){
			renameOutFilesToZip(nackfilePath, nackResponsePath);
			String[] ZipFileNames = nackfilePath.list();
			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(nackResponsePath + File.separator + responseFileName);

				cleanDir(nackResponsePath);

				zippy.extract(SourceZipFile, nackfilePath);
				try{
					processNackFile(nackResponsePath,responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :"+ e.getMessage());
				}
				moveToArchive(SourceZipFile);
			}
		}
	}

	private void processInboundOutResponse(String outResponsePath, Long jobExecutionId) {
		File outfilePath = new File(outResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(outfilePath.exists()){
			renameOutFilesToZip(outfilePath, outResponsePath);
			String[] ZipFileNames = outfilePath.list();

			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(outResponsePath + File.separator + responseFileName);

				cleanDir(outResponsePath);

				zippy.extract(SourceZipFile, outfilePath);
				try{
					processManifestFile(outResponsePath,responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :"+ e.getMessage());
				}
				moveToArchive(SourceZipFile);
			}
		}
	}

	private void cleanDir(String outResponsePath) {
		// clean directory before extracting file
		File[] nonZipFileList = EnrollmentUtils.getFilesInAFolderNotByName(new File(outResponsePath).getAbsolutePath(), ".zip" );
		for (File file : nonZipFileList) {
			file.delete();
		}

	}

	private void processManifestFile(String outResponsePath,String responseFileName, Long jobExecutionId) {
		//Process Manifest file
		File[] manifestResponseFiles = EnrollmentUtils.getFilesInAFolderByName(new File(outResponsePath).getAbsolutePath(), MANIFEST );
		if(manifestResponseFiles != null && manifestResponseFiles.length == 1){
			try{
				Enrollment1095Inbound enrollment1095Inbound = enrollment1095Factory.getInboundInstanceFromFile(manifestResponseFiles[0]);
				enrollment1095Inbound.processEnrollmentIn1095ForOutResponse(manifestResponseFiles[0], responseFileName, outResponsePath, jobExecutionId);
			}catch (Exception e) {
				LOGGER.error("Exception while Processing Out File");
			}

		}
	}

	private void processAckFile(String ackResponsePath, String responseFileName, Long jobExecutionId) {
		LOGGER.info("Process ACK File");
		File[] irsAckResponseFiles = EnrollmentUtils.getFilesInAFolderByName(ackResponsePath, ACK );
		try{
			for (File file : irsAckResponseFiles) {
				Enrollment1095Inbound enrollment1095Inbound = enrollment1095Factory.getInboundInstanceFromFile(file);
				enrollment1095Inbound.updateEnrollmentIn1095ForAckAndNack(file, ACK, jobExecutionId);
			}
		}catch (Exception e) {
			LOGGER.error("Error processing ACK file", e);
		}
	}

	private void processNackFile(String nackFilePath, String responseFileName, Long jobExecutionId) {
		LOGGER.info("Process NACK File");
		File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(nackFilePath, NACK );
		try{
			for (File file : irsNackResponseFiles) {
				Enrollment1095Inbound enrollment1095Inbound = enrollment1095Factory.getInboundInstanceFromFile(file);
				enrollment1095Inbound.updateEnrollmentIn1095ForAckAndNack(file, NACK, jobExecutionId);
			}
		}catch (Exception e) {
			LOGGER.error("Error processing NACK file", e);
		}
	}



	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param allInFileForMail 
	 */
	private void sendenrollment1095InEmail(File[] allInFileForMail) {

		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_INBOUND_PROCESS_JOB);
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.IN_FILE_NAME_ARRAY, getFormattedFileName(Arrays.asList(allInFileForMail).toString()));
		try {
			enrollment1095NotificationService.sendEnrollmentIn1095Email(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment1095AInboundProcess email notification :: ", e);
		}
	}

	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param allInFileForMail 
	 */
	private void sendDoNothingEmail(EnrollmentIn1095 enrollmentIn1095) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_INBOUND_PROCESS_JOB);
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.IN_FILE_NAME, enrollmentIn1095.getInPackageFileName());
		try {
			enrollment1095NotificationService.sendDoNothingEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment1095AInboundProcess email notification :: ", e);
		}
	}

	private String getFormattedFileName(String fileNameArray) {
		return fileNameArray.replace("[", "").replace("]", "").replace(",", "<br>");
	}

	private void sendenrollment1095InProcessEmail(List<EnrollmentIn1095> processList) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_STAGING_JOB);
		StringBuffer processFile = new StringBuffer();
		for (EnrollmentIn1095 enrollmentIn1095 : processList) {
			processFile.append("<tr>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn1095.getInPackageFileName());
			processFile.append("</td>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn1095.getIsProcessedFlag());
			processFile.append("</td>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn1095.getProcessError());
			processFile.append("</td>");

			processFile.append("</tr>");
		}
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_IN_BatchEmail.IN_FILE_PROCESS_DATA, processFile.toString());

		try {
			enrollment1095NotificationService.sendEnrollmentIn1095ProcessEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment1095InProcess email notification :: ", e);
		}

	}

	private void moveToArchive(File sourceZipFile) {
		String irs1095ResponseArchivePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN).append(File.separator).append(EnrollmentConstants.ARCHIVE_FOLDER).toString();
		//String irs1095ResponseArchivePath = "D:\\vino\\D2C\\TestIRSArchive";

		try{
			EnrollmentUtils.createDirectory(irs1095ResponseArchivePath);
			if (sourceZipFile!=null && new File(irs1095ResponseArchivePath).exists()) {
				//move all files from source to target folder
				sourceZipFile.renameTo(new File(irs1095ResponseArchivePath+File.separator+sourceZipFile.getName()));
			}
		}catch(Exception e){
			LOGGER.error("Error in moveFiles()"+e.toString(),e);
		}
	}

	/**
	 * Rename .OUT files to .zip
	 * @param file
	 * @param sourceFolder
	 */
	private void renameOutFilesToZip(File file, String sourceFolder) {
		String[] renameZipFileNames = file.list();
		for(String name : renameZipFileNames){
			File sourceFile = new File(sourceFolder + File.separator + name);
			if(sourceFile.isFile() && sourceFile.getName().endsWith(".OUT")){
				File oldzipFile =sourceFile;          
				// File (or directory) with new name
				String newZipFileName =  oldzipFile + ".zip";			
				File newZipFile = new File(newZipFileName);   // Rename file (or directory)
				oldzipFile.renameTo(newZipFile);
			}
		}		
	}

	private static File[] getWipDirList(String outZipWipPath, String manifest) {
		File[] files = null;
		try{
			File folder= new File(outZipWipPath);
			final String finalResponseType=manifest;
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if((pathname.isFile() && pathname.getName().contains(finalResponseType)) || (pathname.isDirectory()
							&& pathname.getName().contains(EnrollmentConstants.ARCHIVE_FOLDER))){
						return false;
					}
					return true;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location");
		}
		return files;
	}


}
