<script type="text/ng-template" id="income-other.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeOther" >
    <h3><spring:message code="label.lce.otherincome"/></h3>

    <form class="form-horizontal" id="otherIncome" name="otherIncome" novalidate>
      <spring:message code="label.lce.whatothertypeofincome"/> {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}} <spring:message code="label.lce.have"/>
      <div class="control-group margin10-t">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls">
            <label class="checkbox inline">
              <input type="checkbox" id="otherIncomeTypeDescription1" name="other" value="Canceled Debts"> <spring:message code="label.lce.canceleddebts"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox inline">
              <input type="checkbox" id="otherIncomeTypeDescription2" name="other" value="Court Awards"> <spring:message code="label.lce.courtawards"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox inline">
              <input type="checkbox" id="otherIncomeTypeDescription3" name="other" value="Jury Duty Pay"> <spring:message code="label.lce.jurydutypay"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox inline">
              <input type="checkbox" id="otherIncomeTypeDescription4" name="other" ng-model="otherIncomeType"> <spring:message code="label.lce.other"/>
            </label>
          </div>
      </div>

  	  <div ng-show="otherIncomeType">
  	    <spring:message code="label.lce.whatothertypeofincome"/> {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}} <spring:message code="label.lce.have"/>
  	    <div class="control-group">
  	      <label class="control-label margin10-t">Type:</label>
  	      <div class="controls margin10-t">
  	          <input type="text" class="span7" value="" name="otherIncomeTypeDescriptionDetails" ng-model="householdMember.detailedIncome.otherIncome.otherIncomeTypeDescription" ng-required="otherIncomeType==true" ng-trim>
  	      </div>
  	      <div class="controls margin5-t">
  	        <span class="validation-error-text-container" ng-show="submitted && otherIncome.otherIncomeTypeDescriptionDetails.$error.required"><spring:message code="label.lce.incomeother.error1"/></span>
  	      </div>
  	      <label class="control-label" for="typeOfRequest"></label>
  	      <div class="controls margin20-t">
  	        <div class="input-prepend">
  	          <span class="add-on">$</span>
  	          <input type="text" id="otherIncome.incomeAmount" name="otherIncomeAmount" class="span10" value="" ng-model="householdMember.detailedIncome.otherIncome.incomeAmount" ng-required="otherIncomeType==true" numbers-Only ng-trim>
  	        </div>
  	      </div>
  	      <div class="controls margin5-t">
  	        <span class="validation-error-text-container" ng-show="submitted && otherIncome.otherIncomeAmount.$error.required"><spring:message code="label.lce.incomeother.error2"/></span>
  	      </div>
  	    </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="otherIncome.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!otherIncome.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>