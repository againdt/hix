package com.getinsured.referraltask;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * Purposely kept in package outside the com.getinsured.eligibility As com.getinsured.eligibility is
 * scanned by dispatcher servlet application context & also contextloadlistener
 * and task @Async will not work if it is loaded at two places
 *
 * @author chopra_s
 *
 */

@Component("referralEligibilityTask")
@DependsOn("dynamicPropertiesUtil")
public class ReferralEligibilityTaskImpl implements ReferralEligibilityTask {

	private static final Logger LOGGER = Logger.getLogger(ReferralEligibilityTaskImpl.class);

	private static final String ERROR_WHILE_EXECUTION = "Error occurred while executing the eligibility referral integration.";
	
	private String stateCode;
	@PostConstruct
	public void createStateContext() {
		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Value("#{configProp['ghixEligibilityServiceURL']}")
	private String erpSvcUrl;

	private static final String PROCESS_AT_REFERRAL = "/initiateATReferral";

	private static final String END_POINT_FUNCTION = "ERP-TRANSFERACCOUNT";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Override
	@Async
	public void runEligibility(long ssapApplicationId) {
		try {
			LOGGER.info("Start Eligibility execution for ssapappliction id - " + ssapApplicationId);

			final List<GIWSPayload> giwsPayloads = giwsPayloadRepository.findBySsapApplicationId(ssapApplicationId, END_POINT_FUNCTION);

			if (ReferralUtil.listSize(giwsPayloads) != 0) {

				AccountTransferRequestPayloadType payload = null;
				
				if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
				{
					String requestBody = ReferralUtil.extractSoapBoady(giwsPayloads.get(0).getRequestPayload());
					payload = ReferralUtil.unmarshal(requestBody);
				}
				else
				{
					payload = ReferralUtil.getAccountTransferRequestObject(giwsPayloads.get(0).getRequestPayload());
				}
				final boolean isFullDetermination;
				final String atResponseType;
				if ("NM".equals(stateCode)){
					isFullDetermination = ReferralUtil.isFullDetermination(payload, stateCode);
					atResponseType = isFullDetermination ? AtResponseTypeEnum.FULL_DETERMINATION.toString() : AtResponseTypeEnum.ASSESSMENT.toString();
				} else {
					boolean isUpdated = ReferralUtil.isFullDetermination(payload, stateCode);
					atResponseType = isUpdated ? AtResponseTypeEnum.UPDATED.toString() : AtResponseTypeEnum.FULL_DETERMINATION.toString();
					isFullDetermination = true;
				}
				final String caseNumber = fetchCaseNumber(ssapApplicationId);

				LOGGER.debug("caseNumber - " + caseNumber + " ssapappliction id - " + ssapApplicationId + " GIWSPayload Id - " + giwsPayloads.get(0).getId() + "isFullDetermination - " + isFullDetermination + " atResponseType - " + atResponseType);

				AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
				accountTransferRequestDTO.setAccountTransferRequestPayloadType(payload);
				accountTransferRequestDTO.setGiwsPayloadId(giwsPayloads.get(0).getId());
				accountTransferRequestDTO.setCaseNumber(caseNumber);
				accountTransferRequestDTO.setFullDetermination(isFullDetermination);
				accountTransferRequestDTO.setAtResponseType(atResponseType);

				ghixRestTemplate.exchange(erpSvcUrl + PROCESS_AT_REFERRAL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
				
			}

			LOGGER.info("End Eligibility execution for ssapappliction id - " + ssapApplicationId);
		} catch (Exception e) {
			LOGGER.error(ERROR_WHILE_EXECUTION, e);
		}
	}
	
	private String fetchCaseNumber(long ssapApplicationId) {
	    return ssapApplicationRepository.fetchCaseNumberById(ssapApplicationId);
    }
}
