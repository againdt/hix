package com.getinsured.iex.hub.vci.wrapper;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeRequestPayloadType;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.ObjectFactory;

/**
 * Wrapper com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeRequestPayloadType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 *
 */
public class CurrentIncomeRequestPayloadTypeWrapper {
	
	private CurrentIncomeRequestPayloadType request;
	
	private List<PersonTypeWrapper> person;
		
	public CurrentIncomeRequestPayloadTypeWrapper(){
		ObjectFactory factory = new ObjectFactory(); 
		this.request = factory.createCurrentIncomeRequestPayloadType();
	}
	
	public CurrentIncomeRequestPayloadType getRequest(){
		
		for(PersonTypeWrapper applicant : this.person){
			this.request.getPerson().add(applicant.getPerson());
		}
		
		return request;
	}
	
	public List<PersonTypeWrapper> getPerson() {
        if (person == null) {
            person = new ArrayList<PersonTypeWrapper>();
        }
        return this.person;
    }
	
}
