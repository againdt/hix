
package com.getinsured.iex.ssap.enums;



/**
 * 
 * @author Deepa
 *
 *
 */

public enum MaritalStatusEnum {
	
	X("Legally Separated"),
	W("Widowed"),
	U("Unmarried"),
	S("Separated"),
	R("Unreported"),
	M("Married"),
	I("Single"),
	D("Divorced"),
	B("Registered Domestic Partner");
    
	private String  value;
	
	private MaritalStatusEnum(String s) {
		this.value = s;
	}
	
	public String value() {
		return value;
	}
	
	public static String value(String v) {
		if (v != null) {
			final MaritalStatusEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}
	
	public static String getCode(String v) {
		String  value = "";
		for (MaritalStatusEnum c : MaritalStatusEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				value = c.toString();
				break;
			}
		}
		return value;
	}
	
	private static MaritalStatusEnum readValue(String v) {
		MaritalStatusEnum data = null;
		for (MaritalStatusEnum c : MaritalStatusEnum.class.getEnumConstants()) {
			if (c.toString().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
	
	public static void main(String[] args) {
		System.out.println(MaritalStatusEnum.getCode("Divorced"));
		System.out.println(MaritalStatusEnum.value("I"));
	}
}
