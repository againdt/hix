<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
				<li><spring:message  code="pgheader.update.companyProfile"/></li>
			</ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="companyProfile"/>
				</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.update.companyProfile"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>

				<form class="form-horizontal" id="frmIssuerAcctProfComInfo" name="frmIssuerAcctProfComInfo" action="<c:url value="/admin/issuer/company/profile/save"/>" method="POST" enctype="multipart/form-data">
                    <df:csrfToken/>
                    <input type="hidden" id="id" name="id" value="${issuerObj.id}"/>
                    <div class="gutter10">
                    <div class="control-group">
						<label class="control-label" for="companyLegalName"><spring:message  code="label.companyLegalName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="companyLegalName" id="companyLegalName" class="input-large" value="${issuerObj.companyLegalName}"  maxlength="100"/>
                            <div id="companyLegalName_error" class=""></div>
						</div>
					</div>
					<!-- end of control-group-->
                    <div class="control-group">
						<label class="control-label" for="companyLogoUI"><spring:message  code="label.companyLogo"/><a class="ttclass" rel="tooltip" href="#" data-original-title="JPEG or PNG file of the Issuer logo"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="file" id="companyLogoUI" name="companyLogoUI" accept=".gif,.jpeg,.jpg,.png" class="input-file" />
							<input type="hidden"  id="companyLogo_Size" name="companyLogo_Size" value="1">
							<p class="blur">Recommended dimension: 160px X 60px </p>
							<div id="companyLogo_display">
							 <c:if test="${not empty companyLogoExist}">	
							 	<a href="<c:url value="/admin/issuer/downloadlogo/${issuerObj.hiosIssuerId}"/>" target="_blank" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a>
							 </c:if>	
							</div>
							<div id="companyLogoUI_error" class="" style="color: red">${ERROR}</div>
                            <input type="hidden" id="companyLogo" name="companyLogo" value=""/>
						</div>
					</div><!-- end of control-group-->

                    <div class="control-group">
						<label class="control-label" for="stateOfDomicile"><spring:message code="label.stateOfDomicile"/></label>
						<div class="controls">
						 <c:choose>
	                    	<c:when test="${exchangeType == 'PHIX'}">
							<select class="input-large" name="stateOfDomicile" id="stateOfDomicile">
								<option value="" selected>Select</option>
                            	<c:forEach var="stateObj" items="${statelist}">
						    			<option <c:if test="${stateObj.code == issuerObj.stateOfDomicile}">selected</c:if> value="${stateObj.code}">${stateObj.name}</option>
								</c:forEach>
                            </select>
                          </c:when>
                          <c:otherwise>
                          <select class="input-large" name="stateOfDomicile" id="stateOfDomicile">
                          		<option value="" selected>Select</option>
                            	<c:forEach var="stateObj" items="${statelist}">
						    			<option 
						    			<c:choose>
						    			<c:when test="${stateObj.code == issuerObj.stateOfDomicile}"> SELECTED  value="${stateObj.code}" </c:when>
						    			<c:when test="${stateObj.code == stateCodeOnExchange && issuerObj.stateOfDomicile == null }"> SELECTED  value="${stateObj.code}" </c:when>
						    			</c:choose> value="${stateObj.code}" >${stateObj.name}
						    			</option> 
								</c:forEach>
                            </select>
                          </c:otherwise> 
                          </c:choose> 
                       	</div>
					</div><!-- end of control-group-->

					<c:if test="${exchangeType == 'PHIX'}">
					<div class="control-group">
						<label class="control-label" for="nationalProduerNumber"><spring:message  code="label.nationalProducerNumber"/></label>
						<div class="controls">
							<input type="text" name="nationalProducerNumber" id="nationalProducerNumber" class="input-large" value="${issuerObj.nationalProducerNumber}"/>
                            <div id="nationalProducerNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
					<div class="control-group">
						<label class="control-label" for="agentFirstName"><spring:message  code="label.agentFirstName"/></label>
						<div class="controls">
							<input type="text" name="agentFirstName" id="agentFirstName" class="input-large" value="${issuerObj.agentFirstName}"/>
                            <div id="agentFirstName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
					<div class="control-group">
						<label class="control-label" for="agentLastName"><spring:message  code="label.agentLastName"/></label>
						<div class="controls">
							<input type="text" name="agentLastName" id="agentLastName" class="input-large" value="${issuerObj.agentLastName}"/>
                            <div id="agentLastName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
					<div class="control-group">
						<label class="control-label" for="disclaimer"><spring:message  code="label.pmdisclaimer"/></label>
						<div class="controls"><textarea class="input-xlarge" name="disclaimer" id="disclaimer" rows="4" cols="40" style="resize: none;" 
								maxlength="2000" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();" onKeyPress="return(this.value.length < 2000 );">${issuerObj.disclaimer}</textarea></div>
								<div id="disclaimer_error"  class="help-inline"></div>
								<div class="controls"><div id="chars_left"><spring:message  code="label.charLeft"/> <b>2000</b></div></div>
					</div><!-- end of control-group-->
					</c:if>
                    </div><!-- /.gutter10 -->	
                        
                        <div class="header">
                            <h4><spring:message  code="pgheader.update.companyAddress"/></h4>
                        </div>
                   	
                        
                    <div class="gutter10">                   
                    <div class="control-group">
						<label class="control-label" for="companyAddressLine1"><spring:message  code="label.addressLine1"/></label>
						<div class="controls">
							<input type="text" name="companyAddressLine1" id="companyAddressLine1" class="input-large" value="${issuerObj.companyAddressLine1}" maxlength="200"/>
                            <div id="companyAddressLine1_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="companyAddressLine2"><spring:message  code="label.addressLine2"/></label>
						<div class="controls">
							<input type="text" name="companyAddressLine2" id="companyAddressLine2" class="input-large" value="${issuerObj.companyAddressLine2}" maxlength="200"/>
                            <div id="companyAddressLine2_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="companyCity"><spring:message  code="label.city"/></label>
						<div class="controls">
							<input type="text" name="companyCity" id="companyCity" maxlength="30" class="input-large" value="${issuerObj.companyCity}"/>
                            <div id="companyCity_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    
                    <div class="control-group">
						<label class="control-label" for="companyState"><spring:message  code="label.state"/></label>
						<div class="controls">
							<select class="input-large" name="companyState" id="companyState">
								<option value="" selected>Select</option>
                            	<c:forEach var="stateObj" items="${statelist}">
						    		<option 
						    			<c:choose>
							    			<c:when test="${stateObj.code == issuerObj.companyState}"> SELECTED  value="${stateObj.code}" </c:when>
							    			<c:when test="${stateObj.code == stateCodeOnExchange && issuerObj.companyState == null }"> SELECTED  value="${stateObj.code}" </c:when>
						    			</c:choose> value="${stateObj.code}" >${stateObj.name}
						    		</option>
								</c:forEach>
                            </select>
                            <div id="bank-state-error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="companyZip"><spring:message  code="label.zipCode"/></label>
						<div class="controls">
							<input type="text" name="companyZip" id="companyZip" class="input-large" value="${issuerObj.companyZip}" maxlength="5"/>
                            <div id="companyZip_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="siteUrl"><spring:message  code="label.issuerWebsite"/></label>
						<div class="controls">
							<input type="text" name="siteUrl" id="siteUrl" class="input-large" value="${issuerObj.siteUrl}" maxlength="200"/>
                            <div id="siteUrl_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="companySiteUrl"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls">
							<input type="text" name="companySiteUrl" id="companySiteUrl" class="input-large" value="${issuerObj.companySiteUrl}" maxlength="200"/>
                            <div id="companySiteUrl_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
					<div class="control-group">
						<label class="control-label" for="paymentUrl"><spring:message  code="label.paymentUrl"/></label>
						<div class="controls">
							<input type="text" name="paymentUrl" id="paymentUrl" class="input-large" value="${issuerObj.paymentUrl}"/>
                            <div id="paymentUrl_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    </div><!-- /.gutter10 -->
					<div class="form-actions">
					<a class="btn cancel-btn" href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a>
					<a class="btn btn-primary" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
					</div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->

<script type="text/javascript">
var csrValue = $("#csrftoken").val();
function submitForm(){
	 $("#frmIssuerAcctProfComInfo").submit(); 
}
 
 function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
         String.prototype.trim = function() {
         	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
         }
		}
	}
 
 $(function(){
	 $('#companyLogoUI').change(function(){
			var rv = -1; // Return value assumes failure.
			var fileObject=this.files[0];

			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
				if((fileObject.size > 5242880)||(fileObject.fileSize > 5242880)){
					$("#companyLogo_Size").val(0);
				}
				else{
					$("#companyLogo_Size").val(1);	
				} 
			}
			else{
				$("#companyLogo_Size").val(1);	
			} 

			var excludeTypes = ['image/gif', 'image/jpeg', 'image/png'];
			if (fileObject && !excludeTypes.includes(fileObject.type)) {
				alert("Invalid File type, allowed only " + excludeTypes);
				$("#companyLogoUI").val(null);
			}
		});
	});
jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
	  ie8Trim();
	  if($("#companyLogo_Size").val()==1){
		  return true;
	  }
	  else{
		  return false;
	  }
});

</script>
  
  <script type="text/javascript">
	$('.ttclass').tooltip();
	var validator = $("#frmIssuerAcctProfComInfo").validate({ 
		rules : {
			companyLegalName : {required : true},
			companyLogo : {required : false, sizeCheck:true},
			//addressLine1 : { required : true},
			//addressLine2 : { required : true},
			//city : { required : true},
			companyZip : { required : false, number : true, minlength : 5, maxlength : 5},
			//issuerWebsite : {required : false, url:{depends: function(element) {var enteredURL=$("#issuerWebsite").val(); var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");if (urlregex.test(enteredURL)){return (false);}else{return (true);}}}},
			//companySiteUrl : {required : false, url:{depends: function(element) {var enteredURL=$("#facingWebSite").val(); var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");if (urlregex.test(enteredURL)){return (false);}else{return (true);}}}},	
			//siteUrl : {required : false, url:{depends: function(element) {var enteredURL=$("#siteUrl").val(); var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");if (urlregex.test(enteredURL)){return (false);}else{return (true);}}}}
			companySiteUrl : {required : false, checkForValidUrl : true},
			paymentUrl : {required : false, checkForValidUrl : true}, 
			//siteUrl : {required : false, url:{depends: function(element) {var enteredURL=$("#siteUrl").val(); var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");if (urlregex.test(enteredURL)){return (false);}else{return (true);}}}}
			siteUrl : {required : false, checkForValidUrl : true}
		},
		messages : {
			companyLegalName : { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLegalName' javaScriptEscape='true'/></span>"},
			companyLogo: { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLogo' javaScriptEscape='true'/></span>",
				sizeCheck :"<span> <em class='excl'>!</em>Please Select File With Size Less Than 5 MB </span>" },
			//addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
			//addressLine2 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine2' javaScriptEscape='true'/></span>"},
			//city: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
			companyZip : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>",
				        number : "<span><em class='excl'>!</em><spring:message  code='err.issuervalidZip' javaScriptEscape='true'/></span>",
				        minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>",
				        maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>"
			          },
			companySiteUrl : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
				checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
			},
			paymentUrl : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
				checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
			},
			siteUrl : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
				checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
				//url : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
			}
					
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
	// Update remaining characters for comments
	function updateCharCount(){	
		var disclaimerMaxLen = 2000;
		var currentLen = $.trim(document.getElementById("disclaimer").value).length;
		var charLeft = disclaimerMaxLen - currentLen;
		if(currentLen > disclaimerMaxLen) {
			$("#disclaimer").val($("#disclaimer").val().substr(0, disclaimerMaxLen));
			$('#chars_left').html('Characters left <b>' + 0 + '</b>' );
	    }
		$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
	}
	
	jQuery.validator.addMethod("checkForValidUrl", function (value, element, param) {		
	    var urlRegExp = new RegExp("(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$");
	    if(value == '') {
			return true;
		}else{
			if((value.indexOf('.') == -1)){
				 return false;
			}
			if(urlRegExp.test(value)){
		    		return (true);
			    }else{
			    	return (false);
			    }
	    }   	    
	});
</script>
