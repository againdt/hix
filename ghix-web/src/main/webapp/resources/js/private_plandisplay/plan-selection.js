////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
function populateQualityRatingsHtml(issuerQualityRating){
	var state = $('#stateCode').val();
	var noOfStars = state === 'CT' ? 4 : 5;

	var qualityRatingsHtml = '';


	if(issuerQualityRating.QualityRating !== '' && issuerQualityRating.QualityRating !== undefined && issuerQualityRating.QualityRating !== null){
		qualityRatingsHtml = '<span class=\'aria-hidden\'>' + issuerQualityRating.QualityRating + 'out of' + noOfStars + ' stars.</span>';
	}

	if($('#stateCode').val() === 'CA' || $('#stateCode').val() === 'MN'){

		var tooltip = populateQualityRatings(issuerQualityRating);

		qualityRatingsHtml = '<a class=\'quality-rating\' data-placement=\'top\' rel=\'tooltip\' role=\'tooltip\' href=\'#\' data-original-title=\'';
		qualityRatingsHtml += tooltip;
		qualityRatingsHtml += '\' aria-label=\'';


		qualityRatingsHtml += issuerQualityRating.QualityRating + ' stars ' + tooltip.replace(/(<([^>]+)>)/ig, ' ');
		qualityRatingsHtml += '\'>';

		qualityRatingsHtml += getTooltipText(issuerQualityRating);
		qualityRatingsHtml += '</a>';

	}else{

		if(issuerQualityRating.QualityRating === '' || issuerQualityRating.QualityRating === undefined || issuerQualityRating.QualityRating === null){
			qualityRatingsHtml += plan_display['pd.label.tooltip.qualityRatings.seven'];
		}else{
			qualityRatingsHtml += getQualityScoreBars(issuerQualityRating.QualityRating);
		}
	}

	return qualityRatingsHtml;

}

function populateQualityRatings(issuerQualityRating){
	var qualityRatingCount = 0;
	var qualityRatingData = '';

	var subRating = '';

	if(issuerQualityRating.SummaryClinicalQualityManagement !== undefined && issuerQualityRating.SummaryClinicalQualityManagement !== ''){
		qualityRatingCount++;
		subRating += '<div class="qr-label">'+plan_display['pd.label.tooltip.qualityRatings.three']+':</div><div class="qr-value">';
		subRating += getQualityScoreBars(issuerQualityRating.SummaryClinicalQualityManagement) + '<span class="hide">' + issuerQualityRating.SummaryClinicalQualityManagement + ' starts</span></div>';
		if($('#stateCode').val() === 'MN'){
			subRating += '<div class="clear">'+plan_display['pd.label.tooltip.qualityRatings.threedotone']+plan_display['pd.label.tooltip.qualityRatings.threedottwo'] + '</div>';
		}
	}

	if(issuerQualityRating.SummaryEnrolleeExperience !== undefined && issuerQualityRating.SummaryEnrolleeExperience !== ''){
		qualityRatingCount++;
		subRating += '<div class="qr-label">'+plan_display['pd.label.tooltip.qualityRatings.four']+':</div><div class="qr-value">';
		subRating += getQualityScoreBars(issuerQualityRating.SummaryEnrolleeExperience) + '<span class="hide">' + issuerQualityRating.SummaryEnrolleeExperience + ' starts</span></div>';
		if($('#stateCode').val() === 'MN'){
			subRating += '<div class="clear">'+plan_display['pd.label.tooltip.qualityRatings.fourdotone']+plan_display['pd.label.tooltip.qualityRatings.fourdottwo'] + '</div>';
		}
	}

	if(issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement !== undefined && issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement !== ''){
		qualityRatingCount++;
		subRating += '<div class="qr-label">'+plan_display['pd.label.tooltip.qualityRatings.five']+':</div><div class="qr-value">';
		subRating += getQualityScoreBars(issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement) + '<span class="hide">' + issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement + ' starts</span></div>';
		if($('#stateCode').val() === 'MN'){
			subRating += '<div class="clear">'+plan_display['pd.label.tooltip.qualityRatings.fivedotone']+plan_display['pd.label.tooltip.qualityRatings.fivedottwo'] + '</div>';
		}
	}


	if(qualityRatingCount > 0){
		qualityRatingData += '<b>'+plan_display['pd.label.tooltip.qualityRatings.one']+'</b><br>';
	}

	if(qualityRatingCount === 1){
		qualityRatingData += plan_display['pd.label.tooltip.qualityRatings.eight'];
	}else if(qualityRatingCount > 1 && $('#stateCode').val() !== 'MN'){
		qualityRatingData += getQualityScoreBars(issuerQualityRating.QualityRating);
		qualityRatingData += plan_display['pd.label.tooltip.qualityRatings.two'];
	}

	qualityRatingData += subRating;

	return qualityRatingData;

}


function getTooltipText(issuerQualityRating){
	var qualityRatingCount = 0;
	if(issuerQualityRating.SummaryClinicalQualityManagement !== undefined && issuerQualityRating.SummaryClinicalQualityManagement !== ''){
		qualityRatingCount++;
	}
	if(issuerQualityRating.SummaryEnrolleeExperience !== undefined && issuerQualityRating.SummaryEnrolleeExperience !== ''){
		qualityRatingCount++;
	}
	if(issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement !== undefined && issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement !== ''){
		qualityRatingCount++;
	}

	if(qualityRatingCount === 0){
		return plan_display['pd.label.tooltip.qualityRatings.seven'];
	}else if(qualityRatingCount === 1){
		return plan_display['pd.label.tooltip.qualityRatings.six'];
	}else{
		return getQualityScoreBars(issuerQualityRating.QualityRating);
	}


	return qualityRatingCount;
}


function getQualityScoreBars(score){
	var qualityScoreBars = '';
	var noOfStars = 5;

	if($('#stateCode').val() === 'CT'){
		noOfStars = 4;
	}

	if(score !== undefined && score !== ''){
		for(var i = 1; i <= noOfStars; i ++){
			if(i <= score){
				qualityScoreBars += '<i class="icon icon-star"></i>';
			} else if (score % 1 != 0 && Math.ceil(score) == i) {
				qualityScoreBars += '<i class="icon icon-star-half-empty"></i>'
	 		} else {
	 			qualityScoreBars += '<i class="icon icon-star-empty"></i>';
	     	}
	     }
	}
	return qualityScoreBars;
}
////////////////////////////////////////////////
////////////////////////////////////////////////
function getMemberLevelPremiumHtml(planDetailsByMember, premium, aptc, premiumAfterCredit) {
	var tooltipHtml = '<div class=\'margin5-b\'><strong>' + plan_display['pd.label.memberPremium.title'] + '</strong></div>';
	for(var i = 0; i < planDetailsByMember.length; i ++) {
		tooltipHtml += plan_display['pd.label.memberPremium.memberAge'] + ' ' +planDetailsByMember[i].age + ' <span class=\'margin20-l pull-right\'>$' + planDetailsByMember[i].premium + '</span><br>';
	}

	tooltipHtml += '<div class=\'margin5-t\'>' + plan_display['pd.label.memberPremium.total'] + '<span class=\'pull-right\'>$' + premium + '</span></div>';
	tooltipHtml += '<div>' + plan_display['pd.label.memberPremium.aptc'] + '<span class=\'pull-right\'>-$' + aptc + '</span></div>';
	tooltipHtml += '<hr class=\'margin0\'>';
	tooltipHtml += plan_display['pd.label.memberPremium.netPremium'] + '<strong class=\'pull-right\'>$' + premiumAfterCredit + '</strong>';

	return tooltipHtml;
}


$('.changeLink').click(function() {
	if ($(this).hasClass('active') ){

		var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
		for(var i = 0; i < seekingCoverageDentalObj.length; i++)
		{
			if(seekingCoverageDentalObj[i].seekingCoverageDental=="Y")
			{
				$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", true );
			}
			else
			{
				$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", false );
			}
		}
		$('#shopping-modal-error').hide();
		$('#shopping-modal-error').html("");
		$('#shopping-modal').modal({
			backdrop: 'static',
		  	keyboard: false
		});
	}

});


$('a.accordion-toggle').click(function() {
	var elem = $(this).find('i')
	if(elem.hasClass('icon-chevron-down')) {
		elem.addClass('icon-chevron-right').removeClass('icon-chevron-down');
	}else {
		var accordionName = elem.closest('.accordion-toggle').text();
		accordionName = accordionName.trim();
		window.dataLayer.push({'event': 'compareEvent', 'eventCategory': 'Plan Selection - Compare', 'eventAction': 'Accordion Expansion', 'eventLabel': accordionName});
		elem.addClass('icon-chevron-down').removeClass('icon-chevron-right');
	}
});

var progressbarwrapPosition = $('.navbar').height();


$(document).ready(function() {

	var hasno = $('td').find('i');
	$(hasno).parent('td').addClass('txt-center');


	$('body').on('mouseenter focus', '[rel=tooltip]', function(){
		$(this).tooltip('show');
		//console.log($('.out-of-network-tooltip').closest('.tooltip').length);
		$('.pointerEventsTooltipLink').closest('.tooltip').css('pointer-events', 'auto');
	})

	$('body').on('blur', '[rel=tooltip]', function(){
		$(this).tooltip('hide');
	})

 	$("a[href='#'], .addtofav ").live('click', function(event){
		event.preventDefault();
 	});

//Loading
 window.setInterval(counter, 5000); //Repeat my counter function very 5 seconds
     var maxScore = 99; //Max score +1 so I can reset my score to 50 after it hits 99
     $(counter);
     function counter() {
         var currentScore = parseFloat($('.loading .circle-num').text()); //Grab the value
         currentScore += 1; //Iterate
         $('.loading .circle-num').text(currentScore); //Print new score into circle-num at a rate of 100 milli secs
         if (currentScore < maxScore)
             setTimeout(counter, 100);

         if (currentScore == 60) { //Tile is set to poor by default. If score is 60 set tile class to ok
             $(".loading .center-circle").fadeTo(200, "fast", function () {
                 $(this).parents('.tile').removeClass('poor').addClass('ok');
             });
         }
         if (currentScore == 80) { //If score is 80 set tile class to best
             $(".loading .center-circle").fadeTo(200, "fast", function () {
                 $(this).parents('.tile').removeClass('ok').addClass('best');
             });
         }
         if (currentScore === 99) { //If score is 90 set tile class to poor
             $('.loading .circle-num').text('50') //Change the circle number text to 50, so it can start again
             $(".loading .center-circle").fadeTo(200, "fast", function () {
                 $(this).parents('.tile').removeClass('best').addClass('poor');
             });
         }

         $(currentScore).each(function () { //Grab each score
             drawPie(currentScore); //Draw a pie at each score

             function drawPie(currentScore) {
                 var val = currentScore;    //Rename the score value as it iterates
                 var deg = 360 / 100 * currentScore;
                 $('.loading .pie').css({
                     '-moz-transform': 'rotate(' + deg + 'deg)',
                     '-webkit-transform': 'rotate(' + deg + 'deg)',
                     '-o-transform': 'rotate(' + deg + 'deg)',
                     'transform': 'rotate(' + deg + 'deg)'
                 });
             }
         }); //draw end
     }

	

});

	$(window).load(function() {
		
	// DataLayer event for filter checkboxes
		$('body').on('change', '.gtm_filter', function(){
			var categoryLabelText = $.trim($(this).closest('.ps-sidebar__filter').find('h3').text().replace('tooltip link', '')) + ' Filter Click';
			if($(this).prop('checked')){
				window.dataLayer.push({
					'event': 'leftnavfilterEvent', 
					'eventCategory': 'Plan Selection - Left Nav Filter', 
					'eventAction': categoryLabelText, 
					'eventLabel': $.trim($(this).data("filter-label"))
				});
			}
		})

		// DataLayer event for radio buttons
		// Note: The radio buttons are not loaded yet upon document.ready, so it has to be window.load.
		$('body').on('click', '.gtm_sort', function(){

			var labelText = $.trim($(this).parent().text().replace(/\s\s+/g,' '))

			window.dataLayer.push({
				'event': 'displaysortingEvent', 
				'eventCategory': 'Plan Selection - Plan Display Sorting', 
				'eventAction': 'Display Sorting Select', 
				'eventLabel': labelText
			});
		})

		// DataLayer event for continue to dental plans button
		$('body').on('click', '.gtm_continue_dentalplans', function(){

			var labelText = $.trim($(this).text().replace(/\s\s+/g,' '))

			window.dataLayer.push({
				'event': 'continueToDentalEvent', 
				'eventCategory': 'Plan Selection - Shop & Compare', 
				'eventAction': 'Continue to Dental Button Click', 
				'eventLabel': labelText
			});
		})

	})

  function resizeimage(imageObj){
  	  var maxWidth = 140;
  	  var maxHeight = 40;
        var width = imageObj.width;
        var height = imageObj.height;

        // Check if the current width is larger than the max
        if(width > maxWidth){
          ratio = maxWidth / width; // get ratio for scaling image
          imageObj.style.width =  maxWidth+'px'; // Set new width
          imageObj.style.height = (height * ratio)+'px'; // Scale height based on ratio
          height = height * ratio; // Reset height to match scaled image
          width = width * ratio; // Reset width to match scaled image
        }
        // Check if current height is larger than max
        if(height > maxHeight){
          ratio = maxHeight / height; // get ratio for scaling image
          imageObj.style.height= maxHeight+'px'; // Set new height
          imageObj.style.width = (width * ratio) +'px'; // Scale width based on ratio
        }
        imageObj.style.display = "block";
   }

  $.ajaxSetup ({
	    // Disable caching of AJAX responses
	    cache: false
	});

	window.onpopstate = function(e){
    var hashLocation = location.hash;
    if($('#view').val() == 'compare' && hashLocation != '#compare'){
    	App.views.compareView.backToAll(e);
	}

	if($('#view').val() == 'detail' && hashLocation != '#detail'){
		App.views.detailView.backToAll(e);
	}

	var hashLocation = location.hash;
	if(hashLocation == '#compare'){
		App.views.plans.comparePlansTrg(e);
	}
	if(hashLocation == '#detail'){
		App.views.plans.detailPlanTrg(e);
	}

	if($('#view').val() == '' && hashLocation == ''){
		App.views.plans.loadAll();
	}
	window.scrollTo(0,0);
  }


	  var convertToCSV = function(objArray) {

	        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	        var str = '';

	        for (var i = 0; i < array.length; i++) {
	            var line = '';
	            for (var index in array[i]) {
	                if (line != '') line += ','

	                line += array[i][index];
	            }

	            str += line + '\r\n';
	        }

	        return str;
	    };

	    var exportCSVFile = function(items, fileTitle) {

	        var csv = convertToCSV(items);

	        var exportedFilename = fileTitle + '.csv' || 'Individual-Plans.csv';

	        var blob = new Blob([csv], {
	            type: 'text/csv;charset=utf-8;'
	        });
	        if (navigator.msSaveBlob) { // IE 10+
	            navigator.msSaveBlob(blob, exportedFilename);
	        } else {
	            var link = document.createElement("a");
	            if (link.download !== undefined) { // feature detection
	                // Browsers that support HTML5 download attribute
	                var url = URL.createObjectURL(blob);
	                link.setAttribute("href", url);
	                link.setAttribute("download", exportedFilename);
	                link.style.visibility = 'hidden';
	                document.body.appendChild(link);
	                link.click();
	                document.body.removeChild(link);
	            }
	        }
	    };

	    // format date to 'mm/dd/yyyy'
	    var formatDate = function(date) {
	       if(date != undefined && date != null){
	          var year = date.getFullYear();
	          var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
	          var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

	          return month + '/' + day + '/' + year;
	       }
	    };
