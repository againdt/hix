<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "CREATION_TIMESTAMP <= systimestamp";
		
	}
%>

<sql:query dataSource="jdbc/ghixDS" var="result1">
select range1, count(range1) as total
from
(
select
CASE
	WHEN  FAMILY_SIZE = 0  THEN  '0' 
    WHEN  FAMILY_SIZE = 1  THEN  '1'    
    WHEN  FAMILY_SIZE = 2  THEN  '2'
    WHEN  FAMILY_SIZE = 3  THEN  '3'
    WHEN  FAMILY_SIZE = 4  THEN  '4'
    WHEN  FAMILY_SIZE = 5  THEN  '5'
    WHEN  FAMILY_SIZE = 6  THEN  '6'
    WHEN  FAMILY_SIZE = 7  THEN  '7'
    ELSE  '7+'
END AS range1
from(
select FAMILY_SIZE
from elig_lead
where <%=whereClause%>
order by FAMILY_SIZE
)
)
group by range1
order by range1
</sql:query> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Lead - Family Sizes</title>
  

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});

 function drawVisualization() {
        // Create and populate the data table.
        var data = google.visualization.arrayToDataTable([
          ['Range', 'Total'],
		<c:forEach var="row" items="${result1.rows}">
		['<c:out value="${row.range1}"/>',<c:out value="${row.total}"/>],
		</c:forEach>
        ]);
      
        // Create and draw the visualization.
        new google.visualization.ColumnChart(document.getElementById('visualization')).
            draw(data,
                 {title:"Family Sizes",
                  width:700, height:550,
                  hAxis: {title: "Age"}}
            );
      }    

    google.setOnLoadCallback(drawVisualization);
</script>
<script type="text/javascript">
	
$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
});

$(function() {
	$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
});

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	
	//Step 1: Get the Dates
	var date1 = document.getElementById('date1').value;
	var date2 = document.getElementById('date2').value;

	if (date1 && date2) {

		if (whereClause) {
			whereClause = whereClause
					+ "and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
			

		} else {
			whereClause = whereClause
					+ " CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
		}
		
		filterStatement = filterStatement + ' Start Date : ' + date1
		+ ' End date : ' + date2;
		
	} else {
		if (date1 && !date2) {
			if (whereClause) {

				whereClause = whereClause
					+ " and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				
			} else {
				whereClause = whereClause
						+ " CREATION_TIMESTAMP between to_timestamp('"
						+ date1
						+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				+ date1 + "'";
				
			}
			
			filterStatement = filterStatement + ' Start Date : ' + date1;
			
		} else {

			if (!date1 && date2) {
				if (whereClause) {
					whereClause = whereClause
							+ " and CREATION_TIMESTAMP <= to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				} else {
					whereClause = whereClause
							+ " CREATION_TIMESTAMP <=  to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				}
				
				filterStatement = filterStatement + ' End Date : ' + date2;
				
			}
			
			
		}

	}
	if (!whereClause) {
		whereClause = "CREATION_TIMESTAMP <= systimestamp";
	}
	
	document.getElementById('whereClause').value = whereClause;
	document.getElementById('filterStatement').value = filterStatement;
}

</script>
</head>
<body style="font-family: Arial;border: 0 none;">
<h4>Family Size - Shows the family sizes for requests</h4>
<form method="post" action="range_age">
	<table border="1" style="border-color:#C0C0C0">
		<tbody>
		<tr><td>
				Date Start:	<input type="text" id="date1" name="date1" size="10" maxlength="10"/> 
				End: <input type="text" id="date2" name="date2" size="10" maxlength="10"/>
			</td>
			<td>
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause();"/>
				<input id="whereClause" name="whereClause" type="hidden"/>
				<input id="filterStatement" name="filterStatement" type="hidden"/>
			</td>
		</tr>
		</tbody>
	</table>
</form>
<br>
<%
	if (request.getParameter("filterStatement") != null) {
		out.println("Search Criteria : " + request.getParameter("filterStatement"));
	}
%>
<table>
<tr><td>
<b>Family Sizes</b>
<div id="visualization"></div>
</td><td>
<b>Family Sizes</b>
<br>
<display:table name="${result1.rows}"  requestURI="range_age" id="table1" export="true" >
	<display:column property="range1" title="Age" sortable="false" />
	<display:column property="total" title="Total" sortable="false" />
</display:table>
</td>
</tr>
</table>
</body>
</html>

