package com.getinsured.eligibility.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * Eligibility Utils class.
 *
 * @author Ekram Ali Kazi
 *
 */
public final class EligibilityUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(EligibilityUtils.class);

	private static final XStream xstream = GhixUtils.getXStreamStaxObject();

	private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

	private static final String STATE_CODE_CA = "CA";
	private static final String STATE_CODE_NM = "NM";
	private static final String STATE_CODE_MS = "MS";
	private static final String STATE_CODE_PHIX = "PHIX";
	private static final String STATE_CODE_ID = "ID";
	private static final String STATE_CODE_MN = "MN";
	private static final String STATE_CODE_NV = "NV";

	private EligibilityUtils() {}

	public static String marshal(Object response) {
		return xstream.toXML(response);
	}


	public static Object unmarshal(String xmlString) {
		return xstream.fromXML(xmlString);
	}

	public static String readFile() {

		/*StringBuilder sb = null;

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(EligibilityUtils.class.getResourceAsStream("/state_request.txt"), "UTF-8"));) {
			sb = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			LOGGER.error("Error reading file - " + e);
		}

		return sb.toString();*/
		throw new GIRuntimeException("Method not in use!");
	}

	public static Date stringToDate(String dateStr ){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMDD");
		Date date = null;
		try{
			date = simpleDateFormat.parse(dateStr);
		}catch(ParseException e){
			LOGGER.error(e.getMessage()); 
		}
		return date; 
	}

	public static Date getDateWithoutTimeUsingFormat(Date datePassed) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date= formatter.parse(formatter.format(datePassed));
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());						
		}
		return date;
	}

	/**
	 * Checks if date2 is greater or equal date1(time in a day not considered)
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isEqualOrAfter(Date date1, Date date2){
		boolean isEqualOrBefore = false;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

			Date date3 = formatter.parse(formatter.format(date1));
			Date date4 = formatter.parse(formatter.format(date2));

			if(date4.compareTo(date3) >= 0 ){
				isEqualOrBefore= true;
			}
		}catch(Exception e){
			LOGGER.error("Error parsing date" , e);
			isEqualOrBefore = false;
		}
		return isEqualOrBefore;
	}

	public static boolean isNmCall(){
		boolean isNmCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_NM)){
			isNmCall = true;
		}
		return isNmCall;
	}

	public static boolean isCaCall(){
		boolean isCaCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}

		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_CA)){
			isCaCall = true;
		}
		return isCaCall;
	}

	public static boolean isMsCall(){
		boolean isMsCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_MS)){
			isMsCall = true;
		}
		return isMsCall;
	}

	public static boolean isPhixCall(){
		boolean isPhixCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_PHIX)){
			isPhixCall = true;
		}
		return isPhixCall;
	}
	public static boolean isIdCall(){
		boolean isIdCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_ID)){
			isIdCall = true;
		}
		return isIdCall;
	}
	public static boolean isMnCall(){
		boolean isMnCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_MN)){
			isMnCall = true;
		}
		return isMnCall;
	}
	public static boolean isNvCall(){
		boolean isNvCall = false;
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		if(STATE_CODE.equalsIgnoreCase(STATE_CODE_NV)){
			isNvCall = true;
		}
		return isNvCall;
	}

	public static void getStateCode(){
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public static String returnStateCode(){
		if(STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")){
			getStateCode();
		}
		return STATE_CODE; 
	}

}
