<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>
 <script src="//www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded" async defer></script>

 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget'
 };
 </script>
<div class="row-fluid" role="main">
	<div class="landing-signup">
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1><spring:message code="label.homePage.headerCopy"/></h1>
            </div>
        </div>
		<div class="row-fluid" role="main">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="register-group clearfix">
				<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 iconfamily-wrapper">
					<div class="icons-family"></div>
				</div>
				
				<div class="col-xs-12 col-sm-9 col-md-6 col-lg-7 maintext-wrapper">
					<h2><spring:message code="label.homePage.individual"/></h2> 
					<p><spring:message code="label.homePage.individualCopy"/></p>
				</div>
					
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 gutter0">
					<div class="shopbutton-wrapper pull-right">
						<a class="btn btn-fullwidth-xs" href="<c:url value='/preeligibility' />">START SHOPPING<i class="icon-large icon-arrow-right"></i><span class="aria-hidden">Individual</span></a>
					</div>
				</div>
				
			</div>
		</div>
		</div> <!-- end main -->
			
		<div class="row-fluid">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	 	 		<div class="access-code register-signin" ng-app="accessCodeApp" ng-controller="accessCodeController" ng-cloak>
	 	 			<form id="access-code-form" name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" novalidate autocomplete="off" class="margin30-t">
	 	 				<df:csrfToken/>
		 	 			<p for="access-code">
		 	 				<i class="icon-lock"></i><spring:message code="label.homePage.haveAccessCode"/>
		 	 			</p>
		 	 			<input type="text" id="accessCode" name="accessCode" length="10" required ng-focus ng-model="accessCode" ng-keypress="formSubmit($event)">
		 	 			<br>
		 	 			
		 	 			<div class="help-inline" id="accessCode_error" style="min-height: 35px">
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
							</label>
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
							</label>
						</div>
						
		 	 			<div class="error help-inline">
		 	 				<c:if test="${not empty errorMsg}">
   								${errorMsg}
							</c:if>
		 	 			</div>
							
						<!--custom captcha starts-->
						<div class="recap-div">
							<div vc-recaptcha key="siteKey" on-success="updateRecaptchaValidation()" class="g-recaptcha inline-block"></div>
						</div>
						<!--custom captcha ends-->

						<div id="access-code-button">
			 	 			<input type="submit" class="btn btn-primary btn-fullwidth-xs" value="Submit" ng-disabled="accessCodeForm.$invalid || !isRecaptchaValid">
		 	 			</div>
	 	 			</form>
	 	 			<div id="info" style="color: red;"></div>
				</div>
	 	 	</div> <!-- end span6 -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="register-signin">
                    <div class="margin30-t">
                        <p>
                            <i class="icon-lock"></i> <spring:message code="label.homePage.haveAccountQuestion"/>
                            <a class="btn btn-fullwidth-xs btn-primary" href="<c:url value='/account/user/login'/>"><spring:message code="label.homePage.haveAccountAction"/></a>
                        </p>
                    </div>
                </div>
            </div>
	 	 </div> <!-- end row-fluid -->
        <div class="row-fluid">
            <p class="margin10-t margin20-b access-code">Open Enrollment begins November 1, 2019 and ends December 15, 2019. If you need to make changes to your 2019 plan please visit <a href="https://www.healthcare.gov/">www.HealthCare.gov</a></p>
        </div>
	</div> 	<!-- end landing-signup -->
</div>	<!-- end main -->
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-recaptcha.min.js" />"></script>
<script>
function scaleCaptcha(elementWidth) {
	var reCaptchaWidth = 304;
	var containerWidth = $('.recap-div').width();

	if(reCaptchaWidth > containerWidth) {
		var captchaScale = containerWidth / reCaptchaWidth;
		$('.g-recaptcha').css({
		'transform':'scale('+captchaScale+')'
		});
	}
}

$(function() {
	$(window).resize(scaleCaptcha());
});
</script>
<script>
	
var accessCodeApp = angular.module('accessCodeApp', ['vcRecaptcha']);

accessCodeApp.controller('accessCodeController', function($scope) {
	$scope.siteKey = '<%=prop.getProperty("Recaptcha.siteKey")%>';

	$scope.isRecaptchaValid = false;
	$scope.updateRecaptchaValidation = function(){
		$scope.isRecaptchaValid = true;
	}
	
	$scope.formSubmit = function($event){
		 if($event.which === 13) {
			 $event.preventDefault();
			 if($scope.accessCodeForm.$valid === true){
				 $('#access-code-form').submit();
			}else if($scope.accessCodeForm.accessCode.$valid === false){
				$scope.accessCodeForm.accessCode.$dirty = true;				
			}
			
         }
		
	}
	
	
});

accessCodeApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);
</script>
