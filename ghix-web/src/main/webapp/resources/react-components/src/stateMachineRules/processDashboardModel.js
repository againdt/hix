import React from 'react';
import moment from 'moment';
import StateMachine from '../services/stateMachine';
import getSMFurtherActionRules from '../stateMachineRules/smFurtherActionRules';
import { RENEWAL_STATUSES, SUPER_USERS } from '../constants/dashboardModelTypes';

const ENROLLMENT_TYPES = {
    DENTAL: 'DENTAL',
    HEALTH: 'HEALTH'
};

/**
 * Runs state machine, which returns an object with btnText, paragraphText and destination link.
 * @param model - tab data
 * @param config - configurations
 * @param state - 'MN'/'CA' loads specific smRules
 */
const getFurtherActionData = (model, config, state) => {
    const rootState = 's_tabIsInsideOEEnrollmentWindow';
    let smFurtherActionsRes = {};
    const smFurtherActionRules = getSMFurtherActionRules(state);
    const onFinishCb = res => smFurtherActionsRes = res;

    let stateMachineFurtherActions = new StateMachine(smFurtherActionRules,
        rootState,
        onFinishCb);

    stateMachineFurtherActions.run(model, config);

    return smFurtherActionsRes;
};


/**
 * Returns an array of 'DENTAL' enrollments.
 * @param enrollments
 * @returns {*}
 */
const getDentalEnrollments = (enrollments) => {
    return enrollments && enrollments.filter(enrollment => {
        return enrollment.type.toUpperCase() === ENROLLMENT_TYPES.DENTAL;
    });
};

/**
 * Returns true if there is at least one 'DENTAL' type of an enrollment.
 * @param enrollments
 * @param config
 * @returns {*}
 */
const isEnrolledInDental = (enrollments, config) => {
    const { serverDate } = config;
    return enrollments && enrollments.some(i => i.type.toUpperCase() === ENROLLMENT_TYPES.DENTAL
        && !(i.enrollmentStatus === 'TERM' && moment(serverDate, "MM-DD-YYYY").isAfter(moment(i.coverageEndDate, "MM-DD-YYYY"))));
};

const isEnrolledInHealth = (enrollments, config) => {
    return enrollments && enrollments.some(i => i.type.toUpperCase() === ENROLLMENT_TYPES.HEALTH);
};

/**
 * Returns true if there is at least one AI/AN applicant
 * @param applicants
 * @returns {*}
 */
const isAIAN = applicants => applicants && applicants.some(i => i.nativeAmerican);

/**
 * Returns true if period is within OE Enrollment start date and OE enrollment end date.
 */
const isPeriodWithinDates = (config) => {
    let { serverDate, oepStartDate, oepEndDate } = config;

    return moment(serverDate, "MM-DD-YYYY").isBetween(moment(oepStartDate, "MM-DD-YYYY"), moment(oepEndDate, "MM-DD-YYYY"));
};

/**
 * Returns true if serverDate is before oepStartDate
 * @param applicationYear
 * @param config
 * @returns {boolean}
 */
const isPriorToOEStartDate = (applicationYear, config) => {
    const { serverDate, oepStartDate } = config;
    const appYearGreaterThanServerDate = parseInt(applicationYear) > parseInt(moment(serverDate,"MM-DD-YYYY").year());
    return appYearGreaterThanServerDate && moment(serverDate, "MM-DD-YYYY").isBefore(moment(oepStartDate, "MM-DD-YYYY"));
};

/**
 * Retruns true if at least one enrollment has allowChangePlan flag set to true;
 * @param enrollments
 * @param config
 * @returns {*}
 */
const isChangePlans = (enrollments, config) =>  {
    const { serverDate } = config;
    return enrollments && enrollments.some(i => i.allowChangePlan === true &&
        !(i.enrollmentStatus === 'TERM' && moment(serverDate, "MM-DD-YYYY").isAfter(moment(i.coverageEndDate, "MM-DD-YYYY"))));
};

const isSuperUser = (userRole) => {
    return SUPER_USERS.includes(userRole);
};

/**
 * Renewal Status is considered as 'Set' if the property is not null and Renewals status not equal to "NOT_TO_CONSIDER" OR "APP_ERROR" for either health or dental.
 * @param healthRenewalStatus
 * @param dentalRenewalStatus
 * @returns {boolean}
 */
const isRenewalStatusSet = (healthRenewalStatus, dentalRenewalStatus) => {
    if (!healthRenewalStatus && !dentalRenewalStatus) {
        return false;
    }
    return !([healthRenewalStatus, dentalRenewalStatus].every((renewalStatus) => {
        return renewalStatus === RENEWAL_STATUSES.NOT_TO_CONSIDER || renewalStatus === RENEWAL_STATUSES.APP_ERROR;
    }));
};

/**
 * Check if at least one person in the HH is QHP eligible.
 * @param applicants
 * @returns {*}
 */
const isQHPEligible = (applicants) => {
    return applicants && applicants.some((applicant) => applicant.exchangeEligible)
};

export {
    getFurtherActionData,
    getDentalEnrollments,
    isEnrolledInDental,
    isEnrolledInHealth,
    isAIAN,
    isPeriodWithinDates,
    isPriorToOEStartDate,
    isChangePlans,
    isSuperUser,
    isRenewalStatusSet,
    isQHPEligible
}