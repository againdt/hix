package com.getinsured.ssap.model.financial;

import java.util.StringJoiner;

/**
 * Describes IFSV social security card of the household member {@link IfsvHouseholdMember}.
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public class SocialSecurityCard {
  private String socialSecurityNumber;

  public SocialSecurityCard(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
  }

  public String getSocialSecurityNumber() {
    return socialSecurityNumber;
  }

  public void setSocialSecurityNumber(String socialSecurityNumber) {
    this.socialSecurityNumber = socialSecurityNumber;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", SocialSecurityCard.class.getSimpleName() + "[", "]")
        .add("socialSecurityNumber='" + socialSecurityNumber + "'")
        .toString();
  }
}
