package com.getinsured.hix.batch.web.task;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.platform.util.GhixEndPoints.WebServiceEndPoints;


public class CalheersDataUploadTask implements Tasklet{
	
	private static final Logger LOGGER = Logger.getLogger(CalheersDataUploadTask.class);
	
	private String url = WebServiceEndPoints.GHIX_WEB_PROVIDER_MANAGEMENT_CALHEERS_DATA_UPLOAD_URL;
	private static ObjectMapper mapper = new ObjectMapper();
	

	private RestTemplate restTemplate;
	



	public RestTemplate getRestTemplate() {
		return restTemplate;
	}




	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}




	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
			
		LOGGER.info("Calheers Data Upload Job called");
		try{
		restTemplate.postForObject(url, null, String.class);
		}catch(Exception e){
			LOGGER.info("Error :"+e.getMessage());
		}
		return null;
	}
	
	
}
