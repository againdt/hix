/**
 * 
 */
package com.getinsured.eligibility.verification.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Used for defining all the constants for Verification Rules
 */
@Component
public final class SSAPConstants {
	
	public enum MEC_TYPE {
		ESI, NON_ESI;
	}
	
	public enum ORGANIZATION_CODES{
		MEDI("MEDI"),TRIC("TRIC"),PECO("PECO"),BHPC("BHPC"),VHAC("VHAC");

		ORGANIZATION_CODES(String s) {
	    }
	}
	
	public enum VERIFICATION_TYPE{
		SSN,DEATH,INCARCERATION,RESIDENCY,CITIZENSHIP,MEC,VLP,INCOME,EQUIFAX;

	}
	public enum RULES{
		SSN_VERIFICATION_RULE,DEATH_VERIFICATION_RULE,INCARCERATION_VERIFICATION_RULE,RESIDENCY_VERIFICATION_RULE,CITIZENSHIP_VERIFICATION_RULE,
		MEC_VERIFICATION_RULE,VLP_VERIFICATION_RULE,INCOME_VERIFICATION_RULE,EQUIFAX_VERIFICATION_RULE;
	}

	public static final long PRIMARY_APPLICANT_PERSON_ID_LONG = 1;
	public static final String PRIMARY_APPLICANT_PERSON_ID = "1";
	public static final String HUB = "HUB";
	public static final String ACTIVE = "Active";
	public static final String COLON = ":";
	public static final String VERIFIED = "VERIFIED";
	public static final String NOT_VERIFIED = "NOT VERIFIED";
	public static final String PENDING = "PENDING";
	public static final String EQUIFAX_PENDING = "EQUIFAX_PENDING";
	public static final String FAILED = "FAILED";
	public static final String CONFIRMED = "Confirmed";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failed";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final int SSN_VERIFICATION_RULE = 1;
	public static final int DEATH_VERIFICATION_RULE = 2;
	public static final int INCARCERATION_VERIFICATION_RULE = 3;
	public static final int RESIDENCY_VERIFICATION_RULE = 4;
	public static final int CITIZENSHIP_VERIFICATION_RULE = 5;
	public static final int MEC_VERIFICATION_RULE = 6;
	public static final int VLP_VERIFICATION_RULE = 7;
	public static final int INCOME_VERIFICATION_RULE = 8;
	public static final int EQUIFAX_VERIFICATION_RULE = 9;
	public static final String SLASH = "/";
	public static final String YES = "Y";
	public static final String PARTIAL = "P";
	public static final String NO = "N";
	public static final String UNDERSCORE = "_";
	public static final String VERIFICATION_RULE = "VERIFICATION_RULE";
	public static final String EQUIFAX_SUCCESS = "HS000000";
	public static final String NO_SSN = "NO_SSN";
	public static final String ZERO = "0";
	public static final String NO_DOCUMENT = "NO_DOCUMENT";
	
	//Json Parsing Parameters
	public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	public static final String HOUSEHOLD_MEMBER = "householdMember";
	public static final String TAX_HOUSEHOLD= "taxHousehold";
	public static final String SSAP_APPLICANT = "ssapApplicants";
	public static final String EMBEDDED = "_embedded";
	public static final String LINKS = "_links";
	public static final String SELF = "self";
	public static final String HREF = "href";
	public static final String FIND_BY_EXTERNAL_APPLICANT_ID_URL = "SsapApplicant/search/findByExternalApplicantId?externalApplicantId=";
	public static final String SSAP_APPLICANT_UPDATE_URL = "SsapApplicant/{id}";
	public static final String SSAP_APPLICANT_RECORD_ID = "id";
	public static final String ELIGIBLE_FOR_EIS_INDICATOR = "EligibleForEIS";
	public static final String IS_INSURED_INDICATOR = "IsInsured";
	public static final String SSA_COMPOSITE_RESPONSE = "SSACompositeResponse";
	public static final String SSA_COMPOSITE_INDIVIDUAL_RESPONSE = "SSACompositeIndividualResponse";
	public static final String PERSON_SSN_IDENTIFICATION = "PersonSSNIdentification";
	public static final String EQUIFAX_PERSON_SSN_IDENTIFICATION = "personSSNIdentification";
	public static final String MEC_INFO = "MECInfo";
	public static final String APPLICANT_RESPONSE = "ApplicantResponse";
	public static final String RESPONSE_SET = "ResponseSet";
	public static final String INDIVIDUAL_RESPONSE_SET = "IndividualResponseSet";
	public static final String INDIVIDUAL_RESPONSE = "IndividualResponse";
	public static final String INDIVIDUAL_RESPONSE_TYPE = "IndividualResponseType";
	public static final String SSN = "SSN";
	public static final String PERSON = "Person";
	public static final String OTHER_COVERAGE = "OtherCoverage";
	public static final String OTHER_COVERAGES = "OtherCoverages";
	public static final String ORGANIZATION_CODE = "OrganizationCode";
	public static final String MEC_COVERAGE_TYPE = "MECCoverageType";
	public static final String MEC_VERIFICATION_CODE = "MECVerificationCode";
	public static final String AGENCY3_INIT_VERIF_RESPONSE_SET = "Agency3InitVerifResponseSet";
	public static final String AGENCY3_INIT_VERIF_INDIVIDUAL_RESPONSE = "Agency3InitVerifIndividualResponse";
	public static final String APPLICATION_PERSON_ID = "applicationid_personid";
	public static final String LAWFUL_PRESENCE_VERIFIED_CODE = "LawfulPresenceVerifiedCode";
	public static final String APPLICANT = "Applicant";
	public static final String APPLICANT_TYPE = "ApplicantType";
	public static final String PERSON_ID = "personId";
	public static final String SSAP_APPLICATION_ID = "ssapApplicationId";
	public static final String HOUSEHOLD = "Household";
	public static final String APPLICANT_VERIFICATION = "ApplicantVerification";
	public static final String TAX_RETURN = "TaxReturn";
	public static final String TAX_RETURN_MAGI_AMOUNT = "TaxReturnMAGIAmount";
	public static final String IRS_INCOME = "Income";
	public static final String HOUSEHOLD_INCOME = "houseHoldIncome";
	public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
	public static final String SOCIAL_SECURITY_CARD_HOLDER_INDICATOR = "socialSecurityCardHolderIndicator";
	public static final String REASONABLE_EXPLANATION_FOR_NO_SSN = "reasonableExplanationForNoSSN";
	public static final String DEATH_INDICATOR = "deathIndicator";
	public static final String INCARCERATION_STATUS = "incarcerationStatus";
	public static final String INCARCERATION_STATUS_INDICATOR = "incarcerationStatusIndicator";
	public static final String HEALTH_COVERAGE = "healthCoverage";
	public static final String CURRENT_OTHER_INSURANCE = "currentOtherInsurance";
	public static final String MEDICARE_ELIGIBLE_INDICATOR = "medicareEligibleIndicator";
	public static final String TRICARE_ELIGIBLE_INDICATOR = "tricareEligibleIndicator";
	public static final String PEACE_CORPS_INDICATOR = "peaceCorpsIndicator";
	public static final String OTHER_STATE_OR_FEDERAL_PROGRAM_INDICATOR = "otherStateOrFederalProgramIndicator";
	public static final String LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR = "livesWithHouseholdContactIndicator";
	public static final String HOUSEHOLD_CONTACT = "householdContact";
	public static final String LIVES_AT_OTHER_ADDRESS_INDICATOR = "livesAtOtherAddressIndicator";
	public static final String OTHER_ADDRESS = "otherAddress";
	public static final String ADDRESS = "address";
	public static final String LIVING_OUTSIDE_OF_STATE_TEMPORARILY_INDICATOR = "livingOutsideofStateTemporarilyIndicator";
	public static final String HOUSEHOLD_CONTACT_INDICATOR = "householdContactIndicator";
	public static final String TAX_FILER = "taxFiler";
	public static final String TAX_FILER_DEPENDENTS = "taxFilerDependants";
	public static final String HOME_ADDRESS = "homeAddress";
	public static final String STATE = "state";
	public static final String MAILING_ADDRESS = "mailingAddress";
	public static final String DEPENDANT_HOUSEHOLD_MEMBER_ID = "dependantHouseholdMemeberId";
	public static final String ANNUAL_COMPENSATION = "AnnualCompensation";
	public static final String ANNUAL_COMPENSATION_INFORMATION = "AnnualCompensationInformation";
	public static final String CURRENT_INCOME_INFORMATION = "CurrentIncomeInformation";
	public static final String EMPLOYEE_INFORMATION = "EmployeeInformation";
	public static final String EMPLOYMENT_INFORMATION = "EmploymentInformation";
	public static final String EMPLOYEE_STATUS_CODE = "EmployeeStatusCode";
	public static final String RESPONSE_METADATA = "ResponseMetadata";
	public static final String RESPONSE_CODE = "ResponseCode";
	public static final String RESPONSE_INFORMATION = "ResponseInformation";
	public static final String INCOME_YEAR = "IncomeYear";
	public static final String TOTAL_COMPENSATION = "TotalCompensation";
	public static final String VERIFIED_SOURCE_INCOME = "verifiedSourceIncome";
	public static final String PRIMARY_TAX_FILER_PERSON_ID = "primaryTaxFilerPersonId";
	public static final String CITIZENSHIP_IMMIGRATION_STATUS = "citizenshipImmigrationStatus";
	public static final String CITIZENSHIP_STATUS_INDICATOR = "citizenshipStatusIndicator";
	public static final String ETHNICITY_AND_RACE = "ethnicityAndRace";
	public static final String HISPANICLATINOSPANISHORIGININDICATOR =  "hispanicLatinoSpanishOriginIndicator";
	public static final String RACE =  "race";
	public static final String ETHNICITY =  "ethnicity";
	public static final String CODE =  "code";
	public static final String LABEL =  "label";
	public static final String OTHER_LABEL =  "otherLabel";
	public static final String PHONE =  "phone";
	public static final String PHONE_EXTENSION  =  "phoneExtension";
	public static final String PHONE_NUMBER =  "phoneNumber";
	public static final String PHONE_TYPE =  "phoneType";
	public static final String OTHER_PHONE =  "otherPhone";
	public static final String SSN_VERIFICATION_INDICATOR = "SSAResponse/SSNVerificationIndicator";
	public static final String DEATH_CONFIRMATION_CODE = "SSAResponse/DeathConfirmationCode";
	public static final String PRISONER_IDENTIFICATION = "SSAResponse/SSAIncarcerationInformation/PrisonerIdentification";
	public static final String PRISONER_CONFINEMENT_DATE = "SSAResponse/SSAIncarcerationInformation/PrisonerConfinementDate";
	public static final String PERSON_US_CITIZEN_INDICATOR = "SSAResponse/PersonUSCitizenIndicator";
	public static final String PERSON_INCARCERATION_INFORMATION_INDICATOR = "SSAResponse/PersonIncarcerationInformationIndicator";
	public static int IRS_INCOME_COMPATIBILITY_RANGE;
	public static int EQUIFAX_COMPATIBILITY_RANGE;
	public static final String BLOOD_RELATIONSHIPS =  "bloodRelationship";
	public static final String INDIVIDUAL_PERSONID =  "individualPersonId";
	public static final String RELATED_PERSONID =  "relatedPersonId";
	public static final String RELATION =  "relation";
	public static final String TEXT_DEPENDENCY =  "textDependency";
	public static final int ELEVEN = 11;
	public static final int THIRTY_ONE = 31;
	public static final String CONTACT_PREFERENCES = "contactPreferences";
	public static final String PREFERRED_WRITTEN_LANGUAGE = "preferredWrittenLanguage";
	public static final String PREFERRED_SPOKEN_LANGUAGE = "preferredSpokenLanguage";
	public static final String SEEKS_QHP = "seeksQhp";
/*	public static String STATE_EXCHANGE;
	
	@Value("#{verificationRules['state_exchange']}")
	public void setSTATE_EXCHANGE(String pSTATE_EXCHANGE) {
		STATE_EXCHANGE = pSTATE_EXCHANGE;
	}
*/
	@Value("#{verificationRules['irs_income_compatibility_range']}")
	public void setIRS_INCOME_COMPATIBILITY_RANGE(
			int pIRS_INCOME_COMPATIBILITY_RANGE) {
		IRS_INCOME_COMPATIBILITY_RANGE = pIRS_INCOME_COMPATIBILITY_RANGE;
	}

	@Value("#{verificationRules['equifax_compatibility_range']}")
	public void setEQUIFAX_COMPATIBILITY_RANGE(
			int pEQUIFAX_COMPATIBILITY_RANGE) {
		EQUIFAX_COMPATIBILITY_RANGE = pEQUIFAX_COMPATIBILITY_RANGE;
	}
}
