
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "applicationId",
    "clientIp",
    "operation",
    "payload"
})
public class NonEsiMecVerificationRequest implements Serializable
{

    @JsonProperty("applicationId")
    private Long applicationId;
    @JsonProperty("clientIp")
    private String clientIp;
    @JsonProperty("operation")
    private String operation;
    @JsonProperty("payload")
    private NonEsiMecPayload payload;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -1150156729212360654L;

    @JsonProperty("applicationId")
    public Long getApplicationId() {
        return applicationId;
    }

    @JsonProperty("applicationId")
    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    @JsonProperty("clientIp")
    public String getClientIp() {
        return clientIp;
    }

    @JsonProperty("clientIp")
    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    @JsonProperty("payload")
    public NonEsiMecPayload getPayload() {
        return payload;
    }

    @JsonProperty("payload")
    public void setPayload(NonEsiMecPayload payload) {
        this.payload = payload;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("applicationId", applicationId).append("clientIp", clientIp).append("operation", operation).append("payload", payload).append("additionalProperties", additionalProperties).toString();
    }

}
