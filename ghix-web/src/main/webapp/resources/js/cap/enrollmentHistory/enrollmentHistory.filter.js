(function() {
	"use strict";
	angular.module("enrollmentHistoryApp").filter('uiDateFormat', uiDateFormat);
	function uiDateFormat(){
		return function(dbDate){			
			var formattedDate = new Date(dbDate);

			var dd = formattedDate.getDate();
			var mm =  formattedDate.getMonth() + 1;
			var yyyy = formattedDate.getFullYear();
			
			
			dd = dd < 10 ? '0'+dd : dd;
			mm = mm < 10 ? '0'+mm : mm;

			return  mm + "/" + dd + "/" + yyyy;
					
		};
	}
	
	
	
	angular.module("enrollmentHistoryApp").filter('phoneFormat', phoneFormat);
	function phoneFormat(){
		return function(phoneNumber){
			if(phoneNumber === undefined){
				return;
			}
			return '(' + phoneNumber.substring(0, 3) + ')' + ' ' + phoneNumber.substring(3, 6) + '-' + phoneNumber.substring(6, 10);
		};
	}
	
	
	angular.module("enrollmentHistoryApp").filter('firstLastCharacterTrim', firstLastCharacterTrim);
	function firstLastCharacterTrim(){
		return function(input){
			return input.substring(1, input.length-1);
		};
	}
	
	
	angular.module("enrollmentHistoryApp").filter('numberFormat', numberFormat);
	function numberFormat(){
		return function(input){
			return parseInt(input);
		};
	}
	
	
	angular.module("enrollmentHistoryApp").filter('ssnFormat', ssnFormat);
	function ssnFormat(){
		return function(input){
			if(input === undefined || input === ''){
				return 'N/A';
			}
			return '***' + input.substring(5, 9);
		};
	}
	
	
	angular.module("enrollmentHistoryApp").filter('monthName', monthName);
	function monthName(){
		return function(input){
			var monthNameArray = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			return monthNameArray[input - 1];
		};
	}
	
	angular.module("enrollmentHistoryApp").filter('titlecase', titlecase);
	function titlecase() {
	    return function (input) {
	        var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

	        input = input.toLowerCase();
	        return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
	            if (index > 0 && index + match.length !== title.length &&
	                match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
	                (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
	                title.charAt(index - 1).search(/[^\s-]/) < 0) {
	                return match.toLowerCase();
	            }

	            if (match.substr(1).search(/[A-Z]|\../) > -1) {
	                return match;
	            }

	            return match.charAt(0).toUpperCase() + match.substr(1);
	        });
	    }
	}
	
	
})();