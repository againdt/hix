
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/ng-template" id="home.jsp">
  <div class="gutter10">
    <div class="row-fluid">
      <div id="sidebar" class="span3">
        <div class="header">
          <h4><spring:message code="label.lce.sidebar.leftheader"/></h4>
        </div><!-- header -->
        <!-- Sidebar of what the user selects -->
        <ul class="nav nav-list sidebar" ng-repeat="individualItem in lifeEvents | filter:{value:'true'}">
          <li class="selectedItem active">{{individualItem.eventName}}</li>
        </ul><!-- Sidebar of what the user selects -->
        <ul class="nav nav-list sidebar">
          <li ng-if="(lifeEvents |filter:{value:'true'}).length <= 0"><spring:message code="label.lce.sidebar.nolceselected"/></li>
        </ul>

      </div><!-- sidebar -->

      <div id="rightpanel" class="span9">
        <div class="header">
          <!-- PROPERTIES -->
          <h4><spring:message code="label.lce.maintitle"/></h4>
        </div><!-- header -->

        <section>
          <div class="margin20 gutter10 alert alert-info">
            <p><spring:message code="label.lce.mainjsp.alertp1" htmlEscape="false"/></p>
            <p><spring:message code="label.lce.mainjsp.alertp2" htmlEscape="false"/></p>
          <!--  <p><spring:message code="label.lce.mainjsp.alertp3"/> </p> -->
          </div>
        </section>

        <section class="clearfix">
          <div class="fixme pull-right">
            <a ng-if="(lifeEvents |filter:{value:'true'}).length > 0" class="btn btn-primary pull-right" ng-click="next('/next')"><spring:message code="label.lce.continue"/></a>
          </div>
        </section>

        <section id="set-1" class="clearfix">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
            <label ng-repeat="individualItem in lifeEvents" class="checkbox inline" >
    		  <input type="checkbox" ng-model="individualItem.value" ng-if="individualItem.eventCode !=otherCategory && !((lifeEvents |filter:{eventCode:otherCategory,value:true}).length > 0)" >
			  <input type="checkbox" ng-model="individualItem.value" ng-if="individualItem.eventCode == otherCategory && (lifeEvents |filter:{eventCode:'!'+otherCategory,value:true}).length <= 0" >
			  <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                <img ng-src="{{individualItem.image}}" alt="Lost coverage" pagespeed_url_hash="2832760219">
                <div class="emptygreendiv"></div>
                <div></div>
                <div class="eventnameblock">
                  <p>
                    {{individualItem.eventName}}
                    <!-- Tool Tips -->
					<a ng-if="!individualItem.tooltipHeading" class="ttclass" rel="tooltip" href="javascript:void(null);" data-original-title="{{individualItem.reason}}">
                      <i class="icon-question-sign"></i>
                    </a>
                    <a ng-if="individualItem.tooltipHeading" class="ttclass" rel="tooltip" href="javascript:void(null);" data-original-title="<b>{{individualItem.eventName}}</b>: {{individualItem.reason}}">
                      <i class="icon-question-sign"></i>
                    </a>
                  </p>
                </div>
              </span>
            </label>
          </div>
        </section>

        <!-- NEXT BUTTON -->
        <dl class="dl-horizontal pull-right">
          <a ng-if="(lifeEvents |filter:{value:'true'}).length > 0" class="btn btn-primary" ng-click="next('/next')"><spring:message code="label.lce.continue"/></a>
        </dl><!-- NEXT BUTTON -->

      </div><!-- rightside -->
    </div> <!-- row-fluid -->
  </div><!-- gutter10 -->
</script>