package com.getinsured.hix.broker.service.jpa;

import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.ATTACHMENT;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.ATTACHMENT_COL_CONTRACT;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.ATTACHMENT_COL_E_O_DECL_PAGE;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.ATTACHMENT_COL_SUPPORTING;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.COMMENT;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.COMMENT_COL;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.MODEL_NAME;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.NEW_STATUS;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.PREVIOUS_STATUS;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.STATUS_COL;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.UPDATED_DATE_COL;
import static com.getinsured.hix.broker.history.BrokerHistoryRendererImpl.UPDATE_DATE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.agency.utils.AgencyUtils;
import com.getinsured.hix.broker.history.BrokerActivityStatusHistoryRendererImpl;
import com.getinsured.hix.broker.repository.IBrokerDocumentRepository;
import com.getinsured.hix.broker.repository.IBrokerNumberRepository;
import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.agency.AgencyBrokerDTO;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyDetailsDto;
import com.getinsured.hix.dto.agency.AgencyDetailsListDTO;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.SearchAgencyDto;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.broker.BrokerExportDTO;
import com.getinsured.hix.dto.broker.BrokerExportResponseDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.BrokerDocuments;
import com.getinsured.hix.model.BrokerNumber;
import com.getinsured.hix.model.BrokerPhoto;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.DelegationResponseUtils;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

/**
 * BrokerSeriveImpl is the implementation of {@link BrokerService}
 *
 */
@Service("brokerService")
@Repository
@Transactional
public class BrokerServiceImpl implements BrokerService, ApplicationContextAware {

	private static final String AGENCY_ID = "agencyId";

	private static final String AGENCY_NAME = "agencyName";

	private static final String ESCAPED_CHAR_PERCENTAGE = "\\%";

	private static final String LIKE_ESCAPE_CHAR = "\\";

	private static final int ZERO = 0;
	
	private static final double STATE_WIDTH_FACTOR = 4;

	private static final double MILES_FACTOR = 69;

	private static final String SUCCESS = "SUCCESS";

	private static final String FAILURE = "FAILURE";

	private static final String EMPLOYER = "Employer";

	private static final String RECORD_COUNT = "recordCount";

	private static final String BROKERLIST = "brokerlist";

	private static final String LOCATION_ZIP = "location.zip";

	private static final String CLIENTS_SERVED = "clientsServed";

	private static final String USER_FIRST_NAME = "user.firstName";

	private static final String USER_LAST_NAME = "user.lastName";

	private static final String COMPANY_NAME = "companyName";
	
	private static final String LICENSE_NUMBER = "licenseNumber";
	
	private static final String NPN = "npn";
	
	private static final String CONTACT_NUMBER = "contactNumber";
	private static final String BUSINESS_CONTACT_PHONE_NUMBER = "businessContactPhoneNumber";
	private static final String ALTERNATE_PHONE_NUMBER = "alternatePhoneNumber";

	private static final String LANGUAGES_SPOKEN = "languagesSpoken";

	private static final String CERTIFICATION_STATUS = "certificationStatus";

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);

	private static final int COL_PHOTO = 1;

	private static final String Y = "Y";
	
	private static final String N = "N";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String TARGET_NAME = "BROKER";

	@Autowired
	private HistoryRendererService brokerHistoryRendererImpl;
	
	@Autowired
	private HistoryRendererService brokerActivityStatusHistoryRendererImpl;

	@Autowired
	private IBrokerRepository brokerRepository;

	@Autowired
	private IBrokerDocumentRepository brokerDocumentsRepository;

	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;

	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;

	@Autowired
	private UserService userService;

	@Autowired
	private DelegationResponseUtils delegationResponseUtils;

	@Autowired
	private IBrokerNumberRepository brokerNumberRepository;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Autowired
	private ZipCodeService zipCodeService;

	@Autowired
	private ContentManagementService ecmService;

	@Autowired 
	private AccountActivationService accountActivationService;

	@Autowired 
	private BrokerIndTriggerService brokerIndTriggerService;
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private AgencyUtils agencyUtils;
	
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	
	@Autowired 
	private CommentTargetService commentTargetService;
	
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	
	private static final String DATE_STRING = "Date";
	private static final String NEW_CERTIFICATION_STATUS = "New Status";
	private static final String VIEW_ATTACHMENT = "View Attachment";
	private static final String VIEW_COMMENTS = "View Comment";
	private static final String PERCENTAGE = "%";
	private static final String EMPTY_STRING = "";
	private static final String ZERO_STRING ="0";
	private static final int COL_BROKER_ID = 0;
	private static final int COL_CONTACT_NUMBER = 1;
	private static final int COL_PRODUCT_EXPERTISE = 2;
	private static final int COL_LANGUAGES_SPOKEN = 3;
	private static final int COL_FIRST_NAME = 4;
	private static final int COL_LAST_NAME = 5;
	private static final int COL_EMAIL = 6;
	private static final int COL_DISTANCE = 7;
	private static final int COL_ADDRESS1 = 8;
	private static final int COL_ADDRESS2 = 9;
	private static final int COL_CITY = 10;
	private static final int COL_STATE = 11;
	private static final int COL_ZIP = 12;
	private static final int COL_COMPANY_NAME = 13;
	private static final double ZERO_DISTANCE = 0.0;
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String AGENT_STATUS = "agentStatus";
	public static final String LANGUAGE_SPOKEN_PARAM = "languageSpoken_";
	public static final String DISTANCE = "distance";
	public static final String ZIP = "zip";
	public static final String ZIP_SELECT = "zip1";
	public static final String BROKER_LIST = "brokerlist";
	private static final String EXCEPTION_OCCURRED_WHILE_SEARCHING_BROKERS = "Exception occurred while searching brokers ";
	private static final String LATTITUDE_CONST = "lattitude";
	private static final String LONGITUDE_CONST = "longitude";
	private static final int DISTANCE_FOR_OPTION_ANY = 4000;
	private static final String BROKER_IMAGE = "broker_image";
	private static final String JPG = "jpg";
	private static final String SLASH = "/";
	private static final String BROKER_IMAGES_FOLDER = "BROKER_IMAGES_FOLDER";
	private static final int COL_BROKER_IMAGE_DOCUMENT_ID = 2;
	private static final String RANGE_CHAR_UNDERSCORE = "[_]";
	private static final String ESCAPED_CHAR_UNDERSCORE = "\\_";
	private static final String APPLICATION_DATE = "applicationDate";
	private static final int COL_USER_ID = 18;
	private static final String UI_DATE_FORMAT = "MM-dd-yyyy";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final Integer PAGE_SIZE = 10;
	private static final String BUSINESS_LEGAL_NAME = "businessLegalName";
	private static final String SUBMITTED_ON = "creationTimestamp";
	private static final String CERTIFIED_ON = "certificationDate";
	private static final String BUSINESS_NAME = "businessLegalName";
	private static final String FUNCTION_REPLACE = "replace";
	private static final String DASH = "-";
	

	@Autowired private EERestCallInvoker restClassCommunicator;
	 	
	@Autowired private GhixRestTemplate ghixRestTemplate;

	private ApplicationContext appContext;

	/**
	 * @see BrokerService#saveBrokerWithLocation(Broker, boolean)
	 */
	@Override
	@Transactional
	public Broker saveBrokerWithLocation(Broker broker) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveBrokerWithLocation : START-> "+startTime);
		Broker brokerObj = null;
		
		try {
			brokerObj = brokerRepository.saveAndFlush(broker);

			// Generate Broker Number if does not exist
			if (brokerObj != null && brokerObj.getId() != 0
					&& (brokerObj.getBrokerNumber() == null || brokerObj.getBrokerNumber() == 0)) {
				generateBrokerNumber(brokerObj);
			}
			brokerObj = brokerRepository.saveAndFlush(brokerObj);
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				if (broker != null
						&& !certification_status.Incomplete.toString().equalsIgnoreCase(broker.getCertificationStatus())) {
					PaymentMethods paymentMethods = getPaymentMethodDetailsForBroker(brokerObj.getId(),
				 	 	 	PaymentMethods.ModuleName.BROKER);

					LOGGER.info("Send Broker Details call starts");
					brokerIndTriggerService.triggerInd35(broker, paymentMethods);
					LOGGER.info("Send Broker Details call ends");
				}
			}

		LOGGER.info("saveBrokerWithLocation: END");
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveBrokerwithLocation method", exception);

		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveBrokerWithLocation : END->"+endTime);
		return brokerObj;
	}

	/**
	 * Retrieves {@link BrokerNumber} if exists, else generate broker number and
	 * set into {@link Broker} object
	 *
	 * @param broker
	 *            {@link Broker}
	 */
	private void generateBrokerNumber(Broker broker) {
		LOGGER.info("generateBrokerNumber : START");
		BrokerNumber brokerNumberObj = brokerNumberRepository.findByBrokerId(broker.getId());
		if (brokerNumberObj == null) {
			LOGGER.debug("Generating unique broker number for Broker : " + broker.getId());
			BrokerNumber brokerNumber = new BrokerNumber();
			brokerNumber.setBrokerId(broker.getId());
			brokerNumberObj = brokerNumberRepository.saveAndFlush(brokerNumber);
			LOGGER.debug("Broker Number is : " + brokerNumberObj.getId());
		}
		broker.setBrokerNumber(brokerNumberObj.getId());
		LOGGER.info("generateBrokerNumber : END");
	}

	/**
	 * @see BrokerService#findBroker(Broker, Integer, Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findBroker(Broker broker, Integer startRecord, Integer pageSize) throws GIRuntimeException 
	{
		LOGGER.info("findBroker : START");
		Map<String, Object> brokerListAndRecordCount = null;

		try {
			QueryBuilder<Broker> brokerQuery = delegateFactory.getObject();

			brokerQuery.buildSelectQuery(Broker.class, Arrays.asList("user", "location"));

			brokerQuery.applySelectColumns(Arrays.asList("id", USER_FIRST_NAME, USER_LAST_NAME, APPLICATION_DATE,"companyName", "certificationDate", "deCertificationDate", CERTIFICATION_STATUS));

			createWhereCriteriaForQuery(broker, brokerQuery);

			try {
				AccountUser user = userService.getLoggedInUser();
				if (userService.hasUserRole(user, RoleService.EMPLOYER_ROLE) && !(userService.hasUserRole(user, RoleService.OPERATIONS_ROLE) || userService.hasUserRole(user, RoleService.ADMIN_ROLE))) {
					//AND condition added to this if to resolve issue HIX-29152
					String strClinetsServed = EMPLOYER;
					brokerQuery.applyWhere(CLIENTS_SERVED, strClinetsServed.toUpperCase(), DataType.STRING,
							ComparisonType.LIKE);
				}
				brokerQuery.applySort(APPLICATION_DATE, SortOrder.DESC);
			} catch (InvalidUserException ex) {
				LOGGER.error("User not logged in", ex);				
			}

			try {
				brokerListAndRecordCount = new HashMap<String, Object>();

				brokerQuery.setFetchDistinct(true);

				List<Map<String, Object>> brokers = brokerQuery.getData(startRecord, pageSize);

				if(brokers != null){
					brokerListAndRecordCount.put(BROKERLIST, brokers);
				}
				if(brokerQuery.getRecordCount() != null){
					brokerListAndRecordCount.put(RECORD_COUNT, brokerQuery.getRecordCount());
				}
			}
			catch(Exception ex) {
				LOGGER.error("Agent Search", ex);				
			}
		}
		catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occured while fetching Agents : ", giexception);
           throw giexception;
		}
		finally {
			LOGGER.info("findBroker : END");
		}

		return brokerListAndRecordCount;
	}

	/**
	 * Method to create Where criteria in query.
	 *
	 * @param broker The current Broker instance.
	 * @param brokerQuery The current query's  QueryBuilder instance.
	 * @throws NumberFormatException The NumberFormatException instance.
	 */
	private void createWhereCriteriaForQuery(Broker broker, QueryBuilder<Broker> brokerQuery) {
		createWhereCriteriaForQueryForName(broker, brokerQuery);

		String companyName = (broker != null) ? broker.getCompanyName() : EMPTY_STRING;

		if (StringUtils.isNotBlank(companyName)) {
			addLikePredicate(brokerQuery, COMPANY_NAME, companyName);
			
		}
		
		String phoneNumber = (broker != null) ? broker.getContactNumber() : EMPTY_STRING;
		
		if (StringUtils.isNotBlank(phoneNumber)) {
			addLikeWithTwoConjunctionOr(brokerQuery, CONTACT_NUMBER, phoneNumber, BUSINESS_CONTACT_PHONE_NUMBER, broker.getBusinessContactPhoneNumber(), ALTERNATE_PHONE_NUMBER, broker.getAlternatePhoneNumber());
		}
		
		
		String npn = (broker != null) ? broker.getNpn() : EMPTY_STRING;
		String licenseNumber = (broker != null) ? broker.getLicenseNumber() : EMPTY_STRING;
		
		if(StringUtils.isNotBlank(npn) && StringUtils.isNotBlank(licenseNumber)) {
			
			addLikeWithConjunctionOr(brokerQuery, LICENSE_NUMBER, licenseNumber, NPN, npn);
			
		}else if(StringUtils.isNotBlank(licenseNumber)){
			
			addLikePredicate(brokerQuery, LICENSE_NUMBER, licenseNumber);
			
		}
		
		String language = (broker != null) ? broker.getLanguagesSpoken() : EMPTY_STRING;
		if (StringUtils.isNotBlank(language)) {
			addLikePredicate(brokerQuery, LANGUAGES_SPOKEN, language);
		}

		createWhereCriteriaForQueryForCertificationStatus(broker, brokerQuery);

		int zipcode = (broker != null && broker.getLocation() != null) ? Integer
				.parseInt(broker.getLocation().getZip()) : 0;
				if (zipcode != 0) {
					brokerQuery.applyWhere(LOCATION_ZIP, zipcode, DataType.NUMERIC, ComparisonType.EQ);
				}
	}

	/**
	 * Method to create Where criteria for certification status in query.
	 *
	 * @param broker The current Broker instance.
	 * @param brokerQuery The current query's QueryBuilder instance.
	 */
	private void createWhereCriteriaForQueryForCertificationStatus(Broker broker, QueryBuilder<Broker> brokerQuery) {
		String status = ((broker != null) ? (broker.getCertificationStatus() != null ? broker.getCertificationStatus()
				.toString() : EMPTY_STRING) : EMPTY_STRING);
		if (StringUtils.isNotBlank(status)) {
			brokerQuery.applyWhere(CERTIFICATION_STATUS, status.toUpperCase(), DataType.STRING,
					ComparisonType.STARTWITH);
		} else {
			brokerQuery.applyWhere(CERTIFICATION_STATUS, status, DataType.STRING, ComparisonType.ISNOTNULL);
		}
	}

	/**
	 * Method to create Where criteria for name (firstname, lastname) in query.
	 *
	 * @param broker The current Broker instance.
	 * @param brokerQuery The current query's QueryBuilder instance.
	 */
	private void createWhereCriteriaForQueryForName(Broker broker,
			QueryBuilder<Broker> brokerQuery) {
		String firstName = (broker != null && broker.getUser() != null) ? broker.getUser().getFirstName() : EMPTY_STRING;

		if (StringUtils.isNotBlank(firstName)) {
			addLikeWithConjunctionOr(brokerQuery, USER_FIRST_NAME, firstName,FIRST_NAME, firstName);
		}

		String lastName = (broker != null && broker.getUser() != null) ? broker.getUser().getLastName() : EMPTY_STRING;
		if (StringUtils.isNotBlank(lastName)) {
			addLikeWithConjunctionOr(brokerQuery, USER_LAST_NAME, lastName, LAST_NAME, lastName);
		}
	}
	
	
	private void addLikeWithConjunctionOr(QueryBuilder<?> queryBuilder,String columnName,String value,String orColumnName,String orValue){
		Predicate predicate =  getLikePredicate(queryBuilder, columnName, value);
		Predicate orPredicate =  getLikePredicate(queryBuilder, orColumnName, orValue);
		Predicate conjunctionPredicate = queryBuilder.getBuilder().or(predicate,orPredicate);
		queryBuilder.updateCriteriaPredicate(conjunctionPredicate);
	}
	
	private void addLikeWithTwoConjunctionOr(QueryBuilder<?> queryBuilder,String columnName1,String value1,String columnName2,String value2,String columnName3,String value3){
		Predicate predicate1 =  getLikePredicateWithReplaceFunctionApplied(queryBuilder, columnName1, value1, DASH, EMPTY_STRING);
		Predicate predicate2 =  getLikePredicateWithReplaceFunctionApplied(queryBuilder, columnName2, value2, DASH, EMPTY_STRING);
		Predicate conjunctionPredicate = queryBuilder.getBuilder().or(predicate1,predicate2);
		Predicate predicate3 =  getLikePredicateWithReplaceFunctionApplied(queryBuilder, columnName3, value3, DASH, EMPTY_STRING);
		Predicate finalConjunctionPredicate = queryBuilder.getBuilder().or(conjunctionPredicate,predicate3);
		queryBuilder.updateCriteriaPredicate(finalConjunctionPredicate);
	}
	
	private void addLikePredicate(QueryBuilder<?> queryBuilder,String columnName,String value){
		Predicate predicate =  getLikePredicate(queryBuilder, columnName, value);
		queryBuilder.updateCriteriaPredicate(predicate);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Predicate getLikePredicate(QueryBuilder<?> queryBuilder,String columnName,String value){
		value = value.replace(LIKE_ESCAPE_CHAR,LIKE_ESCAPE_CHAR+LIKE_ESCAPE_CHAR).replace("_", ESCAPED_CHAR_UNDERSCORE).replace("%", ESCAPED_CHAR_PERCENTAGE);
		Expression colName = queryBuilder.getPath(columnName);
		Predicate likePredicate = queryBuilder.getBuilder().like(queryBuilder.getBuilder().upper(colName), PERCENTAGE+value.toUpperCase()+PERCENTAGE,'\\');
		return likePredicate;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Predicate getLikePredicateWithReplaceFunctionApplied(QueryBuilder<?> queryBuilder,String columnName,String value,String replaced,String replacement) {
		value = value.replace(LIKE_ESCAPE_CHAR,LIKE_ESCAPE_CHAR+LIKE_ESCAPE_CHAR).replace("_", ESCAPED_CHAR_UNDERSCORE).replace("%", ESCAPED_CHAR_PERCENTAGE);
		Expression colName = queryBuilder.getPath(columnName);
		CriteriaBuilder builder = queryBuilder.getBuilder();
		Expression replacedColumn = builder.function(FUNCTION_REPLACE, String.class, colName,builder.literal(replaced),builder.literal(replacement));
		Predicate likePredicate = builder.like(replacedColumn, PERCENTAGE+value.toUpperCase()+PERCENTAGE,'\\');
		return likePredicate;
	}

	/**
	 * @see BrokerService#searchBroker(Broker, Integer, Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> searchBroker(String anonymousFlag, Broker broker, Integer startRecord, Integer pageSize) throws GIRuntimeException {
		LOGGER.info("searchBroker: START");

		int totalCount = 0;
		List<Object[]> brokerObjectArrayList = null;
		List<Broker> brokerList = null;
		Query query = null;
		ZipCode zipCode = null;
		Map<String, Object> brokerListAndRecordCount = new HashMap<String, Object>();
		EntityManager em = null;
		
		try {
			if (broker.getLocation() != null && broker.getLocation().getZip()!=null && !"".equals(broker.getLocation().getZip())) {
				if(!StringUtils.isNumeric(broker.getLocation().getZip()) || broker.getLocation().getZip().length()!=5){
					throw new GIRuntimeException("Invalid Zip Code");
				}
			}

			if (broker.getDistance() != null && !"".equals(broker.getDistance())) {
				if(!StringUtils.isNumeric(broker.getDistance())){
					throw new GIRuntimeException("Invalid Distance");
				}
			}
			
			int zip = getIntegerFromString(broker.getLocation() != null ? broker.getLocation().getZip() : ZERO_STRING);

			if (zip > 0) {
				zipCode = zipCodeService.findByZipCode(zip + EMPTY_STRING);
				if (zipCode == null || (zipCode.getLat() == ZERO && zipCode.getLat() == ZERO)){
 	 	 		   brokerListAndRecordCount.put(RECORD_COUNT, ZERO);
			       brokerListAndRecordCount.put(BROKER_LIST, brokerList);
			       LOGGER.info("Invalid zip code : " + zip);
			       return brokerListAndRecordCount;
			    }
			}
			
			em = emf.createEntityManager();
			query = createBrokerSearchQuery(broker, false, zipCode, zip,em);
			query.setFirstResult(startRecord);
			query.setMaxResults(pageSize);
			brokerObjectArrayList = (List<Object[]>) query.getResultList();
			brokerList = getBrokerListFromObjectArrayList(anonymousFlag, brokerObjectArrayList);
			
			if ((startRecord.intValue() == 0 || startRecord.intValue() == 1) && (brokerList != null && brokerList.size() < pageSize) ) {
				totalCount = brokerList.size();
			} else {
				query = createBrokerSearchQuery(broker, true, zipCode, zip,em);				
				List totalCountList = query.getResultList();
				if (totalCountList != null && !totalCountList.isEmpty()) {
					totalCount = Integer.parseInt(totalCountList.get(0) + EMPTY_STRING);
				}
			}			
			brokerListAndRecordCount.put(RECORD_COUNT, totalCount);
			brokerListAndRecordCount.put(BROKER_LIST, brokerList);
		} catch (GIRuntimeException giexception) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_SEARCHING_BROKERS, giexception);
			 throw giexception;
		} finally {
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
			LOGGER.info("searchBroker: END");
		}

		return brokerListAndRecordCount;
	}

	/**
	 * Method to create Broker search query.
	 *
	 * @param broker The current Broker instance.
	 * @param isTotalCountQuery The total count query indicator.
	 * @param zipCode The current ZipCode instance.
	 * @param zip The current zip.
	 * @return query The updated Query instance.
	 */
	private Query createBrokerSearchQuery(Broker broker, boolean isTotalCountQuery, ZipCode zipCode, int zip,EntityManager em) {
		String queryStr;
		Query query = null;
		
		try {
			queryStr = getBrokerSearchQuery(broker, isTotalCountQuery, zip, zipCode);
			query = em.createNativeQuery(queryStr);

			query = setParametersInBrokerSearchQuery(broker, query, zip, zipCode);
		}
		catch(Exception ex) {
			LOGGER.warn("Exception occurred.", ex);
		}
		
		return query;
	}

	/**
	 * Method to get Broker object list from list of object array.
	 *
	 * @param brokerObjectArrayList list of object array
	 * @return list of broker objects
	 */
	private List<Broker> getBrokerListFromObjectArrayList(String anonymousFlag,List<Object[]> brokerObjectArrayList ) {

		LOGGER.info("getBrokerListFromObjectArrayList: START");

		List<Broker> brokerList =  new ArrayList<Broker>();

		Broker brokerObject = null;

		AccountUser userObject = null;

		Location locationObject = null;
		
		AccountUser user = null;
		int activeModuleId = 0;
		if(null != anonymousFlag && !anonymousFlag.trim().isEmpty() && anonymousFlag.equalsIgnoreCase(N)) 
		{
			try { 
				user = userService.getLoggedInUser();
			} catch (InvalidUserException e) {
				LOGGER.error("Invalid user exception ", e);
			}
		}
		
		if (user != null) {
			ModuleUser userDefModule = userService.getUserSelfSignedModule(
					ModuleUserService.BROKER_MODULE, user.getId());

			if (userDefModule != null) {
				activeModuleId = userDefModule.getModuleId();
				
			}
		}
		
		

		if ( brokerObjectArrayList != null && !brokerObjectArrayList.isEmpty() ) {

			for ( Object[] brokerObjArr : brokerObjectArrayList ) {

				brokerObject = new Broker();

				populateBrokerObject(brokerObject, brokerObjArr);

				userObject = populateUserObject(brokerObjArr);

				brokerObject.setUser(userObject);

				locationObject = populateLocationObject(brokerObjArr);

				brokerObject.setLocation(locationObject);
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					brokerList.add(brokerObject);		
				}else{
					// Checking if the agent is not self signed employer.
					// If the agent is self-signed employer , do not show him on the list of find agents for that self signed employer.
	
					if(brokerObject.getUser() != null)
					{
						addBrokerToList(brokerList, brokerObject, activeModuleId);					
					}				
				}
			}
		}

		LOGGER.info("getBrokerListFromObjectArrayList: END");

		return brokerList;
	}

	private void addBrokerToList(List<Broker> brokerList, Broker brokerObject,
			int activeModuleId) {
		boolean selfSignedAgent = false;
		//Fetch the list of active module Ids(
		List<ModuleUser>  moduleusers = userService.getUserModules(brokerObject.getUser().getId());					
		// Traverse through this list of active moduleIds
		if (moduleusers != null && !moduleusers.isEmpty()) {
			
			for(ModuleUser moduleUser : moduleusers) {
				
				//If for that user self signed agent is 'Y'
				// Do not put that user/agent into the broker list.
				if( moduleUser.getModuleName()!=null && moduleUser.getModuleName().equalsIgnoreCase(ModuleUserService.BROKER_MODULE) 
						&& moduleUser.getIsSelfSigned().equals(Y) && moduleUser.getModuleId() == activeModuleId) {
					
					selfSignedAgent = true;
					break;
				}					
			}
			if(selfSignedAgent == false)
			{
				brokerList.add(brokerObject);
			}							
		}
	}

	/**
	 * The method to populate User object.
	 *
	 * @param brokerObjArr The current broker object array.
	 * @return userObject The updated user object.
	 */
	private AccountUser populateUserObject(Object[] brokerObjArr) {
		LOGGER.info("populateUserObject: START");

		AccountUser userObject;
		userObject = new AccountUser();

		userObject.setId(brokerObjArr[COL_USER_ID] != null ? getIntegerFromString(brokerObjArr[COL_USER_ID].toString()) : -1);
		userObject.setFirstName(brokerObjArr[COL_FIRST_NAME] != null && isNotEmptyString(brokerObjArr[COL_FIRST_NAME].toString()) ? brokerObjArr[COL_FIRST_NAME].toString() : EMPTY_STRING);
		userObject.setLastName(brokerObjArr[COL_LAST_NAME] != null && isNotEmptyString(brokerObjArr[COL_LAST_NAME].toString()) ? brokerObjArr[COL_LAST_NAME].toString() : EMPTY_STRING);

		LOGGER.info("populateUserObject: END");
		return userObject;
	}

	/**
	 * Method to populate Location object.
	 *
	 * @param brokerObjArr The current object array.
	 * @return locationObject The updated location object.
	 */
	private Location populateLocationObject(Object[] brokerObjArr) {

		LOGGER.info("populateLocationObject: START");

		Location locationObject;
		locationObject = new Location();

		populateLocationObjectWithAddress(brokerObjArr, locationObject);

		locationObject.setCity(brokerObjArr[COL_CITY] != null && isNotEmptyString(brokerObjArr[COL_CITY].toString()) ? brokerObjArr[COL_CITY].toString() : EMPTY_STRING);
		locationObject.setState(brokerObjArr[COL_STATE] != null && isNotEmptyString(brokerObjArr[COL_STATE].toString()) ? brokerObjArr[COL_STATE].toString() : EMPTY_STRING);
		locationObject.setZip(brokerObjArr[COL_ZIP] != null && isNotEmptyString(brokerObjArr[COL_ZIP].toString()) ? brokerObjArr[COL_ZIP].toString() : EMPTY_STRING);

		LOGGER.info("populateLocationObject: END");

		return locationObject;
	}

	/**
	 * Method to populate Location Object with address(address1, address2) information.
	 *
	 * @param brokerObjArr The Broker Object array.
	 * @param locationObject The Location instance.
	 */
	private void populateLocationObjectWithAddress(Object[] brokerObjArr, Location locationObject) {
		LOGGER.info("populateLocationObjectWithAddress: START");

		locationObject.setAddress1(brokerObjArr[COL_ADDRESS1] != null && isNotEmptyString(brokerObjArr[COL_ADDRESS1].toString()) ? brokerObjArr[COL_ADDRESS1].toString() : EMPTY_STRING);
		locationObject.setAddress2(brokerObjArr[COL_ADDRESS2] != null && isNotEmptyString(brokerObjArr[COL_ADDRESS2].toString()) ? brokerObjArr[COL_ADDRESS2].toString() : EMPTY_STRING);

		LOGGER.info("populateLocationObjectWithAddress: END");
	}

	/**
	 * Method to populate Broker object.
	 *
	 * @param brokerObject The current Broker instance.
	 * @param brokerObjArr The Broker Object array.
	 */
	private void populateBrokerObject(Broker brokerObject, Object[] brokerObjArr) {
		LOGGER.info("populateBrokerObject: START");

		brokerObject.setId(brokerObjArr[COL_BROKER_ID] != null ? getIntegerFromString(brokerObjArr[COL_BROKER_ID].toString()) : -1);
		brokerObject.setContactNumber(brokerObjArr[COL_CONTACT_NUMBER] != null && isNotEmptyString(brokerObjArr[COL_CONTACT_NUMBER].toString()) ? brokerObjArr[COL_CONTACT_NUMBER].toString() : EMPTY_STRING);
		brokerObject.setProductExpertise(brokerObjArr[COL_PRODUCT_EXPERTISE] != null && isNotEmptyString(brokerObjArr[COL_PRODUCT_EXPERTISE].toString()) ? brokerObjArr[COL_PRODUCT_EXPERTISE].toString() : EMPTY_STRING);

		populateBrokerObjectWithLanguagesDistanceCompanyNamePublicEmail(brokerObject, brokerObjArr);

		LOGGER.info("populateBrokerObject: END");
	}

	/**
	 * Method to populate Broker Object with Languages, Distance, CompanyName, YourPublicEmail fields.
	 *
	 * @param brokerObject The current Broker instance.
	 * @param brokerObjArr The Broker Object array.
	 */
	private void populateBrokerObjectWithLanguagesDistanceCompanyNamePublicEmail(Broker brokerObject, Object[] brokerObjArr) {

		LOGGER.info("populateBrokerObjectWithLanguagesDistanceCompanyNamePublicEmail: START");

		brokerObject.setLanguagesSpoken(brokerObjArr[COL_LANGUAGES_SPOKEN] != null && isNotEmptyString(brokerObjArr[COL_LANGUAGES_SPOKEN].toString()) ? brokerObjArr[COL_LANGUAGES_SPOKEN].toString() : EMPTY_STRING);
		brokerObject.setDistance(brokerObjArr[COL_DISTANCE] != null && isNotEmptyString(brokerObjArr[COL_DISTANCE].toString()) ? brokerObjArr[COL_DISTANCE].toString() : EMPTY_STRING);
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			brokerObject.setAgencyName(brokerObjArr[19] != null && isNotEmptyString(brokerObjArr[19].toString()) ? brokerObjArr[19].toString() : EMPTY_STRING);
		}
		brokerObject.setCompanyName(brokerObjArr[COL_COMPANY_NAME] != null && isNotEmptyString(brokerObjArr[COL_COMPANY_NAME].toString()) ? brokerObjArr[COL_COMPANY_NAME].toString() : EMPTY_STRING);
		
		brokerObject.setYourPublicEmail(brokerObjArr[COL_EMAIL] != null && isNotEmptyString(brokerObjArr[COL_EMAIL].toString()) ? brokerObjArr[COL_EMAIL].toString() : EMPTY_STRING);

		LOGGER.info("populateBrokerObjectWithLanguagesDistanceCompanyNamePublicEmail: END");
	}

	/**
	 * Method to get broker search query
	 *
	 * @param broker The Broker instance.
	 * @param isTotalCountQuery The total count query flag.
	 * @param zip The zip code.
	 * @param zipCode The ZipCode instance.
	 * @return The stringified parameterized query string.
	 */
	private String getBrokerSearchQuery(Broker broker, boolean isTotalCountQuery, int zip, ZipCode zipCode) {

		LOGGER.info("getBrokerSearchQuery: START");

		String tempDistance = null;
		StringBuilder rawQuery = new StringBuilder();

		if (broker != null) {
			tempDistance = broker.getDistance();

			double longitude = ZERO_DISTANCE;
			double lattitude = ZERO_DISTANCE;

			if (zipCode !=  null) {
				longitude = zipCode.getLon();
				lattitude = zipCode.getLat();
			}

			if(isTotalCountQuery) {
				createTotalCountRawQuery(zipCode, rawQuery, longitude, lattitude);
			}
			else {
				createRefineSearchRawQuery(zipCode, rawQuery, longitude, lattitude);
			}


			rawQuery.append(" from brokers b inner join users u on  u.id = b.userid inner join locations l on l.id = b.location_id");
			if(BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)) {
				if(isNotEmptyString(broker.getCompanyName())) {
					rawQuery.append(" inner ");
				}else {
					rawQuery.append(" left outer ");
				}
				rawQuery.append("join agency a on a.id = b.agency_id");
			}
			
			rawQuery.append(" where ");
			rawQuery.append(" clients_served is not null ");
			appendRawQueryWithCertificationStatus(broker, rawQuery);
			
			rawQuery.append(" and (b.status is null or b.status='Active') ");

			appendRawQueryWithClienstServed(broker, rawQuery);
			
			appendRawQueryWithName(broker, rawQuery);

			if(BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)) {
				appendRawQueryWithAgencyName(broker, rawQuery);
			}else {
				appendRawQueryWithCompanyName(broker, rawQuery);
			}
			

			appendRawQueryForLanguagesSpoken(broker, rawQuery);
			
			int distance = getIntegerFromString(tempDistance);
			appendRawQueryWithLatLongRange(broker, rawQuery, lattitude, longitude, distance, zip);

			if(!isTotalCountQuery) {
				rawQuery.append(" ) SUB1 ");
			}
			else if(zipCode !=  null && longitude != 0 && lattitude != 0) {
				rawQuery.append(" ) SUB1 ");
			}	
			appendRawQueryWithDistance(zip, rawQuery, distance);
			
			createOrderyByCriteriaForRawQuery(broker, isTotalCountQuery, zip, rawQuery, distance);
		}


		LOGGER.info("getBrokerSearchQuery: END");

		return rawQuery.toString();
	}

	private void appendRawQueryWithLatLongRange(Broker broker,
			StringBuilder rawQuery, double lattitude, double longitude, double distance, int zip) {
		if(longitude != 0 && lattitude != 0) {
			double dis = (distance * STATE_WIDTH_FACTOR)/MILES_FACTOR;
			rawQuery.append(" AND (l.lattitude between ("+lattitude+ " - " + dis + ") and ("+lattitude+" + " + dis + ") ");
			rawQuery.append(" AND l.longitude between ( " +longitude +" - " + dis + ") and (" +longitude +" + " + dis + ") or l.zip = '"+zip+"' ) ");
		}
		
	}

	/**
	 * Method to append distance  condition in where clause of raw query.
	 *
	 * @param zip The current zip code.
	 * @param rawQuery The raw query's StringBuilder instance.
	 * @param distance The current distance.
	 */
	private void appendRawQueryWithDistance(int zip, StringBuilder rawQuery, int distance) {
		LOGGER.info("appendRawQueryWithDistance: START");

		if (distance > 0 && zip > 0 && distance != DISTANCE_FOR_OPTION_ANY) {

			rawQuery.append(" where ((distance > 0 and distance <= :" + DISTANCE + " ) or (zip = :" + ZIP + " ))");
		}

		LOGGER.info("appendRawQueryWithDistance: END");
	}

	/**
	 * Method to append Company name condition in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The raw query's StringBuilder instance.
	 */
	private void appendRawQueryWithCompanyName(Broker broker, StringBuilder rawQuery) {
		LOGGER.info("appendRawQueryWithCompanyName: START");
		
		if (isNotEmptyString(broker.getCompanyName())) {

			rawQuery.append(" and upper(company_name) like upper(:" + COMPANY_NAME + ") ");
		}

		LOGGER.info("appendRawQueryWithCompanyName: END");
	}

	private void appendRawQueryWithAgencyName(Broker broker, StringBuilder rawQuery) {
		LOGGER.info("appendRawQueryWithAgencyName: START");
		
		if (isNotEmptyString(broker.getCompanyName())) {
			
			rawQuery.append(" and upper(a.agency_name) like upper(:" + AGENCY_NAME + ") ");
		}
		
		LOGGER.info("appendRawQueryWithAgencyName: END");
	}

	/**
	 * Method to append clients served where clause in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The raw query's StringBuilder instance.
	 */
	private void appendRawQueryWithClienstServed(Broker broker, StringBuilder rawQuery) {

		LOGGER.info("appendRawQueryWithClienstServed: START");

		if (isNotEmptyString(broker.getClientsServed())) {

			rawQuery.append(" and clients_served like :" + CLIENTS_SERVED + " ");
		}

		LOGGER.info("appendRawQueryWithClienstServed: END");
	}

	/**
	 * The method to append Certification status where clause in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The raw query's StringBuilder instance.
	 */
	private void appendRawQueryWithCertificationStatus(Broker broker, StringBuilder rawQuery) {

		LOGGER.info("appendRawQueryWithCertificationStatus: START");

		if (isNotEmptyString(broker.getCertificationStatus())) {

			rawQuery.append(" and b.certification_status = :" + CERTIFICATION_STATUS + " ");
		}

		LOGGER.info("appendRawQueryWithCertificationStatus: END");
	}

	/**
	 * Method to append name (first name, last name) where clauses in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The raw query's StringBuilder instance.
	 */
	private void appendRawQueryWithName(Broker broker, StringBuilder rawQuery) {

		LOGGER.info("appendRawQueryWithName: START");

		if (broker.getUser() != null && isNotEmptyString(broker.getUser().getFirstName())) {

			rawQuery.append(" and upper(u.first_name) like upper(:" + FIRST_NAME + ") ");
		}

		if (broker.getUser() != null && isNotEmptyString(broker.getUser().getLastName())) {

			rawQuery.append(" and upper(u.last_name) like upper(:" + LAST_NAME + ") ");
		}

		LOGGER.info("appendRawQueryWithName: END");
	}

	/**
	 * Method to append languages spoken where clauses in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The raw query's StringBuilder instance.
	 */
	private void appendRawQueryForLanguagesSpoken(Broker broker, StringBuilder rawQuery) {

		LOGGER.info("appendRawQueryForLanguagesSpoken: START");

		String[] languages;
		if (isNotEmptyString(broker.getLanguagesSpoken())) {

			languages = broker.getLanguagesSpoken().split(",");

			if ( languages != null && languages.length > 0 ) {

				rawQuery.append(" and ( ");

				for ( int i = 0; i < languages.length; i++) {

					if (i > 0) { rawQuery.append(" or "); }

					rawQuery.append(" languages_spoken like :" + LANGUAGE_SPOKEN_PARAM + i);
				}
				rawQuery.append(" ) ");
			}
		}

		LOGGER.info("appendRawQueryForLanguagesSpoken: END");
	}

	/**
	 * Method to create Order By criteria for raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param isTotalCountQuery The total count query indicator.
	 * @param zip The current zip value.
	 * @param rawQuery The raw query.
	 * @param distance The distance value.
	 */
	private void createOrderyByCriteriaForRawQuery(Broker broker,
			boolean isTotalCountQuery, int zip, StringBuilder rawQuery,
			int distance) {
		LOGGER.info("createOrderyByCriteriaForRawQuery: START");

		if(!isTotalCountQuery) {
			rawQuery.append(" ORDER BY ");
			if ((distance > 0 && zip > 0) || (distance == DISTANCE_FOR_OPTION_ANY) ) {
				rawQuery.append(" distance, id ");
			}
			else {
				createOrderyByCriteriaWithNameForRawQuery(broker, rawQuery);
			}
		}
		LOGGER.info("createOrderyByCriteriaForRawQuery: END");
	}

	/**
	 * Method to conditionally update Order By criteria with name in raw query.
	 *
	 * @param broker The current Broker instance.
	 * @param rawQuery The current StringBuilder instance of raw query.
	 */
	private void createOrderyByCriteriaWithNameForRawQuery(Broker broker, StringBuilder rawQuery) {
		LOGGER.info("createOrderyByCriteriaWithNameForRawQuery: START");

		if(null == broker.getUser()) {
			rawQuery.append(" last_name, id ");
		}
		else{
			if( (broker.getUser().getLastName() != null && !broker.getUser().getLastName().equals(EMPTY_STRING))
					&& (broker.getUser().getFirstName() == null || broker.getUser().getFirstName().equals(EMPTY_STRING)) ){
				rawQuery.append(" last_name, first_name ");
			}else if((broker.getUser().getFirstName() != null && !broker.getUser().getFirstName().equals(EMPTY_STRING))
					&& (broker.getUser().getLastName() == null || broker.getUser().getLastName().equals(EMPTY_STRING)) ){
				rawQuery.append(" first_name, last_name ");
			}
			else{
				rawQuery.append(" last_name, id ");
			}
		}

		LOGGER.info("createOrderyByCriteriaWithNameForRawQuery: END");
	}
	
	/**
	 * Method to create refine search query.
	 *
	 * @param zipCode The current ZipCode instance.
	 * @param rawQuery The raw query.
	 * @param longitude The longitude value.
	 * @param lattitude The latitude value.
	 */
	private void createRefineSearchRawQuery(ZipCode zipCode,
			StringBuilder rawQuery, double longitude, double lattitude) {
		LOGGER.info("createRefineSearchRawQuery: START");

		rawQuery.append("select * from ( ");
		rawQuery.append("select ");
		rawQuery.append("b.id, ");
		rawQuery.append("b.contactNumber, ");
		rawQuery.append("b.product_expertise, ");
		rawQuery.append("b.languages_spoken, ");
		rawQuery.append("u.first_name, ");
		rawQuery.append("u.last_name, ");
		rawQuery.append("b.your_public_email, ");

		if (zipCode !=  null && longitude != 0 && lattitude != 0) {
			//rawQuery.append(" CASE WHEN l.zip = :" + ZIP_SELECT + " THEN 0 ");
			//rawQuery.append(" ELSE ");
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				rawQuery.append("TRUNC( CAST( 3959 * acos( TRUNC( CAST( cos( TRUNC(   CAST( (:" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC ) ,4) )  * ");
			}else{
				rawQuery.append("TRUNC(   3959 * acos( TRUNC(   cos( TRUNC(    :" + LATTITUDE_CONST + " / 180.0 * 3.14   ,4) )  * ");
			}
			
			rawQuery.append("cos( TRUNC(    l.lattitude / 180.0 * 3.14   ,4  )   ) * ");
			rawQuery.append("cos( (TRUNC(    l.longitude / 180.0 * 3.14   , 4 )  ) - ");
			
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				rawQuery.append("TRUNC( CAST(   (:" + LONGITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  ,4  ) ) + ");
				rawQuery.append("sin( TRUNC(  CAST(  ( :" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  , 4) ) ");
				
				
				rawQuery.append("* sin( TRUNC(   l.lattitude / 180.0 * 3.14   ,4) )  AS NUMERIC ) , 15) )  AS NUMERIC ) ,4) as distance , ");
			}else{
				rawQuery.append("TRUNC(    (:" + LONGITUDE_CONST + " / 180.0 * 3.14 )  ,4  ) ) + ");
				rawQuery.append("sin( TRUNC(    :" + LATTITUDE_CONST + " / 180.0 * 3.14   , 4) ) ");
				
				
				rawQuery.append("* sin( TRUNC(   l.lattitude / 180.0 * 3.14   ,4) )    , 15) )    ,4) as distance , ");
			}
			
		}
		else {
			rawQuery.append(" -1 as distance , ");
		}
		rawQuery.append("l.address1, ");
		rawQuery.append("l.address2, ");
		rawQuery.append("l.city, ");
		rawQuery.append("l.state, ");
		rawQuery.append("l.zip, ");
		rawQuery.append("b.company_name, ");
		rawQuery.append("b.clients_served, ");
		rawQuery.append("b.certification_status, ");
		rawQuery.append("l.lattitude, ");
		rawQuery.append("l.longitude, ");
		rawQuery.append("b.userid");
		if(BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)) {
			rawQuery.append(",a.agency_name");
		}

		LOGGER.info("createRefineSearchRawQuery: END");
	}

	/**
	 * Method to create total count raw query.
	 *
	 * @param zipCode The current ZipCode instance.
	 * @param rawQuery The raw query.
	 * @param longitude The longitude value.
	 * @param lattitude The latitude value.
	 */
	private void createTotalCountRawQuery(ZipCode zipCode,
			StringBuilder rawQuery, double longitude, double lattitude) {
		LOGGER.info("createTotalCountRawQuery: START");

		rawQuery.append("select count(*) ");

		if (zipCode !=  null && longitude != 0 && lattitude != 0) {
			rawQuery.append(" from ( ");
			rawQuery.append(" select ");
			//rawQuery.append(" CASE WHEN l.zip = :" + ZIP_SELECT + " THEN 0 ");
			//rawQuery.append(" ELSE ");
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				rawQuery.append(" TRUNC( CAST( 3959 * acos( TRUNC(  CAST( cos( TRUNC( CAST(   ( :" + LATTITUDE_CONST + " / 180.0 * 3.14) AS NUMERIC )  ,4) )  * ");
			}else{
				rawQuery.append(" TRUNC(   3959 * acos( TRUNC(   cos( TRUNC( CAST (   ( :" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC) ,4) )  * ");
			}
			
			rawQuery.append("cos( TRUNC(   l.lattitude / 180.0 * 3.14  ,4 )  ) * ");
			rawQuery.append("cos( (TRUNC(   l.longitude / 180.0 * 3.14  ,4 )  ) - ");
			
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				rawQuery.append("TRUNC( CAST (   (:" + LONGITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  ,4 ) ) + ");
				rawQuery.append("sin( TRUNC(  CAST ( ( :" + LATTITUDE_CONST + " / 180.0 * 3.14) AS NUMERIC )  ,4) ) ");
				
				rawQuery.append("* sin( TRUNC(    l.lattitude / 180.0 * 3.14   ,4) ) AS NUMERIC ) , 15) ) AS NUMERIC ),4)  ");	
			}else{
				rawQuery.append("TRUNC(     (:" + LONGITUDE_CONST + " / 180.0 * 3.14 )   ,4 ) ) + ");
				rawQuery.append("sin( TRUNC(    ( :" + LATTITUDE_CONST + " / 180.0 * 3.14)    ,4) ) ");
				
				
				rawQuery.append("* sin( TRUNC(    l.lattitude / 180.0 * 3.14   ,4) )   , 15) )  ,4)  ");
			}
			
			rawQuery.append(" as distance, l.zip ");
		}

		LOGGER.info("createTotalCountRawQuery: END");
	}

	/**
	 * Method to set parameters in broker search query
	 *
	 * @param broker The Broker instance.
	 * @param query The Query instance.
	 * @param zip The zip code.
	 * @param zipCode The ZipCode instance.
	 * @return query The Query instance with set parameters.
	 */
	private Query setParametersInBrokerSearchQuery(Broker broker, Query query, int zip, ZipCode zipCode) {
		LOGGER.info("setParametersInBrokerSearchQuery: START");

		String distance = null;
		if ( broker != null ) {
			distance = broker.getDistance();

			setParametersInBrokerSearchQueryForLocation(query, zip, zipCode);

			setParametersInBrokerSearchQueryForName(broker, query);

			if (isNotEmptyString(broker.getCompanyName())) {
				if(BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)) {
					query.setParameter(AGENCY_NAME, PERCENTAGE+broker.getCompanyName()+PERCENTAGE);
				}else {
					query.setParameter(COMPANY_NAME, PERCENTAGE+broker.getCompanyName()+PERCENTAGE);
				}
			}

			setParametersInBrokerSearchQueryForLanguagesSpoken(broker, query);

			if (isNotEmptyString(broker.getClientsServed())) {

				query.setParameter(CLIENTS_SERVED, PERCENTAGE+broker.getClientsServed()+PERCENTAGE);
			}

			if (isNotEmptyString(broker.getCertificationStatus())) {

				query.setParameter(CERTIFICATION_STATUS, broker.getCertificationStatus());
			}

			if (getIntegerFromString(distance) > 0 && zip > 0 && getIntegerFromString(distance)!=DISTANCE_FOR_OPTION_ANY) {

				query.setParameter(DISTANCE, getIntegerFromString(distance));
				query.setParameter(ZIP, zip+"");
			}
		}

		LOGGER.info("setParametersInBrokerSearchQuery: END");

		return query;
	}

	/**
	 * Method to set parameters in Broker search query for name.
	 *
	 * @param broker The current Broker instance.
	 * @param query The current Query instance.
	 */
	private void setParametersInBrokerSearchQueryForName(Broker broker, Query query) {
		if (broker.getUser() != null) {
			if(isNotEmptyString(broker.getUser().getFirstName())) {

				query.setParameter(FIRST_NAME, PERCENTAGE+broker.getUser().getFirstName()+PERCENTAGE);
			}

			if (isNotEmptyString(broker.getUser().getLastName())) {

				query.setParameter(LAST_NAME, PERCENTAGE+broker.getUser().getLastName()+PERCENTAGE);
			}
		}
	}

	/**
	 * Method to set parameters in Broker search query for Languages spoken.
	 *
	 * @param broker The current Broker instance.
	 * @param query The current Query instance.
	 */
	private void setParametersInBrokerSearchQueryForLanguagesSpoken(
			Broker broker, Query query) {
		String[] languages;
		if (isNotEmptyString(broker.getLanguagesSpoken())) {

			languages = broker.getLanguagesSpoken().split(",");

			for ( int i = 0; i < languages.length; i++) {

				query.setParameter(LANGUAGE_SPOKEN_PARAM+i, PERCENTAGE+languages[i]+PERCENTAGE);
			}
		}
	}

	/**
	 * Method to set parameters in Broker search query for location data (zip, lattitude, longitude).
	 *
	 * @param query The current Query instance.
	 * @param zip The zipcode.
	 * @param zipCode The current ZipCode instance.
	 */
	private void setParametersInBrokerSearchQueryForLocation(Query query, int zip, ZipCode zipCode) {
		double longitude = ZERO_DISTANCE ;
		double lattitude = ZERO_DISTANCE;

		if (zipCode !=  null) {
			longitude = zipCode.getLon();
			lattitude = zipCode.getLat();

			if (longitude != 0 && lattitude != 0) {
				query.setParameter(LATTITUDE_CONST, lattitude);
				query.setParameter(LONGITUDE_CONST, longitude);
				//query.setParameter(ZIP_SELECT, zip);
			}
		}
	}

	/**
	 * Method to check if string is not empty
	 * @param str String
	 * @return <code>true</code> if not empty string, else <code>false</code>
	 */
	private boolean isNotEmptyString(String str) {

		boolean notEmptyString = false;

		if ( str != null && !str.trim().isEmpty() ) {

			notEmptyString = true;
		}

		return notEmptyString;
	}

	/**
	 * Method to get Integer from String
	 * @param str string
	 * @return integer
	 */
	private int getIntegerFromString(String str) {

		int integer = -1;

		if ( isNotEmptyString(str) ) {

			integer = Integer.parseInt(str);
		}

		return integer;
	}

	/**
	 * @see BrokerService#getBrokerDetail(int)
	 */
	@Override
	@Transactional(readOnly = true)
	public Broker getBrokerDetail(int brokerId) {
	LOGGER.info("getBrokerDetail : START");
	Broker broker = brokerRepository.findById(brokerId);
	if(broker ==null){
		throw new GIRuntimeException(null,"Broker Object is null for ID : "+brokerId, Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	}
	LOGGER.info("getBrokerDetail : END");
	return broker;
	}

	/**
	 * @see BrokerService#findAllBrokers()
	 */
	@Override
	public List<Broker> findAllBrokers() {
		LOGGER.info("findAllBrokers : START");
		LOGGER.info("findAllBrokers : END");
		return brokerRepository.findAll();
	}

	/**
	 * @see BrokerService#findBrokerByLicenseNumber(String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Broker findBrokerByLicenseNumber(String licenseNumber) {
		LOGGER.info("findBrokerByLicenseNumber : START");
		LOGGER.info("findBrokerByLicenseNumber : END");
		return brokerRepository.findByLicenseNumber(licenseNumber);
	}

	@Override
	public List<Broker> findBrokersByLicenseNumber(String licenseNumber) {
		return brokerRepository.findBrokersByLicenseNumber(licenseNumber);
	}
	
	@Override
	public List<Broker> findCompleteBrokersByLicenseNumber(String licenseNumber) {
		return brokerRepository.findCompleteBrokersByLicenseNumber(licenseNumber);
	}
	
	/**
	 * @see BrokerService#findBrokerByUserId(int)
	 */
	@Override
	@Transactional(readOnly = true)
	public Broker findBrokerByUserId(int userid) {
		LOGGER.info("findBrokerByUserId : START");
		LOGGER.info("findBrokerByUserId : END");
		return brokerRepository.findByUserId(userid);
	}

	/**
	 * @see BrokerService#saveBrokerCertification(Broker)
	 */
	@Override
	@Transactional
	public void saveBrokerCertification(Broker broker, PaymentMethods paymentMethods) {
		LOGGER.info("saveBrokerCertification : START");
		if (certification_status.Incomplete.toString().equalsIgnoreCase(broker.getCertificationStatus())) {
			broker.setCertificationStatus(certification_status.Pending.toString());
		}

		brokerRepository.save(broker);
		if (paymentMethods != null) {
			LOGGER.info("Send Broker Details call starts");
			//IND35 change start
			brokerIndTriggerService.triggerInd35(broker, paymentMethods);
			//brokerMgmtUtils.sendBrokerDetails(broker, paymentMethods);
			//IND35 change end
			LOGGER.info("Send Broker Details call ends");
		}
		LOGGER.info("saveBrokerCertification : END");
	}

	/**
	 * @see BrokerService#loadBrokerStatusHistory(Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadBrokerStatusHistory(Integer brokerId) {
		LOGGER.info("loadBrokerStatusHistory : START");
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(STATUS_COL, NEW_CERTIFICATION_STATUS);
		requiredFieldsMap.put(COMMENT_COL, VIEW_COMMENTS);
		requiredFieldsMap.put(ATTACHMENT_COL_E_O_DECL_PAGE, VIEW_ATTACHMENT);
		requiredFieldsMap.put(ATTACHMENT_COL_CONTRACT, VIEW_ATTACHMENT);
		requiredFieldsMap.put(ATTACHMENT_COL_SUPPORTING, VIEW_ATTACHMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(STATUS_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENT);
		displayCols.add(ATTACHMENT);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> brokerHistoryData = service.findRevisions(brokerRepository, MODEL_NAME,
					requiredFieldsMap, brokerId);
			if(brokerHistoryData!=null){
				data = brokerHistoryRendererImpl.processData(brokerHistoryData, compareCols, displayCols);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Broker Status History", e);
		}
		LOGGER.info("loadBrokerStatusHistory : END");
		return data;
	}

	/**
	 * @see BrokerService#getPaymentMethodDetailsForBroker(int, String)
	 */
	@Override
	@Transactional(readOnly = true)
	public PaymentMethods getPaymentMethodDetailsForBroker(int moduleId, PaymentMethods.ModuleName moduleName) {
		LOGGER.info("getPaymentMethodDetailsForBroker : START");
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
 	 	paymentMethodRequestDTO.setModuleID(moduleId);
 	 	paymentMethodRequestDTO.setModuleName(moduleName);
 	 	paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);
 	 	paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentIsDefault.Y);
 	 	LOGGER.info("Retrieving Payment Detail REST Call Starts");
 	 	String paymentMethodsStr = restClassCommunicator.retrieveBrokerPaymentDetails(paymentMethodRequestDTO);
 	 	LOGGER.info("Retrieving Payment Detail REST Call Ends");
 	 	XStream xstream = GhixUtils.getXStreamStaxObject();
 	 	PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) xstream
 	 	.fromXML(paymentMethodsStr);
 	 	PaymentMethods paymentMethods = null;
 	 	Map<String, Object> paymentMethodMap = paymentMethodResponse.getPaymentMethodMap();
 	 	if(paymentMethodMap != null && paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY) != null){
 	 	List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
 	 	paymentMethods = paymentMethodList != null ? paymentMethodList.get(0) : null;
 	 	}
 	 	
 	 	if(paymentMethods != null && paymentMethods.getPaymentType() == PaymentMethods.PaymentType.ACH){
 	 	String paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentMethods.getId(), String.class);
 	 	PaymentMethodResponse  paymentMethodPCIResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
 	 	return paymentMethodPCIResponse.getPaymentMethods();
 	 	}
		LOGGER.info("getPaymentMethodDetailsForBroker : END");
		return paymentMethods;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.getinsured.hix.broker.service.BrokerService#getAhbxBrokerDelegateResp
	 * (java.util.Map) added by kuldeep to fetch AHBX Delegation response
	 */
	@Override
	public DelegationResponse getAhbxBrokerDelegateResp(DelegationRequest delegationRequest) throws GIException {
		LOGGER.info("getAhbxBrokerDelegateResp : START");

		DelegationResponse delegationResponse = null;

		LOGGER.info("IND54 : Send Broker details call starts");
		delegationResponse = delegationResponseUtils.validateIssuerResponse(delegationRequest);
		LOGGER.info("IND54 : Send Broker details call ends. Response received is: " + delegationResponse);
		LOGGER.info("getAhbxBrokerDelegateResp : END");
		return delegationResponse;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.getinsured.hix.broker.service.BrokerService#updateDelegateInfo(int,
	 * java.lang.String, int, java.lang.String) added by kuldeep to update
	 * delegation info.
	 */
	@Override
	public Broker updateDelegateInfo(int brokerId, String delegationCode, int responseCode, String responseDescription) {

		LOGGER.info("updateDelegateInfo : START");
		LOGGER.debug("Updating delegation code into DB: " + delegationCode);

		Broker broker = null;
		broker = brokerRepository.findOne(brokerId);
		Broker savedBrokerObj = null;

		LOGGER.debug("Corresponding broker Record found in DB");

		if (broker != null) {
			broker.setDelegationCode(delegationCode);
			broker.setResponseCode(responseCode);
			broker.setResponseDescription(responseDescription);
			LOGGER.info("updateDelegateInfo : END");
			savedBrokerObj = brokerRepository.save(broker);
		}
		LOGGER.info("updateDelegateInfo : END");
		return savedBrokerObj;
	}

	/**
	 * @see BrokerService#saveDocument(String, String, String, String, String)
	 */
	@Override
	public int saveDocument(String createdBy, String documentId, String documenttype, String mimeType,
			String originalFile) {
		LOGGER.info("saveDocument : START");
		BrokerDocuments brokerDocuments1 = null;

		try {
			BrokerDocuments brokerDocuments = new BrokerDocuments();
			brokerDocuments.setDocumentName(documentId);
			brokerDocuments.setCreatedBy(createdBy);
			brokerDocuments.setMimeType(mimeType);
			brokerDocuments.setOrgDocumentName(originalFile);
			brokerDocuments.setCreatedDate(new TSDate());
			brokerDocuments.setDocumentType(documenttype);
			brokerDocuments1 = brokerDocumentsRepository.save(brokerDocuments);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Broker Status History", e);
		}
		LOGGER.info("saveDocument : END");
		if(null != brokerDocuments1) {
			return brokerDocuments1.getId();
		}
		
		return -1;
	}

	/**
	 * @see BrokerService#getListofDocuments(Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public BrokerDocuments getListofDocuments(Integer documentId) {
		LOGGER.info("getListofDocuments : START");
		LOGGER.info("getListofDocuments : END");
		return brokerDocumentsRepository.findOne(documentId);

	}

	/** This method generates activation link and send to the employer
	 * to activate their account after completes the registration process.
	 * @param employer			Its is the Employer Object
	 * @throws GIException 		throws GIException
	 */
	@Override
	public String generateEmployerActivationLink(Employer employer, Broker brokerObj) throws GIException{

		String activationLinkResult = SUCCESS;
		Map<String,String> employerActivationDetails = new HashMap<String, String>();
		employerActivationDetails.put("employerName", employer.getName());
		employerActivationDetails.put("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		employerActivationDetails.put("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		employerActivationDetails.put("exchangeURL", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		employerActivationDetails.put("emailType","employerActivationEmail");
		employerActivationDetails.put("expirationDays", DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYER));

		CreatedObject createdObject = new CreatedObject();
		int createdObjectID = employer.getId();
		String createdObjectType = GhixRole.EMPLOYER.toString();
		List<String> phoneList = new ArrayList<String>();
		phoneList.add(employer.getContactNumber());

		createdObject.setObjectId(createdObjectID);
		createdObject.setEmailId(employer.getContactEmail());
		createdObject.setRoleName(createdObjectType);
		createdObject.setPhoneNumbers(phoneList);
		createdObject.setFullName(employer.getName());
		createdObject.setFirstName(employer.getContactFirstName());
		createdObject.setLastName(employer.getContactLastName());
		createdObject.setCustomeFields(employerActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		int creatorObjectID = brokerObj.getId();
		String creatorObjectType = GhixRole.BROKER.toString();
		creatorObject.setObjectId(creatorObjectID);
		creatorObject.setFullName(brokerObj.getUser().getFullName());
		creatorObject.setRoleName(creatorObjectType);

		// 1. Get the Activation Object w.r.t createdObjectID , creatorObjectID,creatorObjectType,createdObjectType and status=notprocessed.
		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectIdAndCreatorObjectId(createdObjectID,createdObjectType,
				creatorObjectID, creatorObjectType, STATUS.NOTPROCESSED );

		// 2. If the activation object is not null, then call resendMail method and update the existing activation object.
		if(accountActivation != null) {
			accountActivation =  accountActivationService.resendActivationMail(createdObject, creatorObject, Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYER)), accountActivation);
		}
		else //3. If the activation object is null, then call initiateActivationForCreatedRecord method to create a new object
		{

			accountActivation =  accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject,
					Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYER)));
		}
		if (accountActivation != null) {
			LOGGER.info("Account activation link sent to the employer...");
			activationLinkResult = SUCCESS;
		}
		else
		{
			activationLinkResult = FAILURE;

		}
		return activationLinkResult;
	}

	@Override
	@Transactional(readOnly = true)
	public BrokerPhoto getBrokerPhotoById(int brokerId) {

		LOGGER.debug(" Get Broker photo by broker id: " + brokerId);
		EntityManager em = emf.createEntityManager();
		BrokerPhoto brokerPhoto = null;
		Query query = null;
		String queryStr = null;
		byte[] fileContent = null;
		try {
			queryStr = "SELECT ID, PHOTO,BROKER_IMAGE_DOCUMENT_ID FROM BROKERS WHERE ID = :brokerId";
			query = em.createNativeQuery(queryStr);
			query.setParameter("brokerId", brokerId);
			List<Object[]> resultList = query.getResultList();
			brokerPhoto = getBrokerPhoto(resultList);
			if( (brokerPhoto.getImageDocumentId() == 0) && (brokerPhoto.getPhoto() != null))
			{
				brokerPhoto = uploadPhoto(brokerPhoto);
			}

			if(brokerPhoto.getImageDocumentId() != 0)
			{
				BrokerDocuments brokerDocuments = getECMDocumentIDForPhoto(brokerPhoto.getImageDocumentId());
				fileContent = retrievePhotoFromECM(brokerDocuments.getDocumentName());
				if (fileContent !=null && fileContent.length > ZERO) {
					brokerPhoto.setPhoto(fileContent);
				}
				brokerPhoto.setMimeType(brokerDocuments.getMimeType());
			}
		}
		catch (Exception exception) {
			LOGGER.error("Exception while retrieving broker photo: " + exception);
		}
		finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		return brokerPhoto;
	}

	private BrokerDocuments getECMDocumentIDForPhoto(int documentId) {
		return brokerDocumentsRepository.findById(documentId);
	}

	private byte[] retrievePhotoFromECM(String documentId) {
		byte[] photo = null;
		try {
			photo = ecmService.getContentDataById(documentId);
		} catch (ContentManagementServiceException e) {
			LOGGER.error("Unable to show attachment", e);
		}
		return photo;
	}



	@Override
	@Transactional(readOnly = true)
	public BrokerPhoto getBrokerPhotoByUserId(int userId) {
		LOGGER.debug(" Get Broker photo by user id: " + userId);
		EntityManager em = emf.createEntityManager();

		BrokerPhoto brokerPhoto = null;
		Query query = null;
		String queryStr = null;

		try {

			queryStr = "SELECT B.ID, B.PHOTO FROM BROKERS B INNER JOIN USERS U ON B.USERID = U.ID WHERE B.USERID = :userId";

			query = em.createNativeQuery(queryStr);
			query.setParameter("userId", userId);

			List<Object[]> resultList = query.getResultList();

			brokerPhoto = getBrokerPhoto(resultList);

		}
		catch (Exception exception) {

			LOGGER.error("Exception while retrieving broker photo: " + exception);
		}
		finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}

		return brokerPhoto;
	}

	@Override
	public int saveBrokerPhoto(int brokerId, byte[] fileContent) {
		LOGGER.debug(" Save Broker photo for broker id: " + brokerId);
		EntityManager em = emf.createEntityManager();

		Session session = null;
		int updateStatus = 0;
		final int brokerIdFinal = brokerId;
		final byte[] fileContentFinal = fileContent;

		try {

			session = (Session) em.getDelegate();

			session.beginTransaction();

			Work work = new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {
					PreparedStatement statement =null;
					ByteArrayInputStream byteArrayIS = null;

					try
					{
						String queryStr = "UPDATE BROKERS SET PHOTO = ? WHERE ID = ? ";

						statement = connection.prepareStatement(queryStr);

						byteArrayIS = new ByteArrayInputStream(fileContentFinal);
						statement.setBinaryStream(1, byteArrayIS);
						statement.setInt(2,brokerIdFinal);

						int status = statement.executeUpdate();


						LOGGER.debug("Saved broker photo successfully for brokerId: " + brokerIdFinal +  ". UpdateStatus: " + status);
					}
					catch(SQLException sqlEx)
					{
						LOGGER.error("Error while saving broker photo for brokerId: " + brokerIdFinal);
						throw sqlEx;
					}
					finally
					{
						if (statement != null)
						{
							statement.close();
						}
						
						IOUtils.closeQuietly(byteArrayIS);
					}
				}
			};

			session.doWork(work);

			session.getTransaction().commit();
			updateStatus = 1;
		}
		catch (Exception exception) {
			if (session != null && session.getTransaction() != null)
			{
				session.getTransaction().rollback();
			}
			LOGGER.error("Exception while saving broker photo: " + exception);
		}
		finally {
			try
			{
				//No need to close session explicitly, will be closed once we close entitymanager
				/*if (session != null)
	                                {
	                                        session.clear();
	                                        session.close();
	                                }
				 */
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
			catch (Exception ex)
			{
				LOGGER.error(" Error while closing entitymanager ", ex);
			}
		}

		return updateStatus;
	}

	/**
	 * Method to get BrokerPhoto from list of object array
	 * @param brokerPhotoObjectArrayList list of object array
	 * @return BrokerPhoto
	 * @throws SQLException
	 */
	private BrokerPhoto getBrokerPhoto(List<Object[]> brokerPhotoObjectArrayList) throws SQLException {

		LOGGER.info("getBrokerPhoto: START");
		BrokerPhoto brokerPhoto = null;
		if ( brokerPhotoObjectArrayList != null && !brokerPhotoObjectArrayList.isEmpty()) {

			Object[] brokerObjArr = brokerPhotoObjectArrayList.get(0);
			brokerPhoto = new BrokerPhoto();
			brokerPhoto.setId( brokerObjArr[COL_BROKER_ID] != null ? getIntegerFromString(brokerObjArr[COL_BROKER_ID].toString()) : -1 );
			if (brokerObjArr[COL_PHOTO] != null) {

				Blob photoblob = (Blob)brokerObjArr[COL_PHOTO];
				brokerPhoto.setPhoto(photoblob.getBytes(1, (int)photoblob.length()));
			}
			brokerPhoto.setImageDocumentId(brokerObjArr[COL_BROKER_IMAGE_DOCUMENT_ID] != null ? getIntegerFromString(brokerObjArr[COL_BROKER_IMAGE_DOCUMENT_ID].toString()) : 0);
		}
		LOGGER.info("getBrokerPhoto: END");
		return brokerPhoto;
	}

	@Override
	public int uploadPhoto(int brokerId, MultipartFile fileInput)
			throws GIException {
		String modifiedFileName = null;
		int docID = 0;
		String originalFileName;
		String newFileName;
		String fileNameToUpload;
		String extension;
		
		try {
			extension = FilenameUtils.getExtension(fileInput.getOriginalFilename());
			originalFileName = FilenameUtils.getBaseName(fileInput.getOriginalFilename());
			newFileName = EntityUtils.removeDots(originalFileName);
			fileNameToUpload = newFileName+"."+extension;
			
			// If the photo blob is already null, then directly upload the photo on ECM
			modifiedFileName = newFileName + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(fileInput.getOriginalFilename());	
			// Upload the photo On ECM
			docID = uploadPhotoOnECM(BROKER_IMAGES_FOLDER, brokerId, null, modifiedFileName, userService.getLoggedInUser().getUserName(),
					fileInput.getBytes(), fileNameToUpload, getContentTypeFromBytes(fileInput));

			// Save the document ID in Brokers table
			saveBrokerPhoto(brokerId, docID);
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid user exception ", e);
		} catch (Exception e) {
			LOGGER.error("Exception while uploading the photo", e);
		}
		return docID;
	}

	@Override
	public BrokerPhoto uploadPhoto(BrokerPhoto brokerPhoto) throws GIException {

		String modifiedFileName = null;
		int docID = 0;
		int brokerId = brokerPhoto.getId();
		modifiedFileName = BROKER_IMAGE + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + JPG;

		// First Upload the photo on ECM
		// Get the document ID.
		try {
			docID = uploadPhotoOnECM(BROKER_IMAGES_FOLDER, brokerId, null, modifiedFileName, "SYSTEM", brokerPhoto.getPhoto(), modifiedFileName, JPG);
			brokerPhoto.setImageDocumentId(docID);

			// Set the Photo blob object to null in Broker table and set the document ID
			saveBrokerPhoto(brokerId, docID);
		} catch (Exception e) {
			LOGGER.error("Exception while uploading the photo", e);
		}
		return brokerPhoto;
	}

	private int saveBrokerPhoto(int brokerId,Integer imageDoumentID) throws GIException  {
		LOGGER.debug(" Save Broker photo for broker id: " + brokerId);
		EntityManager em = null;
		int updateStatus = 0;
		final int brokerIdFinal = brokerId;
		final Integer docID = imageDoumentID;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Query query = em.createNativeQuery("UPDATE BROKERS SET PHOTO = NULL,broker_image_document_id=? WHERE ID = ?");
			query.setParameter(1,docID);
			query.setParameter(2,brokerIdFinal);
			updateStatus = query.executeUpdate();
			em.getTransaction().commit();
			LOGGER.debug("Saved broker photo successfully for brokerId: " + brokerIdFinal +  ". UpdateStatus: " + updateStatus);
		}
		catch (Exception exception) {
			LOGGER.error("Exception while updating Broker table with ECM Document ID : "+ exception);
			throw new GIException("Exception while updating Broker table with ECM Document ID::" + exception.getLocalizedMessage(), exception);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		LOGGER.debug(" Update Broker table with ECM Document ID Ends. Status : " + updateStatus);
		return updateStatus;
	}

	private int uploadPhotoOnECM(String folderName, int brokerId, String documenttype, String modifiedFileName, String createdBy,
			byte[] fileInBytes, String fileNameToUpload, String mimeType) throws ContentManagementServiceException {

		int docID;
		String documentId;
		documentId = ecmService.createContent(GhixConstants.BROKER_DOC_PATH + SLASH + brokerId + SLASH + folderName, modifiedFileName, fileInBytes
				, ECMConstants.Broker.DOC_CATEGORY, ECMConstants.Broker.PHOTO, null);
		docID = saveDocument(createdBy, documentId, documenttype, mimeType, fileNameToUpload);
		return docID;
	}
	
	private String getContentTypeFromBytes(MultipartFile fileInput) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        java.io.InputStream is = null;
        ByteArrayInputStream bais = null;
        
		try {
			is = fileInput.getInputStream();
	        byte[] b = new byte[100];
	        int read = is.read(b, 0, b.length);
	        baos.write(b, 0, read);
	        baos.close();
	        bais = new ByteArrayInputStream(baos.toByteArray());
			String contentType = URLConnection.guessContentTypeFromStream(bais);
			
			if(contentType == null) {
				return fileInput.getContentType();
			}
			else {
				return contentType;
			}
		} 
		catch (Exception e) { 
			LOGGER.debug("Error finding mime type from content");
		}
		finally {
			IOUtils.closeQuietly(baos);
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(bais);
		}
		
		return "application/octet-stream";
	}

	/**
	 * @see BrokerService#findBroker(Broker, Integer, Integer, String, SortOrder)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findBroker(Broker broker, Integer startRecord, Integer pageSize, String sortBy, SortOrder sortOrder)  
	{
		LOGGER.info("findBroker : START");
		Map<String, Object> brokerListAndRecordCount = null;

		try {
			QueryBuilder<Broker> brokerQuery = delegateFactory.getObject();

			brokerQuery.buildSelectQuery(Broker.class, Arrays.asList("user", "location"));

			brokerQuery.applySelectColumns(Arrays.asList("id", USER_FIRST_NAME, USER_LAST_NAME, "firstName","lastName", APPLICATION_DATE, LICENSE_NUMBER, "companyName", "certificationDate", "deCertificationDate", CERTIFICATION_STATUS, "user.id","contactNumber"));

			createWhereCriteriaForQuery(broker, brokerQuery);


				AccountUser user = userService.getLoggedInUser();
				if (userService.hasUserRole(user, RoleService.EMPLOYER_ROLE) && !(userService.hasUserRole(user, RoleService.OPERATIONS_ROLE) || userService.hasUserRole(user, RoleService.ADMIN_ROLE))) {
					//AND condition added to this if to resolve issue HIX-29152
					String strClinetsServed = EMPLOYER;
					brokerQuery.applyWhere(CLIENTS_SERVED, strClinetsServed.toUpperCase(), DataType.STRING,
							ComparisonType.LIKE);
				}
				
				if (StringUtils.isNotEmpty(sortBy)) {
					if(BrokerConstants.CONTACT_NAME.equals(sortBy)){
						List<String> columnNames = new ArrayList<String>();
						columnNames.add(USER_FIRST_NAME);
						columnNames.add(USER_LAST_NAME);
						brokerQuery.applySortList(columnNames, sortOrder);
					} else {
						brokerQuery.applySort(sortBy, sortOrder);
					}
				}
	
				brokerListAndRecordCount = new HashMap<String, Object>();

				brokerQuery.setFetchDistinct(true);

				List<Map<String, Object>> brokers = brokerQuery.getData(startRecord, pageSize);

				if(brokers != null){
					brokerListAndRecordCount.put(BROKERLIST, brokers);
				}
				if(brokerQuery.getRecordCount() != null){
					brokerListAndRecordCount.put(RECORD_COUNT, brokerQuery.getRecordCount());
				}
		
			
		}
		catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occured while fetching Agents : ", giexception);
           throw giexception;
		}		
		catch (InvalidUserException ex) {
			LOGGER.error("User not logged in", ex);				
		}
		catch(Exception ex) {
			LOGGER.error("Agent Search", ex);				
		}
		finally {
			LOGGER.info("findBroker : END");
		}

		return brokerListAndRecordCount;
	}
	
	@Override
	@Transactional(readOnly = true)
	public BrokerExportResponseDTO getExportAgentList()  
	{
		LOGGER.info("getExportAgentList : START");
		
		BrokerExportDTO brokerExportDTO;
		BrokerExportResponseDTO brokerExportResponseDTO = new BrokerExportResponseDTO();

		List<BrokerExportDTO> brokerExportList = new ArrayList<>();
		try{
			long currentNanoTime = System.nanoTime();
			LOGGER.info("Loop through Main Query Started at " + currentNanoTime);
			List<Object[]> brokerExportListObj = brokerRepository.getBrokerExportList();
			Iterator<Object[]> iterator = brokerExportListObj.iterator();
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yy");
			
			while (iterator.hasNext()) {
				brokerExportDTO = new BrokerExportDTO();
				Object[] objArray = (Object[]) iterator.next();
	
				if (objArray[0] != null) {
					brokerExportDTO.setId(Integer.valueOf(objArray[0].toString()));
				}
				if (objArray[1] != null) {
					brokerExportDTO.setFirstName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					brokerExportDTO.setLastName(objArray[2].toString());
				}
				if (objArray[3] != null) {
					brokerExportDTO.setCompanyName(objArray[3].toString());
				}
				if (objArray[4] != null) {
					brokerExportDTO.setFederalEIN(objArray[4].toString());
				}
				if (objArray[5] != null) {
					brokerExportDTO.setLicenseNumber(objArray[5].toString());
				}
				if (objArray[6] != null) {
					brokerExportDTO.setLicenseRenewalDate(dateformat.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(objArray[6].toString())));
				}
				if (objArray[7] != null) {
					brokerExportDTO.setCertificationStatus(objArray[7].toString());
				}
				if (objArray[8] != null) {
					brokerExportDTO.setContactNumber(objArray[8].toString());
				}
				if (objArray[9] != null) {
					brokerExportDTO.setAddress1(objArray[9].toString());
				}
				if (objArray[10] != null) {
					brokerExportDTO.setAddress2(objArray[10].toString());
				}
				if (objArray[11] != null) {
					brokerExportDTO.setCity(objArray[11].toString());
				}
				if (objArray[12] != null) {
					brokerExportDTO.setState(objArray[12].toString());
				}
				if (objArray[13] != null) {
					brokerExportDTO.setZip(objArray[13].toString());
				}
				if (objArray[14] != null) {
					brokerExportDTO.setEmail(objArray[14].toString());
				}
				if (objArray[15] != null) {
					brokerExportDTO.setPrefMethodOfCommunication(objArray[15].toString());
				}
				if (objArray[16] != null) {
					brokerExportDTO.setPendingRequestsCount(Integer.parseInt(objArray[16].toString()));
				}
				if (objArray[17] != null) {
					brokerExportDTO.setActiveRequestsCount(Integer.parseInt(objArray[17].toString()));
				}
				if (objArray[18] != null) {
					brokerExportDTO.setInactiveRequestsCount(Integer.parseInt(objArray[18].toString()));
				}
				brokerExportList.add(brokerExportDTO);
			}
			LOGGER.info("Loop through Main Query Ended in " + (System.nanoTime() - currentNanoTime));

		} catch (Exception exception) {
			LOGGER.error("Exception while fetching broker export list: " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		if(brokerExportList!=null && brokerExportList.size()!=0){
			brokerExportResponseDTO.setBrokerExportDTO(brokerExportList);
		}
		return brokerExportResponseDTO;
		
	}
	
	public AgencyBrokerListDTO getBrokersForAgency(SearchBrokerDTO searchBrokerDTO){
		AgencyBrokerDTO agencyBrokerDTO = null;
		List<AgencyBrokerDTO> recordsPerPage = null;
		AgencyBrokerListDTO agencyBrokerListDTO = new AgencyBrokerListDTO();
		StringBuilder query = new StringBuilder();
		List<Object[]> brokers = null;
		EntityManager em = null;
		
		Integer startRecord = 0;
		Integer endRecord = 0;
		startRecord = (searchBrokerDTO.getPageNumber() - 1) * PAGE_SIZE;
		endRecord = startRecord+PAGE_SIZE;
		String sortOder = "asc";
		
		long currentNanoTime = System.nanoTime();
		LOGGER.info("Loop through Main Query Started at " + currentNanoTime);
		List<AgencyBrokerDTO> brokerList = new ArrayList<AgencyBrokerDTO>();
		try{
			em = entityManagerFactory.createEntityManager(); 
			query.append("select b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END) as first_name,");
			query.append("(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END) as last_name, b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS,");
			query.append("count(CASE WHEN d.status = 'Pending' OR d.status = 'Active' THEN brokerId END) as consumers_count ");
			query.append("from brokers b ");
			query.append("left join users u on u.id = b.userid ");
			query.append("left join DESIGNATE_BROKER d on d.BROKERID = b.ID ");
			query.append("where b.agency_id =  :"+AGENCY_ID);
			
			//HIX-102200 - filter
			if(searchBrokerDTO.getFirstName()!=null && StringUtils.isNotBlank(searchBrokerDTO.getFirstName())){
				query.append(" and (CASE WHEN b.userid is not null THEN upper(u.FIRST_NAME) ELSE upper(b.FIRST_NAME) END) like upper(:"+FIRST_NAME+")");
			}
			if(searchBrokerDTO.getLastName()!=null && StringUtils.isNotBlank(searchBrokerDTO.getLastName())){
				query.append(" and (CASE WHEN b.userid is not null THEN upper(u.LAST_NAME) ELSE upper(b.LAST_NAME) END) like upper(:"+LAST_NAME+")");
			}
			if(searchBrokerDTO.getStatus()!=null && StringUtils.isNotBlank(searchBrokerDTO.getStatus())){
				query.append(" and upper(b.STATUS) in upper(:"+AGENT_STATUS+")");
			}
			if(searchBrokerDTO.getCertificationStatus()!=null && StringUtils.isNotBlank(searchBrokerDTO.getCertificationStatus())){
				query.append(" and b.CERTIFICATION_STATUS in (:"+CERTIFICATION_STATUS+")");//
			}
			if(searchBrokerDTO.getLicenseNumber()!=null && StringUtils.isNotBlank(searchBrokerDTO.getLicenseNumber())){
				query.append(" and b.LICENSE_NUMBER like :"+LICENSE_NUMBER);
			}
			query.append(" group by b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END), ");
			query.append("(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END),u.FIRST_NAME, u.LAST_NAME, b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS  ");
			query.append("order by ");
			
			if(StringUtils.isNotBlank(searchBrokerDTO.getSortOrder()) && searchBrokerDTO.getSortOrder().equalsIgnoreCase("desc")){
				sortOder = "desc";
			}
			
			List<String> sortableColumnsList = Broker.getSortableColumnsList();
			
			if(!CollectionUtils.isEmpty(sortableColumnsList) && StringUtils.isNotBlank(searchBrokerDTO.getSortBy())) {
				if(!sortableColumnsList.contains(searchBrokerDTO.getSortBy())) {
					LOGGER.warn("Ignoring {} for sorting, not registered",searchBrokerDTO.getSortBy());
					searchBrokerDTO.setSortBy(null);
				}
			} 
			
			if(StringUtils.isNotBlank(searchBrokerDTO.getSortBy()) && searchBrokerDTO.getSortBy().equalsIgnoreCase("agentLicense")){
				query.append("b.LICENSE_NUMBER ");
			}
			else{
				query.append("upper(first_name) ").append(sortOder).append(", upper(last_name) ");
			}
			
			query.append(sortOder);
			
			
			Query pQuery = em.createNativeQuery(query.toString());
			
			setQueryParams(pQuery,searchBrokerDTO);
			brokers = pQuery.getResultList();
			Iterator<Object[]> iterator = brokers.iterator();
			Map<String,String> paramMap= new HashMap<String,String>();
			String encryptedValue = null;
			while (iterator.hasNext()) {
				agencyBrokerDTO = new AgencyBrokerDTO();
				Object[] objArray = (Object[]) iterator.next();
				
				if (objArray[0] != null) {
					paramMap.put("brokerId", objArray[0].toString() );
			        encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
			        agencyBrokerDTO.setEncryptedId(encryptedValue);
					   
				}
				if (objArray[1] != null) {
					agencyBrokerDTO.setFirstName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					agencyBrokerDTO.setLastName(objArray[2].toString());
				}
				if (objArray[3] != null) {
					agencyBrokerDTO.setStatus(objArray[3].toString());
				}
				if (objArray[4] != null) {
					agencyBrokerDTO.setLicenseNumber(objArray[4].toString());
				}
				if (objArray[5] != null) {
					agencyBrokerDTO.setCertificationStatus(objArray[5].toString());
				}
				if (objArray[6] != null) {
					agencyBrokerDTO.setConsumersCount(Integer.parseInt(objArray[6].toString()));
				}
				brokerList.add(agencyBrokerDTO);
			}
			
			int totalCount = brokerList.size();
			if(totalCount <= PAGE_SIZE){
				recordsPerPage = brokerList;
				searchBrokerDTO.setPageNumber(1);
			} else if(totalCount > endRecord){
				recordsPerPage = brokerList.subList(startRecord, endRecord);
			} else {
				recordsPerPage = brokerList.subList(startRecord, totalCount);
			}
			agencyBrokerListDTO.setPageNumber(String.valueOf(searchBrokerDTO.getPageNumber()));
			agencyBrokerListDTO.setTotalRecords(totalCount);
			agencyBrokerListDTO.setBrokersList(recordsPerPage);
			agencyBrokerListDTO.setCertificationStatus(brokerMgmtUtils.getAgentStatusList());
			LOGGER.info("Loop through Main Query Ended in " + (System.nanoTime() - currentNanoTime));
		} catch (Exception exception) {
			LOGGER.error("Exception while fetching broker export list: " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return agencyBrokerListDTO;
	}
	
	private void setQueryParams(Query pQuery, SearchBrokerDTO searchBrokerDTO) {
		
		pQuery.setParameter(AGENCY_ID, searchBrokerDTO.getId());
		
		if(searchBrokerDTO.getFirstName()!=null && StringUtils.isNotBlank(searchBrokerDTO.getFirstName())){
			pQuery.setParameter(FIRST_NAME, "%"+searchBrokerDTO.getFirstName()+"%");
		}
		if(searchBrokerDTO.getLastName()!=null && StringUtils.isNotBlank(searchBrokerDTO.getLastName())){
			pQuery.setParameter(LAST_NAME, "%"+searchBrokerDTO.getLastName()+"%");
		}
		if(searchBrokerDTO.getStatus()!=null && StringUtils.isNotBlank(searchBrokerDTO.getStatus())){
			pQuery.setParameter(AGENT_STATUS,searchBrokerDTO.getStatus());
		}
		if(searchBrokerDTO.getCertificationStatus()!=null && StringUtils.isNotBlank(searchBrokerDTO.getCertificationStatus())){
			pQuery.setParameter(CERTIFICATION_STATUS,searchBrokerDTO.getCertificationStatus());
		}
		if(searchBrokerDTO.getLicenseNumber()!=null && StringUtils.isNotBlank(searchBrokerDTO.getLicenseNumber())){
			pQuery.setParameter(LICENSE_NUMBER,searchBrokerDTO.getLicenseNumber());
		}
				
	}

	/**
	 * @see BrokerService#saveAgencyManagerUser(Broker)
	 */
	@Override
	@Transactional
	public Broker saveAgencyManagerUser(Broker broker) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveAgencyManagerUser : START-> "+startTime);
		Broker brokerObj = null;
		try {
			// This method will save userId to Agency manager if userId is not linked to agent, no need to trigger IND35 for this action
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				brokerObj = brokerRepository.saveAndFlush(broker);
			}
		LOGGER.info("saveAgencyManagerUser: END");
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveAgencyManagerUser method", exception);
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveAgencyManagerUser : END->"+endTime);
		return brokerObj;
	}
	
	private void createWhereCriteriaForAgencyQuery(SearchAgencyDto searchAgencyDto, QueryBuilder<Agency> agencyQuery) {

		String businessLegalName = (searchAgencyDto != null) ? searchAgencyDto.getBusinessName() : EMPTY_STRING;

		if (StringUtils.isNotBlank(businessLegalName)) {
			agencyQuery.applyWhere(BUSINESS_NAME, businessLegalName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		String status = ((searchAgencyDto != null) ? (searchAgencyDto.getCertificationStatus() != null ? searchAgencyDto.getCertificationStatus() : EMPTY_STRING) : EMPTY_STRING);
		if (StringUtils.isNotBlank(status)) {
			agencyQuery.applyWhere(CERTIFICATION_STATUS, Agency.CertificationStatus.getCertificationStatus(status), DataType.ENUM, ComparisonType.LIKE);
		} else {
			agencyQuery.applyWhere(CERTIFICATION_STATUS, status, DataType.STRING, ComparisonType.ISNOTNULL);
		}

	}
	
	/**
	 * @see BrokerService#findAgency(SearchAgencyDto searchAgencyDto)
	 */
	@Override
	@Transactional(readOnly = true)
	public AgencyDetailsListDTO findAgency(SearchAgencyDto searchAgencyDto)  
	{
		LOGGER.info("findBroker : START");
		SimpleDateFormat dateFormat = new SimpleDateFormat(UI_DATE_FORMAT);
		AgencyDetailsListDTO agencyDetailsListDTO = null;
		List<Agency> recordsList =  null;
		AgencyDetailsDto agencyDetailsDto = null;
		List<AgencyDetailsDto> agencydetails = null;
		Integer startRecord = 0;
		Integer endRecord = 0;
		startRecord = (searchAgencyDto!=null ? (searchAgencyDto.getPageNumber()!=null ? (searchAgencyDto.getPageNumber() - 1) : 1) : 1) * PAGE_SIZE;
		endRecord = startRecord+PAGE_SIZE;
		
		try {
			QueryBuilder<Agency> agencyQuery = delegateFactory.getObject();

			agencyQuery.buildSelectQuery(Agency.class);

			agencyQuery.applySelectColumns(Arrays.asList("id", BUSINESS_LEGAL_NAME, SUBMITTED_ON, CERTIFIED_ON, CERTIFICATION_STATUS));

			createWhereCriteriaForAgencyQuery(searchAgencyDto, agencyQuery);

			agencyDetailsListDTO = new AgencyDetailsListDTO();
			agencyDetailsListDTO.setCertificationStatus(agencyUtils.getAgencyStatusList());

			recordsList = agencyQuery.getRecords(startRecord, PAGE_SIZE);
			agencydetails = new ArrayList<AgencyDetailsDto>();
			Iterator iterator = recordsList.iterator();
			while(iterator.hasNext()){
				agencyDetailsDto = new AgencyDetailsDto();
				Object[] objArray = (Object[]) iterator.next();
				if (objArray[0] != null) {
					agencyDetailsDto.setEncryptedId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(objArray[0])));
				}
				if (objArray[1] != null) {
					agencyDetailsDto.setBusinessLegalName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					agencyDetailsDto.setSubmittedOn(dateFormat.format(objArray[2]));
				}
				if (objArray[3] != null) {
					agencyDetailsDto.setCertifiedOn(dateFormat.format(objArray[3]));
				}
				if (objArray[4] != null) {
					Agency.CertificationStatus status = (Agency.CertificationStatus)objArray[4];
					agencyDetailsDto.setCertificationStatus(status.getValue());
				}
				agencydetails.add(agencyDetailsDto);
			}
			
			if(agencydetails!=null && agencydetails.size()>0){
				agencyDetailsListDTO.setAgencysList(agencydetails);
			}
			if(agencyQuery.getRecordCount() != null){
				agencyDetailsListDTO.setTotalRecords(agencyQuery.getRecordCount());
			}
		
			
		}
		catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occured while fetching Agencies : ", giexception);
           throw giexception;
		}		
		catch(Exception ex) {
			LOGGER.error("Agency Search", ex);				
		}
		finally {
			LOGGER.info("findAgency : END");
		}

		return agencyDetailsListDTO;
	}
	
	@Override
	public List<Map<String, Object>> loadBrokerActivityStatusHistory(Integer brokerId) {

		LOGGER.info("loadBrokerActivityStatusHistory: START");
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(BrokerActivityStatusHistoryRendererImpl.ACTIVITY_STATUS_COL, BrokerActivityStatusHistoryRendererImpl.NEW_ACTIVITY_STATUS);
		requiredFieldsMap.put(BrokerActivityStatusHistoryRendererImpl.COMMENTS_ID, BrokerActivityStatusHistoryRendererImpl.VIEW_COMMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(BrokerActivityStatusHistoryRendererImpl.ACTIVITY_STATUS_COL);
		compareCols.add(BrokerActivityStatusHistoryRendererImpl.COMMENTS_ID);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENT);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> entityHistoryData = service.findRevisions(BrokerActivityStatusHistoryRendererImpl.REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, brokerId);
			data = brokerActivityStatusHistoryRendererImpl.processData(entityHistoryData, compareCols, displayCols);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Broker Activity Status History", e);
		}

		LOGGER.info("loadBrokerActivityStatusHistory: END");

		return data;
	}
	
	@Override
	public Comment saveComments(long targetId, String commentText) {
		List<Comment> comments = null;
		AccountUser accountUser = null;
		String commenterName = "";
		
		try {
			CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(targetId, TargetName.valueOf(TARGET_NAME));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				commentTarget.setTargetId(targetId);
				commentTarget.setTargetName(TargetName.valueOf(TARGET_NAME));
			}
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			}
			else{
				comments = commentTarget.getComments();
			}

			accountUser = userService.getLoggedInUser();
				
			String firstName = accountUser.getFirstName();
			String lastName  = accountUser.getLastName();
			commenterName += (StringUtils.isBlank(firstName)) ? "" :  firstName+" ";
			commenterName += (StringUtils.isBlank(lastName)) ? "" : lastName;
			
			//Checking if user's name is blank
			if(StringUtils.isBlank(commenterName)){
				commenterName = "Anonymous User";
			}
			
			Comment comment = new Comment();
			comment.setComentedBy(accountUser.getId());
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTarget = commentTargetService.saveCommentTarget(commentTarget);
			List<Comment> list = commentTarget.getComments();
			Collections.sort(list);
			
			return list.get(0);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving agency comments.", ex);
			throw new GIRuntimeException("Exception occured while saving agency comments.", ex);
		}
	}
	
	@Override
	public AgencyInformationDto findAgencyByLoggedInUser(AccountUser user){
		AgencyInformationDto agencyInformationDto = null;
		String response = null;
		
		try {
			if(user.getActiveModuleId() != 0){
				Broker broker = brokerRepository.findById(user.getActiveModuleId());
				if(broker.getAgencyId()!=null && StringUtils.isNotBlank(broker.getAgencyId().toString())){
					response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ broker.getAgencyId().toString() , String.class);
				}
			} else {
				response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user)) , String.class);
			}
			
			if(response!=null){
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AgencyInformationDto.class);
				agencyInformationDto = reader.readValue(response);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching Agency.", ex);
			throw new GIRuntimeException("Exception occured while fetching Agency.", ex);
		}
		
		return agencyInformationDto;
	}

	@Override
	public int getAgencyManagerCount(Long agencyId, String agencyRole) {
		return brokerRepository.getAgencyManagerCount(agencyId, RoleService.AGENCY_MANAGER); 
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}
	
}
