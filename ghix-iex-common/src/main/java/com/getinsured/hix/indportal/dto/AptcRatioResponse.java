package com.getinsured.hix.indportal.dto;

import java.math.BigDecimal;
import java.util.List;

public class AptcRatioResponse {
	private List<Member> memberList;
	
	public List<Member> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	public class Member{
		private String id;
		private BigDecimal aptc;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public BigDecimal getAptc() {
			return aptc;
		}
		public void setAptc(BigDecimal aptc) {
			this.aptc = aptc;
		}
		
	}
}
