package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentReconciliationService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrlReconPopulateTask  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconPopulateTask.class);
	private EnrollmentReconciliationService enrollmentReconciliationService;
	private JobService jobService;
	private Job job;
	private JobLauncher jobLauncher;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		
		if(null != enrollmentReconciliationService){
			jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			String batchJobStatus=null;
			
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
										batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			LOGGER.trace(" EnrlReconPopulateTask calling EnrollmentReconciliationService.processReconciliationInFile");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			
			enrollmentReconciliationService.processReconciliationInFile(jobService, stepExecution, jobLauncher, job);
			
		}else{
			
			LOGGER.error(" EnrollmentReconciliationService is null ");
			throw new RuntimeException("EnrollmentReconciliationService is null") ;
		}
		
		return RepeatStatus.FINISHED;
	}

	public EnrollmentReconciliationService getEnrollmentReconciliationService() {
		return enrollmentReconciliationService;
	}

	public void setEnrollmentReconciliationService(
			EnrollmentReconciliationService enrollmentReconciliationService) {
		this.enrollmentReconciliationService = enrollmentReconciliationService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}

}
