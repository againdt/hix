package com.getinsured.hix.batch.referral.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.iex.ssap.repository.PassiveEnrollmentRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

@Service("passiveEnrollmentNotice")
@DependsOn({"ghixPlatformEndPoints", "dynamicPropertiesUtil"})
@Transactional(readOnly = true)
public class PassiveEnrollmentNoticeImpl implements PassiveEnrollmentNotice {

	private static final Logger LOGGER = Logger.getLogger(PassiveEnrollmentNoticeImpl.class);

	private static final String MODULE_NAME = GhixRole.INDIVIDUAL.toString().toLowerCase();

	private static final String PRIMARY_APPLICANT_NAME = "primaryApplicantName";

	private static final String TODAY_DATE = "todaysDate";

	private static final String PASSIVE_ENROLLMENT_NOTICE_DTO = "passiveEnrollmentNoticeDTO";

	private static String ACCOUNT_USER_ACTIVATION_LINK;

	private static final String ADMINISTRATOR = "Administrator";

	private static String EXCHANGE_NAME;

	private static String EXCHANGE_PHONE;

	private static String EXCHANGE_URL;

	private static final String CONSUMER_ACCOUNT_ACTIVATION_EMAIL = "consumerAccountActivationEmail";


	private static final String NO_PHONE_NUMBER_IN_HOUSEHOLD = "no phone number in household";
	private static final String ACCOUNT_EXISTS = "account_exists";

	private static int EXPIRATION_DAYS;

	private static String EXPIRATION_DAYS_STR;

	private static final String PASSIVE_ENROLLMENT_NOTICE = "PassiveEnrollment";

	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private NoticeService noticeService;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IAccountActivationRepository iAccountActivationRepository;
	@Autowired private PassiveEnrollmentRepository passiveEnrollmentRepository;
	@Autowired
	private ILocationRepository iLocationRepository;


	@Autowired private UserService userService;
	@Autowired private RoleService roleService;


	@Autowired
	private AccountActivationService passiveEnrollmentAccountActivation;
	private AccountActivationService accountActivationService;

	@PostConstruct
	public void createStateContext() {
		accountActivationService = passiveEnrollmentAccountActivation;
		ACCOUNT_USER_ACTIVATION_LINK = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/";
		EXCHANGE_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		EXCHANGE_PHONE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
		EXCHANGE_URL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		EXPIRATION_DAYS = StringUtils.isNumeric(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) ?	
				Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) : 30 ;

		EXPIRATION_DAYS_STR = String.valueOf(EXPIRATION_DAYS);
	}
	
	@Override
	public List<PassiveEnrollmentNoticeDTO> getEnrollmentData(Date startDate, Date endDate, List<String> roleNameList, Long ssapAppId) {


		LOGGER.info("Fetching all enrollments processed by ROLES - " + Arrays.asList(roleNameList) + " for DATE RANGE { " + startDate + " - " + endDate + " }");
		
		List<Enrollment> datas = new ArrayList<Enrollment>();
		if(ssapAppId != null)
		{
			datas = passiveEnrollmentRepository.getEnrollmentsByCreatedUser(ssapAppId);
		} else {
			Role tmp = null;
			for (String suppliedRoleName : roleNameList) {
				tmp = roleService.findRoleByName(suppliedRoleName);
				if (tmp == null){
					throw new GIRuntimeException("Invalid roles "+roleNameList+" passed from console");
				}
			}
			List<AccountUser> users = userService.findAccountUsersForRoleNames(roleNameList);
			datas = passiveEnrollmentRepository.getEnrollmentsByCreatedUser(users, startDate, endDate);
		}

		LOGGER.info("Total enrollments found - " + datas.size());

		List<PassiveEnrollmentNoticeDTO> finalData = new ArrayList<>();
		for (Enrollment enrollment : datas) {
			PassiveEnrollmentNoticeDTO record = new PassiveEnrollmentNoticeDTO();
			record.setPlanName(enrollment.getPlanName());
			record.setNetPremiumAmt(enrollment.getNetPremiumAmt());
			record.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
			record.setInsurerName(enrollment.getInsurerName());
			record.setSsapApplicationId(enrollment.getSsapApplicationid());
			record.setIssuerName((null != enrollment.getInsurerName() ? enrollment.getInsurerName() : ""));

			Set<String> names = new LinkedHashSet<String>(); /* to maintain order of insertion */
			for (Enrollee enrollee : enrollment.getEnrollees()) {
				if (enrollee.getPersonTypeLkp() != null && (EnrollmentConstants.PERSON_TYPE_SUBSCRIBER.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode()))) {
					names.add(enrollee.getFirstName() + " " + enrollee.getLastName());
				} if (enrollee.getPersonTypeLkp() != null && (EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE.equalsIgnoreCase(enrollee.getPersonTypeLkp().getLookupValueCode()))) {
				names.add(enrollee.getFirstName() + " " + enrollee.getLastName());
			}
			}
			record.setEnrolleeNames(names);

			finalData.add(record);
		}

		return finalData;
	}

	@Override
	public String generateNoticeInInbox(PassiveEnrollmentNoticeDTO passiveEnrollmentNoticeDTO)
			throws Exception {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByAppId(passiveEnrollmentNoticeDTO.getSsapApplicationId());
		if (ssapApplicationList == null || ssapApplicationList.size() == 0){
			throw new GIRuntimeException("Application not found for enrollment for ssap application id - " + passiveEnrollmentNoticeDTO.getSsapApplicationId());
		}

		SsapApplication ssapApplication = ssapApplicationList.get(0);
		passiveEnrollmentNoticeDTO.setCaseNumber(ssapApplication.getCaseNumber());

		Household household = fetchConsumer(ssapApplication);
		int moduleId = household.getId();

		String emailId = getEmailId(ssapApplication);

		String result = null;
		try {
			result = generateActivationLink(household, emailId);
		} catch (GIException e) {
			throw e;
		}

		if (null != result && !NO_PHONE_NUMBER_IN_HOUSEHOLD.equals(result)){
			passiveEnrollmentNoticeDTO.setActivationUrl(result);
		}

		String fullName = household.getFirstName() + " " + household.getLastName();
		Location location = getLocation(ssapApplication);

		String relativePath = "cmr/" + moduleId + "/ssap/" + ssapApplication.getId() + "/notifications/";
		String ecmFileName = PASSIVE_ENROLLMENT_NOTICE + "_" + moduleId + (new Date().getTime()) + ".pdf";

		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		Notice notice = noticeService.createModuleNotice(PASSIVE_ENROLLMENT_NOTICE, GhixLanguage.US_EN,
				getReplaceableObjectData(ssapApplication, passiveEnrollmentNoticeDTO),
				relativePath, ecmFileName,
				MODULE_NAME, moduleId, validEmails,
				EXCHANGE_NAME,
				fullName, location, GhixNoticeCommunicationMethod.Mail);

		return notice.getEcmId();
	}
	
	private Location getLocation(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null){
					return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null){
					return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		return null;
	}
	
	
	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1){
				return ssapApplicant.getEmailAddress();
			}
		}

		return null;
	}


	private Household fetchConsumer(SsapApplication ssapApplication) {

		int cmrId = fetchModuleId(ssapApplication);
		return cmrHouseholdRepository.findOne(cmrId);
	}

	private int fetchModuleId(SsapApplication ssapApplication) {

		int cmrId = ssapApplication.getCmrHouseoldId() != null ? ssapApplication.getCmrHouseoldId().intValue() : 0;

		if (cmrId == 0){
			throw new GIRuntimeException("Cannot generate notification! CMR Household ID not found for case number - " + ssapApplication.getCaseNumber());
		}
		return cmrId;
	}

	private Map<String, Object> getReplaceableObjectData(SsapApplication ssapApplication, PassiveEnrollmentNoticeDTO passiveEnrollmentNoticeDTO) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put(PRIMARY_APPLICANT_NAME, getName(ssapApplication));
		tokens.put(TODAY_DATE, DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
		SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new Date()));
		tokens.put(PASSIVE_ENROLLMENT_NOTICE_DTO, passiveEnrollmentNoticeDTO);
		tokens.put("ssapApplicationId", ssapApplication.getId());
		return tokens;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName()+ " "+ssapApplicant.getLastName();
			}
		}

		return null;
	}


	private String generateActivationLink(Household household, String emailId) throws GIException {

		/* If HH is linked with account, skip */
		if (household.getUser() != null){
			return ACCOUNT_EXISTS;
		}
		/* If HH has activation link then fetch activation link from table */
		List<AccountActivation> activationList = iAccountActivationRepository.findByCreatedObjectTypeAndCreatorObjectId(ActivationJson.OBJECTTYPE.INDIVIDUAL.toString(), household.getId());
		if (activationList != null && activationList.size() > 0){
			AccountActivation activationObj = activationList.get(0);
			return ACCOUNT_USER_ACTIVATION_LINK + activationObj.getActivationToken();
		}

		/* If HH has phone number then generate activation link in db and donot trigger email, return activationUrl */
		if (StringUtils.isEmpty(household.getPhoneNumber())){
			return NO_PHONE_NUMBER_IN_HOUSEHOLD;
		}


		Map<String,String> consumerActivationDetails = new HashMap<String, String>();
		consumerActivationDetails.put("consumerName", household.getFirstName()+" " +household.getLastName());
		consumerActivationDetails.put("exchangeName",EXCHANGE_NAME);
		consumerActivationDetails.put("exchangePhone",EXCHANGE_PHONE);
		consumerActivationDetails.put("exchangeURL",EXCHANGE_URL);
		consumerActivationDetails.put("emailType",CONSUMER_ACCOUNT_ACTIVATION_EMAIL);
		consumerActivationDetails.put("expirationDays",EXPIRATION_DAYS_STR);

		CreatedObject createdObject = new CreatedObject();
		createdObject.setObjectId(household.getId());
		createdObject.setEmailId(emailId);
		createdObject.setRoleName(GhixRole.INDIVIDUAL.toString());
		createdObject.setPhoneNumbers(Arrays.asList(household.getPhoneNumber()));
		createdObject.setFullName(household.getFirstName() + " " + household.getLastName());
		createdObject.setFirstName(household.getFirstName());
		createdObject.setLastName(household.getLastName());
		createdObject.setCustomeFields(consumerActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		creatorObject.setObjectId(0);
		creatorObject.setFullName(ADMINISTRATOR);
		creatorObject.setRoleName(GhixRole.ADMIN.toString());


		AccountActivation activationObj = accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, EXPIRATION_DAYS);
		return ACCOUNT_USER_ACTIVATION_LINK + activationObj.getActivationToken();

	}


}
