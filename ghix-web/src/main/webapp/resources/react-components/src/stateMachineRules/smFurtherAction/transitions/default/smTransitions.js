const transitions = {
    't_tabIsInsideOE': {
        conditionId: 'c_tabIsInsideOE',
        stateId: 's_periodTypeOE'
    },
    't_tabIsOutsideOE': {
        conditionId: 'c_tabIsOutsideOE',
        stateId: 's_periodTypeOutsideOE'
    },

    // ** OE
    't_periodTypeOE_2': {
        conditionId: 'c_AppStatusGroup1',
        stateId: 's_startApplicationOE1'
    },
    't_periodTypeOE_3': {
        conditionId: 'c_AppStatusOP',
        stateId: 's_resumeApplicationOE2'
    },
    't_periodTypeOE_4': {
        conditionId: 'c_AppStatusSG_SU',
        stateId: 's_noButtonOE3'
    },

    't_periodTypeOE_5': {
        conditionId: 'c_AppStatusQU',
        stateId: 's_noButtonOE10'
    },

    't_OE_AppType': {
        conditionId: 'c_AppStatusEN_PN_ER',
        stateId: 's_OEAppType'
    },

    // OE OEP
    't_OE_OEP': {
        conditionId: 'c_AppTypeOEP',
        stateId: 's_isRenewalAppSet',
    },

    't_renewalAppSet': {
        conditionId: 'c_renewalStatusSet',
        stateId: 's_OE_OEP_isQHPEligible',
    },
    't_renewalAppNotSet': {
        conditionId: 'c_renewalStatusNotSet',
        stateId: 's_OE_OEP_NotRenewedAppStatus',
    },

    't_OE_OEP_QHPEligible': {
        conditionId: 'c_QHPEligible',
        stateId: 's_OE_OEP_RenewalsAppStatus',
    },

    't_OE_OEP_QHPNotEligible': {
        conditionId: 'c_QHPNotEligible',
        stateId: 's_finalizePlansREN6',
    },

    't_OE_OEP_RenewalsAppStatusER' : {
        conditionId: 'c_AppStatusER',
        stateId: 's_OE_OEP_RenewalsAppStatusER_isDentalAppStatusEN',
    },

    't_OE_OEP_RenewalsAppStatusER_dentalAppStatusEN' : {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingREN2',
    },

    't_OE_OEP_RenewalsAppStatusER_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_finalizePlansREN1',
    },

    't_OE_OEP_RenewalsAppStatusPN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_continueShoppingREN3',
    },
    't_OE_OEP_RenewalsAppStatusEN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_OE_OEP_RenewalsAppStatusEN_isDentalAppStatusEN',
    },

    't_OE_OEP_RenewalsAppStatusEN_dentalAppStatusEN' : {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_changePlansREN4',
    },
    't_OE_OEP_RenewalsAppStatusEN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingREN5',
    },

    // OE OEP >> Not Renewed >> ER
    't_OE_OEP_NotRenewedAppStatusER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_OE_OEP_NotRenewedER',
    },
    't_OE_OEP_NotRenewed_ER_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingOE11',
    },
    't_OE_OEP_NotRenewed_ER_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_shopForPlansOE12',
    },

    // OE OEP >> Not Renewed >> PN
    't_OE_OEP_NotRenewedAppStatusPN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_continueShoppingOE13',
    },

    // OE OEP >> Not Renewed >> EN
    't_OE_OEP_NotRenewedAppStatusEN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_OE_OEP_NotRenewedEN',
    },
    't_OE_OEP_NotRenewed_EN_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_changePlansOE15',
    },
    't_OE_OEP_NotRenewed_EN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingOE4',
    },

    // OE QEP
    't_OE_QEP': {
        conditionId: 'c_AppTypeQEP',
        stateId: 's_OE_QEP_isEventVerified',
    },
    't_OE_QEP_eventVerified': {
        conditionId: 'c_eventVerified',
        stateId: 's_OE_OEP_NotRenewedAppStatus',
    },
    't_OE_QEP_eventNotVerified': {
        conditionId: 'c_eventNotVerified',
        stateId: 's_outsideOE_QEP_ER_verificationStatus',
    },

    // OE SEP
    't_OE_SEP': {
        conditionId: 'c_AppTypeSEP',
        stateId: 's_OE_SEP_AppStatus',
    },

    // OE SEP >> ER
    't_OE_SEP_AppStatusER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_outsideOE_SEP_ER',
    },

    // OE SEP >> PN
    't_OE_SEP_AppStatusPN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_continueShoppingSEP5',
    },

    // OE SEP >> EN
    't_OE_SEP_AppStatusEN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_OE_SEP_EN',
    },
    't_OE_SEP_EN_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_changePlansOE15',
    },
    't_OE_SEP_EN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingOE4',
    },

    // ** OUTSIDE OE
    't_OutsideOE_2': {
        conditionId: 'c_AppStatusGroup2',
        stateId: 's_reportLifeEventAndShopOE5'
    },
    't_OutsideOE_3': {
        conditionId: 'c_AppStatusSG_SU',
        stateId: 's_noButtonOE3'
    },
    't_OutsideOE_4': {
        conditionId: 'c_AppStatusQU',
        stateId: 's_noButtonOE10'
    },
    't_OutsideOE_AppType': {
        conditionId: 'c_AppStatusEN_PN_ER',
        stateId: 's_outsideOEAppType'
    },

    // OUTSIDE OE QEP
    't_OutsideOE_QEP': {
        conditionId: 'c_AppTypeQEP',
        stateId: 's_outsideOE_QEP'
    },

    // OUTSIDE OE QEP >> ER
    't_outsideOE_QEP_ER_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingQLE2',
    },
    't_outsideOE_QEP_ER_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_shopForPlansQLE6',
    },

    // OUTSIDE OE SEP
    't_OutsideOE_SEP': {
        conditionId: 'c_AppTypeSEP',
        stateId: 's_outsideOE_SEP'
    },

    // OUTSIDE OE SEP >> ER
    't_outsideOE_SEP_ER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_outsideOE_SEP_ER',
    },
    't_outsideOE_SEP_ER_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingSEP3',
    },
    't_outsideOE_SEP_ER_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_finalizePlansSEP2',
    },
    't_outsideOE_SEP_ER_eventIdentified': {
        conditionId: 'c_eventIdentified',
        stateId: 's_outsideOE_SEP_ER_isSepOpen',
    },
    't_outsideOE_SEP_ER_eventNotIdentified': {
        conditionId: 'c_eventNotIdentified',
        stateId: 's_confirmEventAndFinalizePlansSEP1',
    },
    't_outsideOE_SEP_ER_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_outsideOE_SEP_ER_isEventVerified',
    },
    't_outsideOE_SEP_ER_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_outsideOE_SEP_ER_isInsideOEEnrollmentWindow',
    },
    't_outsideOE_SEP_ER_insideOEEnrollmentWindow': {
        conditionId: 'c_tabIsInsideOEEnrollment',
        stateId: 's_outsideOE_SEP_ER_insideOEEnrollmentWindow_isDentalAppStatusEN',
    },
    't_outsideOE_SEP_ER_insideOEEnrollmentWindow_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingOE11',
    },
    't_outsideOE_SEP_ER_insideOEEnrollmentWindow_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_shopForPlansOE12'
    },

    't_outsideOE_SEP_ER_outsideOEEnrollmentWindow': {
        conditionId: 'c_tabIsOutsideOEEnrollment',
        stateId: 's_reportAChangeSEP4',
    },
    't_outsideOE_SEP_ER_eventVerified': {
        conditionId: 'c_eventVerified',
        stateId: 's_outsideOE_SEP_ER_isDentalAppStatusEN',
    },
    't_outsideOE_SEP_ER_eventNotVerified': {
        conditionId: 'c_eventNotVerified',
        stateId: 's_outsideOE_QEP_ER_verificationStatus',
    },
    't_outsideOE_SEP_ER_enrolledInDental': {
        conditionId: 'c_enrolledInDental',
        stateId: 's_continueShopping_3',
    },
    't_outsideOE_SEP_ER_notEnrolledInDental': {
        conditionId: 'c_notEnrolledInDental',
        stateId: 's_finalizePlans',
    },

    // OUTSIDE OE SEP >> PN
    't_outsideOE_SEP_PN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_outsideOE_SEP_PN',
    },
    't_outsideOE_SEP_PN_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_continueShoppingSEP5',
    },
    't_outsideOE_SEP_PN_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_outsideOE_SEP_PN_isAIAN',
    },
    't_outsideOE_SEP_PN_AIAN': {
        conditionId: 'c_AIAN',
        stateId: 's_changePlansQLE9'
    },
    't_outsideOE_SEP_PN_NotAIAN': {
        conditionId: 'c_notAIAN',
        stateId: 's_outsideOE_SEP_PN_notAIAN_isInsideOEEnrollmentWindow'
    },
    't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow': {
        conditionId: 'c_tabIsInsideOEEnrollment',
        stateId: 's_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_isDentalAppStatusEN'
    },
    't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_changePlansOE15'
    },
    't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingOE4'
    },
    't_outsideOE_SEP_PN_notAIAN_outsideOEEnrollmentWindow': {
        conditionId: 'c_tabIsOutsideOEEnrollment',
        stateId: 's_reportAChangeSEP4'
    },

    // OUTSIDE OE SEP >> EN
    't_outsideOE_SEP_EN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_outsideOE_SEP_EN',
    },
    't_outsideOE_SEP_EN_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_outsideOE_SEP_EN_isChangePlan2',
    },
    't_outsideOE_SEP_EN_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_outsideOE_SEP_PN_isAIAN',
    },
    't_outsideOE_SEP_EN_enrolledInDental': {
        conditionId: 'c_enrolledInDental',
        stateId: 's_outsideOE_SEP_EN_isDentalAppStatusEN',
    },
    't_outsideOE_SEP_EN_notEnrolledInDental': {
        conditionId: 'c_notEnrolledInDental',
        stateId: 's_continueShoppingSEP3'
    },
    't_outsideOE_SEP_EN_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_outsideOE_SEP_PN_isAIAN',
    },
    't_outsideOE_SEP_EN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingSEP3',
    },
    't_outsideOE_SEP_EN_changePlan2': {
        conditionId: 'c_changePlans',
        stateId: 's_changePlansSEP6'
    },
    't_outsideOE_SEP_EN_notChangePlan2': {
        conditionId: 'c_notChangePlans',
        stateId: 's_outsideOE_SEP_EN_isEnrolledInDental'
    },

    // OUTSIDE OE OEP
    't_OutsideOE_OEP': {
        conditionId: 'c_AppTypeOEP',
        stateId: 's_outsideOE_OEP'
    },
    't_outsideOE_OEP_periodWithinDates': {
        conditionId: 'c_tabIsInsideOEEnrollment',
        stateId: 's_outsideOE_OEP_AppStatus',
    },
    't_outsideOE_OEP_periodNotWithinDates': {
        conditionId: 'c_tabIsOutsideOEEnrollment',
        stateId: 's_outsideOE_OEP_isPriorOEStartDate',
    },
    't_outsideOE_OEP_priorOEStartDate': {
        conditionId: 'c_priorToOEStartDate',
        stateId: 's_noButtonOE8',
    },
    't_outsideOE_OEP_notPriorOEStartDate': {
        conditionId: 'c_notPriorToOEStartDate',
        stateId: 's_outsideOE_OEP_notPriorOEStartDateAppStatus',
    },
    't_outsideOE_OEP_notPriorOEStartDate_ER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_reportAChangeOE9',
    },
    't_outsideOE_OEP_notPriorOEStartDate_EN_PN': {
        conditionId: 'c_AppStatusEN_PN',
        stateId: 's_outsideOE_OEP_notPriorOEStartDate_isAIAN',
    },
    't_outsideOE_OEP_notPriorOEStartDate_AIAN': {
        conditionId: 'c_AIAN',
        stateId: 's_changePlansQLE8',
    },
    't_outsideOE_OEP_notPriorOEStartDate_NotAIAN': {
        conditionId: 'c_notAIAN',
        stateId: 's_reportAChangeQLE11',
    },

    // OUTSIDE OE OEP >> ER
    't_outsideOE_OEP_ER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_outsideOE_OEP_ER'
    },
    't_outsideOE_OEP_ER_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_continueShoppingOE11',
    },
    't_outsideOE_OEP_ER_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_shopForPlansOE12',
    },

    // OUTSIDE OE OEP >> EN
    't_outsideOE_OEP_EN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_outsideOE_OEP_EN'
    },
    't_outsideOE_OEP_EN_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_outsideOE_OEP_EN_isAIAN',
    },
    't_outsideOE_OEP_EN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingOE4'
    },
    't_outsideOE_OEP_EN_AIAN': {
        conditionId: 'c_AIAN',
        stateId: 's_changePlansQLE9'
    },
    't_outsideOE_OEP_EN_NotAIAN': {
        conditionId: 'c_notAIAN',
        stateId: 's_reportAChangeSEP4'
    },
    't_outsideOE_OEP_EN_enrolledInDental': {
        conditionId: 'c_enrolledInDental',
        stateId: 's_outsideOE_QEP_PN_isAIAN',
    },
    't_outsideOE_OEP_EN_notEnrolledInDental': {
        conditionId: 'c_notEnrolledInDental',
        stateId: 's_continueShopping_3',
    },

    // OUTSIDE OE OEP >> PN
    't_outsideOE_OEP_PN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_continueShoppingOE3'
    },

    // OUTSIDE OE QEP >> ER
    't_outsideOE_QEP_ER': {
        conditionId: 'c_AppStatusER',
        stateId: 's_outsideOE_QEP_ER'
    },
    't_outsideOE_QEP_ER_eventNotIdentified': {
        conditionId: 'c_eventNotIdentified',
        stateId: 's_confirmEventAndShopQLE1'
    },
    't_outsideOE_QEP_ER_eventIdentified': {
        conditionId: 'c_eventIdentified',
        stateId: 's_outsideOE_QEP_ER_isSepOpen'
    },
    't_outsideOE_QEP_ER_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_reportAChangeQLE4'
    },
    't_outsideOE_QEP_ER_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_outsideOE_QEP_ER_isEventVerified'
    },
    't_outsideOE_QEP_ER_eventGated': {
        conditionId: 'c_eventGated',
        stateId: 's_outsideOE_QEP_ER_isEventVerified'
    },
    't_outsideOE_QEP_ER_eventVerified': {
        conditionId: 'c_eventVerified',
        stateId: 's_outsideOE_QEP_ER_isDentalAppStatusEN'
    },
    't_outsideOE_QEP_ER_eventNotVerified': {
        conditionId: 'c_eventNotVerified',
        stateId: 's_outsideOE_QEP_ER_verificationStatus'
    },
    't_outsideOE_QEP_ER_verificationStatusPending': {
        conditionId: 'c_verificationStatusPending',
        stateId: 's_uploadDocumentsQLE3'
    },
    't_outsideOE_QEP_ER_verificationStatusPendingInfo': {
        conditionId: 'c_verificationStatusPendingInfo',
        stateId: 's_provideAdditionalInfoQLE15'
    },
    't_outsideOE_QEP_ER_enrolledInDental': {
        conditionId: 'c_enrolledInDental',
        stateId: 's_continueShopping_2'
    },
    't_outsideOE_QEP_ER_notEnrolledInDental': {
        conditionId: 'c_notEnrolledInDental',
        stateId: 's_shopForPlans'
    },

    // OUTSIDE OE QEP >> PN
    't_outsideOE_QEP_PN': {
        conditionId: 'c_AppStatusPN',
        stateId: 's_outsideOE_QEP_PN'
    },
    't_outsideOE_QEP_PN_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_continueShoppingQLE5'
    },
    't_outsideOE_QEP_PN_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_outsideOE_QEP_PN_isAIAN'
    },
    't_outsideOE_QEP_PN_AIAN': {
        conditionId: 'c_AIAN',
        stateId: 's_changePlansQLE8'
    },
    't_outsideOE_QEP_PN_NotAIAN': {
        conditionId: 'c_notAIAN',
        stateId: 's_reportAChangeQLE7'
    },

    // OUTSIDE OE QEP >> EN
    't_outsideOE_QEP_EN': {
        conditionId: 'c_AppStatusEN',
        stateId: 's_outsideOE_QEP_EN'
    },
    't_outsideOE_QEP_EN_SEPOpen': {
        conditionId: 'c_SEPOpen',
        stateId: 's_outsideOE_QEP_EN_isDentalAppStatusEN'
    },
    't_outsideOE_QEP_EN_dentalAppStatusEN': {
        conditionId: 'c_dentalAppStatusEN',
        stateId: 's_outsideOE_QEP_PN_isAIAN',
    },
    't_outsideOE_QEP_EN_dentalAppStatusNotEN': {
        conditionId: 'c_dentalAppStatusNotEN',
        stateId: 's_continueShoppingQLE10',
    },
    't_outsideOE_QEP_EN_SEPNotOpen': {
        conditionId: 'c_SEPNotOpen',
        stateId: 's_outsideOE_QEP_PN_isAIAN'
    },
    't_outsideOE_QEP_EN_enrolledInDental': {
        conditionId: 'c_enrolledInDental',
        stateId: 's_outsideOE_QEP_PN_isAIAN'
    },
    't_outsideOE_QEP_EN_notEnrolledInDental': {
        conditionId: 'c_notEnrolledInDental',
        stateId: 's_continueShopping_2'
    },
};

export default transitions;