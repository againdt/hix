package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.StructuredAddressType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.StructuredAddressType;
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class StructuredAddressTypeWrapper implements JSONAware{

	
	private StreetTypeWrapper locationStreet;
	private String locationCityName;
	private String locationStateUSPostalServiceCode;
	private String locationPostalCode;
	
	private static final String STREET_KEY = "LocationStreet";
	private static final String CITY_KEY = "LocationCityName";
	private static final String STATE_POSTAL_CODE_KEY = "LocationStateUSPostalServiceCode";
	private static final String POSTAL_CODE_KEY = "LocationPostalCode";
	
	public StructuredAddressTypeWrapper(
			StructuredAddressType structuredAddressType) {

		if (structuredAddressType == null) {
			return;
		}
		
		this.locationStreet = new StreetTypeWrapper(structuredAddressType.getLocationStreet());
		this.locationCityName = structuredAddressType.getLocationCityName() != null ? structuredAddressType.getLocationCityName().getValue() : null;
		this.locationStateUSPostalServiceCode = structuredAddressType.getLocationStateUSPostalServiceCode()!=null ? structuredAddressType.getLocationStateUSPostalServiceCode().getValue().value():null;
		this.locationPostalCode = structuredAddressType.getLocationPostalCode()!= null ? structuredAddressType.getLocationPostalCode().getValue() : null;

	}

	public StreetTypeWrapper getLocationStreet() {
		return locationStreet;
	}

	public String getLocationCityName() {
		return locationCityName;
	}

	public String getLocationStateUSPostalServiceCode() {
		return locationStateUSPostalServiceCode;
	}

	public String getLocationPostalCode() {
		return locationPostalCode;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.locationStreet != null){
			obj.put(STREET_KEY, this.locationStreet);
		}
		obj.put(CITY_KEY, this.locationCityName);
		obj.put(STATE_POSTAL_CODE_KEY, this.locationStateUSPostalServiceCode);
		obj.put(POSTAL_CODE_KEY, this.locationPostalCode);
		return obj.toJSONString();
	}
	

}
