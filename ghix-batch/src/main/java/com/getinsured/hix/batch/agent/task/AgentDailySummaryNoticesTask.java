package com.getinsured.hix.batch.agent.task;


import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.agent.service.AgentBatchService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

public class AgentDailySummaryNoticesTask extends StepExecutionListenerSupport implements Tasklet {

	private static final Logger lOGGER = Logger.getLogger(AgentDailySummaryNoticesTask.class);
	private AgentBatchService agentBatchService;
	//private AgedOutDisenrollmentNoticeService agedOutDisenrollmentNoticeService;
	private static volatile Boolean isBatchRunning = false;
	@Override
	public void beforeStep(StepExecution stepExecution) {
		  JobParameters jobParameters = stepExecution.getJobParameters();		  
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
	
		synchronized (isBatchRunning) {
			if(isBatchRunning) {
				return RepeatStatus.FINISHED;
			}
			else {
				isBatchRunning = true;
			}
		}
		
		try {
		
			lOGGER.debug(this.getClass().getName()+" started at " + new Timestamp(System.currentTimeMillis()));
			if(DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENT_BOOKOFBUSINESSSUMMARYNOTICES_IS_ENABLED).equalsIgnoreCase("Y"))
			{
								
				agentBatchService.sendNotices();
				//generate the notification with this list of individuals
			}
			
			
		} catch (Exception e) {
			lOGGER.error(e);
		}finally{
			isBatchRunning = false;
		}
	
		lOGGER.debug(this.getClass().getName()+" finishing at " + new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}

	public AgentBatchService getAgentBatchService() {
		return agentBatchService;
	}

	public void setAgentBatchService(AgentBatchService agentBatchService) {
		this.agentBatchService = agentBatchService;
	}	
	
}
