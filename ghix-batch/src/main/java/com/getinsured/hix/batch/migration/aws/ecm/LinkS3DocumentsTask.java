package com.getinsured.hix.batch.migration.aws.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.migration.aws.ecm.S3DataConfig.MigrationStepEnum;
import com.getinsured.hix.batch.migration.couchbase.ecm.CouchMigrationUtil;
import com.getinsured.hix.batch.migration.couchbase.ecm.CouchbaseECMMigrationDTO;
import com.getinsured.hix.batch.migration.couchbase.ecm.JDBCUtil;
import com.getinsured.hix.batch.migration.couchbase.ecm.Pagination;

public class LinkS3DocumentsTask extends BaseEcmS3MigrationClass {
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkS3DocumentsTask.class);
	private JdbcTemplate jdbcTemplate;
	
	public LinkS3DocumentsTask() {
		super(MigrationStepEnum.UPDATE);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	void startExecution() throws Exception {
		Connection con = null;
		try {
			LOGGER.info("Initiate the process of linking documents in db with S3");
			con = jdbcTemplate.getDataSource().getConnection();
			for (CouchbaseECMMigrationDTO tableDTO : S3DataConfig.migrationTables.values()) {
				LOGGER.info("Linking documents from Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName());
				int maxPrimaryRecordId = extractMaximumPrimaryRecordId(con, tableDTO.getTableName(),
						tableDTO.getColumnName(), S3DataConfig.MigrationStatusEnum.S3DONE);
				LOGGER.info("Maximum Primary Id for Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName() + " is - " + maxPrimaryRecordId);
				if (maxPrimaryRecordId != 0) {
					startDocumentLinking(con, tableDTO.getTableName(), tableDTO.getColumnName(),
							tableDTO.getPrimaryIdColumn(), maxPrimaryRecordId);
				}
			}
			LOGGER.info("Process of linking documents in db with couchbase completes successfully");
		} catch (Exception e) {
			LOGGER.error("Encountered exception in process of linking documents in db with S3", e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.setAutoCommit(true);
				} catch (Exception e2) {

				}

				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}
	}

	private void startDocumentLinking(Connection con, String tableName, String columnName, String primaryIdColumn,
			int maxPrimaryRecordId) throws Exception {
		Pagination paging = new Pagination(maxPrimaryRecordId);
		List<Map<String, String>> data;
		int iSize;
		while (paging.isNext()) {
			LOGGER.info("Link records for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			data = fetchRecordsToLink(con, tableName, columnName, paging.getStartRecordNumber(),
					paging.getEndRecordNumber());
			iSize = CouchMigrationUtil.listSize(data);

			if (iSize == 0) {
				continue;
			}
			LOGGER.info(iSize + " - records found for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			linkDocument(con, tableName, columnName, primaryIdColumn, data);
		}
	}

	private void linkDocument(Connection con, String tableName, String columnName, String primaryIdColumn,
			List<Map<String, String>> data) {
		// boolean blnCheck;
		for (Map<String, String> map : data) {
			if (CouchMigrationUtil.isValidString(map.get(S3DataConfig.S3_ID))) {
				
				updateDb(con, map, tableName, columnName, primaryIdColumn);
			}
		}
	}

	@SuppressWarnings("unused")
	private void updateDb(Connection con, Map<String, String> map, String tableName, String columnName,
			String primaryIdColumn) {
		try {
			con.setAutoCommit(false);
			StringBuilder st1 = new StringBuilder(50);
			st1.append("update ").append(S3DataConfig.S3_MIGRATION).append(" set MIGRATION_STATUS = '")
					.append(S3DataConfig.MigrationStatusEnum.MIGRATIONDONE.name()).append("' where MIGRATION_ID = ")
					.append(map.get(S3DataConfig.MIGRATION_ID));
			StringBuilder st2 = new StringBuilder(50);
			st2.append("update ").append(tableName).append(" set ").append(columnName).append(" = '")
					.append(map.get(S3DataConfig.S3_ID)).append("' where ").append(primaryIdColumn).append(" = ")
					.append(map.get(S3DataConfig.PRIMARY_ID));

			int[] ireturn = JDBCUtil.executeBatchUpdate(con, new String[] { st1.toString(), st2.toString() });

			con.commit();
			st1 = null;
			st2 = null;
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception e2) {
				/** TODO should we eat the exception */
			}
		}
	}

	private List<Map<String, String>> fetchRecordsToLink(Connection con, String tableName, String columnName,
			int startRecordNumber, int endRecordNumber) throws SQLException {
		StringBuilder st = new StringBuilder(50);
		st.append("select MIGRATION_ID, PRIMARY_ID, S3_ID from ").append(S3DataConfig.S3_MIGRATION)
				.append(" where TABLE_NAME = '").append(tableName).append("' and COLUMN_NAME = '").append(columnName)
				.append("' and MIGRATION_STATUS in ('").append(S3DataConfig.MigrationStatusEnum.S3DONE.name())
				.append("')").append(" and PRIMARY_ID between ").append(startRecordNumber).append(" and ")
				.append(endRecordNumber);

		return JDBCUtil.records(con, st.toString(),
				new String[] { S3DataConfig.MIGRATION_ID, S3DataConfig.PRIMARY_ID, S3DataConfig.S3_ID });
	}

}