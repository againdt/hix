<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
 <c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
 	<c:if test="${formulary.formularyId != null && formulary.formularyId != ''}" >
		<c:set var="encPlanYear" ><encryptor:enc value="${formulary.applicableYear}" isurl="true"/> </c:set>
		<c:set var="encHiosId" ><encryptor:enc value="${plan.issuer.hiosIssuerId}" isurl="true"/> </c:set>
		<c:set var="encFormularyId" ><encryptor:enc value="${formulary.formularyId}" isurl="true"/> </c:set>
	</c:if>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="label.plans"/></a></li>
            <li><a href="<c:url value="/issuer/manageqhp"/>"><spring:message  code='label.linkManageQHP'/></a></li>
            <li><spring:message  code='label.linkPlanDetails'/></li>
        </ul><!--page-breadcrumb ends-->
		<h1><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/> ${plan.name}</h1>
		
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.planAbout'/></h4>       			
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li class="active"><a href="#"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/viewbenefits/${encPlanId}"/>"><spring:message  code='label.planBenefits'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/viewrates/${encPlanId}"/>"><spring:message  code='label.planRates'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/qhphistory/${encPlanId}"/>"><spring:message  code='label.planStatusHistory'/></a></li>
			   </ul>

		
        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
				<div class="header"> 
                    <h4>
                        <spring:message code='pgheader.planDetails' />
                        <c:if test="${fn:toUpperCase(STATE_CODE) == 'ID'}">
                        	<a class="btn btn-primary btn-small pull-right margin0" href="<c:url value="/issuer/qhp/createTicket/${encPlanId}"/>"><spring:message code='label.createTicket' /></a>
                        </c:if>
                        <br/>
                    </h4>
                    
				</div>
				<input type="hidden" name="id" id="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate" />
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"  />
					<fmt:formatDate pattern="MM/dd/yyyy" value="${futureDate}" var="enFutureDate" timeZone="${timezone}" />
				<table class="table table-condensed table-border-none">
					<tr>
						<td class="txt-right span4"><spring:message  code='label.planName'/></td>
						<td><strong>${plan.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planNumber'/></td>
						<td><strong>${plan.issuerPlanNumber}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planMarket'/></td>
						<td><strong>${plan.market}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planLevel'/></td>
						<td><strong>
							<c:forEach var="levelList" items="${levelList}">
								<c:if test="${levelList.key == plan.planHealth.planLevel}">${levelList.value}</c:if>
							</c:forEach>
						</strong>
						</td>
					</tr>
					<c:if test="${not empty ehbPremiumFraction}">
						<tr>
							<td class="txt-right"><spring:message  code="label.ehbPremiumFraction"/></td>
							<td><strong>${ehbPremiumFraction}</strong></td>
						</tr>
					</c:if>
                    <tr>
						<td class="txt-right"><spring:message  code='label.linkProviderNetwork'/></td>
						<td><strong>${plan.network.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planHSAPlan'/></td>
						<td><strong>${plan.hsa}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planType"/></td>
						<td><strong>${plan.networkType}</strong></td>
					</tr>
					<c:if test="${plan.planHealth.planDesignType != null && plan.planHealth.planDesignType != ''}">
					<tr>
						<td class="txt-right"><spring:message code="label.planDesignType"/></td>
						<td><strong>${plan.planHealth.planDesignType}</strong></td>
					</tr>
					</c:if>
					<tr>
						<td class="txt-right"><spring:message  code='label.planStartDate'/></td>
						<td><strong>${stDate}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planEndDate'/></td>
						<td><strong>${enDate}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.formularlyId"/></td>
						<td>
						<c:if test="${formulary.formularyId != null && formulary.formularyId != '' && formulary.drugList !=null && formulary.drugList.isDeleted == 'N'}" >
							<strong>${formulary.formularyId}</strong>
							<a class="marginTop10" href="<c:url value="/planmgmt/issuer/downLoadFormularyDrug/${encPlanYear}/${encHiosId}/${encFormularyId}" />"><spring:message code="label.download"/></a>
						</c:if>
						</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.formularlyURL"/></td>
						<td><strong>${formulary.formularyUrl}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code="label.serviceAreaId" /></td>
						<td><strong>${serviceAreaId}</strong>
						<a class="marginTop10" href="<c:url value="/planmgmt/issuer/downLoadServiceArea/${encPlanId}" />"><spring:message code="label.download"/></a>
						</td>
					</tr>
					
					<tr>
						<td class="txt-right"><spring:message  code='label.certifictionStatus'/></td>
						<td><strong>
						<c:choose>
								<c:when test="${fn:toUpperCase(plan.status) == 'CERTIFIED'}"><spring:message code='label.certified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'LOADED'}"><spring:message code='label.loaded' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'DECERTIFIED'}"><spring:message code='label.deCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'NONCERTIFIED'}"><spring:message code='label.nonCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'RECERTIFIED'}"><spring:message code='label.reCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'INCOMPLETE'}"><spring:message code='label.incomplete' /></c:when>
								<c:otherwise>${plan.status}</c:otherwise>
						</c:choose></strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.enrollAvail'/></td>
						<td><strong>${enrollAvailability}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"></td>
						<td>
							<c:if test="${futureStatus != ''}">
								<strong>${futureStatus}</strong> <spring:message code='label.showFrom'/> ${enFutureDate}
							</c:if>							
						</td>
					</tr>
				
				</table>
		</div><!-- end of .span9 -->
	</div><!--  end of .row-fluid -->
</div><!-- end of .gutter10 -->

<script type="text/javascript">
$('.ttclass').tooltip();
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})
</script>
