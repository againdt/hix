import { TestBed, inject } from '@angular/core/testing';

import { DataKeepService } from './data-keep.service';

describe('DataKeepService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataKeepService]
    });
  });

  it('should ...', inject([DataKeepService], (service: DataKeepService) => {
    expect(service).toBeTruthy();
  }));
});
