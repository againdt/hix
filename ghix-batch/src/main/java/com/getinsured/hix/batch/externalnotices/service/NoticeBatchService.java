package com.getinsured.hix.batch.externalnotices.service;

import com.getinsured.hix.platform.util.exception.GIException;

import java.time.LocalDate;

public interface NoticeBatchService {
    void processUndeliverableMail(String filePathName) throws GIException;
}
