{
   "householdMember": [
      {
         "personId": "1",
         "name": {
            "firstName": "Clark",
            "middleName": "B",
            "lastName": "Michael"
         },
         "dateOfBirth": "1936-07-13",
         "socialSecurityCard": {
            "socialSecurityNumber": "221212500"
         },
         "citizenshipImmigrationStatus": {
            "citizenshipAsAttestedIndicator": "false"
         },
         "incarcerationStatus": {
            "incarcerationAsAttestedIndicator": "false"
         }
      }
   ]
}