<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}"> 


<!-- Add Comments dialog -->
<div id="addCommentsDlg" class="modal hide fade" tabindex="-1"	role="dialog" aria-labelledby="addCommentsDlg" aria-hidden="true" >
	<div class="addCommentHeader">
		<div class="header">
			<h4 class="pull-left"> <spring:message code="header.addComment" /></h4>
			<button aria-hidden="true" data-dismiss="modal" id="crossClose"	class="close" title="x" type="button" onClick="cancelBtnClicked()"><spring:message code="btn.crossClose" /></button>
		</div>
	</div>

	<div class="modal-body">
		<div class="control-group">
			<div class="controls">
				<label> <spring:message code="label_ac.comments" /></label>
				<textarea class="span" name="add_comment_text" id="add_comment_text" rows="4" cols="40" style="resize: none;" maxlength="4000"
					spellcheck="true" value=""></textarea>
				<div id="comment_text_error"></div>
			</div>
		</div>
	</div>
	<div class="modal-footer clearfix">
		<button class="btn pull-left" id="cancelAttachment" data-dismiss="modal" aria-hidden="true" onclick="cancelBtnClicked()"><spring:message code="button.cancel" /></button>
		<button class="btn btn-primary"	onclick="javascript:addComments();"><spring:message code="button.save" /></button>
	</div>
	<div id="errorMsg"><label id="errorLabel"></label></div>
</div>
<!--Add Comment dialog ends -->
<script>

	function cancelBtnClicked() {	
		$("#add_comment_text").val("");	
	}
	 
	function addComments() {
		var ticketId = $("#ticketId").val();
		var validateUrl = "<c:url value='/ticketmgmt/ticket/savecomment'/>";
		var commentData = $("#add_comment_text").val();

		$.ajax({
			url : validateUrl,
			type:"post",
			data : {
				ticketId : ticketId,
				comment : commentData,
				'${df:csrfTokenParameter()}' : '<df:csrfToken plainToken="true"/>'
			},
			success : function(response) {
				$('#addCommentsDlg').modal('hide');
				if (response != null && response != "") {
					$('#errorLabel').text(response);
				} 
				

			},
			error : function(error) {
				alert("comments were not added");

			}

		});

	}
</script>