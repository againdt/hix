package com.getinsured.hix.indportal.dto;

/**
 * DTO class for One Touch Renewal
 * 
 * @author vishwanath_s
 *
 */
public class OTR {

	private String caseNumber;
	private boolean showOTR;
	private String coverageYear;
	private boolean renewed;
	private boolean pendingApp;

	/**
	 * @return the caseNumber
	 */
	public String getCaseNumber() {
		return caseNumber;
	}
	
	/**
	 * @param caseNumber the caseNumber to set
	 */
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	/**
	 * @return the showOTR
	 */
	public boolean isShowOTR() {
		return showOTR;
	}

	/**
	 * @param showOTR the showOTR to set
	 */
	public void setShowOTR(boolean showOTR) {
		this.showOTR = showOTR;
	}

	/**
	 * @return the coverageYr
	 */
	public String getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYr the coverageYr to set
	 */
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public boolean isRenewed() {
		return renewed;
	}

	public void setRenewed(boolean renewed) {
		this.renewed = renewed;
	}

	public boolean isPendingApp() {
		return pendingApp;
	}

	public void setPendingApp(boolean pendingApp) {
		this.pendingApp = pendingApp;
	}
	
}
