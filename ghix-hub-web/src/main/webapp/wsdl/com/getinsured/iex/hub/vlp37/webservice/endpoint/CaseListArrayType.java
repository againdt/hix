
package com.getinsured.iex.hub.vlp37.webservice.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseListArrayType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseListArrayType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlpdvc.ee.sim.dsh.cms.hhs.gov}CaseList" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseListArrayType", propOrder = {
    "caseList"
})
public class CaseListArrayType {

    @XmlElement(name = "CaseList", required = true)
    protected List<CaseListType> caseList;

    /**
     * Gets the value of the caseList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caseList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaseList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaseListType }
     * 
     * 
     */
    public List<CaseListType> getCaseList() {
        if (caseList == null) {
            caseList = new ArrayList<CaseListType>();
        }
        return this.caseList;
    }

}
