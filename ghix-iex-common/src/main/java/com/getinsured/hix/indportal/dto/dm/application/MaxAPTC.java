package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;

public class MaxAPTC {
	 private BigDecimal ptcFplPercentage;
	 private BigDecimal taxHouseholdAnnualIncomeAmount;
	 private BigDecimal maxAPTCAmount;
	 private BigDecimal slcspPremiumAmount;
	 private String slcspIdentifier;


	 // Getter Methods 

	 public BigDecimal getPtcFplPercentage() {
	  return ptcFplPercentage;
	 }

	 public BigDecimal getTaxHouseholdAnnualIncomeAmount() {
	  return taxHouseholdAnnualIncomeAmount;
	 }

	 public BigDecimal getMaxAPTCAmount() {
	  return maxAPTCAmount;
	 }

	 public BigDecimal getSlcspPremiumAmount() {
	  return slcspPremiumAmount;
	 }

	 public String getSlcspIdentifier() {
	  return slcspIdentifier;
	 }

	 // Setter Methods 

	 public void setPtcFplPercentage(BigDecimal ptcFplPercentage) {
	  this.ptcFplPercentage = ptcFplPercentage;
	 }

	 public void setTaxHouseholdAnnualIncomeAmount(BigDecimal taxHouseholdAnnualIncomeAmount) {
	  this.taxHouseholdAnnualIncomeAmount = taxHouseholdAnnualIncomeAmount;
	 }

	 public void setMaxAPTCAmount(BigDecimal maxAPTCAmount) {
	  this.maxAPTCAmount = maxAPTCAmount;
	 }

	 public void setSlcspPremiumAmount(BigDecimal slcspPremiumAmount) {
	  this.slcspPremiumAmount = slcspPremiumAmount;
	 }

	 public void setSlcspIdentifier(String slcspIdentifier) {
	  this.slcspIdentifier = slcspIdentifier;
	 }
	}
