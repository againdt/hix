package com.getinsured.eligibility;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.disenrollment.service.ReferralDisenrollmentService;
import com.getinsured.eligibility.at.ref.service.EntireHHLostEligibilityAndDisenrollHandler;
import com.getinsured.eligibility.at.ref.service.ReferralNoticeRequest;
import com.getinsured.eligibility.at.ref.service.ReferralNoticeResponse;
import com.getinsured.eligibility.at.ref.service.ReferralNotificationService;
import com.getinsured.eligibility.at.resp.service.EligibilityService;
import com.getinsured.eligibility.at.service.AccountTransferService;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentRequest;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentService;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.util.ReferralConstants;

@Controller
@RequestMapping("/eligibility")
public class EligibilityController {

	// private static final Logger LOGGER = Logger.getLogger(EligibilityController.class);

	@Autowired
	private EligibilityService eligibilityService;

	@Autowired
	private BenchmarkPlanService benchmarkPlanService;

	@Autowired
	private MoveEnrollmentService moveEnrollmentService;

	@Autowired
	private PlanAvailabilityAdapter planAvailabilityAdapter;

	@Autowired
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;

	@Autowired
	private ActiveEnrollmentService activeEnrollmentService;

	@Autowired
	private AccountTransferService accountTransferService;

	@Autowired
	private ReferralNotificationService referralNotificationService;
	
	@Autowired
	private EntireHHLostEligibilityAndDisenrollHandler entireHHLostEligibilityAndDisenroll;
	
	@Autowired
	@Qualifier("referralDisenrollmentService")
	private ReferralDisenrollmentService referralDisenrollmentService;

	/**
	 * Returns Eligibilities with TRUE indicator for all covered members in household for a given caseNumber.
	 * 
	 * 
	 * @param caseNumber
	 * @return ResponseEntity<SsapApplicationEligibilityDTO>
	 */
	@RequestMapping(value = "/application/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<SsapApplicationEligibilityDTO> getApplicationData(@PathVariable("caseNumber") String caseNumber) {
		SsapApplicationEligibilityDTO result = eligibilityService.getApplicationData(caseNumber);
		return new ResponseEntity<SsapApplicationEligibilityDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/data/application/{applicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<SsapApplicationEligibilityDTO> getApplicationData(@PathVariable("applicationId") Long applicationId) {
		SsapApplicationEligibilityDTO result = eligibilityService.getApplicationDataApplicationId(applicationId);
		return new ResponseEntity<SsapApplicationEligibilityDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/moveenrollment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> moveEnrollment(@RequestBody MoveEnrollmentRequest moveEnrollmentRequest) {
		String response = moveEnrollmentService.process(moveEnrollmentRequest);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/enrollment/status/{applicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> enrollmentStatus(@PathVariable("applicationId") Long applicationId) {

		boolean response = activeEnrollmentService.isEnrollmentActive(applicationId, ReferralConstants.EXADMIN_USERNAME);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/retrigger/accounttransfer/{giWsPayloadId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> reTriggerAccountTransfer(@PathVariable("giWsPayloadId") Integer giWsPayloadId) {

		AccountTransferRequestPayloadType response = accountTransferService.prepareForReTrigger(giWsPayloadId);
		if (response == null) {
			return new ResponseEntity<String>("Unable to prepare to re-trigger account transfer for giWsPayloadId - " + giWsPayloadId, HttpStatus.OK);
		}
		accountTransferService.process(response, giWsPayloadId);
		return new ResponseEntity<String>("Successfully re-trigger account transfer for giWsPayloadId - " + giWsPayloadId, HttpStatus.OK);
	}

	@RequestMapping(value = "/retrigger/notification/", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ReferralNoticeResponse> retriggerReferralNotice(@RequestBody ReferralNoticeRequest referralNoticeRequest) {
		ReferralNoticeResponse response = referralNotificationService.retriggerNotice(referralNoticeRequest);

		return new ResponseEntity<ReferralNoticeResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/plan/application/", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<PlanAvailabilityAdapterResponse> getPlans(@RequestBody PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		PlanAvailabilityAdapterResponse response = planAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
		return new ResponseEntity<PlanAvailabilityAdapterResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/autoenroll/application/", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> autoEnroll(@RequestBody PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = planAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
		String response = enrollmentUpdateAdapter.execute(planAvailabilityAdapterRequest.getSsapApplicationId(), null, null, planAvailabilityAdapterResponse);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/batch/disenroll", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> batchDisenroll(@RequestBody String[] arguments) {

		String response = entireHHLostEligibilityAndDisenroll.batchDisenroll(arguments);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/disenroll/healthordental", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> disenrollHealthOrDentalOnly(@RequestBody String[] arguments) {
		String response = referralDisenrollmentService.disenrollHealthOrDentalOnly(arguments);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	
	
	

}