<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>

<!--remittance report-->
<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
	<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.back"/></a></li>
	<li class="active">Remittance Report</li>
	</ul>
			<h1>View Remittance Report <a href="#" rel="tooltip" data-placement="top"
			data-original-title="You can view payment details of payment done to Issuer." 
			class="info"><i class="icon-question-sign"></i></a></h1>

</div>

<div class="row-fluid">
	<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">Refine Results</h4>
			</div>

					<form class="form-vertical gutter10" id="frmremittancefilter"
			name="frmremittancefilter" action="remittancereport" method="POST">
			<df:csrfToken/>
            <input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
            
            			<div class="control-group">
            				<label class="control-label required" for="carrierName" style="text-align: left">Issuer Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
            				<div class="controls">
								<input class="span12" placeholder="Issuer Name" name="carrierName" id="carrierName" value="${searchCriteria.carrierName}">
							</div>
							<div id="carrierName_error"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="startdate" style="text-align: left">Start Date <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<div class="input-append date date-picker" id="date" data-date="">
									<input class="span10" type="text" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
								<%-- <input title="MM/DD/YYYY" class="datepick input-small"   name="startdate" id="mystartdate" value="${searchCriteria.startdate}" onChange="setdate();"/> --%>
								<div id="mystartdate_error"></div>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label required" for="enddate" style="text-align: left">End Date <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<%-- <input title="MM/DD/YYYY" class="datepick input-small" name="enddate" id="myenddate" value="${searchCriteria.enddate}"  onChange="setdate();"/> --%>
								<div class="input-append date date-picker" id="date" data-date="">
									<input class="span10" type="text" name="enddate" id="myenddate" value="${searchCriteria.enddate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
								<div id="myenddate_error"></div>
							</div>
						</div>
                       	
                       	<div class="txt-center">
							<input type="submit" name="submitBtn" id="submitBtn" Value="Go" title="Go" class="btn input-medium">
						</div>
					</form>
		
	</div>

<!--Scrollable remittance report table-->
	<div class="span9" id="rightpanel">
		<form id="frmremittancelist"
			name="frmremittancelist" class="tableScroll" action="remittancereport" method="POST">
			<%-- <input type="hidden" value="${employerid}" name="id" id="id" /> --%>
			<input type="hidden" value="" name="startdate" id="startdate" />
		    <input type="hidden" value="" name="enddate" id="enddate" />
			<c:choose>
			 <c:when test="${not empty ErrorMsg}">
                <h4 class="alert alert-error h4">${ErrorMsg}</h4>
                </c:when>
			<c:when test="${searchCriteria.carrierName != null}">
				<c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(remittanceList) > 0}">
								<table class="table table924 table-striped">
									<thead>
										<tr class="graydrkbg ">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<%-- <th class="sortable"><dl:sort title="Invoice Number" sortBy="invoiceNumber"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<th style="width:100px" class="sortable"  scope="col"><dl:sort title="Issuer Name" sortBy="carrierName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th  style="width:62px" class="sortable" class="width48" scope="col"><dl:sort title="Plan Type" sortBy="planType"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:70px" class="sortable" scope="col"><dl:sort title="Statement Date" sortBy="statementDate"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:85px" class="sortable" scope="col"><dl:sort title="Employer Name" sortBy="employerName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:62px" class="sortable" scope="col"><dl:sort title="Employee Name" sortBy="employeeName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>		
											<th style="width:60px" class="sortable" scope="col"><dl:sort title="Net Amount" sortBy="netAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:52px" class="sortable" scope="col"><dl:sort title="Paid Amount" sortBy="amountPaidToIssuer"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<%-- <th class="sortable" scope="col"><dl:sort title="Exchange Fees" sortBy="userFee"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<%-- <th class="sortable" scope="col"><dl:sort title="Due Amount" sortBy="dueAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<th style="width:40px" class="sortable" scope="col"><dl:sort title="Status" sortBy="status"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:94px" class="sortable" scope="col"><dl:sort title="Transaction Id" sortBy="transactionId"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:85px" class="sortable" scope="col"><dl:sort title="Remittance Date" sortBy="paymentDate"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th style="width:40px" class="sortable" scope="col"><dl:sort title="Type" sortBy="paymentType"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<%-- <th class="sortable" scope="col"><dl:sort title="Account Number" sortBy="accountNumber"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
										</tr>
									</thead>
									
									<c:forEach items="${remittanceList}" var="remittance" varStatus="vs">
									<tr>
											<!-- <td><input type="checkbox"> </td> -->
											<%-- <td>${remittance.invoiceNumber}</td> --%>
											<td>${remittance.carrierName}</td>
											<td>${remittance.planType}</td>
											<td><fmt:formatDate
													value="${remittance.statementDate}"
													pattern="MM-dd-yyyy" /></td>
											<td>${remittance.employerName}</td>
											<td>${remittance.employeeName}</td>
											<td><fmt:setLocale value="en_US" scope="session"/><fmt:formatNumber value="${remittance.netAmount}" type="currency" /></td>
											<td><fmt:setLocale value="en_US" scope="session"/><fmt:formatNumber value="${remittance.amountPaidToIssuer}" type="currency" /></td>
											<%-- <td>$<fmt:formatNumber value="${remittance.userFee}" type="number"  maxFractionDigits="2"/></td> --%>
											<%-- <td>$<fmt:formatNumber value="${remittance.dueAmount}" type="number"  maxFractionDigits="2"/></td> --%>
											
											<td>
											<c:choose>
											<c:when test="${remittance.status =='PARTIALLY_PAID'}">PARTIALLY PAID
											</c:when>
											<c:otherwise>${remittance.status}</c:otherwise></c:choose>
											</td>
											<td>${remittance.transactionId}</td>
											<td><fmt:formatDate
													value="${remittance.paymentDate}"
													pattern="MM-dd-yyyy" /></td>
											<td>${remittance.paymentType}</td>
											<%-- <td>${remittance.accountNumber}</td> --%>
										</tr>				
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
				</c:choose>
				</c:when>
				<c:otherwise>
				<h4 class="alert alert-info">
				To view this report, Please add filter from refine results.
				</h4>
				</c:otherwise>
			</c:choose>
		</form>
		<div class="center">
		<c:if test="${fn:length(remittanceList) gt 0}">
         <button class="btn" data-dismiss="modal" aria-hidden="true" id="downloadRemittanceReport">Download</button>
        </c:if>
		</div>
	</div>
</div>
</div>


<div class="row-fluid">
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->

<script type="text/javascript">


var validator = $("#frmremittancefilter")
.validate(
		{
			rules : {
				'carrierName' : {
					required : true
				},
				'startdate' : {
					required : true
				},
				'enddate' : {
					required: true,
					dateCheck: true
				}
			},
			messages : {
				'carrierName' : {
					required : "<span> <em class='excl'>!</em>Please provide Issuer Name.</span>"
				},
				'startdate' : {
					required : "<span> <em class='excl'>!</em>Please provide Start Date.</span>"
				},
				'enddate' : {
					required : "<span> <em class='excl'>!</em>Please provide End Date.</span>",
					dateCheck: "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"
				}
			},
			highlight: function (element, errorClass) {
				$('#myenddate').removeAttr('error');
			},
				errorClass : "error",
				errorPlacement : function(error, element) {
					var elementId = element.attr('id');
					error.appendTo($("#" + elementId + "_error"));
					$("#" + elementId + "_error").attr('class', 'error');
				}
			});
jQuery.validator.addMethod("dateCheck", function(value, element,param) {
	
	var startdatevalue = $('#mystartdate').val(),
	regExp = (/(\d{1,2})\-(\d{1,2})\-(\d{2,4})/);

	//console.log('startdatevalue',startdatevalue)
	if (value == null || value.length <1)
		return true;
	//console.log(parseInt(value.replace(regExp, "$3$2$1")) , parseInt(startdatevalue.replace(regExp, "$3$2$1")))
	return (parseInt(value.replace(regExp, "$3$1$2")) > parseInt(startdatevalue.replace(regExp, "$3$1$2")));
});


$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});
		
// for download handler
$('#downloadRemittanceReport').click(function(){
	var params ="startdate="+$("#mystartdate").val() +"&"+"enddate="+$("#myenddate").val()+"&"+"carrierName="+$("#carrierName").val();
	window.location.href =  "${pageContext.request.contextPath}"+'/admin/downloadRemittanceReport'+"?"+params;
});
	
function setdate(){
	$("#startdate").val($("#mystartdate").val());
	$("#enddate").val($("#myenddate").val());
}

$('.info').tooltip();
</script>
