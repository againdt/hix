//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a
 * 				Extended NON QHP Attributes for Applicant of Application.
 * 			
 * 
 * <p>Java class for ExtendedApplicantNonQHPType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedApplicantNonQHPType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ExtendedApplicantNonQHPCode"/>
 *         &lt;element name="ExtendedApplicantNonQHPDate" type="{http://niem.gov/niem/niem-core/2.0}DateType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedApplicantNonQHPType", propOrder = {
    "extendedApplicantNonQHPCode",
    "extendedApplicantNonQHPDate"
})
public class ExtendedApplicantNonQHPType
    extends ComplexObjectType
{

    @XmlElement(name = "ExtendedApplicantNonQHPCode", required = true)
    protected ExtendedApplicantNonQHPCodeType extendedApplicantNonQHPCode;
    @XmlElement(name = "ExtendedApplicantNonQHPDate", required = true)
    protected DateType extendedApplicantNonQHPDate;

    /**
     * Gets the value of the extendedApplicantNonQHPCode property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedApplicantNonQHPCodeType }
     *     
     */
    public ExtendedApplicantNonQHPCodeType getExtendedApplicantNonQHPCode() {
        return extendedApplicantNonQHPCode;
    }

    /**
     * Sets the value of the extendedApplicantNonQHPCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedApplicantNonQHPCodeType }
     *     
     */
    public void setExtendedApplicantNonQHPCode(ExtendedApplicantNonQHPCodeType value) {
        this.extendedApplicantNonQHPCode = value;
    }

    /**
     * Gets the value of the extendedApplicantNonQHPDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getExtendedApplicantNonQHPDate() {
        return extendedApplicantNonQHPDate;
    }

    /**
     * Sets the value of the extendedApplicantNonQHPDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setExtendedApplicantNonQHPDate(DateType value) {
        this.extendedApplicantNonQHPDate = value;
    }

}
