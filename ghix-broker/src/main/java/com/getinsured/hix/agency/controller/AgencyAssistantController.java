package com.getinsured.hix.agency.controller;


import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.agency.service.AgencyAssistantService;
import com.getinsured.hix.agency.service.AgencyService;
import com.getinsured.hix.agency.service.AgencySiteService;
import com.getinsured.hix.agency.util.AgencyDtoMapper;
import com.getinsured.hix.dto.agency.AgencyAssistantApprovalDto;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyAssistantListDto;
import com.getinsured.hix.dto.agency.AgencyAssistantSearchDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusDto;
import com.getinsured.hix.dto.agency.AgencyResponse;
import com.getinsured.hix.dto.agency.AgencySiteLocationDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Controller
@RequestMapping(value="/staff")
public class AgencyAssistantController {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AgencyController.class);
	
	@Autowired
	private AgencyAssistantService assistantService;
	
	@Autowired
	private AgencyDtoMapper agencyDtoMapper;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private AgencySiteService agencySiteService;
	
	@Autowired
	AgencyService agencyService;
	
	@Autowired
	private UserService userService;
	
	@ResponseBody
	@RequestMapping(value={"/information/view/{agencyId}", "/information/view/{agencyId}/{assistantId}"},method=RequestMethod.GET)
	public AgencyAssistantInfoDto viewAssistantInformation(@PathVariable("agencyId") String agencyId, @PathVariable("assistantId") Optional<String> assistantId){
		AgencyAssistant assistant = null;
		AgencyAssistantInfoDto agencyAssistantInfoDto = null;
		List<AgencySiteLocationDto> agencySiteDtos = null;
		List<AgencySite> agencySites = null;
		try {
			if(assistantId.isPresent() && !assistantId.get().isEmpty()){
				assistant = assistantService.findById(Long.parseLong(assistantId.get()));
				agencyAssistantInfoDto = assistantService.getAgencySitesForAssistant(assistant);
			}else{
				agencyAssistantInfoDto = new AgencyAssistantInfoDto();
				if(agencyId != null && !agencyId.isEmpty()){
					agencySites = agencySiteService.getSiteByAgencyId(Long.valueOf(agencyId));
					agencySiteDtos = agencyDtoMapper.convertCollection(AgencySiteLocationDto.class, agencySites);
					Agency agency = agencyService.findById(Long.parseLong(agencyId));
					agencyAssistantInfoDto.setBusinessLegalName(agency.getBusinessLegalName());
					agencyAssistantInfoDto.setAgencySites(agencySiteDtos);
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			throw new GIRuntimeException("Exception occured while processing staff information response.",ex);
		}
		
		return agencyAssistantInfoDto;
	}
	
	@ResponseBody
	@RequestMapping(value={"/information/{assistantId}"},method=RequestMethod.GET)
	public AgencyAssistantInfoDto loadAssistantInformation(@PathVariable("assistantId") String assistantId){
		AgencyAssistant assistant = null;
		AgencyAssistantInfoDto agencyAssistantInfoDto = null;
		try {
			if(StringUtils.isNotEmpty(assistantId)){
				assistant = assistantService.findById(Long.parseLong(assistantId));
				agencyAssistantInfoDto = assistantService.getAgencySitesForAssistant(assistant);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			throw new GIRuntimeException("Exception occured while processing staff information response.",ex);
		}
		
		return agencyAssistantInfoDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/information",method=RequestMethod.POST)
	public AgencyResponse updateStaffInformation(@RequestBody AgencyAssistantInfoDto agencyAssistantInfoDto){
		AgencyAssistant assistant = null;
		AgencyResponse agencyResponse = null;

		try {
			assistant = agencyDtoMapper.convert(AgencyAssistant.class,agencyAssistantInfoDto);
			
			assistant = assistantService.saveAssistantInformation(assistant, agencyAssistantInfoDto);
			
			if(assistant != null){
				agencyResponse = buildSuccessResponse(String.valueOf(assistant.getId()));
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			agencyResponse = buildFailureResponse();
		}
		return agencyResponse; 
	}
	
	@ResponseBody
	@RequestMapping(value="/linkusertostaff",method=RequestMethod.POST)
	public AgencyResponse linkUserToStaff(@RequestBody AgencyAssistantInfoDto agencyAssistantInfoDto){
		AgencyAssistant assistant = null;
		AgencyResponse agencyResponse = null;
		AccountUser user = null;
		String decryptedStaffId = null;

		try {
			user = userService.getLoggedInUser();
			if(agencyAssistantInfoDto.getId()!=null){
				decryptedStaffId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getId());
				if(decryptedStaffId!=null && !decryptedStaffId.isEmpty()){
					assistant = assistantService.saveUserForAgencyAssistant(Long.valueOf(decryptedStaffId), user);
				}
			}
			
			if(assistant != null){
				agencyResponse = buildSuccessResponse(String.valueOf(assistant.getId()));
			}else{
				agencyResponse = buildFailureResponse();
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			agencyResponse = buildFailureResponse();
		}
		return agencyResponse; 
	}
	
	@ResponseBody
	@RequestMapping(value="/getstaffbypersonalemail",method=RequestMethod.GET)
	public AgencyResponse getStaffByPersonalEmail(){
		AgencyAssistant assistant = null;
		AgencyResponse agencyResponse = null;
		AccountUser user = null;

		try {
			user = userService.getLoggedInUser();
			assistant = assistantService.findByPersonalEmailAddress(user.getEmail());
			
			if(assistant != null){
				agencyResponse = buildSuccessResponse(String.valueOf(assistant.getId()));
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			agencyResponse = buildFailureResponse();
		}
		return agencyResponse; 
	}
	
	@ResponseBody
	@RequestMapping(value={"/getstaffbyuser/{userId}"},method=RequestMethod.GET)
	public AgencyAssistantInfoDto getAssistantForUser(@PathVariable("userId") String userId){
		AgencyAssistant assistant = null;
		AgencyAssistantInfoDto agencyAssistantInfoDto = null;
		
		try {
			if(StringUtils.isNotEmpty(userId)){
				assistant = assistantService.findByUserId(Integer.parseInt(userId));
				agencyAssistantInfoDto = assistantService.getAgencySitesForAssistant(assistant);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing staff information response.",ex);
			throw new GIRuntimeException("Exception occured while processing staff information response.",ex);
		}
		
		return agencyAssistantInfoDto;
	}
	
	private AgencyResponse buildFailureResponse() {
		AgencyResponse agencyResponse = new AgencyResponse();
		agencyResponse.setStatusCode("500");
		agencyResponse.setStatusMessage("FAILURE");
		return agencyResponse;
	}
	
	private AgencyResponse buildSuccessResponse(String id) {
		AgencyResponse agencyResponse = new AgencyResponse();
		agencyResponse.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(id));
		agencyResponse.setStatusCode("200");
		agencyResponse.setStatusMessage("SUCCESS");
		return agencyResponse;
	}

	@ResponseBody
	@RequestMapping(value="/agencyassistantlistforadmin",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyAssistantListDto findAgencyAssistantsForAdmin(@RequestBody AgencyAssistantSearchDto agencyAssistantSearchDto){
		AgencyAssistantListDto agencyAssistantListDto = null;
		
		try {
			if(StringUtils.isEmpty(agencyAssistantSearchDto.getSortOrder())){
				agencyAssistantSearchDto.setSortOrder("ASC");
		    } 
			agencyAssistantListDto = assistantService.findAgencyAssistantsForAdmin(agencyAssistantSearchDto);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while fetching agency assistant list.", exception);
			throw new GIRuntimeException("Exception occured while fetching agency assistant list.", exception);
		}
		
		return agencyAssistantListDto; 
	}
	
	@ResponseBody
	@RequestMapping(value="/agencyassistantlistforagency",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyAssistantListDto findAgencyAssistantsForAgency(@RequestBody AgencyAssistantSearchDto agencyAssistantSearchDto){
		AgencyAssistantListDto agencyAssistantListDto = null;
		
		try {
			if(StringUtils.isEmpty(agencyAssistantSearchDto.getSortOrder())){
				agencyAssistantSearchDto.setSortOrder("ASC");
		    }
			agencyAssistantListDto = assistantService.findAgencyAssistantsForAgency(agencyAssistantSearchDto);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while fetching agency assistant list.", exception);
			throw new GIRuntimeException("Exception occured while fetching agency assistant list.", exception);
		}
		
		return agencyAssistantListDto; 
	}
	
	@ResponseBody
	@RequestMapping(value={"/statushistory/{assistantId}"},method=RequestMethod.GET)
	public AgencyAssistantStatusDto viewActivityStatusHistory(@PathVariable("assistantId") String assistantId){
		return assistantService.loadAgencyAssistantStatusHistory(Long.valueOf(assistantId));
	}
	
	@ResponseBody
	@RequestMapping(value={"/approvalhistory/{agencyAssistantId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyAssistantApprovalDto getAgencyAssistantApprovalHistory(@PathVariable("agencyAssistantId") String agencyAssistantId){
		return assistantService.loadAgencyAssistantApprovalHistory(Long.valueOf(agencyAssistantId));
	}
	
	@ResponseBody
	@RequestMapping(value="/status",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyAssistantStatusDto updateAssitantActivityStatus(@RequestBody AgencyAssistantStatusDto agencyAssistantStatusDto){
		AgencyAssistantStatusDto assistantStatusResponse = null;
		AgencyAssistant assistant = null;
		try {
			assistant = assistantService.updateActivityStatus(agencyAssistantStatusDto);
			if(assistant != null){
				assistantStatusResponse = assistantService.loadAgencyAssistantStatusHistory(assistant.getId());
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured while updating agency assistant status.", exception);
			throw new GIRuntimeException("Exception occured while updating agency assistant status.", exception);
		}
		
		return assistantStatusResponse; 
	}
	
	@ResponseBody
	@RequestMapping(value="/approvalstatus",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyAssistantApprovalDto updateAssitantApprovalStatus(@RequestBody AgencyAssistantApprovalDto agencyAssistantApprovalDto){
		AgencyAssistantApprovalDto assistantApprovalDto = null;
		AgencyAssistant assistant = null;
		try {
			assistant = assistantService.updateApprovalStatus(agencyAssistantApprovalDto);
			if(assistant != null){
				assistantApprovalDto = assistantService.loadAgencyAssistantApprovalHistory(Long.valueOf(assistant.getId()));
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured while updating agency assistant approval status.", exception);
			throw new GIRuntimeException("Exception occured while updating agency assistant approval status.", exception);
		}
		
		return assistantApprovalDto; 
	}
}
