package com.getinsured.hix.batch.notification.ssap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.util.GhixEndPoints.EligibilityEndPoints;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Service("UCAQueuedNoticeProcessor")
@Transactional
public class UCAQueuedNoticeProcessor implements QueuedNoticeProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(UCAQueuedNoticeProcessor.class);
    private static final long MAX_CREATION_TIMESTAMP = 60;
    private static final String UNCLAIMED_APPLICATION_STATUS = "UC";

    @Autowired
    private CmrHouseholdRepository cmrHouseholdRepository;

    @Autowired
    private SsapApplicationRepository ssapApplicationRepository;
    
    @Autowired RestTemplate restTemplate;

    @Override
    public boolean verify(NoticeQueued noticeQueued) {
        try {
            SsapApplication ssapApplication = ssapApplicationRepository.findOne(noticeQueued.getColumnValue());
            if(ssapApplication != null) {
                Household household = cmrHouseholdRepository.findOne(ssapApplication.getCmrHouseoldId().intValue());
                if (household == null || household.getUser() != null) {
                    LOGGER.debug("verify::ssapApplication already has a household attached");
                    return false;
                } else if (!ssapApplication.getApplicationStatus().equals(UNCLAIMED_APPLICATION_STATUS)) {
                    // Status of SSAP not equal to UC
                    LOGGER.debug("verify::ssapApplication status not equal to UC: {}", ssapApplication.getApplicationStatus());
                    return false;
                }
            } else {
                LOGGER.debug("verify::ssapApplication null");
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("verify::error processing notice queued", ex);
            return false;
        }

        return true;
    }

    @Override
    public boolean process(NoticeQueued noticeQueued) {
    	try {
            if(noticeQueued != null && noticeQueued.getColumnValue() != null) {
                LOGGER.debug("process::initiating activation for id {} from column name {} table name {}",
                        noticeQueued.getColumnValue(), noticeQueued.getColumnName(), noticeQueued.getColumnName());
                
                restTemplate.getForObject(EligibilityEndPoints.INITIATE_ACCOUNT_ACTIVATION_EMAIL+"/"+noticeQueued.getColumnValue(), String.class);
            }
        } catch (Exception ex) {
            LOGGER.error("processQueuedNotice::error processing notice queued", ex);
            return false;
        }

        LOGGER.debug("process::successful processing of noticeQueued");
        return true;
    }
}
