package com.getinsured.hix.indportal.enums;

public enum DisenrollReasonEnum {
	CANNOT_AFFORD_PAYMENT("14"),
    NO_PROPER_SERVICE("14"),
    EMPLOYER_OFFERED("14"),
    LIFE_EVENT("14"),
    OTHER("14"),
	PHY_OUT_OF_NETWORK("14"),
	AGENT_ERROR("14"),
	NO_OPTION_SELECTED("14");
	
	private String  reasonCd;
	
	private DisenrollReasonEnum(String reasonCd)
    {
    	this.reasonCd = reasonCd;
    }
	public String value() {
		return reasonCd;
	}
	
	
	public static String value(String v) {
		if (v != null) {
			final DisenrollReasonEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}
	private static DisenrollReasonEnum readValue(String v) {
		DisenrollReasonEnum data = null;
		for (DisenrollReasonEnum c : DisenrollReasonEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
}
