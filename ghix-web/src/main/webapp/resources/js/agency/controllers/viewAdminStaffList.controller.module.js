(function() {
'use strict';

	angular.module('viewAdminStaffCtrlModule', ['translateApp'])
	.controller('ViewAdminStaffCtrl', ViewAdminStaffCtrl);

	ViewAdminStaffCtrl.$inject = ['$http', '$stateParams', '$scope', '$state', 'dataStorageService', '$translate', 'StatusService'];

	function ViewAdminStaffCtrl($http, $stateParams, $scope, $state, dataStorageService, $translate, StatusService) {
		var vm = this;


		vm.init = init;
		vm.getData = getData;
		vm.getSorting = getSorting;
		vm.reset = reset;
		vm.isResultsFound = true;
		vm.sortResult = false;
		vm.agencyCertificationStatus = StatusService.CertificationStatus;
		vm.isAdminRole = $('#isAdminRole').val();
		vm.currentUserRole = StatusService.CurrentUserRole;
		vm.approvalStatusList = ["Pending", "Approved", "Eligible", "Denied", "Terminated", "Terminated-For-Cause"];

		/** Add / Remove Active Class from Menu bar **/
        $("a[title = 'Staff']").parent().addClass('active');
        if($("a[title = 'Agencies']").parent().hasClass('active')){
        	$("a[title = 'Agencies']").parent().removeClass('active');
        }
		if($("a[title = 'Agents']").parent().hasClass('active')){
        	$("a[title = 'Agents']").parent().removeClass('active');
        }

		function init(){
			if ((vm.agencyCertificationStatus === 'PENDING' || vm.agencyCertificationStatus === 'SUSPENDED'  || vm.agencyCertificationStatus === 'TERMINATED') && vm.currentUserRole === 'AGENCY_MANAGER') {
                $state.go("viewagentlist");
            }
			vm.agencyId =  $("#agencyId").val() || dataStorageService.getItem('agencyId');
			vm.paginationData = {
				totalRecords: 0
			}
			vm.inputData = {
				agencyId : null || vm.agencyId,
				firstName : '',
				lastName : '',
				businessLegalName: '',
				staffId: '',
				approvalStatus : '',
				pageNumber: 1,
				sortBy: undefined,
				sortOrder: 'ASC',
			}
			vm.filterData = {};
			getData();
		}

		function getData() {
			vm.loader = true;
			if(vm.refineResult || vm.paginationResult){
				vm.filterData = {};
				var properties = ['firstName', 'lastName', 'businessLegalName', 'staffId', 'approvalStatus'];
				for (var i=0; i<properties.length; i++) {
					if(vm.inputData[properties[i]] !== '' && vm.inputData[properties[i]] !== undefined) {
						vm.filterData[properties[i]] = vm.inputData[properties[i]];
					}
				}
			}
			if((vm.isAdminRole === '' || vm.isAdminRole === undefined) && vm.currentUserRole === 'AGENCY_MANAGER') {
				vm.inputData.agencyId = vm.agencyId;
			} else {
				vm.inputData.agencyId = undefined;
			}
			if(vm.inputData.agencyId !== undefined && vm.inputData.agencyId !== '') {
				vm.filterData.agencyId = vm.inputData.agencyId;
			}

			if(vm.refineResult) {
				vm.filterData.pageNumber = 1;
				vm.inputData.pageNumber = 1;
			}
			if (vm.paginationResult) {
				vm.filterData.pageNumber = vm.inputData.pageNumber;
			} else {
				vm.filterData.pageNumber = 1;
			}
			if(vm.inputData.sortOrder && vm.inputData.sortOrder !== undefined) {
				vm.filterData.sortOrder = vm.inputData.sortOrder;
			}
			if(vm.inputData.sortBy && vm.inputData.sortBy !== undefined) {
				vm.filterData.sortBy = vm.inputData.sortBy;
			}
			if((vm.isAdminRole === '' || vm.isAdminRole === undefined) && vm.currentUserRole === 'AGENCY_MANAGER') {
				var promise = $http.post('/hix/agency/staff/agencyassistantlistforagency', vm.filterData);
			} else {
				var promise = $http.post('/hix/agency/staff/agencyassistantlistforadmin', vm.filterData);
			}

			promise.success(function(response){
				vm.loader = false;
				vm.adminStaffList = response;
				vm.refineResult = false;
				vm.paginationResult = false;
				if (response.count > 0) {
					vm.isResultsFound = true;
					vm.paginationData.totalRecords = response.count;
				} else {
					vm.isResultsFound = false;
				}
			})
			promise.error(function(response){
				vm.loader = false;
				vm.refineResult = false;
			})
		}

		function getSorting(column){
			vm.inputData.sortBy = column;
			if(vm.inputData.sortOrder === undefined || vm.inputData.sortOrder === null || vm.inputData.sortOrder === 'DESC'){
				vm.inputData.sortOrder = 'ASC';
			}else{
				vm.inputData.sortOrder = 'DESC';
			}
			getData();
		}

		function reset() {
			vm.inputData = {};
			vm.filterData = {};
			vm.refineResult = true;
			getData();
		}
    }

})();
