package com.getinsured.iex.hub.ssac;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;

public final class VerifySSAC {
	
	private VerifySSAC(){
		
	}
	
	public static void main(String[] args) throws HubMappingException, BridgeException, HubServiceException, JAXBException, InputDataValidationException {
		
		HubServiceBridge ssacServiceBridge = HubServiceBridge
				.getHubServiceBridge("VerifySSAComposite");
		
		ssacServiceBridge.getServiceHandler().setJsonInput("{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"George\",\"middleName\":\"B\",\"lastName\":\"Prince\"},\"dateOfBirth\":\"Feb 3, 1950 10:00:00 CM\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"221-21-2503\"},\"citizenshipImmigrationStatus\":{\"citizenshipAsAttestedIndicator\":true},\"incarcerationStatus\":{\"incarcerationAsAttestedIndicator\":true}}]}]}");
		Object payloadElement = ssacServiceBridge.getServiceHandler().getRequestpayLoad();
		
		if(payloadElement != null){
			JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.ssac.exchange");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new SSACNamespacePrefixMatcher());
			m.marshal(payloadElement, System.out);
		}

	}
	
}	
