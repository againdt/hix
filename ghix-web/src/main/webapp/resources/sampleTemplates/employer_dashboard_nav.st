<div class="gutter10-l">
	<div class="well-step">
		<div  id="step-progress">        
			<div id="progress-bar">
				<ul>
					    $navDataList: {navData|
					        <li style="$navData.STEP_STYLE$" class="step-desc $navData.STEP_CLASS$">
							    	<span class="s-no">$i$</span><span class="step-text" alt="$navData.ALT_TEXT$" aria-label="$navData.ARIA_LABEL$">$navData.LABEL_TEXT$</span>
							</li>
					    }$
				</ul>           
			</div> <!--/End Progress-bar-->
		</div><!--/End step-progress-->
		
		<div class="blurb clearfix" id="stepdesc">
						<h4 class="margin0">What&#039;s Next?</h4>
						<div class="gutter10-lr">
							<div class="span8" id="complete-oep">
								<p>$currentNavData.STEP_DESC$</p>             
							</div>						  
							$if(currentNavData.BUTTON_LABEL)$
								<div class="span4" id="enroll" style="$currentNavData.BUTTON_STYLE$">
								   <button onClick="gotoNext('$navStep$')" class="btn btn-primary pull-right">$currentNavData.BUTTON_LABEL$</button> 
								</div>
							<br>
							$else$
							<div style="float:left;">
								<label class="checkbox"> 
								<form name="frmdashboard" id="frmdashboard" action="dashboard" method="POST">
									<input type="checkbox" value="1" onClick="this.form.submit();" class="terms" name="showMsgCheckbox"><span>Don't show this status message again.</span>
								</form> 
								</label>
							</div>
							$endif$
						  <!-- End -->
						</div>
		</div> <!-- End /.blurb -->   
					                        
	</div><!--End /.well-step-->
</div><!--End /.gutter10-l--> 