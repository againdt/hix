package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for first Name
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class FirstNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FirstNameValidator.class);
	
	/** 
	 * Validations for firstName
	 * String between 1-50 characters
	 * 
	 * Required : Yes
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String firstName = null;
		
		//Value is mandatory
		if(value == null){
			throw new InputDataValidationException(VLPServiceConstants.FIRST_NAME_REQUIRED_ERROR_MESSAGE);
		}
		else{
			firstName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(firstName.length() < VLPServiceConstants.FIRST_NAME_MIN_LEN
				|| firstName.length() > VLPServiceConstants.FIRST_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.FIRST_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
