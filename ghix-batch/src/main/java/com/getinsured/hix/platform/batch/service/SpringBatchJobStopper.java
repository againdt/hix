package com.getinsured.hix.platform.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.ListableJobLocator;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.StepSynchronizationManager;
import org.springframework.batch.core.step.NoSuchStepException;
import org.springframework.batch.core.step.StepLocator;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * Periodically checks job executions in the STOPPING state.
 * If step of job is stoppableTasklet, calls stoppableTasklet.stop.
 * If the stoppableTasklet did not change the state to STOPEED or one of finished states(either successfully or failed),
 * it keeps track of stopping step using StopOperationTracker.
 *
 */
@DependsOn({"jobRegistry"})
@Component
public class SpringBatchJobStopper {
    private static final Logger logger = LoggerFactory.getLogger(SpringBatchJobStopper.class);

    private final BatchJobExecutionService batchJobExecutionService;
    private final ListableJobLocator jobRegistry;
    private final JobExplorer jobExplorer;
    private final JobRepository jobRepository;
    private final StopOperationTracker stopTracker;
    private final ActiveJobMonitor activeJobMonitor;

    @Autowired
    public SpringBatchJobStopper(BatchJobExecutionService batchJobExecutionService,
                                 JobExplorer jobExplorer,
                                 ListableJobLocator jobRegistry,
                                 JobRepository jobRepository,
                                 StopOperationTracker stopTracker,
                                 ActiveJobMonitor activeJobMonitor) {
        this.batchJobExecutionService = batchJobExecutionService;
        this.jobRegistry = jobRegistry;
        this.jobExplorer = jobExplorer;
        this.jobRepository = jobRepository;
        this.stopTracker = stopTracker;
        this.activeJobMonitor = activeJobMonitor;
    }

    private void debug(String s) {
        logger.debug(s);
    }

    // TODO parameterize the schedule using fixedDelayString
    @Scheduled(fixedDelay = 10 * 1000)
    public void stopLegacySpringBatchJobs() {
        Set<JobExecution> stoppingJobs = batchJobExecutionService.findStoppingJobs();
        debug("Found " + stoppingJobs.size() + " stopping jobs");
        if (stoppingJobs != null) {
            Collection<String> jsrJobNames = getJsrJobNames();
            for (JobExecution jobExecution : stoppingJobs) {
                jobExecution = jobExplorer.getJobExecution(jobExecution.getId());
                if (jobExecution == null || jobExecution.getJobInstance() == null) {
                    debug("Can't find jobExecution with id " + jobExecution.getId());
                    continue;
                }

                if (jsrJobNames.contains(jobExecution.getJobInstance().getJobName())) {
                    debug("Skipping Jsr Job " + jobExecution.getJobInstance().getJobName());
                    continue;
                }

                try {
                    Job job = jobRegistry.getJob(jobExecution.getJobInstance().getJobName());

                    if (!(job instanceof StepLocator)) {
                        debug("Skipping non-StepLocator job " + job.toString());
                        continue;
                    }

                    long jobStopDelaySeconds = 0;
                    for (StepExecution stepExecution : jobExecution.getStepExecutions()) {
                        if (!stepExecution.getStatus().isRunning() && !stepExecution.getStatus().equals(BatchStatus.STOPPING)) {
                            debug("step is not in the stoppable state " + stepExecution.toString());
                            continue;
                        }

                        if (!stopTracker.triedStop(stepExecution)) {
                            jobStopDelaySeconds += tryStop((StepLocator) job, stepExecution);
                        }
                    }
                    if (jobStopDelaySeconds > 0) {
                        stopTracker.scheduleStop(jobExecution, jobStopDelaySeconds);
                    }
                }
                catch (NoSuchJobException e) {
                    logger.warn("Cannot find Job object",e);
                    boolean isOneStepJob = jobExecution.getStepExecutions().size() == 1;
                    jobExecution.setStatus(BatchStatus.STOPPED);
                    if (isOneStepJob)
                        jobRepository.updateExecutionContext(jobExecution);
                    jobExecution.setEndTime(new Date());
                    jobExecution.setExitStatus(ExitStatus.STOPPED);
                    if (isOneStepJob)
                        jobRepository.update(jobExecution);
                }
            }
        }
    }

    private long tryStop(StepLocator job, StepExecution stepExecution) {
        long stepStopDelaySeconds = 0;
        try {
            //have the step execution that's running -> need to 'stop' it
            Step step = job.getStep(stepExecution.getStepName());
            if (step instanceof TaskletStep) {
                Tasklet tasklet = ((TaskletStep)step).getTasklet();

                if (tasklet instanceof Proxy && GhixStoppableTasklet.class.isAssignableFrom(AopUtils.getTargetClass(tasklet))) {
                    GhixStoppableTasklet ghixStoppableTasklet = activeJobMonitor.getStoppableTasklet(stepExecution);
                    stepStopDelaySeconds = ghixStoppableTasklet.getStopDelaySeconds();
                    logger.info("stepStopDelaySeconds=" + stepStopDelaySeconds);

                    StepExecution _stepExecution = ghixStoppableTasklet.getStepExecution();
                    if (_stepExecution != null) {
                        StepSynchronizationManager.register(stepExecution);
                        _stepExecution.setTerminateOnly();
                        StepSynchronizationManager.release();
                        logger.info("Set terminationOnly: " + _stepExecution.toString());
                    }
                    else {
                        logger.info("No running steps found: " + _stepExecution.toString());
                    }
                }
            }
        }
        catch (NoSuchStepException e) {
            logger.warn("Step not found",e);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            if (stepExecution.getStatus() != BatchStatus.COMPLETED && stepExecution.getStatus().isLessThan(BatchStatus.STOPPED))  {
                stopTracker.scheduleStop(stepExecution, stepStopDelaySeconds);
            }
            return stepStopDelaySeconds;
        }
    }

    private Collection<String> getJsrJobNames() {
        Set<String> jsr352JobNames = new HashSet<String>();

        try {
            PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new org.springframework.core.io.support.PathMatchingResourcePatternResolver();
            Resource[] resources = pathMatchingResourcePatternResolver.getResources("classpath*:/META-INF/batch-jobs/**/*.xml");

            for (Resource resource : resources) {
                String jobXmlFileName = resource.getFilename();
                jsr352JobNames.add(jobXmlFileName.substring(0, jobXmlFileName.length() - 4));
            }
        } catch (IOException e) {
            logger.debug("Unable to list JSR-352 batch jobs", e);
        }

        return jsr352JobNames;
    }

}
