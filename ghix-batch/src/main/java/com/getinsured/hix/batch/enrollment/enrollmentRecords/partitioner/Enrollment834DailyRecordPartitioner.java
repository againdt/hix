package com.getinsured.hix.batch.enrollment.enrollmentRecords.partitioner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.enrollmentRecords.IssuerFileList;
import com.getinsured.hix.batch.enrollment.skip.Enrollment834Out;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("Enrollment834DailyRecordPartitioner")
@Scope("step")
public class Enrollment834DailyRecordPartitioner  implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment834DailyRecordPartitioner.class);
	private String startDateStr;
	private String endDateStr;
	private String carrierResendFlag;
	private String hiosIssuerIdList;
	private List<Integer> issuerList = null;
	private String jobName;
	private String commitInterval;
	private IEnrollmentRepository enrollmentRepository;
	private LookupService lookupService;
	private Enrollment834Out enrollment834Out;
	private JobService jobService;
	private IssuerFileList issuerFileList;
	private BatchJobExecutionService batchJobExecutionService;
	private String issuerLevelPartition;
	long jobExecutionId = -1;
	//ExecutionContext executionContext;
	//@Value("#{stepExecution}")
	private StepExecution stepExecution;
	

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(
			BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public Date getJOBLastRunDate(String jobName){
		Date lastRunDate=null;
		try{
			if(StringUtils.isNotEmpty(jobName)){
				lastRunDate= batchJobExecutionService.getLastJobRunDate(jobName, EnrollmentConstants.BATCH_JOB_STATUS);
			}

		}catch(Exception e){
			LOGGER.debug("Error in getJOBLastRunDate() ::"+e.getMessage(),e);
		}
		return lastRunDate;
	}
	
	@SuppressWarnings("unused")
	private String getIssuersFromList( List<Integer> issuerList){
		String issuers = "";
		
		if(issuerList != null){
			int issuerCount = issuerList.size();
			
			for(int i=0; i < issuerCount; i++){
				issuers += issuerList.get(i);
				
				if(i < issuerCount-1){
					issuers += ",";
				}
			}
		}
	
		return issuers;
	}
	
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();

		Date startDate=null;
		Date endDate=null;
		boolean datesProvided=false;
		
		try{
			
			if(hiosIssuerIdList != null && !hiosIssuerIdList.toString().equalsIgnoreCase("null")&& !("".equalsIgnoreCase(hiosIssuerIdList.trim()))){
				getIssuerList(hiosIssuerIdList);
			}
			
			if( (startDateStr==null && endDateStr!=null) || (startDateStr!=null &&endDateStr==null) ){
				LOGGER.error("Please provide both Start Date and End Date");
				throw new GIException("Please provide both Start Date and End Date");
			}
			
			Date lastRunDate = getJOBLastRunDate(jobName);
			
			if(startDateStr!=null && endDateStr!=null ){
				if(startDateStr.equalsIgnoreCase("null") && endDateStr.equalsIgnoreCase("null")){
					startDateStr = null;
					endDateStr = null;
				}else{
					if(DateUtil.isValidDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT) && DateUtil.isValidDate(endDateStr, EnrollmentConstants.BATCH_DATE_FORMAT)){
						if(!DateUtil.isValidDateInterval(DateUtil.StringToDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT),DateUtil.StringToDate(endDateStr,EnrollmentConstants.BATCH_DATE_FORMAT))){
							LOGGER.error(" EnrollmentXMLIndividualTask.execute : Start date is greater than End date.");
							throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						}
						datesProvided=true;
					}else{
						LOGGER.error("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
					}
				}
			 }
			
			//validate carrierResendFlag
			if(EnrollmentUtils.isNotNullAndEmpty(carrierResendFlag)){
				if(carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.RESEND_FLAG) || carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.HOLD_FLAG)){
					datesProvided=true;
					carrierResendFlag = carrierResendFlag.toUpperCase();
				}else{
					LOGGER.error("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
					throw new GIException("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
				}
			}
			
			if(startDateStr == null && endDateStr == null){
				if(carrierResendFlag != null){
					startDate= DateUtil.StringToDate(EnrollmentConstants.DEFAULT_BATCH_START_DATE, EnrollmentConstants.BATCH_DATE_FORMAT);
				}
				else if(lastRunDate!=null){
					startDate=lastRunDate;
				}else{
					startDate= DateUtil.StringToDate(EnrollmentConstants.DEFAULT_BATCH_START_DATE, EnrollmentConstants.BATCH_DATE_FORMAT);
				}
				endDate=new Date();
			}else if(startDateStr != null && endDateStr != null){
				startDate = getMaxOrMinMilliseconds(DateUtil.StringToDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT), false);
				endDate = getMaxOrMinMilliseconds(DateUtil.StringToDate(endDateStr, EnrollmentConstants.BATCH_DATE_FORMAT), true);
			}
			if(stepExecution != null){
				ExecutionContext executionContext = stepExecution.getJobExecution().getExecutionContext();
				executionContext.put("datesProvided", datesProvided);
				executionContext.put("lastRunDate",lastRunDate);
				jobExecutionId=stepExecution.getJobExecution().getId();
			}
			
			List<String> statusList=new ArrayList<>();
			statusList.add("ABORTED");
			String enrollmentType="";
			if(StringUtils.isNotEmpty(jobName)){
				if(jobName.equalsIgnoreCase(EnrollmentConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB)){
					enrollmentType=  EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL;
				}else if(jobName.equalsIgnoreCase(EnrollmentConstants.SHOP_ENROLLMENT_BATCH_JOB)){
					enrollmentType=  EnrollmentConstants.ENROLLMENT_TYPE_SHOP;
					statusList.add("PENDING");
				}
			}
			
			if(carrierResendFlag!=null && enrollmentRepository != null && issuerList == null){
				issuerList = enrollmentRepository.findIssuerByResendFlag(startDate, endDate, carrierResendFlag, enrollmentType);
			}else if(issuerList == null){
				issuerList = enrollmentRepository.findIssuerByupdateDate(startDate, endDate, enrollmentType);
			}

			if(issuerList != null && !issuerList.isEmpty()){
				if(enrollment834Out!=null){
					enrollment834Out.clearIssuerEnrollmentIdMap();
				}else{
					enrollment834Out=new Enrollment834Out();
				}
				
				if(issuerFileList!=null ){
					issuerFileList.deleteAllFiles();
					issuerFileList.clearFiles();
				}
				int maxEnrollmentsPerPartitions =5000;
				if(this.commitInterval !=null && !this.commitInterval.isEmpty()){
					maxEnrollmentsPerPartitions=Integer.valueOf(this.commitInterval.trim());
				}
				int issuerCount = issuerList.size();

				LOGGER.info("Count of issuers : " + issuerCount);

				Integer lookupValue=null;
				if(enrollmentType!=null && !enrollmentType.isEmpty()){
						lookupValue=  lookupService.getlookupValueIdByTypeANDLookupValueCode(EnrollmentConstants.LOOKUP_ENROLLMENT_TYPE,enrollmentType);
				}
				boolean multiplePartitionPerIssuer= false;
				if(issuerLevelPartition!=null){
					multiplePartitionPerIssuer=Boolean.parseBoolean(issuerLevelPartition);
				}
				for(Integer issuerId :issuerList){
					if(issuerId!=null){
						List<Integer> enrollmentIds=null;
						String batchJobStatus=null;
						if(jobService != null && jobExecutionId != -1){
							batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
						}
						if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
							issuerFileList.deleteAllFiles();
							issuerFileList.clearFiles();
							throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
						}
						
						if(carrierResendFlag != null){
							enrollmentIds=enrollmentRepository.findEnrollmentByIssuerIDAndEnrollmentTypeWIthFlag(issuerId, startDate, endDate, lookupValue, statusList, carrierResendFlag);
							
						}else{
							enrollmentIds=enrollmentRepository.findEnrollmentByIssuerIDAndEnrollmentType(issuerId, startDate,endDate,lookupValue,statusList);
						}
						
						
						if(enrollmentIds!=null && enrollmentIds.size()>0){
							enrollment834Out.putToIssuerEnrollmentIdMap(""+issuerId, enrollmentIds);
							int size = enrollmentIds.size();
							if(!multiplePartitionPerIssuer){
								maxEnrollmentsPerPartitions=size;
							}
							int numberOfPartitions = size / maxEnrollmentsPerPartitions;
							if (size % maxEnrollmentsPerPartitions != 0) {
								numberOfPartitions++;
							}
							
							LOGGER.debug("issuerId : "+ issuerId);
							LOGGER.debug("multiplePartitionPerIssuer : "+ multiplePartitionPerIssuer);
							LOGGER.debug("enrollmentIds.size() : "+ enrollmentIds.size());
							LOGGER.debug("maxEnrollmentsPerPartitions : "+ maxEnrollmentsPerPartitions);
							LOGGER.debug("numberOfPartitions : "+ numberOfPartitions);
							
							
							int firstIndex = 0;
							int lastIndex = 0;
							for (int i = 0; i < numberOfPartitions; i++) {
								firstIndex = i * maxEnrollmentsPerPartitions;
								lastIndex = (i + 1) * maxEnrollmentsPerPartitions;
								if (lastIndex > size) {
									lastIndex = size;
								}
								
								LOGGER.debug("EnrollmentRecordPartitioner : partition():: partition map entry start for issuer :: "+issuerId +" and sublist "+i );
								ExecutionContext value = new ExecutionContext();
								partitionMap.put("partition - " + issuerId + " - "+i, value);
								value.put("issuerIds", issuerId);
								value.put("startDate", startDate);
								value.put("endDate", endDate);
								if(enrollmentType!=null && !enrollmentType.isEmpty()){
										value.put("enrollmentType", enrollmentType);
								}
								value.putInt("issuerSubList", i);
								value.putInt("startIndex", firstIndex);
								value.putInt("endIndex", lastIndex);
								value.put("carrierResendFlag", carrierResendFlag);
								value.put("enrollmentTypeLookupId", lookupValue);
								LOGGER.debug("EnrollmentRecordPartitioner : partition():: partition map entry end for issuer :: "+issuerId +" and sublist "+i );
								
							}
							
						}
						
					}
					
				}
			}
		}catch(Exception e){
			LOGGER.info("Partitioner failed to execute : "+ e.getMessage());
			
			if(stepExecution != null){
				ExecutionContext executionContext = stepExecution.getJobExecution().getExecutionContext();
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
		}
		LOGGER.debug("partitionMap Details : "+ partitionMap);
		return partitionMap;
	}

	public String getStartDateStr() {
		return startDateStr;
	}
	
	//@Value("#{jobParameters['Start_Date']}")
	public void setStartDateStr(String startDateStr) {
		if(startDateStr != null && (startDateStr.toString().equalsIgnoreCase("null") || ("".equalsIgnoreCase(startDateStr.trim())))){
			this.startDateStr = null;
		}else{
			this.startDateStr = startDateStr;
		}
		
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	//@Value("#{jobParameters['End_Date']}")
	public void setEndDateStr(String endDateStr) {
		if(endDateStr != null && (endDateStr.toString().equalsIgnoreCase("null") || ("".equalsIgnoreCase(endDateStr.trim())))){
			this.endDateStr = null;
		}else{
			this.endDateStr = endDateStr;
		}
	}

	public String getCarrierResendFlag() {
		return carrierResendFlag;
	}
	
	//@Value("#{jobParameters['Carrier_send_flag']}")
	public void setCarrierResendFlag(String carrierResendFlag) {
		if(carrierResendFlag != null && (carrierResendFlag.toString().equalsIgnoreCase("null")|| ("".equalsIgnoreCase(carrierResendFlag.trim())))){
			this.carrierResendFlag = null;
		}else{
			this.carrierResendFlag = carrierResendFlag;
		}
	}

	public String getHiosIssuerIdList() {
		return hiosIssuerIdList;
	}
	
	//@Value("#{jobParameters['HIOS_issuer_id']}")
	public void setHiosIssuerIdList(String hiosIssuerIdList) {
		this.hiosIssuerIdList = hiosIssuerIdList;
		
	}

	private void getIssuerList(String hiosIdList) {
		StringTokenizer stringTok = new StringTokenizer(hiosIdList,"|");
		List<Integer> issuerIdList = new ArrayList<Integer>();

		while (stringTok.hasMoreElements()) {
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
				issuerIdList.add( enrollmentRepository.getIssuerIdByHiosIssuerIdPg((String)stringTok.nextElement()));
			}else if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				issuerIdList.add( enrollmentRepository.getIssuerIdByHiosIssuerIdOracle((String)stringTok.nextElement()));
			}
		}

		issuerList = new ArrayList<Integer>();
		if(issuerIdList!=null && !issuerIdList.isEmpty()){
			issuerList.addAll(issuerIdList);
		}
	}

	public String getCommitInterval() {
		return commitInterval;
	}

	public void setCommitInterval(String commitInterval) {
		this.commitInterval = commitInterval;
	}

	public Enrollment834Out getEnrollment834Out() {
		return enrollment834Out;
	}

	public void setEnrollment834Out(Enrollment834Out enrollment834Out) {
		this.enrollment834Out = enrollment834Out;
	}

	public IEnrollmentRepository getEnrollmentRepository() {
		return enrollmentRepository;
	}

	public void setEnrollmentRepository(IEnrollmentRepository enrollmentRepository) {
		this.enrollmentRepository = enrollmentRepository;
	}

	public LookupService getLookupService() {
		return lookupService;
	}

	public void setLookupService(LookupService lookupService) {
		this.lookupService = lookupService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public IssuerFileList getIssuerFileList() {
		return issuerFileList;
	}

	public void setIssuerFileList(IssuerFileList issuerFileList) {
		this.issuerFileList = issuerFileList;
	}

	public String getIssuerLevelPartition() {
		return issuerLevelPartition;
	}

	public void setIssuerLevelPartition(String issuerLevelPartition) {
		this.issuerLevelPartition = issuerLevelPartition;
	}


	public StepExecution getStepExecution() {
		return stepExecution;
	}

	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}		
	
	private Date getMaxOrMinMilliseconds(Date date, boolean isMaxMilliSecondRequired){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if(isMaxMilliSecondRequired){
			calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		}else{
			calendar.set(Calendar.MILLISECOND, calendar.getMinimum(Calendar.MILLISECOND));
		}
		return calendar.getTime();
	}
}
