<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<div class="gutter10">
	<%-- <div class="row-fluid">
		<div class="page-header">
			<c:choose>
	          	<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
	          		<h1> <spring:message code="label.entity.step"/> 6: <spring:message code="label.entity.docUpload"/></h1>	
				</c:when>
				<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus != 'Incomplete'}">
	          		<h1> <spring:message code="label.entity.docUpload"/></h1>	
				</c:when>
			</c:choose>	
		</div>
	</div> --%>



	<div class="row-fluid margin20-t">
	<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
	<div class="span9" id="rightpanel">	
	<div id="section">
		<div class="header">
			<h4><spring:message code="label.entity.step"/> 6: <spring:message code="label.entity.docUpload"/></h4>	
		</div>
		<div class="gutter10">
			<c:choose>	
				<c:when test="${STATE_CODE  eq 'CA'}">
					<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
						<p><spring:message code="label.entity.inThisSectionUCanUploadDocInSupOfUrApp"/></p>
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus != 'Pending'}">
						<p><spring:message code="label.entity.inThisSectionUCanUploadDocInSupOfUrApp"/></p>
					</c:if>
				</c:otherwise>	
			</c:choose>	   
		
	 <form class="form-horizontal" id="frmupdatecertification"  enctype="multipart/form-data" name="frmupdatecertification" action="<c:url value="/entity/enrollmententity/uploadsubmit"/>"  method="POST">	
	  <df:csrfToken/>
			<input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>"/>
			<input type="hidden" id="documentId" name="documentId" value="<encryptor:enc value="${uploadedDocument}"/>" />
			<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1"/>	
			 <input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"/>		
			 <input type="hidden" name="entityStatus" id = "entityStatus" value="${enrollmentEntity.registrationStatus}"/>					
				<div class="row-fluid">									
<!-- 					<div class="header"> -->
<%-- 						<h4 class="span10"><spring:message code="label.brkCertificationStatus"/></h4> --%>
<%-- 							<a class="btn btn-small" type="button"  href="/hix/admin/broker/certificationstatus/${broker.id}"><spring:message code="label.brkCancel"/></a> --%>
<!-- 					</div> -->
                     
					<div class="">				
					<table class="table table-border-none table-condensed table-auto verticalThead">
						<thead>													
						</thead>
						<!-- header class for accessibility -->
								<tbody>
									
									<tr id=progress>
		                   		     <td >		                   				
		                   				<h5><spring:message code="label.entity.uploading"/></h5>		                   	   			
		                   			</td>
		                   			<td>		                   				
		                   				<div style="width:100px;" id="progressbar"></div>		                   	   			
		                   			</td>
	                  				</tr>
	                  				
									<tr>
										<c:choose>	
											<c:when test="${STATE_CODE  eq 'CA'}">
												<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
													<td class="txt-right"><label for="fileInput"><spring:message code="label.entity.uploadDocument"/></label></td>
	             									<td><input type="file" class="input-file" id="fileInput" name="fileInput" ></td>
	             									<tr>
	             										<td></td>
	             										<td>
															<spring:message code="label.uploadDocumentCaption"/>
														</td>
													</tr>
												</c:if>
					 						</c:when>
											<c:otherwise>
												<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus != 'Pending'}">
													<td class="txt-right"><label for="fileInput"><spring:message code="label.entity.uploadDocument"/></label></td>
	             									<td><input type="file" class="input-file" id="fileInput" name="fileInput" ></td>
	             									<tr>
	             										<td></td>
	             										<td>
															<spring:message code="label.uploadDocumentCaption"/>
														</td>
													</tr>
												</c:if>
			 								</c:otherwise>	
		 								</c:choose>	                   					                   			
	                  				</tr>																											
							</tbody>
					</table>
					</div>							
				</div>
				<input type="hidden" name="formAction" id="formAction" value="" />		
				<div id="loading" class="txt-center" style="display:none">
				<span class="green" class="gutter10" style="display:block;">Saving your data.</span>
				<img  src="<%=request.getContextPath() %>/resources/img/loader_greendots.gif" width="15%" alt="green loader dots"/>
				</div>	
		</form>
				
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
				           <display:column property="Date" titleKey="label.entity.date"  format="{0,date,MMM dd, yyyy}" sortable="false"  />
				           <display:column titleKey="label.entity.fileName" > 
				           <c:set var="enrollmentRegisterStatusHistoryDocumentIdEE" ><encryptor:enc value="${enrollmentRegisterStatusHistory.documentIdEE}" isurl="true"/> </c:set>
				           <c:choose>
				                    <c:when test="${enrollmentRegisterStatusHistory.FileName!=null}">
				                    	  <a href="#" onClick="showdetail('${enrollmentRegisterStatusHistoryDocumentIdEE}');" id="edit_${enrollmentRegisterStatusHistoryDocumentIdEE}">${enrollmentRegisterStatusHistory.FileName}</a> 
			                        </c:when>
			                        <c:otherwise>				                 
			                        </c:otherwise>
			                 </c:choose>
			                 </display:column>
			                 				           
				           <c:choose>
								<c:when test="${STATE_CODE  eq 'CA'}">
									<c:if test="${enrollmentRegisterStatusHistory.documentIdEE!=null && loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
										<display:column titleKey="label.entity.remove" >
											<a href="#" onClick="removeDocument('${enrollmentRegisterStatusHistoryDocumentIdEE}');" id="remove_${enrollmentRegisterStatusHistoryDocumentIdEE}"><spring:message code="label.entity.remove"/></a>
										</display:column>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${enrollmentRegisterStatusHistory.documentIdEE!=null && enrollmentEntity.registrationStatus != 'Pending'}">
										<display:column titleKey="label.entity.remove">
											<a href="#" onClick="removeDocument('${enrollmentRegisterStatusHistoryDocumentIdEE}');" id="remove_${enrollmentRegisterStatusHistoryDocumentIdEE}"><spring:message code="label.entity.remove"/></a>
										</display:column>
									</c:if>
								</c:otherwise>
							</c:choose>
 							
					</display:table>					
		
		<div class="form-actions">  	
						<c:choose>
							<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
								<div class="gutter20">
									<c:choose>
										<c:when test="${STATE_CODE  eq 'CA'}">
											<input class="btn pull-left" type="button" value="<spring:message code="label.entity.back"/>" onclick="window.location.href='/hix/entity/assister/registration'">
									 		<input class="btn btn-primary pull-right" type="submit" name="save" id="save" value="<spring:message code="label.entity.submit"/>" >
										</c:when>
										<c:otherwise>
											<input class="btn pull-left" type="button" value="<spring:message code="label.entity.back"/>" onclick="window.location.href='/hix/entity/assister/registration'">
									 		<input class="btn btn-primary pull-right" type="button" name="update" id="update" value="<spring:message code="label.entity.next"/>" onclick="window.location.href='/hix/entity/enrollmententity/getpaymentinfo'">
										</c:otherwise>
									</c:choose>	 	
									 
								</div>
							</c:when>
							<c:otherwise>
								 	
								
							</c:otherwise>
						</c:choose>		                                       	                           
					         
        	 </div>	
			<!-- Showing comments -->			 					
			<!-- end of span9 -->			
		
		<!-- end of .gutter10 -->
		</div>
		</div>
		</div>
	</div>		
</div>	

<div id="reviewapp" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="congratulations"  aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="congratulations"><spring:message code="label.entity.congratulations"/></h3>
  </div>
  <div class="modal-body">
    <p><spring:message code="label.entity.congratulationsUhaveAppliedAsAnEnrollmentEntity"/>
</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="label.entity.close"/></button>
  </div>
</div>

	<script type="text/javascript">
		$(function(){$('.datepick').each(function(){$(this).datepicker({showOn:"button",buttonImage:"../resources/images/calendar.gif",buttonImageOnly:true});});});
		
		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm-dd-yyyy'
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#documentupload").removeClass("link");
			$("#documentupload").addClass("active");
			document.getElementById('progress').style.display='none';
			
			var uploadedDocument = '${uploadedDocument}';
			if(uploadedDocument !="SIZE_FAILURE") {
			if(uploadedDocument != '') {
				if(uploadedDocument != 0) {
					//alert("File uploaded successfully");	
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body"><h4><spring:message code="label.entity.fileUploadedSuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}else{
					//alert("Unable to upload the file");	
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body"><h4><spring:message code="label.entity.UnableToUploadTheFile"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
			}
			}
			else{
				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body"><h4><spring:message code="label.validatePleaseSelectFileWithSizeLessThan5MB" javaScriptEscape='true'/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
			}
		});			
function submitForm(formAction) {
			$('#formAction').val(formAction);
			if(((formAction == 'upload' && $("#fileInput").val() != '')) || (formAction =='update')) {
				if(formAction == 'update') {
					
					$('#frmupdatecertification').attr('action', '/hix/entity/enrollmententity/documententity');
					$('#frmupdatecertification').attr('method', 'POST');
					}
					
				}
			    if($("#fileInput_Size").val()==1){
				document.getElementById('progress').style.display='block';
				$('#frmupdatecertification').submit();
			    }
			    else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body"><h4><spring:message code="label.validatePleaseSelectFileWithSizeLessThan5MB" javaScriptEscape='true'/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
			
		}
		
$(function(){
	 $('#fileInput').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInput_Size").val(0);
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
			 submitForm('upload');
		});
	});				
</script>
<script>
$( "#progressbar" ).progressbar({
	  value: 50
	});
function showdetail(documentId) {
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.entiy.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.entiy.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/entity/enrollmententity/viewAttachment?documentId="+documentId;
			 window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	}
	 
function removeDocument(documentIdEE) {
	var  contextPath =  "<%=request.getContextPath()%>";
	 var documentUrl = contextPath + "/entity/enrollmententity/remove?documentId="+documentIdEE;
	    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}

$(function() {
	$("#save").click(function(e){
		var frm = $('#frmupdatecertification');
		if ($('#loading').is(':hidden')) {
			$('#loading').show('slow', 'linear');
			$("#finish").attr("disabled","disabled");
		 }
	        $.ajax({
	            type: frm.attr('method'),
	            url: "<c:url value='/entity/enrollmententity/registerentity'/>",
	            data: frm.serialize(),
	            success: function(responseText){
	            	if(responseText == 'success'){
	            		if($("#loggedUser").val()!="entityAdmin"){
	            			if($("#entityStatus").val() == "Incomplete"){
	            				$('#reviewapp').modal('show');
	            				$('#reviewapp').on('hidden', function(){
	            					$('#loading').hide('slow', 'linear');
	            						window.location.href="<c:url value='/entity/enrollmententity/registrationstatus'/>";
	            				});
	            			}
	            			else{
	            				window.location.href="<c:url value='/entity/enrollmententity/documententity'/>";
	            			}
						}
	            		if($("#loggedUser").val()=="entityAdmin"){
							<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/></c:set>
							window.location.href="<c:url value='/entity/entityadmin/documententity/${enrollmentEntityId}'/>";
						}
	            	}else{  
	            		$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
	            		$('#loading').hide('slow', 'linear');
	            		$("#finish").removeAttr("disabled");
	            		return false;
	            	}
	            }
	        }); 
	});
}); 

</script>
