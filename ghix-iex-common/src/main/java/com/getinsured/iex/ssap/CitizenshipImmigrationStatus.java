
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 */
public class CitizenshipImmigrationStatus {
  @Expose
  private Boolean honorablyDischargedOrActiveDutyMilitaryMemberIndicator = null; // UI doesn't want anything preselected until user picks.
  @NotNull
  @Expose
  private Boolean citizenshipStatusIndicator = null; // UI doesn't want anything preselected until user picks.
  @Expose
  private List<EligibleImmigrationDocumentType> eligibleImmigrationDocumentType = new ArrayList<EligibleImmigrationDocumentType>();
  @Expose
  private Boolean naturalizationCertificateIndicator = false;
  @Expose
  private Boolean citizenshipManualVerificationStatus = false;
  @Expose
  private Boolean citizenshipAsAttestedIndicator = false;
  @Expose
  private Boolean eligibleImmigrationStatusIndicator = false;
  @Expose
  private CitizenshipDocument citizenshipDocument = new CitizenshipDocument();
  @Expose
  private Boolean lawfulPresenceIndicator = false;
  @Expose
  private String naturalizationCertificateAlienNumber;
  @Expose
  private String eligibleImmigrationDocumentSelected;
  @Expose
  private OtherImmigrationDocumentType otherImmigrationDocumentType = new OtherImmigrationDocumentType();
  @Expose
  private Boolean naturalizedCitizenshipIndicator = null; // UI doesn't want anything preselected until user picks.
  @Expose
  private Boolean livedIntheUSSince1996Indicator = null; // UI doesn't want anything preselected until user picks.
  @Expose
  private String naturalizationCertificateNaturalizationNumber;

  public Boolean getHonorablyDischargedOrActiveDutyMilitaryMemberIndicator() {
    return honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
  }

  public void setHonorablyDischargedOrActiveDutyMilitaryMemberIndicator(Boolean honorablyDischargedOrActiveDutyMilitaryMemberIndicator) {
    this.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
  }

  public Boolean getCitizenshipStatusIndicator() {
    return citizenshipStatusIndicator;
  }

  public void setCitizenshipStatusIndicator(Boolean citizenshipStatusIndicator) {
    this.citizenshipStatusIndicator = citizenshipStatusIndicator;
  }

  public List<EligibleImmigrationDocumentType> getEligibleImmigrationDocumentType() {
    return eligibleImmigrationDocumentType;
  }

  public void setEligibleImmigrationDocumentType(List<EligibleImmigrationDocumentType> eligibleImmigrationDocumentType) {
    this.eligibleImmigrationDocumentType = eligibleImmigrationDocumentType;
  }

  public Object getNaturalizationCertificateIndicator() {
    return naturalizationCertificateIndicator;
  }

  public void setNaturalizationCertificateIndicator(Boolean naturalizationCertificateIndicator) {
    this.naturalizationCertificateIndicator = naturalizationCertificateIndicator;
  }

  public Boolean getCitizenshipManualVerificationStatus() {
    return citizenshipManualVerificationStatus;
  }

  public void setCitizenshipManualVerificationStatus(Boolean citizenshipManualVerificationStatus) {
    this.citizenshipManualVerificationStatus = citizenshipManualVerificationStatus;
  }

  public Boolean getCitizenshipAsAttestedIndicator() {
    return citizenshipAsAttestedIndicator;
  }

  public void setCitizenshipAsAttestedIndicator(Boolean citizenshipAsAttestedIndicator) {
    this.citizenshipAsAttestedIndicator = citizenshipAsAttestedIndicator;
  }

  public Boolean getEligibleImmigrationStatusIndicator() {
    return eligibleImmigrationStatusIndicator;
  }

  public void setEligibleImmigrationStatusIndicator(Boolean eligibleImmigrationStatusIndicator) {
    this.eligibleImmigrationStatusIndicator = eligibleImmigrationStatusIndicator;
  }

  public CitizenshipDocument getCitizenshipDocument() {
    return citizenshipDocument;
  }

  public void setCitizenshipDocument(CitizenshipDocument citizenshipDocument) {
    this.citizenshipDocument = citizenshipDocument;
  }

  public Boolean getLawfulPresenceIndicator() {
    return lawfulPresenceIndicator;
  }

  public void setLawfulPresenceIndicator(Boolean lawfulPresenceIndicator) {
    this.lawfulPresenceIndicator = lawfulPresenceIndicator;
  }

  public String getNaturalizationCertificateAlienNumber() {
    return naturalizationCertificateAlienNumber;
  }

  public void setNaturalizationCertificateAlienNumber(String naturalizationCertificateAlienNumber) {
    this.naturalizationCertificateAlienNumber = naturalizationCertificateAlienNumber;
  }

  public String getEligibleImmigrationDocumentSelected() {
    return eligibleImmigrationDocumentSelected;
  }

  public void setEligibleImmigrationDocumentSelected(String eligibleImmigrationDocumentSelected) {
    this.eligibleImmigrationDocumentSelected = eligibleImmigrationDocumentSelected;
  }

  public OtherImmigrationDocumentType getOtherImmigrationDocumentType() {
    return otherImmigrationDocumentType;
  }

  public void setOtherImmigrationDocumentType(OtherImmigrationDocumentType otherImmigrationDocumentType) {
    this.otherImmigrationDocumentType = otherImmigrationDocumentType;
  }

  public Boolean getNaturalizedCitizenshipIndicator() {
    return naturalizedCitizenshipIndicator;
  }

  public void setNaturalizedCitizenshipIndicator(Boolean naturalizedCitizenshipIndicator) {
    this.naturalizedCitizenshipIndicator = naturalizedCitizenshipIndicator;
  }

  public Boolean getLivedIntheUSSince1996Indicator() {
    return livedIntheUSSince1996Indicator;
  }

  public void setLivedIntheUSSince1996Indicator(Boolean livedIntheUSSince1996Indicator) {
    this.livedIntheUSSince1996Indicator = livedIntheUSSince1996Indicator;
  }

  public String getNaturalizationCertificateNaturalizationNumber() {
    return naturalizationCertificateNaturalizationNumber;
  }

  public void setNaturalizationCertificateNaturalizationNumber(String naturalizationCertificateNaturalizationNumber) {
    this.naturalizationCertificateNaturalizationNumber = naturalizationCertificateNaturalizationNumber;
  }

  @Override
  public String toString() {
    return "CitizenshipImmigrationStatus{" +
        "honorablyDischargedOrActiveDutyMilitaryMemberIndicator=" + honorablyDischargedOrActiveDutyMilitaryMemberIndicator +
        ", citizenshipStatusIndicator=" + citizenshipStatusIndicator +
        ", eligibleImmigrationDocumentType=" + eligibleImmigrationDocumentType +
        ", naturalizationCertificateIndicator=" + naturalizationCertificateIndicator +
        ", citizenshipManualVerificationStatus=" + citizenshipManualVerificationStatus +
        ", citizenshipAsAttestedIndicator=" + citizenshipAsAttestedIndicator +
        ", eligibleImmigrationStatusIndicator=" + eligibleImmigrationStatusIndicator +
        ", citizenshipDocument=" + citizenshipDocument +
        ", lawfulPresenceIndicator=" + lawfulPresenceIndicator +
        ", naturalizationCertificateAlienNumber='" + naturalizationCertificateAlienNumber + '\'' +
        ", eligibleImmigrationDocumentSelected='" + eligibleImmigrationDocumentSelected + '\'' +
        ", otherImmigrationDocumentType=" + otherImmigrationDocumentType +
        ", naturalizedCitizenshipIndicator=" + naturalizedCitizenshipIndicator +
        ", livedIntheUSSince1996Indicator=" + livedIntheUSSince1996Indicator +
        ", naturalizationCertificateNaturalizationNumber='" + naturalizationCertificateNaturalizationNumber + '\'' +
        '}';
  }
}
