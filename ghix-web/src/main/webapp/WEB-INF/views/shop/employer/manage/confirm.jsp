<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<div class="gutter10">
  <div class="row-fluid">
    <h1 id="skip"><spring:message code="label.confirmnewhirehead"/></h1>
   </div>
    <div class="row-fluid">
    <div class="gutter10">
      <div class="span3" id="sidebar">
      <div class="header">
        <h4><spring:message code="label.youshouldknow"/></h4>
        </div>
        <div class="gutter10">
            <p>The new hire's Effective Date and Open Enrollment Dates are calculated based on the Coverage Eligibility Date you
            provided when adding this employee. These dates for the new hire cannot be before the respective dates used for the
            rest of your employees during Initial Open Enrollment.</p>
            <p>If you need to correct the Coverage Eligibility Date go back to the previous page and update.</p>
          </div>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
        <div class="header">
          <h4><spring:message code="label.newhireinfo"/></h4>
          </div>
          <table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.newhirename"/></strong></td>
						<td>Ken Smith</td>
					</tr>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.coverageeligibilitydate"/></strong></td>
						<td>09/01/2013</td>
					</tr>
				</tbody>
			</table>
        </div>
        <br>
        <div class="row-fluid">
        <div class="header">
          <h4><spring:message code="label.impdatescalc"/></h4>
          </div>
          <table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.effectivedate"/></strong></td>
						<td>01/01/2014</td>
					</tr>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.openenrollmentbegindate"/></strong></td>
						<td>11/01/2013</td>
					</tr>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.openenrollmentenddate"/></strong></td>
						<td>11/30/2013</td>
					</tr>
				</tbody>
			</table>
			<div class="form-actions">
				<input type="button" value="<spring:message code="label.newhireback"/>" title="<spring:message code="label.newhireback"/>" class="btn pull-left">
				<input type="submit" value="<spring:message code="label.newhireconfirm"/>" title="<spring:message code="label.newhireconfirm"/>" class="btn btn-primary pull-right">
			</div>
        </div> 
      </div>
      </div> 
    </div> 
  </div>
<!--gutter10-->