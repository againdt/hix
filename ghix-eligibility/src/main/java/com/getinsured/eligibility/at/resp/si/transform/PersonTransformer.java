package com.getinsured.eligibility.at.resp.si.transform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram;
import com.getinsured.eligibility.at.resp.si.dto.PersonInfo;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.dto.Verification;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.util.ReferralUtil;

public final class PersonTransformer {

	//private static final Logger LOGGER = Logger.getLogger(PersonTransformer.class);

	private PersonTransformer(){}

	public static void personInfo(ERPResponse erpResponse, AccountTransferRequestPayloadType incomingRequest) {

		List<TaxHouseholdMember> atApplicantList = new ArrayList<>();

		List<InsuranceApplicantType> InsuranceApplicantTypeList = incomingRequest.getInsuranceApplication().getInsuranceApplicant();
		
		for (InsuranceApplicantType insuranceApplicantType : InsuranceApplicantTypeList) {

			String applicantIdentificationId = extractPersonInfoString(extractApplicantIdentificationId(insuranceApplicantType));
			com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType pt = (PersonType) insuranceApplicantType.getRoleOfPersonReference().getRef();
			
			// populate person info for applicant...
			TaxHouseholdMember atTaxHouseholdMember = new TaxHouseholdMember();
			PersonInfo personInfo = new PersonInfo();

			//Verifications Map...
			Map<String, Verification> verificationMap = new HashMap<>();

			List<PersonType> personList = incomingRequest.getPerson();
			for (PersonType personType : personList) {

				if (StringUtils.equalsIgnoreCase(pt.getId(),personType.getId())){

					String applicantIdentificationIdNew = extractPersonInfoString(extractApplicantIdentificationId(personType));
					personInfo.setApplicantIdentificationId(applicantIdentificationIdNew);

					personInfo.setFirstName(extractPersonInfoString(personType.getPersonName().get(0).getPersonGivenName()));
					personInfo.setMiddleName(extractPersonInfoString(personType.getPersonName().get(0).getPersonMiddleName()));
					personInfo.setLastName(extractPersonInfoString(personType.getPersonName().get(0).getPersonSurName()));
					personInfo.setSsn(formatSSN(personType));
					personInfo.setDob(TransformerHelper.extractDate(personType.getPersonBirthDate().getDate().getValue()));
					personInfo.setRoleOfPersonReference(personType.getId());

					/* isIncarcerated */
					boolean isIncarcerated = isIncarcerated(insuranceApplicantType);
					personInfo.setIncarcerated(isIncarcerated);

					/* lawful presence */
					boolean isLawfulPresenceAnswered = isLawfulPresenceAnswered(insuranceApplicantType, personType);
					personInfo.setLawfulPresenceAnswered(isLawfulPresenceAnswered);

					atTaxHouseholdMember.setPersonInfo(personInfo);

					// SSN Verifications...
					List<IdentificationType> personSSNIdentification = personType.getPersonSSNIdentification();
					if (personSSNIdentification != null && personSSNIdentification.size() > 0 && personSSNIdentification.get(0) != null){
						List<Verification> ssnVerificationList = VerificationTransformer.extractVerifications(personSSNIdentification.get(0).getMetadata());
						for (Verification ssnVerification : ssnVerificationList) {
							verificationMap.put(ssnVerification.getType(), ssnVerification);
						}
					}

					// eligibleImmigrationStatus Verifications...
					LawfulPresenceStatusType insuranceApplicantLawfulPresenceStatus = insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus();
					if (insuranceApplicantLawfulPresenceStatus != null){
						List<Verification> eligibleImmigrationStatusVerificationList = VerificationTransformer.extractVerifications(insuranceApplicantLawfulPresenceStatus.getMetadata());
						for (Verification eisVerification : eligibleImmigrationStatusVerificationList) {
							verificationMap.put(eisVerification.getType(), eisVerification);
						}
					}

					// PersonUSCitizenIndicator Verifications...
					List<Boolean> personUSCitizenIndicator = personType.getPersonUSCitizenIndicator();
					if (personUSCitizenIndicator != null && personUSCitizenIndicator.size() > 0 && personUSCitizenIndicator.get(0) !=null){
						List<Verification> citizenshipVerifications =  VerificationTransformer.extractVerifications(personUSCitizenIndicator.get(0).getMetadata());
						for (Verification citizenshipVerification : citizenshipVerifications) {
							verificationMap.put(citizenshipVerification.getType(), citizenshipVerification);
						}
					}

					// Current Income Verifications...
					if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonIncome() != null 
							&& personType.getPersonAugmentation().getPersonIncome().size() > 0 && personType.getPersonAugmentation().getPersonIncome().get(0) != null){
						List<Verification> currentIncomeVerificationList = VerificationTransformer.extractVerifications(personType.getPersonAugmentation().getPersonIncome().get(0).getMetadata());
						for (Verification ciVerification : currentIncomeVerificationList) {
							verificationMap.put(ciVerification.getType(), ciVerification);
						}
					}

					break; // person found
				}
			}

			// populate verifications for an AT Applicant...
			atTaxHouseholdMember.setVerificationMap(verificationMap);

			// populate eligibility indicators
			Map<String, EligibilityProgram> eligibilityProgramMap = EligibilityTransformer.eligibilityProgram(insuranceApplicantType);
			atTaxHouseholdMember.setEligibilityProgramMap(eligibilityProgramMap);

			atApplicantList.add(atTaxHouseholdMember);

		}

		erpResponse.setTaxHouseholdMemberList(atApplicantList);

	}



	private static boolean isLawfulPresenceAnswered(InsuranceApplicantType insuranceApplicantType, PersonType personType) {

		boolean isLawfulPresenceAnswered = false;
		boolean isUSCitizen = personType.getPersonUSCitizenIndicator() != null && personType.getPersonUSCitizenIndicator().size() > 0 
				&& personType.getPersonUSCitizenIndicator().get(0) != null ? personType.getPersonUSCitizenIndicator().get(0).isValue() :false;

		if (isUSCitizen){
			isLawfulPresenceAnswered = true;
		} else {
			ImmigrationDocumentCategoryCodeSimpleType documentType = insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus() != null ?
					insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument() != null  
					&& insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().size() > 0 
					&& insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(0) != null ?		
							insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(0).getLawfulPresenceDocumentCategoryCode() != null ?
									insuranceApplicantType.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(0).getLawfulPresenceDocumentCategoryCode().getValue() : null : null : null;
			if (documentType != null) {
				isLawfulPresenceAnswered = true;
			}
		}
		return isLawfulPresenceAnswered;
	}

	private static boolean isIncarcerated(InsuranceApplicantType insuranceApplicantType) {
		boolean isIncarcerated = insuranceApplicantType.getInsuranceApplicantIncarceration() != null 
				&& insuranceApplicantType.getInsuranceApplicantIncarceration().size() > 0 && insuranceApplicantType.getInsuranceApplicantIncarceration().get(0) != null ?
				insuranceApplicantType.getInsuranceApplicantIncarceration().get(0).getIncarcerationIndicator() != null ?
						insuranceApplicantType.getInsuranceApplicantIncarceration().get(0).getIncarcerationIndicator().isValue() : true : true;
		return isIncarcerated;
	}

	@Deprecated
	private static com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String extractApplicantIdentificationId(
			InsuranceApplicantType insuranceApplicantType) {
		return insuranceApplicantType.getReferralActivity() != null ? insuranceApplicantType.getReferralActivity().getActivityIdentification() != null ? insuranceApplicantType.getReferralActivity().getActivityIdentification().getIdentificationID() : null : null;
	}


	private static String extractApplicantIdentificationId(PersonType personType) {
		String identificationId = null;
		if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
			identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
		}

		if (identificationId == null) {
			if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
				identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID().getValue();
			}
		}
		return identificationId;
	}

	public static String formatSSN(PersonType personType) {
		IdentificationType personSSNType = personType.getPersonSSNIdentification() != null  
				&& personType.getPersonSSNIdentification().size() > 0 
				&& personType.getPersonSSNIdentification().get(0) != null ? personType.getPersonSSNIdentification().get(0) : null;
		return personSSNType == null ? null : StringUtils.replace(extractPersonInfoString(personSSNType.getIdentificationID()), "-", "");
	}


	public static String extractPersonInfoString(Object input){

		String output = StringUtils.EMPTY;

		if (input instanceof PersonNameTextType){
			output = input != null ? ((PersonNameTextType) input).getValue() : output;
		} else if (input instanceof com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String){
			output = input != null ? ((com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String) input).getValue() : output;
		} else if (input instanceof String) {
			output = input != null ? input.toString() : output;
		}

		return output;

	}
	
	public static PersonType getPrimaryPerson(AccountTransferRequestPayloadType request) {
		final PersonType pType = (PersonType) request.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
		return pType;
	}


}
