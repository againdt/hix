(function (views) {
	views.Carts = Backbone.View.extend({
		events : {
			'click a.removeItems': 'removeItems',
			'click a.validateCart': 'validateCart'
		},
		el : '#cart',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','removeItems','validateCart');
			var self = this;
			var ehb= "";
			self.hasSrNo = new Array();
			self.collection.fetch({
				success : function () {
					self.collection.setCartSummary(false,false);
					
				}
			});
			
			self.cartModel = options.cartModel;
		
			$('.validateCart').bind('click', this.validateCart);
			self.collection.bind('reset', self.addAll);
		},
		
		addAll : function (models) {
			var self = this;
			this.updateCartSummary();
			this.updatePremium();
			$('#cart').html('');
			$("table[id^='table_'] tbody").empty();
			$("table[id^='table_dental_'] tbody").empty();

			self.collection.each (self.addOne);
			
			$('#loadDiv').hide();
			$("table[id^='table_']").show();
			$("table[id^='table_tbody_']").show();
			$('#summary_table').show();
		},
		
		addOne : function (model) {
			var self = this;
			var pgrmType = $('#PGRM_TYPE').val();
			var keepOnly = $('#KEEP_ONLY').val();
			var isRenewal = $('#isRenewal').val();
			
			model.set({"PGRM_TYPE":pgrmType});
			model.set({"KEEP_ONLY":keepOnly});
			
			if(jQuery.inArray(model.get("id"),self.hasSrNo) === -1){
				self.hasSrNo.push(model.get("id"));
				model.set({srNo: self.hasSrNo.length});
			} 
			var premiumDiff = 0;
			var aptcDiff = 0;
			if(pgrmType == 'Individual'){
				var premium = model.get("premium");
				var oldPremium = model.get("oldPremium");				
				premiumDiff = oldPremium!=null?premium-oldPremium:0;	
				var aptc = model.get("aptc");
				var oldAptc = model.get("oldAptc");				
				aptcDiff = oldAptc!=null?aptc-oldAptc:0;
			}else{
				if(keepOnly!=null && isRenewal == 'Y'){
					var premium = model.get("premium");
					var oldPremium = model.get("oldPremium");				
					premiumDiff = oldPremium!=null?premium-oldPremium:0;				
				}
			}
			model.set({"premiumDiff":premiumDiff.toFixed(2)});
			model.set({"aptcDiff":aptcDiff.toFixed(2)});
			
			var view = new Cart({model:model});
			if(model.get("planType") == "DENTAL") {
				$('#table_dental_'+model.get("groupId")).append(view.render().el);
				//$('.health-cart').hide();
				$('.dental-cart').show();
				$('.removemsg').hide();
				$('.dentaldate').show();
				$('#dentalEHBPremium').val(model.get("ehbPremium"));
			} else {
				$('#table_'+model.get("groupId")).append(view.render().el);
				$('.dental-cart').show();
			//	$('.dental-cart').hide();
			//	$('#health-cart').show();
				//$('.dentaldate').hide();
				$('#healthEHBPremium').val(model.get("ehbPremium"));
			}
			$('.removeItems').bind('click', this.removeItems);
			$("#cart").empty();
		},
		
		updateCartSummary : function () {
			var self = this;
			$('#planCount').html(self.collection.cartSummary.planCount);
			var detailSummary = self.collection.cartSummary.platinum ? self.collection.cartSummary.platinum+plan_display['pd.label.cart.platinumPlans']:"";
			detailSummary += self.collection.cartSummary.gold ? self.collection.cartSummary.gold+plan_display['pd.label.cart.goldPlans'] : "";
			detailSummary += self.collection.cartSummary.silver ? self.collection.cartSummary.silver+plan_display['pd.label.cart.silverPlans'] : "";
			detailSummary += self.collection.cartSummary.bronze ? self.collection.cartSummary.bronze+plan_display['pd.label.cart.bronzePlans'] : "";
			detailSummary += self.collection.cartSummary.catastrophic ? self.collection.cartSummary.catastrophic+plan_display['pd.label.cart.catastrophiPlans'] : "";
			
			$('#detailSummary').html(detailSummary.substring(0,detailSummary.length-2));
		},
		
		updatePremium : function ()
		{
			var totalPremiumBeforeCredit = this.collection.getTotalPremium();
            var totalAptc;
            if($('#PGRM_TYPE').val() == "Individual") 
            {
            	totalAptc = parseFloat($('#healthAPTC').val()) + parseFloat($('#dentalAPTC').val());;
            }
            else 
            {
            	totalAptc = parseFloat($('#healthAPTC').val()) + parseFloat($('#dentalAPTC').val());
            }
			var totalPremiumAfterCredit = totalAptc < totalPremiumBeforeCredit ? totalPremiumBeforeCredit - totalAptc : 0;			
			$('#totalAptc').html("-"+plan_display['pd.label.dollarSign']+totalAptc.toFixed(2));
			$('#totalPremiumBeforeCredit').html(plan_display['pd.label.dollarSign']+totalPremiumBeforeCredit.toFixed(2));
			$('#totalPremiumAfterCredit').html(plan_display['pd.label.dollarSign']+totalPremiumAfterCredit.toFixed(2));
			$('#totalPremiumAfterCreditHeader').html(plan_display['pd.label.dollarSign']+totalPremiumAfterCredit.toFixed(2));
			$('#totalPremiumBeforeCreditVal').val(totalPremiumBeforeCredit.toFixed(2));
			$('#netpremium').val(totalPremiumAfterCredit.toFixed(2));						
		},
		
		removeItems : function (e) {
			var itemIdArr = e.target.id.replace("remove_","").split("_");
			var planId = itemIdArr[1];
			var groupId = itemIdArr[0];
			$('.removemsg').show();
			$('.dentaldate').hide();
			if( $("#dentalAPTC").length ){
					$("#dentalAPTC").val("0.0");
	 	 	}
			this.collection.removeSelectedItems(planId,groupId);
			this.updateCartSummary();
		},

		validateCart : function(){
			if(this.collection.models.length <= 0){
				alert(plan_display['pd.label.alert.noItemsInCart']);
				return false;
			}
			var enrollmentType = $('#enrollmentType').val();
			var healthPlanPresent=$('#healthPlanPresent').val();
			var dentalPlanPresent=$('#dentalPlanPresent').val();
			if(enrollmentType=='S' && healthPlanPresent=='N' && dentalPlanPresent=='Y' )
			{
				$('#no-health-plan-error').modal('show');
				return false;
			}
			if(this.collection.models.length == 1){
				if(this.collection.models[0].get("planType") == "DENTAL"){
					$('#no-health-plan-error').modal('show');
					return false;
				}
			}
			
			var enrollmentType = $('#enrollmentType').val();
			if(enrollmentType == 'S')
			{
				$('#checkout').hide();
				$.ajax({
				  type: "GET",
					url: './personDataUpdate?key='+(Math.random()*10)
			    }).done(function( msg ) {
			    	$('#checkout').show();
			    	if(msg == "SUCCESS"){
			    		$("#showEsign").submit();
			    	}
			    });
			}else{
				$("#showEsign").submit();
			}
		}
	});
	
	var Cart = Backbone.View.extend({
		tagName : 'tbody',
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			var template = _.template($('#tmpCart').html());
			$(this.el).html(template(this.model.toJSON()));
			
			return this;
		}
	});
})(App.views);