import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import { FormattedMessage } from 'react-intl';

import { stateSpecificAHBXRedirect, goToSSAPStart } from '../utils/stateSpecificUtils';

/** HealthPlan Component. */
const HealthPlan = props => {
  const {
    type,
    planType,
    singleColData,
    multiColData,
    isInsideOEWindow,
    applicationStatus,
    applicationYear,
    stateCode,
    ssapFlowVersion,
    config
  } = props;

  let tStatusRef = null;
  let tMembersRef = null;

  const checkForStartShoppingLinkInMN = (text) => stateCode === 'MN' && text === 'plans.action.start_shopping';
  const planTypeText = stateCode === 'MN' && planType === 'Health' ? 'Medical' : planType;

  const onKeyUpEvent = (e, tooltipRef) => {
    // check for Esc key
    if (e.keyCode === 27) {
        hideTooltip(e, tooltipRef);
    }
  }

  const hideTooltip = (e, tooltipRef) => {
      ReactTooltip.hide(tooltipRef);
  }

  const showTooltip = (e, tooltipRef) => {
      ReactTooltip.show(tooltipRef);
  }

  //TODO: Clean this state specific logic
  const getHealthPlans = (planLink, isInsideOEWindow, applicationYear) => {
    const caseNumber = localStorage.getItem('enrollCaseNumber');
    const planLinkMsgId = planLink.props ? planLink.props.id : planLink;
    let {tobbacoEnabled, hardshipEnabled} = config;
    tobbacoEnabled = tobbacoEnabled ? tobbacoEnabled : 'ON';
    hardshipEnabled = hardshipEnabled ? hardshipEnabled : 'ON';

    switch (planLinkMsgId) {
      case "plans.action.complete_application":
        if (stateCode === "CA") {
          stateSpecificAHBXRedirect();
          return;
        } else {
          goToSSAPStart(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
        }
        break;

      case "plans.action.continue_shopping":
        if (tobbacoEnabled === 'ON' || hardshipEnabled === 'ON') {
          window.location = "./indportal#/additionalinfo";
        } else {
          window.caseNumber = caseNumber;
          window.location = "./indportal#/customGrouping";
        }
        break;

      case "plans.action.view_details":
        window.location = "./indportal#/enrollmenthistory";
        break;

      default:
        break;
    }
  };

  const YourPlanText = ({planTypeText}) => {
    if (planTypeText === 'Medical') {
      return <FormattedMessage id="plans.header.medical" />
    } else if (planTypeText === 'Dental') {
      return <FormattedMessage id="plans.header.dental" />
    } else {
      return <FormattedMessage id="plans.header" values={{planType: planTypeText}} />
    }
  }

  let plans = [];

  if (multiColData) {
    plans = multiColData.map(function (item, i) {
      const tooltipCol1Content = item.plan.membersInfo ? item.plan.membersInfo.map((x, i) => {
        return <div key={i}> {x} </div>
      }) : '';

      const tooltipCol2Content = item.tooltipCol2Content;
      const isMNStartShoppingLink = checkForStartShoppingLinkInMN(item.linkInfo.text.props ? item.linkInfo.text.props.id : item.linkInfo.text);

      return <div className="ds-l-row ds-u-font-size--small ds-u-margin--1" key={i}>
        <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--5 ds-l-lg-col--5 ds-l-xl-col--5">
          <div style={{ 'marginBottom': '0' }} className="ds-text"
            id={item.plan.enrollmentId + "_plan_name_" + i}>{item.plan.name}</div>
          <span style={{ 'display': 'block', 'marginTop': 0, 'marginBottom': 0 }}
            className="ds-u-font-size--small ds-text ds-u-measure--wide ds-u-color--muted"
            id={item.plan.type + "_plan_type_" + i}>{item.plan.type}</span>

          {tooltipCol1Content ?
            <div>
              <a data-tip
                ref={ref => tMembersRef = ref}
                data-for={item.plan.type + "_plan_count_tooltip" + i}
                className="ds-u-font-size--small ds-text ds-u-measure--wide ds-u-color--muted ds-u-text-decoration--underline cursor--pointer"
                id={item.plan.type + "_plan_count_" + i}
              >
                <FormattedMessage id="plans.for.member" values={{numMember: item.plan.count}} />
              </a>
              <div tabIndex="0" onFocus={(e) => showTooltip(e, tMembersRef)} onBlur={(e) => hideTooltip(e, tMembersRef)} onKeyUp={(e) => onKeyUpEvent(e, tMembersRef)}>
                <ReactTooltip
                  place="left"
                  type="dark"
                  effect="solid"
                  className='tooltip-component ds-u-text-transform--capitalize'
                  id={item.plan.type + "_plan_count_tooltip" + i}
                >
                  <span>{tooltipCol1Content}</span>
                </ReactTooltip>
              </div>
            </div> : null
          }
        </div>
        <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--4 ds-l-lg-col--4 ds-l-xl-col--4">
          <p
            className="ds-u-text ds-u-measure--wide"
            id={item.plan.enrollmentId + "_plan_status_" + i}
          >
            <span
              className={`ds-u-text-transform--capitalize ${tooltipCol2Content ? "cursor--pointer ds-u-text-decoration--underline" : ''}`}
              data-tip
              ref={ref => tStatusRef = ref}
              data-for={item.plan.enrollmentId + "_plan_status_tooltip" + i}
            >
              {item.status}
            </span>
          </p>
          {tooltipCol2Content ?
            <div tabIndex="0" onFocus={(e) => showTooltip(e, tStatusRef)} onBlur={(e) => hideTooltip(e, tStatusRef)} onKeyUp={(e) => onKeyUpEvent(e, tStatusRef)}>
              <ReactTooltip
                place="left"
                type="dark"
                effect="solid"
                className='tooltip-component'
                id={item.plan.enrollmentId + "_plan_status_tooltip" + i}
              >
                <span>{tooltipCol2Content}</span>
              </ReactTooltip></div> : null
          }
        </div>
        {!isMNStartShoppingLink &&
          <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">
            <div className="ds-u-text-decoration--underline">
              <a href="javascript:void(0)" id={item.plan.enrollmentId + "_plan_link_" + i}
                onClick={() => getHealthPlans(item.linkInfo.text, isInsideOEWindow, applicationYear)}>{item.linkInfo.text}</a>
            </div>
          </div>
        }
      </div>
    }, this);
  } else {
    plans.push(<div></div>);
  }

  if (type === 'MULTIPLE_COL') {
    return (
      <div className="ds-u-margin--2">
        <div className="ds-l-row">
          <h3 className="ds-h6 ds-u-font-weight--semibold"
            id={planType + "_header"}><YourPlanText {...{planTypeText}} /></h3>
        </div>
        {plans}
      </div>
    )
  } else {
    if (singleColData) {
      return (
        <div className="ds-u-font-size--small ds-u-margin--2">
          <div className="ds-l-row">
            <h3 className="ds-h6 ds-u-font-weight--semibold"
              id={planType + "_header"}><YourPlanText {...{planTypeText}} /></h3>
          </div>
          <div className="ds-l-row ds-u-margin--1">
            <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--9 ds-l-lg-col--9 ds-l-xl-col--9">
              <div style={{ 'marginBottom': '0' }}
                className="ds-u-text"
                id={planType + "_text"}>{singleColData.col1}</div>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div></div>
      )
    }
  }
};

HealthPlan.propTypes = {
  /** Status Text */
  status: PropTypes.string,

  /** Link Text */
  link: PropTypes.string,

  /** Plan Name */
  planName: PropTypes.string,

  /** Plan Type */
  planType: PropTypes.string,

  /** Plan Members count */
  planMembers: PropTypes.string,

  /** Link Display Text */
  linkText: PropTypes.string,
};

HealthPlan.defaultProps = {};

export default HealthPlan;