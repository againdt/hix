package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;

@Component("toConsiderPartitionerParams")
public class ToConsiderPartitionerParams {

	List<Long> ssapApplicationIdsList = null;

	String processErrorRecords;
	private int serverName;
	
	
	public String getProcessErrorRecords() {
		return processErrorRecords;
	}

	public void setProcessErrorRecords(String processErrorRecords) {
		this.processErrorRecords = processErrorRecords;
	}

	public ToConsiderPartitionerParams() {
		this.ssapApplicationIdsList = new CopyOnWriteArrayList<Long>();
	}

	public List<Long> getSsapApplicationIdList() {
		return ssapApplicationIdsList;
	}

	public synchronized void clearSsapApplicationIdList() {
		this.ssapApplicationIdsList.clear();
	}

	public synchronized void addAllToSsapApplicationIdList(List<Long> enrollmentIdList) {
		this.ssapApplicationIdsList.addAll(enrollmentIdList);
	}

	public synchronized void addToSsapApplicationIdList(Long ssapApplicationId) {
		this.ssapApplicationIdsList.add(ssapApplicationId);
	}
	
	public int getServerName() {
		return serverName;
	}

	public void setServerName(int serverName) {
		this.serverName = serverName;
	}
}
