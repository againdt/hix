package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;

import com.getinsured.hix.batch.enrollment.service.EnrollmentReconciliationService;

public class EnrollmentReconciliationJobThread implements Callable<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReconciliationJobThread.class);
	
	private String filePath = null;
	private Long jobId = null;
	private JobService jobService = null;
	private StepExecution stepExecution = null;
	private File archivePath = null;
	private JobLauncher jobLauncher;
	private Job job;
	private EnrollmentReconciliationService enrollmentReconciliationService;
	
	public EnrollmentReconciliationJobThread(String filePath, 
			                                 Long jobId, 
			                                 JobService jobService, 
			                                 StepExecution stepExecution,
			                                 File archivePath,  JobLauncher jobLauncher, Job job,
			                                 EnrollmentReconciliationService enrollmentReconciliationService) {
		super();
		this.filePath = filePath;
		this.jobId = jobId;
		this.jobService = jobService;
		this.stepExecution = stepExecution;
		this.archivePath = archivePath;
		this.jobLauncher = jobLauncher;
		this.job = job;
		this.enrollmentReconciliationService = enrollmentReconciliationService;
	}

	@Override
	public String call() throws Exception {
		try {
			 return enrollmentReconciliationService.populateReconciliationData(filePath, jobId, jobService, stepExecution, archivePath,jobLauncher, job);
		} catch (Exception e) {
			LOGGER.error("Exception occurred in EnrollmentReconciliationJobThread: ", e);
		}
		return null;
	}

}
