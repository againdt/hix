package com.getinsured.eligibility.aptcrecalc.service;

import java.util.Map;

import com.getinsured.eligibility.aptcrecalc.dto.Response;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface SsapAptcReCalcService {

	Response callAhbxReCalcApi(Map<String,String> inputData,SsapApplication latestSsapApplication) throws Exception;
}
