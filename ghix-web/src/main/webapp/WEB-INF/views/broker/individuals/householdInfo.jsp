<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Household info popup -->
<script type="text/javascript">
agentEntityApp.controller('houseHoldInfoCtrl', function($scope, $http) {
	$scope.isHouseHoldDataLoaded = false;
    $scope.$on('hoseHoldInfoEvent', function(e, data) {    	
    	$http({
            url: '/hix/broker/HouseholdMemberInformation',
            method: "POST",
            data: data,
            headers: {'Content-Type': 'application/json',"csrftoken": $('#tokid').val()}
        }).success(function (data, status, headers, config) {
              if (data != 'null'){
            	  $scope.houseHoldInfoData = data;
            	  $scope.isHouseHoldDataLoaded = true;
                  angular.element('#houseHoldInfolModal').modal('show');
              }
        }).error(function (data, status, headers, config) {
            alert("error")
        });
    });
});
</script>
<div id="houseHoldInfolModal" class="modal" style="display:none" ng-controller="houseHoldInfoCtrl">
	<div class="modal-header">
		  <p class="pull-left pull-left-small-devices"><spring:message code="label.bobHouseholdMemInfo"/></p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	 <div class="modal-body">
	 	<div class="row-fluid" ng-hide="isHouseHoldDataLoaded">
			  <div class="span12 text-center">
			  		<img src="/hix/resources/images/loader.gif" alt="loading" width="40px" />
			  </div>
		</div>
	 	<div ng-show="isHouseHoldDataLoaded" class="householdDataTable-table-container">
	 	<h3 class="modalMessage"><spring:message code="label.bobHouseholdComposition"/> {{houseHoldInfoData.firstName}} {{houseHoldInfoData.lastName}}</h3>
  		<table class="householdDataTable table table-striped margin10-t"  >
			<thead>
				<tr>
					<th><spring:message code="label.bobHouseholdName"/></th>
					<th><spring:message code="label.bobHouseholdRelationship"/></th>
					<th><spring:message code="label.bobHouseholdDOB"/></th>
					<th><spring:message code="label.bobHouseholdGender"/></th>
					<th><spring:message code="label.bobHouseholdSSN"/></th>
					<th><spring:message code="label.bobHouseholdHomeAddress"/></th>
					<th><spring:message code="label.bobHouseholdMailingAddress"/></th>
					<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
					<th><spring:message code="label.bobHouseholdCitizen"/></th>
					</c:if>
					<th><spring:message code="label.bobHouseholdSeekingCoverage"/></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="data in houseHoldInfoData.applicantEligibility ">
					<td>{{data.firstName}} {{data.lastName}}</td>
					<td>{{data.relationship}}</td>
					<td>{{data.birthDateString}}</td>
					<td class="textCapitalize">{{data.gender}}</td>
					<td>{{data.ssn}}</td>
					<td>{{data.homeAddress.address1}},<span ng-if="data.homeAddress.address2!='null'">{{data.homeAddress.address2}}</span> <br />
					{{data.homeAddress.city}}, {{data.homeAddress.state}} {{data.homeAddress.zip}}</td>
					<td>{{data.mailingLocation.address1}},<span ng-if="data.mailingLocation.address2!='null'">{{data.mailingLocation.address2}}</span><br />
					{{data.mailingLocation.city}}, {{data.mailingLocation.state}} {{data.mailingLocation.zip}}</td>
					<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
					<td>{{data.citizenShipStatus}}</td>
					</c:if>
					<td class="text-center">{{data.seekingCoverage}}</td>
				</tr>
			</tbody>			
		</table>	
		</div>		
	</div>
</div>