package com.getinsured.ssap.model.hub.vlp.documents;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * DHS document interface.
 * @author Yevgen Golubenko
 * @since 2/14/19
 */
public abstract class DhsDocument
{
  protected static final Logger log = LoggerFactory.getLogger(DhsDocument.class);

  /**
   * Date format to be used when serializing documents.
   * <p>
   *   Current format is: {@code MMM dd, yyyy hh:mm:ss a}
   *   Waiting on Vishaka/Abhai to change format to: {@code yyyy-MM-dd}
   * </p>
   *
   * <b>
   *   TODO: Change format of date from "MMM dd, yyyy hh:mm:ss a" to "yyyy-MM-dd" once Vishaka/Abhai update ghix-hub-web.
   * </b>
   */
  public static final String DATE_FORMAT = "yyyy-MM-dd";

  @JsonIgnore
  private DocumentType documentType;

  @JsonProperty("CaseNumber")
  private String caseNumber;

  public DocumentType getDocumentType()
  {
    return documentType;
  }

  public void setDocumentType(final DocumentType documentType)
  {
    this.documentType = documentType;
  }

  public String getCaseNumber()
  {
    return caseNumber;
  }

  public void setCaseNumber(final String caseNumber)
  {
    this.caseNumber = caseNumber;
  }

  /**
   * Validates that all required fields are present.
   * <p>
   *   Always call this method prior sending request to DHS
   *   to make sure you are submitting at least all required fields
   *   in the payload.
   * </p>
   * @return true if all required fields are present.
   */
  @JsonIgnore
  abstract public boolean isValid();

  /**
   * Creates document from given {@link CitizenshipDocument}.
   *
   * @param citizenshipDocument {@link CitizenshipDocument} that is part of
   *  {@link com.getinsured.iex.ssap.HouseholdMember}.
   * @return DhsDocument prefilled with information taken from given
   *  {@link CitizenshipDocument}
   */
  @JsonIgnore
  abstract public DhsDocument from(CitizenshipDocument citizenshipDocument);

  /**
   * Returns Sevis Id from given {@link CitizenshipDocument}.
   * <p>
   *   Since there is for some stupid reason two variants of this field in existing JSON,
   *   and who knows where each of them is used, it tries to find any
   *   that is: not blank and not null.
   * </p>
   * @param citizenshipDocument {@link CitizenshipDocument}
   * @return Sevis Id or {@code null}.
   */
  String getSevisId(final CitizenshipDocument citizenshipDocument) {
    if (notBlank(citizenshipDocument.getSevisId()))
    {
      return citizenshipDocument.getSevisId();
    }
    else if (notBlank(citizenshipDocument.getSEVISId()))
    {
      return citizenshipDocument.getSEVISId();
    }

    return null;
  }

  /**
   * Returns valid value to be set for {@link DhsDocument}.
   * <p>
   *   Valid means that it is not null and not blank.
   * </p>
   * @param field value to return.
   * @return passed value or {@code null}
   */
  String validValue(final String field) {
    if(notBlank(field)) {
      return field;
    }

    return null;
  }

  /**
   * Returns true if given string is not null and not empty.
   * @param field String to test.
   * @return boolean if not null and not empty.
   */
  boolean notBlank(final String field) {
    return field != null && !field.trim().equals("");
  }

  /**
   * Validates if Passport information is valid by following some pre-set rules
   * from DHS.
   *
   * @param passportCountry passport country.
   * @param passportNumber passport number.
   * @param countryOfIssuance country of issuance.
   * @return boolean if it passes DHS logic.
   */
  boolean validPassportInformation(final String passportCountry,
                                   final String passportNumber,
                                   final String countryOfIssuance) {
    if (notBlank(passportCountry))
    {
      if (!notBlank(passportNumber) || !notBlank(countryOfIssuance))
      {
        return false;
      }
    }

    return true;
  }
}
