<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>

<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/tablesorter.css" />" />
<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
Integer totalResultCount = (Integer)request.getAttribute("resultSize"); 
if(totalResultCount == null){
	totalResultCount = 0;
}
Integer currentPage = (Integer)request.getAttribute("currentPage");
int noOfPage = 0;
 
if(totalResultCount>0){
	if((totalResultCount%pageSizeInt)>0){
		noOfPage = (totalResultCount/pageSizeInt)+1;
	}
	else{
		noOfPage = (totalResultCount/pageSizeInt);
	}
}

%>

<div class="gutter10-lr">
		<div class="l-page-breadcrumb"><!--start page-breadcrumb -->
			<div class="row-fluid">
				<ul class="page-breadcrumb">
		        	<li><a href="javascript:history.back()">&lt; <spring:message  code="label.back"/></a></li>
		        	<li><a href="<c:url value="/broker/employers"/>"><spring:message  code="label.employers"/></a></li>
		        	<li>${desigStatus}</li>
		        </ul>
			</div>		
		</div><!--page-breadcrumb ends-->
		<div class="row-fluid">
		       <div id="sidebar" class="span3" style="width: 100%;">
		            <div class="nav nav-list">
		              <h1><a name="skip"></a><spring:message  code="label.employers"/>
				        <small>
					   <c:choose> 
					    	<c:when test="${not empty fn:trim(desigReq)  }"> ${requestSummaryLabel}</c:when>
							<c:otherwise>${resultSize} ${tableTitle}</c:otherwise>
					   </c:choose>
				        </small>
				      </h1>
				   </div>
				</div>
	    </div>
		<div class="row-fluid">
	                 <c:if test="${message != null}">
						<div class="errorblock alert alert-info">
							<p>${message}</p>
						</div>
					</c:if> 
		<div class="pull-right">
 			<div class="controls">
				<div class="dropdown" id="dropdown">
					<span id="counter" class="gray"></span>
						<span id="bulkActions">
							
							<c:if test="${desigStatus == 'Pending' || desigStatus == 'Active'}">
								<a href="#" data-target="#" data-toggle="dropdown" role="button" id="inactiveLabel" class="dropdown-toggle btn btn-primary btn-small">
									<i class="icon-cog icon-white"></i><b class="caret"></b><spring:message  code="label.brkbulkactions"/>
								</a>
							</c:if>
							
							<ul aria-labelledby="inactiveLabel" role="menu" class="dropdown-menu pull-right">
								<li>
									<c:if test="${(desigStatus == 'Pending')}">
										<a class="" onclick="submitRequest('Active');"><spring:message code="label.brkaccept"/></a>
								</li>
								<li>
										<a id="declinelink" class="validateDecline" onclick="markInactive();" href="#mark-inactive"><spring:message  code="label.brkdecline"/></a>
									</c:if>
									
									<c:if test="${(desigStatus == 'Active')}">
										<a id="declinelink" class="validateDecline" onclick="markInactive();" href="#mark-inactive"><spring:message  code="label.brkMarkAsInactive"/></a>
									</c:if>
									
									<c:if test="${(desigStatus == 'InActive')}">
									<!--<a class="" href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">Send a Message</a>  -->
										<%-- <a class="" href="#" data-toggle="modal"><spring:message  code="label.brksenddesignationreq"/></a> --%>
									</c:if>
								</li>
							</ul>
						</span>		
					</div> <!-- dropdown end -->
			</div> <!-- controls end -->
		</div> <!-- pull-right end -->
		</div><!-- row-fluid end -->
	<div class="row-fluid">		
			<div id="sidebar" class="span3">			
					<div class="header">
				        <h4 class="pull-left"><spring:message code="label.brkRefineResultsBy"/></h4> <a class="pull-right margin5-r" href="#" onclick="resetAll()">(<spring:message code="label.brkResetAll"/>)</a>
			       </div>
					<div class="graybg">
						<form class="form-vertical gutter10" id="employersearch" name="employersearch" action="employers" method="POST">
						<df:csrfToken/>
							<input type="hidden" name="desigStatus" id="desigStatus" value="${param.desigStatus}"/>
							<input type="hidden" name="fromModule"	id="fromModule" value="<encryptor:enc value="employers"/>">												
							<div class="control-group">								
								<label class="control-label" for="firstName"><spring:message code="label.contactFirstName"/></label>
								<div class="controls">
									<input class="span" type="text" id="firstName" name="firstName" value="${searchCriteria.firstName}"
										placeholder="">
								</div>
							</div>
							<div class="control-group">								
								<label class="control-label" for="lastName"><spring:message code="label.contactLastName"/></label>
								<div class="controls">
									<input class="span" type="text" id="lastName" name="lastName" value="${searchCriteria.lastName}"
										placeholder="">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="companyName"><spring:message  code="label.companyName"/></label>
								<div class="controls">
									<input class="span" type="text" id="companyName"  name="companyName" value="${searchCriteria.companyName}"
										placeholder="">
								</div>
							</div>

								
								<c:if test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
								<fieldset>
								<legend><spring:message  code="label.brkrequestsent"/></legend>
								<div class="control-group box-tight margin0">
									<div class="control-group">
										<label class="required control-label" for="requestSentFrom"><spring:message  code="label.brkfrom"/>:</label>
										<div class="input-append date date-picker" id="date">
											<input class="input-small" type="text" name="requestSentFrom" id="requestSentFrom" value="${searchCriteria.requestSentFrom}"/> 
											<span class="add-on"><i class="icon-calendar"></i></span>
											<div class="help-inline" id="requestSentFrom_error"></div>
										</div>
											
									</div>
									
									<div class="control-group">
										<label class="required control-label" for="requestSentTo"><spring:message  code="label.brkto"/>:</label>
										<div class="input-append date date-picker" id="date">
											<input class="input-small" type="text" name="requestSentTo" id="requestSentTo" value="${searchCriteria.requestSentTo}" /> 
											<span class="add-on"><i class="icon-calendar"></i></span>
											<div class="help-inline" id="requestSentTo_error"></div>
										</div>
									</div>
								</div>
								</fieldset>
								</c:if>
								
								<c:if test="${(desigStatus == 'InActive')}">
								<fieldset>
								<legend><spring:message code="label.brkInactiveDate"/></legend>
								<div class="control-group box-tight margin0">
									<div class="control-group">
										<label class="required control-label" for="inactiveDateFrom"><spring:message  code="label.brkfrom"/>:</label>
										<div class="input-append date date-picker" id="date">
											<input class="input-small" type="text" name="inactiveDateFrom" id="inactiveDateFrom" value="${searchCriteria.inactiveDateFrom}"/> 
											<span class="add-on"><i class="icon-calendar"></i></span>
											<div class="help-inline" id="inactiveDateFrom_error"></div>
										</div>
									</div>
									
									<div class="control-group">
										<label class="required control-label" for="inactiveDateTo"><spring:message  code="label.brkto"/>:</label>
										<div class="input-append date date-picker" id="date">
											<input class="input-small" type="text" name="inactiveDateTo" id="inactiveDateTo" value="${searchCriteria.inactiveDateTo}"/> 
											<span class="add-on"><i class="icon-calendar"></i></span>
											<div class="help-inline" id="inactiveDateTo_error"></div>
										</div>
									</div>
								</div>
								</fieldset>
								</c:if>
								
								<div class="gutter10">
								     <input type="submit" name="submit" id="submit" class="btn btn-primary input-small offset3" value="<spring:message code="label.brkGo"/>"/>
								</div>
						</form>
					</div>				
			</div><!-- end of span3 -->
	    
			<div id="rightpanel" class="span9">
				
				<form class="form-vertical" id="brokeremployers" name="brokeremployers" action="employers" method="POST">
				 <df:csrfToken/>
					<input type="hidden" name="prevStatus" id="prevStatus" value="${desigStatus}">
					<input type="hidden" name="ids" id="ids" value="">
					<input type="hidden" name="fromModule"	id="fromModule" value="<encryptor:enc value="employers"/>">
					<input type="hidden" name="desigStatus" id="desigStatus" value="${desigStatus}">
					<div id="brokerlist">
					
						<input type="hidden" name="id" id="id" value="">
						<c:set var="encFromModule" ><encryptor:enc value="employers" isurl="true"/> </c:set>		
						<c:choose>
							
							<c:when test="${fn:length(approvedbrokerslist) > 0}">
								<table class="table tablesorter" id="empTable${desigStatus}" name="empTable${desigStatus}">
									<thead>
										<tr class="header">
											<!-- Please do not change checkbox column header from <td> to <th> -->
											<th scope="col" ><input type="checkbox" value="${broker.employerId}" id="check_all${desigStatus}" name="check_all${desigStatus}"></th>
											<th scope="col" class=" sort-col" style="width: 130px; padding: 0;"><spring:message code="label.employeeList.columnHeader.company"/></th>
											<th scope="col" class=" sort-col" style="width: 130px; padding: 0;"><spring:message code="label.brkcontactname"/></th>
											<th scope="col" class=" sort-col" style="width: 130px; padding: 0;"><spring:message code="label.enrollmentStatus"/></th>
											<th scope="col" class=" sort-col" style="width: 130px; padding: 0;"><spring:message code="label.brkaidingsince"/></th>
											<th scope="col" ><spring:message code="label.brkactions"/></th>
										</tr>
									</thead>
									<c:forEach items="${approvedbrokerslist}" var="broker">
									<c:set var="encryptedId" ><encryptor:enc value="${broker.employerId}" isurl="true"/> </c:set>
										<tr>
								    		<td><input type="checkbox" value="${encryptedId}" class="empCheckbox${desigStatus}" name="empCheckbox${desigStatus}"></td>
								    		<td><a href="../broker/employer/employercase/${encryptedId}">${companyNameMap[broker.employerId]}</a></td>	
											<td>${contactNameMap[broker.employerId]}</td>
											<td>${broker.status}</td>
											<td><fmt:formatDate value="${broker.updated}" type="date" /></td>		
										    <td>
										    	<div class="controls">
													<div class="dropdown">
														<a href="/page.html" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle">
															<i class="icon-cog"></i> <b class="caret"></b>
														</a>
														<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right">
															<!--  <li>
																<a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">Send a Message</a>
															</li>
															-->
															<li>
																<a href="/hix/broker/employer/employercase/${encryptedId}"><spring:message code="label.brkdetails"/></a>
															</li>
															<li>
																<a id="declinelink" class="validateDecline" href="<c:url value="/broker/declinePopup?desigId=${encryptedId}&prevStatus=${desigStatus}&fromModule=${encFromModule}"/>"><spring:message code="label.brkMarkAsInactive"/></a>
															</li>
														</ul>
													</div>
												</div>
										    </td>
										</tr>
									</c:forEach>
								</table>
							</c:when>
							<c:when test="${fn:length(brokerslist) > 0}">								
								<table class="table tablesorter" id="empTable${desigStatus}" name="empTable${desigStatus}">
									<thead>
									<tr class="header">		
											<!-- Please do not change checkbox column header from <td> to <th> -->
											<c:if test="${desigStatus == 'Pending' || desigStatus == 'Active'}">
												<td  scope="col"><input type="checkbox" value="${broker.employerId}" name="check_all${desigStatus}" class="check_all${desigStatus}"></td>
											</c:if>
											<th scope="col"><a href="#"><spring:message code="label.employeeList.columnHeader.company"/></a></th>
											<th scope="col"><a href="#"><spring:message code="label.brkcontactname"/></a></th>
											<c:choose>
												<c:when test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
													<th><a href="#"><spring:message code="label.brkrequestsent"/></a></th>
												</c:when>
												<c:otherwise>
													<th><a href="#"><spring:message code="label.brkinactivesince"/></a></th>
												</c:otherwise>
											</c:choose>
											 <c:if test="${desigStatus!='InActive'}">
											 <th  scope="col"><spring:message code="label.brkactions"/></th>
											</c:if> 
										</tr>
									</thead>
									<c:forEach items="${brokerslist}" var="broker">
										<c:set var="encryptedId" ><encryptor:enc value="${broker.employerId}" isurl="true"/> </c:set>
										<tr>
											
											<c:if test="${desigStatus == 'Pending' || desigStatus == 'Active'}">
												<td><input type="checkbox" value="${encryptedId}" name="empCheckbox${desigStatus}" class="empCheckbox${desigStatus}"></td>
											</c:if>
										    <td><a name="emplink" href="<c:url value="/broker/employer/viewemployerdetail/${encryptedId}"/>" id="emp${broker.employerId}" class="empdetail">${companyNameMap[broker.employerId]}</a></td>
											<td>${contactNameMap[broker.employerId]}</td>
											
											<c:choose>
												<c:when test="${(desigStatus == 'Pending')}">
													<td><fmt:formatDate value="${broker.created}" type="date"/></td>
												</c:when>
												<c:otherwise>
													<td><fmt:formatDate value="${broker.updated}" type="date"/></td>
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${(broker.status == 'Pending')}">
											   		<td>
													   	<div class="controls">
															<div class="dropdown">
																<a href="/page.html" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle">
																	<i class="icon-cog"></i> <b class="caret"></b>
																</a>
																<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right">
																	<li>
																		<a href="<c:url value="/broker/approve/${encryptedId}/${encFromModule}?prevStatus=${desigStatus}"/>"><spring:message code="label.brkaccept"/></a>
																	</li>
																	<c:set var="encryptedId" ><encryptor:enc value="${broker.employerId}" isurl="true"/> </c:set>
																	<li>
																		<a id="declinelink" class="validateDecline" href="<c:url value="/broker/declinePopup?desigId=${encryptedId}&prevStatus=${desigStatus}&fromModule=${encFromModule}"/>"><spring:message code="label.brkdecline"/></a>
																	</li>
																	<!-- <li>
																		<a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">Send a Message</a>
																	</li> -->
																</ul>
															</div>
														</div>	
											   		</td>
											   	</c:when>
											   	
										   	</c:choose>									
										</tr>
									</c:forEach>
								</table>
							</c:when>
							</c:choose>
							<c:choose>
							<c:when test="${fn:length(brokerslist) > 0 || fn:length(approvedbrokerslist) > 0}">		
								<div class="center">
							 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>
							</c:when>
							<c:otherwise>
								<hr class="nmhide" />
								<c:choose>
									<c:when test="$(\"#status option:selected\").val() == 'Pending'">
										<div class="alert alert-info"><spring:message code="label.brkapprovalalert"/></div>
									</c:when>
									<c:otherwise>
										<div class="alert alert-info"><spring:message code="label.brknomatching"/></div>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</div>
				</form>
			</div>
	</div><!-- end of .row-fluid -->
</div> <!-- gutter10-lr end -->		
		<%-- Secure Inbox Start--%>
		<%-- <jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
		<%-- Secure Inbox End--%>
		
		<script type="text/javascript">
		
		$(document).ready(function() { 
			
			$('.date-picker').datepicker({
			    autoclose: true,
			    format: 'mm/dd/yyyy'
			});

			
		    $('table[name="approvedbrokerslist"]').tablesorter({ 
		        // pass the headers argument and assing a object 
		        headers: { 
		            // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            },
		            6: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }
		        }

		    }); 
		});
		var validator = $("#employersearch").validate({
			onkeyup : false,
			onclick : false,
			rules : { requestSentFrom : { chkDateFormat : true},
				requestSentTo : { chkDateFormat : true},
				inactiveDateFrom : { chkDateFormat : true},
				inactiveDateTo : { chkDateFormat : true}
			},
			messages : {
				requestSentFrom : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"},
				requestSentTo : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"},
				inactiveDateFrom : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"},
				inactiveDateTo : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"}
			}
		});
		jQuery.validator.addMethod("chkDateFormat", function(value, element, param) {
			if(value == "") {
				return true;
			}
			
			if($("#firstName").val() != undefined && $("#firstName").val() != "") {
					return true;
			}

			if($("#lastName").val() != undefined && $("#lastName").val() != "") {
				return true;
			}
			
			if($("#companyName").val() != undefined && $("#companyName").val() != "") {
				return true;
			}
			
			var isValid = false;
		    var reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
		    if (reg.test(value)) {
		        var splittedDate = value.split('/');
		        var mm = parseInt(splittedDate[0], 10);
		        var dd = parseInt(splittedDate[1], 10);
		        var yyyy = parseInt(splittedDate[2], 10);
		        var newDate = new Date(yyyy, mm - 1, dd);
		        if ((newDate.getFullYear() == yyyy) && (newDate.getMonth() == mm - 1) && (newDate.getDate() == dd))
		            isValid = true;
		        else
		            isValid = false;
		    }
		    else
		        isValid = false;

		    return isValid;

		});
		
				$(function() {
					$('.datepick').each(function() {
						var ctx = "${pageContext.request.contextPath}";
						var imgpath = ctx+'/resources/images/calendar.gif';
						$(this).datepicker({
							showOn : "button",
							buttonImage : imgpath,
							buttonImageOnly : true
						});
					});
				});
				$(function() {
					$(".empdetail").click(function(e){
				        e.preventDefault();
				        var href = $(this).attr('href');
				        if (href.indexOf('#') != 0) {
				           $('<div id="empdetail" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" title="x" class="close" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body" style="overflow:hidden;"><iframe src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe> </div><div class="modal-footer txt-center"><button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.button.close"/></button></div></div>').modal({backdrop:false});
						}
					});
				});
			    function validate(todo)
			    {
			      var confirmMessage="Are you sure to " + todo + " ?";

			      if(confirm(confirmMessage)==false)
			      {
			          return false;
			      }
			    };			    
			    
			    function closeMe(desigId, desigStatus, desigName){
			    	$('#empdetail').remove();
			    	if(desigStatus=='') {
			    		desigStatus='Pending';
			    	}

			    	var href = "/hix/broker/declinePopup?desigId="+desigId+"&prevStatus="+desigStatus+"&fromModule=${encFromModule}";
			    	$('<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="declinepopup" src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>').modal({backdrop:false});
			    	
			    }
				
				function closeIFrame() {
					$("#declinepopup").remove();
				}
			       
			    $(function() {
					$(".validateDecline").click(function(e){
				        e.preventDefault();
				        var href = $(this).attr('href');
				        if (href.indexOf('#') != 0) {
				           $('<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="declinepopup" src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:190px;"></iframe></div></div>').modal({backdrop:false});
						}
					});
				});

			    
			  //If the Master checkbox is checked or unchchecked, process the following code
			  $('input[name="check_all${desigStatus}"]').click(function() {
				  if( $('input[name="check_all${desigStatus}"]').is(":checked") ) {
					  $('input[name="empCheckbox${desigStatus}"]').each( function() {
						  $(this).attr("checked",true);
					  });
				  } else {
					  $('input[name="empCheckbox${desigStatus}"]').each( function() {
						  $(this).attr("checked",false);
					  });
				  }
			  });
			  //For each of the emp checkboxes, process the following code when they are changed
			  $('input[name="empCheckbox${desigStatus}"]').bind('change', function() {
				  if ( $('input[name="empCheckbox${desigStatus}"]').filter(':not(:checked)').length == 0 ) {
				 	$('input[name="check_all${desigStatus}"]').attr("checked",true);
				  } else {
				 	$('input[name="check_all${desigStatus}"]').attr("checked",false);
				  }
			  });

			function countChecked() { 
				var n = $("input[name='empCheckbox${desigStatus}']:checked").length;
				var itemsSelected = "<small>&#40; "+ n + (n === 1 ? " Item" : " Items") +" Selected &#41;&nbsp;</small>";
				//$(this).find(".counter").text(itemsSelected);
				$("#counter").html(itemsSelected);
				if(n > 0) {
					$("#counter").show();
					$("#bulkActions").show();
				} else { 
					$("#counter").show();
					$("#bulkActions").hide();
				}
			}
			countChecked();
			$(":checkbox").click(countChecked);

			function markInactive() {
				var ids = '';
				$.each($("input[name='empCheckbox${desigStatus}']:checked"), function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
				ids += ',';
				
				var desigStatus='${param.desigStatus}';
				var href = '/hix/broker/declinePopup?desigId=' + ids + '&prevStatus=' + desigStatus + '&fromModule=employers';
				$('<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="bulkdeclinepopup" src="'+ href	+ '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>').modal({backdrop : false});
			}

			function submitRequest(requestType) {
					var ids = '';
					$.each($("input[name='empCheckbox${desigStatus}']:checked"), function() {
						ids += (ids?',':'') + $(this).attr('value');
					});
					ids+=',';

					var prevStatus = "${desigStatus}";
					var validUrl = "";
					if(requestType == 'InActive') {
						validUrl = "declineRequests/employers";
					} else if(requestType == 'Active') {
						validUrl = "approveRequests/employers";
					}
					$.ajax({
						url: validUrl,
					    data: {ids: ids, 
					    		desigStatus: prevStatus},
					    success: function(data){
							window.location.reload(); 
					    }
					});
  			}
			function resetAll() {
				 var  contextPath =  "<%=request.getContextPath()%>";
                var documentUrl = contextPath + "/broker/employers/resetall?desigStatus="+"${desigStatus}";
               window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
               }
			
			</script>
		
