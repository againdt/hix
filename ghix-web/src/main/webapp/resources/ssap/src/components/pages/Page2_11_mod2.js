import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import {
    changeData,
    fetchCounties,
    fetchCountiesForExactZipcodes,
    removeCounty,
    showModal
} from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import normalizeAddress from '../../utils/normalizeAddress';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';



/**
 *
 * @param {Object} props
 * @returns {React.Component} = 'Living separately with Head of household' Page
 *
 */
const Page2_11_mod2  = props => {

    const {
        counties,
        data,
        data: {
            singleStreamlinedApplication: {
                taxHousehold
            }
        },
        form: {
            Form: {
                isFormSubmitted,
                isEligiblyForSave
            }
        },
        environment: {
            stateCode
        },
        headOfHousehold,
        homeAddress,
        householdMembers
    } = props;

    const {
        otherAddresses: allAddresses
    } = taxHousehold[0];

    const {
        isFirstVisit,
        pageName
    } = props;

    // Local State of Functional Component

    const [ localState, setLocalState ] = useState({
        noneOfTheAbove: isFirstVisit ?
            null :
            householdMembers
                .every((hhm, idx) => idx === 0 ? true : !hhm.livesAtOtherAddressIndicator)
    });

    const otherAddresses = data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .otherAddresses
        .filter((a, i) => i > 0);

    // Updating some props in JSON with uncheck 'None of Above' checkbox

    const onFieldChange = (...args) => {

        let [ newData, indexInArray, , [ , value ] ] = args;

        const newState = {...data };

        if(!value) {
            (hhm => {
                hhm.addressId = 1;
                hhm.otherAddress.livingOutsideofStateTemporarilyIndicator = null;
                hhm.householdContact.homeAddress = allAddresses.find(a => a.addressId === 1);
            })(newData);
        }

        const currentState = newData.otherAddress.address.state;

        if (currentState !== householdMembers[indexInArray].otherAddress.address.state) {
            newData.otherAddress.address.countyCode = null;
            newData.otherAddress.address.county = null;
        }

        const isLivingTheSameAddress = newData.livesAtOtherAddressIndicator === false;

        if (isLivingTheSameAddress) {
            newData = {
                ...cloneObject(newData),
                householdContact: {
                    ...newData.householdContact,
                    homeAddress: headOfHousehold.householdContact.homeAddress,
                    mailingAddress: headOfHousehold.householdContact.mailingAddress
                },
                livesWithHouseholdContactIndicator: true,
                otherAddress: cloneObject(EMPTY_OBJECTS.OTHER_ADDRESS),
            };
        } else {
            newData = {
                ...cloneObject(newData),
                householdContact: {
                    ...newData.householdContact,
                    homeAddress: cloneObject(EMPTY_OBJECTS.HOUSEHOLD_CONTACT.homeAddress),
                    mailingAddress: headOfHousehold.householdContact.mailingAddress
                },
                livesWithHouseholdContactIndicator: false
            };
        }

        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        setLocalState(state => ({
            ...state,
            noneOfTheAbove: newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember
                .every(hhm => hhm.addressId !== 1)
        }));
        props.actions.changeData(newState);
    };

    const onFieldChange_outside = (newData, indexInArray) => {
        const isLivingOutside = newData.otherAddress.livingOutsideofStateTemporarilyIndicator === false;

        if(isLivingOutside) {
            newData.otherAddress.livingOutsideofStateTemporarilyAddress =
                cloneObject(EMPTY_OBJECTS.OTHER_ADDRESS.livingOutsideofStateTemporarilyAddress);
        }
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        props.actions.changeData(newState);
    };

    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <livesAtOtherAddressIndicator> and <otherAddress> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = (...args)=> {

        const [ , , , [ , value ] ] = args;
        if(!value) {
            return null;
        }
        setLocalState(state => ({
            ...state,
            noneOfTheAbove: value ? true : null
        }));
        let newState = {...data };
        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member.addressId = 1;
                    member.livesWithHouseholdContactIndicator = true;
                    member.livesAtOtherAddressIndicator = false;
                    member.otherAddress = cloneObject(EMPTY_OBJECTS.OTHER_ADDRESS);
                    member.householdContact.homeAddress = headOfHousehold.householdContact.homeAddress;
                    member.householdContact.mailingAddress = headOfHousehold.householdContact.mailingAddress;
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
    };

    const handleAdding = () => {
        props.actions.showModal(
            MODAL_TYPES.OTHER_ADDRESS_MODAL,
            MODAL_TYPES.OTHER_ADDRESS_MODAL,
            [
                {
                    'postalCode': null,
                    'countyCode': null,
                    'county': null,
                    'primaryAddressCountyFipsCode': null,
                    'streetAddress1': null,
                    'streetAddress2': null,
                    'state': null,
                    'city': null,
                    'addressId': otherAddresses.length + 2
                },
                null,
                pageName
            ]
        );
    };

    const handleEditing = e => {

        const {
            name
        } = e.target;

        const idx = Number(name);

        const payload = data.singleStreamlinedApplication.taxHousehold[0].otherAddresses[idx];

        props.actions.showModal(
            MODAL_TYPES.OTHER_ADDRESS_MODAL,
            MODAL_TYPES.OTHER_ADDRESS_MODAL,
            [
                payload,
                idx,
                pageName
            ]
        );
    };

    const handleDeleting = e => {
        const {
            name
        } = e.target;

        const index = Number(name);

        const newState = { ...data };


        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .otherAddresses
            .splice(index, 1);

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .otherAddresses
            .forEach(address => {
                if(address.addressId > index) {
                    address.addressId = address.addressId - 1;
                }
            });

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember
            .forEach(hhm => {
                if(hhm.addressId - 1 === index) {
                    hhm.addressId = 1;
                }
                if(hhm.addressId - 1 > index) {
                    hhm.addressId = hhm.addressId - 1;
                }
            });

        props.actions.changeData(newState);
    };

    const onFieldChange_address = (...args) => {

        const [ , [ hhmIndex, newAddressId ], , [ , value ]] = args;

        const newState = { ...data };

        if(value) {
            (hhm => {
                hhm.addressId = newAddressId;
                hhm.otherAddress.livingOutsideofStateTemporarilyIndicator = null;
                hhm.householdContact.homeAddress = otherAddresses.find(a => a.addressId === newAddressId);
            })(newState.singleStreamlinedApplication.taxHousehold[0].householdMember[hhmIndex]);
        } else {
            (hhm => {
                hhm.addressId = 1;
                hhm.otherAddress.livingOutsideofStateTemporarilyIndicator = null;
                hhm.householdContact.homeAddress = allAddresses.find(a => a.addressId === 1);
            })(newState.singleStreamlinedApplication.taxHousehold[0].householdMember[hhmIndex]);
        }

        props.actions.changeData(newState);
    };


    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = householdMembers => {
        const members = [];
        const { noneOfTheAbove } = localState;

        householdMembers.forEach((member, i) => {
            const {
                livesAtOtherAddressIndicator,
                otherAddress: {
                    livingOutsideofStateTemporarilyIndicator,
                },
                name,
                addressId
            } = member;

            const checkIfPersonLivesOutsideState = () => {
                return (neededState => neededState ? neededState.state : null)(otherAddresses.find(address => address.addressId === addressId)) !== stateCode &&
                addressId !== 1;
            };

            if (i !== 0) {
                members.push(
                    <Fragment key={i}>
                        <fieldset className="usa-fieldset-inputs usa-sans">
                            <legend className="usa-sr-only">Check if {normalizeNameOfPerson(name)} live at address other than the stated address</legend>
                            <ul className="usa-unstyled-list">
                                <li>
                                    <UniversalInput
                                        label={{
                                            text: normalizeNameOfPerson(name)
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            classes:[],
                                            name: 'livesAtOtherAddressIndicator',
                                            checked: livesAtOtherAddressIndicator,
                                            value: livesAtOtherAddressIndicator,
                                            currentData: member,
                                            fields: [],
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={i}
                                    />
                                </li>
                            </ul>
                        </fieldset>

                        {/**********************************************
                         Begin - If #livesAtOtherAddressIndicator (Yes)
                         ***********************************************/}

                        {

                            livesAtOtherAddressIndicator &&

                            <Fragment>
                                {
                                    otherAddresses.length === 0 &&
                                        <p>
                                            Please provide your address
                                        </p>
                                }

                                {
                                    otherAddresses.map((address, idx) => {
                                        return (
                                            <div key={`${address.addressLine1}_${idx}`} className="usa-width-one-whole address_l2">
                                                <UniversalInput
                                                    label={{
                                                        text: normalizeAddress(address)
                                                    }}
                                                    inputData={{
                                                        isInputRequired: false,
                                                        type: INPUT_TYPES.CHECKBOX,
                                                        name: 'addressId',
                                                        classes:['usa-width-one-whole'],
                                                        checked: address.addressId === addressId,
                                                        value: address.addressId,
                                                        currentData: {addressId: address.addressId},
                                                        fields: [],
                                                    }}
                                                    inputActions={{
                                                        onFieldChange: onFieldChange_address
                                                    }}
                                                    indexInArray={[i, address.addressId]}
                                                />
                                                <div className='usa-width-one-whole margin-b-10 margin-l-5ve'>
                                                    <button
                                                        className='btn-small btn-secondary'
                                                        onClick={handleDeleting}
                                                        name={idx + 1}
                                                    >
                                                        Delete
                                                    </button>
                                                    <button
                                                        className='btn-small btn-secondary'
                                                        onClick={handleEditing}
                                                        name={idx + 1}
                                                    >
                                                        Edit
                                                    </button>
                                                </div>
                                            </div>

                                        );

                                    })
                                }

                                <button
                                    onClick={handleAdding}
                                    className='btn-small btn-secondary'
                                >
                                    Add Address
                                </button>

                                {
                                    checkIfPersonLivesOutsideState() &&

                                    <UniversalInput
                                        legend={{
                                            legendText:
                                                <span>
                                                    Is &nbsp;
                                                    <strong>
                                                        {normalizeNameOfPerson(name)} &nbsp;
                                                    </strong>
                                                    living outside Nevada temporarily?
                                                </span>,
                                        }}
                                        label={{
                                            text: '',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-whole margin-b-20'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'livingOutsideofStateTemporarilyIndicator',
                                            value: livingOutsideofStateTemporarilyIndicator,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: member,
                                            fields: ['otherAddress']
                                        }}
                                        errorClasses={['margin-l-20-ve margin-b-10-ve']}
                                        inputActions={{
                                            onFieldChange : onFieldChange_outside
                                        }}
                                        indexInArray={i}
                                    />
                                }
                            </Fragment>
                        }

                        {/**********************************************
                         End - If #livesAtOtherAddressIndicator (Yes)
                         ***********************************************/}

                    </Fragment>
                );
            }
        });

        members.push(
            <UniversalInput
                key={members.length + 1}
                label={{
                    text: 'None of the Above'
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    classes:[],
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );

        return members;
    };

    const checkTopLevelValidation = () => {
        let result = true;
        householdMembers.forEach((hhm, hhmIndex) => {
            if(hhmIndex !== 0 && hhm.livesWithHouseholdContactIndicator === false && hhm.addressId === 1) {
                result = false;
            }
        });

        if(
            isFirstVisit
            && householdMembers.some(hhm => hhm.livesAtOtherAddressIndicator)
            && result
            && localState.noneOfTheAbove === null
        ) {
            return true;
        }

        switch(localState.noneOfTheAbove) {
        case null:
            return false;
        case true:
            return true;
        case false:
            return result;
        }
    };

    return (
        <Fragment>
            <div id="fhAdditionalInfo" className="fade-in">
                <div className="subsection">
                    <PageHeader
                        text="Where Household Members Live"
                        headerClassName={'usa-heading-alt'}
                    />
                    <Form topLevelValidity={checkTopLevelValidation()}>
                        <p className="required-true">
                            Do any of the people below live at an address other than &nbsp;
                            <strong>
                                {normalizeAddress(homeAddress)}
                                ? &nbsp;
                            </strong>
                            (check all that apply)
                        </p>
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isEligiblyForSave}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve']}
                        />
                        {
                            getHouseholdMembers(householdMembers, counties)
                        }
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    counties: state.counties,
    data: state.data,
    environment: state.headerLinks,
    form: state.form,

    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0],

    homeAddress: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0]
        .householdContact
        .homeAddress,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        fetchCounties,
        fetchCountiesForExactZipcodes,
        removeCounty,
        showModal
    }, dispatch)
});

Page2_11_mod2.defaultProps = {
    name: 'Page2_11_mod2'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_11_mod2);
