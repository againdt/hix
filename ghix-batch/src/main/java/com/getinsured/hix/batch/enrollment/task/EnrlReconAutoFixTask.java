package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlReconAutoFixService;

public class EnrlReconAutoFixTask implements Tasklet {
	private static  Logger LOGGER = LoggerFactory.getLogger(EnrlReconAutoFixTask.class);
	private EnrlReconAutoFixService enrlReconAutoFixService;
	private String fileId;
	private Job job;
	private JobLauncher jobLauncher;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		// TODO Auto-generated method stub
		LOGGER.info("EnrlReconAutoFixTask.execute : Start");
		if(fileId!=null && !fileId.trim().equalsIgnoreCase("")){
			enrlReconAutoFixService.processAutoFix(Integer.valueOf(fileId), job, jobLauncher);
		}
		LOGGER.info("EnrlReconAutoFixTask.execute : END");
		return RepeatStatus.FINISHED;
	}
	
	public EnrlReconAutoFixService getEnrlReconAutoFixService() {
		return enrlReconAutoFixService;
	}

	public void setEnrlReconAutoFixService(EnrlReconAutoFixService enrlReconAutoFixService) {
		this.enrlReconAutoFixService = enrlReconAutoFixService;
	}

	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}
}
