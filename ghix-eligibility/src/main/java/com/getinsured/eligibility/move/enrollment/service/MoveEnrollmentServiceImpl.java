package com.getinsured.eligibility.move.enrollment.service;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.ObjectFactory;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;

@Service("moveEnrollmentService")
public class MoveEnrollmentServiceImpl implements MoveEnrollmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MoveEnrollmentServiceImpl.class);

	private static final String MOVE_ENROLLMENT = "MOVE_ENROLLMENT";

	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IntegrationLogService integrationLogService;

	@Override
	public String process(MoveEnrollmentRequest request) {

		String responseValue = GhixConstants.RESPONSE_FAILURE;

		validateIncomingRequest(request);

		SsapApplication enrolledApp = fetchEnrolledApp(request);
		SsapApplication newApp = fetchCurrentApp(request);

		if (newApp.getCmrHouseoldId().compareTo(enrolledApp.getCmrHouseoldId()) != 0){
			String errorMsg = "Households are not same for suppiled newSsapApplicationId and enrolledSsapApplicationId ";
			LOGGER.error(errorMsg);
			logData(enrolledApp.getId(), errorMsg, responseValue);
			throw new GIRuntimeException(errorMsg);
		}

		EnrollmentRequest enrollmentRequest = populateMoveEnrollmentRequest(enrolledApp, newApp);
		Gson gson = new Gson();
		/*XStream xStream = GhixUtils.getXStreamStaxObject();*/
		/*String enrollmentRequestJson = xStream.toXML(enrollmentRequest );*/
		String enrollmentRequestJson = gson.toJson(enrollmentRequest);
		try {
			
			String enrollmentResponseJsonString =  (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL,
					request.getUserName(),
					HttpMethod.POST,
					MediaType.APPLICATION_JSON,
					String.class,
					enrollmentRequestJson)).getBody();

			if (StringUtils.isEmpty(enrollmentResponseJsonString)){
				String errorMsg = "Unable to update Moving Enrollment - Admin Update Enrollment Response is - empty - for request - " + enrollmentRequestJson;
				LOGGER.error(errorMsg);
				logData(newApp.getId(), errorMsg, responseValue);
				throw new GIRuntimeException(errorMsg);
			}

			EnrollmentResponse enrollmentResponse = gson.fromJson(enrollmentResponseJsonString, EnrollmentResponse.class);
			if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus()) && enrollmentResponse.getErrCode()==200){
				responseValue = GhixConstants.RESPONSE_SUCCESS;
				String messageString = "Moving Enrollment - Admin Update Enrollment Response is  - " + GhixConstants.RESPONSE_SUCCESS + " - for request - " + enrollmentRequestJson;
				logData(newApp.getId(), messageString, responseValue);
			} else{
				String errorMsg = "Unable to update Moving Enrollment - Admin Update Enrollment Response is  - " + GhixConstants.RESPONSE_FAILURE + " - for request - " + enrollmentRequestJson;
				LOGGER.error(errorMsg);
				logData(newApp.getId(), errorMsg, responseValue);
				throw new GIRuntimeException(errorMsg);
			}

		} catch(Exception e) {

			String errorMsg = "Exception occurred while invoking Moving Enrollment Rest API - Admin Update Enrollment - for request - " + enrollmentRequestJson + " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorMsg);
			logData(enrolledApp.getId(), errorMsg, responseValue);
			throw new GIRuntimeException(errorMsg);
		}

		return responseValue;

	}

	private SsapApplication fetchCurrentApp(MoveEnrollmentRequest request) {
		SsapApplication newApp = ssapApplicationRepository.findOne(request.getNewSsapApplicationId());

		if (newApp == null || newApp.getCmrHouseoldId() == null){
			String errorMsg = "No Application or attached household found for newSsapApplicationId - " + request.getNewSsapApplicationId();
			logData(null, errorMsg, GhixConstants.RESPONSE_FAILURE);
			throw new GIRuntimeException(errorMsg);
		}
		return newApp;
	}

	private SsapApplication fetchEnrolledApp(MoveEnrollmentRequest request) {
		SsapApplication enrolledApp = ssapApplicationRepository.findOne(request.getEnrolledSsapApplicationId());

		if (enrolledApp == null || enrolledApp.getCmrHouseoldId() == null){
			String errorMsg = "No Application or attached household found for enrolledSsapApplicationId - " + request.getEnrolledSsapApplicationId();
			logData(null, errorMsg, GhixConstants.RESPONSE_FAILURE);
			throw new GIRuntimeException(errorMsg);
		}
		return enrolledApp;
	}

	private void validateIncomingRequest(MoveEnrollmentRequest request) {

		StringBuilder errorMsg = new StringBuilder();
		if (null == request){
			errorMsg.append("MoveEnrollmentRequest object is null.");
		} else {

			if (StringUtils.isEmpty(request.getUserName())){
				errorMsg.append("MoveEnrollmentRequest.userName is empty.");
			}

			if (request.getEnrolledSsapApplicationId() == null){
				errorMsg.append("MoveEnrollmentRequest.enrolledSsapApplicationId is null.");
			}

			if (request.getNewSsapApplicationId() == null){
				errorMsg.append("MoveEnrollmentRequest.newSsapApplicationId is null.");
			}
			
			if (request.getEnrolledSsapApplicationId() != null && request.getNewSsapApplicationId() != null && request.getNewSsapApplicationId().compareTo(request.getEnrolledSsapApplicationId()) == 0){
				errorMsg.append("MoveEnrollmentRequest.newSsapApplicationId is same as MoveEnrollmentRequest.enrolledSsapApplicationId.");
			}
		}

		if (!StringUtils.isEmpty(errorMsg.toString())){
			logData(null, errorMsg.toString(), GhixConstants.RESPONSE_FAILURE);
			throw new GIRuntimeException(errorMsg.toString());
		}


	}

	public EnrollmentRequest populateMoveEnrollmentRequest(SsapApplication enrolledApp, SsapApplication newApp) {

		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		final ObjectFactory objectFactory = new ObjectFactory();
		AdminUpdateIndividualRequest adminUpdateIndRequest = objectFactory.createAdminUpdateIndividualRequest();
		enrollmentRequest.setAdminUpdateIndividualRequest(adminUpdateIndRequest);

		AdminUpdateIndividualRequest.Household household = objectFactory.createAdminUpdateIndividualRequestHousehold();

		household.setSsapApplicationid(enrolledApp.getId());
		household.setIsOnlySsapAppIdUpdated(true);
		household.setUpdatedSsapApplicationid(newApp.getId());

		adminUpdateIndRequest.setHousehold(household);

		enrollmentRequest.setAdminUpdateIndividualRequest(adminUpdateIndRequest);

		return enrollmentRequest;

	}

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, MOVE_ENROLLMENT, status, ssapApplicationId, null);
	}

}
