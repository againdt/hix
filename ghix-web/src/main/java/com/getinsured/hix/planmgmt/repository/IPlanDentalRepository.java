package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanHealth;

public interface IPlanDentalRepository extends JpaRepository<PlanDental, Integer>, RevisionRepository<PlanDental, Integer, Integer> {
	@Query("SELECT planDental.id,planDental.plan.id FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId) and planDental.costSharing = :costSharing ")
    Object[] getPlanDentalIdByParentId(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);
	
	@Query("SELECT planDental.acturialValueCertificate, planDental.benefitFile, planDental.costSharing FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId)")
    List<Object[]> getPlanDentalDetails(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planDental.plan.id FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId) and planDental.costSharing = :costSharing ")
    List<Integer> getCostSharingPlanIds(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);

    @Query("SELECT planDental.plan.id FROM PlanDental planDental WHERE planDental.parentPlanId = :parentPlanId ")
    List<Integer> getChildPlanIds(@Param("parentPlanId") Integer parentPlanId);
    
    @Query("SELECT planDental.id FROM PlanDental planDental WHERE planDental.plan.id = :planId ")
    Integer getPlanDentalId(@Param("planId") Integer planId);
    
    @Query("SELECT pd FROM PlanDental pd WHERE pd.plan.issuer.id=:issuerID and pd.plan.id = :planID and pd.planLevel=:planLevelString")
    List<PlanDental> getPlanDentalInfo(@Param("issuerID") int issuerID,@Param("planID") int planID,@Param("planLevelString") String planLevelString);

    @Query("SELECT pd FROM PlanDental pd WHERE pd.plan.id = :planID and pd.planLevel=:planLevelString")
    List<PlanDental> getPlanDentalInfoList(@Param("planID") int planID,@Param("planLevelString") String planLevelString);

}