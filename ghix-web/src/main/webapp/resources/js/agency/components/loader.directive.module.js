(function() {
    'use strict';

    angular.module('spinningLoaderDirectiveModule', [])
    .directive('spinningLoader', spinningLoader);
    

    spinningLoader.$inject = ['$timeout'];
	
	function spinningLoader($timeout){
		return {
			restrict: "EA", 
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="/hix/resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;                   
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };           
	            }; 
	         });
	      }
		};
	}



})();