package com.getinsured.eligibility.ssap.integration.lce.service;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.google.gson.Gson;

@Component(value = "lceActivator")
public class LCEServiceActivator {
	private static final Logger LOGGER = LoggerFactory.getLogger(LCEServiceActivator.class);
	@Autowired private Gson platformGson;
	@SuppressWarnings("unchecked")
	public Message<String> receive(Message<SsapApplicationEvent> message) {
 		SsapApplicationEvent applicationEvent = message.getPayload();
 		String correlationId = message.getHeaders().get(SSAPIntegrationConstants.CORRELATION_ID_HEADER).toString();
 		String ssapJSON = applicationEvent.getSsapApplication().getApplicationData();
 		  
 		JSONObject payload = new JSONObject();
		payload.put(SSAPIntegrationConstants.SSAP_JSON, ssapJSON);
		payload.put(SSAPIntegrationConstants.CORRELATION_ID_HEADER, correlationId);
		payload.put(SSAPIntegrationConstants.APPLICATION_EVENT_ID, String.valueOf(applicationEvent.getId()));
		payload.put("SSAP_SEP_INTEGRATION_DTO", platformGson.toJson(message.getHeaders().get("SSAP_SEP_INTEGRATION_DTO")));
		payload.put(SSAPIntegrationConstants.APPLICATION_EVENT_TYPE, String.valueOf(applicationEvent.getEventType().value()));
		Message<String> householdAndIncomeFlowMessage = MessageBuilder
				.withPayload(payload.toJSONString())
				.setHeader("Content-Type", "application/json")
				.build();
 		LOGGER.debug("Sending ssap JSON to household flow: " + householdAndIncomeFlowMessage.getPayload().toString());
		return householdAndIncomeFlowMessage;
	}
}
