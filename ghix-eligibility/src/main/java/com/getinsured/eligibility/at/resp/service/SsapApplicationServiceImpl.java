package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionRequest;
import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Service("ssapApplicationService")
@Transactional
public class SsapApplicationServiceImpl implements SsapApplicationService {


	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private Gson platformGson; 
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired private GhixRestTemplate ghixRestTemplate;

	private static final Logger LOGGER = Logger.getLogger(SsapApplicationServiceImpl.class);

	@Override
	public List<SsapApplication> findByCaseNumber(String caseNumber){

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumberOne(caseNumber);
		/*if (ssapApplications != null && ssapApplications.size() > 0) {

			if (ssapApplications.size() > 1) {
				throw new GIException("More than one ssap application found for case number - " + caseNumber);
			}

			ssapApplication = ssapApplications.get(0);
		}*/

		return ssapApplications;
	}

	@Override
	public List<SsapApplication> findByExternalCaseNumber(String caseNumber) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByExternalApplicationId(caseNumber);
		/*if (ssapApplications != null && ssapApplications.size() > 0) {

			if (ssapApplications.size() > 1) {
				throw new GIException("More than one ssap application found for external case number - " + caseNumber);
			}

			ssapApplication = ssapApplications.get(0);
		}*/

		return ssapApplications;
	}

	@Override
	public void closeApplication(SsapApplication application){
		/*SsapApplication ssapApplication = ssapApplicationRepository.findOne(application.getId());
		ssapApplication.setApplicationStatus("CL");*/
		ssapApplicationRepository.updateApplicationStatusById("CL", application.getId(), new Timestamp(new TSDate().getTime()));
	}

	@Override
	public void closeApplication(long applicationId) {
		ssapApplicationRepository.updateApplicationStatusById("CL", applicationId, new Timestamp(new TSDate().getTime()));
	}

	@Override
	public List<SsapApplication> findByHHIdAndCoverageYear(BigDecimal householdId,Long coverageYear) {
		return ssapApplicationRepository.findByHHIdAndCoverageYear(householdId,coverageYear);
	}

	@Override
	public List<SsapApplication> findByHHCaseIdAndCoverageYear(String caseNumber,Long coverageYear) {
		return ssapApplicationRepository.findByHHCaseIdAndCoverageYear(caseNumber,coverageYear);
	}
	
	@Override
	public boolean checkIsEditApplicationFlow(long ssapId) throws Exception {
		SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapId);
		if (ssapApplication != null) {
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			if (singleStreamlinedApplication.getIsEditApplication() == null) {
				return false;
			}
			return singleStreamlinedApplication.getIsEditApplication();
		}
		return false;
	}
	
	@Override
	public void updateIsEditApplicationFlag(String caseNumber, Boolean isEditApplicationFlag) {
	    SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
        if(ssapApplication != null){
        	SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
        	singleStreamlinedApplication.setIsEditApplication(isEditApplicationFlag);
        	ssapApplication.setApplicationData(ssapJsonBuilder.transformToJson(singleStreamlinedApplication));
        	ssapApplicationRepository.save(ssapApplication);
        	LOGGER.info("Saved isEditApplication Flag to :"+isEditApplicationFlag);
        }
	}
	
	@Override
	public EligEngineHouseholdCompositionResponse getHouseholdComposition(Long ssapApplicationId){
		try {
			EligEngineHouseholdCompositionRequest eligEngineHouseholdCompositionRequest = new EligEngineHouseholdCompositionRequest();
			eligEngineHouseholdCompositionRequest.setApplicationId(ssapApplicationId);
			eligEngineHouseholdCompositionRequest.setRequestId(String.valueOf(2));
			
			  
			EligEngineHouseholdCompositionResponse eligEngineHouseholdCompositionResponse = null;
			eligEngineHouseholdCompositionResponse = this.ghixRestTemplate.postForObject(
					GhixEndPoints.EligibilityEndPoints.HOUSEHOLD_COMPOSITION, eligEngineHouseholdCompositionRequest,
					EligEngineHouseholdCompositionResponse.class);
			if("sucess".equals(eligEngineHouseholdCompositionResponse.getStatus())){
				return eligEngineHouseholdCompositionResponse;
			}else{
				LOGGER.error("Response not ok from eligibility engine " + eligEngineHouseholdCompositionResponse.getStatus());
			}
		} catch (Exception e) {
			LOGGER.error(e.getStackTrace());
		}
		return null;
	}
	
	@Override
	public SsapApplication findById(long id) {
		return ssapApplicationRepository.findOne(id);
	}
	
}
