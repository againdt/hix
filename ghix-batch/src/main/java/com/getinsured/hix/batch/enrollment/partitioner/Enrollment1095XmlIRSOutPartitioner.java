package com.getinsured.hix.batch.enrollment.partitioner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095XmlParams;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentIds;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlWrapper;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

import us.gov.treasury.irs.annual.common.BatchCategoryCodeType;

@Component("enrollment1095XmlIRSOutPartitioner")
@Scope("step")
public class Enrollment1095XmlIRSOutPartitioner implements Partitioner  {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlIRSOutPartitioner.class);

	private String strYear;
	int applicableYear;
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private String irsOutboundCommitInterval;

	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;

	private EnrollmentIds enrollmentIdList;
	private Enrollment1095XmlParams enrollment1095XmlParams;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		
		List<BatchJobExecution> batchExecutionList = enrollmentAnnualIrsReportService.getRunningBatchList("enrollment1095XmlPartitionedJob");
		
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			ExecutionContext executionContext = null;
			try{

				boolean status = EnrollmentUtils.cleanDirectory(EnrollmentUtils
						.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
								EnrollmentConstants.TRANSFER_DIRECTION_OUT)
						.append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME).toString());
				LOGGER.info("Folder cleaning status in IRS Annual partitioner is :: "+status);
				setApplicableMonthYear();
				if(stepExecution!=null){
					executionContext=stepExecution.getJobExecution().getExecutionContext();
					if(executionContext!=null){
						executionContext.put("year", applicableYear);
					}
				}
				enrollment1095XmlParams.resetMap();
				// Get enrollment 1095 records
				// Get records for fresh run
				// Get records for correction regeneration by original batch id,batch category code and flag
				List<String> batchCategoryCodeList = getOutboundBatchCategoryCodes();
				List<String> voidBatchCategoryCodeList = getVoidBatchCategoryCodes();
				int partitionSeq = 1;
				Date batchStartTime = new Date();
				if(applicableYear != -1){
					List<Integer> enrollment1095Ids = enrollmentAnnualIrsReportService.getFreshEnrollment1095IdsFromStaging(applicableYear);
					if(null != enrollment1095Ids && !enrollment1095Ids.isEmpty()){
						putToBatchPartitionMap(partitionMap, partitionSeq);
						enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(enrollment1095Ids, true, null, null, applicableYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq)));
						partitionSeq++;
					}

				}else{
					List<Integer> applicableYears = enrollmentAnnualIrsReportService.getCoverageYearListForFreshRun();
					if(null != applicableYears && !applicableYears.isEmpty()){
						for(Integer year : applicableYears){
							List<Integer> enrollment1095Ids = enrollmentAnnualIrsReportService.getFreshEnrollment1095IdsFromStaging(year);
							if(null != enrollment1095Ids && !enrollment1095Ids.isEmpty()){
								putToBatchPartitionMap(partitionMap, partitionSeq);
								enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(enrollment1095Ids, true, null, null, year, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq)));
								partitionSeq++;
							}
						}
					}
				}
				
				// fetch correction and regen records
				// Logic to partition correction enrollment 1095 ids based on original batch id and other criterias
				List<String> originalBatchIdList = new ArrayList<String>();
				originalBatchIdList = enrollmentAnnualIrsReportService.getOriginalBatchIdListFromStaging();
				
				if(EnrollmentUtils.isNotNullAndEmpty(originalBatchIdList)){
					for(String batchId : originalBatchIdList){
						//For intial
						Integer coverageYear = enrollmentAnnualIrsReportService.getCoverageYearOfOriginalBatchId(batchId);
						Boolean resubmitFlag = Boolean.FALSE;
					
						List<Integer> intialIds = enrollmentAnnualIrsReportService.getCorrectionEnrollment1095ByBatchIdAndCategoryCode(batchId, BatchCategoryCodeType.IRS_EOY_REQ.name());
						List<Integer> submitCorrectionList = getCorrectionsFromStaging(batchId, coverageYear, resubmitFlag);
						resubmitFlag = Boolean.TRUE;
						List<Integer> reSubmitCorrectionList = getCorrectionsFromStaging(batchId, coverageYear, resubmitFlag);
//						List<Integer> firstSubmitIds = enrollmentAnnualIrsReportService.getCorrectionEnrollment1095ByBatchIdAndCategoryCode(batchId, EnrollmentConstants.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ);
//						List<Integer> reSubmitIds = enrollmentAnnualIrsReportService.getCorrectionEnrollment1095ByBatchIdAndCategoryCode(batchId, EnrollmentConstants.IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ);
						
						//For void records
						List<Integer> voidRecordIds = enrollmentAnnualIrsReportService.getInitialVoidEnrollment1095ByBatchId(batchId);
						List<Integer> voidResubmitRecordIds = enrollmentAnnualIrsReportService.getVoidEnrollment1095ByBatchIdAndCategoryCode(batchId, voidBatchCategoryCodeList);
						
						if(null != intialIds && !intialIds.isEmpty()){
							enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(intialIds, false, batchId, BatchCategoryCodeType.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ.name(), coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq)));
							putToBatchPartitionMap(partitionMap, partitionSeq);
							partitionSeq++;
						}
						if(null != submitCorrectionList && !submitCorrectionList.isEmpty()){
							enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(submitCorrectionList, false, batchId, BatchCategoryCodeType.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ.name(), coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq)));
							putToBatchPartitionMap(partitionMap, partitionSeq);
							partitionSeq++;
						}
						if(null != reSubmitCorrectionList && !reSubmitCorrectionList.isEmpty()){
							enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(reSubmitCorrectionList, false, batchId, BatchCategoryCodeType.IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ.name(), coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq)));
							putToBatchPartitionMap(partitionMap, partitionSeq);
							partitionSeq++;
						}
						if(null != voidRecordIds && !voidRecordIds.isEmpty()){
							enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(voidRecordIds, false, batchId, BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name(), coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq), true));
							putToBatchPartitionMap(partitionMap, partitionSeq);
							partitionSeq++;
						}
						if(null != voidResubmitRecordIds && !voidResubmitRecordIds.isEmpty()){
							enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(voidResubmitRecordIds, false, batchId, BatchCategoryCodeType.IRS_EOY_RESUBMIT_VOID_RECORDS_REQ.name(), coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq), true));
							putToBatchPartitionMap(partitionMap, partitionSeq);
							partitionSeq++;
						}
						//For regeneration of XML, check Inbound action as REGEN_XML and keep Batch Category Code same
						
						for(String batchCategoryCode : batchCategoryCodeList){
							List<Integer> regenerateIdList = enrollmentAnnualIrsReportService.getRegenerateEnrollment1095ByBatchIdAndCategoryCode(batchId, batchCategoryCode);
							if(null != regenerateIdList && !regenerateIdList.isEmpty()){
								if(BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name().equalsIgnoreCase(batchCategoryCode) 
										|| BatchCategoryCodeType.IRS_EOY_RESUBMIT_VOID_RECORDS_REQ.name().equalsIgnoreCase(batchCategoryCode)){
									enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(regenerateIdList, false, batchId, batchCategoryCode, coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq), true, true));	
								}else{
									enrollment1095XmlParams.putToModifiedPartitionListMap(partitionSeq, new Enrollment1095XmlWrapper(regenerateIdList, false, batchId, batchCategoryCode, coverageYear, getBatchId(batchStartTime, partitionSeq), getFileCreationTimeStamp(batchStartTime, partitionSeq), false, true));	
								}
								putToBatchPartitionMap(partitionMap, partitionSeq);
								partitionSeq++;
							}
						}
					}
				}

			}catch(Exception e){
				LOGGER.error("Enrollment1095XmlIRSOutPartitioner failed to execute : ", e);
				throw new RuntimeException("Enrollment1095XmlIRSOutPartitioner failed to execute : " + e.getMessage()) ;
			}
			return partitionMap;
		}
		
		return null;
		
	}
	
	private List<String> getOutboundBatchCategoryCodes(){
		List<String> batchCategoryCodeList = new ArrayList<String>();
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_VOID_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_FILE_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_MISSING_FILE_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_REJECTED_OR_MISSING_FILE_REQ.name());
		return batchCategoryCodeList;
	}
	
	private List<String> getVoidBatchCategoryCodes(){
		List<String> voidBatchCategoryCodeList = new ArrayList<String>();
		voidBatchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name());
		voidBatchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_VOID_RECORDS_REQ.name());
		return voidBatchCategoryCodeList;
	}

	private List<Integer> getCorrectionsFromStaging(String batchId, Integer coverageYear, Boolean resubmitFlag) {
		
		List<String> batchCategoryCodeList = new ArrayList<String>();
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_FILE_REQ.name());
		batchCategoryCodeList.add(BatchCategoryCodeType.IRS_EOY_RESUBMIT_MISSING_FILE_REQ.name());
		List<Integer> submitCorrectionList = enrollmentAnnualIrsReportService.getCorrectionListFromStaging(batchId, coverageYear, batchCategoryCodeList, resubmitFlag);
		return submitCorrectionList;
	}

	/**
	 * Get Batch Id 
	 * @param batchStartTime 
	 * @param partitionId 
	 * @return String batchId  yyyy-MM-dd'T'HH:mm:ss'Z'
	 */
	private String getBatchId(Date batchStartTime, int partitionId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Calendar cal = Calendar.getInstance();
		cal.setTime(batchStartTime);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) + partitionId);
		return sdf.format(cal.getTime()); 
	}
	
	/**
	 * Get .IN creation timeStamp 
	 * @param batchStartTime 
	 * @param partitionId 
	 * @return String batchId 'D'yyMMdd'.T'HHmmssSSS
	 */
	private String getFileCreationTimeStamp(Date batchStartTime, int partitionId) {
		SimpleDateFormat sdf = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
		Calendar cal = Calendar.getInstance();
		cal.setTime(batchStartTime);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) + partitionId);
		return sdf.format(cal.getTime()); 
	}


	private void putToBatchPartitionMap(Map<String, ExecutionContext> partitionMap, int partitionSeq) {
		ExecutionContext value = new ExecutionContext();
		value.putInt("partition",  partitionSeq);
		value.putInt("year", applicableYear);
		partitionMap.put("partition - " + partitionSeq, value);
		LOGGER.debug("Enrollment1095XmlIRSOutPartitioner : partition():: partition map entry end for partition :: "+partitionSeq);
	}

	private void setApplicableMonthYear() throws GIException {
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if (NumberUtils.isNumber(strYear)) {
			intYear = Integer.parseInt(strYear);
			if ((intYear > 2014 && intYear < currentYear) || intYear == -1) {
				applicableYear = intYear;
			} else {
				throw new GIException("Please provide year (yyyy) < current year");
			}
		} else {
			throw new GIException("Please provide valid year(yyyy)");
		}

	}

	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = "-1";
		} else {
			this.strYear = strYear;
		}
	}
	public EnrollmentIds getEnrollmentIdList() {
		return enrollmentIdList;
	}


	public void setEnrollmentIdList(EnrollmentIds enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}


	public String getIrsOutboundCommitInterval() {
		return irsOutboundCommitInterval;
	}

	public void setIrsOutboundCommitInterval(String irsOutboundCommitInterval) {
		this.irsOutboundCommitInterval = irsOutboundCommitInterval;
	}


	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}


	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}


	public Enrollment1095XmlParams getEnrollment1095XmlParams() {
		return enrollment1095XmlParams;
	}


	public void setEnrollment1095XmlParams(Enrollment1095XmlParams enrollment1095XmlParams) {
		this.enrollment1095XmlParams = enrollment1095XmlParams;
	}
}
