
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Agency3InitVerifRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Agency3InitVerifRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}Agency3InitVerifRequestSet" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency3InitVerifRequestType", propOrder = {
    "agency3InitVerifRequestSet"
})
@XmlRootElement(name = "Agency3InitVerifRequest")
public class Agency3InitVerifRequestType {

    @XmlElement(name = "Agency3InitVerifRequestSet", required = true)
    protected List<Agency3InitVerifRequestSetType> agency3InitVerifRequestSet;

    /**
     * Gets the value of the agency3InitVerifRequestSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agency3InitVerifRequestSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgency3InitVerifRequestSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Agency3InitVerifRequestSetType }
     * 
     * 
     */
    public List<Agency3InitVerifRequestSetType> getAgency3InitVerifRequestSet() {
        if (agency3InitVerifRequestSet == null) {
            agency3InitVerifRequestSet = new ArrayList<Agency3InitVerifRequestSetType>();
        }
        return this.agency3InitVerifRequestSet;
    }

}
