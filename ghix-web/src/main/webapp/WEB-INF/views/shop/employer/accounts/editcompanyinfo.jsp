<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="gutter10">
  <div class="row-fluid">
  		<ul class="page-breadcrumb">
            <li><a href="#">&lt; Back</a></li>
            <li><a href="#">Account</a></li>
            <li><a href="#">Manage Account Information</a></li>
        </ul>
    <h1 id="skip">${name} &nbsp;</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg">Employer Account</h4>
        <ul class="nav nav-list">
          <li class="active"><a href="#">Company Information</a></li>
          <li><a href="#">Eligibility Results</a></li>
        </ul>
        <p class="margin-top50">&nbsp;</p>
        <h4 class=" graydrkbg">Actions</h4>
        <ul class="nav nav-list">
        <li><a href="#">Add Worksite</a></li>
          
        </ul>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
          <div class="graydrkaction margin0">
                    <h4 class="span10">Account Summary</h4>
                    <a class="btn btn-small" href="<c:url value="/admin/issuer/details/${issuerObj.id}"/>"><spring:message  code="label.cancel"/></a> 
					<a class="btn btn-primary btn-small" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                </div>
          <div class="gutter10">
          		<form class="form-horizontal" id="frmEditCompanyInfo" name="frmEditCompanyInfo" action="<c:url value="/admin/issuer/details/save" />" method="POST">
              <df:csrfToken/>
					<input type="hidden" name="id" id="id" value="${issuerObj.id}"/>
					<div class="profile">
						<table class="table table-border-none">
							<thead>
								<tr>
									<td class="txt-right span4"><label for="name">Legal Business Name</label></td>
									<td>
										<input type="text" name="name" id="name" class="input-large" value="" placeholder="ABC Company"/>
	                            		<div id="name_error" class=""></div>
									</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="txt-right"><label for="ein">EIN</label></td>
									<td>
									  	<input type="text" name="naicCompanyCode" id="ein" class="input-large" value="" placeholder="555555555"/>
	                            		<div id="naicCompanyCode_error" class=""></div>
									</td>
								</tr>
								 <tr>
                                	<td class="txt-right"><label for="CorporationType">Corporation Type</label></td>
                              		 <td>
                                		<select id="CorporationType" name="CorporationType" path="CorporationTypelist" class="input-medium">
                                 	    <option value="">Non-Profit</option>
                                 	    <c:forEach var="state" items="${statelist}">
                                	        <option <c:if test="${state.code == issuerObj.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                              		      </c:forEach>
                             		  </select>
                             		   <div id="CorporationTypeId_error"></div>
                             	   </td>
                           		 </tr>
								<tr>
									<td class="txt-right"><label for="FulltimeEmployees">Full-time Employees</label></td>
									<td>
										<input type="text" name="FulltimeEmployees" id="FulltimeEmployees" class="input-small" value="" placeholder="14"/>
	                            		<div id="FulltimeEmployeesId_error" class=""></div>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><label for="ParttimeEmployees">Part-time Employees</label></td>
									<td>
										<input type="text" name="ParttimeEmployees" id="ParttimeEmployees" class="input-small" value="" placeholder="5"/>
	                            		<div id="ParttimeEmployeesId_error" class=""></div>
									</td>
								</tr>
							</tbody>
						</table>
                        <div class="graylightaction marginTop20">
                            <h4 class="span10">Worksite Address 1</h4>
                        </div>
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><label for="addressLine1">Primary Worksite Address Line 1</label></td>
                                <td>
	                                <input type="text" name="addressLine1" id="addressLine1" class="input-large" value="" placeholder="123 Main St"/>
		                            <div id="addressLine1_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><label for="addressLine2">Address Line 2</label></td>
                                <td>
                                	<input type="text" name="addressLine2" id="addressLine2" class="input-large" value="" placeholder="Suite 250"/>
		                            <div id="addressLine2_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="city">City</label></td>
                                <td>
                                 	<input type="text" name="city" id="city" class="input-large" value="" placeholder="Pleasanton"/>
		                            <div id="city_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="state">State</label></td>
                                <td>
                                	<select id="state" name="state" path="statelist" class="input-medium">
                                     <option value="">CA</option>
                                     <c:forEach var="state" items="${statelist}">
                                        <option <c:if test="${state.code == issuerObj.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                    </c:forEach>
                                </select>
                                <div id="state_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="zip">Zip</label></td>
                                <td>
                                	<input type="text" name="zip" id="zip" class="input-small" value="" placeholder="94566"/>
		                            <div id="zip_error" class=""></div>
                                </td>
                            </tr>
                       	</table>
                       	
                       	<div class="graylightaction marginTop20">
                            <h4 class="span10">Worksite Address 2</h4>
                        </div>
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><label for="addressLine1">Additional Worksite Line 1</label></td>
                                <td>
	                                <input type="text" name="addressLine1" id="addressLine1" class="input-large" value="" placeholder="1411 First Street"/>
		                            <div id="addressLine1_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><label for="addressLine2">Address Line 2</label></td>
                                <td>
                                	<input type="text" name="addressLine2" id="addressLine2" class="input-large" value="&nbsp;"/>
		                            <div id="addressLine2_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="city">City</label></td>
                                <td>
                                 	<input type="text" name="city" id="city" class="input-large" value="" placeholder="Santa Clara"/>
		                            <div id="city_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="state">State</label></td>
                                <td>
                                	<select id="state" name="state" path="statelist" class="input-medium">
                                     <option value="">CA</option>
                                     <c:forEach var="state" items="${statelist}">
                                        <option <c:if test="${state.code == issuerObj.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                    </c:forEach>
                                </select>
                                <div id="state_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="zip">Zip</label></td>
                                <td>
                                	<input type="text" name="zip" id="zip" class="input-small" value="" placeholder="94533"/>
		                            <div id="zip_error" class=""></div>
                                </td>
                            </tr>
                       	</table>
					</div>			
				</form>
           
          </div>
          <!--gutter10--> 
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->