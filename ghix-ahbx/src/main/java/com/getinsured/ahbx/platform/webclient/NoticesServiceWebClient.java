package com.getinsured.ahbx.platform.webclient;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.ahbx.platform.notices.GetStaticNotice;
import com.getinsured.ahbx.platform.notices.GetStaticNoticeResponse;
import com.getinsured.ahbx.platform.notices.ObjectFactory;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * 
 * Class to invoke AHBX Notices SOAP Service.
 * 
 * @author Ekram Ali Kazi
 *
 */
@Component
public class NoticesServiceWebClient {

	private static final Logger LOGGER = Logger.getLogger(NoticesServiceWebClient.class);

	@Value("#{configProp['noticesServiceWSDL']}")
	private String wsdlUrl;

	@Autowired private WebServiceTemplate webServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<GetStaticNoticeResponse>> soapWebClient;

	public String sendRequest(PlatformRequest platformRequest){

		/**
		 * Initiate Platform Response Object, to be returned to the caller
		 */
		PlatformResponse platformResponse = new PlatformResponse();
		platformResponse.startResponse();
		platformResponse.setAppID(wsdlUrl);

		/**
		 * Populate AHBX GetStaticNotice Request Object
		 */
		JAXBElement<GetStaticNotice> getStaticNoticeRequest = formAHBXRequest(platformRequest);

		JAXBElement<GetStaticNoticeResponse> ahbxResponse = null;

		try{
			webServiceTemplate.setDefaultUri(wsdlUrl);
			//ahbxResponse = (JAXBElement<GetStaticNoticeResponse>) webServiceTemplate.marshalSendAndReceive(getStaticNoticeRequest);
			ahbxResponse = soapWebClient.send(getStaticNoticeRequest, wsdlUrl, "SHARED-SERVICES-NOTICES", webServiceTemplate);
		}catch(WebServiceIOException wsIOE){
			LOGGER.error(wsIOE);
			platformResponse.setModuleStatusCode("P-N101");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(wsIOE.getMessage());
		}catch(SoapFaultClientException sfce){
			LOGGER.error(sfce);
			LOGGER.error("Fault Code : " + sfce.getFaultCode());
			LOGGER.error("Fault Reason : " + sfce.getFaultStringOrReason());
			LOGGER.error("Fault Role : " + sfce.getSoapFault().getFaultActorOrRole());
			platformResponse.setModuleStatusCode("P-N102");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(sfce.getMessage());
		}catch(Exception e) {
			LOGGER.error(e);
			platformResponse.setModuleStatusCode("P-N103");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(e.getMessage());
		}

		//LOGGER.info("AHBX AdobeLive Cycle response - " + ahbxResponse);
		/**
		 * If AHBX Response found, populate Platform Response Object, in this case AhbxEcmId
		 */
		if (ahbxResponse != null){
			GetStaticNoticeResponse getStaticNoticeResponse = processAHBXResponse(ahbxResponse);
			platformResponse.setModuleStatusCode("P-N000");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			platformResponse.setAhbxEcmId(getStaticNoticeResponse.getReturn());
		}

		platformResponse.endResponse();

		/**
		 * Marshal and send response back to the caller
		 */
		return AHBXUtils.marshal(platformResponse);
	}

	private GetStaticNoticeResponse processAHBXResponse(JAXBElement<GetStaticNoticeResponse> ahbxResponse) {
		//LOGGER.info(ahbxResponse.getValue());
		return ahbxResponse.getValue();
	}

	private JAXBElement<GetStaticNotice> formAHBXRequest(PlatformRequest platformRequest) {

		ObjectFactory of = new ObjectFactory();
		GetStaticNotice getStaticNotice = of.createGetStaticNotice();
		getStaticNotice.setFormat(platformRequest.getAdobeLiveCycleRequest().getFormat());
		getStaticNotice.setTemplateName(platformRequest.getAdobeLiveCycleRequest().getTemplateName());

		// form dataMapAsXml for HashMap
		XStream xstream1 = GhixUtils.getXStreamStaxObject();
		String dataMapAsXml = xstream1.toXML(platformRequest.getAdobeLiveCycleRequest().getTokens());
		getStaticNotice.setDataAsXML(dataMapAsXml);

		//LOGGER.info("AHBX AdobeLive Cycle request - " + getStaticNotice);

		return of.createGetStaticNotice(getStaticNotice);
	}

}
