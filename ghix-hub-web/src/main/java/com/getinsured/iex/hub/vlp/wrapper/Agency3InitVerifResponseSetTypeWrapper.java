package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifIndividualResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifResponseSetType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;


/**
 * Wrapper Class for Agency3InitVerifResponseSetType
 * 
 * @author Nikhil Talreja
 * @since  14-Feb-2014 
 * 
 */
public class Agency3InitVerifResponseSetTypeWrapper implements JSONAware {
	
	private Agency3InitVerifResponseSetType agency3InitVerifResponseSetType;
	
	private List<Agency3InitVerifIndividualResponseTypeWrapper> agency3InitVerifIndividualResponse = new ArrayList<Agency3InitVerifIndividualResponseTypeWrapper>();
	
	private static final String RESPONSE_KEY = "Agency3InitVerifIndividualResponse";
	
	public Agency3InitVerifResponseSetTypeWrapper(Agency3InitVerifResponseSetType agency3InitVerifResponseSetType){
		
		for(Agency3InitVerifIndividualResponseType response : agency3InitVerifResponseSetType.getAgency3InitVerifIndividualResponse()){
			this.agency3InitVerifIndividualResponse.add(new Agency3InitVerifIndividualResponseTypeWrapper(response));
		}
		
		ObjectFactory factory = new ObjectFactory();
		this.agency3InitVerifResponseSetType = factory.createAgency3InitVerifResponseSetType();
	}
	
	public Agency3InitVerifResponseSetType getAgency3InitVerifResponseSetType() {
		return agency3InitVerifResponseSetType;
	}

	public void setAgency3InitVerifResponseSetType(
			Agency3InitVerifResponseSetType agency3InitVerifResponseSetType) {
		this.agency3InitVerifResponseSetType = agency3InitVerifResponseSetType;
	}

	public List<Agency3InitVerifIndividualResponseTypeWrapper> getAgency3InitVerifIndividualResponse() {
		return agency3InitVerifIndividualResponse;
	}

	public void setAgency3InitVerifIndividualResponse(
			List<Agency3InitVerifIndividualResponseTypeWrapper> agency3InitVerifIndividualResponse) {
		this.agency3InitVerifIndividualResponse = agency3InitVerifIndividualResponse;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray agency3InitVerifIndividualResponses = new JSONArray();
		if(this.agency3InitVerifIndividualResponse != null){
			for(Agency3InitVerifIndividualResponseTypeWrapper data : this.agency3InitVerifIndividualResponse){
				agency3InitVerifIndividualResponses.add(data);
			}
		}
		obj.put(RESPONSE_KEY,agency3InitVerifIndividualResponses);
		return obj.toJSONString();
	}
}
