<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" href="<c:url value="/resources/css/phone.css" />" type="text/css">
<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">

<div id="progressbarwrap">
	<ul class="progressbar panelmenu">
	  <li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Answer a few questions and we'll help you find a plan that fits.">Personalize</a></li>
	  <li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Shop and compare health plans.">Shop</a></li>
	  <c:if test="${exchangeType == 'ON'}">
		<li class="active"><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Finalize your eligibility for tax credit by submitting a Single Streamlined Application.">Apply</a></li>
	  </c:if>
	  <c:if test="${dentalPlansOnOff == 'ON'}">
	  	<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Get dental coverage, if your health plan is missing it.">Enhance</a></li>
	  </c:if>
	  <c:if test="${exchangeType == 'ON'}">
		<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Review your selections and make it official.">Checkout</a></li>
	  </c:if>
	  <c:if test="${exchangeType == 'OFF'}">
		<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Review your selections and start the application.">Checkout</a></li>
	  </c:if>
	</ul>
</div> 

<div class="row-fluid">    
 <div class="row-fluid top-nav"> 
 		<div class="span3"></div>
 		<h1 class="choose-plan span8">Welcome back! Time to get enrolled </h1>
 </div>  
  <div class="row-fluid">
    <input type="hidden" name="existingOrderItemId" id="existingOrderItemId" value="${existingOrderItemId}">
	<form class="form-horizontal" id="tobaccoUsageForm" name="tobaccoUsageForm" action="<c:url value="/private/saveTobaccoUsage" />" method="POST">
	  <df:csrfToken/>
	  <div class="span3"></div> <!--sidebar-->
	  <div class="span8" id="rightpanel">
	    <div class="row-fluid">
		  <h4>Have you used tobacco regularly (4 or more times per week) over the last 6 months? Check the box if yes.</h4>
		  <div class="row-fluid">
			<table class="table table-border-none span7 center">
			  <tbody>
				<tr>
				  <th></th>
				  <th><h4><spring:message code="pd.label.phixhome.section1.tobaccoUse" /></h4></th>
				</tr>
			    <c:forEach var="member" items="${memberList}">				      	
				  <tr>
					<td class="txt-right">${member.firstName} ${member.lastName}</td>
					<td class=" gutter15-l txt-center">						        	
					  <label id="tobacco" class="checkbox">
						<input class="tobacco" id="tobacco" name="tobacco" type="checkbox" value="${member.id}" <c:if test="${member.tobacco == 'Y' }">checked="checked"</c:if> <c:if test="${member.childonlyPlanEligible == 'YES'}"> disabled="true" </c:if>/>
					  </label>
					</td>
				  </tr>					          
				</c:forEach>
			  </tbody>
			</table>
		  </div>
		  <div class="form-actions"> 
			<input id="nextButton" class="btn btn-primary pull-right" type="submit" value="  Next  " tabindex="0"/>
		  </div>
		</div>
		<c:if test="${fn:length(nonEligiblePersonNameList) > 0}">
		<h3 class="offset190">The rest of your family will get enrolled later. </h3>
		<p>
	  	  <c:forEach var="member" items="${nonEligiblePersonNameList}" varStatus="loop">	
		    <c:if test="${member.eligibilityReason != null}">
		      <c:if test="${fn:length(nonEligiblePersonNameList) > 1 && loop.last}"> and </c:if>
	  	  	  ${member.firstName}
	  	  	  <c:if test="${fn:length(nonEligiblePersonNameList) > (loop.count+1)}">, </c:if>	 
			</c:if>			          
	  	  </c:forEach> 
	  	  <c:choose><c:when test="${fn:length(nonEligiblePersonNameList) == 1}"> is </c:when>
	  	  <c:otherwise> are </c:otherwise></c:choose>eligible for other types of health coverage.<br> We'll help you get them covered later.
	  	</p>
	  	</c:if>
      </div>
  	</form>
 </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('body').tooltip({
      selector: '[rel=tooltip]'
    });
  });
  
  $(".tobacco").click(function(){
	  var tobacco = '${editTobacco}';
	  var orderItemId = '${existingOrderItemId}';
	  if(tobacco == 'true' && orderItemId!='0'){
		  var resp = confirm("The cart item will be deleted, do you want to continue?");
		  if(resp){
			  $.ajax({
					type: "DELETE",
					url: "/hix/private/individualCartItems/"+orderItemId+"?"+(Math.random()*10),
					}).done(function() {
						
					});
		  }
	  }
		});
  
  </script>
  			