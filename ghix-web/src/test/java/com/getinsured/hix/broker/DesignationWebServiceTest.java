package com.getinsured.hix.broker;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.dto.broker.BrokerRequestDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints.BrokerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEntityEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.entity.DesignateAssister;

public class DesignationWebServiceTest extends BaseTest {

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Test
	public void testAssisterIndividualRestDesignation() {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		//Household individual = getHousehold("Get You Household details"); //Get your Household information
		assisterRequestDTO.setIndividualId(1);	
		assisterRequestDTO.setUserId(1);
		assisterRequestDTO.setEsignName("Bimal Sahay");
		assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Active);
		String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(assisterRequestDTO);
		String response = ghixRestTemplate.postForObject(EnrollmentEntityEndPoints.DESIGNATE_ASSISTER, requestXML, String.class);
		AssisterResponseDTO designateResponse = (AssisterResponseDTO) GhixUtils.xStreamHibernateXmlMashaling().fromXML(response);
		
		System.out.println("designateResponse " + designateResponse);
	}
	
	@Test
	public void testBrokerIndividualRestDesignation() {
		BrokerRequestDTO brokerRequestDTO = new BrokerRequestDTO();
		//Household individual = getHousehold("Get You Household details"); //Get your Household information
		brokerRequestDTO.setIndividualId(1);	
		brokerRequestDTO.setUserId(1);
		brokerRequestDTO.setEsignName("Bimal Sahay");
		brokerRequestDTO.setDesigStatus(DesignateBroker.Status.Active);
		String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(brokerRequestDTO);
		String response = ghixRestTemplate.postForObject(BrokerServiceEndPoints.BROKER_DESIGNATE_INDIVIDUAL, requestXML, String.class);
		BrokerResponse brokerResponseDTO = (BrokerResponse) GhixUtils.xStreamHibernateXmlMashaling().fromXML(response);

		System.out.println("designateResponse " + brokerResponseDTO);
	}
}
