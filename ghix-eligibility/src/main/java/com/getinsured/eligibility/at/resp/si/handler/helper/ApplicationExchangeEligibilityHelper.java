package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class ApplicationExchangeEligibilityHelper {

	private static final String CSR = "CSR";
	private static final String APTC = "APTC";
	private static final String QHP = "QHP";
	private static final String MEDICAID = "MEDICAID";
	private static final String STATE_SUBSIDY = "STATESUBSIDY";

	private static final Logger LOGGER = Logger.getLogger(ApplicationExchangeEligibilityHelper.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	public ExchangeEligibilityStatus processEligibility(Map<Long, List<EligibilityProgram>> eligibilityProgramMap) {

		return calculateApplicationExchangeEligibilityStatus(eligibilityProgramMap);

		/*List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (!ssapApplicationList.isEmpty()){
			SsapApplication ssapApplication = ssapApplicationList.get(0);

			ExchangeEligibilityStatus exchangeEligibilityStatus = calculateApplicationExchangeEligibilityStatus(eligibilityProgramMap);
			ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
			ssapApplicationRepository.save(ssapApplication);
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}*/

	}

	private static ExchangeEligibilityStatus calculateApplicationExchangeEligibilityStatus(Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap) {

		Set<ExchangeEligibilityStatus> applicantEligibility = new HashSet<>();
		String exchangeEligibilityEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE);
		for (Long ssapApplicantId : eligibilityProgramMap.keySet()) {
			List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityList = eligibilityProgramMap.get(ssapApplicantId);

			Set<String> applicationEligibility = new HashSet<>();
			boolean isQHP = false;
			
			for (com.getinsured.eligibility.model.EligibilityProgram ep : eligibilityList) {
				if (StringUtils.equalsIgnoreCase(ep.getEligibilityIndicator(), EligibilityConstants.TRUE)){
					String eligibilityType = EligibilityStatusHelper.readEligibilityType(ep.getEligibilityType());
					if (StringUtils.equalsIgnoreCase(eligibilityType, QHP)){
						isQHP = true;
					}
					/*if (!MEDICAID.equals(eligibilityType)){  exclude Medicaid and CHIP 
						applicationEligibility.add(eligibilityType);
					}*/
					applicationEligibility.add(eligibilityType);
				}
			}

			if(!isQHP) {
				applicationEligibility.remove(APTC);
				applicationEligibility.remove(CSR);
				applicationEligibility.remove(STATE_SUBSIDY);
			}

			if (applicationEligibility.size() > 0){
				ExchangeEligibilityStatus applicantExchangeEligibility = determineExchangeEligibilityStatusEnum(applicationEligibility);
				applicantEligibility.add(applicantExchangeEligibility);
			}

		}
		applicantEligibility.remove(ExchangeEligibilityStatus.NONE);
		if ((applicantEligibility.size() == 1 && applicantEligibility.contains(ExchangeEligibilityStatus.APTC_CSR))
				||(applicantEligibility.size() == 2 && applicantEligibility.contains(ExchangeEligibilityStatus.APTC_CSR) && applicantEligibility.contains(ExchangeEligibilityStatus.CSR))
				||(applicantEligibility.size() == 2 && applicantEligibility.contains(ExchangeEligibilityStatus.APTC_CSR) && applicantEligibility.contains(ExchangeEligibilityStatus.APTC))
				||(applicantEligibility.size() == 2 && applicantEligibility.contains(ExchangeEligibilityStatus.APTC_CSR) && applicantEligibility.contains(ExchangeEligibilityStatus.MEDICAID))){
			// case is APTC_CSR
			return ExchangeEligibilityStatus.APTC_CSR;
		}
		else if (applicantEligibility.contains(ExchangeEligibilityStatus.APTC)
				||(applicantEligibility.contains(ExchangeEligibilityStatus.APTC_CSR)
				&& applicantEligibility.contains(ExchangeEligibilityStatus.QHP))){
			// case is APTC
			return ExchangeEligibilityStatus.APTC;
		} else if (applicantEligibility.contains(ExchangeEligibilityStatus.CSR)){
			// case is CSR
			return ExchangeEligibilityStatus.CSR;
		} else if (applicantEligibility.contains(ExchangeEligibilityStatus.QHP)){
			// case is QHP
			return ExchangeEligibilityStatus.QHP;
		}
		else if (applicantEligibility.contains(ExchangeEligibilityStatus.MEDICAID)){
			// case is MEDICAID
			return ExchangeEligibilityStatus.MEDICAID;
		}
		return ExchangeEligibilityStatus.NONE;
	}

	private static ExchangeEligibilityStatus determineExchangeEligibilityStatusEnum(Set<String> applicationEligibility) {
		if (applicationEligibility.contains(CSR) && (applicationEligibility.contains(APTC) || applicationEligibility.contains(STATE_SUBSIDY))){
			// APTC_CSR
			return ExchangeEligibilityStatus.APTC_CSR;
		} else if (applicationEligibility.contains(APTC)){
			// APTC
			return ExchangeEligibilityStatus.APTC;
		} else if (applicationEligibility.contains(EligibilityStatusHelper.STATESUBSIDY)){
			//HIX-113042 - treat State Subsidy similar to APTC in roll up logic
			return ExchangeEligibilityStatus.APTC;
		} else if (applicationEligibility.contains(CSR)){
			// APTC
			return ExchangeEligibilityStatus.CSR;
		} else if (applicationEligibility.contains(QHP)){
			// QHP
			return ExchangeEligibilityStatus.QHP;
		}else if (applicationEligibility.contains(MEDICAID)){
			// MEDICAID
			return ExchangeEligibilityStatus.MEDICAID;
		}
		else {
			return ExchangeEligibilityStatus.NONE;
		}

	}


}
