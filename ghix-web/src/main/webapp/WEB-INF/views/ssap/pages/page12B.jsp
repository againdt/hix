<%@ include file="../include.jsp" %>
<div id="appscr62Part2" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages">
				<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page12B.citizenship" text="Citizenship"/> &#47;<spring:message code="ssap.page12B.immiStatus" text="Immigration Status"/>
			</h5>
		</div>
		<form method="POST" action="#" class="form-horizontal" id="appscr62Part2Form">
			<h4>
				<strong><spring:message code="ssap.page12B.otherDoc" text="Other Documentation"/></strong> 
				<small><spring:message code="ssap.page12B.selectOne" text="(select one)"/></small>
			</h4>
			<div class="control-group margin10-l">
				<fieldset>
				<legend class="aria-hidden">
				<strong><spring:message code="ssap.page12B.otherDoc" text="Other Documentation"/></strong> 
				<small><spring:message code="ssap.page12B.selectOne" text="(select one)"/></small>
				</legend>
				<label for="docType1-1" class="radio">
					<input id="docType1-1"  type="radio"  name="docType1"  value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart1" text="Document indicating American Indian born in Canada (LPR &minus; I&minus;551)"/>
				</label>
				<label for="docType1-2" class="radio">
					<input id="docType1-2" type="radio"  name="docType1" value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart2" text="Document indicating member of a federally-recognized Indian tribe (If selected, Please upload supporting documentation) "/>
				</label>
				<label for="docType1-3" class="radio">
					<input id="docType1-3" type="radio" name="docType1"  value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart3" text="Certification from U.S. Department of Health and Human Services (HHS) Office of Refugee Resettlement (ORR))"/>
				</label>
				<label for="docType1-4" class="radio">
					<input id="docType1-4" type="radio"  name="docType1" value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart4" text="Office of Refugee Resettlement (ORR) eligibility letter (if under 18)"/>
				</label>
				<label for="docType1-5" class="radio">
					<input id="docType1-5" type="radio" name="docType1"  value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart5" text="Cuban/Haitian Entrant"/>
				</label>
				<label for="docType1-6" class="radio">
					<input id="docType1-6" type="radio"  name="docType1" value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart6" text="Document indicating withholding of removal"/>
				</label>
				<label for="docType1-7" class="radio">
					<input id="docType1-7" type="radio" name="docType1"  value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart7" text="Resident of American Samoa (If selected, you will be required to upload supporting documentation) "/>
				</label>
				<label for="docType1-8" class="radio">
					<input id="docType1-8" type="radio"  name="docType1" value="0" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart8" text=" Resident of Commonwealth of the Northern Mariana Islands (If selected, you will be required to upload supporting documentation)"/>
				</label>
				<label for="docType_otherDocs" class="radio">
					<input  type="radio" id="docType_otherDocs" name="docType1"  value="1" checked="checked" onchange="showOrHideAt62Part2()">
					<spring:message code="ssap.page12B.radiopart8" text="Other"/>
				</label>
				</fieldset>
			</div>
			
			<div id="appscr62P1_otherAlignAndI-94_table">
				<div class="control-group">
					<label for="appscr62OtherDocAlignNumber" class="control-label">
						<spring:message code="ssap.page12B.alignNumWithColon" text="Alien Number:"/>
					</label><!-- end of label -->
					<div class="controls">
						<input placeHolder="<spring:message code="ssap.page12B.placeholder.alignNumber" text="Alien Number"/>" type="text" id="appscr62OtherDocAlignNumber" name="appscr62OtherDocAlignNumber" class="input-medium" />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="appscr62P2I-94Number" class="control-label">
						<spring:message code="ssap.page12B.i" text="I"/>&minus;<spring:message code="ssap.page12B.number94" text="94 Number:"/>
					</label><!-- end of label -->
					<div class="controls">
						<input placeHolder="<spring:message code="ssap.page12B.placeholder.i94Num" text="I-94 Number"/>"   type="text" id="appscr62P2I-94Number" name="appscr62P2I-94Number" class="inout-medium" />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="appscr62OtherComment" class="control-label">
						<spring:message code="ssap.page12B.comments" text="Comments:"/>
					</label><!-- end of label -->
					<div class="controls">
						<textarea name="UScitizen62" rows="2" cols="98" id="appscr62OtherComment" placeholder="<spring:message code="ssap.page12B.placeholder.comment" text="Comment"/>"></textarea>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div><!-- appscr62P1_otherAlignAndI-94_table ends -->
			
		
			
			
			<div class="control-group hide">
				<fieldset>
				<legend>
					<spring:message code="ssap.page12B.isWithSquare" text="Is ["/>
					<strong class="nameOfHouseHold"></strong>&nbsp;
					<spring:message code="ssap.page12B.overAge16PersonName" text="] if over age 16; person name"/>&apos;
					<spring:message code="ssap.page12B.sSpouse" text="s spouse"/>&apos;
					<spring:message code="ssap.page12B.nameWithS" text="s name"/>&minus;&nbsp;
					<spring:message code="ssap.page12B.ifThereOne" text="if there is one; person name"/>&apos;
					<spring:message code="ssap.page12B.parentNameWithS" text="s parent name"/>&minus;
					<spring:message code="ssap.page12B.under19Condition" text="if person is under age 19] an honourably discharged veteran or active duty member of the military?"/>
				</legend>
				<div class="margin5-l">
					<label class="radio" for="62Part_livedIntheUSSince1996IndicatorYes">
						<input type="radio" id="62Part_livedIntheUSSince1996IndicatorYes" name="livedIntheUSSince1996Indicator"  value="yes">
						<spring:message code="ssap.page12B.yes" text="Yes"/>
					</label>
					<label>
						<input type="radio" id="62Part_livedIntheUSSince1996Indicator" name="livedIntheUSSince1996Indicator"  checked="checked" value="no">
						<spring:message code="ssap.page12B.no" text="No"/>
					</label>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
		</form>
	</div>
</div>