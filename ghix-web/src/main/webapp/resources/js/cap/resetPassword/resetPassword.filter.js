(function() {
	"use strict";

	angular.module("resetPasswordApp").filter('ssnMask', ssnMask);
	function ssnMask(){
		return function(plainSSN){	
			if(plainSSN !== undefined && plainSSN !== null && plainSSN.length === 9){
				return "***-**-" + plainSSN.slice(5);
			}
			
			return plainSSN;
					
		};
	}

	
})();