<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="gutter10">
  <div class="row-fluid">
    <h1 id="skip">[EmployerName]</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg">Coverage</h4>
        <ul class="nav nav-list">
          <li><a href="#">Available Plans</a></li>
          <li class="active"><a href="#">Contribution Selections</a></li>
          <li><a href="#">Renew SHOP Coverage</a></li>
          <li><a href="#">Terminate SHOP Coverage</a></li>
        </ul>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
          <h4 class="graydrkbg gutter10">Contribution Selections</h4>
          <div class="gutter10">
            <p>Cras tincidunt lacus sit amet dolor volutpat eget dignissim ipsum imperdiet. Nam fermentum accumsan felis, eget malesuada tellus lacinia vel. Cras tempus, enim eget congue consectetur, turpis lacus accumsan neque, eu semper leo sapien ac purus. Donec sit amet interdum mauris. Integer cursus nisi mauris. Aenean nec porta libero. Vestibulum placerat pellentesque mi, eget eleifend metus dignissim nec. </p>
            <p>Quisque ut mauris enim, vitae dictum odio. Aenean malesuada tempus odio, quis iaculis libero gravida et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas luctus, eros quis ultricies euismod, mi metus sagittis lectus, in vulputate diam mauris vel elit. Vivamus nisl odio, malesuada ac pretium sed, vehicula vel ante. </p>
            <p>Maecenas et eros eu dolor porttitor pretium. Nullam at arcu dui. Nunc mollis molestie nisi, egestas tristique tellus pharetra in. Praesent quis justo vitae diam pellentesque iaculis nec ut turpis. </p>
          </div>
          <!--gutter10--> 
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->