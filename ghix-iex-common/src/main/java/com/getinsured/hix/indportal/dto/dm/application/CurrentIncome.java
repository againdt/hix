package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;

public class CurrentIncome {
	private BigDecimal incomeAmount;
	private String incomeSourceType;
	private String incomeFrequencyType;
	private JobIncome jobIncome;
	private UnemploymentIncome unemploymentIncome;
	private String selfEmploymentIncomeDescription;

	// Getter Methods

	public UnemploymentIncome getUnemploymentIncome() {
		return unemploymentIncome;
	}

	public BigDecimal getIncomeAmount() {
		return incomeAmount;
	}

	public String getIncomeSourceType() {
		return incomeSourceType;
	}

	public String getIncomeFrequencyType() {
		return incomeFrequencyType;
	}

	public JobIncome getJobIncome() {
		return jobIncome;
	}
	
	public String getSelfEmploymentIncomeDescription() {
		return selfEmploymentIncomeDescription;
	}
	
	// Setter Methods
	

	public void setSelfEmploymentIncomeDescription(String selfEmploymentIncomeDescription) {
		this.selfEmploymentIncomeDescription = selfEmploymentIncomeDescription;
	}

	public void setIncomeAmount(BigDecimal incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public void setIncomeSourceType(String incomeSourceType) {
		this.incomeSourceType = incomeSourceType;
	}

	public void setIncomeFrequencyType(String incomeFrequencyType) {
		this.incomeFrequencyType = incomeFrequencyType;
	}

	public void setJobIncome(JobIncome jobIncomeObject) {
		this.jobIncome = jobIncomeObject;
	}

	public void setUnemploymentIncome(UnemploymentIncome unemploymentIncome) {
		this.unemploymentIncome = unemploymentIncome;
	}
}