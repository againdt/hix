//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a telephone number for a telecommunication device.
 * 
 * <p>Java class for TelephoneNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TelephoneNumberType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}FullTelephoneNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelephoneNumberType", propOrder = {
    "fullTelephoneNumber"
})
public class TelephoneNumberType
    extends ComplexObjectType
{

    @XmlElement(name = "FullTelephoneNumber")
    protected FullTelephoneNumberType fullTelephoneNumber;

    /**
     * Gets the value of the fullTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link FullTelephoneNumberType }
     *     
     */
    public FullTelephoneNumberType getFullTelephoneNumber() {
        return fullTelephoneNumber;
    }

    /**
     * Sets the value of the fullTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link FullTelephoneNumberType }
     *     
     */
    public void setFullTelephoneNumber(FullTelephoneNumberType value) {
        this.fullTelephoneNumber = value;
    }

}
