package com.getinsured.consumer.service;

import java.util.Map;


public interface RelinkHouseholdService {
	
	Map<String, Object> linkNewHousehold(Integer linkHouseholdId,
			Integer delinkHouseholdId, Integer userId) throws Exception;

	Map<String, Object> linkNewHousehold(Integer linkHouseholdId, Integer delinkhouseholdId, Integer userid,
			Integer loggedInUserId) throws Exception;
}
