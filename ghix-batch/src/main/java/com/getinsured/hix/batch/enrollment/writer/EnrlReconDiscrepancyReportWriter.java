package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrlReconDiscrepancyReportService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;


public class EnrlReconDiscrepancyReportWriter implements ItemWriter<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportWriter.class);
	private int applicableYear;
	private JobService jobService;
	private boolean processFileId = false;
	private EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService;
	long jobExecutionId = -1;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		ExecutionContext ec = stepExecution.getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		if(ec != null){
			applicableYear=ec.getInt("year",0);
			processFileId =  ec.containsKey("fileId");
		}
	}

	@Override
	public void write(List<? extends String> items) throws Exception {
		List<String> applicableIdList= null;
		try{
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			if(items!=null && items.size()>0){
				applicableIdList= new ArrayList<String>(items);
				for(String applicableId : applicableIdList){
					if(processFileId) {
						LOGGER.info("EnrlReconDiscrepancyReportWriter : File Id :" + applicableId);
						enrlReconDiscrepancyReportService.generateReportForFileId(applicableId, applicableYear, jobService, jobExecutionId);
					}else {
						LOGGER.info("EnrlReconDiscrepancyReportWriter : HiosIssuerId :" + applicableId);
						enrlReconDiscrepancyReportService.generateReportForHiosIssuerId(applicableId, applicableYear, jobService, jobExecutionId);
					}
				}
			}
			
		}catch(Exception e){
			LOGGER.error("Exception occurred in EnrlReconDiscrepancyReportWriter: ", e);
			
		}
		}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public EnrlReconDiscrepancyReportService getEnrlReconDiscrepancyReportService() {
		return enrlReconDiscrepancyReportService;
	}

	public void setEnrlReconDiscrepancyReportService(EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService) {
		this.enrlReconDiscrepancyReportService = enrlReconDiscrepancyReportService;
	}
	
}
