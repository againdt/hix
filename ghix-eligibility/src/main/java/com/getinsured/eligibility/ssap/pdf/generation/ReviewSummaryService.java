package com.getinsured.eligibility.ssap.pdf.generation;

import net.minidev.json.JSONObject;

import com.getinsured.eligibility.ssap.pdf.generation.ReviewSummaryDTO;



public interface ReviewSummaryService {

	ReviewSummaryDTO getApplicationData(String caseNumber);
	
	ReviewSummaryDTO getAppData(String ssapJSON);

	String generateReviewSummaryNotification(String caseNumber) throws Exception;
	
	String generateSummaryPDF(String ssapJason) throws Exception;
}
