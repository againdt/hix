package com.getinsured.iex.hub.ifsv.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.IdentificationType;

/**
 * Wrapper class for com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee.TaxFilerType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class TaxFilerTypeWrapper implements JSONAware{
	
	private String tinIdentification;
	
	private static final String TIN_KEY = "TINIdentification";
	
	public TaxFilerTypeWrapper(TaxFilerType taxFilerType){
		IdentificationType tinIdentificationId = taxFilerType.getTINIdentification();
		if(tinIdentificationId != null){
			this.tinIdentification = tinIdentificationId.getValue();
		}
	}

	public String getTinIdentification() {
		return tinIdentification;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(TIN_KEY, this.tinIdentification);
		return obj.toJSONString();
	}
}
