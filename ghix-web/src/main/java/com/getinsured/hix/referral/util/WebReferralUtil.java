package com.getinsured.hix.referral.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.List;
import java.util.StringTokenizer;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;

/**
 * @author chopra_s
 * 
 */
public final class WebReferralUtil {
	private WebReferralUtil() {
	}

	public static boolean validWorkFlow(ReferralActivationEnum currentWorkFlow, ReferralActivationEnum nextWorkFlow) {
		return currentWorkFlow.ordinal() < nextWorkFlow.ordinal();
	}

	public static String generateFileName(String originalFilename) {
		return (new SimpleDateFormat("yyyyMMddHHmmss").format(TSCalendar.getInstance().getTime()) + "." + originalFilename);
	}

	public static List<String> convertStringTokenToList(final String strParam1, final String strParam2) {
		List<String> objData = null;
		if (strParam1 != null) {
			objData = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer(strParam1, strParam2);
			while (st.hasMoreTokens()) {
				objData.add(st.nextToken());
			}
			st = null;
		}
		return objData;
	}

	public static String[] convertStringTokenToArray(final String strParam1, final String strParam2) {
		StringTokenizer st = new StringTokenizer(strParam1, strParam2);
		int i = 0;
		String[] strReturn = new String[st.countTokens()];
		while (st.hasMoreTokens()) {
			final String strValue = st.nextToken();
			strReturn[i] = strValue;
			i++;
		}
		st = null;
		return strReturn;
	}

	public static String navigateTo(ReferralActivation referralActivation, String referralId) {
		if (referralActivation.isSecurityPhase()) {
			return "redirect:/referral/security/verify/" + referralId;
		} else if (referralActivation.isSecurityFailed() || referralActivation.isCsrVerificationPending()) {
			return "redirect:/referral/document/upload/" + referralId;
		} else if (referralActivation.isCoverageYearPending()) {
			return "redirect:/referral/coverageyear/" + referralId;
		} else if (referralActivation.isRidpPending()) {
			return "redirect:/referral/ridp/processcheck/" + referralId;
		} else if (referralActivation.isLcePending()) {
			return "redirect:/referral/lce/processcheck/" + referralId;
		}
		return "redirect:/indportal";
	}
}
