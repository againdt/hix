<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<%@ page isELIgnored="false"%> 
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script> 
<!-- Provider Search Results Page -->
<style>
.table-results .address {
	font-size: 12px;
}
.table-results h3 {
	display: inline;
	font-size: 18px;
	line-height: 20px;
	margin-top: 0;
}
hr {
	clear: both;
}
.search {
	padding-top: 12px;
}
</style>
    <link href="../../resources/css/provider.css" media="screen" rel="stylesheet" type="text/css" />
	<div class="gutter10">
		<div class="row-fluid">
			<div class="span12">
				<h1 class="span9"><a name="skip" class="skip">${size == 0 ? 'No' : size} 
						<c:if test='${type==fn:toUpperCase("hospital")}'>
							Hospitals/Facilities Found
						</c:if>
						<c:if test='${type==fn:toUpperCase("doctor")}'>
							Doctors Found
						</c:if>
						<c:if test='${type==fn:toUpperCase("dentist")}'>
							Dentists Found
						</c:if>
						<c:if test='${not empty displaySearchParam}'>
							<small>${displaySearchParam}</small>
						</c:if> 
						
					</a>
				</h1>
				<div class="span3 search">
					<a href='<c:url value="/provider/search"><c:param name="planId" value="${plan.id}"/></c:url>' class="btn btn-primary pull-right"><spring:message code='label.searchAgain'/></a>
				</div>
			</div>
			<h2 class="sub-h1">
			    <c:if test="${not empty plan.id}">
			     <spring:message code='label.accepting'/> <!-- Carrier Name: example provided -->${plan.issuer.name} - <!--  Plan Name: example provided -->${plan.name}&nbsp;
			    </c:if>
	            <c:if test="${fn:toUpperCase(plan.networkType) == 'HMO'}">
					<spring:message code='label.hmo' javaScriptEscape='true'/>
				</c:if>	
				<c:if test="${fn:toUpperCase(plan.networkType) == 'PPO'}">
					<spring:message code='label.ppo' javaScriptEscape='true'/>
				</c:if>
			
			</h2>
			<hr />
			<c:if test='${size >0 }'>
				<form class="form-horizontal modal-form" id="gi_provider_results" name="" action="" method="GET"><!-- <c:url value="" /> -->
					<div class="span7">
					<!--  Provider repeats 5 times per page initially -->
					<table class="table table-hover table-results">
						<thead>
							<tr>
								<th>&#35;</th>
								<th><spring:message code='label.nameAndSpeciality'/></th>
								<th>Contact Info</th>
								<c:if test='${type==fn:toUpperCase("doctor") || type==fn:toUpperCase("dentist")}'>
									<th class="txt-center"><spring:message code='label.acceptingNewPatients'/></th>
								</c:if>
							</tr>
						</thead>
	
						<c:choose>
							<c:when test='${type==fn:toUpperCase("hospital")}'>
								<c:forEach var="facility" items="${listFacilities}" varStatus="rowCounter">
									<tr class="provider">
										<td>${fromIndex+rowCounter.count}.</td>
										<td class="name">
											<h3><a href='<c:url value="/provider/details?planId=${plan.id}&id=${facility.id}&type=${type}&hospitalName=${hospitalName}&hospitalType=${hospitalType}&hospitalNearZip=${hospitalNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>'>${facility.provider.name}</a></h3>
											<% StringBuffer specialtyName= new StringBuffer();%>
											<c:forEach var="facilitySpecialRelation" items="${facility.facilitySpecialRelation}">
												<c:set var="splName" value="${facilitySpecialRelation.specialty.name}"/>
	                                			<%specialtyName.append((String)pageContext.getAttribute("splName"));%>
												<br />${facilitySpecialRelation.specialty.name}
											</c:forEach>
										</td>
										<td class="address">
											<c:forEach var="facilityAddress" items="${facility.facilityAddress}">
												<a id="${facilityAddress.id}" href="javascript:showMarkerInfoWindow(${facilityAddress.id})"><img src="<c:url value="/resources/images/red-dot.png"/>" alt="Map pin"/></a>${facilityAddress.address}<br />${facilityAddress.city} ${facilityAddress.state} ${facilityAddress.zip}<br />
												<c:if test="${not empty facilityAddress.phone}">
													<c:set var="fphone" value="${facilityAddress.phone}"/>
		                                            <%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("fphone"))%>
	                                            </c:if>
											</c:forEach>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:when test='${type==fn:toUpperCase("doctor") || type==fn:toUpperCase("dentist")}'>
								<c:forEach var="physician" items="${listPhysicians}" varStatus="rowCounter">
									<tr class="provider" >
										<td>${fromIndex+rowCounter.count}.</td>
										<td class="name">
											<h3>
												<c:if test='${type==fn:toUpperCase("doctor")}'>
													<a href='<c:url value="/provider/details?planId=${plan.id}&id=${physician.id}&type=${type}&doctorName=${doctorName}&doctorGender=${doctorGender}&doctorSpecialties=${doctorSpecialties}&doctorLanguages=${doctorLanguages}&doctorNearZip=${doctorNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>'>${physician.provider.name}</a>
												</c:if>
												<c:if test='${type==fn:toUpperCase("dentist")}'>
													<a href='<c:url value="/provider/details?planId=${plan.id}&id=${physician.id}&type=${type}&dentistName=${dentistName}&dentistGender=${dentistGender}&dentistSpecialties=${dentistSpecialties}&dentistLanguages=${dentistLanguages}&dentistNearZip=${dentistNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>'>${physician.provider.name}</a>
												</c:if>
											</h3>
											<% StringBuffer specialtyName= new StringBuffer();%>
											<c:forEach var="physicianSpecialRelation" items="${physician.physicianSpecialRelation}">
											  <c:set var="splName" value="${physicianSpecialRelation.specialty.name}"/>
	                                			<%specialtyName.append((String)pageContext.getAttribute("splName"));%>
	                                			<br />${physicianSpecialRelation.specialty.name}
												
											</c:forEach>
										</td>
										<td class="addresses">
											<c:forEach var="physicianAddress" items="${physician.physicianAddress}">
												<a id="${physicianAddress.id}" href="javascript:showMarkerInfoWindow(${physicianAddress.id})"><img src="<c:url value="/resources/images/red-dot.png"/>" alt="Map pin"/></a><span class="address">${physicianAddress.address} <br />${physicianAddress.city} ${physicianAddress.state} ${physicianAddress.zip}<br />
													<c:if test="${not empty physicianAddress.phone}">
														<c:set var="pphone" value="${physicianAddress.phone}"/>
			                                            <%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("pphone"))%>
		                                            </c:if>
												</span>
											</c:forEach>
										</td>
										<td class="newPatients txt-center">
											${physician.acceptingNewPatients}
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					
					</table>
						<div class="pagination">
							<ul>
								<c:if test='${type==fn:toUpperCase("hospital")}'>
									<c:if test='${fromIndex>0}'>
										<li><a href='<c:url value="/provider/search/hospitals?planId=${plan.id}&type=${type}&hospitalName=${hospitalName}&hospitalType=${hospitalType}&hospitalNearZip=${hospitalNearZip}&fromIndex=${fromIndex-pageSize}&toIndex=${toIndex-pageSize}"/>'><spring:message code='label.previous'/></a></li>
									</c:if>
									<c:if test='${toIndex<size}'>
										<li><a href='<c:url value="/provider/search/hospitals?planId=${plan.id}&type=${type}&hospitalName=${hospitalName}&hospitalType=${hospitalType}&hospitalNearZip=${hospitalNearZip}&fromIndex=${fromIndex+pageSize}&toIndex=${toIndex+pageSize}"/>'><spring:message code='label.next'/></a></li>
									</c:if>
								</c:if>
								
								<c:if test='${type==fn:toUpperCase("doctor")}'>
									<c:if test='${fromIndex>0}'>
										<li><a href='<c:url value="/provider/search/doctors?planId=${plan.id}&type=${type}&doctorName=${doctorName}&doctorGender=${doctorGender}&doctorSpecialties=${doctorSpecialties}&doctorLanguages=${doctorLanguages}&doctorNearZip=${doctorNearZip}&fromIndex=${fromIndex-pageSize}&toIndex=${toIndex-pageSize}"/>'><spring:message code='label.previous'/></a></li>
									</c:if>
									<c:if test='${toIndex<size}'>
										<li><a href='<c:url value="/provider/search/doctors?planId=${plan.id}&type=${type}&doctorName=${doctorName}&doctorGender=${doctorGender}&doctorSpecialties=${doctorSpecialties}&doctorLanguages=${doctorLanguages}&doctorNearZip=${doctorNearZip}&fromIndex=${fromIndex+pageSize}&toIndex=${toIndex+pageSize}"/>'><spring:message code='label.next'/></a></li>
									</c:if>
								</c:if>
								
								<c:if test='${type==fn:toUpperCase("dentist")}'>
									<c:if test='${fromIndex>0}'>
										<li><a href='<c:url value="/provider/search/dentists?planId=${plan.id}&type=${type}&dentistName=${dentistName}&dentistGender=${dentistGender}&dentistSpecialties=${dentistSpecialties}&dentistLanguages=${dentistLanguages}&dentistNearZip=${dentistNearZip}&fromIndex=${fromIndex-pageSize}&toIndex=${toIndex-pageSize}"/>'><spring:message code='label.previous'/></a></li>
									</c:if>
									<c:if test='${toIndex<size}'>
										<li><a href='<c:url value="/provider/search/dentists?planId=${plan.id}&type=${type}&dentistName=${dentistName}&dentistGender=${dentistGender}&dentistSpecialties=${dentistSpecialties}&dentistLanguages=${dentistLanguages}&dentistNearZip=${dentistNearZip}&fromIndex=${fromIndex+pageSize}&toIndex=${toIndex+pageSize}"/>'><spring:message code='label.next'/></a></li>
									</c:if>
								</c:if>
							</ul>
						</div>
					</div>
	
					<!-- end of .provider-results -->
					
					<div id="map_canvas" class="map pull-right" style="width: 350px; height: 350px;" ></div>
					
				</form>
			</c:if>
		</div>
	</div><!-- .gutter10 -->
 <script>
		function addToPlan(planId,specialtyName,providerName){
		 	if(planId==null || planId=='undefined'|| planId=='')return;
			var id ="#sel_prvdr_for_plan_"+planId+" tr:last";
			window.parent.$(id).after("<tr><td colspan='2'><b>"+providerName+"</b><br/>"+specialtyName+"</td></tr>");
		}
  </script>
 <script type="text/javascript">
    
  	var locations = jQuery.parseJSON('${markers}');
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0].lat,locations[0].lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var markersArray = []; 
	
    for (var i = 0; i < locations.length; i++) {  
       	var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
        name :  locations[i].name,
        address :  locations[i].address,
        city :  locations[i].city,
        state :  locations[i].state,
        zip :  locations[i].zip,
		map: map
      });
	  	markersArray.push(marker);
	  	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(getContent(marker));
        infowindow.open(map, this);
        }); 
      }
      function showMarkerInfoWindow(id){
 	    for (var i = 0; i < locations.length; i++) {  
		  var marker = markersArray[i];
		  if(locations[i].id==id){
			     infowindow.setContent(getContent(marker));
				 infowindow.open(map, marker);
  
		  }
		}
	  }
      function getContent(marker){
    	  var contentString = '<div id="infoWindow">'
              +'<div id="bodyContent">'
              +'<p>'
              + "<b style='color:#359AFF;font-size:15px'>"+marker.name+"</b><br/>"
              + marker.address+",<br/>"+marker.city+","+marker.state+","+marker.zip+'</p>'
              +'</div>'
              + '</div>';
       return contentString;
      }
  </script>
