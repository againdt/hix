<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ghix" uri="/WEB-INF/tld/dropdown-lookup.tld"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<df:csrfToken/>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/manageqdpplans"/>"><spring:message code="label.linkManageQDP"/></a></li>
                <li><spring:message  code='label.linkDentalPlanDetails'/></li>
	        </ul><!--page-breadcrumb ends-->
			<h1><a name="skip"></a><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${plan.name}</h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.aboutPlan'/></h4>
       		</div>
       		<!--  beginning of side bar  -->
	           <ul class="nav nav-list graybg">
     				<li class="active"><a href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}" />"><spring:message  code='label.linkDentalPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpbenefits/${encPlanId}"/>"><spring:message  code='label.linkDentalPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdprates/${encPlanId}" />"><spring:message  code='label.linkDentalPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqdpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkDentalProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>

			   <c:if test="${displayAction == 'YES'}">
				   	<h4 class="margin0"><spring:message  code='label.actions'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqdpdetail/${encPlanId}" />"><i class="icon icon-pencil" style=""></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
        </div><!-- end of span3 -->

		<div class="span9" id="rightpanel">
				<div class="header">
					<h4 class="pull-left"><spring:message  code='label.linkDentalPlanDetails'/></h4>
					<c:if test="${exchangeType != 'PHIX' && displayEditButton == 'YES'}"><a class="btn btn-small pull-right" type="button" title="<spring:message  code='label.editColumnHeading'/>" href="<c:url value="/admin/planmgmt/editqdpdetail/${encPlanId}" />"><spring:message  code='label.editColumnHeading'/></a>
					</c:if>
				</div>
				<%-- <input type="hidden" id="id" name="id" value="${plan.id}"/> --%>
				<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate" />
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate" />
					<fmt:formatDate pattern="MM/dd/yyyy" value="${futureDate}" var="enFutureDate" timeZone="${timezone}" />
				<table class="table table-border-none">
					<tr>
						<td class="txt-right span4"><spring:message  code='label.issuerName'/></td>
						<td><strong>${plan.issuer.name}</strong></td>
					</tr>
					<c:if test="${exchangeType == 'PHIX'}">
						<tr>
							<td class="txt-right"><spring:message code='label.state' /></td>
							<td><strong>${plan.state}</strong></td>
						</tr>
					</c:if>
					<tr>
						<td class="txt-right"><spring:message  code='label.planName'/></td>
						<td><strong>${plan.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planNumber'/></td>
						<td><strong>${plan.issuerPlanNumber}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planMarket"/></td>
						<td><strong>${plan.market}</strong></td>
					</tr>
					<c:if test="${displayPlanLevel == 'YES'}"><tr id="plan_level_Id">
						<td class="txt-right"><spring:message  code="label.planLevel"/></td>
						<td><strong>${fn:toUpperCase(fn:substring(plan.planDental.planLevel, 0,	1))}${fn:toLowerCase(fn:substring(plan.planDental.planLevel, 1, -1))}</strong></td>
					</tr></c:if>
					<tr>
						<td class="txt-right"><spring:message  code="label.planType"/></td>
						<td><strong>${plan.networkType}</strong></td>
					</tr>
					<c:if test="${plan.planDental.ehbApptForPediatricDental != null && plan.planDental.ehbApptForPediatricDental != ''}">
						<c:choose>
						<c:when test="${plan.applicableYear <= 2016}">
							<tr>
								<td class="txt-right"><spring:message code="label.ehbApptForPediatricDentalAmt"/></td>
								<td><strong>${plan.planDental.ehbApptForPediatricDental}</strong></td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td class="txt-right"><spring:message code="label.ehbApptForPediatricDentalPerc"/></td>
								<td><strong>${plan.planDental.ehbApptForPediatricDental * 100}%</strong></td>
							</tr>
						</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${plan.planDental.planDesignType != null && plan.planDental.planDesignType != ''}">
					<tr>
						<td class="txt-right"><spring:message code="label.planDesignType"/></td>
						<td><strong>${plan.planDental.planDesignType}</strong></td>
					</tr>
					</c:if>
					<tr>
						<td class="txt-right"><spring:message  code="label.planStartDate"/></td>
						<td><strong>${stDate}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planEndDate"/></td>
						<td><strong>${enDate}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.linkProviderNetwork"/></td>
						<td><strong>${network.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code="label.providernetworkURL" /></td>
						<td>
							<strong id="networkURLText">${network.networkURL}</strong>
							<c:if test="${hasEditPermissions eq true }">
								<a class="marginTop10" id="editNetworkURLLink" href="#editProviderNetworkUrlDiv" data-toggle="modal"><spring:message code="button.label"/></a>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code="label.serviceAreaId" /></td>
						<td><strong>${serviceAreaId}</strong>
						<a class="marginTop10" href="<c:url value="/admin/planmgmt/downLoadServiceArea/${encPlanId}" />"><spring:message code="label.download"/></a>
						</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.linkCertificationStatus"/></td>
						<td><strong> <c:choose>
								<c:when test="${fn:toUpperCase(plan.status) == 'CERTIFIED'}"><spring:message code='label.certified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'LOADED'}"><spring:message code='label.loaded' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'DECERTIFIED'}"><spring:message code='label.deCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'NONCERTIFIED'}"><spring:message code='label.nonCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'RECERTIFIED'}"><spring:message code='label.reCertified' /></c:when>
								<c:when test="${fn:toUpperCase(plan.status) == 'INCOMPLETE'}"><spring:message code='label.incomplete' /></c:when>
								<c:otherwise>${plan.status}</c:otherwise>
							</c:choose>
					</strong> <%-- <c:if test="${exchangeType != 'PHIX'}"> --%>
						<a class="marginTop10" href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />">Edit</a>
						<%-- </c:if> --%>
						</td>
					</tr>
					<c:if test="${exchangeType == 'PHIX'}">
						<tr>
							<td class="txt-right"><spring:message code="label.providernetworkURL" /></td>
							<td><strong>${network.networkURL}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message  code="label.issuerVerification"/></td>
							<td><strong>${plan.issuerVerificationStatus}</strong></td>
						</tr>
					</c:if>
					<tr>
						<td class="txt-right"><spring:message  code="label.enrollAvail"/></td>
						<td><strong>${enrollAvailability}</strong>
						<%-- <c:if test="${exchangeType != 'PHIX'}"> --%>
						<a class="marginTop10" href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />">Edit</a>
						<%-- </c:if> --%>
						</td>
					</tr>
					<tr>
						<td class="txt-right"></td>
						<td>
							<c:if test="${futureStatus != ''}">
								<strong>${futureStatus}</strong> <spring:message code='label.showFrom'/> ${enFutureDate}
							</c:if>
						</td>
					</tr>

				</table>
		</div><!-- end of .span9 -->
	</div><!--  end of .row-fluid -->
</div><!-- end of .gutter10 -->

<!-- Modal -->
   <div id="editProviderNetworkUrlDiv" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h3 id="myModalLabel"><spring:message code="label.editProviderNetworkURLPopupHeader" /></h3>
       </div>
       <form class="form-horizontal" id="frmEditProviderNetworkURL" name="frmEditProviderNetworkURL" action="<c:url value="/admin/planmgmt/submitnetworkurl" />" method="POST">
       	<div class="modal-body">
         	<table class="table ">
         		<tr>
					<td class="txt-right no-border" style="width: 30%;">
						<label for="network.networkURL"><spring:message code="label.providernetworkURL" /></label>
					</td>
					<td class="no-border">
						<input type="text" style="width: 90%;" id="networkURL" name="networkURL" value="${network.networkURL}" />
						<div id="networkURL_error" class="help-inline"></div>
					</td>
				</tr>
		</table>
       </div>
       		
		<div class="modal-footer">				
			<button class="btn" data-dismiss="modal" id="cancelEditProviderNetworkUrl"><spring:message code="label.btnCancel" /></button>
			<button class="btn btn-primary" type="button" onclick="javascript:submitProviderNetworkUrl();"><spring:message code="label.btnSubmit" /></button>
		</div>
	</form>
   </div>
<!-- Modal end -->

<script type="text/javascript">
var validator = $("#frmEditProviderNetworkURL").validate({
	rules : {
		networkURL : {required : true, url : true }
	},
	messages : {
		networkURL : { required : "<span><spring:message code='error.providerNetworkURL'/></span>", url : "<span><spring:message code='error.notValidNetworkURL'/></span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	}
});

$('#editNetworkURLLink').click(function() {
	$("#networkURL").prop( "disabled", false);
});

function submitProviderNetworkUrl(){
	if($('#frmEditProviderNetworkURL').valid()){
		var networkUrl = $("#networkURL").val();
		$("#networkURL").prop( "disabled", true);
		$.ajax({
			url: "<c:url value='/admin/planmgmt/submitnetworkurl' />",
			data: {"id": $("#id").val(), "csrftoken": $("#csrftoken").val(), "networkURL" : $("#networkURL").val() },
			type: "POST",
			success: function(response){
				if(response == 'true'){
					$( ".close" ).trigger( "click" );
					$('#networkURLText').html(networkUrl);
				}
			},
			error: function (response){
				alert("Technical issue occured while saving Provider Network URL.");
			}
		});
	}
}

function clearProviderNetworkUrlInfo(){
	$('#editProviderNetworkUrlDiv').modal("hide");  
	$('#networkURL_error').html('');
	$("#frmEditProviderNetworkURL")[0].reset();  
}

$("#cancelEditProviderNetworkUrl").click(function(){
	clearProviderNetworkUrlInfo();
});

</script>

<script type="text/javascript">

$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})
</script>
