/**
 * 
 */
package com.getinsured.hix.batch.referral.service;

/**
 * @author Deepa
 *
 */
public interface DenySepQepService {
	
	void overDueSepQepDenial();
	
}
