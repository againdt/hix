package com.getinsured.hub.hubc;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hub.platform.GhixBaseTest;
import com.getinsured.iex.hub.hubc.servicehandler.HubConnectivityServiceHandler;
import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.repository.base.GIWSPayloadRepository;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;

@TransactionConfiguration(transactionManager="defaultTransactionManager")
public class HUBCTest extends GhixBaseTest {
	
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private HubResponseRepository hubResponseRepository;
	@Autowired
	private WebServiceTemplate checkHubConnectivityServiceTemplate;
	
	private static final int MAX_RETRY_COUNT = 3;
	private static final String INITIAL_STATUS = "PENDING";
	private static final String SERVICE = "CHECK_HUB_CONNECTIVITY";
	
	@Test
	@Transactional(readOnly = true)
	public void testVCIService() throws HubMappingException, BridgeException, HubServiceException {

		ServiceHandler handler = new HubConnectivityServiceHandler();
		GIWSPayload requestRecord = new GIWSPayload();
		requestRecord.setEndpointFunction(SERVICE);
		requestRecord.setEndpointOperationName(SERVICE);
		requestRecord.setRetryCount(MAX_RETRY_COUNT);
		requestRecord.setStatus(INITIAL_STATUS);
		
		// Persist initial request
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);
		Assert.assertNotSame(0L, requestRecord.getId());

		// Creating the payload
		Object requestPayload = handler.getRequestpayLoad();
		Assert.assertNotNull(requestPayload);

		// Call HUB service
		long start = System.currentTimeMillis();
		Object webserviceResponse = checkHubConnectivityServiceTemplate.marshalSendAndReceive(requestPayload);
		long timeTaken = System.currentTimeMillis() - start;
		Assert.assertNotNull(webserviceResponse);

		// Save final request
		requestRecord.setEndpointUrl(checkHubConnectivityServiceTemplate.getDefaultUri());
		requestRecord.setRequestPayload(handler.getJsonInput());
		requestRecord.setStatus(RequestStatus.SUCCESS.getStatusCode());
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);

		// Process and save hub response
		String responseStr = handler.handleResponse(webserviceResponse);
		Assert.assertNotNull(responseStr);
		Assert.assertNotSame(responseStr, "");
		GIHUBResponse hubResponse = new GIHUBResponse();
		hubResponse.setEndPointURL(checkHubConnectivityServiceTemplate.getDefaultUri());
		hubResponse.setGiWspayloadId(requestRecord.getId());
		hubResponse.setClientNodeIp("0.0.0.0");
		hubResponse.setSsapApplicationId(requestRecord.getSsapApplicantId());
		hubResponse.setStatus(handler.getRequestStatus().getStatusCode());
		hubResponse.setElapsedTime(timeTaken);
		hubResponse.setHubResponsePayload(responseStr);
		hubResponse.setHubResponseCode(handler.getResponseCode());
		hubResponse = hubResponseRepository.save(hubResponse);
		Assert.assertNotSame(0L, hubResponse.getId());
		Assert.assertNotNull(hubResponse.getHubResponsePayload());
		Assert.assertNotSame(true, hubResponse.getHubResponsePayload().contains("Exception"));

	}
	
}
