package com.getinsured.hix.reports.enrollment.util;

/**
 * CMS Xml Report Generation Constants File
 */
public final class CmsXmlReportConstants {

	public static final String VIEW_CMS_XML_SUMMARY_REPORT = "hasPermission(#model, 'CMS_XML_SUMMARY_REPORT')";
	public static final String VIEW_CMS_XML_ERROR_REPORT = "hasPermission(#model, 'CMS_XML_ERROR_REPORT')";
	public static final String VIEW_CMS_FILE_TRANSMISSION_LOGS = "hasPermission(#model, 'CMS_FILE_TRANSMISSION_LOGS')";
	public static final String VIEW_MONTHLY_IRS_ERROR_REPORT = "hasPermission(#model, 'MONTHLY_IRS_ERROR_REPORT')";

}
