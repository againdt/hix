package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifResponseType;

/**
 * Wrapper Class for Agency3InitVerifResponseType
 * 
 * @author Nikhil Talreja
 * @since 14-Feb-2014
 * 
 */
public class Agency3InitVerifResponseTypeWrapper implements JSONAware{

	private Agency3InitVerifResponseType agency3InitVerifResponseType;
	
	private ResponseMetadataWrapper responseMetadata;
	private Agency3InitVerifResponseSetTypeWrapper agency3InitVerifResponseSet;
	
	public Agency3InitVerifResponseTypeWrapper(Agency3InitVerifResponseType agency3InitVerifResponseType){
		
		if(agency3InitVerifResponseType == null){
			return;
		}
		
		ResponseMetadataWrapper responseMetadataWrapper = new ResponseMetadataWrapper(agency3InitVerifResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
		Agency3InitVerifResponseSetTypeWrapper agency3InitVerifResponseSetTypeWrapper = 
				new Agency3InitVerifResponseSetTypeWrapper(agency3InitVerifResponseType.getAgency3InitVerifResponseSet());
		
		this.agency3InitVerifResponseSet = agency3InitVerifResponseSetTypeWrapper;
	}

	public Agency3InitVerifResponseType getAgency3InitVerifResponseType() {
		return agency3InitVerifResponseType;
	}

	public void setAgency3InitVerifResponseType(
			Agency3InitVerifResponseType agency3InitVerifResponseType) {
		this.agency3InitVerifResponseType = agency3InitVerifResponseType;
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public Agency3InitVerifResponseSetTypeWrapper getAgency3InitVerifResponseSet() {
		return agency3InitVerifResponseSet;
	}

	public void setAgency3InitVerifResponseSet(
			Agency3InitVerifResponseSetTypeWrapper agency3InitVerifResponseSet) {
		this.agency3InitVerifResponseSet = agency3InitVerifResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("Agency3InitVerifResponseSet",this.agency3InitVerifResponseSet);
		return obj.toJSONString();
	}

}
