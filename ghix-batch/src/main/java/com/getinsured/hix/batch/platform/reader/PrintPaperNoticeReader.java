package com.getinsured.hix.batch.platform.reader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.platform.skip.PrintPaperNoticeSkip;
import com.getinsured.hix.model.NoticeDTO;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.DateUtil;

/**
 * 
 * @since 05th January 2015
 * @version 1.0
 * Reader class for printPaperNoticeJob
 *
 */
public class PrintPaperNoticeReader implements ItemReader<NoticeDTO>, ItemStream
{
	private static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
	private static final Logger LOGGER = LoggerFactory.getLogger(PrintPaperNoticeReader.class);
	private String startDateStr;
	private String endDateStr;
	private final long TWELVE_HOUR_MILLIS = 12*60*60*1000;
	private PrintPaperNoticeSkip printPaperNoticeSkip;
	private static final String PRINT_NOTICE_QUERY = "SELECT N.ID, N.ECM_ID, N.CREATION_TIMESTAMP FROM NOTICES N"
			+" LEFT OUTER JOIN CMR_HOUSEHOLD C ON C.ID = N.KEY_ID"
			+" WHERE N.PRINTABLE = 'Y' AND (N.IS_PRINTED IS NULL OR N.IS_PRINTED != 'Y') "
			+" AND ((N.SUBJECT NOT LIKE '%1095A%') OR (N.SUBJECT LIKE '%1095A%' AND N.STATUS='PDF_GENERATED' AND C.PAPERLESS_1095 = 'false'))"
			+" AND N.CREATION_TIMESTAMP >= ? AND N.CREATION_TIMESTAMP <= ?"
			+" ORDER BY N.ID ASC";
	
	private BatchJobExecutionService batchJobExecutionService;
	
	private JdbcTemplate jdbcTemplate;
	private ResultSet resultSet;
	private Connection connection;
	private PreparedStatement statement;
	private Date startDate;
	private Date endDate;
	private ExecutionContext executionContext;
	private SimpleJobOperator jobOperator;	
	public SimpleJobOperator getJobOperator() {
		return jobOperator;
	}


	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}


	/**
	 * 
	 * @param stepExecution
	 * @throws Exception 
	 */
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) throws Exception{
		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob("printPaperNoticeJob");
		boolean ignorePreviousRun = false;
		if(batchExecutionList != null && batchExecutionList.size() > 1)
		{
			for(BatchJobExecution batchJobExecution : batchExecutionList){
				if (stepExecution.getJobExecutionId() != batchJobExecution.getId()){
					LOGGER.warn("PrintPaperNoticeReader:: Found earlier instances running 'printPaperNoticeJob' Job Execution ID: {} analyzing",batchJobExecution.getId());
					Date startTime = batchJobExecution.getCreatedOn();
					Date lastRunThreshold = new Date(System.currentTimeMillis()-TWELVE_HOUR_MILLIS);
					if(startTime.before(lastRunThreshold)) {
						ignorePreviousRun = true;
						LOGGER.warn("Stopping previous job instance {} with id {} in run state, more than 12 hours old and not in completed state", batchJobExecution.getBatchJobInstance().getJobName(),batchJobExecution.getId() );
						jobOperator.stop(batchJobExecution.getId());
						
					}else {
						//We can not ignore it
						LOGGER.error("Previous job instance {} with id {} in run state can not continue.",batchJobExecution.getBatchJobInstance().getJobName(),batchJobExecution.getId());
					}
				}
			}
			//Stop the instance if once instance is already running  within last 12 hours
			if(!ignorePreviousRun) {
				stepExecution.getJobExecution().stop();
			}
		}
		LOGGER.info("Started Execution for PrintPaperNoticeJOB......");
		if(printPaperNoticeSkip != null && printPaperNoticeSkip.getSkippedNoticeMap() != null){
			printPaperNoticeSkip.clearSkippedNoticeMap();
		}
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, -1);
		this.startDate = DateUtil.removeTime(currentDate.getTime());
		this.endDate = new Date();
		startDate = (getStartDateStr()!=null && DateUtil.isValidDate(getStartDateStr(), BATCH_DATE_FORMAT))?
				DateUtil.StringToDate(getStartDateStr(), BATCH_DATE_FORMAT):startDate;
		//Get end date as current date if null else get the actual from job parameter
		endDate	  = (getEndDateStr()!=null && DateUtil.isValidDate(getEndDateStr(), BATCH_DATE_FORMAT))?
				DateUtil.StringToDate(getEndDateStr(), BATCH_DATE_FORMAT):getLastPosibleTimeOfTheDay(startDate);
	}
	

	@Override
	public NoticeDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		PrintNoticeMapper printNoticeMapper = new PrintNoticeMapper();
		NoticeDTO noticeDTO = null;
		if(this.resultSet.next()){
			noticeDTO = printNoticeMapper.mapRow(this.resultSet, 0);
		}
		return noticeDTO;
	}
	
	
	@Override
	public void close() {
		if(this.statement != null){
			try{
				this.statement.close();
			}
			catch (SQLException e) {
				LOGGER.warn("PrintPaperNoticeReader :: Exception "+e.getMessage()+" encountered while closing the statement, Ignoring");
			}
		}
		if(this.connection != null){
			try 
			{
				this.connection.close();
				LOGGER.info("PrintPaperNoticeReader:: closing connection-------------->");
			} catch (SQLException e) {
				LOGGER.warn("PrintPaperNoticeReader :: Exception "+e.getMessage()+" encountered while closing the connection, Ignoring");
			}
		}
		LOGGER.info("Closing provider read stream");

	}
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		try {
			this.connection  =  jdbcTemplate.getDataSource().getConnection();
			this.connection.setAutoCommit(Boolean.FALSE);
			this.statement = connection.prepareStatement(PRINT_NOTICE_QUERY);
			this.statement.setFetchSize(Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRINTPAPER_NOTICE_COMMIT_INTERVAL)));
			this.statement.setTimestamp(1, new java.sql.Timestamp(this.startDate.getTime()));
			this.statement.setTimestamp(2, new java.sql.Timestamp(this.endDate.getTime()));

			this.resultSet = this.statement.executeQuery();
			
		} catch (SQLException e) {
			throw new ItemStreamException(e.getMessage(),e);
		}
	}


	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		//TODO
	}
	
	
	public String getStartDateStr() {
		return startDateStr;
	}
	
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	public PrintPaperNoticeSkip getPrintPaperNoticeSkip() {
		return printPaperNoticeSkip;
	}

	public void setPrintPaperNoticeSkip(PrintPaperNoticeSkip printPaperNoticeSkip) {
		this.printPaperNoticeSkip = printPaperNoticeSkip;
	}
	
	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	/**
	 * 
	 * @param date
	 * @return
	 */
	private Date getLastPosibleTimeOfTheDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE,      calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND,      calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}

}
