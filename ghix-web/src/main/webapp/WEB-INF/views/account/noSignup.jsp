<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="row-fluid" role="main">
	<div class="landing-signup"style="text-align: center;">
		<h1><spring:message code="label.noSignupPage.message"/></h1>
		<p>
			<a class="btn btn-primary" href="/hix"><spring:message code="label.noSignupPage.button"/></a>
		</p>
			
		
	</div> 	<!-- end landing-signup -->	
</div>	<!-- end main -->

