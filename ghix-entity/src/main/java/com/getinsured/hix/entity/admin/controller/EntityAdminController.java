package com.getinsured.hix.entity.admin.controller;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.MapKeySerializer;
import com.getinsured.hix.entity.admin.service.EntityAdminService;
import com.getinsured.hix.entity.assister.service.AssisterLanguagesService;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.enrollmententity.service.EnrollmentEntityService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.thoughtworks.xstream.XStream;

/**
 * The controller class that receives Entity Admin related Rest requests from
 * ghix-web. It then performs the requested operation and returns response
 */
@SuppressWarnings("unused")
@Controller
@RequestMapping(value = "/entity/entityadmin")
public class EntityAdminController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EntityAdminController.class);

	@Autowired
	private EntityAdminService entityAdminService;

	@Autowired
	private ContentManagementService ecmService;

	@Autowired
	private EnrollmentEntityService enrollmentEntityService;

	@Autowired
	private AssisterService assisterService;
	@Autowired
	private AssisterLanguagesService assisterLanguagesService;
	private static final int MAX_COMMENT_LENGTH = 1000;

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getenrollmententitydetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentEntityDetailsbyId(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getEnrollmentEntityDetailsbyId: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		String  responseJSON = null;
		LOGGER.info("inside get EnrollmentEntity Details by Id info");
		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			EnrollmentEntity enrollmentEntity = enrollmentEntityService.getEnrollmentEntityByRecordID(id);
			EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
			enrollmentEntityDTO.setEnrollmentEntity(enrollmentEntity);
			responseJSON = writer.writeValueAsString(enrollmentEntityDTO);

		} catch (Exception exception) {
			 LOGGER.error("Exception occured in getEnrollmentEntityDetailsbyId in ghix-entity." , exception);
			 throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getEnrollmentEntityDetailsbyId: END");
		
		return responseJSON;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getentitydocumentsdetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getBrokerDocumentsdetailsbyId(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getBrokerDocumentsdetailsbyId: START");
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON= null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		LOGGER.info("inside get brokerdocuments details by id info");
		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			EntityDocuments entityDocuments = entityAdminService.getEntityDocumentsByRecordID(id);
			
			enrollmentEntityDTO.setEntityDocuments(entityDocuments);

			responseJSON = writer.writeValueAsString(enrollmentEntityDTO);
			
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getBrokerDocumentsdetailsbyId in ghix-entity." , exception);
			 throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		
		LOGGER.info("getBrokerDocumentsdetailsbyId: END");

		return responseJSON;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getdocumentsfromecmbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getDocumentsFromECMbyid(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getDocumentsFromECMbyid: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON=null;
		LOGGER.info("inside retrieve population served info");
		try {
			String strId = enrollmentEntityRequestDTO.getEntityType();
			byte[] attachment = null;
			try {
				attachment = ecmService.getContentDataById(strId);
			} catch (ContentManagementServiceException e) {
				LOGGER.error("Unable to show attachment", e);
			}
			if (attachment == null) {
				attachment = "UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes();
			}
			
			enrollmentEntityDTO.setAttachment(attachment);
			
			responseJSON = writer.writeValueAsString(enrollmentEntityDTO );
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getDocumentsFromECMbyid in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		
		}

		LOGGER.info("getDocumentsFromECMbyid: END");

		return responseJSON;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/savedocumentsonecm", method = RequestMethod.POST)
	@ResponseBody
	public String saveDocumentsOnECM(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO)
			throws ContentManagementServiceException {

		LOGGER.info("saveDocumentsOnECM: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON=null;
		LOGGER.info("inside retrieve population served info");
		String strDocumentId = null;
		Integer entityId = enrollmentEntityRequestDTO.getModuleId();
		byte[] attachment = enrollmentEntityRequestDTO.getAttachment();
		String docName = enrollmentEntityRequestDTO.getEntityType();
		try {
			strDocumentId = ecmService.createContent(GhixConstants.ENROLLMENT_ENTITY_DOC_PATH + "/" + entityId + "/"
					+ "ADD_SUPPORT_FOLDER", docName, attachment
					, ECMConstants.Entity.DOC_CATEGORY, ECMConstants.Entity.ENTITY_DOC, null);
			enrollmentEntityDTO.setDocument(strDocumentId);
		} catch (ContentManagementServiceException cms) {
			LOGGER.error("Unable to upload file", cms); 
			 
			enrollmentEntityDTO.setResponseCode(500);
			enrollmentEntityDTO.setResponseDescription(cms.getMessage()+ " Error Code - "+cms.getErrorCode());
		}catch(IllegalArgumentException iae){
			LOGGER.error("Unable to upload file", iae); 
			 
			enrollmentEntityDTO.setResponseCode(500);
			enrollmentEntityDTO.setResponseDescription(iae.getMessage());
		}
		
		try {
			responseJSON = writer.writeValueAsString(enrollmentEntityDTO );
		} catch (JsonProcessingException exception) {
			LOGGER.error("Unable to convert Object to JSON in saveDocumentsOnECM ghix-entity", exception); 
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveDocumentsOnECM: END");

		return responseJSON;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/savedocuments", method = RequestMethod.POST)
	@ResponseBody
	public String saveBrokerDocuments(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("saveBrokerDocuments: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON = null;
		LOGGER.info("inside retrieve population served info");
		
		try {
			EntityDocuments entityDocuments = enrollmentEntityRequestDTO.getEntityDocuments();
			EntityDocuments entityDocumentsNew = entityAdminService.saveEntityDocuments(entityDocuments);
			
			enrollmentEntityDTO.setEntityDocuments(entityDocumentsNew);
			responseJSON = writer.writeValueAsString(enrollmentEntityDTO );
		} catch (Exception exception) {
			LOGGER.error("Error occured in saveBrokerDocuments in ghix-entity", exception); 
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		
		
		LOGGER.info("saveBrokerDocuments: END");

		return  responseJSON;
	}

	/**
	 * Retrieves EnrollmentEntity History object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with List<Map<String,object>> object
	 */
	@RequestMapping(value = "/getenrollmententityhistoryforadmin", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEnrollmentEntityHistoryForAdmin(
			@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrieveEnrollmentEntityHistoryForAdmin: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON = null;
		LOGGER.info("inside retrieve EnrollmentEntityHistory For Admin served info");
		try {
			Integer userId = (Integer) enrollmentEntityRequestDTO.getModuleId();
			List<Map<String, Object>> data = entityAdminService.loadEntityrStatusHistoryForAdmin(userId);
			enrollmentEntityDTO.setEnrollmentEntityHistory(data);

			responseJSON = writer.writeValueAsString(enrollmentEntityDTO );
		} catch (Exception exception) {
			LOGGER.error("Error occured in retrieveEnrollmentEntityHistoryForAdmin in ghix-entity", exception); 
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	
		}
		
		LOGGER.info("retrieveEnrollmentEntityHistoryForAdmin: END");

		return responseJSON;
	}

	/**
	 * Retrieves EnrollmentEntity object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getenrollmententitymap", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentEntityMap(@RequestBody String enrollmentEntityRequesttring) {

		LOGGER.info("getEnrollmentEntityMap: START");
		String responseJSON = null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		LOGGER.info("Inside getenrollmententitymap method");
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
			

			enrollmentEntityDTO.setTotalEnrollmentEntitiesBySearch(enrollmentEntityService.searchEnrollmentEntity(
					enrollmentEntityRequestDTO));

			
			Map<String, Object> entityListAndRecordCount = enrollmentEntityService.searchEnrollmentEntity(
					enrollmentEntityRequestDTO, true);

			enrollmentEntityDTO.setEntityListAndRecordCount(entityListAndRecordCount);
			
			enrollmentEntityDTO.setTotalEnrollmentEntities(enrollmentEntityService.findTotalNoOfEnrollmentEntities());
			// enrollmentEntityDTO.setEnrollmentEntitiesUpForRenewal(enrollmentEntityService
			// .findEnrollmentEntityByRegistrationRenewalDate());
			// enrollmentEntityDTO.setInactiveEnrollmentEntities(enrollmentEntityService
			// .findEnrollmentEntityByRegistrationStatus("InActive"));
			/*ObjectMapper mapper = new ObjectMapper();
			SimpleModule simpleModule = new SimpleModule("SimpleModule",    new Version(1,0,0,null));
			simpleModule.addKeySerializer(EnrollmentEntity.class, new MapKeySerializerEntity()) ;
			mapper.registerModule(simpleModule);
			StringWriter writerStr = new StringWriter();
			 mapper.writeValue(writerStr, enrollmentEntityDTO);*/
			//writerStr.toString();//
			responseJSON =  writer.writeValueAsString(enrollmentEntityDTO);
		} catch (Exception exception) {
			LOGGER.error("Error occured in getEnrollmentEntityMap in ghix-entity", exception); 
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Returning EE list to ghix-web");

		LOGGER.info("getEnrollmentEntityMap: END");

		return responseJSON ;
	}

	@RequestMapping(value = "/getassisterdetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterAdminDetailsbyId(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getAssisterAdminDetailsbyId: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		LOGGER.info("Inside getAssisterAdminDetailsbyId info");
		Integer id = assisterRequestDTO.getId();
		if (id != null) {
			try {
				Assister assister = assisterService.findById(id);
				if (assister != null) {
					AssisterLanguages assisterLanguages = assisterLanguagesService.findByAssister(assister);
					assisterResponseDTO.setAssisterLanguages(assisterLanguages);
				}
				assisterResponseDTO.setAssister(assister);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {

				LOGGER.error("Exception occured in getAssisterAdminDetailsbyId", exception);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);

			}
		}

		String responseJSON = null;
		try {
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		} catch (JsonProcessingException exception) {
			LOGGER.error("Exception occured while converting Object ti JSON in getAssisterAdminDetailsbyId (ghix-entity)", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getAssisterAdminDetailsbyId: END");

		return responseJSON;
	}

	@RequestMapping(value = "/saveassisterstatus", method = RequestMethod.POST)
	@ResponseBody
	public String saveAssisterStatus(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("saveAssisterStatus: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		Assister assister = assisterRequestDTO.getAssister();
		if (assister != null) {
			try {
				Assister existingAssister = assisterService.findById(assister.getId());
				existingAssister.setStatus(assister.getStatus());
				//Fetching first 1000 char for activitycomments text;
				String comments = assister.getActivityComments();
				int beginIndex = 0;
				int endIndex = comments.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  comments.length();
				comments = comments.substring(beginIndex, endIndex);				
				existingAssister.setActivityComments(comments);
				Assister assisterSaved = assisterService.saveAssister(existingAssister);
				assisterResponseDTO.setAssister(assisterSaved);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {
				LOGGER.error("Exception occured in saveAssisterStatus : ", exception);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			}
		}
		
		String responseJSON = null;
		try {
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		} catch (JsonProcessingException exception) {
			LOGGER.error("Exception occured while converting Object ti JSON in saveAssisterStatus (ghix-entity)", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveAssisterStatus: END");

		return responseJSON;
	}

	/**
	 * Fetches Assisters associated with all enrollment entities. Parameter
	 * AssisterRequestDTO Returns a String containing list of all available
	 * assisters based on search parameters
	 * 
	 * @param requestJSON
	 *            xml representation of the AssisterRequestDTO}
	 * @return xml representation of AssisterResponseDTO
	 */
	@RequestMapping(value = "/getsearchassisterlist", method = RequestMethod.POST)
	@ResponseBody
	public String getSearchAssisterList(@RequestBody String requestJSON) {

		LOGGER.info("getSearchAssisterList: START");

		LOGGER.info("Inside getAssisterList method in ghix-entity module");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		try {
			AssisterRequestDTO assisterRequestDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class).readValue(requestJSON);

			List<Assister> assisters = entityAdminService.searchAssister(assisterRequestDTO);
			assisterResponseDTO.setListOfAssisters(assisters);
			// EnrollmentEntity entity =
			// assisterRequestDTO.getEnrollmentEntity();

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
		} catch (Exception exception) {
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Exception occured in retrieving list of assisters from db", exception);
		}
		
		String responseJSON = null;
		try {
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		} catch (JsonProcessingException exception) {
			LOGGER.error("Exception occured while converting Object to JSON in getSearchAssisterList (ghix-entity)", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("Exiting getAssisterList in ghix-entity module");

		LOGGER.info("getSearchAssisterList: END");

		return responseJSON;
	}

	 
}
