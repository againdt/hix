package com.getinsured.ssap.model.financial;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes IFSV request.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public class IfsvRequest {
  private long applicationId;
  private String clientIp;
  private IfsvPayload payload;
  @JsonProperty("ssn_dob")
  private List<Map<String, String>> ssnDob;
  @JsonProperty("total_household_income")
  private long totalHouseholdIncome;

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(long applicationId) {
    this.applicationId = applicationId;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public IfsvPayload getPayload() {
    return payload;
  }

  public void setPayload(IfsvPayload payload) {
    this.payload = payload;
  }

  public List<Map<String, String>> getSsnDob() {
    return ssnDob;
  }

  public void setSsnDob(List<Map<String, String>> ssnDob) {
    this.ssnDob = ssnDob;
  }

  public long getTotalHouseholdIncome() {
    return totalHouseholdIncome;
  }

  public void setTotalHouseholdIncome(long totalHouseholdIncome) {
    this.totalHouseholdIncome = totalHouseholdIncome;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", IfsvRequest.class.getSimpleName() + "[", "]")
        .add("applicationId=" + applicationId)
        .add("clientIp='" + clientIp + "'")
        .add("payload=" + payload)
        .add("ssnDob=" + ssnDob)
        .add("totalHouseholdIncome=" + totalHouseholdIncome)
        .toString();
  }
}
