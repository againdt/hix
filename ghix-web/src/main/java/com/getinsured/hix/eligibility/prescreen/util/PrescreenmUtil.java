/**
 *
 */
package com.getinsured.hix.eligibility.prescreen.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.planmgmt.HouseHoldRequest;
import com.getinsured.hix.model.planmgmt.HouseHoldResponse;
import com.getinsured.hix.model.planmgmt.Member;
import com.getinsured.hix.model.prescreen.PrescreenData;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PrescreenServiceEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDeductions;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenHousehold;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenHouseholdMember;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenProfile;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenRequest;

/**
 * Utility class for Prescreen Eligibility
 * @author Nikhil Talreja
 * @since 17 April, 2013
 *
 */
@Component
public class PrescreenmUtil {

	private static final Logger LOGGER = Logger.getLogger(PrescreenmUtil.class);

	@Autowired private HIXHTTPClient hIXHTTPClient;

	private static ObjectMapper mapper = new ObjectMapper();

	private static Set<String> STATES_TO_HIKE_BY_FIVE = new HashSet<String>();
	private static final Set<Integer> NO_PLAN_FOUND_ERRORS = new HashSet<Integer>();

	static{
		STATES_TO_HIKE_BY_FIVE.add("MA");
		STATES_TO_HIKE_BY_FIVE.add("NJ");
		STATES_TO_HIKE_BY_FIVE.add("NY");
		STATES_TO_HIKE_BY_FIVE.add("RI");
		STATES_TO_HIKE_BY_FIVE.add("VT");
		
		NO_PLAN_FOUND_ERRORS.add(4001);
		NO_PLAN_FOUND_ERRORS.add(4004);
		NO_PLAN_FOUND_ERRORS.add(4007);
	}

	private String csr = "";
	private static final String CS6 = "CS6";
	private static final String CS5 = "CS5";
	private static final String CS4 = "CS4";
	private static final String CS1 = "CS1";
	private static final String MEDICAID_CSR = "Medicaid";

	private static final double CS6_LOWER_LIMIT_INCL = 138.0;
	private static final double CS6_UPPER_LIMIT_INCL = 150.0;

	private static final double CS5_LOWER_LIMIT_EXCL = 150.0;
	private static final double CS5_UPPER_LIMIT_INCL = 200.0;

	private static final double CS4_LOWER_LIMIT_EXCL = 200.0;
	private static final double CS4_UPPER_LIMIT_INCL = 250.0;

	private static final double CS1_LOWER_LIMIT_EXCL = 250.0;
	private static final int SIXTEEN = 16;
	
	private static final int PREMIUM_ZERO_ERROR_CODE = 9999;
	private static final String PREMIUM_ZERO_ERROR_MESSAGE = "Premium value was returned as 0";
	
	/**
	 * This method makes a REST call to prescreen service to find APTC for a request
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param request - The Prescreen Request for which APTC needs to be found
	 * @return String - APTC value
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenRequest calculateAptcREST(PrescreenRequest request) throws IOException, URISyntaxException{
		String postParams = mapper.writeValueAsString(request);
		LOGGER.info("Request " + postParams);
		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.CALCULATE_APTC_URL,postParams,PrescreenConstants.CONTENT_TYPE);

		if(response != null){
			try {
				request = mapper.readValue(response, PrescreenRequest.class);
				return request;
			} catch (JsonParseException e) {
				LOGGER.error(e.getMessage(),e);
				return request;
			} catch (JsonMappingException e) {
				LOGGER.error(e.getMessage(),e);
				return request;
			} catch (IOException e) {
				LOGGER.error(e.getMessage(),e);
				return request;
			}
		}
		else{
			return request;
		}
	}

	/**
	 * This method gets a record from PRESCREEN_DATA table using primary key (id)
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param id - The Prescreen Request Object Id to be fetched
	 * @return PrescreenData - The fetched record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData getPrescreenRecord(long id) throws IOException, URISyntaxException{
		String postParams = "\"" + id + "\"";
		LOGGER.info("Record Id " + postParams);

		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.GET_PRESCREEN_RECORD,postParams,PrescreenConstants.CONTENT_TYPE);

		PrescreenData record = null;
		if(StringUtils.isNotBlank(response)){
			record = mapper.readValue(response, PrescreenData.class);
		}

		return record;
	}

	/**
	 * This method saves a record in PRESCREEN_DATA table
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param record - The Prescreen Request Object to be saved
	 * @return PrescreenData - The saved record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData savePrescreenRecord(PrescreenData record) throws IOException, URISyntaxException{

		String postParams = mapper.writeValueAsString(record);
		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.SAVE_PRESCREEN_RECORD,postParams,PrescreenConstants.CONTENT_TYPE);

		PrescreenData prescreenRecord = null;
		if(StringUtils.isNotBlank(response)){
			prescreenRecord = mapper.readValue(response, PrescreenData.class);
		}

		return prescreenRecord;
	}

	/**
	 * This method gets a record from PRESCREEN_DATA table using HHTP session id
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param sessionId - The HTTP session id for the current session
	 * @return PrescreenData - The fetched record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData  getPrescreenRecordBySessionId(String sessionId) throws IOException, URISyntaxException{

		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.GET_PRESCREEN_RECORD_BY_SESSION,sessionId,PrescreenConstants.CONTENT_TYPE);

		PrescreenData record = null;
		if(StringUtils.isNotBlank(response)){
			record = mapper.readValue(response, PrescreenData.class);
		}

		return record;
	}

	/**
	 * This method copies values from Prescreen Request object to PrescreenData record
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param record, prescreenRequest
	 */
	public void updateFieldsForPrescreenRecord(PrescreenData record,
			PrescreenRequest prescreenRequest) {

		PrescreenProfile profile = prescreenRequest.getPrescreenProfile();
		PrescreenHousehold household = prescreenRequest.getPrescreenHousehold();
		PrescreenDeductions deductions = prescreenRequest.getPrescreenDeductions();

		//Updating profile Details
		if(profile != null){
			record.setName(profile.getClaimerName());
			record.setZipCode(profile.getZipCode());
			record.setIsMarried("N");
			if(profile.getIsMarried()){
				record.setIsMarried("Y");
			}
			record.setNoOfDependents(profile.getNumberOfDependents());
			record.setHouseholdIncome(profile.getTaxHouseholdIncome());
			record.setIsDisabled("N");
			if(prescreenRequest.getIsAnyMemberDisabled()){
				record.setIsDisabled("Y");
			}
			record.setStateCd(profile.getStateCode());
			record.setCounty(profile.getCounty());
		}

		//Updating household details
		updateHouseholdDetails(household,record);

		//Updating deductions
		if(deductions != null){
			record.setAlimonyDeduction(deductions.getAlimonyPaid());
			record.setStudentLoanDeduction(deductions.getStudentLoans());
		}

		//APTC, FPL, CSR and Plan
		record.setFinalCreditDisplayed(prescreenRequest.getAptcValue());
		record.setFpl(prescreenRequest.getFplValue());
		record.setCsr(prescreenRequest.getCsr());
		record.setFinalPlanShowed(prescreenRequest.getPlanId());
		//if(prescreenRequest.getBenchmarkPlanPremium() != null){
			record.setPremiumValue(prescreenRequest.getBenchmarkPlanPremium());
		//}
		record.setResultsType(prescreenRequest.getResultsType());
		
		//Benchmark premium call status
		if(StringUtils.isBlank(prescreenRequest.getErrorDescription())){
			record.setBenchmarkStatus("Y");
			record.setBenchmarkStatusDesc("Success");
		}
		else{
			record.setBenchmarkStatus("N");
			record.setBenchmarkStatusDesc(prescreenRequest.getErrorDescription());
		}
		
		//Session Time
		if(record.getSessionStartTime() != null && record.getSessionEndTime() != null){
			record.setTotalTimeSpent(record.getSessionEndTime().getTime() - record.getSessionStartTime().getTime());
		}


		LOGGER.info("Adding session data to Prescreen Data record");
		StringBuffer sessionData = new StringBuffer();

		sessionData.append("Claimer Name : ");
		sessionData.append(profile.getClaimerName());
		sessionData.append("\nZip Code : ");
		sessionData.append(profile.getZipCode());
		sessionData.append("\nIs Married : ");
		sessionData.append(profile.getIsMarried());
		sessionData.append("\nNo of dependents : ");
		sessionData.append(profile.getNumberOfDependents());
		sessionData.append("\nTax household income : ");
		sessionData.append(profile.getTaxHouseholdIncome());

		sessionData.append("\nHouse hold members : ");
		sessionData.append(household.getHouseholdMembers());
		sessionData.append("\nNo of members seeking coverage : ");
		sessionData.append(household.getNumberOfMembersSeekingCoverage());

		sessionData.append("\nMember incomes : ");
		sessionData.append(prescreenRequest.getPrescreenIncomes());

		sessionData.append("\nAlimony Deductions : ");
		sessionData.append(deductions.getAlimonyPaid());
		sessionData.append("\nStudent Loan Deductions : ");
		sessionData.append(deductions.getStudentLoans());

		sessionData.append("\nApplicable Percentage : ");
		sessionData.append(prescreenRequest.getApplicablePercentage());
		sessionData.append("\nBenchmark Premium : ");
		sessionData.append(prescreenRequest.getBenchmarkPlanPremium());
		sessionData.append("\nFPL : ");
		sessionData.append(prescreenRequest.getFplValue());
		sessionData.append("\nCSR : ");
		sessionData.append(prescreenRequest.getCsr());

		//LOGGER.info("sessionData==="+sessionData);
		record.setSessionDataClob(sessionData.toString());

		if(StringUtils.isNumeric(prescreenRequest.getCurrentTab()) && record.getScreenVisisted() != 4){
			record.setScreenVisisted(Integer.parseInt(prescreenRequest.getCurrentTab()));
		}


	}

	private void updateHouseholdDetails(PrescreenHousehold household,PrescreenData record ){

		//Updating household details
		if (household != null) {
			// Updating claimer DOB
			List<PrescreenHouseholdMember> members = household
					.getHouseholdMembers();
			if (!CollectionUtils.isEmpty(members)) {
				for (PrescreenHouseholdMember member : members) {
					if (StringUtils.equalsIgnoreCase(
							member.getRelationshipWithClaimer(), "self")
							&& StringUtils.isNotBlank(member.getDateOfBirth())) {
						record.setClaimantDob(DateUtil.StringToDate(
								member.getDateOfBirth(),
								GhixConstants.REQUIRED_DATE_FORMAT));
						break;
					}
				}
			}
		}

	}
	/**
	 * This method makes a REST call to prescreen service to find APTC for a request
	 * @author Sunil Desu
	 * @since May 08, 2013
	 * @param request - The fpl for which CSR needs to be found
	 * @return String - CSR value
	 * @throws URISyntaxException 
	 * @throws IOException
	 */
	public String calculateCsrREST(double fpl) throws URISyntaxException{
		String response="";
		try {
		LOGGER.info("Request for CSR " + fpl);
		response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.CALCULATE_CSR_URL,Double.toString(fpl),PrescreenConstants.CONTENT_TYPE);
		} catch (ClientProtocolException e) {
			LOGGER.error("Error invoking CSR calculator "+e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Error invoking CSR calculator "+e.getMessage());
		}
		return response;
	}

	/**
	 * This method makes a REST call to BenchmarkPlanPremium interface to find BenchmarkPlanPremium for a household
	 * @author Sunil Desu
	 * @since May 13, 2013
	 * @param request - The PrescreenRequest for which premium needs to be found
	 * @return String - Premium value
	 * @throws IOException
	 */
	public HouseHoldResponse fetchBenchmarkPlanPremium(PrescreenRequest prescreenRequest){
		String response="";
		Double premiumForMonth = 0.0;
		HouseHoldRequest houseHoldRequest = new HouseHoldRequest();
		HouseHoldResponse houseHoldResponse = new HouseHoldResponse();
		try {
		LOGGER.info("Request for fetching BenchmarkPlanPremium " + prescreenRequest.toString());
		houseHoldRequest = generateHouseHoldRequest(prescreenRequest);
		String postParams = mapper.writeValueAsString(houseHoldRequest);
		LOGGER.info("Household Request " + postParams);
		response = hIXHTTPClient.getPOSTData(GhixEndPoints.PlanMgmtEndPoints.GET_BENCHMARK_PREMIUM_URL,postParams,PrescreenConstants.CONTENT_TYPE);
		if(StringUtils.isNotBlank(response)){
			try {
				houseHoldResponse = mapper.readValue(response, HouseHoldResponse.class);
			} catch (JsonParseException e) {
				LOGGER.error("JsonParseException parsing BenchmarkPlanPremium response "+e.getMessage());
			} catch (JsonMappingException e) {
				LOGGER.error("JsonMappingException parsing BenchmarkPlanPremium response "+e.getMessage());
			} catch (IOException e) {
				LOGGER.error("IOException parsing BenchmarkPlanPremium response "+e.getMessage());
			}
		}
		} catch (ClientProtocolException e) {
			LOGGER.error("ClientProtocol Error invoking BenchmarkPlanPremium interface: "+e.getMessage());
		} catch (IOException e) {
			LOGGER.error("IO Error invoking BenchmarkPlanPremium interface: "+e.getMessage());
		}catch (Exception e) {
			LOGGER.error("General Error invoking BenchmarkPlanPremium interface: "+e.getMessage());
		}

		premiumForMonth = houseHoldResponse.getPremium();
		if(STATES_TO_HIKE_BY_FIVE.contains(prescreenRequest.getPrescreenProfile().getStateCode())){
			premiumForMonth *= 1.05;
		}else {
			premiumForMonth *= 1.20;
		}
		houseHoldResponse.setPremium(premiumForMonth);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Benchmark Response is "+houseHoldResponse.getErrCode()+":"+houseHoldResponse.getErrMsg());
			LOGGER.info("Premium value fetched is "+houseHoldResponse.getPremium());
		}
		return houseHoldResponse;
	}

	private HouseHoldRequest generateHouseHoldRequest(PrescreenRequest prescreenRequest){
		HouseHoldRequest houseHoldRequest= new HouseHoldRequest();
		PrescreenHousehold prescreenHousehold = prescreenRequest.getPrescreenHousehold();
		List<Member> members = new ArrayList<Member>();
		Member member;
		SimpleDateFormat jspDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		boolean isPrimaryAdded = false;
		try{
			for(PrescreenHouseholdMember householdMember: prescreenHousehold.getHouseholdMembers()){
				if (!householdMember.isSeekingCoverage()) {
					continue;
				}
				member = new Member();
				if (householdMember.getRelationshipWithClaimer().equals("self")) {
					member.setMemberType("primary");
					member.setGender("MALE");
					member.setStudent(false);
					isPrimaryAdded=true;
				} else if (householdMember.getRelationshipWithClaimer().equals(
						"spouse")) {
					member.setMemberType("spouse");
					member.setGender("FEMALE");
					member.setStudent(false);
				} else {
					member.setMemberType("child");
					member.setGender("FEMALE");
					member.setStudent(true);
				}
				if(householdMember.getDateOfBirth()!=null && !householdMember.getDateOfBirth().trim().equals("")){
					Calendar dateOfBirth = TSCalendar.getInstance();
					dateOfBirth.setTime(jspDateFormat.parse(householdMember.getDateOfBirth()));
					member.setDateOfBirth(dateOfBirth);
					member.setAge(getAgeFromDob(householdMember.getDateOfBirth()));
					//LOGGER.info("Date is "+dateOfBirth.getTime());
					//LOGGER.info("Age is " + member.getAge());
				}
				member.setTobaccoUser(false);
				members.add(member);
			}
			if(!isPrimaryAdded){
				for(Member membr:members){
					if(membr.getMemberType().equalsIgnoreCase("spouse")){
						membr.setMemberType("primary");
						LOGGER.info("Primary tax filer is not an applicant. Making spouse as primary.");
						break;
					}
				}
			}
			houseHoldRequest.setMembers(members);
			houseHoldRequest.setEffectiveDate(computeEffectiveDate());
			houseHoldRequest.setState(prescreenRequest.getPrescreenProfile().getStateCode());
			houseHoldRequest.setCounty(prescreenRequest.getPrescreenProfile().getCounty());
			houseHoldRequest.setZipCode(prescreenRequest.getPrescreenProfile().getZipCode());
			houseHoldRequest.setAppID("Prescreen");
			houseHoldRequest.setRequestTime(TSCalendar.getInstance());
		}catch(Exception ex){
			LOGGER.error("Error generating household request");
		}

		LOGGER.info("HouseHoldRequest request is "+houseHoldRequest.toString());
		return houseHoldRequest;
	}

	/**
	 * This method is invoked by passing the FPL percentage. The corresponding CSR value is returned.
	 *
	 * @author Sunil Desu
	 * @since May 15, 2013
	 * @param request - The fpl for which CSR needs to be found
	 * @return String - CSR value
	 */
	public String calculateCsr(double fpl){
		if (fpl < 138.0) {
			return MEDICAID_CSR;
		}

		if (fpl > CS1_LOWER_LIMIT_EXCL) {
			csr = CS1;
		} else if (fpl > CS4_LOWER_LIMIT_EXCL && fpl <= CS4_UPPER_LIMIT_INCL) {
			csr = CS4;
		} else if (fpl > CS5_LOWER_LIMIT_EXCL && fpl <= CS5_UPPER_LIMIT_INCL) {
			csr = CS5;
		} else if (fpl >= CS6_LOWER_LIMIT_INCL && fpl <= CS6_UPPER_LIMIT_INCL) {
			csr = CS6;
		}

		return csr;
	}

	/**
	 * Utility method to get age of a member
	 *
	 * @author Nikhil Talreja
	 * @since May 20, 2013
	 * @param String - The DOB in MM/dd/yyyy format
	 * @return int - age of the member
	 */
	public int getAgeFromDob(String dob) throws ParseException{

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		int age = 0;
	    int factor = 0;
	    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    Date today = df.parse(df.format(new TSDate()));

		Calendar cal1 = new GregorianCalendar();
	    Calendar cal2 = new GregorianCalendar();

	    Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
	    cal1.setTime(dobDate);
	    cal2.setTime(today);
	    if(cal2.get(Calendar.DAY_OF_YEAR) < cal1.get(Calendar.DAY_OF_YEAR)) {
	            factor = -1;
	    }
	    age = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR) + factor;

	    return age;
	}

	/**
	 * This method checks if a household is eligible for medicare.
	 * Used in results determination
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param household - The household object for a request
	 * @return boolean - true/false
	 */
	public boolean checkMedicare(PrescreenHousehold household) throws ParseException{

		if(household == null || CollectionUtils.isEmpty(household.getHouseholdMembers())){
			return false;
		}

	    int age = 0;

		for(PrescreenHouseholdMember member : household.getHouseholdMembers()){

		    age = getAgeFromDob(member.getDateOfBirth());
		    if(age < 65){
		    	return false;
		    }
		}


		return true;
	}

	/**
	 * This method determines result type based on FPL value
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param request - The prescreen request object
	 */
	public void determineResults(PrescreenRequest request) throws ParseException {

		double fpl = request.getFplValue();

		if (checkMedicare(request.getPrescreenHousehold())) {
			request.setResultsType("Medicare");
			LOGGER.info("Results type : Medicare");
			return;
		}

		// Medicaid
		if (fpl < CS6_LOWER_LIMIT_INCL) {
			request.setResultsType("Medicaid");
		}

		// CS1
		if (fpl >= CS4_UPPER_LIMIT_INCL) {
			request.setResultsType("CS1");
		}

		// CS4
		if (fpl >= CS4_LOWER_LIMIT_EXCL && fpl < CS4_UPPER_LIMIT_INCL) {
			request.setResultsType("CS4");
		}

		// CS5
		if (fpl >= CS5_LOWER_LIMIT_EXCL && fpl < CS4_LOWER_LIMIT_EXCL) {
			request.setResultsType("CS5");
		}

		// CS6
		if (fpl >= CS6_LOWER_LIMIT_INCL && fpl < CS5_LOWER_LIMIT_EXCL) {
			request.setResultsType("CS6");
		}

		LOGGER.info("Results type : " + request.getResultsType());
	}

	/**
	 * This method checks if DOB is entered for all household members
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param household
	 * @return boolean
	 */
	public boolean validateDateOfBirths(PrescreenHousehold household){

		if (household == null || CollectionUtils.isEmpty(household
				.getHouseholdMembers())) {

			return false;
		}

		List<PrescreenHouseholdMember> members = household
					.getHouseholdMembers();

			for (PrescreenHouseholdMember member : members) {
				if (StringUtils.isBlank(member.getDateOfBirth())) {
					LOGGER.info("Date of birth is missing for one or more members");
					return false;
				}
			}



		return true;
	}
	
	public Calendar computeEffectiveDate(){
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();

		Calendar sixteenthOfTheMonth = TSCalendar.getInstance();		
		sixteenthOfTheMonth.set(Calendar.DAY_OF_MONTH, SIXTEEN);
		sixteenthOfTheMonth.set(Calendar.HOUR_OF_DAY, 0);
		sixteenthOfTheMonth.set(Calendar.MINUTE, 0);
		sixteenthOfTheMonth.set(Calendar.SECOND, 0);
		sixteenthOfTheMonth.set(Calendar.MILLISECOND, 0);
		
		try{
			
			effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);
			effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
			effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
			effectiveDate.set(Calendar.MINUTE, 0);
			effectiveDate.set(Calendar.SECOND, 0);
			effectiveDate.set(Calendar.MILLISECOND, 0);

			if(currentDate.getTimeInMillis() < sixteenthOfTheMonth.getTimeInMillis()){				
				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
			}
			else if(currentDate.getTimeInMillis() >= sixteenthOfTheMonth.getTimeInMillis()){
				effectiveDate.set(Calendar.DAY_OF_MONTH, effectiveDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("EffectiveDate is "+TSDate.getNoOffsetTSDate(effectiveDate.getTimeInMillis()));
			}
		}catch(Exception ex){
			LOGGER.info("Error computing EffectiveDate");
		}
		return effectiveDate;
	}
	
	/**
	 * @author Sunil Desu
	 * @since May 28 2013
	 * 
	 * @param PrescreenRequest
	 * @param HouseHoldResponse
	 * @return PrescreenRequest
	 */
	public PrescreenRequest checkForErrorCondition(PrescreenRequest request, HouseHoldResponse houseHoldResponse){
		
		if(houseHoldResponse.getErrCode()>4000){
			request.setErrorDescription(houseHoldResponse.getErrCode()+":"+((houseHoldResponse.getErrMsg()==null||houseHoldResponse.getErrMsg().trim().length()==0)?"Error returned by Benchmark API.":houseHoldResponse.getErrMsg()));
			request.setErrorMessage(getErrorMessageFromErrorCode(houseHoldResponse.getErrCode()));
		}
		else if(houseHoldResponse.getPremium() == 0.0){
			request.setErrorDescription(PREMIUM_ZERO_ERROR_MESSAGE);
			request.setErrorMessage(getErrorMessageFromErrorCode(PREMIUM_ZERO_ERROR_CODE));
		}
		
		else{
			request.setErrorDescription(null);
			request.setErrorMessage(null);
		}
		LOGGER.info("error block "+request.getErrorDescription());
		return request;
	}
	
	/**
	 * @author Sunil Desu
	 * @since May 28 2013
	 * 
	 * @param errorCode
	 * @param errorMessage
	 */
	private String getErrorMessageFromErrorCode(int errorCode){
		String errorMessage ="Your request cannot be completed at this time. Please try again later.";
		if(NO_PLAN_FOUND_ERRORS.contains(errorCode)){
			errorMessage="No plans can be found for the provided data.";
		}
		return errorMessage;
	}
}
