import React, { Fragment } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '@cmsgov/design-system-core';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import classNames from 'classnames';
import _ from 'lodash';

import { TextField } from '../components/form';
import phoneNumberUtils from '../utils/phoneNumberUtils';
import { submitUpdatePhoneNumber } from '../actions/actionCreators';
import ENDPOINTS from '../constants/endPointsList';
import { validatePhoneNumber } from '../utils/validators';

const UpdatePhoneNumber = props => {
  const { handleSubmit, available, status } = props;

  const submitPhone = values => {
    let errors = {};
    let error = validatePhoneNumber(values.brokerConnectPhone);
    if (error) {
      errors.brokerConnectPhone = error;
    }

    if (errors && !_.isEmpty(errors)) {
      throw new SubmissionError(errors);
    }

    let history;
    let body = {
      available,
      phoneNumber: phoneNumberUtils.cleanedUpPhone(values.brokerConnectPhone),
      status
    };
    props.actions.submitUpdatePhoneNumber(
      ENDPOINTS.PARTICIPATION_DETAILS_ENDPOINT,
      body,
      history
    );
  };

  return (
    <Fragment>
      <div className="ds-l-row header">
        <div className="ds-l-xl-col--10 ds-l-lg-col--10 ds-l-md-col--7 ds-l-sm-col--9 ds-l-xs-col">
          <h4>
            <FormattedMessage
              id="broker.updatePhone.header"
              defaultMessage="Update Broker Connect Phone Number"
            />
          </h4>
        </div>
      </div>
      <form onSubmit={handleSubmit(submitPhone)}>
        <div className="ds-l-form-row">
          <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3 ds-u-padding-top--1">
            <label htmlFor="brokerConnectPhone">
              <FormattedMessage
                id="broker.updatePhone.label"
                defaultMessage="Update Phone Number"
              />
            </label>
          </div>
          <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--9 ds-l-lg-col--6 ds-l-xl-col--5">
            <Field
              component={TextField}
              name="brokerConnectPhone"
              id="brokerConnectPhone"
              className={classNames('ds-c-field ds-c-field--medium')}
              normalize={phoneNumberUtils.normalizePhone}
            />
          </div>
          <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--12 ds-l-lg-col--12 ds-l-xl-col--4">
            <div class="ds-u-float--right">
              <Button
                className="ds-u-margin--1 nv-button-primary"
                variation="primary"
                id="save_availability"
                type="submit"
                disabled={status === 'INACTIVE'}
              >
                <FormattedMessage
                  id="broker.updatePhone.button.text"
                  defaultMessage="Update Phone Number"
                />
              </Button>
            </div>
          </div>
        </div>
      </form>
    </Fragment>
  );
};

const validate = values => {
  //console.log(values);
  let errors = {};

  errors.brokerConnectPhone = validatePhoneNumber(values.brokerConnectPhone);

  return errors;
};

const mapStateToProps = state => {
  return {
    status: state.brokerConnectReducer.status,
    available:
      state.brokerAvailabilityReducer.available ||
      state.brokerConnectReducer.isAvailable,
    initialValues: {
      brokerConnectPhone: phoneNumberUtils.normalizePhone(
        state.brokerConnectReducer.phoneNumber,
        ''
      )
    }
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      submitUpdatePhoneNumber
    },
    dispatch
  )
});

const enableReinitialize = true;

const formWrapped = reduxForm({
  form: 'UpdatePhoneNumber',
  validate,
  enableReinitialize
})(UpdatePhoneNumber);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(formWrapped);
