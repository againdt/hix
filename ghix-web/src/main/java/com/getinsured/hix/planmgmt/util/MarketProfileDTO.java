/**
 * 
 */
package com.getinsured.hix.planmgmt.util;

import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;
/**
 * @author gorai_k
 *
 */
public class MarketProfileDTO {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * This validation group provides groupings for fields  {@link #indvCustServicePhone}, 
	 * {@link #indvCustServicePhoneExt}, {@link #indvCustServiceTollFree}, {@link #indvCustServiceTTY} 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditIndividualProfile extends Default{
		
	}
	
	@Pattern(regexp="( )*([0-9]{0,10})?",message="{err.customerServicePhone}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String customerServicePhone;
	
	@Pattern(regexp="( )*([0-9]{0,11})?",message="{err.customerServiceExt}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String customerServiceExt;
	
	@Pattern(regexp="( )*([0-9]{0,10})?",message="{err.issuerPhoneInvalid}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String custServiceTollFreeNumber;
	
	@Pattern(regexp="( )*([0-9]{0,10})?",message="{err.issuerPhoneInvalid}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String custServiceTTY;
	
	//@Pattern(regexp="(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2}([w]{3}[\\.]){0,1})|(^[w]{3}[\\.]))?[a-zA-Z0-9]+[\\.]{1}[a-zA-Z]+[\\.]?[a-zA-Z]+$)|^$",message="{err.validfacingWebSite}",groups={MarketProfileDTO.EditIndividualProfile.class})
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={MarketProfileDTO.EditIndividualProfile.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$",message="{err.validfacingWebSite}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String facingWebSite;

	@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validPaymentWebSite}",groups={MarketProfileDTO.EditIndividualProfile.class})
	private String paymenturl;

	/**
	 * @return the paymenturl
	 */
	public String getPaymenturl() {
		return paymenturl;
	}

	/**
	 * @param paymenturl the paymenturl to set
	 */
	public void setPaymenturl(String paymenturl) {
		this.paymenturl = paymenturl;
	}

	public String getCustomerServicePhone() {
		return customerServicePhone;
	}

	public void setCustomerServicePhone(String customerServicePhone) {
		this.customerServicePhone = customerServicePhone;
	}

	public String getCustomerServiceExt() {
		return customerServiceExt;
	}

	public void setCustomerServiceExt(String customerServiceExt) {
		this.customerServiceExt = customerServiceExt;
	}

	public String getCustServiceTollFreeNumber() {
		return custServiceTollFreeNumber;
	}

	public void setCustServiceTollFreeNumber(String custServiceTollFreeNumber) {
		this.custServiceTollFreeNumber = custServiceTollFreeNumber;
	}

	public String getCustServiceTTY() {
		return custServiceTTY;
	}

	public void setCustServiceTTY(String custServiceTTY) {
		this.custServiceTTY = custServiceTTY;
	}

	public String getFacingWebSite() {
		return facingWebSite;
	}

	public void setFacingWebSite(String facingWebSite) {
		this.facingWebSite = facingWebSite;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
