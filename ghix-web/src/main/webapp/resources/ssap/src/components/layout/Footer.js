import {Fragment} from 'react';
import React from 'react';
import {bindActionCreators} from 'redux';
import {fetchHeaderFooterData} from '../../actions/actionCreators';
import { connect } from 'react-redux';

const Footer = (props) => {
    const {
        devEnvironment,
        exchangeName,
        stateCode
    } = props.footerLinks;
    const {
        AGENT_BROKER_SIGNUP: agentBrokerSignup,
        ENTITY_SIGNUP: entitySignup
    } = props.footerLinks.links;

    return(
        <Fragment>
            <footer id="footer" className="usa-footer" role="contentinfo">
                <div className="footer__container usa-footer-primary-section">
                    <div className="usa-grid footer__container_wrapper">
                        { stateCode == 'MN' ? (
                            <ul>
                                <p>
                            &copy;{(new Date().getFullYear())}
                                    <a href='http://mnsure.org'>MNsure</a>
                                </p>
                                <p>
                                    <a href='https://www.mnsure.com'>Privacy Policy</a>
                                </p>
                            </ul>
                        ) : (
                            <nav className="usa-footer-nav usa-width-one-whole">
                                <ul className="usa-unstyled-list usa-width-one-whole usa-footer-primary-content">
                                    <li className="usa-footer-primary-link">
                                        <a id="footer__logo" href="#" className="seal">
                                            <img src="https://d1q4hslcl8rmbx.cloudfront.net/assets/themes/ssh-en/assets/img/state-seal.png" width="86" alt="State of Nevada"/>
                                        </a>
                                    </li>
                                    <li>
                                        <p>
                                            {exchangeName} is brought to you by the <a href='http://exchange.nv.gov/'>State of Nevada Silver State Health Insurance Exchange</a>
                                        </p>
                                        <p>The Official Site of the Silver State Health Insurance Exchange | Copyright &copy; State of Nevada - All
                                            Rights Reserved |
                                        <a href='https://www.nevadahealthlink.com/privacy-policy' title='Link open in new window or tab'>Privacy Policy</a>
                                        </p>
                                    </li>
                                </ul>
                            </nav>      
                        )
                        }
                        {
                            devEnvironment &&
                            <nav className="usa-footer-nav">
                                <ul className="usa-unstyled-list">
                                    <li className="usa-width-one-half usa-footer-primary-content"><a href={agentBrokerSignup}>Health Insurance Agent/Broker </a></li>
                                    <li className="usa-width-one-half usa-footer-primary-content"><a href={entitySignup}>Enrollment Entities</a></li>
                                </ul>
                            </nav>
                        }
                    </div>
                </div>
            </footer>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    footerLinks: state.headerLinks
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        fetchHeaderFooterData
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);