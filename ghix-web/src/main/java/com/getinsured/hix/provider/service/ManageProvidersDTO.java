package com.getinsured.hix.provider.service;

import org.apache.commons.lang3.StringUtils;

public class ManageProvidersDTO {

	private String groupKey;
	private String name;
	private String specialities;
	private String inNetworkIds;
	private String unknownNetworkIds;

	public ManageProvidersDTO() {
	}

	public void setProviderField(String fieldName, Object fieldValue) {

		if (StringUtils.isNotBlank(fieldName) && null != fieldValue) {

			switch (fieldName) {
				case "group_key_prac_key" : this.groupKey = fieldValue.toString(); break;
				case "name" : this.name = fieldValue.toString(); break;
				case "specialty_name" : this.specialities = fieldValue.toString(); break;
				case "network_id" : this.inNetworkIds = fieldValue.toString(); break;
//				case "" : this.unknownNetworkIds = fieldValue.toString(); break;
			}
		}
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialities() {
		return specialities;
	}

	public void setSpecialities(String specialities) {
		this.specialities = specialities;
	}

	public String getInNetworkIds() {
		return inNetworkIds;
	}

	public void setInNetworkIds(String inNetworkIds) {
		this.inNetworkIds = inNetworkIds;
	}

	public String getUnknownNetworkIds() {
		return unknownNetworkIds;
	}

	public void setUnknownNetworkIds(String unknownNetworkIds) {
		this.unknownNetworkIds = unknownNetworkIds;
	}
}
