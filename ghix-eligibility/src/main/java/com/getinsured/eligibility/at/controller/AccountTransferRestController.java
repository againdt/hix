package com.getinsured.eligibility.at.controller;


import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.ref.service.ReferralActivationNotificationService;
import com.getinsured.eligibility.at.resp.service.EligibilityNotificationService;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.notification.dto.SsapApplicationEligibilityDTO;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;


/**
 * Rest Controller for performing account transfer processing operations.
 *
 * @author Ekram Ali Kazi
 *
 */
@Controller("accountTransferRestController")
@RequestMapping("/atresponse")
public class AccountTransferRestController {

	private static final String ERP_NOTIFICATION_UPDATED = "ERP_NOTIFICATION_UPDATED";
	private static final String CSR_ACTION = "CSR_ACTION";
	private static final String FAILURE = "FAILURE";
	private static final String ERP_NOTIFICATION = "ERP_NOTIFICATION";
	private static final String SUCCESS = "SUCCESS";
	private static final String ERROR_ACTION = "ERROR_ACTION";
	private static final String ACCOUNT_TRANSFER_ERP = "ACCOUNT_TRANSFER_ERP";

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountTransferRestController.class);
	@Autowired private EligibilityNotificationService eligibilityNotificationService;
	@Autowired private IntegrationLogService integrationLogService;
	@Autowired private ReferralActivationNotificationService referralActivationNotificationService;
	

	@RequestMapping(value = "/erpactions", method = RequestMethod.POST)
	@ResponseBody
	public String postEligibilityDecisionActions(@RequestBody String actionMappingXmlString) {

		LOGGER.info("AccountTransferRestController - starts....");
		LOGGER.info("AccountTransferRestController -  " + actionMappingXmlString);

		@SuppressWarnings("unchecked")
		Map<String, Object> resultMap = (Map<String, Object>) EligibilityUtils.unmarshal(actionMappingXmlString);
		String caseNumber = (String) resultMap.get("SSAP_APPLICATION_ID");

		ERPResponse erpResponse = (ERPResponse) resultMap.get(EligibilityConstants.ERP_RESPONSE);
		String serviceName = ACCOUNT_TRANSFER_ERP;
		String status = SUCCESS;
		Long ssap_pk = erpResponse.getSsapApplicationPrimaryKey();
		Long giWsPayloadId = erpResponse.getGiWsPayloadId() != null ? erpResponse.getGiWsPayloadId().longValue() : null;

		integrationLogService.save(actionMappingXmlString, serviceName, status, ssap_pk, giWsPayloadId);
/**
 * Modified by pravin. Notification will not be sent during referral process. Notification will be sent only for non-financial flow.
 * Hix-51812
 */
		
		return "Post Eligibility Decision Actions Completed for -  " + caseNumber;
	}

	private String generateApplicationEligibilityNotification(String caseNumber, Long ssap_pk, Long giWsPayloadId) {
		String result;
		try {
			String ecmId = eligibilityNotificationService.generateEligibilityNotificationInInbox(caseNumber);
			result = "Post Eligibility Decision Actions - Notification generated for case Number - " + caseNumber + " document id - " + ecmId;

			String serviceName = ERP_NOTIFICATION;
			String status = SUCCESS;
			integrationLogService.save(result, serviceName, status, ssap_pk, giWsPayloadId);

		} catch (Exception e) {
			// Log in DB to re-trigger notifications from batch process..
			String message = "Error occured while generating eligibility notification for caseNumber - " + caseNumber +  " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(message);
			result = "Post Eligibility Decision Actions - Notification not generated for case Number - " + caseNumber;

			String serviceName = ERP_NOTIFICATION;
			String status = FAILURE;
			integrationLogService.save(message , serviceName, status, ssap_pk, giWsPayloadId);
		}
		return result;
	}

	private boolean generateUniversalEligibilityNotification(String caseNumber) {
		try {
			LOGGER.debug("generateUniversalEligibilityNotification::caseNumber {}", caseNumber);
			// TODO: Replace UniversalEligibilityNotice template name with actual new template name
			String ecmId = eligibilityNotificationService.generateEligibilityNotificationInInbox(caseNumber, "UniversalEligibilityNotification");

			if(ecmId != null && ecmId.length() > 0) {
				LOGGER.debug("generateUniversalEligibilityNotification::generated notification ecmId {}", ecmId);
				return true;
			}
		} catch (Exception ex) {
			LOGGER.error("generateUniversalEligibilityNotification::error generating notification", ex);
		}

		return false;
	}

	@RequestMapping(value = "/notifications/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> sendEligibilityNotification(@PathVariable("caseNumber") String caseNumber) {

		String result = generateApplicationEligibilityNotification(caseNumber, null, null);

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/notifications/universaleligibility/{caseNumber}")
	@ResponseBody
	public ResponseEntity<String> sendUniversalEligibilityNotification(@PathVariable("caseNumber") String caseNumber) {
		String result = "";
		boolean success = false;
		try {
			if(caseNumber != null && caseNumber.length() > 0) {
				success = generateUniversalEligibilityNotification(caseNumber);
				LOGGER.debug("sendUniversalEligibilityNotification::sending of universal notice was {}", success);
			}
		} catch (Exception ex) {
			LOGGER.error("sendUniversalEligibilityNotification::error sending notification", ex);
		}

		return new ResponseEntity<>(result, success ? HttpStatus.OK :
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value = "/notifications/updated/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> sendUpdatedEligibilityNotification(@PathVariable("caseNumber") String caseNumber) {

		String result;
		try {
			String ecmId = eligibilityNotificationService.generateEligibilityNotificationUpdatedInInbox(caseNumber);
			result = "Post Eligibility Decision Actions - Updated Notification generated for case Number - " + caseNumber + " document id - " + ecmId;
		} catch (Exception e) {
			String message = "Error occured while generating updated eligibility notification for caseNumber - " + caseNumber +  " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(message);
			result = "Post Eligibility Decision Actions - Updated Notification not generated for case Number - " + caseNumber;

			String serviceName = ERP_NOTIFICATION_UPDATED;
			String status = FAILURE;
			integrationLogService.save(message , serviceName, status, null, null);
		}

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/test/notifications/data/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<SsapApplicationEligibilityDTO> test2(@PathVariable("caseNumber") String caseNumber) {

		SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = eligibilityNotificationService.getApplicationData(caseNumber);
		return new ResponseEntity<SsapApplicationEligibilityDTO>(ssapApplicationEligibilityDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/csractions", method = RequestMethod.POST)
	@ResponseBody
	public String csrActions(@RequestBody String csrXmlString) {

		LOGGER.info("AccountTransferRestController csrString - " + csrXmlString);

		@SuppressWarnings("unchecked")
		Map<String, Object> resultMap = (Map<String, Object>) EligibilityUtils.unmarshal(csrXmlString);

		String caseNumber = (String) resultMap.get("SSAP_APPLICATION_ID");
		ERPResponse erpResponse = (ERPResponse) resultMap.get(EligibilityConstants.ERP_RESPONSE);
		String serviceName = ACCOUNT_TRANSFER_ERP;
		String status = CSR_ACTION;
		Long ssap_pk = erpResponse.getSsapApplicationPrimaryKey();
		Long giWsPayloadId = erpResponse.getGiWsPayloadId() != null ? erpResponse.getGiWsPayloadId().longValue() : null;

		LOGGER.info("AccountTransferRestController - Data mis-match in received AT Response... CSR Action starts for ssapApplicationId - " + caseNumber);
		integrationLogService.save(csrXmlString, serviceName, status, ssap_pk, giWsPayloadId);

		return "AccountTransferRestController - Data mis-match in received AT Response... CSR Action starts for ssapApplicationId - " + caseNumber;
	}

	@RequestMapping(value = "/erroractions", method = RequestMethod.POST)
	@ResponseBody
	public String errorActions(@RequestBody String errorXmlString) {

		LOGGER.info("AccountTransferRestController errorXmlString - " + errorXmlString);

		@SuppressWarnings("unchecked")
		Map<String, Object> resultMap = (Map<String, Object>) EligibilityUtils.unmarshal(errorXmlString);
		String caseNumber = (String) resultMap.get("SSAP_APPLICATION_ID");
		String errorReason = (String) resultMap.get("ERROR_REASON");

		ERPResponse erpResponse = (ERPResponse) resultMap.get(EligibilityConstants.ERP_RESPONSE);
		String serviceName = ACCOUNT_TRANSFER_ERP;
		String status = ERROR_ACTION;
		Long ssap_pk = erpResponse.getSsapApplicationPrimaryKey();
		Long giWsPayloadId = erpResponse.getGiWsPayloadId() != null ? erpResponse.getGiWsPayloadId().longValue() : null;

		LOGGER.info("AccountTransferRestController - errorActions starts for ssapApplicationId - " + caseNumber);
		LOGGER.info("AccountTransferRestController - errorActions - errorReason - " + errorReason);

		integrationLogService.save(errorXmlString, serviceName, status, ssap_pk, giWsPayloadId);

		return "AccountTransferRestController - errorActions starts for ssapApplicationId - " + caseNumber;
	}
	
	@RequestMapping(value = "/initiateActivation/{applicationId}", method = RequestMethod.GET)
	public @ResponseBody String findHouseholdByShoppingId(@PathVariable Long applicationId){
		try {
			referralActivationNotificationService.initiateActivation(applicationId);
		} catch (Exception e) {
			LOGGER.error("Error occured while initiateActivation for application Id" + applicationId, e);
			return "FAILURE";
		}
		return "SUCCESS";
	}

}
