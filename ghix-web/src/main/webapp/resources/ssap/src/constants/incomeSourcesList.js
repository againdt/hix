
import { INCOME_TYPES, EXPENSE_TYPES } from './incomeAndExpenceTypes';
import INPUT_TYPES from './inputTypes';
import { FREQUENCY_TYPES } from './frequencyTypes';


export const FREQUENCY_OPTIONS_TYPES = {
    'type_0': [],
    'type_1': [
        'default',
        FREQUENCY_TYPES.HOURLY,
        FREQUENCY_TYPES.DAILY,
        FREQUENCY_TYPES.WEEKLY,
        FREQUENCY_TYPES.BIWEEKLY,
        FREQUENCY_TYPES.BIMONTHLY,
        FREQUENCY_TYPES.MONTHLY,
        FREQUENCY_TYPES.YEARLY,
        FREQUENCY_TYPES.ONCE
    ],
    'type_2': [
        'default',
        FREQUENCY_TYPES.WEEKLY,
        FREQUENCY_TYPES.BIWEEKLY,
        FREQUENCY_TYPES.BIMONTHLY,
        FREQUENCY_TYPES.MONTHLY,
        FREQUENCY_TYPES.YEARLY,
        FREQUENCY_TYPES.ONCE
    ],
    'type_3': [
        'default',
        FREQUENCY_TYPES.MONTHLY,
        FREQUENCY_TYPES.QUARTERLY,
        FREQUENCY_TYPES.YEARLY,
        FREQUENCY_TYPES.ONCE
    ],
    'type_4': [
        'default',
        FREQUENCY_TYPES.MONTHLY
    ],
    'type_5': [
        'default',
        FREQUENCY_TYPES.YEARLY
    ]
};

// Positions:
//  for notices:
//      0 -   between <incomeType> and <additionalInput> on position 1
//      1 -   between <incomeType> and <additionalInput> on position 1 but before common question 'How much...'
//      2 -   between <incomeType> and <amountInput> (there is no <additionalInput> on position 1)
//      3 -   between <amount> and <profitOrLoss>
//
//  for additional inputs:
//      1 - between <incomeType> and <amount>
//      2 - between <amount> and <frequency>
//      3 - after <frequency>

export const INCOME_SOURCES = [
    {
        type: INCOME_TYPES.ALIMONY,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from alimony?',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.CAPITAL_GAIN,
        questionText: 'How much does [Full Name] expect to get from capital gains for all of [Coverage Year]?',
        notice: {
            text: 'Net capital gains are the profit after subtracting capital losses',
            positionOnPage: 2
        },
        additionalInput: {
            additionalInputName: 'profitOrLoss',
            additionalInputText: 'Profit or Loss',
            inputType: INPUT_TYPES.DROPDOWN,
            required: true,
            positionOnPage: 2
        },
        frequencyOptions: 'type_5'
    },
    {
        type: INCOME_TYPES.INVESTMENT,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from investment income, like interest and dividends?',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_3'
    },
    {
        type: INCOME_TYPES.FARMING_FISHING,
        isOnlyPositiveAvailable: true,
        questionText: 'How much does [Full Name] get from net farming or fishing income (the profit after subtracting costs)?',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.JOB,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you currently get from this job?',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            additionalInputName: 'employerName',
            additionalInputText: 'Name of employer',
            inputType: INPUT_TYPES.REGULAR_INPUT,
            required: true,
            positionOnPage: 1
        },
        frequencyOptions: 'type_1'
    },
    {
        type: INCOME_TYPES.OTHER,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from other sources?',
        notice: {
            text: 'You don\'t need to tell us about child support, veteran\'s payments or Supplemental Security Income(SSI)',
            positionOnPage: 1
        },
        additionalInput: {
            additionalInputName: 'otherIncomeTypes',
            additionalInputText: 'Income Type',
            inputType: INPUT_TYPES.DROPDOWN,
            required: true,
            positionOnPage: 1
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.PENSION,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from pension account?',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.RETIREMENT,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from retirement account(s)?',
        notice: {
            text: 'Include amounts received as s distribution from a retirement even if you are not retired',
            positionOnPage: 2
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.RENTAL_ROYALTY,
        questionText: 'How much does [Full Name] get from net rental income (after subtracting property expenses) or royalty income (the profit after subtracting costs).',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            additionalInputName: 'profitOrLoss',
            additionalInputText: 'Profit or Loss',
            inputType: INPUT_TYPES.DROPDOWN,
            required: true,
            positionOnPage: 2
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.SCHOLARSHIP,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from scholarship?',
        notice: {
            text: 'Enter the amount [Full Name] used to pay for education expenses:',
            positionOnPage: 3
        },
        additionalInput: {
            additionalInputName: 'educationExpenses',
            additionalInputText: 'Education Expenses',
            inputType: INPUT_TYPES.MONEY_INPUT,
            isOnlyPositiveAvailable: true,
            required: true,
            positionOnPage: 3
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.SELF_EMPLOYMENT,
        questionText: 'How much net income (profits once expenses are paid) will [Full Name] get from self-employment this month?',
        notice: {
            text: 'You can enter profit or loss',
            positionOnPage: 2
        },
        additionalInput: {
            additionalInputName: 'profitOrLoss',
            additionalInputText: 'Profit or Loss',
            inputType: INPUT_TYPES.DROPDOWN,
            required: true,
            positionOnPage: 2
        },
        frequencyOptions: 'type_4'
    },
    {
        type: INCOME_TYPES.SOCIAL_SECURITY_DISABILITY,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from Social Security?',
        notice: {
            text: 'Don\'t include Supplemental Security Income (SSI)',
            positionOnPage: 2
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_3'
    },
    {
        type: INCOME_TYPES.UNEMPLOYMENT,
        isOnlyPositiveAvailable: true,
        questionText: '',
        notice: {
            text: 'Which state or employer provides [Full Name] with unemployment benefit?',
            positionOnPage: 0
        },
        additionalInput: {
            additionalInputName: 'unemploymentSource',
            additionalInputText: 'State or employer name',
            inputType: INPUT_TYPES.REGULAR_INPUT,
            required: false,
            positionOnPage: 1
        },
        frequencyOptions: 'type_2'
    },
    {
        type: INCOME_TYPES.AI_AN,
        isOnlyPositiveAvailable: true,
        questionText: 'How much income do you get from these sources',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_1'
    },
];

export const DEDUCTIONS_SOURCES = [
    {
        type: EXPENSE_TYPES.ALIMONY,
        isOnlyPositiveAvailable: true,
        questionText: '',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_1'
    },
    {
        type: EXPENSE_TYPES.STUDENT_LOAN_INTEREST,
        isOnlyPositiveAvailable: true,
        questionText: '',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_1'
    },
    {
        type: EXPENSE_TYPES.OTHER,
        isOnlyPositiveAvailable: true,
        questionText: '',
        notice: {
            text: '',
            positionOnPage: null
        },
        additionalInput: {
            positionOnPage: null
        },
        frequencyOptions: 'type_1'
    },
];

