package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class MultiFieldAggregator implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException {
				return objToBeValidated;
		// TODO Auto-generated method stub

	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
