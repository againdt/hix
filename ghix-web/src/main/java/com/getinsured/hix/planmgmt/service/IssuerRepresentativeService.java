package com.getinsured.hix.planmgmt.service;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;

public interface IssuerRepresentativeService {

	/* ##### interface of sending delegation code  ##### */
	void updateDelegationInfo(IssuerRepresentative newIssuerRepresentative, RedirectAttributes redirectAttr, Issuer issuer, AccountUser accountUser);
	
	/* ##### interface of sending delegation code  ##### */
	void updateIssuerDelegationInfo(IssuerRepresentative newIssuerRepresentative, Model model, Issuer issuerObj, AccountUser accountUser);
	
	/* ##### interface of sending email to issuer representative  ##### */
	public void sendNotificationEmailToIssuerRepresentative(Issuer issuerObj);
}
