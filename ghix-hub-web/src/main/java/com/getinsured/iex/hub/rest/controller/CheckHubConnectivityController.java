package com.getinsured.iex.hub.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.hubc.servicehandler.HubConnectivityServiceHandler;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Controller
public class CheckHubConnectivityController extends MessageProcessor {
	
	@Autowired
	private WebServiceTemplate checkHubConnectivityServiceTemplate;
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckHubConnectivityController.class);
	
	@RequestMapping(value = "/checkHubConnectivity", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> checkHubConnectivity(@RequestBody String requestJSON){
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("CHECK_HUB_CONNECTIVITY");
		request.setEndpointOperationName("CHECK_HUB_CONNECTIVITY");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			response = this.executeRequest(request, new HubConnectivityServiceHandler(), this.checkHubConnectivityServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		return response;
	}
	
}
