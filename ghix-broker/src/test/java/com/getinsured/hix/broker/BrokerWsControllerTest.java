/**
 * 
 */
package com.getinsured.hix.broker;


import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.broker.platform.GhixBaseTest;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.broker.BrokerInformationDTO;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * Class to unit test BrokerWsController serviced request.
 * @author chalse_v
 * @version 1.0
 *
 */
public class BrokerWsControllerTest extends GhixBaseTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerWsControllerTest.class);
	
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Method to unit test 'brokerInformation()' method.
	 */
	@Test
	public void brokerInformationTest() {
		LOGGER.info("brokerInformationTest : START");
		
		Assert.assertNotNull("RestTemplate not bound.", restTemplate);
		
		String requestXML = null, responseXML = null;
		BrokerDTO designateResponse = null; 
		final Integer employerId = 24;		
        XStream xstream = GhixUtils.getXStreamStaxObject();
		BrokerInformationDTO brokerInformationDTO = new BrokerInformationDTO();
		
		brokerInformationDTO.setEmployerId(employerId);

		Assert.assertNotNull("BrokerInformationDTO is required for the request.", brokerInformationDTO);
		
		requestXML = xstream.toXML(brokerInformationDTO);

		Assert.assertNotNull("Request missing for Employer ID:" + employerId, requestXML);
		Assert.assertFalse("Invalid request found for Employer ID:" + employerId, requestXML.isEmpty());		
		
		responseXML = restTemplate.postForObject("http://localhost:8080/ghix-broker/broker/brokerinformation", requestXML, String.class);
		
		Assert.assertNotNull("No response found for Employer ID:" + employerId, responseXML);
		Assert.assertFalse("Invalid response found for Employer ID:" + employerId, responseXML.isEmpty());
		
		LOGGER.info("Response:" + responseXML);

        designateResponse = (BrokerDTO)xstream.fromXML(responseXML);        
        
        Assert.assertEquals("Failure response encountered for the request.", BrokerConstants.RESPONSECODESUCCESS, designateResponse.getResponseCode()); 

		LOGGER.info("brokerInformationTest : END");
	}
	
}
