/**
 * 
 */
package com.getinsured.hix.account.util;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.account.web.Loginemailchangeconfirm;
import com.getinsured.hix.account.web.Loginemailchangenotice;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.UserTokens;
import com.getinsured.hix.model.UserTokensEventName;
import com.getinsured.hix.model.UserTokensSourceName;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.tokens.UserTokenService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * @author Biswakesh
 *
 */

@Component
public class UserChangeEmailManager {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserChangeEmailManager.class);
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private UserService userService;
	@Autowired private UserTokenService userTokenService;
	@Autowired private Loginemailchangeconfirm loginemailchangeconfirm;
	@Autowired private Loginemailchangenotice loginemailchangenotice;
	
	/**
	 * @param oldEmailAddress
	 * @param newEmailAddress
	 * @return
	 * @throws NotificationTypeNotFound
	 */
	public String processEmailChangeRequest(String oldEmailAddress, String newEmailAddress)
			throws NotificationTypeNotFound {
		String status = null;
		String randomToken;
		List<UserTokens> deprecatedTokens;
		AccountUser accountUser;

		accountUser = userService.findByEmail(oldEmailAddress);

		deprecatedTokens = userTokenService.findDeprecatedTokens(
				oldEmailAddress,
				UserTokensSourceName.ACC_MGMT.toString(),
				UserTokensEventName.CHANGE_EMAIL.toString());

		if (null != deprecatedTokens && !deprecatedTokens.isEmpty()) {
			userTokenService.deprecateTokens(deprecatedTokens);
		}

		randomToken = createToken(oldEmailAddress, newEmailAddress);

		changeUserEmail(accountUser, newEmailAddress, randomToken);
		notifyOldEmail(accountUser, newEmailAddress);

		status = "Email changed successfully";

		return status;
	}
	
	/**
	 * Create token
	 * @param oldEmailAddress
	 */
	private String createToken(String oldEmailAddress,String newEmailAddress) {
		Long currentTime = -1L;
		String randomToken = null;
		UserTokens userTokens = null;

		try {
			userTokens = new UserTokens();
			userTokens.setSourceName(UserTokensSourceName.ACC_MGMT.toString());
			userTokens.setEventName(UserTokensEventName.CHANGE_EMAIL.toString());
			userTokens.setUserIdentifier(oldEmailAddress);
			
			randomToken = GhixJasyptEncrytorUtil.GENC_TAG_START+ghixJasyptEncrytorUtil.encryptStringByJasypt(newEmailAddress)+GhixJasyptEncrytorUtil.GENC_TAG_END;
			userTokens.setToken(randomToken);
			currentTime = TimeShifterUtil.currentTimeMillis();
			userTokens.setTokenCreationTime(new Timestamp(currentTime));
			userTokens.setTokenExpiry(new Timestamp(currentTime+(24*60*60*1000)));
			userTokens.setUserIdentifier(oldEmailAddress);
			userTokens.setStatus('1');
			userTokenService.createUserToken(userTokens);
		} catch(Exception ex) {
			LOGGER.error("ERR: WHILE CREATING TOKEN: ",ex);
		}

		return randomToken;

	}

	
	/**
	 * Change email
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void changeUserEmail(AccountUser accountUser,String newEmailAddress,String randomToken) throws NotificationTypeNotFound {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("CHNG EMAIL");
		}
		
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", accountUser);
		notificationContext.put("RANDOM_TOKEN", randomToken);
		notificationContext.put("NEW_EMAIL_ADDRESS", newEmailAddress);
		Map<String, String> tokens = loginemailchangeconfirm.getTokens(notificationContext);
		Map<String, String> emailData = loginemailchangeconfirm.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = loginemailchangeconfirm.generateEmail(loginemailchangeconfirm.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+loginemailchangeconfirm.getClass().getSimpleName());
		}
		
		loginemailchangeconfirm.sendEmail(noticeObj);
		
	}
	
	/**
	 * Change email
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void notifyOldEmail(AccountUser accountUser,String newEmailAddress) throws NotificationTypeNotFound {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("NOTIFY OLD EMAIL");
		}
		
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", accountUser);
		notificationContext.put("NEW_EMAIL_ADDRESS", newEmailAddress);
		Map<String, String> tokens = loginemailchangenotice.getTokens(notificationContext);
		Map<String, String> emailData = loginemailchangenotice.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = loginemailchangenotice.generateEmail(loginemailchangenotice.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+loginemailchangenotice.getClass().getSimpleName());
		}
		
		loginemailchangenotice.sendEmail(noticeObj);
	}
}
