package com.getinsured.iex.hub.ridp;




import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.PrimaryRequestInformationType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ResponsePayloadType;
import com.getinsured.iex.hub.ridp.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.ridp.wrapper.ContactInformationWrapper;
import com.getinsured.iex.hub.ridp.wrapper.CurrentAddressWrapper;
import com.getinsured.iex.hub.ridp.wrapper.PersonWrapper;
import com.getinsured.iex.hub.ridp.wrapper.ResponsePayloadWrapper;
import com.getinsured.ridpv2.RIDPNamespaceMapper;

public class RIDPPrimaryServiceHandler extends AbstractServiceHandler {
	
	private static com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory extnFactory = new com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory();
	private static com.getinsured.iex.hub.ridp.cms.dsh.ridp.exchange._1.ObjectFactory exchFactory = new com.getinsured.iex.hub.ridp.cms.dsh.ridp.exchange._1.ObjectFactory();
	private Jaxb2Marshaller marshaller;
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else {
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException {
		JSONObject req = null;
		try{
			if(getJsonInput() != null){
				JSONParser parser = new JSONParser();
				req = (JSONObject) parser.parse(getJsonInput());
				this.processJSONObject(req);
			}
		}catch(Exception pe){
			throw new HubServiceException("failed to process request [Failed with]",pe);
		}
		ContactInformationWrapper contactInformation = (ContactInformationWrapper) getNamespaceObjects().get(ContactInformationWrapper.class.getName());
		PersonWrapper person = (PersonWrapper) getNamespaceObjects().get(PersonWrapper.class.getName()) ;
		
		if(person == null){
			throw new HubServiceException("Mandatory Subject information not available, can not create the RIDP payload for primary service");
		}
		
		PrimaryRequestInformationType preq = extnFactory.createPrimaryRequestInformationType();
		if(contactInformation != null){
			preq.setContactInformation(contactInformation.getSystemRepresentation());
		}
		
		
		preq.setPerson(person.getSystemRepresentation());
		CurrentAddressWrapper address = (CurrentAddressWrapper) getNamespaceObjects().get(CurrentAddressWrapper.class.getName());
		if(address != null){
			preq.setCurrentAddress(address.getSystemRepresentation());
		}
		
		JAXBElement<PrimaryRequestInformationType> pReqJaxb = extnFactory.createPrimaryRequest(preq);
		RequestPayloadType x = extnFactory.createRequestPayloadType();
		x.setRequest(pReqJaxb);
		return exchFactory.createRequest(x);
	}

	
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
			JAXBElement<ResponsePayloadType> resp= (JAXBElement<ResponsePayloadType>)responseObject;
			ResponsePayloadType payload = resp.getValue();
			ResponseMetadataType x = payload.getResponseMetadata();
			if(x != null){
				this.setResponseCode(x.getResponseCode().getValue());
			}
			ResponsePayloadWrapper response = new ResponsePayloadWrapper(payload);
			this.requestStatus = RequestStatus.SUCCESS;
			return response.toJSONString();
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		return "H1.1";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.ridpv2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.ridpv2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}
}
