import React from 'react';
import ProgressBar from '../components/progressBar/ProgressBar';

const ProgressBarContainer = props => {
    return (
        <div
            className="usa-grid"
            // style={{border: '2px solid yellow'}}
        >
            <ProgressBar {...props} />
        </div>
    );
};

export default ProgressBarContainer;
