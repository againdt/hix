<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<link href="../resources/css/assister.css" media="screen" rel="stylesheet" type="text/css" />
<!--[if IE 8]>
	<link href="../resources/css/ie8fixes_nm.css" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
Integer totalResultCount = (Integer)request.getAttribute("resultSize");
Integer currentPage = (Integer)request.getAttribute("currentPage");
int noOfPage = 0;
if(totalResultCount>0){
	if((totalResultCount%pageSizeInt)>0){
		noOfPage = (totalResultCount/pageSizeInt)+1;
	}
	else{
		noOfPage = (totalResultCount/pageSizeInt);
	}
}
%>

<style>

table#list {
    table-layout: fixed;
}
table#list th{padding-top: 0; padding-bottom: 0;}
table#list th {padding-top: 0; padding-bottom: 0;}
</style>
<script src="/hix/resources/js/gi.responsive-table.js" type="text/javascript"></script>
<script type="text/javascript">

function getDetails(val,pagenumber){

	document.getElementById("id").value=val;
	document.getElementById("currentpage").value=pagenumber;
	document.forms["brokerdetail"].submit();
}

</script>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:if test='${stateCode == "CA"}'>
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<c:set var="totalResultCount" value="<%=totalResultCount%>" />

	<div class="row-fluid" id="search_results">
			<div class="gutter10-lr">
				<div class="row-fluid result-count-header">
					<h3>
						<a name="skip" class="skip">
							<c:choose>
								<c:when test="${totalResultCount > 1}">
									<%=totalResultCount%> <spring:message code="label.brkfound"/>
								</c:when>
								<c:otherwise>
									<%=totalResultCount%> <spring:message code="label.brkfound"/>
								</c:otherwise>
							</c:choose>
							<small><c:if test='${searhzipcode != ""}'><span>${searhzipcode}</span></c:if>
							<c:if test='${searchFirstname != ""}'><span>${searchFirstname}</span></c:if>
							<c:if test='${searchLastName != ""}'><span> ${searchLastName}</span></c:if>
							<c:if test='${searchLanguages != ""}'><span> ${searchLanguages}</span></c:if></small>
						</a>
						<span class="search pull-right">
							<a class="btn btn-primary margin20-r" id="aid-brkSearchAgain" href="../broker/search/individual" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Find an Agent', 1]);"><spring:message code="label.brk.SearchAgain"/></a>

						</span>
					</h3>
				</div> <!-- end of page-header -->
				<div>
					<form id="brokerdetail" name="brokerdetail" action="detail" method="post">
					  <df:csrfToken/>
						<input type="hidden" name="id" id="id" value="">
						<input type="hidden" name="currentpage" id="currentpage" value="">
						<c:choose>
							<c:when test="${fn:length(brokerslist) > 0}">
								<!--<c:forEach var="broker" items="${brokerslist}">
									<p><a href="#" onclick="getDetails(${broker.id});"><b>${broker.user.firstName}&nbsp;${broker.user.lastName}</b></a>
									 Email : ${broker.user.email}
									Contact Number : ${broker.contactNumber}
									<a href="#" onclick="getDetails(${broker.id});">More Details</a></p>
								</c:forEach>-->

								<table id="list" class="table table-condense">
									<thead>
										<tr class="header list-header">
											<!-- <th>&#35;</th> -->
											<th><spring:message code="label.brkList.columnHeader.Name"/></th>
											<th><spring:message code="label.brkList.columnHeader.ContactInfo"/></th>
											<c:if test='${searchDistance != ""}'>
												<th><spring:message code="label.brkList.columnHeader.Distance"/></th>
											</c:if>
											<th><spring:message code="label.brkList.columnHeader.ProductExpertise"/></th>
											<th><spring:message code="label.brkList.columnHeader.Languages"/></th>
										</tr>
									</thead>
									<c:forEach items="${brokerslist}" var="broker" begin="0" varStatus="rowCounter" >
<%-- 									<c:set var="encryptedId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set> --%>
										<tr>
											<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="false"/> </c:set>
											 <%-- <td>${rowCounter.index + 1}</td>  --%>
											<td class="broker-name"><a href="#" class="aid-brkName" onclick="getDetails('${encBrokerId}',${currentPage});"><h4 class="link breakword" ><span class="break-word">${broker.user.firstName}&nbsp;${broker.user.lastName}</span></h4></a>
											</td>
											<td>
												<c:choose>
														<c:when test="${stateCode == 'CA' && broker.agencyName != null && broker.agencyName != '' }">
															<div><strong>${broker.agencyName}</strong><br/>
														</c:when>
														<c:otherwise>
															<c:if test="${broker.companyName != null && broker.companyName != ''}"><div><strong>${broker.companyName}</strong><br/></c:if>
														</c:otherwise>
												</c:choose>
												${broker.location.address1} ${broker.location.address2}
												<br/>${broker.location.city}, ${broker.location.state} ${broker.location.zip}
												<br/><c:out value="${broker.contactNumber}" />
												<br/><c:out value="${broker.yourPublicEmail}" /></div>
											</td>
											<c:if test='${searchDistance != ""}'>
												<td class="breakword">
												<fmt:formatNumber type="number"
												minFractionDigits = "1"
            									maxFractionDigits="1" value="${broker.distance}" />m
            									</td>
											</c:if>
											<td class="breakword comma">${broker.productExpertise}</td>
											<td class="breakword comma">${broker.languagesSpoken}</td>
										</tr>
									</c:forEach>
								</table>
							<div class="center">
							 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>
							</c:when>
							<c:otherwise>
								<div class="no-records">
									<b><spring:message code="label.brknomatching"/></b>
								</div>
							</c:otherwise>
						</c:choose>
					</form>
				</div><!-- end of .gutter10 -->

			</div>
	</div>  <!-- end of row-fluid -->

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->
<script>
$(".comma").text(function(i, val) {
    return val.replace(/,/g, ", ");
});
/* accessibity for dynamic content
 */
	window.onload = ResponsiveCellHeaders("list");
 $(document).ready(function () {
	//read page
	$(".pagination a").each(function () {
	    //find the numerical value, make sure that it is greater
	    //than 0 so it's a regular page number
	    var thisval = parseInt($(this).text());
	    if (thisval > 0) {
	        var itxt = $(this).html();
	        //if current page, (.active class), add "current page" to description
	        if($(this).parent().hasClass("active") || $(this).parent().parent().hasClass("active"))
	        {
	            itxt = "<span class='aria-hidden'>Page </span>" + itxt + "<span class='aria-hidden'> Current Page </span>";
	        }
	        //regular description, "Page X"
	            else
	        {
	                itxt = "<span class='aria-hidden'>Page </span>" + itxt;
	        }
	        $(this).html(itxt);
	    }
	});

});

</script>
