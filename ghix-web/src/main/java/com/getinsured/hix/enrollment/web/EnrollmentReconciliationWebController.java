package com.getinsured.hix.enrollment.web;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.enrollment.EnrollmentReconRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @since 08th March 2017
 * @author sharma_k
 *
 */
@Controller
@RequestMapping(value="/enrollmentreconciliation")
public class EnrollmentReconciliationWebController {
	
	private static final String ENROLLMENT_RECON_PERMISSION = "hasPermission(#model,'VIEW_ENROLLMENT_RECONCILIATION_DETAILS')";
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReconciliationWebController.class);
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@GiAudit(transactionName = "getReconMonthlyActivity", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@ApiOperation(value = "Monthly Recon activity", notes = "This method will load all reconciliation summary for a given month")
	@RequestMapping(value = "/enrollmentmonthlyactivity")
	@PreAuthorize(ENROLLMENT_RECON_PERMISSION)
	public String getEnrollmentReconciliationMonthlyActivity(Model model, HttpServletRequest request) {
		
		EnrollmentReconRequest enrollmentReconRequest = new EnrollmentReconRequest();
		Calendar calender = TSCalendar.getInstance();
		calender.setTime(new TSDate());
		
		enrollmentReconRequest.setMonthNum(calender.get(Calendar.MONTH) + 1);
		enrollmentReconRequest.setYear(calender.get(Calendar.YEAR));
		
		String populateCarrierNMonthJson = ghixRestTemplate.getForObject(GhixEndPoints.EnrollmentEndPoints.GET_POPULATED_CARRIER_AND_MONTH_LIST, String.class);
		String monthlyReconDataJson = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.GET_MONTHLY_RECON_ACTIVITY_DATA, platformGson.toJson(enrollmentReconRequest), String.class);
		
		model.addAttribute("populateCarrierNMonthJson", populateCarrierNMonthJson);
		model.addAttribute("monthlyReconDataJson", monthlyReconDataJson);
		
		return "enrollmentreconciliation/enrollmentmonthlyactivity";
		
	}
	
	/**
	 * API to land from Enrollment Edit tool 
	 */
	@ApiOperation(value = "View Enrollment discrepancy details", notes = "This method is the landing page for enrollment discrepancy details")
	@RequestMapping(value = "/enrollmentmonthlyactivity/{encEnrollmentId}", method = {RequestMethod.GET})
	@PreAuthorize(ENROLLMENT_RECON_PERMISSION)
	public String viewEnrollmentDiscrepancyDetail(Model model,
			HttpServletRequest request, @PathVariable("encEnrollmentId") String encEnrollmentId) {
		try {
			String enrollmentIdString = (StringUtils.isNumeric(encEnrollmentId)) ? encEnrollmentId: ghixJasyptEncrytorUtil.decryptStringByJasypt(encEnrollmentId);
			Integer enrollmentId = Integer.parseInt(enrollmentIdString);
	        model.addAttribute("redirectEnrollmentId", enrollmentId);
		} catch (Exception ex) {
			LOGGER.error("Error in viewEnrollment() : ", ex);
		}
		return "enrollmentreconciliation/enrollmentmonthlyactivity";
	}
	
	@GiAudit(transactionName = "Get Populated Carrier and Month Details", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/getpopulatedcarrierandmonthlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getPopulatedMonthAndCarrierList()throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_POPULATED_CARRIER_AND_MONTH_LIST, user.getUserName(), HttpMethod.GET,
						MediaType.APPLICATION_JSON, String.class, null);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
			}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response getPopulatedMonthAndCarrierList(): "+res.toString());
			}
		}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_POPULATED_CARRIER_AND_MONTH_LIST API: ", ex);
			}
		}
		return null;
	}
	
	@GiAudit(transactionName = "Get Monthly Reconciliation details", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/fetchmonthlyreconactivitydata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String fetchMonthlyReconActivityData(@RequestBody String requestString)throws InvalidUserException{
		AccountUser accountUser = userService.getLoggedInUser();
		try{
			if(StringUtils.isNotBlank(requestString)){
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_MONTHLY_RECON_ACTIVITY_DATA, accountUser.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
					
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response fetchMonthlyReconActivityData(): "+res.toString());
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_MONTHLY_RECON_ACTIVITY_DATA API: ", ex);
		}
		return null;
	}
	
	@GiAudit(transactionName = "Search Enrollment Discrepancy", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/searchenrollmentdiscrepancy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String searchEnrollmentDiscrepancy(@RequestBody String requestString)throws InvalidUserException{
		AccountUser accountUser = userService.getLoggedInUser();
		try{
			if(StringUtils.isNotBlank(requestString)){
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.SEARCH_ENROLLMENT_DISCREPANCY, accountUser.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response searchEnrollmentDiscrepancy(): "+res.toString());
				}
				}
			}
		catch(Exception ex){
			LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling SEARCH_ENROLLMENT_DISCREPANCY API: ", ex);
		}
		return null;
	}
	
	@GiAudit(transactionName = "Get Populated Search Discrepancy Drop Down", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/getpopulatedsearchdiscrepancydata", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getPopulatedSearchDiscrepancyDropDown()throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_SEARCH_DISCREPANCY_DROPDOWN_DATA, user.getUserName(), HttpMethod.GET,
						MediaType.APPLICATION_JSON, String.class, null);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response getPopulatedSearchDiscrepancyDropDown(): "+res.toString());
				}
			}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_SEARCH_DISCREPANCY_DROPDOWN_DATA API: ", ex);
			}
		}
		return null;
	}
	
	@GiAudit(transactionName = "Get Populated Discrepancy Detail", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/getenrlrecondiscrepancydetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getEnrlReconDiscrepancyDetail(@RequestBody String requestString)throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_ENRL_RECON_DISCREPANCY_DETAIL, user.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response getEnrlReconDiscrepancyDetail(): "+res.toString());
				}
			}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_ENRL_RECON_DISCREPANCY_DETAIL API: ", ex);
			}
		}
		return null;
	}
	
	@GiAudit(transactionName = "Get discrepancy comment details", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/getenrlreconcommentdetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getEnrlReconCommentDetail(@RequestBody String requestString)throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_ENRL_RECON_COMMENT_DETAIL, user.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response getEnrlReconCommentDetail(): "+res.toString());
				}
			}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_ENRL_RECON_COMMENT_DETAIL API: ", ex);
			}
		}
		return null;
	}
	
	@GiAudit(transactionName = "Add Edit comment or snooze discrepancy", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.UPDATE)
	@RequestMapping(value = "/addeditcommentorsnoozediscrepancy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String addEditOrSnoozeDiscrepancy(@RequestBody String requestString)throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.ADDEDIT_COMMENT_OR_SNOOZE_DISCREPANCY, user.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response addEditOrSnoozeDiscrepancy(): "+res.toString());
				}
			}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling ADDEDIT_COMMENT_OR_SNOOZE_DISCREPANCY API: ", ex);
			}
		}
		return null;
	}
	
	@GiAudit(transactionName = "Get visual summary of dicsrepancy", eventType = EventTypeEnum.ENROLLMENT_RECONCILIATION, eventName = EventNameEnum.READ)
	@RequestMapping(value = "/getvisualsummarydetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getVisualSummaryDetail(@RequestBody String requestString)throws InvalidUserException{
		
		AccountUser user = userService.getLoggedInUser();
		if(null != user && StringUtils.isNotBlank(user.getEmail())){
			try{
				ResponseEntity<String> res = ghixRestTemplate.exchange(
						GhixEndPoints.EnrollmentEndPoints.GET_VISUAL_SUMMARY_DETAIL, user.getUserName(), HttpMethod.POST,
						MediaType.APPLICATION_JSON, String.class, requestString);
				
				if (res != null && res.getStatusCode().equals(HttpStatus.OK) && res.hasBody()) {
					return res.getBody();
				}
				else
				{
					LOGGER.error("ENRL_RECON_WEB_CONTROLLER :: Received empty or failure response getVisualSummaryDetail(): "+res.toString());
				}
			}
			catch(Exception ex){
				LOGGER.error("ENRL_RECON_WEB_CONTROLLER:: Exception occurred while calling GET_VISUAL_SUMMARY_DETAIL API: ", ex);
			}
		}
		return null;
	}

	
}
