package com.getinsured.web.model;

/**
 * Contains all property types.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public enum PropertyType
{
  GOOGLE_ANALYTICS_CODE,
  GOOGLE_ANALYTICS_CONTAINER_ID;
}
