package com.getinsured.hix.broker.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * FAQ Controller class to intercept any FAQ specific requests.
 * 
 */
@Controller
public class FaqController {
	private static final Logger LOGGER = LoggerFactory.getLogger(FaqController.class);

	/**
	 * Intercepts any FAQ specific requests
	 * 
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to FAQ page
	 */
	@RequestMapping(value = "/broker/faq", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String interceptRequest(Model model, HttpServletRequest request) {
		LOGGER.info("interceptRequest : START");
		LOGGER.info("InterceptRequest method has been invoked");
		LOGGER.info("interceptRequest : END");
		return "broker/faq";
	}
}
