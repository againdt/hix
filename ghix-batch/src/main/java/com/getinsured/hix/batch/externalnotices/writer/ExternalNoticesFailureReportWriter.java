package com.getinsured.hix.batch.externalnotices.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.externalnotices.service.ExternalNoticesFailureReportService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.ExternalNotice;

@Component("externalNoticesFailureReportWriter")
@Scope("step")
public class ExternalNoticesFailureReportWriter implements ItemWriter<ExternalNotice> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticesFailureReportWriter.class);
	private JobService jobService;
	private ExternalNoticesFailureReportService externalNoticesFailureReportService;
	long jobExecutionId = -1;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends ExternalNotice> items) throws Exception {
		List<ExternalNotice> externalNotices = new ArrayList<ExternalNotice>();
		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}

		if (items != null && !items.isEmpty()) {
			externalNotices.addAll(items);
			LOGGER.info("Inside the ExternalNoticesFailureReportWriter");
			externalNoticesFailureReportService.processFailureRecords(externalNotices);
		}
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public ExternalNoticesFailureReportService getExternalNoticesFailureReportService() {
		return externalNoticesFailureReportService;
	}

	public void setExternalNoticesFailureReportService(
			ExternalNoticesFailureReportService externalNoticesFailureReportService) {
		this.externalNoticesFailureReportService = externalNoticesFailureReportService;
	}
	
}

