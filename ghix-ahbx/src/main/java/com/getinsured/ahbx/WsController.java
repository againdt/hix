package com.getinsured.ahbx;

import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

/**
 * @author panda_p
 * 
 */

@Controller
public class WsController {
	private static final Logger LOGGER = Logger.getLogger(WsController.class);
	private @Autowired GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	/**
	 * @author panda_p
	 * @since 05-Mar-2013
	 * 
	 *        This method is used to test the working of GHIX-AHBX module after
	 *        deployment
	 * 
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() throws HTTPException, Exception {
		//FIXME ....why you need welcome?
		LOGGER.info("Welcome to GHIX-AHBX SOAP Service module");
		return "Welcome to GHIX-AHBX SOAP Service module";
	}
	
	/**
	 * @author Biswakalyan
	 * @since 7-Jan-2014
	 * 
	 *        This method is used to test the simple test for jasypt working for GHIX-AHBX module after
	 *        deployment
	 * 
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/jasyptencrypt/{toEncrypt}", method = RequestMethod.GET)
	@ResponseBody
	public String jasyptEncryptionTest(@PathVariable("toEncrypt") String toEncrypt) throws HTTPException, Exception {
		String encryptedValue = ghixJasyptEncrytorUtil.encryptStringByJasypt(toEncrypt);
		LOGGER.info("jasypt encrypt test");
		return "Encrypted value for provided text:"+toEncrypt+" is: "+encryptedValue;
	}

	/**
	 * @author Biswakalyan
	 * @since 7-Jan-2014
	 * 
	 *        This method is used to test the simple test for jasypt decryption working for GHIX-AHBX module after
	 *        deployment
	 * 
	 * @return
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/jasyptdecrypt", method = RequestMethod.GET)
	@ResponseBody
	public String jasyptDecryptionTest(@RequestParam("toDecrypt") String toDecrypt) throws HTTPException, Exception {
		String decryptedValue = ghixJasyptEncrytorUtil.decryptStringByJasypt(toDecrypt);
		LOGGER.info("jasypt decrypt test");
		return "Decrypted value for provided text:"+toDecrypt+" is: "+decryptedValue;
	}

}
