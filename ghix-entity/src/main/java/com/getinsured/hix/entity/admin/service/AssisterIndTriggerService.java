package com.getinsured.hix.entity.admin.service;

import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;

public interface AssisterIndTriggerService {
	public String triggerInd35(Assister assister);
	
	public Assister triggerInd54(Assister assister);

	EntityResponseDTO triggerInd47(DesignateAssister designateAssister);
}
