/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095XmlParams;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlJobThread;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlWrapper;
import com.getinsured.hix.batch.householdinfo.irs.service.IrsAnnualHouseholdService;
import com.getinsured.hix.dto.enrollment.EnrollmentAnnualIrsRecipientDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentIrsCoveredIndividualDTO;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollment1095ValidationReportRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrollmentRequotingService;
import com.getinsured.hix.enrollment.util.Enrollment1095Configuration.Enrollment1095ConfigurationEnum;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentExternalRestUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment1095.service.Enrollment1095NotificationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.Enrollment1095.CorrectionSource;
import com.getinsured.hix.model.enrollment.Enrollment1095.YorNFlagIndicator;
import com.getinsured.hix.model.enrollment.Enrollment1095ValidationReport;
import com.getinsured.hix.model.enrollment.Enrollment1095ValidationReport.ProcessedFlag;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095.MemberType;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.EnrollmentPremium1095;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

import us.gov.treasury.irs.annual.common.BatchCategoryCodeType;

/**
 * @author negi_s
 *
 */
@Service("enrollmentAnnualIrsReportService")
@Transactional
public class EnrollmentAnnualIrsReportServiceImpl implements
EnrollmentAnnualIrsReportService {
	
	public static enum Month {
		JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIrsReportServiceImpl.class);

	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private IEnrolleeRepository enrolleeRepository;
	@Autowired private IrsAnnualHouseholdService irsAnnualHouseholdService;
	@Autowired private IEnrollment1095Repository enrollment1095Repository;
	@Autowired private IEnrollment1095ValidationReportRepository iEnrollment1095ValidationReportRepository;
	@Autowired private UserService userService;
//	@Autowired private Enrollment1095StagingParams enrollment1095StagingParams;
	@Autowired private Enrollment1095XmlBatchService enrollment1095XmlBatchService;
	@Autowired private EnrollmentAnnualIrsBatchService enrollmentAnnualIrsBatchService;
	@Autowired private Enrollment1095XmlParams enrollment1095XmlParams;
	@Autowired private Enrollment1095NotificationService enrollment1095NotificationService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private EnrollmentExternalRestUtil enrollmenteExternalRestUtil;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService#getUniqueEnrollmentIds(int)
	 */
	@Override
	public List<Integer> getUniqueEnrollmentIds(int year) throws GIException {
		Date nextYearStartDate = EnrollmentUtils.getStartDateForYear(year+1);
		Date currentYearStartDate = EnrollmentUtils.getStartDateForYear(year);
		List<Integer> enrollmentIds = enrollmentRepository.getEnrollmentsForAnnualIrsReport(DateUtil.dateToString(currentYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(nextYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		return enrollmentIds;
	}


	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService#getUniqueHouseHolds(int)
	 */
	@Override
	public List<String> getUniqueHouseHolds(int year) throws GIException {
		List<String> houseHoldIdList = null; 
		Date nextYearStartDate = EnrollmentUtils.getStartDateForYear(year+1);
		Date currentYearStart = EnrollmentUtils.getStartDateForYear(year);
		if(EnrollmentUtils.isNotNullAndEmpty(nextYearStartDate) && EnrollmentUtils.isNotNullAndEmpty(currentYearStart)){
			houseHoldIdList =  enrollmentRepository.getUniqueHouseHoldIDList(DateUtil.dateToString(currentYearStart, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),  DateUtil.dateToString(nextYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}
		return houseHoldIdList;
	}


	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService#processAnnualIrsEnrollment(Integer, int)
	 */
	@Override
	public Enrollment1095 processAnnualIrsEnrollment(Integer enrollmentId, Boolean isFreshRun ,Map<String, String> failedEnrollmentMap) throws GIException {

		Enrollment1095 enrollment1095record = new Enrollment1095();
		Enrollment hltEnrollment = enrollmentRepository.findById(enrollmentId);
		
//		EnrollmentAnnualIrsPolicyDTO enrollmentAnnualIrsPolicyDTO = new EnrollmentAnnualIrsPolicyDTO();
		//Check flag for fresh run
		boolean isRecordSame = false;
		boolean isAddressSame = false;
		boolean isIssuerNameSame = false;
		
		try{
			Date startDate = hltEnrollment.getBenefitEffectiveDate();
			Calendar startCal = Calendar.getInstance();
			startCal.setTime(startDate);
			populateEnrollment1095Record(enrollment1095record, hltEnrollment, startCal.get(Calendar.YEAR));
			if(!isFreshRun){
				//Populate enrollment1095record
				//Get record from staging table and compare
				Enrollment1095 existingEnrollment1095record = enrollment1095Repository.findByEnrollmentId(enrollmentId);
				if(null != existingEnrollment1095record){
					//Record exists, compare to see if changes present
					isRecordSame =compareExistingRecords(existingEnrollment1095record, enrollment1095record);
					isAddressSame = isAddressSame(existingEnrollment1095record.getRecepient(), enrollment1095record.getRecepient());
					isIssuerNameSame = isIssuerNameSame(existingEnrollment1095record, enrollment1095record);
					if (isRecordSame && isAddressSame && isIssuerNameSame
							&& YorNFlagIndicator.Y.toString().equalsIgnoreCase(existingEnrollment1095record.getIsActive())
							&& YorNFlagIndicator.N.toString().equalsIgnoreCase(existingEnrollment1095record.getIsObsolete())
							&& !YorNFlagIndicator.Y.toString().equalsIgnoreCase(existingEnrollment1095record.getVoidCheckboxindicator())) {
//						failedEnrollmentMap.put(enrollmentId.toString(), "Record unchanged");
						throw new GIException("Skipping :: Record unchanged");//Throw Skip-able exception 
					}else{
						enrollment1095record.setId(existingEnrollment1095record.getId());
						carryForwardFields(existingEnrollment1095record, enrollment1095record);
						enrollment1095record.setIsOverwritten(Boolean.TRUE);
						enrollment1095record.setCorrectionSource(CorrectionSource.DB_REFRESH.toString());
						enrollment1095record.setUpdateNotes("Record updated by staging job");
						if(YorNFlagIndicator.Y.toString().equalsIgnoreCase(existingEnrollment1095record.getVoidCheckboxindicator())){
							enrollment1095record.setVoidCheckboxindicator(null);
							enrollment1095record.setUpdateNotes("Clearing void indicator as enrollment reinstated");
							setCorrectionIndicators(enrollment1095record, existingEnrollment1095record);
						}
						if(YorNFlagIndicator.N.toString().equalsIgnoreCase(existingEnrollment1095record.getIsActive())){
							enrollment1095record.setIsActive(YorNFlagIndicator.Y.toString());
							enrollment1095record.setUpdateNotes("Marking record as active because of enrollment update");
							setCorrectionIndicators(enrollment1095record, existingEnrollment1095record);
						}
						if(YorNFlagIndicator.Y.toString().equalsIgnoreCase(existingEnrollment1095record.getIsObsolete())){
							enrollment1095record.setIsObsolete(YorNFlagIndicator.N.toString());
							enrollment1095record.setUpdateNotes("Clearing obsolete flag as enrollment reinstated");
						}
						if(null != existingEnrollment1095record.getCorrectionSource() 
								&& existingEnrollment1095record.getCorrectionSource().equalsIgnoreCase(CorrectionSource.EDIT_TOOL.toString())){
							enrollment1095record.setEditToolOverwritten(YorNFlagIndicator.Y.toString());	
						}
						if(!isAddressSame){
							enrollment1095record.setIsAddressUpdated(YorNFlagIndicator.Y.toString());
						}
						if(!isIssuerNameSame) {
							enrollment1095record.setUpdateNotes("Policy issuer name updated");
						}
						if(!isRecordSame){
							setCorrectionIndicators(enrollment1095record, existingEnrollment1095record);
							/* HIX-85621 :: Start
							 * 
							enrollment1095record.setCmsXmlFileName(null);
							enrollment1095record.setCmsXmlGeneratedOn(null);
							enrollment1095record.setPdfGeneratedOn(null);
							enrollment1095record.setEcmDocId(null);
							
							HIX-85621 :: End*/
						}
					}
				}else{
					enrollment1095record.setIsOverwritten(Boolean.FALSE);
				}
			}
		}catch(Exception e){
			if(isRecordSame && isAddressSame){
				//Adding this to more specialized exception to skip it from logger
				throw new GIException(EnrollmentConstants.ERROR_CODE_201, e.getMessage(), "LOW");
			}
			else{
			if(null != e.getMessage()){
				failedEnrollmentMap.put(enrollmentId.toString(), e.getMessage());
			}else{
				failedEnrollmentMap.put(enrollmentId.toString(), EnrollmentUtils.shortenedStackTrace(e, 3));
			}
			throw new GIException("Error fetching enrollment", e);
		}
			
		}
		return enrollment1095record;
	}

	/**
	 * Checks if there is an address change in the enrollment 1095
	 * @param existingRecepient
	 * @param newRecepient
	 * @return boolean isAddressSame
	 */
	private boolean isAddressSame(EnrollmentMember1095 existingRecepient, EnrollmentMember1095 newRecepient) {

		if (existingRecepient.getAddress1() == null) {
			if (newRecepient.getAddress1() != null && !newRecepient.getAddress1().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getAddress1().equals(newRecepient.getAddress1())) {
			return false;
		}
		if (existingRecepient.getAddress2() == null) {
			if (newRecepient.getAddress2() != null && !newRecepient.getAddress2().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getAddress2().equals(newRecepient.getAddress2())) {
			return false;
		}
		if (existingRecepient.getCity() == null) {
			if (newRecepient.getCity() != null && !newRecepient.getCity().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getCity().equals(newRecepient.getCity())) {
			return false;
		}
		if (existingRecepient.getCounty() == null) {
			if (newRecepient.getCounty() != null && !newRecepient.getCounty().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getCounty().equals(newRecepient.getCounty())) {
			return false;
		}
		if (existingRecepient.getState() == null) {
			if (newRecepient.getState() != null && !newRecepient.getState().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getState().equals(newRecepient.getState())) {
			return false;
		}
		if (existingRecepient.getZip() == null) {
			if (newRecepient.getZip() != null && !newRecepient.getZip().trim().isEmpty()) {
				return false;
			}
		} else if (!existingRecepient.getZip().equals(newRecepient.getZip())) {
			return false;
		}
		return true;
	}

	/**
	 * Set old fields in new object
	 * @param existingEnrollment1095record
	 * @param enrollment1095record
	 */
	private void carryForwardFields(Enrollment1095 existingEnrollment1095record, Enrollment1095 enrollment1095record) {
		  enrollment1095record.setCreatedBy(existingEnrollment1095record.getCreatedBy());
		  enrollment1095record.setCreatedOn(existingEnrollment1095record.getCreatedOn());
		  enrollment1095record.setOriginalBatchId(existingEnrollment1095record.getOriginalBatchId());
		  enrollment1095record.setResendPdfFlag(existingEnrollment1095record.getResendPdfFlag());
		  enrollment1095record.setNotice(existingEnrollment1095record.getNotice());
		  enrollment1095record.setXmlSkippedFlag(existingEnrollment1095record.getXmlSkippedFlag());
		  enrollment1095record.setPdfSkippedFlag(existingEnrollment1095record.getPdfSkippedFlag());
		  enrollment1095record.setXmlSkippedMsg(existingEnrollment1095record.getXmlSkippedMsg());
		  enrollment1095record.setPdfSkippedMsg(existingEnrollment1095record.getPdfSkippedMsg());
		  enrollment1095record.setBatchCategoryCode(existingEnrollment1095record.getBatchCategoryCode());
		  enrollment1095record.setCorrectedRecordSeqNum(existingEnrollment1095record.getCorrectedRecordSeqNum());
		  enrollment1095record.setInboundBatchCategoryCode(existingEnrollment1095record.getInboundBatchCategoryCode());
		  enrollment1095record.setInboundAction(existingEnrollment1095record.getInboundAction());
		  enrollment1095record.setEnrollmentIn1095Id(existingEnrollment1095record.getEnrollmentIn1095Id());
		  enrollment1095record.setResubCorrectionInd(existingEnrollment1095record.getResubCorrectionInd());
		  
		  /* HIX-85621 :: Start*/
		  enrollment1095record.setPdfGeneratedOn(existingEnrollment1095record.getPdfGeneratedOn());
		  /*enrollment1095record.setEcmDocId(existingEnrollment1095record.getEcmDocId());*/
		  enrollment1095record.setPdfFileName(existingEnrollment1095record.getPdfFileName());
		  enrollment1095record.setCmsXmlFileName(existingEnrollment1095record.getCmsXmlFileName());
		  enrollment1095record.setCmsXmlGeneratedOn(existingEnrollment1095record.getCmsXmlGeneratedOn());
		  
		  /* HIX-85621 :: End*/
		  //enrollment1095record.setInboundErrIndicator(existingEnrollment1095record.getInboundErrIndicator());
		 /* enrollment1095record.setPlrInboundAction(existingEnrollment1095record.getPlrInboundAction());
		  enrollment1095record.setPlrInboundBatchCategoryCode(existingEnrollment1095record.getPlrInboundBatchCategoryCode());
		  enrollment1095record.setPlrSkippedFlag(existingEnrollment1095record.getPlrSkippedFlag());
		  enrollment1095record.setPlrSkippedMsg(existingEnrollment1095record.getPlrSkippedMsg());
		  enrollment1095record.setPlrXmlFileName(existingEnrollment1095record.getPlrXmlFileName());
		  enrollment1095record.setPlrXmlGeneratedOn(existingEnrollment1095record.getPlrXmlGeneratedOn());*/
	}

	/**
	 * Populate enrollment1095 record
	 * @param enrollment1095record
	 * @param hltEnrollment
	 * @param applicableYear
	 * @throws GIException
	 */
	/**
	 * @param enrollment1095record
	 * @param hltEnrollment
	 * @param applicableYear
	 * @throws GIException
	 */
	private void populateEnrollment1095Record(Enrollment1095 enrollment1095record, Enrollment hltEnrollment, int applicableYear) throws GIException {
		enrollment1095record.setExchgAsignedPolicyId(hltEnrollment.getId());
		enrollment1095record.setCoverageYear(applicableYear);
		enrollment1095record.setHouseHoldCaseId(hltEnrollment.getHouseHoldCaseId());
		setPolicyInformation(enrollment1095record, hltEnrollment);
		//Get Covered Individuals
		enrollment1095record.setEnrollmentMembers(getEnrollmentMembers(hltEnrollment, enrollment1095record));
//		enrollment1095record.setEnrollmentPremiums(getEnrollmentPremium(hltEnrollment, applicableYear, enrollment1095record));
		enrollment1095record.setEnrollmentPremiums(getEnrollmentPremiumFromTable(hltEnrollment, applicableYear, enrollment1095record));
		AccountUser user = getUser();
		enrollment1095record.setCreatedBy(user.getId());
		enrollment1095record.setUpdatedBy(user.getId());
		updateAdditionalFields(enrollment1095record,hltEnrollment);
	}
	
	/**
	 * Get premium information from Premium Table
	 * @param hltEnrollment
	 * @param applicableYear
	 * @param enrollment1095record
	 * @return List<EnrollmentPremium1095> 
	 */
	private List<EnrollmentPremium1095> getEnrollmentPremiumFromTable(Enrollment hltEnrollment, int applicableYear,	Enrollment1095 enrollment1095record) {
		
		List<EnrollmentPremium1095> premiumList = new ArrayList<EnrollmentPremium1095>();
		int benefitStartMonth = getMonthFromDate(hltEnrollment.getBenefitEffectiveDate(), applicableYear);
		int benefitEndMonth = getMonthFromDate(hltEnrollment.getBenefitEndDate(), applicableYear);

		List<Enrollment> dentalEnrollmentList = enrollmentRepository.getEnrollmentsForAnnualIRS(hltEnrollment.getHouseHoldCaseId(),
				DateUtil.dateToString(hltEnrollment.getBenefitEndDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(hltEnrollment.getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), "DEN");

		Map<Integer, EnrollmentPremium> premiumMapDb =  hltEnrollment.getEnrollmentPremiumMap();
		if(premiumMapDb != null && premiumMapDb.isEmpty()){
			try{
				boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
		
				if(hltEnrollment!=null && populateMonthlyPremium){
					enrollmentRequotingService.updateMonthlyPremiumForEnrollment(hltEnrollment, false, true, null, true);
					enrollmentRepository.save(hltEnrollment);
					if(!hltEnrollment.getEnrollmentPremiumMap().isEmpty()){
						premiumMapDb = hltEnrollment.getEnrollmentPremiumMap();
						//LOGGER.error("PremiumMapDB .. "+premiumMapDb.size());
					}
				}
			}catch(Exception e){
				LOGGER.error("Error while updating monthly premium for enrollment ID :: "+hltEnrollment.getId(), e);
			}
		}
		for(int month = benefitStartMonth; month < benefitEndMonth+1; month++){
			EnrollmentPremium enrollmentPremiumDb = premiumMapDb.get(month + 1);
			if(null != enrollmentPremiumDb){
				Date endDate = EnrollmentUtils.getStartDateForNextMonthIndividualReport(month,applicableYear);
				Date startDate = new Date();
				try {
					startDate = EnrollmentUtils.getMonthStartDateTime(month, applicableYear);
				} catch (GIException e) {
					LOGGER.debug("Could not get start date", e);
				}
				List<Enrollment> applicableDentalEnrollments = new ArrayList<Enrollment>();
				for(Enrollment denEnrollment : dentalEnrollmentList){
					if(!denEnrollment.getBenefitEndDate().before(startDate)){
						applicableDentalEnrollments.add(denEnrollment);
					}
				}
				EnrollmentPremium1095 enrollmentPremium1095 = getPolicyInfoForMonthFromTable(hltEnrollment, enrollmentPremiumDb , applicableDentalEnrollments , endDate ,startDate, month, applicableYear);
				enrollmentPremium1095.setEnrollment1095(enrollment1095record);
				AccountUser user = getUser();
				enrollmentPremium1095.setCreatedBy(user.getId());
				enrollmentPremium1095.setUpdatedBy(user.getId());
				premiumList.add(enrollmentPremium1095);
			}else{
				LOGGER.info("No Enrollment Premium record found for "+ month + " month for enrollment Id :: " +hltEnrollment.getId());
			}
		}
		return premiumList;
	}


	/**
	 * HIX-87826
	 * New implementation to set premium data from Enrollment Premium table
	 * @param hltEnrollment
	 * @param enrollmentPremiumDb
	 * @param applicableDentalEnrollments
	 * @param endDate
	 * @param startDate
	 * @param month
	 * @param applicableYear 
	 * @return EnrollmentPremium1095
	 */
	private EnrollmentPremium1095 getPolicyInfoForMonthFromTable(Enrollment hltEnrollment, EnrollmentPremium enrollmentPremiumDb,
			List<Enrollment> applicableDentalEnrollments, Date endDate, Date startDate, int month, int applicableYear) {

		EnrollmentPremium1095 enrollmentPremium1095  = new EnrollmentPremium1095();
		enrollmentPremium1095.setMonthNumber(month + 1);
		enrollmentPremium1095.setMonthName(getMonthName(month)); //get month name

		enrollmentPremium1095.setGrossPremium(EnrollmentUtils.round(enrollmentPremiumDb.getGrossPremiumAmount(), 2));
		
		if (null != enrollmentPremiumDb.getGrossPremiumAmount()
				&& (null != hltEnrollment.getEhbPercent() 
				&& hltEnrollment.getEhbPercent().compareTo(0f) != 0)){
			enrollmentPremium1095.setEhbPercent(hltEnrollment.getEhbPercent());
		}
		if(null != enrollmentPremiumDb.getAptcAmount()){
		enrollmentPremium1095.setAptcAmount(EnrollmentUtils.round(enrollmentPremiumDb.getAptcAmount(), 2));
		}else{
			enrollmentPremium1095.setAptcAmount(null);
		}
		enrollmentPremium1095.setSlcspAmount(EnrollmentUtils.round(enrollmentPremiumDb.getSlcspPremiumAmount(), 2));
		enrollmentPremium1095.setSsapAppId(hltEnrollment.getSsapApplicationid());

		if (null != applicableDentalEnrollments && !applicableDentalEnrollments.isEmpty()) {
			Enrollment latestDentalEnrollment = applicableDentalEnrollments.get(0);
			for(Enrollment denEnrollment : applicableDentalEnrollments){
				if(denEnrollment.getBenefitEffectiveDate().after(latestDentalEnrollment.getBenefitEffectiveDate())){
					latestDentalEnrollment = denEnrollment;
				}
			}
			EnrollmentPremium dentalPremiumDb = latestDentalEnrollment.getEnrollmentPremiumForMonth(month+1);
			if(null != dentalPremiumDb){
				Float effectiveDentalAptcAmount = dentalPremiumDb.getAptcAmount();
				if(null != enrollmentPremium1095.getAptcAmount() && null != effectiveDentalAptcAmount){
					enrollmentPremium1095.setAptcAmount(EnrollmentUtils.round((enrollmentPremium1095.getAptcAmount() + effectiveDentalAptcAmount) ,2));				
				}else if(null != effectiveDentalAptcAmount){
					enrollmentPremium1095.setAptcAmount(effectiveDentalAptcAmount);	
				}
			}
			if (applicableYear >= 2017 && null != latestDentalEnrollment.getDntlEssentialHealthBenefitPrmDollarVal() && null != dentalPremiumDb.getGrossPremiumAmount()) {
				enrollmentPremium1095.setPediatricEhbAmt(EnrollmentUtils.round(latestDentalEnrollment.getDntlEssentialHealthBenefitPrmDollarVal() * dentalPremiumDb.getGrossPremiumAmount(), 2));
			} else if (applicableYear < 2017) {
				enrollmentPremium1095.setPediatricEhbAmt(latestDentalEnrollment.getDntlEssentialHealthBenefitPrmDollarVal());
			}
			//count accounted enrollees
			enrollmentPremium1095.setAccountableMemberDental(enrolleeRepository.getAccountableEnrolleeCount(latestDentalEnrollment.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY)).intValue());
		}
		return  enrollmentPremium1095;
	}


	/**
	 * 
	 * @param hltEnrollment
	 * @param applicableYear
	 * @param enrollment1095record 
	 * @return
	 */
	@SuppressWarnings("unused")
	private List<EnrollmentPremium1095> getEnrollmentPremium(Enrollment hltEnrollment, int applicableYear, Enrollment1095 enrollment1095record) {


		List<EnrollmentPremium1095> premiumList = new ArrayList<EnrollmentPremium1095>();

		String householdCaseId = hltEnrollment.getHouseHoldCaseId();
		int benefitStartMonth = getMonthFromDate(hltEnrollment.getBenefitEffectiveDate(), applicableYear);
		int benefitEndMonth = getMonthFromDate(hltEnrollment.getBenefitEndDate(), applicableYear);
		List<EnrollmentAud> dentalEnrollmentList = getLatestDentalEnrollmentForAnnualIrs(hltEnrollment);

		for(int month = benefitStartMonth; month < benefitEndMonth+1; month++){
			// ** get Month wise Enrollment Aud Records **//

			Date endDate = EnrollmentUtils.getStartDateForNextMonthIndividualReport(month,applicableYear);
			Date startDate = new Date();
			try {
				startDate = EnrollmentUtils.getMonthStartDateTime(month, applicableYear);
			} catch (GIException e) {
				LOGGER.debug("Could not get start date", e);
			}

			/**
			 * Get a list of distinct enrollments valid for this month
			 * Find max audit records for each enrollment
			 * 
			 */
			EnrollmentAud enrollmentAudHealth = fetchEnrollmentAudListForMonth(householdCaseId,endDate,startDate, hltEnrollment, "HLT");
//			EnrollmentAud enrollmentAudDental = null;
			List<EnrollmentAud> applicableDentalEnrollments = new ArrayList<EnrollmentAud>();
			if(null != enrollmentAudHealth){
				for(EnrollmentAud denEnrollment : dentalEnrollmentList){
					if(!denEnrollment.getBenefitEndDate().before(startDate)){
						applicableDentalEnrollments.add(denEnrollment);
					}
				}
//				List<Integer> distinctDenEnrollmentIds = enrollmentRepository.getEnrollmentIdsForAnnualIRS(householdCaseId , startDate , enrollmentAudHealth.getBenefitEndDate(), "DEN"); // Call Repository
//				if(null != distinctDenEnrollmentIds && !distinctDenEnrollmentIds.isEmpty()){
//					enrollmentAudDental = fetchEnrollmentAudListForMonth(householdCaseId,endDate,startDate, distinctDenEnrollmentIds.get(0), "DEN");

				/*	Date healthCoverageStartDate = DateUtils.truncate(enrollmentAudHealth.getBenefitEffectiveDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
					List<EnrollmentAud> associatedDentalAudList = new ArrayList<EnrollmentAud>();
					for(EnrollmentAud dentalEnrlAud : enrollmentAudDentalList){
						Date dentalCoverageEndDate = DateUtils.truncate(dentalEnrlAud.getBenefitEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
						if(dentalCoverageEndDate.after(healthCoverageStartDate) || dentalCoverageEndDate.equals(healthCoverageStartDate)){
							associatedDentalAudList.add(dentalEnrlAud);
						}
					}*/
//				}
			
//				EnrollmentPremium1095 enrollmentPremium1095 = getPolicyInfoForMonth(enrollmentAudHealth , enrollmentAudDental , endDate ,startDate, month);
				EnrollmentPremium1095 enrollmentPremium1095 = getPolicyInfoForMonth(enrollmentAudHealth , applicableDentalEnrollments , endDate ,startDate, month);
				enrollmentPremium1095.setEnrollment1095(enrollment1095record);
				AccountUser user = getUser();
				enrollmentPremium1095.setCreatedBy(user.getId());
				enrollmentPremium1095.setUpdatedBy(user.getId());
//				EnrollmentAnnualIrsMonthlyAmountDTO enrollmentAnnualIrsMonthlyAmountDTO  = getEnrollmentIrsPolicyAmount(enrollmentAudHealth , associatedDentalAudList , endDate ,startDate);
				premiumList.add(enrollmentPremium1095);
			}else{
				LOGGER.info("No Enrollment Record found for "+ month + " month for household Id :: "+householdCaseId);
			}
		}
		return premiumList;
	}

	private List<EnrollmentAud> getLatestDentalEnrollmentForAnnualIrs(Enrollment hltEnrollment) {
		List<EnrollmentAud> enrollmentAudList = new ArrayList<EnrollmentAud>();
		List<Integer> distinctDenEnrollmentIds = enrollmentRepository.getEnrollmentIdsForAnnualIRS(hltEnrollment.getHouseHoldCaseId(), DateUtil.dateToString(hltEnrollment.getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(hltEnrollment.getBenefitEndDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), "DEN"); // Call Repository
		for(Integer enrollmentId : distinctDenEnrollmentIds){
			EnrollmentAud enrollmentAud = enrollmentAudRepository.getMaxAuditRecordForEnrollment(enrollmentId);
			if(null != enrollmentAud){
				enrollmentAudList.add(enrollmentAud);
			}
		}
		return enrollmentAudList;
	}

	/**
	 * 
	 * @param enrollment
	 * @param enrollment1095record 
	 * @return
	 * @throws GIException
	 */
	private List<EnrollmentMember1095> getEnrollmentMembers(Enrollment enrollment, Enrollment1095 enrollment1095record) throws GIException {
		List<EnrollmentMember1095> enrollmentMembers = new ArrayList<EnrollmentMember1095>();
		List<Enrollee> enrolleeList = enrollment.getEnrolleesAndSubscriber();
		AccountUser user = getUser();
		if(null != enrolleeList && !enrolleeList.isEmpty()){
			for(Enrollee enrollee : enrolleeList){
				if (null != enrollee.getEnrolleeLkpValue() && 
						(enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM)
								|| enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)
								|| enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_PENDING))) {
				EnrollmentMember1095 member = new EnrollmentMember1095();
				member.setEnrolleeId(enrollee.getId());
				member.setFirstName(enrollee.getFirstName());
				member.setMiddleName(enrollee.getMiddleName());
				member.setLastName(enrollee.getLastName());
				member.setBirthDate(enrollee.getBirthDate());
				member.setSsn(enrollee.getTaxIdNumber());
				member.setNameSuffix(enrollee.getSuffix());
				member.setCoverageStartDate(enrollee.getEffectiveStartDate());
				member.setCoverageEndDate(enrollee.getEffectiveEndDate());
				member.setMemberId(Integer.valueOf(enrollee.getExchgIndivIdentifier())); // Convert to String
				member.setEnrollment1095(enrollment1095record);
				member.setMemberType(MemberType.MEMBER.toString());
				member.setCreatedBy(user.getId());
				member.setUpdatedBy(user.getId());
				enrollmentMembers.add(member);
			}
		}
		}
		//Get Recipient and spouse
		//Get list of ssapApplication Ids for enrollment
		Set<Long> ssapApplicationSet = null;
		List<Long> ssapApplicationIds = enrollmentAudRepository.getSsapApplicationIdForStagingJob(enrollment.getId(), enrollment.getSsapApplicationid());
		if(null != ssapApplicationIds && !ssapApplicationIds.isEmpty()){
			ssapApplicationSet = new HashSet<Long>(ssapApplicationIds);
		}
		EnrollmentAnnualIrsRecipientDTO recipientDTO = irsAnnualHouseholdService.getRecipientInformationForAnnualReport(enrollment.getSsapApplicationid(),  ssapApplicationSet);
		enrollmentMembers.add(setMemberFromRecipient(recipientDTO.getRecipient(),MemberType.RECEPIENT, enrollment1095record, user, enrollment));
		if(null != recipientDTO.getRecipientSpouse()){
			enrollmentMembers.add(setMemberFromRecipient(recipientDTO.getRecipientSpouse(),MemberType.SPOUSE, enrollment1095record, user, enrollment));	
		}
		return enrollmentMembers;
	}


	/**
	 * Set member info from ssap api
	 * @param recipient
	 * @param memberType
	 * @param enrollment1095record 
	 * @param user 
	 * @return
	 */
	private EnrollmentMember1095 setMemberFromRecipient(EnrollmentIrsCoveredIndividualDTO recipient, MemberType memberType, Enrollment1095 enrollment1095record, AccountUser user, Enrollment enrollment)throws GIException {
		EnrollmentMember1095 member = null;
		if(null != recipient){
			member = new EnrollmentMember1095();
			Enrollee enrollee = getLatestEnrollee(enrollment, recipient.getMemberId()) ;
			if(null!=enrollee){
				member.setFirstName(enrollee.getFirstName());
				member.setMiddleName(enrollee.getMiddleName());
				member.setLastName(enrollee.getLastName());
				member.setSsn(enrollee.getTaxIdNumber());
				member.setNameSuffix(enrollee.getSuffix());
				member.setBirthDate(enrollee.getBirthDate());
				member.setCoverageStartDate(enrollee.getEffectiveStartDate());
				member.setCoverageEndDate(enrollee.getEffectiveEndDate());
			}else{
				member.setFirstName(recipient.getFirstName());
				member.setMiddleName(recipient.getMiddleName());
				member.setLastName(recipient.getLastName());
				member.setSsn(recipient.getSsn());
				member.setNameSuffix(recipient.getSuffixName());
				member.setBirthDate(recipient.getDob());
				member.setCoverageStartDate(recipient.getCoverageStartDt());
				member.setCoverageEndDate(recipient.getCoverageEndDt());
			}
			
			member.setMemberId(Integer.valueOf(recipient.getMemberId())); // Convert to String
			if(MemberType.RECEPIENT.equals(memberType)){
				setAddressFromHousehold(member, enrollment1095record.getHouseHoldCaseId(), recipient);
			}
			member.setMemberType(memberType.toString());
			member.setCreatedBy(user.getId());
			member.setUpdatedBy(user.getId());
			member.setEnrollment1095(enrollment1095record);
		}
		return member;
	}

	/**
	 * Set address from household if location present
	 * @param member
	 * @param houseHoldCaseId
	 * @param recipient
	 */
	private void setAddressFromHousehold(EnrollmentMember1095 member, String houseHoldCaseId, EnrollmentIrsCoveredIndividualDTO recipient) throws GIException{
		Household household = cmrHouseholdRepository.findOne(Integer.parseInt(houseHoldCaseId));
		if(null != household && null !=  household.getPrefContactLocation()){
			Location loc =  household.getPrefContactLocation();
			member.setAddress1(loc.getAddress1());
			member.setAddress2(loc.getAddress2());
			member.setCity(loc.getCity());
			member.setZip(loc.getZip());
			member.setState(loc.getState());
		}else{
			/*member.setAddress1(recipient.getAddressLine1Txt());
			member.setAddress2(recipient.getAddressLine2Txt());
			member.setCity(recipient.getCityNm());
			member.setZip(recipient.getUszipCd());
			member.setState(recipient.getUsStateCd());*/
			//HIX-85457
			throw new GIException("Prefered Contact Location at CMR_HOUSEHOLD (LOCATION_ID) is null");
		}
	}


	private boolean compareExistingRecords(Enrollment1095 existingEnrollment1095record, Enrollment1095 enrollment1095record) {
		//  Comparison Logic
		// Can we compare objects
		//Compare flat fields of Enrollment 1095
		boolean isRecordLevelEqual = existingEnrollment1095record.compareFlatFields(enrollment1095record);
		//compare members
		boolean isMemberLevelEqual = compareMemberInformation(existingEnrollment1095record.getActiveEnrollmentMembers(), enrollment1095record.getEnrollmentMembers(), existingEnrollment1095record.getEnrollmentMembersByIsActiveFlag(EnrollmentConstants.N));
		//compare premium
		boolean isPremiumLevelEqual = comparePremiumInformation(existingEnrollment1095record.getActiveEnrollmentPremiums(), enrollment1095record.getEnrollmentPremiums(), existingEnrollment1095record.getEnrollmentPremiumsByIsActiveFlag(EnrollmentConstants.N));
		
		return isRecordLevelEqual && isMemberLevelEqual && isPremiumLevelEqual;
	}

	/**
	 * Compare premium information
	 * @param existingEnrollmentPremiums
	 * @param enrollmentPremiums
	 */
	private boolean comparePremiumInformation(List<EnrollmentPremium1095> existingEnrollmentPremiums,
			List<EnrollmentPremium1095> enrollmentPremiums, List<EnrollmentPremium1095> inActiveEnrollmentPremiumList) {
		boolean isEqual = true;
		if(null != existingEnrollmentPremiums && null != enrollmentPremiums){
			//HIX-79531 Mark months for which enrollment is inactive
			/**
			 * Comparing the size of Existing Enrollment_1095 Records and Fresh created ones.
			 */
			if(existingEnrollmentPremiums.size() != enrollmentPremiums.size()){
				isEqual = false;
			}
			List<EnrollmentPremium1095> differenceList = new ArrayList<EnrollmentPremium1095>();
			differenceList.addAll(existingEnrollmentPremiums);
			
			for(EnrollmentPremium1095 existingPremium : existingEnrollmentPremiums){
				if(!enrollmentPremiums.isEmpty()){
					List<EnrollmentPremium1095> filteredList = enrollmentPremiums.stream().filter(pre1095Obj -> pre1095Obj.getMonthNumber().equals(existingPremium.getMonthNumber())).collect(Collectors.toList());
					
					if(filteredList != null && !filteredList.isEmpty()){
						for(EnrollmentPremium1095 premium : filteredList){
							premium.setId(existingPremium.getId());
							premium.setCreatedOn(existingPremium.getCreatedOn());
							premium.setCreatedBy(existingPremium.getCreatedBy());
							if(!existingPremium.equals(premium)){
								isEqual =  false;	
							}
							differenceList.remove(existingPremium);
						}
					}
				}
			}
			//Code block added to update the Inactive Premium records to Active., Instead of creating new ones.
			if(inActiveEnrollmentPremiumList != null && !inActiveEnrollmentPremiumList.isEmpty() && !enrollmentPremiums.isEmpty()){
				for(EnrollmentPremium1095 existingPremium : inActiveEnrollmentPremiumList){
					List<EnrollmentPremium1095> filteredList = enrollmentPremiums.stream().filter(pre1095Obj -> 
					(pre1095Obj.getMonthNumber().equals(existingPremium.getMonthNumber()) &&
							(pre1095Obj.getId() == EnrollmentConstants.ZERO)
							)).collect(Collectors.toList());
					if(filteredList != null && !filteredList.isEmpty()){
						for(EnrollmentPremium1095 premium : filteredList){
							premium.setId(existingPremium.getId());
							premium.setCreatedOn(existingPremium.getCreatedOn());
							premium.setCreatedBy(existingPremium.getCreatedBy());
						}
					}
				}
			}
			
			for(EnrollmentPremium1095 inActivePremiumObj : differenceList){
				inActivePremiumObj.setIsActive(EnrollmentPremium1095.YorNFlagIndicator.N.toString());
				enrollmentPremiums.add(inActivePremiumObj);
			}
		}
		return isEqual;
	}


	/**
	 * Compare member information
	 * @param existingEnrollmentMembers
	 * @param enrollmentMembers
	 * @return
	 */
	private boolean compareMemberInformation(List<EnrollmentMember1095> existingEnrollmentMembers,
			List<EnrollmentMember1095> enrollmentMembers, final List<EnrollmentMember1095> inActiveEnrollmentMembersList) {
		boolean isEqual = true;
		List<EnrollmentMember1095> differenceList = new ArrayList<EnrollmentMember1095>();
		differenceList.addAll(existingEnrollmentMembers);
		if(null != existingEnrollmentMembers && null != enrollmentMembers){
			if(existingEnrollmentMembers.size() != enrollmentMembers.size()){
				isEqual = false;
			}
			
			for (EnrollmentMember1095 member : enrollmentMembers) {
				EnrollmentMember1095 existingMember = existingEnrollmentMembers.stream()
						.filter(mem1095 -> (mem1095.getMemberId().equals(member.getMemberId()) && mem1095.getMemberType().equals(member.getMemberType())
								&& (mem1095.getEnrolleeId() == null || mem1095.getEnrolleeId().equals(member.getEnrolleeId())))).findAny().orElse(null);
				if (existingMember != null) {
					member.setId(existingMember.getId());
					member.setCreatedOn(existingMember.getCreatedOn());
					member.setCreatedBy(existingMember.getCreatedBy());
					member.setIsActive(EnrollmentPremium1095.YorNFlagIndicator.Y.toString());
					if (!existingMember.equals(member)) {
						isEqual = false;
					}
					differenceList.remove(existingMember); // Remove matching members
				}
			}
			
			//Code block added to update the Inactive members records to Active., Instead of creating new ones.
			if(inActiveEnrollmentMembersList != null && !inActiveEnrollmentMembersList.isEmpty() && !enrollmentMembers.isEmpty()){
				for(EnrollmentMember1095 existingEnrollmentMember : inActiveEnrollmentMembersList){
					List<EnrollmentMember1095> filteredList = enrollmentMembers.stream().filter(member1095Obj -> 
					(member1095Obj.getMemberId().equals(existingEnrollmentMember.getMemberId())
							&& member1095Obj.getMemberType().equals(existingEnrollmentMember.getMemberType()))).collect(Collectors.toList());
					if(filteredList != null && !filteredList.isEmpty()){
						for(EnrollmentMember1095 member : filteredList){
							member.setId(existingEnrollmentMember.getId());
							member.setCreatedOn(existingEnrollmentMember.getCreatedOn());
							member.setCreatedBy(existingEnrollmentMember.getCreatedBy());
						}
					}
				}
			}
			
			for(EnrollmentMember1095 inActiveMemberObj : differenceList){
				inActiveMemberObj.setIsActive(EnrollmentMember1095.YorNFlagIndicator.N.toString());
				enrollmentMembers.add(inActiveMemberObj);
			}
		}
		return isEqual;
	}
	
	/**
	 * 
	 * @param enrollmentAudHealthObj
	 * @param applicableDentalEnrollments
	 * @param endDate
	 * @param startDate
	 * @param month
	 * @return
	 */
	private EnrollmentPremium1095 getPolicyInfoForMonth(EnrollmentAud enrollmentAudHealthObj,
			List<EnrollmentAud> applicableDentalEnrollments, Date endDate, Date startDate, int month) {

		EnrollmentPremium1095 enrollmentPremium1095  = new EnrollmentPremium1095();
		enrollmentPremium1095.setMonthNumber(month + 1);
		enrollmentPremium1095.setMonthName(getMonthName(month)); //get month name
		
		//******* Get Enrollee List
		List<EnrolleeAud> enrolleeAuditList = getEnrolleeAuditList(enrollmentAudHealthObj.getId(), enrollmentAudHealthObj.getRev(), endDate, startDate);
		// *************************

		/**
		 * Calculate Premium Amount here
		 */
		Float proratedGrossPremiumAmount = retroAmountCalculations(enrolleeAuditList,endDate,startDate);
		enrollmentPremium1095.setGrossPremium(EnrollmentUtils.round(proratedGrossPremiumAmount,2));
		/****************************************/
		if (null != proratedGrossPremiumAmount
				&& (null != enrollmentAudHealthObj.getEhbPercent() 
				&& enrollmentAudHealthObj.getEhbPercent().compareTo(0f) != 0)){
			enrollmentPremium1095.setEhbPercent(enrollmentAudHealthObj.getEhbPercent());
//			enrollmentAnnualIrsMonthlyAmountDTO.setMonthlyPremiumAmt(proratedGrossPremiumAmount * multiplier);
		}

		//Get effective APTC
		enrollmentPremium1095.setAptcAmount(getEffectiveAptcAmount(enrollmentAudHealthObj, startDate, endDate));
		enrollmentPremium1095.setSlcspAmount(getEffectiveSlcspPremiumAmt(enrollmentAudHealthObj, startDate, endDate));
		enrollmentPremium1095.setSsapAppId(enrollmentAudHealthObj.getSsapApplicationid());
		
		if (null != applicableDentalEnrollments && !applicableDentalEnrollments.isEmpty()) {
			EnrollmentAud latestDentalEnrollment = applicableDentalEnrollments.get(0);
			for(EnrollmentAud denEnrollment : applicableDentalEnrollments){
				if(denEnrollment.getBenefitEffectiveDate().after(latestDentalEnrollment.getBenefitEffectiveDate())){
					latestDentalEnrollment = denEnrollment;
				}
				if(denEnrollment.getBenefitEndDate().after(enrollmentAudHealthObj.getBenefitEndDate())){
					denEnrollment.setBenefitEndDate(enrollmentAudHealthObj.getBenefitEndDate());
				}
			}
			Float effectiveDentalAptcAmount = getEffectiveAptcAmount(latestDentalEnrollment, startDate, endDate);
			if(null != enrollmentPremium1095.getAptcAmount() && null != effectiveDentalAptcAmount){
				enrollmentPremium1095.setAptcAmount(EnrollmentUtils.round((enrollmentPremium1095.getAptcAmount() + effectiveDentalAptcAmount) ,2));				
			}else if(null != effectiveDentalAptcAmount){
				enrollmentPremium1095.setAptcAmount(effectiveDentalAptcAmount);	
			}
			enrollmentPremium1095.setPediatricEhbAmt(latestDentalEnrollment.getDntlEssentialHealthBenefitPrmDollarVal());
			//count accounted enrollees
			enrollmentPremium1095.setAccountableMemberDental(enrolleeRepository.getAccountableEnrolleeCount(latestDentalEnrollment.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY)).intValue());
		}
		return  enrollmentPremium1095;
	}
	
	/**
	 * Get Pro-rated dental premium amount (Deprecated)
	 * @param latestDentalEnrollment
	 * @param enrollmentAudHealthObj
	 * @param endDate
	 * @param startDate
	 * @return
	 */
	@SuppressWarnings("unused")
	private Float getEffectivePediatricEhbAmt(EnrollmentAud latestDentalEnrollment, EnrollmentAud enrollmentAudHealthObj, Date endDate, Date startDate) {
		Float effectivePediatricEhbAmt = 0f;
		Date truncatedStartDate =  DateUtils.truncate(startDate , Calendar.DAY_OF_MONTH); //Month Start Date
		Date truncatedEndDate =  DateUtils.truncate(endDate , Calendar.DAY_OF_MONTH); //Month End Date
		Date coverageStartDate = DateUtils.truncate(latestDentalEnrollment.getBenefitEffectiveDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
		Date coverageEndDate = DateUtils.truncate(latestDentalEnrollment.getBenefitEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
		Float pediatricEhbAmt = latestDentalEnrollment.getDntlEssentialHealthBenefitPrmDollarVal();
		if(coverageStartDate != null && coverageEndDate !=null ){
			boolean isCoverageAfterMonthStart = coverageStartDate.after(truncatedStartDate);
			//				boolean isCoverageBeforeMonthEnd = coverageStartDate.before(endDate);
			boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(truncatedEndDate);
			//				boolean isCoverageTermAfterMonthStart = coverageEndDate.after(startDate);
			int daysElapsed = EnrollmentUtils.daysInMonth(truncatedStartDate) ;

			if(isCoverageAfterMonthStart && isCoverageTermBeforeMonthEnd){
				// Calculate number of days between coverage start date and coverage end date
				daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, coverageEndDate) + 1;

			}else if(isCoverageAfterMonthStart){
				// Calculate number of days between coverage start date and month end date
				daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, truncatedEndDate);
			}else if(isCoverageTermBeforeMonthEnd){
				// Calculate number of days between month start date and coverage term date
				daysElapsed = EnrollmentUtils.getDaysBetween(truncatedStartDate, coverageEndDate) + 1;
			}
			if(null != pediatricEhbAmt){
				effectivePediatricEhbAmt = pediatricEhbAmt / EnrollmentUtils.daysInMonth(truncatedStartDate) * daysElapsed;
			}
		}
		return effectivePediatricEhbAmt;
	}


	/**
	 * Set Policy Information from enrollment
	 * @param enrollmentAnnualIrsPolicyDTO
	 * @param hltEnrollment
	 */
	private void setPolicyInformation(Enrollment1095 enrollment1095, Enrollment hltEnrollment) {
		//enrollment1095.setPolicyIssuerName(hltEnrollment.getInsurerName());
		enrollment1095.setPolicyStartDate(hltEnrollment.getBenefitEffectiveDate());
		enrollment1095.setPolicyEndDate(hltEnrollment.getBenefitEndDate());
		enrollment1095.setCmsPlanId(hltEnrollment.getCMSPlanID());
		//enrollment1095.setIssuerEin(EnrollmentUtils.removeSpecialCharacters(enrollmenteExternalRestUtil.getIssuerInfoById(hltEnrollment.getIssuerId()).getFederalEin()));
		SingleIssuerResponse singleIssuerResponse=enrollmenteExternalRestUtil.getIssuerInfoById(hltEnrollment.getIssuerId());
		if(singleIssuerResponse!=null){
			enrollment1095.setIssuerEin(EnrollmentUtils.removeSpecialCharacters(singleIssuerResponse.getFederalEin()));
			enrollment1095.setPolicyIssuerName(singleIssuerResponse.getCompanyLegalName());
		}
	}

	/**
	 * Fetch the applicable enrollments for the given month
	 * @author negi_s
	 * @param householdId
	 * @param endDate
	 * @param startDate
	 * @param enrollment1095Ids
	 * @param insuranceType
	 * @return List<EnrollmentAud> enrollmentAudList
	 */
	private EnrollmentAud fetchEnrollmentAudListForMonth(String householdId, Date endDate, Date startDate, Enrollment enrollment, String insuranceType) {

		EnrollmentAud enrollmentAud = enrollmentAudRepository.getMaxEnrollmentForId(householdId, enrollment.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), insuranceType); //Call repository

		//Handle special case when enrollment creation is of later month than coverage start date
		if(enrollmentAud == null){
			enrollmentAud = enrollmentAudRepository.getMinEnrollmentAudForHouseHold(householdId, enrollment.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), insuranceType);
		}

		if(enrollmentAud !=null){
			updateEffectiveDates(enrollmentAud, enrollment);
			if (enrollmentAud.getBenefitEndDate() != null
					&& (enrollmentAud.getBenefitEndDate().before(startDate))) {
				LOGGER.info("Enrollment terminated for the month for household Id :: " + householdId);
				return null;
			}
		}
		return enrollmentAud; 
	}
	
	/**
	 * Update effective dates from main enrollment table
	 * @param enrollmentAud
	 * @param enrollment
	 */
	private void updateEffectiveDates(EnrollmentAud enrollmentAud, Enrollment enrollment) {
		// Update start date, end date, aptc effective date, slcsp effective date
		enrollmentAud.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
		enrollmentAud.setBenefitEndDate(enrollment.getBenefitEndDate());
		enrollmentAud.setAptcEffDate(enrollment.getAptcEffDate());
		enrollmentAud.setAptcAmt(enrollment.getAptcAmt());
		enrollmentAud.setSlcspEffDate(enrollment.getSlcspEffDate());
		enrollmentAud.setSlcspAmt(enrollment.getSlcspAmt());
	}


	/**
	 * Fetches the eligible enrollee audit records for the given month
	 * @author negi_s
	 * @param enrollmentId
	 * @param rev
	 * @param endDate
	 * @return Enrollee Audit List
	 */
	private List<EnrolleeAud> getEnrolleeAuditList(Integer enrollmentId,
			int rev, Date endDate , Date startDate) {

		/**
		 * Get list of all enrollees from enrollee table in conf and term state and effective start date < end date
		 * and benefit end date > start Date 
		 */
		List<Enrollee> currentListEnrolleeTable = enrolleeRepository.getConfirmedAndTermEnrolleesForIRS(enrollmentId, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY)); 
		//		List<EnrolleeAud> finalEnrolleeAuditList = new ArrayList<EnrolleeAud>();

		List<EnrolleeAud> enrolleeAuditList = enrolleeAudRepository.getEnrolleesByEnrollmentIdAndRevisionNo(enrollmentId, rev, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY)); //add check enrollee status
		List<Integer> enrolleeIDfromAud = new ArrayList<Integer>();

		for(EnrolleeAud enrolleeAud : enrolleeAuditList){
			enrolleeIDfromAud.add(enrolleeAud.getId());
		}
		List<Integer> remainingEnrolleeIdList = null;
		if(null != enrolleeIDfromAud && !enrolleeIDfromAud.isEmpty()){
			remainingEnrolleeIdList = enrolleeRepository.getRemainingEnrolleesForIRS(enrollmentId, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), enrolleeIDfromAud, DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}else if(null != currentListEnrolleeTable && !currentListEnrolleeTable.isEmpty()){
			remainingEnrolleeIdList = new ArrayList<Integer>();
			for(Enrollee enrollee : currentListEnrolleeTable){
				remainingEnrolleeIdList.add(enrollee.getId());
			}
		}
		if(null != remainingEnrolleeIdList && !remainingEnrolleeIdList.isEmpty()){
			EnrolleeAud enrolleeAudRecord = null;
			for(Integer enrolleeId : remainingEnrolleeIdList){
				enrolleeAudRecord= enrolleeAudRepository.getMaxEnrolleeAudPerMonth(enrolleeId, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
				if(null == enrolleeAudRecord){
					enrolleeAudRecord= enrolleeAudRepository.getMinEnrolleeAud(enrolleeId);	 
				}
				if(null != enrolleeAudRecord){
					enrolleeAuditList.add(enrolleeAudRecord);
				}
			}
		}
		List<EnrolleeAud> finalEnrolleeAudList = new ArrayList<EnrolleeAud>();
		for(Enrollee enrollee : currentListEnrolleeTable){
			for(EnrolleeAud enrolleeAud : enrolleeAuditList){
				if(enrolleeAud.getId() == enrollee.getId()){
			/*		Date enrolleeEndDate = DateUtils.truncate(enrollee.getEffectiveEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
					Date auditEnrolleeEndDate = DateUtils.truncate(enrolleeAud.getEffectiveEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
					enrolleeAud.setHealthCoveragePolicyNo(enrollee.getHealthCoveragePolicyNo());
					if(!enrolleeEndDate.equals(auditEnrolleeEndDate) && (enrolleeEndDate.before(endDate))){
					enrolleeAud.setEffectiveEndDate(enrollee.getEffectiveEndDate());
					}*/
					enrolleeAud.setEffectiveEndDate(enrollee.getEffectiveEndDate());
					enrolleeAud.setEffectiveStartDate(enrollee.getEffectiveStartDate());
					finalEnrolleeAudList.add(enrolleeAud);
					break;
				}
			}
		}

		return finalEnrolleeAudList;
	}

	/**
	 * Retro Calculations
	 * @author negi_s
	 * @param enrolleeAuditList
	 * @param enrollmentIrsPolicyInfoDTO
	 * @param endDate
	 * @param startDate
	 */
	private Float retroAmountCalculations(List<EnrolleeAud> enrolleeAuditList, Date endDate, Date startDate) {
		Float totalPremiumAmount = 0f;
		Date truncatedStartDate =  DateUtils.truncate(startDate , Calendar.DAY_OF_MONTH); //Month Start Date
		Date truncatedEndDate =  DateUtils.truncate(endDate , Calendar.DAY_OF_MONTH); //Month End Date
		for(EnrolleeAud enrollee : enrolleeAuditList ){
			Date coverageStartDate = DateUtils.truncate(enrollee.getEffectiveStartDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
			Date coverageEndDate = DateUtils.truncate(enrollee.getEffectiveEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
			Float totalIndivRespAmt = enrollee.getTotalIndvResponsibilityAmt();
			Float retroPremium = 0f;
			if(coverageStartDate != null && coverageEndDate !=null ){
				boolean isCoverageAfterMonthStart = coverageStartDate.after(truncatedStartDate);
				//				boolean isCoverageBeforeMonthEnd = coverageStartDate.before(endDate);
				boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(truncatedEndDate);
				//				boolean isCoverageTermAfterMonthStart = coverageEndDate.after(startDate);
				int daysElapsed = EnrollmentUtils.daysInMonth(truncatedStartDate) ;

				if(isCoverageAfterMonthStart && isCoverageTermBeforeMonthEnd){
					// Calculate number of days between coverage start date and coverage end date
					daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, coverageEndDate) + 1;

				}else if(isCoverageAfterMonthStart){
					// Calculate number of days between coverage start date and month end date
					daysElapsed = EnrollmentUtils.getDaysBetween(coverageStartDate, truncatedEndDate);
				}else if(isCoverageTermBeforeMonthEnd){
					// Calculate number of days between month start date and coverage term date
					daysElapsed = EnrollmentUtils.getDaysBetween(truncatedStartDate, coverageEndDate) + 1;
				}
				retroPremium = totalIndivRespAmt / EnrollmentUtils.daysInMonth(truncatedStartDate) * daysElapsed;
			}
			totalPremiumAmount += retroPremium; 
		}
		return totalPremiumAmount;
	}
	
	/**
	 * Get Effective Aptc Amount
	 * @author negi_s
	 * @param enrollmentAudHealthObj
	 * @param startDate
	 * @param endDate 
	 * @param enrollmentIrsPolicyInfoDTO 
	 */
	private Float getEffectiveAptcAmount(EnrollmentAud enrollmentAudHealthObj, Date startDate, Date endDate) {
		Float aptcAmount =  enrollmentAudHealthObj.getAptcAmt();
		if(null != enrollmentAudHealthObj.getAptcEffDate()){
			Date aptcEffDate = DateUtils.truncate(enrollmentAudHealthObj.getAptcEffDate() , Calendar.DAY_OF_MONTH);
			Date truncateEndDate = DateUtils.truncate(endDate , Calendar.DAY_OF_MONTH);
			if(aptcEffDate.after(truncateEndDate) || aptcEffDate.equals(truncateEndDate)){
				aptcAmount = enrollmentAudRepository.getLatestAptcBeforeDate(enrollmentAudHealthObj.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));	
			}
		}
		return aptcAmount;
	}

	/**
	 * Get Effective Prorated Aptc Amount (Deprecated)
	 * @author negi_s
	 * @param enrollmentAud
	 * @param startDate
	 * @param endDate 
	 * @param enrollmentIrsPolicyInfoDTO 
	 */
	@SuppressWarnings("unused")
	private Float getEffectiveProratedAptcAmount(EnrollmentAud enrollmentAud, Date startDate, Date endDate) {
		List<EnrollmentAud> enrAudList = new ArrayList<EnrollmentAud>();
		if((enrollmentAud.getAptcEffDate()!=  null && enrollmentAud.getAptcEffDate().after(startDate)) || 
				(enrollmentAud.getAptcEffDate()!=  null && enrollmentAud.getUpdatedOn()!=  null && enrollmentAud.getUpdatedOn().after(endDate))){
			enrAudList = getEnrollmentAudListForAptcCalc(enrollmentAud.getId(), startDate,  endDate);
		}
		return retroAptcCalculations(enrAudList, startDate,endDate,enrollmentAud);
	}
	
	/**
	 * Get list of all enrollment audits with APTC effective date less than month end
	 * @author negi_s
	 * @param enrollmentID
	 * @param startDate
	 * @param endDate
	 * @return List<EnrollmentAud>
	 */
	private List<EnrollmentAud> getEnrollmentAudListForAptcCalc(Integer enrollmentID, Date startDate, Date endDate) {

		List<Date> distinctAptcEffDates = enrollmentAudRepository.getDistinctAptcEffDateForMonth(enrollmentID, DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		List<EnrollmentAud> enrollmentAudList = new ArrayList<EnrollmentAud>();
		if(null != distinctAptcEffDates){
			for(Date aptcEffDate : distinctAptcEffDates){
				EnrollmentAud enrollmentAud = enrollmentAudRepository.getMaxAuditForAptcEffDate(enrollmentID, DateUtil.dateToString(aptcEffDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
				if(null != enrollmentAud){
					enrollmentAudList.add(enrollmentAud);
				}
			}
		}
		return enrollmentAudList;
	}
	
	/**
	 * Pro-rate APTC amounts from all applicable audit records
	 * @author negi_s
	 * @param enrAudList
	 * @param startDate
	 * @param endDate
	 * @param latestAud
	 * @param isNewEnrollment
	 * @return Float Pro-rated APTC amount
	 */

	private Float retroAptcCalculations(List<EnrollmentAud> enrAudList, Date startDate, Date endDate, EnrollmentAud latestAud) {
		
		Float totalAptc = 0f;
		Date truncatedStartDate =  DateUtils.truncate(startDate , Calendar.DAY_OF_MONTH); //Month Start Date
		Date truncatedEndDate =  DateUtils.truncate(endDate , Calendar.DAY_OF_MONTH); //Month End Date
		
		if(null != enrAudList && !enrAudList.isEmpty()){
			for(int i=0 ; i<enrAudList.size()-1;i++){
				EnrollmentAud currentAud = enrAudList.get(i);
				EnrollmentAud nextAud = enrAudList.get(i+1);
				if(null != currentAud.getAptcEffDate() && null != nextAud.getAptcEffDate() ){
					Date currentAptcEffDate = DateUtils.truncate(currentAud.getAptcEffDate() , Calendar.DAY_OF_MONTH); 
					Date nextAptcEffDate = DateUtils.truncate(nextAud.getAptcEffDate() , Calendar.DAY_OF_MONTH);
					int daysElapsed = EnrollmentUtils.daysInMonth(truncatedStartDate);
					Float aptcAmt = (currentAud.getAptcAmt()!=null)? currentAud.getAptcAmt() : 0f;
					Float retroAptc = 0f;
					if( currentAptcEffDate.before(truncatedStartDate) && nextAptcEffDate.before(truncatedStartDate)){
						continue;
					}else if(currentAptcEffDate.before(truncatedStartDate)){
						daysElapsed = EnrollmentUtils.getDaysBetween(truncatedStartDate, nextAptcEffDate);
					}else{
						daysElapsed = EnrollmentUtils.getDaysBetween(currentAptcEffDate, nextAptcEffDate);
					}
					retroAptc = aptcAmt / EnrollmentUtils.daysInMonth(truncatedStartDate) * daysElapsed;
					totalAptc += retroAptc;
				}
			}
		}
		
		EnrollmentAud finalAuditRecord = latestAud;
		
		if(null != enrAudList && !enrAudList.isEmpty()){
			finalAuditRecord = enrAudList.get(enrAudList.size()-1);
			if(enrAudList.size() == 1){
				Date currentAptcEffDate = null;
				Date aptcEffDateListRecord = null;
				EnrollmentAud singleAuditRecord = enrAudList.get(0);
				if(latestAud.getAptcEffDate() !=  null && singleAuditRecord.getAptcEffDate() !=null ){
					currentAptcEffDate = DateUtils.truncate(latestAud.getAptcEffDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
					aptcEffDateListRecord = DateUtils.truncate(singleAuditRecord.getAptcEffDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
					 if(aptcEffDateListRecord.before(currentAptcEffDate)){
						 finalAuditRecord = singleAuditRecord;
					 }
				}
			}
		}
		Date coverageEndDate = DateUtils.truncate(finalAuditRecord.getBenefitEndDate(), Calendar.DAY_OF_MONTH); // To remove timestamp
		Date latestAptcEffDate = null;
		if(coverageEndDate !=null ){
//			boolean isCoverageAfterMonthStart = coverageStartDate.after(truncatedStartDate);
//			boolean isCoverageBeforeMonthEnd = coverageStartDate.before(endDate);
			boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(truncatedEndDate);
			if(finalAuditRecord.getAptcEffDate() !=  null){
				latestAptcEffDate = DateUtils.truncate(finalAuditRecord.getAptcEffDate(), Calendar.DAY_OF_MONTH); // To remove timestamp	
			}
			boolean isAptcEffDateAfterCoverageMonthStart = false;
			if(latestAptcEffDate != null){
				isAptcEffDateAfterCoverageMonthStart = latestAptcEffDate.after(truncatedStartDate);	
			}
			Float retroAptc = 0f;
			
//			boolean isCoverageTermAfterMonthStart = coverageEndDate.after(startDate);
			int daysElapsed = EnrollmentUtils.daysInMonth(truncatedStartDate) ;
			
			if( isCoverageTermBeforeMonthEnd && isAptcEffDateAfterCoverageMonthStart){
				// Calculate number of days between aptc eff date and coverage end date
				daysElapsed = EnrollmentUtils.getDaysBetween(latestAptcEffDate, coverageEndDate) + 1;
			
			}else if(isAptcEffDateAfterCoverageMonthStart){
				// Calculate number of days aptc eff date and coverage end date
				daysElapsed = EnrollmentUtils.getDaysBetween(latestAptcEffDate, truncatedEndDate);
				
			}else if(isCoverageTermBeforeMonthEnd){
				// Calculate number of days between month start and coverage end date
				daysElapsed = EnrollmentUtils.getDaysBetween(truncatedStartDate, coverageEndDate) + 1;
			}
			if(null != finalAuditRecord.getAptcAmt()){
				retroAptc = finalAuditRecord.getAptcAmt() / EnrollmentUtils.daysInMonth(truncatedStartDate) * daysElapsed;	
			}
			totalAptc += retroAptc;
		}
		return totalAptc;	
	}

	/**
	 * Returns the effective SLCSPAdjMonthlyPremiumAmt for the month
	 * 
	 * @author negi_s
	 * @param enrollmentAudHealthObj
	 * @param startDate month start date
	 * @param endDate month end date
	 * @param isFinancialFlow
	 * @return Float the SLCSPAdjMonthlyPremiumAmt for the month
	 * @throws GIException 
	 */
	private Float getEffectiveSlcspPremiumAmt(EnrollmentAud enrollmentAudHealthObj, Date startDate, Date endDate) {
		Float effSlcspAdjMonthlyPremiumAmt = enrollmentAudHealthObj.getSlcspAmt();
		if(null != enrollmentAudHealthObj.getSlcspEffDate()){
			Date slcspEffDate = DateUtils.truncate(enrollmentAudHealthObj.getSlcspEffDate() , Calendar.DAY_OF_MONTH);
			Date truncateEndDate = DateUtils.truncate(endDate , Calendar.DAY_OF_MONTH);
			//Find the max aud for which slcspEffDate < endDate
			if(slcspEffDate.after(truncateEndDate) || slcspEffDate.equals(truncateEndDate)){
				effSlcspAdjMonthlyPremiumAmt = enrollmentAudRepository.getLatestSlcspAmtBeforeDate(enrollmentAudHealthObj.getId(), DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
		}
		return effSlcspAdjMonthlyPremiumAmt;
	}
	
	/**
	 * Returns the month of the passed date
	 * @param benefitEffectiveDate
	 * @param applicableYear 
	 * @return Integer month of the date
	 */
	private int getMonthFromDate(Date benefitEffectiveDate, int applicableYear) {
		int month = 0;
		if(null != benefitEffectiveDate){
			Calendar cal = Calendar.getInstance();
			cal.setTime(benefitEffectiveDate);
			if(cal.get(Calendar.YEAR) > applicableYear){
				month = 11;
			}else{
				month = cal.get(Calendar.MONTH);				
			}
		}
		return month;
	}


	@Override
	public List<Integer> getFreshEnrollment1095IdsFromStaging(int applicableYear) {
		return  enrollment1095Repository.getFreshEnrollment1095Id(applicableYear);
	}

	@Override
	public boolean isFreshRunForYear(int applicableYear) {
		Long count = enrollment1095Repository.getEnrollmentCountForYear(applicableYear);
		if(count == 0){
			return true;
		}
		return false;
	}
	
	@Override
	public List<Integer> getEnrollmentIdsUpdatedAfterDate(Date lastJobRunDate, int applicableYear){
		Date nextYearStartDate = EnrollmentUtils.getStartDateForYear(applicableYear+1);
		Date currentYearStartDate = EnrollmentUtils.getStartDateForYear(applicableYear);
		List<Integer> enrollmentIds = enrollmentRepository.getEnrollmentsForAnnualIrsReport( DateUtil.dateToString(currentYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),
				 DateUtil.dateToString(nextYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY),  lastJobRunDate);
		List<Integer> dentalUpdateList = enrollmentRepository.getEnrollmentsAfterDentalUpdate(Integer.toString(applicableYear));
		Set<Integer> enrollmentIdSet = new HashSet<>(enrollmentIds);
		if(null != dentalUpdateList && !dentalUpdateList.isEmpty()){
			enrollmentIdSet.addAll(dentalUpdateList);
		}
		return new ArrayList<Integer>(enrollmentIdSet);
	}


	@Override
	public void saveToEnrollmentStagingTable(Enrollment1095 enrollment1095) throws GIException {
		try{
			enrollment1095Repository.save(enrollment1095);	
		}catch(Exception e){
			LOGGER.debug("EnrollmentAnnualIrsReportServiceImpl :: Error while saving records");
			throw new GIException("Error while saving records" , e);
		}
	}
	
	private String getMonthName(Integer month) {
		Month monthName = null;
		switch(month){
		case 0 :
			monthName = Month.JANUARY;
			break;
		case 1 :
			monthName = Month.FEBRUARY;
			break;
		case 2 :
			monthName = Month.MARCH;
			break;
		case 3 :
			monthName = Month.APRIL;
			break;
		case 4 :
			monthName = Month.MAY;
			break;
		case 5 :
			monthName = Month.JUNE;
			break;
		case 6 :
			monthName = Month.JULY;
			break;
		case 7 :
			monthName = Month.AUGUST;
			break;
		case 8 :
			monthName = Month.SEPTEMBER;
			break;
		case 9 :
			monthName = Month.OCTOBER;
			break;
		case 10 :
			monthName = Month.NOVEMBER;
			break;
		case 11 :
			monthName = Month.DECEMBER;
			break;
		default :
			monthName = null;
			break;
		}
		if(null !=monthName){
			return monthName.toString();	
		}
		return null;
	}
	

	/**
	 * Get logged in user
	 * @return AccountUser user
	 */
	private AccountUser getUser() {
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if(null == user){
				user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			}
		} catch (InvalidUserException e) {
			LOGGER.debug("Error fetching logged in user", e);
		}
		return user;
	}


	@Override
	public void sendEnrollment1095sForProcessing(List<Integer> enrollment1095List, int applicableYear, int partition) throws GIException {
		ExecutorService executor = null;
		Enrollment1095XmlWrapper enrollment1095XmlWrapper = enrollment1095XmlParams.getWrapperFromId(partition);
		Boolean isFreshPartition = 	enrollment1095XmlWrapper.isFreshRun();
		Map<Integer, String> recordSequenceIdMap = Collections.synchronizedMap(new HashMap<Integer,String>());
		String batchId = enrollment1095XmlWrapper.getBatchId();
		String batchCategoryCode = null;
		if(!enrollment1095XmlWrapper.isVoidIndicator()){
			if(isFreshPartition || null == enrollment1095XmlWrapper.getBatchCategoryCode()) {
				batchCategoryCode = EnrollmentConstants.IRS_ANNUAL_BATCHCATEGORYCODE_INITIAL;
			}else{
				batchCategoryCode = enrollment1095XmlWrapper.getBatchCategoryCode();
			}
//			}else if(EnrollmentConstants.IRS_ANNUAL_BATCHCATEGORYCODE_INITIAL.equalsIgnoreCase(enrollment1095XmlWrapper.getBatchCategoryCode())){
//				batchCategoryCode = EnrollmentConstants.IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ;
//			}else{
//				batchCategoryCode = EnrollmentConstants.IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ;
//			}
		}else{
			//Void Partition
			batchCategoryCode = BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name();
			if(BatchCategoryCodeType.IRS_EOY_SUBMIT_VOID_RECORDS_REQ.name().equalsIgnoreCase( enrollment1095XmlWrapper.getBatchCategoryCode())){
				batchCategoryCode = BatchCategoryCodeType.IRS_EOY_RESUBMIT_VOID_RECORDS_REQ.name();
			}
		}
		
		//Splitting logic goes here
		int numberOfPartitions = 0;
		if(enrollment1095List!=null && !enrollment1095List.isEmpty()){
			String strThreadPoolSize =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_1095_XML_THREAD_POOL_SIZE);
			if(strThreadPoolSize != null && NumberUtils.isNumber(strThreadPoolSize)){
				executor=Executors.newFixedThreadPool(Integer.valueOf(strThreadPoolSize));
			}else{
				executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}
			String wipFolder = "WIP_"+ partition;
			int maxEnrollmentsPerPartitions  = 2000;
			String maxEnrollmentCount = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_1095_XML_ENROLLMENT_COUNT); // Get from gi_app_config
			if(maxEnrollmentCount !=null && !maxEnrollmentCount.isEmpty()){
				maxEnrollmentsPerPartitions=Integer.valueOf(maxEnrollmentCount.trim());
			}
			int size = enrollment1095List.size();
			numberOfPartitions = size / maxEnrollmentsPerPartitions;
			if (size % maxEnrollmentsPerPartitions != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				firstIndex = i * maxEnrollmentsPerPartitions;
				lastIndex = (i + 1) * maxEnrollmentsPerPartitions;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<Integer> subList = enrollment1095List.subList(firstIndex, lastIndex);
				try{
					executor.submit(new Enrollment1095XmlJobThread(enrollment1095XmlBatchService, subList, i,
							wipFolder, recordSequenceIdMap, batchCategoryCode, enrollment1095XmlWrapper));
				}catch(Exception e){
					LOGGER.error("sendEnrollment1095sForProcessing SERVICE :The batch did not execute succesfully for the sublist :: " + subList , e);
				}
			}
			executor.shutdown();
			try {
				String strThreadTimeOut = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_1095_XML_THREAD_WAIT);
				if(strThreadTimeOut != null && NumberUtils.isNumber(strThreadTimeOut)){
					executor.awaitTermination(Integer.parseInt(strThreadTimeOut), TimeUnit.HOURS);
				}else{
					executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);
				}
			} catch (InterruptedException e) {
				LOGGER.error("Error :"+e.getMessage());
				throw new GIException("Thread Interrupted Exception :"+e.getMessage(),e);
			}
			// Add packaging logic, get batch category code according to type of submission
			String xmlFileLocation = EnrollmentUtils
					.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
							EnrollmentConstants.TRANSFER_DIRECTION_OUT)
					.append(File.separator).append(EnrollmentConstants.WIP_FOLDER_NAME)
					.append(File.separator).append(wipFolder).toString();
			boolean isGeneratedXMLsValid = enrollmentAnnualIrsBatchService.validateGeneratedXMLs(xmlFileLocation, enrollment1095XmlWrapper.getCoverageYear());
			enrollmentAnnualIrsBatchService.generateManifestXML(enrollment1095XmlWrapper.getCoverageYear(),
					batchCategoryCode, xmlFileLocation, isFreshPartition, enrollment1095XmlWrapper.getOriginalBatchId(),
					batchId, wipFolder, enrollment1095XmlWrapper.getFileCreationTimeStamp(), false, isGeneratedXMLsValid);
			//Send Email
			sendXmlJobCompletionEmail(enrollment1095XmlWrapper, numberOfPartitions);
		}
	}
	
	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param numberOfPartitions 
	 */
	private void sendXmlJobCompletionEmail(Enrollment1095XmlWrapper enrollment1095XmlWrapper, int numberOfPartitions) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_XML_JOB);
		if(enrollment1095XmlWrapper.isFreshRun()){
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.INTIAL_COMPLETED_SUBMISSION_COUNT, enrollment1095XmlWrapper.getRecordCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.INTIAL_FAILED_SUBMISSION_COUNT, enrollment1095XmlWrapper.getSkippedCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.RESUBMIT_COMPLETED_SUBMISSION_COUNT, "0");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.RESUBMIT_FAILED_SUBMISSION_COUNT, "0");
		}else{
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.INTIAL_COMPLETED_SUBMISSION_COUNT, "0" );
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.INTIAL_FAILED_SUBMISSION_COUNT, "0" );
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.RESUBMIT_COMPLETED_SUBMISSION_COUNT, enrollment1095XmlWrapper.getRecordCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.RESUBMIT_FAILED_SUBMISSION_COUNT, enrollment1095XmlWrapper.getSkippedCount()+"");
		}
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_CMS_XML_BatchEmail.NUMBER_OF_FILES, numberOfPartitions+"");
		try {
			enrollment1095NotificationService.sendCMSXMLGenerationEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending staging email notification :: ", e);
		}
	}

	@Override
	public List<Integer> getCoverageYearListForFreshRun() {
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		return enrollment1095Repository.getCoverageYearListForFreshRun(currentYear);
	}
	
	@Override
	public List<String> getOriginalBatchIdListFromStaging() {
		return enrollment1095Repository.getOriginalBatchIdListFromStagingCorrections();
	}


	@Override
	public List<Integer> getCorrectionEnrollment1095ByBatchIdAndCategoryCode(String batchId, String batchCategoryCode) {
		return enrollment1095Repository.getCorrectionEnrollment1095ByBatchIdAndCategoryCode(batchId, batchCategoryCode);
	}


	@Override
	public Integer getCoverageYearOfOriginalBatchId(String batchId) {
		return enrollment1095Repository.getCoverageYearOfOriginalBatchId(batchId);
	}
	
	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}

	@Override
	public List<Integer> getInitialVoidEnrollment1095ByBatchId(String batchId) {
		return enrollment1095Repository.getInitialVoidEnrollment1095ByBatchId(batchId);
	}

	@Override
	public List<Integer> getVoidEnrollment1095ByBatchIdAndCategoryCode(String batchId, List<String> batchCategoryCodeList) {
		return enrollment1095Repository.getVoidEnrollment1095ByBatchIdAndCategoryCode(batchId, batchCategoryCodeList);
	}


	@Override
	public boolean setVoidIndicatorForCancelledEnrollment() {
		boolean status = true;
		//Fetch cancelled enrollments present in the staging table
		List<Enrollment1095> enrollment1095List = enrollment1095Repository.fetchCancelledEnrollment1095();
		if(null != enrollment1095List && !enrollment1095List.isEmpty()){
			for(Enrollment1095 enrollment1095 : enrollment1095List){
				boolean isXMLgenerated = null != enrollment1095.getCmsXmlFileName() && null != enrollment1095.getCmsXmlGeneratedOn();
				boolean isPDFgenerated = null != enrollment1095.getNotice() && null != enrollment1095.getPdfGeneratedOn();
				if(isXMLgenerated || isPDFgenerated){
					enrollment1095.setVoidCheckboxindicator(Enrollment1095.YorNFlagIndicator.Y.toString());
					enrollment1095.setCorrectionSource(CorrectionSource.DB_REFRESH.toString());
					enrollment1095.setUpdateNotes("Setting void indicator as enrollment was cancelled");
					enrollment1095.setCorrectedCheckBoxIndicator(null);
					if(isXMLgenerated){
						enrollment1095.setCorrectionIndicatorXml(YorNFlagIndicator.Y.toString());
					}
					if(isPDFgenerated){
						enrollment1095.setCorrectionIndicatorPdf(YorNFlagIndicator.Y.toString());
					}
				}else{
//					enrollment1095.setIsActive(Enrollment1095.YorNFlagIndicator.N.toString());
					enrollment1095.setIsObsolete(Enrollment1095.YorNFlagIndicator.Y.toString());
					enrollment1095.setCorrectionSource(CorrectionSource.DB_REFRESH.toString());
					enrollment1095.setUpdateNotes("Setting isObsolete flag as Y as enrollment was cancelled");
				}
				try{
					enrollment1095Repository.save(enrollment1095);
				}catch(Exception e){
					LOGGER.error("Error setVoidIndicatorForCancelledEnrollment() for enrollment Id :: " + enrollment1095.getExchgAsignedPolicyId());
					status = false;
				}
			/*	if(!isXMLgenerated && !isPDFgenerated){
					try{
						enrollment1095Repository.deletePremiumAudit(enrollment1095.getId());
						enrollment1095Repository.deletePremium(enrollment1095.getId());
						enrollment1095Repository.deleteMemberAudit(enrollment1095.getId());
						enrollment1095Repository.deleteMember(enrollment1095.getId());
						iEnrollment1095ValidationReportRepository.deleteValidationReport(enrollment1095.getId());
						enrollment1095Repository.deleteEnrollment1095Audit(enrollment1095.getId());
						enrollment1095Repository.deleteEnrollment1095(enrollment1095.getId());
					}catch(Exception e){
						LOGGER.error("Error deleting cancelled records for enrollment Id :: " + enrollment1095.getExchgAsignedPolicyId(), e);
					}
				}*/
			}
		}
		return status;
	}


	@Override
	public List<Integer> getCorrectionListFromStaging(String batchId, Integer coverageYear, List<String> batchCategoryCodeList, Boolean resubmitFlag) {
		List<Integer> correctionList = null;
		if(!resubmitFlag){
			correctionList = enrollment1095Repository.getSubmitCorrectionListFromStaging(batchId, batchCategoryCodeList);
		}else{
			correctionList = enrollment1095Repository.getReSubmitCorrectionListFromStaging(batchId, batchCategoryCodeList);
		}
		return correctionList;
	}


	@Override
	public List<Integer> getRegenerateEnrollment1095ByBatchIdAndCategoryCode(String batchId, String batchCategoryCode) {
		return enrollment1095Repository.getRegenerateEnrollment1095ByBatchIdAndCategoryCode(batchId, batchCategoryCode);
	}
	
	public Enrollee getLatestEnrollee(Enrollment enrollment, String memberId){
		Enrollee latest = null;
		if(null!=enrollment){
			List<Enrollee> enrollees= enrollment.getEnrollees();
			if(enrollees != null && enrollees.size() > 0){
				for(Enrollee enrollee : enrollees){
					if(enrollee.getPersonTypeLkp().getLookupValueCode() != null && enrollee.getExchgIndivIdentifier().equalsIgnoreCase(memberId) && (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) ||  enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE))){
						if(latest==null || latest.getUpdatedOn().before(enrollee.getUpdatedOn())){
							latest = enrollee;
						}
					}
				}
			}
		}
		return latest;
	}
	
	@Override
	public void validateEnrollment1095Records(){
			//TODO populate Coverage Year List
			List<Integer> coverageYearList = new ArrayList<Integer>();
			if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.IS_VALIDATE_ALL_RECORDS))){
				coverageYearList = enrollment1095Repository.getAllCoverageYear();
			}
			else{
				Integer currentYear = DateUtil.getYearFromDate(new Date());
				Integer lastYear = currentYear - 1;
				
				coverageYearList.add(currentYear);
				coverageYearList.add(lastYear);
			}
			
			if(coverageYearList != null && !coverageYearList.isEmpty()){
				List<Enrollment1095ValidationReport> enrollment1095ValidationReportList = new ArrayList<Enrollment1095ValidationReport>();
				List<Enrollment1095> enrollment1095List = new ArrayList<Enrollment1095>();
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.ASSIGNED_POLICY_NUMBER_NULL))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForAssignedPolicyNumberNull(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.ASSIGNED_POLICY_NUMBER_NULL);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.CATASTROPHIC_ENROLLMENT_RECORD))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForCatastrophicEnrollment(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.CATASTROPHIC_ENROLLMENT_RECORD);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.COVERAGE_AND_PREMIUM_MONTH_MISMATCH))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForCoverageAndPremiumMonthMismatch(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.COVERAGE_AND_PREMIUM_MONTH_MISMATCH);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.COVERAGE_START_BEFORE_BIRTH_DATE))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForCoverageStartBeforeBirthDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.COVERAGE_START_BEFORE_BIRTH_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.COVERAGE_START_GREATER_THAN_END_DATE))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForCoverageStartGreaterThanEndDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.COVERAGE_START_GREATER_THAN_END_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.COVERAGE_YEAR_MISMATCH))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForCoverageYearMismatch(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.COVERAGE_YEAR_MISMATCH);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.DENTAL_ENROLLMENT))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForDentalEnrollment(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.DENTAL_ENROLLMENT);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}
				
				//HIX-107395 Remove overlap validations
				/*if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.ENROLLMENT_1095_OVERLAPPING_COVERAGE))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForEnrollment1095OverlappingCoverage(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.ENROLLMENT_1095_OVERLAPPING_COVERAGE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}*/
			 
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.INVALID_ADDRESS_FIELDS))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordForInvalidAddressFields(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.INVALID_ADDRESS_FIELDS);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.INVALID_EHB_SLCSP_AMT))){
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordForInvalidEhbSlcspAmt(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.INVALID_EHB_SLCSP_AMT);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.INVALID_MEMBER_COVERAGE_DATE))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForInvalidMemberCoverageDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.INVALID_MEMBER_COVERAGE_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.INVALID_POLICY_DATES))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordForInvalidPolicyDates(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.INVALID_POLICY_DATES);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.MEMBER_COVERAGE_START_GREATER_THAN_END_DATE))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForMemberCoverageStartGreaterThanEndDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.MEMBER_COVERAGE_START_GREATER_THAN_END_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				//HIX-107395 Remove overlap validations
				/*if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.MEMBER_LEVEL_OVERLAPPING))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForMemberLevelOverlapping(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.MEMBER_LEVEL_OVERLAPPING);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}*/
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.MISSING_RECEIPIENT_RECORD))){
					
					List<BigDecimal> enrollment1095IdList = enrollment1095Repository.getRecordsForMissingReceipientRecord(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (BigDecimal id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id.intValue());
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.MISSING_RECEIPIENT_RECORD);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.NEGATIVE_APTC))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForNegativeAptc(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.NEGATIVE_APTC);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.NON_VOID_CASE_SAME_START_AND_END_DATE))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForNonVoidCaseSameStartAndEndDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.NON_VOID_CASE_SAME_START_AND_END_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.NO_ACTIVE_MEMBERS))){
					List<BigDecimal> enrollment1095IdList = enrollment1095Repository.getRecordForNoActiveMembers(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (BigDecimal id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id.intValue());
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.NO_ACTIVE_MEMBERS);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.NO_PREMIUM_MONTH_PRESENT))){
					
					List<BigDecimal> enrollment1095IdList = enrollment1095Repository.getRecordsForNoPremiumMonthPresent(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (BigDecimal id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id.intValue());
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.NO_PREMIUM_MONTH_PRESENT);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.NULL_BIRTH_DATE))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForNullBirthDate(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.NULL_BIRTH_DATE);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.POLICY_ISSUER_NAME_NULL))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForPolicyIssuerNameNull(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.POLICY_ISSUER_NAME_NULL);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.SSN_LENGTH_INVALID))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordForSsnLengthInvalid(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.SSN_LENGTH_INVALID);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.SSN_VALUE_INVALID))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForSsnValueInvalid(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.SSN_VALUE_INVALID);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.VOID_CORRECTED_CHECKBOX_SET))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForVoidCorrectedCheckboxSet(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.VOID_CORRECTED_CHECKBOX_SET);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}

				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.ZERO_GROSS_PREMIUM))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForZeroGrossPremium(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.ZERO_GROSS_PREMIUM);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}
				
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(Enrollment1095ConfigurationEnum.HOUSEHOLD_MISMATCH))){
					
					List<Integer> enrollment1095IdList = enrollment1095Repository.getRecordsForHouseholdMismatch(coverageYearList);
					if(enrollment1095IdList != null && !enrollment1095IdList.isEmpty()){
						for (Integer id : enrollment1095IdList){
							Enrollment1095ValidationReport enrollment1095ValidationReport = new Enrollment1095ValidationReport();
							Enrollment1095 enrollment1095 = new Enrollment1095();
							enrollment1095.setId(id);
							enrollment1095ValidationReport.setErrorType(EnrollmentConstants.HOUSEHOLD_MISMATCH);
							enrollment1095ValidationReport.setProcessed(ProcessedFlag.Y);
							enrollment1095ValidationReport.setEnrollment1095(enrollment1095);
							enrollment1095List.add(enrollment1095);
							enrollment1095ValidationReportList.add(enrollment1095ValidationReport);
						}
					}
			
				}
				
				if(enrollment1095ValidationReportList != null && !enrollment1095ValidationReportList.isEmpty()
					&& enrollment1095List != null && !enrollment1095List.isEmpty()){
					saveEnrollment1095AndValidationDetails(enrollment1095ValidationReportList, enrollment1095List);
				}
			}
	}
	
	@Transactional(rollbackFor=Exception.class)
	private void saveEnrollment1095AndValidationDetails(List<Enrollment1095ValidationReport> enrollment1095ValidationReportList, 
			List<Enrollment1095> enrollment1095List){
		
		for(Enrollment1095 enrollment1095 : enrollment1095List){
			Enrollment1095 enrollment1095DB = enrollment1095Repository.findById(enrollment1095.getId());
			if(enrollment1095DB != null){
				enrollment1095DB.setIsActive(EnrollmentConstants.N);
				enrollment1095Repository.saveAndFlush(enrollment1095DB);
			}
		}
		iEnrollment1095ValidationReportRepository.save(enrollment1095ValidationReportList); 
	}
	
	private void setCorrectionIndicators(Enrollment1095 enrollment1095record, Enrollment1095 existingEnrollment1095record) {
		if(null != existingEnrollment1095record.getNotice() && null != existingEnrollment1095record.getPdfGeneratedOn()){
			enrollment1095record.setCorrectionIndicatorPdf(YorNFlagIndicator.Y.toString());
			enrollment1095record.setCorrectedCheckBoxIndicator(YorNFlagIndicator.Y.toString());
		}
		if(null != existingEnrollment1095record.getOriginalBatchId()){
			enrollment1095record.setCorrectionIndicatorXml(YorNFlagIndicator.Y.toString());
			enrollment1095record.setCorrectedCheckBoxIndicator(YorNFlagIndicator.Y.toString());
		}
	}
	
	private boolean isIssuerNameSame(Enrollment1095 existingEnrollment1095record, Enrollment1095 enrollment1095record) {
		if (existingEnrollment1095record.getPolicyIssuerName() == null) {
			if (enrollment1095record.getPolicyIssuerName() != null) {
				return false;
			}
		} else if (!existingEnrollment1095record.getPolicyIssuerName().equals(enrollment1095record.getPolicyIssuerName())) {
			return false;
		}
		return true;
	}
	
	private void updateAdditionalFields(Enrollment1095 enrollment1095record, Enrollment enrollment) {
		if (enrollment.getAptcAmt() != null) {
			enrollment1095record.setFinancial(true);
		}
		if (enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM) && enrollment.getPremiumPaidToDateEnd() != null) {
			enrollment1095record.setPremiumPaidToDateEnd(enrollment.getPremiumPaidToDateEnd());
			List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
			enrollmentEventList.addAll(enrollment.getEnrollmentEvents());
			Collections.sort(enrollmentEventList, new Comparator<EnrollmentEvent>() {
				@Override
				public int compare(EnrollmentEvent o1, EnrollmentEvent o2) {
					if (o1.getCreatedOn() != null && o2.getCreatedOn() != null) {
						return o2.getCreatedOn().compareTo(o1.getCreatedOn());
					}
					return 0;
				}
			});
			for (EnrollmentEvent enrollmentEvent : enrollmentEventList) {
				if (enrollmentEvent.getEventTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.EVENT_TYPE_TERMINATION)) {
					if (enrollmentEvent.getEventReasonLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.EVENT_REASON_NON_PAYMENT)) {
						enrollment1095record.setTermByNonPayment(true);
						break;
					} else {
						break;
					}
				}
			}
		}
	}

}
