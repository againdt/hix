import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import bitWiseTools from '../../utils/bitWiseOperatorTools';
import currencyTools from '../../utils/currencyTools';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import MASKS from '../../constants/masksConstants';
import VALIDATION_TYPES from '../../constants/validationTypes';

// Components

// ActionCreators

// Utils

// Constants


const Page3_35_E1 = props => {

    const {
        data,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        householdMembers,
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        flagBucket: {
            annualTaxIncomeFlags
        },
        annualTaxIncome:{
            reportedIncome,
            projectedIncome,
        },
        name
    } = currentHhm;

    const {
        annualTaxIncome: {
            variableIncomeFlags,
            unknownIncomeFlags
        }
    } = MASKS;

    const _fl = bitWiseTools.getFlagValue(annualTaxIncomeFlags);
    const variableIncome = _fl(variableIncomeFlags);
    const unknownIncome = _fl(unknownIncomeFlags);

    const onFieldChange = (...args) => {
        const [ newData , , , [ name, value ] ] = args;

        if(name === 'variableIncome') {
            newData.annualTaxIncome.projectedIncome = value ? 0 : projectedIncome;
            newData.flagBucket.annualTaxIncomeFlags =
                bitWiseTools
                    .getUpdatedFlags(
                        annualTaxIncomeFlags,
                        [
                            [variableIncomeFlags, value],
                            [unknownIncomeFlags, null]
                        ]
                    );
        }

        if(name === 'unknownIncome') {
            newData.flagBucket.annualTaxIncomeFlags =
                bitWiseTools
                    .getUpdatedFlags(
                        annualTaxIncomeFlags,
                        [
                            [unknownIncomeFlags, value]
                        ]
                    );
            newData.annualTaxIncome.projectedIncome = 0;
        }

        const newState = { ...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    const checkTopLevelValidation = () => {
        if(variableIncome === false) {
            return !!projectedIncome;
        }
        return true;
    };

    return (
        <div id="iiExpectedIncom" className="fade-in">
            <div className="subsection">
                <Form  topLevelValidity={checkTopLevelValidation()}>
                    <UniversalInput
                        legend={{
                            legendText:
                                <span>
                                    Based on what you told us,&nbsp;
                                    <strong>
                                        { normalizeNameOfPerson(name) }
                                    </strong>
                                    {'\'s income will be about'}&nbsp;
                                    <strong>
                                        { reportedIncome === 0 ? '$0.00' : currencyTools.getMoneyFormat(reportedIncome, true) }
                                    </strong>
                                    . Is this how much you think will get in&nbsp;
                                    <strong>
                                        { coverageYear }
                                    </strong>
                                    ?
                                </span>
                        }}
                        label={{
                            text: '',
                            labelClass: ['usa-width-one-whole', 'layout--02'],
                            ulClass: ['usa-width-one-whole margin-b-20'],
                            showRequiredIcon: true
                        }}
                        inputData={{
                            isInputRequired: true,
                            type: INPUT_TYPES.RADIO_BUTTONS,
                            classes:['disp-styl-none'],
                            name: 'variableIncome',
                            value: variableIncome,
                            options: DROPDOWN_OPTIONS.YES_NO,
                            validationType: VALIDATION_TYPES.CHOSEN,
                            currentData: householdMembers[member],
                            fields: ['annualTaxIncome']
                        }}
                        inputActions={{
                            onFieldChange
                        }}
                        errorClasses={['margin-l-0 margin-l-20-ve']}
                    />

                    {
                        variableIncome === false &&

                        <Fragment>
                            <p className='required-true'>
                                <span>
                                    Based on what you know today, how much do you think&nbsp;
                                    <strong>
                                        { normalizeNameOfPerson(name) }&nbsp;
                                    </strong>
                                    will make in&nbsp;
                                    <strong>
                                        { coverageYear }
                                    </strong>
                                    ?
                                </span>
                            </p>

                            {
                                !unknownIncome &&

                                <UniversalInput
                                    label={{
                                        text: 'Total yearly amount',
                                        labelClasses: ['usa-width-one-third']
                                    }}
                                    inputData={{
                                        isInputRequired: false,
                                        type: INPUT_TYPES.MONEY_INPUT,
                                        name: 'projectedIncome',
                                        value: projectedIncome,
                                        isOnlyPositiveAvailable: true,
                                        inputClasses: ['usa-width-two-thirds'],
                                        validationType: VALIDATION_TYPES.MONEY,
                                        placeholder: 'Amount',
                                        validationErrorMessage: ['Please enter a dollar value'],
                                        currentData: householdMembers[member],
                                        fields: ['annualTaxIncome']
                                    }}
                                    inputActions={{
                                        onFieldChange
                                    }}

                                />
                            }

                            <UniversalInput
                                label={{
                                    text: 'I don’t know'
                                }}
                                inputData={{
                                    isInputRequired: false,
                                    type: INPUT_TYPES.CHECKBOX,
                                    name: 'unknownIncome',
                                    classes:['layout--01'],
                                    checked: unknownIncome,
                                    value: unknownIncome,
                                    currentData: householdMembers[member],
                                    fields: ['annualTaxIncome']
                                }}
                                inputActions={{
                                    onFieldChange
                                }}
                            />
                        </Fragment>
                    }

                    {
                        unknownIncome &&

                        <Fragment>
                            <p>
                                <span>
                                    {'That\'s okay. Make your best estimate of'}&nbsp;
                                    <strong>
                                        { normalizeNameOfPerson(name) }
                                    </strong>
                                    {'\'s total income for'}&nbsp;
                                    <strong>
                                        { coverageYear }
                                    </strong>
                                    .
                                </span>
                            </p>
                            <UniversalInput
                                label={{
                                    text: 'Total yearly amount',
                                    labelClasses: ['usa-width-one-third']
                                }}
                                inputData={{
                                    isInputRequired: true,
                                    type: INPUT_TYPES.MONEY_INPUT,
                                    name: 'projectedIncome',
                                    value: projectedIncome,
                                    isOnlyPositiveAvailable: true,
                                    inputClasses: ['usa-width-two-thirds'],
                                    validationType: VALIDATION_TYPES.MONEY,
                                    placeholder: 'Amount',
                                    currentData: householdMembers[member],
                                    fields: ['annualTaxIncome']
                                }}
                                inputActions={{
                                    onFieldChange
                                }}
                            />
                        </Fragment>
                    }
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page3_35_E1.defaultProps = {
    name: 'Page3_35_E1'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page3_35_E1);
