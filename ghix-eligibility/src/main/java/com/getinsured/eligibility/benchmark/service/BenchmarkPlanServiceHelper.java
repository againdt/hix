package com.getinsured.eligibility.benchmark.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest.Members;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest.Members.Member;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanResponse;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.ObjectFactory;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;


/**
 * Assumptions:
 * 	All members in the tax household section of ssap json would be send to BenchMark Service.
 *
 *
 *
 * @author kazi_e
 *
 */
@Service("benchmarkPlanServiceHelper")
public class BenchmarkPlanServiceHelper {

	private static final String QHP = "QHP";
	private static final String COVERAGE_DATE_NOT_FOUND = "Coverage Date not found for ";
	private static final String ID = "ID";
	private static final String TOBACCO_N = "N";
	private static final String TOBACCO_Y = "Y";
	private static final String CHILD = "child";
	private static final String SPOUSE = "spouse";
	private static final String SELF = "self";
	private static final String DEPENDANT = "dependent";
	private static final String SSAP_APPLICATION_ID_PATH = "$.singleStreamlinedApplication.ssapApplicationId";
	private static final String SSAP_CASE_NUMBER_PATH = "$.singleStreamlinedApplication.applicationGuid";
	private static final String SSAP_TAX_HOUSEHOLD_PATH = "$.singleStreamlinedApplication.taxHousehold[*]";

	private static final String IDAHO_COUNTY_FIPS_CODE = "16";
	private static final String NEWMEXICO_COUNTY_FIPS_CODE = "35";

	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired private BenchmarkPlanWebClient benchmarkPlanWebClient;

	private static final String WS_DATE_FORMAT = "MM/dd/yyyy";
	private static SimpleDateFormat WS_SIMPLE_DATE_FORMATTER = new SimpleDateFormat(WS_DATE_FORMAT);


	@SuppressWarnings("serial")
	private static final Map<String , String> BLOOD_RELATIONS = new HashMap<String , String>() {{
		put("18", SELF);
		put("01", SPOUSE);
		put("03",CHILD);
		put("16",CHILD);
//Added 03,16 relationship as child bcoz in referral we are doing inverse relationship mapping. in look up table 03,16 represents dad or mom, Step parent -Pravin
	}};

	/*@Deprecated
	public GetBenchmarkPlanResponse invoke(SsapApplication ssapApplication) {

		GetBenchmarkPlanRequest request = formRequest(ssapApplication.getApplicationData());
		return benchmarkPlanWebClient.invokeBenchMarkWebService(request);
	}*/


	public GetBenchmarkPlanResponse invoke(SsapApplication ssapApplication, Date coverageStartDate) {

		GetBenchmarkPlanRequest request = formRequest(ssapApplication, formWSDate(coverageStartDate));
		return benchmarkPlanWebClient.invokeBenchMarkWebService(request);
	}


	private String computeCoverageStartDate(String ssapJson) {
		final String primaryId = (String) JsonPath.read(ssapJson,SSAP_APPLICATION_ID_PATH);
		//SsapApplicationEvent ssapApplicationEvent =  ssapApplicationEventRepository.findEventBySsapApplication(Long.valueOf(primaryId));
		List<SsapApplicationEvent> ssapApplicationEvents = ssapApplicationEventRepository.findEventBySsapApplicationId(Long.valueOf(primaryId));
		if (ssapApplicationEvents != null && ssapApplicationEvents.size() > 0){
			SsapApplicationEvent ssapApplicationEvent = ssapApplicationEvents.get(0);
			if(ssapApplicationEvent == null || ssapApplicationEvent.getCoverageStartDate() == null){
				throw new GIRuntimeException(COVERAGE_DATE_NOT_FOUND+ primaryId);
			}

			Date date = new TSDate(ssapApplicationEvent.getCoverageStartDate().getTime());
			return formWSDate(date);
		} else {
			throw new GIRuntimeException(COVERAGE_DATE_NOT_FOUND+ primaryId);
		}
	}

	/*private GetBenchmarkPlanRequest formRequest(String ssapJson) {

		final String coverageStartDate = computeCoverageStartDate(ssapJson);
		return formRequest(ssapJson, coverageStartDate);
	}*/

	private GetBenchmarkPlanRequest formRequest(SsapApplication ssapApplication,
			final String coverageStartDate) {
		ObjectFactory factory = new ObjectFactory();
		GetBenchmarkPlanRequest getBenchmarkPlanRequest = factory.createGetBenchmarkPlanRequest();
		getBenchmarkPlanRequest.setHouseholdCaseId(buildHHCaseId()); //benchmark expects 10 character
		getBenchmarkPlanRequest.setCoverageStartDate(coverageStartDate);
		getBenchmarkPlanRequest.setMembers(prepareMembers(ssapApplication, factory));

		return getBenchmarkPlanRequest;
	}

	/*private GetBenchmarkPlanRequest formRequest(String ssapJson,
			final String coverageStartDate) {
		ObjectFactory factory = new ObjectFactory();
		GetBenchmarkPlanRequest getBenchmarkPlanRequest = factory.createGetBenchmarkPlanRequest();
		getBenchmarkPlanRequest.setHouseholdCaseId(buildHHCaseId()); //benchmark expects 10 character
		getBenchmarkPlanRequest.setCoverageStartDate(coverageStartDate);
		getBenchmarkPlanRequest.setMembers(prepareMembers(ssapJson, factory));

		return getBenchmarkPlanRequest;
	}*/


	private String buildHHCaseId() {
		return RandomStringUtils.randomAlphanumeric(10);
	}

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	
	private Members prepareMembers(SsapApplication ssapApplication, ObjectFactory factory) {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>> token = new TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>>(){};
		Gson ggg = new Gson();
		String ssapJson = ssapApplication.getApplicationData();
		
		List<String> eligibleMembers = new ArrayList<>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (QHP.equals(applicant.getEligibilityStatus())){
				eligibleMembers.add(applicant.getApplicantGuid());
			}
		}
		
		String taxhh = JsonPath.read(ssapJson ,SSAP_TAX_HOUSEHOLD_PATH).toString();
		List<com.getinsured.iex.ssap.TaxHousehold> thhList = ggg.fromJson(taxhh, token.getType());

		if (thhList == null || thhList.size() == 0){
			throw new GIRuntimeException();
		}

		List<HouseholdMember> txMembers = thhList.get(0).getHouseholdMember();

		if (txMembers == null || txMembers.size() == 0){
			throw new GIRuntimeException();
		}

		java.util.Collections.sort(txMembers, new HouseholdMemberComparator());

		List<BloodRelationship> bloodRelations = txMembers.get(0).getBloodRelationship();


		if (txMembers.get(0).getHouseholdContact() == null || txMembers.get(0).getHouseholdContact().getHomeAddress() == null
				|| StringUtils.isEmpty(txMembers.get(0).getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode())
				|| StringUtils.isEmpty(txMembers.get(0).getHouseholdContact().getHomeAddress().getPostalCode())){
			throw new GIRuntimeException();
		}

		String primaryAddressCountyFipsCode = txMembers.get(0).getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();

		String primaryCountycode = null;
		if (StringUtils.length(primaryAddressCountyFipsCode) == 5){
			primaryCountycode = primaryAddressCountyFipsCode;
		} else {
			primaryCountycode = ID.equalsIgnoreCase(txMembers.get(0).getHouseholdContact().getHomeAddress().getState()) ?
					IDAHO_COUNTY_FIPS_CODE + primaryAddressCountyFipsCode : NEWMEXICO_COUNTY_FIPS_CODE + primaryAddressCountyFipsCode;

		}
		String primaryZipcode = txMembers.get(0).getHouseholdContact().getHomeAddress().getPostalCode();

		Members members = factory.createGetBenchmarkPlanRequestMembers();
		for (HouseholdMember householdMember : txMembers) {
			
			if (!eligibleMembers.contains(householdMember.getApplicantGuid())){
				continue;
			}

			Member member = factory.createGetBenchmarkPlanRequestMembersMember();

			BloodRelationship brss = bloodRelations.get(bloodRelations.indexOf(BloodRelationship.build("1", householdMember.getPersonId().toString())));
			String relationShip = DEPENDANT;
			if (BLOOD_RELATIONS.containsKey((brss.getRelation()))){
				relationShip = BLOOD_RELATIONS.get(brss.getRelation());
			}

			member.setCountyCode(primaryCountycode);
			member.setZip(primaryZipcode);

			member.setDob(formWSDate(householdMember.getDateOfBirth()));
			member.setRelation(relationShip);
			//HIX-110285: Set tobacco user as NULL for CA 
			if("CA".equalsIgnoreCase(stateCode)){
				member.setTobacco(null);
			}
			else{
				member.setTobacco(householdMember.getTobaccoUserIndicator() != null ? householdMember.getTobaccoUserIndicator() ? TOBACCO_Y : TOBACCO_N: TOBACCO_N);	
			}
			
			
			
			members.getMember().add(member);
		}

		return members;
	}


	private static String formWSDate(Date date) {
		return WS_SIMPLE_DATE_FORMATTER.format(date);
	}


	private class HouseholdMemberComparator implements Comparator<HouseholdMember>{

		@Override
		public int compare(HouseholdMember o1, HouseholdMember o2) {
			return o1.getPersonId().compareTo(o2.getPersonId());
		}
	}


}
