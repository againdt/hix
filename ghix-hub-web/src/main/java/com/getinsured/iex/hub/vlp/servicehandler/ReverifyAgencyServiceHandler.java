package com.getinsured.iex.hub.vlp.servicehandler;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifResponseType;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp.wrapper.ReverifyAgency3InitVerifRequestTypeWrapper;
import com.getinsured.iex.hub.vlp.wrapper.ReverifyAgency3InitVerifResponseTypeWrapper;

/**
 * Service handler ReverifyAgencyRequest
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 */
public class ReverifyAgencyServiceHandler extends AbstractServiceHandler{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReverifyAgencyServiceHandler.class);
	private static ObjectFactory factory;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException {

		ReverifyAgency3InitVerifRequestTypeWrapper vlpWrapper = null;
		try{
			JSONParser parser = new JSONParser();
			JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
			processJSONObject(payloadObj);
			vlpWrapper = ReverifyAgency3InitVerifRequestTypeWrapper.fromJsonString(payloadObj.toJSONString());
			LOGGER.info("Setting input JSON to:"+getJsonInput());
			return vlpWrapper.getAgency3VerifRequestType();
		}
		catch(Exception e){
			throw new HubServiceException("Failed to generate the payload:",e);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
		 
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value.toString());
	        }
	 }
	
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = "";
		try{
			JAXBElement<ReverifyAgency3InitVerifResponseType> resp= (JAXBElement<ReverifyAgency3InitVerifResponseType>)responseObject;
			ReverifyAgency3InitVerifResponseType payload = resp.getValue();
			ReverifyAgency3InitVerifResponseTypeWrapper response = new ReverifyAgency3InitVerifResponseTypeWrapper(payload);

			if(response != null){		
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				if(response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
				//Case number
				if(response != null && response.getAgency3InitVerifResponse() != null){
					this.getResponseContext().put("HUB_CASE_NUMBER", response.getAgency3InitVerifResponse().getCaseNumber());
				}
				this.requestStatus = RequestStatus.SUCCESS;
			}

		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+ReverifyAgency3InitVerifResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
