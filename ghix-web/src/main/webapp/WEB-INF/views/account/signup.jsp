<%@page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />

<%@page import="java.util.Properties"%>
	<%@page import="org.springframework.context.ApplicationContext"%>
	<%!private static ApplicationContext ctx = null;%>
	<%!private static Properties prop = null;%>
	<%
	        if (ctx == null) {
	                ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	        }
	        if (prop == null) {
	                prop = (Properties) ctx.getBean("configProp");
	        }
	%>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	<script type="text/javascript">
window.onload = changeTitle();
function changeTitle() {
	if ($.trim(document.title) == 'Getinsured Health Exchange: Manage Broker'){
		document.title ='Getinsured Health Exchange: Manage Agent';
	 }
}
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style rel="stylesheet" type="text/css">
#recaptcha table tr{
	height:auto !important;
}
</style>

<html>
	<body>

	<c:url value="/account/signup/checkUserPassword" var="theurlforCheckUserPassword"></c:url>
	<c:url value="/account/signup/checkSSNAndBirthDate" var="theurlforCheckSSNAndBirthDate"></c:url>
	
	<c:url value="signup/checkEmail" var="theUrltocheckEmail"> </c:url>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="form-wrapper">
	<div class="row-fluid">
	<%
    	WebApplicationContext webAppContext = RequestContextUtils.getWebApplicationContext(request);
    	MessageSource messageSource = (MessageSource)webAppContext.getBean("messageSource");
    	String passwordPolicyRegex = "";
    	String errorMsg = "";
    	String exchnagefullName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME);
    	String isPolicyActive = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE);
    	if(isPolicyActive != null && isPolicyActive.equalsIgnoreCase("Y")){
    		passwordPolicyRegex = DisplayUtil.replaceDynamicValuesInMessageSource(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX));
    		errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
    	}%>
    	<label for="passwordPolicyRegex" aria-hidden="true" class="aria-hidden">passwordPolicyRegex</label>
    	<input type="hidden" aria-hidden="true" id="passwordPolicyRegex" name="passwordPolicyRegex" value="<%=passwordPolicyRegex%>"/>	 
    	  <label for="passwordPolicyErrorMsg" aria-hidden="true" class="aria-hidden">passwordPolicyErrorMsg</label>
        <input type="hidden" aria-hidden="true" id="passwordPolicyErrorMsg" name="passwordPolicyErrorMsg" value="<%=errorMsg%>"/>	
	<!-- HIX-27186 Redirecting user to account/user/addrole/<nonDefRoleNem> on button click and add the entry in user_role table with default flag = 'N' -->

	<!-- HIX-46354, HIX-44864  , HIX-44856 : Disable Add additional role Button only for 'ASSISTER','ASSISTERENROLLMENTENTITY','INDIVIDUAL'; 
	     if global.StateExchangeType='Individual' then also disable for 'BROKER'  -->
	<!-- HIX-46354 :  Disable for all roles (until SHOP8 ref: HIX-45641) -->
	<%-- 	<c:set var="stateExchangeType" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE)%>" /> 
		<c:choose>
			<c:when test="${fn:toUpperCase(stateExchangeType) eq 'INDIVIDUAL'}">
				<c:choose>
					<c:when test="${(defUserRoleName != 'ASSISTER') && (defUserRoleName != 'ASSISTERENROLLMENTENTITY')&& (defUserRoleName != 'BROKER') &&(defUserRoleName != 'INDIVIDUAL')}">
						<span class="pull-right gutter10-tb"><small><spring:message  code="label.entity.alreadyaccount"/></small><a class="btn btn-small btn-primary margin10-l" role="button" href="<c:url value="/account/user/addrole/${defUserRoleName}" />"><span class="aria-hidden"><spring:message  code="label.entity.alreadyaccount"/></span>&nbsp;&nbsp;<spring:message  code="label.entity.add"/></a></span>
					</c:when>
				</c:choose>	
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${(defUserRoleName != 'ASSISTER') && (defUserRoleName != 'ASSISTERENROLLMENTENTITY') &&(defUserRoleName != 'INDIVIDUAL')}">
						<span class="pull-right gutter10-tb"><small><spring:message  code="label.entity.alreadyaccount"/></small><a class="btn btn-small btn-primary margin10-l" role="button" href="<c:url value="/account/user/addrole/${defUserRoleName}" />"><span class="aria-hidden"><spring:message  code="label.entity.alreadyaccount"/></span>&nbsp;&nbsp;<spring:message  code="label.entity.add"/></a></span>
					</c:when>
				</c:choose>	
			</c:otherwise>
		</c:choose>	
		 --%>

	</div>
    <div class="row-fluid">
      <div class="titlebar">
			  <h1>
			  <c:choose>
			    <c:when test="${fn:containsIgnoreCase(signUpUser.signUpRole.label,'individual')}">
				    <a name="skip"></a>Set Up Your Individual Account on&nbsp;<%=exchnagefullName%>
			    </c:when>
			    <c:otherwise>
				    <a name="skip"></a><spring:message  code="signup_lbl.new"/>
				    <!-- HIX-27186 Adding Role Label in place of RoleName for signup -->
				    <c:out value="${signUpUser.signUpRole.label}" />
				    </a><spring:message  code="signup_lbl.accountSetup"/>
			   </c:otherwise>
			  </c:choose>
	  		</h1>
  		</div>
			</div>

	
	<!-- start of span3 -->
	<div class="row-fluid signup-container">
		
		<!-- start of span9 --->
		<div class="" id="rightpanel">
			<form class="form-horizontal" id="frmuserreg" name="frmuserreg"
				action="signup" method="POST" autocomplete="off" onsubmit="submitForm()">
				<df:csrfToken/>
				<small class="margin10-t">All fields on this form marked with an asterisk (*) are required.</small>
				<div id="basic-information">
					<h4><spring:message  code="signup_lbl.basicInformation"/></h4>
					
					<fieldset>
						<legend class="aria-hidden">Basic Information</legend>  <%-- Accessibility use --%>
						<div class="form-group">
							<label for="firstName" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message
									code="label.userFirstName" /> <img
								src="<c:url value="/resources/images/requiredAsterix.png" />"
								width="10" height="10" alt="Required!" /> </label>
							<!-- end of label -->
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="text" name="firstName" id="firstName" value="${signUpUser.firstName}" class="input-large" size="30" onkeypress="resetErrorMsg(this)" />
									
								<%-- <input type="text" name="firstName" id="firstName"
									<c:if test="${activationFirstName != null && activationFirstName != ''}">
										value="<c:out value="${activationFirstName}"/>"
									</c:if>
									class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)" /> --%>
								<div id="firstName_error"></div>
							</div>
							<!-- end of form-group-->
						</div>
						<!-- end of form-group -->

						<div class="form-group">
							<label for="lastName" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message
									code="label.userLastName" /> <img
								src="<c:url value="/resources/images/requiredAsterix.png" />"
								width="10px" height="10px" alt="Required!" /> </label>
							<!-- end of label -->
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" name="lastName" id="lastName" value="${signUpUser.lastName}" 	class="input-large" size="30" onkeypress="resetErrorMsg(this)" />
									
								<%-- <input type="text" name="lastName" id="lastName"
									<c:if test="${activationLastName != null && activationLastName != ''}">
										value="<c:out value="${activationLastName}"/>"</c:if>
									class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)" /> --%>
								<div id="lastName_error"></div>
							</div>
							<!-- end of form-group-->
						</div>
						<!-- end of form-group -->

						<div class="form-group">
							<label for="email" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message
									code="label.userEmail" /> <img
								src='<c:url value="/resources/images/requiredAsterix.png" />'
								width="10" height="10" alt="Required!" /> </label>
							<!-- end of label -->
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<c:choose>
									<c:when test="${not signUpUser.isActivationFlow || empty signUpUser.email}">
										<input type="email" name="email" id="email" value="${signUpUser.email}" class="input-large" size="30" />
									</c:when>
									<c:otherwise>
										<input type="email" name="email" id="email" value="${signUpUser.email}" readOnly="true" class="input-large" size="30" />
									</c:otherwise>
								</c:choose>	
		
								<%-- <input type="email" name="email" id="email"
									<c:if test="${activationEmail != null && activationEmail != ''}">
										value="<c:out value="${activationEmail}"/>"
										<c:if test="${captchaError == null || captchaError == ''}">
										readOnly="true"
										</c:if>
									</c:if>
									class="input-xlarge" size="30" /> --%>
								<div id="email_error" class="error"></div>
							</div>
							<!-- end of form-group-->
						</div>
						<div class="form-group">
							<label for="confirmEmail" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.userConfirmEmail" />
								<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="Required!" /> 
							</label>
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="email" name="confirmEmail" id="confirmEmail" value="${signUpUser.confirmEmail}" class="input-large" size="30" onkeypress="resetErrorMsg(this)" />
								<%-- <input type="email" name="confirmEmail" id="confirmEmail" class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)" 
									<c:choose>
										<c:when test="${sigupUpPreValueMap != null && sigupUpPreValueMap['confirmEmail'] != ''}">
											value="<c:out value="${sigupUpPreValueMap['confirmEmail']}"/>"
										</c:when>
										<c:otherwise>
											value =""
										</c:otherwise>
									</c:choose>
									
								class="input-xlarge" size="30" /> --%>
								<div id="confirmEmail_error" class="error"></div>
							</div>
							<!-- end of form-group-->
						</div>
						<!-- end of form-group -->
						<div class="form-group">
							<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="phone1"><spring:message code="label.phone" />
							<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="Required!" />
							<a class="ttclass" rel="tooltip" href="#" tabindex="-1" data-original-title='<spring:message  code="label.signup.mobiletooltip" />'><i class="icon-question-sign"></i></a></label>
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="input-mini" name="phone1" id="phone1" data-type="number" maxlength="3" value ="${signUpUser.phone1}" title="Enter first 3 digits of your phone number" />
								<input type="text" class="input-mini" name="phone2" id="phone2" data-type="number" maxlength="3" value ="${signUpUser.phone2}" title="Enter second 3 digits of your phone number" />
								<input type="text" class="input-mini" name="phone3" id="phone3" data-type="number" maxlength="4" value ="${signUpUser.phone3}" title="Enter last 4 digits of your phone number" />
								<div class="error" id="phone3_error"></div>
								<div id="phone_error" class="error"></div>
							</div>

						</div>
						
						<c:if test="${ssnRequired == 'true'}">
							<div class="form-group">
								<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="ssn1">
									<spring:message code="indportal.communicationPreferences.ssn" />
									<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
								</label>
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<input 
										type="hidden" 
										id="ssn" 
										name="ssn">
									<input 
										type="password" 
										id="ssn1" 
										name="ssn1" 
										class="input-mini"
										maxlength="3">
										
									<input 
										type="password" 
										id="ssn2" 
										name="ssn2" 
										class="input-mini"
										maxlength="2">
										
									<input 
										type="text" 
										id="ssn3" 
										name="ssn3" 
										class="input-mini"
										maxlength="4">
									<div class="error" id="ssn3_error"></div>	
									<div class="error" id="ssnExists_error"></div>	
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="confirmSsn1">
									<spring:message code="indportal.communicationPreferences.confirmSsn" />
									<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
								</label>
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<input 
										type="password" 
										id="confirmSsn1" 
										name="confirmSsn1" 
										class="input-mini"
										maxlength="3">
										
									<input 
										type="password" 
										id="confirmSsn2" 
										name="confirmSsn2" 
										class="input-mini"
										maxlength="2">
										
									<input 
										type="text" 
										id="confirmSsn3" 
										name="confirmSsn3" 
										class="input-mini"
										maxlength="4">
									<div class="error" id="confirmSsn3_error"></div>	
								</div>
							</div>
										
							<div class="form-group margin20-t">
								<label for="dob" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
									<spring:message code="indportal.communicationPreferences.dob" />
									<img 
										src='<c:url value="/resources/images/requiredAsterix.png" />' 
										width="10" 
										height="10" 
										alt="required" />
								</label>
						
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<input 
										type="text" 
										name="birthDate" 
										id="birthDate" 
										placeholder="MM/DD/YYYY"
										value="${dob}"
										class="input-medium"
										<c:if test="${not empty dob}"> readonly="readonly"</c:if>
										>
									
									<div class="error" id="birthDate_error"></div>
								</div>
							</div>
						</c:if>
						
					</fieldset>
				</div>

				<!--/#basic-information -->
				<!--#security-questions : HIX-46480 :Configuration to set the number of SecurityQuestion Sets -->
				<div id="security-questions">
					<h4><spring:message code="label.securityQuestions" /></h4>
					<fieldset>
						<legend class="aria-hidden" aria-hidden="true">Security Question</legend>  <%-- Accessibility use --%>
						
		          		 <c:forEach var="currQstnSet" items="${htQuestionSet}" varStatus="qstnSetStatus">
		          		 	<c:set var="qstnSetIdx" value="${qstnSetStatus.index+1}" />
		          		 	<c:set var="qstnSetKey" value="securityQuestionSet${qstnSetIdx}" />
							<c:set var="currQstnList" value="${htQuestionSet[qstnSetKey]}" /> 
			          		 <div class="form-group">
								<label class="gutter10-tb col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="securityQuestion${qstnSetIdx}" ><spring:message code="label.securityQuestions" /><img
									src='<c:url value="/resources/images/requiredAsterix.png" />'
									width="10" height="10" alt="Required!" />
								</label>
								<div class="gutter10-b col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<label for="securityQuestion${qstnSetIdx}"><span class="aria-hidden">Security Question</span></label>
									<select id="securityQuestion${qstnSetIdx}" name="securityQuestion${qstnSetIdx}"
										path="securityQuestionsList${qstnSetIdx}" class="input-large">
										<option value=""><spring:message code="label.select" /></option>
										<c:forEach var="questionList" items="${currQstnList}">
											<option 
												<c:choose>
													<c:when test="${(questionList.name eq signUpUser.securityQuestions[qstnSetIdx-1])}">
														SELECTED
													</c:when>
												</c:choose>
												value="<c:out value="${questionList.name}" />">
												<c:out value="${questionList.name}" />
											</option>
										</c:forEach>
									</select>
									<div id="securityQuestion${qstnSetIdx}_error"></div>
								</div> 
								<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
									<label for="securityAnswer${qstnSetIdx}" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label aria-hidden">Security Answer</label>
									<input type="text" class="input-large" name="securityAnswer${qstnSetIdx}" id="securityAnswer${qstnSetIdx}"  value ="${signUpUser.securityAnswers[qstnSetIdx-1]}"	/>
									<div id="securityAnswer${qstnSetIdx}_error"></div>
								</div>
							</div>
				        </c:forEach>
					</fieldset>
				</div>
				<!--/#security-questions  -->

				<div id="set-password">
					<h4><spring:message code="label.setPassword" /></h4>
					<div class="form-group">
						<label for="password" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message
								code="label.password" /> <img
							src="<c:url value="/resources/images/requiredAsterix.png" />"
							width="10" height="10" alt="Required!" /> </label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="password" name="password" id="password" autocomplete="off" class="input-large" size="30" onkeypress="resetErrorMsg(this)" />
							<div id="password_error" class="error" style="color: red">
							<!-- ${passwordError} -->
							</div>
						</div>
						<!-- end of form-group-->
					</div>
					<!-- end of form-group -->

					<div class="form-group">
						<label for="confirmPassword" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message
								code="label.userConfirmPassword" /> <img
							src="<c:url value="/resources/images/requiredAsterix.png" />"
							width="10" height="10" alt="Required!" /> </label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="password" name="confirmPassword"
								id="confirmPassword" value="" autocomplete="off" class="input-large" size="30"
								onkeypress="resetErrorMsg(this)" />
							<div id="confirmPassword_error" class="error"></div>
						</div>
					</div>
				</div>
				<!--/#set-password -->
				<c:if test="${hideCaptcha != 'true'}">
					<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %> 
					<div id="captcha">
						<h4><spring:message code="label.proveYouAreNotARobot" /></h4>
						<fieldset>
							<legend class="aria-hidden">Captcha Code</legend> <%-- Accessibility use --%>
							<div class="form-group">
								<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="recaptcha_response_field">
									<spring:message code="label.securityCode" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
								</label>
								
								<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
									<div class="g-recaptcha" data-sitekey="<%=prop.getProperty("Recaptcha.siteKey")%>" data-callback="reCapchaFilled"></div>
									
									<input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha">
									<div class="error help-inline" id="hiddenRecaptcha_error"></div>
									
									<div id="securityCode_error" class=" error help-inline" style="color: red">
										${captchaError}
									</div>
								</div>
									
							</div>
						</fieldset>
					</div>
					<!--/#captcha -->
				</c:if>
	
				<div id="termsDiv">
					<fieldset>
						<legend class="aria-hidden">Agree to Privacy Policy</legend> <%-- Accessibility use --%>
						<div class="form-group">
							<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
							<label class="checkbox" for="terms" title="Please check this box to signup"> <input type="checkbox" value="Accept Terms" name="terms" id="terms">
								<spring:message code="label.IAgreeToThe" /> <a target="_blank" href="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL)%>"><spring:message code="label.getInsuredTermsAndConditions" /></a>
							</label>
							<div id="terms_error" class=""></div>
						</div>
						</div>
					</fieldset>
				</div>
				<!--/#terms -->
				
				
				<input type="hidden" name="userNameCheck" id="userNameCheck" value="" /> 
				<input type="hidden" name="encRoleName"	id="encRoleName" value="${encRoleName}" /> 
				<input type="hidden" name="activationId" id="activationId" value="${signUpUser.activationId}" />

				<div class="form-actions">
					<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
					<input type="button" disabled="disabled" name="submitbtn" id="submitbtn" onClick="javascript:validateForm();"
					value="<spring:message  code='label.signupBtn'/>" title="<spring:message  code='label.signupBtn'/>"	class="btn btn_save btn-primary" />
					
					<button class="btn btn_cancel pull-left" type="button" onClick="location.href = '/hix';">
						<spring:message code="indportal.communicationPreferences.cancel" />
					</button>
				</div>
				</div>
				<!-- end of form-actions -->
			</form>
			<!-- end of form -->
		</div>
		<!-- end of span9 -->
	</div>
	<!-- end of row-fluid -->
</div>
<!-- end of gutter10-->
	</div>
</body>
</html>
<script type="text/javascript">

	var theUrltocheckEmail = '<c:out value="${theUrltocheckEmail}"/>';
	var theurlforCheckUserPassword =  '<c:out value="${theurlforCheckUserPassword}"/>';
    var theurlforCheckSSNAndBirthDate=	'<c:out value="${theurlforCheckSSNAndBirthDate}"/>';

    $('.ttclass').tooltip();
	jQuery.validator.addMethod("userNameCheck",	function(value, element, param) {
		return $("#userNameCheck").val() == 0 ? false : true;
	});

	jQuery.validator.addMethod("confirmEmail", function(value, element, param) {
		password = $("#email").val().toLowerCase();
		confirmpwd = $("#confirmEmail").val().toLowerCase();

		if (password != confirmpwd) {
			return false;
		}
		return true;
	});
	
	/*disable all the keypress except for numbers*/
	$('#phone1, #phone2, #phone3, .ssn').keypress(function(e){
	  	if (window.event) { var charCode = window.event.keyCode; }
	    else if (e) { var charCode = e.which; }
	    else { return true; }
	    if (charCode > 31 && (charCode < 48 || charCode > 57))  	{return false; }
	    return true;
	  
	});

	function chkUserName() {
		$.get('signup/chkUserName', {
			userName : $("#userName").val()
		}, function(response) {
			
			if (response == "EXIST") {
				$("#firstNameValue").val($("#firstName").val());
				$("#userNameCheck").val(0);
			} else {
				$("#firstNameValue").val($("#firstName").val());
				$("#userNameCheck").val(1);
			}
		});
	}

	function resetErrorMsg(element) {
		var elementId = element.id;

		$("#" + elementId + "_error").html('');
	}

	jQuery.validator.addMethod("passwordcheck",
	function(value, element, param) {
		password = $("#password").val();
		confirmed = $("#confirmPassword").val();

		if (password != confirmed) {
			return false;
		}
		return true;
	});
	
	jQuery.validator.addMethod("ssnCheck", function(value, element, param) {
		$('#ssnExists_error').html('');
		var ssn1 = $("#ssn1").val();
		var ssn2 = $("#ssn2").val();
		var ssn3 = $("#ssn3").val();
		
		var ssn1Reg = /(?!000|9\d{2})\d{3}/;
		var ssn2Reg = /(?!00)\d{2}/;
		var ssn3Reg = /\d{4}/;
		
		if(!ssn1Reg.test(ssn1) || !ssn2Reg.test(ssn2) || !ssn3Reg.test(ssn3)) {
			return false;
		}
		
		return true;
	});
	
	jQuery.validator.addMethod("ssnMatchCheck", function(value, element, param) {
		
		
		var ssn = $("#ssn1").val() + $("#ssn2").val() + $("#ssn3").val();
		var confirmSsn = $("#confirmSsn1").val() + $("#confirmSsn2").val() + $("#confirmSsn3").val();
	
		if (ssn != confirmSsn) {
			return false;
		}
		
		return true;
	});

	jQuery.validator.addMethod("validEmailCheck", function(value, element, param) {
		password = $("#email").val();
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,25})?$/;
		if (!emailReg.test(password)) {
			return false;
		} else {
			return true;
		}
		return false;
	});

	jQuery.validator.addMethod("validFirstNameCheck", function(value,
			element, param) {
		fName = $("#firstName").val();
		  var asciiReg = /^[a-zA-Z ,.'-]+$/;
		  if( !asciiReg.test(fName ) ) {
			return false;
		  } else {
			return true;
		  }
		return false;
	});	

	jQuery.validator.addMethod("validLastNameCheck", function(value,
			element, param) {
		lName = $("#lastName").val();
		  var asciiReg = /^[a-zA-Z ,.'-]+$/;
		  if( !asciiReg.test(lName ) ) {
			return false;
		  } else {
			return true;
		  }
		return false;
	});
	
	jQuery.validator.addMethod("validSSNCheck", function(value, element, param) {
		ssn = $("#ssn").val();
		var ssnRegEx = /^\d{3}-?\d{2}-?\d{4}$|^XXX-XX-XXXX$/;
		if (!ssnRegEx.test(ssn)) {
			return false;
		} else {
			return true;
		}
		return false;
	});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {	
	
	var birthDate = new Date(value);
	var today = new Date();
	
	var years = (today.getFullYear() - birthDate.getFullYear());

    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
        years--;
    }
    
    if(years >= parseInt(0) && years <= parseInt(104)){
    	return true;
    }
    
    return false;
});
function getCurrentDate(){
	var currentDate = new Date();
   var year = currentDate.getFullYear();
   var currMonth = currentDate.getMonth()+1;
   var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
   var day = currentDate.getDate();

   var today = month + '/' + day + '/' + year ;
   
   return today;
}
	jQuery.validator.addMethod("validatepassword", function(value, element, param) {
		password = $("#password").val();

		//var re = /^[A-Za-z0-9!@#$%^&*()_]{8,20}$/;
		//var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
		//var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
		/* var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/;

		if (!re.test(password)) {
			return false;
		}
		return true; */

		var re = RegExp(($("#passwordPolicyRegex").val()));///(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 
		if (!re.test(password)) {
			return false;
		}
		return true;

	});
	
	jQuery.validator.addMethod("phonecheck", function(value, element, param) {
		 phone1 = $("#phone1").val(); 
		 phone2 = $("#phone2").val(); 
		 phone3 = $("#phone3").val(); 
		var intPhone1= parseInt( phone1);
		if( 
				(phone1 == "" || phone2 == "" || phone3 == "")  || 
				(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
				(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)
		  ){ 
			return false; 
		}else if(intPhone1<100){
			return false;
		}
		var phone = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
		
		$("#phone").val(phone);
		return true;
	});
	
	
	jQuery.validator.addMethod("phone", function(value, element, param) {
		var phonenumber = $("#phone").val();

		var phoneno = /^\d{10}$/;
		  if(phonenumber.match(phoneno))      {
	          return true;
	        }
		return false;
	});
	
	
	var validator = $("#frmuserreg").validate(
	{
		ignore: ".ignore",
		rules : {
			firstName : {
				required : true,
				validFirstNameCheck : true
			},
			lastName : {
				required : true,
				validLastNameCheck : true
			},
			birthDate : {
				required : true,
				dobcheck: true},
			confirmEmail : {
				required : true,
				confirmEmail : true,
				validEmailCheck : true
			},
			email : {
				required : true,
				validEmailCheck : true
			//remote : 'signup/checkEmail'
			},
			userName : {
				required : true,
				userNameCheck : true
			},
			password : {
				required : true,
				validatepassword : true
			},
			confirmPassword : {
				required : true,
				passwordcheck : true
			},
			securityQuestion1 : {
				required : true
			},
			securityQuestion2 : {
				required : true
			},
			securityQuestion3 : {
				required : true
			},
			securityAnswer1 : {
				required : true
			},
			securityAnswer2 : {
				required : true
			},
			securityAnswer3 : {
				required : true
			},
			terms : {
				required : true
			},
			phone: {
				required : true,
				phone:true
			},
			phone3 : {
				phonecheck : true
			},
			ssn3 : {
				ssnCheck : true,
			},
			confirmSsn3 : {
				ssnMatchCheck : true,
			},
			hiddenRecaptcha: {
	            required: function () {
	                if(grecaptcha.getResponse() === '') {
	                	return true;
	                }else {
	                	return false;
	                }
	            }
	        }
			
		},
		messages : {

			firstName : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>",
				validFirstNameCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateFirstNameCheck' javaScriptEscape='true'/></span>"
				
			},
			lastName : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>",
				validLastNameCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateLastNameCheck' javaScriptEscape='true'/></span>"
			},

			ssn : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateSSN' javaScriptEscape='true'/></span>"
			},
			
			birthDate :{
				required :  "<span> <em class='excl'>!</em><spring:message code='indportal.communicationPreferences.dobError2' javaScriptEscape='true'/></span>" , 
				dobcheck:  "<span> <em class='excl'>!</em><spring:message code='indportal.communicationPreferences.dobError1' javaScriptEscape='true'/></span>"
			},
		
			confirmEmail : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmailConfirm' javaScriptEscape='true'/></span>",
				confirmEmail : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmailConfirm' javaScriptEscape='true'/></span>",
				validEmailCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"
			},

			email : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
				validEmailCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"
			//remote : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailCheck' javaScriptEscape='true'/></span>"
			},

			userName : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateUserName' javaScriptEscape='true'/></span>",
				userNameCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateUserNameCheck' javaScriptEscape='true'/></span>"
			},
			password : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validatePassword' javaScriptEscape='true'/></span>",
				validatepassword : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em> "+$("#passwordPolicyErrorMsg").val()+" </span>"
			},
			confirmPassword : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validatePasswordConfirm' javaScriptEscape='true'/></span>",
				passwordcheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validatePasswordMatch' javaScriptEscape='true'/></span>"
			},
			securityQuestion1 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityQuestion' javaScriptEscape='true'/></span>"
			},
			securityQuestion2 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityQuestion' javaScriptEscape='true'/></span>"
			},
			securityQuestion3 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityQuestion' javaScriptEscape='true'/></span>"
			},
			securityAnswer1 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityAnswer' javaScriptEscape='true'/></span>"
			},
			securityAnswer2 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityAnswer' javaScriptEscape='true'/></span>"
			},
			securityAnswer3 : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.selectSecurityAnswer' javaScriptEscape='true'/></span>"
			},
			terms : {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.agreeTermsAndConditions' javaScriptEscape='true'/></span>"
			},
			
			phone: {
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.provideValidPhoneNumber' javaScriptEscape='true'/></span>",
				phone:"<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.provideValidPhoneNumber' javaScriptEscape='true'/></span>"
			},
			phone3 : {
				phonecheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
			},
			ssn3 : {
				ssnCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='indportal.communicationPreferences.ssnError1' javaScriptEscape='true'/> <a href='<c:url value="/preeligibility" />'><spring:message code='indportal.communicationPreferences.ssnError1.1' javaScriptEscape='true'/></a> <spring:message code='indportal.communicationPreferences.ssnError1.2' javaScriptEscape='true'/></span>" 
			},
			confirmSsn3 : {
				ssnMatchCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='indportal.communicationPreferences.ssnError2' javaScriptEscape='true'/></span>"
			},
			hiddenRecaptcha: {
		        required: "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please confirm that you are not a robot.</span>"
		    }
			

		},
		onkeyup : false,
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		},

	});
	
	function validateForm() {
	if($("#frmuserreg").validate().form() )
		checkExistingEmail();
	else return false;
	}
	
	function isInvalidCSRFToken(xhr) {
	    var rv = false;
	    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
	    	alert($('Session is invalid').text());
	           rv = true;
	    }
	    return rv;
	}

	function checkExistingEmail() {
		var validateUrl = theUrltocheckEmail;
		$("#submitbtn").attr("disabled", true);
		var csrftoken = '${sessionScope.csrftoken}';
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				email : $("#email").val(),
				csrftoken : csrftoken
			},
			success : function(response) {
				if(isInvalidCSRFToken(response)){
					$("#submitbtn").attr("disabled", false);
					return; 
				}                                  
	                
				
				if (!response) {
					error = "<label class='error' generated='true'><span> <em class='excl'>!</em><spring:message code='label.validateEmailCheckID' javaScriptEscape='true'/></span></label>";
					$('#email_error').html(error);
					$("#submitbtn").attr("disabled", false);
					return false;
				} else {
					if($("#ssn").length > 0){
						checkSSNAndBirthDate();
					}else{
						checkUserPassword();
					}
				}
			},

		}); 
	}
	
function checkSSNAndBirthDate() {
		var validateUrl = theurlforCheckSSNAndBirthDate;
		$("#submitbtn").attr("disabled", true);
		var csrftoken = '${sessionScope.csrftoken}';
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				ssn : $('#ssn1').val() + $('#ssn2').val() + $('#ssn3').val(),
				dob : $("#birthDate").val(),
				csrftoken : csrftoken
			},
			success : function(response) {
				if(isInvalidCSRFToken(response)){
					$("#submitbtn").attr("disabled", false);
					return; 
				}                                  
	                
				
				if (response !== ''){
					var error = '';
					
					if(response === 'Household exists') {
						error = '<spring:message code="indportal.signup.householdExistsError1"/> <a href="<c:url value="/account/user/login"/>" target="_blank"><spring:message code="indportal.signup.householdExistsError2"/></a> <spring:message code="indportal.signup.householdExistsError3"/>';
					}else {
						error = response;
					}
															
					$('#ssnExists_error').html('<label for="ssn3" class="error" aria-invalid="true"><span><em class="excl">!<span class="aria-hidden">Error: Invalid Entry </span></em>' + error +'</span></label>');
					$("#submitbtn").attr("disabled", false);
					return;
				} else {
					checkUserPassword();
				}
			},

		}); 
	}
	function checkUserPassword()
	{
		var validateUrl = theurlforCheckUserPassword;
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				password : $("#password").val(),
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true"/>"
			},
			success: function(response)
			{
				if(isInvalidCSRFToken(response)){
					$("#submitbtn").attr("disabled", false);
					return;
				}                                  

				if (response!="")
				{
					$("#password_error").html("<label class='error' style='text-transform:none;'><span> <em class='excl'>!</em>"+response+"</span></label>");
					$("#password_error").attr('class','error help-inline');
					$("#submitbtn").attr("disabled", false);
					return false;
				}
				else{
					 $("#frmuserreg").submit();  
				}
			},
			
		});
	}

	$(document).ready(function() {
		$("#termsDiv :checkbox").click(function() {
			if (this.checked){
				$("#submitbtn").attr("disabled", false);
		    } else {
		    	$("#submitbtn").attr("disabled", true);
		    }
		 });
		
		//hide terms and conditions - HIX-26436
		// HIX-31627
		//hide terms and conditions HIX-33645
		if ('${hideTermsCondition}' == 'true') {
			$('#termsDiv').html('');
			$("#submitbtn").attr("disabled", false);
		}

		$("body").on("keydown", function(event) {
			if (event.which == 13) {
				event.preventDefault();
				$("#submit").click();
			}
		});
		
		$('#birthDate').mask("99/99/9999");
	});
	
	
	function submitForm() {
		var ssn = $('#ssn1').val() + $('#ssn2').val() + $('#ssn3').val();
		$('#ssn').val(ssn);
		$('#securityCode_error').hide();
	}
	
	function reCapchaFilled() {
		$('#hiddenRecaptcha_error, #securityCode_error').hide();
	}
</script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.maskedinput.min.js"/>"></script>
<script>
    $('#securityQuestion1').change(function(){
        $(this).find(':selected').parent().css("height", parseInt($("option").length) * 12);
    });
</script>