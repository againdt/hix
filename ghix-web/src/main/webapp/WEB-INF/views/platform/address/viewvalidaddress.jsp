<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<div class="row-fluid">
	<form class="form-vertical margin0" id="frmviewAddressList" name="frmviewAddressList" action="viewvalidaddress" method="POST" target="_parent">
			<div class="gutter10">
				<div>
					<div style="font-size: 14px; color: red">
					<p><c:out value="This address validation screen is DEPRECATED. Please use NEW ADDRESS VALIDATION."></c:out></p>
					</div>
					
					<c:if test="${enteredLocation != null}">
						<h4 class="lightgray"><spring:message code="label.platform.address.enteredAddress"/></h4>
						<table class="table table-border-none">
							<tbody>
								<tr>
									<td><input type="radio" class="margin0" value="${enteredLocation.address1}, ${enteredLocation.address2}, ${enteredLocation.city}, ${enteredLocation.state}, ${enteredLocation.zip},${enteredLocation.lat},${enteredLocation.lon}" name="addr" id="userdefault"/></td>
									<td class="txt-right">Address 1</td>
									<td><strong>${enteredLocation.address1}</strong></td>
								</tr>
								<tr>
									<td></td>
									<td class="txt-right">Address 2</td>
									<td><strong>${enteredLocation.address2}</strong></td>
								</tr>
								<tr>
									<td></td>
									<td class="txt-right">City</td>
									<td><strong>${enteredLocation.city}</strong></td>
								</tr>
								<tr>
									<td></td>
									<td class="txt-right">State</td>
									<td><strong>${enteredLocation.state}</strong></td>
								</tr>
								<tr>
									<td></td>
									<td class="txt-right">Zip Code</td>
									<td><strong>${enteredLocation.zip}</strong></td>
								</tr>
							</tbody>
						</table>
					</c:if>
				</div>
					<h4 class="lightgray"><spring:message code="label.platform.address.likelyMatch"/></h4> 
				<div class="row-fluid">
					<div id="validAddressList">
						<input type="hidden" name="id" id="id" value="">
						<c:if test="${fn:length(validAddressList) > 0}">
							<c:forEach items="${validAddressList}" var="address" begin="0" varStatus="rowCounter">
								<table class="table table-border-none">
									<tbody>
										<tr>
											<td><input type="radio" class="margin0" value="${address.address1},${address.address2},${address.city},${address.state},${address.zip},${address.lat},${address.lon}" name="addr" id="${rowCounter.index}"/></td>
											<td class="txt-right">Address 1</td>
											<td><strong>${address.address1}</strong></td>
										</tr>
										<tr>
											<td></td>										
											<td class="txt-right">Address 2</td>
											<td><strong>${address.address2}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right">City</td>
											<td><strong>${address.city}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right">State</td>
											<td><strong>${address.state}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right">Zip Code</td>
											<td><strong>${address.zip}</strong></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
						</c:if>
						<div class="">
							<input name="submitAddr" id="submitAddr" type="button" value='Submit' class="btn btn-primary" />
						</div>
					</div>
				</div>
			</div>
		<input type="hidden" name="ids" id="ids" value="${ids}">
	</form>
</div>
	
<script>
	
	$('#submitAddr').click(function(){
		 var csrfURL = '<c:url value="/platform/address/viewvalidaddress"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" />  </c:param> </c:url>';
		 var  pathURL=csrfURL;
		 $.post(pathURL, {selectedAdrs : $("input:radio:checked").val()}, function(data) {
			
			//alert($('#ids').val());
			if(data=="SUCCESS"){
				var idss = $('#ids').val();
				var retVal=idss.split("~");			

				if ($("input:radio:checked").val() == undefined){
					alert('Please select one address.');
					return false;
				}
				var actualVal=$("input:radio:checked").val().split(",");
				
				parent.document.getElementById(retVal[0]).value=actualVal[0];
				parent.document.getElementById(retVal[1]).value=actualVal[1];
				parent.document.getElementById(retVal[2]).value=actualVal[2];
				parent.document.getElementById(retVal[3]).value=actualVal[3];
				parent.document.getElementById(retVal[4]).value=actualVal[4];
				parent.document.getElementById(retVal[5]).value=actualVal[5];
				parent.document.getElementById(retVal[6]).value=actualVal[6];
	            $("#modalData", top.document).parents('#addressIFrame').find("#iFrameClose").trigger('click');		             
	            parent.document.getElementById(retVal[0]).focus();
			}
			
		}); 
	});
	
</script>
