<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.getinsured.hix.model.Broker"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />


<%
Broker mybroker = (Broker)request.getAttribute("broker");
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();

}
%>
<div class="gutter10">
		<c:choose>
			<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<div class="l-page-breadcrumb hide"></div>
			</c:when>				
			<c:otherwise>	
				<div class="l-page-breadcrumb">
				<!--start page-breadcrumb -->
					<div class="row-fluid">
						<ul class="page-breadcrumb">
							<li><a href="javascript:history.back()">&lt; <spring:message  code="label.back"/></a></li>
							<li><a href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
							<li><spring:message code="label.paymentinfo" /></li>
						</ul>
					</div>
					<!--  end of row-fluid -->
				</div>
			</c:otherwise>
	    </c:choose>
	
	<div class="row-fluid">
		<c:choose>
			<c:when
				test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == 'Incomplete' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<h1 id="skip"><spring:message code="label.newBrokerReg"/></h1>
			</c:when>
			<c:otherwise>
				<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="row-fluid">
		<!-- #sidebar -->      
			<jsp:include page="brokerLeftNavigationMenu.jsp" />
		<div class="span9" id="rightpanel">
		<form class="form-vertical" id="frmviewpaymentinfo" name="frmviewbrokerprofile" action="viewpaymentinfo" method="POST">
				<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.paymentInformation"/></h4>
                    <c:if test="${broker.certificationStatus == 'Certified'}">
                    	<a class="btn btn-small pull-right" href="<c:url value="/broker/paymentinfo"/>"><spring:message  code="label.edit"/></a>
                    </c:if>	
                </div>
				<div class="row-fluid">					
					<div class="span gutter10">
						<table class="table table-border-none table-condensed">
							<tbody>
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.paymentMethod"/></td>
									<td><strong>${paymentMethodsObj.paymentType}</strong></td>
								</tr>
							</tbody>
						</table>
						
						<div class="row-fluid" id="achDetailsDiv">
							<table class="table table-border-none table-condensed">
								<tbody>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.bankName"/></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.bankName}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.routingNumber"/></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.routingNumber}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.bankAcctNumber"/></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.accountNumber}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.nameofaccount"/></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.accountType"/></td>
										<td><strong> <c:if test="${paymentMethodsObj.financialInfo.bankInfo.accountType == 'S'}"> Saving </c:if><c:if test="${paymentMethodsObj.financialInfo.bankInfo.accountType == 'C'}"> Checking </c:if></strong></td>
									</tr>								
								</tbody>
							</table>
						</div>			
						
						<div class="row-fluid" id="paymentAddressDetailsDiv" style="display:none;">
							<table class="table table-border-none">
						       	<tbody>	
									<tr>
										<td class="span4 txt-right" style="vertical-align:top;">Payment Address</td>
										<td><strong>${paymentMethodsObj.financialInfo.address1}
												<c:if test="${paymentMethodsObj.financialInfo.address2 != null && paymentMethodsObj.financialInfo.address2 != \"\"}">
												   <br/>${paymentMethodsObj.financialInfo.address2}
											 	</c:if>
												<br/> ${paymentMethodsObj.financialInfo.city},
													${paymentMethodsObj.financialInfo.state}  ${paymentMethodsObj.financialInfo.zipcode} <br/>
											</strong>
										</td>
									</tr>
								</tbody>
							</table>
						</div>			
					</div>
				</div>
		</form>
		</div>
	</div>		
</div>
	
<script type="text/javascript">

$(document).ready(function() {
	$("#brkPaymentInfo").removeClass("link");
	$("#brkPaymentInfo").addClass("active");
	
	var brkId = ${broker.id};	
	if(brkId != 0) {
		$('#certificationInfo').attr('class', 'done');
		$('#buildProfile').attr('class', 'done');
	} else {
		$('#certificationInfo').attr('class', 'visited');
		$('#buildProfile').attr('class', 'visited');
	};	
	
	//Added this code so that if education grant exists, it displays this block
	var optionVal = '${paymentMethodsObj.paymentType}';    	
	if(optionVal == 'CHECK') {
		$('#achDetailsDiv').hide();
		$('#paymentAddressDetailsDiv').show();
	} else {		
		$('#achDetailsDiv').show();
		$('#paymentAddressDetailsDiv').hide();
	}
		
});


	
</script>
