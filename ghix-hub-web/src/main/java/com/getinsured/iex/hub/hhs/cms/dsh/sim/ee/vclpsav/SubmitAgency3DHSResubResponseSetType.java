
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmitAgency3DHSResubResponseSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitAgency3DHSResubResponseSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}CaseNumber"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitLastName" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitFirstName" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitMiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitBirthDate" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitEntryDate" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitAdmittedToDate" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitAdmittedToText" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitCountryBirthCd" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitCountryCitCd" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}NonCitCoaCode" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}EligStatementCd"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}EligStatementTxt"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}WebServSftwrVer"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}FiveYearBarApplyCode"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}LawfulPresenceVerifiedCode"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}DSHAutoTriggerStepTwo"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}QualifiedNonCitizenCode"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}FiveYearBarMetCode"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}USCitizenCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitAgency3DHSResubResponseSetType", propOrder = {
    "caseNumber",
    "nonCitLastName",
    "nonCitFirstName",
    "nonCitMiddleName",
    "nonCitBirthDate",
    "nonCitEntryDate",
    "nonCitAdmittedToDate",
    "nonCitAdmittedToText",
    "nonCitCountryBirthCd",
    "nonCitCountryCitCd",
    "nonCitCoaCode",
    "eligStatementCd",
    "eligStatementTxt",
    "webServSftwrVer",
    "fiveYearBarApplyCode",
    "lawfulPresenceVerifiedCode",
    "dshAutoTriggerStepTwo",
    "qualifiedNonCitizenCode",
    "fiveYearBarMetCode",
    "usCitizenCode"
})
public class SubmitAgency3DHSResubResponseSetType {

    @XmlElement(name = "CaseNumber", required = true)
    protected String caseNumber;
    @XmlElement(name = "NonCitLastName")
    protected String nonCitLastName;
    @XmlElement(name = "NonCitFirstName")
    protected String nonCitFirstName;
    @XmlElement(name = "NonCitMiddleName")
    protected String nonCitMiddleName;
    @XmlElement(name = "NonCitBirthDate")
    protected XMLGregorianCalendar nonCitBirthDate;
    @XmlElement(name = "NonCitEntryDate")
    protected XMLGregorianCalendar nonCitEntryDate;
    @XmlElement(name = "NonCitAdmittedToDate")
    protected XMLGregorianCalendar nonCitAdmittedToDate;
    @XmlElement(name = "NonCitAdmittedToText")
    protected String nonCitAdmittedToText;
    @XmlElement(name = "NonCitCountryBirthCd")
    protected String nonCitCountryBirthCd;
    @XmlElement(name = "NonCitCountryCitCd")
    protected String nonCitCountryCitCd;
    @XmlElement(name = "NonCitCoaCode")
    protected String nonCitCoaCode;
    @XmlElement(name = "EligStatementCd", required = true)
    protected BigInteger eligStatementCd;
    @XmlElement(name = "EligStatementTxt", required = true)
    protected String eligStatementTxt;
    @XmlElement(name = "WebServSftwrVer", required = true)
    protected String webServSftwrVer;
    @XmlElement(name = "FiveYearBarApplyCode", required = true)
    protected String fiveYearBarApplyCode;
    @XmlElement(name = "LawfulPresenceVerifiedCode", required = true)
    protected String lawfulPresenceVerifiedCode;
    @XmlElement(name = "DSHAutoTriggerStepTwo")
    protected boolean dshAutoTriggerStepTwo;
    @XmlElement(name = "QualifiedNonCitizenCode", required = true)
    protected String qualifiedNonCitizenCode;
    @XmlElement(name = "FiveYearBarMetCode", required = true)
    protected String fiveYearBarMetCode;
    @XmlElement(name = "USCitizenCode", required = true)
    protected String usCitizenCode;

    /**
     * Gets the value of the caseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNumber() {
        return caseNumber;
    }

    /**
     * Sets the value of the caseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNumber(String value) {
        this.caseNumber = value;
    }

    /**
     * Gets the value of the nonCitLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitLastName() {
        return nonCitLastName;
    }

    /**
     * Sets the value of the nonCitLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitLastName(String value) {
        this.nonCitLastName = value;
    }

    /**
     * Gets the value of the nonCitFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitFirstName() {
        return nonCitFirstName;
    }

    /**
     * Sets the value of the nonCitFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitFirstName(String value) {
        this.nonCitFirstName = value;
    }

    /**
     * Gets the value of the nonCitMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitMiddleName() {
        return nonCitMiddleName;
    }

    /**
     * Sets the value of the nonCitMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitMiddleName(String value) {
        this.nonCitMiddleName = value;
    }

    /**
     * Gets the value of the nonCitBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitBirthDate() {
        return nonCitBirthDate;
    }

    /**
     * Sets the value of the nonCitBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitBirthDate(XMLGregorianCalendar value) {
        this.nonCitBirthDate = value;
    }

    /**
     * Gets the value of the nonCitEntryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitEntryDate() {
        return nonCitEntryDate;
    }

    /**
     * Sets the value of the nonCitEntryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitEntryDate(XMLGregorianCalendar value) {
        this.nonCitEntryDate = value;
    }

    /**
     * Gets the value of the nonCitAdmittedToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitAdmittedToDate() {
        return nonCitAdmittedToDate;
    }

    /**
     * Sets the value of the nonCitAdmittedToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitAdmittedToDate(XMLGregorianCalendar value) {
        this.nonCitAdmittedToDate = value;
    }

    /**
     * Gets the value of the nonCitAdmittedToText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitAdmittedToText() {
        return nonCitAdmittedToText;
    }

    /**
     * Sets the value of the nonCitAdmittedToText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitAdmittedToText(String value) {
        this.nonCitAdmittedToText = value;
    }

    /**
     * Gets the value of the nonCitCountryBirthCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCountryBirthCd() {
        return nonCitCountryBirthCd;
    }

    /**
     * Sets the value of the nonCitCountryBirthCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCountryBirthCd(String value) {
        this.nonCitCountryBirthCd = value;
    }

    /**
     * Gets the value of the nonCitCountryCitCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCountryCitCd() {
        return nonCitCountryCitCd;
    }

    /**
     * Sets the value of the nonCitCountryCitCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCountryCitCd(String value) {
        this.nonCitCountryCitCd = value;
    }

    /**
     * Gets the value of the nonCitCoaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCoaCode() {
        return nonCitCoaCode;
    }

    /**
     * Sets the value of the nonCitCoaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCoaCode(String value) {
        this.nonCitCoaCode = value;
    }

    /**
     * Gets the value of the eligStatementCd property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEligStatementCd() {
        return eligStatementCd;
    }

    /**
     * Sets the value of the eligStatementCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEligStatementCd(BigInteger value) {
        this.eligStatementCd = value;
    }

    /**
     * Gets the value of the eligStatementTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligStatementTxt() {
        return eligStatementTxt;
    }

    /**
     * Sets the value of the eligStatementTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligStatementTxt(String value) {
        this.eligStatementTxt = value;
    }

    /**
     * Gets the value of the webServSftwrVer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebServSftwrVer() {
        return webServSftwrVer;
    }

    /**
     * Sets the value of the webServSftwrVer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebServSftwrVer(String value) {
        this.webServSftwrVer = value;
    }

    /**
     * Gets the value of the fiveYearBarApplyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiveYearBarApplyCode() {
        return fiveYearBarApplyCode;
    }

    /**
     * Sets the value of the fiveYearBarApplyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiveYearBarApplyCode(String value) {
        this.fiveYearBarApplyCode = value;
    }

    /**
     * Gets the value of the lawfulPresenceVerifiedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLawfulPresenceVerifiedCode() {
        return lawfulPresenceVerifiedCode;
    }

    /**
     * Sets the value of the lawfulPresenceVerifiedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLawfulPresenceVerifiedCode(String value) {
        this.lawfulPresenceVerifiedCode = value;
    }

    /**
     * Gets the value of the dshAutoTriggerStepTwo property.
     * 
     */
    public boolean isDSHAutoTriggerStepTwo() {
        return dshAutoTriggerStepTwo;
    }

    /**
     * Sets the value of the dshAutoTriggerStepTwo property.
     * 
     */
    public void setDSHAutoTriggerStepTwo(boolean value) {
        this.dshAutoTriggerStepTwo = value;
    }

    /**
     * Gets the value of the qualifiedNonCitizenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifiedNonCitizenCode() {
        return qualifiedNonCitizenCode;
    }

    /**
     * Sets the value of the qualifiedNonCitizenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifiedNonCitizenCode(String value) {
        this.qualifiedNonCitizenCode = value;
    }

    /**
     * Gets the value of the fiveYearBarMetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiveYearBarMetCode() {
        return fiveYearBarMetCode;
    }

    /**
     * Sets the value of the fiveYearBarMetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiveYearBarMetCode(String value) {
        this.fiveYearBarMetCode = value;
    }

    /**
     * Gets the value of the usCitizenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSCitizenCode() {
        return usCitizenCode;
    }

    /**
     * Sets the value of the usCitizenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSCitizenCode(String value) {
        this.usCitizenCode = value;
    }

}
