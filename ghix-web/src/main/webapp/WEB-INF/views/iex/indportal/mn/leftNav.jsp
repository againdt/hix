<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

<%
String currentRoleName = (String) session.getAttribute("userActiveRoleName");
String userActiveRoleLabel =  (String) session.getAttribute("userActiveRoleLabel");
%>
<c:set var="currentRoleName" value="<%=currentRoleName%>" />
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<c:set var="mnsureEnv" value='<%=GhixConstants.MNSURE_ENV%>' />

<div class="span3" id="sidebar">
	<div class="header">
		<h4><spring:message code="indportal.portalhome.mystuff" javaScriptEscape="true"/></h4>
	</div>
	<ul class="nav nav-list">
		<sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_AND_PRINT" domainObject="user">
            <li><a href="javascript:void(0)" ng-click="goDash()"><i class="icon-dashboard"></i> <spring:message code="indportal.portalhome.dashboard" javaScriptEscape="true"/></a></li>
		</sec:accesscontrollist>
		<sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<li><a href="#applications"><i class="icon-info-sign"></i> <spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/></a></li>
        </sec:accesscontrollist>
		<c:if test="${not empty hasMultiHousehold && hasMultiHousehold}">
			<li><a href="#multiHousehold"><i class="icon-check-sign"></i> <spring:message code="indportal.portalhome.myhouseholds" javaScriptEscape="true"/></a></li>
		</c:if>
		<li><a href="#enrollmenthistory"><i class="icon-check-sign"></i> <spring:message code="indportal.portalhome.myenrollments" javaScriptEscape="true"/></a></li>
        <sec:accesscontrollist hasPermission="IND_PORTAL_INBOX" domainObject="user">
			<li><a href="<c:url value="/inbox/home" />"><i class="icon-envelope"></i> <spring:message code="indportal.portalhome.myinbox" javaScriptEscape="true"/></a></li>
		</sec:accesscontrollist>
		<c:if test="${userActiveRoleLabel == 'Individual'}">		
			<li><a href="<c:url value="${mnsureEnv}/CitizenPortal/application.do?CitizenAccountHome" />"><i class="icon-home"></i> <spring:message code="indportal.portalhome.eligibilityHome" javaScriptEscape="true"/></a></li>
		</c:if>
		
	</ul>
</div>