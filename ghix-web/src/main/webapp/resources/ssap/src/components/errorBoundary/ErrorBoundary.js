import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

import { clearState } from '../../actions/actionCreators';

class ErrorBoundary extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            errorFromReact: null,
            errorInfo: null
        };
    }

    componentDidCatch(error, errorInfo) {
        this.setState({
            error: true,
            errorInfo
        });
    }

    render() {
        const { error, children, actions: { clearState } } = this.props;
        const { errorFromReact, errorInfo } = this.state;
        if (errorFromReact) {
            return (
                <Fragment>
                    <h3 style={{color: 'red'}}>
                        App is under construction! Error has occurred!
                        <br/>
                        &emsp; Error is somewhere:
                    </h3>
                    {
                        errorInfo && errorInfo.componentStack
                            .split('in ')
                            .map((line, i) =>
                                (i > 0 && i < 6) ?
                                    (
                                        <p key={i}
                                            style={{
                                                color: 'teal',
                                                paddingLeft: `${1.8 * (i - 1)}em`,
                                                fontSize: '1.4rem'
                                            }}
                                        >
                                            {`in ${line}`}
                                            <br/>
                                        </p>
                                    ) :
                                    i === 6 ?
                                        <h5 key={i}>
                                            <b>
                                                    ... Only 5 lines from Stack is on UI.
                                                <br/>
                                                    &emsp; See Console for detailed information ...
                                            </b>
                                        </h5> :
                                        null
                            )
                    }
                </Fragment>

            );
        }
        if (error.status) {
            return (
                <div style={{minHeight: '640px'}}>
                    <h3 style={{color: 'red'}}>
                        App is under construction! Error has occurred!
                        <br/>
                        &emsp; Error message: {error.errorMessage}
                        &emsp; Error type: {error.errorType}
                    </h3>
                    <Button
                        onClick={clearState}
                    >
                        Refresh State
                    </Button>
                </div>
            );
        }
        return (
            <Fragment>
                {children}
            </Fragment>
        );

    }
}

const mapStateToProps = state => ({
    error: state.error,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        clearState
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorBoundary);