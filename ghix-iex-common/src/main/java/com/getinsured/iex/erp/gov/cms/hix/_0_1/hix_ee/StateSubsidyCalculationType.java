package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A data type for a
 * 				calculation of an Advance Premium Tax Credit (APTC).
 *
 *
 * <p>Java class for APTCCalculationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="APTCCalculationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}APTCMaximumAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StateSubsidyCalculationType", propOrder = {
        "stateSubsidyAmount"
})
public class StateSubsidyCalculationType
        extends ComplexObjectType
{

    @XmlElement(name = "StateSubsidyAmount")
    protected AmountType stateSubsidyAmount;

    /**
     * Gets the value of the aptcMaximumAmount property.
     *
     * @return
     *     possible object is
     *     {@link AmountType }
     *
     */
    public AmountType getStateSubsidyAmount() {
        return stateSubsidyAmount;
    }

    /**
     * Sets the value of the aptcMaximumAmount property.
     *
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *
     */
    public void setStateSubsidyAmount(AmountType value) {
        this.stateSubsidyAmount = value;
    }

}
