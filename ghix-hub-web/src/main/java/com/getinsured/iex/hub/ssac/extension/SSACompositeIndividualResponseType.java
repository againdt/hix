
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * Response for each individual person in the request. 
 *             
 * 
 * <p>Java class for SSACompositeIndividualResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSACompositeIndividualResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonSSNIdentification"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSAResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSACompositeIndividualResponseType", propOrder = {
    "responseMetadata",
    "personSSNIdentification",
    "ssaResponse"
})
public class SSACompositeIndividualResponseType
    extends ComplexObjectType
{

    @XmlElement(name = "ResponseMetadata", required = true)
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "PersonSSNIdentification", required = true)
    protected PersonSSNIdentificationType personSSNIdentification;
    @XmlElement(name = "SSAResponse")
    protected SSAResponseType ssaResponse;

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the personSSNIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link PersonSSNIdentificationType }
     *     
     */
    public PersonSSNIdentificationType getPersonSSNIdentification() {
        return personSSNIdentification;
    }

    /**
     * Sets the value of the personSSNIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonSSNIdentificationType }
     *     
     */
    public void setPersonSSNIdentification(PersonSSNIdentificationType value) {
        this.personSSNIdentification = value;
    }

    /**
     * Gets the value of the ssaResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SSAResponseType }
     *     
     */
    public SSAResponseType getSSAResponse() {
        return ssaResponse;
    }

    /**
     * Sets the value of the ssaResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSAResponseType }
     *     
     */
    public void setSSAResponse(SSAResponseType value) {
        this.ssaResponse = value;
    }

}
