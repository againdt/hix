package com.getinsured.eligibility.ind19.util.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.repository.Ind19SsapApplicationEventRepository;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TSLocalTime;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
/**
 * 
 * @author Sunil Desu
 *
 */
@Service
public class CoverageStartDateServiceImpl implements CoverageStartDateService{

	private static final Logger LOGGER = LoggerFactory.getLogger(CoverageStartDateServiceImpl.class);
	
	public static final int TYPE_0_EVENT = 0;
	public static final int TYPE_1_EVENT = 1;
	public static final int TYPE_2_EVENT = 2;
	public static final int TYPE_3_EVENT = 3;
	public static final int TYPE_4_EVENT = 4;
	
	public static final int FIFTEEN = 15;
    public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	@Autowired
	private Ind19SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	public UserService userService;
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	
	@Autowired private Gson platformGson;
	@Autowired private IndividualPortalUtil individualPortalUtil;
	
	@Override
	public Timestamp computeCoverageStartDate(BigDecimal applicationId,
			Map<String, EnrollmentShopDTO> enrollments,
			boolean isChangePlanFlow, boolean isChangeSEPPlanFlow)
			throws GIException {
		LOGGER.info("Computing coverage start date for the SSAP");
		return computeCoverageDate(applicationId, enrollments, isChangePlanFlow, isChangeSEPPlanFlow,null);
	}

	private Timestamp computeCoverageDate(BigDecimal applicationId, Map<String, EnrollmentShopDTO> enrollments,
			boolean isChangePlanFlow, boolean isChangeSEPPlanFlow,Date enrollmentEffectiveDate) throws GIException {
		Timestamp coverageStarDate = null;
		
		//Get the SSAP Event from SSAP Id
		BigDecimal ssapId = (BigDecimal)applicationId;
		SsapApplicationEvent sepEvent = getSsapApplicationEventById(ssapId.longValue());
		long coverageYear = sepEvent.getSsapApplication().getCoverageYear();
		Timestamp overwrittenCoverageStartDate = sepEvent.getCoverageStartDate();
		if (overwrittenCoverageStartDate != null) {
			return overwrittenCoverageStartDate;
		}
		
		if(isChangePlanFlow || isChangeSEPPlanFlow){
			
			
			if(individualPortalUtil.isInsideOEEnrollmentWindow((int) coverageYear)){
				Date globalOEOverrideCoverageStartDate = getGlobalOEOverrideCoverageStartDate();
				if(null!=globalOEOverrideCoverageStartDate){
					return new Timestamp(globalOEOverrideCoverageStartDate.getTime());
				}
			}
				
			return new Timestamp(computeEffectiveDate(coverageYear).getTime());
		}
		
		//Check Event Type
		String applicationType = sepEvent.getSsapApplication().getApplicationType();
	
		//If its OE - Open Enrollment
		if (StringUtils.equalsIgnoreCase(applicationType, "OE")) {
			LOGGER.debug("Event type is OE");

			
			Date globalOEOverrideCoverageStartDate = getGlobalOEOverrideCoverageStartDate();
			if(null!=globalOEOverrideCoverageStartDate){
				return new Timestamp(globalOEOverrideCoverageStartDate.getTime());
			}
				

			// return coverageStarDate on the basis of 15 day rule if all above
			// condition fails
			coverageStarDate = new Timestamp(computeEffectiveDate(coverageYear).getTime());

		}
		//If its SEP - Special Enrollment Period HIX-60543 For QEP
		else if(StringUtils.equalsIgnoreCase(applicationType, "SEP") ||
				StringUtils.equalsIgnoreCase(applicationType, "QEP")){
			LOGGER.debug("Event type is SEP/QEP");
			
			// Check we are under OE window for the SEP application, If yes return the coverage date based on OEP logic
			if(StringUtils.equalsIgnoreCase(applicationType, "SEP") && individualPortalUtil.isInsideOEEnrollmentWindow((int) coverageYear)){
				Date globalOEOverrideCoverageStartDate = getGlobalOEOverrideCoverageStartDate();
				if(null!=globalOEOverrideCoverageStartDate){
					return new Timestamp(globalOEOverrideCoverageStartDate.getTime());
				}
				return new Timestamp(computeEffectiveDate(coverageYear).getTime());
			}
			coverageStarDate = computeCoverageStartDateForSEP(sepEvent.filteredPreferredSsapApplicantEvents(), coverageYear);
			/*
			 * HIX-56541 CoverageStartDateService- Implement the logic for SEP application
			 */
			// This condition is not applicable for QEP since there is no existing enrollment
			if(StringUtils.equalsIgnoreCase(applicationType, "SEP")){
				
				//Support for HIX-70832
				if(enrollmentEffectiveDate == null && enrollments != null)
				{
					enrollmentEffectiveDate = computeEnrollmentEffectiveDate(enrollments);
				}
				
				if(enrollmentEffectiveDate != null && enrollmentEffectiveDate.after(coverageStarDate)){
					coverageStarDate = new Timestamp(enrollmentEffectiveDate.getTime());
				}
			}
		}
		
		return coverageStarDate;
	}
	
	/**
	 * Support for HIX-70832
	 * Checks if an enrollment is active
	 * 
	 * @author Nikhil Talreja
	 */
	private boolean isEnrollmentActive(EnrollmentShopDTO enrollment){
		
		if(enrollment == null){
			return false;
		}
		
		String enrollmentStatus = enrollment.getEnrollmentStatusLabel();
		if(StringUtils.equalsIgnoreCase(enrollmentStatus, "pending") ||
				StringUtils.equalsIgnoreCase(enrollmentStatus, "enrolled")){
			return true;
		}
		
		//If enrollment is terminated in future it is active
		if(StringUtils.equalsIgnoreCase(enrollmentStatus, "terminated")){
			LocalDate termDate = new LocalDate(enrollment.getCoverageEndDate());
			LocalDate today = TSLocalTime.getLocalDateInstance();
			if(termDate.isAfter(today)){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Support for HIX-70832
	 * Computes enrollment effective date
	 * 
	 * @author Nikhil Talreja
	 */
	private Date computeEnrollmentEffectiveDate(Map<String, EnrollmentShopDTO> enrollments){
		
		Date healthEffectiveDate = null;
		Date dentalEffectiveDate = null;
		EnrollmentShopDTO healthEnrollment = enrollments.get("health");
		EnrollmentShopDTO dentalEnrollment = enrollments.get("dental");
		if(isEnrollmentActive(healthEnrollment)){
			healthEffectiveDate = healthEnrollment.getCoverageStartDate();
		}
		if(isEnrollmentActive(dentalEnrollment)){
			dentalEffectiveDate = dentalEnrollment.getCoverageStartDate();
		}
		Date enrollmentEffectiveDate = null;
		//No Active enrollments
		if(healthEffectiveDate == null && dentalEffectiveDate == null){
			return null;
		}
		//Only dental enrollment is active
		else if(healthEffectiveDate == null){
			enrollmentEffectiveDate = dentalEffectiveDate;
		}
		//Only health enrollment is active
		else if(dentalEffectiveDate == null){
			enrollmentEffectiveDate = healthEffectiveDate;
		}
		//Both type of enrollment active
		else{
			//Choose max of health and dental effective date
			enrollmentEffectiveDate = healthEffectiveDate.compareTo(dentalEffectiveDate) > 0 ? healthEffectiveDate : dentalEffectiveDate;
		}
		
		return enrollmentEffectiveDate;
	}
	
	private SsapApplicationEvent getSsapApplicationEventById(Long applicationId) throws GIException{
			return ssapApplicationEventRepository.findBySsapApplicationId(applicationId);
	}

	private Date computeEffectiveDate(long coverageYear) {
		
		LocalDate effectiveDate = TSLocalTime.getLocalDateInstance();
		LocalDate currentDate = TSLocalTime.getLocalDateInstance();

		// Current Date is on or before 15 of month
		if (currentDate.getDayOfMonth() <= FIFTEEN) {
			effectiveDate = effectiveDate.plusMonths(1);
		}
		// Current Date is after 15 of month
		else {
			effectiveDate = effectiveDate.plusMonths(2);
		}
		
		effectiveDate = effectiveDate.withDayOfMonth(1);
		
		//Minimum coverage start date is 1st Jan of the coverage year
		LocalDate minCovStart = new LocalDate(new Long(coverageYear).intValue(),DateTimeConstants.JANUARY,1);
		if(effectiveDate.isBefore(minCovStart)){
			effectiveDate = minCovStart;
		}
		
		return effectiveDate.toDate();

	}

	
	private Timestamp computeCoverageStartDateForSEP(List<SsapApplicantEvent> applicantEvents, long coverageYear){
		
		LOGGER.info("Computing coverage start date for SEP");
		Timestamp coverageStarDate = null;
		
		Map<Integer, Integer> eventCounts = getSEPEventTypeCount(applicantEvents);
		
		//At least one type 0
		if(eventCounts.get(TYPE_0_EVENT) > 0){
			coverageStarDate = computeCoverageStartDateForEventType0(eventCounts.get(TYPE_0_EVENT), 
					applicantEvents);
		}//At least one type 1
		else if(eventCounts.get(TYPE_1_EVENT) > 0){
			coverageStarDate = computeCoverageStartDateForEventType1(eventCounts.get(TYPE_1_EVENT), 
					applicantEvents);
		}//At least one type 4
		else if(eventCounts.get(TYPE_4_EVENT) > 0){
			coverageStarDate = computeCoverageStartDateForEventType4(eventCounts.get(TYPE_4_EVENT), 
					applicantEvents);
		}
		//At least one type 2
		else if(eventCounts.get(TYPE_2_EVENT) > 0){
			coverageStarDate = computeCoverageStartDateForEventType2();
		}
		//No type 1 or 2
		else{
			if(individualPortalUtil.isInsideOEEnrollmentWindow((int)coverageYear)){
				Date globalOEOverrideCoverageStartDate = getGlobalOEOverrideCoverageStartDate();
				if(null!=globalOEOverrideCoverageStartDate){
					return new Timestamp(globalOEOverrideCoverageStartDate.getTime());
				}
			}
			coverageStarDate =  new Timestamp(computeEffectiveDate(coverageYear).getTime());
		}
		
		return coverageStarDate;
	}

	private Map<Integer, Integer> getSEPEventTypeCount(List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.info("Getting Event counts: for SSAP Applicant events");
		Map<Integer, Integer> eventCounts = null;
		
		if(applicantEvents == null || applicantEvents.isEmpty()){
			return eventCounts;
		}
		int type0Count = 0;
		int type1Count = 0;
		int type2Count = 0;
		int type3Count = 0;
		int type4Count = 0;
		
		for (SsapApplicantEvent event : applicantEvents) {

			SepEvents sepEvent = event.getSepEvents();
			if (sepEvent != null) {
				switch (sepEvent.getType()) {
				case TYPE_0_EVENT:
					type0Count++;
					break;
				case TYPE_1_EVENT:
					type1Count++;
					break;
				case TYPE_2_EVENT:
					type2Count++;
					break;
				case TYPE_3_EVENT:
					type3Count++;
					break;
				case TYPE_4_EVENT:
					type4Count++;
					break;
				default:
					LOGGER.warn("Invalid Event Type");
				}
			}
		}
		eventCounts = new HashMap<Integer, Integer>();
		eventCounts.put(TYPE_0_EVENT, type0Count);
		eventCounts.put(TYPE_1_EVENT, type1Count);
		eventCounts.put(TYPE_2_EVENT, type2Count);
		eventCounts.put(TYPE_3_EVENT, type3Count);
		eventCounts.put(TYPE_4_EVENT, type4Count);
		
		LOGGER.debug("Event counts: " + eventCounts);
		return eventCounts;
	}

	/**
	 * 
	 * @author Sunil Desu
	 * @param typeCount
	 * @param applicantEvents
	 * @return
	 * 
	 */
	private Timestamp computeCoverageStartDateForEventType0(int typeCount, List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.info("Computing coverage start date for Event Type 0");
		Timestamp coverageStartDate = null;
		Timestamp newCoverageStartDate =  new Timestamp(TSDateTime.getInstance().getMillis());
		if(applicantEvents.size()>0){
			coverageStartDate = applicantEvents.get(0).getEventDate();

			Calendar covStart = TSCalendar.getInstance();
			covStart.setTime(coverageStartDate);
			covStart.add(Calendar.DATE, 1);
			
			newCoverageStartDate.setTime(covStart.getTimeInMillis());
			
		}
		
		return newCoverageStartDate;
	}
	
	
	/**
	 * Story HIX-53164 (Coverage date logic) Dashboard - Report Changes, Proceed
	 * to Plan Selection and compute coverage start date for valid event when
	 * change referral is received
	 * 
	 * Sub-task HIX-55450 API to filter event types and compute coverage start date for event type1
	 * 
	 * Computes the coverage start date for type 1 event:
	 * 1. If count is 1, coverage start date is EVENT_DATE for the that applicant event
	 * 2. If count > 1, coverage start date is earliest of the applicant events
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param applicantEvents - List of SSAP Applicant Events
	 * @return Timestamp - Coverage Start Date
	 */
	private Timestamp computeCoverageStartDateForEventType1(int typeCount, List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.info("Computing coverage start date for Event Type 1");
		Timestamp coverageStarDate = null;
		
		//Count is 1
		if(typeCount == 1){
			for(SsapApplicantEvent applicantEvent : applicantEvents){
				SepEvents sepEvent = applicantEvent.getSepEvents();
				if(sepEvent != null && sepEvent.getType() == TYPE_1_EVENT){
					coverageStarDate = applicantEvent.getEventDate();
				}
			}
		}
		else if (typeCount > 1){
			//Sort Events based on event date and return the earliest one
			
			//HIX-66258 - Use only type 1 events
			List<SsapApplicantEvent> type1applicantEvents = new ArrayList<>();
			
			//HIX-64337
			List<SsapApplicantEvent> type1birthEvents = new ArrayList<>();
			
			for(SsapApplicantEvent applicantEvent : applicantEvents){
				SepEvents sepEvent = applicantEvent.getSepEvents();
				if(sepEvent != null && sepEvent.getType() == TYPE_1_EVENT){
					type1applicantEvents.add(applicantEvent);
					if(StringUtils.equalsIgnoreCase(LifeChangeEventConstant.UPDATE_DEPENDENTS_BIRTH, 
							sepEvent.getName())){
						type1birthEvents.add(applicantEvent);
					}
				}
			}
			
			if(!type1birthEvents.isEmpty()){
				Collections.sort(type1birthEvents, new Comparator<SsapApplicantEvent>() {
					@Override
					public int compare(SsapApplicantEvent eventOne,
							SsapApplicantEvent eventTwo) {
						return eventTwo.getEventDate().compareTo(eventOne.getEventDate());
					}
				});
				coverageStarDate = type1birthEvents.get(0).getEventDate();
			}
			else{
				Collections.sort(type1applicantEvents, new Comparator<SsapApplicantEvent>() {
					@Override
					public int compare(SsapApplicantEvent eventOne,
							SsapApplicantEvent eventTwo) {
						return eventOne.getEventDate().compareTo(eventTwo.getEventDate());
					}
				});
				coverageStarDate = type1applicantEvents.get(0).getEventDate();
			}
		}
		
		return coverageStarDate;
	}
	
	/**
	 * HIX-70832
	 * 
	 * Support for type 4 events
	 * 
	 * For type 4 events, the coverage start date is 1st of next month 
	 * from the EARLIEST event date
	 */
	private Timestamp computeCoverageStartDateForEventType4(int typeCount,
			List<SsapApplicantEvent> applicantEvents) {
		
		LOGGER.info("Computing coverage start date for Event Type 4");
		Timestamp coverageStartDate = null;
		Timestamp newCoverageStartDate =  new Timestamp(TSDateTime.getInstance().getMillis());
		//Count is 1
		if(typeCount == 1){
			for(SsapApplicantEvent applicantEvent : applicantEvents){
				SepEvents sepEvent = applicantEvent.getSepEvents();
				if(sepEvent != null && sepEvent.getType() == TYPE_4_EVENT){
					coverageStartDate = applicantEvent.getEventDate();
					Calendar covStart = TSCalendar.getInstance();
					covStart.setTime(coverageStartDate);
					covStart.set(Calendar.HOUR_OF_DAY, 0);
					covStart.set(Calendar.MINUTE,0);
					covStart.set(Calendar.SECOND,0);
					covStart.set(Calendar.MILLISECOND, 0);
					covStart.set(Calendar.DAY_OF_MONTH,1);
					covStart.add(Calendar.MONTH, 1);
					newCoverageStartDate.setTime(covStart.getTimeInMillis());
					return newCoverageStartDate;
				}
			}
		}
		else if (typeCount > 1){
			
			//Sort Events based on event date and use the earliest one
			
			List<SsapApplicantEvent> type4applicantEvents = new ArrayList<>();
			for(SsapApplicantEvent applicantEvent : applicantEvents){
				SepEvents sepEvent = applicantEvent.getSepEvents();
				if(sepEvent != null && sepEvent.getType() == TYPE_4_EVENT){
					type4applicantEvents.add(applicantEvent);
				}
			}
			
			Collections.sort(type4applicantEvents, new Comparator<SsapApplicantEvent>() {
				@Override
				public int compare(SsapApplicantEvent eventOne,
						SsapApplicantEvent eventTwo) {
					return eventOne.getEventDate().compareTo(eventTwo.getEventDate());
				}
			});
			
			coverageStartDate = type4applicantEvents.get(0).getEventDate();
			Calendar covStart = TSCalendar.getInstance();
			covStart.setTime(coverageStartDate);
			covStart.set(Calendar.HOUR_OF_DAY, 0);
			covStart.set(Calendar.MINUTE,0);
			covStart.set(Calendar.SECOND,0);
			covStart.set(Calendar.MILLISECOND, 0);
			covStart.set(Calendar.DAY_OF_MONTH,1);
			covStart.add(Calendar.MONTH, 1);
			newCoverageStartDate.setTime(covStart.getTimeInMillis());
		}
		
		return newCoverageStartDate;
	}
	
	/**
	 * Story HIX-53164 (Coverage date logic) Dashboard - Report Changes, Proceed
	 * to Plan Selection and compute coverage start date for valid event when
	 * change referral is received
	 * 
	 * Sub-task HIX-55451 Compute coverage start date for type 2 events
	 * 
	 * For type 2 event, coverage start date is first of next month
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @return Timestamp - Coverage Start Date
	 */
	private Timestamp computeCoverageStartDateForEventType2(){
		
		LOGGER.info("Computing coverage start date for Event Type 2");
		
		//Coverage date is 1st of next month
		Calendar today = TSCalendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE,0);
		today.set(Calendar.SECOND,0);
		today.set(Calendar.MILLISECOND, 0);
		today.set(Calendar.DAY_OF_MONTH,1);
		today.add(Calendar.MONTH, 1);
		
		return new Timestamp(today.getTimeInMillis());
	}

	
	private Date getGlobalOEOverrideCoverageStartDate() {

		Date globalCoverageStartDate = null;
		String globalConfigCoverageStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_GLOBAL_COVERAGE_DATE);

		if (StringUtils.isNotBlank(globalConfigCoverageStartDate)) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			try {
				globalCoverageStartDate = formatter.parse(globalConfigCoverageStartDate);
			} catch (ParseException e) {
				LOGGER.error("Unable to parse global oe coverage start override date ", e);
			}
		}
		return globalCoverageStartDate;
	}
	
		
	@Override
	public Timestamp computeCoverageStartDateIncludingTermMembers(BigDecimal applicationId,
			boolean isChangePlanFlow, boolean isChangeSEPPlanFlow, Long priorApplicationId) throws GIException
	{
		Date latestActiveEnrollmentStartDate = null;
		if (priorApplicationId != null)
		{
			latestActiveEnrollmentStartDate = getLatestActiveEnrollmentStartDate(priorApplicationId);
		}
		Timestamp coverageStartDate = computeCoverageDate(applicationId, null, isChangePlanFlow, isChangeSEPPlanFlow, latestActiveEnrollmentStartDate);
		BigDecimal ssapId = (BigDecimal)applicationId;
		SsapApplicationEvent sepEvent = getSsapApplicationEventById(ssapId.longValue());
		
		int applicaitonCoverageYear = (int) sepEvent.getSsapApplication().getCoverageYear();
		LocalDate coverageDate = new LocalDate(coverageStartDate.getTime());
		if(coverageDate.getYear() < applicaitonCoverageYear){
			LocalDate minCovStartDate = new LocalDate(applicaitonCoverageYear, DateTimeConstants.JANUARY, 1);
			coverageStartDate =  new Timestamp(minCovStartDate.toDate().getTime());
		}
		
		if(latestActiveEnrollmentStartDate == null)
		{
			return coverageStartDate;
		}
		Timestamp terminationDate = getTerminationDate(sepEvent.filteredPreferredSsapApplicantEvents(), coverageStartDate);
		if(terminationDate != null && terminationDate.after(latestActiveEnrollmentStartDate))
		{
			return terminationDate;
		}
		else
		{
			return coverageStartDate;
		}
	}
	
	private Date getLatestActiveEnrollmentStartDate(Long priorApplicationId) {
		EnrollmentResponse enrolleeResponse = null;
		try {
				AccountUser user = this.userService.getLoggedInUser();
				String userName = user == null ? "exadmin@ghix.com"  : user.getUsername();
				EnrollmentRequest enrollmentRequest =new EnrollmentRequest();
				enrollmentRequest.setSsapApplicationId(priorApplicationId);
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_LATEST_ENRL_EFFECTIVE_DATE_BY_SSAP_ID,
						userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(enrollmentRequest)).getBody();

				if (null != response){
					enrolleeResponse = platformGson.fromJson(response, EnrollmentResponse.class);
					if(enrolleeResponse != null
						&& enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						String benefitEffectiveDate = enrolleeResponse.getBenefitEffectiveDate();
						
						Date covStartDate = null;
						if(!StringUtils.isEmpty(benefitEffectiveDate))
						{
							covStartDate = DateUtil.StringToDate(benefitEffectiveDate, "MM/dd/yyyy");
						}
						return covStartDate;
					}
				}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			throw new GIRuntimeException("Exception occured while fetching enrollment details :");
		}
		return null;
	}

	private Timestamp getTerminationDate(List<SsapApplicantEvent> applicantEvents, Date coverageStartDate) {
		if(applicantEvents == null){
			return new Timestamp(coverageStartDate.getTime());
		}
		Map<Integer, List<Timestamp>> eventTimestamps = getSEPRemoveEventTimestamps(applicantEvents);
		List<Timestamp> type1Timestamps = eventTimestamps.get(TYPE_1_EVENT);
		Calendar terminationDate = TSCalendar.getInstance();
		
		//Check for type 1 event
		if(type1Timestamps != null && !type1Timestamps.isEmpty()){
			List<Timestamp> allEventTimestamps = new ArrayList<Timestamp>();
			allEventTimestamps.addAll(eventTimestamps.get(TYPE_1_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(TYPE_2_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(TYPE_3_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(TYPE_4_EVENT));
			allEventTimestamps.add(new Timestamp(coverageStartDate.getTime()));

			Collections.sort(allEventTimestamps,Collections.reverseOrder());
			terminationDate.setTimeInMillis(allEventTimestamps.get(0).getTime());
			if(terminationDate.getTime().before(new TSDate())){
				terminationDate.add(Calendar.DATE, 1);
			}
			return new Timestamp(terminationDate.getTimeInMillis());
		}
		return null;
	}
	
	private Map<Integer, List<Timestamp>> getSEPRemoveEventTimestamps(List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.debug("Getting Event times for SSAP Applicant events");
		Map<Integer, List<Timestamp>> eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		
		if(applicantEvents == null || applicantEvents.isEmpty()){
			return eventTimestamps;
		}
		
		List<Timestamp> type1EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type2EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type3EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type4EventDates = new ArrayList<Timestamp>(0);
		
		for (SsapApplicantEvent event : applicantEvents) {

			SepEvents sepEvent = event.getSepEvents();
			if (sepEvent != null && StringUtils.equalsIgnoreCase(sepEvent.getChangeType(), "REMOVE")) {
				switch (sepEvent.getType()) {
				case TYPE_1_EVENT:
					type1EventDates.add(event.getEventDate());
					break;
				case TYPE_2_EVENT:
					type2EventDates.add(event.getEventDate());
					break;
				case TYPE_3_EVENT:
					type3EventDates.add(event.getEventDate());
					break;
				case TYPE_4_EVENT:
					type4EventDates.add(event.getEventDate());
					break;
				default:
					LOGGER.warn("Invalid Event Type");
				}
			}
		}
		eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		eventTimestamps.put(TYPE_1_EVENT, type1EventDates);
		eventTimestamps.put(TYPE_2_EVENT, type2EventDates);
		eventTimestamps.put(TYPE_3_EVENT, type3EventDates);
		eventTimestamps.put(TYPE_4_EVENT, type4EventDates);
		
		LOGGER.debug("Event times: " + eventTimestamps);
		return eventTimestamps;
	}
}
