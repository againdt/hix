package com.getinsured.hix.batch.migration.aws.ecm;

import static com.getinsured.hix.dto.platform.ecm.CMISConstants.ZERO;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.CATEGORY_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.DATA_BYTES_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.FILE_NAME_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.SUB_CATEGORY_CANNOT_BE_NULL;
import static com.getinsured.hix.dto.platform.ecm.CMISErrors.UNSUPPORTED_DOCUMENT_TYPE;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.content.dto.Document;
import com.getinsured.content.dto.Document.PropertyKey;
import com.getinsured.hix.batch.migration.aws.ecm.S3DataConfig.MigrationStepEnum;
import com.getinsured.hix.batch.migration.couchbase.ecm.CouchMigrationUtil;
import com.getinsured.hix.batch.migration.couchbase.ecm.CouchbaseECMMigrationDTO;
import com.getinsured.hix.batch.migration.couchbase.ecm.JDBCUtil;
import com.getinsured.hix.batch.migration.couchbase.ecm.Pagination;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.document.service.ContentMicroServiceDocumentClient;
import com.getinsured.hix.platform.ecm.CMISSessionUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class MigrateS3DocumentsTask extends BaseEcmS3MigrationClass {

	private static final String COUCHBASE_DOC_ID_SEPARATOR = "::";

	private static final String ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3 = "Encountered Exception in the process of migrating documents from Couchbase to S3";

	private static final String PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3_COMPLETED_SUCCESSFULLY = "Process of migrating documents from Couchbase to S3 completed successfully";

	private static final String INITIATE_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3 = "Initiate the process of migrating documents from Couchbase to S3";

	private static final Logger LOGGER = LoggerFactory.getLogger(MigrateS3DocumentsTask.class);

	private ContentMicroServiceDocumentClient contentMicroServiceDocumentClient;

	private ContentManagementService ecmService;

	private JdbcTemplate jdbcTemplate;

	private static final String UPLOAD_PATH = "%s/%s/%s";

	private static final String CHARSET_UTF_8 = "; charset=UTF-8";

	private String tenantCode = null;

	@Value("#{configProp['ecm.username']}")
	private String ecmUser;

	@Value("#{configProp['msContentSvcUrl']}")
	private String msContentSvcUrl;

	@Value("#{configProp['mscontent.document.bucketName']}")
	private String bucketName;

	@Value("#{configProp['mscontent.document.baseFolder']}")
	private String baseFolder;

	public MigrateS3DocumentsTask() {
		super(MigrationStepEnum.COPY);
	}

	public void setContentMicroServiceDocumentClient(ContentMicroServiceDocumentClient contentMicroServiceDocumentClient) {
		this.contentMicroServiceDocumentClient = contentMicroServiceDocumentClient;
	}

	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	void startExecution() throws Exception {

		String tenantCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		if (StringUtils.isEmpty(tenantCode)){
			throw new GIRuntimeException("Unable to read stateCode/tenantCode from gi_app_config");
		}

		this.tenantCode = tenantCode;

		Connection con = null;
		try {
			LOGGER.info(INITIATE_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3);
			con = jdbcTemplate.getDataSource().getConnection();
			for (CouchbaseECMMigrationDTO tableDTO : S3DataConfig.migrationTables.values()) {
				LOGGER.info("Migrating documents from Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName());
				int maxPrimaryRecordId = extractMaximumPrimaryRecordId(con, tableDTO.getTableName(),
						tableDTO.getColumnName(), S3DataConfig.MigrationStatusEnum.NOTSTARTED);
				LOGGER.info("Maximum Primary Id for Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName() + " is - " + maxPrimaryRecordId);
				if (maxPrimaryRecordId != 0) {
					startDocumentMigration(con, tableDTO.getTableName(), tableDTO.getColumnName(), maxPrimaryRecordId);
				}
			}
			//handleDuplicateEcm(con);
			LOGGER.info(PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3_COMPLETED_SUCCESSFULLY);
		} catch (Exception e) {
			LOGGER.error(ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3, e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.setAutoCommit(true);
				} catch (Exception e2) {

				}

				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}

	}

	private void startDocumentMigration(Connection con, String tableName, String columnName, int maxPrimaryRecordId) throws Exception {
		Pagination paging = new Pagination(maxPrimaryRecordId);
		List<Map<String, String>> data;
		int iSize;
		while (paging.isNext()) {
			LOGGER.info("Migrate records for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			data = fetchRecordsToMigrate(con, tableName, columnName, paging.getStartRecordNumber(),
					paging.getEndRecordNumber());
			iSize = CouchMigrationUtil.listSize(data);

			if (iSize == 0) {
				continue;
			}
			LOGGER.info(iSize + " - records found for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			migrateDocument(con, tableName, columnName, data);
		}

	}

	private List<Map<String, String>> fetchRecordsToMigrate(Connection con, String tableName, String columnName,
			int startRecordNumber, int endRecordNumber) throws SQLException {
		StringBuilder st = new StringBuilder(50);
		st.append("select MIGRATION_ID, PRIMARY_ID, EXISTING_ECM_ID from ").append(S3DataConfig.S3_MIGRATION)
				.append(" where TABLE_NAME = '").append(tableName).append("' and COLUMN_NAME = '").append(columnName)
				.append("' and MIGRATION_STATUS in ('").append(S3DataConfig.MigrationStatusEnum.NOTSTARTED.name())
				.append("')").append(" and PRIMARY_ID between ").append(startRecordNumber).append(" and ")
				.append(endRecordNumber);

		return JDBCUtil.records(con, st.toString(),
				new String[] { S3DataConfig.MIGRATION_ID, S3DataConfig.PRIMARY_ID, S3DataConfig.EXISTING_ECM_ID });
	}

	private void migrateDocument(Connection con, String tableName, String columnName, List<Map<String, String>> data) throws Exception {

		Stream.of(data).forEach((l) -> {
			l.parallelStream().forEach((m) -> {
				try {
					String s3Id = fetchAndPutInS3(m, tableName, columnName);
					updateMigrationtable(con, s3Id, m, tableName, columnName);
				} catch (Exception e){
					LOGGER.error(ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_COUCHBASE_TO_S3 +" for table: "+tableName+" and column: "+ columnName, e);
				}
			});
		});
	}

	private void updateMigrationtable(Connection con, String s3Id, Map<String, String> map, String tableName,
			String columnName) throws Exception {
		try {
			con.setAutoCommit(false);
			StringBuilder st1 = new StringBuilder(50);
			StringBuilder st2 = new StringBuilder(50);

			if (CouchMigrationUtil.isValidString(s3Id)) {
				st1.append("update ").append(S3DataConfig.S3_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(S3DataConfig.MigrationStatusEnum.S3DONE.name()).append("', S3_ID = '")
						.append(s3Id).append("' where MIGRATION_ID = ").append(map.get(S3DataConfig.MIGRATION_ID));
				st2.append("update ").append(S3DataConfig.S3_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(S3DataConfig.MigrationStatusEnum.S3DONE.name()).append("', S3_ID = '")
						.append(s3Id).append("' where EXISTING_ECM_ID = '")
						.append(map.get(S3DataConfig.EXISTING_ECM_ID)).append("'").append(" and MIGRATION_STATUS = '")
						.append(S3DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("'");
			} else {
				st1.append("update ").append(S3DataConfig.S3_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(S3DataConfig.MigrationStatusEnum.ECMFILENOTFOUND.name()).append("' where MIGRATION_ID = ")
						.append(map.get(S3DataConfig.MIGRATION_ID));
				st2.append("update ").append(S3DataConfig.S3_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(S3DataConfig.MigrationStatusEnum.ECMFILENOTFOUND.name()).append("' where EXISTING_ECM_ID = '")
						.append(map.get(S3DataConfig.EXISTING_ECM_ID)).append("'")
						.append(" and MIGRATION_STATUS = '").append(S3DataConfig.MigrationStatusEnum.DUPLICATE.name())
						.append("'");
			}

			JDBCUtil.executeBatchUpdate(con, new String[] { st1.toString(), st2.toString() });

			con.commit();
			st1 = null;
			st2 = null;
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception e2) {
				/** TODO should we eat the exception */
			}
		}
	}

	private String fetchAndPutInS3(Map<String, String> map, String tableName, String columnName) throws Exception {
		String s3Id = null;
		try {

			String existingEcmId = map.get(S3DataConfig.EXISTING_ECM_ID);

			String parts[] = existingEcmId.split(COUCHBASE_DOC_ID_SEPARATOR);
			String category = null;
			String subCategory = null;

			if(parts!=null && parts.length>1) {
				category = parts[0];
				subCategory = parts[1];
			} else {
				return null; // invalid document ID; ignore and skip
			}

			Content content = ecmService.getContentById(existingEcmId);
			if (content == null || content.getContentId() == null) {
				return null;
			}

			byte[] contentData = null;
			try {
				contentData = ecmService.getContentDataById(existingEcmId);
			} catch (Exception e) {
				// This may happen when doc is found in nonbinary, however, document is missing in binary; so ignore and treat as not found.
				// found some record on phix1dev.
				LOGGER.error("For existingEcmId - " + existingEcmId + " ==> Error from ECM getContentDataById - " + e.getMessage() + " ==> Exception -  " + e.getClass().getName() + "\n"
						+ ExceptionUtils.getFullStackTrace(e));
			}
			if (contentData == null || contentData.length == 0) {
				return null;
			}

			String mimeType = validateIncomingContent(content.getOriginalFileName(), contentData);
			validateCategories(category, subCategory);

			String type = CMISSessionUtil.getType(content.getOriginalFileName());

			Document s3Doc = formDocument(content.getRelativePath(), content.getOriginalFileName(), contentData,
					category, subCategory, type,
					mimeType);

			s3Id = contentMicroServiceDocumentClient.createObject(String.format(UPLOAD_PATH, tenantCode, category, subCategory), s3Doc, true);

		} catch (Exception e) {
			LOGGER.error("==> Error from ECM - " + e.getMessage() + " ==> Exception -  " + e.getClass().getName() + "\n"
					+ ExceptionUtils.getFullStackTrace(e));
			/** Eat the Exception to treat as file not found */
			if (!isFileNotFoundException(e)) {
				LOGGER.info("Exiting Step since not a fileNotFoundException - " + e.getMessage() + "\n"
						+ ExceptionUtils.getFullStackTrace(e));
				throw e;
			}

		}

		return s3Id;
	}

	private boolean isFileNotFoundException(Exception e) {
		return (e instanceof ContentManagementServiceException);
	}


	private Document formDocument(String relativePath, String fileName, byte[] dataBytes, String category,
			String subCategory, String type, String mimeType) {

		HashMap<PropertyKey, String> propertyMap = new HashMap<>();
		propertyMap.put(PropertyKey.CATEGORY, category);
		propertyMap.put(PropertyKey.SUB_CATEGORY, subCategory);
		propertyMap.put(PropertyKey.RELATIVE_PATH, relativePath);
		propertyMap.put(PropertyKey.FILE_NAME, fileName);
		propertyMap.put(PropertyKey.DOC_TYPE, type);
		propertyMap.put(PropertyKey.MODIFIED_BY, ecmUser);
		propertyMap.put(PropertyKey.CREATED_BY, ecmUser);


		Document s3Document = new Document();
		s3Document.setMimeType(mimeType + CHARSET_UTF_8);
		s3Document.setFileSize(dataBytes.length);
		s3Document.setData(dataBytes);
		s3Document.getProperties().putAll(propertyMap);

		return s3Document;
	}


	private String validateIncomingContent(String fileName, byte[] dataBytes) {
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException(FILE_NAME_CANNOT_BE_NULL);
		}

		String mimeType = CMISSessionUtil.getMime(fileName);
		if (StringUtils.isEmpty(mimeType)) {
			throw new IllegalArgumentException(UNSUPPORTED_DOCUMENT_TYPE);
		}

		if (dataBytes == null || dataBytes.length <= ZERO) {
			throw new IllegalArgumentException(DATA_BYTES_CANNOT_BE_NULL);
		}
		return mimeType;
	}

	private void validateCategories(String category, String subCategory) {

		if (StringUtils.isEmpty(category)) {
			throw new IllegalArgumentException(CATEGORY_CANNOT_BE_NULL);
		}

		if (StringUtils.isEmpty(subCategory)) {
			throw new IllegalArgumentException(SUB_CATEGORY_CANNOT_BE_NULL);
		}

	}
}
