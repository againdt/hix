import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// ActionCreators

import { changePage } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../normalizeNameOfPerson';


const HyperButtons = props => {
    const {
        adjustedFlow,
        data,
        data: {
            singleStreamlinedApplication: {
                ssapApplicationId
            }
        },
        page
    } = props;

    const [ isIdsShows, toggleIdsVisibility ] = useState(false);

    //console.log(adjustedFlow);

    const handleClick = e => {
        const {
            name
        } = e.currentTarget;

        props.actions.changePage(+name);
    };

    const handleIdsVisibilityClick = () => {
        toggleIdsVisibility(isIdsShows => !isIdsShows);
    };

    const getReasons = reasons => {
        return reasons.map((reason, i) => (
            <div key={`${reason}_${i}`}>
                {`${reason};`}
            </div>
        ));
    };

    const getIds = () => {
        return data
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember.map(hhm => `${hhm.applicantGuid} (${normalizeNameOfPerson(hhm.name)})`).toLocaleString();
    };

    const getComponentName = component => {
        return component.props.name ?
            `<${component.props.name}/>` :
            typeof component.type === 'string' ?
                `<${component.type}/>` :
                `<${component.type.WrappedComponent.defaultProps.name}/>`;
    };

    return (
        <div
            className='usa-width-five-sixths'
            style={{
                padding: '7px 0px'
            }}
        >
            {
                adjustedFlow.map((currentPage, i) => {
                    return (
                        <Fragment key={i}>
                            <button
                                className='hyper-btn'
                                name={i}
                                onClick={handleClick}
                                style={{
                                    width: '20px',
                                    padding: '3px',
                                    border: `1px solid ${page === i ? 'red' : 'white'}`,
                                    backgroundColor: currentPage.isActive ? '#00a6d2' : 'black',
                                    outline: 'none',
                                    fontSize: '1.2rem',
                                }}
                            >
                                {i}
                                {
                                    currentPage.hhm !== -1 &&

                                    <Fragment>
                                        <hr/>
                                        { currentPage.hhm }
                                    </Fragment>
                                }
                                <div className="paginator-btn-tooltip">
                                    <div style={{
                                        color: 'darkred',
                                        fontWeight: 'bold'
                                    }}>
                                        { currentPage.page.navBarText }
                                    </div>
                                    <div style={{
                                        color: 'darkred',
                                        fontWeight: 'bold'
                                    }}>
                                        { getComponentName(currentPage.page.component) }
                                    </div>
                                    {
                                        currentPage.reasons.length !== 0 ?
                                            getReasons(currentPage.reasons) :
                                            null

                                    }
                                    <span className='explanation'/>
                                </div>
                            </button>
                        </Fragment>


                    );
                })
            }
            <div>
                <button className="btn-small" onClick={handleIdsVisibilityClick}>
                    Toggle Ids Visibility
                </button>
                {
                    isIdsShows &&

                    <Fragment>
                        <p>
                            ApplicantsGuids: &nbsp;
                            <strong>
                                {
                                    getIds()
                                }
                            </strong>
                        </p>
                        <p>
                            SsapApplicationId: &nbsp;
                            <strong>
                                {
                                    ssapApplicationId
                                }
                            </strong>
                        </p>
                    </Fragment>
                }
            </div>

        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changePage
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(HyperButtons);
