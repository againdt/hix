package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for aka (also known as)
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class AkaValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AkaValidator.class);
	
	/** 
	 * Validations for aka
	 * String between 1-40 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String aka = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			aka = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(aka.length() < VLPServiceConstants.AKA_MIN_LEN
				|| aka.length() > VLPServiceConstants.AKA_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.AKA_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
