<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

<%@page contentType="text/html" import="java.util.*" %>
<%@ page isELIgnored="false"%>
<link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<%-- <script type="text/javascript" src="<c:url value="/resources/entity/js/vendor/jquery-1.9.1.min.js" />"></script> --%>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>	

  <script type="text/javascript">
  $(document).ready(function(){
	  var fileCheck='${sizeExceed}';
	  if(fileCheck =="1"){
		  $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4>Please Select a File with Size Less than 5 MB</h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});  
	  }
	  var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });
    
  </script>
<!-- <script>$(function(){
   var data = {items: [
{value: "1", clientsServed: "Employer"},
{value: "2", clientsServed: "Individual"}
]};
   
$("#clientsServed").autoSuggest(data.items, {selectedItemProp: "clientsServed",selectedValuesProp:"clientsServed", searchObjProps: "clientsServed"});
})
	</script> -->
	
<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
           String.prototype.trim = function() {
           	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
           }
	}
}
function removecomma(objName)
{
	//$("#clientsServed").onfocus();
  var lstLetters = objName;
  str = String(lstLetters);
  ie8Trim();
  str = str.trim();
 // alert(str);
  var temp = "";
  var temp1 = "";
   temp1 = str.substring(str.length-1, str.length);

  if(temp1 == ",")
	  {
  temp = str.substring(0, str.length-1);
	  }
  
  else
	  {
	  	
	  temp = str;
	  }


//  alert(temp);
  return temp;
}

$(document).ready(function() {
	//CA changes
	$('.dropdown-toggle').dropdown();
	$("#brkProfile").removeClass("link");
	$("#brkProfile").addClass("active");
	
	$("a[title='Account']").parent().addClass("active");
	
	var brkId = ${broker.id};
	//var brkId = '<c:out value="${broker.id}"/>';
	if(brkId != 0) {
		$('#certificationInfo').attr('class', 'done');
		$('#buildProfile').attr('class', 'done');
	} else {
		$('#certificationInfo').attr('class', 'visited');
		$('#buildProfile').attr('class', 'visited');
	};
	
	//var paymentid = ${paymentId};
	var paymentid =  '<c:out value="${message}"/>';
	if (paymentid != 0) {
		$('#paymentinfo').attr('class', 'done');			
	} else {
		$('#paymentinfo').attr('class', 'visited');			
	};
});

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}
//Function for autocompte of spoken languages
$(document).ready(function() {
    $( "#languagesSpoken").autocomplete({
        source: function (request, response) {
            $.getJSON("${pageContext. request. contextPath}/getLanguageSpokenList", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
     
});

jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
	if(value == '') {
		return true;
	}
	checkLanguageSpoken();
	return $("#languagesSpokenCheck").val() == 1 ? false : true;
});

function checkLanguageSpoken(){
	var pathURL = '<c:url value="/broker/buildProfile/checkLanguagesSpoken"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" />  </c:param> </c:url>'+'&languagesSpoken='+$("#languagesSpoken").val();
	$.post(pathURL,
             function(response){
                    if(response == true){
                            $("#languagesSpokenCheck").val(0);
                            
                    }
                    else{
                            $("#languagesSpokenCheck").val(1);
                    }
                    
                return true;
            }
        );
	}
</script>

<div class="gutter10">
<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	<c:choose>
			<c:when test="${broker.certificationStatus != 'Certified'}">
				<div class="l-page-breadcrumb hide"></div>
			</c:when>				
			<c:otherwise>
				<div class="l-page-breadcrumb">
					<!--start page-breadcrumb -->
					<div class="row-fluid">
						<ul class="page-breadcrumb">
							<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
							<li><a
								href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
							<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
						</ul>
					</div>
					<!--  end of row-fluid -->
				</div>
			</c:otherwise>
	</c:choose>
	<div class="row-fluid">
		<h1 class="break-word">
		<c:choose>
			<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
				${broker.user.firstName}&nbsp;${broker.user.lastName}
			</c:when>
			<c:otherwise>
				${broker.firstName}&nbsp;${broker.lastName}
			</c:otherwise>
		</c:choose>
		</h1> 
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<div class="header"></div>
				<ul class="nav nav-list">						
					<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
					<li class="active"><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
					<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>	
					<c:if test="${CA_STATE_CODE}">
						<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
					</c:if>
					<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
					<c:if test="${CA_STATE_CODE}">
						<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
					</c:if>
					<c:if test="${!CA_STATE_CODE && broker.user != null}">	
					<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
					<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	 --%>
					</c:if>
				</ul>		
		</div><!-- end of span3 -->
		<div class="span9">
			<div class="header"><h4 class="pull-left"><spring:message code="label.profile"/></h4>
					<a class="btn btn-primary btn-small pull-right" type="button" href="#" onClick="document.getElementById('saveAgentProfile').click()"><spring:message code="label.saveBtn"/></a>
					<a class="btn btn-small pull-right margin5-r" type="button" href='<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>'><spring:message code="label.brkCancel"/></a>
			</div> 
				
				
			<form class="form-horizontal gutter10" id="frmbrokerProfile" name="frmbrokerProfile" enctype="multipart/form-data"  action="/hix/admin/broker/updateprofile/${encBrokerId}" method="POST">
			<df:csrfToken/>
				<div class="control-group">
				<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
				<table class="table table-border-none span9">
							<tbody>
								<tr>
									<td class="span2 text-right">
				                        <img id="brokerPhoto" src="${photoUrl}" alt="Agent photo thumbnail" class="profilephoto thumbnail" />
									</td>	
									<td>
										<spring:message code="label.photoCaption"/>
									</td>
								</tr>
							</tbody>
				</table>					
				</div>
				<div class="control-group">
					<label for="brkChangephoto" class="control-label required"><spring:message code="label.brkChangephoto"/></label>
					<div class="controls">
						<input type="file" class="input-file" id="fileInputPhoto" name="fileInputPhoto">
						<input type="hidden"  id="fileInputPhoto_Size" name="fileInputPhoto_Size" value="1">		                	
                  		<input type="submit" id="btn_UploadPhoto" name="btn_UploadPhoto" value="<spring:message code="label.brokerupload"/>"  class="btn"  />
                  		<%-- <a class="btn" href="<c:url value="/admin/broker/uploadphoto/${encBrokerId}"/>" style="margin-top: 0;" value="<spring:message code="label.upload"/>">Upload</a> --%>
                  		
                  		<div>
                  			<spring:message code="label.uploadCaption"/>
                  		</div>                 	                  		
					</div>	<!-- end of controls -->
					<div id="fileInputPhoto_error"></div>
				</div><!-- end of  control-group -->
								
				
				
				<div class="control-group">
					<label for="brkName" class="control-label required"><spring:message code="label.brkBrokerName"/></label>
					<div class="controls">
						<c:choose>
							<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
								<input type="text" name="brkName" id="brkName" value="${broker.user.firstName} ${broker.user.lastName}" readonly class="input-xlarge" size="30" /> 
							</c:when>
							<c:otherwise>
								<input type="text" name="brkName" id="brkName" value="${broker.firstName} ${broker.lastName}" readonly class="input-xlarge" size="30" /> 
							</c:otherwise>
						</c:choose>
						<div id="brkName_error"></div>
					</div>	<!-- end of controls -->
				</div><!-- end of  control-group -->		
				
			
				<h4 class="lightgray"><spring:message code="label.brkBusinessAddress"/></h4>
						<input type="hidden" id="lat" name="location.lat" value="${broker.location.lat}">
						<input type="hidden" id="lon" name="location.lon" value="${broker.location.lon}">
						<input type="hidden" id="rdi" name="location.rdi" value="${broker.location.rdi}">
						<input type="hidden" id="county" name="location.county" value="${broker.location.county}">
						
						<div class="control-group">
						<label for="address1" class="control-label required"><spring:message code="label.brkAddress1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" <c:if test="${not empty broker.agencyId}">readonly="readonly" </c:if> placeholder="<spring:message code="label.brkAddressLine1"/>" name="location.address1" id="address1" value="${broker.location.address1}" class="input-xlarge" size="30" maxlength="25" />
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brkAddressLine1"/>" name="location.address1" id="address1" value="${broker.location.address1}" class="input-xlarge" size="30" />
								</c:otherwise>
							</c:choose>	
							<div id="address1_error"></div>
							<input type="hidden" id="address1_hidden" name="address1_hidden" value="${broker.location.address1}">
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
					
					<%-- <input type="hidden" name="location.lat" id="lat" value="${broker.location.lat != null ? broker.location.lat : 0.0}" />
					<input type="hidden" name="location.lon" id="lon" value="${broker.location.lon != null ? broker.location.lon : 0.0}" />	 --%>
					
					<div class="control-group">
						<label for="address2" class="control-label"><spring:message code="label.brkAddress2"/></label>
						<div class="controls">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" <c:if test="${not empty broker.agencyId}">readonly="readonly" </c:if> placeholder="<spring:message code="label.brk.placeholder.address2Business"/>" name="location.address2" id="address2" value="${broker.location.address2}" class="input-xlarge" size="30" maxlength="25" />
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brk.placeholder.address2Business"/>" name="location.address2" id="address2" value="${broker.location.address2}" class="input-xlarge" size="30" />
								</c:otherwise>
							</c:choose>	
							<input type="hidden" id="address2_hidden" name="address2_hidden" value="${broker.location.address2}">
						<div id="address2_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
					

					<div class="control-group">
						<label for="city" class="control-label"><spring:message code="label.brkCity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" <c:if test="${not empty broker.agencyId}">readonly="readonly" </c:if> placeholder="<spring:message code="label.brk.placeholder.cityBusiness"/>" name="location.city" id="city" value="${broker.location.city}" class="input-xlarge" size="30" maxlength="30" />
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brk.placeholder.cityBusiness"/>" name="location.city" id="city" value="${broker.location.city}" class="input-xlarge" size="30" />
								</c:otherwise>
							</c:choose>	
							
							<input type="hidden" id="city_hidden" name="city_hidden" value="${broker.location.city}">
							<div id="city_error"></div>
						</div><!-- .controls -->
					</div>

					<div class="control-group">
						<label for="state" class="control-label"><spring:message code="label.brkState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="" height="" alt="Required!" /></label>
						<div class="controls">
							<select <c:if test="${not empty broker.agencyId}">readonly="readonly" </c:if> size="1" id="state" name="location.state" path="statelist" class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option <c:if test="${state.code == broker.location.state}"> SELECTED </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_hidden" name="state_hidden" value="${broker.location.state}">
							<div id="state_error"></div>
						</div>	<!-- end of controls-->
					</div>

					<div class="control-group">
						<label for="zip" class="control-label"><spring:message code="label.brkZip"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="" height="" alt="Required!" /></label>
						<div class="controls">
							<input type="text" placeholder="" <c:if test="${not empty broker.agencyId}">readonly="readonly" disabled="disabled" </c:if> name="location.zip" id="zip" value="${broker.location.zip}" class="input-small" maxlength="5"
							 onblur="validateAddress(zip);"/>
							<input type="hidden" id="zip_hidden" name="zip_hidden" value="${broker.location.zip}">	
							<div id="zip_error"></div>
						</div><!-- end of controls-->
					</div>	<!-- end of control-group -->
					
					<div class="control-group">
						<label for="email" class="control-label required"><spring:message  code="label.brkEmail" /><img src="/hix/resources/img/requiredAsterisk.png" alt="Required!" aria-hidden="true"></label>
						<div class="controls">
							<input type="text" name="email" id="email" value='${broker.yourPublicEmail}' />
							<div id="email_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					
					<fieldset  class="edit-profile-fieldset">
						<legend class="customLegend control-label"><spring:message code="label.brkClientsServed"/><a class="brktip" rel="tooltip" href="#" alt="tooltip to view profile" data-original-title="<spring:message code="label.brkClientsServedtooltip"/>">&nbsp;<i class="icon-question-sign"></i></a></legend>
<!-- 						<div class="control-group"> -->
<!-- 							<div class="control-label"> -->
<!-- 							</div> -->
	<!-- 						<label for="clientsServed" class="control-label required">Clients Served <a class="brktip" rel="tooltip" href="#" data-original-title="This field allows you to control which users will be able to view your profile. For example, if you leave &#34;individuals&#34; unchecked, only employers will be able to find and view your profile."><i class="icon-question-sign"></i></a></label> -->
							<div class="controls"> 
	 							<label class="checkbox" for="individualFamilies">
	 								<input id="individualFamilies" type="checkbox" name="clientsServed" value="Individuals &#47; Families" ${fn:contains(broker.clientsServed,'Individuals') ? 'checked="checked"' : ''} />  <spring:message code="label.broker.IndividualFamily"/>
	 							</label> 
	 							<label class="checkbox" for="employers"> 
	 								<input id="employers" type="checkbox" name="clientsServed" value="Employers" ${fn:contains(broker.clientsServed,'Employers') ? 'checked="checked"' : ''} />  <spring:message code="label.employers"/>
	 							</label>
								<div id="clientsServed_error"></div>
							</div>	<!-- end of controls-->
<!-- 						</div>end of control-group -->
					</fieldset>
						
		
					<div class="control-group">
						<label for="languagesSpoken" class="control-label required"><spring:message  code="label.brkLanguagesSpoken"/></label>
						<div class="controls">
								<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="languagesSpoken" name="languagesSpoken"  class="chosen-select" multiple style="width:315px;"  ></select>
							<div id="languagesSpoken_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->
					
					<fieldset  class="edit-profile-fieldset">
						<legend class="customLegend control-label"><spring:message  code="label.brkProductExpertise"/><a class="brktip" rel="tooltip" href="#" alt="Broker Tip" data-original-title="<spring:message code="label.brkProductExpertise.tooltip"/>">&nbsp;<i class="icon-question-sign"></i></a></legend>
<!-- 						<div class="control-group"> -->
<!-- 							<div class="control-label"> -->
<!-- 							</div> -->
<%-- 							<label for="productExpertise" class="control-label required"><spring:message  code="label.brkProductExpertise"/> <a class="brktip" rel="tooltip" href="#" data-original-title="This field allows you to list the insurance products that you focus on, such as dental, vision, primary health, life, etc."><i class="icon-question-sign"></i></a></label> --%>
							<div class="controls">
								<label class="checkbox" for="health">
									<input id="health" type="checkbox" name="productExpertise" value="Health" ${fn:contains(broker.productExpertise,'Health') ? 'checked="checked"' : ''} /><spring:message code="label.broker.health"/><br/>
						        </label>
						        <label class="checkbox" for="dental">
						            <input id="dental" type="checkbox" name="productExpertise" value="Dental" ${fn:contains(broker.productExpertise,'Dental') ? 'checked="checked"' : ''} /><spring:message code="label.broker.dental"/><br/>
						        </label>
						        <label class="checkbox" for="vision">
						            <input type="checkbox" name="productExpertise" value="Vision" ${fn:contains(broker.productExpertise,'Vision') ? 'checked="checked"' : ''} /><spring:message code="label.broker.vision"/><br/>
						        </label>
						        <label class="checkbox" for="life">
						            <input id="life" type="checkbox" name="productExpertise" value="Life" ${fn:contains(broker.productExpertise,'Life') ? 'checked="checked"' : ''} /><spring:message code="label.broker.life"/><br/>
						        </label>
						        <label class="checkbox" for="Medicare">
						            <input id="Medicare" type="checkbox" name="productExpertise" value="Medicare" ${fn:contains(broker.productExpertise,'Medicare') ? 'checked="checked"' : ''} /><spring:message code="label.broker.medicare"/><br/>
						        </label>
						        <label class="checkbox" for="Medi-Cal">
						            <input id="Medi-Cal" type="checkbox" name="productExpertise" value="Medi-Cal" ${fn:contains(broker.productExpertise,'Medi-Cal') ? 'checked="checked"' : ''} /><spring:message code="label.broker.mediCal"/><br/>
						        </label>
						        <label class="checkbox" for="CHIP">
						            <input id="CHIP" type="checkbox" name="productExpertise" value="CHIP"${fn:contains(broker.productExpertise,'CHIP') ? 'checked="checked"' : ''} /><spring:message code="label.broker.chip"/><br/>
								</label>
								<label class="checkbox" for="compensation">
						            <input id="compensation" type="checkbox" name="productExpertise" value="Workers Compensation"${fn:contains(broker.productExpertise,'Workers Compensation') ? 'checked="checked"' : ''} /><spring:message code="label.broker.workersCompensation"/><br/>
								</label>
								<label class="checkbox" for="casualty">
						            <input id="casualty" type="checkbox" name="productExpertise" value="Property/Casualty"${fn:contains(broker.productExpertise,'Property/Casualty') ? 'checked="checked"' : ''} /><spring:message code="label.broker.propertyCasualty"/><br/>
								</label>
								<div id="productExpertise_error"></div>
							</div>	<!-- end of controls-->
<!-- 						</div>end of control-group -->
					</fieldset>

			<!--	<div class="control-group">
						<label for="productExpertise" class="control-label required"><spring:message  code="label.brkProductExpertise"/></label>
						<div class="controls">
							<input type="text" name="productExpertise" id="productExpertise" value="${broker.productExpertise}" class="input-xlarge" size="30" />
							<div id="productExpertise_error"></div>
						</div>	
					</div>	-->

					<%-- <div class="control-group">
						<label for="clientsServed" class="control-label required">Clients Served<label>
						<div class="controls">
							<select id="clientsServed" name="clientsServed">
								<option value="">--</option>
								<option value="Individuals"  <c:if test="${'Individuals' == broker.clientsServed}"> SELECTED </c:if>>Individuals</option>
								<option value="Employers" <c:if test="${'Employers' == broker.education}"> SELECTED </c:if>>Employers</option>
							</select>
							
						</div>
						<div id="clientsServed_error"></div>
					</div>	<!-- end of control-group -->
 --%>
 					<div class="control-group">
						<label for="yourWebSite" class="control-label required"><spring:message  code="label.brkWebsite"/><a class="brktip" rel="tooltip" href="#" alt="Website address" data-original-title="<spring:message code="label.broker.brktip"/>">&nbsp;<i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" id="yourWebSite" class="input-xlarge" name="yourWebSite" size="50" value="${broker.yourWebSite}" />
							<div id="yourWebSite_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->
										
					<div class="control-group">
						<label for="aboutMe" class="control-label required"><spring:message  code="label.aboutYourself"/></label>
						<div class="controls">
							<textarea name="aboutMe" id="aboutMe" class="input-xlarge" rows="3" maxlength="4000">${broker.aboutMe}</textarea>
						<div id="aboutMe_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

			<!-- 	<div class="control-group">
						<label for="training" class="control-label required"><spring:message code="label.brkTraining"/></label>
						<div class="controls">
							<input type="text" name="training" id="training" value="${broker.training}" class="input-xlarge" size="30" />
							<div id="training_error"></div>
						</div>
					</div> -->


					
			<!-- <h5 class="alert alert-soft"> Mailing Address </h5>
					<div class="control-group">
						<label for="address1" class="control-label required"> Address1</label>
						<div class="controls">
							<input type="text" name="mailingLocation.address1" id="mailingAddress1" value="${mailingLocationObj.address1}" class="input-xlarge" size="30" />
							<div id="mailingAddress1_error"></div>
						</div>	
					</div>	

					<div class="control-group">
						<label for="address2" class="control-label required"> Address2</label>
						<div class="controls">
							<input type="text" name="mailingLocation.address2" id="mailingAddress2" value="${mailingLocationObj.address2}" class="input-xlarge" size="30" />
							
						</div>	
					</div>	

					<div class="control-group">
						<label for="city" class="control-label required"><spring:message  code="label.brkcity"/></label>
						<div class="controls">
							<input type="text" name="mailingLocation.city" id="mailingCity" value="${mailingLocationObj.city}" class="input-xlarge" size="30" />
							<div id="mailingCity_error"></div>
						</div>	
					</div>	

					<div class="control-group">
						<label for="state" class="control-label required"><spring:message  code="label.brkState"/></label>
						<div class="controls">
							<select size="1"  id="mailingState" name="mailingLocation.state" path="statelist" class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option <c:if test="${state.code == mailingLocationObj.state}"> SELECTED </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<div id="mailingState_error"></div>
						</div>	
					</div>

					<div class="control-group">
						<label for="zip" class="control-label required"><spring:message  code="label.brkZip"/></label>
						<div class="controls">
							<input type="text" name="mailingLocation.zip" id="mailingZip" value="${mailingLocationObj.zip}" class="input-small" maxlength="5" onBlur="viewValidAddressList(this.form.mailingAddress1.value,this.form.mailingAddress2.value,this.form.mailingCity.value,this.form.mailingState.value,this.form.mailingZip.value,'mailingLocation');" />
							<div id="mailingZip_error"></div>
						</div>
					</div>	-->
				
				<!-- Comment Engine Start -->
				
<%-- 				<jsp:include page="../platform/comment/addcomments.jsp"> --%>
<%-- 					<jsp:param value="" name=""/> --%>
<%-- 				</jsp:include> --%>
<!-- 				<input type="hidden" value="BROKER" id="target_name" name="target_name" /> -->
<%-- 				<input type="hidden" value="${encBrokerId}" id="target_id" name="target_id" /> --%>
				
				<!-- Comment Engine end -->
			
				
				
				<div class="form-actions center-align-elements">
						<span>
							<a class="btn cancel-btn" href="<c:url value='/admin/broker/viewprofile/${encBrokerId}'/>" style="margin-top: 0;"><spring:message code="label.brkCancel"/></a>
							<input type="submit" name="saveAgentProfile" id="saveAgentProfile" value="<spring:message code="label.saveBtn"/>" class="btn btn-primary"/>						
						</span>
				</div>
				<input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="1" />	
			</form>
		</div>
		</div><!-- end of span9 -->
	</div>

		
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p><spring:message code="label.brkBrokerEligibilityInfo"/></p>
				</div>
			</div>
		</div><!--  end of .notes -->
	

<script type="text/javascript">
var validator = $("#frmbrokerProfile").validate({ 
	onkeyup: false,
	rules : {
			brkName : {required : true},
			"location.address1" : {required : true,notDefaultText : true},
			"location.address2" : {notDefaultText : true},
			"location.city" : {required : true,notDefaultText : true},
			"location.state" : {required : true},
			"location.zip" : {required : true, number: true,ZipCodecheck:true },
			"mailingLocation.address1" : {required : true},
			"mailingLocation.city" : {required : true},
			"mailingLocation.state" : {required : true},
			"mailingLocation.zip" : {required : true, number: true,MailingZipCodecheck:true },
			email : {
						required : true, 
						email: true 
					},
			phone1 : {phonecheck1 : true},
			phone2 : {phonecheck2 : true},
			phone3 : {phonecheck : true},
			languagesSpoken : {required : false,number:false,languagesSpokenCheck:true},
			education : {required : false},
			fileInputPhoto : {PhotoUploadCheck: true,sizeCheck:true},
			aboutMe : {notDefaultText : true},
			yourWebSite : {checkurl:true}
	},
	messages : {
		brkName : { required : "<span> <em class='excl'>!</em><spring:message code='label.brkvalidateBrokerNameIsRequired'/></span>"},
		phone1: { phonecheck1 : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"},
		phone2: { phonecheck2 : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"},
		phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"},
		"location.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>",
			notDefaultText:"<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		"location.city" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>",
			notDefaultText:"<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"location.address2" : {notDefaultText :"<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		"location.state" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"location.zip" :"<span> <em class='excl'>!</em><spring:message code='label.brkvalidatePleaseentervalidZipCode'/></span>", 
		"mailingLocation.address1" : { required : "<span><em class='excl'>!</em><spring:message code='label.brkvalidatePleaseEnterMailingAddress1'/></span>"},
		"mailingLocation.city" : { required : "<span><em class='excl'>!</em><spring:message code='label.brkvalidatePleaseEnterMailingAddress-City'/></span>"},
		"mailingLocation.state" : { required : "<span><em class='excl'>!</em><spring:message code='label.brkvalidatePleaseSelectMailingAddress-State'/></span>"},
		"mailingLocation.zip" :"<span> <em class='excl'>!</em><spring:message code='label.brkvalidatePleaseentervalidZipCode'/></span>",
		email : {required : "<span><em class='excl'>!</em><spring:message  code='label.validateEmail' javaScriptEscape='true'/></span>"},
		education : { required : "<span> <em class='excl'>!</em><spring:message  code='label.brkPleaseenterEducationDetails' javaScriptEscape='true'/></span>"},
		fileInputPhoto : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
                          sizeCheck :"<span> <em class='excl'>!</em><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/></span>"	},
        aboutMe :{notDefaultText :"<span><em class='excl'>!</em><spring:message  code='label.brkValidAboutYorSelf' javaScriptEscape='true'/></span>"} ,                 
		yourWebSite : {checkurl : "<span><em class='excl'>!</em><spring:message  code='err.validateYourWebSite' javaScriptEscape='true'/></span>"},
				languagesSpoken : {languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.brkValidatePleaseEnterLanguageSpokenFromDropDown'/></span>"}
	},

	errorClass: "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	}
});

jQuery.validator.addMethod("checkurl", function(value, element) {
	//Should validate only if a value has been entered
	if(value == '') {
		return true;
	}
	if((value.indexOf('..') != -1 )|| (value[value.length-1]=='.'))
		return false;
	if(!(value.indexOf('http://') == 0 || value.indexOf('https://') == 0 || value.indexOf('www.') == 0))
	{
		return false;
	}
	iChars="%";
	if(value.indexOf(iChars) != -1){
		return false;
	}
	// now check if valid url
	var regx = "(((https?:[/][/](www.)?)|(www.))([a-z/-]|[A-Z/-]|[0-9/-]|[/.]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	if(value.indexOf('http://www') == 0 || value.indexOf('https://www')==0)
		regx = "(((https?:[/][/])((www)))[.]([a-z/-]|[A-Z/-]|[0-9/-]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	var regexp = new RegExp(regx);
	return regexp.test(value);
	}
);	
$(function(){
	 $('#fileInputPhoto').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInputPhoto_Size").val(0);
			}
			else{
				$("#fileInputPhoto_Size").val(1);	
			} 
			}
			else{
				$("#fileInputPhoto_Size").val(1);	
			} 

		});
	});
jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
	  ie8Trim();
	  if($("#fileInputPhoto_Size").val()==1){
		  return true;
	  }
	  else{
		  return false;
	  }
});

jQuery.validator.addMethod("notDefaultText", function (value, element) {
    var check=value;
    iChars="%";
	if(check.indexOf(iChars) != -1){
		return false;
	}
	if (value != $(element).attr('placeholder')) {
		return true;
	}
	 
	});
jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000"))){ 
		return false; 
	}
	return true;
});
jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#mailingZip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
		return false; 
	}
	return true;
});


function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	ie8Trim();
	phone1 = $("#phone1").val().trim(); 
	phone2 = $("#phone2").val().trim(); 
	phone3 = $("#phone3").val().trim(); 


	if( (phone1 == "" || phone2 == "" || phone3 == "")  || (isNaN(phone1)) || (phone1.length < 3 ) || (isNaN(phone2)) || (phone2.length < 3 ) || (isNaN(phone3)) || (phone3.length < 4 )  ) { return false; }
	return true;
});

jQuery.validator.addMethod("phonecheck1", function(value, element, param) {
	ie8Trim();
	phone1 = $("#phone1").val().trim(); 
	if((phone1.length < 3 ) || (phone1 == "") || (isNaN(phone1))) {
		 	return false;
		}
	return true;
});

jQuery.validator.addMethod("phonecheck2", function(value, element, param) {
	ie8Trim();
	phone2 = $("#phone2").val().trim(); 
	if((phone2.length < 3 ) || (phone2 == "") || (isNaN(phone2)) ){
			return false;
	}
	return true;
});

/* 
$('#frmbrokerProfile').submit(function() {
	alert('Hello');
  	
  	var csValue = $("#clientsServed").val();
  	str = new String(csValue);
  	str = str.trim();
  	str = str.replace(/,*$/,"");
  	$("#clientsServed").val(str);
  	
}); */




//call this function on blur of zip code and if address is valid invoke validateAddress
function validateAddress(e){
	var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
	var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';

	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
		
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	
	viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);
}

jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) { 
	  ie8Trim();
    var file = $('input[type="file"]').val();
    var exts = ['jpg','jpeg','gif','png','bmp'];
    if ( file ) {
      var get_ext = file.split('.');
      get_ext = get_ext.reverse();

      if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
        return true;
      } else {
        return false;
      }
   }
   return true;
});

$('#languagesSpoken').focusout(function() {
    county=$("#languagesSpoken").val().trim();
    var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
        $('#languagesSpoken').val(index);
	 }
	 else{
		  }
 });

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}


$(document).ready(function(){
	$("#btn_UploadPhoto").click(function() {
		var file = $('input[type="file"]').val();
		if(file!=""){
			var file1 = $('input[type="file"]').val();
		    var exts = ['jpg','jpeg','gif','png','bmp'];
		    var get_ext = file1.split('.');
		      get_ext = get_ext.reverse();
			 if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 if(document.getElementById('fileInputPhoto_Size').value==1){		 
		$('#frmbrokerProfile').ajaxSubmit({				
			url: "<c:url value='/broker/uploadphoto/${encBrokerId}'/>",
			dataType:'text',
			success: function(responseText) { 
				if(responseText.indexOf('Missing CSRF token') != -1){
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
					return false;
				}
				if(responseText=="SUCCESS"){
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
					$("#brokerPhoto").attr("src", "<c:url value='/broker/photo/${encBrokerId}?ts=" + ((new Date()).getTime()) + "'/>");								
				}
				else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}			
			},
			error : function(responseText) {
				//alert("Failed to Upload File");	
				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				 }
			});
				 }
		else{
			//alert("Please Select a File Before upload");
             $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
             }
            }
        else{
				//alert("Please Select a File Before upload");
            $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        }		 
         }
        else{
              //alert("Please Select a File Before upload");
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
            }
		
			return false; 
	});	// button click close

/* function hideContentBeforeLoad(){
	$('#crossClose').focus();
	$("#btn_UploadPhoto").removeAttr('disabled');
	$('.modal-header, .modal-body, .modal-footer').show();
	$('.txt-center').remove();
} */

function isInvalidCSRFToken(xhr) {
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}

$('.brktip').tooltip();

function loadUsers() {
		var matched = $("#languagesSpoken option:selected").text();
		var pathURL = '<c:url value="/hix/broker/buildprofile/getlanguage"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" />  </c:param> </c:url>';
		$('#errorMsg').val("");
		$.ajax({
			url : pathURL,
			type : "GET",
			data : {
				matched : matched
			},
			success: function(response){
				 if(isInvalidCSRFToken(xhr)){                    
                     return;
				}
				loadData(response);
    		},
   			error: function(e){
    			alert("Failed to load Languages");
    		},
		});
		
   	}
	
	//load the jquery chosen plugin with the AJAX data
	function populateLanguages() {
		$("#languagesSpoken").html('');
		var respData = $.parseJSON('${languagesList}');
		var languages='${broker.languagesSpoken}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(languages!=null){
			      isSelected = checkLanguages(respData[key]);
		      }
		      if(isSelected){
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    
		$('#languagesSpoken').trigger("liszt:updated");
	}
	 $(document).ready(function() {
		 populateLanguages();
     });
	function checkLanguages(county){
        var languages='${broker.languagesSpoken}';
        var countiesArray=languages.split(',');
        var found = false;
                for (var i = 0; i < countiesArray.length && !found; i++) {
                        var countyTocompare=countiesArray[i].replace(/^\s+|\s+$/g, '');
                       if (countyTocompare.toLowerCase() == county.toLowerCase()) {
                            found = true;
                           
                        }
                    }
              return found;
	}
	

});
	
</script>	