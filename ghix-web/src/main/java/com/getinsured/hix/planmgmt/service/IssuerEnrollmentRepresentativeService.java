package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;

public interface IssuerEnrollmentRepresentativeService {

	/* ##### interface of prepare enrollee search criteria ##### */
	Map<String, Object> createEnrolleeSearchCriteria(HttpServletRequest request, Integer issuerId, boolean doPagination);
	
	
	/* ##### interface of call enrollee search API ##### */
	EnrolleeResponse callSearchEnrolleeAPI(EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO, AccountUser loggedInUser);
	
	
	/* ##### interface of prepare enrollment details API ##### */
	EnrollmentDataDTO callEnrollmentDetailsAPI(Integer enrollmentId, String userName);
	
	
	/* ##### interface of get Year list ##### */
	List<Integer> getYearsList();
	
	
	/* ##### implementation of download/export enrollee search result  ##### */
	void downloadEnrolleeData(HttpServletResponse response, List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList, String year);
	
	EnrollmentSubscriberRequestDTO createEnrollmentSubscriberRequest(HttpServletRequest request, Integer issuerId, boolean doPagination);	
}
