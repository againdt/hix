/**
 * 
 */
package com.getinsured.hix.batch.enrollment.util;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.enrollment.service.EnrlReconDiscrepancyReportService;
import com.getinsured.hix.dto.enrollment.EnrlDiscrepancyReportCsvDTO;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;

/**
 * Enrollment discrepancy report generation Thread
 * @author negi_s
 * @since 21/03/2017
 */
public class EnrlReconDiscrepancyReportThread implements Callable<List<EnrlDiscrepancyReportCsvDTO>> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportThread.class);
	
	private List<EnrlReconDiscrepancy> discrepancyList;
	private  String reconFileName;
	private int fileId;
	private EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService;
	
	/**
	 * @param subList
	 * @param reconFileName
	 * @param fileId
	 * @param enrlReconDiscrepancyReportService
	 */
	public EnrlReconDiscrepancyReportThread(List<EnrlReconDiscrepancy> subList, String reconFileName, int fileId,
			EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService) {
		super();
		this.discrepancyList = subList;
		this.reconFileName = reconFileName;
		this.fileId = fileId;
		this.enrlReconDiscrepancyReportService = enrlReconDiscrepancyReportService;
	}


	@Override
	public List<EnrlDiscrepancyReportCsvDTO> call() throws Exception {
		LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" fileId ::" + fileId);
		return enrlReconDiscrepancyReportService.populateDiscrepancyDto(discrepancyList, reconFileName, fileId);
	}
	
}
