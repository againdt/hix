package com.getinsured.eligibility.renewal.service;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplication;



public interface SsapToConsiderService {
	
	boolean processErrorApplications(Long renewalYear,Long batchSize) throws Exception;
	
	String processSsap(SsapApplication currentApplication, Long renewalYear) throws Exception;
	
	RenewalApplication executeToConsiderLogicForRenewal(SsapApplication currentApplication, Long renewalYear, Integer serverName) throws Exception, GIException;
	
	boolean processErrorApplications(Long ssapApplicationId) throws Exception;
}
