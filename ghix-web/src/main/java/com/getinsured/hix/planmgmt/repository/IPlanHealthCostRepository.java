package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanHealthCost;

public interface IPlanHealthCostRepository extends JpaRepository<PlanHealthCost, Integer>{
	@Query("SELECT phc from PlanHealthCost phc WHERE phc.planHealth.id = :planId")
	List<PlanHealthCost> getPlanHealthCostByPlanId(@Param("planId") Integer planId);
}
