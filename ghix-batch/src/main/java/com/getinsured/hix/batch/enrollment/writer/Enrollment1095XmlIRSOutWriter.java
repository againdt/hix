/**
 * 
 */
package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

/**
 * @author parhi_s
 *
 */
public class Enrollment1095XmlIRSOutWriter implements ItemWriter<Integer> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlIRSOutWriter.class);
	
	List<Integer> enrollmentIds;
	int applicableYear;
	int partition;
	private ExecutionContext executionContext;
	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private JobService jobService;
	long jobExecutionId = -1;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		executionContext = stepExecution.getJobExecution().getExecutionContext();
		ExecutionContext ec = stepExecution.getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		if(ec != null){
			applicableYear=ec.getInt("year",0);
			partition =ec.getInt("partition");
		}
		
	}
	
	@Override
	public void write(List<? extends Integer> enrollment1095BatchList) throws Exception {
		LOGGER.info("Inside Enrollment1095XmlIRSOutWriter");
		try{
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			if(enrollment1095BatchList!=null && !enrollment1095BatchList.isEmpty()){
				List<Integer> enrollment1095List= new ArrayList<Integer>(enrollment1095BatchList);
				enrollmentAnnualIrsReportService.sendEnrollment1095sForProcessing(enrollment1095List, applicableYear, partition);
			}
		}catch(Exception e){
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
		}
	}
	
	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}
	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
