package com.getinsured.eligibility.ssap.integration.household.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.iex.ssap.model.SsapApplicant;

@Component(value = "checkVerificationStatusServiceActivator")
public class CheckVerificationStatusServiceActivator {
	
	private static final String YES = "Y";


    private static final Logger LOGGER = Logger.getLogger(CheckVerificationStatusServiceActivator.class);

    @Value("#{configProp['accountTransferMaxRetryAttempts'] != null ? configProp['accountTransferMaxRetryAttempts'] : 10}")
	private int accountTransferMaxRetryAttempts;
	
	@ServiceActivator
	@SuppressWarnings({ "rawtypes", "serial", "unchecked" })
	public String receive(final Message message) {
		LOGGER.debug("In CheckVerificationStatusServiceActivator retry count = " + message.getHeaders().get(SSAPIntegrationConstants.ACCOUNT_TRANSFER_RETRY_COUNT).toString());
		
		int accountTransferCurrentAttempts = Integer.valueOf(message.getHeaders().get(SSAPIntegrationConstants.ACCOUNT_TRANSFER_RETRY_COUNT).toString());
		
		if(accountTransferCurrentAttempts >= accountTransferMaxRetryAttempts) {
			LOGGER.debug("In CheckVerificationStatusServiceActivator max retries exceeded. Forcing account transfer....");
			return SSAPIntegrationConstants.FORCE_ACCOUNT_TRANSFER;
		}
		
		List<SsapApplicant> applicants = null;
		if(message.getPayload() instanceof SsapApplicant) {
			applicants = new ArrayList<SsapApplicant>() {
				{
					add((SsapApplicant)message.getPayload());
				}
			};
		} else {
			applicants = (List<SsapApplicant>) message.getPayload();
		}
		
		for(SsapApplicant applicant : applicants) {
			boolean incarcerationStatus = StringUtils.isNotBlank(applicant.getIncarcerationStatus());
			boolean citizenshipImmigrationStatus = StringUtils.isNotBlank(applicant.getCitizenshipImmigrationStatus());
			boolean ssnVerificationStatus = StringUtils.isNotBlank(applicant.getSsnVerificationStatus());
			boolean mecVerificationStatus = StringUtils.isNotBlank(applicant.getMecVerificationStatus());
			boolean incomeVerificationStatus = StringUtils.isNotBlank(applicant.getIncomeVerificationStatus());
			boolean vlpVerificationStatus = StringUtils.isNotBlank(applicant.getVlpVerificationStatus());
			
			if(StringUtils.isNotBlank(applicant.getApplyingForCoverage()) && applicant.getApplyingForCoverage().equals(YES) && 
			        incarcerationStatus && ssnVerificationStatus && mecVerificationStatus && incomeVerificationStatus && 
			        (vlpVerificationStatus || citizenshipImmigrationStatus)) {
				return SSAPIntegrationConstants.VERIFICATION_PENDING;
			}
		}
		return SSAPIntegrationConstants.VERIFICATION_COMPLETE;
	}
	
}
