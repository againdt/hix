package com.getinsured.iex.dto;

public class PortalResponse {
    private String status;
    private String errorMessage;
    private String existingAppId;
    private String clonedAppId;
    private String clonedAppCaseNumber;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getExistingAppId() {
		return existingAppId;
	}

	public void setExistingAppId(String existingAppId) {
		this.existingAppId = existingAppId;
	}

	public String getClonedAppId() {
		return clonedAppId;
	}

	public void setClonedAppId(String clonedAppId) {
		this.clonedAppId = clonedAppId;
	}

	public String getClonedAppCaseNumber() {
		return clonedAppCaseNumber;
	}

	public void setClonedAppCaseNumber(String clonedAppCaseNumber) {
		this.clonedAppCaseNumber = clonedAppCaseNumber;
	}

}
