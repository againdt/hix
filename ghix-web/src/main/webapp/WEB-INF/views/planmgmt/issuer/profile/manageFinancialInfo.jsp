<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page isELIgnored="false"%>
<%
	Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
%>
<!-- start of secondary navbar -->

<ul class="nav nav-pills">
	<li><a href="#"><spring:message code="label.accountInfo"/></a></li>
	<li><a href="managerepresentative"><spring:message code="label.authRep"/></a></li>
	<li class="active"><a href="viewfinancialinfo"><spring:message code="pgheader.financialInformation"/></a></li>
</ul>
<!-- start of third navbar -->

<div class="row-fluid gutter10">
	<div class="span12">
		<div class="page-header">
			<h1><spring:message code="pgheader.financialInformation"/></h1>

		</div>
		<!-- end of page-header -->

		<div class="span6 gutter10">
			<form class="form-horizontal" action="managefinancialinfo">
				<div class="control-group">
					<label class="control-label" for="inputTin"><spring:message code="label.tin"/></label>
					<div class="controls">
						<input type="text" id="inputTin" placeholder="XXX-XX-XXXX">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputBank"><spring:message code="label.bankName"/></label>
					<div class="controls">
						<input type="text" id="inputBank" placeholder="<spring:message code='label.bankNameVal'/>"> 
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputAccountNumber"> <spring:message code="label.bankAcNumber"/> <small><a href="#" class="account-number"><spring:message code="label.whatIsThis"/></a></small></label>
					<div class="controls">
						 <input type="text" id="inputAccountNumber" placeholder="XXXXXXX7890">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputAccountNumber"> <spring:message code="label.bankRoutingNumber"/> <small><a href="#" class="routing-number"> <spring:message code="label.whatIsThis"/> </a></small></label>
					<div class="controls">
						 <input type="text" id="inputAccountNumber" placeholder="XXXX00497">
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<a href="viewfinancialinfo" class="btn btn-primary"><spring:message code="label.issuerSubmit"/></a>
					</div>
				</div>
			</form>
		</div>

		<div class="span5 account">
			<img src="/hix/resources/images/bank_account_number.png" alt="<spring:message code='label.bankAcNumber'/>" />
		</div>
		<div class="span5 routing">
			<img src="/hix/resources/images/routing_number.png" alt="<spring:message code='label.bankRoutingNumber'/>" />
		</div>
		<!-- assister registration -->
	</div>
	<!-- end of span12 -->

	<script type="text/javascript">
	$(function() {	
		$('.datepick').each(function(){
	    $(this).datepicker({
			showOn: "button",
			buttonImage: "/resources/img/calendar.gif",
			buttonImageOnly: true
		}); 
	});
	});
	
	$(document).ready(function() {
		$('.account, .routing').hide();
		$('.account-number').mouseover(function(){
			$('.account').fadeIn();
			}).mouseout(function(){
				$('.account, .routing').fadeOut();
		});
		
		$('.routing-number').mouseover(function(){
			$('.routing').fadeIn();
			}).mouseout(function(){
				$('.account, .routing').fadeOut();
		});
		
	});
	</script>
</div>
<!--  end of row-fluid -->
