 <%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<body>
		<div id="declinebox gutter10-lr">
			<form class="form-vertical margin0" id="validatedecline"
				name="validatedecline" action="decline" method="POST"
				target="_parent">
				<df:csrfToken/>
				<input type="hidden" name="isEntity" id="isEntity" value="" /> 
				<input type="hidden" name="assisterId" id="assisterId" value="" /> 
				<input type="hidden" name="desigId" id="desigId" value="" /> <input
					type="hidden" name="desigStatus" id="desigStatus" value="" /> <input
					type="hidden" name="fromModule" id="fromModule" value="" />

				<div class="profile">
					<div id="confirmationMessage">
					<c:choose>
							<c:when test="${CA_STATE_CODE}">
						 		<p>Are you sure you want to remove the delegation from ${consumerFirstName} ${consumerLastName}? 
						 		You will lose access to the consumer's case and application information.
								In order to assist this consumer in the future, 
								they will need to access their own account and re-send you a request for assistance. 
								Click Confirm below to remove the delegation and move this case to the Inactive Delegations screen. 
 								</p>  
						 	</c:when>
						 	<c:otherwise>
						<p>Are you sure you want to mark this account as inactive? If you
						confirm, you will no longer have access to this account. </p>
						<p>Should you change your mind in the future, this person will have
						to send you another designation request before you can access
						their account again. </p>
						 	</c:otherwise>
					</c:choose>	
					</div>
					<div class="marginTop20">
					<button name="submitRequest" id="submitRequest" type="button" onClick="declineRequest();return false;" title="Confirm" class="btn btn-primary pull-right" >Confirm</button>
					<button class="btn pull-left" title="Cancel" type="button"	data-dismiss="modal"  onClick="closeIFrame();" aria-hidden="true">Cancel</button>
					</div>
				</div>
			</form>
		</div>
</body>

		
<script type="text/javascript">
	$(document).ready(function() {
		
		var isEntity = getParameterByName('isEntity');

		if(undefined == isEntity || isEntity === '') {
			isEntity = '${isEntity}';
		}
		
		var assisterId = getParameterByName('assisterId');
		
		if(undefined == assisterId || assisterId === '') {
			assisterId = '${assisterId}';
		}
		
		var desigId = getParameterByName('desigId');

		if(undefined == desigId || desigId === '') {
			desigId = '${desigId}';
		}
		
		var desigName = getParameterByName('desigName');
		var desigStatus = getParameterByName('prevStatus');
		var fromModule = getParameterByName('fromModule');
		
		if(desigId.indexOf(',') !== -1) {
			desigId = '${desigId}';
		}
		
		$('#isEntity').val(isEntity);
		$('#assisterId').val(assisterId);
		$('#desigId').val(desigId);
		$('#desigStatus').val(desigStatus);
		$('#fromModule').val(fromModule);
		//$('#header').html("<h4>" + desigName + "</h4>");
	});

	function declineRequest() {
		$('#submitRequest').attr("disabled","disabled");
		var formAction = null;
		
		if($('#isEntity').val() === 'y') {
			formAction = "<c:url value='/entity/assister/decline/'/>" + $('#assisterId').val() + "/" + $('#desigId').val() + "?prevStatus=" + $('#desigStatus').val();
		}
		else {
			formAction = "<c:url value='/entity/assister/decline/'/>" + $('#desigId').val() + "?prevStatus=" + $('#desigStatus').val();
		}
		
		if($('#desigId').val().indexOf(',') !== -1) {
			formAction = "declineRequests/" + $('#fromModule').val()+ "?ids=" + $('#desigId').val() + "&prevStatus="
					+ $('#desigStatus').val();
		}
		
		$("#validatedecline").attr('action', formAction);
		$("#validatedecline").submit();
	}

	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null)
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	function closeIFrame() {
		$("#imodal").remove();
		parent.location.reload();
		window.parent.closeIFrame();
	}
</script>
