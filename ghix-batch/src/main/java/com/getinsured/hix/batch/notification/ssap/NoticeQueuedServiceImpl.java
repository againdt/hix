package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.platform.repository.NoticeQueuedRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.sql.TSTimestamp;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

@Service("noticeQueuedService")
@Transactional
public class NoticeQueuedServiceImpl implements NoticeQueuedService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NoticeQueuedServiceImpl.class);
    private static final int QUEUE_PROCESSING_BATCH_SIZE = 500;
    private static final NoticeQueued.QueuedStatus NOTICE_QUEUED_STATUS = NoticeQueued.QueuedStatus.STAGED;
    private static final String SORT_BY_VALUE = "creationTimestamp";
    private static final String SCHEDULED_DATE_FORMAT = "yyyy-MM-dd";

    @Autowired
    private NoticeQueuedRepository noticeQueuedRepository;

    @Override
    public void processNoticesQueued(String scheduledDate) throws GIException {
        try {
            LOGGER.debug("processNoticesQueued::scheduledDate = {}", scheduledDate);
            Date runDate;
            if(StringUtils.isBlank(scheduledDate)) {
                TSTimestamp timestamp = new TSTimestamp();
                runDate = DateUtil.removeTime(new Date(timestamp.getTime()));
            } else {
                runDate = DateUtil.StringToDate(scheduledDate, SCHEDULED_DATE_FORMAT);
            }

            LOGGER.debug("processNoticesQueued::getting queued notices for date {}", runDate);
            double noticeQueuedCount = noticeQueuedRepository.getNoticeQueuedCount(NOTICE_QUEUED_STATUS, runDate);
            double numberOfPages = Math.ceil(noticeQueuedCount / QUEUE_PROCESSING_BATCH_SIZE);
            LOGGER.debug("processNoticesQueued::queue count {} so number of pages = {}", noticeQueuedCount, numberOfPages);

            IntStream.range(0, (int) numberOfPages).forEach(page -> {
                LOGGER.debug("processNoticesQueued::page {}", page);
                // Getting the first page of QUEUE_PROCESSING_BATCH_SIZE since while processing pages we are also updating status of notices.
                // We cannot get increasing pages because setting the new status of notices removes them from the next query, essentially skipping any notice that now exists on the previous page.
                PageRequest pageRequest = new PageRequest(0, QUEUE_PROCESSING_BATCH_SIZE, new Sort(Sort.Direction.ASC, SORT_BY_VALUE));
                List<NoticeQueued> queuedNotices = noticeQueuedRepository.findNoticeQueuedByStatusAndDate(NOTICE_QUEUED_STATUS, runDate, pageRequest);
                LOGGER.debug("processNoticesQueued::queuedNotices size = {}", queuedNotices.size());

                for (NoticeQueued noticeQueued : queuedNotices) {
                    long noticeId = noticeQueued.getId();

                    try {
                        LOGGER.debug("processNoticesQueued::processing notice {} in {}", noticeQueued.getColumnValue(), noticeQueued.getTableName());
                        LOGGER.debug("processNoticesQueued::getting processor of class {}", noticeQueued.getNoticeType().getQueuedEmailClass());
                        QueuedNoticeProcessor queuedNoticeProcessor = GHIXApplicationContext.getBean(noticeQueued.getNoticeType().getQueuedEmailClass(), QueuedNoticeProcessor.class);

                        if (queuedNoticeProcessor != null) {
                            if (queuedNoticeProcessor.verify(noticeQueued)) {
                                if (queuedNoticeProcessor.process(noticeQueued)) {
                                    LOGGER.debug("processNoticesQueued::Successfully processed queued notice {}", noticeId);
                                    noticeQueued.setStatus(NoticeQueued.QueuedStatus.PROCESSED);
                                } else {
                                    LOGGER.debug("processNoticesQueued::Failed to process queued notice {}", noticeId);
                                    noticeQueued.setStatus(NoticeQueued.QueuedStatus.ERROR);
                                }
                            } else {
                                LOGGER.debug("processNoticesQueued::Failed to verify queued notice {}", noticeId);
                                noticeQueued.setStatus(NoticeQueued.QueuedStatus.SUPPRESSED);
                            }
                        } else {
                            LOGGER.debug("processNoticesQueued::queuedNoticeProcessor was null processing queued notice {}", noticeId);
                            noticeQueued.setStatus(NoticeQueued.QueuedStatus.ERROR);
                        }
                    } catch (Exception ex) {
                        LOGGER.error("processUndeliverableMail::error processing notice queued id {}", noticeId);
                        noticeQueued.setStatus(NoticeQueued.QueuedStatus.ERROR);
                    }

                    noticeQueuedRepository.save(noticeQueued);
                }
            });
        } catch (Exception ex) {
            LOGGER.error("processUndeliverableMail::Exception occurred setting broker availability", ex);
            throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), GIRuntimeException.Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
        }
    }
}