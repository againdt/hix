<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="row-fluid">
    <div class="gutter10">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/profile/update/market/shop"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message code="pgheader.step"/> 5 </li>
        </ul><!--page-breadcrumb ends-->
        <h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
    </div>
</div>
		<div class="row-fluid">
        	<div class="gutter10">
                <div class="span3" id="sidebar">
                    <div class="header">
                        <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                    </div>
                    <!--  beginning of side bar -->
                    <ol class="nav nav-list nav-steps">
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.addRepresentative"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.issuerApplication"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.financialInformation"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.updateProfile"/></a></li>
                        <li class="active"><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                    </ol>
                    <!-- end of side bar -->
                </div>
                <!-- end of span3 -->
                <div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="span10"><spring:message code="pgheader.steps"/> 5: <spring:message  code="pgheader.signApplication"/></h4>
                    </div>
                    <div class="gutter10">
                    <p> <spring:message code="label.exchangePolicy"/> </p>
                    <form class="form-horizontal" id="frmIssuersignApp" name="frmIssuersignApp" action="<c:url value="/planmgmt/issuer/sign/application/save" />" method="POST">
                         <df:csrfToken/>
                        <div class="control-group">
                            <label class="control-label" for="sing-statement"><spring:message  code="label.planStatement"/></label>
                            <div class="controls">
                                <textarea  readonly="readonly" style="width: 245px; height: 73px;" rows="3" id="statements" name="statements"  class="input-large">${statement}</textarea>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group"> 
                            <label for="sign-declare" class="control-label"><spring:message  code="label.planDeclaration"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 
                            <div class="controls"> 
                                <label class="checkbox"> 
                                    <input type="checkbox" class="checkbox" id="signDeclare" name="signDeclare" value=""/>
                                        <spring:message  code="label.planDeclarationText"/>
                                </label>
                                <div id="signDeclare_error" class=""></div> 
                            </div> 
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="sign-name"><spring:message  code="label.planIssuerName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls controls-row">
                                <input type="text"  id="firstName" name="firstName" class="span3 focusEvent" placeholder="<spring:message  code='label.first'/>" />
                                <input type="text"  id="lastName" name="lastName" class="span3 focusEvent" placeholder="<spring:message  code='label.last'/>" />
                                <div id="firstName_error" class=""></div>
                                <div id="lastName_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
    
                        <div class="control-group">
                            <label class="control-label" for="sign-eSign"><spring:message  code="label.planeSignature"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="eSignature" id="eSignature" class="input-xlarge focusEvent" placeholder="<spring:message code='label.planeSignatureText'/>"/>
                                <div id="eSignature_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="sign-date"><spring:message  code="label.planVerifyDate"/></label>
                            <div class="controls">
                                <label class="help-block paddingT5">${todaysDate}</label>							
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="form-actions paddingLR20">
                            <a href='<c:url value="/planmgmt/issuer/profile/update/market/shop"/>' class="btn"><spring:message  code="label.back"/></a>
                            <button type="submit" class="btn btn-primary"><spring:message  code="label.issuerSubmit"/></button>
                        </div>
                    </form>
                </div>
            </div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->
<script type="text/javascript">

var validator = $("#frmIssuersignApp").validate({ 
	rules : {
		signDeclare : {required : true},
		firstName : {required : true},
		lastName : {required : true},
		eSignature :  {required : true}
	},
	messages : {
		signDeclare : { required : "<span><em class='excl'>!</em><spring:message  code='err.checkbox' javaScriptEscape='true'/></span>"}
		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

$("input:text").each(function(){
	$(this).data("value", $(this).val());
	var fieldValue = $(this).data("value");
	$(this).focus(function(){
		if ( $(this).val() != ""){
			$(this).val("")
		}
	});
	$(this).blur(function(){
	//if value changed
	if($(this).val() == ""){
		$(this).val(fieldValue)
		}
	});
});
</script>
