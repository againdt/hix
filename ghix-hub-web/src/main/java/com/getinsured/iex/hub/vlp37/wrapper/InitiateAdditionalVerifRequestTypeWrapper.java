package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.FDSHDataTypeWrapper;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.InitiateAdditionalVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ObjectFactory;
import com.getinsured.iex.hub.vlp37.wrapper.util.InitiateAdditionalVerifRequestTypeWrapperUtil;


public class InitiateAdditionalVerifRequestTypeWrapper implements JSONAware, FDSHDataTypeWrapper{
	
	private static ObjectFactory factory = null;
	
	private InitiateAdditionalVerifRequestType request = null;
	private String caseNumber;
	private String aka;
	private String alienNumber;
	private String i94Number;
	private InitiateAddVerifPassportCountryWrapper passportCountry;
	private String receiptNumber;
	private String comments;	  
	private boolean requestSponsorDataIndicator;
	private boolean requestGrantDateIndicator;
	private String casePOCFullName;
	private String casePOCPhoneNumber;
	private String casePOCPhoneNumberExtension;
		
	private static final String CASE_NUMBER_KEY = "CaseNumber";
	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	private static final String RECEIPT_NUMBER_KEY = "ReceiptNumber";
	private static final String AKA_KEY = "AKA";
	private static final String COMMENTS_KEY = "Comments";
	private static final String REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
	private static final String REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
	private static final String CASE_POC_FULLENAME_KEY = "CasePOCFullName";
	private static final String CASE_POC_PHONE_NUMBER_KEY = "CasePOCPhoneNumber";
	private static final String CASE_POC_PHONE_NUMBER_EXT_KEY = "CasePOCPhoneNumberExtension";
		
	public InitiateAdditionalVerifRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createInitiateAdditionalVerifRequestType();
	}
	
	public InitiateAdditionalVerifRequestTypeWrapper(InitiateAdditionalVerifRequestType request) {
		this.request = request;
		this.aka = request.getAKA();
		this.caseNumber = request.getCaseNumber();
		this.alienNumber = request.getAlienNumber();
		this.i94Number = request.getI94Number();
		this.passportCountry = new InitiateAddVerifPassportCountryWrapper(request.getPassportCountry());
		this.receiptNumber = request.getReceiptNumber();
		this.comments = request.getComments();
		this.casePOCFullName = request.getCasePOCFullName();
		this.casePOCPhoneNumber = request.getCasePOCPhoneNumber();
		this.casePOCPhoneNumberExtension = request.getCasePOCPhoneNumberExtension();
		this.requestGrantDateIndicator = request.isRequestGrantDateIndicator();
		this.requestSponsorDataIndicator = request.isRequestSponsorDataIndicator();
	}

	/**
	 * This method will also do the request specific validations
	 */
	public InitiateAdditionalVerifRequestType getRequest() throws HubServiceException {
		return this.request;
	}
	
	public String getAKA() {
		return aka;
	}

	public void setAKA(String aka) {
		this.aka = aka;
		request.setAKA(aka);
	}

	public String getAlienNumber() {
		return alienNumber;
	}

	public void setAlienNumber(String alienNumber) {
		this.alienNumber = alienNumber;
		request.setAlienNumber(alienNumber);
	}

	public String getI94Number() {
		return i94Number;
	}

	public void setI94Number(String i94Number) {
		this.i94Number = i94Number;
		request.setI94Number(i94Number);
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
		request.setReceiptNumber(receiptNumber);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
		request.setComments(comments);
	}

	public boolean isRequestSponsorDataIndicator() {
		return requestSponsorDataIndicator;
	}

	public void setRequestSponsorDataIndicator(boolean requestSponsorDataIndicator) {
		this.requestSponsorDataIndicator = requestSponsorDataIndicator;
		request.setRequestSponsorDataIndicator(requestSponsorDataIndicator);
	}

	public boolean isRequestGrantDateIndicator() {
		return requestGrantDateIndicator;
	}

	public void setRequestGrantDateIndicator(boolean requestGrantDateIndicator) {
		this.requestGrantDateIndicator = requestGrantDateIndicator;
		request.setRequestGrantDateIndicator(requestGrantDateIndicator);
	}

	public String getCasePOCFullName() {
		return casePOCFullName;
	}

	public void setCasePOCFullName(String casePOCFullName) {
		this.casePOCFullName = casePOCFullName;
		request.setCasePOCFullName(casePOCFullName);
	}

	public String getCasePOCPhoneNumber() {
		return casePOCPhoneNumber;
	}

	public void setCasePOCPhoneNumber(String casePOCPhoneNumber) {
		this.casePOCPhoneNumber = casePOCPhoneNumber;
		request.setCasePOCPhoneNumber(casePOCPhoneNumber);
	}

	public String getCasePOCPhoneNumberExtension() {
		return casePOCPhoneNumberExtension;
	}

	public void setCasePOCPhoneNumberExtension(String casePOCPhoneNumberExtension) {
		this.casePOCPhoneNumberExtension = casePOCPhoneNumberExtension;
		request.setCasePOCPhoneNumberExtension(casePOCPhoneNumberExtension);
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
		request.setCaseNumber(caseNumber);
	}
	
	public InitiateAddVerifPassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(InitiateAddVerifPassportCountryWrapper passportCountry) {
		this.passportCountry = passportCountry;
		request.setPassportCountry(passportCountry.getPassportCountryType());
	}

	public void setRequest(InitiateAdditionalVerifRequestType request) {
		this.request = request;
	}

	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static InitiateAdditionalVerifRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
		
		InitiateAdditionalVerifRequestTypeWrapper wrapper = new InitiateAdditionalVerifRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		
		param = obj.get(AKA_KEY);
		if(param != null){
			wrapper.setAKA((String)param);
		}
		
		param = obj.get(CASE_NUMBER_KEY);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
	
		param = obj.get(COMMENTS_KEY);
		if(param != null){
			wrapper.setComments((String)param);
		}
		
		param = obj.get(RECEIPT_NUMBER_KEY);
		if(param != null){
			wrapper.setReceiptNumber((String)param);
		}

		param = obj.get(ALIEN_NUMBER_KEY);
		if(param != null){
			wrapper.setAlienNumber((String)param);
		}

		param = obj.get(I94_NUMBER_KEY);
		if(param != null){
			wrapper.setI94Number((String)param);
		}
		
		param = obj.get(PASSPORT_COUNTRY_KEY);
		if(param != null){
			JSONObject passportCountry = (JSONObject)param;
			wrapper.setPassportCountry(InitiateAddVerifPassportCountryWrapper.fromJSONObject(passportCountry));
		}

		param = obj.get(AKA_KEY);
		if(param != null){
			wrapper.setAKA((String)param);
		}
		
		param = obj.get(CASE_POC_FULLENAME_KEY);
		if(param != null){
			wrapper.setCasePOCFullName((String)param);
		}
		
		param = obj.get(CASE_POC_PHONE_NUMBER_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumber((String)param);
		}
		
		param = obj.get(CASE_POC_PHONE_NUMBER_EXT_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumberExtension((String)param);
		}
		
		InitiateAdditionalVerifRequestTypeWrapperUtil.addIndicatorsToWrapper(obj, wrapper);
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(AKA_KEY, this.aka);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(CASE_NUMBER_KEY, this.caseNumber);			
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);		
		obj.put(I94_NUMBER_KEY, this.i94Number	);		
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}	
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber	);	
		obj.put(COMMENTS_KEY, this.comments);			
		obj.put(CASE_POC_FULLENAME_KEY, this.casePOCFullName);
		obj.put(CASE_POC_PHONE_NUMBER_KEY, this.casePOCPhoneNumber	);
		obj.put(CASE_POC_PHONE_NUMBER_EXT_KEY, this.casePOCPhoneNumberExtension);
		return obj.toJSONString();

	}
	
	public InitiateAdditionalVerifRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		Object param = null;
		InitiateAdditionalVerifRequestTypeWrapper wrapper = new InitiateAdditionalVerifRequestTypeWrapper();
		wrapper.setAKA((String)jsonObj.get(AKA_KEY));
		wrapper.setRequestSponsorDataIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTSPONSORDATAINDICATOR_KEY)));
		wrapper.setRequestGrantDateIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTGRANTDATEINDICATOR_KEY)));
		wrapper.setCaseNumber((String)jsonObj.get(CASE_NUMBER_KEY));
		
		wrapper.setAlienNumber((String)jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setI94Number((String)jsonObj.get(I94_NUMBER_KEY));
		param = jsonObj.get(PASSPORT_COUNTRY_KEY);
		if(param != null){
			JSONObject passportCountry = (JSONObject)param;
			wrapper.setPassportCountry(InitiateAddVerifPassportCountryWrapper.fromJSONObject(passportCountry));
		}
		wrapper.setReceiptNumber((String)jsonObj.get(RECEIPT_NUMBER_KEY));
		wrapper.setComments((String)jsonObj.get(COMMENTS_KEY));
		wrapper.setCasePOCFullName((String)jsonObj.get(CASE_POC_FULLENAME_KEY));
		wrapper.setCasePOCPhoneNumber((String)jsonObj.get(CASE_POC_PHONE_NUMBER_KEY));
		wrapper.setCasePOCPhoneNumberExtension((String)jsonObj.get(CASE_POC_PHONE_NUMBER_EXT_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(AKA_KEY, this.aka);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(CASE_NUMBER_KEY, this.caseNumber);			
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);		
		obj.put(I94_NUMBER_KEY, this.i94Number	);		
		obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry	);	
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber	);	
		obj.put(COMMENTS_KEY, this.comments);			
		obj.put(CASE_POC_FULLENAME_KEY, this.casePOCFullName);
		obj.put(CASE_POC_PHONE_NUMBER_KEY, this.casePOCPhoneNumber	);
		obj.put(CASE_POC_PHONE_NUMBER_EXT_KEY, this.casePOCPhoneNumberExtension);

		return obj;
	}

	@Override
	public InitiateAdditionalVerifRequestType getSystemRepresentation() {
		// TODO Auto-generated method stub
		return this.request;
	}
}
