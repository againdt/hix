import { onDemandActionTypes } from '../constants/actionTypes';

const initialState = {
  isAvailable: false,
  phoneNumber: '',
  status: 'INACTIVE'
};

export default (state = initialState, action) => {
  switch (action.type) {
    case onDemandActionTypes.SET_ON_DEMAND_DATA:
      return { ...state, ...action.data };
    case onDemandActionTypes.SUBMIT_ON_DEMAND_DATA:
      return { ...state, ...action.data };
    default:
      return state;
  }
};
