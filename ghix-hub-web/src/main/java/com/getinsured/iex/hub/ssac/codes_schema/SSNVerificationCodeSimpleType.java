
package com.getinsured.iex.hub.ssac.codes_schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SSNVerificationCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SSNVerificationCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="B"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="D"/>
 *     &lt;enumeration value="E"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SSNVerificationCodeSimpleType", namespace = "http://codes.ssac.ee.sim.dsh.cms.hhs.gov")
@XmlEnum
public enum SSNVerificationCodeSimpleType {


    /**
     * Verified; The SSN, Name and DOB are Verified
     * 
     */
    A,

    /**
     * Not Verified; The input SSN does not exist on the numident or is marked for deletion or is inaccessible 
     * 
     */
    B,

    /**
     * Not verified;  SSN exists; There is no match on either name or DOB
     * 
     */
    C,

    /**
     * Not verified; SSN exists; There is a match on name but not on DOB 
     * 
     */
    D,

    /**
     * Not verified; SSN exists:  There is a match on DOB but not on DOB
     * 
     */
    E;

    public String value() {
        return name();
    }

    public static SSNVerificationCodeSimpleType fromValue(String v) {
        return valueOf(v);
    }

}
