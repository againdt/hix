package com.getinsured.hix.indportal.dto;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;

/**
 * Story HIX-48237
 * As an Exchange, allow consumer (House hold) to 
 * change the Elected APTC Amount
 * 
 * Sub-task HIX-57890 
 * Create api to accept enrollment id, elected aptc 
 * from plan summary page
 * 	
 * DTO to be used for changing elected APTC
 * 
 * @author Nikhil Talreja
 */
public class AptcUpdate {
	
	private String userName;
	
	private String caseNumber;
	
	private String healthEnrollmentId;
	
	private String dentalEnrollmentId;
	
	private float aptcAmt;
	
	private Date aptcEffectiveDate;
	
	private Date terminationDate;
	
	private Date healthEnrollmentEndDate;
	
	private Boolean isFinancialConversion;
	
	private Boolean isNonFinancialConversion;
	
	private String aptcPremiumEffectiveDate;
	
	private String response;
	
	private AppGroupResponse appGroupResponse;

	private List<EnrollmentShopDTO> enrollmentList;

	public String getUserName() {
		if(null==userName || (null!=userName && userName.trim().length()==0)){
			return "exadmin@ghix.com";
		}
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public Boolean getIsFinancialConversion() {
		return isFinancialConversion;
	}

	public void setIsFinancialConversion(Boolean isFinancialConversion) {
		this.isFinancialConversion = isFinancialConversion;
	}

	public Boolean getIsNonFinancialConversion() {
		return isNonFinancialConversion;
	}

	public void setIsNonFinancialConversion(Boolean isNonFinancialConversion) {
		this.isNonFinancialConversion = isNonFinancialConversion;
	}

	public void setHealthEnrollmentId(String healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public String getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(String dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public float getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public Date getAptcEffectiveDate() {
		return aptcEffectiveDate;
	}

	public void setAptcEffectiveDate(Date aptcEffectiveDate) {
		this.aptcEffectiveDate = aptcEffectiveDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	
	public Date getHealthEnrollmentEndDate() {
		return healthEnrollmentEndDate;
	}

	public void setHealthEnrollmentEndDate(Date healthEnrollmentEndDate) {
		this.healthEnrollmentEndDate = healthEnrollmentEndDate;
	}

	/**
	 * @return the aptcPremiumEffectiveDate
	 */
	public String getAptcPremiumEffectiveDate() {
		return aptcPremiumEffectiveDate;
	}

	/**
	 * @param aptcPremiumEffectiveDate the aptcPremiumEffectiveDate to set
	 */
	public void setAptcPremiumEffectiveDate(String aptcPremiumEffectiveDate) {
		this.aptcPremiumEffectiveDate = aptcPremiumEffectiveDate;
	}

	/**
	 * @return the errorCode
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	public AppGroupResponse getAppGroupResponse() {
		return appGroupResponse;
	}

	public void setAppGroupResponse(AppGroupResponse appGroupResponse) {
		this.appGroupResponse = appGroupResponse;
	}
	
	public List<EnrollmentShopDTO> getEnrollmentList() {
		return enrollmentList;
	}

	public void setEnrollmentList(List<EnrollmentShopDTO> enrollmentList) {
		this.enrollmentList = enrollmentList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AptcUpdate [userName=");
		builder.append(userName);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", healthEnrollmentId=");
		builder.append(healthEnrollmentId);
		builder.append(", dentalEnrollmentId=");
		builder.append(dentalEnrollmentId);
		builder.append(", aptcAmt=");
		builder.append(aptcAmt);
		builder.append(", aptcEffectiveDate=");
		builder.append(aptcEffectiveDate);
		builder.append(", terminationDate=");
		builder.append(terminationDate);
		builder.append(", healthEnrollmentEndDate=");
		builder.append(healthEnrollmentEndDate);
		builder.append(", isFinancialConversion=");
		builder.append(isFinancialConversion);
		builder.append(", isNonFinancialConversion=");
		builder.append(isNonFinancialConversion);
		builder.append(", aptcPremiumEffectiveDate=");
		builder.append(aptcPremiumEffectiveDate);
		builder.append(", response=");
		builder.append(response);
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
