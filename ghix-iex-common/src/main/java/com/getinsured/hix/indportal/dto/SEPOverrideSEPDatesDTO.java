package com.getinsured.hix.indportal.dto;

public class SEPOverrideSEPDatesDTO {

	private String sepStartDate;
	private String sepEndDate;
	/**
	 * @return the sepStartDate
	 */
	public String getSepStartDate() {
		return sepStartDate;
	}
	/**
	 * @param sepStartDate the sepStartDate to set
	 */
	public void setSepStartDate(String sepStartDate) {
		this.sepStartDate = sepStartDate;
	}
	/**
	 * @return the sepEndDate
	 */
	public String getSepEndDate() {
		return sepEndDate;
	}
	/**
	 * @param sepEndDate the sepEndDate to set
	 */
	public void setSepEndDate(String sepEndDate) {
		this.sepEndDate = sepEndDate;
	}
	
}
