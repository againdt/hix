package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentHouseholdIncomeType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentHouseholdIncomeType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class CurrentHouseholdIncomeTypeWrapper implements JSONAware {

	private int tier;
	private String asOfDate;
	private VerificationMetadataTypeWrapper verificationMetadata;
	private PersonTypeWrapper employeeInformation;
	private EmployerInfoTypeWrapper employerInformation;
	private EmploymentInfoTypeWrapper employmentInformation;
	private BaseCompensationInfoTypeWrapper baseCompensationInformation;
	private AnnualCompensationTypeWrapper annualCompensation;
	private PayPeriodInfoTypeWrapper payPeriod;
	
	private static final String TIER_KEY = "Tier";
	private static final String AS_OF_DATE_KEY = "AsOfDate";
	private static final String METADATA_KEY = "VerificationMetadata";
	private static final String EMPLOYEE_KEY =  "EmployeeInformation";
	private static final String EMPLOYER_KEY = "EmployerInformation";
	private static final String EMPLOYMENT_KEY = "EmploymentInformation";
	private static final String BASE_SALARY_KEY = "BaseCompensationInformation";
	private static final String ANNUAL_SALARY_KEY = "AnnualCompensation";
	private static final String PAY_PERIOD_KEY = "PayPeriod";
	
	public CurrentHouseholdIncomeTypeWrapper(CurrentHouseholdIncomeType currentHouseholdIncomeType) {

		if (currentHouseholdIncomeType == null) {
			return;
		}
		
		this.tier = currentHouseholdIncomeType.getTier();
		if(currentHouseholdIncomeType.getAsOfDate() != null 
				&& currentHouseholdIncomeType.getAsOfDate().getDateRepresentation() != null){
			Date afDate = (Date) currentHouseholdIncomeType.getAsOfDate().getDateRepresentation().getValue();
			this.asOfDate = afDate.getValue().toString();
		}
		this.verificationMetadata = new VerificationMetadataTypeWrapper(currentHouseholdIncomeType.getVerificationMetadata());
		this.employeeInformation  = new PersonTypeWrapper(currentHouseholdIncomeType.getEmployeeInformation());
		this.employerInformation = new EmployerInfoTypeWrapper(currentHouseholdIncomeType.getEmployerInformation());
		this.employmentInformation = new EmploymentInfoTypeWrapper(currentHouseholdIncomeType.getEmploymentInformation());
		this.baseCompensationInformation = new BaseCompensationInfoTypeWrapper(currentHouseholdIncomeType.getBaseCompensationInformation());
		this.annualCompensation = new AnnualCompensationTypeWrapper(currentHouseholdIncomeType.getAnnualCompensation());
		this.payPeriod = new PayPeriodInfoTypeWrapper(currentHouseholdIncomeType.getPayPeriod());
	}

	public int getTier() {
		return tier;
	}

	public String getAsOfDate() {
		return asOfDate;
	}

	public VerificationMetadataTypeWrapper getVerificationMetadata() {
		return verificationMetadata;
	}

	public PersonTypeWrapper getEmployeeInformation() {
		return employeeInformation;
	}

	public EmployerInfoTypeWrapper getEmployerInformation() {
		return employerInformation;
	}

	public EmploymentInfoTypeWrapper getEmploymentInformation() {
		return employmentInformation;
	}

	public BaseCompensationInfoTypeWrapper getBaseCompensationInformation() {
		return baseCompensationInformation;
	}

	public AnnualCompensationTypeWrapper getAnnualCompensation() {
		return annualCompensation;
	}

	public PayPeriodInfoTypeWrapper getPayPeriod() {
		return payPeriod;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(TIER_KEY, this.tier);
		obj.put(AS_OF_DATE_KEY, this.asOfDate);
		obj.put(METADATA_KEY, this.verificationMetadata);
		obj.put(EMPLOYEE_KEY, this.employeeInformation);
		obj.put(EMPLOYER_KEY, this.employerInformation);
		obj.put(EMPLOYMENT_KEY, this.employmentInformation);
		obj.put(BASE_SALARY_KEY, this.baseCompensationInformation);
		obj.put(ANNUAL_SALARY_KEY, this.annualCompensation);
		obj.put(PAY_PERIOD_KEY, this.payPeriod);
		return obj.toJSONString();
	}
	

}
