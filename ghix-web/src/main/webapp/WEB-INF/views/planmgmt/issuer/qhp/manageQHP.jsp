<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@page import="com.getinsured.hix.model.Plan"%>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<!-- start of secondary navbar -->
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<style>
#sidebar {
        margin-left: 0;
        border-right: none;
}
.control-group {
   display: inline-block;
   vertical-align: top;
    width: 100%;
}
#go {
    margin-top: 20px;
    padding: 4px 18px;
}
#rightpanel {
    padding-left: 4px;
}

.table tbody tr td .dropdown.open li a {
  text-align: left;
}

.dropdown-menu {
    right: 0;
    left: auto;
}
#plansBulk  {
	margin-left: -5px;
}
#plansBulk i.icon-cog {
	line-height: 40px;
}
#plansBulk i.caret {
	margin-top: 0;
	margin-left: 0;
}
</style>


<div class="gutter10">
    <div class="row-fluid"> 
       	<ul class="page-breadcrumb">
            <li><a href="javascript:history.back()">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="label.healthplansTitle"/></a></li>
        	<li><spring:message code='label.linkManageQHP'/></li> 
        </ul> <!--page-breadcrumb ends-->
        <h1><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/> <spring:message code='label.qhp' /> <small>${resultSize} <spring:message code='label.totalplans' /></small></h1>
    </div>
    <%-- <a href="<c:url value="/issuer/qhp/generatePlanReport/rest" />" class="pull-right marginTop10"><i class="icon icon-user"></i>Generating Report Call</a> --%>

    <div class="row-fluid">
	  <div class="span3"></div>
	  <div class="pull-right">
			<label class="inline-block margin10-r" for="planYear" style="vertical-align: super;">Plan Year </label>
			<select id="planYear" name="planYear" class="input-medium">
				<c:if test="${fn:length(planYearsList) <= 0}"><option value="">Select Year</option></c:if>
				<c:forEach var="planYearsList" items="${planYearsList}">
					<option <c:if test="${planYearsList.planYearsKey == selectedPlanYear.planYearsKey}">selected</c:if> value="<c:out value="${planYearsList.planYearsKey}"/>">${planYearsList.planYearsKey}</option>
				</c:forEach>
			</select>
	  </div>
    </div>

    <div class="row-fluid">
        <div class="span3" id="sidebar">
            
            <div class="header">
            	<h4><spring:message code='label.refineresult' /></h4>
            </div>
            <form class="form-vertical" id="frmfindplan" name="frmfindplan" action="<c:url value="/issuer/manageqhp" />" method="POST">
            <df:csrfToken/>
            <input type="hidden" id="planYearSelected" name="planYearSelected">
            <div class="graybg gutter10">
                
                        <div class="control-group">
                            <label for="planNumber" class="control-label"><spring:message code='label.planNumber' /></label>
                            <div class="controls">
                                <input type="text" name="planNumber" id="planNumber" value="${planNumber}" class="span12" maxlength="16"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                                            
                        <div class="control-group">
                                    <label for="planLevel" class="control-label"><spring:message  code="label.planLevel"/></label>
                                    <div class="controls">
                                        <select id="planLevel" name="planLevel" class="span12">
                                            <option value=""><spring:message code='label.planLevel' /></option>
                                            <c:forEach var="levelList" items="${levelList}">
                                                <option <c:if test="${levelList.key == selectedPlanLevel}">selected</c:if> value="${levelList.key}">${levelList.value}</option>
                                            </c:forEach>
                                        </select>
                                    </div> <!-- end of controls-->
                                </div>
                        
                        <%--<div class="control-group">
                            <label for="market" class="control-label"><spring:message  code="label.planMarket"/></label>
                            <div class="controls">
                                <select id="market" name="market" class="span12">
                                    <option value=""><spring:message code='label.any' /></option>
                                    <c:forEach var="marketList" items="${marketList}">
                                        <option <c:if test="${market==marketList.key}">selected</c:if> value="${marketList.key}">${marketList.value}</option>
                                    </c:forEach>
                                    
                                </select>
                            </div> 
                        </div> --%>
                        <div class="control-group">
                            <label for="status" class="control-label"><spring:message code='label.status.plan' /></label>
                            <div class="controls">
                                <select id="status" name="status" class="span12" style="width:145px;">
                                <option value=""><spring:message code='label.any' /></option>
                                <c:forEach var="statusList" items="${statusList}">
                                    <option <c:if test="${status==statusList.key}">selected</c:if> value="${statusList.key}">${statusList.value}</option>
                                </c:forEach>
                            </select>
                            </div> <!-- end of controls-->
                        </div> <!-- end of control-group -->
                        <div class="control-group">
							<label for="enrollmentAvailability" class="control-label"><spring:message
									code='label.enrollAvail' /></label>
							<div class="controls">
								<select id="enrollmentAvailability" name="enrollmentAvailability" class="span12">
									<option value="">
										<spring:message code='label.any' />
									</option>
									<c:forEach var="enrollmentAvailList" items="${enrollmentAvailList}">
										<option <c:if test="${enrollmentAvailability==enrollmentAvailList.key}">selected</c:if>
											value="${enrollmentAvailList.key}">${enrollmentAvailList.value}</option>
									</c:forEach>
								</select>
							</div>
							<!-- end of controls-->
						</div>
                        <div class="txt-center">
                        <input type="submit" class="btn btn-primary" value="<spring:message  code='label.go'/>" id="go" title="<spring:message  code='label.go'/>">
                        </div>
                        </div>
                </form> <!-- .actions -->
            </div>
            
 
        
        
        <div class="span9" id="rightpanel">
            <c:choose>
                <c:when test="${fn:length(plans) > 0}">
                	<spring:message code='label.planNumber' var="planNumberVal"/>	
                	<spring:message code='label.planName' var="planNameVal" />					
                    <spring:message code='label.issuer' var="issuerVal"/>
                    <spring:message code='label.level' var="levelVal"/>
                    <%--<spring:message code='label.Market' var="marketVal"/>--%>
                    <spring:message code='label.lastUpdate' var="lastUpdateVal"/>
                    <spring:message code='label.status.plan' var="statusVal"/>
                    <spring:message code='label.enrollmentAvailability' var="enrollmentAvailability"/>	
                    <table class="table table-condensed table-border-none table-striped">
                        <thead>
                            <tr class="header">
                            	<!-- <th scope="col" class="sortable" style="width: 40px;">Select</th> -->
                            	<th scope="col" style="width: 3%;"><input type="checkbox" name="planIdCheck" value="planIdCheck" class="planIdCheckHeader"></th>
                                <th scope="col" class="sortable" style="width: 16%;"><dl:sort title="${planNumberVal}" sortBy="planNumber" sortOrder="${sortOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'planNumber'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Plan name sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'planNumber'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Plan name sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>
                                <th class="sortable" scope="col"><dl:sort title="${planNameVal}" sortBy="name" sortOrder="${tempOrder}"></dl:sort>
                                        <c:choose>
                                            <c:when test="${sortOrder == 'ASC' && sortBy == 'planName'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Plan name sort ascending"/></c:when>
                                            <c:when test="${sortOrder == 'DESC' && sortBy == 'planName'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Plan name sort descending"/></c:when>
                                            <c:otherwise></c:otherwise>
                                        </c:choose>
                                </th>
                                <th scope="col" class="sortable" style="width: 7%;"><dl:sort title="${levelVal}" sortBy="level" sortOrder="${tempOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'level'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Plan level sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'level'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Plan level sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>
                                <%--<th scope="col" class="sortable" style="width: 73px;"><dl:sort title="${marketVal}" sortBy="planMarket" sortOrder="${tempOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'planMarket'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Market plan sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'planMarket'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Market plan sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>--%>
                                <th scope="col" class="sortable" style="width: 11%;"><dl:sort title="${lastUpdateVal}" sortBy="createdOn" sortOrder="${tempOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'createdOn'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Last update sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'createdOn'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Last update sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>
                               
                                <th scope="col" class="sortable" style="width: 7%;"><dl:sort title="${statusVal}" sortBy="planStatus" sortOrder="${tempOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'planStatus'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Plan status sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'planStatus'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Plan status sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>
                                <th scope="col" class="sortable"><dl:sort title="${enrollmentAvailability}" sortBy="enrollment" sortOrder="${tempOrder}"></dl:sort>
                                    <c:choose>
                                        <c:when test="${sortOrder == 'ASC' && sortBy == 'enrollment'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="Enrollment sort ascending"/></c:when>
                                        <c:when test="${sortOrder == 'DESC' && sortBy == 'enrollment'}"><img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="Enrollment sort descending"/></c:when>
                                        <c:otherwise></c:otherwise>
                                    </c:choose>
                                </th>
								<th class="dropdown pull-right"  scope="col" style="width: 40px;">
									<a id="plansBulk" class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
										<li><a id="issuerVerification" href="#"><i class="icon icon-user"></i><spring:message code='label.issuerVerify' /></a></li>
									</ul>
								</th>
                            </tr>
                        </thead>
                        <c:forEach items="${plans}" var="plan">
                         <c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
                            <tr>
                                <td><input name="planIdCheck" class="planIdCheckRows" type="checkbox" value="${plan.id}" /></td>
                                
                                <td><a href="<c:url value="/issuer/qhp/plandetails/${encPlanId}" />">${plan.issuerPlanNumber}</a></td>
								<td class="gutter5-r">${plan.name}</td>	
                                <td>  
                               		<c:forEach var="levelList" items="${levelList}">
										<c:if test="${levelList.key == plan.planHealth.planLevel}">${levelList.value}</c:if>
									</c:forEach>
                                </td>
                                <%--<td> ${plan.market} </td>--%>
                                <td ><fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${plan.lastUpdateTimestamp}" timeZone="${timezone}" /></td>
                                <td >
                               			<c:choose>
												<c:when test="${fn:toUpperCase(plan.status) == 'CERTIFIED'}"><spring:message code='label.certified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'LOADED'}"><spring:message code='label.loaded' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'DECERTIFIED'}"><spring:message code='label.deCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'NONCERTIFIED'}"><spring:message code='label.nonCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'RECERTIFIED'}"><spring:message code='label.reCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'INCOMPLETE'}"><spring:message code='label.incomplete' /></c:when>
												<c:otherwise>${plan.status}</c:otherwise>
										</c:choose>
                                
                                <td >
                                	<% pageContext.setAttribute("NOTAVAILABLE", Plan.EnrollmentAvail.NOTAVAILABLE); %>
							       <%-- if enrollment availability either available/dependents only and enrollment available date > than today show availability as not available --%>
							       <c:choose>
							              <c:when test="${plan.enrollmentAvail != NOTAVAILABLE && plan.enrollmentAvailEffDate > now}"><spring:message code='label.notAvailable'/></c:when>
							              <c:when test="${fn:toUpperCase(plan.enrollmentAvail) == 'AVAILABLE'}"><spring:message code='label.available'/></c:when>
										  <c:when test="${fn:toUpperCase(plan.enrollmentAvail) == 'DEPENDENTSONLY'}"><spring:message code='label.dependentsOnly'/></c:when>
										  <c:when test="${fn:toUpperCase(plan.enrollmentAvail) == 'NOTAVAILABLE'}"><spring:message code='label.notAvailable'/></c:when>
							              <c:otherwise>${plan.enrollmentAvail}</c:otherwise>
							       </c:choose>
								</td>
								<td>
                                    <!-- If plan status is CERTIFY and issuer verification status is pending  -->
                                    <c:if test="${(plan.status == certifyStatus) && plan.issuerVerificationStatus == pendingStatus}">
                                    <div class="dropdown"> 
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                            <li><a href="<c:url value="/issuer/qhp/verifyplan/${encPlanId}?planYearSelected=${planYearSelected}" />"><i class="icon icon-user"></i><spring:message  code='label.planVerify'/></a></li>
                                        </ul>
                                    </div>
                                    </c:if>
                                </td>
                            </tr>
                        
                        </c:forEach>
                    </table>
                    <div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"  hideTotal="true"/></div>
                </c:when>
            <c:otherwise>
               <div class="alert alert-info"><spring:message code='label.notification'/></div>
            </c:otherwise>
        </c:choose>
        </div><!-- end of .span9 -->
        <form id="issuerVerificationForm" name="issuerVerificationForm" action="<c:url value="/issuer/qhp/verifyplansBulk"/>" method="POST">
         <df:csrfToken/>
        	<input type="hidden" id="planIdList" name="planIdList" value="<encryptor:enc value="${planIdList}"/>" ></input>
        	<input type="hidden" id="hdnPlanYearSelected" name="planYearSelected" ></input>
        </form>
       
    </div>
</div>
<div id="dialog" title="Plan Rate and Benefit Report"></div>

<div id="dialog-modal" style="display: none;">
  <p>
  	<c:if test="${not empty IS_VERIFIED}">
  		<spring:message code='err.issuerVerified'/>
	</c:if>
	</p>
	
	<p>
  	<c:if test="${not empty IS_CERTIFIED}">
  		<spring:message code='err.planNonCertified'/>
	</c:if>
	</p>
</div>




<script type="text/javascript">
$(document).ready(function(){
	if('${IS_VERIFIED}' != null && '${IS_VERIFIED}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "<spring:message  code='label.issuerVerify'/>",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	
	if('${IS_CERTIFIED}' != null && '${IS_CERTIFIED}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "<spring:message  code='label.issuerVerify'/>",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	
	
	var aNext = $("#nextPage");
	if(null != aNext && undefined !== aNext && typeof(aNext) === "object" && aNext.length > 0) {
	    aNext.html("Next");
	}
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	});
});
$("#go").click(function(){
	  var value = $("#planYear").val();
	  $("#planYearSelected").val(value);
	  //alert($("#planYearSelected").val());
});
$("#planYear").change(function(){
	  var value = $("#planYear option:selected").val();
	  $("#planYearSelected").val(value);
	  $("#frmfindplan").submit();
	  //alert($("#planYearSelected").val());
});



$('#generatePlanReport').click(function(){ generatePlanReport(); });
function generatePlanReport(){
	var selectedPlanIds = "";
    $('.planIdCheckRows:checked').each(function(){
        if(selectedPlanIds !== "") {
        	selectedPlanIds = selectedPlanIds +"," + $(this).val();
        }else{
        	selectedPlanIds += $(this).val();
        }
    });
    if(selectedPlanIds == ""){
    	$("#dialog").dialog({ modal: true });
    	$("#dialog").text("Please Select Plan(s) to generate Report");
		$( "#dialog" ).dialog();
    }else{
    	$('#planIdList').val(selectedPlanIds);
    	$('#frmfindplan').ajaxSubmit({
			url: "<c:url value='/issuer/qhp/generatePlanReport'/>",
			data: {"planIdList": selectedPlanIds},
			success: function(responseText){
			    var splitString = responseText.split(" - ");
			    $("#dialog").dialog({ modal: true });
				$("#dialog").text(splitString[0]);
				$("#dialog").dialog();
				$("#dialog").on( "dialogclose", function(event, ui){
					$('.planIdCheck:checked').each(function(){
						$('.planIdCheck').attr('checked', false); 
				    });					
				});
	       	}
		});
    }
    //alert($('#planIdList').val());
    
    
}


$('#issuerVerification').click(function(){ issuerVerification(); });
function issuerVerification(){
	var selectedPlanIds = "";
	var sYear = $("#planYear option:selected").val();
    $("#hdnPlanYearSelected").val(sYear);
	
    $('.planIdCheckRows:checked').each(function(){
        if(selectedPlanIds !== "") {
        	selectedPlanIds = selectedPlanIds +"," + $(this).val();
        }else{
        	selectedPlanIds += $(this).val();
        }
    });
    if(selectedPlanIds == ""){
    	$("#dialog").dialog({ modal: true });
    	$("#dialog").text("Please Select Plan(s) to Issuer verification");
		$( "#dialog" ).dialog();
    }else{
    	$('#planIdList').val(selectedPlanIds);
    	$('#issuerVerificationForm').submit();
    	
    }
    //alert($('#planIdList').val());
    
    
}

jQuery('.planIdCheckHeader').on('click', function(){
    if(jQuery('.planIdCheckHeader').attr('checked') == 'checked'){
        var r=confirm("Do you want to select all plans");
        if (r==true)
          {
        
        jQuery(".planIdCheckRows").each(function(){
            if(jQuery(this).attr('disabled') == 'true' || jQuery(this).attr('disabled') == 'disabled')
                {
                jQuery(this).removeAttr('checked');
                }else{
                    jQuery(this).attr('checked', 'checked');
                    $( "#plansBulk" ).removeClass("disabled");
                }
            });
          }else{
              jQuery(this).removeAttr('checked');
          }
        
    }else{
        var r=confirm("Do you want to de-select all plans");
        if (r==true)
          {
        
    jQuery('.planIdCheckRows').removeAttr('checked');
    jQuery(this).removeAttr('checked');
    $( "#plansBulk" ).addClass("disabled");
    }else{
        jQuery(this).attr('checked', 'checked');
    }
    }
  });
  
jQuery('.planIdCheckRows').on('click', function(){
    var activate = 0;
    jQuery(".planIdCheckRows").each(function(){
    if(jQuery(this).attr('checked') == 'checked'){
        $( "#plansBulk" ).removeClass("disabled");
        activate++;
        }else{
             $( "#plansBulk" ).addClass("disabled");
        }
    if(activate !=0){
        $( "#plansBulk" ).removeClass("disabled");
    }
    });
});
  

</script>
<!--  end of row-fluid -->
