package com.getinsured.hix.plandisplay;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PersonData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.FlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.SessionData;
import com.getinsured.hix.plandisplay.util.PlanDisplayConstants;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixMember;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixRequest;
import com.google.gson.Gson;


@Controller
@RequestMapping(value="/private")
public class AnonymousPlanDisplayController 
{
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AnonymousPlanDisplayController.class);
	
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private ZipCodeService zipCodeService;	
	@Autowired private PlanDisplayWebHelper planDisplayWebHelper;
	@Autowired private PlanDisplayRestClient planDisplayRestClient;
	@Autowired private Gson platformGson;
	public static final String PARAM_EFFECTIVE_START_DATE = "EFFECTIVE_START_DATE";
	public static final String EMPTY = "";
	
	@RequestMapping(value="/quoteform",method = RequestMethod.GET)
	public String getQuoteForm(final HttpServletRequest request, final Model model){			
		return "private/quoteform";
		
	}
	
	@RequestMapping(value = "saveAnonymous", method = RequestMethod.POST)
	@ResponseBody
	public String saveAnonymous(@RequestBody PhixRequest phixRequest, HttpServletRequest request) {
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveAnonymous POST Method---->", null, false);
		HttpSession session = request.getSession();
		if(session == null){
			session = request.getSession(true);
		}
		
		String zipCode = phixRequest.getZipCode();
		String countyCode = phixRequest.getCountyCode();
		String coverageStartDate = (String)session.getAttribute(PARAM_EFFECTIVE_START_DATE);
		Date coverageStart = DateUtil.StringToDate(coverageStartDate, GhixConstants.REQUIRED_DATE_FORMAT);
		List<PersonData> personDataList = new ArrayList<PersonData>();
		List<PhixMember> memberList = phixRequest.getMembers();		
		for(PhixMember member:memberList){
			PersonData personData = new PersonData();
			personData.setPersonId("0");
			personData.setFirstname(EMPTY);
			personData.setLastname(EMPTY);
			personData.setExternalPersonId(EMPTY);
			personData.setExistingMedicalEnrollmentID(EMPTY);
			personData.setExistingSADPEnrollmentID(EMPTY);
			personData.setDob(member.getDateOfBirth());
			personData.setDentalEligible(SessionData.YESorNO.NO.toString());
			personData.setSmoker(SessionData.YorN.N.toString());
			personData.setRelationship(member.getRelationshipToPrimary().toString());			
			personDataList.add(personData);
		}
		
		GroupData groupData = new GroupData();
		groupData.setGroupId(0);
		groupData.setAptc(0f);
		groupData.setCsr(null);
		groupData.setZipcode(zipCode);
		groupData.setCountycode(countyCode);
		groupData.setPersonDataList(personDataList);
		PdResponse pdResponse = planDisplayWebHelper.invokePreShopping(null, InsuranceType.HEALTH, coverageStart, groupData);
		if(pdResponse ==null || pdResponse.getPdHouseholdDTO()==null){
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdResponse.getPdHouseholdDTO();
		PdSession pdSession = new PdSession();
		pdSession.setPdHouseholdDTO(pdHouseholdDTO);
		pdSession.setFlowType(FlowType.PREELIGIBILITY);
		pdSession.setPdPreferencesDTO(pdResponse.getPdPreferencesDTO());
			
		List<PdPersonDTO> pdPersonDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		pdSession.setPdPersonDTOList(pdPersonDTOList);
		planDisplayWebHelper.setCatastrophicEligibility(pdSession, pdPersonDTOList);
		pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);

		session.setAttribute("pdSession", pdSession);
	 	return "redirect:/private/preferences";
	}

	private String anonymousValidationError(String zip,
											String listdobs,
											String county,
											String coveragestart){

		//--- DOB validation ---
		if(StringUtils.isEmpty(listdobs))  {
			return "Missing required field : listdobs";
		}

		//--- zipcode validation ---
		if(StringUtils.isEmpty(zip))  {
			return "Missing required field : zip";
		}

		String zipCountyRegex = "^[0-9]{5}";
		Pattern pattern = Pattern.compile(zipCountyRegex);
		Matcher matcher = pattern.matcher(zip);

		if(!matcher.matches()) {
			return "Invalid data in required field : zip";
		}

		//--- coverage start date validation ---
		if(StringUtils.isEmpty(coveragestart))  {
			return "Missing required field : coveragestart";
		}

		String dateRegex = "^[0-9]{8}";
		Pattern datePattern = Pattern.compile(dateRegex);
		Matcher dateMatcher = datePattern.matcher(coveragestart);
		if(!dateMatcher.matches())  {
			return "Invalid data in required field : coveragestart";
		}

		Date coverageStart = DateUtil.StringToDate(coveragestart, "MMddyyyy");
		if(coverageStart == null)  {
			return "Invalid data in required field : coveragestart";
		}

		//--- county code validation ---
		if(StringUtils.isEmpty(county))  {
			return "Missing required field : county";
		}

		matcher = pattern.matcher(county);
		if(!matcher.matches()) {
			return "Invalid data in required field : county";
		}

		final String[] dobsList = listdobs.split(",");
		for (final String element : dobsList)
		{
			dateMatcher = datePattern.matcher(element);
			if(!dateMatcher.matches())  {
				return "Invalid data in required field : listdobs";
			}

			Date dob = DateUtil.StringToDate(element, "MMddyyyy");
			if (dob == null) {
				return "Invalid data in required field : listdobs";
			}
		}

		return null;
	}
	
	@RequestMapping(value="/anonymous",method = RequestMethod.GET)
	public String saveAnonymous(final HttpServletRequest request,
								final Model model,
								@RequestParam String zip,
								@RequestParam String listdobs,
								@RequestParam(required = false) String aptc,
								@RequestParam(required = false) BigDecimal statesubsidy,
								@RequestParam(required = false) String csr,
								@RequestParam String county,
								@RequestParam String coveragestart)
	{	
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveAnonymous GET Method---->", null, false);
		long startTime = planDisplayUtil.startMethodLog("saveAnonymous");
		LOGGER.info("anonymous request:", zip, listdobs, aptc, statesubsidy, csr, county, coveragestart);

		HttpSession session = request.getSession();
		if(session == null){
			session = request.getSession(true);
		}
		final List<Map<String,String>> personList = new ArrayList<Map<String,String>>();
		String validationError = anonymousValidationError(zip,listdobs,county,coveragestart);
		if (validationError != null){
			model.addAttribute("errMassage", validationError);
			return "private/errorPage";
		}

		listdobs = listdobs.trim();
		final String[] dobsList = listdobs.split(",");
		Date coverageStart = DateUtil.StringToDate(coveragestart, "MMddyyyy");

		final List<Date> dobsDateList = new ArrayList<Date>();
		for (final String element : dobsList) {
			Date dob = DateUtil.StringToDate(element, "MMddyyyy");
			dobsDateList.add(dob);
		}

		boolean childFlag = false;
		for (int i = 0; i < dobsDateList.size(); i++)
		{
			final Map<String, String> personMap = new HashMap<String, String>();

			personMap.put("pldPersonId", "" + i);
			personMap.put(PlanDisplayUtil.PARAM_FIRST_NAME, "");
			personMap.put(PlanDisplayUtil.PARAM_LAST_NAME, "");
			personMap.put(PlanDisplayUtil.PARAM_ZIPCODE, zip);
			personMap.put(PlanDisplayUtil.PARAM_DOB, DateUtil.dateToString(dobsDateList.get(i), GhixConstants.REQUIRED_DATE_FORMAT));
			personMap.put(PlanDisplayUtil.PARAM_COUNTY_CODE, county);

			personMap.put(PlanDisplayUtil.PARAM_SMOKER, "N");
			personList.add(personMap);
			if (!childFlag) {
				childFlag = planDisplayUtil.getChildFlag(dobsDateList.get(i),coverageStart);
			}
		}
		
		Collections.sort(dobsDateList);
		for (int j = 0; j < personList.size(); j++) {
			if (dobsDateList.size() > 0
					&& personList.get(j).containsValue(
							DateUtil.dateToString(dobsDateList.get(0),
									GhixConstants.REQUIRED_DATE_FORMAT))) {
				personList.get(j).put(PlanDisplayUtil.PARAM_RELATIONSHIP, PlanDisplayConstants.SELF);
			}
		}
		for (int j = 0; j < personList.size(); j++) {
			if (dobsDateList.size() > 1
					&& personList.get(j).containsValue(
							DateUtil.dateToString(dobsDateList.get(1),
									GhixConstants.REQUIRED_DATE_FORMAT))) {
				personList.get(j).put(PlanDisplayUtil.PARAM_RELATIONSHIP, PlanDisplayConstants.SPOUSE);
			}
		}
		for (int j = 0; j < personList.size(); j++) {
			if (personList.get(j).get(PlanDisplayUtil.PARAM_RELATIONSHIP) == null) {
				personList.get(j).put(PlanDisplayUtil.PARAM_RELATIONSHIP, PlanDisplayConstants.CHILD);
			}
		}
		
				
		/*Setting the ANONYMOUS_PERSON_LIST to session*/
		List<PersonData> personDataList = planDisplayUtil.extractPersonList(personList);
		float aptcValue = 0;
			
		if(aptc != null )
		{	

			try{
				aptcValue = BigDecimal.valueOf(Float.valueOf(aptc)).setScale(2,RoundingMode.HALF_UP).floatValue();
			}	
			catch(final NumberFormatException nfe)
			{
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception : ---->", nfe, false);
			}			
		}



		GroupData groupData = new GroupData();
		groupData.setGroupId(0);
		groupData.setAptc(aptcValue);

		String stateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
		if ("Y".equalsIgnoreCase(stateSubsidyEnabled)) {
			groupData.setStateSubsidy(statesubsidy);
		}
		groupData.setCsr(csr);
		groupData.setZipcode(personList.get(0).get("ZIPCODE"));
		groupData.setCountycode(personList.get(0).get("countyCode"));
		groupData.setPersonDataList(personDataList);
		PdResponse pdResponse = planDisplayWebHelper.invokePreShopping(null, InsuranceType.HEALTH, coverageStart, groupData);
		planDisplayUtil.endMethodLog("saveAnonymous", startTime);
		if(pdResponse ==null || pdResponse.getPdHouseholdDTO()==null){
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdResponse.getPdHouseholdDTO();
		PdSession pdSession = new PdSession();
		pdSession.setPdHouseholdDTO(pdHouseholdDTO);
		pdSession.setFlowType(FlowType.PREELIGIBILITY);
		pdSession.setPdPreferencesDTO(pdResponse.getPdPreferencesDTO());
			
		List<PdPersonDTO> pdPersonDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		pdSession.setPdPersonDTOList(pdPersonDTOList);
		planDisplayWebHelper.setCatastrophicEligibility(pdSession, pdPersonDTOList);
		pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);

		session.setAttribute("pdSession", pdSession);
	 	return "redirect:/private/preferences";
	}	
	
	@RequestMapping(value = "/getCountyList",  method = RequestMethod.POST)
	public @ResponseBody String getCountyList(final HttpServletRequest request, final HttpServletResponse response) 
	{
		long startTime = planDisplayUtil.startMethodLog("getCountyList");
		final String strZip = request.getParameter("zip");
		//LOGGER.info("The Zip Code is: " + strZip);
		final Gson gson = platformGson;
		final Map<String,String> countyMap =  new HashMap<String, String>();

		List<ZipCode> zipcodes = zipCodeService.findByZip(strZip);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----zipcode size---->", null, false);
		for (final ZipCode zipCodeObj : zipcodes) {
			if(StringUtils.isNotEmpty(zipCodeObj.getCounty())){
				countyMap.put(zipCodeObj.getCounty(), zipCodeObj.getCountyCode());
			}
		}
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, countyMap.size()+"<----county found---->", null, false);
		response.setContentType("application/json");
		planDisplayUtil.endMethodLog("getCountyList", startTime);
		return gson.toJson(countyMap);
	}
	

}
