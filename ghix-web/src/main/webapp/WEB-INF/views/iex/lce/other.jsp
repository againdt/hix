<script type="text/ng-template" id="other.jsp">
<div class="gutter10" ng-controller="otherController">
	<form class="form-horizontal" name="otherReasonPage">

				<div class="control-group">
					<label class="control-label" for="dateOfEvent1">Event Date<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!"></label>
					<div class="controls">
						<div id="date" class="input-append" name="dateOfEventOther" ng-model="currentEvent.dateOfChange" datetimez="" required >
							<input type="text" id="dateOfEvent1" name="dateOfEvent" class="span12 ng-pristine ng-valid ng-valid-mask"  placeholder="MM/DD/YYYY" ng-model="currentEvent.dateOfChange" />
							<span class="add-on">
								<i class="icon-calendar"></i>
							</span>
						</div>
						<div class="clearfix">
							<span class="validation-error-text-container" ng-show="submitted && (otherReasonPage.dateOfEventOther.$error.required)"><spring:message code="label.lce.plsEnterEventDate"/></span>
						</div>
					</div>
				</div>

			<div class="control-group margin30-t">
				<h4><spring:message code="label.lce.selectApplicableEvent"/>:</h4>
				<div class="margin40-l margin40-r">
					<div class="radio-inline">
						<label for="opt1">
							<input type="radio" name="otherreason" id="opt1" value="{{otherEventArray[0].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[0].label}}
						</label>
					</div>
					<div class="alert alert-info margin10-t margin20-l">
						<p>
							<spring:message code="label.lce.seriousCon"/>:
						</p>
						<ul>
							<li><spring:message code="label.lce.tempDisablility"/></li>
							<li><spring:message code="label.lce.naturalDisaster"/></li>
						</ul>
					</div>
				</div>
				<div class="margin40-l margin40-r">
					<div class="radio-inline">
						<label for="opt2">
							<input type="radio" name="otherreason" id="opt2" value="{{otherEventArray[1].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[1].label}}
						</label>
					</div>
					<div class="alert alert-info margin10-t margin20-l">
						<p>
							<spring:message code="label.lce.insCoMisconduct"/>: 
						</p>
						<ul>
							<li><spring:message code="label.lce.insCoMisconductPt1"/></li>
							<li><spring:message code="label.lce.insCoMisconductPt2"/></li>
							<li><spring:message code="label.lce.insCoMisconductPt3"/></li>
						</ul>
					</div>
				</div>
				<div class="margin40-l margin40-r">
					<div class="radio-inline">
						<label for="opt3">
							<input type="radio" name="otherreason" id="opt3" value="{{otherEventArray[2].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[2].label}}
						</label>
					</div>
					<div class="alert alert-info margin10-t margin20-l">
						<p>
							<spring:message code="label.lce.techErrYHI"/>
						</p>
					</div>
				</div>
				<div class="margin40-l margin40-r margin10-t">
					<div class="radio-inline">
						<label for="opt5">
							<input type="radio" name="otherreason" id="opt5" value="{{otherEventArray[4].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[4].label}}
						</label>
					</div>
				</div>
				<div class="margin40-l margin40-r margin10-t">
					<div class="radio-inline">
						<label for="opt6">
							<input type="radio" name="otherreason" id="opt6" value="{{otherEventArray[5].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[5].label}}
						</label>
					</div>
				</div>
				<div class="margin40-l margin40-r margin10-t">
					<div class="radio-inline">
						<label for="opt7">
							<input type="radio" name="otherreason" id="opt7" value="{{otherEventArray[6].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[6].label}}
						</label>
					</div>
				</div>
				<div class="margin40-l margin40-r margin10-t">
					<div class="radio-inline">
						<label for="opt9">
							<input type="radio" name="otherreason" id="opt9" value="{{otherEventArray[7].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[7].label}}
						</label>
					</div>
					<div class="alert alert-info margin10-t margin20-l">
						<p>
							<spring:message code="label.lce.domesticProblem"/>
						</p>
					</div>
				</div>
				<div class="margin40-l margin40-r">
					<div class="radio-inline">
						<label for="opt10">
							<input type="radio" name="otherreason" id="opt10" value="{{otherEventArray[8].name}}" ng-model="eventInfo.eventSubCategory" required ng-click="addChangedApplicants(1);" />
							{{otherEventArray[8].label}}
						</label>
					</div>
					<div class="alert alert-info margin10-t margin20-l">
						<p>
							<spring:message code="label.lce.otherError"/>
						</p>
					</div>
				</div>
				<div class="margin40-l margin40-r">
					<span class="validation-error-text-container" ng-show="submitted && (otherReasonPage.otherreason.$error.required)"><spring:message code="label.lce.eventReasonReq"/></span>
				</div>
			</div>
	</form>

	<dl class="dl-horizontal pull-right">
		<a class="btn btn-secondary" ng-click="back()">Back</a>
		<a class="btn btn-primary" ng-click="eventInfo.eventCategory = otherEventArray[0].category;eventInfoArrayTemp[0].otherEventDate=currentEvent.dateOfChange ; updateDates(); next();" ng-if="otherReasonPage.$valid">
			<spring:message code="label.lce.next"/>
		</a>
		<a class="btn btn-secondary" ng-click="$parent.submitted=true;" ng-if="!otherReasonPage.$valid">
			<spring:message code="label.lce.next"/>
		</a>
	</dl>
</div>
</script>
