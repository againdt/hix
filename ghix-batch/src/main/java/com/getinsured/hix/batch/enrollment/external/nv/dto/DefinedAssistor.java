package com.getinsured.hix.batch.enrollment.external.nv.dto;

public class DefinedAssistor {
	 private String definedAssistorReferenceTypeCodeName;


	 // Getter Methods 

	 public String getDefinedAssistorReferenceTypeCodeName() {
	  return definedAssistorReferenceTypeCodeName;
	 }

	 // Setter Methods 

	 public void setDefinedAssistorReferenceTypeCodeName(String definedAssistorReferenceTypeCodeName) {
	  this.definedAssistorReferenceTypeCodeName = definedAssistorReferenceTypeCodeName;
	 }
	}