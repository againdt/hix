<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%-- <script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script> --%>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript"> window.history.forward(); </script>


	
	<div class="gutter10">
		<div class="page-header">
			<h1><spring:message  code="label.setupcompany"/><small> <spring:message  code="label.worksitesh2"/></small></h1>
		</div>
	
	<div class="row-fluid">	
		<!-- add id for skip side bar -->
		<div class="span3" id="sidebar">
			<util:nav pageName="Enter Worksites" navStepNo="1"></util:nav>
			
		</div>	
		<div class="span9" id="rightpanel">
				<form class="form-horizontal" id="frmworksites" name="frmworksites" action="createworksites" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="id" id="id" value="${employerOBJ.id}"/>
				
				<c:set var="location_id" value="${not empty empPrimaryLocationObj.location.id ? empPrimaryLocationObj.location.id : '0'}"/>
				<input type="hidden" id="id_0" name="locations[0].location.id" value="${location_id}"/>
				<c:set var="employer_location_id" value="${not empty empPrimaryLocationObj.id ? empPrimaryLocationObj.id : '0'}"/>  
				<input type="hidden" id="employer_location_id_0" name="locations[0].id" value="${employer_location_id}"/>
				<input type="hidden" id="location_FID" name="location[]" value="FID"/>
				<input type="hidden" id="lat_0" name="locations[0].location.lat" value="0.0" />
				<input type="hidden" id="lon_0" name="locations[0].location.lon" value="0.0" />
				<input type="hidden" id="rdi_0" name="locations[0].location.rdi" value="" />
				<input type="hidden" id="countycode_0" name="locations[0].location.countycode" value="" />
				<input type="hidden" id="primaryLocation_0" name="locations[0].primaryLocation" value="YES" />
				 	 <div class="header margin10-b">
						<h4><spring:message  code="label.worksitesh3"/> ${stateName}</h4>
					</div>
					<div class="profile addressBlock">
						<div class="control-group">
							<label for="address1_0" class="control-label required"><spring:message code="label.employeraddress1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="" height=" "alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="locations[0].location.address1" id="address1_0"  value="${empPrimaryLocationObj.location.address1}"  class="input-xlarge" size="30">
								<div id="address1_0_error"></div>	
								<input type="hidden" id="address1_0_hidden" name="address1_0_hidden">		
							</div>
						</div>
						
						<div class="control-group">
							<label for="address2_0" class="control-label required"><spring:message code="label.employeraddress2"/></label>
							<div class="controls">
								<input type="text"  name="locations[0].location.address2" id="address2_0"   value="${empPrimaryLocationObj.location.address2}" class="input-xlarge" size="30">
								<input type="hidden"  id="address2_0_hidden" name="address2_0_hidden">
							</div>
						</div>
						
						<div class="control-group">
							<label for="city_0" class="control-label required"><spring:message code="label.employercity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="locations[0].location.city" id="city_0" class="input-xlarge"  value="${empPrimaryLocationObj.location.city}">
								<div id="city_0_error"></div>	
								<input type="hidden" id="city_0_hidden" name="city_0_hidden">		
							</div>
						</div>
						
						<div class="control-group">
							<label for="state_0" class="control-label required"><spring:message code="label.employerstate"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium" name="locations[0].location.state" id="state_0"  >
									 <option id="${defaultStateCode}" value="${defaultStateCode}">${stateName}</option>
								</select>
								<div id="state_0_error"></div>		
								<input type="hidden" id="state_0_hidden" name="state_0_hidden" >	
							</div>
						</div>
						<div class="control-group">
							<label for="zip_0" class="control-label required"><spring:message code="label.employerzipcode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text"  name="locations[0].location.zip" id="zip_0" maxlength="5" class="input-small zipCode"  value="${empPrimaryLocationObj.location.zip}">
								<div id="zip_0_error"></div>	
								<input type="hidden" id="zip_0_hidden" name="zip_0_hidden" >		
							</div>
						</div>
						<div class="control-group" id="county_div_0" style="">
							<label for="county_0" class="control-label required"><spring:message  code="label.country"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
							<div class="controls">
								<select class="input-large" name="locations[0].location.county"  id="county_0">
								<option value="">Select County...</option>
								</select>
								<div id="county_0_error"></div>		
							</div>
						</div>
						<div class="control-group" id="validateZipDiv" class="fade" style="display:none">
							<div class="controls">
								<div class="error help-inline"><label class="error"><span><em class="excl">!</em> The primary worksite address must be in ${stateName} to participate in ${exchangeName}. <!-- <input type="button" value="OK" onclick="closeDiv();"/> --></span></label> </div>		
							</div>
						</div>
					</div>
						
						<div id="locationsBox"></div>
						<div class="gutter10">
							<input id="addlocation" class="btn btn-small" type="button" name="addlocation" 	value="<spring:message  code='label.employeraddworksite'/>" onclick="employer_location.add_worksite('', '${defaultStateCode}');">
					
							<%-- <small>&nbsp;<spring:message  code='label.employeraddadrworksite'/></small> --%>
							<small class="span9 pull-right">
									<%-- If you plan to offer ${exchangeName} coverage to employees who work at other locations, add those locations here. You may include out-of-state locations. --%>
									<spring:message  code='label.addworksitemessage1'/> ${exchangeName} <spring:message  code='label.addworksitemessage2'/>
							</small>
						</div>
						<div class="form-actions">
							<input type="submit" name="submitbutton" id="submitbutton" class="btn btn-primary pull-right" value="<spring:message  code='label.next'/>" title="<spring:message  code='label.next'/>"/>
						</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
function populateStateList(){
	var stateOptions = "";
	<c:forEach var="state" items="${statelist}">
		stateOptions += "<option id='${state.code}' value='${state.code}'>${state.name}</option> ";
	</c:forEach>
	
	return stateOptions;
}

function deleteEmployerLocation(employerlocationid, index) {
	var subUrl = '<c:url value="/shop/employer/deletelocation">
					<c:param name="${df:csrfTokenParameter()}"> 
						<df:csrfToken plainToken="true" />
					</c:param> 
				</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data: {employerLocationId: employerlocationid}, 
		success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
			 if(response.toString().indexOf('Success') != -1){
			 	  $('#employer_location_'+index).remove(); 
			 }else{
			 	alert(response);
			 }
		},
		error : function(e) {
			alert("<spring:message code='label.employerfailedtodelete' javaScriptEscape='true'/>");
		}
	});
}

function getCountyList(eIndex, zip, county) {
	var subUrl =  '<c:url value="/shop/employer/addCounties"><c:param name="${df:csrfTokenParameter()}"><df:csrfToken plainToken="true" /></c:param></c:url>'; 
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response, xhr) {
			if(isInvalidCSRFToken(xhr))                    
                return;
			populateCounties(response, eIndex, county);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, eIndex,county){	
	//console.log(response, eIndex,county,"populate counties")
	$('#county_'+eIndex).html('');
	var optionsstring = '<option value="">Select County...</option>';
	var i =0;
	$.each(response, function(key, value) {
		var optionVal = key+'#'+value;
		var selected = (county == key) ? 'selected' : '';
		var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
		optionsstring = optionsstring + options;
		i++;
	});

	$('#county_'+eIndex).html(optionsstring);
}

function validateZipCode(eindex,zipval){
	var responseVal="";
	var subUrl = '<c:url value="/shop/employer/validateZipCode">
					<c:param name="${df:csrfTokenParameter()}"> 
						<df:csrfToken plainToken="true" />
					</c:param> 
				</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zipval}, 
		dataType:'json',
		success : function(response,xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			if(response=='NO'){
				$('#validateZipDiv').show();
				$('#county_'+eindex).html('');
			}
			else{
				$('#validateZipDiv').hide();
				getCountyList(eindex, zipval, '');
			}
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
	return responseVal;
}
function closeDiv(){
	$('#validateZipDiv').hide();
}
</script>

<script type="text/javascript" src="<c:url value="/resources/js/employer_location.js" />"></script>  

<script type="text/javascript">
jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
    if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}return true;
});


var validator = $('#frmworksites').validate({ 
	rules : {
		'locations[0].location.address1' : { required : true},
		'locations[0].location.zip' : { required : true, zipcheck:true},
		'locations[0].location.city' : {required: true},
		'locations[0].location.state' : {required: true},
		'locations[0].location.county' : {required: true}
		
	},
	messages : {
		'locations[0].location.address1' : { required : "<span> <em class='excl'>!</em> <spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		'locations[0].location.zip' : { 
					required : "<span> <em class='excl'>!</em> <spring:message code='label.validateZip' javaScriptEscape='true'/></span>" ,
					zipcheck : "<span><em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"
				 },
		'locations[0].location.city' : {required : "<span> <em class='excl'>!</em> <spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		'locations[0].location.state' : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validateState' javaScriptEscape='true'/></span>" },
		'locations[0].location.county' : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validatecounty' javaScriptEscape='true'/></span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

function validateAddress(eindex){
	
	var address1_e='address1_'; var address2_e='address2_'; var city_e= 'city_'; var state_e='state_'; var zip_e='zip_';
	var lat_e='lat_';var lon_e='lon_'; var rdi_e='rdi_'; var county_e='county_';
	address1_e=address1_e+eindex;
	address2_e=address2_e+eindex;
	city_e=city_e+eindex;
	state_e=state_e+eindex;
	zip_e=zip_e+eindex;
	lat_e=lat_e+eindex;
	lon_e=lon_e+eindex;
	rdi_e=rdi_e+eindex;
	county_e=county_e+eindex;

	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
	
	
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
		if(($('#'+ address2_e).val())==="Address Line 2"){
			$('#'+ address2_e).val('');
		}	
		 viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);
		//console.log(eindex,'eindexeindexeindexeindexeindex');	 
	}
	
	
}

function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue, zipId);
	//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue);  
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( indexValue,zipCodeValue, '' );
}

$(document).ready(function(){
	//$('#zip_0').bind("focusout",function () {validateAddress(0);});
	var primeAddDone=false;
	var index = 0;
	var empLocationJson = ${empLocationJson};
	if(empLocationJson != ""){
		for(var i=1; i<=empLocationJson.length; i++){
		    var empLocationObj = empLocationJson[i-1];
		    if(empLocationObj['primary'] == 'YES'){
		    	index = 0;
		    	primeAddDone=true;
		    }else{
		    	index = primeAddDone ? i-1 : i;
		    	employer_location.add_worksite(empLocationObj, '');
		    }
		    getCountyList(index, empLocationObj['zip'], empLocationObj['county']);
		};
	};
	
	$(document).on('onGetCountyList',getCountyList);
});

</script>