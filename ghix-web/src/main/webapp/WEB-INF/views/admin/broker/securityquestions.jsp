<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 



<div class="gutter10-lr">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p>
						<c:out value="${errorMsg}"></c:out>
					<p />
				</c:if>
				<br>
			</div>
			
		</div><!--  end of row-fluid -->
	</div><!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1><a name="skip"></a>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">						
				<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				
				<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
				<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
				<li  class="active"><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
			</div>
			<ul class="nav nav-list">
			<c:if test="${checkDilog == null}">  
				<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
			</c:if>
			<c:if test="${checkDilog != null}">  
				<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
			</c:if>
            </ul>   
		</div><!-- end of span3 -->
		
		<div id="rightpanel" class="span9">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.brkSecurityQuestions"/></h4>
			</div>
			<div class="gutter10">
				<form class="form-horizontal" id="" name="" action="" method="POST">
					<table class="table table-border-none verticalThead">

						<tbody>
							<tr>
								<td class="txt-left span4"><strong> <spring:message code="label.brkSecurityQuestions1"/></strong></td>
							</tr>
							<tr>
								<td>${broker.user.securityQuestion1}</td>
							</tr>
							<tr>
								<td class="txt-left span4"><spring:message code="label.bkr.Answer"/></td>
							</tr>
							<tr>
								<td>${broker.user.securityAnswer1}</td>
							</tr>
						</tbody>
					</table>
				<input type="hidden" name="brokerName" id="brokerName" value="${broker.user.firstName}&nbsp;${broker.user.lastName}" />
				</form>
			</div>
		</div>
		
</div>
</div>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>
 
  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>