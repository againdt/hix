package com.getinsured.iex.im.wso2;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.im.wso2.wrappers.SetMultipleUserClaimsWrapper;

public class SetMultipleUserAttributesHandler extends AbstractServiceHandler {

	private boolean handleResponseCalled = false;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		SetMultipleUserClaimsWrapper wrapper = SetMultipleUserClaimsWrapper.fromJson(getJsonInput());
		if(wrapper == null){
			throw new HubServiceException("Failed to initialize the wrapper");
		}
		return wrapper.getSystemRepresentation();
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		// TODO Auto-generated method stub
		handleResponseCalled  = true;
		return "Success";
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.handleResponseCalled){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

}