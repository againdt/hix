import keyMirror from 'keymirror';

const PERIOD_TYPES = keyMirror({
    OE: null,
    OUTSIDE_OE: null
});

const APP_STATUSES = keyMirror({
    UC: null,
    CC: null,
    CL: null,
    EN: null,
    ER: null,
    PN: null,
    OP: null,
    SU: null,
    SG: null,
    QU: null
});

const APP_TYPES = keyMirror({
    SEP: null,
    QEP: null,
    OE: null
});

const VALIDATION_STATUSES = keyMirror({
    NOTREQUIRED: null,
    PENDING: null,
    PENDING_INFO: null
});

const FURTHER_ACTION_LINKS = keyMirror({
    START_NEW_APPLICATION: null,
    RESUME_APPLICATION: null,
    GROUPING_SCREEN: null,
    GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH: null,
    REPORT_CHANGE: null,
    REPORT_LIFE_EVENT_SHOP: null,
    UPLOAD_DOCUMENTS: null,
    CONFIRM_EVENT: null,
    VERIFICATION: null,
    SHOPPING_CART: null,
    CONTINUE: null,
    CONTACT_US: null
});

const SUPER_USERS = ['L2_CSR', 'SUPERVISOR', 'ADMIN'];

const APP_STATUS_MESSAGES = {
    NOT_STARTED: 'application_status.status.not_started',
    IN_PROGRESS: 'application_status.status.in_progress',
    COMPLETE: 'application_status.status.complete'
};
const APP_STATUS_LINKS = {
    START_APP: 'application_status.action.start_application',
    CONTINUE_APP: 'application_status.action.continue_application',
    VIEW_APP: 'application_status.action.view_application',
};

const RENEWAL_STATUSES = keyMirror({
    TO_CONSIDER: null,
    NOT_TO_CONSIDER: null,
    MANUAL_ENROLLMENT: null,
    RENEWED: null,
    PARTIAL_RENEWED: null,
    APP_ERROR: null
});

const WARNING_TYPES = keyMirror({
    WARNING: null,
    ALERT: null,
    ANNOUNCEMENT: null
});

export {
    PERIOD_TYPES,
    APP_STATUSES,
    APP_TYPES,
    VALIDATION_STATUSES,
    FURTHER_ACTION_LINKS,
    SUPER_USERS,
    APP_STATUS_MESSAGES,
    APP_STATUS_LINKS,
    RENEWAL_STATUSES,
    WARNING_TYPES
}