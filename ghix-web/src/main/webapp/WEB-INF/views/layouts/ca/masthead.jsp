<%@page import="com.getinsured.hix.model.UserRole"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page session="true" %>
<%@page import="java.util.Set"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

<%
String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
String isUserFullyAuthorized =  (String) request.getSession().getAttribute("isUserFullyAuthorized");
String isUserSwitchToOtherView = (String)request.getSession().getAttribute("isUserSwitchToOtherView");
Set<UserRole> userRoles = (Set<UserRole>)request.getSession().getAttribute("userConfiguredRoles");
String userActiveRoleLabel =  (String) request.getSession().getAttribute("userActiveRoleLabel");
 String householdCaseID =  (String) request.getSession().getAttribute("externalHouseholdCaseId");
	/*
	 * START:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName
	 */
	 String myAccountUrl= GhixConstants.MY_ACCOUNT_URL;
	 String myInboxUrl= GhixConstants.MY_INBOX_URL;
	 String userName =  (String) request.getSession().getAttribute("USER_NAME");
	 String logoutUrl = request.getContextPath()+"/account/user/logout";
	 String logoUrl = (String)request.getSession().getAttribute("userLandingPage");
	 String chatUrl = GhixConstants.CHAT_URL;
  	 String mastHeadHome= GhixConstants.MASTER_HEAD_HOME;

	if(StringUtils.isBlank(myAccountUrl)){
		myAccountUrl="#";
	}
	if(StringUtils.isNotBlank(myInboxUrl) && StringUtils.isNotBlank(userName)) {
		if ( GhixConstants.MY_INBOX_URL.contains("?")){
			myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
		}else{
			myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
		}
	}else{
		myInboxUrl="#";
	}

	 String switchToModuleNameLocal=(String)request.getSession().getAttribute("switchToModuleName");
	 String showFindAgentInEm="FALSE";
	 if(   ("Y".equalsIgnoreCase(isUserSwitchToOtherView)) && "employer".equalsIgnoreCase(switchToModuleNameLocal) ){
		 showFindAgentInEm="TRUE";
	 }

	if(StringUtils.isBlank(logoutUrl)){
		logoutUrl="#";
	}

	if(StringUtils.isBlank(isUserSwitchToOtherView)){
		isUserSwitchToOtherView="N";
	}

	if(StringUtils.isBlank(chatUrl)){
		chatUrl="#";
	}
	String 	faqUrl = "/faq/home";

	if(StringUtils.isNotBlank(userActiveRoleName)){
		if(StringUtils.equalsIgnoreCase(userActiveRoleName, "admin") || StringUtils.equalsIgnoreCase(userActiveRoleName, "operations") || StringUtils.equalsIgnoreCase(userActiveRoleName, "csr")){
			faqUrl= "/faq/admin";
		}else if(StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer") || StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer_representative")){
			faqUrl= "/faq/issuer";
		}else{
			faqUrl= "/faq/"+userActiveRoleName;
		}
    }
    
    faqUrl = "https://www.coveredca.com/find-help/faqs/";
    
	/*
	 * END:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName
	 */



%>
<c:set var="userName" value="<%=userName%>" />
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
<c:set var="isUserFullyAuthorized" value="<%=isUserFullyAuthorized%>" />
<c:set var="isUserSwitchToOtherView" value="<%=isUserSwitchToOtherView%>" />
<c:set var="logoUrl" value="<%=logoUrl%>" />
<c:set var="logoutUrl" value="<%=logoutUrl%>" />
<c:set var="landingPageUrl" value="<%=logoUrl%>" />
<c:set var="faqUrl" value="<%=faqUrl%>" />
<c:set var="userRoles" value="<%=userRoles%>" />
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<c:set var="showFindAgentInEm" value="<%=showFindAgentInEm%>" />
<c:set var="mastHeadHome" value="<%=mastHeadHome%>" />
<c:set var="householdCaseID" value="<%=householdCaseID%>" />
<c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />


<script type="text/javascript">

$(document).ready(function() {
	$('a.lang').click(function (){

	var val= $(this).attr('id');

	var currUrl = window.location.href;
	var newUrl="";

	///alert(val);
	/* Check if Query String parameter is set or not
	* If yes
	*/
	if(currUrl.indexOf("?", 0) > 0)
	{
		/* Check if locale is already set without querystring param  */
		 if(currUrl.indexOf("?lang=", 0) > 0)
			  newUrl = "?lang="+val;
		/*  Check if locale is already set with querystring param  */
		 else if(currUrl.indexOf("&lang=", 0) > 0)
			 newUrl = currUrl.substring(0, currUrl.length-2)+val;
		 else
			  newUrl = currUrl + "&lang="+val;
	}/* If No  */
	else
		newUrl = currUrl + "?lang="+val;
		window.location = newUrl;
	});
});

</script>
<sec:authorize access="isAuthenticated()" var="isUserAuthenticated">
  <script type="text/javascript">
	function countUnreadMessages(currentTime) {

		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxUnread";
		var csrftoken;
		csrftoken= '${sessionScope.csrftoken}';

		$.post(pathURL,{ csrftoken:csrftoken,currentTime:currentTime}, function(data) {
				//HIX-38087
				if(data != null && data>0) {
					//$(".unreadRecords").show();
					$(".unreadRecords").removeClass("superHide");
					$(".unreadRecords").html(data);
				}
				else {
					//$(".unreadRecords").hide();//.html("0");
					$(".unreadRecords").addClass("superHide");

					/* HIX-45273 */
					$("#notificationMessages .unreadRecords").html(0).removeClass("superHide");
				}
			});
			//return data;
		}
		$(document).ready(function(){
			var secureInboxUnread = '${sessionScope.unreadRecords}';
			var currentTime = new Date().getTime();
			var lastUpdatedTime = '${sessionScope.lastUpdatedTime}';
		    var counter = currentTime-lastUpdatedTime;
			if(secureInboxUnread == null || secureInboxUnread =='' || counter >= 300000){
				countUnreadMessages(currentTime);
			}else{
				$(".unreadRecords").removeClass("superHide");
				$(".unreadRecords").html(secureInboxUnread);
			}
		});
		//setInterval(countUnreadMessages, 10000);
</script>
</sec:authorize>
<%
	/* if session attribute is not set  */
	if(request.getParameter("lang") == null && session.getAttribute("lang") == null){
		session.setAttribute("lang","en");
	}
	else if(request.getParameter("lang") !=null && request.getParameter("lang") != session.getAttribute("lang")   ){
			session.setAttribute("lang", request.getParameter("lang"));
	}

	String enval=null;String esval=null;


	if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("en")  )
		enval="selected";
	else if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("es"))
		esval="selected";
	else
		enval="";
%>
<c:set var="brand_image" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"></c:set>
<input type="hidden" value="<%=session.getAttribute("lang")%>" id="lang">
<input type="hidden" id="exchangeName" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>">
<input type="hidden" id="stateCode" name="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>"/>
<input type="hidden" id="stateSubsidyEnabled" name="stateSubsidyEnabled" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY)%>"/>
<input type="hidden" id="isUserAuthenticated" value="${isUserAuthenticated=='true'}"/>
<input type="hidden" id="userActiveRoleName" value="${userActiveRoleName}"/>
<input type="hidden" id="calheersEnv" value="${calheersEnv}"/>
<input type="hidden" id="householdCaseID" name="householdCaseID" value="<%=householdCaseID%>">
<input type="hidden" id="userName" value="${userName}"/>

<!-- ********************** End Setting up session Variable ********************** cs-->

<div class="sscreen">
    <div class="navbar navbar-inverse" id="masthead" role="banner">

        <c:if test="${userName != null}">
            <!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
            <!--  This is an ADMIN MENU, located above the MAIN masthead menu  -->
            <c:if test="${userRoles.size() > 1}">
                <div id="first-menu-bgcolor-nm"></div>
            </c:if>
        </c:if>
  	    <div class="navbar-inner">
            <div class="container">
                <div class="nav logo">
                    <c:if test="${userName == null}">
                        <c:if test="${stateCode !='CA'}">
                            <a href="<c:url value='${mastHeadHome}'/>" class="mast-logo">
                                <img class="brand" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                            </a>
                        </c:if>
                        <c:if test="${stateCode =='CA'}">
                            <img class="brand" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                        </c:if>
                    </c:if>

                    <c:if test="${userName != null}">
                        <c:choose>
                            <c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
                                <c:if test="${stateCode !='CA'}">
                                    <a href="#">
                                        <img class="brand masthead-img" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span>
                                    </a>
                                </c:if>
                                <c:if test="${stateCode =='CA'}">
                                     <img class="brand masthead-img" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${stateCode !='CA'}">
                                    <a href="<c:url value='${mastHeadHome}'/>" class="mast-logo">
                                        <img class="brand" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                                    </a>
                                </c:if>
                                <c:if test="${stateCode =='CA'}">
                                    <img class="brand" src="<gi:cdnurl value='/resources/img/mobile_${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </div>
                <div class="nav-list pull-right">
                    <ul class="nav pull-right clearfix">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="" class="mobile-header-icon" src="<gi:cdnurl value='/resources/img/icon-globe.png' />"></a>
                            <ul class="dropdown-menu">
                                <li id="m-lang-es" class="divide">
                                    <a href="#" id="language-select-es">Espa&ntilde;ol</a>
                                </li>
                                <li id="m-lang-en">
                                    <a href="#" id="language-select-en">English</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="" class="mobile-header-icon" src="<gi:cdnurl value='/resources/img/icon-question.png' />"></a>
                            <ul class="dropdown-menu">
                                    <%--<li id="call"><a href="#">Call Us</a> </li>--%>
                                    <li id="m-call" class="divide"><a href="tel:1-800-300-1506"><spring:message code="pd.label.header.callus"/></a></li>
                                    <li id="chatpopup" class="foo divide"><a href="#"><spring:message code="pd.label.header.onlineChat"/></a></li>
                                    <li id="help" class="divide">
										<a class="icons" id="pop_findAgent_disabled" rel="tooltip" 
										data-original-title="<spring:message code="pd.label.header.findlocalhelptooltip"/>">
										<spring:message code="pd.label.header.findlocalhelp"/></a></li>
                                    <li id="faq" class="divide"><a href="https://www.coveredca.com/find-help/faqs/" target="_blank"><spring:message code="pd.label.header.faqs"/></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="" class="mobile-header-icon" src="<gi:cdnurl value='/resources/img/accountHome.svg' />"></a>
                            <ul class="dropdown-menu">
                                <c:if test="${userName != null}">
                                            <c:choose>

                                                <c:when test="${userActiveRoleName == 'individual' || userActiveRoleName == 'INDIVIDUAL' || userActiveRoleName == 'L2_CSR' || userActiveRoleName == 'l2_csr' || userActiveRoleName == 'L1_CSR'
                                                	|| userActiveRoleName == 'l1_csr' || userActiveRoleName == 'L0_READONLY' || userActiveRoleName == 'l0_readonly' || userActiveRoleName == 'SUPERVISOR' || userActiveRoleName == 'supervisor'}">
                                                    <li class="divide">
                                                        <a href="<c:url value='https://${calheersEnv}/static/lw-web/account-home?householdCaseId=${householdCaseID}'/>"><spring:message code="pd.label.header.accountHome"/></a>
                                                    </li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="divide">
                                                        <a href="<c:url value='${landingPageUrl}'/>"><spring:message code="pd.label.header.accountHome"/></a>
													</li>
													<li>
														<a href="javascript:void(0)" onclick="window.open('<%=DynamicPropertiesUtil.getPropertyValue("global.ca.myprofileurl")%>?languageCode=<%=session.getAttribute("lang") %>', '_self')"><spring:message code="pd.label.header.mySecurityProfile"/></a>
													</li>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:if test="${userActiveRoleName == 'broker' || userActiveRoleName == 'assister' || userActiveRoleName == 'assisterenrollmententity' || userActiveRoleName == 'issuer_representative' || userActiveRoleName == 'agency_manager'}">
                                                <li class="divide">
                                                    <a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a>
                                                </li>
											</c:if>
											
                                            <c:if test="${userRoles.size()==1}">
                                                <li class="login divide"><a href="${logoutUrl}" id="aid_logout_03" class="divide"><spring:message code="pd.label.header.logout"/></a></li><!-- all -->
                                            </c:if>
                                        </c:if>
                                <c:if test="${userName == null}">
                                <c:choose>
                                    <c:when test="${userRoles.size()==1}">
                                        <li class="login"><a href="${logoutUrl}" id="aid_logout_04" class="divide"><spring:message code="pd.label.header.logout"/></a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="login"><a href="<c:url value='/account/user/login'/>" class="divide"><spring:message code="pd.label.header.login"/></a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> <!-- sscreen ends -->

<div class="lscreen">
    <div class="navbar navbar-inverse" id="masthead" role="banner">
	    <c:if test="${userName != null}">
		<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
		<!--  This is an ADMIN MENU, located above the MAIN masthead menu  -->
            <c:if test="${userRoles.size() > 1}">
                <div id="first-menu-bgcolor-nm"></div>
            </c:if>
	    </c:if>
  	    <div class="navbar-inner">
		    <div class="container">
				<div class="nav logo">
					<c:if test="${userName == null}">
                        <c:if test="${stateCode !='CA'}">
                            <a href="<c:url value='${mastHeadHome}'/>" class="mast-logo">
                                <img class="brand" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                            </a>
                        </c:if>
                        <c:if test="${stateCode =='CA'}">
                            <img class="brand" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                        </c:if>
					</c:if>

					<c:if test="${userName != null}">
						<c:choose>
							<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
                                <c:if test="${stateCode !='CA'}">
                                    <a href="#" class="mast-logo">
                                        <img class="brand masthead-img" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span>
                                    </a>
                                </c:if>
                                <c:if test="${stateCode =='CA'}">
                                    <img class="brand masthead-img" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span>
                                </c:if>
							</c:when>
							<c:otherwise>
                                <c:if test="${stateCode !='CA'}">
                                    <a href="#" class="mast-logo">
                                        <img class="brand" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                                    </a>
                                </c:if>
                                <c:if test="${stateCode =='CA'}">
                                    <img class="brand" src="<gi:cdnurl value='/resources/img/${brand_image}' />"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
                                </c:if>
                            </c:otherwise>
						</c:choose>
					</c:if>
				</div>
		      	<div class="nav-list pull-right">

			        <ul class="nav pull-right clearfix">
                        <li id="lang">
                            <a href="#" id="language-select">Espa&ntilde;ol</a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="pd.label.header.help"/></a>
                            <ul class="dropdown-menu">
                                <li id="call"><spring:message code="pd.label.header.helpoverphone"/><br> 1-800-300-1506</li>
								<li id="chatpopup" class="foo divide"><a class="icons" href="#"><spring:message code="pd.label.header.onlineChat"/></a></li>
								<li id="help" class="divide"><a class="icons" id="pop_findAgent_disabled" rel="tooltip" 
									data-original-title="<spring:message code="pd.label.header.findlocalhelptooltip"/>">
									<spring:message code="pd.label.header.findlocalhelp"/></a></li>
                                <li id="faq" class="divide"><a class="icons" href="https://www.coveredca.com/find-help/faqs/" target="_blank"><spring:message code="pd.label.header.faqs"/></a></li>
                            </ul>
                        </li>
                        <c:if test="${userName != null}">
                            <c:choose>
                                <c:when test="${userActiveRoleName == 'individual' || userActiveRoleName == 'INDIVIDUAL' || userActiveRoleName == 'L2_CSR' || userActiveRoleName == 'l2_csr' || userActiveRoleName == 'L1_CSR' 
                                                	|| userActiveRoleName == 'l1_csr' || userActiveRoleName == 'L0_READONLY' || userActiveRoleName == 'l0_readonly' || userActiveRoleName == 'SUPERVISOR' || userActiveRoleName == 'supervisor'}">
                                    <li>
                                        <a href="<c:url value='https://${calheersEnv}/static/lw-web/account-home?householdCaseId=${householdCaseID}'/>"><spring:message code="pd.label.header.accountHome"/></a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li>
                                        <a href="<c:url value='${landingPageUrl}'/>"><spring:message code="pd.label.header.accountHome"/></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" onclick="window.open('<%=DynamicPropertiesUtil.getPropertyValue("global.ca.myprofileurl")%>?languageCode=<%=session.getAttribute("lang") %>', '_self')"><spring:message code="pd.label.header.mySecurityProfile"/></a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                            <c:if test="${userActiveRoleName == 'broker' || userActiveRoleName == 'assister' || userActiveRoleName == 'assisterenrollmententity' || userActiveRoleName == 'issuer_representative' || userActiveRoleName == 'agency_manager'}">
                                <li>
                                    <a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a>
                                </li>
                            </c:if>
                            <c:if test="${userRoles.size()==1}">
                                <li class="login"><button id="aid_logout_01" data-automation-id="aid_logout" class="btn login-btn logout aid_logout"><spring:message code="pd.label.header.logout"/></button></li><!-- all -->
                            </c:if>
                        </c:if>
                        <c:if test="${userName == null}">
                            <c:choose>
                                <c:when test="${userRoles.size()==1}">
                                <li class="login"><button id="aid_logout_02" data-automation-id="aid_logout" class="btn login-btn logout aid_logout"><spring:message code="pd.label.header.logout"/></button></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="login"><button id="aid_login_01" data-automation-id="aid_login" class="btn login-btn logIn"><spring:message code="pd.label.header.login"/></button></li>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </ul>
			    	<!-- </div> -->
		    	</div>
	    		<!-- nav-collapse ends -->
			</div>
        </div>
    </div>
</div> <!-- lscreen ends -->

<script type="text/javascript">
  			// toggle for languages.
$(document).ready(function(){

	if(!sessionStorage.getItem('language')) {
		sessionStorage.setItem('language','English');
		$('#language-select').text("Espa\xF1ol");
	}

	var language = sessionStorage.getItem('language');
	//set the initial language button value
	if (language ==="English") {
		$('#language-select').text("Espa\xF1ol");
	}
	else {
		$('#language-select').text("English");
	}

		$('#language-select').click(function(e) {
            e.preventDefault();
			var _this = $(this);
            var language = sessionStorage.getItem('language');

			if (language === "English") {
                changeLang('es', 'Spanish');
				_this.text('English');
				window.dataLayer.push({
					'event': 'languageChange',
					'eventCategory': 'Language Change',
					'eventAction': 'Button Click',
					'eventLabel': 'Espanol'
				});
			}
			else if(language === "Spanish") {
                changeLang('en', 'English');
				_this.text('Espanol');
				window.dataLayer.push({
					'event': 'languageChange',
					'eventCategory': 'Language Change',
					'eventAction': 'Button Click',
					'eventLabel': 'English'
				});
			}
		});
        $('#language-select-es').click(function(e) {
            e.preventDefault();
			changeLang('es', 'Spanish');
			window.dataLayer.push({
				'event': 'languageChange',
				'eventCategory': 'Language Change',
				'eventAction': 'Button Click',
				'eventLabel': 'Espanol'
			});
		});

        $('#language-select-en').click(function(e) {
            e.preventDefault();
			changeLang('en', 'English');
			window.dataLayer.push({
				'event': 'languageChange',
				'eventCategory': 'Language Change',
				'eventAction': 'Button Click',
				'eventLabel': 'English'
			});
        });

		$('body').on('click', '.logIn', function(){
			window.location = "/hix/account/user/login";
		});
		$('body').on('click', '.logout', function(){
            localStorage.removeItem('currentTab');
			window.location = logoutUrl;
		});
		$("#chatpopup a").click( function(event) {
			event.preventDefault();
			window.open(("<%=chatUrl%>"),'','width=380,height=495,toolbar=no,status=no,menubar=no,location=no,scrollbars=yes,resizable=yes,titlebar=no,dialog=yes');
		});
		function printfunc() {
			window.print(); return false;
		}
	});
</script>
<script type="text/javascript">
	/*
		HIX-18015  :  If the current url is one of registration flow, then
		              it required to hide the following masthead urls;
		              inbox
		              My Account -->Account Settings
		              My Account -->Dashboard

	*/
	var registrationUrls=new Array("account/securityquestions","/account/phoneverification/sendcode","/user/activation", "account/signup");

	var currDocUrl = document.URL;
	var urlIdx=0;
	var noOfRegUrls = registrationUrls.length;

	while(urlIdx <noOfRegUrls ){
		if(currDocUrl.indexOf(registrationUrls[urlIdx]) >0){
			$('#masthead-inbox').hide();
			$('#masthead-account-settings').hide();
			$('#masthead-dashboard').hide();
		}
		urlIdx++;
	}
	$('a.dropdown-toggle, .dropdown-menu a').on('touchstart', function(e) {
		  e.stopPropagation();
	});
</script>