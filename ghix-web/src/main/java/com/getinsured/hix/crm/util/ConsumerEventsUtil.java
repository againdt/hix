package com.getinsured.hix.crm.util;



import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.dto.cap.consumerapp.ConsumerCallLogDTO;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.RestfulResponseException;

/**
 * All the utility functions for Admin consumer Controller live here
 * 
 * @author bhatt
 * 
 */
@Component
public class ConsumerEventsUtil {

	private static final String APPEVENT_CSR_OVERRIDES = "APPEVENT_CSR_OVERRIDES";
	private static final String INDIVIDUAL = "INDIVIDUAL";
	private static final String SUPERVISOR_OVERRIDES_RESET_PASSWORD_EMAILID = "SUPERVISOR_OVERRIDES_RESET_PASSWORD_EMAIL";
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private RestTemplate mvcRestTemplate;
	@Autowired
	private AppEventService appEventService;
	@Autowired
	private LookupService lookupService;
	
	public static final String CONTENT_TYPE = "application/json";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ConsumerEventsUtil.class);

	private static final String INDIVIDUAL_HOUSEHOLD_CREATED = "INDIVIDUAL_HOUSEHOLD_CREATED";
	private static final String APPEVENT_ACCOUNT = "APPEVENT_ACCOUNT";
	private static final String HOUSEHOLD_RELINKED = "HOUSEHOLD_RELINKED";
	private static final String HOUSEHOLD_ID = "Household Id";
	private static final String PREVIOUS_USER_ID = "Previous User ID";
	private static final String CURRENT_USER_ID = "Current User ID";
	public enum EnrollmentModality {
		Manual, Automatic
	};

	/**
	 * 
	 * @param enrollment
	 * @return
	 * @throws ParseException
	 */
	public boolean postConsumerLoginEvent(ConsumerEventHistory consumerEventRequest) {
		boolean response=false;
		if (consumerEventRequest != null) {
			try {
				LOGGER.info("consumer - postConsumerLoginEvent : START");
				String callResponse = mvcRestTemplate
						.postForObject(
								GhixEndPoints.ConsumerServiceEndPoints.SAVE_CONSUMER_EVENT_LOG,
								consumerEventRequest, String.class);

				if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(callResponse)) {
					response = true;
				}
			} 
			catch(RestfulResponseException ex){
				switch(ex.getHttpStatus()){
				case INTERNAL_SERVER_ERROR:
					LOGGER.error("Error in postConsumerLoginEvent.",ex);	
					break;
				
				default:
					LOGGER.error("Unknown error occurred. ", ex);
					break;
				}
				return response;
			}
			catch (Exception e) {
				LOGGER.error("Error in postConsumerLoginEvent "+e.getMessage());
				return response;
			}
		}

		return response;
	}

	public void generateHouseholdCeateEvent(Household household) {
		if (!(household != null )) {
			return;
		}
		LookupValue lookupValue = lookupService
				.getlookupValueByTypeANDLookupValueCode(APPEVENT_ACCOUNT,
						INDIVIDUAL_HOUSEHOLD_CREATED);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleName(INDIVIDUAL);
		eventInfoDto.setModuleId(household.getId());
		appEventService.record(eventInfoDto, null);
	}
	public void generateHouseholdRelinkedEvent(Map<String, Object> householdMap) {
		if (!(householdMap != null)) {
			return;
		}
		Map<String, String> mapEventParam = new HashMap<>();
		String householdId=null;
		if (null != householdMap.get("household")) {
			Map cmrMap = (Map) householdMap.get("household");
			householdId = String.valueOf(cmrMap.get("id"));
			mapEventParam.put(HOUSEHOLD_ID, householdId);
		}
		if (null != householdMap.get("newUserId")) {
			Integer newUserId = (Integer) householdMap.get("newUserId");
			mapEventParam.put(CURRENT_USER_ID, String.valueOf(newUserId));
		}
		if (null != householdMap.get("oldUserId")) {
			Integer oldUserId = (Integer) householdMap.get("oldUserId");
			mapEventParam.put(PREVIOUS_USER_ID, String.valueOf(oldUserId));
		}
		LookupValue lookupValue = lookupService
				.getlookupValueByTypeANDLookupValueCode(APPEVENT_ACCOUNT,
						HOUSEHOLD_RELINKED);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleName(INDIVIDUAL);
		if (StringUtils.isNotEmpty(householdId)) {
			eventInfoDto.setModuleId(Integer.valueOf(householdId));
		}
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	public void createCallLogEvent(String callLogJson) {

		if (StringUtils.isEmpty(callLogJson)) {
			return;
		}
		ObjectMapper mapper = new ObjectMapper();
		ConsumerCallLogDTO dto;
		try {
			dto = mapper.readValue(callLogJson, ConsumerCallLogDTO.class);

			Map<String, String> mapEventParam = new HashMap<>();
			String date = new SimpleDateFormat("MM/dd/yyyy HH:mm",Locale.ENGLISH).format(new TSDate());
			mapEventParam.put("Timestamp", date);
			mapEventParam.put("Duration", dto.getDuration());
			mapEventParam.put("Next Steps", dto.getNextSteps());

			LookupValue lookupValue = lookupService
					.getlookupValueByTypeANDLookupValueCode(
							"APPEVENT_CUSTOMERCALLS", "MANUAL_CALL");
			EventInfoDto eventInfoDto = new EventInfoDto();
			eventInfoDto.setEventLookupValue(lookupValue);
			eventInfoDto.setModuleName(INDIVIDUAL);
			eventInfoDto.setTimeStamp(dto.getTimeStamp());
			eventInfoDto.setComments(dto.getNotes());
			if (StringUtils.isNotEmpty(dto.getConsumerId())) {
				eventInfoDto.setModuleId(Integer.valueOf(dto.getConsumerId()));
			}

			appEventService.record(eventInfoDto, mapEventParam);
		} catch (JsonParseException e) {
			LOGGER.error("Error in parsing JSON:" + e.getMessage());
		} catch (JsonMappingException e) {
			LOGGER.error("Error in parsing JSON:" + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Error in recording call log event:" + e.getMessage());
		}

	}
	
	public void saveResetPasswordAlternateEmailDtls(Map<String,Object> memberDetails){
		LookupValue lookupValue = lookupService
				.getlookupValueByTypeANDLookupValueCode(APPEVENT_CSR_OVERRIDES,
						SUPERVISOR_OVERRIDES_RESET_PASSWORD_EMAILID);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleName(INDIVIDUAL);
		eventInfoDto.setModuleId((Integer)memberDetails.get("household_id"));
		eventInfoDto.setComments((String)memberDetails.get("reason"));
		appEventService.record(eventInfoDto, null);
	}
	
	/**
	 * 
	 * @param enrollment
	 * @return
	 * @throws ParseException
	 */
	public Household getHouseHoldByCaseId(String caseId){
		Household household = null;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Rest call to get householeId starts");
		}

		//get the household using rest call
		try{
			String response = ghixRestTemplate.getForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_CASE_ID +  caseId, String.class );
			
			if(response != null){
				 ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(Household.class);
				 household = objectReader.readValue(response);
			}
		}catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("rest call to get householeId ends");
		}
		return household;
	}
}
