import React, { Fragment } from 'react';
import { connect } from 'react-redux';

// Components

import PageHeader from '../headers/PageHeader';
import AdditionalInformationSummary from '../summary/AdditionalInformationSummary';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - Summary page for 'Family and Household' Section
 *
 */
const Page4_64  = props => {

    const { taxHousehold, householdMembers, buttonContainer, PAGES, adjustedFlow } = props;


    return (
        <Fragment>
            <div id="fhSummary" className="fade-in ">
                <div className="subsection">
                    <PageHeader
                        text="Review and Confirm"
                        headerClassName={'usa-heading-alt'}
                    />
                    <div className="usa-width-one-whole">
                        <div className="usa-alert usa-alert-info alert--info alert__review">
                            <div className="usa-alert-body" >
                                <p className="usa-alert-text">
                                    {
                                        `Here is the information you provided about everyone who is part of your household.
                                            Please take a moment to review and double-check the information.
                                            If you see any mistakes, please edit them now.`
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                    <div  className="usa-width-one-whole">
                        <div className="table-res-wrap">
                            <AdditionalInformationSummary
                                {...{buttonContainer, householdMembers, taxHousehold, PAGES, adjustedFlow}}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    taxHousehold: state.data.singleStreamlinedApplication.taxHousehold[0],
    householdMembers: state.data.singleStreamlinedApplication.taxHousehold[0].householdMember,
    adjustedFlow: state.adjustedFlow
});

Page4_64.defaultProps = {
    name: 'Page4_64'
};

export default connect(mapStateToProps)(Page4_64);
