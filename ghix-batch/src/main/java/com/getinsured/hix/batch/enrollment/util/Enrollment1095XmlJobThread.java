/**
 * 
 */
package com.getinsured.hix.batch.enrollment.util;

import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.enrollment.service.Enrollment1095XmlBatchService;

/**
 * @author negi_s
 *
 */
public class Enrollment1095XmlJobThread implements Callable<Boolean> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlJobThread.class);

	private Enrollment1095XmlBatchService enrollment1095XmlBatchService;
	private List<Integer> enrollment1095Ids;
	private Integer docSeqId;
	private String wipFolder;
	private Map<Integer, String> recordSequenceIdMap;
//	private String batchId;
	private String batchCategoryCode;
	private Enrollment1095XmlWrapper enrollment1095XmlWrapper;

	public Enrollment1095XmlJobThread(Enrollment1095XmlBatchService enrollment1095XmlBatchService,
			List<Integer> enrollment1095Ids, Integer docSeqId, String wipFolder, Map<Integer, String> recordSequenceIdMap, String batchCategoryCode, Enrollment1095XmlWrapper enrollment1095XmlWrapper) {
		this.enrollment1095XmlBatchService = enrollment1095XmlBatchService;
		this.enrollment1095Ids = enrollment1095Ids;
		this.docSeqId = docSeqId;
		this.wipFolder = wipFolder;
		this.recordSequenceIdMap = recordSequenceIdMap;
		this.batchCategoryCode = batchCategoryCode;
		this.enrollment1095XmlWrapper = enrollment1095XmlWrapper;
	}


	@Override
	public Boolean call() throws Exception {
		boolean status=false;
		try{
			status=enrollment1095XmlBatchService.processXmlForEnrollment1095(enrollment1095Ids, docSeqId, wipFolder, recordSequenceIdMap, batchCategoryCode, enrollment1095XmlWrapper);
			LOGGER.info("Time taken by thread with ID = "+ Thread.currentThread().getId() +" For Document Sequence Id = "+docSeqId+" and enrollment 1095 :: "+enrollment1095Ids.toString()+" " + ManagementFactory.getThreadMXBean().getThreadCpuTime(Thread.currentThread().getId())+" nano second of cpu time.");
		}catch(Exception e){
			LOGGER.error("Error while processing creating XML :: "+e.getMessage(), e);
		}
		return status;
	}

}
