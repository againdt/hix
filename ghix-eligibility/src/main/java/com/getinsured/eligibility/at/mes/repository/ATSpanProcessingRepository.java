package com.getinsured.eligibility.at.mes.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;

@RestResource(path="AccountTransferSpanInfo")
public interface ATSpanProcessingRepository extends JpaRepository<AccountTransferSpanInfo, Long>{

	@RestResource(path="findByExternalApplicantIdAndCoverageYear")
	@Query("from AccountTransferSpanInfo where externalApplicantId = :externalApplicantId and coverageYear = :coverageYear order by id desc ")
	public List<AccountTransferSpanInfo> findByExternalApplicantIdAndCoverageYear(@Param("externalApplicantId") String externalApplicantId,@Param("coverageYear") long coverageYear);
	
	@RestResource(path="findByCmrHouseholdIdAndCoverageYearAndStatus")
	@Query("from AccountTransferSpanInfo where cmrHouseholdId = :cmrHouseholdId and coverageYear = :coverageYear and queueStatus = :queueStatus"
			+ " order by creationTimestamp desc ")
	public List<AccountTransferSpanInfo> findByCmrHouseholdIdAndCoverageYearAndStatus(@Param("cmrHouseholdId") BigDecimal cmrHouseholdId,
			@Param("coverageYear") long coverageYear,@Param("queueStatus") AccountTransferQueueStatus queueStatus);
	
	
	@RestResource(path="findBySsapApplicationId")
	@Query("from AccountTransferSpanInfo where ssapApplicationId = :ssapApplicationId ")
	public AccountTransferSpanInfo findBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);

	@RestResource(path="getQueuedApplicationsForCurrentDate")
	@Query("from AccountTransferSpanInfo where dueDate <= :dueDate and queueStatus = :queueStatus")
	public List<AccountTransferSpanInfo> getQueuedApplicationsForCurrentDate(@Param("dueDate") Date dueDate, @Param("queueStatus") AccountTransferQueueStatus queued);
	
	@RestResource(path="findSpansByExtMemberIdAndProcessStatus")
	@Query("from AccountTransferSpanInfo where externalApplicantId = :externalApplicantId and coverageYear = :coverageYear"
			+ " and processStatus = :processStatus order by creationTimestamp desc ")
	public List<AccountTransferSpanInfo> findSpansByExtMemberIdAndProcessStatus(@Param("externalApplicantId") String externalApplicantId,@Param("coverageYear") long coverageYear,@Param("processStatus") AccountTransferProcessStatus processStatus);
	
	
	@RestResource(path="updateAtSpanStatusById")
	@Modifying
	@Transactional
	@Query("update AccountTransferSpanInfo set  processStatus = :spanProcessStatus , ssapApplicationId = :ssapApplicationId where id = :atSpanInfoId ")
	public int updateAtSpanStatusById(@Param("spanProcessStatus") AccountTransferProcessStatus spanProcessStatus,@Param("atSpanInfoId") long atSpanInfoId, @Param("ssapApplicationId") long ssapApplicationId);
	
	public AccountTransferSpanInfo findById(long id);
	
	@RestResource(path="findPastSpansByStatusAndInterval")
	@Query("from AccountTransferSpanInfo where externalApplicantId = :externalApplicantId and coverageYear = :coverageYear"
			+ " and creationTimestamp >= :fromDate and creationTimestamp <= :toDate " 
			+ " and processStatus = :processStatus order by creationTimestamp desc ")
	public List<AccountTransferSpanInfo> findPastSpansByStatusAndInterval(@Param("externalApplicantId") String externalApplicantId,@Param("coverageYear") long coverageYear,@Param("processStatus") AccountTransferProcessStatus processStatus,@Param("fromDate") Date fromDate,@Param("toDate") Date toDate);
	
}
