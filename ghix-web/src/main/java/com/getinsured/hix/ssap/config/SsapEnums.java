package com.getinsured.hix.ssap.config;

public class SsapEnums {

	
	public enum ApplicationStatusEnum
	  {

		  APPLICATION_INPROGRESS ("P"),
		  APPLICATION_COMPLETE ("C"),
		  APPLICATION_SUBMITTED ("S");
		 
		  
		  private final String value;
		  
		  public String getValue(){return this.value;}
		  
		  ApplicationStatusEnum(String value)
		  {
			  this.value = value;
		  }
	  };
	  
	  public enum ApplicantVerificationStatusEnum
	  {

		  VERIFIED ("VERIFIED"),
		  NOT_VERIFIED ("NOT VERIFIED");
		 
		  
		  private final String value;
		  
		  public String getValue(){return this.value;}
		  
		  ApplicantVerificationStatusEnum(String value)
		  {
			  this.value = value;
		  }
	  };
	  
	  public enum ApplicationTypeEnum
	  {

		  OE ("OE");
		  
		  private final String value;
		  
		  public String getValue(){return this.value;}
		  
		  ApplicationTypeEnum(String value)
		  {
			  this.value = value;
		  }
	  };
}
