package com.getinsured.iex.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class LifeChangeEventDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String caseNumber;
	private String userName;
	private String status;
	private Set<Long> personIds = new HashSet<>();
	private Long oldApplicationId;
	private Long newApplicationId;
	private Long applicationEventId;
	private String keepOnly;
	private Boolean mailingAddressChanged;
	private Long updatedMailingAddressLocationId;
	
	public Long getApplicationEventId() {
		return applicationEventId;
	}

	public void setApplicationEventId(Long applicationEventId) {
		this.applicationEventId = applicationEventId;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getOldApplicationId() {
		return oldApplicationId;
	}

	public void setOldApplicationId(Long oldApplicationId) {
		this.oldApplicationId = oldApplicationId;
	}

	public Long getNewApplicationId() {
		return newApplicationId;
	}

	public void setNewApplicationId(Long newApplicationId) {
		this.newApplicationId = newApplicationId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Set<Long> getPersonIds() {
		return personIds;
	}

	public void setPersonIds(Set<Long> personIds) {
		this.personIds = personIds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getMailingAddressChanged() {
		return mailingAddressChanged;
	}

	public void setMailingAddressChanged(Boolean mailingAddressChanged) {
		this.mailingAddressChanged = mailingAddressChanged;
	}

	public Long getUpdatedMailingAddressLocationId() {
		return updatedMailingAddressLocationId;
	}

	public void setUpdatedMailingAddressLocationId(Long updatedMailingAddressLocationId) {
		this.updatedMailingAddressLocationId = updatedMailingAddressLocationId;
	}

	
}
