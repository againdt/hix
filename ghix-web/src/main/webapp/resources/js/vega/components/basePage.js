﻿define(function () {
    var basePage = new baseComponent();
    basePage.type = "basePage";
    basePage.getHTML = function (cgfObj) {
        var defCfg = {
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var str = "";
        str += '<div id=\"' + cgfObj.id + '\"><div class="row-fluid pageHeader" >';
        str += '    <div class="span12 pull-left"><h1>' + cgfObj.title + '</h1></div>';
        str += '</div>';
        str += '<div class="row-fluid pageContent">';
        str += '    <div class="span12" id="form_target"></div>';
        str += '</div></div>';

        return str;
    };
    return basePage;
});