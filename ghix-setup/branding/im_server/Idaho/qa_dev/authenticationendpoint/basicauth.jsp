<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<%
	if (CharacterEncoder.getSafeText(request.getParameter("username")) == null
			|| "".equals(CharacterEncoder.getSafeText(request.getParameter("username").trim()))) {
%>

<%
	loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
	if (loginFailed != null) {
%>
<div class="alert alert-error">
	<fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter("errorMessage"))%>' />
</div>
<%
	}
%>

<!-- Username -->
<div class="control-group">
	<label class="control-label" for="username">Email Address</label>

	<div class="controls">
		<input class="xlarge" type="text" id='username' name="username"
			size='30' style="text-transform: lowercase;"/>
	</div>
</div>

<%
	} else {
%>

<input type="hidden" id='username' name='username'
	value='<%=CharacterEncoder.getSafeText(request.getParameter("username"))%>' />

<%
	}
%>

<!--Password-->
<div class="control-group">
	<label class="control-label" for="password">Password</label>

	<div class="controls">
		<input type="password" id='password' name="password"
			class="xlarge" size='30' />
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<div class="clearfix nmhide mshide" style="display:none;">
			<input type="hidden"
			name="sessionDataKey"
			value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>' /> <label
			class="checkbox" style="margin-top: 10px"><input
			type="checkbox" id="chkRemember" name="chkRemember">Remember Me</label>
		</div>
		<input type="submit" id="submit" onclick="convert()" 
			value="Login"
			class="btn btn-primary btn-large margin10-t">
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<a href="https://idahohix.yourhealthidaho.org/hix/account/user/forgotpassword"><small>Forgot Password?</small></a>
	</div>
</div>
<script type="text/javascript">
function convert() 
{
    var str = document.getElementById("username").value;
    document.getElementById("username").value = str.toLowerCase();
}
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-43319506-1', 'auto');
  ga('send', 'pageview');
 
</script>

