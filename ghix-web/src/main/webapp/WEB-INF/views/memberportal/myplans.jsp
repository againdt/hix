<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/consumer-dashboard-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />


<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name"><%-- ${employee.name} --%><span></h1>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff ffmClear="${ffmClear}"></util:myStuff>
      <util:quickLinks user="${user}"></util:quickLinks>
    </div><!--/span3-->
     <div class="span9" id="rightpanel">
    
      <div class="header">  
        <h4>My Plans</h4>        
      </div>

      <div class="gutter10">
      
        <div class="row-fluid">
            <p>Following is the information about plans selected by you during open enrollment.</p>                    
        </div>

        <util:view></util:view>  

    </div>
  </div>
  </div><!--/row-fluid-->    
</div><!--/main .gutter10-->
<script>
$(function() {
	  $("*[data-toggle]").live("click",
	          function(e) {
	            e.preventDefault();
	            var href = $(this).attr('href');
	            if (href.indexOf('#') != 0) {
	              $(
	                  '<div id="plandetails" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>')
	                  .modal();
	            }
	          });
	});


</script>
<%-- <jsp:include page="reportChange.jsp" >
	<jsp:param name="" value="" />
</jsp:include>  --%> 


                
  
