import React, {Fragment} from 'react';
import {injectIntl} from 'react-intl';
import SummaryItem from './SummaryItem';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import helperFunctions from '../../utils/helperFunctions';
import SubHeader from '../headers/SubHeader';
import {getOptionsForHardcodedState} from '../../constants/dropdownOptions';

// Components

// Utils


const AdditionalInformationSummary = props => {
    const {
        intl,
        taxHousehold,
        householdMembers,
        buttonContainer: ButtonContainer,
        PAGES,
        adjustedFlow
    } = props;

    const {
        reconciledAptc,
        primaryTaxFilerPersonId
    } = taxHousehold;
    const checkHouseholdMember = (i) => {
        if (adjustedFlow === undefined) {
            return true;
        } else {
            const pagesListToCheck = [
                PAGES.OTHER_HEALTH_COVERAGE_PAGE,
                PAGES.RECONCILIATION_IN_APTC_PAGE,
                PAGES.EMPLOYER_COVERAGE_DETAIL_PAGE,
                PAGES.STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE,
                PAGES.ADDITIONAL_INFORMATION_PAGE
            ];
            return !pagesListToCheck.every(pageName => !adjustedFlow.find(pg => pg.pageName === pageName && pg.hhm === i).isActive);

        }
    };
    return householdMembers.filter((hhm, indx) => checkHouseholdMember(indx)).map((member, i) => {

        const {
            applyingForCoverageIndicator,
            name,
            personId,
            householdContact: {
                homeAddress: {
                    state
                }
            },
            healthCoverage: {
                currentlyHasHealthInsuranceIndicator,
                employerWillOfferInsuranceIndicator,
                currentOtherInsurance: {
                    otherStateOrFederalPrograms
                },
                otherInsurance,
                stateHealthBenefit
            },
            employerSponsoredCoverage
        } = member;
        const [ stateAbbr ] = state ? getOptionsForHardcodedState(state) : [{}];
        return (
            <div className='summary__block' key={`${member.name.firstName}${i}`}>
                <div className="usa-grid usa-width-one-whole summary__header">
                    <SubHeader
                        text={
                            <span>
                                {normalizeNameOfPerson(name)}
                                {i === 0 ? '(' + intl.formatMessage({id: 'label.contact.primary'}) + ')' : ''}
                            </span>
                        }
                        headerClassName={'usa-heading-alt'}
                        additionalElement={(
                            <ButtonContainer
                                type="editButton_secondary"
                                args={[i, PAGES.OTHER_HEALTH_COVERAGE_PAGE]}
                            />
                        )}
                        additionalElementClassName={'header-btn'}
                    />
                </div>
                {applyingForCoverageIndicator &&
                    <Fragment>
                        <SummaryItem
                            category="Do you currently have health coverage?"
                            value={currentlyHasHealthInsuranceIndicator ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        {
                            currentlyHasHealthInsuranceIndicator &&
                            <Fragment>
                                <span className="usa-width-one-whole usa-offset-one-twelfth">
                                    Coverage Type
                                </span>
                                <span className="usa-width-one-whole usa-offset-one-twelfth">
                                    {helperFunctions.getCoverageType(otherStateOrFederalPrograms)}
                                    {helperFunctions.getOtherCoverageInsuranceName(otherInsurance)}
                                </span>

                            </Fragment>

                        }
                    </Fragment>
                }
                {
                    primaryTaxFilerPersonId === personId &&
                    <SummaryItem
                        category="Did reconcile premium tax credits on their tax return for past years"
                        value={reconciledAptc ? 'Yes' : 'No'}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                }
                {applyingForCoverageIndicator &&
                <Fragment>
                    <SummaryItem
                        category="Offered Employer Coverage"
                        value={employerWillOfferInsuranceIndicator ? 'Yes' : 'No'}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                    {
                        employerWillOfferInsuranceIndicator &&
                        <Fragment>
                            {helperFunctions.getEmployerSponsoredCoverageDetails(employerSponsoredCoverage)}
                        </Fragment>

                    }
                    <SummaryItem
                        category={`Are you offered the ${stateAbbr.text} state employee health benefit plan through a job or a family member’s job?`}
                        value={stateHealthBenefit ? 'Yes' : 'No'}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                </Fragment>
                }
            </div>

        );
    });
};

export default injectIntl(AdditionalInformationSummary);
