package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.SponsorshipDataType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper Class for SponsorshipDataTypeGCD
 * 
 * @author Vishaka Tharani
 * @since  18-Feb-2014 
 * 
 */
public class SponsorshipDataTypeGCDWrapper implements JSONAware{
	
	private ObjectFactory factory;
	private SponsorshipDataType sponsorshipDataType;
	
	private String lastName;
	private String firstName;
	private String middleName;
	private String addr1;
	private String addr2;
	private String city;
	private String stateCode;
	private String zipCode;
	
	private static final String LAST_NAME_KEY = "lastName";
	private static final String FIRST_NAME_KEY = "firstName";
	private static final String MIDDLE_NAME_KEY = "middleName";
	private static final String ADDR1_KEY = "addr1";
	private static final String ADDR2_KEY = "addr2";
	private static final String CITY_KEY = "city";
	private static final String STATECODE_KEY = "stateCode";
	private static final String ZIPCODE_KEY = "zipCode";
	
	public SponsorshipDataTypeGCDWrapper(){
		this.factory = new ObjectFactory();
		this.sponsorshipDataType = factory.createSponsorshipDataType();
	}
	
	public SponsorshipDataTypeGCDWrapper(SponsorshipDataType sponsorshipDataType){
		this.factory = new ObjectFactory();
		this.sponsorshipDataType = sponsorshipDataType;
		
		lastName = sponsorshipDataType.getLastName();
		firstName = sponsorshipDataType.getFirstName();
		middleName = sponsorshipDataType.getMiddleName();
		addr1 = sponsorshipDataType.getAddr1();
		addr2 = sponsorshipDataType.getAddr2();
		city = sponsorshipDataType.getCity();
		stateCode = sponsorshipDataType.getStateCode();
		zipCode = sponsorshipDataType.getZipCode();
	}
	
	public ObjectFactory getFactory() {
		return factory;
	}
	public void setFactory(ObjectFactory factory) {
		this.factory = factory;
	}
	public SponsorshipDataType getSponsorshipDataType() {
		return sponsorshipDataType;
	}
	public void setSponsorshipDataType(SponsorshipDataType sponsorshipDataType) {
		this.sponsorshipDataType = sponsorshipDataType;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) throws HubServiceException {
		
		if(lastName == null){
			throw new HubServiceException("No lastName provided");
		}
		
		this.lastName = lastName;
		this.sponsorshipDataType.setLastName(this.lastName);
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) throws HubServiceException {
		
		if(firstName == null){
			throw new HubServiceException("No firstName provided");
		}
		
		this.firstName = firstName;
		this.sponsorshipDataType.setFirstName(this.firstName);
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) throws HubServiceException {
		
		if(middleName == null){
			throw new HubServiceException("No middleName provided");
		}
		
		this.middleName = middleName;
		this.sponsorshipDataType.setMiddleName(this.middleName);
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) throws HubServiceException {
		
		if(addr1 == null){
			throw new HubServiceException("No middleName provided");
		}
		
		this.addr1 = addr1;
		this.sponsorshipDataType.setAddr1(this.addr1);
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) throws HubServiceException {
		
		if(addr2 == null){
			throw new HubServiceException("No addr2 provided");
		}
		
		this.addr2 = addr2;
		this.sponsorshipDataType.setAddr2(this.addr2);
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) throws HubServiceException {
		
		if(city == null){
			throw new HubServiceException("No city provided");
		}
		
		this.city = city;
		this.sponsorshipDataType.setCity(this.city);
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) throws HubServiceException {
		
		if(stateCode == null){
			throw new HubServiceException("No stateCode provided");
		}
		
		this.stateCode = stateCode;
		this.sponsorshipDataType.setStateCode(this.stateCode);
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) throws HubServiceException {
		
		if(zipCode == null){
			throw new HubServiceException("No zipCode provided");
		}
		
		this.zipCode = zipCode;
		this.sponsorshipDataType.setZipCode(this.zipCode);
	}
		
	/**
	 * Converts the JSON String to SponsorshipDataTypeWrapper object
	 * 
	 * @param jsonString - String representation for SponsorshipDataTypeWrapper
	 * @return SponsorshipDataTypeWrapper Object
	 */
	public static SponsorshipDataTypeGCDWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create CertOfCitDocumentID23Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		SponsorshipDataTypeGCDWrapper wrapper = new SponsorshipDataTypeGCDWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(LAST_NAME_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No last name found");
		}
		wrapper.lastName = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.lastName);
		
		tmpObj = obj.get(FIRST_NAME_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No first name found");
		}
		wrapper.firstName = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.firstName);
		
		tmpObj = obj.get(MIDDLE_NAME_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No first middle found");
		}
		wrapper.middleName = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.middleName);
		
		tmpObj = obj.get(ADDR1_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No addr1 found");
		}
		wrapper.addr1 = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.addr1);
		
		tmpObj = obj.get(ADDR2_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No addr2 found");
		}
		wrapper.addr2 = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.addr2);
		
		tmpObj = obj.get(CITY_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No city found");
		}
		wrapper.city = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.city);
		
		tmpObj = obj.get(STATECODE_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No stateCode found");
		}
		wrapper.stateCode = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.stateCode);
		
		tmpObj = obj.get(ZIPCODE_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create SponsorshipDataTypeWrapper, No zipCode found");
		}
		wrapper.zipCode = (String)tmpObj;
		wrapper.sponsorshipDataType.setLastName(wrapper.zipCode);
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(LAST_NAME_KEY,this.lastName);
		obj.put(FIRST_NAME_KEY,this.firstName);
		obj.put(MIDDLE_NAME_KEY,this.middleName);
		obj.put(ADDR1_KEY,this.addr1);
		obj.put(ADDR2_KEY,this.addr2);
		obj.put(CITY_KEY,this.city);
		obj.put(STATECODE_KEY,this.stateCode);
		obj.put(ZIPCODE_KEY,this.zipCode);
		return obj.toJSONString();
	}
	
}
