package com.getinsured.hix.cap.util;

public class AjaxResponseBase {
	private boolean status;
	private String message;
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("[Status:%s]", status));
		sb.append(String.format("[Message:%s]", message));
		return String.format("[%s]", sb.toString());
	}
	public AjaxResponseBase(boolean status, String message)
	{
		this.status = status;
		this.message = message;
	}
}
