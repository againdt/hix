<%-- <%@page isELIgnored="false"  %> --%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>

<jsp:useBean id="now" class="java.util.Date" />

<style>
#waiving-reason label:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}
</style>

<form class="form-horizontal" id="formterminateemp" name="formterminateemp" action="terminateemployercoverage" method="post" >
<df:csrfToken/>
<div id="terminate-emp-coverage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h3 id="myModalLabel">Terminate Your Coverage</h3>
   </div>
                
   <div class="modal-body">
   <!--Heading label-->
	    <div class="headingterminateEmpCov">Are you sure you want to terminate your coverage? If so please select the trigger event and the date.</div>
	    <!-- /Event-Details starts here -->	
        <div class="control-group" id="event-details" >
        	<label class="control-label pull-left" for="eventName">Event <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
           	<div class="controls">
            	 
				 <select id="eventName" name="eventName" class="input-xlarge pull-left" >
				    <option value="">Select Event </option>
		 			<option value="Marriage">Marriage</option>
		  			<option value="Birth">Birth</option>
		  			<option value="Adoption">Adoption</option>
		  			<option value="Divorce">Divorce</option>
		  			<option value="Death">Death of a Family Member</option>
		  			<option value = "TerminationofBenefit">Termination of Benefits</option>
		  			<c:if test="${nativeAmr == true}">
		  				<option value = "PlanChange">Plan Change</option>
		  			</c:if>
				</select>
				<span id="eventName_error"></span>
			</div>
		</div>				
		<div class="control-group">
			<label class="control-label pull-left" for="eventDate">Event Date <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
			<div class="controls">
				<div class="input-append date date-picker" data-date="" data-date-format="mm/dd/yyyy">
						<input class="span10" type="text" name="eventDate" id="terminateEventDate">
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
				<span id="terminateEventDate_error"></span>
			</div>
		</div>
		 <!-- /Event-Details ends here-->		
						
		<div class="control-group" id="div-employee-esign">
			<label class="control-label pull-left" for="esignName">Employee's e-Signature <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
			<div class="controls">
				<input type="text" class="input-large pull-left" id="esignName"	name="esignName" size="30" placeholder="John Doe">
				<div class="esign-tip">Type	your full name here as your electronic signature.</div>				
			</div>
			<div id="esignName_error" style="width: 1800px;"></div>
		</div>
		<div class="control-group" id="div-employee-esign-date">
			<label class="control-label pull-left" for="today_date">Today&#39;s Date</label>
			<div class="controls">
			<input type="text" name="name" id="todaysesigndate" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="MM/dd/yyyy" />" readonly/>
			</div>	
		</div>
		<div class="control-group" id="employee-attestation">
			<input type="checkbox" id="employeeAttestation" name ="attestation">
			<label class="pull-left" for="attestation">I attest that I want to terminate my coverage, and I understand that I will be unable to enroll again until next open enrollment unless I experience a qualifying event</label>
			<span id="employeeAttestation_error"></span>					
		</div>
	</div><!-- /modal-body -->
	<div class="modal-footer">
		<input type="hidden" name="employeeId" id="employeeId" value="${param.employeeId}"/>
	    <button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>						
	    <input type="submit" class="btn btn-primary pull-right" aria-hidden="true" alt="Submit" value="Submit"/>
	</div>
	
</div><!-- /#waive-emp-coverage -->
</form>
<script type="text/javascript">
$(document).ready(function(){
	
	   $('.date-picker').datepicker({
			startDate: '-30d',
			endDate:'+0d',
			format:'mm/dd/yyyy',
			autoclose:true,
			forceParse: false
	   });
});
/* function validateTermForm(){
	if(  $("#frmterminateemp").validate().form() ) {
		alert('validated' + $("#frmterminateemp").validate().form());
		$("#frmterminateemp").submit();
	}
} */

/* function validateDeathDate(){
	var today=new Date();
	var eventDate = $("#eventDate").val();
    eventDate_arr = eventDate.split(/\//);
	eventDate_mm = eventDate_arr[0]; eventDate_dd = eventDate_arr[1]; eventDate_yy = eventDate_arr[2];
	var deathDate=new Date();
	deathDate.setFullYear(eventDate_yy ,eventDate_mm - 1,eventDate_dd);
	if( (today.getFullYear() - 100) >  deathDate.getFullYear() ) { return false; }
	if( (eventDate_dd != deathDate.getDate()) || (eventDate_mm - 1 != deathDate.getMonth()) || (eventDate_yy != deathDate.getFullYear()) ) { return false; }
	if(today.getTime() < deathDate.getTime()){ return false; }
	return true;
} */

var validator = $("#formterminateemp").validate({ 
	
 	onkeyup: false,
	onclick: false,
	onfocusout: false,
	onSubmit: true,
	
	rules : {'eventName' : { required : true},
 			 'eventDate' :{required : true, date: true},
			 'esignName' : {required : true},
			 'attestation' : {required: true} 
	},
	messages : {
		'eventName' : {  required : "<span> <em class='excl'>!</em>Please select an Event</span>"},
		'eventDate': {required :  "<span> <em class='excl'>!</em>Plese select an Event Date</span>", 
		'date':  "<span> <em class='excl'>!</em>Plese select a valid Event Date</span>",
		'validateDeathDate':  "<span> <em class='excl'>!</em>Plese select a valid Event Date</span>"},
	   
		'esignName': { required : "<span> <em class='excl'>!</em>Please enter your first name and last name </span>"},
		'attestation' :  { required : "<span> <em class='excl'>!</em>Please accept all Attestation </span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class',	'error span10');
	}	
});
</script>