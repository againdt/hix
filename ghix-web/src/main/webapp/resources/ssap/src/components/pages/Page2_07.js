import React, { Fragment } from 'react';
import { FormattedMessage } from 'react-intl';
import Floater from 'react-floater';

const Page2_07 = () => (
    <Fragment>
        <div id="saPrimaryContact" className="">
            <div className="subsection">
                <div className="usa-width-one-whole">
                    <div className="usa-alert usa-alert-info alert--info">
                        <div className="usa-alert-body" >
                            <p className="usa-alert-text">
                                <FormattedMessage
                                    id="ssap.familyHousehold.getReady.msg.one"
                                    defaultMessage="In this section, we will ask for more detailed information about everyone in your household."
                                />
                                <br/>
                                <strong>
                                    <FormattedMessage
                                        id="ssap.familyHousehold.getReady.msg.two"
                                        defaultMessage="If you step away from this application at any time, please be sure to save your progress. You can save your application at any time by clicking the &quot;Save&quot; button."
                                    />
                                </strong>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="usa-width-one-whole">
                    <p>
                        <i>
                            <FormattedMessage
                                id="ssap.familyHousehold.getReady.msg.three"
                                defaultMessage="All fields on this Family & Household section marked with an asterisk (*) are required unless otherwise indicated."
                            />
                        </i>
                    </p>
                    <p>
                        <strong>
                            <FormattedMessage
                                id="ssap.familyHousehold.getReady.msg.four"
                                defaultMessage="For anyone you want to insure, you will need:"
                            />
                        </strong>
                    </p>
                    <ul>
                        <li>
                            <FormattedMessage
                                id="ssap.familyHousehold.getReady.msg.five"
                                defaultMessage="Social Security Number"
                            />
                        </li>
                        <li>
                            <FormattedMessage
                                id="ssap.familyHousehold.getReady.msg.six"
                                defaultMessage="Document numbers for anyone with eligible"
                            />
                            <Floater
                                content="Anyone who is seeking coverage may be required to provide documents to prove ability to enroll in coverage"
                                placement="top">
                                <span>&nbsp;Immigration status</span>
                            </Floater>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </Fragment>
);

Page2_07.defaultProps = {
    name: 'Page2_07'
};

export default Page2_07;
