package com.getinsured.hix.batch.web.task;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class TestBatchTask implements Tasklet{
	
	private static final Logger LOGGER = Logger.getLogger(TestBatchTask.class);
	

	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		LOGGER.info("########## TEST JOB EXECUTED ######## ");
    	 return null;
	}
	
}
