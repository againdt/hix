//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.03 at 12:07:52 PM IST 
//


package com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_ee.APTCType;
import com.getinsured.iex.hub.apc.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.hub.apc.niem.niem.proxy.xsd._2.String;
import com.getinsured.iex.hub.apc.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for Response received from IRS
 * 
 * <p>Java class for IRSResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IRSResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}RequestID"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}APTC" minOccurs="0"/>
 *         &lt;element ref="{http://aptc.dsh.cms.gov/extension/1.0}SystemCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IRSResponseType", propOrder = {
    "requestID",
    "aptc",
    "systemCode"
})
public class IRSResponseType
    extends ComplexObjectType
{

    @XmlElement(name = "RequestID", namespace = "http://hix.cms.gov/0.1/hix-core", required = true)
    protected String requestID;
    @XmlElement(name = "APTC", namespace = "http://hix.cms.gov/0.1/hix-ee")
    protected APTCType aptc;
    @XmlElement(name = "SystemCode", required = true)
    protected TextType systemCode;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the aptc property.
     * 
     * @return
     *     possible object is
     *     {@link APTCType }
     *     
     */
    public APTCType getAPTC() {
        return aptc;
    }

    /**
     * Sets the value of the aptc property.
     * 
     * @param value
     *     allowed object is
     *     {@link APTCType }
     *     
     */
    public void setAPTC(APTCType value) {
        this.aptc = value;
    }

    /**
     * Gets the value of the systemCode property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getSystemCode() {
        return systemCode;
    }

    /**
     * Sets the value of the systemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setSystemCode(TextType value) {
        this.systemCode = value;
    }

}
