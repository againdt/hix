<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style type="text/css">
.form-border{
      border: 1px solid #d3d3d3;
    border-top: none;
}
.search-applicant-results th, .search-applicant-results td {
  word-break: normal;
}
.effectiveYearHolder{
  overflow: hidden;
  height: 39px;
  margin-top:-12px;
}
.effectiveLabel{
  font-weight: normal;
  font-size: 12px;
}
.effectiveYearHolder select{
  margin-top:8px;
}
#consumerSearch .control-group{
  margin-bottom:5px !important;
}
</style>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>


<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ssap/jquery.mask.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ghixcustom.js" />"></script>
<%@ page isELIgnored="false"%>

<jsp:include page="checkPreLinkApplData.jsp" />

<div class="gutter10">
  <div class="row-fluid">
    <ul class="page-breadcrumb">
      <li><a href="javascript:history.back()">&lt; <spring:message
            code="label.back" /></a></li>

      <li><a href="<c:url value="/crm/member/searchapplicants/screenpop" />">SSAP Applicants
      </a> <span class="divider">/</span></li>

      <li class="active">Search</li>
    </ul>
    
    <c:if test="${errorMsg != ''}">
      <div style="font-size: 14px; color: red">
        <c:out value="${errorMsg}"></c:out>
      </div>
    </c:if>
  </div>
    <div class="row-fluid">
      <form  method="POST" action="<c:url value="/crm/member/searchapplicants/screenpop" />" id="consumerSearch" name="consumerSearch" class="form-vertical gutter10 lightgray form-border" >
        <df:csrfToken/>
        <input type="hidden" id="effectiveYear" name="effectiveYear" value="${searchCriteria.effectiveYear}"></input>
        <input type="hidden" name="ispost" value="${ispost}" id="ispost">     
        <div class="row-fluid">
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.firstName" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.firstName" />" name="firstName" id="firstName" value="${searchCriteria.firstName}" maxlength="40" />
              </div>
              <div id="firstName_error"></div>
            </div>            
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.lastName" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.lastName" />" name="lastName" id="lastName" value="${searchCriteria.lastName}" maxlength="40" />
              </div>
              <div id="lastName_error"></div>
            </div>            
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.columnHeaderPhone" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.columnHeaderPhone" />" name="contactNumber" id="contactNumber" value="${searchCriteria.contactNumber}" maxlength="10" />
              </div>
              <div id="contactNumber_error"></div>	
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.ssnLastFour" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.ssnLastFour" />" name="ssn" id="ssn" value="${searchCriteria.ssn}" maxlength="4" />
              </div>
              <div id="ssn_error"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.applicationId" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.applicationId" />" name="appId" id="appId" value="${searchCriteria.appId}" maxlength="10" />
              </div>
              <div id="appId_error"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase"><spring:message code="label.externalAppId" /></label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="<spring:message code="label.externalAppId" />" name="extAppId" id="extAppId" value="${searchCriteria.extAppId}" maxlength="40" />
              </div>
              <div id="extAppId_error"></div>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase">App Source</label>
              <div class="controls">
                <select id="appSource" name="appSource" class="filterField">
                  <option value="All">All</option>
                  <c:forEach items="${applicationSourceList}" var="applicationSource" >
                    <option value="${applicationSource.applicationSourceCode}" <c:if test="${searchCriteria.appSource == applicationSource.applicationSourceCode}"> selected="selected" </c:if>>${applicationSource.description}</option>
                  </c:forEach>
                </select>
              </div>
              <div id="appSource_error"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <sec:accesscontrollist hasPermission="APPLICANT_SEARCH_ACCESS_CODE" domainObject="user">
	          	<label class="control-label" text-transform="uppercase">Access Code</label>
	            <div class="controls">
	            	<input class="filterField" type="text" placeholder="Access Code" name="accessCode" id="accessCode" value="${searchCriteria.accessCode}" maxlength="40" />
	            </div>
	            <div id="accessCode_error"></div>
              </sec:accesscontrollist>
            </div>
            <sec:accesscontrollist hasPermission="APPLICANT_SEARCH_EXTERNAL_APPLICANT_ID" domainObject="user">
              <div class="control-group">
	          	<label class="control-label" text-transform="uppercase">External Applicant Id</label>
	            <div class="controls">
	            	<input class="filterField" type="text" placeholder="External Applicant Id" name="externalApplicantId" id="externalApplicantId" value="${searchCriteria.externalApplicantId}" maxlength="40" />
	            </div>
	            <div id="externalApplicantId_error"></div>
              </div>
          	</sec:accesscontrollist>
          </div>
          <div class="span4">     
            <div class="control-group">
              <label class="control-label"><spring:message code="label.dob"/></label>
              <div class="controls">
                <div class="input-append date-picker date">
                  <input type="text" title="MM/DD/YYYY" value="${searchCriteria.dateOfBirth}" class="datepick input-medium date-format" name="dateOfBirth" id="dateOfBirth" placeholder="MM/DD/YYYY" />
                  <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
                <div id="dateOfBirth_error"></div>
              </div>
            </div>  
          </div>
        </div>
        <div class="row-fluid">
        	<div class="control-group">
          		<div class="span4">
          			<label class="control-label" text-transform="uppercase">Exchange Assigned Policy ID</label>
	              	<div class="controls">
	                	<input class="filterField" type="text" placeholder="Exchange Assigned Policy ID" name="enrollmentId" id="enrollmentId" value="${searchCriteria.enrollmentId}" maxlength="40" />
	              	</div>
	              	<div id="enrollmentId_error"></div>
              	</div>
             </div>
             	
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase">Enrollment Status</label>
              <div class="controls">
                <select id="enrollmentStatus" name="enrollmentStatus" class="filterField">
                  <option value="">ALL</option>
                  <c:forEach items="${enrollmentStatusList}" var="enrollmentStatus" >
                    <option value="${enrollmentStatus.lookupValueId}" <c:if test="${searchCriteria.enrollmentStatus == enrollmentStatus.lookupValueId}"> selected="selected" </c:if>>${enrollmentStatus.lookupValueCode}</option>
                  </c:forEach>
                </select>
              </div>
              <div id="enrollmentStatus_error"></div>
            </div>
          </div>
          <div class="span4">
            <div class="control-group">
              <label class="control-label" text-transform="uppercase">Household Id</label>
              <div class="controls">
                <input class="filterField" type="text" placeholder="Household Id" name="cmrHouseholdId" id="cmrHouseholdId" value="${searchCriteria.cmrHouseholdId}" maxlength="10" />
              </div>
              <div id="cmrHouseholdId_error"></div>
            </div>
          </div>
        </div>
        <div class="txt-right">
          <a class="btn" href="<c:url value="/crm/member/searchapplicants/screenpop" />"><spring:message code="label.crmResetAll" /></a>
          <button type="submit" class="btn btn-primary margin10-l"><spring:message code='label.go'/></button>
        </div>
		
        <input  type="hidden" id="pageNumber" name="pageNumber">
                  <input  type="hidden" id="sortOrder" name="sortOrder">
                  <input  type="hidden" id="changeOrder" name="changeOrder">
                  <input  type="hidden" id="sortBy" name="sortBy">
      </form>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" id="rightpanel">
      <form class="form-horizontal" id="frmSearchConsumer"
        name="frmSearchConsumer"
        action="<c:url value="/crm/member/searchapplicants/screenpop" />"
        method="POST">
        <!-- below hidden field is to avoid fetching data on first load -->
        <c:choose>
          <c:when test="${onPageLoad!='onSearchTrue'}">
            <div class="alert alert-info">
              <spring:message code="label.filterSearchCriteria" />
            </div>
          </c:when>
          <c:otherwise>
            <c:choose>
              <c:when test="${resultSize >= 1}">
                <c:choose>
                  <c:when test="${fn:length(applicantList) > 0}">
                    <table class="table table-striped search-applicant-results">
                      <thead>
                        <tr class="graydrkbg ">
                          <spring:message code='label.columnHeaderName' var="firstName" />
                          <th class="sortable" scope="col" >
                            <dl:sort title="${firstName}" sortBy="firstName" customFunctionName="sortApplicants" sortOrder="${searchCriteria.sortOrder}"></dl:sort> 
                            <c:choose>
                              <c:when test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'firstName'}">
                                <img src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>" alt="State sort ascending" />
                              </c:when>
                              <c:when test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'firstName'}">
                                <img src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>" alt="State sort descending" />
                              </c:when>
                              <c:otherwise></c:otherwise>
                            </c:choose>
                          </th>                       
                          <th >Primary Contact </th>
                          <th ><spring:message code='label.columnHeaderPhone' /></th>
                          <th >SSN</th>
                          <th >App Status</th>
                          <th >Ext App Id</th>
                          <th >Auth Rep Name</th>
                          <th >Access Code</th>
                          <th >Action</th>
                                                  
                        </tr>
                      </thead>

                      <% int index = 0;%>
                      
                      <c:forEach items="${applicantList}" var="applicant" varStatus="vs">
                        <tr>
                          <td>${applicant.firstName} ${applicant.lastName}</td>
                          <td>${applicant.primaryContactName}</td>
                          <td style="min-width: 86px;">${applicant.phoneNumber} </td> <!-- appl.phone number -->
                          <td style="min-width: 65px;">
                            <c:if test="${not empty applicant.ssn}">
                                            <c:set var="ssnVal">${applicant.ssn}</c:set>
                                            <c:set var="ssnLength">${fn:length(ssnVal)}</c:set>
                                            <c:set var="replacessn">${fn:substring(applicant.ssn, 0, 5)}</c:set>
                                           ${fn:replace(applicant.ssn,replacessn, '***-*')}
                                       </c:if>                          
                          </td> <!-- appl.ssn -->
                          <td style="min-width: 60px;">${applicant.appStatusDescription} </td>
                          <td>
                          ${applicant.externalApplicantId}
                            <%--<c:choose>
                              <c:when test="${exchangeType != 'Both'}">${applicant.externalApplicantId}</c:when>
                              <c:otherwise>${applicant.externalApplicationId}</c:otherwise>
                            </c:choose> --%>
                          </td>
                          <td style="min-width: 86px;">${applicant.authRepName}</td> <!-- appl.phone number -->
                          <td style="min-width: 80px;"> <!-- Access code only for financial app  -->
                            <c:if test="${applicant.financialAssistanceFlag == 'Y'}"> 
                              ${applicant.applicantGuidCode} 
                            </c:if>
                          </td>
                          <td>
                            <c:if test="${applicant.financialAssistanceFlag == 'Y' && (applicant.source == 'RF' || applicant.source == 'CN') 
                            && applicant.applicationStatus == 'UC' && applicant.personId==1}" >
                              <c:set var="encAccessCode"><encryptor:enc value="${applicant.applicantGuidCode}" isurl="true"/></c:set>
                              <a href="<c:url value="/referral/dynamicsecurityquestions/${encAccessCode}" />" class="btn btn-primary">Link HOUSEHOLD</a>
                            </c:if>
                            
                            <c:choose>
                              <c:when test="${not empty applicant.householdId }">
                              <c:set var="encHouseholdid" ><encryptor:enc value="${applicant.householdId}" isurl="true"/> </c:set>
                              	<a href="<c:url value="/crm/member/viewmember/${encHouseholdid}" />" class="btn btn-primary">GO TO HOUSEHOLD</a>
                              </c:when>
                            </c:choose>
                          </td>
                          
                          
                        </tr>
                      </c:forEach>
                    
                    </table>
                    <div class="pagination txt-center">
                      <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" customFunctionName="GoToSearchApplicantPage"/>
                    </div>
                  </c:when>
                </c:choose>
              </c:when>
              <c:otherwise>
                <h4 class="alert alert-info"> <spring:message code='label.norecords' /></h4>
              </c:otherwise>
            </c:choose>
          </c:otherwise>
        </c:choose>
      </form>
    </div>
  </div>
  </div>
  
  <!-- row-fluid -->
</div>

<script type="text/javascript">
  var validator = $("#consumerSearch").validate({
    rules : {
      'contactNumber' : {
        minlength : 10,
        number : true
      },
      
      'zipCode' : {
        minlength : 5,
        number : true
      },
      'cmrHouseholdId' :{
    	  number : true
      },
      'enrollmentId' :{
    	  number : true
      },
      'dateOfBirth' :{
    	  validateDOB : true
      }
    },
    messages : {
      'contactNumber' : {
        minlength : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNoFilterLength' javaScriptEscape='true'/></span>",
        number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
      },
      
      'zipCode' : {
        minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipFilterLength' javaScriptEscape='true'/></span>",
        number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
      },
      'cmrHouseholdId' : {
          number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
        },
       'enrollmentId' : {
            number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
          },
        'dateOfBirth' : {
        	validateDOB : "<span> <em class='excl'>!</em>Enter a valid date</span>"
            }
    },
    errorClass : "error",
    errorPlacement : function(error, element) {
      var elementId = element.attr('id');
      
      if ($("#" + elementId + "_error").html() != null) {
        $("#" + elementId + "_error").html('');
      }
      error.appendTo($("#" + elementId + "_error"));
      $("#" + elementId + "_error").attr('class', 'error');
    }
  });

  jQuery.validator.addMethod("validateDOB", function(value, element) {
	  if(!value){
		  return true;
	  }
	  else{
		  var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
		  if(!(date_regex.test(value)))
		  {
		  	return false;
		  }
		  return true;
	  }
	}, '');

  $(document).ready(function(){
    var isFilterEngaged=function()
    {
      var flag=false;
      $("#consumerSearch .filterField").each(function(indx,obj){
        if($(obj).val()!="")
        {
          flag=true;
        };
      });
      return flag;
    }
    var yearDD=$("#effectiveYearProxy");
    var currentYear=(new Date()).getFullYear();
    currentYear="${effectiveYear}";
    var previousValue="${searchCriteria.effectiveYear}";
    if(previousValue=="")
    {
      yearDD.val('All');
    }else{
      yearDD.val(previousValue);
    };
    $("#effectiveYear").val(yearDD.val());
    /*if($("#effectiveYear").val()=="")
    {
      if(isFilterEngaged()){
        //log("filter engaged seting year to "+$("#effectiveYear").val());
        yearDD.val($("#effectiveYear").val());
      }else{
        yearDD.val(currentYear);
        //log("filter not engaged seting year to "+currentYear);
        $("#effectiveYear").val(currentYear);
      };
    }else{
      //log("effective is not blank so value is "+$("#effectiveYear").val());
      yearDD.val($("#effectiveYear").val());
    };*/
    yearDD.bind("change",function(){
      //$("#effectiveYear").val(yearDD.val());
      //if(isFilterEngaged())
      //{
        $("#effectiveYear").val(yearDD.val());
        var contactNo= $("#contactNumber").val();
        var ssn= $("#ssn").val();       
         var reg1 =/^[0-9]{10}$/;
         var reg2 =/^[0-9]*$/;
         var condition1 =reg1.test(contactNo) && reg2.test(ssn);
         var condition2 =reg1.test(contactNo) && ssn=="";
         var condition3 =reg2.test(ssn) && contactNo=="";
        if(condition1|| condition2 || condition3){
          $("#consumerSearch")[0].submit();
        }
        else{
          
          if(reg1.test(contactNo)===false && ssn==""){
            $("#contactNumber").addClass("error");
            $("#contactNumber").focus();
            return;
          }
          if(reg1.test(ssn)===false && contactNo=="" ){
            $("#ssn").addClass("error");
            $("#ssn").focus();
            return;
          }
          if(reg1.test(contactNo)===false && reg1.test(ssn)===false){
            $("#contactNumber").addClass("error");
            $("#contactNumber").focus();
            return;
          }
          return;
        }
      //};      
    });

    $('.date-format').mask("00/00/0000");
    $('.date-picker').datepicker({
      autoclose: true
    });
  });

  var log=function(str)
  {
    try{
      console.log(str);
    }catch(e)
    {

    };
  };
      
  function unlinkApplicant(ssapAppId, householdId) {
    alert("TODO: Unlink the application " + ssapAppId + " with household: " + householdId);
  }

  function doSort(url)
  {
    executeSort('consumerSearch',url);
  };
  
  function GoToSearchApplicantPage (url)
  {
    var pageNumber=GetParameterByName(url,"pageNumber");
    $("#pageNumber").val(pageNumber);
    $("#consumerSearch").submit();
  };
  
  function sortApplicants (url)
  {
    var pageNumber=GetParameterByName(url,"pageNumber");
    var sortBy=GetParameterByName(url,"sortBy");
    var changeOrder=GetParameterByName(url,"changeOrder");
    var sortOrder=GetParameterByName(url,"sortOrder");
    $("#pageNumber").val(pageNumber);
    $("#sortBy").val(sortBy);
    $("#changeOrder").val(changeOrder);
    $("#sortOrder").val(sortOrder);
    
    $("#consumerSearch").submit();
  };
    
  $(document).one("ready",function(){
   if($("#contactNumber").val() != '' && $('#ispost').val() != 'submit'){
   		 $('#ispost').val('submit');
		 $("#consumerSearch").submit();
	}	
});
  
</script>