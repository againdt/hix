package com.getinsured.ssap.model.hub.vlp;

/**
 * Enum that contains next steps for given VLP return code.
 *
 * @see VlpActionResult
 * @author Yevgen Golubenko
 * @since 2/28/19
 */
public enum VlpAction {
  /**
   * No next step is determined.
   */
  NONE,

  /**
   * Means that we are calling step 1 (Initial Verification)
   */
  VERIFY,

  /**
   * Means we are calling step 1a (Additional Verification)
   */
  REVERIFY,

  /**
   * Means we are calling step 1b (Additional Verification),
   * because original call did not have SEVIS id present and it
   * is required to complete the validation process.
   */
  RESUBMIT_WITH_SEVISID,

  /**
   * Second verification.
   * @deprecated Not going to use it on our side.
   */
  SECOND_VERIFICATION,

  /**
   * Third verification.
   * @deprecated Not going to use it on our side.
   */
  THIRD_VERIFICATION, // Other I-94 5 "Invoke InitiateAdditionalVerif or InitiateThirdVerif Web method.",

  /**
   * Check case resolution.
   */
  CHECK_CASE_RESOLUTION,

  /**
   * Check case resolution for step 3
   */
  CHECK_CASE_RESOLUTION_3,

  /**
   * Close case action.
   */
  CLOSE_CASE
  ;
}
