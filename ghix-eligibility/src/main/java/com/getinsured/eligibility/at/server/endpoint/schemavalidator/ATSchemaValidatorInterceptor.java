package com.getinsured.eligibility.at.server.endpoint.schemavalidator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;

import com.getinsured.eligibility.at.server.endpoint.MessageContextHolder;
import com.getinsured.eligibility.ssap.integration.at.client.service.AddHttpHeaderInterceptor;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferConstants;

public class ATSchemaValidatorInterceptor implements EndpointInterceptor, Ordered{
	
	private static Logger lOGGER = Logger.getLogger(ATSchemaValidatorInterceptor.class);
	
	@Override
	public boolean handleRequest(MessageContext messageContext, Object endpoint)
			throws Exception {
		
		TransportContext context = TransportContextHolder.getTransportContext();
		HttpServletConnection connection =(HttpServletConnection) context.getConnection();
		String inputXml = extractInputXml(messageContext);
		//TODO: REQUEST_SOURCE will comes only in case of inbound at request AccountTransferInboundSoapWebClient -> AddHttpHeaderInterceptor
		String requestSource = "";
	    if(connection.getRequestHeaders(AccountTransferConstants.REQUEST_SOURCE) != null && connection.getRequestHeaders(AccountTransferConstants.REQUEST_SOURCE).hasNext()){
		   requestSource = connection.getRequestHeaders(AccountTransferConstants.REQUEST_SOURCE).next();
	    }
		if(AccountTransferConstants.REQUEST_SOURCE_VALUE.toString().equals(requestSource)){
		  inputXml = inputXml.replace("http://www.w3.org/2001/XMLSchema-instance", "https://www.w3.org/2001/XMLSchema-instance");
		}
	    lOGGER.info(inputXml);
		messageContext.setProperty("inputXml", inputXml);
		MessageContextHolder.setMessageContext(messageContext);
		return true;
	}

	private String extractInputXml(MessageContext messageContext)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		messageContext.getRequest().writeTo(baos);
		return baos.toString();
	}

	@Override
	public boolean handleResponse(MessageContext messageContext, Object endpoint)
			throws Exception {
		return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext, Object endpoint)
			throws Exception {
		return true;
	}

	@Override
	public void afterCompletion(MessageContext messageContext, Object endpoint,
			Exception ex) throws Exception {
		/* Nothing to process as of now after completion */
	}

	@Override
	public int getOrder() {
		return 2;
	}

}
