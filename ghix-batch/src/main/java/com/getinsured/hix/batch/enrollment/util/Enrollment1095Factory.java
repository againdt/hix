package com.getinsured.hix.batch.enrollment.util;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory class to get the correct 1095 XML schema version according to coverage year
 * @author negi_s
 *
 */
@Component
public class Enrollment1095Factory {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095Factory.class);
	
	@Autowired
	private Enrollment1095XmlCy2018 enrollment1095XmlCy2018;
	@Autowired
	private Enrollment1095XmlLegacy enrollment1095XmlLegacy;
	@Autowired
	private Enrollment1095InboundCy2018 enrollment1095InboundCy2018;
	@Autowired
	private Enrollment1095InboundLegacy enrollment1095InboundLegacy;
	@Autowired
	private Enrollment1095XmlCy2019 enrollment1095XmlCy2019;
	@Autowired
	private Enrollment1095InboundCy2019 enrollment1095InboundCy2019;

	public Enrollment1095Xml getOutboundInstance(Integer year) {
		if (year >= 2018) {
			if (year == 2018) {
				return enrollment1095XmlCy2018;
			} else if (year > 2018) {
				return enrollment1095XmlCy2019;
			}
		} else {
			return enrollment1095XmlLegacy;
		}
		return null;
	}

	public Enrollment1095Inbound getInboundInstance(Integer year) {
			return year >=2018 ? enrollment1095InboundCy2018 : enrollment1095InboundLegacy;
	}
	
	public Enrollment1095Inbound getInboundInstanceFromYear(Integer year) {
		return year >=2018 ? enrollment1095InboundCy2018 : enrollment1095InboundLegacy;
	}
	
	public Enrollment1095Inbound getInboundInstanceFromFile(File file) {
		LOGGER.info("using Enrollment1095InboundCy2019 for inbound 1095 schema validation");
		return enrollment1095InboundCy2019 ;
	}
	
}