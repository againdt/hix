package com.getinsured.hix.batch.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.enrollment.skip.Enrollment1095Out;
import com.getinsured.hix.dto.enrollment.Enrollment1095PDFDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMember1095PDFDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremium1095PDFDTO;
import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.service.EnrollmentServiceImpl;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentPremium1095;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.pdf.filler.PlatformPdfFiller;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.Helpers;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Service("enrollmentAnnual1095BatchService")
public class EnrollmentAnnual1095BatchServiceImpl implements EnrollmentAnnual1095BatchService {

	@Autowired private IEnrollment1095Repository iEnrollment1095Repository;
	@Autowired private GhixDBSequenceUtil ghixDBSequenceUtil;
	@Autowired private PlatformPdfFiller platformPdffFiller;
	@Autowired private Enrollment1095PdfNotificationService enrollment1095PdfNotificationService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private NoticeRepository noticeRepository;
	@Autowired private UserService userService;
	@Autowired private Enrollment1095Out enrollment1095Out;
	@Autowired private Gson platformGson;
	
	private static final String TEMPLATE_BASE_FOLDER = "Enrollment1095Resources/PDF/Templates";
	private static final String CONFIG_BASE_FOLDER = "Enrollment1095Resources/PDF/Configurations";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnual1095BatchServiceImpl.class);
	
	private static final Float zeroFloat = 0.00f;
	private static String enrollment1095Config;
	private static Integer updatedBy;
	
	
	@Override
	public List<Integer> getUniqueIdsFor1095Pdf(){
		Date date = new Date();
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
		return iEnrollment1095Repository.getUniqueIdsFor1095Pdf(cal.get(Calendar.YEAR));
	}
	
	@Override
	public List<Integer> getUniqueIdsFor1095Pdf(List<String> householdIds){
		Date date = new Date();
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
		return iEnrollment1095Repository.getUniqueIdsFor1095Pdf(cal.get(Calendar.YEAR), householdIds);
	}
	
	@Override
	public List<String> getUniqueHouseholdIdsFor1095Pdf(){
		Date date = new Date();
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
		return iEnrollment1095Repository.getUniqueHouseholdIdsFor1095Pdf(cal.get(Calendar.YEAR));
	}
	
	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}
	
	@Override
	public String process1095Enrollment(Integer enrollment1095Id) throws Exception {
		Enrollment1095 enrollment1095 = null;
		String ecmPdfLink = "";
		if (enrollment1095Id != null) {
			try {
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Enrollment_1095: Current_Thread: " + Thread.currentThread().getName()+ " Enrollment_1095Id: " + enrollment1095Id);
				}
				enrollment1095 = iEnrollment1095Repository.findOne(enrollment1095Id);

				String fileName = "";
				if (enrollment1095.getVoidCheckboxindicator() != null && enrollment1095.getVoidCheckboxindicator().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())) {
					fileName = EnrollmentConstants.VOID_1095_OUT_FORMAT + (new Date().getTime()) + enrollment1095.getId() + ".pdf";
				} else if (enrollment1095.getCorrectedCheckBoxIndicator() != null && enrollment1095.getCorrectedCheckBoxIndicator().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())
						&& enrollment1095.getPdfFileName() != null && enrollment1095.getPdfGeneratedOn() != null) {
					fileName = EnrollmentConstants.CORRECTION_1095_OUT_FORMAT + (new Date().getTime()) + enrollment1095.getId() + ".pdf";
				} else if (enrollment1095.getResendPdfFlag() != null && enrollment1095.getResendPdfFlag().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())) {
					fileName = enrollment1095.getPdfFileName();
					if (StringUtils.isNotEmpty(fileName)) {
						fileName = fileName.substring(0, fileName.lastIndexOf("_") + 1) + (new Date().getTime()) + enrollment1095.getId() + ".pdf";
					} else {
						throw new Exception("For 1095A Resend request last sent PDF File name is found to be null or empty, Enrollment_1095 Id: "+ enrollment1095Id);
					}
				} else {
					fileName = EnrollmentConstants.INITIAL_1095_OUT_FORMAT + (new Date().getTime()) + enrollment1095.getId() + ".pdf";
				}

				String referenceNumber = String.format("%010d", Integer.parseInt(ghixDBSequenceUtil.getNextSequenceFromDB(EnrollmentConstants.NOTICE_SEQ_NAME)));
				Integer noticeId = new Integer(referenceNumber);
				if (enrollment1095.getResendPdfFlag() != null && Enrollment1095.YorNFlagIndicator.Y.toString().equalsIgnoreCase(enrollment1095.getResendPdfFlag())
						&& (enrollment1095.getCorrectionIndicatorPdf() == null || Enrollment1095.YorNFlagIndicator.N.toString().equalsIgnoreCase(enrollment1095.getCorrectionIndicatorPdf()))) {
					// call resend
					if(StringUtils.isNotBlank(enrollment1095.getIsAddressUpdated()) && enrollment1095.getIsAddressUpdated().equalsIgnoreCase("Y")){
						prepareAndSendPdf(enrollment1095, fileName, referenceNumber);
					}
					else{
						Notice notice = noticeRepository.findById(enrollment1095.getNotice());
						if (notice != null && StringUtils.isNotBlank(notice.getEcmId())) {
							ecmPdfLink = enrollment1095PdfNotificationService.resend1095ASecureInboxMsg(noticeId, fileName,
									new Integer(enrollment1095.getHouseHoldCaseId()), notice.getEcmId(),
									enrollment1095.getExchgAsignedPolicyId(), enrollment1095.getPolicyIssuerName(), notice.getEmailBody());
						} else {
							throw new Exception("No Notice Object found for Enrollment1095:  " + enrollment1095Id);
						}
					}
					enrollment1095Out.incrementResendPdfCount(1);
				} else {
					prepareAndSendPdf(enrollment1095, fileName, referenceNumber);
				}
				enrollment1095.setResendPdfFlag(Enrollment1095.YorNFlagIndicator.N.toString());
				enrollment1095.setIsAddressUpdated(Enrollment1095.YorNFlagIndicator.N.toString());
				enrollment1095.setPdfGeneratedOn(new Date());
				enrollment1095.setPdfFileName(fileName);
				if (isNotNullAndEmpty(referenceNumber)) {
					enrollment1095.setNotice(Integer.valueOf(referenceNumber));
				}
				enrollment1095.setPdfSkippedFlag(null);
				enrollment1095.setPdfSkippedMsg(null);

			} catch (Exception e) {
				LOGGER.error("Exception caught in process1095Enrollment(-) method:: ", e);
				enrollment1095Out.incrementSkippedPdfCount(1);
				enrollment1095.setPdfSkippedFlag(Enrollment1095.YorNFlagIndicator.Y.toString());
				enrollment1095.setPdfSkippedMsg(e.getMessage());
				throw new Exception("Exception caught in process1095Enrollment(-) method:: " + e);
			} finally {
				if (enrollment1095 != null) {
					LOGGER.info("Enrollment_1095: Before Saving: Thread_Name: " + Thread.currentThread().getName()+ " FileName: " + enrollment1095.getPdfFileName());
					if (updatedBy == null) {
						AccountUser user = getUser();
						if (user != null) {
							updatedBy = user.getId();
						}
					}
					enrollment1095.setUpdatedBy(updatedBy);
					iEnrollment1095Repository.saveAndFlush(enrollment1095);
				}
			}
		}
		return ecmPdfLink;
	}
	
	private String prepareAndSendPdf(Enrollment1095 enrollment1095, String fileName, String referenceNumber)throws Exception{
		String ecmPdfLink = StringUtils.EMPTY;
		List<EnrollmentMember1095> enrollmentMemberList = enrollment1095.getEnrollmentMembers();
		List<EnrollmentPremium1095> enrollmentPremiumList = enrollment1095.getEnrollmentPremiums();
		int memberPageCount = EnrollmentConstants.ONE;
		Enrollment1095PDFDTO enrollment1095PDFDTO = transformToEnrollment1095PDFDTO(enrollment1095, referenceNumber, enrollmentMemberList, enrollmentPremiumList);

		double memberCount = EnrollmentConstants.ZERO;
		if (enrollment1095PDFDTO.getCoveredIndividuals() != null) {
			memberCount = enrollment1095PDFDTO.getCoveredIndividuals().size();
		}

		/*
		 * Total Members Per Page is 5 Default Page size is 2
		 * {Address and Instructions for Recipient Pages}
		 */
		if (memberCount > 5) {
			memberPageCount = (int) Math.ceil(memberCount / 5);
		}
		enrollment1095PDFDTO.setPageNumber(("page 1 of " + (memberPageCount + EnrollmentConstants.TWO)));
		// to generate PDDocument
		PDDocument pdf = preparePDFEnrollment1095(enrollment1095PDFDTO, enrollment1095.getCoverageYear());

		for (int i = 4; i > memberPageCount; i--) {
			pdf.removePage(i);
		}

		try {
			Integer noticeId = new Integer(referenceNumber);
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			LOGGER.info("Enrollment_1095:: Thread-Name: " + Thread.currentThread().getName() + " FileName: "+ fileName);
			ecmPdfLink = enrollment1095PdfNotificationService.send1095ASecureInboxMsg(noticeId, fileName,
					toByteArray(pdf), new Integer(enrollment1095.getHouseHoldCaseId()),
					enrollment1095.getExchgAsignedPolicyId(), enrollment1095.getPolicyIssuerName(), gson.toJson(enrollment1095PDFDTO));
		} catch (Exception e) {
			LOGGER.error("Exception:: Failed to send PDF to inbox ", e);
			throw new Exception("Failed to send PDF to inbox " + e.getMessage());
		}

		if (enrollment1095.getVoidCheckboxindicator() != null && enrollment1095.getVoidCheckboxindicator().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())) {
			enrollment1095Out.incrementVoidPdfCount(1);
		} else if (enrollment1095.getCorrectedCheckBoxIndicator() != null && enrollment1095.getCorrectedCheckBoxIndicator().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())
				&& enrollment1095.getPdfFileName() != null && enrollment1095.getPdfGeneratedOn() != null) {
			enrollment1095Out.incrementCorrectedPdfCount(1);
		} else {
			enrollment1095Out.incrementInitialPdfCount(1);
		}

		enrollment1095.setCorrectionIndicatorPdf(Enrollment1095.YorNFlagIndicator.N.toString());
		
		return ecmPdfLink;
	}
			
	private Enrollment1095PDFDTO transformToEnrollment1095PDFDTO(Enrollment1095 enrollment1095, String referenceNumber, List<EnrollmentMember1095> enrollmentMemberList, List<EnrollmentPremium1095> enrollmentPremiumList) {
		Enrollment1095PDFDTO enrollment1095PDFDTO = null;
		
		if (enrollment1095 != null) {
			enrollment1095PDFDTO = new Enrollment1095PDFDTO();
			enrollment1095PDFDTO.setReferenceNumber("Reference Number: " + referenceNumber);
			if (enrollment1095.getExchgAsignedPolicyId() != null) {
				enrollment1095PDFDTO.setAssignedPolicyNumber(String.format("%06d", enrollment1095.getExchgAsignedPolicyId()));
			}
			enrollment1095PDFDTO.setPolicyIssuersName(enrollment1095.getPolicyIssuerName());
			if (enrollment1095.getVoidCheckboxindicator() != null && Enrollment1095.YorNFlagIndicator.Y.toString().equalsIgnoreCase(enrollment1095.getVoidCheckboxindicator())) {
				enrollment1095PDFDTO.setCorrectedFlag("false");
				enrollment1095PDFDTO.setVoidFlag("true");
			} else if (enrollment1095.getCorrectedCheckBoxIndicator() != null && Enrollment1095.YorNFlagIndicator.Y.toString().equalsIgnoreCase(enrollment1095.getCorrectedCheckBoxIndicator())
					&& enrollment1095.getPdfFileName() != null && enrollment1095.getPdfGeneratedOn() != null) {
				enrollment1095PDFDTO.setCorrectedFlag("true");
				enrollment1095PDFDTO.setVoidFlag("false");
			} else {
				enrollment1095PDFDTO.setCorrectedFlag("false");
				enrollment1095PDFDTO.setVoidFlag("false");
			}
			if (enrollment1095.getPolicyStartDate() != null) {
				enrollment1095PDFDTO.setPolicyStartDate(DateUtil.dateToString(enrollment1095.getPolicyStartDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			if (enrollment1095.getPolicyEndDate() != null) {
				enrollment1095PDFDTO.setPolicyTermDate(DateUtil.dateToString(enrollment1095.getPolicyEndDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
			}
			enrollment1095PDFDTO.setMarketPlaceIdentifier(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));

			setPremiumDetails(enrollment1095PDFDTO, enrollmentPremiumList, enrollment1095);
			setCoverageIndividualDetails(enrollment1095PDFDTO, enrollmentMemberList);
		}
		return enrollment1095PDFDTO;
	}

	/**
	 * 
	 * @param enrollment1095PDFDTO
	 * @param enrollmentMemberList
	 */
	private void setCoverageIndividualDetails(Enrollment1095PDFDTO enrollment1095PDFDTO, List<EnrollmentMember1095> enrollmentMemberList) {
		if (enrollment1095PDFDTO != null && enrollmentMemberList != null && enrollmentMemberList.size() > 0) {
			List<EnrollmentMember1095PDFDTO> memberDTOList = new ArrayList<>();
			for (EnrollmentMember1095 enrollmentMember1095 : enrollmentMemberList) {
				if (enrollmentMember1095.getIsActive() == null || Enrollment1095.YorNFlagIndicator.Y.toString().equalsIgnoreCase(enrollmentMember1095.getIsActive())) {
					EnrollmentMember1095PDFDTO member1095PDFDTO = new EnrollmentMember1095PDFDTO();
					String ssn = "";
					if (isNotNullAndEmpty(enrollmentMember1095.getSsn()) && enrollmentMember1095.getSsn().length() > 4) {
						ssn = EnrollmentConstants.SSN_PREFIX_MASKING_TEXT + enrollmentMember1095.getSsn().substring(enrollmentMember1095.getSsn().length() - 4);
						member1095PDFDTO.setSsn(ssn);
					}

					if (enrollmentMember1095.getBirthDate() != null) {
						member1095PDFDTO.setIndividualDOB(DateUtil.dateToString(enrollmentMember1095.getBirthDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
					}

					if (enrollmentMember1095.getCoverageStartDate() != null) {
						member1095PDFDTO.setCoverageStartDate(DateUtil.dateToString(enrollmentMember1095.getCoverageStartDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
					}
					if (enrollmentMember1095.getCoverageEndDate() != null) {
						member1095PDFDTO.setCoverageTerminationDate(DateUtil.dateToString(enrollmentMember1095.getCoverageEndDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
					}
					StringBuilder name = new StringBuilder();

					if (isNotNullAndEmpty(enrollmentMember1095.getFirstName())) {
						name.append(enrollmentMember1095.getFirstName());
						name.append(" ");
					}
					if (isNotNullAndEmpty(enrollmentMember1095.getMiddleName())) {
						name.append(enrollmentMember1095.getMiddleName());
						name.append(" ");
					}
					if (isNotNullAndEmpty(enrollmentMember1095.getLastName())) {
						name.append(enrollmentMember1095.getLastName());
						name.append(" ");
					}
					if (isNotNullAndEmpty(enrollmentMember1095.getNameSuffix())) {
						name.append(enrollmentMember1095.getNameSuffix());
					}
					if (enrollmentMember1095.getMemberType() != null && enrollmentMember1095.getMemberType().equalsIgnoreCase(EnrollmentMember1095.MemberType.MEMBER.toString())) {
						memberDTOList.add(member1095PDFDTO);
					}
					member1095PDFDTO.setName(name.toString());
					if (enrollmentMember1095.getMemberType() != null && enrollmentMember1095.getMemberType().equalsIgnoreCase(EnrollmentMember1095.MemberType.RECEPIENT.toString())) {
						if (enrollmentMember1095.getBirthDate() != null) {
							enrollment1095PDFDTO.setRecipientDOB(DateUtil.dateToString(enrollmentMember1095.getBirthDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
						}

						enrollment1095PDFDTO.setRecipientName(name.toString());
						enrollment1095PDFDTO.setRecipientSSN(ssn);
						enrollment1095PDFDTO.setCity(enrollmentMember1095.getCity());
						enrollment1095PDFDTO.setState(enrollmentMember1095.getState());
						String countryZip = "";
						
						if (isNotNullAndEmpty(enrollmentMember1095.getZip())) {
							countryZip = countryZip + " " + enrollmentMember1095.getZip();
						}
						enrollment1095PDFDTO.setCountryAndZip(countryZip);
						StringBuilder address = new StringBuilder();
						StringBuilder streetAddress = new StringBuilder();

						address.append("To:");
						address.append("\n");
						address.append(name);
						address.append("\n");
						address.append(enrollmentMember1095.getAddress1());
						streetAddress.append(enrollmentMember1095.getAddress1());
						if (isNotNullAndEmpty(enrollmentMember1095.getAddress2())) {
							address.append("\n");
							address.append(enrollmentMember1095.getAddress2());
							streetAddress.append(" ");
							streetAddress.append(enrollmentMember1095.getAddress2());
						}
						enrollment1095PDFDTO.setStreetAddress(streetAddress.toString());
						address.append("\n");
						if (isNotNullAndEmpty(enrollmentMember1095.getCity())) {
							address.append(enrollmentMember1095.getCity());
							address.append(", ");
						}
						if (isNotNullAndEmpty(enrollmentMember1095.getState())) {
							address.append(enrollmentMember1095.getState());
							address.append(", ");
						}
						if (isNotNullAndEmpty(enrollmentMember1095.getZip())) {
							address.append(enrollmentMember1095.getZip());
						}
						enrollment1095PDFDTO.setAddress(address.toString());
					}
					if (enrollmentMember1095.getMemberType() != null && enrollmentMember1095.getMemberType().equalsIgnoreCase(EnrollmentMember1095.MemberType.SPOUSE.toString())) {
						enrollment1095PDFDTO.setSpouseName(name.toString());
						enrollment1095PDFDTO.setSpouseSSN(ssn);
						if (enrollmentMember1095.getBirthDate() != null) {
							enrollment1095PDFDTO.setSpouseDOB(DateUtil.dateToString(enrollmentMember1095.getBirthDate(), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
						}
					}
				}
			}
			if (memberDTOList != null && memberDTOList.size() > 0) {
				enrollment1095PDFDTO.setCoveredIndividuals(memberDTOList);
			}
		}
	}

	/**
	 * 
	 * @param enrollment1095PDFDTO
	 * @param enrollmentPremiumList
	 * @param coverageYear
	 */
	private void setPremiumDetails(Enrollment1095PDFDTO enrollment1095PDFDTO,
			List<EnrollmentPremium1095> enrollmentPremiumList, Enrollment1095 enrollment1095) {

		BigDecimal annualPremium = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal annualSLCSP = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal annualAPTC = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		Integer lastPremPaidMonth = null;
		if (enrollment1095.isFinancial() && enrollment1095.isTermByNonPayment() && enrollment1095.getPremiumPaidToDateEnd() != null) {
			lastPremPaidMonth = enrollment1095.getPremiumPaidToDateEnd().getMonth() + 1;
		}

		if (enrollment1095PDFDTO != null && enrollmentPremiumList != null && enrollmentPremiumList.size() > 0) {
			for (EnrollmentPremium1095 enrollmentPremium1095 : enrollmentPremiumList) {
				if ((lastPremPaidMonth == null) || (lastPremPaidMonth != null && enrollmentPremium1095.getMonthNumber() != null && enrollmentPremium1095.getMonthNumber() <= lastPremPaidMonth)) {
					if (enrollmentPremium1095.getIsActive() == null || Enrollment1095.YorNFlagIndicator.Y.toString().equalsIgnoreCase(enrollmentPremium1095.getIsActive())) {
						EnrollmentPremium1095PDFDTO enrollmentPremium1095PDFDTO = new EnrollmentPremium1095PDFDTO();
						Float monthlyPremiumAmount = 0.0f;
						if (enrollmentPremium1095.getGrossPremium() != null && enrollmentPremium1095.getEhbPercent() != null) {
							monthlyPremiumAmount = enrollmentPremium1095.getGrossPremium() * enrollmentPremium1095.getEhbPercent();
						}
						// HIX-85422 2017 EHB portion for SADPs is a percentage of
						// total premium and not a dollar amount
						if (enrollment1095.getCoverageYear() >= 2017 && enrollmentPremium1095.getPediatricEhbAmt() != null) {
							monthlyPremiumAmount += enrollmentPremium1095.getPediatricEhbAmt();
						} else if (enrollment1095.getCoverageYear() < 2017 && enrollmentPremium1095.getPediatricEhbAmt() != null && enrollmentPremium1095.getAccountableMemberDental() != null) {
							monthlyPremiumAmount += (enrollmentPremium1095.getPediatricEhbAmt() * enrollmentPremium1095.getAccountableMemberDental());
						}
						annualPremium = annualPremium.add(getBigDecimalFromFloat(monthlyPremiumAmount));
						enrollmentPremium1095PDFDTO.setMonthlyEnrollmentPremium(
								String.format(EnrollmentConstants.DECIMAL_POINT_TWO, monthlyPremiumAmount));
						if (enrollmentPremium1095.getAptcAmount() != null) {
							enrollmentPremium1095PDFDTO.setMonthlyAPTC(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollmentPremium1095.getAptcAmount()));
							annualAPTC = annualAPTC.add(getBigDecimalFromFloat(enrollmentPremium1095.getAptcAmount()));
						} else {
							enrollmentPremium1095PDFDTO.setMonthlyAPTC(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, zeroFloat));
						}
						if (enrollmentPremium1095.getSlcspAmount() != null) {
							enrollmentPremium1095PDFDTO.setMonthlySLCSP(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollmentPremium1095.getSlcspAmount()));
							annualSLCSP = annualSLCSP.add(getBigDecimalFromFloat(enrollmentPremium1095.getSlcspAmount()));
						} else {
							enrollmentPremium1095PDFDTO.setMonthlySLCSP(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, zeroFloat));
						}
						if (enrollmentPremium1095.getMonthNumber() != null) {
							switch (enrollmentPremium1095.getMonthNumber()) {
							case 1:
								enrollment1095PDFDTO.setJanEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 2:
								enrollment1095PDFDTO.setFebEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 3:
								enrollment1095PDFDTO.setMarEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 4:
								enrollment1095PDFDTO.setAprEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 5:
								enrollment1095PDFDTO.setMayEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 6:
								enrollment1095PDFDTO.setJunEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 7:
								enrollment1095PDFDTO.setJulEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 8:
								enrollment1095PDFDTO.setAugEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 9:
								enrollment1095PDFDTO.setSepEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 10:
								enrollment1095PDFDTO.setOctEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 11:
								enrollment1095PDFDTO.setNovEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							case 12:
								enrollment1095PDFDTO.setDecEnrollmentPremium(enrollmentPremium1095PDFDTO);
								break;
							default:
								LOGGER.error("Invalid Month " + enrollmentPremium1095.getMonthNumber());
								break;
							}
						}
					}
				} 
			}
			if (lastPremPaidMonth != null && lastPremPaidMonth != 12) {
				annualAPTC = populateGracePeriodMonth(lastPremPaidMonth, enrollment1095PDFDTO, enrollmentPremiumList, annualAPTC);
			}
			enrollment1095PDFDTO.setAnnualAPTC(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, annualAPTC));
			enrollment1095PDFDTO.setAnnualPremium(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, annualPremium));
			enrollment1095PDFDTO.setAnnualSLCSP(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, annualSLCSP));
		}
	}

	/**
	 * 
	 * @param enrollment1095Dto
	 * @param coverageYear
	 * @return
	 * @throws Exception
	 */
	private PDDocument preparePDFEnrollment1095(Enrollment1095PDFDTO enrollment1095Dto, Integer coverageYear)throws Exception {
		PDDocument document = null;
		StringBuffer templateFileNameBuilder = null;
		StringBuffer configFileNameBuilder = null;
		if (enrollment1095Dto != null) {
			PDDocument enrollment1095Template;
			templateFileNameBuilder = new StringBuffer();
			configFileNameBuilder = new StringBuffer();
			templateFileNameBuilder.append(TEMPLATE_BASE_FOLDER).append("/").append(coverageYear).append("_").append("Enrollment1095Template.pdf");
			configFileNameBuilder.append(CONFIG_BASE_FOLDER).append("/").append(coverageYear).append("_").append("Enrollment1095Config.config");
			try (InputStream stream = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(templateFileNameBuilder.toString())) {
				enrollment1095Template = PDDocument.load(stream);
			} catch (IOException e) {
				LOGGER.error("Failed in loading template File: " + e);
				throw new Exception("Failed in loading template File");
			}
			if (enrollment1095Config == null || enrollment1095Config.trim().isEmpty()) {
				try {
					enrollment1095Config = readFile(StandardCharsets.UTF_8, configFileNameBuilder.toString());
				} catch (IOException e) {
					LOGGER.error("Failed in loading Config  File: " + e);
					throw new Exception("Failed in loading Config  File");
				}
			}
			try {
				document = platformPdffFiller.fill(Helpers.getFormString(enrollment1095Config), platformGson.toJson(enrollment1095Dto), enrollment1095Template);
			} catch (Exception e) {
				LOGGER.error("preparePDFEnrollment1095:: Failed to fill PDF: " + e);
				throw new Exception("Failed to fill pdf :: " + e.getMessage());
			}
		}
		return document;
	}
	
	/**
	 * 
	 * @param encoding
	 * @param configFileName
	 * @return
	 * @throws IOException
	 */
	private synchronized String readFile(Charset encoding, String configFileName) throws IOException {
		try (InputStream stream = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(configFileName)) {
			byte[] encoded = IOUtils.toByteArray(stream);
			return new String(encoded, encoding);
		}
	}
	
	/**
	 * 
	 * @param pdDoc
	 * @return
	 * @throws Exception
	 */
	private static byte[] toByteArray(PDDocument pdDoc) throws GIException {
		try(ByteArrayOutputStream out = new ByteArrayOutputStream();){
			pdDoc.save(out);
			pdDoc.close();
			return out.toByteArray();
		}
		catch(Exception ex){
			LOGGER.error("Exception caught while saving pdf: ", ex);
			throw new GIException("Exception caught while saving 1095 pdf",ex);
		}
	}

	/**
	 * 
	 * @return
	 */
	private AccountUser getUser() {
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if (null == user) {
				user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			}
		} catch (InvalidUserException e) {
			LOGGER.debug("Error fetching logged in user", e);
		}
		return user;
	}
	
	/**
	 * Converts float to BigDecimal
	 * @param amount
	 * @return
	 */
	private BigDecimal getBigDecimalFromFloat(Float amount) {

		BigDecimal bigDecimalAmount = null;
		if(amount !=null){
			try{
				bigDecimalAmount = new BigDecimal(Float.toString(amount)).setScale(2, BigDecimal.ROUND_HALF_UP);
			}catch(Exception e){
				LOGGER.error("Error parsing float amount" , e);
			}
		}
		return bigDecimalAmount;
	}
	
	private BigDecimal populateGracePeriodMonth(Integer lastPremPaidMonth, Enrollment1095PDFDTO enrollment1095PDFDTO, List<EnrollmentPremium1095> enrollmentPremium1095List, BigDecimal annualAPTC) {

		Float aptcAmt = null;
		for (EnrollmentPremium1095 enrollmentPremium1095 : enrollmentPremium1095List) {
			if (enrollmentPremium1095.getMonthNumber() != null && enrollmentPremium1095.getMonthNumber() == lastPremPaidMonth && enrollmentPremium1095.getAptcAmount() != null) {
				aptcAmt = enrollmentPremium1095.getAptcAmount();
			} else if (enrollmentPremium1095.getMonthNumber() != null && enrollmentPremium1095.getMonthNumber() == lastPremPaidMonth + 1 && enrollmentPremium1095.getAptcAmount() != null) {
				aptcAmt = enrollmentPremium1095.getAptcAmount();
				break;
			}
		}
		
		annualAPTC = annualAPTC.add(getBigDecimalFromFloat(aptcAmt));

		EnrollmentPremium1095PDFDTO enrollmentPremium1095PDFDTO = new EnrollmentPremium1095PDFDTO();
		enrollmentPremium1095PDFDTO.setMonthlyAPTC(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, aptcAmt));
		enrollmentPremium1095PDFDTO.setMonthlySLCSP(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, zeroFloat));
		enrollmentPremium1095PDFDTO.setMonthlyEnrollmentPremium(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, zeroFloat));
		
		switch (lastPremPaidMonth + 1) {
		case 1:
			enrollment1095PDFDTO.setJanEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 2:
			enrollment1095PDFDTO.setFebEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 3:
			enrollment1095PDFDTO.setMarEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 4:
			enrollment1095PDFDTO.setAprEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 5:
			enrollment1095PDFDTO.setMayEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 6:
			enrollment1095PDFDTO.setJunEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 7:
			enrollment1095PDFDTO.setJulEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 8:
			enrollment1095PDFDTO.setAugEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 9:
			enrollment1095PDFDTO.setSepEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 10:
			enrollment1095PDFDTO.setOctEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 11:
			enrollment1095PDFDTO.setNovEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		case 12:
			enrollment1095PDFDTO.setDecEnrollmentPremium(enrollmentPremium1095PDFDTO);
			break;
		default:
			LOGGER.error("Invalid Month ");
			break;
		}
		return annualAPTC;
	}
}
