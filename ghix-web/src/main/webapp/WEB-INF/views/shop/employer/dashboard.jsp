<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
<%@ taglib uri="/WEB-INF/tld/employer-dashboard-util" prefix="employerDashboardData"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<!-- <link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="<c:url value="/resources/js/shop/jquery.cookie.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>


<style>
.disablable {
    position:relative;
    overflow:hidden;
}

.disablable-disabled .glasspanel {
    display:block;
    position:absolute; 
    top:0px;
    bottom:0px;
    opacity:0.4; 
    filter:alpha(opacity=40); 
/*     background-color: #F0F0F0;  */
    height:expression(parentElement.scrollHeight + 'px');
    width:100%;
}

.disablable-enabled .glasspanel {
    display: none;
}



</style>
			
<div class="gutter10" id="portal">
	<div class="row-fluid">
		<h1 id="skip">${employerOBJ.name} Dashboard</h1>
    </div> <!-- End /.rowfluid -->
    
  	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>About your Dashboard</h4>
			</div>
			<div  class="gutter10">
             	<p>Your dashboard is the <br>place to check your <br>progress and review your<br> overall situation. If there<br> are action items that you <br> need to complete, you'll<br>find them here.</p>
			</div>            
			<div class="header">
                  <h4>Quick Links</h4>
			</div>
			<ul class="nav nav-list">
				<li><a href="<c:url value="/broker/search" />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
				<c:if test="${empCount > 0}">  
				<li><a href="#" onclick="javascript:window.location.href ='<c:url value="/shop/employer/manage/addemployee" />'" id="refid"><i class="icon-plus"></i> Add an Employee</a></li>
				</c:if>  
			</ul>   
		</div> <!-- End /.sidebar -->
			<div class="span9" id="rightpanel">
			    <c:if test="${EmployerCoverageRenewalStarted == 'N'}">
			   	<employerDashboardData:nav employer="${employerOBJ}"></employerDashboardData:nav>
			   	</c:if>
			   	
			   	<c:if test="${EmployerCoverageRenewalStarted == 'Y'}">
			    <employerDashboardData:renewalnav employer="${employerOBJ}"></employerDashboardData:renewalnav>
			    </c:if>   
					
				<div class="row-fluid ">
				    <!-- Employee summary util -->
				    <c:if test="${EmployerCoverageRenewalStarted == 'N'}">
			            <employerDashboardData:employeesummary employer="${employerOBJ}"></employerDashboardData:employeesummary>
			            <employerDashboardData:coverageSummary employer="${employerOBJ}"></employerDashboardData:coverageSummary> 
		 			</c:if>      
		 			<c:if test="${EmployerCoverageRenewalStarted == 'Y'}">
		 				<employerDashboardData:renewalemployeesummary employer="${employerOBJ}"></employerDashboardData:renewalemployeesummary>
			            <employerDashboardData:renewalcoverageSummary employer="${employerOBJ}"></employerDashboardData:renewalcoverageSummary> 
		 			</c:if>
				</div> <!-- End .row-fluid -->
			</div>
	</div><!-- End /.row-fluid -->
</div> <!-- End /#portal -->
 
  	<!-- update company Info modal start -->
			   	
			   	<div id="updateCompanyInfoModal" class="modal hide fade" tabindex="-1"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateCompanyInfo" aria-hidden="true">
				<!-- <input  type="hidden" name="changeEliDate" id="changeEliDate" value="N"> -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" onclick="resetForm();" aria-hidden="true">&times;</button>
						<h3 id="verifyNoOfEmpLabel"><spring:message  code='label.verifyNoOfEmpLabel'/></h3>
					</div>
				 	<form id="frmUpdateCompanyInfo" name="frmUpdateCompanyInfo" class="form-horizontal noBotMar">
						<df:csrfToken/>
						<div class="modal-body">
							<span id="updateEmpInfoBodySpan">
							 <spring:message code = "label.updateEmpInfoText" />
							</span><br/><br/>
							<div class="profile">
								<div class="control-group">
									<label class="control-label required" for="fullTimeEmp"><spring:message code='label.emplFulltime' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							        <div class="controls">
										<input id="fullTimeEmp" name="fullTimeEmp" " class="input-mini" size="3" maxlength="3" value="${employerOBJ.fullTimeEmp}"/>
										&nbsp;&nbsp;<small class="span9 pull-right"><a href="#fteModal" role="button" class="" data-toggle="modal">How do I calculate Full-Time Equivalent Employees?</a></small>
										<div id="fullTimeEmp_error"></div>
									</div>	
								</div>
								<div class="control-group">
									<label class="control-label required" for="averageSalary"><spring:message code='label.averageSalary' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.averageSalaryInfo"/> " class="info"> <i class="icon-question-sign"></i></a></label>
							        <div class="controls">
										<input id="averageSalary" name="averageSalary" class="input-large" size="30" maxlength="20" value="${employerOBJ.averageSalary}"/>
										<div id="averageSalary_error"></div>
									</div>	
								</div>						
							</div>
						</div>
					</form>
					<div class="modal-footer noTopPad">
						<button class="btn pull-left" id="updateCompanyInfoLaterBtn" data-dismiss="modal" onclick="resetForm();" aria-hidden="true"><spring:message  code='label.later'/></button> 
						<button class="btn btn-primary" id="submitCompanyInfoBtn" onclick = "javascript:submitCompanyInfo();">OK</button> 
						
					</div>
				</div>
			   	<!-- update company Info modal end -->
			   	
			   	<!-- update coverage options  modal start -->
			   	<div id="updateCoverageOptionsModal" class="modal hide fade" tabindex="-1"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateCoverageOptions" aria-hidden="true">
				<!-- <input  type="hidden" name="changeEliDate" id="changeEliDate" value="N"> -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3><spring:message code='label.updateCoverageOptionsLabel'/></h3>
					</div>
				 	 <form id="frmUpdateCoverageOptions" name="frmUpdateCoverageOptions">
						<df:csrfToken/>
						<div class="modal-body">
							<spring:message code='label.updateCoverageOptionsText'/>
							<%-- <span id="updateCoverageOptionsBodySpan">
							 <spring:message code = "label.updateEmpInfoText" />
							</span> --%><br/><br/>
							<div class="profile">
												
							</div>
						</div>
					</form> 
					<div class="modal-footer">
						<button class="btn btn-primary" id="getCoverageHomeBtn" onclick="javascript:selectCoverageOptions();"><spring:message  code='label.okButton'/></button>
					</div>
				</div> 
			   	<!-- update coverage options modal end -->
			   	
			   	<!-- Full-Time Equivalent Employees Modal -->
			<div id="fteModal" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3><spring:message code='label.emplFulltime' javaScriptEscape='true'/></h3>
				</div>
				<div class="modal-body">
					<p><spring:message  code="label.howcalculateFTEmodal1"/> ${maxShopEmployee} <spring:message  code="label.howcalculateFTEmodal2"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal3"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal4"/> <a href="http://www.irs.gov/uac/Small-Business-Health-Care-Tax-Credit-Questions-and-Answers:-Determining-FTEs-and-Average-Annual-Wages"><spring:message  code="label.howcalculateFTEmodal5"/></a> <spring:message  code="label.howcalculateFTEmodal6"/></p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.close'/></button>
				</div>
			</div>
 <script type="text/javascript">
 jQuery.validator.addMethod("fullTimeEmpcheck", function(value, element, param) {
		var fulltime = ( isNaN(parseInt($('#fullTimeEmp').val())) ? 0 : $('#fullTimeEmp').val() );
		var totalEmployees = parseInt(fulltime);
		if(totalEmployees <= 0){
	  		return false;	
	  	}return true;
	});
	jQuery.validator.addMethod("currencyCheck", function(value, element, param) {
		//var averageSalary = $('#averageSalary').val();
		var pattern = /^(\\$)?([0-9\,])+(\.\d{0,5})?$/;
		return pattern.test(value);
	});

	jQuery.validator.addMethod("greaterThanZero", function(value, element, param) {
		//var averageSalary = $('#averageSalary').val();
		var numVal = parseFloat(value.replace(/[^0-9.-]+/g, ''));
		
		if(isNaN(numVal)){
			return false;
		}
		if(numVal <=0){
			return false;}
		else{ 
			return true;}	
	});
 function openFindBrokerModal(){
	//if(selrecs.length>0){
		$('#findbroker').modal('show');
	//}else{
		//alert("Error opening light box");
	//}
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 function gotoNext(pageNum){
		if(pageNum=='1'){
			location.href="<c:url value='/shop/employer/companyinfo' />";
		}
		else if(pageNum=='2'){
			location.href="<c:url value='/shop/employer/roster/info' />";
		}
		else if(pageNum=='3'){
			location.href="<c:url value='/shop/employer/application/attestation' />";
		}
		else if(pageNum=='4'){
			location.href="<c:url value='/shop/employer/planselection/home' />";
		}
	}
 
 function gotoNextDuringRenewal(pageNum){
		$("#submitCompanyInfoBtn").attr("disabled", false); 
		if(pageNum=='1'){
			$('#updateCompanyInfoModal').modal('show');
		}
		else if(pageNum=='2'){
			location.href="<c:url value='/shop/employer/setupcoverage/home'/>";
		}
		else if(pageNum=='3'){
			location.href="<c:url value='/shop/employer/makepayment'/>";
		}
		else if(pageNum=='4'){

		}
	}

 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});
 
 $(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
 });
 
 function submitCompanyInfo() {
	 if( $("#frmUpdateCompanyInfo").validate().form()) {
		submitCompanyDetails();
	}  
}
 
 function resetForm() {
	document.getElementById('frmUpdateCompanyInfo').reset();
 }
 

function submitLocation() {
		$.ajax({
			type : "POST",
			url : "<c:url value='/shop/employer/roster/addWorksites'/>",
			data : $("#frmworksites").serialize(),
			dataType : 'json',
			beforeSend: function( xhr ) {
				$('#submitbutton').attr("disabled", true);
			},			
			success : function(response,xhr) {
				if(isInvalidCSRFToken(xhr))	    				
					return;
				populateLocations(response);
				application.remove_modal();
				$('#submitbutton').attr("disabled", false);				
			},
			error : function(e) {
				alert("<spring:message code='label.failedtoaddaddr' javaScriptEscape='true'/>");
				$('#submitbutton').attr("disabled", false);				
			}
		});
	}

 
 function submitCompanyDetails(){
	 $('#submitCompanyInfoBtn').attr("disabled", true);
	var subUrl = '<c:url value="/shop/employer/updatecompanyinfo">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	 $.ajax({
 		type: "POST",
		url: subUrl,
 		data: $("#frmUpdateCompanyInfo").serialize(),
 		async:false,
 		cache:false,
 		success: function(xhr){
			if(isInvalidCSRFToken(xhr))	    				
				return;
 			$('#updateCompanyInfoModal').modal('hide');
 			$('.modal-backdrop').remove();
 			$('#updateCoverageOptionsModal').modal('show');
		},error: function(xhr, status, error) {
			//var err = eval("(" + xhr.responseText + ")");
			//alert(err.Message);
			$("#submitCompanyInfoBtn").attr("disabled", false);       				
			alert("<spring:message code='label.failedtoupdatecompanyInfo' javaScriptEscape='true'/>");
		}
	 });
	}
 
 function gotoCoverageOptionsModal(){
	 $('#updateCoverageOptionsModal').modal('show');
 } 
  
 function selectCoverageOptions(){
	 location.href=location.href= 'dashboard?dt='+(new Date()).getTime();
 }
 var validator = $('#frmUpdateCompanyInfo').validate({ 
		rules : {
			fullTimeEmp : {required : true, number: true, fullTimeEmpcheck: true},
			averageSalary : { required : true, currencyCheck: true,greaterThanZero:true},
		},
		messages : {
			fullTimeEmp : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeemployee' javaScriptEscape='true'/> </span>",
							number: "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeempnumber' javaScriptEscape='true'/></span>" ,
							fullTimeEmpcheck: "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeempnumber' javaScriptEscape='true'/></span>" },
			averageSalary : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalary' javaScriptEscape='true'/> </span>",
							currencyCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalaryformat' javaScriptEscape='true'/> </span>",
							greaterThanZero : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalaryformat' javaScriptEscape='true'/> </span>"
			}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	}); 
	$('.info').tooltip();
	
</script>
	