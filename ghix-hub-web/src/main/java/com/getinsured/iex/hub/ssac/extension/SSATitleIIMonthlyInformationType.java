
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * A data type for the TitleII Monthly income information
 *             
 * 
 * <p>Java class for SSATitleIIMonthlyInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSATitleIIMonthlyInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}IncomeMonthYear"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}BenefitCreditedAmount"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}OverpaymentDeductionAmount" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PriorMonthAccrualAmount" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ReturnedCheckAmount" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PaymentInSuspenseIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}NetMonthlyBenefitCreditedAmount"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}MonthlyIncomeAmount"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSATitleIIMonthlyInformationType", propOrder = {
    "incomeMonthYear",
    "benefitCreditedAmount",
    "overpaymentDeductionAmount",
    "priorMonthAccrualAmount",
    "returnedCheckAmount",
    "paymentInSuspenseIndicator",
    "netMonthlyBenefitCreditedAmount",
    "monthlyIncomeAmount"
})
public class SSATitleIIMonthlyInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "IncomeMonthYear", required = true)
    protected String incomeMonthYear;
    @XmlElement(name = "BenefitCreditedAmount", required = true)
    protected BigDecimal benefitCreditedAmount;
    @XmlElement(name = "OverpaymentDeductionAmount")
    protected BigDecimal overpaymentDeductionAmount;
    @XmlElement(name = "PriorMonthAccrualAmount")
    protected BigDecimal priorMonthAccrualAmount;
    @XmlElement(name = "ReturnedCheckAmount")
    protected BigDecimal returnedCheckAmount;
    @XmlElement(name = "PaymentInSuspenseIndicator", required = true)
    protected Boolean paymentInSuspenseIndicator;
    @XmlElement(name = "NetMonthlyBenefitCreditedAmount", required = true)
    protected BigDecimal netMonthlyBenefitCreditedAmount;
    @XmlElement(name = "MonthlyIncomeAmount", required = true)
    protected BigDecimal monthlyIncomeAmount;

    /**
     * Gets the value of the incomeMonthYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomeMonthYear() {
        return incomeMonthYear;
    }

    /**
     * Sets the value of the incomeMonthYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomeMonthYear(String value) {
        this.incomeMonthYear = value;
    }

    /**
     * Gets the value of the benefitCreditedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBenefitCreditedAmount() {
        return benefitCreditedAmount;
    }

    /**
     * Sets the value of the benefitCreditedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBenefitCreditedAmount(BigDecimal value) {
        this.benefitCreditedAmount = value;
    }

    /**
     * Gets the value of the overpaymentDeductionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOverpaymentDeductionAmount() {
        return overpaymentDeductionAmount;
    }

    /**
     * Sets the value of the overpaymentDeductionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOverpaymentDeductionAmount(BigDecimal value) {
        this.overpaymentDeductionAmount = value;
    }

    /**
     * Gets the value of the priorMonthAccrualAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriorMonthAccrualAmount() {
        return priorMonthAccrualAmount;
    }

    /**
     * Sets the value of the priorMonthAccrualAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriorMonthAccrualAmount(BigDecimal value) {
        this.priorMonthAccrualAmount = value;
    }

    /**
     * Gets the value of the returnedCheckAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReturnedCheckAmount() {
        return returnedCheckAmount;
    }

    /**
     * Sets the value of the returnedCheckAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReturnedCheckAmount(BigDecimal value) {
        this.returnedCheckAmount = value;
    }

    /**
     * Gets the value of the paymentInSuspenseIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPaymentInSuspenseIndicator() {
        return paymentInSuspenseIndicator;
    }

    /**
     * Sets the value of the paymentInSuspenseIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaymentInSuspenseIndicator(Boolean value) {
        this.paymentInSuspenseIndicator = value;
    }

    /**
     * Gets the value of the netMonthlyBenefitCreditedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetMonthlyBenefitCreditedAmount() {
        return netMonthlyBenefitCreditedAmount;
    }

    /**
     * Sets the value of the netMonthlyBenefitCreditedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetMonthlyBenefitCreditedAmount(BigDecimal value) {
        this.netMonthlyBenefitCreditedAmount = value;
    }

    /**
     * Gets the value of the monthlyIncomeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMonthlyIncomeAmount() {
        return monthlyIncomeAmount;
    }

    /**
     * Sets the value of the monthlyIncomeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMonthlyIncomeAmount(BigDecimal value) {
        this.monthlyIncomeAmount = value;
    }

}
