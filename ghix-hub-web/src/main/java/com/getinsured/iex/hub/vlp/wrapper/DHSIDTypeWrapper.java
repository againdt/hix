package com.getinsured.iex.hub.vlp.wrapper;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.DHSIDType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.wrapper.util.DHSIDTypeWrapperUtil;


/**
 * This class is the wrapper for com.getinsured.gateway.vlp.request.model.DHSIDType
 * 
 * HIX-26932
 * 
 * @author Nikhil Talreja
 * @since 14-Jan-2014
 *
 */
@SuppressWarnings("rawtypes")
public class DHSIDTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory;
	private DHSIDType dhsIdType;
	
	private I327DocumentID3Wrapper i327DocumentID;
	private I551DocumentID4Wrapper i551DocumentID;
	private I571DocumentID5Wrapper i571DocumentID;
	private I766DocumentID9Wrapper i766DocumentID;
	private CertOfCitDocumentID23Wrapper certOfCitDocumentID;
	private NatrOfCertDocumentID20Wrapper natrOfCertDocumentID;
	private MacReadI551DocumentID22Wrapper macReadI551DocumentID;
	private TempI551DocumentID21Wrapper tempI551DocumentID;
	private I94DocumentID2Wrapper i94DocumentID;
	private I94UnexpForeignPassportDocumentID10Wrapper i94UnexpForeignPassportDocumentID;
	private UnexpForeignPassportDocumentID30Wrapper unexpForeignPassportDocumentID;
	private I20DocumentID26Wrapper i20DocumentID;
	private DS2019DocumentID27Wrapper ds2019DocumentID;
	private OtherCase1DocumentID1Wrapper otherCase1DocumentID;
	private OtherCase2DocumentID1Wrapper otherCase2DocumentID;
	
	private static final String I327_KEY = "I327DocumentID";
	private static final String I551_KEY = "I551DocumentID";
	private static final String I571_KEY = "I571DocumentID";
	private static final String I766_KEY = "I766DocumentID";
	private static final String CC_KEY = "CertOfCitDocumentID";
	private static final String NC_KEY = "NatrOfCertDocumentID";
	private static final String MAC_READ_I551_KEY = "MacReadI551DocumentID";
	private static final String TEMP_I551_KEY = "TempI551DocumentID";
	private static final String I94_KEY = "I94DocumentID";
	private static final String I94_UNEXP_KEY = "I94UnexpForeignPassportDocumentID";
	private static final String UNEXP_PASSPORT_KEY = "UnexpForeignPassportDocumentID";
	private static final String I20_KEY = "I20DocumentID";
	private static final String DS2019_KEY = "DS2019DocumentID";
	private static final String OTHER_CASE_1_KEY = "OtherCase1DocumentID";
	private static final String OTHER_CASE_2_KEY = "OtherCase2DocumentID";
	
	private static Map<Class,String> setterMappings; 
	
	private static final String UNCHECKED = "unchecked";
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
		
		setterMappings = new HashMap<Class, String>();
		
		setterMappings.put(I327DocumentID3Wrapper.class,"setI327Document");
		setterMappings.put(I551DocumentID4Wrapper.class,"setI551Document");
		setterMappings.put(I571DocumentID5Wrapper.class,"setI571Document");
		setterMappings.put(I766DocumentID9Wrapper.class,"setI766Document");
		setterMappings.put(CertOfCitDocumentID23Wrapper.class,"setCertOfCitDocument");
		setterMappings.put(NatrOfCertDocumentID20Wrapper.class,"setNatrOfCertDocument");
		setterMappings.put(MacReadI551DocumentID22Wrapper.class,"setMacReadI551Document");
		setterMappings.put(TempI551DocumentID21Wrapper.class,"setTempI551Document");
		setterMappings.put(I94DocumentID2Wrapper.class,"setI94Document");
		setterMappings.put(I94UnexpForeignPassportDocumentID10Wrapper.class,"setI94UnExpForeignPassportDoc");
		setterMappings.put(UnexpForeignPassportDocumentID30Wrapper.class,"setUnexpForeignPassportDocument");
		setterMappings.put(I20DocumentID26Wrapper.class,"setI20Document");
		setterMappings.put(DS2019DocumentID27Wrapper.class,"setDs2019Document");
		setterMappings.put(OtherCase1DocumentID1Wrapper.class,"setOtherCase1Document");
		setterMappings.put(OtherCase2DocumentID1Wrapper.class,"setOtherCase2Document");
	}
	public DHSIDTypeWrapper(){ 
		this.dhsIdType  = factory.createDHSIDType();
	}
	
	/**
	 * Parametrised constructor to call the setter for the required DHS ID type doc
	 * The setter also does specific validations for the Doc type
	 * 
	 * @author - Nikhil Talreja
	 * @throws HubServiceException in case of any exceptions
	 * @since - 14-Feb-2014
	 * 
	 * @param dhsDoc - Object containing the DHS Doc Id type
	 */
	public DHSIDTypeWrapper(Object dhsDoc) throws HubServiceException{ 
		
		this.dhsIdType  = factory.createDHSIDType();
		
		try{
			Class[] args = {dhsDoc.getClass()};
			Method setter = getClass().getMethod(setterMappings.get(dhsDoc.getClass()), args);
			setter.invoke(this,dhsDoc);
		}
		catch(Exception e){
			throw new HubServiceException(e.getMessage(),e);
		}
		
	}
	
	public DHSIDType getDhsId(){
		return this.dhsIdType;
	}
	
	/**
	 * This method sets the I327 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @throws HubServiceException 
	 * @since - 14-Jan-2014
	 *
	 */
	public void setI327Document(I327DocumentID3Wrapper i327Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(i327Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for I327 document");
		}
		
		this.i327DocumentID = i327Doc;
		this.dhsIdType.setI327DocumentID(i327Doc.geti327DocumentID3Type());
	}
	
	/**
	 * This method sets the I551 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setI551Document(I551DocumentID4Wrapper i551Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(i551Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for I551 document");
		}
		
		if(StringUtils.isBlank(i551Doc.getReceiptNumber())){
			throw new HubServiceException("Receipt number is required for I551 document");
		}
		
		this.i551DocumentID = i551Doc;
		this.dhsIdType.setI551DocumentID(i551Doc.geti551DocumentID4Type());
	}
	
	/**
	 * This method sets the I571 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 */
	public void setI571Document(I571DocumentID5Wrapper i571Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(i571Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for I571 document");
		}
		
		this.i571DocumentID = i571Doc;
		this.dhsIdType.setI571DocumentID(i571Doc.geti571DocumentID5Type());
	}
	
	/**
	 * This method sets the I766 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setI766Document(I766DocumentID9Wrapper i766Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(i766Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for I766 document");
		}
		
		if(StringUtils.isBlank(i766Doc.getReceiptNumber())){
			throw new HubServiceException("Receipt number is required for I766 document");
		}
		
		if(StringUtils.isBlank(i766Doc.getExpiryDate())){
			throw new HubServiceException("Expiry date is required for I766 document");
		}
		
		this.i766DocumentID = i766Doc;
		this.dhsIdType.setI766DocumentID(i766Doc.getI766DocumentID9TypeType());
	}
	
	/**
	 * This methods sets the Certificate of Citizenship information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setCertOfCitDocument(CertOfCitDocumentID23Wrapper certOfCitizenship) throws HubServiceException{
		
		if(StringUtils.isBlank(certOfCitizenship.getAlienNumber())){
			throw new HubServiceException("Alien number is required for Citizenship Certificate");
		}
		
		if(StringUtils.isBlank(certOfCitizenship.getCitizenshipNumber())){
			throw new HubServiceException("Citizenship number is required for Citizenship Certificate");
		}
		
		this.certOfCitDocumentID = certOfCitizenship;
		this.dhsIdType.setCertOfCitDocumentID(certOfCitizenship.getCertOfCitDocumentID23Type());
	}
	
	/**
	 * This methods sets the Naturalization Certificate information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setNatrOfCertDocument(NatrOfCertDocumentID20Wrapper naturalizationNumber) throws HubServiceException{
		
		if(StringUtils.isBlank(naturalizationNumber.getAlienNumber())){
			throw new HubServiceException("Alien number is required for Naturalization Certificate");
		}
		
		if(StringUtils.isBlank(naturalizationNumber.getNaturalizationNumber())){
			throw new HubServiceException("Naturalization number is required for Naturalization Certificate");
		}
		
		this.natrOfCertDocumentID = naturalizationNumber;
		this.dhsIdType.setNatrOfCertDocumentID(naturalizationNumber.getNatrOfCitDocumentID23Type());
	}
	
	/**
	 * This method sets the Machine Readable I551 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 */
	public void setMacReadI551Document(MacReadI551DocumentID22Wrapper macReadI551) throws HubServiceException{
		
		if(StringUtils.isBlank(macReadI551.getAlienNumber())){
			throw new HubServiceException("Alien number is required for Machine Readable I551 document");
		}
		
		if(StringUtils.isBlank(macReadI551.getPassportNumber())){
			throw new HubServiceException("Passport number is required for Machine Readable I551 document");
		}
		
		this.macReadI551DocumentID = macReadI551;
		this.dhsIdType.setMacReadI551DocumentID(macReadI551.getMacReadI551DocumentID22Type());
	}
	
	/**
	 * This method sets the Temporary I551 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setTempI551Document(TempI551DocumentID21Wrapper tmpI551Doc) throws HubServiceException{
		
		if(!validatePassport(tmpI551Doc.getPassportCountry())){
				throw new HubServiceException("Passport Number and/or Passport Country is missing for Temporary I551 document");
		}
		
		if(StringUtils.isBlank(tmpI551Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for Temporary I551 document");
		}
		
		this.tempI551DocumentID = tmpI551Doc;
		this.dhsIdType.setTempI551DocumentID(tmpI551Doc.getTempI551DocumentID21Type());
	}
	
	/**
	 * This method sets the I94 Doc information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setI94Document(I94DocumentID2Wrapper i94Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(i94Doc.getI94Number())){
			throw new HubServiceException("I94 number is required for I94 document");
		}
		
		this.i94DocumentID = i94Doc;
		this.dhsIdType.setI94DocumentID(i94Doc.geti94DocumentID2Type());
	}
	
	/**
	 * This method sets the I94 Unexpired Foreign Passport information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setI94UnExpForeignPassportDoc(
			I94UnexpForeignPassportDocumentID10Wrapper i94UnexpPassportDoc) throws HubServiceException{
		
		if(StringUtils.isBlank(i94UnexpPassportDoc.getI94Number())){
			throw new HubServiceException("I94 number is required for I94 Unexpired Foreign Passport");
		}
		
		if(StringUtils.isBlank(i94UnexpPassportDoc.getPassportNumber())){
			throw new HubServiceException("Passport number is required for I94 Unexpired Foreign Passport");
		}
		
		if(StringUtils.isBlank(i94UnexpPassportDoc.getExpiryDate())){
			throw new HubServiceException("Expiry date is required for I94 Unexpired Foreign Passport");
		}
		
		this.i94UnexpForeignPassportDocumentID = i94UnexpPassportDoc;
		this.dhsIdType.setI94UnexpForeignPassportDocumentID(i94UnexpPassportDoc.getI94UnexpForeignPassportDocumentID10Type());
	}
	
	/**
	 * This method sets the Unexpired Foreign Passport information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setUnexpForeignPassportDocument(
			UnexpForeignPassportDocumentID30Wrapper unexpForeignPassportDoc) throws HubServiceException{
		
		if(StringUtils.isBlank(unexpForeignPassportDoc.getPassportNumber())){
			throw new HubServiceException("Passport number is required for Unexpired Foreign Passport");
		}
		
		if(StringUtils.isBlank(unexpForeignPassportDoc.getExpiryDate())){
			throw new HubServiceException("Expiry date is required for Unexpired Foreign Passport");
		}
		
		this.unexpForeignPassportDocumentID = unexpForeignPassportDoc;
		this.dhsIdType.setUnexpForeignPassportDocumentID(unexpForeignPassportDoc.getUnexpForeignPassportDocumentID30Type());
	}
	
	/**
	 * This method sets the I20 Document information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 */
	public void setI20Document(I20DocumentID26Wrapper i20Doc) throws HubServiceException{
		
		if(!validatePassport(i20Doc.getPassportCountry())){
			throw new HubServiceException("Passport Number and/or Passport Country is missing for I20 document");
		}
		
		if(StringUtils.isBlank(i20Doc.getSevisid())){
			throw new HubServiceException("SEVIS is required for I20 document");
		}
		
		this.i20DocumentID = i20Doc;
		this.dhsIdType.setI20DocumentID(i20Doc.getI20DocumentID26Type());
	}
	
	/**
	 * This method sets the DS 2019 Document information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setDs2019Document(DS2019DocumentID27Wrapper ds2019Doc) throws HubServiceException{
		
		if(StringUtils.isBlank(ds2019Doc.getSevisid())){
			throw new HubServiceException("SEVIS is required for DS2019 document");
		}
		
		this.ds2019DocumentID = ds2019Doc;
		this.dhsIdType.setDS2019DocumentID(ds2019Doc.getDS2019DocumentID27Type());
	}
	
	/**
	 * This method sets the Other Case 1 Document information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setOtherCase1Document(
			OtherCase1DocumentID1Wrapper otherCaseId1Doc) throws HubServiceException{
		
		if(!validatePassport(otherCaseId1Doc.getPassportCountry())){
			throw new HubServiceException("Passport Number and/or Passport Country is missing for Other Case 1 document");
		}
		
		if(StringUtils.isBlank(otherCaseId1Doc.getAlienNumber())){
			throw new HubServiceException("Alien number is required for Other Case 1 document");
		}
		
		if(StringUtils.isBlank(otherCaseId1Doc.getDocDescReq())){
			throw new HubServiceException("Doument description is required for Other Case 1 document");
		}
		
		this.otherCase1DocumentID = otherCaseId1Doc;
		this.dhsIdType.setOtherCase1DocumentID(otherCaseId1Doc.getOtherCase1DocumentID1Type());
	}
	
	/**
	 * This method sets the Other Case 2 Document information for the request to DHS
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 */
	public void setOtherCase2Document(
			OtherCase2DocumentID1Wrapper otherCaseId2Doc) throws HubServiceException{
		
		if(!validatePassport(otherCaseId2Doc.getPassportCountry())){
			throw new HubServiceException("Passport Number and/or Passport Country is missing for Other Case 2 document");
		}
		
		if(StringUtils.isBlank(otherCaseId2Doc.getI94Number())){
			throw new HubServiceException("I94 number is required for Other Case 2 document");
		}
		
		if(StringUtils.isBlank(otherCaseId2Doc.getDocDescReq())){
			throw new HubServiceException("Doument description is required for Other Case 2 document");
		}
		
		this.otherCase2DocumentID = otherCaseId2Doc;
		this.dhsIdType.setOtherCase2DocumentID(otherCaseId2Doc.getOtherCase2DocumentID1Type());
		
	}
	
	/**
	 * Passport country is required if passport number is provided
	 * Only passport country is not allowed
	 */
	private boolean validatePassport(PassportCountryWrapper passportCountry){
		
		if(passportCountry == null || 
				(StringUtils.isBlank(passportCountry.getPassportCountryType().getPassportNumber()) 
						&& StringUtils.isBlank(passportCountry.getPassportCountryType().getCountryOfIssuance()))){
			return true;				
		}
		
		if(passportCountry != null &&
				(StringUtils.isBlank(passportCountry.getPassportCountryType().getPassportNumber()) 
						|| StringUtils.isBlank(passportCountry.getPassportCountryType().getCountryOfIssuance()))){
			return false;
		}
		
		return true;
	}
	
	/**
	 * Converts the JSON String to DHSIDTypeWrapper object
	 * 
	 * @param jsonString - String representation for DHSIDTypeWrapper
	 * @return DHSIDTypeWrapper Object
	 */
	public static DHSIDTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create DHSIDTypeWrapper from null or empty input");
		}

		DHSIDTypeWrapper wrapper = new DHSIDTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		DHSIDTypeWrapperUtil.setITypeDocsToWrapper(obj, wrapper);
		
		param = obj.get(MAC_READ_I551_KEY);
		if(param != null){
			wrapper.setMacReadI551Document(MacReadI551DocumentID22Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(TEMP_I551_KEY);
		if(param != null){
			wrapper.setTempI551Document(TempI551DocumentID21Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I94_KEY);
		if(param != null){
			wrapper.setI94Document(I94DocumentID2Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I94_UNEXP_KEY);
		if(param != null){
			wrapper.setI94UnExpForeignPassportDoc(I94UnexpForeignPassportDocumentID10Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(UNEXP_PASSPORT_KEY);
		if(param != null){
			wrapper.setUnexpForeignPassportDocument(UnexpForeignPassportDocumentID30Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I20_KEY);
		if(param != null){
			wrapper.setI20Document(I20DocumentID26Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(DS2019_KEY);
		if(param != null){
			wrapper.setDs2019Document(DS2019DocumentID27Wrapper.fromJsonString((String)param));
		}
		
		DHSIDTypeWrapperUtil.setOtherTypeDocsToWrapper(obj, wrapper);

		
		return wrapper;
	}
	
	public static DHSIDTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		DHSIDTypeWrapper wrapper = new DHSIDTypeWrapper();
		
		DHSIDTypeWrapperUtil.setITypeKeysToWrapper(jsonObj, wrapper);
		
		if(jsonObj.get(CC_KEY) != null){
			wrapper.setCertOfCitDocument(CertOfCitDocumentID23Wrapper.fromJsonObject((JSONObject) jsonObj.get(CC_KEY)));
		}
		if(jsonObj.get(NC_KEY) != null){
			wrapper.setNatrOfCertDocument(NatrOfCertDocumentID20Wrapper.fromJsonObject((JSONObject) jsonObj.get(NC_KEY)));
		}
		if(jsonObj.get(MAC_READ_I551_KEY) != null){
			wrapper.setMacReadI551Document(MacReadI551DocumentID22Wrapper.fromJsonObject((JSONObject) jsonObj.get(MAC_READ_I551_KEY)));
		}
		if(jsonObj.get(TEMP_I551_KEY) != null){
			wrapper.setTempI551Document(TempI551DocumentID21Wrapper.fromJsonObject((JSONObject) jsonObj.get(TEMP_I551_KEY)));
		}
		if(jsonObj.get(I94_KEY) != null){
			wrapper.setI94Document(I94DocumentID2Wrapper.fromJsonObject((JSONObject) jsonObj.get(I94_KEY)));
		}
		if(jsonObj.get(I94_UNEXP_KEY) != null){
			wrapper.setI94UnExpForeignPassportDoc(I94UnexpForeignPassportDocumentID10Wrapper.fromJsonObject((JSONObject) jsonObj.get(I94_UNEXP_KEY)));
		}
		if(jsonObj.get(UNEXP_PASSPORT_KEY) != null){
			wrapper.setUnexpForeignPassportDocument(UnexpForeignPassportDocumentID30Wrapper.fromJsonObject((JSONObject) jsonObj.get(UNEXP_PASSPORT_KEY)));
		}
		if(jsonObj.get(I20_KEY) != null){
			wrapper.setI20Document(I20DocumentID26Wrapper.fromJsonObject((JSONObject) jsonObj.get(I20_KEY)));
		}
		if(jsonObj.get(DS2019_KEY) != null){
			wrapper.setDs2019Document(DS2019DocumentID27Wrapper.fromJsonObject((JSONObject) jsonObj.get(DS2019_KEY)));
		}
		
		DHSIDTypeWrapperUtil.setOtherTypeKeysToWrapper(jsonObj, wrapper);
		
		return wrapper;
	}
	
	@SuppressWarnings(UNCHECKED)
	public String toJSONString() {
		
		JSONObject obj = new JSONObject();
		
		setDocTypesToJSONString(obj);
		
		if(this.certOfCitDocumentID != null){
			obj.put(CC_KEY, this.certOfCitDocumentID.toJSONObject());
		}
		if(this.natrOfCertDocumentID != null){
			obj.put(NC_KEY, this.natrOfCertDocumentID.toJSONObject());
		}
		if(this.macReadI551DocumentID != null){
			obj.put(MAC_READ_I551_KEY, this.macReadI551DocumentID.toJSONObject());
		}
		if(this.tempI551DocumentID != null){
			obj.put(TEMP_I551_KEY, this.tempI551DocumentID.toJSONObject());
		}
		if(this.i94DocumentID != null){
			obj.put(I94_KEY, this.i94DocumentID.toJSONObject());
		}
		if(this.i94UnexpForeignPassportDocumentID != null){
			obj.put(I94_UNEXP_KEY, this.i94UnexpForeignPassportDocumentID.toJSONObject());
		}
		if(this.unexpForeignPassportDocumentID != null){
			obj.put(UNEXP_PASSPORT_KEY, this.unexpForeignPassportDocumentID.toJSONObject());
		}
		if(this.i20DocumentID != null){
			obj.put(I20_KEY, this.i20DocumentID.toJSONObject());
		}
		if(this.ds2019DocumentID != null){
			obj.put(DS2019_KEY, this.ds2019DocumentID.toJSONObject());
		}

		setOtherDocTypesToJSONString(obj);
		
		return obj.toJSONString();
	}
	
	@SuppressWarnings(UNCHECKED)
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		
		setDocTypesToJSONString(obj);
		
		if(this.certOfCitDocumentID != null){
			obj.put(CC_KEY, this.certOfCitDocumentID.toJSONObject());
		}
		if(this.natrOfCertDocumentID != null){
			obj.put(NC_KEY, this.natrOfCertDocumentID.toJSONObject());
		}
		if(this.macReadI551DocumentID != null){
			obj.put(MAC_READ_I551_KEY, this.macReadI551DocumentID.toJSONObject());
		}
		if(this.tempI551DocumentID != null){
			obj.put(TEMP_I551_KEY, this.tempI551DocumentID.toJSONObject());
		}
		if(this.i94DocumentID != null){
			obj.put(I94_KEY, this.i94DocumentID.toJSONObject());
		}
		if(this.i94UnexpForeignPassportDocumentID != null){
			obj.put(I94_UNEXP_KEY, this.i94UnexpForeignPassportDocumentID.toJSONObject());
		}
		if(this.unexpForeignPassportDocumentID != null){
			obj.put(UNEXP_PASSPORT_KEY, this.unexpForeignPassportDocumentID.toJSONObject());
		}
		if(this.i20DocumentID != null){
			obj.put(I20_KEY, this.i20DocumentID.toJSONObject());
		}
		if(this.ds2019DocumentID != null){
			obj.put(DS2019_KEY, this.ds2019DocumentID.toJSONObject());
		}
		
		setOtherDocTypesToJSONString(obj);
		
		return obj;
	}
	
	@SuppressWarnings(UNCHECKED)
	private void setDocTypesToJSONString(JSONObject obj){
		
		if(this.i327DocumentID != null){
			obj.put(I327_KEY, this.i327DocumentID.toJSONObject());
		}
		if(this.i551DocumentID != null){
			obj.put(I551_KEY, this.i551DocumentID.toJSONObject());
		}
		if(this.i571DocumentID != null){
			obj.put(I571_KEY, this.i571DocumentID.toJSONObject());
		}
		if(this.i766DocumentID != null){
			obj.put(I766_KEY, this.i766DocumentID.toJSONObject());
		}
		
	}
	
	@SuppressWarnings(UNCHECKED)
	private void setOtherDocTypesToJSONString(JSONObject obj){
		
		if(this.otherCase1DocumentID != null){
			obj.put(OTHER_CASE_1_KEY, this.otherCase1DocumentID.toJSONObject());
		}
		if(this.otherCase2DocumentID != null){
			obj.put(OTHER_CASE_2_KEY, this.otherCase2DocumentID.toJSONObject());
		}
		
	}
	
}
