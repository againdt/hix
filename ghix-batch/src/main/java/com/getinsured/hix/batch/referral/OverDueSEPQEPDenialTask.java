/**
 * 
 */
package com.getinsured.hix.batch.referral;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.referral.service.DenySepQepService;

/**
 * @author Deepa
 *
 */
public class OverDueSEPQEPDenialTask extends
		BaseLCEReminderNotificationTask {
	private static final Logger LOGGER = Logger.getLogger(OverDueSEPQEPDenialTask.class);
	
	private DenySepQepService denySepQepService;

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		
		try {	
			denySepQepService.overDueSepQepDenial();
		} catch(Exception ex) {
			LOGGER.error("ERR: WHILE EXECUTING OVER DUE SEP QEP DENIAL: ",ex);
		}
		
		return RepeatStatus.FINISHED;
	}

	public DenySepQepService getDenySepQepService() {
		return denySepQepService;
	}

	public void setDenySepQepService(DenySepQepService denySepQepService) {
		this.denySepQepService = denySepQepService;
	}

	
}
