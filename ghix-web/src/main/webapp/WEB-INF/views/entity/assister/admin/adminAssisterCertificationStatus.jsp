<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@page import="com.getinsured.hix.model.Assister"%> --%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

<%-- <%
Assister mybroker = (Assister)request.getAttribute("assister");
Date currentdate = new Date();
//if certification date is null, then renew button need not be shown, thus, initializing diffDays with 40 any number which is greater than 30
long diffDays = 40;
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();
}
Calendar calUpdated = Calendar.getInstance();
if(mybroker.getReCertificationDate()!=null){
calUpdated.set(mybroker.getReCertificationDate().getYear()+1900, mybroker.getReCertificationDate().getMonth(),  mybroker.getReCertificationDate().getDate());
long currentDateMilliSec = currentdate.getTime();
long updateDateMilliSec = calUpdated.getTimeInMillis();
diffDays = (updateDateMilliSec - currentDateMilliSec) / (24 * 60 * 60 * 1000);
System.out.println("test differ days "+diffDays);
}
%> --%>

<script>
function getComment(comments)
{	
	$('#commentDet').html("<p> Loading Comment...</p>");
	comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
	comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
	$('#commentDet').html("<p> "+ comments + "</p>");
	
}
$(function () {
	$("a[rel=twipsy]").twipsy({
		live: true
	});
});

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault()  // prevent the default action, e.g., following a link
		});
});

$(document).ready(function(){
	$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes *
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val; 
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
</script>

				<div class="gutter10">
					<div class="navbar" id="menu">
						<div class="navbar-inner">
							<ul class="nav">
								<li class="dropdown"><a href="#" title="Dashboard" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i></a></li>
								<li class="dropdown"><a href="/hix/entity/assisteradmin/viewassisterinformation?assisterId=${assister.id}" title="Assister" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="label.assister.assisters"/></a></li>
								<li class="dropdown"><a href="/hix/entity/assisteradmin/viewassisterinformation?assisterId=${assister.entity.id}" title="Plans" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="label.assister.entities"/></a></li>
								<li class="dropdown"><a href="#" title="Brokers" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="label.assister.tickets"/></a></li>
							</ul>
						</div>
					</div>
				</div>

<div class="gutter10-lr">
	
	<div class="row-fluid">
		<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">
				<li><a href="/hix/entity/assisteradmin/viewassisterinformation?assisterId=${assister.id}"><spring:message code="label.assister.assisterInformation"/></a></li>
				<li class="active"><a href="/hix/entity/assisteradmin/viewassisterprofile?assisterId=${assister.id}"><spring:message code="label.assister.profile"/></a></li>
				<li><a	href="/entity/assister/viewassistercertificationstatus?assisterId=${assister.id}"><spring:message code="label.assister.certificationStatus"/></a></li>
			</ul>
		</div>
		
		<div class="span9">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.brkCertificationStatus"/></h4>
				<c:if test="${assister.certificationStatus != 'InComplete'}">
					<a class="btn btn-small pull-right"  href="<c:url value="/entity/assister/registration/${assister.id}"/>"><spring:message code="label.brkEdit"/></a>
				</c:if>
			</div>	
		
			<form class="form-horizontal" id="frmassistercertificationstatus" name="frmassistercertificationstatus" action="certificationstatus"  method="POST">
			
			<input type="hidden" id="documentId1" name="documentId1" value=""/>	
				<div class="gutter10-tb">
					<table class="table table-border-none verticalThead">
						<tbody>						
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.brkCertificationStatus"/></th>
								<td><strong>${assister.certificationStatus}</strong></td>
							</tr>
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.brkRenewalDate"/></th>
								<td><strong><fmt:formatDate value= "${assister.reCertificationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.brkCertificationNumber"/></th>
								<td><strong>${assister.certificationNumber}</strong></td>
							</tr>
																		
						</tbody>
					</table>
				</div><!-- end of .gutter10 -->
				<div class="gutter10">	
                	<p><spring:message code="label.assister.viewCertificationHistory"/></p>
                    <h4><spring:message code="label.assister.certificationHistory"/></h4>			
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
				           <display:column property="updatedOn" title="Date"  format="{0,date,MMM dd, yyyy}" sortable="false"  />
				           <display:column property="previousRegistrationStatus" title="Previous status" sortable="false" />
				           <display:column property="newRegistrationStatus" title="New Status" sortable="false" />
						   <display:column property="comments" title="View Comment" sortable="false" />
							<display:column title="View Attachment" > 
				                 <c:choose>
				                      <c:when test="${enrollmentRegisterStatusHistory.documentId!=null}">	
			                                <a href="#" onClick="showdetail('${enrollmentRegisterStatusHistory.documentId}');" id="edit_${enrollmentRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment" /></a> 
			                          </c:when>
			                          <c:otherwise>	
			                                <spring:message code="label.assister.noAttachment"/>
			                          </c:otherwise>
			                     </c:choose>
						   </display:column>
					</display:table>					
				</div><!-- end of .gutter10 -->
				<!-- end of form-actions -->
<%-- 	<% if(diffDays<=30) { %> --%>
				<div class="form-actions">
					<input type="submit" name="submit" id="submit" value='<spring:message code="label.assister.getRenewed" />' class="btn btn-primary" /> 
				</div><!-- end of form-actions -->
	<%-- <%} %> --%>
			</form>
		</div><!-- end of span9 -->
		</div>
	</div><!--  end of row-fluid -->

<script>
 function showdetail(documentId) {
	 

		 var  contextPath =  "<%=request.getContextPath()%>";
		 var documentUrl = contextPath + "/entity/assisteradmin/viewAttachment?documentId="+documentId;
	       window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
	     
 }		
</script>
