<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="row-fluid">
	<div class="span3">
		<h2>Let&#39;s Start</h2>
	 </div>
	<div class="span9">
		<form class="form-stacked">
          <df:csrfToken/>
       		<fieldset>
	   			<div class="clearfix">
	   				<legend>Welcome John Smith</legend>
					<p>What would you like to do today?</p>
				</div>
				<div class="clearfix">
					<ol>
						<li>Verify your information</li>
						<li>See what coverage options your employer offers</li>
						<li>Verify that your plan is affordable</li>
						<li>Enroll in your chosen plan</li>
					</ol>
					<a class="btn large primary" href="/shop/employee/verifyinfo">Next</a>
				</div>
			</fieldset>
		</form>		
	</div> 
</div>
<div class="notes" style="display:none">
	<div class="row">
		<div class="span">
	      <p>This is a welcome page which will describe the key steps of the enrollment process.  More information will also be available on the Frequently Asked Questions (FQA) page. Users will also be able to call a toll-free number or initiate an interactive chat to ask any questions they may have.
	      </p>
		</div>
    </div>
</div>



