package com.getinsured.hix.indportal.dto.dm.application;

public class UnemploymentIncome {
	
	private String expirationDate;
	private String incomeDescription;
	
	// Getter Methods
	
	public String getExpirationDate() {
		return expirationDate;
	}
	
	public String getIncomeDescription() {
		return incomeDescription;
	}
	
	// Setter Methods
	
	public void setIncomeDescription(String incomeDescription) {
		this.incomeDescription = incomeDescription;
	}
	
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	

}
