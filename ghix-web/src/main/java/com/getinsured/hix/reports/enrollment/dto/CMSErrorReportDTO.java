package com.getinsured.hix.reports.enrollment.dto;

import java.util.Date;

public class CMSErrorReportDTO {

	public CMSErrorReportDTO() {
		super();
	}

	private static final long serialVersionUID = 1L;
	private Integer	reportMonth;
	private Integer reportYear;
	private String fileName;
	private String hoisIssuerId;
	private String fileStatus;
	private Integer exchangeAssignedPolicyID;
	private Integer subscriberID;
	private String cmsErrorCode;
	private String cmsErrorDetail;
	private String fieldNameAssociatedwithError;
	private String additionalErrorInfo;
	private Date creationTime;
	private String nextAction;
	
	public Integer getReportMonth() {
		return reportMonth;
	}
	public void setReportMonth(Integer month) {
		this.reportMonth = month;
	}
	public Integer getReportYear() {
		return reportYear;
	}
	public void setReportYear(Integer year) {
		this.reportYear = year;
	}
	public String getHoisIssuerId() {
		return hoisIssuerId;
	}
	public void setHoisIssuerId(String hoisIssuerId) {
		this.hoisIssuerId = hoisIssuerId;
	}
	public String getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	public Integer getExchangeAssignedPolicyID() {
		return exchangeAssignedPolicyID;
	}
	public void setExchangeAssignedPolicyID(Integer exchAssignPolicyId) {
		this.exchangeAssignedPolicyID = exchAssignPolicyId;
	}
	public Integer getsubscriberID() {
		return subscriberID;
	}
	public void setsubscriberID(Integer issuerAssignSubsriberId) {
		this.subscriberID = issuerAssignSubsriberId;
	}
	public String getCmsErrorCode() {
		return cmsErrorCode;
	}
	public void setCmsErrorCode(String errorCode) {
		this.cmsErrorCode = errorCode;
	}
	public String getCmsErrorDetail() {
		return cmsErrorDetail;
	}
	public void setCmsErrorDetail(String errorDesc) {
		this.cmsErrorDetail = errorDesc;
	}
	public String getFieldNameAssociatedwithError() {
		return fieldNameAssociatedwithError;
	}
	public void setFieldNameAssociatedwithError(String elementInError) {
		this.fieldNameAssociatedwithError = elementInError;
	}
	public String getAdditionalErrorInfo() {
		return additionalErrorInfo;
	}
	public void setAdditionalErrorInfo(String additionalErrorInfo) {
		this.additionalErrorInfo = additionalErrorInfo;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date createdOn) {
		this.creationTime = createdOn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @return the nextAction
	 */
	public String getNextAction() {
		return nextAction;
	}
	/**
	 * @param nextAction the nextAction to set
	 */
	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}
	public CMSErrorReportDTO(Integer month, Integer year, String fileName, String nextAction, String hoisIssuerId, String fileStatus,
			Integer exchAssignPolicyId, Integer issuerAssignSubsriberId, String errorCode, String errorDesc,
			String elementInError, String additionalErrorInfo, Date createdOn) {
		super();
		this.reportMonth = month;
		this.reportYear = year;
		this.fileName = fileName;
		this.hoisIssuerId = hoisIssuerId;
		this.fileStatus = fileStatus;
		this.exchangeAssignedPolicyID = exchAssignPolicyId;
		this.subscriberID = issuerAssignSubsriberId;
		this.cmsErrorCode = errorCode;
		this.cmsErrorDetail = errorDesc;
		this.fieldNameAssociatedwithError = elementInError;
		this.additionalErrorInfo = additionalErrorInfo;
		this.creationTime = createdOn;
		this.nextAction = nextAction;
	}
}
