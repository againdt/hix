import { messagesActionTypes } from '../constants/actionTypes';

const initialState = null;

export default (state = initialState, action) => {
    switch(action.type) {
    case messagesActionTypes.SET_MESSAGES:
        return {
            ...state,
            [action.language]: action.messages.default
        };
    case messagesActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};