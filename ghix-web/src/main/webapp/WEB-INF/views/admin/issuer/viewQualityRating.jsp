<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
                 <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                 <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.issuerTitle"/></a></li>
        		<li><spring:message  code="pgheader.qualityrating"/></li>
            </ul><!--page-breadcrumb ends-->
            <h1 class="pull-left"><spring:message  code="pgheader.qualityrating"/> <small><spring:message  code="pgheader.headingText"/></small></h1>
		</div>
	
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.fileHistory"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li>
                    	<spring:message  code="label.QualityRatingInfo"/>
                    </li>
                </ul>
                <!-- end of side bar -->
                <br />
                <a href="<c:url value="/admin/qualityrating/upload"/>" class="btn btn-primary span11" id="uploadNewFile" name="uploadNewFile"><spring:message  code='pgheader.uploadNewFile'/></a>
			</div>
			<!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
				<form class="form-horizontal" id="frmIssuerQuality" name="frmIssuerQuality" action="#" method="POST">
              
					<display:table name="documentList" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
						<display:column property="uploadedFileName" titleKey="label.fileName" sortable="false" />
			           	<display:column property="uploadedDate" titleKey="label.dateUpdated" sortable="false" />
			           	<display:column property="uploadedBy" titleKey="label.userName" sortable="false"  />
			           	<display:column property="uploadCommentText" titleKey="label.comment" sortable="false" />
			           
			           <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
                       <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}">Next &gt;</a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
								<li><a href="{3}">Next &gt;</a></li>
							</ul>
						</div>
						'/>
			</display:table>
				</form>
				
            </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	</div>
<script type="text/javascript">
function showComment(comment){
	var commentStr=comment.replace(/_/g," ");
	$('<div id="alert" class="modalalert"><div class="modal-body"><iframe id="alert" src="#" style="overflow-x:hidden;width:0%;border:0;margin:0;padding:0;height:25px;"></iframe>'+commentStr+'<br/><br/><button class="btn offset2" data-dismiss="modal" aria-hidden="true">OK</button><br/></div></div>').modal({backdrop:false});
}
</script>
<style type="text/css">
.modalalert {position: fixed;top: 10%;left: 50%;z-index: 1050;width: 420px;margin-left: -280px;background-color: #ffffff;border: 1px solid #999;border: 1px solid rgba(0, 0, 0, 0.3);-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-moz-background-clip: padding;-webkit-background-clip: padding-box;background-clip: padding-box;-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-moz-background-clip: padding;-webkit-background-clip: padding-box;background-clip: padding-box;-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-webkit-background-clip: padding-box;-moz-background-clip: padding-box;background-clip: padding-box;outline: none;}
</style>
