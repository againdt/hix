package com.getinsured.eligibility;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.at.ref.exception.ReferralException;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.service.ReferralAccountActivationService;
import com.getinsured.eligibility.at.ref.service.ReferralActivationDocumentService;
import com.getinsured.eligibility.at.ref.service.ReferralBatchDisenrollService;
import com.getinsured.eligibility.at.ref.service.ReferralDemographicService;
import com.getinsured.eligibility.at.ref.service.ReferralLCECompareService;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.at.ref.service.ReferralTriggerAdminupdateService;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.hix.dto.consumer.ssap.AptcHistoryRequest;
import com.getinsured.hix.dto.eligibility.PersonTypeRequest;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.iex.referral.ReferralLinkingOverride;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.referraltask.ReferralEligibilityTask;
import com.google.gson.Gson;

/**
 * @author chopra_s
 * 
 */
@Controller
@RequestMapping("/referral")
public class ReferralController {

	private static final Logger LOGGER = Logger.getLogger(ReferralController.class);

	@Autowired
	@Qualifier("referralProcessingService")
	private ReferralProcessingService referralProcessingService;

	@Autowired
	@Qualifier("referralAccountActivationService")
	private ReferralAccountActivationService referralAccountActivationService;

	@Autowired
	@Qualifier("referralActivationDocumentService")
	private ReferralActivationDocumentService referralActivationDocumentService;

	@Autowired
	@Qualifier("referralEligibilityTask")
	private ReferralEligibilityTask referralEligibilityTask;

	@Autowired
	@Qualifier("referralDemographicService")
	private ReferralDemographicService referralDemographicService;

	@Autowired
	@Qualifier("referralTriggerAdminupdateService")
	private ReferralTriggerAdminupdateService referralTriggerAdminupdateService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;

	@Autowired
	@Qualifier("referralLCECompareService")
	private ReferralLCECompareService referralLCECompareService;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	
	@Autowired
	@Qualifier("referralBatchDisenrollService")
	private ReferralBatchDisenrollService referralBatchDisenrollService;
	
	@Autowired private Gson platformGson;

	@RequestMapping(value = "/activation/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ReferralActivation> referralActivation(@PathVariable("id") long id) {
		LOGGER.debug("Fetch Data for Referral Activation - " + id);
		final ReferralActivation result = referralProcessingService.referralActivationById(id);
		return new ResponseEntity<ReferralActivation>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/ssapapplication/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ReferralActivation> referralActivationBySsap(@PathVariable("id") int id) {
		LOGGER.debug("Fetch Data for Referral Activation By ssap - " + id);
		final ReferralActivation result = referralProcessingService.referralActivationBySsapApplication(id);
		return new ResponseEntity<ReferralActivation>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/find/{user}/{ssapapplication}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ReferralActivation> referralActivationByUser(@PathVariable("user") int user, @PathVariable("ssapapplication") int ssapApplication) {
		LOGGER.debug("Fetch Data for Referral Activation  By user & Ssap - " + user + " : " + ssapApplication);
		final ReferralActivation result = referralProcessingService.referralActivationByUserAndSsapApplication(user, ssapApplication);
		return new ResponseEntity<ReferralActivation>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/findByCmr/{cmrHousehold}/{ssapapplication}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ReferralActivation> referralActivationByCmrHousehold(@PathVariable("cmrHousehold") int cmrHousehold, @PathVariable("ssapapplication") int ssapApplication) {
		LOGGER.debug("Fetch Data for Referral Activation  By cmrHousehold & Ssap - " + cmrHousehold + " : " + ssapApplication);
		final ReferralActivation result = referralProcessingService.referralActivationByCmrAndSsapApplication(cmrHousehold, ssapApplication);
		return new ResponseEntity<ReferralActivation>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/linkcmr", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Long> linkReferralActivationUser(@RequestBody Integer[] arguments) {
		LOGGER.debug("linkReferralActivationUser - " + arguments);
		final Long result = referralProcessingService.linkUserCmrActivation(arguments);
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/validate", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> validateReferralAnswers(@RequestBody ReferralVerification referralVerification) {
		LOGGER.debug("referralVerification - " + referralVerification);

		final Integer result = referralProcessingService.validateReferralAnswers(referralVerification);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/updateStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> updateStatus(@RequestBody Integer[] arguments) {
		LOGGER.debug("updateStatus - " + arguments);
		final Integer result = referralProcessingService.updateActivationStatus(arguments);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/updateDocumentStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String updateReferralAtDocument(@RequestBody String jsonInput) {
		String jsonResponse = null;
		LOGGER.debug("Inside updateReferralAtDocument");
		try {
			jsonResponse = referralActivationDocumentService.updateDocumentStatus(jsonInput);
		} catch (ReferralException referralException) {
			LOGGER.error("Error in updating the referral activation document status");
			jsonResponse = ReferralUtil.handleExceptionForJson(referralException);
		} catch (Exception exception) {
			LOGGER.error("Unknown error in updating referral activation document status", exception);
			jsonResponse = ReferralUtil.handleExceptionForJson(exception);
		}
		LOGGER.debug("Done updateReferralAtDocument");
		return jsonResponse;
	}

	@RequestMapping(value = "/activation/coverageyear", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> persistCoverageYear(@RequestBody Integer[] arguments) {
		LOGGER.debug("persistCoverageYear - " + arguments);
		final Integer result = referralProcessingService.persistCoverageYear(arguments);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/processridp", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, String>> processRidp(@RequestBody Integer[] arguments) {
		LOGGER.debug("processRidp - " + arguments[0]);
		Map<String, String> result = referralProcessingService.processRidp(arguments);
		return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);
	}

	@RequestMapping(value = { "/pending", "/pending/" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<PendingReferralDTO>> pendingReferralsForCmr(@RequestBody int cmrhousehold) {
		LOGGER.debug("initialReferralsForCmr - " + cmrhousehold);
		final List<PendingReferralDTO> result = referralProcessingService.pendingReferralsForCmr(cmrhousehold);
		return new ResponseEntity<List<PendingReferralDTO>>(result, HttpStatus.OK);
	}

	@RequestMapping(value = { "/csr/linkingstatus/{ssapplicationid}/{cmrhouseholdid}", "/enrolledapps/{ssapplicationid}/{cmrhouseholdid}" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, String>> getLinkingStausbyCmrIDSsapId(@PathVariable("ssapplicationid") long ssapplicationid, @PathVariable("cmrhouseholdid") BigDecimal cmrhouseholdid) {
		LOGGER.debug("Inside getLinkingStausbyCmrIDSsapId");
		Map<String, String> statusMap = referralProcessingService.linkingStatusForCSR(ssapplicationid, cmrhouseholdid);
		return new ResponseEntity<Map<String, String>>(statusMap, HttpStatus.OK);

	}

	/**
	 * created by pravin.
	 * 
	 * @param argument
	 * @return
	 */
	@RequestMapping(value = "/activation/noticeToUploadDoc", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> sendNotificationToUploadDoc(@RequestBody long argument) {
		LOGGER.debug("referralId - " + argument);
		final Integer result = referralActivationDocumentService.sendNotificationToUploadDoc(argument);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);

	}

	/**
	 * Testing Utility Method
	 * */
	@RequestMapping(value = "/activation/runeligibility/{applicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> test(@PathVariable("applicationId") Long applicationId) {

		referralEligibilityTask.runEligibility(applicationId);

		return new ResponseEntity<Integer>(1, HttpStatus.OK);
	}

	@RequestMapping(value = "/getenrollment/{applicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<EnrolleeResponse> getEnrollment(@PathVariable("applicationId") long applicationId) {
		EnrolleeResponse enrollmentResponse = null;
		try {
			enrollmentResponse = ssapEnrolleeHandler.invokeEnrollmentApi(applicationId);
		} catch (Exception e) {
			LOGGER.error("Unknown error while getenrollment", e);
		}
		return new ResponseEntity<EnrolleeResponse>(enrollmentResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/activation/activeappcount/{cmrHouseHoldId}/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Long> getActiveApplicationCountForCMR(@PathVariable("cmrHouseHoldId") int cmrhouseholdId, @PathVariable("ssapApplicationId") int ssapApplicationId) {
		long count = 0;
		try {
			count = referralProcessingService.getActiveApplicationCountForCMR(cmrhouseholdId, ssapApplicationId);
		} catch (Exception e) {
			LOGGER.error("Unknown error while getActiveApplicationCountForCMR", e);
		}
		return new ResponseEntity<Long>(count, HttpStatus.OK);
	}

	/**
	 * Demographic Step - 2 of LCE Referral
	 * */
	@RequestMapping(value = "/rundemographic/{enrolledApplicationId}/{currentApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> runDemographic(@PathVariable("enrolledApplicationId") long enrolledApplicationId, @PathVariable("currentApplicationId") long currentApplicationId) {
		try {
			referralDemographicService.executeCompare(enrolledApplicationId, currentApplicationId);
		} catch (Exception e) {
			LOGGER.error("Unknown error while rundemographic", e);
		}
		return new ResponseEntity<Integer>(1, HttpStatus.OK);
	}

	/**
	 * Trigger Admin Step - 3 of LCE Referral
	 * */
	@RequestMapping(value = "/runadminupdate/{enrolledApplicationId}/{currentApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> runAdminUpdate(@PathVariable("enrolledApplicationId") long enrolledApplicationId, @PathVariable("currentApplicationId") long currentApplicationId) {
		try {
			referralTriggerAdminupdateService.triggerAdminupdate(enrolledApplicationId, currentApplicationId, true, new com.getinsured.iex.ssap.Address(),null);
		} catch (Exception e) {
			LOGGER.error("Unknown error while runadminupdate", e);
		}
		return new ResponseEntity<Integer>(1, HttpStatus.OK);
	}

	/**
	 * SSAP Cmr Link Step - 4 of LCE Referral
	 * */
	@RequestMapping(value = "/runcmrssaplink/{enrolledApplicationId}/{currentApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> runCmrSsapLinking(@PathVariable("enrolledApplicationId") long enrolledApplicationId, @PathVariable("currentApplicationId") long currentApplicationId) {
		try {
			referralSsapCmrLinkService.executeLinking(enrolledApplicationId, currentApplicationId, null,null);
		} catch (Exception e) {
			LOGGER.error("Unknown error while runcmrssaplink", e);
		}
		return new ResponseEntity<Integer>(1, HttpStatus.OK);
	}

	/**
	 * Lce Compare Step - 7 of LCE Referral
	 * */
	@RequestMapping(value = "/runlcecompare/{enrolledApplicationId}/{currentApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> runLceCompare(@PathVariable("enrolledApplicationId") long enrolledApplicationId, @PathVariable("currentApplicationId") long currentApplicationId) {
		try {
			referralLCECompareService.executeCompare(currentApplicationId, enrolledApplicationId);
		} catch (Exception e) {
			LOGGER.error("Unknown error while runLceCompare", e);
		}
		return new ResponseEntity<Integer>(1, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getssapapplicant/{accessCode}/{isCsr}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<Integer, String>> getSsapApplicant(@PathVariable("accessCode") String accessCode, @PathVariable("isCsr") String isCsr) {
		ResponseEntity<Map<Integer, String>> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<Map<Integer, String>>(referralProcessingService.getSsapQuestions(accessCode, Boolean.valueOf(isCsr)), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING SSAP APPLICANT: ", ex);
		}
		
		return responseEntity;
	}
	
	
	@RequestMapping(value = "/getssapapplicant/home/{accessCode}/{isCsr}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<Integer, String>> getSsapQuestionsFromHome(@PathVariable("accessCode") String accessCode, @PathVariable("isCsr") String isCsr) {
		ResponseEntity<Map<Integer, String>> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<Map<Integer, String>>(referralProcessingService.getSsapQuestionsFromHome(accessCode, Boolean.valueOf(isCsr)), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING SSAP APPLICANT: ", ex);
		}
		
		return responseEntity;
	}
	
	
	@RequestMapping(value = "/getssapapplicantguid/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getSsapApplicantGuid(@PathVariable("ssapApplicationId") String ssapApplicationId) {
		ResponseEntity<String> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<String>(referralProcessingService.getApplicantGuid(ssapApplicationId), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING SSAP APPLICANT: ", ex);
		}
		
		return responseEntity;
	}
	
	@RequestMapping(value = "/activation/updateReferralStatus", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> updateReferralStatus(@RequestBody String[] arguments) {
		LOGGER.debug("updateStatus - " + arguments);
		final Integer result = referralProcessingService.updateReferralActivation(arguments);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create/household/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> createNewHousehold(@PathVariable("ssapApplicationId") long ssapApplicationId) {
		ResponseEntity<Integer> responseEntity = null;
		
		try {
			// null param is householdCaseId in case of CA AT.
			responseEntity = new ResponseEntity<Integer>(referralProcessingService.checkHouseholdExistsAndThenCreate(ssapApplicationId,null), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE CREATING HOUSEHOLD for: " + ssapApplicationId + " - " + ex);
		}
		
		return responseEntity;
	}
	
	@RequestMapping(value = "/find/household/fromSsnDoB/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> findHouseholdFromSsnDoB(@PathVariable("ssapApplicationId") long ssapApplicationId) {
		ResponseEntity<Integer> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<Integer>(referralProcessingService.findHouseholdFromSsnDoB(ssapApplicationId), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FINDING HOUSEHOLD FROM SSN DoB for: " + ssapApplicationId + " - " + ex);
		}
		
		return responseEntity;
	}
	
	@RequestMapping(value = "/compare/householdPrimary/{ssapApplicationId}/{householdId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ReferralLinkingOverride> compareHouseholdAndPrimary(@PathVariable("ssapApplicationId") long ssapApplicationId, @PathVariable("householdId") int householdId) {
		ResponseEntity<ReferralLinkingOverride> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<ReferralLinkingOverride>(referralProcessingService.compareHouseholdAndPrimary(ssapApplicationId, householdId), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE COMPARING HOUSEHOLD AND PRIMARY for: " + ssapApplicationId + " - " + ex);
		}
		
		return responseEntity;
	}
	
	@RequestMapping(value = "/activation/updateReferralCount", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> updateReferralCount(@RequestBody String referralId) {
		final Integer result = referralProcessingService.updateReferralCount(referralId);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/activation/updateReferralSuccessCount", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> updateReferralSuccessCount(@RequestBody String referralId) {
		final Integer result = referralProcessingService.updateReferralSuccessCount(referralId);
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/activation/triggerReferralNotice/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> triggerNotification(@PathVariable("ssapApplicationId") long ssapApplicationId) {
		ResponseEntity<Boolean> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<Boolean>(referralAccountActivationService.triggerAccountActivation(ssapApplicationId),HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE TRIGGERING NOTIFICATION: ",ex);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/batch/automateCsLevel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> batchAutomateCsLevel(@RequestBody String[] arguments) {
		String response = referralBatchDisenrollService.csrLevelChange(arguments);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/batch/automateGeolocale", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> batchAutomateGeolocale(@RequestBody String[] arguments) {
		String response = referralBatchDisenrollService.geoLocaleChange(arguments);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAptcHistory", method = RequestMethod.POST)
	@ResponseBody
	public String getAptcHistory(@RequestBody String aptcHistory, HttpServletResponse response) {
		Map<String,BigDecimal> aptcResponse = null;
		AptcHistoryRequest aptcHistoryRequest = null;
		String responseStr = null;
		try{
			aptcHistoryRequest = platformGson.fromJson(aptcHistory, AptcHistoryRequest.class);
		}catch(Exception e){
			response.setStatus(400);
			return "Invalid Request";
		}
		if(aptcHistoryRequest != null) {
			
			aptcResponse = referralProcessingService.getMonthWiseAPTC(aptcHistoryRequest);
			if(aptcResponse!=null) {
				responseStr = platformGson.toJson(aptcResponse);
			}else {
				response.setStatus(400);
				return "No APTC history found";
			}
		}
		return responseStr;
	}
	
	@RequestMapping(value = "/getPersonType", method = RequestMethod.POST)
	@ResponseBody
	public String getPersonType(@RequestBody String personTypeRequestJson, HttpServletResponse response) {
		Map<String,SsapApplicantPersonType> personTypeMap = null;
		PersonTypeRequest personTypeRequest = null;
		String responseStr = null;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			personTypeRequest = mapper.readValue(personTypeRequestJson, PersonTypeRequest.class);
		}
		catch(Exception e){
			LOGGER.error(personTypeRequestJson);
			response.setStatus(400);
			return "Invalid Request";
		}
		if(personTypeRequest != null) 
		{
			try 
			{
				personTypeMap = referralProcessingService.determineApplicantPersonType(personTypeRequest);
				responseStr = mapper.writeValueAsString(personTypeMap);
			} catch (Exception e) {
				LOGGER.error(personTypeRequestJson);
				response.setStatus(400);
				return e.getMessage();
			}
		}
		return responseStr;
	}
}