<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<link rel="stylesheet" type="text/css"
	href="<gi:cdnurl value="/resources/css/tablesorter.css" />" />

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<gi:cdnurl value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<gi:cdnurl value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<gi:cdnurl value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<gi:cdnurl value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<style>
#rightpanel tbody tr td:first-child{padding-left: 11px;}
.page-breadcrumb {margin-top: 0 !important;}
.page-breadcrumb li i.icon-circle-arrow-left {top: 0px;}
</style>

<c:url value="/entity/enrollmententity/getindividualstoassign" var="theUrl">  
	 	<c:param name="${df:csrfTokenParameter()}">
	      <df:csrfToken plainToken="true"/>     
	    </c:param>
</c:url>

<div class="gutter10">
	<!--start page-breadcrumb -->
	<div class="l-page-breadcrumb">
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<c:if test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">				
				<li><a href="<c:url value="/entity/enrollmententity/individuals?desigStatus=Pending"/>"><spring:message
							code="label.individuals" /></a></li>
				</c:if>
				<c:if test="${(desigStatus == 'InActive')}">
				<li><a href="<c:url value="/entity/enrollmententity/individuals?desigStatus=InActive"/>"><spring:message
							code="label.individuals" /></a></li>
				</c:if>	
				<c:if test="${(desigStatus == 'Active')}">
				<li><a href="<c:url value="/entity/enrollmententity/individuals?desigStatus=Active"/>"><spring:message
							code="label.individuals" /></a></li>
				</c:if>				
				<li>${desigStatus}</li>
			</ul>
		</div>
	</div>
	<!--page-breadcrumb ends-->

	
	<div class="row-fluid">
		<c:if test="${message != null}">
			<div class="errorblock alert alert-info">
				<p>${message}</p>
			</div>
		</c:if>
	</div>
	
	<div class="row-fluid">
		<c:if test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">		
			<h1><a name="skip"></a><spring:message code="label.pendingrequest"/><c:if test="${resultSize > 1}"><spring:message code="label.s"/></c:if>
				<small>
					${resultSize} <spring:message code="label.pendingrequest"/><c:if test="${resultSize > 1}"><spring:message code="label.s"/></c:if>${tableTitle}
				</small>
			</h1>
		</c:if>	
		<c:if test="${(desigStatus == 'InActive')}">
			<h1><a name="skip"></a>
				<spring:message code="label.assister.inActive"/> <small>${resultSize} <spring:message code="label.inactiveindividuals"/>
						${tableTitle}</small>
			</h1>
		</c:if>	
		<c:if test="${(desigStatus == 'Active')}">
			<h1><a name="skip"></a>
				<spring:message code="label.activeindividuals"/> <small>${resultSize} <spring:message code="label.active-individuals"/>
						${tableTitle}</small>
			</h1>
			<c:if test="${CA_STATE_CODE}">
				<p><spring:message code = "label.activePageInstructions"/></p>
			</c:if>
		</c:if>	
	</div>
	
	<div class="txt-right">
					<div id="dropdown" class="dropdown">
						<span class="gray" id="counter"><small><c:if test="${(desigStatus == 'Active')}">( 0 Items Selected )</c:if>&nbsp;</small></span> <span id="bulkActions" style="display: none;">
							<a class="dropdown-toggle btn btn-primary btn-small" id="inactiveLabel" role="button" data-toggle="dropdown" data-target="#" href="#"><spring:message code="label.assister.bulkAction"/> <i class="icon-cog icon-white"></i><i class="caret icon-white"></i></a>
							<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="inactiveLabel">
								<li><a id="reassignlink" href="#"><spring:message code="label.assister.reassign"/></a></li>																	
							</ul>
						</span>
					</div>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><spring:message code="label.entity.refResuBy"/> <a class="pull-right" href="#" onclick="resetAll()"><spring:message code="label.entity.resetAll"/></a></h4>
			</div>
			<div class="graybg">
				<form class="form-vertical gutter10" id="individualsearch" name="individualsearch" action="<c:url value="/entity/enrollmententity/individuals"/>" method="POST">
					<df:csrfToken/>
						<input type="hidden" name="desigStatus" id="desigStatus" value="${desigStatus}" /> 
						<input type="hidden" name="fromModule" id="fromModule" value="<encryptor:enc value="individuals"/>" />

					<input type="hidden" name="totalCountAtClient" value="${resultSize}" />
						 
					<input type="hidden" id="sortBy" name="sortBy" >
					<input type="hidden" id="sortOrder" name="sortOrder" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					<input type="hidden" id="previousSortBy" name="previousSortBy" value="${previousSortBy}">
					<input type="hidden" id="previousSortOrder" name="previousSortOrder" value="${previousSortOrder}">

					<div class="control-group">
						<label class="control-label" for="indFirstOrLastName"><spring:message code="label.IndividualName"/></label>
						<div class="controls">
							<input class="span" type="text" id="indFirstOrLastName" name="indFirstOrLastName"
								value="${searchCriteria.indFirstOrLastName}" placeholder="" />
						</div>
					</div>
					<!--<div class="control-group">
						<label class="control-label" for="individualFirstName">INDIVIDUAL LAST NAME</label>
						<div class="controls">
							<input class="span" type="text" id="individualLastName" name="individualLastName"
								value="${searchCriteria.individualLastName}" placeholder="" />
						</div>
					</div>-->
					<div class="control-group">
						<label class="control-label" for="cecFirstOrLastName"><spring:message code="label.EnrollmentCounselorName"/></label>
						<div class="controls">
							<input class="span" type="text" id="cecFirstOrLastName" name="cecFirstOrLastName"
								value="${searchCriteria.cecFirstOrLastName}" placeholder="" />
						</div>
					</div>
					
					<!--<div class="control-group">
						<label class="control-label" for="enrollmentCounselorLastName">ENROLLMENT COUNSELOR'S LAST NAME</label>
						<div class="controls">
							<input class="span" type="text" id="enrollmentCounselorLastName" name="enrollmentCounselorLastName"
								value="${searchCriteria.enrollmentCounselorLastName}" placeholder="" />
						</div>
					</div>-->
					<c:if
						test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
					<fieldset>
					<legend><spring:message code="label.ReceivedOn"/></legend>
						<div class="control-group box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="requestSentFrom"><spring:message code="label.assister.from"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="requestSentFrom" name="requestSentFrom" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.requestSentFrom}" />
									<div class="help-inline" id="requestSentFrom_error"></div>
								</div>
								<!-- end of controls-->
							</div>

							<div class="control-group">
								<label class="required control-label" for="requestSentTo"><spring:message code="label.assister.to"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="requestSentTo" name="requestSentTo" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.requestSentTo}" />
									<div class="help-inline" id="requestSentTo_error"></div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
						</fieldset>
					</c:if>

					<c:if test="${(desigStatus == 'InActive')}">
					<fieldset>
					<legend><spring:message code="label.assister.indInactiveSince"/></legend>
						<div class="control-group box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="inactiveDateFrom"><spring:message code="label.assister.from"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="inactiveDateFrom" name="inactiveDateFrom" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateFrom}" />
									<div class="help-inline" id="inactiveDateFrom_error"></div>
								</div>
								<!-- end of controls-->
							</div>

							<div class="control-group">
								<label class="required control-label" for="inactiveDateTo"><spring:message code="label.assister.to"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="inactiveDateTo" name="inactiveDateTo" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateTo}" />
									<div class="help-inline" id="inactiveDateTo_error"></div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
						</fieldset>
					</c:if>

					<c:if test="${(desigStatus == 'Active')}">					
					<fieldset>
						<c:choose>
					   		<c:when test="${CA_STATE_CODE}">
								<legend><spring:message code="label.enrollmentstatus"/></legend>
								<div class="control-group box-tight margin0">
									<select size="1" id="enrollmentStatus" name="enrollmentStatus" class="span12">
										<option value="">Select Enrollment Status</option>
										<c:forEach var="enrollmentStatus" items="${enrollmentStatuslist}">
											<option <c:if test="${enrollmentStatus == searchCriteria.enrollmentStatus}"> SELECTED </c:if> value="${enrollmentStatus}">${enrollmentStatus}</option> 
										</c:forEach>
									</select>
								</div>
							</c:when>
							<c:otherwise>
								<legend><spring:message code="label.agent.employers.EligibilityStatus"/></legend>
								<div class="control-group box-tight margin0">
									<select size="1" id="eligibilityStatus" name="eligibilityStatus" class="span12">
										<option value=""><spring:message code='label.search.dropdown.option.select'/></option>
										<c:forEach var="eligibilityStatus" items="${eligibilityStatuslist}">
										   <c:choose>
										   		<c:when test="${CA_STATE_CODE}">
														<option <c:if test="${eligibilityStatus == searchCriteria.eligibilityStatus}"> SELECTED </c:if> value="${eligibilityStatus}">${eligibilityStatus}</option> 
												</c:when>
												<c:otherwise>
													<option <c:if test="${eligibilityStatus == searchCriteria.eligibilityStatus}"> SELECTED </c:if> value="${eligibilityStatus}">${eligibilityStatus.description2}</option>
												</c:otherwise>
										   </c:choose>	
																																								
										</c:forEach>
									</select>
								</div>
							</c:otherwise>
					    </c:choose>	
					</fieldset>
					
					<fieldset>
					 	<c:choose>
					   		<c:when test="${CA_STATE_CODE}">
								<legend><spring:message code="label.assister.currentStatus"/></legend>
								<div class="control-group box-tight margin0">
									<select size="1" id="currentStatus" name="currentStatus" class="span12">
										<option value="">Select Current Status</option>
										<c:forEach var="currentStatus" items="${currentStatusList}">
											<option <c:if test="${currentStatus == searchCriteria.currentStatus}"> SELECTED </c:if> value="${currentStatus}">${currentStatus}</option> 
										</c:forEach>
									</select>
								</div>
							</c:when>
							<c:otherwise>
								<legend>Application Status</legend>
								<div class="control-group box-tight margin0">
									<select size="1" id="applicationStatus" name="applicationStatus" class="span12">
										<option value="">Select</option>
										<c:forEach var="applicationStatus" items="${applicationStatusList}">
											 <c:choose>
										   		<c:when test="${CA_STATE_CODE}">
													<option <c:if test="${applicationStatus == searchCriteria.applicationStatus}"> SELECTED </c:if> value="${applicationStatus}">${applicationStatus}</option> 
												</c:when>
												<c:otherwise>
													<option <c:if test="${applicationStatus == searchCriteria.applicationStatus}"> SELECTED </c:if> value="${applicationStatus}">${applicationStatus.description}</option>
												</c:otherwise>
										   </c:choose>
										</c:forEach>
									</select>
								</div>
							</c:otherwise>
					    </c:choose>
						
					</fieldset>
					<fieldset>
						<legend><spring:message code="label.activesince"/></legend>
						<div class="control-group box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="activeDateFrom"><spring:message code="label.assister.from"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="activeDateFrom" name="activeDateFrom" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateFrom}" />
									<div class="help-inline" id="activeDateFrom_error"></div>
								</div>
								<!-- end of controls-->
							</div>

							<div class="control-group">
								<label class="required control-label" for="activeDateTo"><spring:message code="label.assister.to"/>:</label>
								<div class="input-append date date-picker" data-date="">
									<input type="text" id="activeDateTo" name="activeDateTo" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateTo}" />
									<div class="help-inline" id="activeDateTo_error"></div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
					</fieldset>
					</c:if>
					<div class="gutter10">
						<input type="submit" name="submit" id="submit"
							class="btn input-small offset3" value="<spring:message code="label.assister.indGo"/>" />
					</div>
				</form>
			</div>


		</div>
		<!-- end of span3 -->

		<div id="rightpanel" class="span9">

			<form class="form-vertical" id="assisterindividuals"
				name="assisterindividuals" action="<c:url value="/entity/enrollmententity/individuals" />" method="POST">
			<df:csrfToken/>
				<input type="hidden" id="pageNumber" name="pageNumber" value="">
				<input type="hidden" id="indIDList" name="indIDList" />

				<div id="assisterlist">

					<input type="hidden" name="id" id="id" value="">
					<c:set var="firstNameSorter" value="${sortOrder}" />
					<c:set var="inactiveSinceSort" value="${sortOrder}" />
					<c:set var="cecFirstNameSort" value="${sortOrder}" />
					<c:set var="eligibilityStatusSort" value="${sortOrder}" />
					<c:set var="applicationStatusSort" value="${sortOrder}" />
					<c:set var="enrollmentStatusSort" value="${sortOrder}" />
					<c:set var="currentStatusSort" value="${sortOrder}" />
					<c:choose>

						<c:when test="${fn:length(individualList) > 0 and desigStatus == 'Active'}">
								<table class="table" id="individualList">
									<thead>
										<tr class="header">
												
												<spring:message code="label.IndividualName" var="nameVal"/>
												<spring:message code="label.activesince" var="activeSinceVal"/>											
												<spring:message code="label.EnrollmentCounselorName" var="enrollmentCounselorVal"/>
												<spring:message code="label.assister.indELIGIBILITYSTATUS" var="eligibilityStatusVal"/>
												<spring:message code="label.ApplicationStatus" var="applicationStatusVal"/>
												<spring:message code="label.enrollmentstatus" var="enrollmentStatusVal"/>
												<spring:message code="label.assister.currentStatus" var="currentStatusVal"/>
												<spring:message code="label.brkactions" var="bulkActionVal"/>
												<!-- <th class="header" scope="col"><input type="checkbox" value="" id="check_all${desigStatus}" name="check_all${desigStatus}"></th> -->
												<th scope="col" class="header"><label for="check_allActive"><input id="check_allActive" class="checkall" type="checkbox" name="check_allActive" value=""></label></th>
												<th class="sortable" scope="col" style="width: 275px;"><dl:sort  title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${firstNameSorter}"></dl:sort></th>
												
												<c:choose>
													<c:when test="${CA_STATE_CODE}">
														<th class="sortable" scope="col" style="width: 180px;"><dl:sort title="${activeSinceVal}" sortBy="ACTIVE_SINCE" sortOrder="${inactiveSinceSort}"></dl:sort></th>
														<th class="sortable" scope="col" style="width: 260px;"><dl:sort title="${enrollmentCounselorVal}" sortBy="CEC_FIRST_NAME" sortOrder="${cecFirstNameSort}"></dl:sort></th>
												 		<th class="sortable" scope="col" style="width: 150px;"><dl:sort title="${enrollmentStatusVal}" sortBy="ENROLLMENT_STATUS" sortOrder="${enrollmentStatusSort}"></dl:sort></th>
														<th class="sortable" scope="col" style="width: 175px;"><spring:message code="label.assister.currentStatus"/></th>
												 	</c:when>
												 	<c:otherwise>
												 		<th class="sortable" scope="col" style="width: 180px;"><dl:sort title="${activeSinceVal}" sortBy="INACTIVESINCE" sortOrder="${inactiveSinceSort}"></dl:sort></th>
														<th class="sortable" scope="col" style="width: 260px;"><dl:sort title="${enrollmentCounselorVal}" sortBy="CEC_FIRST_NAME" sortOrder="${cecFirstNameSort}"></dl:sort></th>
												 		<th class="sortable" scope="col" style="width: 150px;"><dl:sort title="${eligibilityStatusVal}" sortBy="ELIGIBILITY_STATUS" sortOrder="${eligibilityStatusSort}"></dl:sort></th>
														<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${applicationStatusVal}" sortBy="APPLICATION_STATUS" sortOrder="${applicationStatusSort}"></dl:sort></th>
												 	</c:otherwise>
												</c:choose>			
												<th class="sortable" scope="col" style="width:55px"><spring:message code="label.brkAction"/></th>											
											</tr>
									</thead>
									<c:set var="counter" value="0" />
											<c:forEach items="${individualList}" var="entry">
											<tr>
												<td><label for="${counter}"><input id="${counter}" class="indCheckboxActive individual-checkbox" type="checkbox" name="indCheckboxActive" value="${entry.id}"></label></td>
												 <td>
												 	<c:choose>
												 		<c:when test="${CA_STATE_CODE}">
												 			<c:set var="entryIdForEncryption" ><encryptor:enc value="${entry.id}" isurl="true"/> </c:set>
												 			<a name="indlink"  style="display: inline-block;word-break:break-all;"  href="<c:url value="/individual/viewindividualdetail/${entryIdForEncryption}?phoneNumber=${entry.phoneNumber}&emailAddress=${entry.emailAddress }&firstName=${entry.firstName}&lastName=${entry.lastName}"/>" 
															 id="ind${entry.id}" class="inddetail">${entry.firstName}&nbsp;${entry.lastName}</a>
												  		</c:when>
         												<c:otherwise>
												 			<a href="/hix/entity/individualcase/<encryptor:enc value="${entry.id}" isurl="true"/>" style="display: inline-block;word-break:break-all;">${entry.firstName}&nbsp;${entry.lastName}</a>
												 		</c:otherwise>
												 	</c:choose>
												 </td>
												 <td class="txt-left">
												 	<c:choose>
												 		<c:when test="${CA_STATE_CODE}">
												 			<fmt:formatDate value="${entry.activeSince}" type="date" pattern="MM/dd/yyyy"/>
												  		</c:when>
         												<c:otherwise>
												 			<fmt:formatDate value="${entry.inActiveSince}" type="date" pattern="MM/dd/yyyy"/>
												 		</c:otherwise>
												 	</c:choose>
												 </td>
												 <c:set var="entryCECId" ><encryptor:enc value="${entry.cecId}" isurl="true"/> </c:set>												 
												 <c:set var="encEntryId"> <encryptor:enc value="${entry.id}" isurl="true"/> </c:set>												 
												 <td><a href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${entryCECId}"/>">${entry.cecFirstName}&nbsp;${entry.cecLastName}</a></td>
												<c:choose>
														<c:when test="${CA_STATE_CODE}">
													 		   <td class="txt-left">${entry.enrollmentStatus}</td>  
													 		   <td class="txt-left">${entry.currentStatus}</td>
													 	</c:when>
													 	<c:otherwise>
													 		<c:if test="${entry.eligibilityStatus == null || entry.eligibilityStatus == '' }">  <td class="txt-center">${entry.eligibilityStatus}</td> </c:if>
													 		<c:forEach var="eligibilityStatus" items="${eligibilityStatuslist}">
													 			<c:if test="${entry.eligibilityStatus == eligibilityStatus }">  <td class="txt-center">${eligibilityStatus.description}</td> </c:if>
													 		</c:forEach>
													 		<td class="txt-center">${entry.applicationStatus}</td>
													 	</c:otherwise>
												</c:choose>													
												 
												 <td class="txt-right">
													<div class="controls">
														<div class="dropdown">
															<a href="/page.html" data-target="#" data-toggle="dropdown"
																role="button" id="dLabel" class="dropdown-toggle"> <i
																class="icon-cog"></i><b class="caret"></b>
															</a>																														
															
															<ul aria-labelledby="dLabel" role="menu"
																class="dropdown-menu pull-right txt-left">
																<li><a href="<c:url value="/entity/individualcase/${encEntryId}"/>"><spring:message code="label.brkdetails"/></a></li>
																<li><a id="declinelink" class="validateDecline"	href="<c:url value="/entity/assister/declinePopup/${entryCECId}/${encEntryId}"/>?prevStatus=${desigStatus}&firstName=${entry.firstName}&lastName=${entry.lastName}"><spring:message code="label.brkMarkAsInactive"/></a></li>
																</li>
															</ul>
																	
																
														</div>
													</div>
										</td>
												 											 												 
											</tr>
											<c:set var="counter" value="${counter+1}" />
											</c:forEach>
										</table>			
							</c:when>
							<c:when test="${fn:length(individualList) > 0 and (desigStatus == 'Pending' || desigStatus == 'InActive')}">
								<table class="table" id="individualList">
									<thead>
										<tr class="header">		
											<spring:message code="label.IndividualName" var="nameVal"/>									
											<spring:message code="label.ReceivedOn" var="receivedOnVal"/>
											<spring:message code="label.entity.entityIndInActiveSince" var="inActiveSinceVal"/>										
											<spring:message code="label.EnrollmentCounselorName" var="enrollmentCounselorVal"/>
											<!--  <th class="header" scope="col"><input type="checkbox" value="" id="check_all${desigStatus}" name="check_all${desigStatus}"> </th> -->	
											<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${sortOrder}"></dl:sort></th>
											<c:choose>
												<c:when test="${desigStatus == 'Pending' || desigStatus == '' || desigStatus == null}">
													<th class="sortable" scope="col" style="width: 175px;"><dl:sort  customFunctionName="traverse" title="${receivedOnVal}" sortBy="REQUESTSENT" sortOrder="${inactiveSinceSort}"></dl:sort></th>	
												</c:when>
												<c:otherwise>
													<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${inActiveSinceVal}" sortBy="INACTIVESINCE" sortOrder="${inactiveSinceSort}"></dl:sort></th>
												</c:otherwise>
											</c:choose>	
											<th class="sortable" scope="col" style="width: 175px;"><dl:sort  customFunctionName="traverse" title="${enrollmentCounselorVal}" sortBy="CEC_FIRST_NAME" sortOrder="${cecFirstNameSort}"></dl:sort></th>
											 <c:choose>
												<c:when test="${desigStatus == 'Pending'}">
													<th class="sortable" scope="col" style="width: 60px;"><spring:message code="label.assister.indACT"/></th>	
												</c:when>
											</c:choose>												
										</tr>
									</thead>
											<c:forEach items="${individualList}" var="entry">
												<tr>
													 <c:set var="entryIdForEncryption" ><encryptor:enc value="${entry.id}" isurl="true"/> </c:set>												
													 <c:set var="cecIdForEncryption" ><encryptor:enc value="${entry.cecId}" isurl="true"/> </c:set>												
													 <td><a name="indlink" href="<c:url value="/individual/viewindividualdetail/${entryIdForEncryption}?phoneNumber=${entry.phoneNumber}&emailAddress=${entry.emailAddress }&firstName=${entry.firstName}&lastName=${entry.lastName}"/>" 
													 id="ind${entry.id}" class="inddetail">${entry.firstName}&nbsp;${entry.lastName}</a></td>										
													 <c:choose>
													 	<c:when test="${desigStatus == 'Pending' || desigStatus == '' || desigStatus == null}">
													 		<td><fmt:formatDate value="${entry.requestSent}" type="date" pattern="MM/dd/yyyy"/></td>		
													 	</c:when>
													 	<c:otherwise>
													 		<td><fmt:formatDate value="${entry.inActiveSince}" type="date" pattern="MM/dd/yyyy"/></td>	
													 	</c:otherwise>
													 </c:choose>
													 <td><a href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${cecIdForEncryption}"/>">${entry.cecFirstName}&nbsp;${entry.cecLastName}</a></td>
													  <c:choose>
														<c:when test="${desigStatus == 'Pending'}">
															 <td class="txt-right">
																	<div class="dropdown">
																		<a href="/page.html" data-target="#" data-toggle="dropdown"	role="menubar" id="dLabel" class="dropdown-toggle">
																			<i class="icon-gear"></i><b class="caret"></b>
																		</a>
																		
																		
																				<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right txt-left">
																					<li><a href="<c:url value="/entity/assister/approve/${cecIdForEncryption}/${entryIdForEncryption}?&prevStatus=${desigStatus}"/>"><spring:message code="label.assister.indAccept"/></a></li>
																					<li><a id="declinelink" class="validateDecline"	href="<c:url value="/entity/assister/declinePopup/${cecIdForEncryption}/${entryIdForEncryption}?prevStatus=${desigStatus}"/>"><spring:message code="label.assister.indDecline"/></a></li>
																				</ul>
																	</div>
																</td>
															</c:when>
														</c:choose>
																		
												</tr>
											</c:forEach>
								</table>							
							</c:when>
						</c:choose>	
						<c:choose>
							<c:when
								test="${fn:length(individualList) > 0 || resultSize > 0}">
								<div class="center" id="paginationContainer">
								 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
								 </div>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="$(\"#status option:selected\").val() == 'Pending'">
										<div class="alert alert-info">No approvals needed.</div>
									</c:when>
									<c:otherwise>
										<div class="alert alert-info">No matching records found.</div>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
					</c:choose>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end of .row-fluid -->

<%-- Secure Inbox Start--%>
<%-- Secure Inbox End--%>



<script type="text/javascript">
$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx + '/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});
$(document).ready(function() {
	
	$(".inddetail").click(
			function(e) {
				e.preventDefault();
				var href = $(this).attr('href');
				if (href.indexOf('#') != 0) {
					$(
							'<div id="inddetail" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe src="'
									+ href
									+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe> <button class="btn offset2" data-dismiss="modal" aria-hidden="true">Cancel</button></div></div>')
							.modal();
				}
	});
	
		
	var desigStatus = '${param.desigStatus}';
	if(desigStatus =="Active"){
		$('table[name="individualListTable${desigStatus}"]').tablesorter({
			sortList : [ [ 1, 0 ]]
		});
	}
	else{
		$('table[name="individualListTable${desigStatus}"]').tablesorter({
			sortList : [ [ 3, 0 ]]
		});	
	}
	
	$('input[name="check_allActive"]').click(function() {
		  
		  if( $('input[name="check_allActive"]').is(":checked") )
		  {
			  $('input[name="indCheckboxActive"]').each( function() {
				  	$(this).attr("checked",true);
      			$('input[name="indCheckboxActive"]').prop('checked', true);						  
			  });
			  
		  } else {
			  $('input[name="indCheckboxActive"]').each( function() {
      			$('input[name="indCheckboxActive"]').prop('checked', false);
				  	$(this).prop('checked', false);
			  });
		  }
	  });
	$(":checkbox").click(countChecked);
	
	
	//Code to add onlick event for pagination links
    if(${CA_STATE_CODE} == true){
    	
        
        $('#paginationContainer').find('a').click(function(e) {
        	//prevent Default functionality
            e.preventDefault();
        	
            var individualIdArray = new Array();
            <c:forEach items="${individualsId}" var="oneId" varStatus="status"> 
            	individualIdArray.push(${oneId});
        	</c:forEach>
        	
            var newForm = document.createElement("form");
			var existURL = $(this).attr('href');
			
			if (existURL.indexOf('individualsIdAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('individualsIdAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('individualsIdAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			if (existURL.indexOf('totalCountAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('totalCountAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('totalCountAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			
			if (existURL.indexOf('landingFromPrev' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('landingFromPrev'));
		        var suffix = existURL.substring(existURL.indexOf('landingFromPrev'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			
            
            $(newForm).attr("action", existURL)
                   .attr("method", "post");
            
            $(individualIdArray).each(function( index ) {
            	 $(newForm).append('<input type="hidden" name="individualsIdAtClient" value="' + individualIdArray[index] + '" />');
            });
            $(newForm).append('<input type="hidden" name="totalCountAtClient" value="' + ${resultSize}  + '" />');            
            $(newForm).append('<input type="hidden" name="csrftoken" value="' + $("#csrftoken").attr('value') + '" />');
            
          //If Landing from Prev click
            var textOfPageLink =  $(this).text();
            textOfPageLink = textOfPageLink.trim();
            var textOfPageLinkArray = textOfPageLink.split( /\s+/);
            var isnum = /^\d+$/.test(textOfPageLinkArray[textOfPageLinkArray.length-1]);
            if(!isnum){
          	  var localTempPageNumStr = getParameterByName('pageNumber',existURL);
          	  isnum = /^\d+$/.test(localTempPageNumStr);
          	  if(isnum && ((parseInt(localTempPageNumStr)%10)==0) ){
          		  $(newForm).append('<input type="hidden" name="landingFromPrev" value="true" />');
          	  }
            }
            
            
            document.body.appendChild(newForm);
            $(newForm).submit();
            document.body.removeChild(newForm);
        	return false;
    	});
    }
	
	
	

});  // end of document ready.

	var validator = $("#individualsearch")
			.validate(
					{
						onkeyup : false,
						onclick : false,
						rules : {
							requestSentFrom : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							requestSentTo : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							inactiveDateFrom : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							inactiveDateTo : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							activeDateFrom : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							activeDateTo : {
								chkDateFormat : true, chkDateFromAndTo:true
							}
						},
						messages : {
							requestSentFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							requestSentTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							inactiveDateFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							inactiveDateTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							activeDateFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							activeDateTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							}
						}
					});
	
	jQuery.validator.addMethod("chkDateFromAndTo", function(value, element, param) {
 		var requestSentFrom = $('#requestSentFrom').val();
		var requestSentTo = $('#requestSentTo').val();
		
		var inactiveDateFrom = $('#inactiveDateFrom').val();
		var inactiveDateTo = $('#inactiveDateTo').val();
		
		var activeDateFrom = $('#activeDateFrom').val();
		var activeDateTo = $('#activeDateTo').val();
		
		var fromDate;
		var toDate;
		
		if(requestSentFrom!=null && requestSentFrom!=''){
			fromDate = requestSentFrom;
		}
		if(requestSentTo!=null && requestSentTo!=''){
			toDate = requestSentTo;
		}
		if(inactiveDateFrom!=null && inactiveDateFrom!=''){
			fromDate = inactiveDateFrom;
		}
		if(inactiveDateTo!=null && inactiveDateTo!=''){
			toDate = inactiveDateTo;
		}
		if(activeDateFrom!=null && activeDateFrom!=''){
			fromDate = activeDateFrom;
		}
		if(activeDateTo!=null && activeDateTo!=''){
			toDate = activeDateTo;
		}
		if((fromDate != null) &&(toDate !=null)){
			if((new Date(fromDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))  > (new Date(toDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))){
				
				return false;
			}  
		}
		return true;
});
	
	jQuery.validator.addMethod("chkDateFormat",
			function(value, element, param) {
		if(value!=""){
				var isValid = false;
				var reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
				if (reg.test(value)) {
					var splittedDate = value.split('/');
					var mm = parseInt(splittedDate[0], 10);
					var dd = parseInt(splittedDate[1], 10);
					var yyyy = parseInt(splittedDate[2], 10);
					var newDate = new Date(yyyy, mm - 1, dd);
					if ((newDate.getFullYear() == yyyy)
							&& (newDate.getMonth() == mm - 1)
							&& (newDate.getDate() == dd))
						isValid = true;
					else
						isValid = false;
				} else
					isValid = false;

				return isValid;
		}
		return true;

			});
	
	function resetAll() {
			var  contextPath =  "<%=request.getContextPath()%>";
		    var documentUrl = contextPath + "/entity/enrollmententity/resetall?desigStatus=${desigStatus}";
		    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
	 }
	
	function getParameterByName(name, url) {
		if (!url) '10';
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		 results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		 return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
	function countChecked() { 
		var n = $("input[name='indCheckboxActive']:checked").length;			
		var itemsSelected = "<small>&#40; " + n
		+ (n === 1 ? " Item" : " Items")
		+ " Selected &#41;&nbsp;</small>";
		$("#counter").html(itemsSelected);
		if(n > 0) {
			$("#bulkActions").show();
			
		} else { 
			$("#bulkActions").hide();
			
		}
	}
	function validate(todo) {
		var confirmMessage = "Are you sure to " + todo + " ?";

		if (confirm(confirmMessage) == false) {
			return false;
		}
	};
	$(".validateDecline").on('click', function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		if (href.indexOf('#') != 0) {
			$('<div id="declinepopup" class="modal"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3><spring:message code="label.indAreYouSure"/></h3></div><div class="modal-body"><iframe id="declinepopup" src="'
							+ href
							+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:135px;"></iframe></div></div>')
					.modal();
		}
	});
 
 function closeIFrame() {
	 
	 	window.location.reload(true);
		 
		$("#reassignpopup").remove();
		
	}

 $("#reassignlink").on('click', function(e) {
		e.preventDefault();			
		var chkSize = $("input[name='indCheckboxActive']:checked").length;
		if(chkSize && chkSize > 0) {				
			var chkValues = "";				
			for(var i = 0; i < chkSize; i++) {
				
				if($("input[name='indCheckboxActive']:checked")[i] && $("input[name='indCheckboxActive']:checked")[i].value) {
					chkValues += $("input[name='indCheckboxActive']:checked")[i].value + ",";						
				}					
			}								
		}			
		var href1 = '<c:out value="${theUrl}"/>';		
		
		var csrfValue= '';
		
		if (href1.indexOf('csrftoken' + "=") >= 0){
	        var prefix = href1.substring(0, href1.indexOf('csrftoken'));
	        var suffix = href1.substring(href1.indexOf('csrftoken'));
	        suffix = suffix.substring(suffix.indexOf("=") + 1);
	        csrfValue = suffix;
	        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
	        href1 = prefix + suffix;
	    }
		
		
		var href = '<c:url value="/entity/enrollmententity/reassignIndividuals/N"/>';
		$.ajax({
			url : href1,		
			type : 'POST',
			data : {				
				indIDList : chkValues ,
				'csrftoken' : csrfValue
			},				
			dataType : "text",
			success : function(data,xhr) {
				if(isInvalidCSRFToken(xhr))                                  
                    return; 
				if (data == "success") {
					
					$('<div id="reassignpopup" class="modal"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><iframe id="reassignpopup" src="'
							+ href
+ '"style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div></div>')
					.modal();	
					}
		},
			error : function(data) {							
				alert("Failed to get respose");
			},
		});
					
		
	});
 
 function isInvalidCSRFToken(xhr) {
     var rv = false;
     if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
            alert($('Session is invalid').text());
            console.log("Session is invalid");
            rv = true;
     }
 return rv;
} 
 
 
 	function traverse(url){
		var queryString = {};
		url.replace(
			    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
			    function($0, $1, $2, $3) { queryString[$1] = $3; }
			);
		 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
		 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
		 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
		 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
		$("#individualsearch input[type=submit]").click();
	}
 
	 
	 
</script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.tablesorter.min.js" />"></script>
