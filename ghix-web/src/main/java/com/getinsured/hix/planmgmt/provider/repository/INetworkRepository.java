package com.getinsured.hix.planmgmt.provider.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Plan;

public interface INetworkRepository extends JpaRepository<Network, Integer> {

	@Query("SELECT count(*) FROM Network n where LOWER(n.type) = LOWER(:type) and LOWER(n.name)= LOWER(:name) and n.issuer.id= :issuerId ")
    long countNetworkByTypeAndName(@Param("type") String netType, @Param("name") String name, @Param("issuerId") int issuerId);
	
	@Query("SELECT p FROM Plan p where p.network.id = :networkId")
    Plan getPlanByNetworkId(@Param("networkId") int networkId);
	
	@Query("SELECT n FROM Network n where n.issuer.id = :issuerId")
    List<Network> getProviderNetworkByIssuerId(@Param("issuerId") int issuerId);
	
	@Query("SELECT n FROM Network n where n.issuer.id = :issuerId order by n.creationTimestamp desc")
    Page<Network> getProviderNetworkByIssuerId(@Param("issuerId") int issuerId, Pageable pageable);
	
	@Query("SELECT n FROM Network n where n.networkID = :networkId")
	List<Network> getProviderNeworkByNetworkIdValue(@Param("networkId") String networkId);
	
	@Query("SELECT n FROM Network n, Issuer i where n.networkID = :networkId AND LOWER(i.hiosIssuerId) = LOWER(:hiosId) AND n.issuer = i.id")
	List<Network> getProviderNeworkByNetworkAndHiosValue(@Param("networkId") String networkId,@Param("hiosId") String hiosId);
	
	@Query("SELECT n FROM Network n where n.networkID = :networkId")
	Network getNeworkByNetworkIdValue(@Param("networkId") String networkId);
}
