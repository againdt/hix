package com.getinsured.ssap.controller

import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository
import com.getinsured.hix.ssap.SsapConstants
import com.getinsured.ssap.model.eligibility.RunMode
import com.getinsured.ssap.service.EligibilityEngineIntegrationService
import com.getinsured.ssap.service.SsapComparisonService
import com.getinsured.ssap.util.JsonUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.util.*

/**
 * Pre-eligibility controller.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-31
 */
@RestController
open class PreEligibilityController @Autowired constructor(
  private val ssapApplicationRepository: SsapApplicationRepository,
  private val eligibilityEngineIntegrationService: EligibilityEngineIntegrationService,
  private val ssapApplicantRepository: SsapApplicantRepository,
  private val ssapComparisonService: SsapComparisonService
) {
  companion object {
    val log: Logger = LoggerFactory.getLogger(PreEligibilityController::class.java)
  }

  @PreAuthorize(SsapConstants.SSAP_VIEW)
  @RequestMapping(value = ["/newssap/preeligibility/{applicationId}"], method = [RequestMethod.GET])
  open fun runPreEligibility(@PathVariable applicationId: Long): ResponseEntity<Any> {
    val ssapApplication = ssapApplicationRepository.findOne(applicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapOf("error" to true, "message" to "SSAP not found"))

    val ssap = JsonUtil.parseApplicationDataJson(ssapApplication)
      ?: return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(mapOf("error" to true, "message" to "Could not parse SSAP JSON"))

    log.info("Invoking pre-eligibility");

    try {
      val eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(ssap, RunMode.PRE_ELIGIBILITY)

      if (eligibilityResponse != null) {
        log.debug("Got pre-eligibility response: {}", eligibilityResponse)

        val applicants = ssapApplicantRepository.findBySsapApplicationId(applicationId);

        if(applicants != null && applicants.isNotEmpty()) {
          val resp = ArrayList<PersonResponse>(applicants.size)
          eligibilityResponse.members.forEach { member ->
              if(member.isMedicaid || member.isChip) {
                Optional.ofNullable(applicants.find { applicant -> applicant.applicantGuid == member.applicantId.toString() }).ifPresent {
                  resp.add(PersonResponse(it.applicantGuid, member.isMedicaid, member.isChip))
                }
              }
          }
          log.info("Returning pre-eligibility results with {} members", resp.size)
          return ResponseEntity.ok(resp)
        }
        else {
          log.error("Could not find SSAP Applicants for given SSAP id: {}", applicationId);

          return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED)
            .body(mapOf("error" to true, "message" to "Could not find SSAP Applicants for given SSAP id: $applicationId"));
        }
      }
    } catch (e: Exception) {
      log.error("Exception occurred during pre-eligibility call", e)
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapOf("error" to true, "message" to e.message));
    }

    return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(mapOf("error" to true))
  }

  @PreAuthorize(SsapConstants.SSAP_VIEW)
  @RequestMapping(value = ["/newssap/preeligibility/compare/{applicationId}"], method = [RequestMethod.GET])
  open fun compare(@PathVariable applicationId: Long): ResponseEntity<Any> {
    val ssapApplication = ssapApplicationRepository.findOne(applicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapOf("error" to true, "message" to "SSAP not found"))

    val ssap = JsonUtil.parseApplicationDataJson(ssapApplication)
      ?: return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(mapOf("error" to true, "message" to "Could not parse SSAP JSON"))

    if(ssapApplication.parentSsapApplicationId == null) {
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(mapOf("error" to true, "message" to "Given SSAP was not cloned, parentSsapApplicationId is not set"))
    }

    val parentApplication = ssapApplicationRepository.findOne(ssapApplication.parentSsapApplicationId.longValueExact())
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapOf("error" to true, "message" to "Parent SSAP not found"));

    val parentSsap = JsonUtil.parseApplicationDataJson(parentApplication)
      ?: return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(mapOf("error" to true, "message" to "Could not parse Parent SSAP JSON"))

    val comparisonResult = ssapComparisonService.compare(ssap, parentSsap)

    return ResponseEntity.ok(comparisonResult)
  }
}

data class PersonResponse(val applicantGuid : String, val medicaid: Boolean, val chip: Boolean)
