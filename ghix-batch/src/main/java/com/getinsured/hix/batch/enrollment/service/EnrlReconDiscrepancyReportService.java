/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import org.springframework.batch.admin.service.JobService;

import com.getinsured.hix.dto.enrollment.EnrlDiscrepancyReportCsvDTO;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;

/**
 * Enrollment Reconciliation Discrepancy Report Service
 * @author negi_s
 * @since 22/03/2017
 *
 */
public interface EnrlReconDiscrepancyReportService {
	
	/**
	 * Generates the reconciliation discrepancy report for the given HIOS Issuer ID and the year
	 * @param hiosIssuerId
	 * @param applicableYear
	 * @param jobService
	 * @param jobExecutionId
	 * @throws Exception
	 */
	void generateReportForHiosIssuerId(String hiosIssuerId, int applicableYear, JobService jobService,
			long jobExecutionId) throws Exception;
	
	/**
	 * Populates a list of EnrlDiscrepancyReportCsvDTO for the given discrepancy list
	 * @param discrepancyList
	 * @param reconFileName
	 * @param fileId
	 * @return List of EnrlDiscrepancyReportCsvDTO
	 */
	List<EnrlDiscrepancyReportCsvDTO> populateDiscrepancyDto(List<EnrlReconDiscrepancy> discrepancyList, String reconFileName, Integer fileId);
	
	/**
	 * Get HIOS Issuer ID's for discrepancy report generation
	 * @param applicableYear
	 * @return List of HIOS ID's
	 */
	List<String> getHiosIdListForReportGeneration(int applicableYear);
	
	/**
	 * Get a list of started batch jobs for the given job name
	 * @param jobName
	 * @return List of BatchJobExecution
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);
	
	/**
	 * Generates the reconciliation discrepancy report for the given file Id
	 * @param fileId
	 * @param applicableYear
	 * @param jobService
	 * @param jobExecutionId
	 * @throws Exception
	 */
	void generateReportForFileId(String fileId, int applicableYear, JobService jobService,
			long jobExecutionId) throws Exception;
}
