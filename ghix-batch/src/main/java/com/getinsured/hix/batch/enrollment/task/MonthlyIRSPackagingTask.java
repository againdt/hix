package com.getinsured.hix.batch.enrollment.task;

import java.util.Calendar;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyIRSBatchService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author Priya
 * 
 */
public class MonthlyIRSPackagingTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonthlyIRSPackagingTask.class);
	private EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService;
	private String strMonth;
	private String strYear;
	int applicableMonth = 0;
	int applicableYear = 0;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
	     try{
	    	 setApplicableMonthYear();
	    	 enrollmentMonthlyIRSBatchService.generateFilePayloadBatchTransmissionXML(applicableMonth,applicableYear);
	     }
	     catch (Exception e) {
				LOGGER.info("FilePayloadIRSReportingTask.execute : FAIL "+e.getMessage());
				throw new GIException("FilePayloadIRSReportingTask.execute : FAIL "+ e.getMessage(),e);
			}
			return RepeatStatus.FINISHED;
	}
	public EnrollmentMonthlyIRSBatchService getEnrollmentMonthlyIRSBatchService() {
		return enrollmentMonthlyIRSBatchService;
	}
	public void setEnrollmentMonthlyIRSBatchService(EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService) {
		this.enrollmentMonthlyIRSBatchService = enrollmentMonthlyIRSBatchService;
	}
	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = ""+Calendar.getInstance().get(Calendar.YEAR);
		} else {
			this.strYear = strYear;
		}
	}
	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		if ((null == strMonth) || (strMonth.equalsIgnoreCase("null")) || strMonth.isEmpty()) {
			this.strMonth = "-1";
		} else {
			this.strMonth = strMonth;
		}
	}
	private void setApplicableMonthYear() throws GIException{
		int intMonth = 0;
		int intYear = 0;

		// TODO - Get as input and pass to Service.
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		int currentMonth = calObj.get(Calendar.MONTH);
		if(NumberUtils.isNumber(strMonth) && NumberUtils.isNumber(strYear)){
			intMonth = Integer.parseInt(strMonth);
			intYear = Integer.parseInt(strYear);
			if(((intMonth>0 && intMonth<=12) || intMonth==-1) && intYear <= currentYear){
				applicableMonth = intMonth;
				applicableYear = intYear;
				if(intYear < currentYear && intMonth == -1){
					applicableMonth = 0;
					applicableYear = intYear+1;
				}else if(intMonth == -1){
					applicableMonth = currentMonth;
					applicableYear = intYear;
				}else{
					applicableMonth--;
				}


			}else{
				throw new GIException("Please provide valid month(mm) in the range [1,12] (Jan - Dec) and year <= current year");
			}
		}else{
			throw new GIException("Please provide valid month(mm) and year(yyyy)");
		}

	}
}
