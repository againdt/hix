package com.getinsured.eligibility.at.ref.service.nonfinancial;

import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 * 
 */
public interface ReferralNFCompareService {
	boolean memberMatching(NFProcessDTO nfProcessRequestDTO);
	
	boolean renewalMemberMatching(NFProcessDTO nfProcessRequestDTO);
	
	CompareMainDTO executeCompare(SsapApplication currentApplication, SsapApplication nonFinancialApplication);
	
	CompareMainDTO executeRenewalCompare(SsapApplication currentApplication, SsapApplication nonFinancialApplication);
	
	void demoUpdate(CompareMainDTO compareMainDTO) throws Exception;
}
