package com.getinsured.hix.platform.notices.jpa;

import java.util.List;

/*
 * Model Class for List testing
 * @author Kuldeep
 * @since 10th Feb 2013
 */
public class Book
{
	private String title;

	private int pages;

	private String author;

	private String publications;

	private float price;

	private List<Chapter> chapters;


	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public int getPages()
	{
		return pages;
	}

	public void setPages(int pages)
	{
		this.pages= pages;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public String getPublications()
	{
		return publications;
	}

	public void setPublications(String publications)
	{
		this.publications = publications;
	}

	public float getPrice()
	{
		return price;
	}

	public void setPrice(float price)
	{
		this.price = price;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

}
