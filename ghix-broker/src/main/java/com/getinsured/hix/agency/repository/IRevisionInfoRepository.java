package com.getinsured.hix.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.agency.RevisionInfo;

public interface IRevisionInfoRepository extends JpaRepository<RevisionInfo, Long> {

}
