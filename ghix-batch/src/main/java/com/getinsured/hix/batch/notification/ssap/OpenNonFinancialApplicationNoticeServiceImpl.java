package com.getinsured.hix.batch.notification.ssap;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

@Service("openNonFinancialApplicationNoticeService")
public class OpenNonFinancialApplicationNoticeServiceImpl implements OpenNonFinancialApplicationNoticeService {
	
	private static final Logger lOGGER = Logger.getLogger(OpenNonFinancialApplicationNoticeServiceImpl.class);
	//@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private NoticeService noticeService;
	@Autowired private ILocationRepository iLocationRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.notification.ssap.OpenNonFinancialApplicationNoticeService#generateNoticeDocument(com.getinsured.iex.ssap.model.SsapApplication)
	 */
	@Override
	public String generateNoticeDocument(SsapApplication ssapApplication) throws NoticeServiceException {
		
		return generate(ssapApplication, "EE004IncompleteApplication");
	}

	private String generate(SsapApplication ssapApplication, String noticeTemplateName) throws NoticeServiceException{
		
		ssapApplication = ssapApplicationRepository.findByCaseNumber(ssapApplication.getCaseNumber()).get(0);
		
		int moduleId = fetchModuleId(ssapApplication);
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		Location location = fetchPrimarySsapApplicantAddress(ssapApplication.getSsapApplicants());

		String relativePath = "cmr/" + moduleId + "/ssap/"+ ssapApplication.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId+ (new Date().getTime()) + ".pdf";
		String emailId = getEmailId(ssapApplication);
		List<String> validEmails = emailId != null ? Arrays.asList(emailId): null;
		Notice notice = null;
		 notice = noticeService.createModuleNotice(
						noticeTemplateName,
						GhixLanguage.US_EN,
						getReplaceableObjectData(ssapApplication),
						relativePath,
						ecmFileName,
						moduleName,
						moduleId,
						validEmails,
						DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),fullName, location, GhixNoticeCommunicationMethod.Mail);
	
		return notice.getEcmId();
	}
	
	private Location fetchPrimarySsapApplicantAddress(
			List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return fetchPrimarySsapApplicantAddress(ssapApplicant);
			}
		}

		return null;

	}

	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}

		return null;
	}
	

	private Location fetchPrimarySsapApplicantAddress(
			SsapApplicant ssapApplicant) {
		int locationid = ssapApplicant.getMailiingLocationId() != null ? ssapApplicant
				.getMailiingLocationId().intValue() : 0;
		if (locationid == 0) {
			locationid = ssapApplicant.getOtherLocationId() != null ? ssapApplicant
					.getOtherLocationId().intValue() : 0;
		}
		return iLocationRepository.findOne(locationid);

	}

	private int fetchModuleId(SsapApplication ssapApplication) {

		int cmrId = ssapApplication.getCmrHouseoldId() != null ? ssapApplication.getCmrHouseoldId().intValue() : 0;

		if (cmrId == 0) {
			throw new GIRuntimeException(
					"Cannot generate notification! CMR Household ID not found for case number - "
							+ ssapApplication.getCaseNumber());
		}
		return cmrId;
	}
	private Map<String, Object> getReplaceableObjectData(
			SsapApplication ssapApplication) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("primaryApplicantName", getName(ssapApplication));
		tokens.put("ApplicationID", ssapApplication.getCaseNumber());
		tokens.put("Date", DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", ssapApplication.getId());
		lOGGER.debug(" loading these tokens in template :-: " +tokens.toString());
		return tokens;
	}
	
	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName()+ " "+ssapApplicant.getLastName();
			}
		}

		return null;
	}
	
}
