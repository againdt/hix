package com.getinsured.hix.batch.enrollment.task;

import com.getinsured.hix.platform.batch.service.GhixStoppableTasklet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Created by song_s on 7/31/17.
 */
public class LegacyStoppableJob extends GhixStoppableTasklet {
    private static final Logger logger = LoggerFactory.getLogger(LegacyStoppableJob.class);

    @Override
    protected RepeatStatus _execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        StepExecution se = chunkContext.getStepContext().getStepExecution();
        int loop = 10;
        while (loop-- > 0) {
            logger.info("working...." + chunkContext.toString());
            try {
                Thread.sleep((getStopDelaySeconds() - 1) * 1000);
                //super.setProgressPercentage((10-loop)*10);
            }
            catch (InterruptedException e) {
                logger.info("Interrupted: " + chunkContext.getStepContext().getStepExecution().toString());
            }

            if(se.isTerminateOnly()){
                logger.info("Received signal to stop the job");
                if (se.getJobExecution().getExitStatus().equals(ExitStatus.EXECUTING)) {
                    se.getJobExecution().setExitStatus(ExitStatus.STOPPED);
                }
                break;
            }
        }
        logger.info("finished: " + chunkContext.getStepContext().getStepExecution().toString());
        return RepeatStatus.FINISHED;
    }

    @Override
    public long getStopDelaySeconds() {
        return 5;
    }
}
