package com.getinsured.eligibility.ssap.integration.at.client.service;

import net.minidev.json.JSONObject;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.OrganizationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonOrganizationAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AuthorizedRepresentativeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.util.ReferralUtil;

public class AuthorizedRepresentativeMapper {

	
	public AuthorizedRepresentativeType createAuthorizedRep(JSONObject authorizedRepresentative){
		
		AuthorizedRepresentativeType authorizedRepresentativeType = AccountTransferUtil.insuranceApplicationObjFactory.createAuthorizedRepresentativeType();
		PersonType personType = AccountTransferUtil.hixCoreFactory.createPersonType();
		PersonNameType personNameType =AccountTransferUtil.niemCoreFactory.createPersonNameType();
		
		if(ReferralUtil.isNotNullAndEmpty(((JSONObject)authorizedRepresentative.get("name")).get("firstName").toString())){
			personNameType.setPersonGivenName(AccountTransferUtil.createPersonNameTextType(((JSONObject)authorizedRepresentative.get("name")).get("firstName").toString()));
		}
		if(ReferralUtil.isNotNullAndEmpty(((JSONObject)authorizedRepresentative.get("name")).get("middleName").toString())){
			personNameType.setPersonMiddleName(AccountTransferUtil.createPersonNameTextType(((JSONObject)authorizedRepresentative.get("name")).get("middleName").toString()));
		}
		if(ReferralUtil.isNotNullAndEmpty(((JSONObject)authorizedRepresentative.get("name")).get("lastName").toString())){
			personNameType.setPersonSurName(AccountTransferUtil.createPersonNameTextType(((JSONObject)authorizedRepresentative.get("name")).get("lastName").toString()));
		}
		if(ReferralUtil.isNotNullAndEmpty(((JSONObject)authorizedRepresentative.get("name")).get("suffix").toString())){
			TextType textType =AccountTransferUtil.niemCoreFactory.createTextType();
			textType.setValue(((java.lang.String) ((JSONObject)authorizedRepresentative.get("name")).get("suffix")));
			personNameType.setPersonNameSuffixText(textType);
		}
		personType.getPersonName().add(personNameType);

		PersonAugmentationType representativePersonAugmentationType = AccountTransferUtil.hixCoreFactory.createPersonAugmentationType();
		PersonOrganizationAssociationType personOrganizationAssociationType = AccountTransferUtil.hixCoreFactory.createPersonOrganizationAssociationType();
		OrganizationType organizationType = AccountTransferUtil.hixCoreFactory.createOrganizationType();
		organizationType.setOrganizationName(AccountTransferUtil.createTextType((java.lang.String) authorizedRepresentative.get("companyName")));

		IdentificationType orgIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		orgIdentificationType.setIdentificationID(AccountTransferUtil.createTextType((java.lang.String) authorizedRepresentative.get("organizationId")));
		organizationType.setOrganizationIdentification(orgIdentificationType);
		
		personOrganizationAssociationType.setOrganization(organizationType);
		representativePersonAugmentationType.setPersonOrganizationAssociation(personOrganizationAssociationType);
		personType.setPersonAugmentation(representativePersonAugmentationType);
		authorizedRepresentativeType.setRolePlayedByPerson(personType);
	
		return authorizedRepresentativeType;
	}
	
}
