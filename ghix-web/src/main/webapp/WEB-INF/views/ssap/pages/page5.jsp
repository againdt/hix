 <%@ include file="../include.jsp" %>         
<div id="appscr55" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page5.whoIsHelpingYou" text="Who is Applying?" /></h5>
		</div>
		<form method="POST" action="#" class="form-horizontal" id="get-help-with-costs-form" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.page5.whoAreYouApplying" text="Who are you applying for?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></h4>
			</div>
			<%-- <p class="gutter10">
				<spring:message code="ssap.page5.whoAreYouApplyingFor" text="Who are you applying for health insurance and health benefits for ?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
			</p> --%>
			<div class="control-group margin10-l">
				<label class="capitalize-none radio" for="ApplyingForhouseHoldOnly">
					<input type="radio" id="ApplyingForhouseHoldOnly" name="ApplyingForhouseHold" value="houseHoldContactOnly" onchange="applyingHousehold()" data-parsley-errors-container="#applyingForhouseHoldOnlyErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/> 
					<span id="houseHold_Contact_span1" class="camelCaseName"></span> <spring:message code="ssap.page5.only" text="only" />
				</label>
				<label class="capitalize-none radio" for="ApplyingForhouseHoldMember">
					<input type="radio" id="ApplyingForhouseHoldMember" name="ApplyingForhouseHold" value="otherFamilyMember" onchange="applyingHousehold()" />
					<span id="houseHold_Contact_span2" class="camelCaseName"></span><spring:message code="ssap.page5.otherFamilyMembers" text="&amp; Other family members " />
				</label>
				<label class="capitalize-none radio" for="ApplyingForhouseHoldOther">
					<input type="radio" id="ApplyingForhouseHoldOther" name="ApplyingForhouseHold" value="otherExcludingHousehold" onchange="applyingHousehold()" />
					<spring:message code="ssap.page5.otherFamilyMembersNot" text=" Other family members, not" />
					<span id="houseHold_Contact_span3" class="camelCaseName"></span>
				</label>
				<div id="applyingForhouseHoldOnlyErrorDiv"></div>
			</div>
			
			<p class="gutter10 non_financial_hide_id">
				<label class="capitalize-none">
					<spring:message code="ssap.page5.doYouWantToFindOut" text="Do you want to find out if you/your family can get help paying for some or all of your health insurance?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</label>
			</p>
			
			<div class="control-group margin10-l non_financial_hide_id">
				<label class="radio" for="wantToGetHelpPayingHealthInsurance_id1">
					<input type="radio" id="wantToGetHelpPayingHealthInsurance_id1" class="" name="wantToGetHelpPayingHealthInsurance" value="Yes" onclick="showOrHideOptionDiv();"  data-parsley-errors-container="#wantToGetHelpPayingHealthInsurance_id1ErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/> 
					<spring:message code="ssap.page5.doYouWantToFindOutYes" text="Yes" />
				</label>
				
				<label class="radio" for="wantToGetHelpPayingHealthInsurance_id2">
					<input type="radio" id="wantToGetHelpPayingHealthInsurance_id2" class="" name="wantToGetHelpPayingHealthInsurance" value="No" onclick="showOrHideOptionDiv();"/>
					<spring:message code="ssap.page5.doYouWantToFindOutNo" text="No" />
				</label>
				<div id="wantToGetHelpPayingHealthInsurance_id1ErrorDiv"></div>
			</div>
			
			
			<div  id="contant_message_ui" class="content-box border-custom hide">
				<h4 class="margin0"><spring:message code="ssap.page5.getHelpWithCost" text="Get help with costs (optional)" /></h4>
				<div class="gutter10">
					<p>
		   				<spring:message code="ssap.page5.getHelpWithCostText1" text="Even working families can pay less for health insurance right now." />
		   				<spring:message code="ssap.page5.getHelpWithCostText2" text="You may be eligible for a free or low cost plan, or a new kind of tax credit that lowers your monthly premiums right away." />
		   				<spring:message code="ssap.page5.getHelpWithCostText3" text="Answer 2 questions to see if you can get a break on costs:" />
		   			</p>
		   			<p>
		   				<spring:message code="ssap.page5.noOfPeopleText1" text="How many people are on your federal income tax return this year?" /> 
		   				<spring:message code="ssap.page5.noOfPeopleText2" text="(If you didn&apos;t file taxes last year, tell us how many people live with you, including yourself.)" />
		   			</p>
		   			<div class="gutter10">
			   			<div class="margin10-b radio" for="noOfPeople53">
								<input required type="text" id='noOfPeople53'  name="noOfPeople53" value='1' 
								class="input-medium" placeholder="No. of people" onKeyUp='setFederalAmount400(this);'
								aria-label="No. of people">
						</div>
		   			</div>
		   			<p class="clearfix">
		   				<spring:message code="ssap.page5.basedOnYourText1" text="Based on your best guess, do you expect your total household income to be less than Equivalent to" />
		   				<span id='amount400'></span>
		   				<spring:message code="ssap.page5.basedOnYourText2" text="of the FPL in dollars for family size listed plus buffer this year?" />
		   			</p>
		   			<div class="gutter20">
						<label class="margin5-b radio" for="page55radio6">
							<input type="radio" name="appscr55paragraph" value="yes" id="page55radio6" onclick="setDiffertContent();" >
							<spring:message code="ssap.page5.basedOnYourYes" text="Yes" />
						</label>
						<label class="margin5-b radio" for="page55radio7">
							<input type="radio" name="appscr55paragraph" value="no" id="page55radio7" onclick="setDiffertContent();">
							<spring:message code="ssap.page5.basedOnYourNo" text="No" />
						</label>
						<label class="margin5-b capitalize-none radio" for="page55radio8">
							<input type="radio" name="appscr55paragraph" value="unknown" id="page55radio8" onclick="setDiffertContent();">
							<spring:message code="ssap.page5.basedOnYourIDontKnow" text="I don&apos;t know" />
						</label>
					</div>
					<div id="contant_message_ui11" class="blue-background margin10-tb gutter10">
						<strong>
							<spring:message code="ssap.page5.weEncourageText1" text="We encourage you to apply to see what help you can get paying for health insurance." />
						</strong>
						<spring:message code="ssap.page5.weEncourageText2" text="Based on what you told us, you may be eligible to get health benefits or help paying for health insurance through the Health Insurance Marketplace." />
						<spring:message code="ssap.page5.weEncourageText3" text="To begin the application, select &quot;yes&quot; on the next question." />
					</div>
					<p class="margin5-b">
						<spring:message code="ssap.page5.doYouWantToSee" text="Do you want to see what help you can get paying for health insurance?" />
					</p>
					<div class="gutter20">
						<label class="margin5-b radio" for="page55radio9">
							<input type="radio" onchange="unknown55ShowHide()" name="checkTypeOfHelp4HealthInsurance" value="yes" id="page55radio9">
							<spring:message code="ssap.page5.doYouWantToSeeYes" text="Yes" />
							<small><spring:message code="ssap.page5.doYouWantToSeeYesExplanation" text="We will ask you questions about your income and personal situations to see how much you qualify for." /></small>
						</label>
						<label class="margin5-b radio" for="page55radio10">
							<input type="radio" onchange="unknown55ShowHide()" name="checkTypeOfHelp4HealthInsurance" value="no" id="page55radio10">
							<spring:message code="ssap.page5.doYouWantToSeeNo" text="No" />
							<small><spring:message code="ssap.page5.doYouWantToSeeNoExplanation" text="You will answer fewer questions, but you will not get help paying for health coverage." /></small>
						</label>
					</div>
				</div>
			</div>
			
		</form>
		<!-- add content above this -->
	</div>
</div>