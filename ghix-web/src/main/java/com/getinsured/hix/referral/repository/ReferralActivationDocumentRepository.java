package com.getinsured.hix.referral.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.referral.util.WebReferralConstants;

/**
 * @author chopra_s
 */
@Repository
@Qualifier("referralActivationDocumentRepository")
public interface ReferralActivationDocumentRepository extends JpaRepository<ConsumerDocument, Long> {
	@Query("select doc from ConsumerDocument doc where targetId = :referralActivation and targetName = '"+ WebReferralConstants.REFERRAL_TKM_MODULE_NAME +"'")
	List<ConsumerDocument> getDocumentsForReferralActivationId(@Param("referralActivation") Long referralActivation);
}
