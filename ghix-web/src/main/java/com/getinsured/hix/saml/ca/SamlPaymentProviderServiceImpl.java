package com.getinsured.hix.saml.ca;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.saml.SamlPaymentProviderService;

import gov.ca.calheers.carrierpayment.beans.CarrierPayment;
import gov.ca.calheers.saml2encode.Saml2EncodeManager;


@Component
public class SamlPaymentProviderServiceImpl implements SamlPaymentProviderService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SamlPaymentProviderServiceImpl.class);
	
	@Autowired
	private Saml2EncodeManager samlEncoder;

	private String paymentEnv;
	
	private Map<String, IssuerOverrideConfig> issuerPaymentOverride = Collections.synchronizedMap(new HashMap<String, IssuerOverrideConfig>());

	private long lastConfigLoad=-1l;
	
	@Value("#{configProp['issuer.payment.env'] != null ? configProp['issuer.payment.env'] : 'QA'}")
	public void setDefaultCredentials(String env) {
		this.paymentEnv = env.trim().toLowerCase();
	}
	
	@Override
	public boolean isApplicable(String stateCode, String issuerName) {
		if(stateCode.equalsIgnoreCase("CA")){
			return true;
		}
		return false;
	}
	
	@Override
	public  String processPaymentRequest(CarrierPayment carrierPayment, IssuerPaymentInfoResponse issuerPaymentInfo) {
		Saml2Solicitor solicit = this.setupSolictiation(carrierPayment, issuerPaymentInfo);
		
		samlEncoder.encode(solicit);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method Call To Proceed Payment Request through Saml..Encode Result is as ...: "
					+ solicit.getEncodedResult());
		}

		return solicit.getEncodedResult();

	}
	
	/**
	 * commenting this code for 19037 Carrier Payment input generator
	 * 
	 * @param carrierPayment
	 * @param carrierPaymentTransDto
	 * @return Saml2Solicitor instance
	 */


	private Saml2Solicitor setupSolictiation(CarrierPayment itemToEncode, IssuerPaymentInfoResponse issuerPaymentInfo) {
		Saml2Solicitor solicit = new Saml2Solicitor();
		solicit.setItemToEncode(itemToEncode);
		try {
			solicit.setAuthorizationDateTime(buildCurrentDate());
			solicit.setValidBeforeDate(buildValidBeforeDate());
			solicit.setValidAfterDate(buildValidAfterDate());
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
		String audience = issuerPaymentInfo.getIssuerAuthURL();
		String responseDesUrl = issuerPaymentInfo.getIssuerAuthURL();
		String assertionDestUrl = issuerPaymentInfo.getIssuerAuthURL();
		String issuerName = "www.coveredca.com";
		String hiosid = issuerPaymentInfo.getHiosId();
		IssuerOverrideConfig issuerOverride = null;
		try {
			issuerOverride = this.issuerPaymentOverride.get(hiosid);
			
			JSONObject destinationConfig = null;
			if(issuerOverride != null) {
				destinationConfig = issuerOverride.getConfig();
			}
			if(destinationConfig == null) {
				JSONParser parser = new JSONParser();
				String ghixHome = System.getProperty("GHIX_HOME");
				String configPath = ghixHome + File.separatorChar +"ghix-setup"
						+ File.separatorChar + "conf" + File.separatorChar + hiosid + "_saml.config";
				File f = new File(configPath);
				if (f.exists()) {
					LOGGER.info("Loading issuer payment override for HIOSID {}",hiosid);
					Reader r = new InputStreamReader(new FileInputStream(f));
					destinationConfig = (JSONObject) parser.parse(r);
					this.issuerPaymentOverride.put(hiosid, new IssuerOverrideConfig(destinationConfig, System.currentTimeMillis()));
					r.close();
				}
			}
			if(destinationConfig != null) {
				JSONObject envConfig = (JSONObject) destinationConfig.get(this.paymentEnv);
				if(envConfig != null) {
					Object obj = envConfig.get("audience_restriction_url");
					if (obj != null) {
						audience = (String) obj;
					}					
					obj = envConfig.get("response_destination_url");
					if (obj != null) {
						responseDesUrl = (String) obj;
					}
					obj = envConfig.get("assertion_destination_url");
					if (obj != null) {						
						assertionDestUrl = (String) obj;
					}
					obj = envConfig.get("issuer_name");
					if (obj != null) {
						issuerName = (String) obj;
					}
				}	
			}
		} catch (ParseException | IOException e) {
			LOGGER.error("Error parsing the payment destination config for HIOSID {}, Using default behavior", issuerPaymentInfo.getHiosId(),e);
		}
		solicit.setIssuerName(issuerName);
		solicit.setAudienceRestriction(audience);
		solicit.setResponseDestination(responseDesUrl);
		solicit.setAssertionDestination(assertionDestUrl);
		solicit.setNameQualifier("");
		solicit.setResponseItemName("carrierPayment");
		solicit.setSubjectName(issuerPaymentInfo.getSecurityDnsName());
		solicit.setSubjectDNS(issuerPaymentInfo.getSecurityDnsName());
		solicit.setSubjectAddress(issuerPaymentInfo.getSecurityAddress());
		solicit.setResponseUUID(UUID.randomUUID());
		solicit.setAssertionUUID(UUID.randomUUID());
		return solicit;

	}

	/**
	 * Determines whether SAML is configured to PROD mode
	 * @return true if SAML is configured to PROD mode
	 */
	
	private static XMLGregorianCalendar buildCurrentDate()
			throws DatatypeConfigurationException {

		GregorianCalendar calendar = new GregorianCalendar(
				TimeZone.getTimeZone("UTC"));
		calendar.set(GregorianCalendar.MILLISECOND, 0);
		DatatypeFactory factory = DatatypeFactory.newInstance();

		XMLGregorianCalendar xmlCal = factory.newXMLGregorianCalendar(calendar);

		return xmlCal;
	}

	private static XMLGregorianCalendar buildValidBeforeDate()
			throws DatatypeConfigurationException {
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		GregorianCalendar calendar = new GregorianCalendar(
				TimeZone.getTimeZone("UTC"));

			calendar.set(GregorianCalendar.MILLISECOND, 0);
			calendar.add(GregorianCalendar.MINUTE, 15);
		XMLGregorianCalendar xmlGregorianCalendar = datatypeFactory
				.newXMLGregorianCalendar(calendar);
		return xmlGregorianCalendar;
	}

	private XMLGregorianCalendar buildValidAfterDate()
			throws DatatypeConfigurationException {
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		GregorianCalendar calendar = new GregorianCalendar(
				TimeZone.getTimeZone("UTC"));

			calendar.set(GregorianCalendar.MILLISECOND, 0);
		XMLGregorianCalendar xmlGregorianCalendar = datatypeFactory
				.newXMLGregorianCalendar(calendar);
		return xmlGregorianCalendar;
	}
	
	private class IssuerOverrideConfig{
		private JSONObject obj;
		private Long initTime;

		IssuerOverrideConfig(JSONObject obj, Long initTime){
			this.obj = obj;
			this.initTime = initTime;
		}
		
		private boolean isStale() {
			return System.currentTimeMillis()-this.initTime > 2*60*1000;
		}
		
		public JSONObject getConfig() {
			if(isStale()) {
				return null;
			}
			return this.obj;
		}
	}

	
}
