package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrlReconSnapshotGenerationService;


public class EnrlReconSnapshotGenerationWriter implements ItemWriter<Long> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconSnapshotGenerationWriter.class);
	Integer partition;
	Long batchExecutionId;
	private JobService jobService;
	long jobExecutionId = -1;
	EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Writer ");

		ExecutionContext ec = stepExecution.getExecutionContext();
		if (ec != null) {
			//partition = ec.getInt("partition");
			batchExecutionId = stepExecution.getJobExecutionId();
			LOGGER.info("EnrlReconSnapshotGenerationWriter : Thread Name: " + Thread.currentThread().getName() + "Partition :: "
					+ partition);
		}
	}
	@Override
	public void write(List<? extends Long> items) throws Exception {
		// TODO Auto-generated method stub
		if(items!=null && items.size()>0){
			@SuppressWarnings("unchecked")
			List<Long> issuerPolicyIds=(ArrayList<Long>)items;
			
			
		}
		
		
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	public EnrlReconSnapshotGenerationService getEnrlReconSnapshotGenerationService() {
		return enrlReconSnapshotGenerationService;
	}
	public void setEnrlReconSnapshotGenerationService(
			EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService) {
		this.enrlReconSnapshotGenerationService = enrlReconSnapshotGenerationService;
	}
	
	
	
}
