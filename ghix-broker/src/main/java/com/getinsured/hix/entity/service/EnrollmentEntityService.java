package com.getinsured.hix.entity.service;

import com.getinsured.hix.model.entity.EnrollmentEntity;

/**
 * Encapsulates service layer method calls for Enrollment Entity
 */
public interface EnrollmentEntityService {

	/**
	 * Generates partial record for the given Enrollment Entity from third party
	 * and return the generated sequence id
	 * 
	 * @param enrollmentEntity
	 *            object with partial Enrollment Entity information like
	 *            business legal name,state ein etc.
	 * @return the Enrollment Entity id
	 */
	Integer generateEnrollmentEntityRecordId(EnrollmentEntity enrollmentEntity);

	/**
	 * Finds the Enrollment Entity by given email address
	 * 
	 * @param emailAddress
	 *            the given email address
	 * @return the existing Enrollment Entity if found
	 */
	EnrollmentEntity findByEmailAddress(String emailAddress);
	

	/**
	 * Finds the Enrollment Entity by given State EIN
	 * 
	 * @param stateEIN
	 *            the given state ein
	 * @return the existing Enrollment Entity if found
	 */
	EnrollmentEntity findByStateEIN(String stateEIN);
}