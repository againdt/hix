package com.getinsured.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.calldata.CallDataRequestPayload;

public interface ICallDataRepository extends JpaRepository<CallDataRequestPayload, Integer> {
	
	@Query(value = " select count(id) from CALL_DATA_REQUEST_PAYLOAD "
			+ "	where raw_request->>'interaction_id' = :interactionId",nativeQuery = true)
	int findByInteractionId(@Param("interactionId") String interactionId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE CALL_DATA_REQUEST_PAYLOAD SET raw_request = CAST(:rawRequest as jsonb)  WHERE id = :id" ,nativeQuery = true)
	void updateRawRequest(@Param("rawRequest") String rawRequest, @Param("id") int id);
	 
}
