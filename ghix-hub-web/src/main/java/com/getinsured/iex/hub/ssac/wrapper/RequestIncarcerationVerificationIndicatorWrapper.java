package com.getinsured.iex.hub.ssac.wrapper;


import java.io.IOException;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.google.gson.Gson;

public class RequestIncarcerationVerificationIndicatorWrapper {
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	private static com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsdFactory;
	//private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	private  boolean incarcerationVerificationRequested;
	private Boolean requestIncarcerationVerificationIndicator;
	
	public RequestIncarcerationVerificationIndicatorWrapper() {
		xsdFactory = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
		//extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public void setIncarcerationVerificationRequested(boolean isVerifyFlag) {
		this.incarcerationVerificationRequested = isVerifyFlag;
		this.requestIncarcerationVerificationIndicator= xsdFactory.createBoolean();
		this.requestIncarcerationVerificationIndicator.setValue(isVerifyFlag);
	}
	
	public boolean isIncarcerationVerificationRequested() {
		return this.incarcerationVerificationRequested;
	}
	
	public Boolean getSystemRepresentation(){
		return this.requestIncarcerationVerificationIndicator;
	}
	
	public static RequestIncarcerationVerificationIndicatorWrapper fromJsonString(String jsonString)
		throws IOException {
		return gson.fromJson(jsonString, RequestIncarcerationVerificationIndicatorWrapper.class);
	}
	
	/*public static void main(String[] args) throws JAXBException, IOException {
		RequestIncarcerationVerificationIndicatorWrapper wrapper = fromJsonString("{\"incarcerationVerificationRequested\":\"false\"}");
		SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
		req.setRequestCitizenshipVerificationIndicator(wrapper.requestIncarcerationVerificationIndicator);
		JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ssaCompReq, System.out);
	}*/
}
