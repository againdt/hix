(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .directive('yearSelect', yearSelect);

    function yearSelect(){

        return {
            restrict: 'E',
            replace: true,
            scope: '=',
            template: '<select id="yearSelect" class="input-full-width"><option ng-repeat="year in years" value="{{year}}">{{year}}</option></select>',
            // template: '<select ng-options="month.value for month in months"></select>',
            controller: ["$scope", "$element", "$attrs", function (scope, element, attrs) {
            }]
        }
    }
})();