(function (views) {
	views.CompareView = Backbone.View.extend({
		
		events : {
			//'click .comparePlans' : 'comparePlans',
			'click .back-to-all-plans-link-compare' : 'backToAll'
		},
		el : '.back-to-all-plans-link-container',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','comparePlans','addPrevYearPlanToCompare');
			var self = this;
			
			//self.tmpl = _.template($('#tmpCompare').html());
			//var html = this.tmpl(self.collection.info());
			//$(this.el).html(html);
			
			//self.collection.bind('reset', self.addAll);
			//self.collection.bind('reset', this.render);
		},
		addAll : function () {
			var self = this;
			$('#mainSummaryCmpHead').show();
			$('#benefitHead').show();
			
			$('#mainSummary').empty();
			$('#detailHead').hide();
			
			var planType = self.collection.models[0].get("planType");
			if(planType =="DENTAL"){
				self.removeDentalCompare();
				//$('.back-to-all-plans-link-container').append($('#tmpCompare').html());
				self.collection.each (self.dentalAddOne);
			}else{
				self.removeCompare();
				//$('.back-to-all-plans-link-container').append($('#tmpCompare').html());
				self.collection.each (self.addOne);
			}			
			
			$('.addToCart').bind('click', self.addToCart);
			$('.removeCompare').bind('click', self.removeCompare);
		},
		
		addCompareAll : function () {
 	 	 	var self = this;
 	 	 	//Empty compare widget box
 	 	 	$('#compareBox').empty();
 	 	 	
 	 	 	var cmpPlanLenght = self.collection.compareModels.length;
 	 	 	if(cmpPlanLenght == 0){
 	 	 		$('.js-plan-compare-widget').hide();
 	 	 	}
 	 	 	
 	 	 	self.collection.comparePager();
 	 	 	var sortCol = new Array();
 	 	 	sortCol.push('planId');
            var planModels = self.collection._sortBy(self.collection.drawermodels, sortCol);

 	 	    var planZeroId = -1;  
 	 	 	_.each(self.collection.orgmodels, function(planModel){ 
 	 	 		if(0 == planModel.get("planId")){ 
 	 	 		planZeroId = 0;
 	 	 		} 
 	 	 		}); 


 	 	 	var emptyContainerHtml = '<div class="cp-compare-plans__plan cp-compare-plans__plan--empty">' + plan_display['pd.label.title.addPlan'] + '</div>';
			var linkContainerHtmlForLastYearPlan = '<div class="cp-compare-plans__plan cp-compare-plans__plan--empty"><a href="javascript:void(0);" class="addPrevYrPlan">' + plan_display['pd.label.title.comparePrevPlan'] + '</a></div>';
 	 	 	if(cmpPlanLenght == 1){                         
 	 	 		_.each(planModels, self.addComparePlan);
 	 	 		if(planZeroId == 0 && self.collection.compareModels[0] != 0) {
					$('#compareBox').append(emptyContainerHtml + linkContainerHtmlForLastYearPlan);
				} else {
	 	 	 		$('#compareBox').append(emptyContainerHtml + emptyContainerHtml);
				}
 	 	 	}
 	 	 	if(cmpPlanLenght == 2){
 	 	 	
 	 	 		_.each(planModels, self.addComparePlan);
 	 	 		if(planZeroId == 0  && self.collection.compareModels[0] != 0  && self.collection.compareModels[1] != 0) {
					$('#compareBox').append(linkContainerHtmlForLastYearPlan);
				} else {
	 	 	 		$('#compareBox').append(emptyContainerHtml);
				}
 	 	 	}
 	 	 	if(cmpPlanLenght == 3){
 	 	 	
 	 	 		_.each(planModels, self.addComparePlan);
 	 	 	}

 	 	 	
 	 	 	var csr = $('#csr').val();
			if(csr === 'CS0' || csr === 'CS1'|| csr === ''){
				$('.ps-non-csr-show').css('display', 'inline-block');
			}else{
				$('.ps-csr-show').css('display', 'inline-block');
			}
 	 	 },
 	 	 addComparePlan  : function (model){
 	 	 	var self = this;
 	 	 	var planType = model.get("planType");
			var compareSummary;
			if (planType == 'DENTAL'){
				compareSummary = new DentalCompareSummary({model:model});
			}else{
				compareSummary = new CompareSummary({model:model});
			}
 	 	 	
 	 	 	//Compare append the children
 	 	 	$('#compareBox').append(compareSummary.getCompareBoxData());
 	 	 	 	 	 	
	 	},

		addPrevYearPlanToCompare: function() {
            this.collection.compareModels.push("0");
			$('#compareCount').html(this.collection.compareModels.length);
			this.addCompareAll();
        },

		addOne : function (model,index) {
			var self = this;

			var PERSON_COUNT = $('#PERSON_COUNT').val();
			model.set({"PERSON_COUNT": PERSON_COUNT});

			var PLAN_TYPE = $('#PLAN_TYPE').val();
			model.set({"PLAN_TYPE": PLAN_TYPE});
			
			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			model.set({"ISSUBSIDIZED":ISSUBSIDIZED});
			
			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});
			
			var elegibilityComplete = $('#elegibilityComplete').val();
			model.set({"elegibilityComplete":elegibilityComplete});
			
			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});
			
			var compareSummary = new CompareSummary({model:model});
			$('#mainSummaryCmp').append(compareSummary.getMainSummaryCmp());
			$('#smartScoreRow').append(compareSummary.getSmartScore());
			$('#estimatedCost').append(compareSummary.getEstimatedCost());
			$('#providerSearch').append(compareSummary.getProviderSearch());
			$('#productTypeCompare').append(compareSummary.getProductType());
			$('#hsaType').append(compareSummary.getHsaType()); 
			$('#discounts').append(compareSummary.getDiscount());
			$('#qualityRating').append(compareSummary.getQualityRating()); 
			$('#networkTransparencyRating').append(compareSummary.getNetworkTransparencyRating());
			if(index==0){
				$('#providerNames').html(compareSummary.getProviderNamesCompare());
				if($(".drugName").length == 0) {
					$('#summaryCompare').append(compareSummary.getDrugNamesCompare());
				}
			}
			
			$('#havingBabyCompare').append(compareSummary.getHavingBabySBC());
			$('#havingDiabetesCompare').append(compareSummary.getHavingDiabetesSBC());
			$('#treatmentFractureCompare').append(compareSummary.getHavingFractureSBC());
			
			compareSummary.getProviderSearchInfoTplCompare();
			compareSummary.getDrugSearchInfoTplCompare();
			
			$('#planDeductible1').append(compareSummary.getDeductible1());
			$('#planDeductible2').append(compareSummary.getDeductible2());
			$('#planDeductible3').append(compareSummary.getDeductible3());
			$('#planDeductible4').append(compareSummary.getDeductible4());
			$('#planDeductible5').append(compareSummary.getDeductible5());
			$('#planDeductible6').append(compareSummary.getDeductible6());
			$('#planDeductible7').append(compareSummary.getDeductible7());
			$('#planDeductible8').append(compareSummary.getDeductible8());
			$('#planDeductible9').append(compareSummary.getDeductible9());
			$('#planDeductible10').append(compareSummary.getDeductible10());
			$('#planDeductible11').append(compareSummary.getDeductible11());
			$('#planDeductible12').append(compareSummary.getDeductible12());
			$('#planDeductibleOther').append(compareSummary.getOptionalDeductible());
			
			$('#doctorVisit1').append(compareSummary.getDoctorVisit1());
			$('#doctorVisit2').append(compareSummary.getDoctorVisit2());
			$('#doctorVisit3').append(compareSummary.getDoctorVisit3());
			$('#doctorVisit4').append(compareSummary.getDoctorVisit4());
			
			$('#test1').append(compareSummary.getTest1());
			$('#test2').append(compareSummary.getTest2());
			$('#test3').append(compareSummary.getTest3());
			
			$('#drug1').append(compareSummary.getDrug1());
			$('#drug2').append(compareSummary.getDrug2());
			$('#drug3').append(compareSummary.getDrug3());
			$('#drug4').append(compareSummary.getDrug4());
			
			$('#outpatient1').append(compareSummary.getOutpatient1());
			$('#outpatient2').append(compareSummary.getOutpatient2());
			$('#outpatientServicesOfficeVisits').append(compareSummary.getOutpatientServicesOfficeVisits());
			
			$('#urgent1').append(compareSummary.getUrgent1());
			$('#urgent2').append(compareSummary.getUrgent2());
			$('#urgent3').append(compareSummary.getUrgent3());
			$('#urgentProfessionalFee').append(compareSummary.getUrgentProfessionalFee());
			
			$('#hospital1').append(compareSummary.getHospital1());
			$('#hospital2').append(compareSummary.getHospital2());
			
			$('#mentalHealth1').append(compareSummary.getMentalhealth1());
			$('#mentalHealth2').append(compareSummary.getMentalhealth2());
			$('#mentalHealthInpatientProfFee').append(compareSummary.getMentalHealthInpatientProfFee());
			$('#mentalHealth3').append(compareSummary.getMentalhealth3());
			$('#mentalHealth4').append(compareSummary.getMentalhealth4());
			$('#mentalHealthSubDisorderInpProfFee').append(compareSummary.getMentalhealthSubDisorderInpProfFee());
			
			$('#pregnancy1').append(compareSummary.getPregnancy1());
			$('#pregnancy2').append(compareSummary.getPregnancy2());
			$('#pregnancyInpatientProfFee').append(compareSummary.getPregnancyInpatientProfFee());
			
			$('#specialNeed1').append(compareSummary.getSpecialNeed1());
			$('#specialNeed2').append(compareSummary.getSpecialNeed2());
			$('#specialNeed3').append(compareSummary.getSpecialNeed3());
			$('#specialNeed4').append(compareSummary.getSpecialNeed4());
			$('#specialNeed5').append(compareSummary.getSpecialNeed5());
			$('#specialNeed6').append(compareSummary.getSpecialNeed6());
			$('#specialNeed7').append(compareSummary.getSpecialNeed7());
			$('#specialNeed8').append(compareSummary.getSpecialNeed8());
			$('#specialNeed9').append(compareSummary.getSpecialNeed9());
			$('#specialNeed10').append(compareSummary.getSpecialNeed10());
			$('#specialNeed11').append(compareSummary.getSpecialNeed11());
			$('#specialNeed12').append(compareSummary.getSpecialNeed12());
			$('#specialNeed13').append(compareSummary.getSpecialNeed13());
			$('#specialNeed14').append(compareSummary.getSpecialNeed14());
			
			$('#childrensVision1').append(compareSummary.getChildrensVision1());
			$('#childrensVision2').append(compareSummary.getChildrensVision2());
			$('#hearingAids').append(compareSummary.getHearingAids());
			
			$('#childrensDental1').append(compareSummary.getChildrensDental1());
			$('#childrensDental2').append(compareSummary.getChildrensDental2());
			$('#childrensDental3').append(compareSummary.getChildrensDental3());
			$('#childrensDental4').append(compareSummary.getChildrensDental4());
			$('#childrensDental5').append(compareSummary.getChildrensDental5());
			$('#childrensDental6').append(compareSummary.getChildrensDental6());
			$('#childrensDental7').append(compareSummary.getChildrensDental7());
			$('#childrensDental8').append(compareSummary.getChildrensDental8());
			$('#childrensDental9').append(compareSummary.getChildrensDental9());
			
			$('#simplifiedDeductible').append(compareSummary.getSimplifiedDeductible());
			$('#simplifiedSeparateDrugDeductible').append(compareSummary.getSimplifiedSeparateDrugDeductible());
			$('#simplifiedOOPMax').append(compareSummary.getSimplifiedOOPMax());
			$('#simplifiedOtherDeductible').append(compareSummary.getSimplifiedOtherDeductible());
			$('#simplifiedMaxCostPerPrescription').append(compareSummary.getSimplifiedMaxCostPerPrescription());
			$('#providerHeatMap').append(compareSummary.getProviderHeatMapCmpTpl());
			if($('#providerMapConfig').val() == 'ON'){
			providerDensity(model.toJSON().networkKey,model.toJSON().planId);
			}
		},
		
		dentalAddOne : function (model) {
			var self = this;

			var PERSON_COUNT = $('#PERSON_COUNT').val();
			model.set({"PERSON_COUNT": PERSON_COUNT});

			var PLAN_TYPE = $('#PLAN_TYPE').val();
			model.set({"PLAN_TYPE": PLAN_TYPE});
			
			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			model.set({"ISSUBSIDIZED":ISSUBSIDIZED});
			
			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});
			
			var elegibilityComplete = $('#elegibilityComplete').val();
			model.set({"elegibilityComplete":elegibilityComplete});
			
			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});
			
			var compareSummary = new DentalCompareSummary({model:model});
			$('#mainSummaryCmp').append(compareSummary.getMainSummaryCmp());
			$('#smartScoreRow').append(compareSummary.getSmartScore());
			$('#planType').append(compareSummary.getPlanType());
			$('#planTier').append(compareSummary.getPlanTier()); 
			$('#premiumType').append(compareSummary.getPremiumType());
			$('#planDeductibleFly').append(compareSummary.getDeductibleFly());
			$('#planDeductibleInd').append(compareSummary.getDeductibleInd()); 
			$('#planOopMaxFly').append(compareSummary.getOopMaxFly());
			$('#planOopMaxInd').append(compareSummary.getOopMaxInd());
			$('#planAdultDeductibleFly').append(compareSummary.getPlanAdultDeductibleFly());
			$('#planAdultAnnualBenefitLimitFly').append(compareSummary.getPlanAdultAnnualBenefitLimitFly());
			$('#planAdultOutOfPocketMaxFly').append(compareSummary.getPlanAdultOutOfPocketMaxFly());
			$('#planAdultDeductibleInd').append(compareSummary.getPlanAdultDeductibleInd());
			$('#planAdultAnnualBenefitLimitInd').append(compareSummary.getPlanAdultAnnualBenefitLimitInd());
			$('#planAdultOutOfPocketMaxInd').append(compareSummary.getPlanAdultOutOfPocketMaxInd());
			$('#majorDentalCareAdult').append(compareSummary.getMajorDentalCareAdult());
			$('#orthodontiaAdult').append(compareSummary.getOrthodontiaAdult());
			$('#accidentalDental').append(compareSummary.getAccidentalDental());
			$('#basicDentalCareAdult').append(compareSummary.getBasicDentalCareAdult());
			$('#rtnDentalAdult').append(compareSummary.getRtnDentalAdult());
			$('#majorDentalCareChild').append(compareSummary.getMajorDentalCareChild());
			$('#orthodontiaChild').append(compareSummary.getOrthodontiaChild());
			$('#dentalCheckupChildren').append(compareSummary.getDentalCheckupChildren());
			$('#basicDentalCareChild').append(compareSummary.getBasicDentalCareChild());
			
		},

		comparePlans: function () {
		  /*compare view should have at least 2 plans to compare*/
		  if(this.collection.compareModels.length > 0){
			this.collection.page=1;
			this.collection.resetFieldFilter();
			this.collection.showCompare=true;
			this.collection.pager();
			$('#view').val('compare');
			$('#simplifiedSeparateDrugDeductible').addClass('hide');
			this.addAll();
			this.collection.updateCompareSummary();	
			this.collection.preserveOtherOptions();
			$('.js-plan-compare-widget').hide();
			$('.choose-plan').hide();
			$('.detail-plan').hide();
			$('.comparing-plans').show();
			
			this.animateToTop();
			
		  }else if(this.collection.compareModels.length == 0 && this.collection.isBackToAll == true){ 
				$('#mainSummaryCmpHead').hide();
				$('#benefitHead').hide();
				this.collection.isBackToAll = false;
				this.collection.resetFieldFilter();
				this.collection.loadIssuers();
				this.collection.preserveOtherOptions();
			}
		  else { 
			$('#atleast-one-plan').modal('show');
			return false;
		    //alert('Please add at least one plan to compare');
		  }
		},
		
		animateToTop: function(){
			$('body, html').animate({
				scrollTop: 0
			},600);
		},
		
		compareBox: function (ele) {
		 	/*compare view should have at least 2 plans to compare*/
		 	//this.collection.showCompare == false;
		 	this.addCompareAll();
	 	},
		
		addToCart: function (e) {
			e.target.pageName= plan_display['pd.label.title.compare'];
			App.views.plans.addToCart(e);
		},
		
		backToAll : function(){
		  this.removeCompare();
//		  $('#mainSummaryCmpHead').hide();
		  $('#benefitHead').hide();
//		  $('#sort').show();
		  this.collection.showCompare=false;
		  this.collection.resetFieldFilter();
		  this.collection.loadIssuers();
		  this.collection.preserveOtherOptions();
		  
		  //$('.js-plan-compare-widget').show();
		  App.views.plans.filter();
		  location.hash = "";
		  //$('.choose-plan').show();
		  
		  //this.addingStyleToMiddleTile();
		},
		
		addingStyleToMiddleTile: function(){
        	/*
        		Clear the tiles after every 3rd Tile and add after new clearfix element to clear the float layout
        		and css class to every second tile:
        	*/
			$('#mainSummary').find('.jsTilesClearFix').remove();
			$('#mainSummary .tile').css({marginLeft: 0, marginRight:0}); //Required to overwrite .tile:nth-child(3n-1) from css as it is using on other pages
			$('#mainSummary .tile:nth-of-type(2n)').removeClass('middleTile'); //First remove clearfix and middleTile class as event fires twice so needed these code
        	$('#mainSummary .tile:nth-of-type(3n)').after('<div class="clearfix jsTilesClearFix"></div>');
        	$('#mainSummary .tile:nth-of-type(2n)').addClass('middleTile');
		},
		
		removeCompare : function(e){
			$('#compare').html("");
			//$('#mainSummaryCmp').find('.plan-details').remove();
			$('#mainSummaryCmp').find('.ps-compare__plan-tile').remove()
			$('#smartScoreRow').find('.plan-details').remove();
			$('#estimatedCost').find('.plan-details').remove();
			$('#providers').find('.plan-details').remove();
			$('#providerSearch').find('.plan-details').remove();
			$('#productTypeCompare').find('.plan-details').remove();
			$('#hsaType').find('.plan-details').remove();
			$('#discounts').find('.plan-details').remove();
			$('#qualityRating').find('.plan-details').remove();
			$('#networkTransparencyRating').find('.plan-details').remove();
			
			$('#havingBabyCompare').find('.plan-details').remove();
			$('#havingDiabetesCompare').find('.plan-details').remove();
			$('#treatmentFractureCompare').find('.plan-details').remove();
			
			$('#planDeductible1').find('.plan-details').remove();
			$('#planDeductible2').find('.plan-details').remove();
			$('#planDeductible3').find('.plan-details').remove();
			$('#planDeductible4').find('.plan-details').remove();
			$('#planDeductible5').find('.plan-details').remove();
			$('#planDeductible6').find('.plan-details').remove();
			$('#planDeductible7').find('.plan-details').remove();
			$('#planDeductible8').find('.plan-details').remove();
			$('#planDeductible9').find('.plan-details').remove();
			$('#planDeductible10').find('.plan-details').remove();
			$('#planDeductible11').find('.plan-details').remove();
			$('#planDeductible12').find('.plan-details').remove();
			$('#planDeductibleOther').find('.plan-details').remove();
			
			$('#doctorVisit1').find('.plan-details').remove();
			$('#doctorVisit2').find('.plan-details').remove();
			$('#doctorVisit3').find('.plan-details').remove();
			$('#doctorVisit4').find('.plan-details').remove();
			
			$('#test1').find('.plan-details').remove();
			$('#test2').find('.plan-details').remove();
			$('#test3').find('.plan-details').remove();
			
			$('#drug1').find('.plan-details').remove();
			$('#drug2').find('.plan-details').remove();
			$('#drug3').find('.plan-details').remove();
			$('#drug4').find('.plan-details').remove();
			
			$('#outpatient1').find('.plan-details').remove();
			$('#outpatient2').find('.plan-details').remove();
			$('#outpatientServicesOfficeVisits').find('.plan-details').remove();
			
			$('#urgent1').find('.plan-details').remove();
			$('#urgent2').find('.plan-details').remove();
			$('#urgent3').find('.plan-details').remove();
			$('#urgentProfessionalFee').find('.plan-details').remove();
			
			$('#hospital1').find('.plan-details').remove();
			$('#hospital2').find('.plan-details').remove();
			
			$('#mentalHealth1').find('.plan-details').remove();
			$('#mentalHealth2').find('.plan-details').remove();
			$('#mentalHealthInpatientProfFee').find('.plan-details').remove();
			$('#mentalHealth3').find('.plan-details').remove();
			$('#mentalHealth4').find('.plan-details').remove();
			$('#mentalHealthSubDisorderInpProfFee').find('.plan-details').remove();
			
			$('#pregnancy1').find('.plan-details').remove();
			$('#pregnancy2').find('.plan-details').remove();
			$('#pregnancyInpatientProfFee').find('.plan-details').remove();
			
			$('#specialNeed1').find('.plan-details').remove();
			$('#specialNeed2').find('.plan-details').remove();
			$('#specialNeed3').find('.plan-details').remove();
			$('#specialNeed4').find('.plan-details').remove();
			$('#specialNeed5').find('.plan-details').remove();
			$('#specialNeed6').find('.plan-details').remove();
			$('#specialNeed7').find('.plan-details').remove();
			$('#specialNeed8').find('.plan-details').remove();
			$('#specialNeed9').find('.plan-details').remove();
			$('#specialNeed10').find('.plan-details').remove();
			$('#specialNeed11').find('.plan-details').remove();
			$('#specialNeed12').find('.plan-details').remove();
			$('#specialNeed13').find('.plan-details').remove();
			$('#specialNeed14').find('.plan-details').remove();
			
			$('#childrensVision1').find('.plan-details').remove();
			$('#childrensVision2').find('.plan-details').remove();
			$('#hearingAids').find('.plan-details').remove();
			
			$('#childrensDental1').find('.plan-details').remove();
			$('#childrensDental2').find('.plan-details').remove();
			$('#childrensDental3').find('.plan-details').remove();
			$('#childrensDental4').find('.plan-details').remove();
			$('#childrensDental5').find('.plan-details').remove();
			$('#childrensDental6').find('.plan-details').remove();
			$('#childrensDental7').find('.plan-details').remove();
			$('#childrensDental8').find('.plan-details').remove();
			$('#childrensDental9').find('.plan-details').remove();
			
			$('#simplifiedDeductible').find('.plan-details').remove();
			$('#simplifiedSeparateDrugDeductible').find('.plan-details').remove();
			$('#simplifiedOOPMax').find('.plan-details').remove();
			$('#simplifiedOtherDeductible').find('.plan-details').remove();
			$('#simplifiedMaxCostPerPrescription').find('.plan-details').remove();
			$('#collapseDoctorsFacilities .removeProvidersInfo').remove();
			$('.removeDrugsInfo').remove();
			$('#providerHeatMap').find('.plan-details').remove();
			
			if(e !== null && e !== undefined)
			{
				
				App.views.plans.removeCompare(e);
				App.views.plans.collection.isBackToAll = true;
				App.views.compareView.comparePlans();
			}
		},
		
		removeDentalCompare : function(){
			$('#compare').html("");
			//$('#mainSummaryCmp').find('.plan-details').remove();
			$('#mainSummaryCmp').find('.ps-compare__plan-tile').remove()
			$('#smartScoreRow').find('.plan-details').remove();
			$('#planType').find('.plan-details').remove();
			$('#planTier').find('.plan-details').remove();
			$('#premiumType').find('.plan-details').remove();
			$('#planDeductibleFly').find('.plan-details').remove();
			$('#planDeductibleInd').find('.plan-details').remove();
			$('#planOopMaxFly').find('.plan-details').remove();
			$('#planOopMaxInd').find('.plan-details').remove();
			$('#planAdultDeductibleFly').find('.plan-details').remove();
			$('#planAdultAnnualBenefitLimitFly').find('.plan-details').remove();
			$('#planAdultOutOfPocketMaxFly').find('.plan-details').remove();
			$('#planAdultDeductibleInd').find('.plan-details').remove();
			$('#planAdultAnnualBenefitLimitInd').find('.plan-details').remove();
			$('#planAdultOutOfPocketMaxInd').find('.plan-details').remove();
			$('#planDeductibleOther').find('.plan-details').remove();
			$('#orthodontiaAdult').find('.plan-details').remove();
			$('#majorDentalCareAdult').find('.plan-details').remove();
			$('#accidentalDental').find('.plan-details').remove();
			$('#basicDentalCareAdult').find('.plan-details').remove();
			$('#rtnDentalAdult').find('.plan-details').remove();
			$('#orthodontiaChild').find('.plan-details').remove();
			$('#majorDentalCareChild').find('.plan-details').remove();	
			$('#dentalCheckupChildren').find('.plan-details').remove();	
			$('#basicDentalCareChild').find('.plan-details').remove();			
						
		}
	});
	
	if($('#insuranceType').val() == 'DENTAL'){				
		var DentalCompareSummary = Backbone.View.extend({
			template: _.template($('#tmpCompare').html()),
			compareBoxTpl: _.template($('#compareBoxTemplate').html()),
			mainSummaryCmpTpl: _.template($('#mainSummaryCmpTpl').html()),
			smartScoreTpl: _.template($('#smartScoreTpl').html()),
			planTypeTpl: _.template($('#planTypeTpl').html()),
			planTierTpl: _.template($('#planTierTpl').html()),
			premiumTypeTpl: _.template($('#premiumTypeTpl').html()),
			deductibleIndTpl: _.template($('#deductibleIndTpl').html()),
			deductibleFlyTpl: _.template($('#deductibleFlyTpl').html()),
			benefitTpl: _.template($('#benefitTpl').html()),
						
			initialize: function() {
				this.model.bind('change', this.render, this);
				this.model.bind('destroy', this.remove, this);
			},
	
			render : function () {
				//$(this.el).html(this.template(this.model.toJSON()));
				return this;
			},
			
			getCompareBoxData : function () { return this.compareBoxTpl(this.model.toJSON()); },
			getMainSummaryCmp : function () { return this.mainSummaryCmpTpl(this.model.toJSON()); },
			getSmartScore : function () { return this.smartScoreTpl({SMART_SCORE : this.model.toJSON().smartScore});},
			getPlanType : function () {return this.planTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
			getPlanTier : function () {return this.planTierTpl({LEVEL :  this.model.toJSON().level});},
			getPremiumType: function () {return this.premiumTypeTpl({DENTAL_GURANTEE :  this.model.toJSON().dentalGuarantee});},
			getDeductibleInd : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductibleFly : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getOopMaxInd : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getOopMaxFly : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getPlanAdultDeductibleFly : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] ? this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] : null; 
				return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultDeductibleFly').hide() ;
				}},
			getPlanAdultAnnualBenefitLimitFly : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] ? this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] : null; 
				return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultAnnualBenefitLimitFly').hide() ;
				}},
			getPlanAdultOutOfPocketMaxFly : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] ? this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] : null; 
				return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultOutOfPocketMaxFly').hide() ;
				}},
			getPlanAdultDeductibleInd : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] ? this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] : null; 
				return this.deductibleIndTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultDeductibleInd').hide() ;
				}},
			getPlanAdultAnnualBenefitLimitInd : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] ? this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] : null; 
				return this.deductibleIndTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultAnnualBenefitLimitInd').hide() ;
				}},
			getPlanAdultOutOfPocketMaxInd : function () { 
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] ? this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] : null; 
				return this.deductibleIndTpl({DEDUCTIBLE : duductible}); 
				}else{
					$('#planAdultOutOfPocketMaxInd').hide() ;
				}},
			getAccidentalDental : function () { 
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL, EXPLANATION : this.model.toJSON().benefitExplanation.ACCIDENTAL_DENTAL, EXCLUSION : this.model.toJSON().netwkException.ACCIDENTAL_DENTAL}); 
				}else{
					$('#accidentalDental').hide() ;
				}},
			getBasicDentalCareAdult : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_ADULT, EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_ADULT}); },
			getBasicDentalCareChild : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_CHILD, EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_CHILD}); },
			getDentalCheckupChildren : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN, EXPLANATION : this.model.toJSON().benefitExplanation.DENTAL_CHECKUP_CHILDREN, EXCLUSION : this.model.toJSON().netwkException.DENTAL_CHECKUP_CHILDREN}); },
			getMajorDentalCareAdult : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_ADULT, EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_ADULT}); },
			getMajorDentalCareChild : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_CHILD, EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_CHILD}); },
			getOrthodontiaAdult : function () { 
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_ADULT, EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_ADULT}); 
				}else{
					$('#orthodontiaAdult').hide() ;
				}},
			getOrthodontiaChild : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_CHILD, EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_CHILD}); },
			getRtnDentalAdult : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.RTN_DENTAL_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.RTN_DENTAL_ADULT, EXCLUSION : this.model.toJSON().netwkException.RTN_DENTAL_ADULT}); }
					
		});
	 
		}else{		
		
		var CompareSummary = Backbone.View.extend({
			template: _.template($('#tmpCompare').html()),
			compareBoxTpl: _.template($('#compareBoxTemplate').html()),
			mainSummaryCmpTpl: _.template($('#mainSummaryCmpTpl').html()),
			smartScoreTpl: _.template($('#smartScoreTpl').html()),
			estimatedCostTpl: _.template($('#estimatedCostTpl').html()),
			providerSearchTpl: _.template($('#providerSearchLinkTpl').html()),
			productTypeTpl: _.template($('#productTypeTpl').html()),
			hsaTypeCompareTpl: _.template($('#hsaTypeCompareTpl').html()),
			qualityRatingTpl: _.template($('#qualityRatingTpl').html()),
			discountTpl: _.template($('#discountTpl').html()),
			deductibleIndTpl: _.template($('#deductibleIndTpl').html()),
			deductibleFlyTpl: _.template($('#deductibleFlyTpl').html()),
			benefitTpl: _.template($('#benefitTpl').html()),
			deductibleTpl: _.template($('#deductibleTpl').html()),
			simplifiedDeductibleTpl: _.template($('#simplifiedDeductibleTpl').html()),
			simplifiedOtherDeductibleTpl: _.template($('#simplifiedOtherDeductibleTpl').html()),
			maxCoinseForSpecialtyDrugsTpl: _.template($('#maxCoinseForSpecialtyDrugsTpl').html()),
			providerNamesTpl: _.template($('#providerNamesTpl').html()),
			providerSearchInfoTplCompareTpl: _.template($('#providerSearchInfoTplCompare').html()),	
			drugNamesTpl: _.template($('#drugNamesTpl').html()),
			drugSearchInfoTplCompareTpl: _.template($('#drugSearchInfoTplCompare').html()),	
			networkTransparencyCompareTpl: _.template($('#networkTransparencyCompareTpl').html()),
			providerHeatMapCmpTpl: _.template($('#providerHeatMapCompareTpl').html()),
			havingBabyTpl: _.template($('#havingBabyTpl').html()),
			havingDiabetesTpl: _.template($('#havingDiabetesTpl').html()),
			treamentFractureTpl: _.template($('#treamentFractureTpl').html()),
			
			initialize: function() {
				this.model.bind('change', this.render, this);
				this.model.bind('destroy', this.remove, this);
			},
	
			render : function () {
				//$(this.el).html(this.template(this.model.toJSON()));
				return this;
			},
	
			getCompareBoxData : function () { return this.compareBoxTpl(this.model.toJSON()); },
			getMainSummaryCmp : function () { return this.mainSummaryCmpTpl(this.model.toJSON()); },
			getSmartScore : function () { return this.smartScoreTpl({SMART_SCORE : this.model.toJSON().smartScore});},
			getEstimatedCost : function () {
				if($('#stateCode').val() != 'ID'){
					return this.estimatedCostTpl({EXPENSE_ESTIMATE :  this.model.toJSON().estimatedTotalHealthCareCost, ANNUAL_PREMIUM : this.model.toJSON().annualPremiumAfterCredit});
				} else {
					return this.estimatedCostTpl({EXPENSE_ESTIMATE :  this.model.toJSON().expenseEstimate, ANNUAL_PREMIUM : this.model.toJSON().annualPremiumAfterCredit});
				}
			},
			getProviderSearch : function () {return this.providerSearchTpl({PROVIDER_LINK :  this.model.toJSON().providerLink});},
			getProductType : function () {return this.productTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
			getHsaType : function () {return this.hsaTypeCompareTpl({HSA :  this.model.toJSON().hsa});},
			getDiscount: function () {return this.discountTpl({LEVEL :  this.model.toJSON().level});},
			getQualityRating : function () {return this.qualityRatingTpl({issuerQualityRating1 :  this.model.toJSON().issuerQualityRating});},
			getNetworkTransparencyRating : function () {return this.networkTransparencyCompareTpl({networkTransparencyRating :  this.model.toJSON().networkTransparencyRating });},
			
			getHavingBabySBC : function () { return this.havingBabyTpl({
				TOTAL_COST : this.model.toJSON().havingBabySBC, 
			}); },
			getHavingDiabetesSBC : function () { return this.havingDiabetesTpl({
				TOTAL_COST : this.model.toJSON().havingDiabetesSBC, 
			}); },
			getHavingFractureSBC : function () { return this.treamentFractureTpl({
				TOTAL_COST : this.model.toJSON().simpleFractureSBC, 
			}); },
			
			getDeductible1 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible2 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible3 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible4 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible5 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible6 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible7 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible8 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible9 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible10 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible11 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible12 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getOptionalDeductible : function () { return this.deductibleTpl({DEDUCTIBLES : this.model.toJSON().optionalDeductible}); },
	
			
			getSimplifiedDeductible: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				if(PLAN_COSTS.DEDUCTIBLE_INTG_MED_DRUG != null){
					deductible =  PLAN_COSTS.DEDUCTIBLE_INTG_MED_DRUG;
				} else if(PLAN_COSTS.DEDUCTIBLE_MEDICAL != null){
					deductible =  PLAN_COSTS.DEDUCTIBLE_MEDICAL;
				} 
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible});
			},
			
			getSimplifiedSeparateDrugDeductible: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				
				if(PLAN_COSTS.DEDUCTIBLE_DRUG != null || $('#stateCode').val() === 'CT'){
					deductible =  PLAN_COSTS.DEDUCTIBLE_DRUG;
					$('#simplifiedSeparateDrugDeductible').removeClass('hide');
				}
				
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible});
			},
			
			getSimplifiedOOPMax: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				if(PLAN_COSTS.MAX_OOP_INTG_MED_DRUG != null){
					deductible =  PLAN_COSTS.MAX_OOP_INTG_MED_DRUG;
				} else if(PLAN_COSTS.MAX_OOP_MEDICAL != null){
					deductible =  PLAN_COSTS.MAX_OOP_MEDICAL;
				} 
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible});
			},
			
			getSimplifiedOtherDeductible: function(){return this.simplifiedOtherDeductibleTpl({DEDUCTIBLES : this.model.toJSON().optionalDeductible});},
			
			getSimplifiedMaxCostPerPrescription : function () {return this.maxCoinseForSpecialtyDrugsTpl({MAX_COINSE_FOR_SPECIALTY_DRUGS :  this.model.toJSON().maxCoinseForSpecialtyDrugs});},
			
			getProviderNamesCompare: function(){
				return this.providerNamesTpl({doctors : this.model.toJSON().doctors });
			},
			getDrugNamesCompare: function(){
				return this.drugNamesTpl({prescriptionResponseList : this.model.toJSON().prescriptionResponseList });
			},
			
			getProviderHeatMapCmpTpl : function () { 
				return this.providerHeatMapCmpTpl({PROVIDER_NETWORK_KEY : this.model.toJSON().networkKey, PLAN_ID : this.model.toJSON().planId});
			},
			
			getProviderSearchInfoTplCompare : function () { return this.providerSearchInfoTplCompareTpl(this.model.toJSON()); },
			getDrugSearchInfoTplCompare : function () { return this.drugSearchInfoTplCompareTpl(this.model.toJSON()); },
			
			getDoctorVisit1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PRIMARY_VISIT, EXPLANATION : this.model.toJSON().benefitExplanation.PRIMARY_VISIT, EXCLUSION : this.model.toJSON().netwkException.PRIMARY_VISIT, BENEFIT_NAME: "PRIMARY_VISIT", PrimaryCareCostSharingAfterSetNumberVisits: this.model.toJSON().primaryCareCostSharingAfterSetNumberVisits, PrimaryCareDeductOrCoinsAfterSetNumberCopays: this.model.toJSON().primaryCareDeductOrCoinsAfterSetNumberCopays}); },
			getDoctorVisit2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SPECIAL_VISIT, EXPLANATION : this.model.toJSON().benefitExplanation.SPECIAL_VISIT, EXCLUSION : this.model.toJSON().netwkException.SPECIAL_VISIT}); },
			getDoctorVisit3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OTHER_PRACTITIONER_VISIT, EXPLANATION : this.model.toJSON().benefitExplanation.OTHER_PRACTITIONER_VISIT, EXCLUSION : this.model.toJSON().netwkException.OTHER_PRACTITIONER_VISIT}); },
			getDoctorVisit4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PREVENT_SCREEN_IMMU, EXPLANATION : this.model.toJSON().benefitExplanation.PREVENT_SCREEN_IMMU, EXCLUSION : this.model.toJSON().netwkException.PREVENT_SCREEN_IMMU}); },
			
			getTest1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.LABORATORY_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.LABORATORY_SERVICES, EXCLUSION : this.model.toJSON().netwkException.LABORATORY_SERVICES}); },
			getTest2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.IMAGING_XRAY, EXPLANATION : this.model.toJSON().benefitExplanation.IMAGING_XRAY, EXCLUSION : this.model.toJSON().netwkException.IMAGING_XRAY}); },
			getTest3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.IMAGING_SCAN, EXPLANATION : this.model.toJSON().benefitExplanation.IMAGING_SCAN, EXCLUSION : this.model.toJSON().netwkException.IMAGING_SCAN}); },
			
			getDrug1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.GENERIC, EXPLANATION : this.model.toJSON().benefitExplanation.GENERIC, EXCLUSION : this.model.toJSON().netwkException.GENERIC}); },
			getDrug2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PREFERRED_BRAND, EXPLANATION : this.model.toJSON().benefitExplanation.PREFERRED_BRAND, EXCLUSION : this.model.toJSON().netwkException.PREFERRED_BRAND}); },
			getDrug3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.NON_PREFERRED_BRAND, EXPLANATION : this.model.toJSON().benefitExplanation.NON_PREFERRED_BRAND, EXCLUSION : this.model.toJSON().netwkException.NON_PREFERRED_BRAND}); },
			getDrug4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SPECIALTY_DRUGS, EXPLANATION : this.model.toJSON().benefitExplanation.SPECIALTY_DRUGS, EXCLUSION : this.model.toJSON().netwkException.SPECIALTY_DRUGS}); },
			
			getOutpatient1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_FACILITY_FEE, EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_FACILITY_FEE, EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_FACILITY_FEE}); },
			getOutpatient2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_SURGERY_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_SURGERY_SERVICES, EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_SURGERY_SERVICES}); },
			getOutpatientServicesOfficeVisits : function () { 
				if($('#stateCode').val() != 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_SERVICES_OFFICE_VISIT, EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_SERVICES_OFFICE_VISIT, EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_SERVICES_OFFICE_VISIT});}
				else{
					$('#outpatientServicesOfficeVisits').hide() ;
				} },
			
			getUrgent1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.EMERGENCY_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_SERVICES, EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_SERVICES}); },
			getUrgent2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.EMERGENCY_TRANSPORTATION, EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_TRANSPORTATION, EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_TRANSPORTATION}); },
			getUrgent3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.URGENT_CARE, EXPLANATION : this.model.toJSON().benefitExplanation.URGENT_CARE, EXCLUSION : this.model.toJSON().netwkException.URGENT_CARE}); },
			getUrgentProfessionalFee : function () { 
				if($('#stateCode').val() != 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.EMERGENCY_ROOM_PROFESSIONAL_FEE, EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_ROOM_PROFESSIONAL_FEE, EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_ROOM_PROFESSIONAL_FEE});}
				else{
					$('#urgentProfessionalFee').hide() ;
				} },
				
			getHospital1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_HOSPITAL_SERVICE, EXPLANATION : this.model.toJSON().benefitExplanation.INPATIENT_HOSPITAL_SERVICE, EXCLUSION : this.model.toJSON().netwkException.INPATIENT_HOSPITAL_SERVICE}); },
			getHospital2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_PHY_SURGICAL_SERVICE, EXPLANATION : this.model.toJSON().benefitExplanation.INPATIENT_PHY_SURGICAL_SERVICE, EXCLUSION : this.model.toJSON().netwkException.INPATIENT_PHY_SURGICAL_SERVICE}); },
			
			getMentalhealth1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MENTAL_HEALTH_OUT, EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_HEALTH_OUT, EXCLUSION : this.model.toJSON().netwkException.MENTAL_HEALTH_OUT}); },
			getMentalhealth2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MENTAL_HEALTH_IN, EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_HEALTH_IN, EXCLUSION : this.model.toJSON().netwkException.MENTAL_HEALTH_IN}); },
			getMentalHealthInpatientProfFee : function () { 
				if($('#stateCode').val() != 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE, EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE, EXCLUSION : this.model.toJSON().netwkException.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE});}
				else{
					$('#mentalHealthInpatientProfFee').hide() ;
				} },
			getMentalhealth3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SUBSTANCE_OUTPATIENT_USE, EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_OUTPATIENT_USE, EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_OUTPATIENT_USE}); },
			getMentalhealth4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SUBSTANCE_INPATIENT_USE, EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_INPATIENT_USE, EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_INPATIENT_USE}); },
			getMentalhealthSubDisorderInpProfFee : function () { 
				if($('#stateCode').val() != 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE, EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE, EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE});}
				else{
					$('#mentalHealthSubDisorderInpProfFee').hide() ;
				} },
				
			getPregnancy1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PRENATAL_POSTNATAL, EXPLANATION : this.model.toJSON().benefitExplanation.PRENATAL_POSTNATAL, EXCLUSION : this.model.toJSON().netwkException.PRENATAL_POSTNATAL}); },
			getPregnancy2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DELIVERY_IMP_MATERNITY_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.DELIVERY_IMP_MATERNITY_SERVICES, EXCLUSION : this.model.toJSON().netwkException.DELIVERY_IMP_MATERNITY_SERVICES}); },
			getPregnancyInpatientProfFee : function () { 
				if($('#stateCode').val() != 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE, EXPLANATION : this.model.toJSON().benefitExplanation.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE, EXCLUSION : this.model.toJSON().netwkException.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE});}
				else{
					$('#pregnancyInpatientProfFee').hide() ;
				} },
			
			getSpecialNeed1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HOME_HEALTH_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.HOME_HEALTH_SERVICES, EXCLUSION : this.model.toJSON().netwkException.HOME_HEALTH_SERVICES}); },
			getSpecialNeed2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_REHAB_SERVICES, EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_REHAB_SERVICES, EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_REHAB_SERVICES}); },
			getSpecialNeed3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HABILITATION, EXPLANATION : this.model.toJSON().benefitExplanation.HABILITATION, EXCLUSION : this.model.toJSON().netwkException.HABILITATION}); },
			getSpecialNeed4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SKILLED_NURSING_FACILITY, EXPLANATION : this.model.toJSON().benefitExplanation.SKILLED_NURSING_FACILITY, EXCLUSION : this.model.toJSON().netwkException.SKILLED_NURSING_FACILITY}); },
			getSpecialNeed5 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DURABLE_MEDICAL_EQUIP, EXPLANATION : this.model.toJSON().benefitExplanation.DURABLE_MEDICAL_EQUIP, EXCLUSION : this.model.toJSON().netwkException.DURABLE_MEDICAL_EQUIP}); },
			getSpecialNeed6 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HOSPICE_SERVICE, EXPLANATION : this.model.toJSON().benefitExplanation.HOSPICE_SERVICE, EXCLUSION : this.model.toJSON().netwkException.HOSPICE_SERVICE}); },
			getSpecialNeed7 : function () { 
				if($('#stateCode').val() == 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.CHIROPRACTIC, EXPLANATION : this.model.toJSON().benefitExplanation.CHIROPRACTIC, EXCLUSION : this.model.toJSON().netwkException.CHIROPRACTIC});
				}else{
					$('#specialNeed7').hide() ;
				} },
			getSpecialNeed8 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ACUPUNCTURE, EXPLANATION : this.model.toJSON().benefitExplanation.ACUPUNCTURE, EXCLUSION : this.model.toJSON().netwkException.ACUPUNCTURE}); },
			getSpecialNeed9 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.REHABILITATIVE_SPEECH_THERAPY, EXPLANATION : this.model.toJSON().benefitExplanation.REHABILITATIVE_SPEECH_THERAPY, EXCLUSION : this.model.toJSON().netwkException.REHABILITATIVE_SPEECH_THERAPY}); },
			getSpecialNeed10 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.REHABILITATIVE_PHYSICAL_THERAPY, EXPLANATION : this.model.toJSON().benefitExplanation.REHABILITATIVE_PHYSICAL_THERAPY, EXCLUSION : this.model.toJSON().netwkException.REHABILITATIVE_PHYSICAL_THERAPY}); },
			getSpecialNeed11 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.WELL_BABY, EXPLANATION : this.model.toJSON().benefitExplanation.WELL_BABY, EXCLUSION : this.model.toJSON().netwkException.WELL_BABY}); },
			getSpecialNeed12 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ALLERGY_TESTING, EXPLANATION : this.model.toJSON().benefitExplanation.ALLERGY_TESTING, EXCLUSION : this.model.toJSON().netwkException.ALLERGY_TESTING}); },
			getSpecialNeed13 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DIABETES_EDUCATION, EXPLANATION : this.model.toJSON().benefitExplanation.DIABETES_EDUCATION, EXCLUSION : this.model.toJSON().netwkException.DIABETES_EDUCATION}); },
			getSpecialNeed14 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.NUTRITIONAL_COUNSELING, EXPLANATION : this.model.toJSON().benefitExplanation.NUTRITIONAL_COUNSELING, EXCLUSION : this.model.toJSON().netwkException.NUTRITIONAL_COUNSELING}); },
			
			getChildrensVision1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.RTN_EYE_EXAM_CHILDREN, EXPLANATION : this.model.toJSON().benefitExplanation.RTN_EYE_EXAM_CHILDREN, EXCLUSION : this.model.toJSON().netwkException.RTN_EYE_EXAM_CHILDREN}); },
			getChildrensVision2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.GLASSES_CHILDREN, EXPLANATION : this.model.toJSON().benefitExplanation.GLASSES_CHILDREN, EXCLUSION : this.model.toJSON().netwkException.GLASSES_CHILDREN}); },
			getHearingAids : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HEARING_AIDS, EXPLANATION : this.model.toJSON().benefitExplanation.HEARING_AIDS, EXCLUSION : this.model.toJSON().netwkException.HEARING_AIDS}); },
			getChildrensDental1 : function () { 
				if($('#stateCode').val() == 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL, EXPLANATION : this.model.toJSON().benefitExplanation.ACCIDENTAL_DENTAL, EXCLUSION : this.model.toJSON().netwkException.ACCIDENTAL_DENTAL, NC: true});
				}else{
					$('#childrensDental1').hide() ;
				} },
			getChildrensDental2 : function () { 
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_ADULT, EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_ADULT, NC: true}); 
				}else{
					$('#childrensDental2').hide() ;
				}},
			getChildrensDental3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_CHILD, EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_CHILD, NC: true}); },
			getChildrensDental4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN, EXPLANATION : this.model.toJSON().benefitExplanation.DENTAL_CHECKUP_CHILDREN, EXCLUSION : this.model.toJSON().netwkException.DENTAL_CHECKUP_CHILDREN, NC: true}); },
			getChildrensDental5 : function () { 
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_ADULT, EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_ADULT, NC: true});
				}else{
					$('#childrensDental5').hide() ;
				}},
			getChildrensDental6 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_CHILD, EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_CHILD, NC: true}); },
			getChildrensDental7 : function () { 
				if($('#stateCode').val() != 'CA'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_ADULT, EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_ADULT, NC: true}); 
				}else{
					$('#childrensDental7').hide() ;
				}},
			getChildrensDental8 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_CHILD, EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_CHILD, EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_CHILD, NC: true}); },
			getChildrensDental9 : function () { 
				if($('#stateCode').val() == 'ID'){
					return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.RTN_DENTAL_ADULT, EXPLANATION : this.model.toJSON().benefitExplanation.RTN_DENTAL_ADULT, EXCLUSION : this.model.toJSON().netwkException.RTN_DENTAL_ADULT, NC: true}); 
				}else{
					$('#childrensDental9').hide() ;
				}}
			
		});
	 }
	
	var providerDensity = function(networkKey, planId){
		if(networkKey == null || networkKey == ""){
			$("#providerHeat_"+planId+"_"+networkKey).html("N/A");
            $("#viewMap_"+planId+"_"+networkKey).remove();
		}else{
						
            var requestData = {
                            "searchKey" : "",
                            "userZip" : $("#zipcode").val(),
                            "currentPage" : 1,
                            "pageSize" : 1,
                            "otherDistance" : $("#providerDistance").val(),
                            "searchType" : 'doctor',
                            "networkId": networkKey
                        };
			$.ajax({
				type: 'GET',
				url: '/hix/provider/doctors/search',
				data: requestData,
                dataType: 'json',
                async: false,
				success: function(data) {
					if(data){
						if(data.total_hits == 0){
							$("#providerHeat_"+planId+"_"+networkKey).html("N/A");
							$("#viewMap_"+planId+"_"+networkKey).remove();
						}else{
							$("#providerHeat_"+planId+"_"+networkKey).html(addCommas(data.total_hits));
						}
					}
				},
				error: function(){
				$("#providerHeat_"+planId+"_"+networkKey).html("N/A");
                    $("#viewMap_"+planId+"_"+networkKey).remove();
				}
			});
		}
	};
	
	
	function addCommas(input) {
	    if (_.isUndefined(input)) return "";
	    return input.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
})(App.views);