package com.getinsured.iex.util;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.eligibility.enums.ApplicantStatusEnum;

/**
 * @author chopra_s
 * 
 */
public interface ReferralConstants {
	String ACTIVE = "ACTIVE";
	String INACTIVE = "INACTIVE";
	int NONE = 0;
	int ERROR = 0;
	int FAILURE = 0;
	int SUCCESS = 1;
	int STATUS_NOT_VALID = 2;
	int RIDP_MANUAL = 3;
	int LCE_FLOW = 4;
	int INVALID_LCE = 5;
	int OE_CLOSED = 6;
	int ONE = 1;
	String IS_LCE = "Y";
	String NO_REFERRALACTIVATION_FOUND = "No Referral Activation data found for this Id - ";
	String WORKFLOW_STATUS_NOT_VALID = "Workflow Status is not valid";
	String NO_HOUSEHOLD_FOUND = "No Household found for this user - ";
	String NO_SSAP_FOUND = "No SSapapplication found for this application - ";
	String DEFDATEPATTERN = "MM/dd/yyyy";
	long MILLISECS_IN_DAY = 24 * 60 * 60 * 1000;
	String WITHOUT_LINK = "WITHOUT_LINK";
	String WITH_LINK = "WITH_LINK";
	String QHP = "QHP";
	String EXADMIN_USERNAME = "exadmin@ghix.com";
	String NULL_STRING = "null";
	String Y = "Y";
	String N = "N";
	int PRIMARY = 1;
	String ELIGIBLE_NONE = "NONE";
	String MEDICAID = "MEDICAID";
	String REFERRAL_ID = "referralId";
	String SSAP_ID = "ssapId";
	String PHONE_NUMBER_KEY = "8";
	String SSN_KEY = "4";
	String BIRTH_DATE_KEY = "3";
	String ACTIVATION_ID = "activationId";
	String SSAP_APPLICATION_KEY = "1002";
	String HOUSEHOLD_ID_KEY = "1003";
	String DB_DATE_FORMAT = "yyyy-MM-dd";
	String UI_DATE_FORMAT = "MM/dd/yyyy";
	String ACCESS_CODE = "accessCode";
	String SSAP_MAP = "ssapMap";
	String REFERRAL_LOCKED = "REFERRAL_LOCKED";
	String MULTIPLE_HOUSEHOLDS = "MULTIPLE_HOUSEHOLDS";
	String INVALID = "INVALID";
	String VALID = "VALID";
	String REFERRAL_STATUS_KEY = "1001";
	String HOUSEHOLD_ID = "householdId";
	String ENC_REF_ACCESS_CODE = "encAccessCode";
	String REFERRAL_LINKING_FLOW = "referralLinkingFlow";
	String ACCOUNT_ACTIVATION_FLOW = "accountActivationFlow";
	String FIRST_TIME_REFERRAL_LINKING = "firstTimeLinking";
	String FIRST_NAME = "firstName";
	String LAST_NAME = "lastName";
	String EMAIL_ID = "emailId";
	String PHONE = "phone";
	String ACTIVE_APP_PRESENT = "ActiveAppPresent";
	String ENROLLED_APP_PRESENT = "enrolledAppSess";
	String COVERAGE_YEAR = "COVERAGE_YEAR";
	String ROLE = "role";
	String MODULE_NAME="moduleName";
	String REFERRAL_ID_KEY = "1004";
	int REFERRAL_ACCESS_CODE_MAP_SIZE = 8;
	String CSR = "CSR";
	String COVERAGE_DATE_IS_PAST_ENROLLMENT_DATE = "New coverage begin date is past the current enrollment end date";
	String CURRENT_ENROLLMENT_END_DATE_STR_VAL = "& current enrollment end date - ";
	String COVERAGE_DATE_STR_VAL = ", coverageDate - ";
	String HOUSE_HOLD_CASE_ID = "HouseholdCaseId";
	String INTEGRATED_CASE_ID = "IntegratedCaseID";
	String EXT_APPLICATION_ID = "ApplicationID";
	String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";
	String ASSISTER_INFO = "Assister";
	String QEP_WITHOUT_QLE = "QEP_WITHOUT_QLE";
	
	
	@SuppressWarnings("serial")
	Map<Integer, String> randomQuestionsMap = new HashMap<Integer, String>() {
		{
			put(1, "firstName");
			put(2, "lastName");
			put(3, "birthDate");
			put(4, "ssn");
			put(5, "zipCode");
			put(6, "gender");
			put(7, "emailAddress");
			put(8, "phoneNumber");
			put(9, "county");
			put(10, "householdTotal");
			put(11, "medicaidId");
			put(12, "caseNumber");
		}
	};

	Set<ApplicantStatusEnum> DEMOGRAPHIC_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.DEMO_FIRSTNAME,ApplicantStatusEnum.DEMO_NAMESUFFIX,ApplicantStatusEnum.DEMO_MIDDLENAME, ApplicantStatusEnum.DEMO_LASTNAME, ApplicantStatusEnum.DEMO_SSN, ApplicantStatusEnum.DEMO_ADDRESS_LINE1,
	        ApplicantStatusEnum.DEMO_ADDRESS_LINE2,ApplicantStatusEnum.DEMO_PTF, ApplicantStatusEnum.DEMO_PRIMARY_ADDRESS, ApplicantStatusEnum.DEMO_MAILING_ADDRESS, ApplicantStatusEnum.DEMO_OTHER, ApplicantStatusEnum.DEMO_MULTIPLE, ApplicantStatusEnum.DEMO_ADDRESS_CITY,
	        ApplicantStatusEnum.DEMO_MARITIAL_STATUS,ApplicantStatusEnum.DEMO_SPOKEN_LANGUAGE,ApplicantStatusEnum.DEMO_WRITTEN_LANGUAGE);

	@SuppressWarnings("serial")
	List<String> DEMOGRAPHIC_APPLICANT_STATUS_VALUE = new ArrayList<String>() {
		{
			add(ApplicantStatusEnum.DEMO_FIRSTNAME.value());
			add(ApplicantStatusEnum.DEMO_NAMESUFFIX.value());
			add(ApplicantStatusEnum.DEMO_MIDDLENAME.value());
			add(ApplicantStatusEnum.DEMO_LASTNAME.value());
			add(ApplicantStatusEnum.DEMO_SSN.value());
			add(ApplicantStatusEnum.DEMO_ADDRESS_LINE1.value());
			add(ApplicantStatusEnum.DEMO_ADDRESS_LINE2.value());
			add(ApplicantStatusEnum.DEMO_PRIMARY_ADDRESS.value());
			add(ApplicantStatusEnum.DEMO_MAILING_ADDRESS.value());
			add(ApplicantStatusEnum.DEMO_OTHER.value());
			add(ApplicantStatusEnum.DEMO_MULTIPLE.value());
			add(ApplicantStatusEnum.DEMO_ADDRESS_CITY.value());
			add(ApplicantStatusEnum.DEMO_PTF.value());
			add(ApplicantStatusEnum.DEMO_MARITIAL_STATUS.value());
			add(ApplicantStatusEnum.DEMO_SPOKEN_LANGUAGE.value());
			add(ApplicantStatusEnum.DEMO_WRITTEN_LANGUAGE.value());
		}
	};
	Set<ApplicantStatusEnum> NON_DEMOGRAPHIC_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL,
	        ApplicantStatusEnum.UPDATED_DOB, ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS, ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> NON_DEMO_DOB_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL,
	        ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS, ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> NON_APTC_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE,
	        ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL, ApplicantStatusEnum.UPDATED_DOB, ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS,
	        ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> NO_EFFECT = EnumSet.of(ApplicantStatusEnum.ADD_NEW, ApplicantStatusEnum.NO_CHANGE, ApplicantStatusEnum.DELETED);

	Set<ApplicantStatusEnum> DOB_CHANGED = EnumSet.of(ApplicantStatusEnum.UPDATED_DOB);

	Set<ApplicantStatusEnum> APTC_CHANGED = EnumSet.of(ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT);

	Set<ApplicantStatusEnum> NO_CHANGE = EnumSet.of(ApplicantStatusEnum.NO_CHANGE);

	Set<ApplicantStatusEnum> CITIZEN_SHIP_CHANGED = EnumSet.of(ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS);

	Set<ApplicantStatusEnum> RELATIONSHIP_CHANGED = EnumSet.of(ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);
	
	Set<ApplicantStatusEnum> ADDRESS_CHANGE = EnumSet.of(ApplicantStatusEnum.UPDATED_ZIP_COUNTY);

	Set<ApplicantStatusEnum> NON_CITIZEN_SHIP_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT);

	Set<ApplicantStatusEnum> NON_RELATIONSHIP_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT,
	        ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS);

	Set<ApplicantStatusEnum> NEWLY_ELIGIBLE_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE);

	Set<ApplicantStatusEnum> NEWLY_INELIGIBLE_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE);

	Set<ApplicantStatusEnum> OTHER_ELIGIBLE_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT, ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> ALL_CHANGES_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE,
	        ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL, ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT, ApplicantStatusEnum.UPDATED_DOB,
	        ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS, ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> SEP_DENIAL_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS);

	Set<ApplicantStatusEnum> ALLOW_KEEP_ONLY_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY,
	        ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT, ApplicantStatusEnum.UPDATED_DOB, ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION);

	Set<ApplicantStatusEnum> DONT_ALLOW_KEEP_ONLY_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY, ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL,
	        ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS);

	String STATUS_KEY = "status";

	String RESULT_KEY = "result";

	String PROCESS_FLOW = "PROCESS FLOW";

	String QEP = "QEP";
	String SEP = "SEP";

	String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";

	String EE_PACKAGE_NAME = "com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.";

	String NOTAPPLYING_CODE = "998";

	String HEALTH = "Health";

	String HEALTH_ENROLLMENT_DTO = "HealthEnrollmentDTO";

	String DENTAL = "Dental";

	String DENTAL_ENROLLMENT_DTO = "DentalEnrollmentDTO";

	String OTHER_DISENROLLMENT_CODE = "07";

	String DEATH_DISENROLLMENT_CODE = "03";

	String QUALIFYING_EVENT = "QUALIFYING_EVENT";
	String REMOVE_DEPENDENT = "REMOVE_DEPENDENT";
	String REMOVE = "REMOVE";
	String GAIN_OF_MEC_FUTURE = "GAIN_OF_MEC_FUTURE";
	String PRIMARY_GAIN_OF_MEC_FUTURE = "PRIMARY_GAIN_OF_MEC_FUTURE";
	String CS2 = "CS2";
	String CS3 = "CS3";
	String CS4 = "CS4";
	String CS5 = "CS5";
	String CS6 = "CS6";
	String CS1 = "CS1";

	String APPLICATION_ID = "ApplicationId";
	String MEDICAID_ID = "MedicaidId";
	String FLOW = "flow";
	String BOTH = "Both";
}
