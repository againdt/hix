/**
 * 
 */
package com.getinsured.eligibility.prescreen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;

/**
 * Repository class for STATE_MEDICAID_CHIP
 * @author Nikhil Talreja
 * @since 9 July, 2013
 *
 */

@Repository
public interface IStateChipMedicaidRepository extends JpaRepository<StateChipMedicaid, Long> {
	
	/**
	 * Fetches a record using state code
	 * @author Nikhil Talreja
	 * @since 10 September, 2013
	 *
	 */
	StateChipMedicaid findByStateCode(String stateCode);
	
}
