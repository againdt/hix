/**
 * 
 */
package com.getinsured.hix.admin.util;

/**
 * @author gorai_k
 *
 */
public class PlanTenantUtil {

	private Integer planId;
	private String planNumber;
	private Integer countOfTenantIds;
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanNumber() {
		return planNumber;
	}
	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}
	public Integer getCountOfTenantIds() {
		return countOfTenantIds;
	}
	public void setCountOfTenantIds(Integer countOfTenantIds) {
		this.countOfTenantIds = countOfTenantIds;
	}
}
