package com.getinsured.hix.account.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class ManageUser {

	private static final long serialVersionUID = 1L;

	public interface ManageUserData extends Default{
		
	}
	
	@NotEmpty(message="{label.validateFirstName}",groups=ManageUser.ManageUserData.class)
	private String firstName;

	@NotEmpty(message="{label.validateLastName}",groups=ManageUser.ManageUserData.class)
	private String lastName;
	
	@NotEmpty(message="{label.validateEmail}",groups=ManageUser.ManageUserData.class)
	@Email(message="{label.validatePleaseEnterValidEmail}",groups=ManageUser.ManageUserData.class)
	@Size(max=50,groups=ManageUser.ManageUserData.class)
	private String email;
	
	@NotEmpty(message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Size(min=3,max=3,message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	private String phone1;
	
	@NotEmpty(message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Size(min=3,max=3,message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	private String phone2;
	
	@NotEmpty(message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Size(min=4,max=4,message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=ManageUser.ManageUserData.class)
	private String phone3;
	
	private String role;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
	
	

}
