(function (views) {
	views.Pagination = Backbone.View.extend({

		events : {
			'click a.first'        : 'gotoFirst',
			'click a.prev'         : 'gotoPrev',
			'click a.next'         : 'gotoNext',
			'click a.last'         : 'gotoLast',
			'click a.page'         : 'gotoPage'
		},

		el : '#pagination',
		initialize : function () {
			_.bindAll (this, 'render','gotoNext','gotoPrev','gotoNextItem','gotoPrevItem', 'gotoPage');
			var self = this;
			self.tmpl = _.template($('#tmpPagination').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self = this;
            var info = self.collection.info();

            if(info.totalPages < 2){
            	$('.ps-pagination').hide();

            }else {
            	$('.ps-pagination').show();

    			var html = this.tmpl(info);

    			$(this.el).html(html);
    			$("#pagination_bottom").html(html);

    			$('.prev').bind('click', self.gotoPrev);
    			$('.next').bind('click', self.gotoNext);

    			if(info.page != 1){
    				$('.prev').removeClass('cp-pagination__arrow--disabled');
    			}else {
    				$('.prev').addClass('cp-pagination__arrow--disabled');
    			}

    			if(info.page != info.lastPage){
    				$('.next').removeClass('cp-pagination__arrow--disabled');
    			}else {
    				$('.next').addClass('cp-pagination__arrow--disabled');
    			}

            }
		},

		gotoFirst : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(1);
			self.collection.preserveOtherOptions();
		},

		gotoPrev : function (e) {
			e.preventDefault();
			var self = this;
			var info = self.collection.info();

			if(info.page === 1) {
				return;
			}
			self.collection.previousPage();
			self.collection.preserveOtherOptions();
			window.scrollTo(0,0);
		},

		gotoNext : function (e) {
			e.preventDefault();
			var self = this;
			var info = self.collection.info();

			if(info.page === info.lastPage) {
				return;
			}

			self.collection.nextPage();
			self.collection.preserveOtherOptions();
			window.scrollTo(0,0);
		},

		gotoLast : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(self.collection.information.lastPage);
			self.collection.preserveOtherOptions();
		},

		gotoPage : function (e) {
			e.preventDefault();
			var self = this;
			var page = $(e.target).text();
			window.dataLayer.push({'event': 'displayPaginationEvent', 'eventCategory': 'Plan Selection - Plan Display Pagination', 'eventAction': 'Pagination Button Click', 'eventLabel': 'Pagination - ' + page});
			//triggerGoogleTrackingEvent(['_trackEvent', 'Plan Display Pagination', 'click', 'Pagination - ' + page]);
			self.collection.goTo(page);
			self.collection.preserveOtherOptions();
		},

		gotoNextItem : function (e) {
			e.preventDefault();
			var self = this;
			var info = self.collection.info();
			var nextenabled = self.collection.stop == info.totalRecords ? false : true;
			if(nextenabled){
				self.collection.nextItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
		},

		gotoPrevItem : function (e) {
			e.preventDefault();
			var self = this;
			var prevenabled = self.collection.start == 0 ? false : true;
			if(prevenabled){
				self.collection.prevItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
		}

	});
})(App.views);
