package com.getinsured.eligibility.at.resp.si.dto;

import java.util.List;

/**
 * @author chopra_s
 * 
 */

public class ApplicantDetails {

	private String identificationId;
	private Boolean verificationIndicator;
	private List<ApplicantEvent> applicantEvents;
	private List<ApplicantEvent> applicantEventsNonQHP;

	public String getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(String identificationId) {
		this.identificationId = identificationId;
	}

	public Boolean getVerificationIndicator() {
		return verificationIndicator;
	}

	public void setVerificationIndicator(Boolean verificationIndicator) {
		this.verificationIndicator = verificationIndicator;
	}

	public List<ApplicantEvent> getApplicantEvents() {
		return applicantEvents;
	}

	public void setApplicantEvents(List<ApplicantEvent> applicantEvents) {
		this.applicantEvents = applicantEvents;
	}

	public List<ApplicantEvent> getApplicantEventsNonQHP() {
		return applicantEventsNonQHP;
	}

	public void setApplicantEventsNonQHP(List<ApplicantEvent> applicantEventsNonQHP) {
		this.applicantEventsNonQHP = applicantEventsNonQHP;
	}
}
