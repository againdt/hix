(function (views) {
	views.Pagination = Backbone.View.extend({
		
		events : {
			'click a.first'        : 'gotoFirst',
			'click a.prev'         : 'gotoPrev',
			'click a.next'         : 'gotoNext',
			'click a.last'         : 'gotoLast',
			'click a.page'         : 'gotoPage'
		},
		
		el : '#pagination',
		initialize : function () {
			_.bindAll (this, 'render','gotoNext','gotoPrev','gotoNextItem','gotoPrevItem','updateElements');
			var self = this;
			self.tmpl = _.template($('#tmpPagination').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self = this;
            var info = self.collection.info();

            $('#prevItem').unbind('click');
            $('#nextItem').unbind('click');
            if(info.page != 1){
            	$('#prevItem').bind('click',this.gotoPrev);
            }
            if(info.page != info.lastPage){
            	$('#nextItem').bind('click',this.gotoNext);
            }
			
			var html = this.tmpl(info);
			$(this.el).html(html);
		},
		
		gotoFirst : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(1);
			self.collection.preserveOtherOptions();
			self.collection.updateElements();
		},
		
		gotoPrev : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.previousPage();
			self.collection.preserveOtherOptions();
			self.updateElements();
			
		},
		
		gotoNext : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.nextPage();
			self.collection.preserveOtherOptions();
			self.updateElements();
		},
		
		gotoLast : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(self.collection.information.lastPage);
			self.collection.preserveOtherOptions();
			self.updateElements();
		},
		
		gotoPage : function (e) {
			e.preventDefault();
			var self = this;
			var page = e.target.id;
			if (page.indexOf("Page") >= 0){
				var pattern = /[0-9]+/g;
				var pageno = page.match(pattern);
			}
			self.collection.goTo(pageno);
			self.collection.goTo(page);
			self.collection.preserveOtherOptions();
			self.updateElements();
		},
		
		gotoNextItem : function (e) {
			e.preventDefault();
			var self = this;
			var info = self.collection.info();
			var nextenabled = self.collection.stop == info.totalRecords ? false : true;
			if(nextenabled){
				self.collection.nextItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
			self.updateElements();
		},
		
		gotoPrevItem : function (e) {
			e.preventDefault();
			var self = this;
			var prevenabled = self.collection.start == 0 ? false : true;
			if(prevenabled){
				self.collection.prevItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
			self.updateElements();
		},
		
		updateElements : function(){
			var self = this;
			
			/*self.collection.each (function(model){
				if(self.collection.benchmarkplanId !=''){
					if(self.collection.benchmarkplanId == model.get("planId")){
						$('#plan_'+model.get("planId")).attr('checked', true);
					}
				}
				else{
						$('#plan_'+model.get("planId")).attr('checked', false);
				}
			});*/
			
			var currentSort = self.collection.sortColumn;
			if(currentSort =='issuerText')
				$('#'+currentSort+'Order').html("("+self.collection.sortDirection+")");
		}
		
	});
})(App.views);