(function() {
    'use strict';

    angular.module('calendarComponent', [])
    .directive('calendar', calendar);

    function calendar() {
    	var directive = {
    			restrict : 'AE',
    			link : function(scope, element, attrs) {						
    				element.find('.icon-calendar').click(function(){
    					element.datepicker({
    						autoclose: true,
    						orientation: "bottom"
    					});
    					element.find('input').off('click focus');
    				});
    			}
    		};
    		
    		return directive;
    }


})();