<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page contentType="text/html" import="java.util.*" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>


<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.js" />"></script> --%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript">
	
	function ie8Trim() {
		if(typeof String.prototype.trim !== 'function') {
	        String.prototype.trim = function() {
	        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
	        }
		}
	}
	
	function highlightThis(val){
		
		var currUrl = window.location.href;
		var newUrl="";
		/* Check if Query String parameter is set or not */
		if(currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if(currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				 newUrl = "?lang="+val;
			} else if(currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length-2)+val; 
			} else { 
				newUrl = currUrl + "&lang="+val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang="+val;
		}
		window.location = newUrl;
	}


	$(document).ready(function() {
		$('.ttclass').tooltip();	
			 $('#licrenewaldate').focus(function(){
					if ( $(this).val() == 'MM-DD-YYYY' ){
					  $(this).val("");
					}
					});

		 $('#licrenewaldate').blur(function(){
			    if ( $(this).val() == '' ){
			      $(this).val("MM-DD-YYYY");
			    }
			    });
		 
		var brkId = ${broker.id};

		if(brkId != 0) {
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#buildProfile').attr('class', 'visited');
		};
		
	$('#address1_business').keyup(function () {
		if($("#mailingAddressCheck").is(":checked")) {
			var strMailingAddress1 = $("#address1_business").val();
			$("#address1_mailing").val(strMailingAddress1);
			$("#address1_mailing_error").hide();
		}
	});
	
	$('#state_business').change(function () {
		if($("#mailingAddressCheck").is(":checked")) {
			var strMailingState = $("#state_business").val();
			$("#state_mailing").val(strMailingState);
			$("#state_mailing_error").hide();
		}
	});
	
	$('#address2_business').keyup(function () {
		if($("#mailingAddressCheck").is(":checked")) {
			var strMailingAddress2 = $("#address2_business").val();
			$("#address2_mailing").val(strMailingAddress2);
		}
	});
	
	$('#city_business').keyup(function () {
		if($("#mailingAddressCheck").is(":checked")) {
			var strMailingCity = $("#city_business").val();
			$("#city_mailing").val(strMailingCity);
		}
	});
	
	$('#zip_business').keyup(function () {
		if($("#mailingAddressCheck").is(":checked")) {
			var strMailingZip = $("#zip_business").val();
			$("#zip_mailing").val(strMailingZip);
		}
	});
		
		if($("#mailingAddressCheck").is(":checked")) {
			$("#address1_mailing").attr('readonly', true);
			$("#address2_mailing").attr('readonly', true);
			$("#city_mailing").attr('readonly', true);
			$("#state_mailing").attr('disabled', true);
			$("#zip_mailing").attr('readonly', true);
		} else {
			$("#address1_mailing").removeAttr("readonly"); 
			$("#address_mailing").removeAttr("readonly"); 
			$("#city_mailing").removeAttr("readonly"); 
			$("#stat_mailing").removeAttr("disabled"); 
			$("#zip_mailing").removeAttr("readonly");
		}
		
		function chkLicenseNumber(){
		$.get('/hix/broker/certificationapplication/chkLicenseNumber',
			{licenseNumber: $("#licenseNumber").val()},
				function(response){ 
					if(response == "EXIST"){
						$("#licenseNumberCheck").val(0);
					}else{
						$("#licenseNumberCheck").val(1);
					}
				}
			);	}

		$('#licenseNumber').focusout(function(e) {
			chkLicenseNumber();
		});
		
		<c:if test="${not empty broker.agencyId}">
	 		if($("#select_agencyLocation")[0].selectedIndex == 0){
	 			$("#mailingAddressCheck").attr("disabled", true);
	        } else {
	 			$("#mailingAddressCheck").removeAttr("disabled");
	        }
	    </c:if> 
		
		siteJsonObj =  ${agencySitesJsonString};  
		
		 $('#select_agencyLocation').on('change', function() {
				
				var selectedDropDownVal=  this.value;
				
				$.each(siteJsonObj, function( index, value ) {
				 if( selectedDropDownVal == value.id){
					 $('#dynamicLocationBox').text( value.address1+" "+(value.address2!=null ? value.address2 : " ")).append("<br/>").append(value.city+ ", "+value.state+ " "+value.zip);
					 $("#mailingAddressCheck").removeAttr("disabled");
					 if($("#mailingAddressCheck").is(":checked")) {
							$("#address1_mailing").val(value.address1);
							
							$("#address2_mailing").val(value.address2);
							
							$("#city_mailing").val(value.city);
							
							$("#state_mailing").val(value.state);
							$("#state_mailing_hidden").val(value.state);
							
							$("#zip_mailing").val(value.zip);
							
					 }
				 }
				 
				 if($("#select_agencyLocation")[0].selectedIndex == 0){
			 			$("#mailingAddressCheck").attr("disabled", true);
			 			$('#mailingAddressCheck').prop('checked', false);
			 			$('#dynamicLocationBox').empty();
			 			
						$("#address1_mailing").removeAttr("readonly"); 
						$("#address2_mailing").removeAttr("readonly"); 
						$("#city_mailing").removeAttr("readonly"); 
						$("#state_mailing").removeAttr("disabled"); 
						$("#zip_mailing").removeAttr("readonly");
						
						if($("#address1_mailing").is(":visible")){
							$("#address1_mailing").val("");
							$("#address2_mailing").val("");
							$("#city_mailing").val("");
							$("#state_mailing").val(""); 
							$("#zip_mailing").val("");
						}
						
			 		}
				
				});
				
				if($("#address1_mailing_error").html() != null){
					$("#address1_mailing_error").html('');
				}
				
				if($("#address2_mailing_error").html() != null){
					$("#address2_mailing_error").html('');
				}
		    });
		
		/* $("input[name=communicationPreference]:radio").change(function () {
	        if ($("#fax").attr("checked")) {
	            $('#faxReq').show();
	        } else {
	        	 $('#faxReq').hide();
	        }
	        
	    });
		
		if ($("#fax").attr("checked")) {
	        $('#faxReq').show();
	    } else {
	   	 $('#faxReq').hide();
	    } */

 /*function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
		nextelement = document.getElementById(nextelement);
		nextelement.focus()
	}
}*/
		
	});
		
</script>
	
<div class="gutter10">
	<div class="l-page-breadcrumb">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb" style="margin-top:0;">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.profile"/></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.firstName}</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1><a name="skip"></a>
			<c:choose>
				<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
					${broker.user.firstName}&nbsp;${broker.user.lastName}
				</c:when>
				<c:otherwise>
					${broker.firstName}&nbsp;${broker.lastName}
				</c:otherwise>
			</c:choose>
		</h1>
	</div>
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<div class="header"></div>
				<ul class="nav nav-list">						
					<li class="active"><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
					<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
					<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>	
					<c:if test="${CA_STATE_CODE}">
						<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
					</c:if>
					<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>
					<c:if test="${CA_STATE_CODE}">
						<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
					</c:if>
					<c:if test="${!CA_STATE_CODE && broker.user != null}">	
						<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
						<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
					</c:if>						
				</ul>	
				
				 <br>
			<c:if test="${!CA_STATE_CODE}">	
				<div class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">  
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">  
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${broker.user.firstName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
	            </ul> 
	        </c:if>  		
		</div><!-- end of span3 -->

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.brkRegistrationInformation"/></h4>
				<c:if test="${(broker.id != 0)}" >
					<a class="btn btn-primary btn-small pull-right" onClick="document.getElementById('SaveBrkCerti').click();" id="save" name="save" title="<spring:message code="label.agent.employee.addNewComment.save-button"/>"><spring:message code="label.agent.employee.addNewComment.save-button"/></a>
					<a class="btn btn-small pull-right" title="<spring:message code="label.brkCancel"/>" href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkCancel"/></a> 
				</c:if>
			</div>
			<p class="gutter10"><spring:message code="label.brkProvidefollowing"/>&nbsp;<spring:message code="label.brkBrokerStateName"/>.&nbsp;<spring:message code="label.brkAfterQuickReview"/>
			<!-- Please provide your insurance license and exchange certification information for review by an exchange
				administrator. You will receive notification when your application
				is approved. --> 
				</p>


			<form class="form-horizontal gutter10 entityAddressValidation" id="frmbrokerCertification" name="frmbrokerCertification" action="certificationapplication" method="POST">
			 <div class="row-fluid">  
			            <div style="font-size: 14px; color: red">
			              <table>
			              	<c:forEach items="${fieldError}" var="violation">
			                    <tr>
			                        <td><p> 
			                         		<c:catch var="exception"> <c:if test="${violation.defaultMessage !=null}"> </c:if>   </c:catch>
											<c:if test="${empty exception}">
												 <c:choose>
													 	<c:when  test="${violation.defaultMessage != null && fn:indexOf(violation.defaultMessage, '{') eq 0}">
										       			  <spring:message  code="${fn:replace(fn:replace(violation.defaultMessage,'{',''),'}','')}"/>
										    		</c:when>
										    		<c:otherwise>
										    		  ${violation.field}	${violation.defaultMessage}
										    		</c:otherwise>
												 </c:choose> 
											</c:if>
											<c:catch var="exception"> <c:if test=" ${violation.message !=null}"> </c:if>    </c:catch>
											<c:if test="${empty exception}">
												 <spring:message  code="${fn:replace(fn:replace(violation.message,'{',''),'}','')}"/>
											</c:if>
			                             	
			                        	<p/>
			                        </td>
			                    </tr>
			                </c:forEach>
			                </table>
			                <br/>
			            </div>
			    </div>
			
			<df:csrfToken/>
					<input type="hidden" name="certificationStatus" value="${broker.certificationStatus}" />
					<input type="hidden" id="brokerId" name="brokerId" value="<encryptor:enc value="${broker.id}"/>" />
					<div class="control-group">
						<label for="firstName" class="required control-label"><spring:message  code="label.brkFirstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label><!-- end of label -->
						<div class="controls">
							<c:choose>
								<c:when test="${broker.user.firstName != null}">
									<input type="text" name="user.firstName" id="firstName" value="${broker.user.firstName}" class="xlarge" size="30"  maxlength="50"/>
								</c:when>
								<c:otherwise>
									<input type="text" name="user.firstName" id="firstName" value="${broker.firstName}" class="xlarge" size="30"  maxlength="50"/>
								</c:otherwise>
							</c:choose>
							<div id="firstName_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="lastName" class="required control-label"><spring:message  code="label.brkLastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label><!-- end of label -->
						<div class="controls">
							<c:choose>
								<c:when test="${broker.user.lastName != null}">
									<input type="text" name="user.lastName" id="lastName" value="${broker.user.lastName}" class="xlarge" size="30"  maxlength="50"/>
								</c:when>
								<c:otherwise>
									<input type="text" name="user.lastName" id="lastName" value="${broker.lastName}" class="xlarge" size="30"  maxlength="50"/>
								</c:otherwise>
							</c:choose>
							
							<div id="lastName_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
					
				
				
					<div class="control-group">
						<label for="licenseNumber" class="required control-label"><spring:message  code="label.brkLicenseNumber"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="licenseNumber" id="licenseNumber" value="${broker.licenseNumber}" class="input-xlarge" size="30" <c:if test="${CA_STATE_CODE}"> maxlength="7" </c:if> <c:if test="${not CA_STATE_CODE}"> maxlength="10" </c:if>  />
							<spring:message  code="label.not_your_NPN"/>							
						<div id="licenseNumber_error"></div>
						</div>
					</div> 
					
					<c:if test="${stateCode == 'NV'}">
	                    <div class="control-group">
							<label for="npn" class="required control-label"><spring:message  code="label.brkNpn"/></label>
							<div class="controls">
								<input type="text"  name="npn" id="npn" value="${broker.npn}" class="input-xlarge"  maxlength="20" />
								<div id="npn_error"></div>
							</div>
						</div>
					</c:if>

					<div class="control-group">
                      <label class="required control-label"><spring:message  code="label.brkLicenseRenewalDate"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                      <div class="controls">
                          <div class="input-append date date-picker" id="date" data-date="">
                          <c:choose>
								<c:when test="${broker.licenseRenewalDate !=null}">										
										<input class="span10" type="text" name="licrenewaldate" id="licrenewaldate"  value='<fmt:formatDate value= '${broker.licenseRenewalDate}' pattern="MM-dd-yyyy"/>' pattern="mm-dd-yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>	
								</c:when>
								<c:otherwise>										
										<input class="span10" type="text" name="licrenewaldate" id="licrenewaldate"  value="MM-DD-YYYY" pattern="mm-dd-yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>	
								</c:otherwise>
							</c:choose>
						  </div>
						  <div id="licrenewaldate_error" class="help-inline"></div>	
                      </div>
                    </div>  
                    
                    <c:if test="${CA_STATE_CODE}">
	                     <div class="control-group">
		                      <label class="required control-label"><spring:message  code="label.brkPersonalEmail"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
		                      	<a class="ttclass" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='<spring:message code="label.individualEmailTooltip" />'>
									<i class="icon-question-sign"></i>
								</a>
							  </label>
		                      <div class="controls">
		                          <input type="text" name="personalEmailAddress" id="personalEmailAddress" value='${broker.personalEmailAddress}' class="input-xlarge" size="30" maxlength="100"  />	
								  <div id="personalEmailAddress_error" class="help-inline"></div>
		                      </div>
	                     </div>  
                     </c:if>
                     
					<div class="control-group phone-group">
						<label for="phone1" class="required control-label"><spring:message  code="label.brkPrimaryContact"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
						<label for="phone2" class="required hidden"><spring:message  code="label.brkPrimaryContact"/></label>
						<label for="phone3" class="required hidden"><spring:message  code="label.brkPrimaryContact"/></label>
						<div class="controls">
							<input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="area-code input-mini"/>
							<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'phone3')"/>
							<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" />
							<div id="phone3_error"></div>
						</div>
					</div>
						
					<div class="control-group phone-group">
						<label for="businessContactPhone1" class="control-label"><spring:message  code="label.brkBusinessContactPhoneNumber"/></label>
						<label for="businessContactPhone2" class="required hidden"><spring:message  code="label.brkBusinessContactPhoneNumber"/></label>
						<label for="businessContactPhone3" class="required hidden"><spring:message  code="label.brkBusinessContactPhoneNumber"/></label>
						<div class="controls">
							<input type="text" name="businessContactPhone1" id="businessContactPhone1" value="${businessContactPhone1}" maxlength="3" class="area-code input-mini"/>
							<input type="text" name="businessContactPhone2" id="businessContactPhone2" value="${businessContactPhone2}" maxlength="3" class="input-mini"  onKeyUp="shiftbox(this, 'businessContactPhone3')"/>
							<input type="text" name="businessContactPhone3" id="businessContactPhone3" value="${businessContactPhone3}" maxlength="4" class="input-small" />
							<input type="hidden" name="businessContactPhoneNumber" id="businessContactPhoneNumber" value="" />  
							<div id="businessContactPhone3_error"></div>
						</div>
					</div>					
					
					<div class="control-group phone-group">
							<label for="alternatePhone1" class="control-label"><spring:message  code="label.brkAlternatePhoneNumber"/></label>
							<label for="alternatePhone2" class="required hidden"><spring:message  code="label.brkAlternatePhoneNumber"/></label>
							<label for="alternatePhone3" class="required hidden"><spring:message  code="label.brkAlternatePhoneNumber"/></label>
							<div class="controls">
								<input type="text" name="alternatePhone1" id="alternatePhone1" value="${alternatePhone1}" maxlength="3" class="area-code input-mini"/>
								<input type="text" name="alternatePhone2" id="alternatePhone2" value="${alternatePhone2}" maxlength="3" class="input-mini"/>
								<input type="text" name="alternatePhone3" id="alternatePhone3" value="${alternatePhone3}" maxlength="4" class="input-small" />
								<input type="hidden" name="alternatePhoneNumber" id="alternatePhoneNumber" value="" />  
								<div id="alternatePhone3_error"></div>
							</div>
					</div>
					
					<div class="control-group phone-group">
							<label for="faxNumber1" class="control-label"><spring:message  code="label.brkFaxNumber"/><img id="faxReq" class="hide" src="<c:url value="/resources/img/requiredAsterisk.png" />"  alt="Required!" /></label>
							<label for="faxNumber2"  class="required hidden"><spring:message  code="label.brkFaxNumber"/></label>
							<label for="faxNumber3"  class="required hidden"><spring:message  code="label.brkFaxNumber"/></label>
							<div class="controls">
								<input type="text" name="faxNumber1" id="faxNumber1" value="${faxNumber1}" maxlength="3" class="area-code input-mini"/>
								<input type="text" name="faxNumber2" id="faxNumber2" value="${faxNumber2}" maxlength="3" class="input-mini"/>
								<input type="text" name="faxNumber3" id="faxNumber3" value="${faxNumber3}" maxlength="4" class="input-small" />
								<input type="hidden" name="faxNumber" id="faxNumber" value="" />  
								<div id="faxNumber3_error"></div>
							</div>
					</div>	
					<div class="control-group">
			              <fieldset class="idMisMatch">
			                <legend class="control-label" for="communicationPreference"><spring:message code="label.brkPreferredMethodofCommunication"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></legend>
					            <div class="controls">
									<select name="communicationPreference" id="communicationPreference">
					               		<option value="<spring:message code="label.brkSelect"/>"><spring:message code="label.brkSelect"/></option>
					               		<option value="Phone" <c:if test="${broker.communicationPreference  == 'Phone'}">selected </c:if>><spring:message code="label.brkPhone"/></option>
										<option value="Email" <c:if test="${broker.communicationPreference  == 'Email'  or   broker.communicationPreference  ==  'Email Address'    }">selected </c:if>> <spring:message code="label.brkEmail"/></option>
										<option value="Mail" <c:if test="${broker.communicationPreference  == 'Mail'}">selected </c:if>><spring:message code="label.brkMail"/></option>		
									</select>
								<div id="communicationPreference_error"></div>	
							  </div><!-- .controls -->
			               </fieldset>
                   </div><!-- .control-group -->
                   <c:if test="${allowMailNotice=='Y'}">
                   <div class="control-group"> 
						<label for="postalMailcheckbox" class="control-label required"><spring:message  code="label.brk.postalMail"/> </label>
					<div class="controls">
						  <c:set var="clientsServedList" value="${broker.postalMailEnabled}"/>
			<label class="checkbox"> <input type="checkbox" value="Y" id="PostalMail"  name="postalMailEnabled"  ${fn:contains(clientsServedList, 'Y') ? 'checked="checked"' : '' } ></label> 
			<div style="display: block;font-style: italic;font-size: 11px"> <spring:message code="label.brk.postMailWarn"/></div>
				        	<div id="postalMailcheckbox_error"></div>
						</div>	<!-- end of controls-->
					</div>
					</c:if>	
						<div class="control-group">
							<label for="companyName" class="control-label required"><spring:message  code="label.brkCompanyName"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
							<div class="controls">
								<c:choose>
									<c:when test="${CA_STATE_CODE}">
										<input type="text" name="companyName" id="companyName" value="${broker.companyName}" class="input-xlarge" size="30" maxlength="80"/>
									</c:when>
									<c:otherwise>
								<input type="text" name="companyName" id="companyName" value="${broker.companyName}" class="input-xlarge" size="30" maxlength="1020"/>
									</c:otherwise>
								</c:choose>
								<div id="companyName_error"></div>
							</div>
						</div>
						
						<div class="control-group">
							<label for="federalEIN" class="required control-label"><spring:message  code="label.brkfederalEIN"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
							<div class="controls">
	<%-- 						<c:choose> --%>
	<%-- 						<c:when test="${broker.federalEIN!=null && broker.federalEIN != ''}">						 --%>
	<%-- 						<input type="text" name="federalEIN" id="federalEIN" value="${broker.federalEIN}"  class="input-xlarge" size="30"  maxlength="11" /> --%>
						                                            
	<%-- 						</c:when> --%>
	<%-- 						<c:otherwise> --%>
								<c:choose>
									<c:when test="${CA_STATE_CODE}">
										 <input type="text" name="federalEIN" id="federalEIN" value="${broker.federalEIN}" class="input-xlarge" size="30"  maxlength="9" />&nbsp;&nbsp;<small><a href="#einModal" role="button" class="" data-toggle="modal"><spring:message  code="label.eininfo"/></a></small>
									</c:when>
									<c:otherwise>
            							<input type="text" name="federalEIN" id="federalEIN" value="${broker.federalEIN}" class="input-xlarge" size="30"  maxlength="11" />&nbsp;&nbsp;<small><a href="#einModal" role="button" class="" data-toggle="modal"><spring:message  code="label.eininfo"/></a></small>
									</c:otherwise>
								</c:choose>
           
						    	<div id="federalEIN_error"></div>
	<%-- 						</c:otherwise> --%>
	<%-- 						</c:choose>	 --%>
							</div>
						</div> 
						
					<c:if test="${ ROLECHANGEALLOWED == 'TRUE' }">
						<div class="control-group"> 
								<label for="roleChange" class="required control-label">
									<spring:message  code="label.brkRole"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" />
								</label>
								<div class="controls">
									<label class="radio inline" for="ROLEAGENCYMANAGER">
										<input type="radio"	${CURRENTROLE == 'AGENCYMANAGER' ? 'checked="checked"' : ''} <c:if test="${ NOSERVICE == 'TRUE' or AMWONTAVAILABLE == 'TRUE' }"> onclick="showNoservice()" </c:if >      name="allotedRole" id="ROLEAGENCYMANAGER" value="ROLEAGENCYMANAGER"><spring:message code="label.brkAgencyManager"/>
									</label>
									<label class="radio inline" for="ROLEBROKER">
										<input type="radio" ${CURRENTROLE == 'BROKER' ? 'checked="checked"' : ''} <c:if test="${ NOSERVICE == 'TRUE' or AMWONTAVAILABLE == 'TRUE' }"> onclick="hideNoservice()" </c:if >         	 name="allotedRole" id="ROLEBROKER" value="ROLEBROKER"><spring:message code="label.brkAgent"/> 
									</label>
									
									<c:if test="${ NOSERVICE == 'TRUE' }">
										<div id="noServiceErrMsg" ${CURRENTROLE == 'BROKER' ? 'hidden="hidden"' : ''}   >
											<label class="error">
												<span> 
													<spring:message code="label.brkAgent"/> ${broker.user.firstName} ${broker.user.lastName} <spring:message code="label.brkCantService"/> 
												</span>
											</label>
											
										</div>
									</c:if >
									<c:if test="${ AMWONTAVAILABLE == 'TRUE' }">
										<div id="noAmAvailable" hidden="hidden"   >
											<label class="error">
												<span> 
													<spring:message code="label.brkNoAmAvailable"/>   
												</span>
											</label>
											
										</div>
									</c:if >
									
									
									 
								</div>
						</div>
					</c:if >
						
						 
					<h4 class="lightgray"><spring:message code="label.brkBusinessAddress"/></h4>
					<input type="hidden" id="county_business" name="location.county" value="${broker.location.county}">
					<!-- <input type="hidden" name="location.lat" id="lat_business" value="${broker.location.lat != null ? broker.location.lat : 0.0}" />
					<input type="hidden" name="location.lon" id="lon_business" value="${broker.location.lon != null ? broker.location.lon : 0.0}" />	 -->
					<input type="hidden" name="location.rdi" id="rdi_business" value="${broker.location.rdi != null ? broker.location.rdi : ''}" />
					
					<c:choose>
						<c:when test="${not empty broker.agencyId}">
							<div class="control-group">
								<label for="select_agencyLocation" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 control-label required"><spring:message code="label.selectAgencyLocation"/><sup><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></sup></label>
								<div class="controls">
									<select id="select_agencyLocation" name="select_agencyLocation" path="agencySites" class="input-medium"  >
										<option value="" ><spring:message code="label.agencySelectLocation"/></option>
										<c:forEach var="agencySite" items="${agencySites}">
											<option id="${agencySite.id}" <c:if test="${broker.location.id != null && agencySite.location.id == broker.location.id}"> selected="selected"</c:if> value="<c:out value="${agencySite.id}" />">
												<c:out value="${agencySite.siteLocationName}" />
											</option>
										</c:forEach>
									</select>
									 
									<input type="hidden" id="locationId" name="broker.location.id" value="${broker.location.id}">
									<div id="select_agencyLocation_error"></div>
								</div>
							</div>
							
							<div class="control-group">
								<label for="current_location" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 control-label required"><spring:message code="label.currentLocation"/></label>
								<div class="controls" id="dynamicLocationBox" >
									<%-- <c:forEach var="agencySite" items="${agencySites}"> --%>
										<c:if test="${broker.location.id != null}">
												${broker.location.address1} ${broker.location.address2} <br/>
												${broker.location.city}, ${broker.location.state} ${broker.location.zip}
										</c:if>
									<%-- </c:forEach> --%>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="control-group">
								<label for="address1_business" class="control-label required"><spring:message code="label.brkAddressLine1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
								<div class="controls">
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" placeholder="<spring:message code="label.brkAddressLine1"/>" name="location.address1" id="address1_business" value="${broker.location.address1}" class="input-xlarge" size="30" maxlength="25"/>
										</c:when>
										<c:otherwise>
											<input type="text" placeholder="<spring:message code="label.brkAddressLine1"/>" name="location.address1" id="address1_business" value="${broker.location.address1}" class="input-xlarge" size="30" maxlength="50"/>
										</c:otherwise>
									</c:choose>
									<input type="hidden" id="address1_business_hidden" name="address1_business_hidden" value="${broker.location.address1}">
									<div id="address1_business_error"></div>
								</div>	<!-- end of controls-->
							</div>	<!-- end of control-group -->
					
							<%-- <input type="hidden" name="location.lat" id="lat" value="${broker.location.lat != null ? broker.location.lat : 0.0}" />
							<input type="hidden" name="location.lon" id="lon" value="${broker.location.lon != null ? broker.location.lon : 0.0}" />	 --%>
							
							<div class="control-group">
								<label for="address2_business" class="control-label"><spring:message code="label.brkAddressLine2"/></label>
								<div class="controls">
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" placeholder="<spring:message code="label.brkAddressLine2"/>" name="location.address2" id="address2_business" value="${broker.location.address2}" class="input-xlarge" size="30" maxlength="25"/>
										</c:when>
										<c:otherwise>
											<input type="text" placeholder="<spring:message code="label.brkAddressLine2"/>" name="location.address2" id="address2_business" value="${broker.location.address2}" class="input-xlarge" size="30" maxlength="50"/>
										</c:otherwise>
									</c:choose>	
									<input type="hidden" id="address2_business_hidden" name="address2_business_hidden" value="${broker.location.address2}">
								</div>	<!-- end of controls-->
							</div>	<!-- end of control-group -->
					

							<div class="control-group">
								<label for="city_business" class="control-label required"><spring:message code="label.brkCity"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="<spring:message code="label.brkCity"/>" name="location.city" id="city_business" value="${broker.location.city}" class="input-xlarge" size="30" maxlength="30" />
									<input type="hidden" id="city_business_hidden" name="city_business_hidden" value="${broker.location.city}">
									<div id="city_business_error"></div>
								</div><!-- .controls -->
							</div>

							<div class="control-group">
								<label for="state_business" class="control-label"><spring:message code="label.brkState"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
								<div class="controls">
									<select size="1" id="state_business" name="location.state" path="statelist" class="input-medium">
										<option value="">State</option>
										<c:forEach var="state" items="${statelist}">
											<option id="${state.code}" <c:if test="${state.code == broker.location.state}"> selected="selected"</c:if> value="<c:out value="${state.code}" />">
												<c:out value="${state.name}" />
											</option>
										</c:forEach>
									</select>
									<input type="hidden" id="state_business_hidden" name="state_business_hidden" value="${broker.location.state}">
									<div id="state_business_error"></div>
								</div>	<!-- end of controls-->
							</div>

							<div class="control-group">
								<label for="zip_business" class="control-label"><spring:message code="label.brkZipCode"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="" name="location.zip" id="zip_business" value="${broker.location.zip}" class="input-small entityZipCode" maxlength="5"/>
									<input type="hidden" id="zip_business_hidden" name="zip_business_hidden" value="${broker.location.zip}">	
									<div id="zip_business_error"></div>
								</div><!-- end of controls-->
							</div>	<!-- end of control-group -->
						</c:otherwise>
					</c:choose>
						
						<h4 class="lightgray"> <spring:message code="label.brkMailingAddress"/> </h4>
						<div class="control-group">
						<label class="control-label" for="mailingAddressCheck"> <spring:message code="label.brkSameAsBusinessAddress"/></label>
						<div class="controls">
								<input type="checkbox" name="mailingAddressCheck" id="mailingAddressCheck" onclick="updatedSubmit()" ${locationMatching=='Y' ? 'checked' : 'unchecked'}>
						 </div>
						</div>
					
					<div class="control-group">
						<label for="address1_mailing" class="control-label required"><spring:message code="label.brkAddressLine1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
						<div class="controls">							
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" placeholder="<spring:message code="label.brkAddressLine1"/>" name="mailingLocation.address1" id="address1_mailing" value="<c:out value='${broker.mailingLocation.address1}'/>" class="input-xlarge" size="30" maxlength="25"/>
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brkAddressLine1"/>" name="mailingLocation.address1" id="address1_mailing" value="${broker.mailingLocation.address1}" class="input-xlarge" size="30" maxlength="50"/>
								</c:otherwise>
							</c:choose>							
							<input type="hidden" id="address1_mailing_hidden" name="address1_mailing_hidden" value="${broker.mailingLocation.address1}">
							<div id="address1_mailing_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
					
					<div class="control-group">
						<label for="mailingLocation.address2" class="control-label"><spring:message code="label.brkAddressLine2"/></label>
						<div class="controls">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" placeholder="<spring:message code="label.brkAddressLine2"/>" name="mailingLocation.address2" id="address2_mailing" value="<c:out value='${broker.mailingLocation.address2}'/>" class="input-xlarge" size="30" maxlength="25"/>
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brkAddressLine2"/>" name="mailingLocation.address2" id="address2_mailing" value="${broker.mailingLocation.address2}" class="input-xlarge" size="30" maxlength="50"/>
								</c:otherwise>
							</c:choose>	
							<div id="address2_mailing_error"></div>
							<input type="hidden" id="address2_mailing_hidden" name="address2_mailing_hidden" value="${broker.mailingLocation.address2}">
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="city_mailing" class="control-label required"><spring:message code="label.brkCity"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
						<div class="controls">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<input type="text" placeholder="<spring:message code="label.brkCity"/>" name="mailingLocation.city" id="city_mailing" value="<c:out value='${broker.mailingLocation.city}'/>" class="input-xlarge" size="30" maxlength="30"/>
								</c:when>
								<c:otherwise>
							<input type="text" placeholder="<spring:message code="label.brkCity"/>" name="mailingLocation.city" id="city_mailing" value="${broker.mailingLocation.city}" class="input-xlarge" size="30" maxlength="50"/>
								</c:otherwise>
							</c:choose>
							<input type="hidden" id="city_mailing_hidden" name="city_mailing_hidden" value="${broker.mailingLocation.city}">
							<div id="city_mailing_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="state_mailing" class="control-label"><spring:message code="label.brkState"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
						<div class="controls">
							<select size="1" id="state_mailing" name="mailingLocation.state" path="statelist" class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}" <c:if test="${state.code == broker.mailingLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_mailing_hidden" name="state_mailing_hidden" value="${broker.mailingLocation.state}">
							<div id="state_mailing_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="zip_mailing" class="control-label"><spring:message code="label.brkZipCode"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" placeholder="" name="mailingLocation.zip" id="zip_mailing" value="${broker.mailingLocation.zip}" class="input-mini zipCode" maxlength="5" />
							<input type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden" value="${broker.mailingLocation.zip}">	
							<div id="zip_mailing_error"></div>
						</div><!-- end of controls-->
					</div>	<!-- end of control-group -->
					
				<!-- 	<input type="hidden" name="mailingLocation.lat" id="lat_mailing" value="${broker.mailingLocation.lat != null ? broker.mailingLocation.lat : 0.0}" />
					<input type="hidden" name="mailingLocation.lon" id="lon_mailing" value="${broker.mailingLocation.lon != null ? broker.mailingLocation.lon : 0.0}" /> -->
					<input type="hidden" name="mailingLocation.rdi" id="rdi_mailing" value="${broker.mailingLocation.rdi != null ? broker.mailingLocation.rdi : ''}" />
					<input type="hidden" name="mailingLocation.county" id="county_mailing" value="${broker.mailingLocation.county != null ? broker.mailingLocation.county : ''}" />
				<div class="form-actions center-align-elements">
					<c:choose>					
						<c:when test="${broker.certificationStatus != 'Pending' && broker.id != 0}">
							<a class="btn" type="button" href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkCancel"/></a>
							<input type="button" name="SaveBrkCerti" id="SaveBrkCerti" value="<spring:message code="label.agent.employee.addNewComment.save-button"/>" class="btn btn-primary" />							
<!-- 							<a class="btn btn-primary btn-small" type="submit" id="save" name="save">Save</a> -->
						</c:when>
						<c:otherwise>
							<a class="btn" type="button" href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkCancel"/></a>
							<input type="button" name="SaveBrkCerti" id="SaveBrkCerti" value="<spring:message code="label.agent.employee.addNewComment.save-button"/>" class="btn btn-primary" />
						</c:otherwise>
					</c:choose>
				</div>
				</form>
				
				<!-- Modal for EIN information -->
			<div id="einModal" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true"
				role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p id="myModalLabel1">&nbsp;</p>
				</div>
				<div class="modal-body">
					<h3><spring:message  code='label.eininfotitle'/></h3>
					<p><spring:message  code='label.eininfodesc3'/>&nbsp;<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>&nbsp;<spring:message  code='label.eininfodesc4'/></p>
					<a href="http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/Apply-for-an-Employer-Identification-Number-(EIN)-Online"><spring:message  code='label.getnewein'/></a>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.close'/></button>
				</div>
			</div>
		</div>
						<input type="hidden" name="licenseNumberCheck" id="licenseNumberCheck" value="" />
						<input type="hidden" name="mailingAddressCheck" id="mailingAddressCheck" value="" />
	</div><!-- end of row fluid -->
</div>
					<!-- broker registration -->


	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p><spring:message code="label.brkBrokerEligibilityInfo"/></p>
			</div>
		</div>
	</div>

<script type="text/javascript">
/* $('#federalEIN').mask("00-0000000");

function maskEIN() {
  var pattern = /^(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$/;
  var federalEIN = $('#federalEIN').val().replace(/-/g,'');
  if(pattern.test(federalEIN)){
    $('#federalEIN').closest('div.controls').find('.errorsWrapper').hide();
    $('#federalEIN').attr('data-ein', federalEIN).val("**-*******");
    //$('#federalEIN').attr('data-ein', federalEIN).val("**-*******" + federalEIN.substring(5));
  }
} */

$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});
	function ie8Trim(){
        if(typeof String.prototype.trim !== 'function') {
                          String.prototype.trim = function() {
                            return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                          }
                        }
}

jQuery.validator.addMethod("licenseNumberCheck", function(value, element, param) {
	var brkId = ${broker.id};
	if(brkId!=0){
		return true;		
	}else{
	return $("#licenseNumberCheck").val() == 0 ? false : true;
}

});

jQuery.validator.addMethod("licenseNumberZeroCheck", function(value, element, param) {
	try
	  {
		var linNum= parseInt($("#licenseNumber").val());
		if(linNum == 0){
			return false;
		}else{
			return true;
		}
	  }
	catch(err)
	  {
		return false;
	  }
});

jQuery.validator.addMethod("communicationPreferenceValidator", function (value, element, param) {
	try {
		var communicationPreference = $("#communicationPreference").val();
		
		if(null == communicationPreference || "undefined" === communicationPreference || "Select".match(communicationPreference)) {
			return false;
		}
		else {
			return true;
		}
	}
	catch(err) {
		return false;
	}
});

var validator = $("#frmbrokerCertification").validate({ 
	onkeyup: false,
	onclick: false,
	rules : { 
			"user.firstName" : { required : true},
			"user.lastName" : { required : true},
			companyName : { required : true},
			"location.address1" : { required: true},			
			"location.state" : { required: true},
			"location.city" : { required: true},
			"location.zip" : {required: true,
				ZipCodecheck: true},
			"mailingLocation.address1" : { required: true},			
<c:if test="${CA_STATE_CODE}">			"mailingLocation.address2" :{maxlength: 25}, </c:if>
			"mailingLocation.state" : { required: true},
			"mailingLocation.city" : { required: true},
			"mailingLocation.zip" : {required: true,
			MailingZipCodecheck: true},
			phone3 : {phonecheck : true},
			communicationPreference : {communicationPreferenceValidator : true},
			businessContactPhone3 : { businesscontactnumbercheck : true},
			alternatePhone3 : { alternatephonenumbercheck : true},
			faxNumber3 : { faxnumbercheck: true,number : true},
			licenseNumber : { required: true, licenseNumberCheck: true, alphanumeric: true},
			federalEIN : { required: true,federalEINCheck : true},
			licrenewaldate : { licrenewaldateChkFormat : true, licrenewaldatePastChk: true},
			<c:if test="${CA_STATE_CODE}">personalEmailAddress : { required : true, validateEmail: true},</c:if>	
			"select_agencyLocation" :{validateAgencyLocationSelection:true}
	},
	messages : {
		"user.firstName" : {required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.bkr.validateFirstName' javaScriptEscape='true'/></span>"},
		"user.lastName" : {required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.bkr.validateLastName' javaScriptEscape='true'/></span>"},
		phone3: { phonecheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.bkr.validatePhoneNo' javaScriptEscape='true'/></span>"},
		businessContactPhone3: { businesscontactnumbercheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateBusinessContactNumber' javaScriptEscape='true'/></span>"},
		alternatePhone3: { alternatephonenumbercheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateAlternatePhoneNumber' javaScriptEscape='true'/></span>"},
		communicationPreference:{communicationPreferenceValidator : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='error.validateCommunicationPreferenceInvalid' javaScriptEscape='true'/></span>"},
		faxNumber3: { faxnumbercheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.bkr.validateFaxNo' javaScriptEscape='true'/></span>",
			number : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateFaxNumber' javaScriptEscape='true'/></span>"},
		companyName :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateCompanyName' javaScriptEscape='true'/></span>"},
		"location.address1" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},		
		"location.state" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"location.city" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"location.zip" : { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
			ZipCodecheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"},
		"mailingLocation.address1" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},		
<c:if test="${CA_STATE_CODE}">			"mailingLocation.address2" : {maxlength: "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>   <spring:message code='label.address2.maxlength' javaScriptEscape='true'/>    </span>" },   </c:if>
		"mailingLocation.state" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" :{ required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" : { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
				MailingZipCodecheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"},
		licenseNumber: { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateLicenseNumber' javaScriptEscape='true'/></span>",
		licenseNumberCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateLicenseNumberCheck' javaScriptEscape='true'/></span>",
		alphanumeric :"<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateLicenseNumberSpecialChar' javaScriptEscape='true'/></span>"},
		federalEIN: { required : "<span> <em class='excl'></em><spring:message code='label.validateFederalEIN' javaScriptEscape='true'/></span>",
			federalEINCheck : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateFederalEINCheck' javaScriptEscape='true'/></span>"},
		licrenewaldate : { licrenewaldateChkFormat : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='err.validateLicrenewaldate' javaScriptEscape='true'/></span>",
			licrenewaldatePastChk : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='err.validateLicrenewaldatePast' javaScriptEscape='true'/></span>"} , 
			<c:if test="${CA_STATE_CODE}"> personalEmailAddress : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePublicEmail' javaScriptEscape='true'/></span>",
				  validateEmail : "<span> <em class='excl'>!</em><spring:message code='label.validatePublicEmail' javaScriptEscape='true'/></span>"}, </c:if>
			"select_agencyLocation" :{validateAgencyLocationSelection:"<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please select Location</span>"}
			
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	try {
		//As phone number is mandatory no need to check for communication prefrence select box
		ie8Trim();
		var phone1 = $("#phone1").val().trim(); 
		var phone2 = $("#phone2").val().trim(); 
		var phone3 = $("#phone3").val().trim(); 
	
			if( (null != phone1 && null != phone2 && null != phone3) && 
					("undefined" !== phone1 && "undefined" !== phone2 && "undefined" !== phone3) &&
					(phone1 !== "" && phone2 !== "" && phone3 !== "") ) {
				return isValidPhoneNumber(phone1, phone2, phone3);
			}else {		
				return false;
			}
		
	}catch(err) {
		return false;
	}
		
});

jQuery.validator.addMethod("validateEmail", function(value, element) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if($.trim(value) == ""){
		return true;
	}
	if (filter.test(value)) {
	    return true;
	}
	else {
	    return false;
	}
});

		jQuery.validator.addMethod("validateAgencyLocationSelection", function(value, element, param) {
			<c:if test="${not empty broker.agencyId}">        
				if($("#select_agencyLocation")[0].selectedIndex == 0){
					 return false;
				}
			</c:if>		
			return true;
		});
			
/**
 * Validate the entered phone number.
 *
 * @param 1 The first three digits of phone number.
 * @param 2 The next three digits of phone number.
 * @param 3 The last four digits of phone number.
 * @return The status of phone number validation post validation check.
 */
function isValidPhoneNumber(phone1, phone2, phone3) {
	if(!isPositiveInteger(phone1) || !isPositiveInteger(phone2) || !isPositiveInteger(phone3) || 
		(isNaN(phone1)) || (phone1.length < 3 ) || (isNaN(phone2)) || (phone2.length < 3 ) || (isNaN(phone3)) || 
		(phone3.length < 4 ) || (phone1 == '000') ) {
			return false;
	}
	else {
		var phOneInt = parseInt(phone1);
		
		if(phOneInt < 100){
			return false;
		}
		return true;
	}
}

jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $('#zip_business').val().trim();
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || zip == '00000')){
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("licrenewaldatePastChk", function(value, element, param) {
	ie8Trim();
	var Startdate = value;
    if(value.length != 0 && value !='') {
        var licrenewaldateArr = value.split('-');
        licrenewaldateArr = licrenewaldateArr[0]+"/"+licrenewaldateArr[1]+"/"+licrenewaldateArr[2];
        var tempDate = new Date(licrenewaldateArr);
        var today = new Date();
        var day = today.getDate();
        var mon = today.getMonth()+1;
        var year = today.getFullYear();
        today = (mon+"/"+day+"/"+year);
        var today = new Date(today);
        if(today.getTime() > tempDate.getTime() )
        {
            return false;
        } 
        else {
            return true;
        }
    }
		
	return true;
});

jQuery.validator.addMethod("licrenewaldateChkFormat", function(value, element, param) {
	ie8Trim();
	// if license renewal date is not added do not validate anything
	if(value == '') {
		return true; 
	} 
	if(value != '') {
		return /^(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$/.test(value);
	}	
	return true;
});

jQuery.validator.addMethod("federalEINCheck", function(value, element, param) {
	var orig = '${broker.federalEIN}';
	if(orig === value){
		return true;	
	}
	
	if((value == "")  || !isPositiveInteger(value) || (isNaN(value)) || (value.length < 9 ) || (value.length > 9 )){
	// if((value == "")  || isPositiveInteger(value) || !(isNaN(value)) || (value.length < 10 ) || (value.length > 10 )){
		return false; 
	}
	return true;
}); 

jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip_mailing").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || zip == '00000' )){ 
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("businesscontactnumbercheck", function(value, element, param) {
	ie8Trim();
	businessContactPhone1 = $("#businessContactPhone1").val().trim(); 
	businessContactPhone2 = $("#businessContactPhone2").val().trim(); 
	businessContactPhone3 = $("#businessContactPhone3").val().trim(); 
 
	// This is added because business Contact Phone  is not mandatory
	if((businessContactPhone1.length == 0) && (businessContactPhone2.length == 0)  && (businessContactPhone3.length == 0)) {
		return true;
	}
	
	
	if( (null != businessContactPhone1 && null != businessContactPhone2 && null != businessContactPhone3) && 
			("undefined" !== businessContactPhone1 && "undefined" !== businessContactPhone2 && "undefined" !== businessContactPhone3) &&
			(businessContactPhone1 !== "" && businessContactPhone2 !== "" && businessContactPhone3 !== "") ) {
		
		if(isValidPhoneNumber(businessContactPhone1, businessContactPhone2, businessContactPhone3)){
			$("#businessContactPhoneNumber").val(businessContactPhone1 + "-" + businessContactPhone2 + "-" + businessContactPhone3);
			return true;
		}else{
			return false;
		}
		
	}else {		
		return false;
	}
	
		
});

jQuery.validator.addMethod("alternatephonenumbercheck", function(value, element, param) {
	ie8Trim();
	alternatePhone1 = $("#alternatePhone1").val().trim(); 
	alternatePhone2 = $("#alternatePhone2").val().trim(); 
	alternatePhone3 = $("#alternatePhone3").val().trim(); 
 
	// This is added because secondary number is not mandatory
	if((alternatePhone1.length == 0) && (alternatePhone2.length == 0)  && (alternatePhone3.length == 0)) {
		return true;
	}
	
	
	if( (null != alternatePhone1 && null != alternatePhone2 && null != alternatePhone3) && 
			("undefined" !== alternatePhone1 && "undefined" !== alternatePhone2 && "undefined" !== alternatePhone3) &&
			(alternatePhone1 !== "" && alternatePhone2 !== "" && alternatePhone3 !== "") ) {
		
		if(isValidPhoneNumber(alternatePhone1, alternatePhone2, alternatePhone3)){
			$("#alternatePhoneNumber").val(alternatePhone1 + "-" + alternatePhone2 + "-" + alternatePhone3);	
			return true;
		}else{
			return false;
		}
		
	}else {		
		return false;
	}
	
} );

jQuery.validator.addMethod("faxnumbercheck", function(value, element, param) {
	try {
		var currCommunicationPreference = $("#communicationPreference").val();
		
		ie8Trim();
		var faxnum1 = $("#faxNumber1").val().trim(); 
		var faxnum2 = $("#faxNumber2").val().trim(); 
		var faxnum3 = $("#faxNumber3").val().trim(); 
		
		if(null != currCommunicationPreference && "undefined" !== currCommunicationPreference && "Fax".match(currCommunicationPreference)) {
			if( null == faxnum1 || null == faxnum2 || null == faxnum3 
					|| "undefined" === faxnum1 || "undefined" === faxnum2 || "undefined" === faxnum3 
					|| (faxnum1.length == 0) || (faxnum2.length == 0)  || (faxnum3.length == 0)) {
				return false;
			}
				
			return isValidFaxNumber(faxnum1, faxnum2, faxnum3);
		}
		else {
			// This is added because fax number is not mandatory
			if( (null == faxnum1 && null == faxnum2 && null == faxnum3) 
					|| ("undefined" === faxnum1 && "undefined" === faxnum2 && "undefined" === faxnum3) 
					|| (faxnum1.length == 0) && (faxnum2.length == 0)  && (faxnum3.length == 0)) {
				return true;
			}
			else {
				return isValidFaxNumber(faxnum1, faxnum2, faxnum3);
			}
		}
	}
	catch(err) {
		return false;
	}		
} );

/**
 * Validate the entered fax number.
 *
 * @param fax1 The first three digits of fax number.
 * @param fax2 The next three digits of fax number.
 * @param fax3 The last four digits of fax number.
 * @return The status of fax number validation post validation check.
 */
function isValidFaxNumber(faxnum1, faxnum2, faxnum3) {
	/** If user has entered anything then test, if entered value is valid */
	if((faxnum1.length < 3 ) || !isPositiveInteger(faxnum1) || 
			(faxnum2.length < 3 ) || !isPositiveInteger(faxnum2) ||
			(faxnum3.length < 4 ) || !isPositiveInteger(faxnum3) || (faxnum1 == '000')){
		return false;
	} else {		
		$("#faxNumber").val(faxnum1 + "-" + faxnum2 + "-" + faxnum3);		
		return true;
	}	
}

jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
}); 

function  updatedSubmit() {
	
	$("input[id^=address2_]").each(function(){
		if($(this).val() === "Apt, Suite, Unit, Bldg, Floor, etc" || $(this).val() === "Address Line 2"){
			$(this).val("");
		}
	});		
	
	$("input[id^=address1_]").each(function(){
		if($(this).val() === "Street Name, P.O. Box, Company, c/o"){
			$(this).val("");
		}
	});		

	if($("#mailingAddressCheck").is(":checked")) {
		
		var strMailingAddress1;
		var strMailingAddress2;
		var strMailingCity;
		var strMailingState;
		var strMailingZip ;
		var strMailingRdi  ='';
		var strMailingCountry = '';
		
		<c:if test="${not empty broker.agencyId}">
		var selectedAgencylocation = $('#select_agencyLocation').val();
		$.each(siteJsonObj, function( index, value ) {
			  
			 if( selectedAgencylocation == value.id){
				 strMailingAddress1 = value.address1;
				 $("#address1_mailing").val(strMailingAddress1);
				 
				 strMailingAddress2 = value.address2;
				 $("#address2_mailing").val(strMailingAddress2);
				 
				 strMailingCity = value.city;
				$("#city_mailing").val(strMailingCity);		
				
				strMailingState = value.state;
				$("#state_mailing").val(strMailingState);
				$("#state_mailing_hidden").val(strMailingState);
				
				strMailingZip = value.zip;
				$("#zip_mailing").val(strMailingZip);
				
				//strMailingLat = value.lat;
				
				//strMailingLon = value.lon;
			 }
			});
		
		</c:if >
		
		
		<c:if test="${empty broker.agencyId}">
			strMailingAddress1 = $("#address1_business").val();
			$("#address1_mailing").val(strMailingAddress1);
			
			strMailingAddress2 = $("#address2_business").val();
			$("#address2_mailing").val(strMailingAddress2);
			
			strMailingCity = $("#city_business").val();
			$("#city_mailing").val(strMailingCity);
			
			strMailingState = $("#state_business").val();
			$("#state_mailing").val(strMailingState);
			
			strMailingZip = $("#zip_business").val();
			$("#zip_mailing").val(strMailingZip);
			
			strMailingRdi = $("#rdi_business").val();
			
			strMailingCountry = $("#county_business").val();
		</c:if >
		
		
		
		if("#" + strMailingAddress1 + "#" != "##" || strMailingAddress1 !== null || strMailingAddress1 !== undefined) {
			if(strMailingAddress1.length > 25) {
				<c:if test="${not empty broker.agencyId}">	
						$("#address1_mailing_error").show();
				</c:if>
			}else{
				if($("#address1_mailing_error").html() != null){
					$("#address1_mailing_error").html('');
				}
			}
		}else {
			$("#address1_mailing_error").show();
		}
		
		if(strMailingAddress2 !== null &&  (strMailingAddress2 !== "" ||  strMailingAddress2 !== undefined)) {
			if(strMailingAddress2.length > 25) {
				<c:if test="${not empty broker.agencyId}">	
						$("#address2_mailing_error").show();
				</c:if>
			}else{
				if($("#address2_mailing_error").html() != null){
					$("#address2_mailing_error").html('');
				}
			}
		}
		
		if(strMailingCity != "" || strMailingCity != undefined) {
			$("#city_mailing_error").hide();
		}else {
			$("#city_mailing_error").show();
		}
		
		if(strMailingState != "" || strMailingState != undefined) {
			$("#state_mailing_error").hide();
		}else {
			$("#state_mailing_error").show();
		}
		
		if(strMailingZip != "" || strMailingZip != undefined) {
			$("#zip_mailing_error").hide();
		}
		else {
			$("#zip_mailing_error").show();
		}
		
		/* var strMailingLat = $("#lat_business").val();
		if(strMailingLat != '') {
			$("#lat_mailing").val(strMailingLat);
		}else{
			$("#lat_business").val(0.0);
			$("#lat_mailing").val(0.0);
		};
		
		var strMailingLon = $("#lon_business").val();
		if(strMailingLon!='') {
			$("#lon_mailing").val(strMailingLon);
		}else{
			$("#lon_business").val(0.0);
			$("#lon_mailing").val(0.0);		
		}; */
		
		if(strMailingRdi!='') {
			$("#rdi_mailing").val(strMailingRdi);
		}else{
			$("#rdi_business").val('');
			$("#rdi_mailing").val('');		
		};
		
		if(strMailingCountry!='') {
			$("#county_mailing").val(strMailingCountry);
		}else{
			$("#county_business").val("");
			$("#county_mailing").val("");		
		};
			
		
		$("#address1_mailing").attr('readonly', true);
		$("#address2_mailing").attr('readonly', true);
		$("#city_mailing").attr('readonly', true);
		$("#state_mailing").attr('disabled', true);
		$("#zip_mailing").attr('readonly', true);
		
	}else{
		$("#address1_mailing").val("");
		$("#address2_mailing").val("");
		$("#city_mailing").val("");
		$("#state_mailing").val("");
		$("#zip_mailing").val("");
		$("#lat_mailing").val(0.0);
		$("#lon_mailing").val(0.0);
		$("#rdi_mailing").val('');
		$("#county_mailing").val("");
		
		$("#address1_mailing").removeAttr("readonly"); 
		$("#address2_mailing").removeAttr("readonly"); 
		$("#city_mailing").removeAttr("readonly"); 
		$("#state_mailing").removeAttr("disabled"); 
		$("#zip_mailing").removeAttr("readonly"); 
		
		if($("#address1_mailing_error").html() != null){
			$("#address1_mailing_error").html('');
		}
		if($("#address2_mailing_error").html() != null){
			$("#address2_mailing_error").html('');
		}
		$("#address1_mailing_error").show();
		$("#state_mailing_error").show();
		$("#zip_mailing_error").show();
		$("#city_mailing_error").show();
		
	}; 
};


$(function() {
	$("#SaveBrkCerti").click(function(e){
		/*to check the text for address line 2 for IE browsers*/
		$("input[id^=address2_]").each(function(){
			if($(this).val() === "Apt, Suite, Unit, Bldg, Floor, etc" || $(this).val() === "Address Line 2"){
				$(this).val("");
			}
		});		
		
		$("input[id^=address1_]").each(function(){
			if($(this).val() === "Street Name, P.O. Box, Company, c/o"){
				$(this).val("");
			}
		});	
		
		<c:if test="${  empty broker.agencyId}">
			if($("#mailingAddressCheck").is(":checked")) {
				var strMailingAddress1 = $("#address1_business").val();
				$("#address1_mailing").val(strMailingAddress1);
				
				var strMailingState = $("#state_business").val();
				$("#state_mailing").val(strMailingState);
				
				var strMailingAddress2 = $("#address2_business").val();
				$("#address2_mailing").val(strMailingAddress2);
				
				var strMailingCity = $("#city_business").val();
				$("#city_mailing").val(strMailingCity);
				
				var strMailingZip = $("#zip_business").val();
				$("#zip_mailing").val(strMailingZip);
			}
		</c:if>
		
		
		if($("#frmbrokerCertification").validate().form()) {
			$("#frmbrokerCertification").attr("action","/hix/admin/broker/certificationapplication");
			$("#frmbrokerCertification").submit();
		}
	});
});

$(function() {
	$( "#federalEIN").click(function() {
			if( !${CA_STATE_CODE}  ){
				$( "#federalEIN").val('');
			}
		});
});

function isPositiveInteger(s)
{
    return /^\d+$/.test(s);
}



/*placeholder hack for Internet Explorer
$('[placeholder]').focus(function() {
  var input = $(this);
  if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
  }
}).blur(function() {
  var input = $(this);
  if (input.val() == '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));
  }
}).blur();
*/
/*focus out event for the text input to call the lightbox*/
$('.zipCode').focusout(function(e) {
	var startindex = (e.target.id).indexOf("_");
	var index = (e.target.id).substring(startindex,(e.target.id).length);
	var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
	var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
	address1_e=address1_e+index;
	address2_e=address2_e+index;
	city_e=city_e+index;
	state_e=state_e+index;
	zip_e=zip_e+index;
	lat_e=lat_e+index;
	lon_e=lon_e+index;
	rdi_e+=index;
	county_e+=index;
	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
	
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != '')){
		if(($('#'+ address2_e).val())==="Apt, Suite, Unit, Bldg, Floor, etc" || ($('#'+ address2_e).val()) === "Address Line 2"){
			$('#'+ address2_e).val('');
		}	
		viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);	
	}
});

/*if error message is getting displayed on page*/
$('#zip_business_error label, #zip_mailing_error label').hide()

$('#communicationPreference').change(function(){
	if($(this).val() === 'Fax'){
		$('#faxReq').removeClass('hide');
	}else{
		$('#faxReq').addClass('hide');
	}
});

							<c:if test="${ NOSERVICE == 'TRUE' or  AMWONTAVAILABLE == 'TRUE' }">
							
								function hideNoservice(){
									<c:if test="${ NOSERVICE == 'TRUE' }">
										document.getElementById('noServiceErrMsg').hidden = "hidden"; 
									</c:if >
									 
									<c:if test="${ AMWONTAVAILABLE == 'TRUE' }">
										showAmAvailable();
									</c:if > 
								}

								function showNoservice(){
									 
									<c:if test="${ NOSERVICE == 'TRUE' }">
										document.getElementById('noServiceErrMsg').hidden = ""; 
									</c:if >
									<c:if test="${ AMWONTAVAILABLE == 'TRUE' }">
										hideAmAvailable();
									</c:if >

								}

							</c:if >

							<c:if test="${ AMWONTAVAILABLE == 'TRUE' }">
								function hideAmAvailable(){
									 document.getElementById('noAmAvailable').hidden = "hidden";  
								}

								function showAmAvailable(){
									 document.getElementById('noAmAvailable').hidden = "";
								}
								
								
							</c:if >

</script>


<form name="dialogForm" id="dialogForm" action="<c:url value="/admin/broker/dashboard" />" novalidate="novalidate">
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"><spring:message code="label.ViewAgentAccount"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					<spring:message code="label.bkr.AgentViewPopup"/> ${broker.user.firstName}.<br/>
					<spring:message code="label.bkr.AgentSwitchPopup"/><br/>
					<spring:message code="label.bkr.ProceedtoAgentView"/>
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkAgentView" name="checkAgentView"  > 
    <div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
   <button class="btn btn-primary" type="submit"><spring:message code="label.bkr.agentView"/></button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="broker" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${broker.id}"/>" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${broker.user.firstName}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
