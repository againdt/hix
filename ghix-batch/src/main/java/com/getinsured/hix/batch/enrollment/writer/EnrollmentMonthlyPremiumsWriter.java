package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;


public class EnrollmentMonthlyPremiumsWriter implements ItemWriter<Integer> {
	int partition;
	private ExecutionContext executionContext;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPremiumsWriter.class);
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		executionContext = stepExecution.getJobExecution().getExecutionContext();
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
		}
	}

	@Override
	public void write(List<? extends Integer> enrollmentIdFromWriter) throws Exception {
		List<Integer> enrollmentIdList= null;
		try{
			if(enrollmentIdFromWriter!=null && enrollmentIdFromWriter.size()>0){
				enrollmentIdList= new ArrayList<Integer>(enrollmentIdFromWriter);
				LOGGER.info("EnrollmentMonthlyPremiumsWriter :: Enrollment ID List for partition "+partition+" :: " + enrollmentIdList);
				//TODO Call Update Service

			}
		}catch(Exception e){
			LOGGER.error("Exception occurred in EnrollmentMonthlyPremiumsWriter: ", e);
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
		}
	}
}
