package com.getinsured.eligibility.indportal.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.indportal.CreateDTOBean;
import com.getinsured.eligibility.indportal.customGrouping.CustomGroupingController.EligibilityType;
import com.getinsured.eligibility.indportal.dto.HouseholdMemberDTO;
import com.getinsured.eligibility.indportal.dto.IndividualDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO;
import com.getinsured.eligibility.indportal.dto.SsapApplicantsDataDTO;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal;
import com.getinsured.iex.dto.SsapApplicantDto;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.ContactPreferences;
import com.getinsured.iex.ssap.Ethnicity;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SocialSecurityCard;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

@Component
public class IndportalUtil {

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired private IEligibilityProgram eligibilityProgramRepository;
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired private CreateDTOBean createDTOBean;	
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	
	private static final Logger LOGGER = Logger.getLogger(IndportalUtil.class);
	
	
	public HouseholdMemberDTO getHouseholdMemberDTO(String ssapJson) {
		HouseholdMemberDTO householdMemberDTO = new HouseholdMemberDTO();
		if (StringUtils.isNotBlank(ssapJson)) {
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapJson);
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {
				if (householdMember.getPersonId() == 1) {
					householdMemberDTO.setId(householdMember.getApplicantGuid());
					populateHoueholdMemberName(householdMember.getName(), householdMemberDTO);
					populateSocialSecurityNumber(householdMember.getSocialSecurityCard(), householdMemberDTO);						
					if(householdMember.getHouseholdContact() != null){
						populatePhone(householdMember.getHouseholdContact(), householdMemberDTO);
						populatePreferredEmail(householdMember.getHouseholdContact().getContactPreferences(), householdMemberDTO);						
						if( householdMember.getHouseholdContact().getHomeAddress() != null) {
							populateHomeAddress(householdMember.getHouseholdContact().getHomeAddress(), householdMemberDTO);							
						}
					}					
					householdMemberDTO.setType("QD");
				}
			}
		}
		return householdMemberDTO;
	}
	
	public IndividualDTO setIndividualDTO(String objectType, SsapApplication ssapApplication, Date coverageStartDate, com.getinsured.iex.dto.ind19.Ind19ClientRequest ind19ClientRequest, SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {
		IndividualDTO individualDTO = new IndividualDTO();
		if(ssapApplication.getCmrHouseoldId() != null){
			individualDTO.setHouseholdCaseId(ssapApplication.getCmrHouseoldId().longValue());
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");			
		individualDTO.setCoverageStartDate(dateFormat.format(coverageStartDate));
		
		if(ind19ClientRequest.getFinancialEffectiveDate() != null){
			individualDTO.setFinancialEffectiveDate(dateFormat.format(ind19ClientRequest.getFinancialEffectiveDate()));
		}		
					
		
		if("IND19".equalsIgnoreCase(objectType)){
			String applicationType = ssapApplication.getApplicationType();
			if((ind19ClientRequest.getIsChangePlanFlow() || ind19ClientRequest.getIsSEPPlanFlow()) || (StringUtils.equalsIgnoreCase(applicationType,"SEP"))){
				individualDTO.setEnrollmentType("S");
			}
			else if(StringUtils.isEmpty(applicationType) || 
					StringUtils.equalsIgnoreCase(applicationType,"OE") ||
					StringUtils.equalsIgnoreCase(applicationType,"QEP")){
				individualDTO.setEnrollmentType("I");
			}			
			
			
			if(ssapApplicationEligibilityDTO != null){
				if(ssapApplicationEligibilityDTO.getMaxAPTCAmount()!=null){
					individualDTO.setAptc((ssapApplicationEligibilityDTO.getMaxAPTCAmount().floatValue()));
				}
				if(ssapApplicationEligibilityDTO.getCsrLevel()!=null && ssapApplicationEligibilityDTO.getCsrLevel().trim().length()>0){
					individualDTO.setCsr(ssapApplicationEligibilityDTO.getCsrLevel());
				}
			}
		}else{
			individualDTO.setEnrollmentType("S");
			if(ssapApplication.getMaximumAPTC() != null) {
				individualDTO.setAptc(ssapApplication.getMaximumAPTC().floatValue());
			}
			
			individualDTO.setCsr(ssapApplication.getCsrLevel() != null ? ssapApplication.getCsrLevel() : "CS1");
		}	
		
		AptcHistory aptcHistory = aptcHistoryRepository.findLatestSlcspAmount(ssapApplication.getId()); 
		if(aptcHistory !=null && aptcHistory.getSlcspAmount() != null) {
		individualDTO.setEhbAmount(new Float(aptcHistory.getSlcspAmount().toString()));
		}

		return individualDTO;
	}
	
	private void populatePreferredEmail(ContactPreferences contactPreferences, HouseholdMemberDTO householdMemberDTO) {
		if( contactPreferences != null 
				&& ("EMAIL".equalsIgnoreCase(contactPreferences.getPreferredContactMethod())
						|| "POSTAL EMAIL".equalsIgnoreCase(contactPreferences.getPreferredContactMethod()))) {
			householdMemberDTO.setPreferredEmail(nullCheckedValue(contactPreferences.getEmailAddress()));
		}		
	}

	private void populateHomeAddress(HomeAddress homeAddress, HouseholdMemberDTO householdMemberDTO) {
		householdMemberDTO.setHomeAddress1(nullCheckedValue(homeAddress.getStreetAddress1()));
		householdMemberDTO.setHomeAddress2(nullCheckedValue(homeAddress.getStreetAddress2()));
		householdMemberDTO.setHomeCity(nullCheckedValue(homeAddress.getCity()));
		householdMemberDTO.setHomeState(nullCheckedValue(homeAddress.getState()));
		householdMemberDTO.setHomeZip(nullCheckedValue(homeAddress.getPostalCode()));		
	}

	private void populatePhone(HouseholdContact householdContact, HouseholdMemberDTO householdMemberDTO) {
		if((householdContact.getPhone() != null && StringUtils.isNotBlank(householdContact.getPhone().getPhoneNumber())) 
				&& (householdContact.getOtherPhone() != null && StringUtils.isNotBlank(householdContact.getOtherPhone().getPhoneNumber()))){
			householdMemberDTO.setPrimaryPhone(householdContact.getPhone().getPhoneNumber());
			householdMemberDTO.setSecondaryPhone(householdContact.getOtherPhone().getPhoneNumber());
		}else if((householdContact.getPhone() != null && StringUtils.isNotBlank(householdContact.getPhone().getPhoneNumber())) 
				&& (householdContact.getPhone() == null || StringUtils.isBlank(householdContact.getPhone().getPhoneNumber()))){
			householdMemberDTO.setPrimaryPhone(householdContact.getPhone().getPhoneNumber());
		} else if((householdContact.getPhone() == null || StringUtils.isBlank(householdContact.getPhone().getPhoneNumber()))
				&& (householdContact.getOtherPhone() != null && StringUtils.isNotBlank(householdContact.getOtherPhone().getPhoneNumber()))){
			householdMemberDTO.setPrimaryPhone(householdContact.getOtherPhone().getPhoneNumber());
		}
	}

	private void populateSocialSecurityNumber(SocialSecurityCard socialSecurityCard, HouseholdMemberDTO householdMemberDTO) {
		if(socialSecurityCard != null && StringUtils.isNotBlank(socialSecurityCard.getSocialSecurityNumber())){
			householdMemberDTO.setSsn(socialSecurityCard.getSocialSecurityNumber());
		}		
	}

	public List<MemberDTO> getMembersDTO(String ssapJson, String objectType,  Map<Long, SsapApplicantResource> ssapApplicants, Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap
			, boolean issep, String exemptHousehold, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String phoneNumber){
		List<MemberDTO> memberDTOList = new ArrayList<>();
		if (StringUtils.isNotBlank(ssapJson)) {			
						
			Map<Long, Long> ssapApplicantsPersonIdMap = convertToPersonIdPKeyMap(ssapApplicants);
						
			List<SsapApplicantsDataDTO> ssapApplicantsDataList = populateSsapApplicantsDataDTO(ssapApplicants);
						
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapJson);
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {
				MemberDTO memberDTO = new MemberDTO();
				Integer personId = householdMember.getPersonId();
				memberDTO.setMemberId(String.valueOf(personId));
				String applicantGuid = householdMember.getApplicantGuid();				
				
				populateHoueholdMemberName(householdMember.getName(), memberDTO);
				populateSocialSecurityNumber(householdMember.getSocialSecurityCard(), memberDTO);
				
				if(householdMember.getDateOfBirth() != null){
					memberDTO.setDob(dateFormatString(householdMember.getDateOfBirth()));
				}
				if(StringUtils.isNotBlank(householdMember.getGender())){
					if ("male".equalsIgnoreCase(householdMember.getGender())) {
						memberDTO.setGenderCode("M");
					} else if ("female".equalsIgnoreCase(householdMember.getGender())) {
						memberDTO.setGenderCode("F");
					}
				}
				
				memberDTO.setMaritalStatusCode("R");
				if(householdMember.getMarriedIndicator() != null && householdMember.getMarriedIndicator()){
					memberDTO.setMaritalStatusCode("M");					
				}
				
								
				
				if(householdMember.getSocialSecurityCard() != null && StringUtils.isNotBlank(householdMember.getSocialSecurityCard().getSocialSecurityNumber())){
					memberDTO.setSsn(householdMember.getSocialSecurityCard().getSocialSecurityNumber());
				}
				
				if(householdMember.getLivesWithHouseholdContactIndicator() !=null){
					if(householdMember.getLivesWithHouseholdContactIndicator()){
						if( householdMember.getHouseholdContact() != null && householdMember.getHouseholdContact().getHomeAddress() != null) { 
							populateHomeAddress(householdMember.getHouseholdContact().getHomeAddress(), memberDTO);
							memberDTO.setCountyCode(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode()));
						}
					}else{
						Address otherAddress = new Address();
						if(householdMember.getOtherAddress() != null && householdMember.getOtherAddress().getAddress() != null && householdMember.getOtherAddress().getAddress().getPostalCode() != null){
							otherAddress = householdMember.getOtherAddress().getAddress();	
						}else if(applicationData.getTaxHousehold().get(0).getOtherAddresses() != null && applicationData.getTaxHousehold().get(0).getOtherAddresses().size() > 0){	
							for(Address otherAddressObj : applicationData.getTaxHousehold().get(0).getOtherAddresses()) {
								if(otherAddressObj.getAddressId() == householdMember.getAddressId()) {
									otherAddress = otherAddressObj;
								}
							}							
						}
						memberDTO.setHomeAddress1(nullCheckedValue(otherAddress.getStreetAddress1()));
						memberDTO.setHomeAddress2(nullCheckedValue(otherAddress.getStreetAddress2()));
						memberDTO.setHomeCity(nullCheckedValue(otherAddress.getCity()));
						memberDTO.setHomeState(nullCheckedValue(otherAddress.getState()));
						memberDTO.setHomeZip(nullCheckedValue(otherAddress.getPostalCode()));
						memberDTO.setCountyCode(nullCheckedValue(otherAddress.getCountyCode()));
					}
					
				}
								
				if(householdMember.getHouseholdContact() != null){
					populatePhone(householdMember.getHouseholdContact(), memberDTO);
					
					if("IND19".equals(objectType)){	
						if(StringUtils.isBlank(memberDTO.getPrimaryPhone())){
							//HIX-58645 Send Primary Contact Phone number in IND 19
							memberDTO.setPrimaryPhone(phoneNumber);
						}						
						populateMailingAddress(householdMember.getHouseholdContact(), memberDTO);
					}					
				}
					
				if (personId == 1) {
					
					populateRelationShip(householdMember.getBloodRelationship(), memberDTO, ssapApplicantsDataList, objectType);						
						
					if(householdMember.getHouseholdContact() != null){
						if(householdMember.getHouseholdContact().getContactPreferences() != null){
							populatePreferredEmail(householdMember.getHouseholdContact().getContactPreferences(), memberDTO);
							
							if(StringUtils.isNotBlank(householdMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage())) {
								if("English".equalsIgnoreCase(householdMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage())){
									memberDTO.setSpokenLanguageCode("eng");
								}else if("Spanish".equalsIgnoreCase(householdMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage())){
									memberDTO.setSpokenLanguageCode("spa");
								}								
							}
							
							if(StringUtils.isNotBlank(householdMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage())) {
								if("English".equalsIgnoreCase(householdMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage())){
									memberDTO.setWrittenLanguageCode("eng");
								}else if("Spanish".equalsIgnoreCase(householdMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage())){
									memberDTO.setWrittenLanguageCode("spa");
								}								
							}
						}						
						
						if("IND57".equalsIgnoreCase(objectType)){
							populateMailingAddress(householdMember.getHouseholdContact(), memberDTO);
						}						
					}											
				}						
				
				long personIdInLong = personId;
				if ((!ssapApplicantsPersonIdMap.isEmpty() && ssapApplicantsPersonIdMap.get(personIdInLong)!=null) && (ssapApplicants != null && ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong))!=null && ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getTobaccouser()!=null && ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getTobaccouser().equalsIgnoreCase("Y"))) {
					memberDTO.setTobacco("Y");
				} else {
					memberDTO.setTobacco("N");
				}				
				
				populateRacesAndEthnicities(householdMember.getEthnicityAndRace(), memberDTO);
				
				if("IND57".equals(objectType)){
					memberDTO.setMaintenanceReasonCode("25");
				}else if("IND19".equals(objectType)){					
					memberDTO.setNewPersonFLAG("N");
					if(issep){
						memberDTO.setMaintenanceReasonCode("AI");
						if(!ssapApplicantEventPrimaryKeyMap.isEmpty() && !ssapApplicantsPersonIdMap.isEmpty() && ssapApplicantsPersonIdMap.get(personIdInLong) != null 
								&& null!=ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)) 
								&& ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents()!=null){
							
							if(StringUtils.isNotBlank(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getChangeType()) 
									&& ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getChangeType().equalsIgnoreCase("ADD")){
								memberDTO.setNewPersonFLAG("Y");
							}
							if(StringUtils.isNotBlank(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getMrcCode())){
								memberDTO.setMaintenanceReasonCode(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getMrcCode().trim());
							}
						}
						
						Integer existingMedicalEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "health");
						Integer existingSADPEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "dental");
						Set<String> healthPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "health");
						Set<String> dentalPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "dental");
						String healthEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "health");
						String dentalEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "dental");
						
						if(!"Y".equalsIgnoreCase(memberDTO.getNewPersonFLAG())){
							if(existingMedicalEnrollmentId != null && healthPlanEnrollees != null && healthPlanEnrollees.contains(applicantGuid) &&
							        !StringUtils.equalsIgnoreCase(healthEnrollmentStatus, "cancelled")){
								memberDTO.setExistingMedicalEnrollmentID(String.valueOf(existingMedicalEnrollmentId));
							}
							if(existingSADPEnrollmentId != null && dentalPlanEnrollees != null && dentalPlanEnrollees.contains(applicantGuid) &&
							        !StringUtils.equalsIgnoreCase(dentalEnrollmentStatus, "cancelled")){
								memberDTO.setExistingSADPEnrollmentID(String.valueOf(existingSADPEnrollmentId));
							}
						}					
					}
					
					memberDTO.setFinancialHardshipExemption("N");
					if(StringUtils.isNotBlank(exemptHousehold) && exemptHousehold.equalsIgnoreCase("Y")){
						memberDTO.setFinancialHardshipExemption("Y");
					}
					
					memberDTO.setCatastrophicEligible("N");
					if(StringUtils.isNotBlank(exemptHousehold) && exemptHousehold.equalsIgnoreCase("Y")){
						memberDTO.setCatastrophicEligible("Y");
					}
					
					memberDTO.setChildOnlyPlanEligibile("N");
				}
				memberDTOList.add(memberDTO);
			}
		}
		
		return memberDTOList;
	}	
	
	

	private void populateMailingAddress(HouseholdContact householdContact, MemberDTO memberDTO) {
		if(householdContact.getMailingAddressSameAsHomeAddressIndicator() != null){
			if(householdContact.getMailingAddressSameAsHomeAddressIndicator()){
				if( householdContact.getHomeAddress() != null) { 
					memberDTO.setMailingAddress1(nullCheckedValue(householdContact.getHomeAddress().getStreetAddress1()));
					memberDTO.setMailingAddress2(nullCheckedValue(householdContact.getHomeAddress().getStreetAddress2()));
					memberDTO.setMailingCity(nullCheckedValue(householdContact.getHomeAddress().getCity()));
					memberDTO.setMailingState(nullCheckedValue(householdContact.getHomeAddress().getState()));
					memberDTO.setMailingZip(nullCheckedValue(householdContact.getHomeAddress().getPostalCode()));
				}
			}else if(!householdContact.getMailingAddressSameAsHomeAddressIndicator()){
				if(householdContact.getMailingAddress() != null){
					memberDTO.setMailingAddress1(nullCheckedValue(householdContact.getMailingAddress().getStreetAddress1()));
					memberDTO.setMailingAddress2(nullCheckedValue(householdContact.getMailingAddress().getStreetAddress2()));
					memberDTO.setMailingCity(nullCheckedValue(householdContact.getMailingAddress().getCity()));
					memberDTO.setMailingState(nullCheckedValue(householdContact.getMailingAddress().getState()));
					memberDTO.setMailingZip(nullCheckedValue(householdContact.getMailingAddress().getPostalCode()));
				}
			}							
		}		
	}

	private void populateRelationShip(List<BloodRelationship> bloodRelationshipList, MemberDTO memberDTO, List<SsapApplicantsDataDTO> ssapApplicantsDataList, String objectType){
		List<MemberDTO.Relationships> relationships = new ArrayList<MemberDTO.Relationships>(1);
		if(bloodRelationshipList != null && !bloodRelationshipList.isEmpty()){
			for (BloodRelationship bloodRelationship : bloodRelationshipList) {
				MemberDTO.Relationships releation = new MemberDTO().new Relationships();
				releation.setIndividualId(bloodRelationship.getIndividualPersonId());
				releation.setMemberId(bloodRelationship.getRelatedPersonId());
				releation.setRelationshipCode(bloodRelationship.getRelation());
				relationships.add(releation);
			}			
		}
		memberDTO.setRelationships(relationships);
		
	}
	

	public String getEnrollmentStatus(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		return enrollmentShopDTO != null ? enrollmentShopDTO.getEnrollmentStatusLabel() : null;
	}

	public Set<String> getEnrollees(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		Set<String> ernollees  = new HashSet<String>();
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		if(enrollmentShopDTO != null){
			for(EnrolleeShopDTO enrollee: enrollmentShopDTO.getEnrolleeShopDTOList()){
				ernollees.add(enrollee.getExchgIndivIdentifier());
			}					
		}
		return ernollees;
	}

	public Integer getEnrollmentId(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		return enrollmentShopDTO != null ? enrollmentShopDTO.getEnrollmentId() : null;
	}

	private void populateHoueholdMemberName(Name name, HouseholdMemberDTO memberDTO) {
		if(null != name){
			if(StringUtils.isNotBlank(name.getFirstName())){
				memberDTO.setFirstName(name.getFirstName());
			}
			if(StringUtils.isNotBlank(name.getLastName())){
				memberDTO.setLastName(name.getLastName());
			}
			if(StringUtils.isNotBlank(name.getMiddleName())){
				memberDTO.setMiddleName(name.getMiddleName());
			}
			if(StringUtils.isNotBlank(name.getSuffix())){
				memberDTO.setSuffix(name.getSuffix());
			}
		}		
	}

	private void populateRacesAndEthnicities(EthnicityAndRace ethnicityAndRace, MemberDTO memberDTO) {
		if(ethnicityAndRace != null) {
			List<Ethnicity> ethnicities = ethnicityAndRace.getEthnicity();
			List<Race> races = ethnicityAndRace.getRace();
			
			if(races != null) {
				for (Race race : races) {
					MemberDTO.Race raceOjb = new MemberDTO().new Race();
					raceOjb.setRaceEthnicityCode(race.getCode());	
					if("0000-0".equalsIgnoreCase(race.getCode())){
						continue;
					}
					if("2131-1".equalsIgnoreCase(race.getCode())){
						raceOjb.setRaceEthnicityDescription(race.getOtherLabel());
					}
					memberDTO.getRace().add(raceOjb);
				}
			}
			
			if(ethnicities != null) {
				for (Ethnicity ethnicity : ethnicities) {
					MemberDTO.Race raceOjb = new MemberDTO().new Race();
					raceOjb.setRaceEthnicityCode(ethnicity.getCode());	
					if("0000-0".equalsIgnoreCase(ethnicity.getCode())){
						continue;
					}
					if("2131-1".equalsIgnoreCase(ethnicity.getCode())){
						raceOjb.setRaceEthnicityDescription(ethnicity.getOtherLabel());
					}
					memberDTO.getRace().add(raceOjb);
				}
			}
			
		}
	}
	
	private String nullCheckedValue(String value) {
		if(StringUtils.isBlank(value)) {
			return null;
		}
		return value;
	}
	
	private static String dateFormatString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		return simpleDateFormat.format(date);
	}
	
	public List<SsapApplicantsDataDTO> populateSsapApplicantsDataDTO(Map<Long, SsapApplicantResource> ssapApplicants) {
		List<SsapApplicantsDataDTO> ssapApplicantsDataList = new ArrayList<>();
		if(ssapApplicants != null && !ssapApplicants.isEmpty()){
			for(Map.Entry<Long, SsapApplicantResource> key:ssapApplicants.entrySet()){
				SsapApplicantsDataDTO ssapApplicantsDataDTO = new SsapApplicantsDataDTO();
				ssapApplicantsDataDTO.setId(key.getKey());
				ssapApplicantsDataDTO.setPersonId(key.getValue().getPersonId());
				ssapApplicantsDataDTO.setGuid(key.getValue().getApplicantGuid());
				ssapApplicantsDataList.add(ssapApplicantsDataDTO);
			}
		}	
		return ssapApplicantsDataList;
	}
	
	
	
	public Set<String> createSetOfApplicantIds(String ssapJson) {
		Set<String> applicantSet = new HashSet<String>();
		if (StringUtils.isNotBlank(ssapJson)) {
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapJson);
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {
				if(householdMember.getApplyingForCoverageIndicator()){
					applicantSet.add(householdMember.getApplicantGuid());
				}
			}
		}
		return applicantSet;
	}

	
	private Map<Long, Long> convertToPersonIdPKeyMap(Map<Long, SsapApplicantResource> ssapApplicants){
		Map<Long, Long> ssapApplicantsPersonIdMap = new HashMap<Long, Long>();		
		if(ssapApplicants != null && !ssapApplicants.isEmpty()){
			for(Map.Entry<Long, SsapApplicantResource> key:ssapApplicants.entrySet()){
				ssapApplicantsPersonIdMap.put(key.getValue().getPersonId(), key.getKey());				
			}
		}		
		return ssapApplicantsPersonIdMap;
	}

	public void setMemberOtherData(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, List<Member> memberList) {
		if(memberList != null){
			Integer existingMedicalEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "health");
			Integer existingSADPEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "dental");
			Set<String> healthPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "health");
			Set<String> dentalPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "dental");
			String healthEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "health");
			String dentalEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "dental");
			
			for(IndividualInformationRequest.Household.Members.Member member:memberList){
				//Add MRC code of AI - no reason given
				member.setMaintenanceReasonCode("AI");
				//Existing enrollment Ids
				if(existingMedicalEnrollmentId != null && existingMedicalEnrollmentId != 0 && 
				        !StringUtils.equalsIgnoreCase(healthEnrollmentStatus, "cancelled")){
					if(healthPlanEnrollees.contains(member.getMemberId())){
						member.setExistingMedicalEnrollmentID(String.valueOf(existingMedicalEnrollmentId));
						//New person flag
						member.setNewPersonFLAG(YesNoVal.N);
					}
				}
				if(existingSADPEnrollmentId != null && existingSADPEnrollmentId != 0 
				        && !StringUtils.equalsIgnoreCase(dentalEnrollmentStatus, "cancelled")){
					if(dentalPlanEnrollees.contains(member.getMemberId())){
						member.setExistingSADPEnrollmentID(String.valueOf(existingSADPEnrollmentId));
						//New person flag
						member.setNewPersonFLAG(YesNoVal.N);
					}
				}
			}
		}	
	}
	public List<SsapApplicantDto> getEligibleApplicantDetails(Long ssapApplicantionId) throws GIException {
		List<SsapApplicantDto> filteredSsapApplicants = new ArrayList<SsapApplicantDto>();
		
		if(ssapApplicantionId != null){
			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplicantionId);
			for(SsapApplicant ssapApplicant : ssapApplicants){
				if("Y".equals(ssapApplicant.getApplyingForCoverage())){
					List<EligibilityProgram> programs = getProgramEligibility(ssapApplicant);
					if(checkEligibility(programs, EligibilityType.EXCHANGE_ELIGIBILITY_TYPE)){
						SsapApplicantDto dto = new SsapApplicantDto();
						try {
							createDTOBean.copyProperties(dto,ssapApplicant);
						} catch (Exception e) {
							e.printStackTrace();
						}
						filteredSsapApplicants.add(dto);
					}
				}
			}
		}
		return filteredSsapApplicants;
	}
	
	private boolean checkEligibility(List<EligibilityProgram> programs,EligibilityType eligibilityType){
		if(programs != null && programs.size() > 0) {
			for(EligibilityProgram eligibilityProgram : programs){
				if (eligibilityType.toString().equals(eligibilityProgram.getEligibilityType()) && "TRUE".equals(eligibilityProgram.getEligibilityIndicator())){	
					return true;
				}
			}
		}
		return false;
	}
	
	private List<EligibilityProgram> getProgramEligibility(SsapApplicant ssapApplicant) {
		return eligibilityProgramRepository.getApplicantEligibilities(ssapApplicant.getId());
	}
}
