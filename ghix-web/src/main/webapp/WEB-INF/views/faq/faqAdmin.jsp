<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<style>
.tall {
	min-height: 300px;
	margin-top: 50px;
}
</style>
<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a>Administrator FAQs</h1>
		<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
			<df:csrfToken/>
			<ul class="nav nav-list faq-nav" id="nav">
						<li><a href="#CSRAdminManageTicket"><spring:message code="label.CSRAdminManageTicket" htmlEscape="false"/></a></li>
						<li><a href="#CSRAdminAdmins"><spring:message code="label.CSRAdminAdmins" htmlEscape="false"/></a></li>
					</ul>
		</div>
		<div class="span9 " id="rightpanel">
		
		
			<!-------------------------------group 1----------------------------------------------->
			<div id="CSRAdminManageTicket">
				<div class="header">
					<h4><spring:message code="label.CSRAdminManageTicket" htmlEscape="false"/></h4>	
				</div>
				<!--------------------------- question 1 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminHowToCreateTickets" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminCreateTickets" htmlEscape="false"/></p>
	  			<p><spring:message code="label.CSRAdminSubject" htmlEscape="false"/></p>
				
				<!--------------------------- question 2 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminNotClearTicket" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminAnswerNotClearTicket" htmlEscape="false"/></p>
				
				<!--------------------------- question 3 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminWhatTicketsCanBeCreated" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminTicketsCreated" htmlEscape="false"/></p>
				
				<!--------------------------- question 4 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminHowTicketsUse" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminTicketsUse" htmlEscape="false"/></p>
				
				<!--------------------------- question 5 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminWhatTicketWorkflow" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminTicketWorkflow" htmlEscape="false"/></p>
	  			
			</div>
			
			
			
			<!-------------------------------group 2----------------------------------------------->
			<div id="CSRAdminAdmins">
				<div class="header">
					<h4><spring:message code="label.CSRAdminAdmins" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 6 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminWhatDiffFromExAdmin" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.CSRAdminDiffFromExAdmin" htmlEscape="false"/></p>
				
				<!--------------------------- question 7 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.CSRAdminAdministrativeRoles" htmlEscape="false"/></h4>
	  			</div>
	  			<table class="table table-striped table-bordered table-condensed">
	  				<tr class="lightgrey">
	  					<th><spring:message code="label.CSRAdminType" htmlEscape="false"/></th>
	  					<th><spring:message code="label.CSRAdminFocus" htmlEscape="false"/></th>
	  					<th><spring:message code="label.CSRAdminDescription" htmlEscape="false"/></th>  				
	  				</tr>
	  				
	  				<tr>
	  					<td><spring:message code="label.CSRAdminType1" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus1" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription1" htmlEscape="false"/></td>
	  				</tr>
	  				
	  				<tr>
	  					<td><spring:message code="label.CSRAdminType2" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus2" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription2" htmlEscape="false"/></td>
	  				</tr>
	  				
	  				<!-- hix-19103 -->
	  				<%-- <tr>
	  					<td><spring:message code="label.CSRAdminType3" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus3" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription3" htmlEscape="false"/></td>
	  				</tr> --%>
	  				
	  				<%-- <tr>
	  					<td><spring:message code="label.CSRAdminType4" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus4" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription4" htmlEscape="false"/></td>
	  				</tr> --%>
	  				<!-- hix-19103 end -->
	  				
	  				<tr>
	  					<td><spring:message code="label.CSRAdminType5" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus5" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription5" htmlEscape="false"/></td>
	  				</tr>
	  				
	  				<tr>
	  					<td><spring:message code="label.CSRAdminType6" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminFocus6" htmlEscape="false"/></td>
	  					<td><spring:message code="label.CSRAdminDescription6" htmlEscape="false"/></td>
	  				</tr>
	  			</table>
  				<p class="pull-right gutter10"><small><a id="role-top" href="#">Back to Top</a></small></p>
  			</div>
		</div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		