package com.getinsured.iex.hub.mec.wrapper;

import java.text.ParseException;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.InsuranceApplicantResponseType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class InsuranceApplicantResponseWrapper implements JSONAware{

	private static final Logger LOGGER = LoggerFactory.getLogger(InsuranceApplicantResponseWrapper.class);
	private boolean applicantEligibleForESI = false;
	private boolean applicantIsInsured;
	private String startDate;
	private String endDate;

	public InsuranceApplicantResponseWrapper(
			InsuranceApplicantResponseType applicantReponse) {
		
		if(applicantReponse != null)
		{
			List<Boolean> tmp = applicantReponse.getInsuranceApplicantEligibleEmployerSponsoredInsuranceIndicator();
			if(!tmp.isEmpty()){
				this.applicantEligibleForESI = tmp.get(0).isValue();
			}
			tmp = applicantReponse.getInsuranceApplicantInsuredIndicator();
			if(!tmp.isEmpty()){
				this.applicantIsInsured = tmp.get(0).isValue();
			}
			DateRangeType coverageDateRange = applicantReponse.getInsuranceApplicantRequestedCoverage();
			DateType startDateType = coverageDateRange.getStartDate();
			this.startDate = this.getDateString(startDateType);
			DateType endDateType = applicantReponse.getInsuranceApplicantRequestedCoverage().getEndDate();
			this.endDate = this.getDateString(endDateType);
		}
	}
	
	
	private String getDateString(DateType dateType){
		String dateStr = null;
		Date xsdDate = (Date) dateType.getDateRepresentation().getValue();
		XMLGregorianCalendar xmlDate = xsdDate.getValue();
		try {
			dateStr = PlatformServiceUtil.xmlDateToString(xmlDate, "yyyy-MM-dd");
		} catch (ParseException e) {
			LOGGER.info("Invalid date",e);
		} catch (DatatypeConfigurationException e) {
			LOGGER.info("Invalid date",e);
		}
		return dateStr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj  = new JSONObject();
		obj.put("EligibleForEIS", this.applicantEligibleForESI);
		obj.put("IsInsured", this.applicantIsInsured);
		obj.put("CoverageStartDate", this.startDate);
		obj.put("CoverageEndDate", this.endDate);
		return obj.toJSONString();
	}
}
