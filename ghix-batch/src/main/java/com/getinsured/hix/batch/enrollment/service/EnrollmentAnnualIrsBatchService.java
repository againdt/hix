package com.getinsured.hix.batch.enrollment.service;

import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentAnnualIrsBatchService {
	
	/**
	 * Handles the creation manifest file and the packaging of the IRS Annual reports
	 * 
	 * @param year
	 * @param batchCategoryCode 
	 * @param xmlSourceLocation 
	 * @param isFreshRun 
	 * @param originalBatchId 
	 * @param batchId batchId of the .IN file (Can be null or empty)
	 * @param wipFolder temporary folder location for manifest and zip creation (Can be null or empty)
	 * @param fileCreationTimeStamp Timestamp required for .IN file name (Can be null or empty)
	 * @param isResubmission flag to check whether its an inbound resubmission
	 * @throws GIException
	 */
	void generateManifestXML(int year, String batchCategoryCode, String xmlSourceLocation, Boolean isFreshRun, String originalBatchId, String batchId, String wipFolder, String fileCreationTimeStamp, Boolean isResubmission, boolean isGeneratedXMLsValid) throws GIException;
	
	/**
	 * 
	 * @param xmlFileLocation
	 * @param coverageYear 
	 * @return
	 */
	boolean validateGeneratedXMLs(String xmlFileLocation, Integer coverageYear);
}
