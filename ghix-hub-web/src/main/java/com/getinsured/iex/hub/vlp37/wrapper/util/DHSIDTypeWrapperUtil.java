package com.getinsured.iex.hub.vlp37.wrapper.util;

import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.wrapper.CertOfCitDocumentID23Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.DHSIDTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I327DocumentID3Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I551DocumentID4Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I571DocumentID5Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I766DocumentID9Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.NatrOfCertDocumentID20Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.OtherCase1DocumentID1Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.OtherCase2DocumentID1Wrapper;

/**
 * Utility class for DHSIDTypeWrapper
 * Mainly used to fix SONAR violations
 * 
 * @author - Nikhil Talreja
 * @since - 15-Apr-2014
 * 
 */
public final class DHSIDTypeWrapperUtil {
	
	private DHSIDTypeWrapperUtil(){
		
	}
	
	private static final String I327_KEY = "I327DocumentID";
	private static final String I551_KEY = "I551DocumentID";
	private static final String I571_KEY = "I571DocumentID";
	private static final String I766_KEY = "I766DocumentID";
	private static final String CC_KEY = "CertOfCitDocumentID";
	private static final String NC_KEY = "NatrOfCertDocumentID";
	private static final String OTHER_CASE_1_KEY = "OtherCase1DocumentID";
	private static final String OTHER_CASE_2_KEY = "OtherCase2DocumentID";
	
	/*
	 * Used in fromJsonObject method
	 */
	public static void setITypeKeysToWrapper(JSONObject jsonObj, DHSIDTypeWrapper wrapper) throws HubServiceException{
		
		if(jsonObj.get(I327_KEY) != null){
			wrapper.setI327Document(I327DocumentID3Wrapper.fromJsonObject((JSONObject) jsonObj.get(I327_KEY)));
		}
		if(jsonObj.get(I551_KEY) != null){
			wrapper.setI551Document(I551DocumentID4Wrapper.fromJsonObject((JSONObject) jsonObj.get(I551_KEY)));
		}
		if(jsonObj.get(I571_KEY) != null){
			wrapper.setI571Document(I571DocumentID5Wrapper.fromJsonObject((JSONObject) jsonObj.get(I571_KEY)));
		}
		if(jsonObj.get(I766_KEY) != null){
			wrapper.setI766Document(I766DocumentID9Wrapper.fromJsonObject((JSONObject) jsonObj.get(I766_KEY)));
		}
		
	}
	
	/*
	 * Used in fromJsonObject method
	 */
	public static void setOtherTypeKeysToWrapper(JSONObject jsonObj, DHSIDTypeWrapper wrapper) throws HubServiceException{
		
		if(jsonObj.get(OTHER_CASE_1_KEY) != null){
			wrapper.setOtherCase1Document(OtherCase1DocumentID1Wrapper.fromJsonObject((JSONObject) jsonObj.get(OTHER_CASE_1_KEY)));
		}
		if(jsonObj.get(OTHER_CASE_2_KEY) != null){
			wrapper.setOtherCase2Document(OtherCase2DocumentID1Wrapper.fromJsonObject((JSONObject) jsonObj.get(OTHER_CASE_2_KEY)));
		}
		
	}
	
	/*
	 * Used in fromJsonString method
	 */
	public static void setITypeDocsToWrapper(JSONObject obj, DHSIDTypeWrapper wrapper) throws HubServiceException{
		
		Object param = null;
		
		param = obj.get(I327_KEY);
		if(param != null){
			wrapper.setI327Document(I327DocumentID3Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I551_KEY);
		if(param != null){
			wrapper.setI551Document(I551DocumentID4Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I571_KEY);
		if(param != null){
			wrapper.setI571Document(I571DocumentID5Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(I766_KEY);
		if(param != null){
			wrapper.setI766Document(I766DocumentID9Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(CC_KEY);
		if(param != null){
			wrapper.setCertOfCitDocument(CertOfCitDocumentID23Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(NC_KEY);
		if(param != null){
			wrapper.setNatrOfCertDocument(NatrOfCertDocumentID20Wrapper.fromJsonString((String)param));
		}
		
	}
	
	/*
	 * Used in fromJsonString method
	 */
	public static void setOtherTypeDocsToWrapper(JSONObject obj, DHSIDTypeWrapper wrapper) throws HubServiceException{
		
		Object param = null;
		
		param = obj.get(OTHER_CASE_1_KEY);
		if(param != null){
			wrapper.setOtherCase1Document(OtherCase1DocumentID1Wrapper.fromJsonString((String)param));
		}
		
		param = obj.get(OTHER_CASE_2_KEY);
		if(param != null){
			wrapper.setOtherCase2Document(OtherCase2DocumentID1Wrapper.fromJsonString((String)param));
		}
		
	}
	
}
