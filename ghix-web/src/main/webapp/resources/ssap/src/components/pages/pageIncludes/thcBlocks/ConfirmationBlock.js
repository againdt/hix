import React from 'react';

// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Constants

import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import INPUT_TYPES from '../../../../constants/inputTypes';
import VALIDATION_TYPES from '../../../../constants/validationTypes';

const Confirmation = props => {

    const {
        anyChanges,
        onFieldChange,
        ButtonContainer,
        ...restProps
    } = props;

    return (
        <div className="subsection">
            <UniversalInput
                legend={{
                    legendText: 'Do you want to make any changes, including the addition of any household members not listed above?'
                }}
                label={{
                    text: '',
                    labelClass: ['usa-width-one-whole', 'layout--02'],
                    ulClass: ['usa-width-one-whole margin-b-20'],
                    showRequiredIcon: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.RADIO_BUTTONS,
                    classes:['disp-styl-none'],
                    name: 'anyChanges',
                    value: anyChanges,
                    options: DROPDOWN_OPTIONS.YES_NO,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: {},
                    fields: []
                }}
                inputActions={{
                    onFieldChange
                }}
                indexInArray={0}
                {...restProps}
            />

            {
                anyChanges &&

                <ButtonContainer
                    type="updateInformationButton"
                    args={[]}
                />
            }

        </div>
    );
};

export default Confirmation;
