<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<jsp:useBean id="now" class="java.util.Date" />
 
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">



</script>

<div class="row-fluid">
		<!--<div class="gutter10">
			<ul class="page-breadcrumb">
            <li><a href="#">&lt; Back</a></li>
            <li><a href="#">Account</a></li>
            <li><a href="#">Manage Account Information</a></li>
        	
        </ul>page-breadcrumb ends-->
			<h1>${employer.name}</h1>
	</div>
		<div class="row-fluid">
			<!-- add id for skip side bar -->
			<div class="span3" id="sidebar">
                <div class="gray">
                    <h4 class="margin0"><spring:message  code="label.emprattesttxt1"/></h4>
                </div>
                <ul class="nav nav-list">
                        <li class="active"><a href="#"><spring:message  code="label.emprattesttxt2"/> </a></li>
                        <c:if test="${eStatus != 'NEW'}">
                        <li><a href="<c:url value="/shop/employer/application/attestationresult" />"><spring:message  code="label.emprattesttxt3"/></a></li>
                        </c:if>
                </ul>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span10"><spring:message  code="label.emprattesttxt4"/></h4>
                </div>
                <br>
                <div class="header">
                    <h4 class="span10"><spring:message  code="label.emprattesttxt2"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/shop/employer/application/editcompanyinfo"/>" /><spring:message  code="label.empredit"/></a>
                </div>
				<div class="gutter10">
					<form class="form-horizontal" id="frmEmpAppAttestation" name="frmEmpAppAttestation" action="attestation" method="POST">
					<df:csrfToken/>
					<div class="profile">
						<table class="table table-border-none">
							<tbody>
								<tr>
									<td class="txt-right span4"><spring:message  code="label.businessname"/></td>
									<td><strong>${employer.name}</strong></td>
								</tr>
								<tr>
									<td class="txt-right">EIN</td>
									<td><strong>${employer.federalEIN}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.emprattestcorptype"/></td>
									<td><strong>${orgType}</strong>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.emprfultimeemp"/></td>
									<td><strong>${employer.fullTimeEmp}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.emprpriworksiteaddr"/></td>
									<td><strong>${primaryWkSite}</strong></td>
								</tr>
								<c:forEach items="${additionalWkSites}" var="additionalWkSite" varStatus="theCount">
								<tr>
									<td class="txt-right"><spring:message  code="label.employeradditionalworksite"/> ${theCount.count}</td>
									<td><strong>${additionalWkSite}</strong></td>
								</tr>
								</c:forEach>
								
							</tbody>
						</table><br />
						
                       <security:authorize var="esignatureAllowed" access="hasPermission('ESIGNATURE_PERMISSION')"> 
                       <c:if test="${not isAgentLogin }">
                       <fieldset>
	                        <div class="header">
	                            <legend class="headerLegend"><h4 class="span10"><spring:message  code="label.emprattestation"/></h4></legend>
	                        </div>
		                    <div class="control-group">
								
									<label class="checkbox"> 
										<input type="checkbox" value="1" class="terms" name="optionsCheckboxes"> <span><spring:message  code="label.emprauthorize1"/> ${exchangeName} <spring:message  code="label.emprauthorize2"/></span> 
									</label>
									
									<label class="checkbox"> 
										<input type="checkbox" value="2" class="terms" name="optionsCheckboxes"> <span><spring:message  code="label.empragree"/></span> 
									</label>
									
									<label class="checkbox"> 
										<input type="checkbox" value="3" class="terms" name="optionsCheckboxes"> <span><spring:message  code="label.emprattest"/></span> 
									</label>	
								
							</div>
						</fieldset>
						</c:if>
					</security:authorize>
					</div>	
				
				<c:if test="${esignatureAllowed && not isAgentLogin }">
					 <div class="header">
                    	<h4 class="span10"><spring:message  code="label.emprsig"/></h4>
                	</div>
                			
                	<table class="table table-border-none">
					<thead>
						<tr>
							<td class="txt-right span4"><spring:message  code="label.emprcorprep"/> </td>
							<td><strong>${employer.contactFirstName} ${employer.contactLastName}</strong></td>
						</tr>
					 </thead>
						<tbody>
							<tr>
								<td class="txt-right"><label for="esignBy"><spring:message  code="label.applicantesig"/> <span class="aria-hidden"><spring:message  code="label.applicantelecsig"/></span><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="" height="" alt="Required!" /></label></td>
								<td>
									<input type="text" name="esignBy" id="esignBy" class="input-large" class="input-xlarge" size="30"/><br>
									<small><spring:message  code="label.applicantelecsig"/></small>
									 <div class="" id="esignBy_error"></div>
								</td>
							</tr>
						<%-- 	<tr>
								<td class="txt-right"><spring:message  code="label.todaysdate"/></td>
								<td>
									<input type="text" name="name" id="name" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="MM" />" readonly/>
									<input type="text" name="name" id="name" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="dd" />" readonly/>
									<input type="text" name="name" id="name" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="yyyy" />" readonly/>
								</td>
							</tr> --%>
						</tbody>
					</table>
				</c:if>
				<table class="table table-border-none">
					<thead>
					</thead>
						<tbody>
							<tr>
								<td class="txt-right"><spring:message code="label.todaysdate"/>:</td>
								<td>
									<label class="aria-hidden" for="month"><spring:message code="label.todaysdate"/> <spring:message code="label.monthMM"/></label>
									<input type="text" name="name" id="month" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="MM" />" readonly/>
									<label class="aria-hidden" for="day"><spring:message code="label.todaysdate"/> <spring:message code="label.dayDD"/></label>
									<input type="text" name="name" id="day" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="dd" />" readonly/>
									<label class="aria-hidden" for="year"><spring:message code="label.todaysdate"/> <spring:message code="label.yearYYYY"/></label>
									<input type="text" name="name" id="year" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="yyyy" />" readonly/>
								</td>
							</tr>
						</tbody>
					</table>
				<c:if test=" not ${esignatureAllowed}">
					<div class="control-group">
						<p>
							This employer must provide an attestation and esignature before this shop application can be submitted.<br/>
							Please ask your contact person at "${employer.name}" to log in to ${exchangeName} and provide an attestation. 
						</p>
					</div>
				</c:if>
				<c:if test="${eStatus == 'NEW' && validEmpCount <= 0}"> 
					<div>
						<input type="button" name="submitButton" id="submitButton" onclick="javascript:window.location.href ='<c:url value="/shop/employer/roster/addemployee"/>'" class="btn btn-primary pull-left" value="<spring:message  code="label.addEmployee"/>">
					</div>
		 		</c:if>
				<input type="button" name="submitButton" id="submitButton" value="<spring:message  code="label.submitapp"/>" onClick="javascript:validateForm();" class="btn btn-primary pull-right"/>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
<!-- Modal -->
<div class="modal fade hide" id="esignPage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Employer attention required!</h4>
      </div>
      <div class="modal-body">
        <div class="row-fluid">
        	<p>Your client needs to e-sign the application it can be submitted.</p>
        	<p>If you haven't already, please do the following in order to send an activation link to your client:</p>
        	<ol class="steps">
        		<li>Click 'My Account' in the orange bar above to return to your account.</li>
        		<li>Choose Employers &gt; Active Employers.</li>
        		<li>Click on the Employer that you are now setting up.</li>
        		<li>In the Left Navigation bar, click 'Send Activation Link'. This will send an email to your client with instructions for accessing the account you created for them.</li>
        	</ol>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  data-dismiss="modal">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
function validateForm(){
	var isAgentLogin = '${isAgentLogin}';
	
	if(isAgentLogin == 'true'){
		//alert("<spring:message code='label.agentvalidation' javaScriptEscape='true'/>");
		$('#esignPage').modal('show');
	}
	else{
		var empCount = '${validEmpCount}';
		if( $(".terms").length > 0 && $(".terms:checked").length < 3){
			 alert("<spring:message code='label.acceptattest' javaScriptEscape='true'/>");
			 return false;
		 }else if (!$("#frmEmpAppAttestation").validate().form()){
			 return false;
		 }else if(empCount <=0){
			alert("<spring:message code='label.validatempshop' javaScriptEscape='true'/>"); 
			return false;
		 }else{
			 $("#frmEmpAppAttestation").submit();
		 }	
	}
	 
}

jQuery.validator.addMethod("esigncheck", function(value, element, param) {
	var firstname = '${loggedInUser.firstName}'.replace('&amp;','&');
	var lastname = '${loggedInUser.lastName}'.replace('&amp;','&');
	var esign = $("#esignBy").val();
	var userName = firstname + " " + lastname;
	if( esign.toLowerCase() == userName.toLowerCase() ){
		return true ;
	}
	return false;
});

var validator = $("#frmEmpAppAttestation").validate({ 
	rules : {
		  esignBy : {required: {
		        depends: function(element) { return $('#esignBy').length; }
		        },
		        esigncheck :true
		      }
		  },
	messages : {
		esignBy: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>" ,
			esigncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEsignUserName' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});
</script>		