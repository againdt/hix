package com.getinsured.fars2.wrapper;


import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.fars2.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.fars2.cms.dsh.ridp.extension._1.RequestPayloadType;



public class FarsRequestPayloadTypeWrapper implements JSONAware{
	
	
	private RequestPayloadType request;
	
	private String dshReferenceNumber;
	private String subscriberNumber = null;

	private ObjectFactory factory;
	
	public static final String DHS_REF_NUM_KEY = "DSHReferenceNumber";
	public static final String SUBSCRIBER_KEY = "SubscriberNumber";
	
	public FarsRequestPayloadTypeWrapper(){
		factory = new ObjectFactory();
		this.request = factory.createRequestPayloadType();
	}
	
	public String getDshReferenceNumber() {
		return dshReferenceNumber;
	}

	public void setDshReferenceNumber(String dshReferenceNumber) {
		this.dshReferenceNumber = dshReferenceNumber;
		this.request.setDSHReferenceNumber(dshReferenceNumber);
	}

	public String getSubscriberNumber() {
		return subscriberNumber;
	}

	public void setSubscriberNumber(String subscriberNumber) {
		this.subscriberNumber = subscriberNumber;
		this.request.setSubscriberNumber(subscriberNumber);
	}

	public RequestPayloadType getRequest() throws HubServiceException{
		
		if(this.request.getDSHReferenceNumber() == null){
			throw new HubServiceException("DHS Reference Number is mandatory for FARS Service");
		}
		
		return request;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(DHS_REF_NUM_KEY, this.dshReferenceNumber);
		if(this.subscriberNumber != null){
			obj.put(SUBSCRIBER_KEY, this.subscriberNumber);
		}
		return obj.toJSONString();
	}

}
