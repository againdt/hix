package com.getinsured.ahbx.shop.service;

import java.util.Map;

import com.getinsured.ahbx.shop.employercaseorderid.CaseOrderIdSaveResponse;
import com.getinsured.ahbx.shop.employerdetails.EmployerInfoResponse;
import com.getinsured.ahbx.util.GIException;
import com.getinsured.hix.dto.shop.ShopResponse.ResponseType;

public interface EmployerWsService {
	public EmployerInfoResponse getEmployerDetails(String externalEmpId) throws GIException;
	
	public  Map<String, Object> createEmployerData(EmployerInfoResponse employerInfoResponse, ResponseType responseType) throws GIException;
	
	public CaseOrderIdSaveResponse sendCaseOrderID(int employerCaseID, int orderID)  throws GIException;
}
