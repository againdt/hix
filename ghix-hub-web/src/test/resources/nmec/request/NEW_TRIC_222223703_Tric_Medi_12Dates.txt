{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "insurance": {
              "insurancePolicyExpirationDate": "12/31/2014",
              "insurancePolicyEffectiveDate": "01/01/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "organization": [
              {
                "organizationCode": "TRIC"
              }
            ],
            "dateOfBirth": "01/01/1949",
            "socialSecurityCard": {
              "socialSecurityNumber": "222223703"
            },
            "name": {
              "middleName": "C",
              "lastName": "Fox",
              "firstName": "John",
              "personNameSuffixText": ""
            },
            "gender": "F"
          },
          {
            "insurance": {
              "insurancePolicyExpirationDate": "12/31/2014",
              "insurancePolicyEffectiveDate": "01/01/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "organization": [
              {
                "organizationCode": "MEDI"
              }
            ],
            "dateOfBirth": "01/25/1970",
            "socialSecurityCard": {
              "socialSecurityNumber": "222223701"
            },
            "name": {
              "middleName": "C",
              "lastName": "Smith",
              "firstName": "John",
              "personNameSuffixText": ""
            },
            "gender": "F"
          }
        ]
      }
    ]
  }
}