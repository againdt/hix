package com.getinsured.ssap.model.financial;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.ssap.model.hub.vlp.response.ResponseMetadata;

/**
 * Describes IFSV error message details.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IfsvErrorMessageDetail {
  @JsonProperty("XPathContent")
  private String xPathContent;

  @JsonProperty("ResponseMetadata")
  private ResponseMetadata responseMetadata;

  public String getxPathContent() {
    return xPathContent;
  }

  public void setxPathContent(String xPathContent) {
    this.xPathContent = xPathContent;
  }

  public ResponseMetadata getResponseMetadata() {
    return responseMetadata;
  }

  public void setResponseMetadata(ResponseMetadata responseMetadata) {
    this.responseMetadata = responseMetadata;
  }

  @Override
  public String toString() {
    return "IfsvErrorMessageDetail{" +
        "xPathContent='" + xPathContent + '\'' +
        ", responseMetadata=" + responseMetadata +
        '}';
  }
}
