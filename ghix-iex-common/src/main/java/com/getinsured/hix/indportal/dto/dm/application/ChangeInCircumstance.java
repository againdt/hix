package com.getinsured.hix.indportal.dto.dm.application;

public class ChangeInCircumstance {
	 private String changeDate;
	 private Boolean coverage60DayBeforeMarriageIndicator;
	 private Boolean liveInForeignCountry60DayBeforeMarriageIndicator;
	 // Getter Methods 

	 public String getChangeDate() {
	  return changeDate;
	 }

	 // Setter Methods 

	 public void setChangeDate(String changeDate) {
	  this.changeDate = changeDate;
	 }

	public Boolean getCoverage60DayBeforeMarriageIndicator() {
		return coverage60DayBeforeMarriageIndicator;
	}

	public void setCoverage60DayBeforeMarriageIndicator(Boolean coverage60DayBeforeMarriageIndicator) {
		this.coverage60DayBeforeMarriageIndicator = coverage60DayBeforeMarriageIndicator;
	}

	public Boolean getLiveInForeignCountry60DayBeforeMarriageIndicator() {
		return liveInForeignCountry60DayBeforeMarriageIndicator;
	}

	public void setLiveInForeignCountry60DayBeforeMarriageIndicator(
			Boolean liveInForeignCountry60DayBeforeMarriageIndicator) {
		this.liveInForeignCountry60DayBeforeMarriageIndicator = liveInForeignCountry60DayBeforeMarriageIndicator;
	}
	 
	}
