<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<div class="gutter10">
<div class="row-fluid">	
		
    	<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employee/searchemployee"/>">Employees</a></li>
			<li>Ticket Detail</li>
		</ul><!--page-breadcrumb ends-->
                
                <div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	
		<h1>
			${employee.name} <small>Contact Name:
			${employeeDetail.firstName} ${employeeDetail.lastName} </small>
		</h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	<div class="header">
            <h4>About This Employee</h4>
            </div>
            
           <ul class="nav nav-list">
				
				<li  ><a href="/hix/crm/crmemployee/details/${employeeId}">Basic Information</a></li>
				<li ><a href="/hix/crm/crmemployee/comments/${employeeId}">Comments</a></li>
				<li class="active navmenu">Ticket History</li>
				<li class="navmenu"><a href="/hix/crm/crmemployee/securityQuestions/${employeeId}">Security Questions</a></li>
		
			</ul>
          	<div class="header margin20-t">
            	<h4 class="margin0"><i class="icon-cog icon-white"></i>&nbsp;Actions</h4>
            </div>
            <ul class="nav nav-list"> 
            <c:choose>
			<c:when test="${employee.status == 'TERMINATED'}">
			<li class="navmenu disabled"><a href="#" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:when>
			<c:otherwise>
			<c:if test="${checkDilogEmp == null}"> 
			<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<c:if test="${checkDilogEmp != null}">  
			<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employee&switchToModuleId=${employeeId}&switchToResourceName=${employee.name}" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<li class="navmenu"><a href="<c:url value="/crm/crmemployee/passwordreset"/>/${employeeId}?sourcePage=ticket/history" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
			</c:otherwise>
			</c:choose>
                <!-- <li class="navmenu disabled"><a href="#"><i class="icon-envelope"></i>Compose Message</a></li>
                <li class="navmenu disabled"><a href="#"><i class="icon-comment"></i>New Comment</a></li> -->
            </ul>
		</div>
    	
       <div class="span9" id="rightpanel">
			<c:choose>
				<c:when test="${fn:length(ticketHistoryList) > 0}">
					<display:table id="ticketHistory" name="ticketHistoryList"  pagesize="${pageSize}" requestURI="" sort="list" class="table table-condensed table-border-none table-striped">
						 <display:column title="Ticket Id" sortable="true" headerClass="graydrkbg">  
      				 	 <c:set var="id" value="${ticketHistory.id}" />  
       					 <a href="../detail/${id}?employeeId=${employeeId}">${ticketHistory.number} </a>   
   						 </display:column>  
						<display:column property="subject" title="Subject" sortable="true" headerClass="graydrkbg" />
						<display:column property="status" title="Status" sortable="true" headerClass="graydrkbg" />
						<display:column property="created" title="Created Date" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
						<display:column property="updated" title="Close Date" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
					    <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
			           <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
							</ul>
						</div>
					</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
								<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
							</ul>
						</div>
						'/>
					</display:table>
				</c:when>
				<c:otherwise>
					<div class="alert alert-info">
						<spring:message code='label.norecords' />
					</div>
				</c:otherwise>
			</c:choose>
		</div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->

<!-- Modal -->
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employee/home" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employee Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employee View" will take to the Employee's portal for ${employee.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employee.<br/>
					Proceed to Employee view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployeeView" name="checkEmployeeView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary" type="submit">Employee View</button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employee" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employeeId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employee.name}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
<script type="text/javascript">
function saveComment(ticketId)
{
	var validateUrl = '<c:url value="/admin/ticketmgmt/savecomment">
					<c:param name="${df:csrfTokenParameter()}"> 
					<df:csrfToken plainToken="true" />
					</c:param></c:url>';

	var commentData = $("#comment_text").val();
	$.ajax({
		type: "POST",
		url: validateUrl,
		data: {ticketId : ticketId,
			comment : commentData},
		success: function(response, xhr)
		{
	        if(isInvalidCSRFToken(xhr))                                  
    	      return;

			if(response)
			{
				$('#markCompleteDialog').modal('hide');
			}
		}
		});
}
</script>
