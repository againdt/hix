<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<style type="text/css">
.table tr th{
               background: none !important;
               height: 14px;
               font-weight: normal;
               color: #5A5A5A;
       }
</style>
		<div class="row-fluid">				
				<div class="gutter10-lr">			
					<jsp:include page="/WEB-INF/views/entity/activationLinkResult.jsp" />	
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.manage"/></a> &gt;</li>
								<li><a href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${assisterId}" />">${assister.firstName} ${assister.lastName}</a></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
						<div class="span9">
							<div class="header">
								<h4 class="pull-left"><spring:message code="label.assister.certificationStatus"/></h4>
								<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/assisteradmin/editcertificationstatus?assisterId=${assisterId}" />" title="<spring:message code="label.assister.edit"/>"><spring:message code="label.assister.edit"/></a>
							</div>
							<form class="form-horizontal" id="frmcertificationstatus" name="frmcertificationstatus" action="certificationstatus" method="post">
							<df:csrfToken/>
								<input type="hidden" id="documentId1" name="documentId1" value="">
								<div class="gutter10-tb">
									<table class="table table-border-none table-condensed">
									        <tr>
                                                               <td class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterNumber"/></td>
                                                               <td><strong>${assister.assisterNumber}</strong></td>
                                                           </tr>
                                                              <tr>
                                                                     <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationStatus"/></td>
                                                                     <td><strong>${assister.certificationStatus}</strong></td>
                                                              </tr>
                                                              <c:choose>
                                                                 <c:when test="${not empty assister.certificationNumber}">
                                                                   <tr>
	                                                                  <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationNumber"/></td>
	                                                                  <td><strong><fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/></strong></td>
                                                                  </tr>
                                                                 </c:when>
                                                                 <c:otherwise>
                                                                   <tr>
	                                                                  <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationNumber"/></td>
	                                                                  <td><strong>N/A</strong></td>
                                                                  </tr>
                                                                 </c:otherwise>
                                                              </c:choose>
                                                              <c:choose>
                                                              <c:when test="${assister.certificationStatus !='Pending'}">
                                                               <tr>
                                                                 <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationStartDate"/></td>
                                                                 <td><strong><fmt:formatDate value="${assister.certificationDate}" pattern="MM/dd/yyyy"/></strong></td>
                                                              </tr>
                                                              <tr>
                                                                 <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationRenewalDate"/></td>
                                                                 <td><strong><fmt:formatDate value="${assister.reCertificationDate}" pattern="MM/dd/yyyy"/></strong></td>
                                                              </tr>
                                                              </c:when>
                                                              <c:otherwise>
                                                              <tr>
                                                                 <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationStartDate"/></td>
                                                                 <td><strong>N/A</strong></td>
                                                              </tr>
                                                              <tr>
                                                                 <td class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationRenewalDate"/></td>
                                                                 <td><strong>N/A</strong></td>
                                                              </tr>
                                                              </c:otherwise>
                                                              </c:choose>
                                                              
                                                              <c:if test="${CA_STATE_CODE}">
                                                              	<c:if test="${not empty assister.delegationCode}">
                                                              		<tr>
		                                                                  <td class="span4 txt-right" scope="row"><spring:message code="label.brkDelegationCode"/></td>
		                                                                  <td><strong>${assister.delegationCode}</strong></td>
	                                                                  </tr>
                                                              	</c:if>
                                                              </c:if>
                                                              
                                     </table>                         
								</div><!-- end of .gutter10 -->
								     
                                  	
                                  		<div class="header margin-b">
                   						 	<h4><spring:message code="label.assister.certificationHistory"/></h4>
                   						 </div>
                   						 <div class="gutter10">                                        
                   						 <display:table id="assisterRegisterStatusHistory" name="assisterRegisterStatusHistory" list="assisterRegisterStatusHistory" requestURI="" sort="list" class="table" >
                                             <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MMM dd, yyyy}" sortable="false" />
                                             <display:column property="previousRegistrationStatus" titleKey="label.entity.previousStatus" sortable="false" />
                                             <display:column property="newRegistrationStatus" titleKey="label.NewStatus" sortable="false" />
                                                   <display:column property="comments" titleKey="label.entity.vieComment" sortable="false" />
                                                       <display:column titleKey="label.assister.viewAttachment" > 
                                                           <c:choose>
                                                               <c:when test="${assisterRegisterStatusHistory.documentId!=null}">
                                                                  <a href="<c:url value="/entity/assisteradmin/viewAttachment?documentId=${assisterRegisterStatusHistory.documentId}"/>" id="edit_${assisterRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a>
                                                                 <%--  <a href="#" onClick="showdetail('${assisterRegisterStatusHistory.documentId}');" id="edit_${assisterRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a> --%> 
                                                            </c:when>
                                                            <c:otherwise>  
                                                                 <spring:message code="label.assister.noAttachment"/>
                                                            </c:otherwise>
                                                       </c:choose>
                                                       </display:column>
                                         </display:table>
                                        </div>                                
                           		
							</form>
						</div><!-- end of span9 -->
					</div>
				</div><!--  end of row-fluid -->
				<div id="modalBox" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal" data-original-title="">x</a>
						<h3 id="myModalLabel">
							<spring:message code="label.assister.viewComment"/>
						</h3>
					</div>
					<div id="commentDet" class="modal-body">
						<label id="commentlbl"></label>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
					</div>
				</div>

		</div>
		<script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,88,new Date().getTime(),"","","","",""]);
		</script>
		
<%-- <script type="text/javascript">
		$(document).ready(function(){			
			$("#btnUploadDoc").click(function(){	
				if($("#fileInput").val() != '') {					
					$('#frmupdatecertification').ajaxSubmit({					
						url:"<c:url value='/entity/assisteradmin/uploaddocument?id=${assister.id}'/>",					
						success: function(responseText){							
							if(responseText != "0"){
								$('#documentId').val(responseText);
								alert("File uploaded successfully");											
							}else{
								alert("Unable to upload the file");													
							}	
				       	}
					});
				}
			  return false; 
			});	// button click close			
		});
		
		function getComment(comments)
		{					
				
			    $('#commentDet').html("<p> Loading Comment...</p>");
				comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br				
				comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
				$('#commentDet').html("<p> "+ comments + "</p>");			
		}
		
		var validator = $("#frmupdatecertification").validate({ 
			 rules : {
				 registrationStatus : {required : true},
			 },
			 messages : {
				 registrationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateRegistrationStatus' javaScriptEscape='true'/></span>"}
			 },
			 errorClass: "error",
			 errorPlacement: function(error, element) {
			 var elementId = element.attr('id');
			 error.appendTo( $("#" + elementId + "_error"));
			 $("#" + elementId + "_error").attr('class','error help-inline');
			}
		});			
</script>
<script >
$(document).ready(function(){
	document.getElementById('newRegistrationRenewalDate').style.display='none';
});

 function showdetail(documentId) {
		
	 
	 var  contextPath =  "<%=request.getContextPath()%>";
		 
	    var documentUrl = contextPath + "/entity/assisteradmin/viewAttachment?documentId="+documentId;
	    window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
 }
 
 function changeDate(){
	 
	 document.getElementById('newRegistrationRenewalDate').style.display='inline';
	 
	
 } 



</script>--%>

<script>

$(document).ready(function() {
	$("#assisterCertification").removeClass("link");
	$("#assisterCertification").addClass("active");
	$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
});
function getComment(comments)
{	
	$('#commentlbl').html("<p> Loading Comment...</p>");
	comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
	comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
	$('#commentlbl').html("<p> "+ comments + "</p>");
	
}


$(function () {
	$("a[rel=twipsy]").twipsy({
		live: true
	});
});

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault()  // prevent the default action, e.g., following a link
		});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes *
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val; 
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
</script>
<script >
function showdetail(documentId) {
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.assister.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.assister.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/entity/assisteradmin/viewAttachment?documentId="+documentId;
		     window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	}	
</script>
