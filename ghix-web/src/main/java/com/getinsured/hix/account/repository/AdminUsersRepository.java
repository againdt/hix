package com.getinsured.hix.account.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;

@Repository
@Transactional(readOnly = true)
public class AdminUsersRepository implements IGHIXCustomRepository<Object, Integer> {


	/* We always return true as we don't persist createdObject in AccountActivation
	 * while creating different admin accounts (Issuer Admin, Broker Admin etc) unlike Employer, Employee etc
	 *
	 */
	@Override
	public boolean recordExists(Integer id) {
		return true;
	}

	/* We just provide blank implementation as we don't persist createdObject in AccountActivation
	 * while creating different admin accounts (Issuer Admin, Broker Admin etc) unlike Employer, Employee etc
	 */
	@Override
	public void updateUser(AccountUser accountUser, Integer id) {
		;
	}

}
