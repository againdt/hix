
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "taxHousehold"
})
public class NonEsiMecPayload implements Serializable
{

    @JsonProperty("taxHousehold")
    private List<NonEsiMecTaxHousehold> taxHousehold = new ArrayList<NonEsiMecTaxHousehold>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -8713639671819863515L;

    @JsonProperty("taxHousehold")
    public List<NonEsiMecTaxHousehold> getTaxHousehold() {
        return taxHousehold;
    }

    @JsonProperty("taxHousehold")
    public void setTaxHousehold(List<NonEsiMecTaxHousehold> taxHousehold) {
        this.taxHousehold = taxHousehold;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("taxHousehold", taxHousehold).append("additionalProperties", additionalProperties).toString();
    }

}
