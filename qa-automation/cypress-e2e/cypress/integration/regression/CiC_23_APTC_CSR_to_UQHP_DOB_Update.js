const inputHouseholds = ["regression/APTC_CSR_2HH"]
before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC23: APTC CSR to UQHP plus DOB Change for ${file}`, function() {

        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical and Dental - ${file}`, function () {
            cy.enrollIntoMedicalAndDental();
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
        it(`DOB change for one member for ${file}`, function () { //Updating the DoB
            cy.h.people[0].dob = "1988-10-10"
        })
        it(`Add member for ${file}`, function () { //Updating the DoB
            cy.addMember()
        })
        it(`Update aptc/csr eligibility to false for ${file}`, function () { //Updating the DoB
            for (let i = 0; i < cy.h.people.length; i++){
                cy.h.people[i].aptcEligibilityIndicator = false
                cy.h.people[i].csrEligibilityIndicator = false
            }
            cy.h.appStatus = 'enroll_med_dent_aptc_update'
        })
        it(`AT update for ${file}`, function () {
            cy.updateAccountTransfer(cy.h)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})









