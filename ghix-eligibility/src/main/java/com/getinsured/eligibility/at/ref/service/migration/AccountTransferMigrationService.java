package com.getinsured.eligibility.at.ref.service.migration;

import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

public interface AccountTransferMigrationService {

	void process(AccountTransferRequestPayloadType request, AccountTransferResponseDTO result, String enrollmentIds)
			throws Exception;

}
