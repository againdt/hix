package com.getinsured.hix.crm.web;


public class AjaxResponseBase {
	private boolean status;
	private String message;
	private String data;
	private Object data2;
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("[Status:%s]", status));
		sb.append(String.format("[Message:%s]", message));
		return String.format("[%s]", sb.toString());
	}
	public AjaxResponseBase(boolean status, String message)
	{
		this.status = status;
		this.message = message;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Object getData2() {
		return data2;
	}
	public void setData2(Object data2) {
		this.data2 = data2;
	}
}