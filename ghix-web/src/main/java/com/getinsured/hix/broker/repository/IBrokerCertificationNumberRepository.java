package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.BrokerCertificationNumber;

/**
 * Spring-JPA repository that encapsulates methods that interact with the BrokerCertificationNumber
 * table
 */
public interface IBrokerCertificationNumberRepository extends
		RevisionRepository<BrokerCertificationNumber, Integer, Integer>,
		JpaRepository<BrokerCertificationNumber, Integer> {

	/**
	 * Fetches the BrokerCertificationNumber record against the given broker Id
	 * 
	 * @param brokerId
	 *            the Broker id
	 * @return the BrokerCertificationNumber record against the broker Id
	 */
	BrokerCertificationNumber findByBrokerId(Integer brokerId);

	/**
	 * Fetches the BrokerCertificationNumber record against the given Id
	 * 
	 * @param brokerCertificationNumber
	 *            the BrokerCertificationNumber id
	 * @return BrokerCertificationNumber record against the given Id
	 */
	BrokerCertificationNumber findById(long brokerCertificationNumber);

}