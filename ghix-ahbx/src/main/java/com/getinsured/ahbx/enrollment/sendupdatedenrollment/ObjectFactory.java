//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.02 at 02:36:50 PM PDT 
//


package com.getinsured.ahbx.enrollment.sendupdatedenrollment;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.accenture.ahbx.webservice.impl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SaveEnrollmentConfirmationResponse_QNAME = new QName("http://impl.webservice.ahbx.accenture.com/", "saveEnrollmentConfirmationResponse");
    private final static QName _SaveEnrollmentConfirmation_QNAME = new QName("http://impl.webservice.ahbx.accenture.com/", "saveEnrollmentConfirmation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.accenture.ahbx.webservice.impl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveEnrollmentConfirmationResponse }
     * 
     */
    public SaveEnrollmentConfirmationResponse createSaveEnrollmentConfirmationResponse() {
        return new SaveEnrollmentConfirmationResponse();
    }

    /**
     * Create an instance of {@link SaveEnrollmentConfirmation }
     * 
     */
    public SaveEnrollmentConfirmation createSaveEnrollmentConfirmation() {
        return new SaveEnrollmentConfirmation();
    }

    /**
     * Create an instance of {@link ResponseDTO }
     * 
     */
    public ResponseDTO createResponseDTO() {
        return new ResponseDTO();
    }

    /**
     * Create an instance of {@link IndvEnrollmentConfirmationRequest }
     * 
     */
    public IndvEnrollmentConfirmationRequest createIndvEnrollmentConfirmationRequest() {
        return new IndvEnrollmentConfirmationRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveEnrollmentConfirmationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.webservice.ahbx.accenture.com/", name = "saveEnrollmentConfirmationResponse")
    public JAXBElement<SaveEnrollmentConfirmationResponse> createSaveEnrollmentConfirmationResponse(SaveEnrollmentConfirmationResponse value) {
        return new JAXBElement<SaveEnrollmentConfirmationResponse>(_SaveEnrollmentConfirmationResponse_QNAME, SaveEnrollmentConfirmationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveEnrollmentConfirmation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.webservice.ahbx.accenture.com/", name = "saveEnrollmentConfirmation")
    public JAXBElement<SaveEnrollmentConfirmation> createSaveEnrollmentConfirmation(SaveEnrollmentConfirmation value) {
        return new JAXBElement<SaveEnrollmentConfirmation>(_SaveEnrollmentConfirmation_QNAME, SaveEnrollmentConfirmation.class, null, value);
    }

}
