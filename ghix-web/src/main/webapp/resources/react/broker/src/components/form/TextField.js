import React, { Fragment } from 'react';
import InputFeedback from './InputFeedback';
import classNames from 'classnames';

const TextField = ({ input, meta, id, label, className, ...props }) => (
  <Fragment>
    <div className="ds-l-row">
      <div className="ds-l-col--12 ds-l-sm-col--3 ds-l-md-col--3 ds-l-lg-col--4 ds-l-xl-col--4">
        <input
          id={id}
          {...input}
          className={classNames(
            className,
            meta.touched && meta.error
              ? 'ds-c-field--inverse ds-c-field--error'
              : ''
          )}
        />
      </div>
      <div className="ds-l-col--12 ds-l-sm-col--6 ds-l-md-col--6 ds-l-lg-col--8 ds-l-xl-col--8 ds-u-padding-bottom--1">
        {meta.touched && meta.error && (
          <InputFeedback
            error={
              <div className="error help-inline ds-u-margin-bottom--1">
                <span class="error">
                  <em className="excl">!</em>
                  {/*<i className="fas fa-exclamation-circle" />*/}
                  {meta.error}
                </span>
              </div>
            }
            errorClasses="ds-c-field__hint nv-label-error"
          />
        )}
      </div>
    </div>
  </Fragment>
);

export default TextField;
