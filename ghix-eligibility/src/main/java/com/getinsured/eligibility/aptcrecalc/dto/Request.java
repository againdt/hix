package com.getinsured.eligibility.aptcrecalc.dto;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;

public class Request {
	 private String ahbxCaseId;
	 private String enrlYear;
	 private String coverageStartDate;
	 private Map<String, List<EnrollmentDataDTO>> enrollmentDataDtoListMap;


	 // Getter Methods 

	 public String getAhbxCaseId() {
	  return ahbxCaseId;
	 }

	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataDtoListMap() {
		return enrollmentDataDtoListMap;
	}

	public void setEnrollmentDataDtoListMap(Map<String, List<EnrollmentDataDTO>> enrollmentDataDtoListMap) {
		this.enrollmentDataDtoListMap = enrollmentDataDtoListMap;
	}

	public String getEnrlYear() {
	  return enrlYear;
	 }

	 public String getCoverageStartDate() {
	  return coverageStartDate;
	 }

	 

	 // Setter Methods 

	 public void setAhbxCaseId(String ahbxCaseId) {
	  this.ahbxCaseId = ahbxCaseId;
	 }

	 public void setEnrlYear(String enrlYear) {
	  this.enrlYear = enrlYear;
	 }

	 public void setCoverageStartDate(String coverageStartDate) {
	  this.coverageStartDate = coverageStartDate;
	 }

	 
	}
