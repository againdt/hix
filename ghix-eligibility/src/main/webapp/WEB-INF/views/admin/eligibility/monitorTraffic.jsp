<%@ include file="datasource.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<%
	String whereClause = request.getParameter("whereClause");
	if (whereClause == null || whereClause == "") {
		whereClause = "CREATION_TIMESTAMP > sysdate -1 ";
	}
%>

<sql:query dataSource="jdbc/ghixDS" var="affresult">
	SELECT DISTINCT ID,COMPANY_NAME FROM AFF_AFFILIATE WHERE AFFILIATE_STATUS = 'ACTIVE' ORDER BY COMPANY_NAME ASC
</sql:query>

<sql:query dataSource="jdbc/ghixDS" var="result1">
select date1, avg(duration) as duration
from(
select to_char(TRUNC (cast(CREATION_TIMESTAMP as date)) + ( ROUND ( (cast(CREATION_TIMESTAMP as date) - TRUNC (cast(CREATION_TIMESTAMP as date))) * 288 ) / 288 ), 'DD HH24:MI') as date1,
GI_EXECUTION_DURATION as duration
from elig_lead
where <%=whereClause%> and GI_EXECUTION_DURATION is not null
order by CREATION_TIMESTAMP
)
group by date1
order by date1
</sql:query>

<sql:query dataSource="jdbc/ghixDS" var="result2">
select date1, count(date1) AS total
from(
select to_char(TRUNC ( cast(CREATION_TIMESTAMP as date)) + ( ROUND ( (cast(CREATION_TIMESTAMP as date) - TRUNC (cast(CREATION_TIMESTAMP as date))) * 288 ) / 288 ), 'DD HH24:MI') as date1,
to_char(cast(CREATION_TIMESTAMP as date), 'HH24:MI DD') as original
from elig_lead
where <%=whereClause%>
order by CREATION_TIMESTAMP
)
group by date1
order by date1
</sql:query>

<sql:query dataSource="jdbc/ghixDS" var="result3">
select date1, sum(success) AS success, sum(failure) AS failure
from(
select to_char(TRUNC ( cast(CREATION_TIMESTAMP as date)) + ( ROUND ( (cast(CREATION_TIMESTAMP as date) - TRUNC (cast(CREATION_TIMESTAMP as date))) * 288 ) / 288 ), 'DD HH24:MI') as date1,
CASE WHEN OVERALL_API_STATUS='success' THEN 1 ELSE 0 END AS success,
CASE WHEN OVERALL_API_STATUS='failure' OR OVERALL_API_STATUS IS NULL THEN 1 ELSE 0 END AS failure,
to_char(cast(CREATION_TIMESTAMP as date), 'HH24:MI DD') as original
from elig_lead
where <%=whereClause%>
order by CREATION_TIMESTAMP
)
group by date1
order by date1
</sql:query>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Monitor Traffic</title>
<style>
table#table1 {
	border: 1px solid #666;
	width: 100%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}

.group-1 {
	font-weight: bold;
	padding-bottom: 10px;
	border-top: 1px solid black;
}

.group-2 {
	font-style: italic;
	border-top: 1px solid black;
}

.subtotal-sum,.grandtotal-sum {
	font-weight: bold;
	text-align: right;
}

.subtotal-header {
	padding-bottom: 0px;
	border-top: 1px solid white;
}

.subtotal-label,.grandtotal-label {
	border-top: 1px solid white;
	font-weight: bold;
}

.grouped-table tr.even {
	background-color: #fff;
}

.grouped-table tr.odd {
	background-color: #fff;
}

.grandtotal-row {
	border-top: 2px solid black;
}
</style>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load('visualization', '1', {
		packages : [ 'corechart' ]
	});

	function drawVisualization() {
		// Create and populate the data table.
		var data = google.visualization.arrayToDataTable([
				[ 'Affiliate', 'Duration' ],
				<c:forEach var="row" items="${result1.rows}">[
						'<c:out value="${row.date1}"/>',
						<c:out value="${row.duration}"/>], </c:forEach> ]);
		
		var data2 = google.visualization.arrayToDataTable([
		                                  				[ 'Affiliate', 'Transactions' ],
		                                  				<c:forEach var="row" items="${result2.rows}">[
		                                  						'<c:out value="${row.date1}"/>',
		                                  						<c:out value="${row.total}"/>], </c:forEach> ]);
		
		var data3 = google.visualization.arrayToDataTable([
			                                  				[ 'Affiliate', 'Success', 'Failure'],
			                                  				<c:forEach var="row" items="${result3.rows}">[
			                                  						'<c:out value="${row.date1}"/>',
			                                  						<c:out value="${row.success}"/>,
			                                  						<c:out value="${row.failure}"/>], </c:forEach> ]);
		//alert(data);
		if(data){
			// Create and draw the visualization.
			new google.visualization.LineChart(document
					.getElementById('visualization')).draw(data, {
				curveType : "function",
				width : 1000,
				height : 300,
				vAxis : {
					maxValue : 10
				}
			});
		}
		
		
		if(data2){
		// Create and draw the visualization.
		new google.visualization.LineChart(document
				.getElementById('visualization2')).draw(data2, {
			curveType : "function",
			width : 500,
			height : 300,
			vAxis : {
				maxValue : 10
			}
		});
		}
		
		if(data3){
		// Create and draw the visualization.
		new google.visualization.LineChart(document
				.getElementById('visualization3')).draw(data3, {
			curveType : "function",
			width : 500,
			height : 300,
			vAxis : {
				maxValue : 10
			}
		});
		}
	}

	google.setOnLoadCallback(drawVisualization);
</script>
</head>
<script>
	$(function() {
		$("#date1").datepicker().attr('readOnly', 'true');
	});

	function validateDates() {
		var dateStart = document.getElementById('date1').value.split('/');

		if (dateStart != "" && dateStart.length > 0) {

			var boolean1 = validateDate(dateStart);
		}
	}

	function validateDate(dateGiven) {

		var today = new Date();

		var dob_mm = dateGiven[0];
		var dob_dd = dateGiven[1];
		var dob_yy = dateGiven[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 2) > birthDate.getFullYear()) {
			return false;
		}

		if ((dob_dd != birthDate.getDate())
				|| (dob_mm - 1 != birthDate.getMonth())
				|| (dob_yy != birthDate.getFullYear())) {
			return false;
		}

		if (today.getTime() < birthDate.getTime()) {
			return false;
		}

	}

	function setWhereClause(type) {

		var whereClause = '';
		var filterStatement = '';
		switch (type) {
		
		case 'B4':

			//Validate the dates
			validateDates();
			
			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			if (date1) {				
				whereClause = " to_char(CREATION_TIMESTAMP  ,'mm/dd/yyyy' )='"
				+ date1 + "'";				
				filterStatement = ' Date is ' + date1 ;				
			}else{				
				whereClause = " CREATION_TIMESTAMP > sysdate -1" ;  
				filterStatement = ' Last 24 hours ';				
			}

			var affiliateId = document.getElementById('affiliate').value;			
			if (affiliateId) {
				if(affiliateId == 'All'){					
					filterStatement = filterStatement + ' and All Affiliates. ';						
				}else{					
					whereClause = whereClause + " and affiliate_id =" + affiliateId; 
					filterStatement = filterStatement + ', Affiliate Id ' + affiliateId ;
				}				
			}else{
				    filterStatement = filterStatement + ', All Affiliates. ';	
			}
			
			if (!whereClause) {
				whereClause = "CREATION_TIMESTAMP <= systimestamp";
			}

			break;
		}
		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
		//alert(whereClause);
	}
</script>
<body>
	<form method="GET" action="monitorTraffic">
		<b>Monitor Traffic</b>
		<table border="1" style="border-color: #C0C0C0">
			<tbody>
				<tr>
					<td bgcolor="#C0C0C0">Date: <input type="text" id="date1"
						name="date1" size="10" maxlength="10">
					</td>
					<td bgcolor="#66FFCC">
						<p>
							&nbsp;Affiliate: <select size="1" id="affiliate" name="affiliate">								
								<option value="All" selected="selected">All Affiliates</option>
								<c:forEach var="test" items="${affresult.rows}">
									<option value="${test.ID}">${test.ID}-${test.COMPANY_NAME}</option>
								</c:forEach>
							</select>
						</p>
					</td>
					<td><input type="submit" value="Search" name="B4"
						title="Filter requests based on field value"
						onclick="setWhereClause('B4');"></td>
				</tr>
			</tbody>
		</table>	
		<br>	
		
		<% 
		if(request.getParameter("filterStatement") != null){
			out.println( request.getParameter("filterStatement"));
		}else{
			out.println("Last 24 hours for All Affiliates");
		}
		%>
		<input id="whereClause" name="whereClause" type="hidden">
		<input id="filterStatement" name="filterStatement" type="hidden">
	</form>
	
	<table style="border:1px solid black;">
	<tr><td COLSPAN="2"><div id="visualization"></div></td>
	</tr>
	<tr>
	<td><div id="visualization2"></div></td>
	<td><div id="visualization3"></div></td>
	</tr>
	</table>
	
	<br></body>
</html>