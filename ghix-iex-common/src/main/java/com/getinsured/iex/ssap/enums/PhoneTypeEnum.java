
package com.getinsured.iex.ssap.enums;

/**
 * 
 * @author raguram_p
 *
 *
 */
public enum PhoneTypeEnum {

    MOBILE("CELL"),
    HOME("HOME"),
    WORK("WORK");
    
    private String value;

    PhoneTypeEnum(String label)
    {
    	this.value=label;
    }

	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}



	public static String value(String v) {
		if (v != null) {
			final PhoneTypeEnum data = readValue(v);
			if (data != null) {
				return data.getValue();
			}
		}
		return null;
	}

	public static PhoneTypeEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static PhoneTypeEnum readValue(String v) {
		PhoneTypeEnum data = null;
		for (PhoneTypeEnum c : PhoneTypeEnum.class.getEnumConstants()) {
			if (c.name().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
}
