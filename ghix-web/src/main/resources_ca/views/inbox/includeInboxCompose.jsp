<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<form class="form-vertical margin0" id="frmSecureInboxCompose" name="frmSecureInboxCompose" action="${pageContext.request.contextPath}/inbox/secureInboxSend" method="post" enctype="multipart/form-data">
	<df:csrfToken/>
	<input type="hidden" id="currentURL" name="currentURL" />
	
	<%-- Modal Start --%>
	<div id="new-msg" class="modal hide fade bigModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-header">
			<button type="button" title="" class="close" onclick="resetForm();" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="myModalLabel">New Message</h3>
		</div>
	
		<div class="modal-body">
	
			<div class="row-fluid">
				<div class="span12 margin-left0">
				<span id="saving" style="display: none" class="span12"> <span
					class="green">Saving your message <img
					src="${pageContext.request.contextPath}/resources/img/loader_greendots.gif"
					width="10%" alt="Loader dots"></span> 
				</span> 
				<span id="fetching" style="display: none" class="span12"> <span
					class="green">Fetching your message for editing <img
					src="${pageContext.request.contextPath}/resources/img/loader_greendots.gif"
					width="10%" alt="Loader dots"></span> 
				</span> 
				<span id="loadingContacts" class="span12 margin-left0" style="display:none"> <span class="green">Loading
						your Contacts <img
					src="${pageContext.request.contextPath}/resources/img/loader_greendots.gif"
					width="10%" alt="Loader dots"></span> 
				</span>
				<span id="cancelling" style="display: none" class="span12"> <span
					class="green">Cancelling your message <img
					src="${pageContext.request.contextPath}/resources/img/loader_greendots.gif"
					width="10%" alt="Loader dots"></span> 
				</span>
				<br>
					<div class="gutter10">
						<div id="">
								<div class="profile">
									<div class="control-group">
										<input id="id" name="id" type="hidden">
										<!-- Below two hidden fields will be used in case of replies and forwards -->
										<input id="previousId" name="previousId" type="hidden">
										<input id="status" name="status" type="hidden">
										 
										<label class="span2 margin-left0" for="addressBook">To:</label>
										<div class="input-append">
											<select id="addressBook" name="addressBook"
												data-placeholder="Select recipients..."
												style="width: 543px; display: none" multiple="multiple"
												tabindex="3" class="address-book" size="10">
											</select> <input id="names" name="names" type="hidden">
										</div>
										<div class="error" id="addressBook_error"></div>
									</div>
									<div class="control-group">
										<label class="span2" for="subject">Subject:</label>
										<div>
											<input class="input-xxlarge" id="subject" name="subject"
												type="text" autocomplete="off" maxlength="1000">
											<div class="error" id="subject_error"></div>
										</div>
									</div>
									<div class="control-group">
										<span class="span2" for="files">Attachments:</span>	
										
										<!-- File upload plugin starts -->
										<div class="fileupload-buttonbar">
							            <div class="span10">
							                <div class="btn btn-small fileinput-button">
							                    <label for="fileupload">Attach files</label>
							                    <input id="fileupload" name="fileupload" type="file" multiple="multiple"
							                    	 data-url="${pageContext.request.contextPath}/inbox/secureInboxAddAttachment" title="Attach">
							                </div>
							                <div id="fileupload_error"></div>
							                <!-- The table listing the files available for attaching/removing -->
			       							<div id="upload_files_container" class="files" ></div>
			       						<!-- File upload plugin ends -->     
										<input name="forwardedAttachments" id="forwardedAttachments" type="hidden">
							            </div>
							        </div> 
								</div>
								<div class="control-group">
									<label class="control-label" for="body">Body:</label>
									<textarea id="body" name="body" rows="6" cols="300"
										class="span12" draggable="false"></textarea>
									<div class="error" id="body_error"></div>
								</div>
								<div class="control-group">
									<div>
										<button type="button" title="Save Draft" id="saveDraft" class="btn">Save Draft</button>
										<button type="button" title="Save And Close" id="saveAndClose" class="btn" aria-hidden="true" data-dismiss="modal">Save And Close</button>
										<button type="button" title="Discard" id="discard" class="btn input-small" aria-hidden="true" data-dismiss="modal">Discard</button> 
										<input type="submit" id="send" name="send" value="Send" class="btn btn-primary pull-right" style="margin-left: 10px;" title="Send">
									</div>
								</div>
								</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- Modal End --%>
</form>
<!-- Setting up the file upload plugin -->
<script type="text/javascript">

$(function () {
    $('#fileupload').fileupload({
        dataType: 'text',
        maxNumberOfFiles: 5,
        maxNumberOfFilesOriginal: 5,
        filesContainer: $('#upload_files_container'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        add: function (e, data) {
            var that = $(this).data('fileupload'),
                options = that.options,
                files = data.files;
            $.each(files, function (index, file) {
                file.error = that._hasError(file);
                if (!file.error) {
                	that._adjustMaxNumberOfFiles(-1);
                }
            });
            
            $(this).fileupload('process', data).done(function () {
                data.maxNumberOfFilesAdjusted = true;
                data.files.valid = data.isValidated = that._validate(files);
                data.context = that._renderUpload(files).data('data', data);
                options.filesContainer[
                    options.prependFiles ? 'prepend' : 'append'
                ](data.context);
                that._renderPreviews(files, data.context);
                that._forceReflow(data.context);
                that._transition(data.context).done(
                    function () {
                        if ((that._trigger('added', e, data) !== false) &&
                                (options.autoUpload || data.autoUpload) &&
                                data.autoUpload !== false && data.isValidated) {
                            data.submit();
                        }
                    }
                );
            });
        },
        uploadTemplate: function (o) {
            var rows = $();
            $('#fileupload_error').html('');
            $.each(o.files, function (index, file) {
                var row = $((file.error ? '' : '<div class="template-upload fade span9" style="height: 40px;">' +
                	'<span class="name"></span>' +
                    '<span class="size"></span>'+
                    '<div class="progress progress-info progress-striped active">' +
                    '<div class="bar" style="width:0%;"></div></div>' +
                 	'<span class="start">&nbsp;</span>')+
                    '</div>');
                row.find('.name').text(file.name);
                row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    $('#fileupload_error').html(
                    		'<label class="error" style=""><span><em class="excl">!</em>'+file.error+'</span></label>');
                }
                rows = rows.add(row);
            });
            return rows;
        },
        downloadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<div class="template-download fade pull-left">' +
                		(file.error ? '<span></span><span class="name"></span>' +
                        '<span class="size"></span><san class="error"></span>' :
                        '<span class="name"><a></a></span>' +
                         '<span class="size"></span><span></span>'
                         ) + '<span class="delete"><button class="close">&times;</button></span></div>');
                if (file.error) {
                    row.find('.name').text(file.name);
                    row.find('.error').html(
                    		'<label class="error" style=""><span><em class="excl">!</em>'+file.error+'</span></label>');
                } 
                else{
	                row.find('.size').text(o.formatFileSize(file.size));
	                row.find('.name').text(file.name);
	                row.find('.delete button')
	                        .attr('data-type', 'POST')
	                        .attr('data-url', file.name + '~' + $("#id").val() );
                }
                
                rows = rows.add(row);
            });
            return rows;
        },
    });
});

/* form validator*/	
$("#frmSecureInboxCompose").on('submit', function(e) {
	
    if(!$('[name="addressBook"]').valid()) {
        e.preventDefault();
        return false;
    }
    
    $("#discard").attr("disabled", "disabled");
	$("#saveDraft").attr("disabled", "disabled");
	$("#saveAndClose").attr("disabled", "disabled");
	$("#send").attr("disabled", "disabled");
    
	
    $("#addressBook").each(function() {
	    var options = [];
	    $(this).children("option").each(function() {
	    	if($(this).is(':selected')){
		        var $this = $(this);
		        options.push($this.text()+ ",");
	    	}
	    });
	   $("#names").val((options.join("")));
	});
});

jQuery.validator.addMethod("chkSelect", function(value, element, arg) {
	if ( $('select#addressBook option').length == 0){
		return false;
	}
	if ( $('ul.chzn-choices li').length <= 1){
		return false;
	}
	 return true;
});

var validator = $("#frmSecureInboxCompose").validate({ 
	ignore: "",
	rules : {
		"addressBook" : {chkSelect : true},
	},
	messages : {
		"addressBook" : {chkSelect : "<span><em class='excl'>!</em> Please Select recipients</span>"},
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

$(document).ready(function(){
	
	$('#currentURL').val(window.location.href.toString().split(window.location.host + '/hix')[1]);
	
	/* 
	
	    function loadContacts(){
		
		//AJAX call to get address book
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxGetAddressBook";
		
		if ( $('select#addressBook option').length <= 1) {
			
			$("#loadingContacts").show('slow','linear');
			
			$.get(pathURL, function(data) {	
					
				if(data != '' && !jQuery.isEmptyObject(data)){
					$("#addressBook").html('');
					$.each(data, function(index, value) { 		
						$('<option/>').val(index).html(value).appendTo('#addressBook');
					});
					
					$("#addressBook").html($("#addressBook option").sort(function (a, b) {
					    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
					}));
					
				}
				
				//Auto complete
				$("#addressBook").chosen().change(function() {
					if ( $('ul.chzn-choices li').length >= 1){
						$('#addressBook_error').html('');
					}
				});
				$("#loadingContacts").hide('slow','linear');
			});	
			
		}
		
	}
	loadContacts();
	
	*/
	
	$("#saveDraft,#saveAndClose").click(function(){
		saveDraftOfMessage();
		resetForm();
	});
	
	
	$('#fileupload').click(function(){
		
		//Save draft if it was not save earlier
		if($("#id").val() == null || $("#id").val() == '' || $("#id").val() == '0'){
			var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxSaveDraft";
			
			$.post(pathURL,  function(data) {
				if(data == null){
					window.location.replace("${pageContext.request.contextPath}/inbox/home");
				}
				else{
					$.each(data, function(index, value) { 	
						
						switch (index){
							case 'id' :  $("#id").val(value);break;
						}
					});
				}
			});
		}
	});
	
	$("#discard").click(function(){
	
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxDiscard";
			
		$.post(pathURL,{id:$("#id").val()},  function(data) {
				
		});

		resetForm();
	});
	
	//Editing a draft
	$('#edit').click(function(){
		
		var msgId = $('#msgId').val();
		$("#id").val(msgId);
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxGetMessage";
		
		var to = "";
		var names = "";
		
		if ($('#fetching').is(':hidden')) {
			$('#fetching').show('slow', 'linear');
		}
		
		$("#discard").attr("disabled", "disabled");
		$("#saveDraft").attr("disabled", "disabled");
		$("#saveAndClose").attr("disabled", "disabled");
		$("#send").attr("disabled", "disabled");
		
		
		$.post(pathURL,{id:msgId,status:'C'}, function(data){
			
			if(data != null){
				
				var docIds = [];
				var docNames = [];
				var docSizes = [];
				
				$.each(data, function(index, value) { 	
					
					switch (index){
						case 'subject' : $("#subject").val(value);break;
						case 'to':  to = value;break;
						case 'names' : names = value;break;
						case 'body' :  $("#body").val(value);break;
						case 'docIds' : docIds = value;break;
						case 'docNames' : docNames = value;break;
						case 'docSizes' : docSizes = value;break
					}			
				});
				
				var output = [];
				for(var i=0; i<docIds.length; i++){
					if(docIds[i] != null){
						output.push('<tr class="template-download fade in">' +
			                			'<td class="name">'+docNames[i]+'</td>' +
			                            '<td class="size">'+docSizes[i]+'</td><td colspan="2">'+
			                            '<td class="delete">'+
			                            	'<button data-type="POST" data-url="'+docNames[i]+'~'+$('#msgId').val()+'" class="close">&times;</button>'+
			                            '</td>');
					}
				}
				$('.files').html(output.join(''));
				
				//Fetching recipients
				var toIds = [];
				var toNames = [];
				
				if(to != null){
					toIds = to.split(',');
				}
				if(names != null){
					toNames = names.split(',');
				}
				for(i=0; i<toIds.length; i++){
					$('#addressBook').append("<option value='"+toIds[i]+"' selected='selected'>"+toNames[i]+"</option>");
				}
				$('#addressBook').trigger("liszt:updated");
				
				$("#discard").removeAttr("disabled");
				$("#saveDraft").removeAttr("disabled");
				$("#saveAndClose").removeAttr("disabled");
				$("#send").removeAttr("disabled");
				$('#fetching').hide('slow', 'linear');
			}
			
		});
	});
	
	//Reply message
	$('#reply').click(function(){
		
		var msgId = $('#msgId').val();
		$('#previousId').val(msgId);
		$('#status').val('P');
		
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxGetMessage";
		
		var to = "";
		var names = "";
		
		$("#discard").attr("disabled", "disabled");
		$("#saveDraft").attr("disabled", "disabled");
		$("#saveAndClose").attr("disabled", "disabled");
		$("#send").attr("disabled", "disabled");
		
		$.post(pathURL,{id:msgId,status:'P'}, function(data){
			
			if(data != null){
				
				$.each(data, function(index, value) { 	
					
					switch (index){
						case 'subject' : $("#subject").val('RE: ' + value);break;
						case 'to':  to = value;break;
						case 'names' : names = value;break;
						case 'body' :  $("#body").val(value);break;
					}			
				});
				
				$('#addressBook option[value='+to+']').remove();
				$('#addressBook').append("<option value='"+to+"' selected='selected'>"+names+"</option>");
				$('#addressBook').trigger("liszt:updated");
				
				saveDraftOfMessage();
				
				$("#discard").removeAttr("disabled");
				$("#saveDraft").removeAttr("disabled");
				$("#saveAndClose").removeAttr("disabled");
				$("#send").removeAttr("disabled");
			}
		});
	});
	
	//Forward message
	$('#forward').click(function(){
		
		var msgId = $('#msgId').val();
		$('#previousId').val(msgId);
		$('#status').val('F');
		
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxGetMessage";
		
		$("#discard").attr("disabled", "disabled");
		$("#saveDraft").attr("disabled", "disabled");
		$("#saveAndClose").attr("disabled", "disabled");
		$("#send").attr("disabled", "disabled");
		
		$.post(pathURL,{id:msgId,status:'F'}, function(data){
			
			if(data != null){
				
				var docIds = [];
				var docNames = [];
				var docSizes = [];
				
				$.each(data, function(index, value) { 	
					
					switch (index){
						case 'subject' : $("#subject").val('FW: ' + value);break;
						case 'body' :  $("#body").val(value);break;
						case 'docIds' : docIds = value;break;
						case 'docNames' : docNames = value;break;
						case 'docSizes' : docSizes = value;break;
					}	
				});
				
				var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxSaveDraft";
				
				var id = $("#id").val();
				var subject = $.trim($("#subject").val());
				var to = $.trim($("#addressBook").val());
				var body = $.trim($("#body").val());
				var names = "";
				
				//Creating recipients names list
				$("#addressBook").each(function() {
				    var options = [];
				    $(this).children("option").each(function() {
				    	if($(this).is(':selected')){
					        var $this = $(this);
					        options.push($this.text()+ ",");
				    	}
				    });
				    names = (options.join(""));
				});
				
				if ($('#saving').is(':hidden')) {
					$('#saving').show('slow', 'linear');
				}
				
				$.post(pathURL, {id:id, subject:subject, to:to, names:names, body:body,previousId:$('#previousId').val()}, function(data) {
					
					if(data != null){
					
						$.each(data, function(index, value) { 	
							
							switch (index){
								case 'id' :  $("#id").val(value);break;
								/* case 'subject' : $("#subject").val(value);break;
								case 'to': $("#to").val(value);break;
								case 'names': $("#names").val(value);break;
								case 'body' :  $("#body").val(value);break; */
							}
							
						});
						
						var output = [];
						for(var i=0; i<docIds.length; i++){
							if(docIds[i] != null){
							 	//Call to add forwarded attachment to the message
							 	var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxSaveForwardedAttachment";
								$.post(pathURL, {id:$('#id').val(), docId:docIds[i]}, function(data) {
									
									if(data == 'success'){
										output.push('<tr class="template-download fade in">' +
					                			'<td class="name">'+docNames[i]+'</td>' +
					                            '<td class="size">'+docSizes[i]+'</td><td colspan="2">'+
					                            '<td class="delete">'+
					                            	'<button data-type="POST" data-url="'+docNames[i]+'~'+$("#id").val()+'" class="btn btn-danger">Remove</button>'+
					                            '</td>');
									}
									
								});
							}
								
						}
						$('.files').html(output.join(''));		
						
						$("#discard").removeAttr("disabled");
						$("#saveDraft").removeAttr("disabled");
						$("#saveAndClose").removeAttr("disabled");
						$("#send").removeAttr("disabled");
						$('#saving').hide('slow', 'linear');
					}
				});				
			}
		});
	});
});

function resetForm() {
	
	document.getElementById('frmSecureInboxCompose').reset();
	$("#id").val('');
	$('#status').val('');
	$('.error').html('');
	$('.files').html('');
	$('#fileupload').fileupload({
		maxNumberOfFiles: 5,
	});
	$("#addressBook").val('').trigger("liszt:updated");
}

function saveDraftOfMessage(){
	
	var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxSaveDraft";
	
	var id = $("#id").val();
	var subject = $.trim($("#subject").val());
	var to = $.trim($("#addressBook").val());
	var body = $.trim($("#body").val());
	var names = "";
	
	//If all fields are blank dont save draft
	if(subject == '' && to == '' && body == ''){
		return;
	}
	
	//Creating recipients names list
	$("#addressBook").each(function() {
	    var options = [];
	    $(this).children("option").each(function() {
	    	if($(this).is(':selected')){
		        var $this = $(this);
		        options.push($this.text()+ ",");
	    	}
	    });
	    names = (options.join(""));
	});
	

	if ($('#saving').is(':hidden')) {
		$('#saving').show('slow', 'linear');
	}
	
	$("#cancel").attr("disabled", "disabled");
	$("#saveDraft").attr("disabled", "disabled");
	// $("#saveAndClose").attr("disabled", "disabled");
	$("#send").attr("disabled", "disabled");
	
	//AJAX call to controller to save Draft of a message
	$.post(pathURL, {id:id, subject:subject, to:to, names:names, body:body,previousId:$('#previousId').val()}, function(data) {	
		
		if(data == null){
			window.location.replace("${pageContext.request.contextPath}/inbox/home");
		}
		else{
			$.each(data, function(index, value) { 	
				
				switch (index){
					case 'id' :  $("#id").val(value);break;
				/* 	case 'subject' : $("#subject").val(value);break;
					case 'to': $("#to").val(value);break;
					case 'names': $("#names").val(value);break;
					case 'body' :  $("#body").val(value);break; */
				}
			});
			
			$("#cancel").removeAttr("disabled");
			$("#saveDraft").removeAttr("disabled");
			// $("#saveAndClose").removeAttr("disabled");
			$("#send").removeAttr("disabled");
			$('#saving').hide('slow', 'linear');
		}
	});
}
</script>