package com.getinsured.eligibility.at.ref.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;

/**
 * @author chopra_s
 * 
 */
@Component("referralCompareService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralCompareServiceImpl implements ReferralCompareService {
	private static final Logger LOGGER = Logger.getLogger(ReferralCompareServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired ReferralSsapApplicationService referralSsapApplicationService;
	
	@Autowired SsapUtil ssapUtil;
	
	@Override
	public void executeCompare(HouseholdMember primaryHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication, long enrolledApplicationId) {
		LOGGER.info("Compare Current Referral with Enrolled Application starts");
		//As per Srinis comment (e-MAIL#Subject-ID state specific check)Comparison will be applicable for all states
		/*if (!isStateID()) {
			LOGGER.debug("Comparison only for ID state");
			return;
		}*/

		if (enrolledApplicationId == 0l) {
			LOGGER.debug("No enrolled applications");
			return;
		}

		SsapApplication enrolledApplication = enrolledApplicationById(enrolledApplicationId);

		if (enrolledApplication == null) {
			LOGGER.debug("No enrolled applications");
			return;
		}
		LOGGER.info("Found Enrolled Application - " + enrolledApplication.getId());
		enrolledApplication = loadSsapApplicants(enrolledApplication);
		final List<SsapApplicant> applicants = enrolledApplication.getSsapApplicants();
		final List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		final boolean isExemptHouseHold = checkIsExemptHouseHold(enrolledApplication, singleStreamlinedApplication);
		final List<SsapApplicant> removedApplicants = removedFromEnrolledApplication(applicants, householdMembers, isExemptHouseHold);
		final int rSize = ReferralUtil.listSize(removedApplicants);
		LOGGER.info("Number of applicants removed from previous - " + rSize);
		if (0 != rSize) {
			handleRemovedApplicants(enrolledApplication, singleStreamlinedApplication, removedApplicants, isExemptHouseHold);
		}
		LOGGER.info("Compare Current Referral with Enrolled Application ends");
	}

	private boolean checkIsExemptHouseHold(SsapApplication enrolledApplication, SingleStreamlinedApplication singleStreamlinedApplication) {
		singleStreamlinedApplication.setExemptHousehold(enrolledApplication.getExemptHousehold());
		return StringUtils.equalsIgnoreCase(ReferralConstants.Y, enrolledApplication.getExemptHousehold());
	}

	private void handleRemovedApplicants(SsapApplication enrolledApplication, SingleStreamlinedApplication singleStreamlinedApplication, List<SsapApplicant> removedApplicants, boolean isExemptHouseHold) {
		final SingleStreamlinedApplication enrolledSsapApplication = ssapJsonBuilder.transformFromJson(enrolledApplication.getApplicationData());
		final List<HouseholdMember> enrolledHouseholdMembers = enrolledSsapApplication.getTaxHousehold().get(0).getHouseholdMember();
		final int eSize = ReferralUtil.listSize(enrolledHouseholdMembers);
		HouseholdMember enrolledHouseholdMember = null;
		final int jSize = ReferralUtil.listSize(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember());
		int personIdInc = 1 + jSize;
		SsapApplicant removedApplicant = null;
		final List<HouseholdMember> deletedHouseholdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getDeletedHouseholdMember();
		for (int i = 0; i < eSize; i++) {
			enrolledHouseholdMember = enrolledHouseholdMembers.get(i);
			removedApplicant = applicantFromList(removedApplicants, new Long(enrolledHouseholdMember.getPersonId()));
			if (null != removedApplicant) {
				enrolledHouseholdMember.setPersonId(personIdInc++);
				enrolledHouseholdMember.setExternalId(removedApplicant.getExternalApplicantId());
				enrolledHouseholdMember.setStatus(ApplicantStatusEnum.DELETED.value());
				if (isExemptHouseHold) {
					enrolledHouseholdMember.setHardshipExempt(removedApplicant.getHardshipExempt());
					enrolledHouseholdMember.setEcnNumber(removedApplicant.getEcnNumber());
				}
				enrolledHouseholdMember.setTobaccoUserIndicator(removedApplicant.getTobaccouser() != null && ReferralConstants.Y.equals(removedApplicant.getTobaccouser()));
				enrolledHouseholdMember.setApplyingForCoverageIndicator(Boolean.FALSE); //Since this member is to be removed from the application, set seeking coverage as false
				deletedHouseholdMembers.add(enrolledHouseholdMember);
			}
		}
	}

	private List<SsapApplicant> removedFromEnrolledApplication(List<SsapApplicant> applicants, List<HouseholdMember> householdMembers, boolean isExemptHouseHold) {
		final int iSize = ReferralUtil.listSize(applicants);
		final int jSize = ReferralUtil.listSize(householdMembers);
		List<SsapApplicant> removedApplicants = new ArrayList<SsapApplicant>();
		SsapApplicant applicant = null;
		HouseholdMember householdMember = null;
		boolean blnCheck = false;
		for (int i = 0; i < iSize; i++) {
			applicant = applicants.get(i);
			blnCheck = false;
			if (ApplicantStatusEnum.DELETED.value().equals(applicant.getStatus()) || ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.value().equals(applicant.getStatus())) {
				continue;
			}

			for (int j = 0; j < jSize; j++) {
				householdMember = householdMembers.get(j);
				if(isExistingMember(applicant.getExternalApplicantId(),applicant,householdMember)) {
				householdMember.setApplicantGuid(applicant.getApplicantGuid());
					householdMember.setStatus(ApplicantStatusEnum.NO_CHANGE.value());
					if (isExemptHouseHold) {
						householdMember.setHardshipExempt(applicant.getHardshipExempt());
						householdMember.setEcnNumber(applicant.getEcnNumber());
					}
					householdMember.setTobaccoUserIndicator(applicant.getTobaccouser() != null && ReferralConstants.Y.equals(applicant.getTobaccouser()));
					blnCheck = true;
					break;
				}
			}
			if (!blnCheck) {
				removedApplicants.add(applicant);
			}
		}
		return removedApplicants;
	}

	private boolean isExistingMember(String externalApplicantId,SsapApplicant applicant, HouseholdMember householdMember) {
		/*
		 * Order of Member matching
		 * 1. Check externalApplicantId matched
		 * 2. Check SSN AND DOB matched
		 * 3. Check FirstName LastName and DOB matched
		 * else 
		 * return member not matched
		 * */
		
		if ( StringUtils.equalsIgnoreCase(externalApplicantId, householdMember.getExternalId()))
		{
			return true;
		}
		else if(null!=applicant.getSsn() &&  
				Objects.equals(applicant.getSsn(), householdMember.getSocialSecurityCard().getSocialSecurityNumber()) 
				&& Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth() )) {
			return true;
		}
		else if(Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth()) &&
				StringUtils.equalsIgnoreCase(applicant.getFirstName(), householdMember.getName().getFirstName()) &&
				StringUtils.equalsIgnoreCase(applicant.getLastName(), householdMember.getName().getLastName()))
		{
			return true;
		}
		return false;
	}
	
	private boolean compareWithSsnDOB(SsapApplicant applicant, HouseholdMember householdMember) {
		 
		if(Objects.equals(applicant.getSsn(), householdMember.getSocialSecurityCard().getSocialSecurityNumber()) && Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth() )) {
			return true;
		}
		 
		return false;
	}

	private boolean compareWithSsnDOBDto(SsapApplicant applicant, SsapApplicant newApplicant) {
		 
		if(Objects.equals(applicant.getSsn(), newApplicant.getSsn()) && Objects.equals(applicant.getBirthDate(), newApplicant.getBirthDate() )) {
			return true;
		}
		 
		return false;
	}

	private SsapApplication enrolledApplicationById(long enrolledApplicationId) {
		return ssapApplicationRepository.findOne(enrolledApplicationId);
	}

	private SsapApplication loadSsapApplicants(SsapApplication enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp.getId());

	}

	private boolean isStateID() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "ID".equals(stateCode);
	}

	private static SsapApplicant applicantFromList(List<SsapApplicant> removedApplicants, Long personId) {
		SsapApplicant ssapApplicantReturn = null;
		for (SsapApplicant ssapApplicant : removedApplicants) {
			if (ssapApplicant.getPersonId() == personId) {
				ssapApplicantReturn = ssapApplicant;
				break;
			}
		}
		return ssapApplicantReturn;
	}
	
	@Override
	public void executeCompareFromDto(AccountTransferRequestDTO accountTransferRequestDTO) {
		LOGGER.info("Compare Current Referral with Enrolled Application starts");
		//As per Srinis comment (e-MAIL#Subject-ID state specific check)Comparison will be applicable for all states
		/*if (!isStateID()) {
			LOGGER.debug("Comparison only for ID state");
			return;
		}*/
		String caseNumber = accountTransferRequestDTO.getCaseNumber();
		long enrolledApplicationId = accountTransferRequestDTO.getEnrolledApplicationId();

		if (enrolledApplicationId == 0l) {
			LOGGER.debug("No enrolled applications");
			return;
}

		SsapApplication enrolledApplication = enrolledApplicationById(enrolledApplicationId);

		if (enrolledApplication == null) {
			LOGGER.debug("No enrolled applications");
			return;
		}
		LOGGER.info("Found Enrolled Application - " + enrolledApplication.getId());
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);

		SsapApplication ssapApplication = null;
		if (ssapApplications != null && ssapApplications.size() > 0) {
			ssapApplication = ssapApplications.get(0);
		} else {
			LOGGER.debug("No current applications");
			throw new GIRuntimeException("No ssap application found!");
		}
		enrolledApplication = loadSsapApplicants(enrolledApplication);
		final List<SsapApplicant> applicants = enrolledApplication.getSsapApplicants();
		final List<SsapApplicant> newApplicants = ssapApplication.getSsapApplicants();
		//final List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		String ssapJson = ssapApplication.getApplicationData();
		SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapJson);
		//final boolean isExemptHouseHold = checkIsExemptHouseHold(enrolledApplication, singleStreamlinedApplication);
		final List<SsapApplicant> removedApplicants = removedFromEnrolledApplicationDto(applicants,newApplicants);
		final int rSize = ReferralUtil.listSize(removedApplicants);
		LOGGER.info("Number of applicants removed from previous - " + rSize);
		if (0 != rSize) {
			handleRemovedApplicantsDto(enrolledApplication, singleStreamlinedApplication, removedApplicants, ssapApplication, accountTransferRequestDTO);
		}
		LOGGER.info("Compare Current Referral with Enrolled Application ends");
	}
	
	private List<SsapApplicant> removedFromEnrolledApplicationDto(List<SsapApplicant> applicants, List<SsapApplicant> newApplicants) {
		final int iSize = ReferralUtil.listSize(applicants);
		final int jSize = ReferralUtil.listSize(newApplicants);
		List<SsapApplicant> removedApplicants = new ArrayList<SsapApplicant>();
		SsapApplicant applicant = null;
		SsapApplicant newApplicant = null;
		
		boolean blnCheck = false;
		
		// new applicants status set to ADD_NEW if status is NULL
		for(SsapApplicant ssapApplicant : newApplicants) {
			if(ssapApplicant.getStatus() == null) {
				ssapApplicant.setStatus(ApplicantStatusEnum.ADD_NEW.value());
			}
		}
		 
		for (int i = 0; i < iSize; i++) {
			applicant = applicants.get(i);
			blnCheck = false;
			if (ApplicantStatusEnum.DELETED.value().equals(applicant.getStatus()) || ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.value().equals(applicant.getStatus())) {
				continue;
			}

			for (int j = 0; j < jSize; j++) {
				
					newApplicant = newApplicants.get(j);
					//newApplicant.setStatus(ApplicantStatusEnum.ADD_NEW.value());
					if ((null!=applicant.getExternalApplicantId() && null!=newApplicant.getExternalApplicantId() && StringUtils.equalsIgnoreCase(applicant.getExternalApplicantId(), newApplicant.getExternalApplicantId()))
							|| (null!=applicant.getApplicantGuid() && null!=newApplicant.getApplicantGuid() && StringUtils.equalsIgnoreCase(applicant.getApplicantGuid(), newApplicant.getApplicantGuid()) )
							|| compareWithSsnDOBDto(applicant,newApplicant)      ) {
						newApplicant.setApplicantGuid(applicant.getApplicantGuid());
						newApplicant.setStatus(ApplicantStatusEnum.NO_CHANGE.value());
						newApplicant.setTobaccouser(applicant.getTobaccouser());
						blnCheck = true;
						break;
					}
				
			}
			if (!blnCheck) {
				removedApplicants.add(applicant);
			}
		}
		persistSSAPApplicantStatus(newApplicants);
		return removedApplicants;
	}
	
	
	private void handleRemovedApplicantsDto(SsapApplication enrolledApplication, SingleStreamlinedApplication singleStreamlinedApplication, 
			List<SsapApplicant> removedApplicants, SsapApplication newSsapApplication, AccountTransferRequestDTO accountTransferRequestDTO) {
		
		final SingleStreamlinedApplication enrolledSsapApplication = ssapJsonBuilder.transformFromJson(enrolledApplication.getApplicationData());
		final List<HouseholdMember> enrolledHouseholdMembers = enrolledSsapApplication.getTaxHousehold().get(0).getHouseholdMember();
		final List<HouseholdMember> currentHouseholdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		String responsibleMemberPersonId = ssapUtil.getPrimaryTaxFilerPersonIdFromHouseholdMembers(currentHouseholdMembers);
		
		final int eSize = ReferralUtil.listSize(enrolledHouseholdMembers);
		HouseholdMember enrolledHouseholdMember = null;
		final int jSize = ReferralUtil.listSize(currentHouseholdMembers);
		int personIdInc = 1 + jSize;
		SsapApplicant removedApplicant = null;
		final List<HouseholdMember> deletedHouseholdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getDeletedHouseholdMember();
		
		for (int i = 0; i < eSize; i++) {
			enrolledHouseholdMember = enrolledHouseholdMembers.get(i);
			removedApplicant = applicantFromList(removedApplicants, new Long(enrolledHouseholdMember.getPersonId()));
			if (null != removedApplicant) {
				enrolledHouseholdMember.setPersonId(personIdInc++);
				enrolledHouseholdMember.setExternalId(removedApplicant.getExternalApplicantId());
				enrolledHouseholdMember.setStatus(ApplicantStatusEnum.DELETED.value());
				
				enrolledHouseholdMember.setTobaccoUserIndicator(removedApplicant.getTobaccouser() != null && ReferralConstants.Y.equals(removedApplicant.getTobaccouser()));
				deletedHouseholdMembers.add(enrolledHouseholdMember);
			}
		}//add null check here
		int deletedSize = deletedHouseholdMembers.size();
		for (int i = 0; i < deletedSize; i++) {
			referralSsapApplicationService.populateSsapApplicants(newSsapApplication, deletedHouseholdMembers.get(i), ReferralConstants.N, accountTransferRequestDTO.isQE(), null,responsibleMemberPersonId);
		}
		this.persistSSAPApplicantStatus(newSsapApplication.getSsapApplicants());
		final String ssapJson = ssapAssembler.transformSsapToJson(singleStreamlinedApplication);
		updateSSAPJSON(newSsapApplication.getId(), ssapJson);
	}
	
	private void updateSSAPJSON(long ssapApplicationId, String ssapJson) {
		SsapApplication ssapapplication = ssapApplicationRepository.findOne(ssapApplicationId);
		ssapapplication.setApplicationData(ssapJson);
		ssapApplicationRepository.save(ssapapplication);
	}
	
	private void persistSSAPApplicantStatus(List<SsapApplicant> newApplicants) {
		for (SsapApplicant currentApplicant : newApplicants) {
			ssapApplicantRepository.save(currentApplicant);
		}
	}
}
