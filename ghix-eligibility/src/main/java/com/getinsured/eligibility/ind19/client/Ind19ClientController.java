package com.getinsured.eligibility.ind19.client;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.dto.ind19.Ind19ClientResponse;

@Controller("ind19ClientController")
public class Ind19ClientController extends Ind19ClientUtil{

	private static final Logger LOGGER = Logger.getLogger(Ind19ClientController.class);

	@RequestMapping(value = "/postToPlanSelection", method = RequestMethod.POST)
	@ResponseBody
	public Ind19ClientResponse callInd19(@RequestBody Ind19ClientRequest ind19ClientRequest) {
		LOGGER.debug("IND19Client invoked");
		return consumeInd19(ind19ClientRequest);
	}
}
