<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
	#titlebar:after {
		border-bottom: none;
	}
</style>

<div class="gutter10">
	<div class="topnav">
  		<ul class="inline">
    		<li class="active"><a href="#">Verify Information</a></li>
    		<li><a href="#">Update Dependents</a></li>
    		<li><a href="#">Select Plans</a></li>
    		<li><a href="#" class="last">Enroll</a></li>
  		</ul>
	</div> 
	<div class="row-fluid" id="titlebar">
    	<h1><spring:message  code="label.verifyEmployee"/><a href="<c:url value="/shop/employee/editemployee/${employeeApplication.id}" />" class="btn btn-small pull-right"><spring:message  code="label.edit"/></a></h1>
	</div>
  	<div class="row-fluid">
    	<div class="span3" id="sidebar"></div>
    	<!-- /.sidebar-->
    
    <div class="span9" id="rightpanel">
      <div class="row-fluid">
          <p><spring:message  code="label.ifoaddedbyemployer"/></p>
          <table class="table table-border-none">
            <tbody>
              <tr>
                <td class="span3"><spring:message  code="label.emprname"/></td>
                <td><strong>${empDetailsApplication.firstName} ${empDetailsApplication.lastName} ${empDetailsApplication.suffix}</strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.dob"/></td>
                <fmt:formatDate pattern="MM/dd/yyyy" value="${empDetailsApplication.dob}" var="dob" />
                <td><strong> ${dob} </strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.taxId"/></td>
                <c:if test="${empDetailsApplication.ssn != null && empDetailsApplication.ssn != ''}">
				<td>
					<c:set var="ssnParts" value="${fn:split(empDetailsApplication.ssn,'-')}" />
					<strong>	***-**-${ssnParts[2]}</strong>
				</td>
				</c:if>
              </tr>
              <tr>
                <td><spring:message  code="label.gender"/></td>
                <td><strong>${fn:toUpperCase(fn:substring(empDetailsApplication.gender, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.gender, 1, -1))}</strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.martialstatus"/></td>
                <td><strong>${martialStatus}</strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.emplEmail"/></td>
                <td><strong>${empDetailsApplication.email}</strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.homeAddress"/></td>
                <td>
                <br>
                <strong>
                 ${empDetailsApplication.location.address1} <br>
                 ${empDetailsApplication.location.address2} ${(empty empDetailsApplication.location.address2) ? '' : '<br>'} 
                 ${empDetailsApplication.location.city} <br> 
                 ${empDetailsApplication.location.state}  <br>
                 ${empDetailsApplication.location.zip}<br>
                 ${fn:toUpperCase(fn:substring(empDetailsApplication.location.county, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.location.county, 1, -1))}</strong></td>
              </tr>
              <tr>
                <td><spring:message  code="label.phonenumber"/></td>
                <td><strong>${empDetailsApplication.contactNumber}</strong></td>
              </tr>
				<tr>
					<td><spring:message  code="label.tobacoUser"/></td>
                	<td>
		           	   	<c:choose>
		               	  	<c:when test="${empDetailsApplication.smoker == null}">
								<weak>N/A</weak>
							</c:when>
							<c:otherwise>
		               			<strong>${fn:toUpperCase(fn:substring(empDetailsApplication.smoker, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.smoker, 1, -1))}</strong>
		               		</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<c:if test="${empDetailsApplication.smoker=='YES' && empDetailsApplication.tobaccoCessPrg!=null}">
				<tr>
					<td><spring:message  code="label.tobaccoCessationPrg"/></td>
                	<td>
               			<strong>${fn:toUpperCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 1, -1))}</strong>
					</td>
				</tr>
				</c:if>
				<tr>
					<td><spring:message  code="label.nativeAmerican"/></td>
		       		<td>
		       			<c:choose>
						    <c:when test="${empDetailsApplication.nativeAmr == null}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.nativeAmr, 1, -1))}</strong>
						    </c:otherwise>
						</c:choose>
					</td>
				</tr>
				<c:if test="${frtDocId !='none'}">
				<tr>
					<td><spring:message  code="label.uploadsupportingdocument"/></td>
		       		<td>
		       			<strong>${frtDocName} </strong> <a href="javascript:downloadFile('${frtDocId}')">download</a>
					</td>
				</tr>
				</c:if>
              <tr style="display: none;">
                <td><spring:message  code="label.worksitesh2"/></td>
                <td>
                <strong>
                 ${employee.employerLocation.location.address1} <br>
                 ${employee.employerLocation.location.address2} ${(empty employee.employerLocation.location.address2) ? '' : '<br>'} 
                 ${employee.employerLocation.location.city} <br> 
                 ${employee.employerLocation.location.state}  <br>
                 ${employee.employerLocation.location.zip}<br>
                 ${fn:toUpperCase(fn:substring(employee.employerLocation.location.county, 0, 1))}${fn:toLowerCase(fn:substring(employee.employerLocation.location.county, 1, -1))}</strong></td>
              </tr>
              <%-- <tr>
                <td><spring:message  code="label.estimatedannualsal"/></td>
                <fmt:formatNumber value="${employee.yearlyIncome}" var="incomeNumber" type="number"  groupingUsed="false" maxFractionDigits="2"/>
                <fmt:formatNumber value="${incomeNumber}" var="incomeNumber" type="currency" currencySymbol="$"/>
                <td><strong>${incomeNumber}</strong></td>
              </tr>  --%>
            </tbody>
          </table>
          <div class="form-actions margin-top50">
            <a href="<c:url value='/shop/employee/editemployee/${employeeApplication.id}'/>" class="btn pull-left"> <spring:message  code="label.newhireback"/> </a>
            <c:if test="${isSpEnrollment == false}">
            	<a href= "<c:url value='/shop/employee/verifydependent/${employeeApplication.id}' />" class="btn btn-primary pull-right">
            </c:if>
            <c:if test="${isSpEnrollment == true}">
            	<a href="<c:url value='/shop/employee/spplanselection/spenrollverifydependent/${employeeApplication.id}' />" class="btn btn-primary pull-right">
            </c:if>  
             <spring:message  code="label.next"/> </a>
          </div>     
        <!--gutter10--> 
      </div>
      <!--row-fluid--> 
    </div>
    <!--rightpanel--> 
  </div>
  <!--row-fluid--> 
</div>       
<!--gutter10--> 
<jsp:include page="waive_employer_coverage.jsp" >
	<jsp:param name="employeeId" value="${employee.id}" />
</jsp:include>  
<script type="text/javascript">
$(document).ready(function(){
$('.info').tooltip();
$('i.icon-ok').parents('li').css('list-style','none');

});

function downloadFile(fileId){
	var  contextPath =  "<%=request.getContextPath()%>";

	var url = contextPath+"/shop/employee/downloadDocument?udId="+fileId;
	var subUrl =  '<c:url value="'+url+'"/>';
	window.open(subUrl,"Supported Document");
}

</script>