package com.getinsured.ssap.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.ridp.exception.RIDPServiceException;
import com.getinsured.hix.ridp.service.RIDPService;
import com.getinsured.hix.ssap.SsapConstants;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.exception.ExceptionType;
import com.getinsured.ssap.exception.InvalidStateException;
import com.getinsured.ssap.exception.VerificationRequiredException;
import com.getinsured.ssap.model.SaveRequest;
import com.getinsured.ssap.model.financial.IfsvResponse;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecVerificationResponse;
import com.getinsured.ssap.service.IfsvService;
import com.getinsured.ssap.service.NonEsiMecService;
import com.getinsured.ssap.service.SingleStreamlinedApplicationService;
import com.getinsured.timeshift.TSLocalTime;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

/**
 * Single Streamlined Application controller.
 *
 * @author Yevgen Golubenko
 * @since 2018-12-21
 */
@RestController
public class SingleStreamlinedApplicationController {
  private static final Logger log = LoggerFactory.getLogger(SingleStreamlinedApplicationController.class);

  private final SingleStreamlinedApplicationService singleStreamlinedApplicationService;
  private final UserService userService;
  private final RIDPService ridpService;
  private final NonEsiMecService nmecService;
  private final ConsumerPortalUtil consumerPortalUtil;
  private final IfsvService ifsvService;
  private final Gson gson;

  @Autowired
  public SingleStreamlinedApplicationController(final SingleStreamlinedApplicationService singleStreamlinedApplicationService,
                                                final UserService userService,
                                                final RIDPService ridpService,
                                                final NonEsiMecService nmecService,
                                                final ConsumerPortalUtil consumerPortalUtil,
                                                final IfsvService ifsvService)
  {
    this.singleStreamlinedApplicationService = singleStreamlinedApplicationService;
    this.userService = userService;
    this.ridpService = ridpService;
    this.nmecService = nmecService;
    this.consumerPortalUtil = consumerPortalUtil;
    this.ifsvService = ifsvService;
    this.gson = new Gson();
  }

  @GiAudit(
      transactionName = "Start new SSAP application process",
      eventType = EventTypeEnum.PII_READ,
      eventName = EventNameEnum.SSAP_APP)
  @PreAuthorize(SsapConstants.SSAP_EDIT)
  @RequestMapping(value = "/newssap/start")
  public ModelAndView startApplication(
      @RequestParam(value = "ssapId", required = false, defaultValue = "0") long ssapId,
      @RequestParam(value = "caseNumber", required = false) String caseNumber,
      @RequestParam(value = "applicationType", required = false) String applicationType,
      @RequestParam(value = "coverageYear", required = false, defaultValue = "0") int coverageYear,
      @RequestParam(value = "mode", required = false, defaultValue = "edit") String mode,
      HttpServletRequest request) {

    ModelAndView modelAndView = new ModelAndView("ssapmain");

    if(ssapId == 0 && caseNumber == null && coverageYear == 0) {
      coverageYear = TSLocalTime.getLocalDateInstance().getYear();
    }

    // If no ssapId/case number passed, we consider this as user trying to start new SSAP
    if(coverageYear != 0 && ssapId == 0 && caseNumber == null) {
      try
      {
        if(!singleStreamlinedApplicationService.isNewApplicationCreationAllowed(userService.getLoggedInUser(), coverageYear)) {
          RedirectView redirectView = new RedirectView();
          redirectView.setContextRelative(true);
          redirectView.setUrl("/indportal");
          modelAndView.setView(redirectView);
          modelAndView.addObject("ssap.error", "User not allowed to create new SSAP application for given coverage year");
          return modelAndView;
        }
      }
      catch (Exception e)
      {
        log.error("Exception while getting applications for coverage year: " + coverageYear , e);
      }
    }

    // Only do this extra call, if some old code is calling this endpoint with caseNumber instead of
    // ssapId.
    if(ssapId == 0 && caseNumber != null) {
      final SsapApplication ssapApplication = singleStreamlinedApplicationService.getSsapApplicationByCaseNumber(caseNumber);

      if(ssapApplication != null) {
        ssapId = ssapApplication.getId();
      }
    }

    if(ssapId != 0) {
      try {
        SingleStreamlinedApplication oldApplication = singleStreamlinedApplicationService.getApplicationByApplicationId(ssapId);

        if(applicationType == null || "".equals(applicationType)) {
          applicationType = oldApplication.getApplicationType();
        }

        if(coverageYear == 0) {
          coverageYear = Math.toIntExact(oldApplication.getCoverageYear());
        }

      } catch (Exception e) {
        log.error("Unable to get old SSAP to set applicationType/coverage year", e);
      }
    }

    boolean csrOverride = false;

    try
    {
      boolean ridpVerificationRequired = ridpService.isRidpRequired();

      Object csrOverrideFromCap = request.getSession(false).getAttribute(SsapConstants.CSR_OVER_RIDE);

      if(csrOverrideFromCap instanceof Boolean) {
        csrOverride = (boolean) csrOverrideFromCap;
        ridpVerificationRequired = !csrOverride;
      }

      if(ridpVerificationRequired) {
        Household household = consumerPortalUtil.getHouseholdRecord(userService.getLoggedInUser().getActiveModuleId());

        if(household != null && !"Y".equalsIgnoreCase(household.getRidpVerified())) {
          final HttpSession session = request.getSession(false);

          if(session.getAttribute("ssapVisited") == null)
          {
            session.setAttribute("ssapVisited", true);
            RedirectView redirectView = new RedirectView();
            redirectView.setContextRelative(true);
            StringBuilder ridpUrl = new StringBuilder("/ridp/verification?coverageYear=").append(coverageYear);
            
            if(caseNumber != null) {
            	ridpUrl.append("&caseNumber=").append(caseNumber);
            }
            if(applicationType != null) {
            	ridpUrl.append("&applicationType=").append(applicationType);
            }
           
            redirectView.setUrl(ridpUrl.toString());
            modelAndView.setView(redirectView);

            if (caseNumber != null)
            {
              modelAndView.addObject("caseNumber", caseNumber);
            }

            return modelAndView;
          }
        }
      }
    }
    catch (RIDPServiceException e)
    {
      log.error("Unable to check if RIDP verification is enabled for current environment, assuming no.", e);
    }
    catch (InvalidUserException e)
    {
      log.error("Unable to get currently logged in user to check if RIDP is verified for the household.", e);
    }

    modelAndView.addObject("ssapId", ssapId);
    modelAndView.addObject("applicationType", applicationType);
    modelAndView.addObject("coverageYear", coverageYear);
    modelAndView.addObject("mode", mode);
    modelAndView.addObject("csrOverride", csrOverride);

    return modelAndView;
  }

  @RequestMapping(value = "/newssap/save", method = RequestMethod.POST)
  @PreAuthorize(SsapConstants.SSAP_EDIT)
  public ResponseEntity<?> save(@RequestBody SaveRequest saveRequest, HttpServletRequest request) {
    log.info("[new] Saving Single Streamlined Application");
    final HttpSession session = request.getSession(false);

    if (session != null &&
        session.getAttribute(SsapConstants.CSR_OVER_RIDE) != null &&
        session.getAttribute(SsapConstants.CSR_OVER_RIDE).toString().equalsIgnoreCase("Y")) {
      saveRequest.setCsrOverride(true);
    }

    saveRequest.setRemoteClientAddress(getClientIpAddress(request));

    SingleStreamlinedApplication savedApplication = null;

    try {
      // TODO: Convert ssapApplicationId to long
      if (saveRequest.getApplicationId() == 0 && saveRequest.getSsapApplication().getSsapApplicationId() != null
          && !saveRequest.getSsapApplication().getSsapApplicationId().trim().equals("")) {
        try {
          saveRequest.setApplicationId(Long.parseLong(saveRequest.getSsapApplication().getSsapApplicationId()));
        } catch (NumberFormatException nfe) {
          log.error("Unable to parse ssapApplicationId as Long", nfe);
        }
      }

      saveRequest.getSsapApplication().setClientIp(saveRequest.getRemoteClientAddress());

      log.debug("Got SSAP application request:\n\n====\n\n{}\n\n====\n\n", gson.toJson(saveRequest));

      savedApplication = singleStreamlinedApplicationService.save(saveRequest);
    } catch (VerificationRequiredException vre) {
      log.error("Verification was required for given application: {} => {}", vre.getType(), vre.getMessage());

      Map<String, Object> errorResponseMap = getExceptionErrorResponseMap(saveRequest.getSsapApplication(), vre.getMessage(), vre.getType());
      return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(errorResponseMap);
    } catch (InvalidStateException ise) {
      log.error("Invalid state exception for given application, type: {} => msg: {}", ise.getType(), ise.getMessage());
      Map<String, Object> errorResponseMap = getExceptionErrorResponseMap(saveRequest.getSsapApplication(), ise.getMessage(), ise.getType());
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(errorResponseMap);
    } catch (Exception e) {
      log.error("[new] Unable to save Single Streamlined Application", e);
      Map<String, Object> errorResponseMap = getExceptionErrorResponseMapForException(saveRequest.getSsapApplication(), e.getMessage(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseMap);
    }

    return ResponseEntity.ok(savedApplication);
  }

  /**
   * Returns remote IP address.
   *
   * @param request HTTP Servlet Request object.
   * @return remote IP address.
   */
  private String getClientIpAddress(final HttpServletRequest request) {
    String remoteIpAddress = request.getRemoteAddr();

    final String xForwardedFor = request.getHeader("X-FORWARDED-FOR");

    if(xForwardedFor != null && !"".equals(xForwardedFor)) {
      remoteIpAddress = xForwardedFor;
    }

    return remoteIpAddress;
  }

  @RequestMapping(value = "/newssap/application/{applicationId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize(SsapConstants.SSAP_VIEW)
  public ResponseEntity<?> getApplicationById(@PathVariable Long applicationId) {
    SingleStreamlinedApplication app = null;
    try {
      app = singleStreamlinedApplicationService.getApplicationByApplicationId(applicationId);
    } catch (Exception e) {
      log.error("Unable to get Single Streamlined Application for given applicationId: {}", applicationId);
      log.error("Exception while getting SingleStreamlinedApplication", e);
      Map<String, Object> errorResponseMap = getExceptionErrorResponseMapForException(null, e.getMessage(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseMap);
    }

    if (app == null) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(app);
  }

  @RequestMapping(value = "/newssap/applications/year/{coverageYear}")
  @PreAuthorize(SsapConstants.SSAP_VIEW)
  public ResponseEntity<?> getApplicationResourcesForCoverageYear(@PathVariable int coverageYear) {
    List<SsapApplicationResource> apps;
    log.info("Getting SSAP applications for coverage year: {}", coverageYear);
    try {
      apps = singleStreamlinedApplicationService.getSsapApplicationsForYear(coverageYear);
      log.info("Returning SSAP for coverage year: {} (# of apps: {})", coverageYear, apps.size());
      return ResponseEntity.ok(apps);
    } catch (Exception e) {
      log.error("Exception occurred while getting SSAP for coverage year: {}", coverageYear);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getExceptionErrorResponseMapForException(null, e.getMessage(), e));
    }
  }

  private Map<String, Object> getExceptionErrorResponseMap(SingleStreamlinedApplication application,
                                                           String message,
                                                           ExceptionType type) {
    final Map<String, Object> m = new LinkedHashMap<>();
    m.put("message", message);
    m.put("type", type);
    m.put("date", TSDate.getNoOffsetTSDate(new Date()).toString());
    m.put("application", application);
    return m;
  }

  private Map<String, Object> getExceptionErrorResponseMapForException(SingleStreamlinedApplication application,
                                                                       String message,
                                                                       Exception ex) {
    final Map<String, Object> m = new LinkedHashMap<>();
    m.put("message", message);
    m.put("date", TSDate.getNoOffsetTSDate(new Date()).toString());
    m.put("application", application);
    return m;
  }


  @RequestMapping(value = "/newssap/internal/NMEC/verification/{ssapId}")
  @PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
  public ResponseEntity<?> triggerNMECVerificationForSsapId(@PathVariable long ssapId ) {
    SingleStreamlinedApplication app = null;
    try {
      app = singleStreamlinedApplicationService.getApplicationByApplicationId(ssapId);
      if (app == null) {
        return ResponseEntity.notFound().build();
      }
    else
    {
      log.info("calling NMEC service ");
      final NonEsiMecVerificationResponse nmecResponse = nmecService.invoke(ssapId);
      log.info("NMEC Response: {}", nmecResponse);
    }
    } catch (Exception e) {
      log.error("Unable to get Single Streamlined Application for given applicationId: {}", ssapId);
      log.error("Exception while getting SingleStreamlinedApplication", e);
      Map<String, Object> errorResponseMap = getExceptionErrorResponseMapForException(null, e.getMessage(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseMap);
    }
    return ResponseEntity.ok("NMEC Verification Triggerd for "+ssapId);
  }
  
  @RequestMapping(value = "/newssap/internal/IFSV/verification/{ssapId}")
  @PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
  public ResponseEntity<?> triggerIFSVVerificationForSsapId(@PathVariable long ssapId ) {
    SingleStreamlinedApplication app = null;
    try {
      app = singleStreamlinedApplicationService.getApplicationByApplicationId(ssapId);
      if (app == null) {
        return ResponseEntity.notFound().build();
      }
    else
    {
      log.info("calling IFSV service ");
      final IfsvResponse ifsvResponse = ifsvService.invoke(ssapId);
      log.info("IFSV Response: {}", ifsvResponse);
    }
    } catch (Exception e) {
      log.error("Unable to get Single Streamlined Application for given applicationId: {}", ssapId);
      log.error("Exception while getting SingleStreamlinedApplication", e);
      Map<String, Object> errorResponseMap = getExceptionErrorResponseMapForException(null, e.getMessage(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseMap);
    }
    return ResponseEntity.ok("IFSV Verification Triggerd for "+ssapId);
  }
  
}
