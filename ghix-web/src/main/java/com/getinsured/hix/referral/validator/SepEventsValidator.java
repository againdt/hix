package com.getinsured.hix.referral.validator;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.referral.ui.dto.SepEventDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * 
 * @author raguram_p
 * 
 */

@Component("sepEventsValidator")
@Scope("singleton")
public class SepEventsValidator implements Validator {
	private static final String IDAHO = "ID";
	@Override
	public boolean supports(Class<?> arg0) {
		return LceActivityDTO.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		LceActivityDTO lceActivityDTO = (LceActivityDTO) arg0;
		if (lceActivityDTO != null) {
			DateTime todaysDate = new DateTime(new TSDate());
			int i = 0;
			for (String addedApplicantGuid : lceActivityDTO.getAddedMembersMap().keySet()) {
				SepEventDTO sepEvent = lceActivityDTO.getAddedMembersMap().get(addedApplicantGuid);
				
				String msg = checkEventDateisValid(sepEvent.getEventDate());
				if (StringUtils.isNotEmpty(msg)) {
					arg1.rejectValue("addedMembersMap["+addedApplicantGuid+"].eventDate", msg + " for " + sepEvent.getApplicantName());
				} else if(!ReferralConstants.Y.equals(sepEvent.getAllowFutureEvent()) ) {
					i = i + checkEventDateisFuture(todaysDate, sepEvent.getEventDate());
				}
				if (StringUtils.isEmpty(sepEvent.getName())) {
					arg1.rejectValue("addedMembersMap["+addedApplicantGuid+"].name", "Event Name not Selected for " + sepEvent.getApplicantName());
				}
			}
			for (String removedApplicantGuid : lceActivityDTO.getRemovedMembersMap().keySet()) {
				SepEventDTO sepEvent = lceActivityDTO.getRemovedMembersMap().get(removedApplicantGuid);
				{
					String msg = checkEventDateisValid(sepEvent.getEventDate());
					if (StringUtils.isNotEmpty(msg)) {
						arg1.rejectValue("removedMembersMap["+removedApplicantGuid+"].eventDate", msg + " for " + sepEvent.getApplicantName());
					} else if(!ReferralConstants.Y.equals(sepEvent.getAllowFutureEvent())) {
						
						i = i + checkEventDateisFuture(todaysDate, sepEvent.getEventDate());
					}
					if (StringUtils.isEmpty(sepEvent.getName())) {
						arg1.rejectValue("removedMembersMap["+removedApplicantGuid+"].name", "Event Name not Selected for " + sepEvent.getApplicantName());
					}
				}
			}
			if (lceActivityDTO.getChangeOfAddressMembersMap() != null && lceActivityDTO.getChangeOfAddressMembersMap().keySet() != null && lceActivityDTO.getChangeOfAddressMembersMap().keySet().size() > 0) {
				String msg = checkEventDateisValid(lceActivityDTO.getZipCountyChangeEventDate());
				if (StringUtils.isNotEmpty(msg)) {
					arg1.rejectValue("zipCountyChangeEventDate", msg + " for " + "Change of Address");
				} else {
					i = i + checkEventDateisFuture(todaysDate, lceActivityDTO.getZipCountyChangeEventDate());
				}
			}
			if (lceActivityDTO.getHouseholdChangeMembersMap() != null && lceActivityDTO.getHouseholdChangeMembersMap().keySet() != null && lceActivityDTO.getHouseholdChangeMembersMap().keySet().size() > 0
			        && lceActivityDTO.getHouseholdDisplayed() != null) {
				String msg = checkEventDateisValid(lceActivityDTO.getHouseholdChangeEventDate());
				if (StringUtils.isNotEmpty(msg)) {
					arg1.rejectValue("householdChangeEventDate", msg + " for " + "Change in eligibility for entire household");
				} else {
					i = i + checkEventDateisFuture(todaysDate, lceActivityDTO.getHouseholdChangeEventDate());
				}
				if (StringUtils.isEmpty(lceActivityDTO.getHouseholdChangeReason())) {
					arg1.rejectValue("householdChangeReason", "Event Name not selected for Change in eligibility for entire household");
				}
			}

			if (lceActivityDTO.getQualifyingEventMap() != null && lceActivityDTO.getQualifyingEventMap().keySet() != null && lceActivityDTO.getQualifyingEventMap().keySet().size() > 0) {
				
				String msg = checkEventDateisValid(lceActivityDTO.getQualifyEventDate());
				if (StringUtils.isNotBlank(msg)) {
					arg1.rejectValue("qualifyEventDate", msg + " for " + " qualifying events");
				} else if(!ReferralConstants.Y.equals(lceActivityDTO.getQualifyEventAllowFuture())){
					i = i + checkEventDateisFuture(todaysDate, lceActivityDTO.getQualifyEventDate());
				}
				if (StringUtils.isEmpty(lceActivityDTO.getQualifyEventSelected())) {
					arg1.rejectValue("qualifyEventSelected", "Event Name not selected for qualifying events");
				}
				
			}
			if (i > 0) {
				/**
				 * Get the exchange name and phone number from gi_app_config
				 * HIX-112192
				 */
				final String stateCode =  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				final String exchangeName =  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
				final String exchangePhone =  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
				String errorMessage = null;
				if(stateCode.equalsIgnoreCase(IDAHO)) {
					errorMessage= "At this time, Life Change Events that are happening in the future can only be reported over the phone. Please call 1-855-YH-IDAHO"+ " ("+exchangePhone+") to report your event and to see if you qualify for a Special Enrollment Period.";	
				} else {
					errorMessage= "At this time, Life Change Events that are happening in the future can only be reported over the phone. Please call "+exchangeName+ " at "+exchangePhone+" to report your event and to see if you qualify for a Special Enrollment Period.";
				}
				
				arg1.reject(errorMessage);
			}
		}
	}

	private String checkEventDateisValid(String eventDate) {
		Date eventdate = ReferralUtil.convertStringToDate(eventDate);
		String message = null;
		if (eventdate == null) {
			message = "Date is empty or invalid";
		}
		return message;
	}

	private int checkEventDateisFuture(DateTime todaysDate, String eventDate) {
		int i = 0;
		Date eventdate = ReferralUtil.convertStringToDate(eventDate);
		DateTime selEventDate = new DateTime(eventdate);

		if (selEventDate.isAfter(todaysDate)) {
			i = 1;
		}
		return i;

	}

}
