/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.Enrollment1095AInboundBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public class Enrollment1095AInboundProcessingTask implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095AInboundProcessingTask.class);
	private Enrollment1095AInboundBatchService enrollment1095AInboundBatchService;
	private JobService jobService;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		try{
			LOGGER.info("Processing Inbound Response Tasklet");
			StepContext stepContext = chunkContext.getStepContext();
		    StepExecution stepExecution = stepContext.getStepExecution();
		    
		    Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		    
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
		    
		    synchronized(enrollment1095AInboundBatchService){
		    	enrollment1095AInboundBatchService.processInboundResponse(stepExecution.getJobExecutionId());
		    }
			
		}catch(GIException e){
			throw new GIException("Error in Processing Inbound Response", e);
		}
		return RepeatStatus.FINISHED;
	}

	public Enrollment1095AInboundBatchService getEnrollment1095AInboundBatchService() {
		return enrollment1095AInboundBatchService;
	}

	public void setEnrollment1095AInboundBatchService(Enrollment1095AInboundBatchService enrollment1095aInboundBatchService) {
		enrollment1095AInboundBatchService = enrollment1095aInboundBatchService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
