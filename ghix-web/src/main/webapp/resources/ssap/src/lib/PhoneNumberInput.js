import React, {Fragment, useEffect, useState} from 'react';
import phoneNumberTools from '../utils/phoneNumberTools';
import validators from '../utils/validators';
import VALIDATION_TYPES from '../constants/validationTypes';

// Utils

// Constants

const PhoneNumberInput = props => {

    const {
        inputData: {
            id,
            isInputRequired,
            helperClasses,
            name,
            placeholder,
            type,
            value,
            validationType
        },
        inputActions: {
            handleFieldChange
        },
        label: {
            text,
            labelClasses
        },
    } = props;

    const [ currentValue, updateCurrentValue ] = useState(phoneNumberTools.getAdjustedPhoneNumber(value));

    useEffect(() => {
        const newValue = phoneNumberTools.getAdjustedPhoneNumber(value);
        updateCurrentValue(newValue ? newValue : '');
    }, [value]);

    const handlePhoneChange = e => {
        const { value: nextPhoneNumberValue } = e.target;

        if((nextPhoneNumberValue.length > currentValue.length) &&
            isNaN(+nextPhoneNumberValue.slice(-1)) ||
            nextPhoneNumberValue.length > 14) {
            return null;
        }

        const phoneNumberToSave = phoneNumberTools.autoFillPhone(nextPhoneNumberValue, currentValue);

        updateCurrentValue(phoneNumberToSave);
    };

    const handlePhoneBlur = () => {
        const isPhoneNumberValid =
            validators
                .find(validator => validator.validatorType === VALIDATION_TYPES[validationType])
                .verifier({ value: currentValue });

        updateCurrentValue(isPhoneNumberValid ? currentValue : '');
        handleFieldChange(isPhoneNumberValid ? phoneNumberTools.cleanedUpPhone(currentValue) : null);
    };


    return (
        <Fragment>
            <div className={`usa-grid gi-component gi-${type} ${helperClasses && helperClasses.join(' ')}`}>
                <label
                    htmlFor={id}
                    className={`usa-width-one-third gi-${type}-label ${labelClasses && labelClasses.join(' ')} required-${isInputRequired}`}
                    aria-label={isInputRequired}
                >
                    { text }
                </label>
                <input
                    id={id}
                    className={`usa-width-two-thirds gi-${type}-input`}
                    name={name}
                    type="text"
                    value={currentValue || ''}
                    aria-required={isInputRequired}
                    placeholder={placeholder}
                    onChange={handlePhoneChange}
                    onBlur={handlePhoneBlur}
                />
            </div>
        </Fragment>
    );
};

export default PhoneNumberInput;

