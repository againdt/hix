package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;

/**
 * @author chopra_s
 *
 */
public interface ReferralLCEService {

	int executeLCE(long referralActivationId);
	
	int processLce(LceActivityDTO lceActivityDTO, boolean openEnrollment);

}
