package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author chopra_s
 * 
 */
@Component("referralSsapCmrLinkService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralSsapCmrLinkServiceImpl implements ReferralSsapCmrLinkService {

	private static final String EITHER_NO_DATA_FOUND_OR_MULTIPLE_SSN_FOUND_IN_REFERENCE_APPLICATION = "Either no data found or multiple ssn found in reference application";

	private static final String UPDATED_MEMBER_ID_S_FROM_PREVIOUS_APPLICATION_ID = "Updated memberId(s) from previous application Id ";

	private static final String ACCESS_CODE = "ACCESS_CODE";

	private static final String SUCCESS = "SUCCESS";

	private static final Logger LOGGER = Logger.getLogger(ReferralSsapCmrLinkServiceImpl.class);

	private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	@Autowired
	private AptcHistoryService aptcHistoryService;

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	@Autowired
	@Qualifier("referralAccountActivationService")
	private ReferralAccountActivationService referralAccountActivationService;
	
	@Autowired private IUserRepository iUserRepository;
	@Autowired private QLEValidationService qleValidationService;
	
	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;
	
	@Autowired
	ReferralProcessingService referralProcessingService;



	private static final String REFERRAL = "REFERRAL";

	@Override
	@Transactional
	public Map<String, String> executeLinking(long referralActivationId, int userId, int cmrId) {
		Map<String, String> result = null;
		LOGGER.debug("executeLinking starts with referralActivation " + referralActivationId);

		final ReferralActivation referralActivation = referralActivationRepository.findOne(referralActivationId);

		if (referralActivation == null) {
			LOGGER.warn(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
			return result;
		}

		final boolean blnFlag = linkSsapCmr(referralActivation, cmrId);
		updateAptcHistoryCMR(referralActivation.getSsapApplicationId(), cmrId);
		if (blnFlag) {
			referralActivation.setUserId(userId);
			executeReferralActivationStatus(referralActivation);
			// Mark Account Activation Link as PROCESSED
			referralAccountActivationService.updateAccountActivation(referralActivation.getSsapApplicationId());
			result = populateValueforLogging(referralActivation.getSsapApplicationId(), result);

			/**
			 * Handle another UC app for same medicaid, if any
			 * */

			String unclaimedResult = handleAnotherLinking(referralActivation.getSsapApplicationId(), cmrId, userId);
			logData(referralActivation.getSsapApplicationId().longValue(), unclaimedResult, SUCCESS);
			
			/**
			 * Copy member Ids from current year or current year - 1 latest application and call this as reference application.
			 * 
			 * Now find member by ssn from reference application
			 * 1. If member found then update memberId 
			 * 2. If no member found then ignore update part
			 * 3. If multiple members found then stop Copy member Ids step for all members
			 */
			copyMemberIds(referralActivation.getSsapApplicationId(), cmrId);
		}
		
		
		
		LOGGER.debug("executeLinking ends with status " + result);

		return result;
	}

	private void copyMemberIds(Integer ssapApplicationId, int cmrId) {
		SsapApplication currentApplication = readApplication(ssapApplicationId.longValue());
		
		if (currentApplication != null){
			Map<String, SsapApplicant> currentSsnMemberIdMap = formSsnMemberIdMap(currentApplication);
			if (ReferralUtil.mapSize(currentSsnMemberIdMap) == ReferralConstants.NONE){
				return; // this should never happen; a safety-net
			}
			
			SsapApplication referenceApplication = readReferenceApplication(currentApplication);
			
			if (referenceApplication != null){
				referenceApplication = readApplication(referenceApplication.getId());
				
				Map<String, SsapApplicant> referenceSsnMemberIdMap = formSsnMemberIdMap(referenceApplication);
				
				if (ReferralUtil.mapSize(referenceSsnMemberIdMap) == ReferralConstants.NONE){
					logData(currentApplication.getId(), EITHER_NO_DATA_FOUND_OR_MULTIPLE_SSN_FOUND_IN_REFERENCE_APPLICATION + referenceApplication.getId(), SUCCESS);
					return;
				}
				
				boolean dataUpdated = processCopyMemberIds(currentApplication, currentSsnMemberIdMap, referenceSsnMemberIdMap);
				if (dataUpdated){
					logData(currentApplication.getId(), UPDATED_MEMBER_ID_S_FROM_PREVIOUS_APPLICATION_ID + referenceApplication.getId(), SUCCESS);
				}
			}
		}
	}

	private SsapApplication readReferenceApplication(SsapApplication currentApplication) {
		List<SsapApplication> referenceApps = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		
		if (ReferralUtil.listSize(referenceApps) == ReferralConstants.NONE){
			referenceApps = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear() - ReferralConstants.ONE);
		} else {
			 referenceApps = referenceApps.stream()	//ignore current application
					.filter(s -> currentApplication.getId() != s.getId())
					.collect(Collectors.toList());
		}
		
		if (ReferralUtil.listSize(referenceApps) == ReferralConstants.NONE){
			return null;
		}
		
		return referenceApps.stream()
				.sorted((a1, a2) -> a2.getCreationTimestamp().compareTo(a1.getCreationTimestamp())).findFirst().orElse(null);
	}

	private Map<String, SsapApplicant> formSsnMemberIdMap(SsapApplication currentApplication) {
		Map<String, SsapApplicant> ssnMemberIdMap = new HashMap<>();
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.getSsn() != null){
				if (ssnMemberIdMap.containsKey(applicant.getSsn())){
					return Collections.emptyMap(); // multiple ssn found in current application
				}
				ssnMemberIdMap.put(applicant.getSsn(), applicant);
			}
		}
		return ssnMemberIdMap;
	}

	private SsapApplication readApplication(Long ssapApplicationId) {
		List<SsapApplication> apps = ssapApplicationRepository.findByAppId(ssapApplicationId);
		
		if (ReferralUtil.listSize(apps) == ReferralConstants.NONE){
			return null;
		}
		
		return apps.get(0);
	}
	
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	private boolean processCopyMemberIds(SsapApplication currentApplication, Map<String, SsapApplicant> currentSsnMemberIdMap, Map<String, SsapApplicant> referenceSsnMemberIdMap) {
		
		final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		boolean jsonUpdated = false;
		for (String currSsn : currentSsnMemberIdMap.keySet()) {
			SsapApplicant currApplicant = currentSsnMemberIdMap.get(currSsn);
			if (referenceSsnMemberIdMap.containsKey(currSsn)){
				SsapApplicant referenceApplicant = referenceSsnMemberIdMap.get(currSsn);
				if (!StringUtils.equals(referenceApplicant.getApplicantGuid(), currApplicant.getApplicantGuid())){
					updateApplicantGuid(ssappln, currApplicant.getPersonId(), referenceApplicant.getApplicantGuid());
					modifyCurrentApplicants(currentApplication.getSsapApplicants(), currApplicant.getPersonId(), referenceApplicant.getApplicantGuid());
					jsonUpdated = true;
				}
			}
		}
		
		if (jsonUpdated){
			final String ssapJson = ssapJsonBuilder.transformToJson(ssappln);
			updateSSAPJSON(currentApplication, ssapJson);
			updateSsapApplicants(currentApplication.getSsapApplicants());
		}
		
		return jsonUpdated;
	}

	private void updateSsapApplicants(List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			persistSSAPApplicantGuid(ssapApplicant);
		}
		
	}

	private void updateSSAPJSON(SsapApplication currentApplication, String ssapJson) {
		currentApplication.setApplicationData(ssapJson);
		ssapApplicationRepository.save(currentApplication);
    }
	
	private void modifyCurrentApplicants(List<SsapApplicant> applicants, long personId, String newApplicantGuid) {
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == personId) {
				ssapApplicant.setApplicantGuid(newApplicantGuid);
				break;
			}
		}
	}

	private void persistSSAPApplicantGuid(SsapApplicant applicant) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicant.getId());
		ssapapplicant.setApplicantGuid(applicant.getApplicantGuid());
		ssapApplicantRepository.save(ssapapplicant);
	}

	private void updateApplicantGuid(SingleStreamlinedApplication ssappln, long personId, String applicantGuid) {
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == personId) {
				houseHoldMember.setApplicantGuid(applicantGuid);
				break;
			}
		}
	}

	private String handleAnotherLinking(Integer ssapApplicationId, int cmrId, int userId) {
		
		StringBuilder unclaimedResult = new StringBuilder();
		// 1. Fetch UC app for incoming appId's medicaid Id
		List<SsapApplication> apps = ssapApplicationRepository.findByApplicationIdForPrimary(ssapApplicationId.longValue());

		if(ReferralUtil.listSize(apps) == ReferralConstants.NONE || ReferralUtil.listSize(apps.get(0).getSsapApplicants()) == ReferralConstants.NONE ){
			unclaimedResult.append("No app found for - ").append(ssapApplicationId.longValue());
			return unclaimedResult.toString();
		}

		SsapApplicant primaryApplicant = apps.get(0).getSsapApplicants().get(0);
		if (StringUtils.isEmpty(primaryApplicant.getExternalApplicantId())){
			unclaimedResult.append("No medicaid id found for - ").append(ssapApplicationId.longValue());
			return unclaimedResult.toString();
		}
		
		List<SsapApplication> unclaimedApps = ssapApplicationRepository.findbyMedicaidIdForPrimary(primaryApplicant.getExternalApplicantId());
		int unclaimedAppSize = ReferralUtil.listSize(unclaimedApps);

		if(unclaimedAppSize==ReferralConstants.NONE) {
        	final Household household = cmrHouseholdRepository.findOne(cmrId);
        	if(household!=null) {
        		unclaimedApps = ssapApplicationRepository.findMatchingApplication(WordUtils.uncapitalize(household.getFirstName()),WordUtils.uncapitalize(household.getLastName()),household.getBirthDate(),household.getSsn());
        		unclaimedAppSize = ReferralUtil.listSize(unclaimedApps);
        	}
        }
		
		// 2. if count != 1 then return silently
		if (unclaimedAppSize != ReferralConstants.ONE) {
			unclaimedResult.append("Cannot link unclaimed app for medicaid id - ").append(primaryApplicant.getExternalApplicantId()).
			append(" - Total no of unclaimed app found -  ").append(unclaimedAppSize);
			return unclaimedResult.toString();
		}

		SsapApplication unclaimedApp = unclaimedApps.get(0);
		unclaimedResult.append("unclaimed app found for medicaid id - ").append(primaryApplicant.getExternalApplicantId());

		// 3. if count == 1 then process
		// 3.1 do checks for appId and for the coverage year
		long coverageYear = unclaimedApp.getCoverageYear();
		long activeAppCount = ssapApplicationRepository.countOfApplicationByCmrAndApplicationStatus(new BigDecimal(cmrId), coverageYear, STATUS_LIST);
		
		unclaimedResult.append(" ").append(activeAppCount).
		append(" - activeAppCount for coverage year - ").append(coverageYear).
		append(" - hh id ").append(cmrId);
		// 3.2 if failure then return
		if (activeAppCount != 0L){
			unclaimedResult.append(" More than zero app found. Cannot proceed ");
			return unclaimedResult.toString();
		}
		// 3.3 if success 
		// 3.3.1 link app to cmr
		BigDecimal cmrHouseholdId = new BigDecimal(cmrId);
		ssapApplicationRepository.updateCmrHouseholdIdAndApplicationStatusById(cmrHouseholdId, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(), new Timestamp(new TSDate().getTime()), unclaimedApp.getId());

		final ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication(Long.valueOf(unclaimedApp.getId()).intValue());
		referralActivation.setUserId(userId);
		executeReferralActivationStatus(referralActivation);
		// Mark Account Activation Link as PROCESSED
		referralAccountActivationService.updateAccountActivation(referralActivation.getSsapApplicationId());
		
		// 3.3.2 if appYear == currentYear override hh
		unclaimedResult.append(" Linking completed for appId ").append(unclaimedApp.getId()).append(" to household - ").append(cmrId);
		String currentCoverageYearConfiguration = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		if (StringUtils.equals(currentCoverageYearConfiguration, String.valueOf(coverageYear))){
			updateCmrHousehold(referralActivation, userId, cmrId);
			unclaimedResult.append(" household updated");
		}
		
		return unclaimedResult.toString();
		// 4. log result to ssap_integration_log

	}
	
	@Autowired private IntegrationLogService integrationLogService;
	
	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, ACCESS_CODE, status, ssapApplicationId, null);
	}
	
	private void updateCmrHousehold(ReferralActivation referralActivation, int userId, int cmrId) {
		
		final Household household = cmrHouseholdRepository.findOne(cmrId);
		
		if (userId == ReferralConstants.NONE) {
			userId = userService.getSuperUserId(ReferralConstants.EXADMIN_USERNAME);
		}

		final SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(referralActivation.getSsapApplicationId(), ReferralConstants.PRIMARY);
		final Location location = populateLocation(ssapApplicant);
		household.setFirstName(WordUtils.capitalizeFully(ssapApplicant.getFirstName()));
		household.setMiddleName(WordUtils.capitalizeFully(ssapApplicant.getMiddleName()));
		household.setLastName(WordUtils.capitalizeFully(ssapApplicant.getLastName()));
		household.setBirthDate(ssapApplicant.getBirthDate());
		household.setLocation(location);
		household.setPrefContactLocation(populateMailingLocation(ssapApplicant));
		household.setZipCode(location.getZip());
		household.setPhoneNumber(ssapApplicant.getPhoneNumber());
		household.setUpdated(new Timestamp(new TSDate().getTime()));
		household.setUpdatedBy(userId);
		
		/* update RIDP flags if required */
		if (!ReferralConstants.Y.equals(ReferralUtil.checkAndValidString(household.getRidpVerified()))) {
			household.setRidpVerified(ReferralConstants.Y);
			household.setRidpDate(new TSDate());
			household.setRidpSource(REFERRAL);
		}
		
		cmrHouseholdRepository.save(household);
	}
	
	private Location populateMailingLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else {
			return null;
		}
	}

	

	@SuppressWarnings("serial")
	private final List<String> STATUS_LIST = new ArrayList<String>() {
		{
			add(ApplicationStatus.CANCELLED.getApplicationStatusCode());
			add(ApplicationStatus.CLOSED.getApplicationStatusCode());
		}
	};

	private Map<String, String> populateValueforLogging(Integer ssapApplicationId, Map<String, String> result) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByApplicationIdForPrimary(Long.valueOf(ssapApplicationId));
		if (ReferralUtil.listSize(ssapApplications) > 0) {
			SsapApplication ssapApplication = ssapApplications.get(0);
			if (ReferralUtil.listSize(ssapApplication.getSsapApplicants()) > 0) {
				result = new HashMap<String, String>();
				result.put(ReferralConstants.APPLICATION_ID, ssapApplication.getCaseNumber());
				result.put(ReferralConstants.MEDICAID_ID, ssapApplication.getSsapApplicants().get(0).getExternalApplicantId());
				result.put(ReferralConstants.FLOW, String.valueOf(ReferralConstants.SUCCESS));

			}
		}
		return result;

	}

	private boolean linkSsapCmr(ReferralActivation referralActivation, int cmrId) {
		final SsapApplication ssapApplication = ssapApplicationRepository.findOne(Long.valueOf(referralActivation.getSsapApplicationId()));
		if (ssapApplication == null) {
			LOGGER.warn(ReferralConstants.NO_SSAP_FOUND + referralActivation.getSsapApplicationId());
			return false;
		}
		
		if(ssapApplication.getCmrHouseoldId() == null) {
			Date currentDate = new TSDate();
			
			if (cmrId == 0) {
				LOGGER.warn(ReferralConstants.NO_HOUSEHOLD_FOUND + referralActivation.getSsapApplicationId());
				return false;
			}
			BigDecimal cmrHouseholdId = new BigDecimal(cmrId);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			
			if(!"NV".equalsIgnoreCase(stateCode)) {
				ssapApplicationRepository.updateCmrHouseholdIdAndApplicationStatusById(cmrHouseholdId, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(), new Timestamp(currentDate.getTime()), ssapApplication.getId());
			}
			else {
				ssapApplicationRepository.updateCmrHouseholdIdById(cmrHouseholdId, new Timestamp(currentDate.getTime()), ssapApplication.getId());
			}
		}
		return true;
	}

	private boolean updateAptcHistoryCMR(Integer ssapApplicationId, int cmrId) {
		BigDecimal cmrHouseholdId = new BigDecimal(cmrId);
		return aptcHistoryService.updateCMRId(cmrHouseholdId, ssapApplicationId);
	}

	private void executeReferralActivationStatus(ReferralActivation referralActivation) {
		referralActivation.setWorkflowStatus(ReferralActivationEnum.ACTIVATION_COMPELETE);
		referralActivation.setLastUpdatedOn(new TSDate());
		referralActivationRepository.save(referralActivation);
	}

	@Override
	@ReferralTransactionAnno
	public boolean executeLinking(long enrolledApplicationId, long currentApplicationId, String authRep,String hhCaseId) {
		boolean blnReturn = false;
		try {
			LOGGER.info("CMR Linking for Current Referral application " + currentApplicationId);

			final ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication((int) currentApplicationId);

			if (referralActivation == null) {
				LOGGER.error(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
				return blnReturn;
			}

			final SsapApplication enrolledApplication = ssapApplicationRepository.findOne(enrolledApplicationId);
			final SsapApplication currentApplication = ssapApplicationRepository.findOne(currentApplicationId);

			executeReferralActivation(referralActivation, enrolledApplication.getCmrHouseoldId().intValue());

			linkSsapCmr(currentApplication, enrolledApplication.getCmrHouseoldId().intValue());
			
			AccountUser linkedUser = null;
			
			if(StringUtils.isNotBlank(authRep)) {
				linkedUser = iUserRepository.findByExtnAppUserId(authRep);
			}

			modifyCmr(currentApplication, enrolledApplication.getCmrHouseoldId().intValue(), linkedUser,hhCaseId);

			blnReturn = true;

			LOGGER.info("CMR Linking for Current Referral application Ends");
		} catch (Exception e) {
			throw e;
		}
		return blnReturn;
	}

	private void modifyCmr(SsapApplication currentApplication, int cmrHouseholdId, AccountUser linkedUser, String householdCaseId) {
		final SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(currentApplication.getId(), ReferralConstants.PRIMARY);
		updateCmrHousehold(ssapApplicant, cmrHouseholdId, linkedUser,householdCaseId);
	}

	private void updateCmrHousehold(SsapApplicant ssapApplicant, int cmrHouseholdId, AccountUser linkedUser, String householdCaseId) {
		int updatedUserId = userService.getSuperUserId(ReferralConstants.EXADMIN_USERNAME);
		final Household household = cmrHouseholdRepository.findOne(cmrHouseholdId);
		final Location location = populateLocation(ssapApplicant);
		household.setRidpVerified(ReferralConstants.Y);
		household.setRidpDate(new TSDate());
		household.setRidpSource(REFERRAL);
		household.setZipCode(location.getZip());
		household.setNameSuffix(ssapApplicant.getNameSuffix());
		household.setFirstName(WordUtils.capitalizeFully(ssapApplicant.getFirstName()));
		household.setMiddleName(WordUtils.capitalizeFully(ssapApplicant.getMiddleName()));
		household.setLastName(WordUtils.capitalizeFully(ssapApplicant.getLastName()));
		household.setBirthDate(ssapApplicant.getBirthDate());
		household.setLocation(location);
		household.setPrefContactLocation(populateMailingLocation(ssapApplicant));
		household.setUpdated(new Timestamp(new TSDate().getTime()));
		household.setUpdatedBy(updatedUserId);
		household.setFfmHouseholdID(ssapApplicant.getExternalApplicantId());
		household.setPhoneNumber(ssapApplicant.getPhoneNumber());
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if("CA".equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)) {
			household.setSsn(ssapApplicant.getSsn());
		}
		

		if(GhixPlatformConstants.USER_NAME_POLICY.equalsIgnoreCase("ALLOW_DIFFERENT_EMAIL".intern())) {
			household.setEmail((ssapApplicant.getEmailAddress()));
		}
		
		if (household.getPrefContactMethod() == null) {
			Integer prefCommMethodLookup = readDefaultPrefCommMethod(ssapApplicant);
			household.setPrefContactMethod(prefCommMethodLookup);
		}

		// Link User with HouseHold by Organization Identification ID.
		if (hasLinkUserEnable()) {

			if (null != linkedUser && (null == household.getUser() || household.getUser().getId() != linkedUser.getId())) {

				household.setUser(linkedUser);

				// Create/ Update Module User Entry
				try {
					userService.createOrUpdateModuleUser(household.getId(), "individual", linkedUser, true);
				}
				catch (GIException e) {
					LOGGER.error("Invalid API usage : Creating Module (individual) for the User ::" + household.getUser().getId() + "::...");
				}
			}
			else if (null == linkedUser) {
				LOGGER.warn("Linked user is not found.");
			}
					
			// set the External_HH_Case_ID with ICN
			// check for ICN passed in request if not null then set it 
			if (null != householdCaseId) {
				// check the houshold case id is not null then
				if (null != household.getHouseholdCaseId()) {
					// check the household in the database if same as passed then no update 
					// this will be needed in case of SEP flow
					if (!householdCaseId.equalsIgnoreCase(household.getHouseholdCaseId())) {
						// then update
						household.setHouseholdCaseId(householdCaseId);
					}
				} else {
					// set the ICN to External_HH_Case_ID
					household.setHouseholdCaseId(householdCaseId);
				}
			}
		}
		cmrHouseholdRepository.save(household);
	}

	@Autowired 
	private LookupService lookUpService;

	private static final String COMMUNICATION_MODE = "PreferredContactMode";
	
	private Integer readDefaultPrefCommMethod(final SsapApplicant ssapApplicant) {
		LookupValue prefCommMethodLookup = null;
		GhixNoticeCommunicationMethod prefCommunication = null;
		if (StringUtils.isNotBlank(ssapApplicant.getEmailAddress())){
			prefCommunication = GhixNoticeCommunicationMethod.Email;
		} else {
			prefCommunication = GhixNoticeCommunicationMethod.Mail;
		}
		prefCommMethodLookup = lookUpService.getlookupValueByTypeANDLookupValueCode(COMMUNICATION_MODE, prefCommunication.getMethod());
		
		return prefCommMethodLookup != null ? prefCommMethodLookup.getLookupValueId() : null;
	}

	private Location populateLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else {
			return null;
		}
	}

	private boolean linkSsapCmr(SsapApplication currentApplication, final int cmrHouseholdId) {
		ssapApplicationRepository.updateCmrHouseholdIdAndApplicationStatusById(new BigDecimal(cmrHouseholdId), ApplicationStatus.SUBMITTED.getApplicationStatusCode(), new Timestamp((new TSDate()).getTime()), currentApplication.getId());
		return true;
	}

	private void executeReferralActivation(ReferralActivation referralActivation, final int cmrHouseholdId) {
		/*
		 * referralActivation.setWorkflowStatus(ReferralActivationEnum.ACTIVATION_COMPELETE); referralActivation.setLastUpdatedOn(new TSDate()); referralActivation.setCmrHouseholdId(cmrHouseholdId); referralActivationRepository.save(referralActivation);
		 */
		referralActivationRepository.updateWorkFlowStatusAndHouseholdId(cmrHouseholdId, ReferralActivationEnum.ACTIVATION_COMPELETE, new Timestamp(new TSDate().getTime()), referralActivation.getId());
	}

	@Override
	//@ReferralTransactionAnno
	public Map<String, Object> executeOELinking(long applicationId , String hhCaseId) {
		final Map<String, Object> oReturn = executeCmrLinking(applicationId, hhCaseId, null);
		createApplicationEventAndRunValidationEngine(applicationId);
		return oReturn;
	}

	@Override
	public Map<String, Object> executeOELinking(long applicationId, String hhCaseId, String organizationIdentificationID) {
		final Map<String, Object> oReturn = executeCmrLinking(applicationId, hhCaseId, organizationIdentificationID);
		createApplicationEventAndRunValidationEngine(applicationId);
		return oReturn;
	}

	@Override
	//@ReferralTransactionAnno
	public Map<String, Object> executeQELinking(long applicationId, String householdCaseId) {
		return executeCmrLinking(applicationId, householdCaseId, null);
	}

	@Override
	//@ReferralTransactionAnno
	public Map<String, Object> executeQELinking(long applicationId, String householdCaseId, String organizationIdentificationID) {
		return executeCmrLinking(applicationId, householdCaseId, organizationIdentificationID);
	}

	/**
	 * Method is used to check link user is enable or not from config prop - iex.AT.linkuser 
	 */
	@Override
	public boolean hasLinkUserEnable() {

		boolean hasLinkUser = false;
		final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);

		if (StringUtils.isNotBlank(hasLinkUserStr) && "true".equalsIgnoreCase(hasLinkUserStr)) {
			hasLinkUser = true;
		}
		return hasLinkUser;
	}

	private Map<String, Object> executeCmrLinking(long applicationId, String householdCaseId, String organizationIdentificationID) {
		Map<String, Object> oReturn = new HashMap<String, Object>() {
			private static final long serialVersionUID = 9099257287094683801L;

			{
				put(ReferralProcessingConstants.AUTO_LINK, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID, ReferralConstants.NONE);
				put(ReferralProcessingConstants.MULTIPLE_CMR, false);
			}
		};
		try {
			final SsapApplication application = ssapApplicationRepository.findAndLoadApplicantsByAppId(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}

			final BigDecimal household = autoLinkHousehold(ReferralUtil.retreiveApplicant(application, ReferralConstants.PRIMARY), oReturn,  application,householdCaseId);

			if (household != null) {
				LOGGER.info("Household - " + household + " found for linking application - " + applicationId);
				final int cmrHouseholdId = household.intValue();
				final ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication((int) applicationId);

				if (referralActivation == null) {
					LOGGER.error(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
					throw new GIRuntimeException(ReferralConstants.NO_REFERRALACTIVATION_FOUND + applicationId);
				}

				executeReferralActivation(referralActivation, cmrHouseholdId);

				linkSsapCmr(application, cmrHouseholdId);
				AccountUser linkedUser = null;
						
				if( organizationIdentificationID != null){
					linkedUser = iUserRepository.findByExtnAppUserId(organizationIdentificationID);
				}

				modifyCmr(application, cmrHouseholdId, linkedUser,householdCaseId);
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				//--- In case of Nevada the CMR-Application linking will be partial, 
				//--- it should just link the cmr-with app, but to user is created & linked to CMR Also the stattus should be UC
				//-- Hence the below if is added.
				if(!"NV".equalsIgnoreCase(stateCode)) {
					oReturn.put(ReferralProcessingConstants.AUTO_LINK, true);
				}
				else
				{
					oReturn.put(ReferralProcessingConstants.AUTO_LINK, false);
				}
				// this is populated as its needed for External Assister API call
				oReturn.put(ReferralConstants.HOUSEHOLD_ID, cmrHouseholdId);

			} else {
				LOGGER.info("Household not found for linking application - " + applicationId);
			}

		} catch (Exception e) {
			throw e;
		}
		return oReturn;
	}

	private BigDecimal autoLinkHousehold(SsapApplicant primary, Map<String, Object> mp,SsapApplication application, String householdCaseId) {
		LOGGER.info("Auto Link Check for primaryId -  " + primary.getExternalApplicantId());
		BigDecimal cmrReturn = null;
		
		// Find CMR based on HHcaseID .. IF not found .. create here it self and return ... then ...It can be linked here itself...
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		Set<BigDecimal> cmrHouseHolds  ;
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			
			int hhId = referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId(),householdCaseId);
			
			cmrHouseHolds = new HashSet<BigDecimal>();
			cmrHouseHolds.add(BigDecimal.valueOf(hhId) );
		}else {
			cmrHouseHolds = ssapApplicationRepository.allCmrHouseholdForMedicaidId(primary.getExternalApplicantId());	
		}
		
		
		mp.put(ReferralProcessingConstants.MULTIPLE_CMR, ReferralUtil.collectionSize(cmrHouseHolds) > ReferralConstants.ONE);
		if (cmrHouseHolds != null && ReferralUtil.collectionSize(cmrHouseHolds) == ReferralConstants.ONE) {
			cmrReturn = cmrHouseHolds.iterator().next();
			if (ssapApplicationRepository.applicationCountForCmrHousehold(cmrReturn) > ReferralConstants.NONE) {
				mp.put(ReferralProcessingConstants.NON_FINANCIAL_CMR, true);
				mp.put(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID, cmrReturn.intValue());
			}
		}
		
		if(   (cmrHouseHolds == null || cmrHouseHolds.isEmpty() ) && ( StringUtils.isBlank(useExternalId) || "FALSE".equalsIgnoreCase(useExternalId) )  ) {
			cmrReturn = findMatchingApplicantCmrhousehold(primary, mp,application.getId());
		}
		
		LOGGER.info("Auto Link Check for primaryId -  " + primary.getExternalApplicantId() + " ,Output - " + cmrHouseHolds + " , return value - " + cmrReturn);
		// In case of NF application Linking it was before going into conversion flow, i.e. F on top of NF (SEP), so cmrReturn was 
		// set null for NF and old NF app id was sent on map 
		// but as per new change in case of F on NF we will treat as normal LCE so it goes to referral lce flow, so conversion is not reqd
		// that is why sending cmrReturn for F on NF appln too
		return cmrReturn;
	}

	private BigDecimal findMatchingApplicantCmrhousehold(SsapApplicant primary, Map<String, Object> mp,long aaplicationId) {
		BigDecimal cmrReturn = null;
		if(StringUtils.isNotBlank(primary.getSsn()) && primary.getBirthDate()!=null) { 
			List<SsapApplicant> applicants =  ssapApplicantRepository.findBySsnAndDob(primary.getSsn(), primary.getBirthDate());
			
			Set<BigDecimal> cmrHouseHolds = new HashSet<BigDecimal>();
			
			if(applicants != null) {
				for(SsapApplicant applicant : applicants) {
					if(primary.getLastName().equalsIgnoreCase(applicant.getLastName()) ||   "FEMALE".equalsIgnoreCase(primary.getGender().trim())  ) {
						cmrHouseHolds.add(applicant.getSsapApplication().getCmrHouseoldId());
					}	
				}
			}
			mp.put(ReferralProcessingConstants.MULTIPLE_CMR, ReferralUtil.collectionSize(cmrHouseHolds) > ReferralConstants.ONE);
			if (cmrHouseHolds != null && ReferralUtil.collectionSize(cmrHouseHolds) == ReferralConstants.ONE) {
				cmrReturn = cmrHouseHolds.iterator().next();
				if (ssapApplicationRepository.applicationCountForCmrHousehold(cmrReturn) > ReferralConstants.NONE) {
								mp.put(ReferralProcessingConstants.NON_FINANCIAL_CMR, true);
					mp.put(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID, cmrReturn.intValue());
				}
			}

			if (cmrReturn == null && hasLinkUserEnable()) {
				int hhid = referralProcessingService.checkHouseholdExistsAndThenCreate(aaplicationId,null);
				cmrReturn = new BigDecimal(hhid);
			}
			//HIX-108131
			/*if(cmrHouseHolds == null || cmrHouseHolds.isEmpty()) {
				cmrReturn = findMatchingCmrhousehold(primary, mp);
			}*/
		}
		if (cmrReturn == null && hasLinkUserEnable()) {
			int hhid = referralProcessingService.checkHouseholdExistsAndThenCreate(aaplicationId,null);
			cmrReturn = new BigDecimal(hhid);
		}
		return cmrReturn;
	}
					
	/*private BigDecimal findMatchingCmrhousehold(SsapApplicant primary, Map<String, Object> mp) {
		List<Integer> households = cmrHouseholdRepository.findMatchingCmrBySSNDOB(primary.getBirthDate(), primary.getSsn());
		BigDecimal cmrReturn = null;
		
		if (households != null && ReferralUtil.collectionSize(households) == ReferralConstants.ONE) {
			cmrReturn = new BigDecimal(households.iterator().next());
		}	
		return cmrReturn;
	}*/
	
	private boolean isEnrollmentActive(SsapApplication enApp) {
		return activeEnrollmentService.isEnrollmentActive(enApp.getId(), ReferralConstants.EXADMIN_USERNAME);
	}
	
	@Override
	public BigDecimal findMatchingCmrHousehold(SsapApplicant primary,BigDecimal cmrhousehold) {
		List<SsapApplicant> applicants =  ssapApplicantRepository.findBySsnAndDobAndCmrHousehold(primary.getSsn(), primary.getBirthDate(),cmrhousehold);
		boolean nonFinancialCmr = false;
		if (applicants != null) {
			for (SsapApplicant applicant : applicants) {
				if (primary.getLastName().equalsIgnoreCase(applicant.getLastName())
						|| "FEMALE".equalsIgnoreCase(primary.getGender() ) ) {
					return applicant.getSsapApplication().getCmrHouseoldId();
				}
			}
		}
		return null;
	}

	private static Timestamp ENROLLMENT_START_DATE = null;
	private static Timestamp ENROLLMENT_END_DATE = null;

	@PostConstruct
	public void doPost() {
		final String CURRENT_COVERAGE_YEAR = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		final String ENROLLMENT_START_DATE_STR = "01/01/" + CURRENT_COVERAGE_YEAR + " 00:00:00";
		final String ENROLLMENT_END_DATE_STR = "12/31/" + CURRENT_COVERAGE_YEAR + " 23:59:59";

		try {
			ENROLLMENT_START_DATE = new Timestamp((new SimpleDateFormat(DATE_FORMAT).parse((ENROLLMENT_START_DATE_STR)).getTime()));
			ENROLLMENT_END_DATE = new Timestamp((new SimpleDateFormat(DATE_FORMAT).parse((ENROLLMENT_END_DATE_STR)).getTime()));
		} catch (ParseException e) {
			throw new GIRuntimeException(ReferralProcessingService.PARSE_EXCEPTION);
		}

	}

	private void createApplicationEventAndRunValidationEngine(long applicationId) {
		final SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);
		/* Date date = new TSDate(); */

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		/* ssapApplicationEvent.setCreatedTimeStamp(new Timestamp(date.getTime())); */
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		if (ENROLLMENT_START_DATE == null || ENROLLMENT_END_DATE == null) {
			doPost();
		}

		ssapApplicationEvent.setEnrollmentStartDate(ENROLLMENT_START_DATE);
		ssapApplicationEvent.setEnrollmentEndDate(ENROLLMENT_END_DATE);
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.OE);

		ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		updateApplicationValidationStatus(ssapApplication.getCaseNumber());
	}
	
	private void updateApplicationValidationStatus(String caseNumber) {
		LOGGER.info("Get validation status of case number " + caseNumber);
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.runValidationEngine(caseNumber,  Source.EXTERNAL, updateUser.getId());
	}

	@Override
	// @ReferralTransactionAnno
	public Map<String, Object> executeRenewalLinking(long applicationId, boolean linkApplication, String hhCaseId,
			String organizationalId) {
		if (linkApplication) {
			Map<String, Object> oReturn = executeCmrLinking(applicationId, hhCaseId, organizationalId);
			createApplicationEventAndRunValidationEngine(applicationId);
			return oReturn;
		}

		Map<String, Object> nReturn = new HashMap<String, Object>() {
			private static final long serialVersionUID = 9099257287094683801L;

			{
				put(ReferralProcessingConstants.AUTO_LINK, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID, ReferralConstants.NONE);
				put(ReferralProcessingConstants.MULTIPLE_CMR, false);
			}
		};
		return nReturn;
	}
	@Override
		public Map<String, Object> executeRenewalLinking(long applicationId, boolean linkApplication, String hhCaseId) {
			if (linkApplication) {
				Map<String, Object> oReturn = executeCmrLinking(applicationId, hhCaseId, null);
				createApplicationEventAndRunValidationEngine(applicationId);
				return oReturn;
			}

		Map<String, Object> nReturn = new HashMap<String, Object>() {
			private static final long serialVersionUID = 9099257287094683801L;

			{
				put(ReferralProcessingConstants.AUTO_LINK, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR, false);
				put(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID, ReferralConstants.NONE);
				put(ReferralProcessingConstants.MULTIPLE_CMR, false);
			}
		};
		return nReturn;
	}

	@Override
	public boolean executeLinkingWithNFCmr(long applicationId, int cmrId) {

		boolean blnReturn = false;
		try {
			LOGGER.info("CMR Linking for Current Referral application " + applicationId);

			final ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication((int) applicationId);

			if (referralActivation == null) {
				LOGGER.error(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
				return blnReturn;
			}

			final SsapApplication currentApplication = ssapApplicationRepository.findOne(applicationId);

			executeReferralActivation(referralActivation, cmrId);

			linkSsapCmr(currentApplication, cmrId);

			modifyCmr(currentApplication, cmrId, null,null);

			blnReturn = true;

			LOGGER.info("CMR Linking for Current Referral application Ends");
		} catch (Exception e) {
			throw e;
		}
		return blnReturn;
	}
}
