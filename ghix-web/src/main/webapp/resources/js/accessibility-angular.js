(function() {
'use strict'; 

    angular.module('accessibilityApp',[])
    .directive('ariaTooltip', ariaTooltip);
    
    
    
    function ariaTooltip(){
    	var directive = {
			restrict: 'A',
			link: function(scope, element, attrs) {
				
				element.tooltip({
					trigger: 'manual'
				}).focus(function(){
					element.tooltip('show');
				}).blur(function(){
					element.tooltip('hide');
				}).hover(function(){
					element.tooltip('show');
				}, function(){
					element.tooltip('hide');
				});
				
				
				var tooltipContent = element.attr('data-original-title');
				element.append('<span class="aria-hidden">Tooltip text: ' + tooltipContent + '</span>');
			}
    	}
    	
    	return directive;
    }
    
})();