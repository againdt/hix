(function (views) {
	views.dentalModelPlans = Backbone.View.extend({
		events : {
			'click a.addToCart': 'addToCart',
			'click a.checkOut' : 'checkOut'
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addToCart','checkOut');
			var self = this;


			self.collection.fetch({
				success : function () {

					if(self.collection.length==0){
						alert("No Dental Plans available.Redirecting to health plan selected.");
						var imgpath = 'showcart';
						window.top.location.href = imgpath;
						
					}

					//self.loadAll();
					self.collection.pager();
					self.loadCartSummary();
					self.getTotalDentalCount();

				},
				silent:true
			});

			
			self.collection.cartSummary.fetched = false;
			self.cartCollection = options.cartCollection;
			var currentGroupId = $('#currentGroupId').val();

			self.cartCollection.fetch({
				success : function () {
					self.cartCollection.setCartSummary(false,currentGroupId);
					self.collection.cartSummary.fetched = true;
				}
			});


		self.cartModel = options.cartModel;
		
			
			self.collection.bind('reset', self.addAll);
		
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			self.collection.each (self.addOne);
		},

		
		addOne : function (model) {
			var self = this;
			
			var viewSummary = new Summary({model:model});
		
			$('#mainSummary').append(viewSummary.render().el);
			

		},
		
		loadCartSummary : function () {
			
			var self = this;
			var fetchedVal = self.collection.cartSummary.fetched;
			
			if(fetchedVal == false){
				$('#planCount').html("(Loading...)");
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				
				self.collection.loadCartSummary(self.cartCollection.cartSummary,self.cartCollection.cartItems,self.cartCollection.models);
			}
		},

		addToCart: function (e) {
			
			var self = this;
			var planModel = "";

			if(e.target.id.indexOf('cart_') >= 0){
				itemId = e.target.id.replace("cart_","");
			}else{
				itemId = e.target.id.replace("plan_","");
			}
		
			
			var fav = "cart";
			
			if($("#"+e.target.id).attr("class").indexOf('addedItem') >= 0){
				
				 $('#showCart').text("No, Go to Cart");
				 $('#browseDental').show();
				 
				if($("#cart_"+itemId).length){					
							$("#cart_"+itemId).attr("class","addToCart btn btn-primary btn-small");
							$("#cart_"+itemId).html('&nbsp;'+'<i class="icon-white icon-shopping-cart"></i>'+' Add');
					
				}

				//self.collection.emptyCart(self.cartCollection.models);
				

				planModel = self.collection.searchPlan(this.cartCollection.models, itemId);
				orgId = planModel.id;

				planModel.set({id: planModel.get("orderItemId")});
				planModel.destroy({
					success : function () {
						planModel.set({id: orgId});
						//self.cartCollection.remove(planModel);
						//self.collection.updateCart(self.cartCollection.models);
					}
				});


				self.collection.removeFromCart(itemId, planModel.get("level").toLowerCase());

				$('#cartmessageplanremove').fadeIn(250,function() {
					setTimeout(function(){
						$('#cartmessageplanremove').hide();
					},1500);
				});
				
								
			}
			else{
				var planAddedForGroup = self.collection.searchCart(self.cartCollection.models,this.collection.cartItems,itemId);
				
				if(planAddedForGroup == false){
				
				var dentalPlanAdded = self.collection.dentalPlanAdded(self.cartCollection.models);
				if(dentalPlanAdded == true){
					alert("Dental Plan is already added. If you wish to add this dental plan kindly remove the previously added one.");
					return false;
				}	
						
				$('#showCart').text("Go to Cart");
				$('#browseDental').hide();
				
					planModel = self.collection.searchPlan(this.collection.models, itemId);
					
					
					if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
						$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");

						
						this.cartModel.save({id: planModel.get("planId"), premium: planModel.get("premiumAfterCredit"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), planDetailsByMember: planModel.get("planDetailsByMember"), aptc: planModel.get("aptc")},{
							success : function (model,resp) {
								var newModel = new Backbone.Model({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"), 
									level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
									issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
									premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
									groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
									planId: planModel.get("planId"), ehb: planModel.get("ehb"), orderItemId:model.id});
								
								self.cartCollection.add(newModel);							
						//self.collection.emptyCart(self.cartCollection.models);
							self.collection.updateCart(self.cartCollection.models);
								$('.removeFromCart').bind('click', self.addToCart);
								
							}
								
						});
						
						 this.collection.setCartSummary(itemId, planModel.get("level").toLowerCase());
						
					
					}
					
//					if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
//									var newModel = new Backbone.Model({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"), 
//										level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
//										issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
//										premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
//										groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
//										planId: planModel.get("planId"), ehb: planModel.get("ehb")});
//									
//									self.cartCollection.add(newModel);
//								
//									$('.removeFromCart').bind('click', self.addToCart);
//							
//
//						}
					else{
							alert("Selected plan is already present in the cart");
						}
						
				}else{
					alert("Plan for current group is already present in the cart");
				}
				


				}
				
				this.collection.updateCartSummary();
			},
			
			getTotalDentalCount: function () {
				$.ajax({
	 	 	 	 		                         type : "GET",
	 	 	 	 		                         url : "getTotalDentalCount",
	 	 	 	 		                        success : function(response) {
	 	 	 	 		                                $('#dentalCount').empty();
	 	 	 	 		                                $('#dentalCount').append('<p> <strong> Showing 3 out of '+response+' plans </strong> </p>');
	 	 	 	 		                           },
	 	 	 	 		                  });
			},


		checkOut: function () {
			var self = this;
			if(self.cartCollection.models.length <= 0){
				alert("Cart is empty");
			}
			else{
				window.location = "showcart";
			}
			
		}
/*
		addToCompare: function (e) {
			if(e.target.id.indexOf('ompare_')){
				$('#favmessage').fadeIn(250,function() {
					setTimeout(function(){
						$('#favmessage').hide();
					},1500);
				});
			}
			this.collection.addToCompare(e.target.id.replace("compare_",""));
			$('#compareCount').html(this.collection.compareModels.length);

			var selid = e.target.id.replace("compare_","");

			$('#compare_'+selid + '.addToCompare').hide();
			$('#remove_'+selid + '.removeCompare').show();
		},

		removeCompare: function (e) {
			var self=this;
			/* if not compare view
			if(this.collection.showCompare == false){
				this.collection.removeCompare(e.target.id.replace("remove_",""),"pview");
				var selid = e.target.id.replace("remove_","");
				$('#compare_'+selid + '.addToCompare').show();
				$('#remove_'+selid + '.removeCompare').hide();

			}
			else{ /*if from compare view

				this.collection.removeCompare(e.target.id.replace("remove_",""),"cview");
				/*if no items in compare array
				if(this.collection.compareModels.length < 1)
					this.collection.showCompare == false;

			}
			this.collection.updateCompareSummary();

		},*/

	});

	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemDentalTemplateMainSummary').html()),

		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			$(this.el).html(this.template(this.model.toJSON()));
			return this;
		}
			
	});
})(App.views);
