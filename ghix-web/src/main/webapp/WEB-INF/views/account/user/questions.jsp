<%-- <%@page isELIgnored="false"  %> --%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>

<link href="<c:url value="/resources/css/passwordResetChat.css" />" rel="stylesheet" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<style>
	#bot{
		position: absolute; 
		width: 270px; 
		height: 380px; 
		border: solid 2px #3a96dd; 
		right: 1%; 
		bottom: 0;
		background-color: #fff;
	}
	label.wc-upload {
    	display: none;
	}
</style>
	
<div class="gutter10">
	<h1>Reset your password</h1>

	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div> --%>
	
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">You should know</h4>
			</div>
			<ul class="gutter10 unstyled">
			    <li>Answer the following questions in order to reset your password</li>
		    </ul>
		</div>
		<div class="span9" id="rightpanel">
			<div class="graydrkaction">
				<h4 class="span10">Security questions</h4>
			</div>
				<form class="form-horizontal" id="frmquestions" name="frmquestions" action="questions" method="POST">
					<df:csrfToken/>
					<div class="question_answer">
						<%-- <div class="control-group">
							<h3><label for="question1" id="securityQuestion1" class="required">${securityQuestion1}</label></h3>
							
							<label class="control-label">Answer</label>
							<div class="controls">
								<input type="text" name="securityAnswer1" id="securityAnswer1" class="input-xlarge" size="30" value="${securityAnswer1}">
								<div id="securityAnswer1_error"></div>	
							</div>
						</div>
						<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" /> 
						 --%>
	<div class="control-group">
					<c:forEach items="${accountUserDto.userSecQstnAnsList}" var="currSecQstnAns" varStatus="status">
						<c:set var="qstnAnsIdx" value="${status.index+1}" />
						<form:input type="hidden"  path="accountUserDto.userSecQstnAnsList[${status.index}].idx" value="${qstnAnsIdx}" class="input-xlarge" size="30"  />	
       					<label for="accountUserDto.userSecQstnAnsList[${status.index}].question"><c:out value="${currSecQstnAns.question}"></c:out></label>
       					<form:input type="hidden"  path="accountUserDto.userSecQstnAnsList[${status.index}].question" value="${currSecQstnAns.question}" class="input-xlarge"  />		
					 	<label for="accountUserDto.userSecQstnAnsList[${status.index}].answer" class="control-label">Answer</label>
						<div class="controls">
							<form:input type="text" path="accountUserDto.userSecQstnAnsList[${status.index}].answer" class="input-xlarge" size="30"  />	
							<div id="securityAnswer${qstnAnsIdx}_error"></div>						
						</div> 
    				</c:forEach>
				</div>						</div><!-- question_answer -->
					<div class="control-group">
						<div class="controls">							
							<input type="submit" id="continue" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-large" title="<spring:message  code='label.brkContinue'/>">
							
							<c:if test="${!empty errorMsg}">
								<div class="alert alert-error alert-error-custom">
									<c:out value="${errorMsg}"></c:out>
								</div>
							</c:if>
							
						</div>
					</div>	
				</form>
			</div>
	</div>
</div>
<%
		final String secretKey = prop.getProperty("bot.password.reset.secretkey");
        pageContext.setAttribute("botSecretKey", secretKey);
%>
<c:if test="${not empty botSecretKey && (not empty errorMsg && not fn:containsIgnoreCase(errorMsg, 'unsuccessful attempts'))}">
		<div id="bot"></div>
				    <script src="<c:url value="/resources/js/passwordResetChat.js" />"></script>
				    <script>
				      BotChat.App({
				        directLine: { secret: '${botSecretKey}' },	// secret key hard-corded for now..
				        user: { id: 'userid' },
				        bot: { id: 'password-reset' },
				        resize: 'detect'
				      }, document.getElementById("bot"));
				    </script>
		</div>
</c:if>
<script type="text/javascript">
var validator = $("#frmquestions").validate({ 
	onkeyup: false,
	rules : {
		securityAnswer1: { required : true	},
		securityAnswer2: { required : true	},
		securityAnswer3: { required : true	}
	},
	messages : {
		securityAnswer1: { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Answer is required.</span>" },
		securityAnswer2: { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Answer is required.</span>" },
		securityAnswer3: { required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Answer is required.</span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	}
});
	
</script>