package com.getinsured.hix.account.service.jpa;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.account.repository.ILocationRepository;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.model.Location;
import com.google.gson.Gson;

@Service("locationService")
@Repository
public class LocationServiceImpl implements LocationService{
	@Autowired private ILocationRepository locationreposiroty;

	@Override
	public Location findById(int id) {
		return locationreposiroty.findOne(id);
	}
	
	@Override
	public Location save(Location location){
		return locationreposiroty.save(location);
	}
	
	@Override
	public String getLoationsJSON(Location location){
		Gson gson = new Gson();

		Map<String,String> locationData = new HashMap<String,String>();
		locationData.put("id", String.valueOf(location.getId()));
		locationData.put("address",location.getAddress1() );
		locationData.put("address2", location.getAddress2());
		locationData.put("city", location.getCity() );
		locationData.put("state", location.getState() );
		locationData.put("zip", String.valueOf(location.getZip()));
		locationData.put("county", location.getCounty());
		
		return gson.toJson(locationData);
	}
}
