package com.getinsured.hix.platform.file.reader;

import java.util.List;

import com.getinsured.hix.platform.file.mapper.Mapper;


/**
 * Abstract class for reading/parsing the data file  and return List of Model objects.
 */
public abstract class FileReader<T> 
{
	Mapper<T> mapper;
	protected String fileName;

	public void setMapper(Mapper<T> mapper) 
	{
		this.mapper = mapper;
	}
	
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	public abstract void setDelimiter(char delimiter); 
	public abstract List<T> readData();
	public abstract List<String> getColumns();
}
