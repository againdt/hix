package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.hix.model.ConsumerDocument;

public interface ReferralDocumentTriggerMailService {
	
	 void triggerDocumentStatusMail(ConsumerDocument consumerDocument,
			ReferralActivation referralActivation);
	
	 Integer sendNotificationToUploadDoc(ReferralActivation referralActivation);

}
