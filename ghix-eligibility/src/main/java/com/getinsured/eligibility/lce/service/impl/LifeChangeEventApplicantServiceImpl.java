package com.getinsured.eligibility.lce.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
public class LifeChangeEventApplicantServiceImpl implements
		LifeChangeEventApplicantService {

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	private SsapUtil ssapUtil;
	@Autowired
	private ILocationRepository iLocationRepository;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private LifeChangeEventUtil lifeChangeEventUtil;

	private final String SSAPAPPLICANT_GUID_KEY = "SSAP_APPLICANT_DISPLAY_ID";
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventApplicantServiceImpl.class);

	private static final String CONSUMER_ID = "consumerId";

	private static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";

	private static final String GENDER = "gender";

	private static final String LAST_NAME = "lastName";

	private static final String FIRST_NAME = "firstName";

	private static final String BIRTH_DATE = "birthDate";

	@Override
	public List<SsapApplicant> cloneSsapApplicants(
			SsapApplication currentApplication, List<RequestParamDTO> events, Long userId, SepResponseParamDTO sepResponseParamDTO) throws Exception {

		String personId;
		Map<String, String> applicantsGuid = new HashMap<String, String>();
		List<SsapApplicant> ssapApplicants = new ArrayList<SsapApplicant>();

		JsonArray householdMembers = LifeChangeEventUtil.getHouseholdMembers(currentApplication.getApplicationData());
		Map<Long, List<RequestParamDTO>> changedApplicantsWithEvents = this.getChangedApplicantsWithEvents(events, householdMembers);

		List<SsapApplicant> applicantsBeforeClone = ssapApplicantRepository.findBySsapApplicationId(currentApplication.getParentSsapApplicationId().longValue());
		Map<Long, SsapApplicant> applicantsBeforeCloneMap = getApplicantsMap(applicantsBeforeClone);
		Iterator<JsonElement> iterator = householdMembers.iterator();
		
		JsonObject bloodRelationship = ssapUtil.getBloodRelationship(householdMembers);

		while (iterator.hasNext()) {
			JsonObject householdMember = iterator.next().getAsJsonObject();
			personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();

			SsapApplicant ssapApplicant = cloneSsapApplicant(new Long(personId), householdMember, changedApplicantsWithEvents, applicantsBeforeCloneMap, currentApplication, userId, sepResponseParamDTO);
			ssapApplicant = updateSsapApplicant(new Long(personId), householdMember, householdMembers, ssapApplicant, changedApplicantsWithEvents, bloodRelationship);
			if(ssapApplicant != null){
				applicantsGuid.put(personId, ssapApplicant.getApplicantGuid());
			}
			ssapApplicants.add(ssapApplicant);
		}
		updateApplicantsGuid(currentApplication, applicantsGuid);

		return ssapApplicants;
	}

	@Override
	public SsapApplicant cloneSsapApplicant(Long personId, JsonObject householdMember, Map<Long, List<RequestParamDTO>> changedApplicantsWithEvents, Map<Long, SsapApplicant> applicantsBeforeCloneMap, SsapApplication currentApplication, Long userId, SepResponseParamDTO sepResponseParamDTO) throws Exception {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		SsapApplicant ssapApplicant;
		String onApplication = LifeChangeEventConstant.YES;
		String tobaccoUser;
		String applyingForCoverage;
		String dateOfBirth = null;
		String marriedIndicator = LifeChangeEventConstant.FALSE;
		JsonObject householdContact;
		String householdContactIndicator;
		String applicantGUID = null, externalApplicantID = null;
		Map<String, Object> memberData = null;

		if(applicantsBeforeCloneMap.containsKey(personId)){
			ssapApplicant = applicantsBeforeCloneMap.get(personId);

			if(LifeChangeEventConstant.Status.LCE_DELETE.toString().equalsIgnoreCase(ssapApplicant.getStatus())){
				return null;
			} else {
				if(LifeChangeEventUtil.checkDeletedApplicants(householdMember)){
					currentApplication.setApplicationData(LifeChangeEventUtil.updateRelationshipCode(String.valueOf(personId), LifeChangeEventUtil.removePersonFromJson(String.valueOf(personId), currentApplication.getApplicationData())));
					//sepTransientDTO.setCurrentApplication(sepTransientDTO.getCurrentApplication());
					ssapApplicant.setStatus(LifeChangeEventConstant.Status.LCE_DELETE.toString());
					onApplication = LifeChangeEventConstant.NO;
				} else {
					if(changedApplicantsWithEvents.keySet().contains(personId)){
						ssapApplicant.setStatus(LifeChangeEventConstant.Status.LCE_UPDATE.toString());
					} else {
						ssapApplicant.setStatus(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString());
					}
				}
			}
		} else {
			ssapApplicant = new SsapApplicant();
			ssapApplicant.setStatus(LifeChangeEventConstant.Status.LCE_ADD.toString());
			String applicantPersonType = LifeChangeEventUtil.getJsonAsString(householdMember.get("applicantPersonType"));

			SsapApplicantPersonType ssapApplicantPersonType = SsapApplicantPersonType.DEP;

			if(applicantPersonType != null){
				if(applicantPersonType.equalsIgnoreCase("PC_PTF")){
					ssapApplicantPersonType = SsapApplicantPersonType.PC_PTF;
				}
				else if(applicantPersonType.equalsIgnoreCase("PTF")){
					ssapApplicantPersonType = SsapApplicantPersonType.PTF;
				}
			}

			ssapApplicant.setPersonType(ssapApplicantPersonType);
		}
		ssapApplicant.setId(0);
		ssapApplicant.setPersonId(personId);
		ssapApplicant.setOnApplication(onApplication);
		ssapApplicant.setSsapApplication(currentApplication);
		ssapApplicant.setCreatedBy(userId.intValue());
		ssapApplicant.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
		ssapApplicant.setSsapApplicantEvents(null);
		ssapApplicant.setSsapVerifications(null);
		ssapApplicant.setEligibilityProgram(null);
		ssapApplicant.setFirstName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.FIRST_NAME)));
		ssapApplicant.setMiddleName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.MIDDLE_NAME)));
		ssapApplicant.setLastName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.LAST_NAME)));
		ssapApplicant.setGender(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.GENDER)));

		if(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR)) != null){
			marriedIndicator =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR)).equals("true")?"true":"false";
		}
		ssapApplicant.setMarried(marriedIndicator);

		dateOfBirth = LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.DATE_OF_BIRTH));

		if(!LifeChangeEventUtil.isEmpty(dateOfBirth)){
			ssapApplicant.setBirthDate(LifeChangeEventUtil.parseDate(dateOfBirth));
		}

		tobaccoUser = LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.TOBACCO_USER_INDICATOR));

		//HIX-110285: Set tobacco user as NULL for CA
		if("CA".equalsIgnoreCase(stateCode)){
			ssapApplicant.setTobaccouser(null);
		}
		else if(!LifeChangeEventUtil.isEmpty(tobaccoUser)){
			ssapApplicant.setTobaccouser(tobaccoUser.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
		}

		applyingForCoverage =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.APPLYING_FOR_COVERAGE_INDICATOR));

		if(!LifeChangeEventUtil.isEmpty(applyingForCoverage)){
			ssapApplicant.setApplyingForCoverage(applyingForCoverage.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
		}

		householdContactIndicator =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT_INDICATOR));
		if(!LifeChangeEventUtil.isEmpty(householdContactIndicator)){
			ssapApplicant.setHouseholdContactFlag(householdContactIndicator.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
		}

		if(LifeChangeEventUtil.isEmpty(ssapApplicant.getApplicantGuid())){
			memberData = new HashMap<String, Object>();

			memberData.put(FIRST_NAME, ssapApplicant.getFirstName());
			memberData.put(LAST_NAME, ssapApplicant.getLastName());

			memberData.put(BIRTH_DATE, ssapApplicant.getBirthDate());
			memberData.put(GENDER, ssapApplicant.getGender());

			memberData.put(SOCIAL_SECURITY_NUMBER, ssapApplicant.getSsn());

			Validate.notNull(currentApplication);
			Validate.notNull(currentApplication.getCmrHouseoldId());
			memberData.put(CONSUMER_ID, String.valueOf(currentApplication.getCmrHouseoldId()));

			Map<String, String> applicantIDs = ssapUtil.retrieveApplicantIDs(memberData);

			if(null == applicantIDs || applicantIDs.isEmpty()) {
				ssapApplicant.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
			}
			else {
				applicantGUID = applicantIDs.get("APPLICANT_GUID");

				if(StringUtils.isBlank(applicantGUID)) {
					ssapApplicant.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
				}
				else {
					ssapApplicant.setApplicantGuid(applicantGUID);
				}

				externalApplicantID = applicantIDs.get("EXTERNAL_APPLICANT_ID");
				ssapApplicant.setExternalApplicantId(externalApplicantID);
			}
		}

		JsonObject specialCircumstances = householdMember.get(LifeChangeEventConstant.SPECIAL_CIRCUMSTANCES).getAsJsonObject();
		if( specialCircumstances != null && !specialCircumstances.isJsonNull()
				&& specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR) != null && !specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR).isJsonNull()){
			ssapApplicant.setNativeAmericanFlag(specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR).getAsBoolean()?"Y":"N");
		}

		JsonObject socialSecurityCard = householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject();

		householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();

		if(socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER) != null && !socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER).isJsonNull()){
			ssapApplicant.setSsn(socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER).getAsString());
		}

		//Setting Verification Flags for these rules : Death, Tribe Status, SSN, Citizenship and Incarceration
		List<RequestParamDTO> changedEvents = changedApplicantsWithEvents.get(personId);
		if(changedEvents!=null){
			setVerificationFlags(changedApplicantsWithEvents.get(personId), ssapApplicant);
		}

		if(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID==personId){
			boolean isNameChangeEvent = checkEventName(LifeChangeEventConstant.DEMOGRAPHICS_NAME_CHANGE, changedApplicantsWithEvents.get(personId));
			boolean isDobChange = checkEventName(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE, changedApplicantsWithEvents.get(personId));
			if(isNameChangeEvent || isDobChange){
				updateNameInCmrHouseholdAndUserTable(currentApplication, householdMember, sepResponseParamDTO,isNameChangeEvent,isDobChange,ssapApplicant,userId);
			}
		}

		return ssapApplicant;

	}

	@Override
	public Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(
			List<RequestParamDTO> events, JsonArray householdMembers)
			throws Exception {
		Map<Long, List<RequestParamDTO>> setOfModifiedApplicants = new  HashMap<>();
		List<RequestParamDTO> personEvents = null;

		for(RequestParamDTO requestParamDTO:events){
			List<ChangedApplicant> changedApplicants = requestParamDTO.getChangedApplicants();
			if(changedApplicants != null){
				for (ChangedApplicant changedApplicant : changedApplicants) {
					if(setOfModifiedApplicants.get(changedApplicant.getPersonId())!=null){
						personEvents = setOfModifiedApplicants.get(changedApplicant.getPersonId());
						personEvents.add(requestParamDTO);
					} else {
						personEvents = new ArrayList<>();
						personEvents.add(requestParamDTO);
					}
					setOfModifiedApplicants.put(changedApplicant.getPersonId(), personEvents);
				}
			}
		}

		return setOfModifiedApplicants;
	}

	@Override
	public SsapApplicant saveSsapApplicant(SsapApplicant ssapApplicant) {
		return ssapApplicantRepository.saveAndFlush(ssapApplicant);
	}

	private Map<Long, SsapApplicant> getApplicantsMap(
			List<SsapApplicant> applicantsBeforeClone) {
		Map<Long, SsapApplicant> applicantsBeforeCloneMap = new HashMap<Long, SsapApplicant>();
		for (SsapApplicant ssapApplicant : applicantsBeforeClone) {
			applicantsBeforeCloneMap.put(ssapApplicant.getPersonId(), ssapApplicant);
		}
		return applicantsBeforeCloneMap;
	}


	private SsapApplicant updateSsapApplicant(Long personId,
			JsonObject householdMember, JsonArray householdMembers,
			SsapApplicant ssapApplicant, Map<Long, List<RequestParamDTO>> events, JsonObject bloodRelationship) {
		JsonObject primaryHouseholdContact;

		primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject otherPhone = primaryHouseholdContact.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject();
		if(otherPhone!=null && !otherPhone.isJsonNull() && otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER)!=null && !otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER).isJsonNull()){
			ssapApplicant.setPhoneNumber(otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER).getAsString());
		}

		JsonObject phone = primaryHouseholdContact.get(LifeChangeEventConstant.PHONE).getAsJsonObject();
		if(phone!=null && !phone.isJsonNull() && phone.get(LifeChangeEventConstant.PHONE_NUMBER)!=null && !phone.get(LifeChangeEventConstant.PHONE_NUMBER).isJsonNull()){
			ssapApplicant.setMobilePhoneNumber(phone.get(LifeChangeEventConstant.PHONE_NUMBER).getAsString());
		}

		JsonObject contactPreferences = primaryHouseholdContact.get(LifeChangeEventConstant.CONTACT_PREFERENCES).getAsJsonObject();
		if(contactPreferences!=null && !contactPreferences.isJsonNull() && contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS)!=null && !contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS).isJsonNull()){
			ssapApplicant.setEmailAddress(contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS).getAsString());
		}

		//new records should create location ids, updated for HIX-78534
		updateHomeAddress(personId, householdMember, householdMembers, ssapApplicant);
		updateMailingAddress(personId, householdMember, householdMembers, ssapApplicant);

		/*if(!LifeChangeEventUtil.isEmpty(ssapApplicant.getStatus()) && LifeChangeEventConstant.Status.LCE_ADD.toString().equals(ssapApplicant.getStatus())){
			updateHomeAddress(personId, householdMember, householdMembers, ssapApplicant);

			updateMailingAddress(personId, householdMember, householdMembers, ssapApplicant);
		} else {
			if(events!=null){
				if(checkEventCategory(LifeChangeEventConstant.ADDRESS_CHANGE, events.get(personId))){
					updateHomeAddress(personId, householdMember, householdMembers, ssapApplicant);
				}

				if(checkEventName(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE, events.get(personId))){
					updateMailingAddress(personId, householdMember, householdMembers, ssapApplicant);
				}
			}
		}*/
		
		String applicantRelationWithPrimary = ssapUtil.getApplicantRelation(bloodRelationship, personId.toString());
		if(StringUtils.isNotEmpty(applicantRelationWithPrimary)){
			ssapApplicant.setRelationship(applicantRelationWithPrimary);
		}

		return ssapApplicant;
	}

	private void updateHomeAddress(Long personId, JsonObject householdMember, JsonArray householdMembers, SsapApplicant ssapApplicant){
		boolean isPrimaryLocation = true;
		Location primaryLocation = new Location();
		JsonObject primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject homeAddress = primaryHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject();

		if(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID==personId){
			setAddress(primaryLocation, homeAddress);
		} else {
			if(householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR)!=null && !householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).isJsonNull()
					&& householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).getAsBoolean()){
				setAddress(primaryLocation, homeAddress);
			} else {
				JsonObject otherAddress = householdMember.get(LifeChangeEventConstant.OTHER_ADDRESS).getAsJsonObject();
				 if(otherAddress!=null){
					 JsonObject address = otherAddress.get(LifeChangeEventConstant.ADDRESS).getAsJsonObject();
					 if(address!=null){
						 setAddress(primaryLocation, address);
					 }
				 } else {
					 isPrimaryLocation = false;
				 }
			}
		}

		if(isPrimaryLocation){
			iLocationRepository.save(primaryLocation);
			ssapApplicant.setOtherLocationId(new BigDecimal(primaryLocation.getId()));
		}
	}

	private void updateMailingAddress(Long personId, JsonObject householdMember, JsonArray householdMembers, SsapApplicant ssapApplicant){
		boolean isMailingLocation = true;
		Location mailingLocation = new Location();
		JsonObject primaryHouseholdContact;

		primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject mailingAddress = primaryHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject();

		if(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID==personId){
			setAddress(mailingLocation, mailingAddress);
		} else {
			if(householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR)!=null && !householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).isJsonNull()
					&& householdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).getAsBoolean()){
				setAddress(mailingLocation, mailingAddress);
			} else {
				JsonObject otherAddress = householdMember.get(LifeChangeEventConstant.OTHER_ADDRESS).getAsJsonObject();
				 if(otherAddress!=null){
					 JsonObject address = otherAddress.get(LifeChangeEventConstant.ADDRESS).getAsJsonObject();
					 if(address!=null){
						 setAddress(mailingLocation, address);
					 }
				 } else {
					 isMailingLocation = false;
				 }
			}
		}

		if(isMailingLocation){
			iLocationRepository.save(mailingLocation);
			ssapApplicant.setMailiingLocationId(new BigDecimal(mailingLocation.getId()));
		}
	}

	private void setAddress(Location location, JsonObject address){
		location.setAddress1(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		location.setAddress2(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		location.setCity(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.CITY)));
		location.setState(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STATE)));
		location.setZip(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.POSTAL_CODE)));
		location.setCounty(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.COUNTY)));
	}

	private boolean checkEventName(String eventName, List<RequestParamDTO> events){
		if(events!=null){
			for(RequestParamDTO param:events){
				String name = param.getEventSubCategory();
				if(eventName.equals(name)){
					return true;
				}
			}
		}

		return false;
	}

	private boolean checkEventCategory(String eventCategory, List<RequestParamDTO> events){
		if(events!=null){
			for(RequestParamDTO param:events){
				String category = param.getEventCategory();
				if(eventCategory.equals(category)){
					return true;
				}
			}
		}

		return false;
	}

	private void setVerificationFlags(List<RequestParamDTO> events, SsapApplicant ssapApplicant){
		for(RequestParamDTO requestParamDTO:events){
			String category = requestParamDTO.getEventCategory();
			String eventName = requestParamDTO.getEventSubCategory();

			if(LifeChangeEventConstant.MARITAL_STATUS_LEGAL_SEPERATION.equals(eventName) || LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT.equals(eventName) || LifeChangeEventConstant.MARITAL_STATUS_DEATH_OF_SPOUSE.equals(eventName) || LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH_OF_PRIMARY_APPLICANT.equals(eventName) || LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH_OF_DEPENDENTS.equals(eventName)
					|| LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.LOSS_OF_MEC.equals(category) || LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE.equals(category)){
				ssapApplicant.setDeathStatus(null);
				ssapApplicant.setDeathVerificationVid(null);
			}

			if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE_DEPENDENTS.equals(eventName) || LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE.equals(eventName) || LifeChangeEventConstant.TRIBE_STATUS_GAIN_IN_TRIBE_STATUS.equals(eventName) || LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE.equals(eventName)){
				ssapApplicant.setResidencyStatus(null);
				ssapApplicant.setResidencyVid(null);
			}

			if(LifeChangeEventConstant.MARITAL_STATUS_LEGAL_SEPERATION.equals(eventName) || LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.LOSS_OF_MEC.equals(category)
					|| LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT.equals(eventName) || LifeChangeEventConstant.DEMOGRAPHICS_SSN_CHANGE.equals(eventName)){
				ssapApplicant.setSsnVerificationStatus(null);
				ssapApplicant.setSsnVerificationVid(null);
			}

			if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.LOSS_OF_MEC.equals(category)
					 || LifeChangeEventConstant.MARITAL_STATUS_LEGAL_SEPERATION.equals(eventName) || LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT.equals(eventName) || LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE.equals(category)){
				ssapApplicant.setCitizenshipImmigrationStatus(null);
				ssapApplicant.setCitizenshipImmigrationVid(null);
				ssapApplicant.setVlpVerificationVid(null);
				ssapApplicant.setVlpVerificationStatus(null);
			}

			if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName) || LifeChangeEventConstant.LOSS_OF_MEC.equals(category) || LifeChangeEventConstant.MARITAL_STATUS_LEGAL_SEPERATION.equals(eventName) || LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT.equals(eventName)
					 || LifeChangeEventConstant.NOT_INCARCERATED.equals(eventName)){
				ssapApplicant.setIncarcerationStatus(null);
				ssapApplicant.setIncarcerationVid(null);
			}
		}
	}

	private SsapApplication updateApplicantsGuid(SsapApplication ssapApplication, Map<String, String> applicantsGuid) throws Exception{
		ssapApplication.setApplicationData(LifeChangeEventUtil.setApplicantsGuid(ssapApplication.getApplicationData(), applicantsGuid));
		return ssapApplication;
	}

	private void updateNameInCmrHouseholdAndUserTable(SsapApplication currentApplication, JsonObject householdMember, SepResponseParamDTO sepResponseParamDTO,boolean isNameChangeEvent, boolean isDobChange,SsapApplicant ssapApplicant,Long userId) throws Exception{
		try {
			Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());
			//Updating Household Object from Json
			if(isNameChangeEvent){
			household = lifeChangeEventUtil.setNameInCmrHousehold(householdMember, household);
			if(household.getUser()!=null){
				userRepository.saveAndFlush(household.getUser());
				sepResponseParamDTO.setIsNameChangedForPrimary(true);
				sepResponseParamDTO.setPrimaryFirstName(household.getFirstName());
				sepResponseParamDTO.setPrimaryLastName(household.getLastName());
				sepResponseParamDTO.setPrimaryMiddleName(household.getMiddleName());
			}
			}
			if(isDobChange){
				household.setBirthDate(ssapApplicant.getBirthDate());
			}
			//set current user id
			household.setUpdatedBy(userId.intValue());
			cmrHouseholdRepository.saveAndFlush(household);
		} catch(Exception exception){
			throw exception;
		}
	}
}
