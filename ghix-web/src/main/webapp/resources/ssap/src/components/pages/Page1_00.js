import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Privacy of Your Information' Page
 *
 */
const Page1_00 = props => {

    const {
        currentData,
        currentData: {
            acceptPrivacyIndicator
        },
        data
    } = props;

    /**
     *
     * @param {Object} newData - updated <singleStreamlinedApplication> object
     *
     * Updating <acceptPrivacyIndicator> props in JSON
     */
    const onFieldChange = newData => {
        props.actions.changeData({
            ...data,
            singleStreamlinedApplication: newData
        });
    };

    return (
        <Fragment>
            <div id="saPrivacyInfo" className="fade-in">
                <div className="subsection">
                    <div className="header">
                        <h3 className="usa-heading-alt">Privacy of Your Information</h3>
                    </div>
                    <p>
                        <FormattedMessage
                            id="ssap.step.one.paraOne"
                            defaultMessage="The privacy of your information is our top priority.
                            We'll keep your information private as required by law.
                            Your answers on this form will only be used to determine eligibility for health coverage.
                            We'll verify your answers using the information in our electronic databases
                            and the databases of state and federal agencies.
                            If the information doesn't match, we may ask you to send us additional proof.
                            We won't ask any questions about your medical history."
                        />
                    </p>
                    <h4>
                        <strong>
                            <FormattedMessage
                                id="ssap.step.one.paraTwo"
                                defaultMessage="Important:"
                            />
                        </strong>
                    </h4>
                    <p>
                        <FormattedMessage
                            id="ssap.step.one.paraThree"
                            defaultMessage="As part of the application process, we may need to retrieve your information from
                        the Social Security Administration, the Department of Homeland Security,
                        a consumer reporting agency, and other services available through
                        the Federal Data Services Hub. We need this information to check your ability
                        to enroll in for coverage on Nevada Health Link. We may also re-verify
                        your information at a later time to make sure your information
                        is up to date and will notify you if we find something has changed."
                        />
                    </p>
                    <p>
                        <FormattedMessage
                            id="ssap.step.one.paraFour"
                            defaultMessage="To learn more, see the"
                        />
                        <span>&nbsp;</span>
                        <a
                            className="usa-external_link"
                            href="https://www.nevadahealthlink.com/about-nevada-health-link/privacy-policy/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <FormattedMessage
                                id="ssap.step.one.paraFourLink"
                                defaultMessage="Notice of Privacy Practices"
                            />
                        </a>
                    </p>
                </div>
                <div className="subsection">
                    <Form className="usa-grid" style={{width: '100%'}}>
                        <div className="usa-width-one-whole">
                            <fieldset className="usa-fieldset-inputs usa-sans ">
                                <ul className="usa-unstyled-list">
                                    <li>
                                        <UniversalInput
                                            label={{
                                                text: 'I agree to have my information used and retrieved from data sources for this application. ' +
                                                    'I have consent for all the people that will be included on the application for their information ' +
                                                    'to be retrieved and used from data sources mentioned above.'
                                            }}
                                            inputData={{
                                                isInputRequired: true,
                                                classes:['disp-styl-none'],
                                                type: INPUT_TYPES.CHECKBOX,
                                                name: 'acceptPrivacyIndicator',
                                                checked: acceptPrivacyIndicator,
                                                value: acceptPrivacyIndicator,
                                                validationType: VALIDATION_TYPES.CHECKED,
                                                currentData: currentData,
                                                fields: []
                                            }}
                                            inputActions={{
                                                onFieldChange
                                            }}
                                        />
                                    </li>
                                </ul>
                            </fieldset>
                        </div>
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};


const mapStateToProps = state => ({

    currentData: state
        .data
        .singleStreamlinedApplication,

    data: state.data
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page1_00.defaultProps = {
    name: 'Page1_00'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_00);
