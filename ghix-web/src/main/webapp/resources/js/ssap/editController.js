
var editFlag = false;
var editMemberNumber = 0;
var cancleFlag = true;
var loginUserID = "";
var editData = false;
var editNonFinacial = false;
var applicantGuidToBeRemoved = "";
var personIdToBeRemoved = "";
//	Edit functionality of Household Information

function removeApplicant(personID)
{
    if(personID == 1) return; // personid = 1 can't be deleted 
    
    var noOfHouseHoldmember= getNoOfHouseHolds();
    for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
    		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];    		
         	if(householdMemberObj.personId > 1 &&  householdMemberObj.personId == personID){
         		personIdToBeRemoved = personID;
         		applicantGuidToBeRemoved = householdMemberObj.applicantGuid;
         		houseHoldname = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
         		$('#deleteApplicantName').html(houseHoldname);
         		$('#btnDeleteApplicantYes').prop("disabled",false);
         		$('#modalDeleteApplicant').modal();
         		/*if(householdMemberObj.applicantGuid==""){
         			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.splice(cnt-1,1);// remove applicant from array         			
         			afterDeleteApplicant();
         		}else{
         			applicantGuidToBeRemoved = householdMemberObj.applicantGuid;         			
         			saveDataSubmit();         			
         		}*/
         		//processApplicantDeletion(householdMemberObj.applicantGuid, personID);
         		break; //exit for loop
         	}                              
    }   
    
    //nonFinancialHouseHoldDone = noOfHouseHoldmember; // last household done
    
    //saveData()// call existing method to save data into DB

}
function deleteApplicantConfirmed(){
	//$('#btnDeleteApplicantYes').prop("disabled",true); // disabled, restict user to click only once
	$('#modalDeleteApplicant').modal('hide');
	processApplicantDeletion(applicantGuidToBeRemoved, personIdToBeRemoved);
}
function cancelDeleteApplicant(){	
	$('#modalDeleteApplicant').modal('hide');
	applicantGuidToBeRemoved ="";
	personIdToBeRemoved = "";
}
function removeRelationship(personID){
	
	var bloodRelationship=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	var noOfRelations = bloodRelationship.length;
	var currentRelationInd = 0;
    for ( var ind = 0; ind < noOfRelations; ind++) {
    	if (bloodRelationship[currentRelationInd].individualPersonId == personID || bloodRelationship[currentRelationInd].relatedPersonId == personID){
    		bloodRelationship.splice(currentRelationInd,1);
    	}else{
    		currentRelationInd++;
    	}
    }    
    webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = bloodRelationship;
}

function correctIncarcerationIndicator(){
	noOfHouseHold = getNoOfHouseHolds();
	webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = false;
	for (i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];		
		if(householdMemberObj.incarcerationStatus.incarcerationStatusIndicator == true){
			webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = true;
			break;
		}
	}	
}

function afterDeleteApplicant(personID){
	//removeRelationship(personID);// still have to work on this logic         			
	//correctIncarcerationIndicator();
	noOfHouseHoldmember=getNoOfHouseHolds(); 
    $('#numberOfHouseHold').text(noOfHouseHoldmember); //reset no. of household variable
    $('#noOfPeople53').val(noOfHouseHoldmember); //reset no. of household variable    
    if(currentPage == "appscr58"){
		Appscr58DetailJson();
	}else if(currentPage == "appscr67"){
		showHouseHoldContactSummary();
		nonFinancialHouseHoldDone = getNoOfHouseHolds();
		saveCurrentApplicantID(nonFinancialHouseHoldDone);
	}   
}

function findApplicantByPersonId(personID){
	var noOfHouseHoldmember= getNoOfHouseHolds();
    for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
    		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
         	if(householdMemberObj.personId == personID){
         		return householdMemberObj;         		
         		break;
         	}                              
    }   
}

/*
function deleteApplicantFromDB(personId){
	webJsonstring = JSON.stringify(webJson);
	var isApplicantDeleted = false;
	$.ajax({
		type : 'POST',
		url : '"/ssap/deleteSsapApplicant',
		dataType : 'json',
		data : {
			personId : personId,			
			currentPage : currentPage,
			webJson:webJsonstring
					
		},
		//async : true,
		success : function(msg,xhr) {
			if(msg!='' && msg != null) {
				webJson = msg;
			}
			
			if(webJson.singleStreamlinedApplication.mecCheckStatus!=null && webJson.singleStreamlinedApplication.mecCheckStatus!=undefined && "Y"==webJson.singleStreamlinedApplication.mecCheckStatus){
				$('#mecCheckAlert').modal();
			}
			$('#ssapSubmitModal').modal('hide');
			$('#waiting').hide();
			$('#contBtn').removeAttr('disabled');		
			//$('#generateJSONForm').submit();
			isApplicantDeleted = true;
			
		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {	
			$('#waiting').hide();
			$('#ssapSubmitModal').modal('hide');
			alert("Please try Again !");			
			//$('#contBtn').removeAttr('disabled');
			isApplicantDeleted = false;
		}
	});
	return isApplicantDeleted;
}
*/
function appscr58EditController(i)
{
	editFlag = true;

	editMemberNumber = i;

	//$('#contBtn').text('Update');
	$('#countinue_button_div').removeClass("Continue_button");
	$('#countinue_button_div').addClass("ContinueForUpdate_button");
	$('#cancelButtonCancel').show();
	$('#back_button_div').hide();
	$('#contBtn').hide();


	if(i == 1)
	{
		setValuesToappscr53JSON();
		goToPageById("appscr53");
		$('#contBtn').attr('onclick', 'appscr58EditUdate(1),'+true+'');
	}
	else
	{
		$('#appscr57FirstName'+i).focus();
		setValuesToappscr57JSON();
		goToPageById("appscr57");
		$('#contBtn').attr('onclick', 'appscr58EditUdate('+i+'),'+true+'');

		blockOtherFields(i);
	}
}

function enableOtherFields()
{
	/*for(var j=1; j<=noOfHouseHold; j++)
	{
		$('#appscr57FirstName'+j).attr("disabled",false);
		$('#appscr57MiddleName'+j).attr("disabled",false);
		$('#appscr57LastName'+j).attr("disabled",false);
		$('#appscr57Suffix'+j).attr("disabled",false);
		$('#appscr57DOB'+j).attr("disabled",false);

		$('#appscr57DOBVerificationStatus'+j).attr("disabled",false);
		$('input[name=appscr57Sex'+j+']:radio').attr("disabled",false);
		$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",false);
	}*/
	$('#appscr57HouseholdForm input, #appscr57HouseholdForm select').prop('disabled',false);
}

function blockOtherFields(i)
{
	for(var j=1; j<=noOfHouseHold; j++)
	{
		if(j==i)
		{
			$('#appscr57FirstName'+j).focus();
			continue;
		}
		else
		{
			$('#appscr57FirstName'+j).attr("disabled",true);
			$('#appscr57MiddleName'+j).attr("disabled",true);
			$('#appscr57LastName'+j).attr("disabled",true);
			$('#appscr57Suffix'+j).attr("disabled",true);
			$('#appscr57DOB'+j).attr("disabled",true);
			
			$('#appscr57DOBVerificationStatus'+j).attr("disabled",true);
			$('input[name=appscr57Sex'+j+']:radio').attr("disabled",true);
			$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",true);
			
			$('#appscr57checkseekingcoverage'+j).prop("disabled",true);
		}
	}
}

function appscr58EditCancel()
{
	$('#contBtn').show();
	enableOtherFields();
	
	if(!cancleFlag)
	{	validationCheck = true;
		$('#addAnotherHouseHold'+noOfHouseHold).hide();
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();
		noOfHouseHold--;

		$('#numberOfHouseHold').text(noOfHouseHold);
		$('#noOfPeople53').val(noOfHouseHold);
		cancleFlag = true;
	}
	else
	{
		$('#back_button_div').show();
		$('#contBtn').text('Continue');
		$('#countinue_button_div').addClass("Continue_button");
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");
		$('#cancelButtonCancel').hide();
		enableOtherFields();
		editFlag = false;
	}
		goToPageById("appscr58");

}

function appscr58EditCancle()
{
	$('#countinue_button_div').show();
	$('#back_button_div').show();
	$('#editUdateOrCancle_Div').hide();

	goToPageById("appscr58");
	editFlag = false;
}

function appscr58EditUdate(i,updateFlag)
{
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	
	if(!dateValidation){
		
		$('html, body').animate({
			scrollTop : $("#"+currentPage).find('.dobCustomError span').first().offset().top - 250
		}, 'fast');
		return;
	}
	
	$('#contBtn').text('Continue');
	$('#countinue_button_div').removeClass("ContinueForUpdate_button");
	$('#countinue_button_div').addClass("Continue_button");
	$('#cancelButtonCancel').hide();
	$('#back_button_div').show();

	cancleFlag = true;
	if(i == 1)
	{
		if(updateFlag!=false)
		{	
			saveData53();
		}
		
	}
	else
	{
		if(updateFlag!=false)
		{	backData57();
			generateArray57();
			saveData57();
		}	

		if(useJSON == true){
			appscr58EditUdateJson(i);
		}

	}
	
	addAppscr58Detail();

	if(editFlag)
	{
		goToPageById("appscr58");
		enableOtherFields();
	}
	editFlag = false;
}


//	Edit functionality of NonFinancialDetails

function appscr67EditController(i)
{

	if(useJSON == true){
		appscr67EditControllerJSON(i);
		return;
	}	
	$('#back_button_div').show();
}

function appscr74EditController(i)
{
	editMemberNumber = i;
	if(!haveDataFlag){
		editFlag = true;
		//$('#back_button_div').hide();
	}
	if(useJSON == true){
		if(!haveDataFlag){
			financialHouseHoldDone = i;
			goToPageById("appscr69");
		}
		appscr74EditControllerJSON(i);
		return;
	}	
	var editIrsIncome 							  = $('#appscrirsIncome57TD'+i).text();
	var editIncomesStatus 						  = $('#incomesStatus'+i).text();
	var editJobIncome 							  = $('#appscrFinancial_Job57TD'+i).text();
	var editJobFrequency 						  = $('#appscrFinancial_JobFrequency57TD'+i).text();
	var editEmployerName						  = $('#appscrFinancial_JobEmployerName57TD'+i).text();
	var editHoursOrDays							  = $('#appscrFinancial_JobHoursOrDays57TD'+i).text();
	var editSelfEmploymentIncome 				  = $('#appscrFinancial_Self_Employment57TD'+i).text();
	var editEmploymentTypeOfWork				  = $('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text();
	var editSocialSecurityBenefitsIncome 		  = $('#appscrFinancial_SocialSecuritybenefits57TD'+i).text();
	var editSocialSecurityBenefitsIncomeFrequency = $('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text();
	var editUnemploymentIncome 					  = $('#appscrFinancial_Unemployment57TD'+i).text();
	var editUnemploymentIncomeFrequency 		  = $('#appscrFinancial_UnemploymentFrequency57TD'+i).text();
	var editUnemploymentStateGovernment			  = $('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text();
	var editunemploymentIncomeDate				  = $('#appscrFinancial_UnemploymentDate57TD'+i).text();
	var editRetirementPensionIncome 			  = $('#appscrFinancial_Retirement_pension57TD'+i).text();
	var editRetirementPensionIncomeFrequency 	  = $('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text();
	var editCapitalgainsIncome 					  = $('#appscrFinancial_Capitalgains57TD'+i).text();
	var editInvestmentIncome 					  = $('#appscrFinancial_InvestmentIncome57TD'+i).text();
	var editInvestmentIncomeFrequency 			  = $('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text();
	var editRentalOrRoyaltyIncome 				  = $('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text();
	var editRentalOrRoyaltyIncomeFrequency 		  = $('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text();
	var editFarmingOrFishingIncome 				  = $('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text();
	var editFarmingOrFishingIncomeFrequency 	  = $('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text();
	var editAlimonyReceivedIncome 				  = $('#appscrFinancial_AlimonyReceived57TD'+i).text();
	var editAlimonyReceivedIncomeFrequency 		  = $('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text();
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	var editOtherTypeIncome						  = $('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text();

	var editExpectedIncome 						  = $('#appscrexpectedIncome57TD'+i).text();
	var editIrsHouseHoldExplanationTextArea 	  = $('#appscr57HouseHoldExplanationTextAreaTD'+i).text();

	var editHouseHoldalimonyAmount 				  = $('#appscr57HouseHoldalimonyAmountTD'+i).text();
	var editHouseHoldalimonyFrequency 			  = $('#appscr57HouseHoldalimonyFrequencyTD'+i).text();
	var editHouseHoldstudentLoanAmount 			  = $('#appscr57HouseHoldstudentLoanAmountTD'+i).text();
	var editHouseHoldstudentLoanFrequency 		  = $('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text();
	var editHouseHolddeductionsType 			  = $('#appscr57HouseHolddeductionsTypeTD'+i).text();
	var editHouseHolddeductionsAmount 			  = $('#appscr57HouseHolddeductionsAmountTD'+i).text();
	var editHouseHolddeductionsFrequency 		  = $('#appscr57HouseHolddeductionsFrequencyTD'+i).text();

	var editHouseHoldbasedOnInput 				  = $('#appscr57HouseHoldbasedOnInputTD'+i).text();

	var editstopWorking							  = $("#appscrStopWorking57TD"+i).html();
	var editworkAt								  =	$("#appscrWorkAt57TD"+i).html();
	var editdecresedAt							  = $("#appscrDecresedAt57TD"+i).html();
	var editsalaryAt							  = $("#appscrSalaryAt57TD"+i).html();
	var editsesWorker							  = $("#appscrSesWorker57TD"+i).html();
	var editexplainText							  = $("#appscrExplainText57TD"+i).html();
	var editotherJointIncome					  = $("#appscrOtherJointIncome57TD"+i).html();

	var incomeStatusFlag;

	resetPage69Values();
	resetPage71Values();
	resetPage72DeductionValues();

	var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	//var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();

	  $('.nameOfHouseHold').text(houseHoldname);


	if(editIrsIncome != 0)
	{
		$('#irsIncome2013').val(editIrsIncome);

		irsIncomePrevYear();

		$('#irsIncomeLabel').text( $('#appscrirsIncome57TD'+i).text());
	}

	if(editIncomesStatus == "Yes" || editIncomesStatus == "yes")
	{
		$('#appscr70radios4IncomeYes').prop("checked",true);
		selectIncomesCheckBoxes();

		if(editJobFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;

			document.getElementById('Financial_Job').checked = true;
			$('#Financial_Job_EmployerName_id').val(editEmployerName);
			$('#Financial_Job_HoursOrDays_id').val(editHoursOrDays);
			$('#Financial_Job_id').val(editJobIncome);
			$('#Financial_Job_select_id').val(editJobFrequency);
			showDiductionsFields();
		}
		else
		{
			document.getElementById('Financial_Job').checked = false;
			$('#Financial_Job_EmployerName_id').val('');
			$('#Financial_Job_HoursOrDays_id').val('');
			$('#Financial_Job_Div').hide();
		}

		if(editSelfEmploymentIncome != "0" && editSelfEmploymentIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Self_Employment').checked = true;
			$('#Financial_Self_Employment_TypeOfWork').val(editEmploymentTypeOfWork);
			$('#Financial_Self_Employment_id').val(editJobIncome);
		}
		else
		{
			document.getElementById('Financial_Self_Employment').checked = false;
			$('#Financial_Self_Employment_TypeOfWork').val('');
			$('#Financial_Self_Employment_Div').hide();
		}

		if(editSocialSecurityBenefitsIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_SocialSecuritybenefits').checked = true;
			$('#Financial_SocialSecuritybenefits_id').val(editSocialSecurityBenefitsIncome);
			$('#Financial_SocialSecuritybenefits_select_id').val(editSocialSecurityBenefitsIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_SocialSecuritybenefits').checked = false;
			$('#Financial_SocialSecuritybenefits_Div').hide();
		}

		if(editUnemploymentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Unemployment').checked = true;
			$('#Financial_Unemployment_StateGovernment').val(editUnemploymentStateGovernment);
			$('#Financial_Unemployment_id').val(editUnemploymentIncome);
			$('#unemploymentIncomeDate').val(editunemploymentIncomeDate);
			$('#Financial_Unemployment_select_id').val(editUnemploymentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Unemployment').checked = false;
			$('#Financial_Unemployment_StateGovernment').val('');
			$('#unemploymentIncomeDate').val('');
			$('#Financial_Unemployment_Div').hide();
		}

		if(editRetirementPensionIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Retirement_pension').checked = true;
			$('#Financial_Retirement_pension_id').val(editRetirementPensionIncome);
			$('#Financial_Retirement_pension_select_id').val(editRetirementPensionIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Retirement_pension').checked = false;
			$('#Financial_Retirement_pension_Div').hide();
		}

		if(editCapitalgainsIncome != "0" && editCapitalgainsIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Capitalgains').checked = true;
			$('#Financial_Capitalgains_id').val(editCapitalgainsIncome);
		}
		else
		{
			document.getElementById('Financial_Capitalgains').checked = false;
			$('#Financial_Capitalgains_Div').hide();
		}

		if(editInvestmentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_InvestmentIncome').checked = true;
			$('#Financial_InvestmentIncome_id').val(editInvestmentIncome);
			$('#Financial_InvestmentIncome_select_id').val(editInvestmentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_InvestmentIncome').checked = false;
			$('#Financial_InvestmentIncome_Div').hide();
		}

		if(editRentalOrRoyaltyIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = true;
			$('#Financial_RentalOrRoyaltyIncome_id').val(editRentalOrRoyaltyIncome);
			$('#Financial_RentalOrRoyaltyIncome_select_id').val(editRentalOrRoyaltyIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = false;
			$('#Financial_RentalOrRoyaltyIncome_Div').hide();
		}

		if(editFarmingOrFishingIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_FarmingOrFishingIncome').checked = true;
			$('#Financial_FarmingOrFishingIncome_id').val(editFarmingOrFishingIncome);
			$('#Financial_FarmingOrFishingIncome_select_id').val(editFarmingOrFishingIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_FarmingOrFishingIncome').checked = false;
			$('#Financial_FarmingOrFishingIncome_Div').hide();
		}

		if(editAlimonyReceivedIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_AlimonyReceived').checked = true;
			$('#Financial_AlimonyReceived_id').val(editAlimonyReceivedIncome);
			$('#Financial_AlimonyReceived_select_id').val(editAlimonyReceivedIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_AlimonyReceived').checked = false;
			$('#Financial_AlimonyReceived_Div').hide();
		}

		if(editOtherIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_OtherIncome').checked = true;
			$('#Financial_OtherTypeIncome').val(editOtherTypeIncome);
			$('#Financial_OtherIncome_id').val(editOtherIncome);
			$('#Financial_OtherIncome_select_id').val(editOtherIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_OtherIncome').checked = false;
			$('#Financial_OtherTypeIncome').val('');
			$('#Financial_OtherIncome_Div').hide();
		}
	}
	else
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}

	if(incomeStatusFlag == false)
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}

	if(editExpectedIncome != 0)
	{
		/*$('#expectedIncomeByHouseHoldMember').val(editExpectedIncome);*/
		$('#appscr69ExpectedIncome').val(editExpectedIncome);
	}

	if(editIrsHouseHoldExplanationTextArea != "")
	{
		$('#explanationTextArea').val(editIrsHouseHoldExplanationTextArea);
	}



	resetPage72DeductionValues();

	if(editHouseHoldalimonyFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72alimonyPaid').checked = true;
		$('#alimonyAmountInput').val(editHouseHoldalimonyAmount);
		$('#alimonyFrequencySelect').val(editHouseHoldalimonyFrequency);
		$('#appscr72alimonyPaidTR').show();
	}

	if(editHouseHoldstudentLoanFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72studentLoanInterest').checked = true;
		$('#studentLoanInterestAmountInput').val(editHouseHoldstudentLoanAmount);
		$('#studentLoanInterestFrequencySelect').val(editHouseHoldstudentLoanFrequency);
		$('#appscr72studentLoanInterestTR').show();
	}

	if(editHouseHolddeductionsFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72otherDeduction').checked = true;
		$('#deductionTypeInput').val(editHouseHolddeductionsType);
		$('#deductionAmountInput').val(editHouseHolddeductionsAmount);
		$('#deductionsFrequencySelect').val(editHouseHolddeductionsFrequency);
		$('#appscr72otherDeductionTR').show();
	}

	if(editHouseHoldbasedOnInput !="0" && editHouseHoldbasedOnInput != "0.0")
	{
		$('#basedOnAmountInput').val(editHouseHoldbasedOnInput);
	}

	if(editworkAt == "Yes" || editworkAt == "yes"){
		$('#appscr73WorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73WorkingIndicatorNo').prop("checked",true);
	}

	if(editdecresedAt == "Yes" || editdecresedAt == "yes"){
		$('#appscr73DecresedAtYes').prop("checked",true);
	}
	else{
		$('#appscr73DecresedAtNo').prop("checked",true);
	}

	if(editstopWorking == "Yes" || editstopWorking == "yes"){
		$('#appscr73stopWorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73stopWorkingIndicatorNo').prop("checked",true);
	}

	if(editsalaryAt == "Yes" || editsalaryAt == "yes"){
		$('#appscr73SalaryAtYes').prop("checked",true);
	}else{
		$('#appscr73SalaryAtNo').prop("checked",true);
	}

	if(editsesWorker == "Yes" || editsesWorker == "yes"){
		$('#appscr73SesWorkerYes').prop("checked",true);
	}else{
		$('#appscr73SesWorkerNo').prop("checked",true);
	}
	if(editotherJointIncome == "Yes" || editotherJointIncome == "yes"){
		$('#appscr73OtherJointIncomeYes').prop("checked",true);
	}else{
		$('#appscr73OtherJointIncomeNo').prop("checked",true);
	}


	$('#appscr73ExplanationTaxtArea').val(editexplainText);

	if(!haveDataFlag)
		goToPageById("appscr69");

}

function appscr74EditUpdateValues(i) {
	//
}

function resetTotalInputsOrSelectboxesforEdit()
{
	$('#Financial_Job_EmployerName_id').val('');
	$('#Financial_Job_HoursOrDays_id').val(0);
	$('#Financial_Job_id').val(0);
	$('#Financial_Job_select_id').val('SelectFrequency');
	$('#Financial_Self_Employment_TypeOfWork').val('');
	$('#Financial_Self_Employment_id').val(0);
	$('#Financial_SocialSecuritybenefits_id').val(0);
	$('#Financial_SocialSecuritybenefits_select_id').val('SelectFrequency');
	$('#Financial_Unemployment_StateGovernment').val('');
	$('#Financial_Unemployment_id').val(0);
	$('#Financial_Unemployment_select_id').val('SelectFrequency');
	$('#Financial_Retirement_pension_id').val(0);
	$('#Financial_Retirement_pension_select_id').val('SelectFrequency');
	$('#Financial_Capitalgains_id').val(0);
	$('#Financial_InvestmentIncome_id').val(0);
	$('#Financial_InvestmentIncome_select_id').val('SelectFrequency');
	$('#Financial_RentalOrRoyaltyIncome_id').val(0);
	$('#Financial_RentalOrRoyaltyIncome_select_id').val('SelectFrequency');
	$('#Financial_FarmingOrFishingIncome_id').val(0);
	$('#Financial_FarmingOrFishingIncome_select_id').val('SelectFrequency');
	$('#Financial_AlimonyReceived_id').val(0);
	$('#Financial_AlimonyReceived_select_id').val('SelectFrequency');
	$('#Financial_OtherTypeIncome').val('');
	$('#Financial_OtherIncome_id').val(0);
	$('#Financial_OtherIncome_select_id').val('SelectFrequency');
	/*$('#expectedIncomeByHouseHoldMember').val(0);*/
	$('#explanationTextArea').val('');
	$('#alimonyAmountInput').val(0);
	$('#alimonyFrequencySelect').val('SelectFrequency');
	$('#studentLoanInterestAmountInput').val(0);
	$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
	$('#deductionTypeInput').val('');
	$('#deductionAmountInput').val(0);
	$('#deductionsFrequencySelect').val('SelectFrequency');
	$('#basedOnAmountInput').val(0);
}

// Additional Questions Editing

function appscr75AEditController(i)
{

	//$('#houseHoldTRNo').val('HouseHold' + i);
	setNonFinancialPageProperty(i);
	setHouseHoldData();
	goToPageById("appscr76P1");
	appscr76FromMongodb();
}


//	Edit functionality Conditions calling form navigation.js

function editFunctionalityConditions()
{

	if(currentPage == "appscr75A")
		totalIncomeShowMethod();

	if(editFlag == true && currentPage == "appscr62Part1")
	{
		$('#back_button_div').hide();
	}

	if(editFlag == true && currentPage == "appscr70")
	{
		$('#back_button_div').hide();
	}

}


var updateTo='';
var flagToUpdateFromResult=false;

/*
 * @returns no of Household
 */
function checkStillUpdateRequired()
{
	updateTo = $('input[name=editRadioGroup]:radio:checked').val();
	flagToUpdateFromResult=false;
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	for(var memberId=1; memberId <= noOfHouseHold; memberId++ )
		{
				var check = $('#'+updateTo+memberId).is(':checked');
				if(check)
				{
					$('#finalEditDiv').hide();
				switch(updateTo)
				{
				case "HouseHoldDetailsUpdate":
					flagToUpdateFromResult = true;
					fetchDataHouseholdUpdate(memberId);
					$('#'+updateTo+memberId).prop('checked', false);
					editMemberNumber=memberId;
					goToPageById("appscr60");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				case "nonFinancialUpdate":
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					appscr67EditController(memberId);
					goToPageById("appscr61");
					editMemberNumber=memberId;
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				case "financialDetailsUpdate":
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					appscr74EditController(memberId);
					editMemberNumber=memberId;
					goToPageById("appscr69");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				}
				break;
			}
		}
	return flagToUpdateFromResult;
}

function fetchDataHouseholdUpdate(targetIndex) {
	var houseHoldIndex = targetIndex;
	getSpouse(houseHoldIndex);
	var martialStatus=$("#appscr57isMarriedTD"+houseHoldIndex).text();
	var spouseID=$("#appscr57spouseIdTD"+houseHoldIndex).text();
	var isPayingJointly=$("#appscr57payJointlyTD"+houseHoldIndex).text();
	var  isTaxFiller=$("#appscr57isTaxFilerTD"+houseHoldIndex).text();
	if(martialStatus.toLowerCase()=="yes"||martialStatus.toLowerCase()=="true")
		{
			$("#marriedIndicatorYes").prop('checked',true);
		}
	else if(martialStatus.toLowerCase()=="no"||martialStatus.toLowerCase()=="false") {
			$("#marriedIndicatorNo").prop('checked',true);
	}
	if(isPayingJointly.toLowerCase()=="yes"||isPayingJointly.toLowerCase()=="true"){
		$("#planOnFilingJointFTRIndicatorYes").prop('checked',true);
	}
	else if(isPayingJointly.toLowerCase()=="no"||isPayingJointly.toLowerCase()=="false") {
		$("#planOnFilingJointFTRIndicatorNo").prop('checked',true);
	}
	if (isTaxFiller.toLowerCase()=="yes"||isTaxFiller.toLowerCase()=="true") {
		$("#planToFileFTRIndicatorYes").prop('checked',true);

	} else if (isTaxFiller.toLowerCase()=="no"||isTaxFiller.toLowerCase()=="false") { {
		$("#planToFileFTRIndicatorNo").prop('checked',true);
	}
	for ( var hNum = 1; hNum <= noOfHouseHold; hNum++) {
		if($("#HouseHold"+hNum).is(":checked")){
			if ($("#HouseHold"+hNum)==spouseID) {
				$("#HouseHold"+hNum).prop('checked',true);
				break;
			}
		}

		}
	}



}
function appscr67Edit(i){
	editNonFinacial = true;
	appscr67EditControllerJSON(i)
}
function appscr67EditControllerJSON(i){
	editMemberNumber = i;
	nonFinancialHouseHoldDone = i;
	//$('#houseHoldTRNo').val('HouseHold' + i);
	saveCurrentApplicantID(i);
	editFlag = false;
	if( i == noOfHouseHoldmember){
		 //editFlag = true;
		 populateDataToRelations();
	}
	if(!haveDataFlag)
	{
		//editFlag = true;
		//$('#back_button_div').hide();
	}

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	var editHouseHoldgender 				= householdMemberObj.gender; //$('#appscrHouseHoldgender57TD'+i).text();
	var editHouseHoldHavingSSN  			= householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator; //$('#appscr57HouseHoldHavingSSNTD'+i).text();
	var editHouseHoldSSN 					= householdMemberObj.socialSecurityCard.socialSecurityNumber; //$('#appscr57HouseHoldSSNTD'+i).text();
	var editsameNameInSSNCardIndicator		= householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator; //$('#appscr57SameNameInSSNCardIndicatorTD'+i).text();
	var editfirstNameOnSSNCard				= householdMemberObj.socialSecurityCard.firstNameOnSSNCard; //$('#appscr57FirstNameOnSSNCardTD'+i).text();
	var editmiddleNameOnSSNCard				= householdMemberObj.socialSecurityCard.middleNameOnSSNCard; //$('#appscr57MiddleNameOnSSNCardTD'+i).text();
	var editlastNameOnSSNCard				= householdMemberObj.socialSecurityCard.lastNameOnSSNCard; //$('#appscr57LastNameOnSSNCardTD'+i).text();
	var editsuffixOnSSNCard					= householdMemberObj.socialSecurityCard.suffixOnSSNCard; //$('#appscr57SuffixOnSSNCardTD'+i).text(); //WE DON'T HAVE SUFFIX IN JSON
	var editnotAvailableSSNReason			= householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN; //$('#appscr57notAvailableSSNReasonTD'+i).text();
	var editUScitizen 						= $('#appscr57UScitizenTD'+i).text(); //WE DON'T HAVE US CITIZEN IN JSON
	var editlivedIntheUSSince1996Indicator 	= householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator; //$('#appscr57livedIntheUSSince1996IndicatorTD'+i).text();
	var editnaturalizedCitizenshipIndicator	= householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator; //$('#appscr57naturalizedCitizenshipIndicatorTD'+i).text();
	var editcertificateType 				= $('#appscr57certificateTypeIndicatorTD'+i).text(); //NO DATA FOUND IN JSON
	var editcertificateAlignNumber 			= householdMemberObj.citizenshipImmigrationStatus.naturalizationCertificateAlienNumber; //$('#appscr57naturalizedCitizenshipAlignNumberTD'+i).text();
	var editcertificateNumber 				= householdMemberObj.citizenshipImmigrationStatus.naturalizationCertificateNaturalizationNumber; //$('#appscr57certificateNumberTD'+i).text();
	var editaliegnNumber 					= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber; //$('#appscr57alienNoTD'+i).text();
	var editi94Number 						= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number; //$('#appscr57i94NoTD'+i).text();
	var editForeignPassportNumber 			= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber; //$('#appscr57foreignPassNoTD'+i).text();
	var editForeignPassportCountry			= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance; //$('#appscr57foreignPassCountryTD'+i).text();
	var editForeignPassportExpiration		= $('#appscr57foreignPassExpirationTD'+i).text(); //NO DATA FOUND IN JSON
	var editSevisID 						= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.sevisId; //$('#appscr57sevisIDTD'+i).text();
	var editdocumentSameNameIndicator 		= $('#appscr57documentSameNameIndicatorTD'+i).text(); //NO DATA FOUND IN JSON
	var editdocumentFirstName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName; //$('#appscr57documentFirstNameTD'+i).text();
	var editdocumentLastName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName; //$('#appscr57documentMiddleNameTD'+i).text();
	var editdocumentMiddleName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName; //$('#appscr57documentLastNameTD'+i).text();
	var editdocumentSuffix 					= $('#appscr57documentSuffixTD'+i).text(); //NO DATA FOUND IN JSON
	var editethnicityIndicator				=  householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator;// $('#appscr57EthnicityIndicatorTD'+i).text();
	temp = householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator;
	if(temp == "true" || temp == true){
		$('#fnlnsExemptionIndicatorYes').prop("checked", true);
	} else if(temp == "false" || temp == false) {
		$('#fnlnsExemptionIndicatorNo').prop("checked", true);
	}
	$('#exemptionId').val(householdMemberObj.socialSecurityCard.individualManadateExceptionID);	
	/*var ssn1 = editHouseHoldSSN.split("-")[0];
	var ssn2 = editHouseHoldSSN.split("-")[1];
	var ssn3 = editHouseHoldSSN.split("-")[2];*/
	var houseHoldname = householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName; //$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	resetAddNonFinancialDetails();
	$('.nameOfHouseHold').text(houseHoldname);
	if(editHouseHoldgender != "" && editHouseHoldgender != undefined){
		if(editHouseHoldgender.toLowerCase() == 'male')
		{
			$('#appscr61GenderMaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
		else if(editHouseHoldgender.toLowerCase() == 'female')
		{
			$('#appscr61GenderFemaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
	}
	if(editHouseHoldHavingSSN == 'yes' || editHouseHoldHavingSSN == 'true' || editHouseHoldHavingSSN == true )
	{
		$('#socialSecurityCardHolderIndicatorYes').prop('checked',true);
		askForSSNNoPage61();
	}
	else if(editHouseHoldHavingSSN == 'no' || editHouseHoldHavingSSN == 'false' || editHouseHoldHavingSSN === false )
	{
		$('#socialSecurityCardHolderIndicatorNo').prop('checked',true);

		validation('appscr61');
		askForSSNNoPage61();
	}

	/*$('#ssn1').val(ssn1);
	$('#ssn2').val(ssn2);
	$('#ssn3').val(ssn3);*/
	//if 2nd applicant changes ssn and click back, data-ssn is updated due to focusout, then user goes back to 1st applicant, and continue without edit ssn. 1st applicant's ssn will be updated to data-ssn. 
	if(editHouseHoldSSN){
		$("#ssn").attr('data-ssn',editHouseHoldSSN).val("***-**-" + editHouseHoldSSN.substring(5));
	}
	
	if(editsameNameInSSNCardIndicator == "yes" || editsameNameInSSNCardIndicator == 'true' || editsameNameInSSNCardIndicator == true )
		$('#fnlnsSameIndicatorYes').prop("checked", true);
	else if(editsameNameInSSNCardIndicator == "no" || editsameNameInSSNCardIndicator == 'false' || editsameNameInSSNCardIndicator === false )
	{

		$('#fnlnsSameIndicatorNo').prop("checked", true);
		$('#firstNameOnSSNCard').val(editfirstNameOnSSNCard);
		$('#middleNameOnSSNCard').val(editmiddleNameOnSSNCard);
		$('#lastNameOnSSNCard').val(editlastNameOnSSNCard);
		$('#suffixOnSSNCard').val(editsuffixOnSSNCard);
	}

	hasSameSSNNumberP61();

	if(editnotAvailableSSNReason == 0)
		$('#reasonableExplanationForNoSSN').val('');
	else
		$('#reasonableExplanationForNoSSN').val(editnotAvailableSSNReason);

	if(editUScitizen == 'no')
	{
		$('#UScitizenIndicatorNo').prop('checked',true);
		isUSCitizen();
	}
	else if(editUScitizen == 'yes')
	{
		$('#UScitizenIndicatorYes').prop('checked',true);
		isUSCitizen();
	}

	if(editlivedIntheUSSince1996Indicator == 'yes')
		$('#62Part_livedIntheUSSince1996IndicatorYes').prop('checked',true);
	else
		$('#62Part_livedIntheUSSince1996Indicator').prop('checked',true);
	$('input[name=naturalizedCitizenshipIndicator]:radio:checked').prop("checked", false);
	if(editnaturalizedCitizenshipIndicator == true){
		$('#naturalizedCitizenshipIndicatorYes').prop('checked',true);		
	}else if(editnaturalizedCitizenshipIndicator == false){
		$('#naturalizedCitizenshipIndicatorNo').prop('checked',true);		
	}

	if(editcertificateType == 'CitizenshipCertificate')
	{
		$('#naturalizedCitizenNaturalizedIndicator2').prop('checked',true);
		$('#citizenshipAlignNumber').val(editcertificateAlignNumber);
		$('#appscr62p1citizenshipCertificateNumber').val(editcertificateNumber);

		$(".appscr62Part1HideandShowFillingDetails").eq(1).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
	}
	else
	{
		$('#naturalizedCitizenNaturalizedIndicator').prop('checked',true);
		$('#naturalizationAlignNumber').val(editcertificateAlignNumber);
		$('#naturalizationCertificateNumber').val(editcertificateNumber);

		$(".appscr62Part1HideandShowFillingDetails").eq(0).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();
	}

	$('#naturalCitizen_eligibleImmigrationStatus').prop('checked');


	if(editaliegnNumber!=" "){
		$('#DocAlignNumber').val(editaliegnNumber);
	}
	if(editi94Number!=" "){
		$('#DocI-94Number').val(editi94Number);
	}
	if(editForeignPassportNumber!=" "){
		$('#passportDocNumber').val(editForeignPassportNumber);
	}
	if(editForeignPassportCountry!="0"){
		$('#appscr62P1County').val(editForeignPassportCountry);
	}
	if(editForeignPassportExpiration!="0"){
		$('#passportExpDate').val(editForeignPassportExpiration);
	}
	if(editSevisID!="0"){
		$('#sevisIDNumber').val(editSevisID);
	}
	if(editdocumentSameNameIndicator == 'no')
	{
		$('#62Part2_UScitizen').prop('checked',true);
		$('#documentFirstName').val(editdocumentFirstName);
		$('#documentMiddleName').val(editdocumentMiddleName);
		$('#documentLastName').val(editdocumentLastName);
		$('#documentSuffix').val(editdocumentSuffix);

		displayOrHideSameNameInfoPage62();
	}
	else
	{
		$('#62Part2_UScitizenYes').prop('checked',true);
		$('#documentFirstName').val('');
		$('#documentMiddleName').val('');
		$('#documentLastName').val('');
		$('#documentSuffix').val('0');

		displayOrHideSameNameInfoPage62();
	}
	
	if(editethnicityIndicator == true){
		$('#checkPersonNameLanguageYes').prop("checked", true);
		
		for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.ethnicity.length; cnt++){
			$('input:checkbox[name="ethnicity"]').each(function(){
				if($(this).val() == householdMemberObj.ethnicityAndRace.ethnicity[cnt].code){
					
					$(this).prop('checked',true);
					//for other ethnicity
					if($(this).val() == '0000-0'){						
						$('#otherEthnicity').val(householdMemberObj.ethnicityAndRace.ethnicity[cnt].label);
					}
				}
			});
		}
		
	}else if(editethnicityIndicator == false){
		$('#checkPersonNameLanguageNo').prop("checked", true);
	}
	displayOtherEthnicity();	
	checkLanguage();
	
	for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.race.length; cnt++){
		$('input:checkbox[name="race"]').each(function(){
			if($(this).val() == householdMemberObj.ethnicityAndRace.race[cnt].code){
				
				$(this).prop('checked',true);
				//for other race
				if($(this).val() == '2131-1'){
					$('#otherRace').val(householdMemberObj.ethnicityAndRace.race[cnt].label);
				}
			}
		});
	}
	displayOtherRace();
	if(!haveDataFlag)
		goToPageById('appscr61');
}

function appscr74EditControllerJSON(i)
{
	editMemberNumber = i;
	if(editMemberNumber =="" || editMemberNumber < 0){
		return;
	}
	//$('#houseHoldTRNo').val("HouseHold"+i);
	saveCurrentApplicantID(i);
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[editMemberNumber-1];

	if(!haveDataFlag)
	{
		editFlag = true;
		//$('#back_button_div').hide();
	}

	//$('input[name=appscr70radios4Income]:radio:checked').val()

	var editIrsIncome 							  = $('#appscrirsIncome57TD'+i).text();
	var editIncomesStatus 						  = $('#incomesStatus'+i).text();
	var editJobIncome 							  = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	var editJobFrequency 						  = householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency;
	var editEmployerName						  = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	var editHoursOrDays							  = householdMemberObj.detailedIncome.jobIncome[0].workHours;
	var editSelfEmploymentIncome 				  = householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome;
	var editEmploymentTypeOfWork				  = householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork;
	var editSocialSecurityBenefitsIncome 		  = householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount;
	var editSocialSecurityBenefitsIncomeFrequency = householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency;
	var editUnemploymentIncome 					  = householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount;
	var editUnemploymentIncomeFrequency 		  = householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency;
	var editUnemploymentStateGovernment			  = householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName;
	var editunemploymentIncomeDate				  = UIDateformat(householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate);
	var editRetirementPensionIncome 			  = householdMemberObj.detailedIncome.retirementOrPension.taxableAmount;
	var editRetirementPensionIncomeFrequency 	  = householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency;
	var editCapitalgainsIncome 					  = householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains;
	var editInvestmentIncome 					  = householdMemberObj.detailedIncome.investmentIncome.incomeAmount;
	var editInvestmentIncomeFrequency 			  = householdMemberObj.detailedIncome.investmentIncome.incomeFrequency;
	var editRentalOrRoyaltyIncome 				  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount;
	var editRentalOrRoyaltyIncomeFrequency 		  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency;
	var editFarmingOrFishingIncome 				  = householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount;
	var editFarmingOrFishingIncomeFrequency 	  = householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency;
	var editAlimonyReceivedIncome 				  = householdMemberObj.detailedIncome.alimonyReceived.amountReceived;
	var editAlimonyReceivedIncomeFrequency 		  = householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	var editOtherTypeIncome						  = $('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text();

	//var editExpectedIncome 						  = $('#appscrexpectedIncome57TD'+i).text();
	//var editIrsHouseHoldExplanationTextArea 	  = $('#appscr57HouseHoldExplanationTextAreaTD'+i).text();

	var editHouseHoldalimonyAmount 				  = householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount;
	var editHouseHoldalimonyFrequency 			  = householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency;
	var editHouseHoldstudentLoanAmount 			  = householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount;
	var editHouseHoldstudentLoanFrequency 		  = householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency;
	var editHouseHolddeductionsType 			  = householdMemberObj.detailedIncome.deductions.deductionType[2];
	var editHouseHolddeductionsAmount 			  = householdMemberObj.detailedIncome.deductions.otherDeductionAmount;
	var editHouseHolddeductionsFrequency 		  = householdMemberObj.detailedIncome.deductions.otherDeductionFrequency;
	var editHouseHoldbasedOnInput 				  = $('#appscr57HouseHoldbasedOnInputTD'+i).text();
	var editstopWorking							  = householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator;;
	var editworkAt								  =	householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator;
	var editdecresedAt							  = householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator;
	var editsalaryAt							  = householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator;
	var editsesWorker							  = householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator;	
	var editotherJointIncome					  = householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator;
	var incomeStatusFlag;

	resetPage69Values();
	resetPage71Values();
	resetPage72DeductionValues();

	var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	//var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();
	  $('.nameOfHouseHold').text(houseHoldname);
	  job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;	
	  $('.appscr73EmployerName').html(job_income_name);

	  if(currentPage == "appscr68" || currentPage == "appscr69"){
		  appscr69JSON(editMemberNumber);
		  //$('#expeditedIncome69').val(householdMemberObj.expeditedIncome.irsReportedAnnualIncome);
	  }
	  editIncomesStatus = "Yes";
	//if(editIncomesStatus == "Yes" || editIncomesStatus == "yes")
	if(currentPage == "appscr70" || currentPage == "appscr72")
	{

		$('#appscr70radios4IncomeYes').prop("checked",true);
		//selectIncomesCheckBoxes();

		//if(editJobFrequency != "SelectFrequency")
		if(editJobIncome == null || editJobIncome == "")
		{
			$('#Financial_Job_EmployerName_id').val('');
			$('#Financial_Job_HoursOrDays_id').val('');
			if( $('#Financial_Job').is(':checked') == false)
				$('#Financial_Job_Div').hide();
			else
				$('#Financial_Job_Div').show();			

		}
		else
		{
			incomeStatusFlag = true;

			//document.getElementById('Financial_Job').checked = true;
			$('#Financial_Job_EmployerName_id').val(editEmployerName);
			$('#Financial_Job_HoursOrDays_id').val(editHoursOrDays);
			$('#Financial_Job_id').val(Moneyformat(editJobIncome));
			$('#Financial_Job_select_id').val(editJobFrequency);
			showDiductionsFields();

		}
		//if(editSelfEmploymentIncome != "0" && editSelfEmploymentIncome != "0.0")
		if(editSelfEmploymentIncome == null || editSelfEmploymentIncome == "" )
		{
			//document.getElementById('Financial_Self_Employment').checked = false;
			$('#Financial_Self_Employment_TypeOfWork').val('');
			$('#Financial_Self_Employment_Div').hide();
			if( $('#Financial_Self_Employment').is(':checked') == false)
				$('#Financial_Self_Employment_Div').hide();
			else
				$('#Financial_Self_Employment_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Self_Employment').checked = true;
			$('#Financial_Self_Employment_TypeOfWork').val(editEmploymentTypeOfWork);
			$('#Financial_Self_Employment_id').val(Moneyformat(editSelfEmploymentIncome));

		}
		//if(editSocialSecurityBenefitsIncomeFrequency != "SelectFrequency")
		if(editSocialSecurityBenefitsIncome == null || editSocialSecurityBenefitsIncome == "")
		{
			//document.getElementById('Financial_SocialSecuritybenefits').checked = false;
			$('#Financial_SocialSecuritybenefits_Div').hide();
			if( $('#Financial_SocialSecuritybenefits').is(':checked') == false)
				$('#Financial_SocialSecuritybenefits_Div').hide();
			else
				$('#Financial_SocialSecuritybenefits_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_SocialSecuritybenefits').checked = true;
			$('#Financial_SocialSecuritybenefits_id').val(Moneyformat(editSocialSecurityBenefitsIncome));
			$('#Financial_SocialSecuritybenefits_select_id').val(editSocialSecurityBenefitsIncomeFrequency);
		}

		//if(editUnemploymentIncomeFrequency != "SelectFrequency")
		if(editUnemploymentIncome == null || editUnemploymentIncome == "")
		{

			//document.getElementById('Financial_Unemployment').checked = false;
			$('#Financial_Unemployment_StateGovernment').val('');
			$('#unemploymentIncomeDate').val('');
			if( $('#Financial_Unemployment').is(':checked') == false)
				$('#Financial_Unemployment_Div').hide();
			else
				$('#Financial_Unemployment_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Unemployment').checked = true;
			$('#Financial_Unemployment_StateGovernment').val(editUnemploymentStateGovernment);
			$('#Financial_Unemployment_id').val(Moneyformat(editUnemploymentIncome));
			$('#unemploymentIncomeDate').val(editunemploymentIncomeDate);
			$('#Financial_Unemployment_select_id').val(editUnemploymentIncomeFrequency);
		}

		//if(editRetirementPensionIncomeFrequency != "SelectFrequency")
		if(editRetirementPensionIncome == null || editRetirementPensionIncome == "")
		{

			//document.getElementById('Financial_Retirement_pension').checked = false;
			if( $('#Financial_Retirement_pension').is(':checked') == false)
				$('#Financial_Retirement_pension_Div').hide();
			else
				$('#Financial_Retirement_pension_Div').show();

		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Retirement_pension').checked = true;
			$('#Financial_Retirement_pension_id').val(Moneyformat(editRetirementPensionIncome));
			$('#Financial_Retirement_pension_select_id').val(editRetirementPensionIncomeFrequency);
		}

		//if(editCapitalgainsIncome != "0" && editCapitalgainsIncome != "0.0")
		if(editCapitalgainsIncome == null || editCapitalgainsIncome == "")
		{
			//document.getElementById('Financial_Capitalgains').checked = false;
			if( $('#Financial_Capitalgains').is(':checked') == false)
				$('#Financial_Capitalgains_Div').hide();
			else
				$('#Financial_Capitalgains_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Capitalgains').checked = true;
			$('#Financial_Capitalgains_id').val(Moneyformat(editCapitalgainsIncome));
		}

		//if(editInvestmentIncomeFrequency != "SelectFrequency")
		if(editInvestmentIncome == null || editInvestmentIncome == "")
		{
			//document.getElementById('Financial_InvestmentIncome').checked = false;
			if( $('#Financial_InvestmentIncome').is(':checked') == false)
				$('#Financial_InvestmentIncome_Div').hide();
			else
				$('#Financial_InvestmentIncome_Div').show();

		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_InvestmentIncome').checked = true;
			$('#Financial_InvestmentIncome_id').val(Moneyformat(editInvestmentIncome));
			$('#Financial_InvestmentIncome_select_id').val(editInvestmentIncomeFrequency);
		}

		//if(editRentalOrRoyaltyIncomeFrequency != "SelectFrequency")
		if(editRentalOrRoyaltyIncome == null || editRentalOrRoyaltyIncome == "")
		{
			//document.getElementById('Financial_RentalOrRoyaltyIncome').checked = false;
			if( $('#Financial_RentalOrRoyaltyIncome').is(':checked') == false)
				$('#Financial_RentalOrRoyaltyIncome_Div').hide();
			else
				$('#Financial_RentalOrRoyaltyIncome_Div').show();
		}
		else
		{

			incomeStatusFlag = true;
			//document.getElementById('Financial_RentalOrRoyaltyIncome').checked = true;
			$('#Financial_RentalOrRoyaltyIncome_id').val(Moneyformat(editRentalOrRoyaltyIncome));
			$('#Financial_RentalOrRoyaltyIncome_select_id').val(editRentalOrRoyaltyIncomeFrequency);
		}

		//if(editFarmingOrFishingIncomeFrequency != "SelectFrequency")
		if(editFarmingOrFishingIncome == null || editFarmingOrFishingIncome == "")
		{
			//document.getElementById('Financial_FarmingOrFishingIncome').checked = false;
			$('#Financial_FarmingOrFishingIncome_Div').hide();
			if( $('#Financial_FarmingOrFishingIncome').is(':checked') == false)
				$('#Financial_FarmingOrFishingIncome_Div').hide();
			else
				$('#Financial_FarmingOrFishingIncome_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_FarmingOrFishingIncome').checked = true;
			$('#Financial_FarmingOrFishingIncome_id').val(Moneyformat(editFarmingOrFishingIncome));
			$('#Financial_FarmingOrFishingIncome_select_id').val(editFarmingOrFishingIncomeFrequency);
		}

		//if(editAlimonyReceivedIncomeFrequency != "SelectFrequency")
		if(editAlimonyReceivedIncome == null || editAlimonyReceivedIncome == "")
		{
			//document.getElementById('Financial_AlimonyReceived').checked = false;
			if( $('#Financial_AlimonyReceived').is(':checked') == false)
				$('#Financial_AlimonyReceived_Div').hide();
			else
				$('#Financial_AlimonyReceived_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_AlimonyReceived').checked = true;
			$('#Financial_AlimonyReceived_id').val(Moneyformat(editAlimonyReceivedIncome));
			$('#Financial_AlimonyReceived_select_id').val(editAlimonyReceivedIncomeFrequency);
		}

		//var otherincome = false;
		$('#Financial_OtherIncome_Div').hide();
		if (householdMemberObj.detailedIncome.otherIncomeIndicator == true){
			$('#Financial_OtherIncome_Div').show();			
		}
		$('#appscr71CanceleddebtsCB').prop("checked", false);
		deductionCheck('appscr71CanceleddebtsCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[0].incomeAmount == "")){
			$('#appscr71CanceleddebtsCB').prop("checked", true);
			$('#cancelled_Debts_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount));
			$('#cancelledDebtsFrequency').val(householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71CanceleddebtsCB');
		}
		$('#appscr71CourtAwardsCB').prop("checked", false);
		deductionCheck('appscr71CourtAwardsCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[1].incomeAmount == "")){
			$('#appscr71CourtAwardsCB').prop("checked", true);
			$('#court_awards_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount));
			$('#courtAwrdsFrequency').val(householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71CourtAwardsCB');
		}
		$('#appscr71JuryDutyPayCB').prop("checked", false);
		deductionCheck('appscr71JuryDutyPayCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[2].incomeAmount == "")){
			$('#appscr71JuryDutyPayCB').prop("checked", true);
			$('#jury_duty_pay_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount));
			$('#juryDutyPayFrequency').val(householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71JuryDutyPayCB');
		}

	}
	else
	{
		//$('#appscr70radios4IncomeNo').prop("checked",true);
		//selectIncomesCheckBoxes();

		//hideDiductionsFields();
	}

	if(incomeStatusFlag == false)
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}	
	
	resetPage72DeductionValues();
	$('#appscr72alimonyPaid').prop("checked",false);
	$('#appscr72alimonyPaidTR').hide();
	if(editHouseHoldalimonyAmount != "" && editHouseHoldalimonyAmount != null)
	{
		$('#appscr72alimonyPaid').prop("checked",true);
		//document.getElementById('appscr72alimonyPaid').checked = true;
		$('#alimonyAmountInput').val(Moneyformat(editHouseHoldalimonyAmount));
		$('#alimonyFrequencySelect').val(editHouseHoldalimonyFrequency);
		$('#appscr72alimonyPaidTR').show();
	}
	$('#appscr72studentLoanInterest').prop("checked",false);
	$('#appscr72studentLoanInterestTR').hide();
	if(editHouseHoldstudentLoanAmount != "" && editHouseHoldstudentLoanAmount != null)
	{
		$('#appscr72studentLoanInterest').prop("checked",true);
		//document.getElementById('appscr72studentLoanInterest').checked = true;
		$('#studentLoanInterestAmountInput').val(Moneyformat(editHouseHoldstudentLoanAmount));
		$('#studentLoanInterestFrequencySelect').val(editHouseHoldstudentLoanFrequency);
		$('#appscr72studentLoanInterestTR').show();
	}
	$('#appscr72otherDeduction').prop("checked",false);
	$('#appscr72otherDeductionTR').hide();
	if(editHouseHolddeductionsAmount != "" && editHouseHolddeductionsAmount != null)
	{
		$('#appscr72otherDeduction').prop("checked",true);
		//document.getElementById('appscr72otherDeduction').checked = true;
		$('#deductionTypeInput').val(editHouseHolddeductionsType);
		$('#deductionAmountInput').val(Moneyformat(editHouseHolddeductionsAmount));
		$('#deductionsFrequencySelect').val(editHouseHolddeductionsFrequency);
		$('#appscr72otherDeductionTR').show();
	}
	
	if(householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator == true){
		$('#aboutIncomeSppedYes').prop("checked",true);
		appscr72incomeHighOrLow();
	} else if(householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator == false){
		$('#aboutIncomeSppedNo').prop("checked",true);
		appscr72incomeHighOrLow();
	}
	
	if(householdMemberObj.expeditedIncome.expectedIncomeVaration == 'higher'){
		$('#incomeVariationHigh').prop("checked",true);
	} else if(householdMemberObj.expeditedIncome.expectedIncomeVaration == 'lower'){
		$('#incomeVariationLow').prop("checked",true);
	}

	
	if(editHouseHoldbasedOnInput !="0" && editHouseHoldbasedOnInput != "0.0")
	{
		$('#basedOnAmountInput').val(editHouseHoldbasedOnInput);
	}
	if(editdecresedAt == "Yes" || editdecresedAt == "yes"){
		$('#appscr73DecresedAtYes').prop("checked",true);
	}
	else{
		$('#appscr73DecresedAtNo').prop("checked",true);
	}

	if(editstopWorking == true){
		$('#appscr73stopWorkingIndicatorYes').prop("checked",true);
	}
	else if(editstopWorking == false) {
		$('#appscr73stopWorkingIndicatorNo').prop("checked",true);
	}

	if(editsalaryAt == true){
		$('#appscr73SalaryAtYes').prop("checked",true);
	}else if(editsalaryAt == false){
		$('#appscr73SalaryAtNo').prop("checked",true);
	}
	if(editsesWorker == true){
		$('#appscr73SesWorkerYes').prop("checked",true);
	}else if(editsesWorker == false){
		$('#appscr73SesWorkerNo').prop("checked",true);
	}
	if(editotherJointIncome == true){
		$('#appscr73OtherJointIncomeYes').prop("checked",true);
	}else if(editotherJointIncome == false){
		$('#appscr73OtherJointIncomeNo').prop("checked",true);
	}
	$('#explanationTextArea').val(householdMemberObj.detailedIncome.discrepancies.explanationForJobIncomeDiscrepancy);
	$('#appscr73ExplanationTaxtArea').val(householdMemberObj.detailedIncome.discrepancies.explanationForDependantDiscrepancy);


}

function appscr69JSON(i){
	editMemberNumber = i;
	if(i =="" || i < 0){
		return;
	}
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	$('#expeditedIncome69').val(Moneyformat(householdMemberObj.expeditedIncome.irsReportedAnnualIncome));
 }

function updatePrimaryContact(){
	$('#firstName').val($('#appscr57FirstName1').val());
	$('#middleName').val($('#appscr57MiddleName1').val());
	$('#lastName').val($('#appscr57LastName1').val());
	$('#appscr53Suffix').val($('#appscr57Suffix1').val());
	$('#dateOfBirth').val($('#appscr57DOB1').val());

	$('#appscr57FirstNameTD1').text($('#firstName').val());
	$('#appscr57MiddleNameTD1').text($('#middleName').val());
	$('#appscr57LastNameTD1').text($('#lastName').val());
	$('#appscr57SuffixTD1').text($('#appscr53Suffix').val());
	$('#appscr57DOBTD1').text($('#dateOfBirth').val());

	var name= $('#firstName').val() + " " + $('#middleName').val() + " " + $('#lastName').val();

	$('#houseHold_Contact_span1').html(name);
	$('#houseHold_Contact_span2').html(name);
	$('#houseHold_Contact_span3').html(name);
}

function editFinancialSectionPerson(personId) {
	editData = true;
	if(editData == true) {
		$('#prevBtn').hide();
	}


	var hosueholdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	var personObject = null;
	for(var i=0; i<hosueholdSize; i++) {
		var person = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
		if(person.personId == personId) {
			currentPersonIndex = i;
			personObject = person;
			//break;
		}
	}

	if(personObject == null) {
		return false;
	}
	//var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[1];
	$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);
	var lastName = personObject.name.lastName;
	$('.nameOfHouseHold').text(lastName);

	$('#' + currentPage).hide();
	pageId = "appscr69";

	$('#' + pageId).show();

	currentPage = pageId;
}

function updateFullNameOfOtherChild(){
	$(".fullNameOfOtherChild").text($("#parent-or-caretaker-firstName").val() + " " + $("#parent-or-caretaker-middleName").val() + " " + $("#parent-or-caretaker-lastName").val());
}

function updateLiveWithChildUnderAgeView(){
	$("#liveWithChildUnderAgeView").html("");

	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		var dobArr =UIDateformat(householdMemberObj.dateOfBirth);
		if(ageUnder19(dobArr)){
			fullName = householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;

			$("#liveWithChildUnderAgeView").append('<label><input type="checkbox" name="takeCareOf" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#takeCareOf_anotherChildErrorDiv" />'+ fullName +'</label>');
		}
	}

	$("#liveWithChildUnderAgeView").append('<label><input type="checkbox" id="takeCareOf_anotherChild" name="takeCareOf" value="Another child" onclick="method4AnotherChildCheckBox();" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#takeCareOf_anotherChildErrorDiv" />'+jQuery.i18n.prop("ssap.page13.anotherChild")+'</label><div id="takeCareOf_anotherChildErrorDiv"></div>');

}

function hideParentOrCaretaker(){
	//var currentApplicantID = ($('#houseHoldTRNo').val()).substring(9);
	var currentApplicantID = getCurrentApplicantID();
	//var currentApplicantDOB = $("#HouseHold"+currentApplicantID).find(".dob").text();
	var currentApplicantDOB = UIDateformat(webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentApplicantID-1].dateOfBirth);
	if(ageUnder19(currentApplicantDOB)){
		return true;
	}else{
		return false;
	}


}

function ageUnder19(dateOfBirth){
    var birthDate = new Date(dateOfBirth);
    var effectiveDate = getEffectiveDate();;

    var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

    if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
        years--;
    }

    if(years >=19){
    	return false;
    }else{
    	return true;
    }
 }

function editFinancialSectionPerson(personId) {
	editData = true;
	if(editData == true) {
		$('#prevBtn').hide();
	}
}

function maskSSN(){
	var pattern = /^(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$/;
	var ssnValue = $('#ssn').val().replace(/-/g,'');
	if(pattern.test(ssnValue)){
		$('#ssn').closest('div.controls').find('.errorsWrapper').hide();
		$('#ssn').attr('data-ssn',ssnValue).val("***-**-" + ssnValue.substring(5));
	}
}

function ageGreaterThan26(dateOfBirth){
    var birthDate = new Date(dateOfBirth);
    
    var effectiveDate = getEffectiveDate();
    
    var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

    if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
        years--;
    }

    if(years >=26){
    	return true;
    }else{
    	return false;
    }
 }

function coverageMinimumCheckWithUnder26Indicator(){
	var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	for(var i = 0; i < householdMemberArr.length; i++){
		if(householdMemberArr[i].applyingForCoverageIndicator == true && householdMemberArr[i].under26Indicator == true){
			return true;
		};
	};
	
	return false;
	
}

function coverageMinimumCheck(){
	var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	for(var i = 0; i < householdMemberArr.length; i++){
		if(householdMemberArr[i].applyingForCoverageIndicator == true){
			return true;
		};
	};
	
	return false;
	
}


function addJob(){
	var newJob = '';
	
	//var jobArrLength = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[financialHouseHoldDone-1].detailedIncome.jobIncome.length;
	
	newJob += '<div class="jobType"><hr><div class="gutter10">';
		newJob += '<strong class="pull-right margin5-r"><a id="removeJob" class="btn" onclick="removeJob($(this));">'+jQuery.i18n.prop('ssap.page21.removeJob')+'</a></strong>';
		
		newJob += '<div class="control-group">';
			newJob += '<label class="control-label">'+jQuery.i18n.prop('ssap.page21.employerName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></label>';				
			newJob += '<div class="controls">';
				newJob += '<input type="text" placeholder="Employer Name" class="input-medium income_employerName"  data-parsley-length="[8, 256]" data-parsley-length-message="'+jQuery.i18n.prop('ssap.page21.employerNameValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.employerNameRequired')+'"/>';		
			newJob += '</div>';
		newJob += '</div>';
	
		newJob += '<div class="control-group">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howMuch')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.paid')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<label class="control-label">'+jQuery.i18n.prop('ssap.page21.amount')+' <strong>$</strong></label>';				
			newJob += '<div class="controls">';
				newJob += '<input name="" type="text" placeholder="Dollar Amount" class="input-medium currency income_amount" data-parsley-pattern="^[0-9\\,\\.]{1,18}$" data-parsley-pattern-message="'+jQuery.i18n.prop('ssap.page21.amountValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.amountRequired')+'" />';		
				newJob += '</div>';
		newJob += '</div>';
		
		newJob += '<div class="control-group">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howOften')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.paidAmount')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<div class="controls">';
				newJob += '<select class="input-large income_frequency" onchange="howOftenGetThisAmount($(this))" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.required')+'">';
					newJob += '<option value="">'+jQuery.i18n.prop('ssap.page21.selectFrequency')+'</option>';
					newJob += '<option value="Hourly">'+jQuery.i18n.prop('ssap.page21.hourly')+'</option>';
					newJob += '<option value="Daily">'+jQuery.i18n.prop('ssap.page21.daily')+'</option>';
					newJob += '<option value="Weekly">'+jQuery.i18n.prop('ssap.page21.weekly')+'</option>';
					newJob += '<option value="EveryTwoWeeks">'+jQuery.i18n.prop('ssap.page21.biWeekly')+'</option>';
					newJob += '<option value="TwiceAMonth">'+jQuery.i18n.prop('ssap.page21.twiceMonth')+'</option>';
					newJob += '<option value="Monthly">'+jQuery.i18n.prop('ssap.page21.monthly')+'</option>';
					newJob += '<option value="Yearly">'+jQuery.i18n.prop('ssap.page21.yearly')+'</option>';
					newJob += '<option value="OneTimeOnly">'+jQuery.i18n.prop('ssap.page21.oneTime')+'</option>';
				newJob += '</select>';
			newJob += '</div>';
		newJob += '</div>';
		
		newJob += '<div class="control-group hide income_frequencyDiv">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howMuch')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.workPerWeek')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<div class="controls">';
				newJob += '<input type="text" placeholder="Hours Or Days per week" class="input-medium income_hoursOrDays" data-parsley-pattern-message="'+jQuery.i18n.prop('ssap.page21.workPerWeekValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.required')+'"/>';		
			newJob += '</div>';
		newJob += '</div>';
	
	newJob += '</div></div>';
	
	$('#moreJobDiv').append(newJob);
	
	$('.currency').mask('000,000,000,000.00', {reverse: true});
}




function removeJob(jobDiv){
	jobDiv.closest('div.jobType').remove();
}

function getEffectiveDate(){
	var applicationDate = new Date();
	var applicationDayOfMonth = applicationDate.getDate();


	var effectiveMonth = applicationDate.getMonth();
	var effectiveYear = applicationDate.getFullYear();
	var effectiveDate;

	var openEnrollmentDate = new Date('11/15/2014'); 
	
	if(applicationDate < openEnrollmentDate){
	    effectiveDate = new Date('01/01/2015'); 
	}else{
	    if(applicationDayOfMonth < 16){
	        //getMonth starts from 0
	        effectiveMonth += 2;
	    }else{
	        effectiveMonth += 3;
	    }
	    
	    if(effectiveMonth > 12){
	        effectiveMonth -= 12;
	        effectiveYear++;
	    }
	    
	    effectiveDate = new Date(effectiveMonth + "/01/" + effectiveYear);
	}
	
	
	return effectiveDate;

}

function processApplicantDeletion(applicantGuidToBeRemoved, personId) {
	$('#ssapSubmitModal').modal();
	$.ajax({
		type : 'POST',
		url : theUrlFordeleteApplicant,
		dataType : 'json',
		data : {
			currentPage : currentPage,
			webJson:JSON.stringify(webJson),
			applicantGuidToBeRemoved:applicantGuidToBeRemoved,
			personId:personId 
		},
		success : function(msg,xhr) {
			if(isInvalidCSRFToken(msg))	{
				applicantGuidToBeRemoved = "";				
				personIdToBeRemoved = "";
				return;			
			}
			webJson = msg;
			afterDeleteApplicant();
			$('#ssapSubmitModal').modal('hide');
		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {	
			$('#ssapSubmitModal').modal('hide');
			applicantGuidToBeRemoved ="";
			personIdToBeRemoved = "";
			alert("Please try Again !");		
		}
	});
}