
////////////////////////////////////
////      Custom Functions     ////
//////////////////////////////////

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

//////jQuery functions///////

//slides a div left while fading it to an opacity of 0, and then 
//hiding it via display:none;
$.fn.slideLeft = function (duration, callback) {
    var that = $(this[0]);
    //wrap in an inner and wrap the inner in an outer
    that.wrap('<div id="slideLeft-inner" />');
    $("#slideLeft-inner").wrap('<div id="slideLeft-outer" />');
    //give an overflow:hidden and animate a shift of the margin left
    //the length is determined by the width of the parent
    $("#slideLeft-outer").css("overflow", "hidden");
    var shiftLeft = "-" + (parseInt($("#slideLeft-outer").parent().width()) + 10) + "px";
    $("#slideLeft-inner").animate({
        "margin-left": shiftLeft,
            "opacity": "0"
    }, duration, function () {
        //display:none, unwrap the two outer divs
        that.css("display", "none");
        that.unwrap();
        that.unwrap();
        callback();
    });
};
//slides a div right while fading from  an opacity of 0 to an opacity of 1 
//works the opposite of slideLeft
$.fn.slideRight = function (duration, callback) {
    var that = $(this[0]);
    that.wrap('<div id="slideLeft-inner" />');
    $("#slideLeft-inner").wrap('<div id="slideLeft-outer" />');
    $("#slideLeft-outer").css("overflow", "hidden");
    var shiftRight = "-" + (parseInt($("#slideLeft-outer").parent().width()) + 10) + "px";
    $("#slideLeft-inner").css("margin-left", shiftRight);
    $("#slideLeft-inner").css("opacity", "0");
    that.css("display", "block");
    $("#slideLeft-inner").animate({
        "margin-left": "0",
            "opacity": "1"
    }, duration, function () {
        that.unwrap();
        that.unwrap();
        callback();
    });
};

//similar to jQuery's clone(), but does not copy attributes, event handlers,
//or data via jQuery.data(), simply the inner html
$.fn.cloneH = function () {
    //note: DOES NOT copy attributes!!!
    var that = $(this[0]);
    var inH = that.html();
    var tag = that.prop("tagName").toLowerCase();
    inH = "<" + tag + ">" + inH + "</" + tag + ">";
    return $(inH);
};


//$.fn.animateRotate = function(angle, duration, complete) {
//    return this.each(function() {
//        var $elem = $(this);
//
//        $({deg: 0}).animate({deg: angle}, {
//            duration: duration,
//            easing: "linear",
//            step: function(now) {
//                $elem.css({
//                    transform: 'rotate(' + now + 'deg)'
//                });
//            },
//            complete: complete || $.noop
//        });
//    });
//};

//$.fn.animateRotate = function(angle, duration, complete) {
//    return this.each(function() {
//        var $elem = $(this);
//
//        $({deg: 0}).animate({deg: angle}, {
//            duration: duration,
//            easing: "linear",
//            step: function(now) {
//            
//            	if(ie == 8){
//            		//console.log(ie);
//            //convert now to radians
//            var nowRad = now*Math.PI/180;
//            var nRsin = Math.sin(nowRad);
//            var nRcos = Math.cos(nowRad);
//            var inf = "\"progid:DXImageTransform.Microsoft.Matrix(SizingMethod='auto expand', M11="+nRcos+", M12=-"+nRsin+", M21="+nRsin+", M22="+nRcos+")\"";
//                $elem.css({
//                   "-ms-filter": inf
//                });
//        }
//        else
//
//        {
//                $elem.css({
//                    transform: 'rotate(' + now + 'deg)'
//                });
//            }
//            },
//            complete: complete || $.noop
//        });
//    });
//};


//extending easing
$.easing.easeOutExpo = function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    };

/////JavaScript functions /////

//a click event handler function for the remove link in
//"Who needs coverage" form, each member except the first, which
//defaults to seeking coverage
function covRemoveClick(event) {
    //prevent the <a> from jumping to the top of the page b/c of href="#"
    //this is present in many later handlers and functions
    //to disable the jump to top
    event.preventDefault();
    $("#fam-cov_error").hide();
    
    //refresh
	$(".nano").nanoScroller({ alwaysVisible: true });
	setTimeout(function(){
	$(".nano").nanoScroller({scroll : 'top'})
	.nanoScroller({flash: 'true'})
	.nanoScroller({ alwaysVisible: true });
	},100);
	
    if ($(this).parents("tbody").find("tr").length >= 1) {

        //remove the tr from the DOM
        $(this).parents("tr").remove();

        //decrease the value in the number of coverage by 1
        var currVal = parseInt($("#fam-cov").val());
        
        ////if input is changed, show calculation page and hide result page
        showCalculationPage();
        
        $("#fam-cov").val(currVal - 1);

        //IMPORTANT: every time an item is removed using the "X", the table is reevaluated
        //ids and names are changed to represent what index the tr element is in the table
        //for both the input and the error container, so that at any given moment, the id's
        //will be sequential
        $("#chk-plan-table").find("tbody tr input[type=text]").each(function (index) {
            $(this).attr("id", "bdaycov-" + (index + 1)).attr("name", "bdaydate" + (index + 1));
            $(this).parents("td").first().find(".bday_error").attr("id", "bdaycov-" + (index + 1) + "_error");
        });

        $("#chk-plan-table").find("tbody tr .tobacco").each(function (index) {
            $(this).attr("id", "tobacco-" + (index + 1)).attr("name", "tobacco-" + (index + 1));
        });
        
        
        $("#chk-plan-table").find("tbody tr .cov-check").each(function (index) {
            $(this).attr("id", "cov-check-" + (index + 1)).attr("name", "cov-check-" + (index + 1));
        });
    }

}


//validator function: see if the birthday fields are valid
function bdayValid() {
    $(".bdate").each(function () {
        //remove any previous rules so that there isn't too much memory being used
        $(this).rules("remove");
        //add a rule to all elements when validation is about to begin
        $(this).rules("add", {
            required: true,
            datemmddyyyy: true,
            ageUnder120: true,
            ageOver0: true
        });
    });
    //run validation, return true if valid, false if not
    return $("#form-cov").valid();
}

//bound to keyup and also run on page load if the
//zip is prepopulated
function zipbind() {

    if ($("#formzip").val().length == 5) {

        var zipcodeinp = $("#formzip").val();
        var pathURL = "validateZip";

        $.post(pathURL, {
            zip: zipcodeinp,
        }, function (data) {
        	
            $("#formzip").removeAttr("data-zip-invalid");
            
            if (typeof data !== "object") {
                $("#formzip").attr("data-zip-invalid", "true");  
            } 
            else if (Object.keys(data).length > 1) {
            	
            	if(isMobphone || isTablet){
            		$("#zipcounties").removeClass("dropdown-toggle").removeAttr("data-toggle").css("border-radius",4);
            		
            		$("#zipcounties").click(function(){
            			if(!$("#zipcounties").hasClass("disabled"))
            			$(".countymenu").toggle();
            		});
            	}
            	
                $("#zipcounties").html('Select County&nbsp;<span class="caret"></span>');
                $(".countymenu").css("width", $("#zipcounties").outerWidth() + "px");
                
                //"Please select your county..." menu header
                var inH = "";

                //Multiple counties
                $.each(data, function (index, value) {
                   
                	var countyCode ='';
                	var stateCode = '';
                	
                    $.each(value,function(ind,val){
                    	
                    	switch (ind){
	                    	case 'countyCode' : countyCode = val;break;
	                    	case 'stateCode' : stateCode = val;break;
                    	}
                    	
                    });	
                	
                    inH += "<li class='dropdown'><a href='javascript:void(0)' id='"+countyCode+"' class='"+stateCode+"'>" + index.replace(/\w\S*/g, function (txt) {
	                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	                }) + "</a></li>";
                	
                });

                //inject the html
                $(".countymenu").html(inH);
                $("#formzip").addClass("inpcounties");

                //show the dropdown immediately
                $("#zipcounties").removeClass("hide");
                $("#zipcounties").removeClass("disabled");
                $(".countymenu").removeAttr("style");
                
                if(isMobphone || isTablet){
                	$("#zipcounties").show();
                	$(".countymenu").show();
                }
                
                //Pre-populate county if recived in the request
                $(".countymenu a").each(function(index) {
                	if($(this).attr("id") == $("#countyCode").val()){
                		$("#stateCode").val($(this).attr("class"));
                		$(this).parents(".countymenu").find("a").removeClass("countyactive").parent().removeClass("dnone");
                        $(this).addClass("countyactive").parent().addClass("dnone");
                        $("#zipcounties").html($(this).text() + '&nbsp;<span class="caret"></span>');
                        $(".countymenu").css("width", $("#zipcounties").outerWidth() + "px");
                        $("#countyCode").removeAttr("data-county-name");
                	}
                });
                
                //if a selection is made, add the countyactive class and remove the countyactive class from other selections
                $(".countymenu a").on('click keyup', function (event) {
                    if (event.which == 1 || event.which == 13 || ie == 8) {
                    	
                    	$("#countyCode").val($(this).attr("id"));
                        $("#stateCode").val($(this).attr("class"));
                    	
                        $(this).parents(".countymenu").find("a").removeClass("countyactive").parent().removeClass("dnone");
                        $(this).addClass("countyactive").parent().addClass("dnone");
                        
                        $("#zipcounties").html($(this).text() + '&nbsp;<span class="caret"></span>');
                        $(".countymenu").css("width", $("#zipcounties").outerWidth() + "px");
                        $("#countyCode").removeAttr("data-county-name");
                        
                        if(isMobphone || isTablet){
                        	$(".countymenu").hide();
                		}
                    }
                });
            } 
            else {

                $("#zipcounties").html('Select County&nbsp;<span class="caret"></span>');
                $(".countymenu").css("width", $("#zipcounties").outerWidth() + "px");
                
                //Single county
                $.each(data, function (index, value) {
                	
                    $.each(value,function(ind,val){
                    	
                    	switch (ind){
	                    	case 'countyCode' : $("#countyCode").val(val);;break;
	                    	case 'stateCode' : $("#stateCode").val(val);break;
                    	}
                    	
                    });	


	                $("#countyCode").attr("data-county-name", index.replace(/\w\S*/g, function (txt) {
	                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	                }));
                    
                });
            }
            
            var inH = "<span>Zip Code: </span>" + $("#formzip").val();
            if (!($("#countyCode[data-county-name]").length)) {
                inH += "<br>" + "<span>County: </span>" + $(".countymenu").find("a.countyactive").text();
                $("#countyName").val($(".countymenu").find("a.countyactive").text());
            } else {
                inH += "<br>" + "<span>County: </span>" + $("#countyCode").attr("data-county-name");
                $("#countyName").val($("#countyCode").attr("data-county-name"));
            }
            //the span are all defaulted to display:none, show them
            $("#float-zip").html(inH).fadeIn(200);
            
        });
    } else {
        //hide it, even though it is hidden to begin with, this is necessary if the zip code has changed from one with multiple counties to one with a single county
        $(".countymenu").hide();
        $("#zipcounties").addClass("hide");
        $("#formzip").removeClass("inpcounties");
    }

}

//not necessary; buttons are absolutely positioned
//function btnBottomBind(){
//	var ht = ($(window).height()-$(".progressbar").outerHeight()-$(".navbar-inner").outerHeight());
//$(".heightwrap").each(function(i){
//	if($(this).parents("#pg2-1").length)
//		{
//		$(this).css("height",ht*.8-40);
//		}
//	else
//		{
//		$(this).css("height",ht*.8-10);
//		}
//
//});
//}

//find where to put the table headings
function cptHeader(){
	
	//Math.abs(parseInt($("#cont-table").css("right")));
	//because of the space that the scrollbar takes
	$("#cont-table-header").css("margin-left","0px");
//	var ht = ($(window).height()-$(".progressbar").outerHeight()-$(".navbar-inner").outerHeight());
	//push the table up so you can't see the header of the actual table, just the table header on top
	//such that the table can be scrolled
	$("#chk-plan-table").css("margin-top","-" + ($("#chk-plan-table thead tr").outerHeight()+8) + "px");
	//important: height of the table viewing window
/*	$(".nano").css("height",$("#chk-plan-table tbody tr:first").outerHeight()*5+10);
*/	
	
	//$(".nano").css("height",265);
	familyTableHeight();

var cpt = $("#chk-plan-table tbody tr:first");
	//align the positions of the headings
cpt.find("td").each(function(i){
	var low = cpt.find("td").eq(i).outerWidth();
	//$("#cont-table-header th").eq(i).css({"width":low,"text-align":"center"});
	//center all but the birthday headings, set the width to the actual width of the table
	//headings
	if(i!=0)
		{
		$("#cont-table-header th").eq(i).css({"width":low,"text-align":"center"});
		}
	//can't center the birthdate heading, it will be offset
	else
		{
		$("#cont-table-header th").eq(i).css("width",low*.97);
		}
	/*
	if(i != 0) 
		{
	var tow = $(this).outerWidth();
	var low = $("#cont-table-header th").eq(i).width();
	//console.log("tow:"+tow+"\nlow:"+low);
	$("#cont-table-header th").eq(i).css("padding-left",tow/2-low/2);
		}
	else 
	{
		var tow = $(this).outerWidth();
    	var low = $("#cont-table-header li").eq(i).width();
    	var tw = tow/2-low/2;
    	//console.log(tw);
    	$("#cont-table-header th").eq(i).css("padding-left",30);
    	$("#cont-table-header th").eq(i).css("padding-right",tw*1.2);
    	
	}
	*/
});
}

//ticker on intro page
function hcTickerNext()
{
		//margin-left of the ticker
		/*var mleft = 0;
		$("#hc-ticker li").each(function(){
			mleft = ($(this).outerWidth() > mleft) ? $(this).outerWidth() : mleft;
		});

		mleft = "-" + mleft + "px";*/
	//console.log(mleft);
	//$("#hc-ticker span").css("margin-left",mleft);
		//find which ticker is shown now
	var now = $("#hc-ticker li").filter(function(){return $(this).css("opacity")!=0;});
	//find the next, if no next, wrap to top
	var nxt = now.next().length ? now.next() : $("#hc-ticker li").first();
	//shift the next up into view, opacity:0
	nxt.css("top","-20px");
	//fade in
	nxt.animate({opacity: 1},800);
	//shift the now ticker up 30px, and fade out
	//now.animate({"margin-top" : "-30px","opacity":"0"},
	
	now.animate({"opacity":"0"},
				1000,
				function(){
		now.css("opacity",0);
			/*	nxt.css("margin-top","-5px");
				nxt.animate({opacity: 1},800);
				now.css("opacity",0); */
					});

}

//make all radio buttons and checkboxes work
//customized, uses jQuery to check and uncheck
function checkBind() {

//check if any checkboxes are selected on DOMready
//a checkbox/radio button has a i/span inside, containing either
//a check or a bullet that is visibility:visible when checked
$("input[type=checkbox],input[type=radio]").each(function(){
    //console.log($(this).attr("id") + " " + $(this).prop("checked"));
     if($(this).prop("checked")){
        $(this).next().find("i, span").css("visibility","visible");
    }
    else
    {   
         $(this).next().find("i, span").css("visibility","hidden");
    }
     //disabled checkboxes
    if($(this).hasClass("disabled") || $(this).attr("disabled"))
    {
        $(this).next().find("i, span").css("color","#AAAAAA");
    }
    //for the #chk-plan-table pressing plus might duplicate disabled checkbox styles,
    //prevent this
    else
    {
    $(this).next().find("i, span").css("color","inherit");
    $(this).parent().removeAttr("data-toggle").removeAttr("data-original-title");
    }

});
//unnescessary, gradients removed
//    if(ie == 9)
//    {
//        $(".custom-rc:not(.rad)").css("border","1px solid black");
//    }
//the checkbox replacement only works for !ie 
        if(!ie || ie > 8){
$("input[type=checkbox],input[type=radio]").change(function(){
    if($(this).prop("checked")){
        if($(this).attr("type")=="radio"){
        $("[name="+$(this).attr("name")+"]").next().find("i,span").css("visibility","hidden");
        }
    $(this).next().find("i,span").css("visibility","visible");
    }
    else
    {    $(this).next().find("i,span").css("visibility","hidden");
    }
});

//hover class
$("label").hover(function(){
    if(!$(this).find("[type=checkbox],input[type=radio]").length || $(this).find("[type=checkbox][disabled]").length)
        return;
  $(this).find(".custom-rc").addClass("hovered");
},function(){
  $(this).find(".custom-rc").removeClass("hovered");
});
    }
        //disable custom checkboxes
    else {
        $(".custom-rc").css({"display":"none","visibility":"hidden"});
        $("input[type=radio],input[type=checkbox]").css({"display":"block","visibility":"visible"});
    //     $("input[type=checkbox]").parent("label").click(function(){
    // if($(this).find("input[type=checkbox]").prop("checked"))
    // {
        
    // $(this).find(".custom-rc i").css("visibility","visible");
    // }
    // else
    // {
    //     $(this).find(".custom-rc i").css("visibility","hidden");
    // }
    //     });
    }
    //end checkbox stuff
}

//AJAX
function updateOtherInputsInEligLeadRecord(){
    
    //Update ELIG_LEAD table with any changes made on page 4 and 5
    var pathURL = "phix/updateOtherInputsInEligLeadRecord";
 
    var benefits = '';
    $("form input[name=benefits]:checked").each(function(){
        benefits += $(this).val() + ',';
    });
    //Remove trailing comma
	if(benefits){
		benefits = benefits.substring(0, benefits.length - 1);
	}
    
    $.ajax({
         type: "POST",
         url: pathURL,
         data: {
            docVisitFrequency : $('form input[name=pg3-doc-visit]:checked').val(),
            noOfPrescriptions : $('form input[name=pg3-drug]:checked').val(), 
            benefits : benefits
         },
         success: function (data) {
        	 //$('#page4Form').submit();
         },
         error: function (jqXHR, textStatus, errorThrown) {
            
         }
    });
    
}

//AJAX call to update state to PRE_SHOPPING (GPS) or SHOPPING (legacy re-direction)
function updateStageInEligLeadRecord(callType){
    
    var pathURL = "phix/updateStageInEligLeadRecord";
    $.ajax({
         type: "POST",
         url: pathURL,
         data: {
           type: callType
         },
         success: function (data) {
        	 
         },
         error: function (jqXHR, textStatus, errorThrown) {
            
         }
    });
    
}

//if ie8, some things will change UPDATED so works with IE10

//find which version of IE
//false if not IE, a number representing version if IE
var ie = (function()
		{
		  var rv = false; // Return value assumes failure.
		  if (navigator.appName == 'Microsoft Internet Explorer')
		  {
		    var ua = navigator.userAgent;
		    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		    if (re.exec(ua) != null)
		      rv = parseFloat( RegExp.$1 );
		  }
		  return rv;
		}());

//find if it is a mobile phone or tablet
var isMobphone = (navigator.userAgent.match(/(iPhone|iPod|Android|BlackBerry)/) && Math.min($(window).width(), $(window).height()) < 800) ? true : false;
var isTablet = ((navigator.userAgent.match(/(iPad|Android)/) || (navigator.userAgent.match(/(Touch)/))) && Math.min($(window).width(), $(window).height())>600) ? true : false; 

//css3 mediaquery support (minified)
	 
	 /*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
	 /*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
	 window.matchMedia=window.matchMedia||function(a){"use strict";var c,d=a.documentElement,e=d.firstElementChild||d.firstChild,f=a.createElement("body"),g=a.createElement("div");return g.id="mq-test-1",g.style.cssText="position:absolute;top:-100em",f.style.background="none",f.appendChild(g),function(a){return g.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',d.insertBefore(f,e),c=42===g.offsetWidth,d.removeChild(f),{matches:c,media:a}}}(document);

	 /*! Respond.js v1.3.0: min/max-width media query polyfill. (c) Scott Jehl. MIT/GPLv2 Lic. j.mp/respondjs  */
	 (function(a){"use strict";function x(){u(!0)}var b={};if(a.respond=b,b.update=function(){},b.mediaQueriesSupported=a.matchMedia&&a.matchMedia("only all").matches,!b.mediaQueriesSupported){var q,r,t,c=a.document,d=c.documentElement,e=[],f=[],g=[],h={},i=30,j=c.getElementsByTagName("head")[0]||d,k=c.getElementsByTagName("base")[0],l=j.getElementsByTagName("link"),m=[],n=function(){for(var b=0;l.length>b;b++){var c=l[b],d=c.href,e=c.media,f=c.rel&&"stylesheet"===c.rel.toLowerCase();d&&f&&!h[d]&&(c.styleSheet&&c.styleSheet.rawCssText?(p(c.styleSheet.rawCssText,d,e),h[d]=!0):(!/^([a-zA-Z:]*\/\/)/.test(d)&&!k||d.replace(RegExp.$1,"").split("/")[0]===a.location.host)&&m.push({href:d,media:e}))}o()},o=function(){if(m.length){var b=m.shift();v(b.href,function(c){p(c,b.href,b.media),h[b.href]=!0,a.setTimeout(function(){o()},0)})}},p=function(a,b,c){var d=a.match(/@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi),g=d&&d.length||0;b=b.substring(0,b.lastIndexOf("/"));var h=function(a){return a.replace(/(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,"$1"+b+"$2$3")},i=!g&&c;b.length&&(b+="/"),i&&(g=1);for(var j=0;g>j;j++){var k,l,m,n;i?(k=c,f.push(h(a))):(k=d[j].match(/@media *([^\{]+)\{([\S\s]+?)$/)&&RegExp.$1,f.push(RegExp.$2&&h(RegExp.$2))),m=k.split(","),n=m.length;for(var o=0;n>o;o++)l=m[o],e.push({media:l.split("(")[0].match(/(only\s+)?([a-zA-Z]+)\s?/)&&RegExp.$2||"all",rules:f.length-1,hasquery:l.indexOf("(")>-1,minw:l.match(/\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/)&&parseFloat(RegExp.$1)+(RegExp.$2||""),maxw:l.match(/\(\s*max\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/)&&parseFloat(RegExp.$1)+(RegExp.$2||"")})}u()},s=function(){var a,b=c.createElement("div"),e=c.body,f=!1;return b.style.cssText="position:absolute;font-size:1em;width:1em",e||(e=f=c.createElement("body"),e.style.background="none"),e.appendChild(b),d.insertBefore(e,d.firstChild),a=b.offsetWidth,f?d.removeChild(e):e.removeChild(b),a=t=parseFloat(a)},u=function(b){var h="clientWidth",k=d[h],m="CSS1Compat"===c.compatMode&&k||c.body[h]||k,n={},o=l[l.length-1],p=(new Date).getTime();if(b&&q&&i>p-q)return a.clearTimeout(r),r=a.setTimeout(u,i),void 0;q=p;for(var v in e)if(e.hasOwnProperty(v)){var w=e[v],x=w.minw,y=w.maxw,z=null===x,A=null===y,B="em";x&&(x=parseFloat(x)*(x.indexOf(B)>-1?t||s():1)),y&&(y=parseFloat(y)*(y.indexOf(B)>-1?t||s():1)),w.hasquery&&(z&&A||!(z||m>=x)||!(A||y>=m))||(n[w.media]||(n[w.media]=[]),n[w.media].push(f[w.rules]))}for(var C in g)g.hasOwnProperty(C)&&g[C]&&g[C].parentNode===j&&j.removeChild(g[C]);for(var D in n)if(n.hasOwnProperty(D)){var E=c.createElement("style"),F=n[D].join("\n");E.type="text/css",E.media=D,j.insertBefore(E,o.nextSibling),E.styleSheet?E.styleSheet.cssText=F:E.appendChild(c.createTextNode(F)),g.push(E)}},v=function(a,b){var c=w();c&&(c.open("GET",a,!0),c.onreadystatechange=function(){4!==c.readyState||200!==c.status&&304!==c.status||b(c.responseText)},4!==c.readyState&&c.send(null))},w=function(){var b=!1;try{b=new a.XMLHttpRequest}catch(c){b=new a.ActiveXObject("Microsoft.XMLHTTP")}return function(){return b}}();n(),b.update=n,a.addEventListener?a.addEventListener("resize",x,!1):a.attachEvent&&a.attachEvent("onresize",x)}})(this);

	 //object keys
	 Object.keys = Object.keys || (function () {
		    var hasOwnProperty = Object.prototype.hasOwnProperty,
		        hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
		        DontEnums = [ 
		            'toString', 'toLocaleString', 'valueOf', 'hasOwnProperty',
		            'isPrototypeOf', 'propertyIsEnumerable', 'constructor'
		        ],
		        DontEnumsLength = DontEnums.length;

		    return function (o) {
		        if (typeof o != "object" && typeof o != "function" || o === null)
		            throw new TypeError("Object.keys called on a non-object");

		        var result = [];
		        for (var name in o) {
		            if (hasOwnProperty.call(o, name))
		                result.push(name);
		        }

		        if (hasDontEnumBug) {
		            for (var i = 0; i < DontEnumsLength; i++) {
		                if (hasOwnProperty.call(o, DontEnums[i]))
		                    result.push(DontEnums[i]);
		            }   
		        }

		        return result;
		    };
		})();
//Validators

//check whether the date is in proper mm/dd/yyyy format
//check if the month and day are valid
jQuery.validator.addMethod(
    "datemmddyyyy",

function (value, element) {
    	
    	//var today = new Date();
		dob_arr = value.split('/');

		dob_mm = dob_arr[0];
		dob_dd = dob_arr[1];
		dob_yy = dob_arr[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		//if ((today.getFullYear() - 120) > birthDate.getFullYear()) {return false;}

		if ((dob_dd != birthDate.getDate())
					|| (dob_mm - 1 != birthDate.getMonth())
					|| (dob_yy != birthDate.getFullYear())) {
				return false;
		}
		
		//if (today.getTime() < birthDate.getTime()) {return false;}

		return true;
},"Please enter a valid date of birth in the format MM/DD/YYYY.");

//check whether the age is under 120
jQuery.validator.addMethod("ageUnder120",

function (value, element) {
    var today = new Date();

    var ddn = today.getDate();
    var mmn = today.getMonth() + 1;
    var yyyyn = today.getFullYear() - 120;

    var ddv = value.substring(0, value.indexOf("/"));
    var mmv = value.substring(value.indexOf("/") + 1, value.lastIndexOf("/"));
    var yyyyv = value.substring(value.lastIndexOf("/") + 1);

    //find the difference between today's date and the date given, see if
    //it is over 120 years
    var diff = (yyyyv - yyyyn) * 10000 + (mmv - mmn) * 100 + (ddv - ddn);

    return (diff > 0);

},"Please enter an age below 120.");

//check whether the age is over 0
jQuery.validator.addMethod(
    "ageOver0",

function (value, element) {
    var today = new Date();

    var ddn = today.getDate();
    var mmn = today.getMonth() + 1;
    var yyyyn = today.getFullYear();

    var mmv = value.substring(0, value.indexOf("/"));
    var ddv = value.substring(value.indexOf("/") + 1, value.lastIndexOf("/"));
    var yyyyv = value.substring(value.lastIndexOf("/") + 1);
    //find the difference between today's date and the date given, see if
    //it is under 0 years
    var diff = (yyyyv - yyyyn) * 10000 + (mmv - mmn) * 100 + (ddv - ddn);
    return (diff <= 0);

},"Quotes cannot be generated for anyone with a future birth date.");

//if the zip has a corresponding county dropdown, make
//sure that an option has been selected from the dropdown
jQuery.validator.addMethod(
    "zipcountyval",

function (value, element) {
    
    if(!$("#countyCode").val()){
        return false;
    }	
    	
    //check if the dropdown is shown, an indication that it is present
    if ($("#zipcounties").hasClass("hide")) {
        return true;
    } else {
        //if shown, check that an option has been selected
        if ($(".countymenu li").find(".countyactive").length && $("#countyCode").val()) {
            return true;
        }
        return false;
    }
},"Please select a county.");


//make sure the zip is valid
jQuery.validator.addMethod(
    "zipvalid",

function (value, element) {
    return ($("#formzip[data-zip-invalid]").length != 1);
},
    "Please enter a valid zip code.");

$(".search-dd").find("a,input").click(function (event) {
    event.stopPropagation();
});

//animate a label from opacity 0 to 1 showing what category the user falls in
function findUsage(){
	
	//Finding the frequency
    var frequency = 0;
    var docVisits = $("[name=pg3-doc-visit]:checked").val();
    var drug = $("[name=pg3-drug]:checked").val();

    switch (docVisits) {
        case '1':
            frequency += 1;
            break;
        case '2':
            frequency += 2;
            break;
        case '5':
            frequency += 5;
            break;
        case '10':
            frequency += 10;
            break;
        default:
            break;
    }

    switch (drug) {
        case '1':
            frequency += 1;
            break;
        case '2':
            frequency += 2;
            break;
        case '5':
            frequency += 5;
            break;
        case '10':
            frequency += 10;
            break;
        default:
            break;
    }

    if ($("[name=pg3-doc-visit]:checked,[name=pg3-drug]:checked").length == 2) {

        $("#label-usage").animate({
            "opacity": "1"
        }, 500);

        if (frequency >= 0 && frequency <= 2) {
            $("#usageValue").html("Low");
        } else if (frequency >= 3 && frequency <= 4) {
            $("#usageValue").html("Moderate");
        } else if (frequency >= 5 && frequency <= 10) {
            $("#usageValue").html("High");
        } else if (frequency > 10) {
            $("#usageValue").html("Very High");
        }
        
        $("#usage").val($("#usageValue").html());
        $("#float-usage").html("<span>Medical Usage: </span>" + $("#usageValue").html()).fadeIn(200);
        $("#label-usage").removeClass("hide");
    }
	
}

$(document).ready(function () {
	//make the checkboxes and radio buttons work
	checkBind();
	
	//placeholder for ie8
	$('input, textarea').placeholder();
		
    //vegas, the jQuery slideshow plugin
	
	$.vegas('slideshow', {
        delay: 6000,
        walk: hcTickerNext,
        backgrounds: [{
            src: ctx + '/resources/images/bg1.png',
            fade: 1000
        }, {
            src: ctx + '/resources/images/bg2.jpg',
            fade: 1000
        }, {
            src: ctx + '/resources/images/bg3.jpg',
            fade: 1000
        }, {
            src: ctx + '/resources/images/bg4.jpg',
            fade: 1000
        }]
    })('overlay', {
        src: ctx + '/resources/images/11.png'
    });
	
    //scroll to top regardless, not necessarily on a cache refresh
    //window.scrollTo(0, 0);


	//align the fam-cov height
	setTimeout(function(){$("#fam-cov").css("height",$("#fam-cov-plus").outerHeight());},100);
	
	
	//find the date from now until October 1st, put that value in place of "X" in the hc-ticker
	//Set the two dates
	today=new Date();
	var reldate=new Date(today.getFullYear()+1, 2, 31); //Month is 0-11 in JavaScript
	//map the dates to the time
	reldate = reldate.getTime();
	today = today.getTime();
	if(today > reldate)
		{
		$("#dl-tobuy").remove();
		}
	else
		{
	$("#dl-tobuy strong").html(Math.ceil((reldate-today)/86400000));
		}
	
	$("#hc-ticker").animate({opacity:.75},200);
	
	//ie doesn't not support css3 animation
    if(ie && ie<10){
//    	setInterval(function(){$(".icon-spin").animateRotate(180,800);},800);
    	//temporary fix for spinning, hide it
    	$(".icon-spin").css("display","none").css("visibility","hidden");
    	//$("#page1-form").prepend("<span style='font-size:36px;font-weight:bold'>Zip Code:</span>");
    	$("#intro-message p").css({"margin":"0 0 10px","background":"none"}).find("span").css({"background":"rgb(34,34,34)","border-radius":"4px"});
    	}
//panel snap, disabled if mobile, for testing as  of now
//    if ($(window).width() < 766) {
//        $(".panelwrap").css("margin-top", $(".navbar").outerHeight() + $(".progressbar").outerHeight() - 30);
//    }
//    $(window).resize(function () {
//        if ($(window).width() < 766) {
//            $(".panelwrap").css("margin-top", $(".navbar").outerHeight() + $(".progressbar").outerHeight() - 30);
//        } else $(".panelwrap").css("margin-top", "0");
//    });
    
    
  
    //important: functions that are only run on mobile/tablet or only on a computer
    if (isMobphone) {
        /*
	   $(".progressbar li a").each(function(){
		   $(this).attr("href","#"+$(this).attr("data-panel"));
	   });
	   $("section:not(#intro)").each(function(){
		   $(this).attr("id",$(this).attr("data-panel"));
	   });
	   */
    	//only mobile/tablet
    	$("#cont-table").css({
    		"height":"auto",
    		"overflow-y": "visible"
    	});
    	//remove the floating header for #chk-plan-table
    	//have a regular table
    	$("#cont-table-header").css("display","none");
    	$("#chk-plan-table").css("margin-top","0");
    	
        $("section:not(#intro)").css("height", "auto");
        $("#intro").css("height", "1200px");
        //	   $("p").css("font-size","50px !important");
        //took out #pg2-2-next
        $("section:not(#intro) .btn").not("#pg2-1-next,#pg2-2-previous,#pg4-1-next,#fam-cov-minus,#fam-cov-plus").hide();

        //scrolling to proper location upon clicking on links in
        //progressbar
        $(".progressbar a[data-panel]").click(function (event) {
            event.preventDefault();
            if ($(this).attr("data-panel") != "intro") {
                $('html, body').scrollTop($("section[data-panel=" + $(this).attr("data-panel") + "] .panelwrap").offset().top - 200);

            } else {
                $("html,body").scrollTop(0);
            }
        });
        $(".content").unwrap();
    } else {
    	//panelsnap: menu in .panelmenu
    	//directionThreshold: how sensitive the snapping is
       /*
    	$('body').panelSnap({
            $menu: $('.panelmenu'),
            directionThreshold: 1
        });
        */
    	//removed because of the presence of a footer
//        $(window).scroll(function () {
//            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
//            	if(!ie || ie>8) {
//                $(".progressbar").find("[data-panel=page4]").click();
//            	}
//            }
//        });
        //keep the button at the bottom at all costs   
        //make sure the table headings are accurately aligned
        //btnBottomBind();
        cptHeader();
        $(window).resize(function(){
        	//btnBottomBind();
        	cptHeader();
        });
    }

	//to get rid of the greyedout scrollbar
if(!isMobphone){	
    $(".nano").nanoScroller({ alwaysVisible: true }).nanoScroller({destroy:true});
}

//trigger a click within the nano div so that scrolling in it works
//however, if one scrolls to the top, default behavior is to then scroll
//the document, likewise for scrolling down
$(".nano").hover(function(){
	$(".nano").trigger('click');
},function(){
	$("section[data-panel=page1]").trigger('click');
});

    //MM/DD/YYYY mask for date
    $(".bdate").mask("?99/99/9999", {
        placeholder: "-"
    });
    //5 digit mask for zip
    $("#formzip").removeAttr("disabled").mask("?99999", {
        placeholder: ""
    });

    //Masking should not be done for income field
    /* $("#income").mask("?9999999", {
        placeholder: ""
    });*/

    //check if the zip has multiple counties on page load
    //if the zip field is prepopulated
    zipbind();
    //sticky the progressbar
    $('#progressbarwrap').waypoint('sticky');
    $("#progressbarwrap").parent("div").css("height","50px");
    // $("#progressbarwrap").parent("div").css("position", "absolute");

    //fam-cov default value will be set by the model object 
    //$("#fam-cov").val("1");

    //focus on first input    
  /*  setTimeout(function () {
        $("input[type=text]:visible").first().focus();
    }, 10);*/
    
    //tooltip for 1st person
    $("#chk-plan-table tbody tr").first().find(".custom-rc").last()
    .attr("data-toggle","tooltip")
    .attr("data-original-title","As you are the applicant, you cannot be removed.")
    .attr("data-placement","right");
    
    //boostrap popovers and tooltips initialized
    $("[data-toggle='tooltip'],[rel='tooltip']").attr("data-html", "true").tooltip();

    //prevent it from jumping onclick
    $("a[data-toggle='tooltip'],a[rel='tooltip']").click(function (event) {
        event.preventDefault();
    });
    //zipcode validation
    $("#page1-form").validate({
        //make true if you need a form submission
        //with a function inside, ex
        //onsubmit:function(){alert("submit")},
        onsubmit: false,
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            formzip: {
                required: true,
                digits: true,
                minlength: 5,
                maxlength: 5,
                //custom
                zipvalid: true,
                zipcountyval: true
                
            }
        },
        //all messages are the same
        messages: {
            formzip: {
                required: "Please enter a valid zip code.",
                digits: "Please enter a valid zip code.",
                minlength: "Please enter a valid zip code.",
                maxlength: "Please enter a valid zip code.",
                zipvalid: "Please enter a valid zip code.",
                zipcountyval: "Please select a county."
                
            }
        },
        errorLabelContainer: "#page1-form_error"
    });
    
    //validate "who needs coverage" specifically input[type=text] fields
    $("#form-cov").validate({
        onsubmit: false,
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        //only validates shown birthday fields
        ignore: "[type=checkbox],:hidden",
        //dynamically added, empty for now
        rules: {},
        //place the error in the corresponding error container
        //suffixed by "_error"
        errorPlacement: function (error, element) {
        	
        	if(element.attr("id")){
        		$("#" + element.attr("id") + "_error").html(error.html());
        	}
        	else{
        		$("#" + element.attr("name") + "_error").html(error.html());
        	}
        }
    });
    //remove the last member from the list of
    //"who needs coverage"
    $("#fam-cov-minus").click(function () {

        
        var currVal = parseInt($("#fam-cov").val());
     
        if(!isMobphone) {
        if(currVal == 4)
        	{

        	setTimeout(function(){
            	$(".nano").nanoScroller({stop : true})
            	.nanoScroller({scroll : 'top'})
            	.nanoScroller({ alwaysVisible: true });
            	},100);
        	}
        else if(currVal > 4)
    	{
    	//refresh
    	$(".nano").nanoScroller({ alwaysVisible: true });
    	setTimeout(function(){
    	$(".nano").nanoScroller({scroll : 'top'})
    	.nanoScroller({flash: 'true'})
    	.nanoScroller({ alwaysVisible: true });
    	},100);
    	}
    else
    	{
    	$(".nano").nanoScroller({destroy:true}).nanoScroller({ alwaysVisible: true });
    	}
    }
        
        var cpt = $("#chk-plan-table");

        if (currVal > 1) {
            $("#fam-cov_error").hide();
            cpt.find("tbody tr").last().remove();
          ////if input is changed, show calculation page and hide result page
            if($("#fam-cov").val()!=1){
            	showCalculationPage();
    		}
            $("#fam-cov").val(currVal - 1);
        } else {
            $("#fam-cov").val("1");
            $("#fam-cov_error").html("<label for='fam-cov'>At least one family member must be seeking coverage to receive quotes.</label>").show();
        }
    });

    $("#fam-cov-plus").click(function () {
    	
        var currVal = parseInt($("#fam-cov").val());
        var cpt = $("#chk-plan-table");
        
        if(!isMobphone) {
	        if(currVal >= 2){
		    	// refresh
		    	$(".nano").nanoScroller({ alwaysVisible: true });
		    	setTimeout(function(){
		    	$(".nano").nanoScroller({scroll : 'top'})
		    		.nanoScroller({flash: 'true'})
		    		.nanoScroller({ alwaysVisible: true });
		    	},100);
	    	}
	        else{
	        	$(".nano").nanoScroller({stop:true}).nanoScroller({ alwaysVisible: true });
	    	}
        }
        
        if (currVal >= 7) {
            $("#fam-cov_error").html("<label for='fam-cov'>You cannot add more than 7 members.</label>").show();
        } else if (currVal >= 1) {
            $("#fam-cov_error").hide();
            //copy the previous row, clear the data inside
            cpt.find("tbody tr").last().cloneH().appendTo("#chk-plan-table tbody");
            ////if input is changed, show calculation page and hide result page
            if($("#fam-cov").val()!=7){
            	showCalculationPage();
    		}
            $("#fam-cov").val(currVal + 1);

            //find the index, use it to have an id tied to the index, ex bdaycov-3 represents
            //the 4th item in the series and the error container is that id with
            //a _error suffix. 

            var ind = (currVal + 1);
            cpt.find("tbody tr span.add-on").last().html("");
            cpt.find("tbody tr input[type=text]").last().val("").attr("id", "bdaycov-" + ind).attr("name", "bdaydate" + ind).attr("value", "");
            cpt.find("tbody tr input[type=text]").last().mask("?99/99/9999", {
                placeholder: "-"
            });
            cpt.find("tbody tr .bday_error").last().attr("id", "bdaycov-" + ind + "_error").html("");

            cpt.find("tbody tr .tobacco").last().attr("id", "tobacco-" + ind);
            cpt.find("tbody tr .tobacco").last().attr("name", "tobacco-" + ind);
            cpt.find("tbody tr .tobacco").last().removeAttr("disabled");
            cpt.find("tbody tr .tobacco").last().removeAttr("checked");

            cpt.find("tbody tr .cov-check").last().attr("id", "cov-check-" + ind);
            cpt.find("tbody tr .cov-check").last().attr("name", "cov-check-" + ind);
            cpt.find("tbody tr .cov-check").last().removeAttr("disabled");
            cpt.find("tbody tr .cov-check").last().next().removeClass("disabled");
            cpt.find("tbody tr .cov-check").last().attr("checked", "checked");

            cpt.find("tbody tr .remove-button").last().html("<a href='javascript:void(0)' class='cov-remove'>REMOVE</a>");

            cpt.find("tbody tr .bday_error").last().attr("id", "bdaycov-" + ind + "_error").html("");

            //bind the remove click function to the "X"
            cpt.find("tbody tr").last().find(".cov-remove").click(covRemoveClick);
            checkBind();
        } else {
            $("#fam-cov").val("1");
        }
    });

    $("#formzip").keyup(zipbind);
    
    $("a:not([href])").on("focus keydown", function (e) {
        if (e.which == 13) {
            $(this).click();
        }
    });
    
//disabled as of now
//    $("#media-activate").click(function(){
//        $("#allblack").fadeIn(1000,function(){
//            var lnk = $("#mediabox-content").attr("src")+"?autoplay=1";
//            $("#mediabox-content").attr("src",lnk);
//            $("#mediabox").fadeIn(300);
//            
//        });
//    });
//    $("#allblack,#mediabox-close").click(function(){
//
// $("#mediabox").fadeOut(200,function(){
//    var lnk = $("#mediabox-content").attr("src").replace("?autoplay=1","");
//    $("#mediabox-content").attr("src",lnk);
//            $("#allblack").fadeOut(500);
//        });
//    });
    $("#getstarted").click(function (event) {
        event.preventDefault();
        //temporary timeout while the server validatesZip,
        //replace with actual validation in next version
        setTimeout(function () {
            //validate form, if valid click the progressbar to go
            //to the next page
            if ($("#page1-form").valid()) {
                $(".progressbar").find("[data-panel=page1]").click();
                //remove any evidence of the dropdown, make the zip field uneditable
                $("#formzip").attr("disabled", "").removeClass("inpcounties");
                $("#zipcounties").addClass("disabled");
                //replace the getstarted button with the edit zip button
                $("#getstarted").addClass("hide");
                $("#editzip").removeClass("hide");
                //activate the floatpanel
                setTimeout(
                //fadein
                function () {
                    $('#floatpanel').animate({
                        "opacity": "1"
                    }, 300);
                    //remove hidden
                    $('#floatpanel').removeClass("hide");
                    //make it sticky
                    $('#floatpanel').waypoint('sticky');

                    //add new data
                    $("#floatpanel").parent("div").css("position", "absolute");
                    //$("#floatpanel").parent("div").css({"position": "absolute","height":"168px"});
                }, 500);
                //add in zip and county (if there is one)
                var inH = "<span>Zip Code: </span>" + $("#formzip").val();
                if (!($("#countyCode[data-county-name]").length)) {
                    inH += "<br>" + "<span>County: </span>" + $(".countymenu").find("a.countyactive").text();
                    $("#countyName").val($(".countymenu").find("a.countyactive").text());
                } else {
                    inH += "<br>" + "<span>County: </span>" + $("#countyCode").attr("data-county-name");
                    $("#countyName").val($("#countyCode").attr("data-county-name"));
                }
                //the span are all defaulted to display:none, show them
                $("#float-zip").html(inH).fadeIn(200);
            }

        }, 100);
    });

    //make the zipcounty dropdown work on a keypress
    $("#zipcounties").keydown(function (event) {
        event.stopPropagation();
        if (event.which == 13) {
            $("#zipcounties").dropdown('toggle');
        }
    });

    //clicking on the editzip box makes the zip field editable
    //and replaces the editzip with getstarted
    $("#editzip").click(function (event) {
        event.preventDefault();
        $("#formzip").removeAttr("disabled");
        if (!($("#zipcounties:contains(Select)").length)) {
            $("#zipcounties").removeClass("disabled");
            $("#zipcounties").removeClass("hide");
        }
        $("#getstarted").removeClass("hide");
        $("#editzip").addClass("hide");
        $("#page1-form_error").html("<label class='error' for='formzip'>Changing your location may change your results below.</label>").show();

    });

    //if the user if focused in the inputs for birthdate, check if
    //the zip is valid, and scroll to the zip panel if not
    $("#chk-plan-table tbody tr input").focus(function () {
        $("#getstarted").click();
        if (!$("#page1-form").valid()) {
            $(".progressbar").find("[data-panel=intro]").click();
            $("#formzip").focus();
        }
    });
    //"who needs coverage" previous button
    $("#pg1-previous").click(function (event) {
        event.preventDefault();
        $(".progressbar").find("[data-panel=intro]").click();
    });
    //"who needs coverage" next button
    $("#pg1-next").click(function (event) {
        event.preventDefault();
        //won't validate for zip; not necessary through the focus handler
        //validate for birthdate
        $(".bday_error").html("");
        if (bdayValid()) {
            //finds the numer of coverage checked and puts that in the floatpanel
            $(".progressbar").find("[data-panel=page2]").click();
            //pluralization
            if ($(".cov-check:checked").length == 1) {
                $("#float-covfamily").html("<span>Seeking Coverage : </span>" + $(".cov-check:checked").length).fadeIn(200);

            }else {
                $("#float-covfamily").html("<span>Seeking Coverage : </span>" + $(".cov-check:checked").length).fadeIn(200);
                $("#peopleNeedingCoverage").val($(".cov-check:checked").length);
            }
        }else{
        	//paco:improve tabindex
        	$("#chk-plan-table").find("div.bday_error:not(:empty)").first().prev().find("input").focus();
        }
        $(".nano").nanoScroller({ alwaysVisible: true });
    	setTimeout(function(){
    	$(".nano").nanoScroller({scroll : 'top'})
    	.nanoScroller({flash: 'true'})
    	.nanoScroller({ alwaysVisible: true });
    	},100);
    });

    //"Save on monthly premium" previous button
    $("#pg2-1-previous").click(function (event) {
        event.preventDefault();
        $(".progressbar").find("[data-panel=page1]").click();

    });

    //check for savings button - call backend to find APTC/CSR
    $("#pg2-1-next").click(function (event) {
        event.preventDefault();
        
        
        if($(this).hasClass("disabled")){
            return;
        }
        
        //validate zip and members
        if (!$("#page1-form").valid()) {
            $(".progressbar").find("[data-panel=intro]").click();
        } 
        else if (!bdayValid()) {
            $(".progressbar").find("[data-panel=page1]").click();
        } 
        //validate income if present
        else if($("#income").val()){
            
            $("#income_error").hide();
            var income = $("#income").val();
            
            if(isNaN(income) || parseFloat(income) < 0.0){
                 $(".progressbar").find("[data-panel=page2]").click();
                 $("#income_error label").html("Please enter a valid income");
                 $("#income_error").show();
            }
            else if(parseFloat(income) > 9999999){
                 $(".progressbar").find("[data-panel=page2]").click();
                 $("#income_error label").html("Please enter a value below 10,000,000");
                 $("#income_error").show();
            }
            else {
                //slide next page in, a results page
                $("#pg2-1").fadeOut(800, function () {
                    calculateSavings('page2');
                   /* getResultHeight();*/
                    //update annual household income, if there is one
                    if ($("#income").val() != "") {
                        $("#float-hhincome").html("<span>Income: </span>$" + $("#income").val()).fadeIn(200);
                    }
                    $("#float-covfamily").html("<span>Seeking Coverage : </span>" + $(".cov-check:checked").length).fadeIn(200);
                    //change the button style to "Recheck for savings", functionality is the same
                    $("#pg2-1-next").removeClass("btn-info").addClass("btn-warning").html("Recheck for savings")
                    .parents(".next-button-wrap").first().hide();
                });
            }
        }
        else {
            //slide next page in, a results page
            $("#pg2-1").fadeOut(800, function () {
                calculateSavings('page2');

                //update annual household income, if there is one
                if ($("#income").val() != "") {
                    $("#float-hhincome").html("<span>Annual Household Income: </span>$" + $("#income").val()).fadeIn(200);
                }
                //change the button style to "Recheck for savings", functionality is the same
                $("#pg2-1-next").removeClass("btn-info").addClass("btn-warning").html("Recheck for savings")
                .parents(".next-button-wrap").first().hide();
            });
        }
    });
    
    //No thanks button on income page
    $("#pg2-1-noThanks").click(function(event){
    	event.preventDefault();
    	$(".progressbar").find("[data-panel=page3]").click();
    });
    
    //previous button for the results page, takes to the "save on monthly premium" page
    $("#pg2-2-previous").click(function (event) {
        event.preventDefault();
        $("#resultsSection").fadeOut(100, function () {
            $("#savingsButtons").fadeOut(100, function () {});
            $("#pg2-1").fadeIn(800, function () {});
            
            $("section[data-panel=page2]").css("height","100%");
     		$(".progressbar").find("[data-panel=page2]").click();
     		
            $("#pg2-1-next").parents(".next-button-wrap").first().show();
            
            //paco:improve tabindex
            $("#income").focus();
        });
        
        //$("section[data-panel=page3],section[data-panel=page4]").show();
        //$(".progressbar a[data-panel=page3],.progressbar a[data-panel=page4]").show();

    });

    $("#pg2-2-next").click(function (event) {
        event.preventDefault();
        
       // if($(this).hasClass("error")){
        	//window.location.href="phixhome";
        //}
        //else
        	if($(this).attr("href") != '#'){
            window.open(
                    $(this).attr("href"),
                      '_blank'
                    );
            
        }
        else{
        	$(".progressbar").find("[data-panel=page3]").click();
        }
    });

    //clear the radio buttons on the current page, go to previous page
    $("#pg3-previous").click(function (event) {
        event.preventDefault();
        // $("#pg3").find("[type=radio]").prop("checked", false);
        $(".progressbar").find("[data-panel=page2]").click();
    });


    $("#pg3-next").click(function (event) {
        event.preventDefault();
        // //update floatpanel if the radio buttons were checked
        // if ($('form input[name=pg3-doc-visit]:checked').length) {
        //     //$("#float-oop-doc").html("<span>Frequency of Doctor visits: </span>" + $('form input[name=pg3-doc-visit]:checked').parents("label").text()).fadeIn(200);
        // }
        // if ($('form input[name=pg3-drug]:checked').length) {
        //     //$("#float-oop-drug").html("<span>Prescriptions drugs: </span>" + $('form input[name=pg3-drug]:checked').parents("label").text()).fadeIn(200);
        // }
        findUsage();
        //goto next page
        $(".progressbar").find("[data-panel=page4]").click();
    });
    
    $("#pg4-1-previous").click(function (event) {
        event.preventDefault();
        //clear previous and current page entered data, goto previous page
        // $("#pg4-1,#pg3").find("[type=radio],[type=checkbox]").prop("checked", false);
        //$("#float-oop-drug,#float-oop-doc").html("");
        $(".progressbar").find("[data-panel=page3]").click();
    });
    
    //find plans button
    //$("#pg4-1-next").html("SUBMIT");
    
    $("#pg4-1-next").click(function (event) {
    	
        //check if the zipcode fields and the "covered members" fields are valid
        if (!$("#page1-form").valid()) {
            event.preventDefault();
            $(".progressbar").find("[data-panel=intro]").click();
        } else if (!bdayValid()) {
            event.preventDefault();
            $(".progressbar").find("[data-panel=page1]").click();
        } else {
            $("#peopleNeedingCoverage").val($(".cov-check:checked").length);
            calculateSavings('page4');
        }
    });
    
    
	//Find usage on page load to show label from pre-populated data
    findUsage();
    $("[name=pg3-doc-visit],[name=pg3-drug]").click(function () {
        findUsage();
    });
    
    //bind the covRemoveClick to the .cov-remove link on DOM ready
    $(".cov-remove").click(covRemoveClick);

    //search functionality in development
    $(".searchNav").css("text-decoration", "none");
    $(".searchNav").click(function (event) {
        event.preventDefault();

        var lipr = $(this).parents("li").prev();
        if (lipr.hasClass("dnone")) {
            lipr.removeClass("dnone");
        } else {
            lipr.addClass("dnone");
        }
    });

    $(".inputquery").keydown(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#searchRes").find("iframe").attr("src", "http://m.bing.com/search?q=" + $(this).val());
            $("#searchRes").modal('show');
        }
    });

    //enter keys
    $("input:not(.inputquery)").keydown(function (event) {
        if (event.which == 13) {
            event.preventDefault();

            if ($(this).attr("id") == "formzip") {
                $("#getstarted").click();
            } else {

                $(this).parents(".panel").first().find("a[id$='-next']").click();
            }
        }

    });
    $(".progressbar li form").parent().css({

        "padding-bottom": "0px",
            "padding-top": "10px"
    });
    //ends here

  //paco:improve tabindex
    
    $("section,.navbar-inner").find("a,input,button,.custom-rc").attr("tabindex", "0");
    $("section").keyup(function(event) {
    	if ( event.which == 9 ){	
    		if($(this).find("a,input,button,.custom-rc").is(":focus")){
    			if(!$(this).hasClass("active")){
    				var panel=$(this).attr("data-panel");
    				$(".progressbar").find(("[data-panel="+panel+"]")).click();
    			}
    		}
    	}
    	/*if($("#houseHold").is(":focus")){
    		 $("#answerSection").attr("tabindex", "0");
    	}*/
    	
    	
    });
    
    $("#progressbarwrap").keyup(function(event) {
    	if($("#progressbarwrap ul li a[data-panel='intro']").is(":focus")){
    		$("#answerSection").attr("tabindex", "0");
    	}
    });
    
   /* $("section").find("a,input,button").attr("tabindex", "-1");
    $("[data-panel=intro]").find("a,input,button").attr("tabindex", "1");

    $("body").on("panelsnap:finish", function (e, $target) {
        $("section").find("a,input,button").attr("tabindex", "-1");
        $target.find("a,input,button").attr("tabindex", "1");
        //focus on first input
        //$target.find("input[type=text]:visible").not(".disabled,[disabled],.bdate").first().focus();
        //temporary fix
        if($target.offset().top!=$(window).scrollTop()){
        	$(".progressbar").find("[data-panel="+$target.attr("data-panel")+"]").click();
        }
    });*/
    

    
    $('.numbersOnlyField').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    
    //For Legacy or FFM integration - Show Q2 during page load and if Q1 is answered as Y
    if($('input[name=noSubsidyRadioOne]:checked').val() == 'Y'){
		
		$('.noSubisidyQ2').show();
		$('.noSubsidycallUs').hide();
		
		//Legacy site with 2013 plans
    	if($('input[name=noSubsidyRadioTwo]:checked').val() == 'Y'){
    		$('.noSubsidycallUs').hide();
        	$("#pg2-2-next").attr("onclick","submitToLegacySite();");
        	$("#pg2-2-next").removeAttr("disabled");
        	$("#pg2-2-next").show();
    	}
    	
    	//Show Call us div
    	if($('input[name=noSubsidyRadioTwo]:checked').val() == 'N'){
    		$('.noSubsidycallUs').show();
        	$("#pg2-2-next").attr("onclick","javascript:void(0);");
        	$("#pg2-2-next").attr("disabled","disabled");
        	$("#pg2-2-next").show();
    	}
    	
	}
    
    //PHIX 1 - Legacy or FFM integration result
    $('.noSubsidyRadio').change(function(){
    	
    	//GPS 
    	if($('input[name=noSubsidyRadioOne]:checked').val() == 'N'){
    		
    		$('.noSubisidyQ2').hide();
    		$('.noSubsidycallUs').hide();
    		
    		if(planInfoAvailable == 'Y'){
    			$("#pg2-2-next").html("Go Shopping");
	    		$("#pg2-2-next").attr("onclick","postToPlanDisplay();");
	        	$("#pg2-2-next").removeAttr("disabled");
	        	$("#pg2-2-next").show();
    		}
    		else{
    			$("#pg2-2-next").hide();
        		$("#noPlansAvailable").show();
    		}
    	}
    	
    	if($('input[name=noSubsidyRadioOne]:checked').val() == 'Y'){
    		
    		$('.noSubisidyQ2').show();
    		$('.noSubsidycallUs').hide();
        	
    		//Legacy site with 2013 plans
        	if($('input[name=noSubsidyRadioTwo]:checked').val() == 'Y'){
        		$('.noSubsidycallUs').hide();
        		$("#pg2-2-next").html("Go Shopping");
            	$("#pg2-2-next").attr("onclick","submitToLegacySite();");
            	$("#pg2-2-next").removeAttr("disabled");
            	$("#pg2-2-next").show();
            	$("#noPlansAvailable").hide();
        	}
        	
        	//Show Call us div
        	if($('input[name=noSubsidyRadioTwo]:checked').val() == 'N'){
        		$('.noSubsidycallUs').show();
            	$("#pg2-2-next").attr("onclick","javascript:void(0);");
            	$("#pg2-2-next").attr("disabled","disabled");
            	$("#pg2-2-next").show();
            	$("#noPlansAvailable").hide();
        	}
        	
    	}
    	
    	
    	
    });
    
    //PHIX 1 - Legacy result
    $('.legacyRadio').change(function(){
    	
    	if($('input[name=legacyRadioOne]:checked').val() == 'N' || $('input[name=legacyRadioTwo]:checked').val() == 'N'){
    		$('.legacyCallUs').show();
        	$("#pg2-2-next").attr("onclick","javascript:void(0);");
        	$("#pg2-2-next").attr("disabled","disabled");
        	$("#pg2-2-next").show();
    	}
    	else{
    		$('.legacyCallUs').hide();
        	$("#pg2-2-next").attr("onclick","submitToLegacySite();");
        	$("#pg2-2-next").removeAttr("disabled");
        	$("#pg2-2-next").show();
    	}
    	
    });
    
    //Disable the check for savings button if income is invalid
    $("#income").on('keyup keypress blur change', function(){
    	
    	var income = $(this).val();
    	if(!income || isNaN(income) || parseFloat(income) < 0.0){
    		$("#pg2-1-next").addClass("disabled");
    	}
    	else{
    		$("#pg2-1-next").removeClass("disabled");
    	}
    	
    });
    
    //Check income field on page load and enable/disable button
    var income = $("#income").val();
	if(!income || isNaN(income) || parseFloat(income) < 0.0){
		$("#pg2-1-next").addClass("disabled");
	}
	else{
		$("#pg2-1-next").removeClass("disabled");
	}
    
    //$("#ben-list a").click(function(){
    //	var that = $(this).parent("li").find("input[type=checkbox]");
    //	that.prop("checked",!that.prop("checked"));
    //});

    //Disable refresh on error
    /*$("#errorDialog button").click(function(e){
    	location.reload(true);
    });*/
    
    //Re-direct user based on pre-populated input
    
    //Zip and county code are entered
    var redirect = 'intro';
    if($("#formzip").val() && $("#countyCode").val()){
    	
    	redirect = 'page1';
    	
    	$("#getstarted").click();
    }	
        
    //Check DOB of all members
    var memberCount = parseInt($("#fam-cov").val());
    var dateOfBirth = '';
    for (var i = 1; i <= memberCount; i++) {
	   dateOfBirth = $("#bdaycov-" + i).val();
       redirect = 'page2';
       if(!dateOfBirth){
    	   redirect = 'page1';
    	   break;
       }
    }
        
    if(redirect == 'page2'){
    	$("#pg1-next").click();
    }
   
    //Check if income is entered
    var income = $("#income").val();
    income = parseInt(income);
    if(!isNaN(income) && income >= 0){
    	$("#income").val(income);
    	if(redirect == 'page2'){
    		$("#pg2-1-next").removeClass("disabled");
    		$("#pg2-1-next").click();
    	}
    }
    else{
    	$("#income").val('');
    }
    
    //Re-create household section if browser back is hit
	if(householdData && $("#formzip").val()){
		recreateHousehold(householdData);
	}	
    
    
//snap scroll becomes different for internet explorer    
//var scrollData = [0,0,true];
    if(ie == 8)
    {
    	//fix tooltip hover
    	$("[data-toggle=tooltip],[rel=tooltip]").each(function(){
//    		var sthis = $(this);
    		//$(".tooltip").css("opacity","0.1");
    		$(this).trigger("mouseover");
    		$(this).trigger("mouseout");
//    		setTimeout(function(){sthis.trigger("mouseout");
//    		$(".tooltip").css("height","1");
//    		},1);
    		});    	
//setTimeout(function(){$(".tooltip").css("visibility","visible");},10);
    //panelsnap for ie8
//    
//    //clicking on the progressbar
//    $("a[data-panel]").click(function(){
//    	//console.log("bef "+scrollData[2]);
//    	scrollData[2] = false;
//    	$("body").one("panelsnap:finish",function(){
//    		setTimeout(function(){scrollData[2] = true;},100);
//    		//console.log("aft "+scrollData[2]);
//    	});
//    });
//    
//    $(window).scroll(function (event) {
//      
//
//        //to prevent continous propagation
//        var ts = $.now();
//        if (ts - scrollData[0] >= 810  && scrollData[2]) {
//        	 
//    	//console.log("diff is"+($(window).scrollTop() - scrollData[1]));
//            //has scroll pos least distance to section, originally a huge number
//            var lst = 999999;
//            var currd = 0;
//            var currv = 0;
//            $("section").each(function () {
//                currd = $(window).scrollTop() - $(this).offset().top;
//                if (Math.abs(currd) < lst) {
//                    lst = currd;
//                    currv = $(this);
//                }
//            });
//          //  console.log("current: " + currv.attr("data-panel"));
//            var toClk;
//
//            //intro
//
//            //page4
//
//            if ($(window).scrollTop() > parseInt(scrollData[1])) {
//    //console.log("down");
//                toClk = currv.nextAll("section").first().attr("data-panel");
//                if (currv.attr("data-panel") == "intro") {
//                    toClk = "page1";
//                }
//            } else {
//    //console.log("up");
//                toClk = currv.prevAll("section").first().attr("data-panel");
//                if (currv.attr("data-panel") == "page4") {
//                    toClk = "page3";
//                }
//            }
//           // console.log("will click: " + toClk);
////            $(".progressbar a[data-panel=" + toClk + "]").click();
////            scrollData[1] = $(window).scrollTop();
//            scrollData[0] = ts;
//            
////            $("body").one("panelsnap:finish",function(){
////                scrollData[1] = $(window).scrollTop();
////                });
//            scrollData[2] = false;
//            //console.log(toClk);
//            //if scrolltop is undefined, prevent an error from being thrown by
//            //skipping the scroll
//            if(typeof toClk === "undefined")
//            	{
//            	scrollData[1]=$(window).scrollTop();
//            	scrollData[2]=true;
//            	return false;
//            	}
//            toClk = "section[data-panel="+toClk+"]";
//
//            $('html, body').animate({
//                scrollTop: $(toClk).offset().top
//             }, {
//            	 duration:800,
//            	 done:function(){
//            	 scrollData[1] = $(window).scrollTop();
//            	 scrollData[2] = true;
//            	 $(".progressbar a").removeClass("active");
//            	 $(".progressbar a[data-panel="+$(toClk).attr("data-panel")+"]").addClass("active");
//             },
//             easing: "easeOutExpo"
//             });
//            
//        }
//    	else
//    {
//    	
//    }
//
//    });


    
}    
}); //end document.ready

//Creating Request object for backend call
var homeRequest = '';
var previousHomeRequest = '';
var savedData = '';
//AJAX
function createRequest() {

    var noOfMembers = parseInt($("#fam-cov").val());

    phixHouseholdMembers.reset();
    
    for (var i = 1; i <= noOfMembers; i++) {
        member = new PhixMemberModel({
            dateOfBirth: $("#bdaycov-" + i).val(),
            isTobaccoUser: "N",
            isSeekingCoverage: "N",
        });

        if ($('input:checkbox[name="tobacco-' + i + '"]').is(':checked')) {
            member.set({
                isTobaccoUser: "Y"
            });
        }
        if ($('input:checkbox[name="cov-check-' + i + '"]').is(':checked')) {
            member.set({
                isSeekingCoverage: "Y"
            });
        }
        
        phixHouseholdMembers.add([member]);
    }
    
    var income = $("#income").val();
	
	if(!income){
		income = -1;
	}

    homeRequest = new PhixRequestModel({
        zipCode: $("#formzip").val(),
        countyCode: $("#countyCode").val(),
        stateCode:$("#stateCode").val(),
        familySize: noOfMembers,
        members: phixHouseholdMembers,
        householdIncome: income,
        email: $("#emailValue").val(),
        phone: $("#phoneValue").val()
    });

}
//validation for income
function incomeIsValid(){
	
	var income = $("#income").val();
	
	if(!income){
		$("#pg2-1-next").removeClass("disabled");
		return true;
	}
	else if(isNaN(income) || parseFloat(income) < 0.0){
		 $(".progressbar").find("[data-panel=page2]").click();
		 $("#income_error label").html("Please enter a valid income");
		 $("#income_error").show();
		 $("#pg2-1-next").addClass("disabled");
		 return false;
	}
	else if(parseFloat(income) > 9999999){
		 $(".progressbar").find("[data-panel=page2]").click();
		 $("#income_error label").html("Please enter a value below 10,000,000");
		 $("#income_error").show();
		 $("#pg2-1-next").addClass("disabled");
		 return false;
	}
	
	$("#income_error").hide();
	$("#pg2-1-next").removeClass("disabled");
	return true;
	
}

var planInfoAvailable = '';
//AJAX call to find savings
function calculateSavings(caller) {

    createRequest();
    
    if(!incomeIsValid()){
    	return;
    }
    
    //If income is not present skip call to API
    if(!$("#income").val()){
    	homeRequest.set({
    		callToApiRequired: 'N'
    	});
    }
    
    //AJAX call to caluclate APTC
    var pathURL = "calculateSavings";
    
    //Call API only if data is changed and income field is valid
    if(previousHomeRequest != JSON.stringify(homeRequest)){
    	
    	previousHomeRequest = JSON.stringify(homeRequest);
    	
    	/*
    	 * These values are added after comparison with previous request since a change in these values
    	 * will not require a call to API
    	 */
    	homeRequest.set({
    		docVisitFrequency : $('form input[name=pg3-doc-visit]:checked').val(),
    		noOfPrescriptions : $('form input[name=pg3-drug]:checked').val(),
    	});
    	
    	var benefits = '';
    	$("form input[name=benefits]:checked").each(function(){
    		benefits += $(this).val() + ',';
    	});
    	//Remove trailing comma
    	if(benefits){
    		benefits = benefits.substring(0, benefits.length - 1);
    	}
    	homeRequest.set({
    		benefits : benefits
    	});
    	
    	$("#pg2-3").fadeIn(100);
        $("#savingsButtons").hide();
    	
	    $.ajax({
	        type: "POST",
	        url: pathURL,
	        contentType: "application/json; charset=utf-8",
	        data: JSON.stringify(homeRequest),
	        success: function (data) {
	        	
	            savedData = JSON.stringify(data);
	            var response = new PhixRequestModel(JSON.parse(savedData));
	            
                $(".results").hide();
	            var status = response.get('status');
	            if (status == 'failure') {
	            	$("#resultsSection").fadeIn(function(){
		            	getResultHeightwithoutTimeout();
		            	resultPageHeight();
		            });
	            	$("#errorResult").fadeIn(100, function () {});
	                $("#callUsError").show();

	 	            $("#pg2-2-next").addClass("error");
	 	            $("#pg2-2-next").hide();
	 	            $("#float-eligibility-results").fadeOut(200);
	 	           
	 	            $("#pg2-3").fadeOut(100);
		            $("#savingsButtons").show();
	                return;
	            }
	            else{
	            	$("#pg2-2-next").attr("href","#");
	            	$("#pg2-2-next").removeClass("error");
	            }
	            
	            var maxAptc = response.get('aptc');
	            var aptcEligible = response.get('aptcEligible');
	            var expectedMonthlyPremium = response.get('expectedMonthlyPremium');
	            var lspPremiumAfterAptc = response.get('lspPremiumAfterAptc');
	            var noOfAptcCsr = response.get('noOfAptcCsr');
	            var medicaidEligible = response.get('medicaidEligible');
	            var medicareEligible = response.get('medicareEligible');
	            var chipEligible = response.get('chipEligible');
	            var chipHousehold = response.get('chipHousehold');
	            var medicareHousehold = response.get('medicareHousehold');
	            var csr = response.get('csrLevel');
	            $('#affid').val(response.get('affFlowCustomField1'));
	            $('#legacySiteUrl').val(response.get('legacySiteUrl'));
	            $("#page2-2buttons").hide();
	       
	           	var aptcVal = parseInt(maxAptc.replace(/\$/g, ''),10);
	            var premiumAfterAptc = parseInt(lspPremiumAfterAptc.replace(/\$/g, ''),10);
	            var expectedPremium = parseInt(expectedMonthlyPremium.replace(/\$/g, ''),10);
	            
	            planInfoAvailable = response.get('planInfoAvailable');
	            //If Submit button triggered this call, go to pre launch results page
	            /*if(caller == 'page4'){
	            	$("#phixRequest").val(savedData);
                    //This call is to update Lead Stage
	            	updateOtherInputsInEligLeadRecord();
	            	return;
	            }*/
	            
	            /*
	             * Results page priority
	             *  1. APTC/Expected monthly premium (w or w/o sufficient data)
				 *	2. No Savings
				 *	3. CSR
				 *	4. CHIP
				 *	5. Medicare
				 *	6. Medicaid
	             */
	            
	            //$("section[data-panel=page3],section[data-panel=page4]").show();
	            //$(".progressbar a[data-panel=page3],.progressbar a[data-panel=page4]").show();
	            
	            //Results div for PHIX1
	            var callUs = response.get('callUs');
	            $("#pg2-2-next").removeAttr("disabled");
	            if(callUs == 'LEGACY'){
	            	$('.legacyRadio').each(function(){
	            		if($(this).val() == 'Y'){
	            			$(this).prop("checked",true);
	            			$(this).trigger('change');
	            		}
	            	});
                	$("#pg2-2-next").html("Go Shopping");
                	$("#pg2-2-next").attr("onclick","submitToLegacySite()");
                	$("#pg2-2-next").show();
                }
	            else if(callUs == 'FFM'){
	            	
	            	if(planInfoAvailable == 'Y'){
	                	$("#pg2-2-next").html("Next: Personalize My Shopping");
	                	$("#pg2-2-next").attr("onclick","postToPlanDisplay()");
	                	$("#pg2-2-next").show();
	                	$("#noPlansAvailable").hide();
	            	}
	            	else{
	            		$("#pg2-2-next").hide();
	            		$("#noPlansAvailable").show();
	            	}
                }
	            else if(callUs == 'LEGACY_FFM'){
	            	
	            	$('.noSubsidyRadio').each(function(){
	            		if($(this).val() == 'N' && $(this).attr("name") == 'noSubsidyRadioOne'){
	            			$(this).prop("checked",true);
	            			$(this).trigger('change');
	            		}
	            		if($(this).val() == 'Y' && $(this).attr("name") == 'noSubsidyRadioTwo'){
	            			$(this).prop("checked",true);
	            			$(this).trigger('change');
	            		}
	            	});
	            	
	            	if(planInfoAvailable == 'Y'){	            	
		            	$("#pg2-2-next").html("Go Shopping");
	                	$("#pg2-2-next").attr("onclick","postToPlanDisplay()");
	                	$("#pg2-2-next").show();
	                	$("#noPlansAvailable").hide();
	            	}
	            	else{
	            		$("#pg2-2-next").hide();
	            		$("#noPlansAvailable").show();
	            	}
	            }
                else if(callUs == 'CALL'){
                	$("#callUs").show();
                	$("#callUs .stateName").html(response.get('stateName'));
                	$("#pg2-2-next").removeAttr("onclick");
                	$("#pg2-2-next").hide();
                }
                else if(callUs == 'SBE'){
                	$("#sbeAndAnswers").show();
                	$("#sbeAndAnswers .stateName").html(response.get('stateName'));
                	$("#pg2-2-next").html("Next: My state's website");
                	$("#pg2-2-next").attr("href",response.get('sbeWebsite'));
                	$("#sbeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
                	$("#pg2-2-next").removeAttr("onclick");
                	$("#pg2-2-next").show();
                }
                else if(callUs == 'PUBLIC'){
                	$("#exchangeAndAnswers").show();
                	$("#exchangeAndAnswers .stateName").html(response.get('stateName'));
                	$("#pg2-2-next").html("Next: Healthcare.gov");
                	$("#pg2-2-next").attr("href","http://www.healthcare.gov");
                	$("#exchangeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
                	$("#pg2-2-next").removeAttr("onclick");
                	$("#pg2-2-next").show();
                }
	            
	            var eligibilityResults = null;
	            var count=0;
	            //APTC with sufficient data i.e APTC > $0
	            if(!isNaN(aptcVal) && aptcVal > 0){
	            	
	            	//Show title
	            	if($('#fam-cov').val() > 1){
	            		$('#familyCongratulations').show();
	            	}else {
	            		$('#singleCongratulations').show();
	            	}
	            	
	            	//Show max aptc (monthly and yearly)
	            	$('.maxAptc').html('$'+aptcVal);
	            	$("#maxAptcYearly").html(aptcVal * 12);
	            	
	            	//Show greater of LSP premium and no. of members seeking coverage
	            	$('#lspPremium').html('$'+noOfAptcCsr);
	            	if(!isNaN(premiumAfterAptc) && premiumAfterAptc > 0){
	            		$('#lspPremium').html('$'+premiumAfterAptc);
	            	}
	            	
	            	eligibilityResults = '$'+aptcVal+'/month of tax credits';
	            	
	            	//Check for CSR
	            	$("#csrResults").hide();
	            	if (csr == 'CS4' || csr == 'CS5' || csr == 'CS6') {
	            		 count++;
	                     $("#csrResults").show();
	                     if(eligibilityResults){
	                    	 eligibilityResults+=', Cost-sharing reduction';
	                     }
	                     else{
	                    	 eligibilityResults='Cost-sharing reduction';
	                     }
	                }
	            	count++;
	            	$("#aptcAllPlans").show();
	            	
	            	//CHIP
	            	if (chipEligible == 'Y') {
	            		count++;
	                    $("#chipResults").show();
	                    if(response.get('noOfChip') == 1){
	                    	$("#chipTextOne").show();
	                    	$("#chipHeaderOne").show();
	                    }
	                    else{
	                    	$("#chipTextTwo").show();
	                    	$("#chipHeaderTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', CHIP';
	                    }
	                    else{
	                    	 eligibilityResults='CHIP';
	                    }
	                }
	            	
	                //Medicare
	                if (medicareEligible == 'Y') {
	                	count++;
	                    $("#medicareResults").show();
	                    if(response.get('noOfMedicare') == 1){
	                    	$("#medicareTextOne").show();
	                    }
	                    else{
	                    	$("#medicareTextTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicare';
	                    }
	                    else{
	                    	 eligibilityResults='Medicare';
	                    }
	                }
	                
	            	//Medicaid
	                if (medicaidEligible == 'Y') {
	                	count++;
	                    $("#medicaidResults").show();
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicaid';
	                    }
	                    else{
	                    	 eligibilityResults='Medicaid';
	                    }
	                }
	            }
	            
	            //APTC Eligible but aptcVal =0
	            else if(aptcEligible=='Y' && aptcVal <= 0){
	            	
	            	//Show title
	            	if($('#fam-cov').val() > 1){
	            		$('#familySizeText').html('your household');
	            	}else {
	            		$('#familySizeText').html('you');
	            	}
	            	
            		$('#aptcEligibleNoAmount').show();
            		
            		//Show max aptc (monthly and yearly)
	            	$('.maxAptc').html('$'+aptcVal);
	            	$("#maxAptcYearly").html(aptcVal * 12);
	            	
	            	//Show county name and state code
	            	$(".aptcAllPlansNoAptcCountyName").html($('#countyName').val());
	            	$(".aptcAllPlansNoAptcStateName").html($('#stateCode').val());
	            	            	
	            	//Show greater of LSP premium and no. of members seeking coverage
	            	$('#lspPremiumVal').html('$'+noOfAptcCsr);
	            	if(!isNaN(premiumAfterAptc) && premiumAfterAptc > 0){
	            		$('#lspPremiumVal').html('$'+premiumAfterAptc);
	            	}
	            	
	            	//Check for CSR
	            	$("#csrResults").hide();
	            	if (csr == 'CS4' || csr == 'CS5' || csr == 'CS6') {
	            		count++;
	                     $("#csrResults").show();
	                     if(eligibilityResults){
	                    	 eligibilityResults+=', Cost-sharing reduction';
	                     }
	                     else{
	                    	 eligibilityResults='Cost-sharing reduction';
	                     }
	                }
	            	count++;
	            	$("#aptcAllPlansNoAptc").show();
	            	
	            	//CHIP
	            	if (chipEligible == 'Y') {
	            		count++;
	                    $("#chipResults").show();
	                    if(response.get('noOfChip') == 1){
	                    	$("#chipTextOne").show();
	                    	$("#chipHeaderOne").show();
	                    }
	                    else{
	                    	$("#chipTextTwo").show();
	                    	$("#chipHeaderTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', CHIP';
	                    }
	                    else{
	                    	 eligibilityResults='CHIP';
	                    }
	                }
	            	
	                //Medicare
	                if (medicareEligible == 'Y') {
	                	count++;
	                    $("#medicareResults").show();
	                    if(response.get('noOfMedicare') == 1){
	                    	$("#medicareTextOne").show();
	                    }
	                    else{
	                    	$("#medicareTextTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicare';
	                    }
	                    else{
	                    	 eligibilityResults='Medicare';
	                    }
	                }
	                
	            	//Medicaid
	                if (medicaidEligible == 'Y') {
	                	count++;
	                    $("#medicaidResults").show();
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicaid';
	                    }
	                    else{
	                    	 eligibilityResults='Medicaid';
	                    }
	                }
	                
	                //If no CSR/public programs
	                if(!eligibilityResults){
	                	eligibilityResults = 'Not eligible for financial help';
	                }
	            }
	            
	            //APTC is N/A but expected premium is not
	            else if(isNaN(aptcVal) && !isNaN(expectedPremium)){
	            	
	            	//Show title
	            	if($('#fam-cov').val() > 1){
	            		$('#familyCongratulations').show();
	            	}else {
	            		$('#singleCongratulations').show();
	            	}
	            	
	            	//Show expected monthly premium
	            	$('.expectedMonthlyPremium').html('$'+expectedPremium);
	            	
	            	//Check for CSR
	            	$("#csrResults").hide();
	            	if (csr == 'CS4' || csr == 'CS5' || csr == 'CS6') {
	            		count++;
	                     $("#csrResults").show();
	                     if(eligibilityResults){
	                    	 eligibilityResults+=', Cost-sharing reduction';
	                     }
	                     else{
	                    	 eligibilityResults='Cost-sharing reduction';
	                     }
	                }
	               
	            	//CHIP
	            	if (chipEligible == 'Y') {
	            		count++;
	                    $("#chipResults").show();
	                    if(response.get('noOfChip') == 1){
	                    	$("#chipTextOne").show();
	                    	$("#chipHeaderOne").show();
	                    }
	                    else{
	                    	$("#chipTextTwo").show();
	                    	$("#chipHeaderTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', CHIP';
	                    }
	                    else{
	                    	 eligibilityResults='CHIP';
	                    }
	                }
	            	
	                //Medicare
	                if (medicareEligible == 'Y') {
	                	count++;
	                    $("#medicareResults").show();
	                    if(response.get('noOfMedicare') == 1){
	                    	$("#medicareTextOne").show();
	                    }
	                    else{
	                    	$("#medicareTextTwo").show();
	                    }
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicare';
	                    }
	                    else{
	                    	 eligibilityResults='Medicare';
	                    }
	                }
	                
	            	//Medicaid
	                if (medicaidEligible == 'Y') {
	                	count++;
	                    $("#medicaidResults").show();
	                    if(eligibilityResults){
	                    	 eligibilityResults+=', Medicaid';
	                    }
	                    else{
	                    	 eligibilityResults='Medicaid';
	                    }
	                }
	            	
	                //If no CSR/public programs
	                if(!eligibilityResults){
	                	eligibilityResults = 'Not eligible for financial help';
	                }
	                count++;
	            	$("#aptcNoPlans").show();
	            }
	            
	            
	            //No savings case - No APTC, No CSR, No public programs
	            else if ((isNaN(aptcVal) || aptcVal <= 0) && chipEligible != 'Y' 
	            	&& medicareEligible != 'Y' && medicaidEligible != 'Y'
	            		&& chipHousehold != 'Y' && medicareHousehold != 'Y') {
	            	
	            	//Show title
	            	if($('#fam-cov').val() > 1){
	            		$('#noSubsidyFamily').show();
	            	}else {
	            		$('#noSubsidySingle').show();
	            	}
	            	
	            	eligibilityResults='Not eligible for financial help';
	            	
	            	//$("#noSavingsResultsOne").show();
	            	//$("#noSavingsResultsTwo").show();
	            	
	            	if(callUs == 'LEGACY'){
	                	$("#legacy").show();
	                }
		            else if(callUs == 'LEGACY_FFM'){
	                	$("#legacyOrFfm").show();
		            }
	                else if(callUs == 'SBE'){
	                	$("#sbeAndAnswers").show();
	                	$("#sbeAndAnswers .stateName").html(response.get('stateName'));
	                	$("#noAptcSbe").show();
	                	$("#pg2-2-next").html("Next: My state's website");
	                	$("#pg2-2-next").attr("href",response.get('sbeWebsite'));
	                	$("#sbeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
	                	$("#pg2-2-next").removeAttr("onclick");
	                	$("#pg2-2-next").show();
	                }
                	
                	if(!eligibilityResults){
	                	eligibilityResults = 'Not eligible for financial help';
	                }
	            }
	            
	            //No savings case - No APTC, No CSR but some/all public program eligible members
	            else if ((isNaN(aptcVal) || aptcVal <= 0) && 
	            		(chipEligible == 'Y' || medicareEligible == 'Y' || medicaidEligible == 'Y')) {
	            	
	            	//Some public program eligible members
	            	if(chipHousehold != 'Y' && medicareHousehold != 'Y' && medicaidEligible != 'Y'
	            		&& response.get('noOfChip') + response.get('noOfMedicare') != $('#fam-cov').val()){
	            		count++;
	            		//Show title
		            	if($('#fam-cov').val() > 1){
		            		$('#noSubsidyFamily').show();
		            	}else {
		            		$('#noSubsidySingle').show();
		            	}
		            	$("#noAptcSbe").show();
		            	if(response.get('sbeWebsite') && response.get('sbeWebsite') != 'N/A'){
		                	$("#sbeAndAnswers").show();
		                	$("#sbeAndAnswers .stateName").html(response.get('stateName'));
		                	$("#pg2-2-next").html("Next: My state's website");
		                	$("#pg2-2-next").attr("href",response.get('sbeWebsite'));
		                	$("#sbeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
		                	$("#pg2-2-next").removeAttr("onclick");
		                	$("#pg2-2-next").show();
		                }
		            	else{
		            		$("#noAptcExchangeAndAnswers").show();
		            		$("#pg2-2-next").html("Next: Healthcare.gov");
		                	$("#pg2-2-next").attr("href","http://www.healthcare.gov");
		                	$("#noAptcExchangeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
		                	$("#pg2-2-next").removeAttr("onclick");
		                	$("#pg2-2-next").show();
		            	}
	            	}
	            	//All public program eligible members
	            	else{
		            	//Show title
		            	if($('#fam-cov').val() > 1){
		            		$('#familyCongratulations').show();
		            	}else {
		            		$('#singleCongratulations').show();
		            	}
		            	
		            	//CHIP
		            	if (chipEligible == 'Y') {
		            		count++;
		                    $("#chipResults").show();
		                    if(response.get('noOfChip') == 1){
		                    	$("#chipTextOne").show();
		                    	$("#chipHeaderOne").show();
		                    }
		                    else{
		                    	$("#chipTextTwo").show();
		                    	$("#chipHeaderTwo").show();
		                    }
		                    if(eligibilityResults){
		                    	 eligibilityResults+=', CHIP';
		                    }
		                    else{
		                    	 eligibilityResults='CHIP';
		                    }
		                }
		            	
		                //Medicare
		                if (medicareEligible == 'Y') {
		                	count++;
		                    $("#medicareResults").show();
		                    if(response.get('noOfMedicare') == 1){
		                    	$("#medicareTextOne").show();
		                    }
		                    else{
		                    	$("#medicareTextTwo").show();
		                    }
		                    if(eligibilityResults){
		                    	 eligibilityResults+=', Medicare';
		                    }
		                    else{
		                    	 eligibilityResults='Medicare';
		                    }
		                }
		                
		            	//Medicaid
		                if (medicaidEligible == 'Y') {
		                	count++;
		                    $("#medicaidResults").show();
		                    if(eligibilityResults){
		                    	 eligibilityResults+=', Medicaid';
		                    }
		                    else{
		                    	 eligibilityResults='Medicaid';
		                    }
		                }
		                
		                if(callUs == 'SBE'){
		                	$("#sbeAndAnswers").show();
		                	$("#sbeAndAnswers .stateName").html(response.get('stateName'));
		                	$("#pg2-2-next").html("Click Next: My state's website");
		                	$("#pg2-2-next").attr("href",response.get('sbeWebsite'));
		                	$("#sbeAndAnswers a").attr("href","http://www.getinsured.com/answers/" + response.get('stateName').replace(/ /g, '-').toLowerCase()+"/");
		                	$("#pg2-2-next").removeAttr("onclick");
		                	$("#pg2-2-next").show();
		                }
	            	}
	            	
	            	
	            }
	            
	            if(!eligibilityResults){
                	eligibilityResults = 'Not eligible for financial help';
                }
                $("#float-eligibility-results").html("<span>Financial Help: </span>"+eligibilityResults).fadeIn(200);
	            $("#resultsSection").fadeIn(function(){
	            	getResultHeightwithoutTimeout();
	            	resultPageHeight();
	            });
	            
	           
	            if(count==1){
	            	$('.resultwrap').each(function(){
	            		if($(this).height()>0){
	            			$(this).removeClass("span3 span6 span4").addClass("span9");
	            			//$(this).css("display","inline-block");	    		
	            		}	
	            	});	
	            }else if(count==2){
	            	$('.resultwrap').each(function(){	            		
	            		if($(this).height()>0){
	            			$(this).removeClass("span3 span9 span4").addClass("span6");
	            			$(this).css("margin","0");	
	            		}
	            	});
	            }else if(count==3){
	 	           	$('.resultwrap').each(function(){	            		
	 	           		if($(this).height()>0){
	 	           			$(this).removeClass("span3 span6 span9").addClass("span4");
	 	           			$(this).css("margin","0");	
	 	           		}
	            	});
	            }else{
	            	$('.resultwrap').each(function(){	            		
	            		if($(this).height()>0){
	            			$(this).removeClass("span9 span6 span4").addClass("span3");
	            			$('#noSubsidy').removeClass("span3");
	            		        		
	            		}	
	            	});
	            };
	            
	            $("#pg2-3").fadeOut(100);
	            $("#savingsButtons").show();
	            
	            //paco:improve tabindex
	            $("div.activeResult").find("a,input,button,.custom-rc").first().focus();
	            
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	        	$("#resultsSection").fadeIn(function(){
	            	getResultHeightwithoutTimeout();
	            	resultPageHeight();
	            });
	            $("#errorResult").fadeIn(100, function () {});
	            $("#callUsError").show();
                $("#pg2-2-next").attr("href","#");
 	            $("#pg2-2-next").addClass("error");
 	            $("#float-eligibility-results").fadeOut(200);
	            $("#pg2-3").fadeOut(100);
	            $("#savingsButtons").show();
	            previousHomeRequest = '';
	        }
	    });
    }
    //Show result of last call to API
    else{
    	 $("#resultsSection").fadeIn(function(){
         	getResultHeightwithoutTimeout();
         	changeSectionHeight=1;
         	resultPageHeight();
         });
    	 $("#pg2-3").fadeOut(100);
         $("#savingsButtons").show();
    	 if(caller == 'page4'){   
    		
    		var newRequest = new PhixRequestModel(JSON.parse(savedData));
    		
        	 /* We are creating a new request to submit to pre launch page since old request has become stale
        	 * if call to API is skipped
        	 * */
        	 
    		newRequest.set({
        		docVisitFrequency : $('form input[name=pg3-doc-visit]:checked').val(),
        		noOfPrescriptions : $('form input[name=pg3-drug]:checked').val(),
        	});
        	
        	var benefits = '';
        	$("form input[name=benefits]:checked").each(function(){
        		benefits += $(this).val() + ',';
        	});
        	//Remove trailing comma
        	if(benefits){
        		benefits = benefits.substring(0, benefits.length - 1);
        	}
        	newRequest.set({
        		benefits : benefits
        	});
    		
    		$("#phixRequest").val(JSON.stringify(newRequest));
    		updateOtherInputsInEligLeadRecord();
    	}
        
    }

}

//This methods populates the form for submitting to legacy site
function submitToLegacySite(){
	
	//validate zip 
    if (!$("#page1-form").valid()) {
        $(".progressbar").find("[data-panel=intro]").click();
    } 
    //validate houshold
    else if (!bdayValid()) {
        $(".progressbar").find("[data-panel=page1]").click();
    }
    else{
    	//Do a get call to legacy site 	
    	var href = $("#legacySiteUrl").val()+'zipcode='+$("#formzip").val()+'&affid='+$("#affid").val();
    	$("#frmAffIndQuote").attr("action",href);
    	updateStageInEligLeadRecord('legacy');
		$("#frmAffIndQuote").submit();
    }
	
}

//Re-loads household section when back button is clicked
function recreateHousehold(householdData){
	
	var cpt = $("#chk-plan-table");
	
	for (var i=0; i<householdData.length;i++){
		
		if(i != 0){
			cpt.find("tbody tr").last().cloneH().appendTo("#chk-plan-table tbody");
		}
		var ind = (i + 1);
        cpt.find("tbody tr span.add-on").last().html("");
        cpt.find("tbody tr input[type=text]").last().val(householdData[i].dateOfBirth).attr("id", "bdaycov-" + ind).attr("name", "bdaydate" + ind).attr("value", "");
        cpt.find("tbody tr input[type=text]").last().mask("?99/99/9999", {
            placeholder: "-"
        });
        cpt.find("tbody tr .bday_error").last().attr("id", "bdaycov-" + ind + "_error").html("");
        
        cpt.find("tbody tr .tobacco").last().attr("id", "tobacco-" + ind);
        cpt.find("tbody tr .tobacco").last().attr("name", "tobacco-" + ind);
        cpt.find("tbody tr .tobacco").last().removeAttr("disabled");
        if(householdData[i].isTobaccoUser == 'Y'){
            cpt.find("tbody tr .tobacco").last().attr("checked","checked");
        }
        else{
        	cpt.find("tbody tr .tobacco").last().removeAttr("checked");
        }
        
        cpt.find("tbody tr .cov-check").last().attr("id", "cov-check-" + ind);
        cpt.find("tbody tr .cov-check").last().attr("name", "cov-check-" + ind);
        cpt.find("tbody tr .cov-check").last().removeAttr("disabled");
        cpt.find("tbody tr .cov-check").last().next().removeClass("disabled");
        if(householdData[i].isSeekingCoverage == 'Y'){
        	cpt.find("tbody tr .cov-check").last().attr("checked", "checked");
        }
        else{
        	cpt.find("tbody tr .cov-check").last().removeAttr("checked");
        }
        
        cpt.find("tbody tr .remove-button").last().html("<a href='javascript:void(0)' class='cov-remove'>REMOVE</a>");
        

        //bind the remove click function to the "X"
        cpt.find("tbody tr").last().find(".cov-remove").click(covRemoveClick);
        checkBind();
	}
	
	
}




function getResultHeightwithoutTimeout() {
	//result div height
	var viewportHeight = $(window).height();
	var buttom=$("#savingsButtons").height();
	var progressbar=$("#progressbarwrap").height();	
	var navbar=$(".navbar") .height();
	
	var resultHeight=viewportHeight-buttom-progressbar-navbar;
	
	$("#resultsMain").css("height",resultHeight-50);
	
	
	//span3 height=50% * result div height
	$("#resultsMain div.row-fluid div.resultwrap").css("height",resultHeight*0.5);
	var resultWrapHeight = $("#resultsMain div.row-fluid div.resultwrap ").height();
	
	var number=0;
	$('.topwrap').each(function(){
		topwrapHeight = $(this).height();
		var nanoheight = resultWrapHeight-topwrapHeight-15;
		$(this).next().css("height",nanoheight);
		if(topwrapHeight>0){
			number++;
		}
		
		
		//refresh
		$(".nano").nanoScroller({ alwaysVisible: true });
		setTimeout(function(){
		$(".nano").nanoScroller({scroll : 'top'})
		.nanoScroller({flash: 'true'})
		.nanoScroller({ alwaysVisible: true });
		},100);
		
	});	
}

$(document).ready(function(){
	var resizeTimer;
	
	$(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(getResultHeightwithoutTimeout, 250);
    });
	
	//getResultHeightwithoutTimeout();	
	
});

var changeSectionHeight=1;
function resultPageHeight(){
	//if($("#resultsSection").is(":visible")){
		var resultsMainHeight = $("#resultsMain").height();
		var resultDivsHeight = $("#resultDivs").height();
		var savingsButtonsHeight = $("#savingsButtons").height();
		var callUsHeight = $("#callUs").parent().height();
		var resultsTitleHeight = $(".resultsTitle").height();
		
		//console.log("resultsTitleHeight"+resultsTitleHeight);
		
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();
		
		var actualSectionHeight = viewportHeight-resultsMainHeight+resultDivsHeight+callUsHeight+savingsButtonsHeight+resultsTitleHeight+30;
		if((resultsMainHeight-resultsTitleHeight)<=resultDivsHeight){
			$("section[data-panel=page2]").css("height",actualSectionHeight);							
			//changeSectionHeight=0;
		}else{
			$("section[data-panel=page2]").css("height","100%");
			//$(".progressbar").find("[data-panel=page2]").click();
			//changeSectionHeight=1;
		}
		
	//}
	//	console.log("resultsMainHeight:"+resultsMainHeight);
	//	console.log("resultDivsHeight:"+resultDivsHeight);
		

}

$(document).ready(function(){
	var resizeTimer;
	
	$(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resultPageHeight, 250);
    });
	
});



function familyTableHeight(){
	//result div height
	var viewportHeight = $(window).height();
	var buttom=$(".next-button-wrap").height();
	var progressbar=$("#progressbarwrap").height();	
	var navbar=$(".navbar") .height();
	
	var tableHeight=viewportHeight-buttom-progressbar-navbar;
	
	$("#form-cov .nano").css("height",tableHeight*0.55);
}

$(document).ready(function(){
	var resizeTimer;
	
	$(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(familyTableHeight, 250);
    });
	
	familyTableHeight();		
});



function postToPrelaunch(){
	//$("#page4Form").submit();
	History.pushState({household: phixHouseholdMembers},null,ctx+'/');
	calculateSavings('page4');
}

//$(document).ready(function(){
//	$(".brand").click(function(){
//		event.preventDefault();
//		$(".progressbar").find("[data-panel=intro]").click();
//	});
//});



//if input is changed, show calculation page and hide result page
$(document).ready(function(){
	$("#formzip").change(function(){
	    	showCalculationPage();
	    
	  });
	
	$('#chk-plan-table').on("change","input.bdate, input:checkbox",function(){
			 showCalculationPage();	  
	 });
	
	$('#chk-plan-table').on("click",".cov-remove",function(){
			 showCalculationPage();	
	 });
});

function showCalculationPage(){
	if(!$("#pg2-1").is(':visible')){
		$("#resultsSection").fadeOut(100, function () {
	        $("#savingsButtons").fadeOut(100, function () {});
	        $("#pg2-1").fadeIn(800, function () {});
	        $("section[data-panel=page2]").css("height","100%");
	        $("#pg2-1-next").parents(".next-button-wrap").first().show();
	    });
	}
}
