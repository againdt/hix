<script type="text/ng-template" id="changeinaddress-complete.jsp">
  <div class="gutter10">

    <spring:message code="label.lce.changeAddressComplete"/>

    <!-- BACK & NEXT BUTTON -->
     <dl class="dl-horizontal pull-right">
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/> {{nextButtonLifeEvent}}</a>
    </dl>
  </div>
</script>