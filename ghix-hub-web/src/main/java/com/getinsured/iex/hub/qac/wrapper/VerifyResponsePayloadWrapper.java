package com.getinsured.iex.hub.qac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.EDSResponseSetType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.VerifyResponsePayloadType;

public class VerifyResponsePayloadWrapper implements JSONAware {
	private ResponseMetadataWrapper metadata;
	private InsuranceApplicantResponseWrapper applicant;
	

	public VerifyResponsePayloadWrapper(VerifyResponsePayloadType response){
		if(response != null){
			EDSResponseSetType x = response.getEDSResponseSet();
			if(x != null){
				this.applicant = new InsuranceApplicantResponseWrapper(x.getResponseApplicant());
			}
			ResponseMetadataType  responseMetadata = response.getResponseMetadata();
			
			if(responseMetadata != null)
			{
				this.metadata = new ResponseMetadataWrapper(responseMetadata);
			}
		}
	}
	
	public InsuranceApplicantResponseWrapper getApplicant() {
		return applicant;
	}
	
	public ResponseMetadataWrapper getResponseMetadata(){
		return this.metadata;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.applicant != null){
			obj.put("EDSResponseSet", this.applicant);
		}
		if(this.metadata != null){
			obj.put("ResponseMetadata", this.metadata);
		}
		return obj.toJSONString();
	}

}
