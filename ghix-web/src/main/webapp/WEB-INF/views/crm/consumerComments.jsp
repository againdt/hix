<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>
<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#commentsMenu").addClass("active navmenu");
	});
    $(function() {
    	$(".newcommentiframe").click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if (href.indexOf('#') != 0) {
            	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
    		}
    	});
    });
    
    function closeCommentBox() {
    	$("#newcommentiframe").remove();
    	window.location.reload(true);
    }
    function closeIFrame() {
    	$("#newcommentiframe").remove();
    	window.location.reload(true);
    }
</script>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>

<jsp:include page="sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>
<div class="gutter10-lr">
    <div class="l-page-breadcrumb" style="margin-top:10px;">
        <!--start page-breadcrumb -->
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:history.back()">
                        &lt; 
                        <spring:message code="label.back" />
                    </a>
                </li>
                <li>
                    <a href="
                    <c:url value="/crm/member/viewmember/${householdId}"/>
                    ">
                    <spring:message code="label.account" />
                    </a>
                </li>
                <li>${household.firstName}&nbsp;${household.lastName}</li>
            </ul>
            <div style="font-size: 14px; color: red">
                <c:if test="${errorMsg != ''}">
                    <p>
                        <c:out value="${errorMsg}"></c:out>
                    <p />
                </c:if>
                <br>
            </div>
        </div>
        <!--  end of row-fluid -->
    </div>
    <!--  end l-page-breadcrumb -->
    <div class="row-fluid">
		<h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
    </div>
    <div class="row-fluid">
        <div id="sidebar" class="span3">
		           <jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
		</div>
		
        <div id="rightpanel" class="span9">
            <div class="header">
                <h4>Comments</h4>
            </div>
            <div class="gutter10">
                <form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="2" class="">
                                    <comment:view targetId="${householdId}" targetName="CONSUMER">
                                    </comment:view>
                                    <jsp:include page="../platform/comment/addcomments.jsp">
                                        <jsp:param value="" name=""/>
                                    </jsp:include>
                                    <input type="hidden" value="CONSUMER" id="target_name" name="target_name" /> 
                                    <input type="hidden" value="${householdId}" id="target_id" name="target_id" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="consumerName" id="consumerName" value="${household.firstName} ${household.lastName}" />
                </form>
            </div>
        </div>
    </div>
</div>
<form name="dialogForm" id="dialogForm" method="POST" action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
    <div class="markCompleteHeader">
        <div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
    <div class="modal-body clearfix gutter10-lr">
        <div class="control-group">
            <div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the Member.<br/>
                Proceed to Member view?
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
	        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' />
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />
        <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
    </div>
</div>
</form>