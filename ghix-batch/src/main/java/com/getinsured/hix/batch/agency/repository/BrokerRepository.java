package com.getinsured.hix.batch.agency.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;

@Repository
@Transactional(readOnly = true)
public class BrokerRepository implements IGHIXCustomRepository<Broker, Integer>{

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerRepository.class);
	
	@Autowired
	private IBrokerRepository iBrokerRepository;
	@Autowired 
	private UserService userService;
	
	@Override
	public boolean recordExists(Integer id) {
		return iBrokerRepository.exists(id);
	}

	@Override
	@Transactional(readOnly=false)
	public void updateUser(AccountUser accountUser, Integer id) {
		
		try {
			Broker existingBroker = iBrokerRepository.findOne(id);
			userService.createModuleUser(id, ModuleUserService.BROKER_MODULE, accountUser, true);
			if(existingBroker != null)
			{
				existingBroker.setUser(accountUser);
				iBrokerRepository.save(existingBroker);
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating Module user for broker :" , e);
		}
	}

}
