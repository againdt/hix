

// Services and utils

import API from '../services/API';

// Constants

import { APPLICATION_STATUSES, USER_MODES } from '../constants/applicationStatusesAndModes';
import ENDPOINTS from '../constants/endPointsList';
import EXCEPTIONS from '../constants/exceptionList';
import FLOW_TYPES, { ENVIRONMENT_TYPES } from '../constants/flowTypes';
import MODAL_TYPES from '../constants/modalTypes';

// Actions Types

import {
    activePageActionTypes,
    bloodRelationshipsTypes,
    commonActionTypes,
    consoleActionTypes,
    contextActionTypes,
    countiesActionTypes,
    countriesActionTypes,
    dataActionTypes,
    errorActionTypes,
    exceptionActionTypes,
    instructionsActionTypes,
    flowActionTypes,
    formActionTypes,
    headerFooterLinkTypes,
    languageActionTypes,
    methodsActionTypes,
    memberAffordabilityTypes,
    messagesActionTypes,
    modalActionTypes,
    adjustedFlowActionTypes,
    scrollActionTypes,
    sessionActionTypes,
    spinnerActionTypes,
    switchDirectionsActionTypes,
    tribesActionTypes
} from '../constants/actionTypes';


/*************************

 # Sync action creators

 *************************/




// // Here is boilerplate reducing solution that creates ActionCreators in single line
//
// function makeSyncActionCreator(type, ...argNames) {
//     return (...args) => {
//         const action = { type };
//         argNames.forEach((arg, index) => {
//             action[argNames[index]] = args[index];
//         });
//         return action;
//     };
// }
//
// // Example of usage(creates changePage ActionCreator):
//
// export const changePage = makeSyncActionCreator(activePageActionTypes.CHANGE_PAGE, 'page');


export const addContext = (context, name) => ({
    type: contextActionTypes.ADD_CONTEXT,
    context,
    name
});

export const changeData = newState => ({
    type: dataActionTypes.CHANGE_DATA,
    newState
});

export const changeDirection = direction => ({
    type: switchDirectionsActionTypes.CHANGE_DIRECTION,
    direction
});

export const changeInputStatus = (input, formName) => ({
    type: formActionTypes.CHANGE_INPUT_STATUS,
    input,
    formName
});

export const changeLanguage = language => async dispatch => {

    // TODO: Ask Khoa why do we need this GET request at all?

    const data = await API.getData(`${window.location.origin}${ENDPOINTS.CHANGE_LANGUAGE}?lang=${language}`, dispatch, actions);
    data && dispatch({ type: languageActionTypes.CHANGE_LANGUAGE, language });
};

export const changePage = (page, direction = undefined) => dispatch => {
    switch(page) {
    case 'increase':
        dispatch(changeDirection(direction ? direction : 'increase')); break;
    case 'decrease':
        dispatch(changeDirection(direction ? direction : 'decrease')); break;
    default:
        dispatch(changeDirection(direction ? direction : 'exact')); break;
    }
    return dispatch({
        type: activePageActionTypes.CHANGE_PAGE,
        page
    });
};

export const adjustFlow = value => ({
    type: adjustedFlowActionTypes.ADJUST_FLOW,
    value
});

export const clearFormSubmission = formName => ({
    type: formActionTypes.CLEAR_SUBMISSION,
    formName
});

export const clearMethods = owner => ({
    type: methodsActionTypes.CLEAR_METHODS,
    owner
});

export const clearScrollToElement = () => ({
    type: scrollActionTypes.CLEAR_ELEMENT_FOR_SCROLLING
});

export const clearState = () => ({
    type: commonActionTypes.CLEAR_STATE
});

export const hideAllModals = () => ({
    type: modalActionTypes.HIDE_ALL_MODALS
});

export const deleteForm = formName => ({
    type: formActionTypes.REMOVE_FORM,
    formName
});

export const hideModal = modalOwner => ({
    type: modalActionTypes.HIDE_MODAL,
    modalOwner
});

export const registerInput = (input, formName) => ({
    type: formActionTypes.REGISTER_INPUT,
    input,
    formName
});

export const reRegisterInput = (input, formName) => ({
    type: formActionTypes.RE_REGISTER_INPUT,
    input,
    formName
});

export const removeContext = name => ({
    type: contextActionTypes.REMOVE_CONTEXT,
    name
});

export const removeCounty = index => ({
    type: countiesActionTypes.REMOVE_COUNTY,
    index
});

export const removeError = () => ({
    type: errorActionTypes.REMOVE_ERROR
});

export const removeException = () => ({
    type: exceptionActionTypes.REMOVE_EXCEPTION,
});

export const removeInput = (input, formName) => ({
    type: formActionTypes.REMOVE_INPUT,
    input,
    formName
});

export const scrollToElement = element => ({
    type: scrollActionTypes.DEFINE_ELEMENT_FOR_SCROLLING,
    element
});

export const setBloodRelationships = bloodRelationships => ({
    type: bloodRelationshipsTypes.SET_RELATIONS,
    bloodRelationships
});

export const setBloodRelationshipsWholeList = bloodRelationships => ({
    type: bloodRelationshipsTypes.SET_RELATIONS_WHOLE_LIST,
    bloodRelationships
});

export const setCounties = (counties, state) => ({
    type: countiesActionTypes.SET_COUNTIES,
    counties,
    state
});

export const setCounty = (payload, pageName) => ({
    type: countiesActionTypes.SET_COUNTY_FOUND_BY_STATE_AND_ZIPCODE,
    payload,
    pageName
});

export const setCountries = countries => ({
    type: countriesActionTypes.SET_COUNTRIES,
    countries
});

export const setData = data => ({
    type: dataActionTypes.SET_DATA,
    data
});

export const setEnvironments = headerLinks => ({
    type: headerFooterLinkTypes.SET_ENVIRONMENTS,
    headerLinks
});

export const setInstructions = instructions => ({
    type: instructionsActionTypes.SET_INSTRUCTIONS,
    instructions
});

export const setMemberAffordabilty = affordabilityData => ({
    type: memberAffordabilityTypes.SET_MEMBER_AFFORDABILITY,
    affordabilityData
});

export const setError = errorData => ({
    type: errorActionTypes.SET_ERROR,
    errorData
});

export const setException = (exceptionType, exceptionPages, shouldChangePage = undefined) => dispatch => {
    if(shouldChangePage === true) {
        dispatch(changePage(exceptionPages[0], 'increase'));
    }
    if(Number.isInteger(shouldChangePage)) {
        dispatch(changePage(shouldChangePage, 'increase'));
    }
    return dispatch({
        type: exceptionActionTypes.SET_EXCEPTION,
        exceptionType,
        exceptionPages
    });
};

export const setFlow = flowType => ({
    type: flowActionTypes.SET_FLOW,
    flowType
});

export const setLanguage = language => ({
    type: languageActionTypes.CHANGE_LANGUAGE,
    language
});

export const setMethods = (owner, methods) => ({
    type: methodsActionTypes.SET_METHODS,
    owner,
    methods
});

export const setMessages = (messages, language) => ({
    type: messagesActionTypes.SET_MESSAGES,
    messages,
    language
});

export const setSession = session => ({
    type: sessionActionTypes.REFRESH_SESSION,
    session
});


export const setTribes = tribes => ({
    type: tribesActionTypes.SET_TRIBES,
    tribes
});

export const showModal = (content, modalOwner = content, data, callbacks) => ({
    type: modalActionTypes.SHOW_MODAL,
    callbacks,
    content,
    data,
    modalOwner
});

export const startSpinner = message => ({
    type: spinnerActionTypes.START_SPINNER,
    message
});

export const stopSpinner = () => ({
    type: spinnerActionTypes.STOP_SPINNER
});

export const submitForm = formName => ({
    type: formActionTypes.SUBMIT_FORM,
    formName
});

export const switchConsoleMode = payload => ({
    type: consoleActionTypes.SWITCH_CONSOLE_MODE,
    payload
});

export const updateException = exceptionPages => ({
    type: exceptionActionTypes.UPDATE_EXCEPTION,
    exceptionPages
});

export const validateForm = (payload, formName) => ({
    type: formActionTypes.VALIDATE_FORM,
    payload,
    formName
});


/************************

 # Async action creators

 ************************/

const actions = { setError, startSpinner, stopSpinner };



/************************

 # Get calls

************************/

export const fetchHeaderFooterData = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    if(data) {
        const FAV_ICON = data.assets.FAV_ICON;
        document.getElementById('favicon').setAttribute('href', FAV_ICON);
        dispatch(setEnvironments(data));
    }
};

export const fetchPreEligibility = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    return data && Promise.resolve(data);
};

export const fetchMemberAffordability = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && dispatch(setMemberAffordabilty(data));
};

export const fetchFlow = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    const {
        financialFlow
    } = data ? data : {};

    data && dispatch(setFlow(financialFlow ?
        FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE :
        FLOW_TYPES.NON_FINANCIAL_FLOW_NON_SWITCHABLE
    ));
};

export const fetchBloodRelationships = (url, isWholeList) => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && dispatch(isWholeList ? setBloodRelationshipsWholeList(data) : setBloodRelationships(data));
};

export const fetchInitialData = (...args) => async dispatch => {

    const superUserMode = window.ssapApplicationObj.mode;

    const [
        url,
        [ impersonationDetails, user, USER_ROLES ],
        environment,
        resolvedFlow,
        getCachedFlows,
        PAGES
    ] = args;

    const data = await API.getData(url, dispatch, actions);

    const {
        applicationStatus,
        applyingForFinancialAssistanceIndicator,
        currentPage,
        maxAchievedPage,
        taxHousehold
    } = data ? data : {};

    let currentFlow = FLOW_TYPES.NON_FINANCIAL_FLOW_NON_SWITCHABLE;

    if(
        environment === ENVIRONMENT_TYPES.NEVADA ||
        environment === ENVIRONMENT_TYPES.MINNESOTA ||
        environment === ENVIRONMENT_TYPES.IDAHO
    ) {
        switch (true) {

        // Here we got NFF

        case resolvedFlow === FLOW_TYPES.NON_FINANCIAL_FLOW_NON_SWITCHABLE:
            currentFlow = FLOW_TYPES.NON_FINANCIAL_FLOW_NON_SWITCHABLE;
            break;

            // Here we handle click 'Edit' button during OEP
            // Action: we change flow type to <FINANCIAL_FLOW>


        case resolvedFlow === FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE
        && maxAchievedPage < 4 && applyingForFinancialAssistanceIndicator:
            currentFlow = FLOW_TYPES.FINANCIAL_FLOW;
            dispatch(setFlow(FLOW_TYPES.FINANCIAL_FLOW));
            break;

            // Here we got NFSF and User hasn't got to switching page where he/she can decide which flow to proceed
            // Action: we change flow type to <NON_FINANCIAL_FLOW_SWITCHABLE>

        case resolvedFlow === FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE
        && maxAchievedPage < 4:
            currentFlow = FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE;
            dispatch(setFlow(FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE));
            break;

            // Here we got NFSF and User's progress more than switching page but he/she decided to apply with non-financial application
            // Action: we change flow type to <NON_FINANCIAL_FLOW_SWITCHABLE>

        case resolvedFlow === FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE
        && maxAchievedPage >= 4
        && !applyingForFinancialAssistanceIndicator:
            currentFlow = FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE;
            dispatch(setFlow(FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE));
            break;

            // Here we got NFSF and User's progress more than switching page but he/she decided to apply with financial application
            // Action: we change flow type to <FINANCIAL_FLOW>

        case resolvedFlow === FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE
        && maxAchievedPage >= 4
        && applyingForFinancialAssistanceIndicator:
            currentFlow = FLOW_TYPES.FINANCIAL_FLOW;
            dispatch(setFlow(FLOW_TYPES.FINANCIAL_FLOW));
            break;

        }
    }

    const finalReviewPageNumber = getCachedFlows(currentFlow, taxHousehold[0].householdMember.length)
        .findIndex(p => p.pageName === PAGES.FINAL_APPLICATION_REVIEW_PAGE);

    const { applicationType, coverageYear } = window.ssapApplicationObj;

    let updatedData = {
        ...data,
        finalized: false,
        applicationType: (applicationType !== null && applicationType !== '') ? applicationType : data.applicationType,
        coverageYear: (coverageYear !== null && coverageYear !== '') ? coverageYear : data.coverageYear
    };

    dispatch(setData(updatedData));

    if(impersonationDetails &&
        Object.values(USER_ROLES.SUPERUSERS).some(role => role === user.role)) {
        if(applicationStatus !== APPLICATION_STATUSES.OPEN && applicationStatus !== APPLICATION_STATUSES.UNCLAIMED) {
            switch(true) {
            case applicationStatus === APPLICATION_STATUSES.ELIGIBILITY_RECEIVED && superUserMode === USER_MODES.EDIT:
                dispatch(changePage(0));
                break;
            default:
                dispatch(setException(
                    EXCEPTIONS.REVIEW_AFTER_SIGN,
                    [ finalReviewPageNumber ],
                    true
                ));
            }
        } else {
            dispatch(changePage(currentPage, 'increase'));
        }
    } else {
        if(applicationStatus !== APPLICATION_STATUSES.OPEN && applicationStatus !== APPLICATION_STATUSES.UNCLAIMED) {
            dispatch(setException(
                EXCEPTIONS.REVIEW_AFTER_SIGN,
                [ finalReviewPageNumber ],
                true
            ));
        } else {
            dispatch(changePage(currentPage, 'increase'));
        }
    }
};

export const fetchTribes = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && dispatch(setTribes(data));
};

export const fetchCounties = (url, state) => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    let payload = [];

    if(Array.isArray(data)) {
        payload = data[Symbol.for('sort-by-object-values')]();
    } else {
        Object.keys(data).forEach(key => {
            if(Array.isArray(data[key])) {
                data[key] = data[key][Symbol.for('sort-by-object-values')]();
            }
        });
        payload = data;
    }

    dispatch(setCounties(payload, state));
};

export const fetchCountiesForExactZipcodes = (requestsList, url, pageName) => async dispatch => {

    // TODO: Should be refactored (there is no need in multi requests)

    const result = await Promise.all(requestsList.map(async request =>
        await API.getData(`${url}${request.state}/${request.postalCode}`, dispatch, actions)));

    dispatch(setCounty(result[0], pageName));
};

export const fetchCounty = (url, pageName, callback) => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && data.length === 0 && callback();

    data && dispatch(setCounty(data, pageName));

};

export const fetchCountries = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && dispatch(setCountries(data));
};

export const restoreSession = url => async dispatch => {

    const data = await API.getData(url, dispatch, actions);

    data && dispatch(setSession(data));
};


// For testing errors handling

// export const wrongEndpointCall = url => async dispatch => {
//
//     const data = await API.getData(url, dispatch, actions);
//
//     data && console.log(data);
// };



/************************

 # POST calls

************************/


export const saveData = (callbacks, ...args) => async dispatch => {

    const data = await API.postData([ ...args ], dispatch, actions);

    const {
        currentPage
    } = data ? data : {};

    if(data) {
        dispatch(restoreSession(`${window.location.origin}${ENDPOINTS.SESSION_PING}`));
        dispatch(setData(data));
        dispatch(changePage(currentPage, 'increase'));
        callbacks.forEach(callback => {
            dispatch(callback());
        });
    }
};


export const validateAddress = (callbacks, ...restArgs) => async dispatch => {

    const data = await API.postData( [...restArgs ], dispatch, actions);

    const {
        exactMatch
    } = data ? data : {};

    if(data && !exactMatch) {
        dispatch(showModal(MODAL_TYPES.VALIDATE_ADDRESS_MODAL, MODAL_TYPES.VALIDATE_ADDRESS_MODAL, data, callbacks));
    }
};

export const submitApplication = (...args) => async dispatch => {

    const [ url, body ] = args;

    const params = [ url, body, undefined, true ];

    const data = await API.postData(params, dispatch, actions);
    if(data) {
        window.location = `${window.location.origin}${ENDPOINTS.DASHBOARD_ENDPOINT}`;
    } else {
        dispatch(stopSpinner());
        dispatch(hideAllModals());
    }
};