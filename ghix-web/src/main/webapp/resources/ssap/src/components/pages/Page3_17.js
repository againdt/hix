import React, {Fragment, useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Form from './Form';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import {
    changeData,
    clearMethods,
    fetchPreEligibility,
    setFlow,
    setMethods,
    showModal,
    submitApplication
} from '../../actions/actionCreators';
import createSignature from '../../utils/createSignature';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import {getStateName} from '../../utils/getStateName';
import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import ENDPOINTS from '../../constants/endPointsList';
import INPUT_TYPES from '../../constants/inputTypes';
import FLOW_TYPES from '../../constants/flowTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import {VALIDATION_ERROR_MESSAGES} from '../../constants/validationErrorMessages';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - Final Page of SSAP
 *
 */
const Page3_17 = props => {

    const {
        contexts,
        data,
        data: {
            singleStreamlinedApplication: {
                applicationType,
                maxAchievedPage,
                ssapApplicationId
            }
        },
        environment: {
            impersonation,
            exchangeName,
            stateCode,
            links: {
                PHONE
            }
        },
        flow: {
            flowType
        },
        form: {
            Form: {
                isFormSubmitted,
                isEligiblyForSave
            }
        },
        householdMembers,
        modals,
        page
    } = props;

    const {
        name: householdHeadName
    } = householdMembers[0];

    const {
        singleStreamlinedApplication : currentData,
        singleStreamlinedApplication: {
            incarcerationAsAttestedIndicator,
            consentAgreement,
            numberOfYearsAgreed,
            agreeToUseIncomeDetails,
            agreeToEndCoverage,
            medicaidConsent
        }
    } = data;

    const isAbsentParentSectionShown = (() => householdMembers.some(hhm => hhm.healthCoverage.absentParent))();


    // Local State of Functional Component

    const [ localState, setLocalState ] = useState({
        isReadyToReportForChanges: false,
        isTruthful: false,
        preeligibility: [],
        signature: '',
        formUpdate: false
    });

    const {
        isReadyToReportForChanges,
        isTruthful,
        preeligibility,
        signature,
        formUpdate
    } = localState;

    const handleEditClick = () => {
        props.actions.showModal(
            MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
            MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
            [ preeligibility , props.actions.changeData ],
        );
    };

    const handleSubmit = () => {

        const getBody = bodyMix => {

            return {
                'application': JSON.stringify({
                    ...currentData,
                    currentPage: page,
                    finalized: true,
                    maxAchievedPage: maxAchievedPage < page ? page : maxAchievedPage
                }),
                'applicationId': ssapApplicationId,
                'csrftoken': window.csrf_token,
                'csrOverride': impersonation,
                ...(bodyMix ? bodyMix : {})
            };
        };

        const submitApplication = () => props.actions.submitApplication(`${window.location.origin}${ENDPOINTS.SAVE_DATA_ENDPOINT}${window.csrf_token}`, getBody());

        const submitApplication2 = bodyMix => {
            props.actions.submitApplication(`${window.location.origin}${ENDPOINTS.SAVE_DATA_ENDPOINT}${window.csrf_token}`, getBody(bodyMix));
        };

        const showPreEligibilityModal = () => props.actions.showModal(
            MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
            MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
            [ preeligibility, props.actions.changeData, submitApplication ],

        );

        const showPreEligibilityModal2 = body => {
            props.actions.showModal(
                MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
                MODAL_TYPES.PRE_ELIGIBILITY_MODAL,
                [ preeligibility, props.actions.changeData, submitApplication2, body ],

            );
        };

        const showQualifyingEventModal = () => props.actions.showModal(
            MODAL_TYPES.QUALIFYING_EVENT_DETAILS_MODAL,
            MODAL_TYPES.QUALIFYING_EVENT_DETAILS_MODAL,
            [
                `${window.location.origin}${ENDPOINTS.SAVE_DATA_ENDPOINT}${window.csrf_token}`,
                getBody(),
                preeligibility.length > 0,
                showPreEligibilityModal2
            ],
            props.actions.submitApplication
        );

        contexts.Form.onFormSubmit(null, null, finalizeApplication);
        function finalizeApplication() {
            if(CONFIG.ENV.PRODUCTION) {
                if(applicationType === 'QEP') {
                    showQualifyingEventModal();
                } else {
                    preeligibility.length > 0 ? showPreEligibilityModal() : submitApplication();
                }
            } else {
                console.log('Submit Application button is activated!');
            }
        }
    };

    useEffect(() => {
        props.actions.setMethods('Page3_17', { handleEditClick, handleSubmit });
        return () => {
            props.actions.clearMethods('Page3_17');
        };
    }, [ modals.length, Object.keys(contexts).length, preeligibility.length, formUpdate ]);


    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.actions.fetchPreEligibility(`${window.location.origin}${ENDPOINTS.PRE_ELIGIBILITY}${ssapApplicationId}`)
                .then(response => {
                    setLocalState(state => ({
                        ...state,
                        preeligibility: response,
                        formUpdate: true
                    }));
                    return response;
                }).then(response => {

                    const qhpMembers = response.map(hhm => hhm.applicantGuid);

                    const newState = { ...data };

                    newState
                        .singleStreamlinedApplication
                        .taxHousehold[0]
                        .householdMember = householdMembers.map(hhm =>
                            qhpMembers.some(q => q === hhm.applicantGuid) ?
                                { ...hhm, seeksQhp: false } : hhm
                        );

                    props.actions.changeData(newState);
                });
        }
    }, []);



    /**
     *
     * @param {Object} newData - updated <singleStreamlinedApplication>
     *
     * Function updates JSON in Redux Store
     */
    const onFieldChange = newData => {

        // Here we clean for all household members
        // <incarcerationStatusIndicator> and <incarcerationPendingDispositionIndicator>
        // only in case if User chose that nobody from his household is incarcerated

        newData.incarcerationAsAttestedIndicator && newData.taxHousehold[0].householdMember.forEach(member => {
            member.incarcerationStatus.incarcerationStatusIndicator = false;
            member.incarcerationStatus.incarcerationPendingDispositionIndicator = false;
        });

        props.actions.changeData({
            ...props.data,
            singleStreamlinedApplication: {...newData}
        });
    };

    /**
     *
     * @param {Object} newData - updated household member
     * @param {number} indexInArray - index of updated household member in array of all household members
     */
    const onFieldChange_incarceration = (newData, indexInArray) => {
        const newState = { ...props.data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        props.actions.changeData(newState);
    };
    const onFieldChange_data = (newData) => {
        const newState = { ...props.data };
        newState.singleStreamlinedApplication = newData;
        props.actions.changeData(newState);
    };
    /**
     *
     * @param {Object} newData - updated prop to make change in Component local state
     */
    const onFieldChange_signing = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
    };

    const onFieldBlur_signing = () => {
        setLocalState(state => ({
            ...state,
            signature: state.signature.trim()
        }));
    };

    const getTopLevelValidation = () => {
        return incarcerationAsAttestedIndicator ||
            !!householdMembers.find(member => member.incarcerationStatus.incarcerationStatusIndicator);
    };

    return (
        <Fragment>
            <div className="fade-in">

                <div className="subsection">
                    <PageHeader
                        text="Read and check the box next to each statement if you agree"
                        headerClassName={'usa-heading-alt'}
                    />
                    <Form topLevelValidity={getTopLevelValidation()}>
                        <fieldset className="usa-fieldset-inputs usa-sans">
                            <ValidationErrorMessage
                                isVisible={isFormSubmitted && !isEligiblyForSave}
                                messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                                errorClasses={['margin-l-0 margin-l-20-ve']}
                            />
                            <legend className="required-true">Are any applicants incarcerated (in prison or jail)</legend>
                            <ul className="usa-unstyled-list">
                                <li>
                                    <UniversalInput
                                        label={{
                                            text: 'No. No one listed on this health insurance application is incarcerated (in prison or jail).',
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            classes:['disp-styl'],
                                            name: 'incarcerationAsAttestedIndicator',
                                            checked: incarcerationAsAttestedIndicator,
                                            value: incarcerationAsAttestedIndicator,
                                            currentData: currentData,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                    />
                                </li>

                                {/*****************************************************
                                 Begin - If #incarcerationAsAttestedIndicator (false)
                                 ******************************************************/}
                                {
                                    !incarcerationAsAttestedIndicator &&

                                    householdMembers.map((member, i) => {
                                        const {
                                            name,
                                            name: {
                                                firstName
                                            },
                                            incarcerationStatus: {
                                                incarcerationStatusIndicator,
                                                incarcerationPendingDispositionIndicator
                                            },
                                            applyingForCoverageIndicator
                                        } = member;

                                        return (
                                            <Fragment key={`${firstName}_${i}`}>
                                                { applyingForCoverageIndicator &&
                                                    <Fragment>
                                                        <li>
                                                            <UniversalInput
                                                                label={{
                                                                    text:
                                                                        <span> {normalizeNameOfPerson(name)} &nbsp;is incarcerated </span>
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: true,
                                                                    type: INPUT_TYPES.CHECKBOX,
                                                                    classes: ['disp-styl margin-t-10'],
                                                                    name: 'incarcerationStatusIndicator',
                                                                    checked: incarcerationStatusIndicator,
                                                                    value: incarcerationStatusIndicator,
                                                                    currentData: member,
                                                                    fields: ['incarcerationStatus']
                                                                }}
                                                                inputActions={{
                                                                    onFieldChange: onFieldChange_incarceration
                                                                }}
                                                                indexInArray={i}
                                                            />
                                                        </li>

                                                        {/***********************************************
                                                         Begin - If #incarcerationStatusIndicator (true)
                                                         ************************************************/}

                                                        {
                                                            incarcerationStatusIndicator &&

                                                            <UniversalInput
                                                                legend={{
                                                                    legendText: 'Is this person pending disposition?'
                                                                }}
                                                                label={{
                                                                    text: '',
                                                                    labelClass: ['usa-width-one-whole', 'layout--02'],
                                                                    ulClass: ['usa-width-one-whole'],
                                                                    showRequiredIcon: true
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: true,
                                                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                                                    classes: ['disp-styl-none'],
                                                                    name: 'incarcerationPendingDispositionIndicator',
                                                                    value: incarcerationPendingDispositionIndicator,
                                                                    options: DROPDOWN_OPTIONS.YES_NO,
                                                                    validationType: VALIDATION_TYPES.CHOSEN,
                                                                    currentData: member,
                                                                    fields: ['incarcerationStatus']
                                                                }}
                                                                inputActions={{
                                                                    onFieldChange: onFieldChange_incarceration
                                                                }}
                                                                indexInArray={i}
                                                            />
                                                        }

                                                        {/*********************************************
                                                         End - If #incarcerationStatusIndicator (true)
                                                         **********************************************/}
                                                    </Fragment>
                                                }
                                            </Fragment>
                                        );
                                    })
                                }
                            </ul>
                        </fieldset>
                        {/**************************************************
                         End - If #incarcerationAsAttestedIndicator (false)
                         ***************************************************/}
                        { flowType === FLOW_TYPES.FINANCIAL_FLOW &&
                            <Fragment>
                                <div className="usa-width-one-whole">
                                    <UniversalInput
                                        legend={{
                                            legendText: <Fragment>To make it easier to determine my eligibility for help paying for coverage in future years,
                                                I agree to allow the Marketplace to use my income data, including information from tax returns, for the next 5 years.
                                                The Marketplace will send me a notice and let me make changes. I can opt out at any time.</Fragment>,
                                            dataInstructions: 'sign_submit_agreement'
                                        }}
                                        label={{
                                            text: DROPDOWN_OPTIONS.AGREE_DISAGREE,
                                            labelClass: ['usa-width-one-whole  layout--04'],
                                            ulClass: ['usa-width-one-whole'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'agreeToUseIncomeDetails',
                                            value: agreeToUseIncomeDetails,
                                            options: DROPDOWN_OPTIONS.AGREE_DISAGREE,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: currentData,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                    />
                                </div>
                                {
                                    agreeToUseIncomeDetails === false &&
                                    <Fragment>
                                        <div className="usa-width-one-whole">
                                            <UniversalInput
                                                legend={{
                                                    legendText: 'How long would you like your eligibility for help paying for coverage to be renewed?'
                                                }}
                                                label={{
                                                    text: DROPDOWN_OPTIONS.NUMBER_OF_YEARS,
                                                    labelClass: ['usa-width-one-whole', 'layout--02'],
                                                    ulClass: ['usa-width-one-whole'],
                                                    showRequiredIcon: true
                                                }}
                                                inputData={{
                                                    isInputRequired: true,
                                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                                    name: 'numberOfYearsAgreed',
                                                    value: numberOfYearsAgreed === '' ? null : +numberOfYearsAgreed,
                                                    options: DROPDOWN_OPTIONS.NUMBER_OF_YEARS,
                                                    validationType: VALIDATION_TYPES.CHOSEN,
                                                    currentData: currentData,
                                                    fields: []
                                                }}
                                                inputActions={{
                                                    onFieldChange
                                                }}
                                            />
                                        </div>
                                    </Fragment>
                                }
                                <div className="usa-width-one-whole">
                                    <UniversalInput
                                        label={{
                                            text: <span className="required-true">I understand that if anyone on my application who enrolls in coverage through
                                                a Marketplace plan, is later found to have other qualifying health coverage (including Medicare, Medicaid,
                                                or CHIP), Nevada Health Link will automatically end their Marketplace coverage.</span>
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.CHECKBOX,
                                            name: 'agreeToEndCoverage',
                                            classes:['disp-styl'],
                                            checked: agreeToEndCoverage,
                                            value: agreeToEndCoverage,
                                            validationType: VALIDATION_TYPES.CHECKED,
                                            currentData: currentData,
                                            fields: []
                                        }}
                                        errorClasses={['margin-l-0 margin-l-20-ve']}
                                        inputActions={{
                                            onFieldChange: onFieldChange_data
                                        }}
                                    />
                                </div>
                            </Fragment>
                        }
                        { flowType !== FLOW_TYPES.FINANCIAL_FLOW &&
                            <Fragment>
                                <div className="usa-width-one-whole">
                                    <UniversalInput
                                        legend={{
                                            legendText: <Fragment>
                                                <div className="para-heading">Would you like to save time in future
                                                    applications?&nbsp;<sup><FontAwesomeIcon icon="asterisk"/></sup></div>
                                                <div className="para-heading-supporting-txt">If you agree, Nevada Health Link
                                                    can save your application information. We will send you a notice every year
                                                    to see if you have any changes
                                                    to your application. If you do not have any changes, we will automatically
                                                    e-submit your application and reconfirm your ability to enroll in coverage
                                                    through Nevada Health Link
                                                </div>
                                            </Fragment>
                                        }}
                                        label={{
                                            text: DROPDOWN_OPTIONS.AGREE_DISAGREE,
                                            labelClass: ['usa-width-one-whole  layout--04 required-false'],
                                            ulClass: ['usa-width-one-whole'],
                                            showRequiredIcon: false
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'consentAgreement',
                                            value: consentAgreement,
                                            options: DROPDOWN_OPTIONS.AGREE_DISAGREE,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: currentData,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                    />
                                </div>
                                {/************************************
                                 Begin - If #consentAgreement (false)
                                 *************************************/}

                                {
                                    consentAgreement === false &&
                                    <div className="usa-width-one-whole">
                                        <UniversalInput
                                            legend={{
                                                legendText: `I give my permission to Nevada Health Link to renew my application
                                                                  for a period of:`
                                            }}
                                            label={{
                                                text: DROPDOWN_OPTIONS.NUMBER_OF_YEARS,
                                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                                ulClass: ['usa-width-one-whole'],
                                                showRequiredIcon: true
                                            }}
                                            inputData={{
                                                isInputRequired: true,
                                                type: INPUT_TYPES.RADIO_BUTTONS,
                                                name: 'numberOfYearsAgreed',
                                                value: numberOfYearsAgreed === '' ? null : +numberOfYearsAgreed,
                                                options: DROPDOWN_OPTIONS.NUMBER_OF_YEARS,
                                                validationType: VALIDATION_TYPES.CHOSEN,
                                                currentData: currentData,
                                                fields: []
                                            }}
                                            inputActions={{
                                                onFieldChange
                                            }}
                                            errorClasses={['margin-l-0 margin-l-20-ve']}
                                        />
                                    </div>
                                }
                            </Fragment>
                        }
                        {/************************************
                         End - If #consentAgreement (false)
                         *************************************/}
                        <UniversalInput
                            label={{
                                text: <span className="required-true">I understand that I have 30 days to notify Nevada Health Link of any change of
                                      information in this application. I will report any changes within this
                                      time period. I understand that changes in my household size, address or other
                                      details might affect my or my household&apos;s eligibility for specific benefits.
                                      I understand and will notify Nevada Health Link if my application information
                                    changes</span>,
                                dataInstructions: 'sign_submit_changes_report_duty',
                                replacements: {
                                    exchangeName,
                                    phone: PHONE,
                                    stateCode: getStateName(stateCode)
                                }
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.CHECKBOX,
                                classes:['disp-styl-none'],
                                name: 'isReadyToReportForChanges',
                                checked: isReadyToReportForChanges,
                                value: isReadyToReportForChanges,
                                validationType: VALIDATION_TYPES.CHECKED,
                                currentData: { isReadyToReportForChanges },
                                fields: []
                            }}
                            errorClasses={['margin-l-0 margin-l-20-ve']}
                            inputActions={{
                                onFieldChange: onFieldChange_signing
                            }}
                        />
                        {
                            isAbsentParentSectionShown &&

                            <UniversalInput
                                label={{
                                    text: <Fragment><span className="required-true">I know I&apos;ll be asked to
                                    cooperate with the agency that collects medical support from an absent parent.
                                    If I think that cooperating to collect medical support will harm me or my children, I can tell Medicaid and I may not have to cooperate.</span></Fragment>
                                }}
                                inputData={{
                                    isInputRequired: true,
                                    classes: ['disp-styl'],
                                    type: INPUT_TYPES.CHECKBOX,
                                    name: 'medicaidConsent',
                                    checked: medicaidConsent,
                                    value: medicaidConsent,
                                    validationType: VALIDATION_TYPES.CHECKED,
                                    currentData: currentData,
                                    fields: []
                                }}
                                errorClasses={['margin-l-0 margin-l-20-ve']}
                                inputActions={{
                                    onFieldChange: onFieldChange_data
                                }}
                            />
                        }
                        <UniversalInput
                            label={{
                                text: <Fragment><span className="required-true">By typing my name in the box below, I’m signing this application under penalty of perjury,
                                    which means I’ve provided true answers to all of the questions to the best of my knowledge.
                                        I know I may be subject to penalties under federal law if I intentionally provide false information.</span></Fragment>
                            }}
                            inputData={{
                                isInputRequired: true,
                                classes: ['disp-styl'],
                                type: INPUT_TYPES.CHECKBOX,
                                name: 'isTruthful',
                                checked: isTruthful,
                                value: isTruthful,
                                validationType: VALIDATION_TYPES.CHECKED,
                                currentData: {isTruthful},
                                fields: []
                            }}
                            errorClasses={['margin-l-0 margin-l-20-ve']}
                            inputActions={{
                                onFieldChange: onFieldChange_signing
                            }}
                        />

                        <UniversalInput
                            label={{
                                labelClasses: [],
                                text:
                                    <span>
                                        <strong>
                                            {normalizeNameOfPerson(householdHeadName)}
                                        </strong>
                                        {'\'s Electronic Signature'}
                                    </span>
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'signature',
                                helperClasses: [],
                                value: signature,
                                valuesToMatch: createSignature(householdHeadName),
                                placeholder: 'Type Your Full Name Here',
                                validationType: VALIDATION_TYPES.IS_SIGNED,
                                currentData: {signature},
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange_signing,
                                onFieldBlur: onFieldBlur_signing
                            }}

                        />
                    </Form>
                </div>
            </div>

        </Fragment>
    );
};

const mapStateToProps = state => ({
    contexts: state.contexts,
    data: state.data,
    environment: state.headerLinks,
    form: state.form,
    flow: state.flow,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    modals: state.modals,
    page: state.page

});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        fetchPreEligibility,
        changeData,
        clearMethods,
        setFlow,
        setMethods,
        showModal,
        submitApplication
    }, dispatch)
});

Page3_17.defaultProps = {
    name: 'Page3_17'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page3_17);
