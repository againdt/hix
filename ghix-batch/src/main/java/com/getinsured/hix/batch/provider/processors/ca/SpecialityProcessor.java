package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class SpecialityProcessor implements ProviderDataFieldProcessor {

	private Logger logger = LoggerFactory.getLogger(SpecialityProcessor.class);
	private ValidationContext context;
	private int index;
//	private static HashMap<String, String> specialityMap = new HashMap<String, String>();
	public static boolean sourceAvailable = false;

	public SpecialityProcessor(){
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object process(Object objToBeValidated) throws ProviderValidationException {

		/* Existing Validation had the checked but new does not confirmed to this validation so, commented out.
		Object source = this.context.getContextField("specialities");
		if(source == null){
			throw new ProviderValidationException("Can not process the speciality field code, source data not available");
		}
		specialityMap = (HashMap<String, String>) source;*/

		String s = objToBeValidated.toString();
		String[] specialties = s.split("\\|");
//		String desc;
		ArrayList<String> specialtyList = new ArrayList<String>();

		for(String specialty : specialties){

			if(specialty != null && specialty.length() == 0){
				continue;
			}
			/*desc = this.specialityMap.get(spciality);
			if(desc == null){
				desc = "Not Available";
			}*/
			specialtyList.add(specialty);
		}

		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null)? null: (String)tmp;

		if(outputField == null){
			//Not available in metadata file, create a default one
			outputField = "specialty_code";
			this.context.addContextInfo("output_field", outputField);
		}

		if(specialtyList.size() > 0){
			context.addContextInfo(outputField, specialtyList);
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
