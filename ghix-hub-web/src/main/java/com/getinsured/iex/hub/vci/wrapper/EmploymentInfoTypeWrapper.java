package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.EmploymentInfoType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.EmploymentInfoType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class EmploymentInfoTypeWrapper implements JSONAware{
	
	private String employeeStatusCode;
	private String employeeStatusMessage;
	private PersonEmploymentAssociationTypeWrapper personEmploymentAssociation;
	
    private static final String EMP_STATUS_CODE_KEY = "EmployeeStatusCode";
    private static final String EMP_STATUS_MSG_KEY = "EmployeeStatusMessage";
    private static final String PERSON_EMP_ASSO_KEY = "PersonEmploymentAssociation";
    
	public EmploymentInfoTypeWrapper(EmploymentInfoType employmentInfoType){
		
		if(employmentInfoType == null){
			return;
		}
		
		this.employeeStatusCode = employmentInfoType.getEmployeeStatusCode() != null ? employmentInfoType.getEmployeeStatusCode().getValue() : null;
		this.employeeStatusMessage = employmentInfoType.getEmployeeStatusMessage() != null ? employmentInfoType.getEmployeeStatusMessage().getValue().value() : null;
		this.personEmploymentAssociation = new PersonEmploymentAssociationTypeWrapper(employmentInfoType.getPersonEmploymentAssociation());
		
	}

	public String getEmployeeStatusCode() {
		return employeeStatusCode;
	}

	public String getEmployeeStatusMessage() {
		return employeeStatusMessage;
	}

	public PersonEmploymentAssociationTypeWrapper getPersonEmploymentAssociation() {
		return personEmploymentAssociation;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(EMP_STATUS_CODE_KEY, this.employeeStatusCode);
		obj.put(EMP_STATUS_MSG_KEY, this.employeeStatusMessage);
		obj.put(PERSON_EMP_ASSO_KEY, this.personEmploymentAssociation);
		return obj.toJSONString();
	}
    
	
}
