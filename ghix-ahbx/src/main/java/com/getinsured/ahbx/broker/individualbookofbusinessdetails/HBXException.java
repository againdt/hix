//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.30 at 02:39:14 PM IST 
//


package com.getinsured.ahbx.broker.individualbookofbusinessdetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HBXException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HBXException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="svcProviderPayload" type="{http://impl.webservice.ahbx.accenture.com/}svcProviderPayload" minOccurs="0"/>
 *         &lt;element name="responseMsg" type="{http://impl.webservice.ahbx.accenture.com/}responseDTO" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HBXException", propOrder = {
    "svcProviderPayload",
    "responseMsg",
    "message"
})
public class HBXException {

    protected SvcProviderPayload svcProviderPayload;
    protected ResponseDTO responseMsg;
    protected String message;

    /**
     * Gets the value of the svcProviderPayload property.
     * 
     * @return
     *     possible object is
     *     {@link SvcProviderPayload }
     *     
     */
    public SvcProviderPayload getSvcProviderPayload() {
        return svcProviderPayload;
    }

    /**
     * Sets the value of the svcProviderPayload property.
     * 
     * @param value
     *     allowed object is
     *     {@link SvcProviderPayload }
     *     
     */
    public void setSvcProviderPayload(SvcProviderPayload value) {
        this.svcProviderPayload = value;
    }

    /**
     * Gets the value of the responseMsg property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseDTO }
     *     
     */
    public ResponseDTO getResponseMsg() {
        return responseMsg;
    }

    /**
     * Sets the value of the responseMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseDTO }
     *     
     */
    public void setResponseMsg(ResponseDTO value) {
        this.responseMsg = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
