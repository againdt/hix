package com.getinsured.ssap.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.ssap.model.financial.TaxFilingRequirement;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.TaxFilingThresholds;

/**
 * Implementation of {@link TaxFilingRequirementService}.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
@Service("taxFilingRequirementService")
public class TaxFilingRequirementServiceImpl implements TaxFilingRequirementService {
  private static final Logger log = LoggerFactory.getLogger(TaxFilingRequirementServiceImpl.class);
  private static ObjectMapper objectMapper;

  @PostConstruct
  public void postConstruct() {
    if(objectMapper == null) {
      objectMapper = new ObjectMapper();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TaxFilingRequirement> getTaxFilingRequirementsByYear(final int year) {
   return TaxFilingThresholds.getTaxFilingRequirementsByYear(year);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<Integer, List<TaxFilingRequirement>> getTaxFilingRequirements() {
    final ClassPathResource resource = new ClassPathResource(TaxFilingThresholds.TAX_FILING_REQUIREMENT_FILE);
    log.debug("Returning tax filing requirements for all years from: {}", TaxFilingThresholds.TAX_FILING_REQUIREMENT_FILE);

    if(!resource.exists() || !resource.isReadable()) {
      log.error("Could not read tax filing requirements from resource: {}", TaxFilingThresholds.TAX_FILING_REQUIREMENT_FILE);
      return null;
    }

    final String json = JsonUtil.readFile(resource);
    final Map<Integer, List<TaxFilingRequirement>> returnMap = new HashMap<>();
    try {
      final JsonNode rootNode = objectMapper.readTree(json);
      Map<String, Map<String, Integer>> yearToTaxFilingRequirementMap = objectMapper.treeToValue(rootNode, Map.class);
      yearToTaxFilingRequirementMap.keySet().forEach(yearStr -> {
        returnMap.put(Integer.parseInt(yearStr), getTaxFilingRequirementsByYear(Integer.parseInt(yearStr)));
      });
    } catch (IOException e) {
      log.error("Exception while reading JSON content of {}", TaxFilingThresholds.TAX_FILING_REQUIREMENT_FILE);
      log.error("Exception details", e);
    }

    return returnMap;
  }
}
