/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface CmsSftpService {

	List<BatchJobExecution> getRunningBatchList(String jobName);
	
	/**
	 * Upload files to CMS inbound location
	 * @param reportType 
	 * @param batchExecutionId
	 * @throws GIException 
	 */
	void uploadFilesForReportType(String reportType, Long batchExecutionId) throws GIException;

	/**
	 * Download files from CMS outbound location
	 * @param reportType
	 * @param batchExecutionId
	 */
	void downloadFilesForReportType(String reportType, Long batchExecutionId) throws GIException;
	
	/**
	 * Get Exchange SFTP parameters to provide report copies
	 * @return Map
	 */
	Map<String, String> getExchgSftpParameters();
	
	/**
	 * Log SFTP transfers into the ENRL_CMS_FILE_TRANSFER_LOG table
	 * @param fileTransferList
	 * @param batchExecutionId
	 * @param reportType
	 * @param sourceLocation
	 * @param hostLocation
	 * @return a List
	 */
	List<EnrollmentCmsFileTransferLog> logSftpTransfers(List<EnrollmentSftpFileTransferDTO> fileTransferList, Long batchExecutionId,
			String reportType, String sourceLocation, String hostLocation);
	
	/**
	 * Check connection before entering into SFTP job
	 */
	void checkConnection();

}
