(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .directive('d3TrendChart', d3TrendChart);

    function d3TrendChart() {

        return {
            restrict: 'E',
            scope: '=',
            link: function (scope, element, attrs) {
                var initStackedBarChart = {
                    draw: function(config) {
                        var me = this;
                        var domEle = config.element;
                        var stackKey = config.key;
                        var data = config.data;
                        var margin = {top: 20, right: 20, bottom: 30, left: 50};
                        var parseDate = d3.timeParse("%m/%Y");
                        var width = 400 - margin.left - margin.right;
                        var height = 300 - margin.top - margin.bottom;
                        var xScale = d3.scaleBand().range([0, width]).padding(0.1);
                        var yScale = d3.scaleLinear().range([height, 0]);
                        var color = d3.scaleOrdinal(d3.schemeSet2);
                        var xAxis = d3.axisBottom(xScale).tickFormat(d3.timeFormat("%b"));
                        var yAxis =  d3.axisLeft(yScale);

                        var svg = d3.select(element[0]).append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                        var stack = d3.stack()
                            .keys(stackKey)
                            .order(d3.stackOrderNone)
                            .offset(d3.stackOffsetNone);

                        var div = d3.select(element[0]).append("div")
                            .attr("class", "tooltip chart-tooltip")
                            .style("opacity", 0);

                        var layers= stack(data);
                        // data.sort(function(a, b) { return b.total - a.total; });
                        xScale.domain(data.map(function(d) { return parseDate(d.date); }));
                        yScale.domain([0, 100]).nice();
                        var layer = svg.selectAll(".layer")
                            .data(layers)
                            .enter().append("g")
                            .attr("class", "layer")
                            .style("fill", function(d, i) { return color(i); });
                        layer.selectAll("rect")
                            .data(function(d) { return d; })
                            .enter().append("rect")
                            .attr("x", function(d) { return xScale(parseDate(d.data.date)); })
                            .attr("y", function(d) { return yScale(d[1]); })
                            .attr("height", function(d) { return yScale(d[0]) - yScale(d[1]); })
                            .attr("width", xScale.bandwidth())
                            .on("mouseover", function(d, i) {
                                div.transition()
                                    .duration(200)
                                    .style("opacity", .9);
                                div.html(
                                    "<h4>Trends</h4><h5> Date:  " + d.data.date + "</h5>" +
                                    "<div><div class=color-desc style=background-color:" + d3.schemeSet2[0] + "></div>&nbsp;&nbsp;&nbsp;&nbsp;" + "Fully Matched: " + d.data.totalSuccess + " ( " + d.data.totalSuccessPercent + "%)</div>" +
                                    "<div><div class=color-desc style=background-color:" + d3.schemeSet2[1] + "></div>&nbsp;&nbsp;&nbsp;&nbsp;" + "Missing In File: " + d.data.totalMissingInFile + "(" + d.data.totalMissingInFilePercent + "%)</div>" +
                                    //"<div><div class=color-desc style=background-color:" + d3.schemeSet2[2] + "></div>&nbsp;&nbsp;&nbsp;&nbsp;" + "Missing In Hix: " +  d.data.totalMissingInHix + "(" + d.data.totalMissingInHixPercent + "%)</div>" +
                                    "<div><div class=color-desc style=background-color:" + d3.schemeSet2[2] + "></div>&nbsp;&nbsp;&nbsp;&nbsp;" + "Error: " + d.data.totalFailure + "(" + d.data.totalFailurePercent + "%)</div>" +
                                    "<div><div class=color-desc style=background-color:" + d3.schemeSet2[3] + "></div>&nbsp;&nbsp;&nbsp;&nbsp;" + "Discrepancies: " + d.data.totalEnrlDiscrepancyCount + "(" + d.data.totalEnrlDiscrepancyCountPercent + "%)</div>")
                                    .style("left", (d3.event.pageX) + "px")
                                    .style("top", function(){
                                        if(d3.event.pageY +70 > 500){
                                            return d3.event.pageY -250+"px";
                                        }else{
                                            return d3.event.pageY+100+"px"
                                        }
                                    });
                            })
                            .on("mouseout", function(d) {
                                div.transition()
                                    .duration(500)
                                    .style("opacity", 0);
                            });

                        svg.append("g")
                            .attr("class", "axis axis--x")
                            .attr("transform", "translate(0," + (height+5) + ")")
                            .call(xAxis);
                        svg.append("g")
                            .attr("class", "axis axis--y")
                            .attr("transform", "translate(0,0)")
                            .call(yAxis);

                    }
                };

                var key = ["totalSuccessPercent", "totalMissingInFilePercent", "totalFailurePercent", "totalEnrlDiscrepancyCountPercent"];

                // var key = ["wounds", "other", "disease"];
                initStackedBarChart.draw({
                    data: scope.rollingTrends,
                    key: key,
                    element: d3.select(element[0])
                });
            }
        }
    }
})();