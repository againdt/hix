<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="disenrollsep">
<div class="row-fluid dashboard-rightpanel">
  <div class="header ng-scope">
    <h4><spring:message code="indportal.disenrollSEP.header" javaScriptEscape="true"/></h4>
  </div>

	<div class="well ng-scope">
  	<div class="well ng-scope">
	  	<div class="well-step">
	    	<div class="margin20-lr">
	    		<p><spring:message code="indportal.disenrollSEP.bodyContent" javaScriptEscape="true"/></p>
	    		<p class="alert alert-info"> <spring:message code="indportal.disenrollSEP.bodyWarning" javaScriptEscape="true"/></p>
	    		<p><spring:message code="indportal.disenrollSEP.contactInfo" javaScriptEscape="true"/></p>
	    	</div>
	    </div>
    </div>
	</div>


  <div ng-hide="disenrollresult">
   <a class="btn btn-primary pull-right" ng-click="disenrollsepapp(${sepEventsNumber})" href="javascript:void(0)"><spring:message code="indportal.portalhome.disenroll2" javaScriptEscape="true"/></a>
  </div>
  
  <!-- GOOD -->
	<div id="contactRequestAckSuccess" aria-labelledby="contactRequestAckSuccessLabel" ng-if="disenrollresult=='success'">
	    <h3><spring:message code="indportal.disenrollSEP.submissionSuccessMsg" javaScriptEscape="true"/></h3>
	    <a href="javascript:void(0)" ng-click="goDash()" class="btn btn-primary"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
	</div>

	<!-- BAD -->
	<div id="contactRequestAckFailure" aria-labelledby="contactRequestAckFailureLabel" ng-if="disenrollresult=='failure'">
	    <h3 id="contactRequestAckFailureLabel"><spring:message code="indportal.contactus.submissionfailed" javaScriptEscape="true"/></h3>
	    <p><spring:message code="indportal.disenrollSEP.submissionFailureMsg" javaScriptEscape="true"/></p>
	    <a href="javascript:void(0)" ng-click="goDash()" class="btn btn-primary"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
	</div>
  
  <div spinning-loader="loader"></div>
</div>

</script>
