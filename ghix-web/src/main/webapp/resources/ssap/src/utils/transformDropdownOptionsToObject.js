

export default options => {
    if(Array.isArray(options)) {
        return Object.assign(
            {},
            ...options.map(({ value, text }) => ({
                [value]: text
            }))
        );
    } else {
        return {};
    }
};