package com.getinsured.ssap.model.financial;

/**
 * Describes tax filing requirement.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
public class TaxFilingRequirement {
  private TaxFilingRequirementType type;
  private long dollarAmount;

  public TaxFilingRequirementType getType() {
    return type;
  }

  public void setType(TaxFilingRequirementType type) {
    this.type = type;
  }

  public long getDollarAmount() {
    return dollarAmount;
  }

  public void setDollarAmount(long dollarAmount) {
    this.dollarAmount = dollarAmount;
  }
}
