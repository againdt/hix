package com.getinsured.hix.serff.service.crosswalk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.CrossWalk;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.repository.IcrossWalkRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.serff.service.crosswalk.CrosswalkExcelColumn.CROSSWALKExcelColumnEnum;
import com.getinsured.hix.serff.service.excel.ExcelReader;
import com.getinsured.hix.util.PlanMgmtConstants;

/**
 * Class is used to read Crosswalk Plan Excel data to CrosswalkExcelVO POJO class.
 * 
 * @since May 3, 2014
 */
@Service("crosswalkPlanReader")
public class CrosswalkPlanReader extends ExcelReader {
	
	private static final Logger LOGGER = Logger.getLogger(CrosswalkPlanReader.class);

	private static final String MSG_ERROR_CROSSWALK_PLANS = "Failed to upload plans crosswalk file.";
	private static final String MSG_CROSSWALK_PLANS = "Plans crosswalk file has been successfully loaded.";
	private static final String MSG_ERROR_NO_TRACKING_RECORD = "No tracking record found.";
	private static final String MSG_ERROR_INVALID_SHEET1_NAME = " Invalid applicable year in 1st sheet. Name: ";
	private static final String MSG_ERROR_INVALID_SHEET2_NAME = " Invalid applicable year in 2nd sheet. Name: ";
	
	
	private static final int HIOS_ID_COLUMN_INDEX = 1;
	private static final int STATE_COLUMN_INDEX = 1;
	private static final int DENTAL_ONLY_PLAN = 1;
	private static final int SHEET_1 = 0;
	private static final int SHEET_2 = 1;
	private static final int SPLIT_INDEX1 = 0;
	private static final int SPLIT_INDEX2 = 1;
	private static final int HIOS_PLAN_ID_INDEX = 7;
	private static final int STATE_CODE_INDEX = 5;
	private static final String OPTION_VALUE = "yes,no,y,n";
	private static final String CLOSING_BRACKET = ")";

	@Autowired private IssuerService issuerService;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private PlanMgmtUtil pmUtil;
	@Autowired private IcrossWalkRepository icrosswalkRepo = null;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	
	/**
	 * @param inputStream
	 * @param fileName
	 * @param applicableYear
	 * @param currentIssuer
	 * @param userID
	 * @param trackingRecord
	 * @return
	 */
	public String loadExcel(InputStream inputStream, String fileName, String applicableYear, Issuer currentIssuer, int userID, SerffPlanMgmt trackingRecord) {
		
		LOGGER.debug("loadExcel() Start");
		StringBuilder message = new StringBuilder();
		
		try{
			
			if (null != trackingRecord) {
				message = loadAllExcelTemplate(trackingRecord, fileName ,inputStream ,applicableYear , currentIssuer ,userID);
			}
			else {
				LOGGER.error(MSG_ERROR_NO_TRACKING_RECORD);
				message.append(MSG_ERROR_CROSSWALK_PLANS);
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_CROSSWALK_PLANS);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (StringUtils.isEmpty(message.toString())) {
				message.append(MSG_CROSSWALK_PLANS);
			}
			LOGGER.info(message);
			LOGGER.debug("loadExcel() End");
		}
		return message.toString();
	}
	
	
	/**
	 * This method picks excel templates from UI 
	 * @param trackingRecord
	 * @param fileName
	 * @param inputStream
	 * @param applicableYear
	 * @param currentIssuer
	 * @param userId
	 * @return
	 */
	private StringBuilder loadAllExcelTemplate(SerffPlanMgmt trackingRecord, String fileName, InputStream inputStream, String applicableYear,
			Issuer currentIssuer, int userId) {
		
		LOGGER.debug("loadAllExcelTemplate() Start");
		StringBuilder message = new StringBuilder();
		boolean isSuccess = false;
		
		try {
		
			// Upload Excel File to ECM
			SerffDocument attachment = addExcelInECM(message, trackingRecord, fileName, inputStream);
			
			if (null != attachment && null != applicableYear) {
				
				byte[] content = ecmService.getContentDataById(attachment.getEcmDocId());
				InputStream myInputStream = new ByteArrayInputStream(content); 
				
				// Load Excel File
				loadFile(myInputStream);
				
				// Get first sheet from the workbook
				XSSFSheet sheet1 = getSheet(SHEET_1);
				XSSFSheet sheet2 = getSheet(SHEET_2);

				if (validateApplicableYearInFile(trackingRecord, applicableYear, sheet1.getSheetName(), sheet2.getSheetName(), message)) {
					// Read and Persist Crosswalk data from Excel
					isSuccess = readAndPersistCrosswalkData(message, fileName, sheet1, sheet2, trackingRecord, applicableYear, currentIssuer, userId);
				}
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_CROSSWALK_PLANS);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (isSuccess) {
				message.append(MSG_CROSSWALK_PLANS);
				trackingRecord.setRequestStateDesc(MSG_CROSSWALK_PLANS + " for file: " + fileName);
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
			}
			else {

				if (StringUtils.isBlank(trackingRecord.getRequestStateDesc())) {
					trackingRecord.setRequestStateDesc(message.toString()  + " for file: " +  fileName);
				}
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
			}
			trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
			trackingRecord.setEndTime(new TSDate());
			serffService.saveSerffPlanMgmt(trackingRecord);
			LOGGER.debug("loadAllExcelTemplate() End");
		}
		return message;
	}

	/**
	 * Validate Applicable Year in UI, Crosswalk Excel sheet names.
	 * 
	 * Here, selected Applicable Year should match with Crosswalk Excel 2nd sheet name and it should 1 year less from 1st sheet name.
	 */
	private boolean validateApplicableYearInFile(SerffPlanMgmt trackingRecord, String applicableYear, String sheet1Name, String sheet2Name,
			StringBuilder message) {

		LOGGER.debug("validateApplicableYearInFile() Start");
		boolean isValid = true;
		StringBuilder errorMessage = new StringBuilder();

		try {

			if (StringUtils.isNumeric(applicableYear)) {

				final int INDEX_ZERO = 0;
				int aYear = Integer.valueOf(applicableYear);
				String applicableYearInSheet1 = sheet1Name.split(PlanMgmtConstants.SPACE)[INDEX_ZERO].trim();
				String applicableYearInSheet2 = sheet2Name.split(PlanMgmtConstants.SPACE)[INDEX_ZERO].trim();

				if (!StringUtils.isNumeric(applicableYearInSheet1)) {
					errorMessage.append(MSG_ERROR_INVALID_SHEET1_NAME).append(applicableYearInSheet1);
					isValid = false;
				}

				if (!StringUtils.isNumeric(applicableYearInSheet2)) {
					errorMessage.append(MSG_ERROR_INVALID_SHEET2_NAME).append(applicableYearInSheet2);
					isValid = false;
				}

				if (isValid) {

					int aYearInSheet1 = Integer.valueOf(applicableYearInSheet1);
					int aYearInSheet2 = Integer.valueOf(applicableYearInSheet2);
	
					if (aYear != (aYearInSheet1 + 1)) {
						errorMessage.append(MSG_ERROR_INVALID_SHEET1_NAME).append(applicableYearInSheet1);
						isValid = false;
					}
	
					if (aYear != aYearInSheet2) {
						errorMessage.append(MSG_ERROR_INVALID_SHEET2_NAME).append(applicableYearInSheet2);
						isValid = false;
					}
				}
			}
			else {
				errorMessage.append(" Invalid selected applicable year: ").append(applicableYear);
				isValid = false;
			}
		}
		finally {

			if (!isValid) {
				message.append(MSG_ERROR_CROSSWALK_PLANS).append(errorMessage).append(PlanMgmtConstants.SPACE);
				LOGGER.error(message.toString());
				trackingRecord.setRequestStateDesc(message.toString());
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("validateApplicableYearInFile() End with validity: " + isValid);
			}
		}
		return isValid;
	}
	
	/**
	 * This method add uploaded excel to ECM
	 * @param message
	 * @param trackingRecord
	 * @param fileName
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private SerffDocument addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String fileName , InputStream inputStream) throws IOException {
	
		SerffDocument attachment = null;
		try {
			
			if (null != inputStream) {
				String ecmFolderName = trackingRecord.getSerffReqId() + PlanMgmtConstants.FTP_CROSSWALK_FILE + TSCalendar.getInstance().getTimeInMillis();
				String basePath = PlanMgmtConstants.CROSSWALK_ECM_BASE_PATH + ecmFolderName + PlanMgmtConstants.PATH_SEPERATOR;
				LOGGER.info("Uploading File Name with path to ECM:" + basePath + fileName);
				// Uploading Crosswalk Excel at ECM.
				attachment = pmUtil.addAttachmentInECM(ecmService, trackingRecord, basePath, fileName, IOUtils.toByteArray(inputStream));
				serffService.saveSerffDocument(attachment);
				
			}
			else {
				message.append(MSG_ERROR_CROSSWALK_PLANS);
				trackingRecord.setRequestStateDesc("Content is not valid for file: " + fileName);
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_CROSSWALK_PLANS);
			trackingRecord.setRequestStateDesc(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		return attachment;
	}
	/**
	 * 
	 * @param message
	 * @param issuerSheet
	 * @param crossWalkSheet
	 * @param trackingRecord
	 * @param applicableYear
	 * @param currentIssuer
	 * @param userId
	 * @return
	 */
	private boolean readAndPersistCrosswalkData(StringBuilder message, String fileName, XSSFSheet issuerSheet, XSSFSheet crossWalkSheet,
			SerffPlanMgmt trackingRecord, String applicableYear, Issuer currentIssuer, int userId) {
		
		LOGGER.debug("readAndPersistCrosswalkData() Start");
		boolean status = Boolean.FALSE;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		if (null == entityManager) {
			LOGGER.debug("entityManager is null");
			return status;
		}

		entityManager.getTransaction().begin();
		try {
			
			if (null == crossWalkSheet || issuerSheet == null) {
				trackingRecord.setPmResponseXml("Required CrossWalk Sheet is missing.");
				LOGGER.error(trackingRecord.getPmResponseXml());
				message.append(MSG_ERROR_CROSSWALK_PLANS);
				return status;
			}
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = issuerSheet.iterator();
			boolean isFirstRow = true;
			boolean isSecondRow = true;
			String hiosId = null;
			String state = null;
			String dentalOnlyPlan = null;
			String currentState = StringUtils.isNotBlank(currentIssuer.getStateOfDomicile()) ? currentIssuer.getStateOfDomicile() : currentIssuer.getState();

			if (StringUtils.isBlank(currentState)) {
				trackingRecord.setPmResponseXml(" State Code is missing for Current Issuer: " + currentIssuer.getHiosIssuerId());
				LOGGER.error(trackingRecord.getPmResponseXml());
				message.append(MSG_ERROR_CROSSWALK_PLANS).append(trackingRecord.getPmResponseXml());
				return status;
			}

			while (rowIterator.hasNext()) {
				Row excelRow = rowIterator.next();	// HIOS Issuer ID
				
				if (isFirstRow || isSecondRow) {
					if(isFirstRow) {
						isFirstRow = false;
					} else {
						isSecondRow = false;
					}
				
					continue;
				}

				if (excelRow != null) {
					hiosId = getCellValue(excelRow.getCell(HIOS_ID_COLUMN_INDEX), true);
				}

				excelRow = rowIterator.next();	// Issuer State
				if (excelRow != null) {
					state = getCellValue(excelRow.getCell(STATE_COLUMN_INDEX), true);
				}
				rowIterator.next();				// Market Coverage
				excelRow = rowIterator.next();	// Dental Only Plan
				if (excelRow != null) {
					dentalOnlyPlan = getCellValue(excelRow.getCell(DENTAL_ONLY_PLAN), true);
				}
				break;
			}
			
			if (StringUtils.isNotBlank(hiosId) && StringUtils.isNotBlank(state)) {
				List<CrossWalk> CrosswalkList = new ArrayList<CrossWalk>();
				rowIterator = crossWalkSheet.iterator();
				isFirstRow = true;
				isSecondRow = true;
				int counter = 3;
				String dentalOnly = null;
				HashSet<String> hiosIssuerIdList = new HashSet<String>();
				hiosIssuerIdList.add(hiosId);

				if (PlanMgmtConstants.YES.equalsIgnoreCase(dentalOnlyPlan)
						|| PlanMgmtConstants.Y.equalsIgnoreCase(dentalOnlyPlan)) {
					dentalOnly = CrossWalk.DENTAL_ONLY.Y.name();
				}
				else if (PlanMgmtConstants.NO.equalsIgnoreCase(dentalOnlyPlan)
						|| PlanMgmtConstants.N.equalsIgnoreCase(dentalOnlyPlan)) {
					dentalOnly = CrossWalk.DENTAL_ONLY.N.name();
				}
				else {
					message.append(MSG_ERROR_CROSSWALK_PLANS).append(" 'Dental Only Plan' is empty / invalid in issuer information.");
					return status;
				}
				
				while (rowIterator.hasNext()) {
					
					Row excelRow = rowIterator.next();
					
					if (isFirstRow || isSecondRow) {

						if (isFirstRow) {
							isFirstRow = false;
						}
						else {
							isSecondRow = false;
						}
						continue;
					}
					
					if (StringUtils.isBlank(getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CURRENT_PLAN_ID.getColumnIndex())))) {
						LOGGER.debug("Empty row of crosswalk template.");
						continue;
					}
					
					CrossWalk crosswalk = setCrosswalkExcelData(message, excelRow, hiosId, state, hiosIssuerIdList, dentalOnly, applicableYear, currentIssuer, currentState, userId);
					
					if (null == crosswalk) {
						LOGGER.debug("crosswalk object id null");
						status = Boolean.FALSE;
						trackingRecord.setPmResponseXml("Crosswalk upload failed as plan data is invalid ");
						
						if (StringUtils.isBlank(message.toString())) {
							message.append(MSG_ERROR_CROSSWALK_PLANS);
						}
						message.append(" This error was detected on row " + counter + " of '" + fileName + "'");
						return status;
					}
					else {
						CrosswalkList.add(crosswalk);
					}
					counter++;
				}
				LOGGER.debug("Adding crosswalk records - count: " + CrosswalkList.size());
				
				if(!CollectionUtils.isEmpty(CrosswalkList)){
					//Soft deleting existing records
					softDeleteExixtingCrosswalk(entityManager, hiosId, dentalOnly, applicableYear, userId);
					for(CrossWalk crosswalkPlan : CrosswalkList ){
						entityManager.merge(crosswalkPlan);
					
					}
					entityManager.getTransaction().commit();
					status = Boolean.TRUE;
					if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
						trackingRecord.setPmResponseXml(MSG_CROSSWALK_PLANS);
					}
				}
				else {
					LOGGER.error("Invalid CrossWalk Sheet");
					trackingRecord.setPmResponseXml("Invalid CrossWalk Sheet ");
					message.append(MSG_ERROR_CROSSWALK_PLANS);
				}
			
			}
			else {
				LOGGER.error("Required Hios id or state is empty");
				trackingRecord.setPmResponseXml("Crosswalk upload failed as Hios issuer id or state is empty ");
				message.append(MSG_ERROR_CROSSWALK_PLANS);
			}
		}
		catch (Exception ex) {
			entityManager.getTransaction().rollback();
			LOGGER.error(ex.getMessage(), ex);
			message.append(MSG_ERROR_CROSSWALK_PLANS);
		}
		finally {

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.debug("readAndPersistCrosswalkData() End");
		}
		return status;
	}
	
	/**
	 * This method soft deleted already existing crosswalk for Issuer
	 * @param entityManager
	 * @param hiosId
	 * @param applicableYear
	 */
	private void softDeleteExixtingCrosswalk(EntityManager entityManager, String hiosId, String dentalOnly, String applicableYear, int userId) {

		LOGGER.info("softDeleteExixtingCrosswalk() : starts");

		try {
			List<CrossWalk> existingCrossWalkList = icrosswalkRepo.getAllCrossWalkPlansforIssuer(hiosId, dentalOnly, Integer.parseInt(applicableYear));
			LOGGER.debug("Fetching list of exixting crosswalk");

			if (!CollectionUtils.isEmpty(existingCrossWalkList)) {

				for (CrossWalk existingCrosswalk : existingCrossWalkList) {
					existingCrosswalk.setIsDeleted(CrossWalk.IS_DELETED.Y.name());
					existingCrosswalk.setLastUpdateBy(userId);
					entityManager.merge(existingCrosswalk);
				}
			}
			else {
				LOGGER.debug("Existing crosswalk record is not present for issuer : "+hiosId);
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("softDeleteExixtingCrosswalk() : ends");
		}
	}


	/**
	 * 
	 * @param message
	 * @param excelRow
	 * @param hisoIssuerId
	 * @param state
	 * @param applicableYear
	 * @param currentIssuer
	 * @param userId
	 * @return
	 */
	private CrossWalk setCrosswalkExcelData(StringBuilder message, Row excelRow, String hisoIssuerId, String state, HashSet<String> hiosIssuerIdList, String dentalOnly,
			String applicableYear, Issuer currentIssuer, String currentState, int userId) {

		LOGGER.info("setCrosswalkExcelData() Starts");
		CrossWalk crosswalk = null;

		try {

			if (null != excelRow) {

				String currentPlan = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CURRENT_PLAN_ID.getColumnIndex()));
				Cell countyCell = excelRow.getCell(CROSSWALKExcelColumnEnum.COUNTY_NAME_CODE.getColumnIndex());
				String county = null;
				String countyName = null;
				String countyCode = null;

				if (null != countyCell) {
					county = countyCell.getStringCellValue();

					if (StringUtils.isNotBlank(county)) {
						String[] countyString = county.trim().split("-");
						countyName = countyString[SPLIT_INDEX1];
						countyCode = countyString[SPLIT_INDEX2];
					}
				}
				String crossWalkPlan = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CROSSWALK_PLAN_ID.getColumnIndex()));
				String ziplist = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.SERVICE_AREA_ZIP_CPDES.getColumnIndex()));
				String isCatastrophicOrChildOnly = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.IS_CATASTROPHIC_OR_CHILD_ONLY.getColumnIndex()));
				String agingOffCrosswalkPlanId = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.AGING_OFF_CROSSWALK_PLAN_ID.getColumnIndex()));
				// Validating Require data of Crosswalk template. 
				boolean valid = validateHiosAndPlans(message, hisoIssuerId, state, hiosIssuerIdList, currentPlan, crossWalkPlan, currentIssuer, currentState, applicableYear,
						isCatastrophicOrChildOnly, agingOffCrosswalkPlanId);

				String crosswalkLevel = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CROSSWALK_LEVEL.getColumnIndex()));
				String crosswalkReason = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CROSSWALK_REASON.getColumnIndex()));
				String currentPlanFormNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CUR_PLAN_FORM_NUM.getColumnIndex()));
				String currentPlanNAICTrackNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CUR_PLAN_NAIC_TRACK_NUM.getColumnIndex()));
				String crosswalkPlanFormNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CW_PLAN_FORM_NUM.getColumnIndex()));
				String crosswalkPlanNAICTrackNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CW_PLAN_NAIC_TRACK_NUM.getColumnIndex()));
				String crosswalkAgingOffPlanFormNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CW_AGINGOFF_PLAN_FORM_NUM.getColumnIndex()));
				String crosswalkAgingOffPlanNAICTrackNumber = getCellValueTrim(excelRow.getCell(CROSSWALKExcelColumnEnum.CW_AGINGOFF_PLAN_NAIC_TRKNUM.getColumnIndex()));

				if (valid) {
					LOGGER.debug("Crosswalk templaet is valid");
					crosswalk = new CrossWalk();
					crosswalk.setCrosswalkTier(getTierforCrossWalk(county , ziplist));
					crosswalk.setHisoIssuerId(hisoIssuerId);
					crosswalk.setCountyName(StringUtils.trim(countyName));
					crosswalk.setCountyCode(StringUtils.trim(countyCode));
					crosswalk.setCurrentHiosPlanId(currentPlan);
					crosswalk.setCrosswalkHiosPlanId(crossWalkPlan);
					crosswalk.setZiplist(ziplist);
					crosswalk.setCrosswalkLevel(StringUtils.isNotBlank(crosswalkLevel) ? crosswalkLevel : null);
					crosswalk.setCrosswalkReason(StringUtils.isNotBlank(crosswalkReason) ? crosswalkReason : null);
					crosswalk.setIsDeleted(CrossWalk.IS_DELETED.N.name());

					if (null != applicableYear) {
						crosswalk.setApplicableYear(Integer.parseInt(applicableYear));
					}
					crosswalk.setCreatedBy(userId);
					crosswalk.setLastUpdateBy(userId);

					if (PlanMgmtConstants.YES.equalsIgnoreCase(isCatastrophicOrChildOnly)
							|| PlanMgmtConstants.Y.equalsIgnoreCase(isCatastrophicOrChildOnly)) {
						crosswalk.setIsCatastrophicOrChildOnly("Y");
					}
					else {
						crosswalk.setIsCatastrophicOrChildOnly("N");
					}
					crosswalk.setAgingOffCrosswalkPlanId(StringUtils.isNotBlank(agingOffCrosswalkPlanId) ? agingOffCrosswalkPlanId : null);
					crosswalk.setCurrentPlanFormNumber(StringUtils.isNotBlank(currentPlanFormNumber) ? currentPlanFormNumber : null);
					crosswalk.setCurrentPlanNAICTrackNumber(StringUtils.isNotBlank(currentPlanNAICTrackNumber) ? currentPlanNAICTrackNumber : null);
					crosswalk.setCrosswalkPlanFormNumber(StringUtils.isNotBlank(crosswalkPlanFormNumber) ? crosswalkPlanFormNumber : null);
					crosswalk.setCrosswalkPlanNAICTrackNumber(StringUtils.isNotBlank(crosswalkPlanNAICTrackNumber) ? crosswalkPlanNAICTrackNumber : null);
					crosswalk.setCrosswalkAgingOffPlanFormNumber(StringUtils.isNotBlank(crosswalkAgingOffPlanFormNumber) ? crosswalkAgingOffPlanFormNumber : null);
					crosswalk.setCrosswalkAgingOffPlanNAICTrackNumber(StringUtils.isNotBlank(crosswalkAgingOffPlanNAICTrackNumber) ? crosswalkAgingOffPlanNAICTrackNumber : null);

					if (StringUtils.isNotBlank(dentalOnly)) {
						crosswalk.setDentalOnly(dentalOnly);
					}
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while persisting data: ", e);
			message.append(MSG_ERROR_CROSSWALK_PLANS);
			return null;
		}
		finally {
			LOGGER.info("setCrosswalkExcelData() Ends");
		}
		return crosswalk;
	}

	/**
	 * This method checks possible value of crosswalk tier 
	 * @param county
	 * @param ziplist
	 * @return
	 */
	private String getTierforCrossWalk(String county, String ziplist) {
		String tier = null;
		LOGGER.info("getTierforCrossWalk() : Starts");
			if(StringUtils.isNotBlank(ziplist)){
				tier = "ZIP";
			}else if(StringUtils.isNotBlank(county)){
				tier = "COUNTY";
			}else{
				tier = "PLAN";
			}
		
		LOGGER.info("getTierforCrossWalk() : ends");
		return tier;
		
	}


	/**
	 * This method validates if current plan and crosswalk plan is valid for current issuer
	 */
	private boolean validateHiosAndPlans(StringBuilder message, String hisoIssuerId, String state,
			HashSet<String> hiosIssuerIdList, String currentPlan, String crossWalkPlan, Issuer currentIssuer,
			String currentState, String applicableYear, String isCatastrophicOrChildOnly, String agingOffCrosswalkPlanId) {

		LOGGER.info("validateHiosAndPlans() :starts");
		boolean valid = false;

		try {
			String currentPlanIdPrefix = null;
			String stateCodeOfCrossWalkPlanId = null;
			String stateCodeOfAgingOffCrosswalkPlanId = null;
			String hiosPlanIdPrefix = null;
			String currentIssuerPlanidPrefix = null;
			boolean nullValueFlag = StringUtils.isBlank(hisoIssuerId) || StringUtils.isBlank(state) || StringUtils.isBlank(currentPlan);
			String errorMessage = StringUtils.EMPTY;

			if (!(nullValueFlag || StringUtils.isBlank(crossWalkPlan) || !StringUtils.isNumeric(applicableYear))) {
				currentIssuerPlanidPrefix = currentIssuer.getHiosIssuerId() + currentState;
				currentPlanIdPrefix = currentPlan.length() > HIOS_PLAN_ID_INDEX ? currentPlan.substring(0, HIOS_PLAN_ID_INDEX) : currentPlan;
				stateCodeOfCrossWalkPlanId = crossWalkPlan.length() > HIOS_PLAN_ID_INDEX ? crossWalkPlan.substring(STATE_CODE_INDEX, HIOS_PLAN_ID_INDEX) : crossWalkPlan;
				hiosPlanIdPrefix = hisoIssuerId.concat(state);
				errorMessage = " Current 'HIOS Issuer ID' & 'State'(" + currentIssuer.getHiosIssuerId() + " " + currentState + CLOSING_BRACKET;

				if (currentIssuerPlanidPrefix.equals(hiosPlanIdPrefix)) {
					valid = true;
				}
				else {
//					valid = false;
					errorMessage += ", Template's 'HIOS Issuer ID' & 'State'(" + hisoIssuerId + " " + state + CLOSING_BRACKET;
				}

				if (!currentState.equals(stateCodeOfCrossWalkPlanId)) {
					valid = false;
					errorMessage += ", Mismatched state code in crosswalk 'HIOS Plan ID'(" + crossWalkPlan + CLOSING_BRACKET;
				}

				if (!currentIssuerPlanidPrefix.equals(currentPlanIdPrefix)) {
					valid = false;
					errorMessage += ", Current 'HIOS Plan ID'(" + currentPlan + CLOSING_BRACKET;
				}

				if (StringUtils.isNotBlank(agingOffCrosswalkPlanId)) {
					stateCodeOfAgingOffCrosswalkPlanId = agingOffCrosswalkPlanId.length() > HIOS_PLAN_ID_INDEX ? agingOffCrosswalkPlanId.substring(STATE_CODE_INDEX, HIOS_PLAN_ID_INDEX) : agingOffCrosswalkPlanId;

					if (!currentState.equals(stateCodeOfAgingOffCrosswalkPlanId)) {
						valid = false;
						errorMessage += ", Mismatched 'State Code' in 'Plan ID for Enrollees Aging off Catastrophic or Child-Only Plan'(" + agingOffCrosswalkPlanId + CLOSING_BRACKET;
					}
				}
			}
			else {
				valid = false;
				errorMessage = " Due to required values are empty";
			}

			if (valid) {

				if (!validateHiosIssuerIdInDB(crossWalkPlan.substring(0, STATE_CODE_INDEX), hiosIssuerIdList)) {
					valid = false;
					errorMessage = " Carrier of 'Crosswalk Plan' ["+ crossWalkPlan +"] does not exist in database.";
				}

				if (StringUtils.isNotBlank(agingOffCrosswalkPlanId) &&
						!validateHiosIssuerIdInDB(agingOffCrosswalkPlanId.substring(0, STATE_CODE_INDEX), hiosIssuerIdList)) {
					valid = false;
					errorMessage = " Carrier of 'Plan ID for Enrollees Aging off Catastrophic or Child-Only Plan' ["+ agingOffCrosswalkPlanId +"] does not exist in database.";
				}
			}

			if (!valid) {
				message.append(MSG_ERROR_CROSSWALK_PLANS).append(errorMessage).append(".");
				return valid;
			}
			List<String> list = Arrays.asList(OPTION_VALUE.split(","));
			valid = false;

			if (StringUtils.isNotBlank(isCatastrophicOrChildOnly)
					&& list.contains(isCatastrophicOrChildOnly.toLowerCase())) {

				if (isCatastrophicOrChildOnly.equalsIgnoreCase(PlanMgmtConstants.YES)
						&& StringUtils.isNotBlank(agingOffCrosswalkPlanId)) {
					valid = true;
				}
				else if (!isCatastrophicOrChildOnly.equalsIgnoreCase(PlanMgmtConstants.YES)) {
					valid = true;
				}
				else {
					message.append(MSG_ERROR_CROSSWALK_PLANS).append(" 'Plan ID for Enrollees Aging off Catastrophic or Child-Only Plan' is require.");
				}
			}
			else if (StringUtils.isBlank(isCatastrophicOrChildOnly)) {
				valid = true;
			}
			else {
				message.append(MSG_ERROR_CROSSWALK_PLANS).append(" Invalid value for 'Catastrophic or Child-Only Plan'.");
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_CROSSWALK_PLANS);
			LOGGER.error("Error while validating Crosswalk: ", ex);
		}
		finally {

			if (!valid) {

				if (StringUtils.isBlank(message)) {
					message.append(MSG_ERROR_CROSSWALK_PLANS);
				}
				LOGGER.error(SecurityUtil.sanitizeForLogging(message.toString()));
			}
			LOGGER.info("validateHiosAndPlans() :ends");
		}
		return valid;
	}

	private boolean validateHiosIssuerIdInDB(String hiosIssuerID, HashSet<String> hiosIssuerIdList) {

		boolean validHiosIssuerId = false;

		if (!hiosIssuerIdList.contains(hiosIssuerID)) {
			Issuer crossWalkIssuer = issuerService.getIssuerByHiosID(hiosIssuerID);

			if (null != crossWalkIssuer) {
				hiosIssuerIdList.add(hiosIssuerID);
				validHiosIssuerId = true;
			}
		}
		else {
			validHiosIssuerId = true;
		}
		return validHiosIssuerId;
	}
}
