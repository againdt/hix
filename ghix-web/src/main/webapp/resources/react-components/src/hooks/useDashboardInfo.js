import React, {useEffect, useReducer} from 'react';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';

import ProcessRules from '../rules/processRules';
import ProcessFacts from '../rules/processFacts';
import ProcessHouseHold from '../rules/processHouseHold';
import ProcessPlans from '../rules/processPlans';
import ProcessPlanRules from '../rules/processPlanRules';
import ProcessDentalPlans from '../rules/processDentalPlans';
import ProcessDentalPlanRules from '../rules/processDentalPlanRules';
import ProcessApplicationStatus from '../rules/processApplicationStatus';
import ProcessApplicationStatusRules from '../rules/processApplicationStatusRules';

const actions = {
    LOAD_SUCCESS: 'LOAD_SUCCESS',
    LOAD_FAILURE: 'LOAD_FAILURE'
};

function reducer(state, {type, payload}) {
    switch (type) {
        case actions.LOAD_SUCCESS:
            return {...state, yearlyData: payload.yearlyDatas, dashboardModel: payload.dashboardModel, isLoading: false};
        case actions.LOAD_FAILURE:
            return {...state, yearlyData: [], dashboardModel: {}, err: payload, isLoading: false, isError: true};
        default:
            return state;
    }
}

function processFurtherActionContent(config, responseData, data) {
    let currentDate = config.serverDate ? moment(config.serverDate, 'MM-DD-YYYY') : moment();
    let hasDental = responseData.dentalPlans && responseData.dentalPlans.type && responseData.dentalPlans.type !== 'SINGLE_COL';
    const hasHealth = responseData.healthPlans && responseData.healthPlans.type && responseData.healthPlans.type !== 'SINGLE_COL';
    
    return  <FormattedMessage id={data.paragraphText.props ? data.paragraphText.props.id : data.paragraphText} values={{
        OEPEndDate: config.oepEndDate,
        ishealth: hasHealth ? <FormattedMessage id="further_action.coverage_status.health"/> : "",
        isdental: hasHealth && hasDental ? <FormattedMessage id="further_action.coverage_status.and_dental"/> : (hasDental ? <FormattedMessage id="further_action.coverage_status.dental"/> : ""),
        upcomingyear: currentDate.year() + 1,
        QLElink: <FormattedMessage id="further_action.coverage_status.qualifying_life_event"/>,
        currentyear: currentDate.year()
    }}/>
}

function processStatus(status){

    if(status.toLowerCase() === 'confirm'){
        return <FormattedMessage id="plans.status.enrolled"/>;
    }

    if(status.toLowerCase() === 'term' || status.toLowerCase() === 'terminated'){
        return <FormattedMessage id="plans.status.terminated"/>;
    }

    if(status.toLowerCase() === 'pending'){
        return <FormattedMessage id="plans.status.pending"/>;
    }

    return status
}

function processHealthDentalPlans(planDataInfo, planType, rules, tabData) {

    // console.log("processHealthDentalPlans - planDataInfo" + JSON.stringify(planDataInfo));
    // console.log("processHealthDentalPlans - planType" + JSON.stringify(planType));
    // console.log("processHealthDentalPlans - rules" + JSON.stringify(rules));

    let planInfo = planDataInfo[0];

    if(!planInfo) {
        return null;
    }

    let response = {
        type: planInfo.type,
        planType: planType
    };

    if (planInfo.type === 'SINGLE_COL') {

        response.singleColData = {
            col1: planInfo.col1 ? <FormattedMessage id={planInfo.col1}/> : "",
            col2: planInfo.col2 ? <FormattedMessage id={planInfo.col2}/> : "",
            linkInfo: {
                link: 'www.getinsured.com',
                text: planInfo.col3 ? <FormattedMessage id={planInfo.col3}/> : "",
            }
        };
    }

    if (planInfo.type === 'MULTIPLE_COL'){
        // If enrollee has this status, we are not going to display it.
        const ENROLLEE_STATUS = ['TERM', 'CANCEL'];
        const ENROLLEE_STATUS_CANCEL = 'CANCEL';
        const ENROLLMENT_STATUS_TERM = 'TERM';
        // console.log(planType + " Rules: " + JSON.stringify(rules));

        if(tabData && tabData.enrollments && tabData.enrollments.length > 0) {

            let panTypeEnrollments = tabData.enrollments.filter(x => x.type.toLowerCase() === planType.toLowerCase());

            let enrollments = panTypeEnrollments.map(function (enrollment) {

                // Array of enrollees (members) that are going to be shown.
                let eligibleEnrollees = [];

                // If enrollmentStatus is 'TERM' any enrollee with CANCEL status will not be shown,
                // any enrollee with any status other than CANCEL ( ACTIVE, PENDING, TERM) will be shown.
                if (enrollment.enrollmentStatus === ENROLLMENT_STATUS_TERM) {
                    eligibleEnrollees = enrollment.enrollees.filter(enrollee => {
                        if (enrollee.status !== ENROLLEE_STATUS_CANCEL) {
                            return enrollee;
                        }
                    })
                } else {
                    eligibleEnrollees = enrollment.enrollees.filter(enrollee => {
                        if (!ENROLLEE_STATUS.includes(enrollee.status)) {
                            return enrollee;
                        }
                    })
                }

                return {
                    name: enrollment.plan.issuerName,
                    type: enrollment.plan.planName,
                    status: enrollment.enrollmentStatus, // HIX-109959,
                    membersInfo: eligibleEnrollees && eligibleEnrollees.length && eligibleEnrollees.map(x => x.firstName + " " + x.middleName + " " + x.lastName + " " + x.suffix),
                    count: eligibleEnrollees && eligibleEnrollees.length ? eligibleEnrollees.length : 0,
                    enrollmentId: enrollment.id
                }
            });

            response.multiColData = enrollments.map(function (enrollment) {
                const planStatusPending = 'pending';

                return {
                    plan: enrollment,
                    status: enrollment.status ?
                        (processStatus(enrollment.status )): planInfo.col2, // HIX-109959
                    tooltipCol2Content: enrollment.status && (enrollment.status).toLowerCase() === planStatusPending ?
                        <FormattedMessage id="further_action.cta.pending_tooltip"/> : '',
                    linkInfo: {
                        link: 'www.getinsured.com',
                        text:  planInfo.col3 ? <FormattedMessage id={planInfo.col3}/> : '',
                    }
                }
            });
        } else if(tabData && tabData.preEligibility && tabData.preEligibility.plans && tabData.preEligibility.plans.length > 0) {
            let planTypeEnrollments = tabData.preEligibility.plans.filter(x => x.type.toLowerCase() === planType.toLowerCase());
            const planStatusInCart = 'plans.status.in_your_cart';

            response.multiColData = planTypeEnrollments.map((plan) => {
                const {issuerName: name, planName: type} = plan;
                return {
                    plan: {name, type},
                    status: planInfo.col2 ? <FormattedMessage id={planInfo.col2}/> : '',
                    tooltipCol2Content: planInfo.col2 === planStatusInCart ? <FormattedMessage id="further_action.cta.view_in_your_cart_tooltip"/> : '',
                    linkInfo: {
                        link: 'www.getinsured.com',
                        text:  planInfo.col3 ? <FormattedMessage id={planInfo.col3 }/> : '',
                    }
                }
            });
        }
    }

    // console.log("Enrollement Plans - " + planType + " === " +  JSON.stringify(response));

    return response;
}

function getTabData(tabData, config) {

    return new Promise((resolve, reject) => {
        // -- Process Facts --- //
        let processFacts = new ProcessFacts(tabData, config);
        let facts = processFacts.executeFacts();
        // console.log("processFacts : " + JSON.stringify(facts));
        let processRules = new ProcessRules(facts);

        processRules.executeRules().then(function (data) {
            let processHouseHold = new ProcessHouseHold(tabData, config);
            let houseHoldData = processHouseHold.getHouseHoldInfo();
            let assessedMedicaidOrChipEligible = processHouseHold.getAssessedMedicaidOrChipEligibility();
            let responseData = {
                matchedRowId: data[0].id,
                encyptedCaseNumber: tabData.encyptedCaseNumber,
                whereUserLeftOff: tabData.whereUserLeftOff,
                caseNumber: tabData.caseNumber,
                isFinancial: tabData.isFinancial,
                applicationStatus: tabData.applicationStatus,
                enrollments: tabData.enrollments,
                tobbacoEnabled: config.tobbacoEnabled,
                hardshipEnabled: config.hardshipEnabled,
                serverDate: config.serverDate,
                ssapFlowVersion: config.ssapFlowVersion,
                isEligibilityConditional: tabData.isEligibilityConditional,
                isAssessedMedicaidOrChipEligible : assessedMedicaidOrChipEligible,
                houseHoldInfo: houseHoldData ? houseHoldData : null,
                coverageStatus: {
                    header: <FormattedMessage id="your_heath_coverage_status"/>,
                    status: <FormattedMessage id={data[0].healthCoverageStatus}/>,
                    headingType: 'ds-u-color--success'
                },
            };
            //TODO: Refactor this section after Demo..

            if( (data[0].id === "127" || data[0].id === "128") &&
                (data[0].paragraphText.indexOf(("{dental} - (if enrolled in dental)")) !== -1)){
                let panTypeEnrollments = tabData.enrollments.filter(x => x.type.toLowerCase() === "dental");
                if(panTypeEnrollments.length > 0){
                    responseData.furtherAction.content = data[0].paragraphText.replace("{dental} - (if enrolled in dental)", "dental");
                } else {
                    responseData.furtherAction.content = data[0].paragraphText.replace("and {dental} - (if enrolled in dental)", "");
                }
            }

            let processHealthPlanRules = new ProcessPlans(tabData, config);
            let healthRules = processHealthPlanRules.getHealthPlanInfo();
            let processPlanRules = new ProcessPlanRules(healthRules);

            processPlanRules.executeRules().then(function (healthPlanInfo) {
                // console.log("Processing health Plan Rules ----- : " + JSON.stringify(healthPlanInfo));
                responseData.healthPlans = processHealthDentalPlans(healthPlanInfo, 'Health', healthRules, tabData);

                let processDentalPlan = new ProcessDentalPlans(tabData, config);
                let dentalRules = processDentalPlan.getDentalPlanInfo();
                let processDentalPlanRules = new ProcessDentalPlanRules(dentalRules);

                processDentalPlanRules.executeRules().then(function (dentalPlanInfo) {
                    // console.log("Processing Dental Plan Rules ----- : " + JSON.stringify(dentalPlanInfo));
                    responseData.dentalPlans = processHealthDentalPlans(dentalPlanInfo, 'Dental', dentalRules, tabData);

                    responseData.furtherAction = {
                        header: data[0].paragraphHeader && <FormattedMessage id={data[0].paragraphHeader.props ? data[0].paragraphHeader.props.id : data[0].paragraphHeader}/>,
                        content: data[0] && processFurtherActionContent(config, responseData, data[0]),
                        buttonText: data[0].ctaButton && <FormattedMessage id={data[0].ctaButton.props ? data[0].ctaButton.props.id : data[0].ctaButton} />,
                        linkDestination: data[0].linkDestination || ''
                    };

                    let processApplicationStatus = new ProcessApplicationStatus(tabData, config);
                    let applicationStatuses = processApplicationStatus.getApplicationStatus();
                    let processApplicationStatusRules = new ProcessApplicationStatusRules(applicationStatuses);

                    processApplicationStatusRules.executeRules().then(function (applicationStatusInfo) {
                        // console.log("processApplicationStatusRules ----- : " + JSON.stringify(applicationStatusInfo));

                        let applicationInfo = applicationStatusInfo[0];
                        let applicationStatusInfoData = applicationStatuses;

                        responseData.applicationStatusInfo = {
                            period: applicationStatusInfoData.period,
                            applicationYear: applicationStatusInfoData.applicationYear ? applicationStatusInfoData.applicationYear : "-",
                            applicationMembersCount: applicationStatusInfoData.hhMemebersTotal ? applicationStatusInfoData.hhMemebersTotal : 0,
                            applicantsInfo: applicationStatusInfoData.applicantsInfo,
                            status: applicationInfo.col2,
                            linkInfo: {
                                link: 'www.getinsured.com',
                                text: applicationInfo.col3
                            }
                        };

                        // console.log("responseData.applicationStatusInfo ===" + JSON.stringify(responseData.applicationStatusInfo));

                        // console.log("dispatch:" + JSON.stringify(responseData));

                        resolve(responseData);

                    });
                });

            });
        }, function (err) {
            reject(null);
        })
    });

}

export default ({userId}) => {
    let [data, dispatch] = useReducer(reducer, {
        yearlyData: [],
        isLoading: false,
        isError: false
    });

    let fetchDashboardInfo = (userId) => {
        // dispatch({type:types.REQUEST_DASHBOARD_INFO, data: {userId}});
        axios.get('indportal/getDashboardModels', {
            params: {userId}
        })
        .then((response) => {
            let responseJSON = response.data;
            // console.log("responseJSON--" + JSON.stringify(responseJSON));
            let config = responseJSON.configuration;
            let tabs = responseJSON.tabs;
            let yearlyDatas = [];
    
            for(let i = 0; i < tabs.length; i++){
                getTabData(tabs[i], config).then(function(tabData) {
                    yearlyDatas.push(tabData);
    
                    if(i === tabs.length -1){
                        // console.log("Yearly tabbed Data --- " + JSON.stringify(yearlyDatas));
                        dispatch({
                            type: actions.LOAD_SUCCESS,
                            payload: {yearlyDatas: yearlyDatas, dashboardModel: responseJSON}
                        });
                    }
                });
            }
        })
        .catch((err) => {
            dispatch({
                type:actions.LOAD_FAILURE,
                payload: err
            });
        });
    };

    useEffect(() => {
        fetchDashboardInfo(userId)
    }, []);

    return {
        ...data
    }
}