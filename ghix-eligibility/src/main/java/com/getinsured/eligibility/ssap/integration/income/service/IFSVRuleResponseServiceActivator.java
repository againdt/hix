package com.getinsured.eligibility.ssap.integration.income.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value="ifsvRuleResponseServiceActivator")
public class IFSVRuleResponseServiceActivator {
    
    private static final Logger LOGGER = Logger.getLogger(IFSVRuleResponseServiceActivator.class);
    
    @ServiceActivator
    public String receive(Message<String> message) {
        String payload = message.getPayload();
        String nextPayload = null;
        if(StringUtils.isNotBlank(payload) && "false".equals(payload)) {
            nextPayload = message.getHeaders().get("IFSV_REQUEST").toString();
        } else {
            LOGGER.info("Stopping after income verification as IRS verification returned success for :" + message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER));
        }
        return nextPayload;
    }
}
