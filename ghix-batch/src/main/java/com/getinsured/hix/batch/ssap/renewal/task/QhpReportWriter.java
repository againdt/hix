/**
 * 
 */
package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.ssap.renewal.service.QhpReportService;
import com.getinsured.hix.batch.ssap.renewal.util.QhpReportPartitionerParams;
import com.getinsured.hix.dto.enrollment.QhpReportDTO;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

/**
 * QHP Report Writer class is used to clean Old QHP Reports data using using Job Execution ID.
 * 
 * @since June 14, 2019
 */
public class QhpReportWriter implements ItemWriter<List<QhpReportDTO>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private QhpReportService qhpReportService;
	private QhpReportPartitionerParams qhpReportPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends List<QhpReportDTO>> qhpReportDTOListToWriter) throws Exception {

		String batchJobStatus=null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		boolean qhpReportsStatus = false;

		if (CollectionUtils.isNotEmpty(qhpReportDTOListToWriter)) {

			List<QhpReportDTO> qhpReportDTOList = qhpReportDTOListToWriter.stream().flatMap(List::stream).collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(qhpReportDTOList)) {
				qhpReportsStatus = qhpReportService.storeQHPReportData(qhpReportDTOList, jobExecutionId);
			}
		}

		if (qhpReportsStatus) {
			LOGGER.debug("Successfully load QHP Reports data in database.");
		}
		else {
			qhpReportService.saveAndThrowsErrorLog("Failed load QHP Reports data in database.");
		}
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public QhpReportService getQhpReportService() {
		return qhpReportService;
	}

	public void setQhpReportService(QhpReportService qhpReportService) {
		this.qhpReportService = qhpReportService;
	}

	public QhpReportPartitionerParams getQhpReportPartitionerParams() {
		return qhpReportPartitionerParams;
	}

	public void setQhpReportPartitionerParams(QhpReportPartitionerParams qhpReportPartitionerParams) {
		this.qhpReportPartitionerParams = qhpReportPartitionerParams;
	}
}
