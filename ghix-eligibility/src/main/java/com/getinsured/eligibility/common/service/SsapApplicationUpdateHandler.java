package com.getinsured.eligibility.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.common.dto.SsapApplicationUpdateDTO;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class SsapApplicationUpdateHandler {
	
	@Autowired
    private SsapApplicationRepository ssapApplicationRepository;

	public SsapApplication updateSsapApplicationData(SsapApplicationUpdateDTO ssapApplicationUpdate, Long applicationId) {
		
		if(ssapApplicationUpdate == null) {
			return null; 
		}
        // Re-fetch the application
    	SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);

    	if(ssapApplication == null) {
    		throw new GIRuntimeException("Ssap application not found to update for application id: " + applicationId);
    	}
    	
    	if(ssapApplicationUpdate.isAllowEnrollmentFlag()) { 
    		ssapApplication.setAllowEnrollment(ssapApplicationUpdate.getAllowEnrollment()); 
    	}
    	
    	if(ssapApplicationUpdate.isApplicationStatusFlag()) { 
    		ssapApplication.setApplicationStatus(ssapApplicationUpdate.getApplicationStatus());
    	}

    	if(ssapApplicationUpdate.isCsrLevelFlag()) { 
    		ssapApplication.setCsrLevel(ssapApplicationUpdate.getCsrLevel());
    	}
    	
    	if(ssapApplicationUpdate.isEhbAmountFlag()) { 
    		ssapApplication.setEhbAmount(ssapApplicationUpdate.getEhbAmount());
    	}
    	
    	if(ssapApplicationUpdate.isEligibilityReceivedDateFlag()) {
    		ssapApplication.setEligibilityReceivedDate(ssapApplicationUpdate.getEligibilityReceivedDate());
    	}
    	
    	if(ssapApplicationUpdate.isEligibilityStatusFlag()) {
    		ssapApplication.setEligibilityStatus(ssapApplicationUpdate.getEligibilityStatus());
    	}
    	
    	if(ssapApplicationUpdate.isExchangeEligibilityStatusFlag()) {
    		ssapApplication.setExchangeEligibilityStatus(ssapApplicationUpdate.getExchangeEligibilityStatus());
    	}
    	
    	if(ssapApplicationUpdate.isNativeAmericanFlag()) {
    		ssapApplication.setNativeAmerican(ssapApplicationUpdate.getNativeAmerican());
    	}

    	if(ssapApplicationUpdate.isUpdated()) {
    		ssapApplication = ssapApplicationRepository.save(ssapApplication);
    	}
    	
    	return ssapApplication;
	}

}