// JavaScript Document

//$(window).load(function(){
//	$('body').css('height',$(window).height()); // set body height to window height
//}); //window load
$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });

//$('document').ready(function(){
//	$('i.icon-question-sign').css({ opacity: 0.65 });
//});



$(document).ready(function(){


	$('.resize-img').each(function() {
 		$(this).css('width','auto','height','auto');
 		var maxWidth = 166; // Max width for the image
 		var maxHeight = 60; // Max height for the image
 		var ratio = 0; // Used for aspect ratio
 		var width = $(this).width();
 		var height = $(this).height();
 		// Check if the current width is larger than the max
	 		if(width > maxWidth){
		 		ratio = maxWidth / width; // get ratio for scaling image
		 		$(this).css("width", maxWidth); // Set new width
		 		$(this).css("height", height * ratio); // Scale height based on ratio
		 		height = height * ratio; // Reset height to match scaled image
		 		width = width * ratio; // Reset width to match scaled image
	 		}
	 		// Check if current height is larger than max
	 		if(height > maxHeight){
		 		ratio = maxHeight / height; // get ratio for scaling image
		 		$(this).css("height", maxHeight); // Set new height
		 		$(this).css("width", width * ratio); // Scale width based on ratio
		 		width = width * ratio; // Reset width to match scaled image
	 		}
	 		$(this).css('display','block');
	    });
// 	$('.carrierlogo').each(function() {
// 		$(this).css('width','auto','height','auto');
// 		var maxWidth = 163; // Max width for the image
// 		var maxHeight = 36; // Max height for the image
// 		var ratio = 0; // Used for aspect ratio
// 		var width = $(this).width();
// 		var height = $(this).height();
// 		// Check if the current width is larger than the max
//	 		if(width > maxWidth){
//		 		ratio = maxWidth / width; // get ratio for scaling image
//		 		$(this).css("width", maxWidth); // Set new width
//		 		$(this).css("height", height * ratio); // Scale height based on ratio
//		 		height = height * ratio; // Reset height to match scaled image
//		 		width = width * ratio; // Reset width to match scaled image
//	 		}
//	 		// Check if current height is larger than max
//	 		if(height > maxHeight){
//		 		ratio = maxHeight / height; // get ratio for scaling image
//		 		$(this).css("height", maxHeight); // Set new height
//		 		$(this).css("width", width * ratio); // Scale width based on ratio
//		 		width = width * ratio; // Reset width to match scaled image
//	 		}
//	 		$(this).css('display','block');
//	    });


	    /* Replace graydrkaction */
	    $('#rightpanel div').each(function(){
	    	if($(this).hasClass('graydrkaction margin0')){
	    $(this).removeClass('graydrkaction margin0').addClass('header');
	    	}

	    });

	    //Modal resize we need to re-write it. Triggering for more than 600
	  //  var viewportHeight = $(window).height();
	   // if (viewportHeight < 600) {
      	//  $('.container').append('<style>.modal { top: 5%; height: 450px;} .modal.fade.in {top: 5%;} iframe#search {min-height: 400px} .modal-body {height: 300px;} .popup-address {margin-top:0% } div#addressIFrame.modal.popup-address.in  {height: auto;} div#addressIFrame.modal.popup-address.in .modal-body {height: auto}</style>');

	  //  }


	    //Modal resize
	    //Modal resize
	 	var viewportHeight = $(window).height();
	 	//console.log(viewportHeight,'ghixcustom')
	 	if (viewportHeight < 650) {
	 		$('.container').append('<style>.modal { top: 5%; height: auto;} .modal.fade.in {top: 5%;} iframe#search {min-height: 585px} .modal-body {height: auto;} .popup-address {margin-top:0% } div#addressIFrame.modal.popup-address.in  {height: auto;} div#addressIFrame.modal.popup-address.in .modal-body {height: auto}</style>');
	 	 }else{
	 		$('.container').append('<style>.modal { top: 5%; height: auto;} iframe#search {min-height: 540px} .modal-body {height: auto;}</style> ');
	 	 }




	 	//Auto Height Modal
	 	//var mymodal = $('#fileUoload').remove


		/*Fix for HIX-23993*/
		/*This will hide the image if the url for the image fails to load*/
		var imgs = $('.resize-img');
		for(var i=0,j=imgs.length;i<j;i++){
				imgs[i].onerror = function(e){
				this.parentNode.removeChild(this);
			}
		}



		$('a[href^="http"]:not(.mast-logo), a[href^="https"]:not(.mast-logo)').each(function(){
            $(this).live('click',function(e){
            	var ind66Url = $(this).attr('name');
            	if(ind66Url=='IND66_URL_CA'){
            		return true;
            	}else if(ind66Url == 'PREVENT_DIRECT_HIT' ){
            		e.preventDefault();
            		return true;
            	}
            	var that = $(this),
            		getURL = that.attr('href'),
            		getExchangeName = $('#exchangeName').val(),
            		htmlContent = '<h4>'+find_help['pd.label.commontext.externalWarnBox.leavingAlert1']+' '+getExchangeName+' '+find_help['pd.label.commontext.externalWarnBox.leavingAlert2']+'</h4> <p>'+find_help['pd.label.commontext.externalWarnBox.access']+'</p>',
            		htmlURL = '<a href="'+getURL+'" target="_blank">'+getURL+'</a>',
            		htmlContent1 = '<p>'+getExchangeName+' '+find_help['pd.label.commontext.externalWarnBox1']+'</p>'+
            		'<div><a href="#" class="btn externalModalClose">'+find_help['pd.label.commontext.externalWarnBox.no']+'<span class="aria-hidden">'+find_help['pd.label.commontext.externalWarnBox.closeModal']+'</span></a> <a href="'+getURL+'" class="btn btn-primary" target="_blank" id="yesBtn" aria-hidden="false">'+find_help['pd.label.commontext.externalWarnBox.yes']+'</a></div>'+
            		'<p>'+find_help['pd.label.commontext.externalWarnBox.thankYou']+'</p>';
            	//that.attr('href','#'); //commented for HIX-107269
            	e.preventDefault();
            	$('#warningBox').remove();

            	var warningHTML = $('<div class="modal" id="warningBox" tabindex="0"><div class="modal-header" style="border-bottom:0; "><h3 style="float:left"></h3><button type="button" class="close externalModalClose" aria-hidden="false"><span aria-hidden="true">&times;</span><span class="aria-hidden">close modal</span></button></div><div class="modal-body gutter10" style="max-height:470px;"><div class="uploadText"></div></div><div class="modal-footer"></div></div>').modal({backdrop:"static",keyboard:"false"});
            	$(warningHTML).find('h3').html(htmlHeader);
            	$(warningHTML).find('div.uploadText').html(htmlContent + htmlURL + htmlContent1);

            	$('.externalModalClose').on('click',function(){
            		that.attr('href',getURL);
            		$('#warningBox').modal("hide");
            		if($('#einModal').length) {
            			console.log("enter key pressed to go back to ein modal");
            			$('#einModal').focus();
            			console.log(document.activeElement);
            		}

            	});

            	$('#yesBtn').live('click',function(){
            		that.attr('href',getURL);
            		$('#warningBox').modal("hide");
            	});
            });
		});

		 $('.accordion-toggle').click(function() {
	            $(this).find('i').addClass('icon-chevron-down');
	          });
	   	$('.accordion-group').on('hidden', function () {
	            $(this).find('.accordion-toggle').find('i').removeClass('icon-chevron-down').addClass('icon-chevron-right');
	          });


	});

function GetParameterByName(url,name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};
function executeSort(formID,url,dosubmit)
{
	var sortBy=GetParameterByName(url,"sortBy");
	var sortOrder=GetParameterByName(url,"sortOrder");
	var $frm=$("#"+formID);
	if($frm.find("input#sortBy").length==0)
	{
		$frm.append("<input type=\"hidden\" id=\"sortBy\" name=\"sortBy\" />");
	};
	if($frm.find("input#sortOrder").length==0)
	{
		$frm.append("<input type=\"hidden\" id=\"sortOrder\" name=\"sortOrder\" />");
	};
	$frm.find("#sortBy").val(sortBy);
	$frm.find("#sortOrder").val(sortOrder);

	if(dosubmit==undefined)
	{
		$frm.submit();
	}
};
