 <%@ include file="../include.jsp" %>
 <div id="providers" style="display:none">
<div style="margin:2%">

   
   <div class="border-grey"></div>
   <div id="selectedProvidersCount" class="font-size16">Selected Providers (0)</div>
   
   <div class="selected-provid">
     <ul id="selectedProviders">
      <!-- content is added dynamically here by plans.js's addProvider(img) function -->
     </ul>
   </div>
   
  <div class="clear"></div>
   <div class="border-grey"></div>
   <div id="providerSearchResultCount" class="font-size16">Preferred Health Care Provider search results (0)</div>
   
    <div class="list-arrow">
    <ul>
      <li style="margin-right:10px">1 to 10 of</li>
      <li><img src="img/lest_arrow_left.jpg"><img src="img/lest_arrow_right.jpg"></li>
     </ul> 
    </div>
    
   <div class="Pref-resTb">
       <div class="Pref-resTit">
       <ul>
          <li class="Tbname">Name</li>  
          <li class="Tbaddress">Address</li>    
          <li class="TbCods">Zip Code</li>  
          <li class="TbCods">Phone</li>  
          <li class="Tbaction">Actions</li>
        </ul>
       </div>
        <table width="100%" border="0" class="Pref-resTit" cellpadding="0" cellspacing="0" id="providersTable">
      <tbody>
       <tr><td class="Trname" style="width:823px; text-align:center">Please enter the search criteria below</td></tr>
      </tbody>    
       </table>
     </div>
  
  
   <div class="clear"></div>
   <div class="border-grey"></div>
   
   
   <div class="font-size16">Find a Health Care Provider</div>
   <p>Find your health care provider by entering the information below and clicking the Search button.</p>
  
   <div class="Find-search">
       <div class="Find-searBox">
         <h1>Provider`s First Name</h1>
          <input name="providerFirstName" id="providerFirstName" type="text" onclick="clearValidation('pfn')">
          
       </div>
       
       <div class="Find-searBox-or">
          <p class="or">OR</p>
       </div>
       
       <div class="Find-searBox">
         <h1>Hospital or Facility Name</h1>
          <input name="hospitalName" id="hospitalName" type="text" onclick="clearValidation('hfn')">
          
       </div>
       
       <div class="clear"></div>
       
       <div class="Find-searBox">
         <h1>Provider`s Last Name (Leave empty when searching by Hospital or Facility name)</h1>
          <input name="providerLastName" id="providerLastName" type="text" onclick="clearValidation('pln')">
          
       </div>
       
       <div class="clear"></div>
       
       <div class="Find-searBox">
         <h1>Zip Code <font style="color:red"> * </font></h1>
          <input name="zipCode" id="zipCode" type="text" style="width:150px" onclick="clearValidation('zip')">
          
       </div>
       
       <div class="clear"></div>
       
       <div class="Find-searBox">
         <h1>Distance <font style="color:red"> * </font></h1>
          <select name="distance" id="distance" onclick="clearValidation('dis')">
            <option value="">-- Select --</option>
            <option value="1">-- 1 Mile --</option>
            <option value="5">-- 5 Miles --</option>
            <option value="10">-- 10 Miles --</option>
            <option value="15">-- 15 Miles --</option>
            <option value="20">-- 20 Miles --</option>
            <option value="50">-- 50 Miles --</option>
            <option value="100">-- 100 Miles --</option>
          </select>
       </div>
       
  <div class="clear"></div>
       
       <div class="search-btn" onclick="providerSearch()">Search</div>
       
   </div>
   
   

</div>

</div>

