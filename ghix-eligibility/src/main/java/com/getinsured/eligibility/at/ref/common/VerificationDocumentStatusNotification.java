package com.getinsured.eligibility.at.ref.common;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * 
 * @author raguram_p
 *
 */
@Component
@Scope("prototype")
public class VerificationDocumentStatusNotification extends NotificationAgent{
	
	private Map<String, String> singleData;
	
	private ConsumerDocument consumerDocument;
	
	private SsapApplicant SsapApplicant;
	
	private Location location;
	
	private String referralId;

	public String getReferralId() {
		return referralId;
	}

	public void setReferralId(String referralId) {
		this.referralId = referralId;
	}

	@Override
	public Map<String, String> getSingleData() {
		
		String exchangeName=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		String exchangeUrl = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		String phoneNum=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
		Map<String,String> bean = new HashMap<String, String>();
		String toUploadUrl=GhixPlatformEndPoints.GHIXWEB_SERVICE_URL+"referral/document/upload/"+referralId;
		bean.put("exchangePhone", phoneNum);
		bean.put("exchangeName", exchangeName );
		bean.put("consumerDocumentcomments", consumerDocument.getComments());
		bean.put("consumerDocumentName", consumerDocument.getDocumentType());
		bean.put("exchangeUrl", exchangeUrl);
		bean.put("primarySSAPName", SsapApplicant.getFirstName()+" "+SsapApplicant.getLastName());
		bean.put("addressLine1", location.getAddress1()!=null?location.getAddress1():"");
		bean.put("addressLine2", location.getAddress2()!=null?location.getAddress2():"");
		bean.put("cityName", location.getCity()!=null?location.getCity():"");
		bean.put("stateCode", location.getState()!=null?location.getState():"");
		bean.put("pinCode", location.getZip()!=null?location.getZip():"");
		String value= consumerDocument.getAccepted().equalsIgnoreCase("Y")?"Accepted":"Rejected";
		if(consumerDocument.getAccepted().equalsIgnoreCase("N"))
		{
			bean.put("rejectedFlg", "Yes");
		}
		else
		{
			bean.put("approvedFlg", "Yes");
		}
		bean.put("toUploadURL", toUploadUrl);
		bean.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		bean.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		bean.put("exchangeAddress1", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		bean.put("content", value);
		bean.put("name", SsapApplicant.getFirstName()+" "+SsapApplicant.getLastName());
		setTokens(bean);
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", SsapApplicant.getEmailAddress());
		data.put("Subject", "Uploaded document to " + exchangeName + " Exchange" +" is verified"); 
		
		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public ConsumerDocument getConsumerDocument() {
		return consumerDocument;
	}

	public void setConsumerDocument(ConsumerDocument consumerDocument) {
		this.consumerDocument = consumerDocument;
	}

	public SsapApplicant getSsapApplicant() {
		return SsapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		SsapApplicant = ssapApplicant;
	}

	public void setSingleData(Map<String, String> singleData) {
		this.singleData = singleData;
	}
	
	

}
