<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.GhixPlatformConstants"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page
	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.model.UserRole"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page session="true"%>
<%@page import="java.util.Set"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate var="now" value="${date}" pattern="yyyy" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
@charset "UTF-8";

[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide
	{
	display: none !important;
}

ng\:form {
	display: block;
}

.ng-animate-block-transitions {
	transition: 0s all !important;
	-webkit-transition: 0s all !important;
}
</style>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<meta http-equiv="Cache-Control" content="max-age=0">
<meta http-equiv="Cache-Control"
	content="no-cache,no-store,must-revalidate">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Expires" content="Mon, 22 Jul 2002 11:12:01 GMT">
<meta http-equiv="Pragma" content="no-cache">

<title>Covered California</title>


<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon"
	href="<c:url value="/resources/images/favicon.ico" />">


<link href="<c:url value="/resources/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/formvalidation.css" />"
	rel="stylesheet" type="text/css" media="screen,print">

<link rel="stylesheet" type="text/css" media="screen"
	href="<c:url value="/resources/css/browser-detection.css" />">

<link
	href="<c:url value="/resources/css/jquery-ui-1.8.16.custom.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/jquery.ui.datepicker.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<!--  <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' media="screen,print" />     -->
<link href="<c:url value="/resources/css/css" />" rel="stylesheet"
	type="text/css" media="screen,print">
<!--  <link href='//fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css' media="screen,print"> -->

<link href="<c:url value="/resources/css/bootstrap_CA.css" />"
	media="screen,print" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/css/ghixcustom.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/quickfixes_ca.css" />"
	media="screen,print" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/css/jquery.multiselect.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<link href='http://fonts.googleapis.com/css?family=Montserrat'
	rel='stylesheet' type='text/css'>
<link href="<c:url value="/resources/css/datepicker.css" />"
	rel="stylesheet" type="text/css" media="screen,print">

<script async="" src="<c:url value="/resources/js/prum.min.js" />"></script>
<script async="" src="<c:url value="/resources/js/analytics.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.8.2.min.js" />"></script>
<style type="text/css"></style>
<script type="text/javascript"
	src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/angular-route.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/angular-resource.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.multiselect.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/ghixcustom.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-touch-punch-0.2.2.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/accessibility.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/sessioninactivity.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/ghixcustom_ca.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap-datepicker.js" />"></script>
<%String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;%>
<c:if test="${not empty fn:trim(trackingCode)}">
	<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m);
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', '${trackingCode}', '<%=GhixConfiguration.EXCHANGE_URL%>');
	ga('send', 'pageview');
</script>
</c:if>
</head>
<body id="body-default">
	<div class="topbar">
		<script type="text/javascript">$(document).ready(function(){$('a.lang').click(function(){var val=$(this).attr('id');var currUrl=window.location.href;var newUrl="";if(currUrl.indexOf("?",0)>0)
{if(currUrl.indexOf("?lang=",0)>0)
newUrl="?lang="+val;else if(currUrl.indexOf("&lang=",0)>0)
newUrl=currUrl.substring(0,currUrl.length-2)+val;else
newUrl=currUrl+"&lang="+val;}
else
newUrl=currUrl+"?lang="+val;window.location=newUrl;});});</script>
		<div class="navbar navbar-fixed-top navbar-inverse" id="masthead"
			role="banner">
			<input type="hidden" id="exchangeName" value="Covered California">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<ul class="nav">
						<li><a href="/hix/#" title="Link open in new window or tab"
							onclick="openInNewTab(&#39;/&#39;)"><img class="brand"
								src="<c:url value="/resources/img/logo_ca.png" />"
								alt="Covered California" pagespeed_url_hash="765750227"></a></li>
					</ul>
					<div class="nav-collapse collapse pull-right">
						<ul class="nav pull-right clearfix" id="second-menu-nm">
							<!-- Loged In Ends -->
							<!-- Logged Out -->
							<li><a href="/hix/#" name="skip" class="skip-nav"
								accesskey="n">Skip Navigation</a></li>
							<!-- all -->
							<li><a href="/hix/account/user/login">Log In</a></li>
							<!-- all -->
							<li class="dropdown"><a class="dropdown-toggle"
								data-toggle="dropdown" href="/hix/#">Get Assistance <b
									class="caret"></b><span aria-hidden="true" class="hide">
										Dropdown Menu. Press enter to open it and tab through its
										options</span></a>
								<ul class="dropdown-menu">
									<!-- <iframe class="hidden-iframe" src="about:blank"></iframe> -->
									<li><a href="/hix/#" id="pop_findAgent">Find Local
											Assistance</a></li>
									<!-- HIX -19718 -->
									<li><a href="/hix/faq">Help Center</a></li>
									<!-- all -->
									<li><a href=/hix/# " style="cursor: default">Consumers
											<i class="icon-phone"></i> 1-800-300-1506 <span
											class="aria-hidden"> End of dropdown</span>
									</a></li>

								</ul></li>
							<!-- Logged Out Ends -->
							<!-- Menu for Employee, Employer, Individual, Admin, Broker, Carrier portals ENDS -->
						</ul>
					</div>
					<!-- nav-collapse ends -->
					<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 540px
}

.modal-body {
	height: auto;
}
</style>
					<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 595px
}

.modal-body {
	height: auto;
}
</style>
				</div>
				<!-- container ends -->
			</div>
			<!-- navbar-inner ends -->
		</div>
		<script type="text/javascript">var registrationUrls=new Array("account/securityquestions","/account/phoneverification/sendcode","/user/activation","account/signup");var currDocUrl=document.URL;var urlIdx=0;var noOfRegUrls=registrationUrls.length;while(urlIdx<noOfRegUrls){if(currDocUrl.indexOf(registrationUrls[urlIdx])>0){$('#masthead-inbox').hide();$('#masthead-account-settings').hide();$('#masthead-dashboard').hide();}
urlIdx++;}</script>
		<script>$('a.dropdown-toggle, .dropdown-menu a').on('touchstart',function(e){e.stopPropagation();});</script>
		<!-- navbar  ends -->
	</div>
	<div id="topnav" class="topbar"></div>
	<div id="container-wrap" class="row-fluid">
		<div class="container">
			<div class="row-fluid" role="main">
				<div class="errorpage">
					<c:if test="${pageContext.errorData.statusCode == 404}">
						<div class="errorpage__icon errorpage__icon--stack">
							<span class="icon-stack"> <i
								class="icon-search icon-stack-base"></i> <i href=""
								class="icon-question"></i>
							</span>
						</div>
						<h1 class="errorpage__mainheader--404">404: Page not found</h1>
						<p class="errorpage__copy1">Hmm... We can't find that page
							right now.</p>
					</c:if>
					<c:if test="${pageContext.errorData.statusCode == 403}">
						<i class="errorpage__icon icon-ban-circle"></i>
						<h1 class="errorpage__mainheader">403: Access denied</h1>
						<p class="errorpage__copy1">We're sorry, you do not have
							permission to access this page.</p>
					</c:if>
					<c:if test="${pageContext.errorData.statusCode == 500}">
						<i class="errorpage__icon icon-warning-sign"></i>
						<h1 class="errorpage__mainheader">500: Internal Server Error</h1>
						<p class="errorpage__copy1">The page cannot be displayed.</p>
						<p class="errorpage__copy2">Most likely causes:</p>
						<ul class="errorpage__cause">
							<li class="errorpage__cause__item">The website is under
								maintanence.</li>
							<li class="errorpage__cause__item">The website has a program
								error.</li>
						</ul>
					</c:if>
					<div class="errorpage__btn">
						<a href="" class="errorpage__btn__back btn btn-primary"
							role="button" onclick="history.back()">Go Back</a> <a href="/"
							class="errorpage__btn__home btn btn-primary" role="button">Homepage</a>
					</div>
					<p class="errorpage__contact">
						If this happens again, please try later, or contact us at
						<%=DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE)%>
					</p>
				</div>
			</div>
			<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 540px
}

.modal-body {
	height: auto;
}
</style>
			<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 595px
}

.modal-body {
	height: auto;
}
</style>
		</div>
		<div class="push"></div>
		<!-- sticky footer -->
	</div>
	<%!String currStateExchangeType = null;%>
	<%
		currStateExchangeType = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
	%>
	<div id="footer">
		<footer class="container" role="footer">
			<div class="row-fluid margin30-t">

				<div class="pull-left">
					<ul class="nav nav-pills">
						<li id="copyrights">&copy;${now} <%=DynamicPropertiesUtil
					.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></li>
						<li><a href="#" title="Link open in new window or tab"
							onclick="openInNewTab('<%=DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL)%>')"
							class="margin20-l">Privacy Policy</a></li>
					</ul>
				</div>

				<div class="pull-right">
					<c:set var="assisterRoleName">
						<encryptor:enc value="assisterenrollmententity" isurl="true" />
					</c:set>
					<sec:authorize access="!isAuthenticated()">
						<%-- <ul class="nav nav-pills">
	                    		<% if("Individual".equalsIgnoreCase(currStateExchangeType) || "Both".equalsIgnoreCase(currStateExchangeType)) { %>
 	                    			<li><a href="<gi:cdnurl value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a></li> 
 	                    		<% } %>
	 	                        <li><a href="/hix/shop/employer/paperapplication">Submitting a paper application</a></li>
	                    		<li><a href="/hix/account/user/login">Administrators</a></li>
	                    		<li><a href="/hix/account/user/login">Issuers</a></li>
	                    	</ul>  --%>
					</sec:authorize>
					<%
						if (GhixConstants.GHIX_ENVIRONMENT.equals("PROD")) {
					%>
					<ul class="nav nav-list pull-right" id="build">

						<li style="white-space: nowrap;"><a href="#">Build
								Number: <%=GhixPlatformConstants.BUILD_NUMBER%>
						</a></li>
						<li style="white-space: nowrap;"><a href="#">Build Date:
								<%=GhixPlatformConstants.BUILD_DATE%></a></li>

					</ul>
					<%
						}
					%>

					<%
						if (GhixConstants.GHIX_ENVIRONMENT.equals("DEV")) {
					%>
					<ul class="nav nav-list pull-right hide" id="buildshow">
						<li style="white-space: nowrap;"><a href="#">Branch Name:
								<%=GhixPlatformConstants.BUILD_BRANCH_NAME%></a></li>
						<li style="white-space: nowrap;"><a href="#">Build
								Number: <%=GhixPlatformConstants.BUILD_NUMBER%></a></li>
						<li style="white-space: nowrap;"><a href="#">Build Date:
								<%=GhixPlatformConstants.BUILD_DATE%></a></li>

					</ul>
					<%
						}
					%>

					<ul class="nav nav-list span6 ">
						<li><a id="callSupport" href="#"><span
								class="aria-hidden">Call Support</span></a></li>
						<%--  <spring:message code="label.call"/>  --%>
						<%--GhixConfiguration.BRANDNAME> Health Exchange Helpline 1-800-123-1234</a></li>--%
	                      <%--   <li><a id="" href="#"><spring:message code="label.chat"/></a></li> --%>
					</ul>
				</div>
				<!--  
                    <div class="span1" id="social">
                   
                       <a href="#" id="twitter">Twitter</a>
                        <a href="#" id="fb">Facebook</a>
                        
                    </div>
               -->
			</div>

			<!--      		<div class="row-fluid" style="border-top:solid 1px #ccc;margin-top:20px;"> -->
			<%--  				<p class="span6" id="copyrights">&copy;<small> <%=GhixConfiguration.BRANDNAME%> <spring:message code="label.footerbrand"/> ${now}</small></p> --%>
			<!--      			<ul class="nav nav-list pull-right" id="build"> -->
			<%--      				<li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %></a></li> --%>
			<%--                     <li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li> --%>
			<!--         		</ul> -->
			<!--         	</div> -->

		</footer>
		<!-- footer -->
		<!--      		<div class="row-fluid" style="border-top:solid 1px #ccc;margin-top:20px;"> -->
		<!--      			<ul class="nav nav-list pull-right" id="build"> -->
		<!--         		</ul> -->
		<!--         	</div> -->
		<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 540px
}

.modal-body {
	height: auto;
}
</style>
		<style>
.modal {
	top: 5%;
	height: auto;
}

iframe#search {
	min-height: 595px
}

.modal-body {
	height: auto;
}
</style>
		</footer>
		<!-- footer -->
	</div>
	<div id="pingData"></div>
	<%
  		String userActiveRoleName=(String)request.getSession().getAttribute("userActiveRoleName");
        String switchToModuleNameLocal=(String)request.getSession().getAttribute("switchToModuleName");
  %>
	<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
	<c:set var="switchToModuleNameLocal"
		value="<%=switchToModuleNameLocal%>" />
	<input type="hidden" id="encSwitchToModuleNameLocal"
		name="encSwitchToModuleNameLocal"
		value="<encryptor:enc value="${switchToModuleNameLocal}"/>" />

	<script type="text/javascript">
   $(window).load(function(){
	 var userActiveRoleName = '${userActiveRoleName}';
	 var switchToModuleNameLocal = $('#encSwitchToModuleNameLocal').val();
	 
	 $.ajax({ url: "/hix/ping",
		 	  type: "get",
		 	 data: {
		 		  	localmodname:switchToModuleNameLocal
	 		  },
		 	  cache: false,
		 	  success: function(response){
	        	  	if(null != response) {
	        	  		if(1 == response) {
	        				$("#pingData").html = response;
				  			window.location.href = "<%=GhixConstants.APP_URL%>account/user/login";
	        	  		} else if(2 == response) {
                            $("#pingData").html = response;
                            window.location.href = "<%=GhixConstants.APP_URL%>account/user/switchNonDefRole/${userActiveRoleName}";
                        }
				  	}
	          },
	 		  error : function(e) {  
      			//alert('Error: ' + e);   
     	  }
	 });
  });

 function openInNewTab(url) {
		var win = window.open(url, '_blank');
		win.focus();
}
 function openInSameTab(url) {
		window.location.href= url;
} 
		$(function() {
			$("#getAssistance").click(function(e) {
				e.preventDefault();
				var href = $(this).attr('href');
				if (href != undefined && href.indexOf('#') != 0 && href.indexOf('/broker/search') != 0) {
					$(
							'<div id="searchBox" class="modal bigModal modalsize-l" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>')
							.modal();
				}
			});
			$("#pop_findAgent, #pop_findLocalAssistance, #pop_getAssistance").click(function(e) {
				e.preventDefault();
				var href = '<c:out value="${completeURL}"/>';
				//console.log("hre"+href);
				openFindBrokerDialog(href);
			});
		});
		
		$(function() {
			$(".financialHelpCheck").click(function(e) {
			e.preventDefault();
				$(
				'<div id="financialHelp" class="modal hide fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="confirmApplyModal" aria-hidden="true">'
					+ '<div class="modal-header">'
						+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
						+ '<h3 id="confirmApplyModal">Apply for health insurance coverage</h3>'
					+ '</div>'
					+ '<div class="modal-body">'
					+ '<p>You may qualify for cost-savings on your health insurance coverage. If you are interested, click &#34;Apply for Cost-Savings&#34; to determine your eligibility for cost-savings programs on Covered California. If you are not interested in cost-savings you can apply for coverage without cost-savings on Covered California.</p>'
					+ '</div>'
					+ '<div class="modal-footer">'
						+ '<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>'
						+ '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="window.open(\'<%=DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.FINANCIAL_SSAP_URL)%>\');$(\'.modal-backdrop\').hide();">Apply for Cost-Savings</button>'
						+ '<a class="btn btn-primary" href="<c:url value=".'
						+ $(this).attr('redirectOnNoUrl')
						+ '" />">Apply without Cost-Savings</a>'
					+ '</div>'
				+ '</div>').modal();
			});
		});

		function openFindBrokerDialog(href)
		{
			$('<div id="brokersearchBox" class="modal bigModal modalsize-l"><div class="searchModal-header"><button type="button" class="agentClose close"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>').modal({show: true});
		}
		
		function closeSearchLightBox() {
			$("#searchBox").remove();
			$("#brokersearchBox, .modal-backdrop").remove();
		}

		function closeSearchLightBoxOnCancel() {
			$("#searchBox").remove();
		}
		
		$('.agentClose').live('click',function(){
			$('#brokersearchBox, .modal-backdrop').remove();
			$('body').removeClass('modal-open');
			//location.reload();			
		});
		
		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				//$('#brokersearchBox, .modal-backdrop').remove(); // commented modal-backdrop getting removed on ESC key - HIX-48280			
			}  
		});
		
		$('#closeAgentSerchBox').click(function(event){
			event.preventDefault();
			$( ".agentClose" ).trigger( "click" );
		});
	</script>

	<script type="text/javascript"
		src="<gi:cdnurl value="/resources/js/browser-detection.js" />"></script>
	<script>
  var notSupportedBrowsers = [
	  {'os': 'Any', 'browser': 'Chrome', 'version': 34},
	  {'os': 'Any', 'browser': 'MSIE', 'version': 10},
	  {'os': 'Any', 'browser': 'Safari', 'version': 5},
	  {'os': 'Any', 'browser': 'Firefox', 'version': 28}
	];
  </script>
	<!-- Code/EasyXDM Script added by HP -->
	<!--  <script type="text/javascript"> 
	var socket = new easyXDM.Socket({
		onReady:  function() {
		// this is needed for setting height on initial load
		socket.postMessage(document.body.scrollHeight)
	},
	onMessage: function(message, origin){
		// this is called from consumer (maybe by a timer)
		socket.postMessage(document.body.scrollHeight)
	}});
</script> -->
	<sec:authorize access="isAuthenticated()" var="isUserAuthenticated">
		<input type="hidden" id="reload" value="false" />
	</sec:authorize>

	<sec:authorize access="!isAuthenticated()" var="isUserAuthenticated">
		<input type="hidden" id="reload" value="true" />
	</sec:authorize>

	<input type="hidden"
		value="<%=DynamicPropertiesUtil
					.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.CLIENT_INACTIVITY_THRESHOLD)%>"
		id="clientInactivityThreshold" name="clientInactivityThreshold">

	<div id="activityModal" class="modal hide fade" tabindex="-1">
		<div class="modal-body">
			<p>Your Session is about to expire. Select "OK" to keep your
				session.</p>
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" onclick="sessionRenew();">OK</button>
		</div>
	</div>
</body>
</html>