package com.getinsured.eligibility.at.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.eligibility.at.mes.service.BatchApplicationProcessingService;

@RestController
@Validated
@RequestMapping("/applicationProcess")
public class QueuedApplicationProcessingController {
	
@Autowired BatchApplicationProcessingService batchApplicationProcessingService;
	
	@RequestMapping(value = "/callBatchProcessing", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> callBatchProcessing() {
		batchApplicationProcessingService.executeQueuedApplication();
		return new ResponseEntity<>("Ok", HttpStatus.OK);
	}
}
