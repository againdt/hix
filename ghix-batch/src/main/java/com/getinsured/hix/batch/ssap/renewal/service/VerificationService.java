package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.springframework.batch.core.UnexpectedJobExecutionException;

/**
 * Interface is used to provide services for Verification.
 * 
 * @since August 12, 2019
 */
public interface VerificationService {

	String JOB_NAME = "ssapRedeterminationVerificationJob";
	String ERROR_CODE = "RENEWALBATCH_50013";

	enum APPLICATION_STATUS {
		EN, SG, UC;
	}

	void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException;

	Integer logToGIMonitor(Exception e, String caseNumber);

	List<Long> getSsapApplicationsByCoverageYearAndStatus(long coverageYear, List<String> applicationStatusList, Long batchSize);

	String verificationSsap(Long applicationId);
}
