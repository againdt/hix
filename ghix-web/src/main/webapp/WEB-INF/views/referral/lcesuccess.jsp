<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
        String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
%>
<c:set var="stateCode" value="<%=stateCode%>" />
<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1 data-original-title="">Financial Application - Life Change Reporting</h1>
	</div>
	<div class="row-fluid">
	<div id="sidebar" class="span3">
				<div class="header">
					<h4>
						Help
					</h4>
				</div>
					<ul class="nav nav-list">
	      		<li>
                    <c:choose>
                        <c:when test="${stateCode == 'CA'}">
                            <a href="<c:url value="https://www.coveredca.com/find-help/contact/"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
                        </c:when>
                        <c:when test="${stateCode == 'ID'}">
                            <a href="<c:url value="https://www.yourhealthidaho.org/contact-us/"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
						</c:when>
						<c:when test="${stateCode == 'MN'}">
                            <i class="icon-phone"></i><spring:message code="label.sep.contact.mn"/>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="#"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
                        </c:otherwise>
                    </c:choose>
                </li>
	      	</ul>
			</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4>You qualify for a Special Enrollment Period</h4>
			</div>
			<div class="gutter20">
				<div class="alert alert-info margin30-b">
					<p>A Special Enrollment Period has been opened for you, which allows you to update your plan selection. Please note that this Special Enrollment Period is for a limited time. If you want to change your plan, you need to enroll in a new plan before 
						<b>${endDate}</b></p>
					<p><b>A few things to consider: </b></p>
					<ul>
					<li>If you are already enrolled in a health plan and are eligible for an SEP, you can change to a different plan, but you don't have to. You can choose to stay in your current plan.</li>
					<li>If you decide to change plans, you will start over with a new deductible and out-of-pocket maximum. Your spending toward the deductible and out-of-pocket maximum from your old health plan will NOT carry over to the new plan.</li>
					<li>If you enroll in a health plan mid-year, you are subject to the full annual deductible and out-of-pocket maximum, just as if you were enrolled in the plan for a full 12 months. Deductibles and maximum out-of-pocket limits are not pro-rated based on the length of enrollment.</li>
					<li>Please note your coverage may not start right away. The date your coverage starts is based on the type of life event, date of the event and the date you enroll in new coverage. If you have a current plan, that will stay in effect until your new coverage begins.</li>
					</ul>
				</div>
				<form:form>	<df:csrfToken />
				<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
				</form:form>
			</div>
		</div>

	</div>
</div>
