
// Experimental syntax via spread operator and Array.map() to copy and update JSON

// this.props.actions.changeData({
//     ...this.props.data,
//     singleStreamlinedApplication: {
//         ...this.props.data.singleStreamlinedApplication,
//         taxHousehold: this.props.data.singleStreamlinedApplication.taxHousehold.map(
//             (household, i) => i !== 0 ?
//                 household :
//                 {
//                     ...household,
//                     householdMember: household.householdMember.map(
//                         (member, i) => i !== 0 ?
//                             member :
//                             {
//                                 ...newData,
//                             }
//                     )
//                 }
//         )
//     }
// });