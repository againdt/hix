package com.getinsured.hix.admin.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.Announcement.Status;

@Repository
public interface IAnnouncementRepository extends JpaRepository<Announcement, Integer> {
	public Announcement findByName(String name);
	
	public Announcement findByStatus(Status name);
	
	@Query("Select an.announcementText FROM Announcement as an "+
			" inner join an.announcementRole as ar"+
			" where an.status = :status "+
			" and an.effectiveStartDate <= :datecriteria "+
			" and an.effectiveEndDate >= :datecriteria "+
			" and ar.role.id= :roleid "+
			" order by an.id desc")
	public List<String> viewAnnouncement(@Param("status") Status status,@Param("roleid") int roleid, @Param("datecriteria") Date datecriteria);
	
	@Query("Select an FROM Announcement as an"+
			" where an.moduleName= :moduleName "+
			" and an.effectiveStartDate <= :currentDate "+
			" and an.effectiveEndDate >= :currentDate "+
			" order by an.effectiveEndDate asc")
	public List<Announcement> getActiveAnnoucements(@Param("moduleName") String moduleName,@Param("currentDate") java.sql.Date currentDate);
	
}