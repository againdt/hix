	/**
 	 * Function to trim beginning and trailing spaces
 	 * 
 	 * @param rawStr The raw String
 	 * @returns {String} The processes String. 
 	 */
	function trim(rawStr) {
 	        return rawStr.replace(/^\s+|\s+$/g,"");
 	}
 	
 	/* Function to update Social Security Number for persistence.*/
		function updateSocialSecurityNumber() {
			var socialSecurityNumber;
	 		var ssn1 = trim($("#ssn1").val());
	 		var ssn2 = trim($("#ssn2").val());
	 		var ssn3 = trim($("#ssn3").val());
	 		
	 		if(ssn1 == undefined && ssn2 == undefined && ssn3 == undefined) {
	 			return true;
	 		}
	 		
	 		if(null == ssn1 || ssn1 == undefined || ssn1 == "") {
	 			socialSecurityNumber = "-";
	 		}
	 		else {
	 			socialSecurityNumber = (ssn1 + "-");
	 		}

	 		if(null == ssn2 || ssn2 == undefined || ssn2 == "") {
	 			socialSecurityNumber += "-";
	 		}
	 		else {
	 			socialSecurityNumber += (ssn2 + "-");
	 		}
	
	 		if(null != ssn3 && ssn3 != undefined) {
	 			socialSecurityNumber += ssn3;
	 		}
	 		
	 		if(null != socialSecurityNumber && socialSecurityNumber != undefined  ) {
	 			$("#socialSecurityNumber").val(socialSecurityNumber);
	 			$("#socialSecurityNumber").trigger("change"); 
	 		}
		}
 	
 	$(document).ready(function(){	
 		
	 	/* Function to display social secureity number in individual boxes.*/
	 	$("#maritalStatusSSNForm").load(function() {
	 		var socialSecurityNumber = $("#socialSecurityNumber").val();
	 		
	 		if(null != socialSecurityNumber && socialSecurityNumber != undefined  ) {
	 			var matches = socialSecurityNumber.match(/(\d{3})*-(\d{2})*-(\d{4})*/);
	 			
	 			if(null != matches && matches != undefined && matches.length > 0) {
	 				for(var i = 1; i < matches.length; i++) {
	 	 				$("#ssn"+i).val(trim(matches[i]));
	 				} 				
	 			}
	 		}
	 	});
	 	
	 	if($("#maritalStatusSSNForm").length>0) {
	 		$("input[id^='ssn']").change(updateSocialSecurityNumber());
	 	}
	 	
 	});
 