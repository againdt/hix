package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for document description
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class DocDescValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocDescValidator.class);
	
	/** 
	 * Validations for document description
	 * Should be between 1-35 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String docDesc = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			docDesc = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(docDesc.length() < VLPServiceConstants.DOC_DESC_MIN_LEN
				|| docDesc.length() > VLPServiceConstants.DOC_DESC_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.DOC_DESC_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
