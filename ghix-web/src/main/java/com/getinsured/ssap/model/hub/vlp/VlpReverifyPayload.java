package com.getinsured.ssap.model.hub.vlp;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yevgen Golubenko
 * @since 7/19/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VlpReverifyPayload {
  @JsonProperty("CaseNumber")
  private String caseNumber;

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("ReceiptNumber")
  private String receiptNumber;

  @JsonProperty("I94Number")
  private String i94Number;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonProperty("PassportCountry")
  private String passportCountry;

  @JsonProperty("NaturalizationNumber")
  private String naturalizationNumber;

  @JsonProperty("CitizenshipNumber")
  private String citizenshipNumber;

  @JsonProperty("VisaNumber")
  private String visaNumber;

  @JsonProperty("FirstName")
  private String firstName;

  @JsonProperty("LastName")
  private String lastName;

  @JsonProperty("MiddleName")
  private String middleName;

  @JsonProperty("DateOfBirth")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date dateOfBirth;

  @JsonProperty("FiveYearBarApplicabilityIndicator")
  private boolean fiveYearBarApplicabilityIndicator;

  @JsonProperty("RequesterCommentsForHub")
  private String requesterCommentsForHub;

  public String getCaseNumber() {
    return caseNumber;
  }

  public void setCaseNumber(String caseNumber) {
    this.caseNumber = caseNumber;
  }

  public String getAlienNumber() {
    return alienNumber;
  }

  public void setAlienNumber(String alienNumber) {
    this.alienNumber = alienNumber;
  }

  public String getReceiptNumber() {
    return receiptNumber;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }

  public String getI94Number() {
    return i94Number;
  }

  public void setI94Number(String i94Number) {
    this.i94Number = i94Number;
  }

  public String getSevisId() {
    return sevisId;
  }

  public void setSevisId(String sevisId) {
    this.sevisId = sevisId;
  }

  public String getPassportCountry() {
    return passportCountry;
  }

  public void setPassportCountry(String passportCountry) {
    this.passportCountry = passportCountry;
  }

  public String getNaturalizationNumber() {
    return naturalizationNumber;
  }

  public void setNaturalizationNumber(String naturalizationNumber) {
    this.naturalizationNumber = naturalizationNumber;
  }

  public String getCitizenshipNumber() {
    return citizenshipNumber;
  }

  public void setCitizenshipNumber(String citizenshipNumber) {
    this.citizenshipNumber = citizenshipNumber;
  }

  public String getVisaNumber() {
    return visaNumber;
  }

  public void setVisaNumber(String visaNumber) {
    this.visaNumber = visaNumber;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public boolean isFiveYearBarApplicabilityIndicator() {
    return fiveYearBarApplicabilityIndicator;
  }

  public void setFiveYearBarApplicabilityIndicator(boolean fiveYearBarApplicabilityIndicator) {
    this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
  }

  public String getRequesterCommentsForHub() {
    return requesterCommentsForHub;
  }

  public void setRequesterCommentsForHub(String requesterCommentsForHub) {
    this.requesterCommentsForHub = requesterCommentsForHub;
  }

  @Override
  public String toString() {
    return "VlpReverifyPayload{" +
      "caseNumber='" + caseNumber + '\'' +
      ", alienNumber='" + alienNumber + '\'' +
      ", receiptNumber='" + receiptNumber + '\'' +
      ", i94Number='" + i94Number + '\'' +
      ", sevisId='" + sevisId + '\'' +
      ", passportCountry='" + passportCountry + '\'' +
      ", naturalizationNumber='" + naturalizationNumber + '\'' +
      ", citizenshipNumber='" + citizenshipNumber + '\'' +
      ", visaNumber='" + visaNumber + '\'' +
      ", firstName='" + firstName + '\'' +
      ", lastName='" + lastName + '\'' +
      ", middleName='" + middleName + '\'' +
      ", dateOfBirth=" + dateOfBirth +
      ", fiveYearBarApplicabilityIndicator=" + fiveYearBarApplicabilityIndicator +
      ", requesterCommentsForHub='" + requesterCommentsForHub + '\'' +
      '}';
  }
}
