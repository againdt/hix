package com.getinsured.iex.hub.ssac.wrapper;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.google.gson.Gson;

public class RequestQuartersOfCoverageVerificationIndicatorWrapper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestQuartersOfCoverageVerificationIndicatorWrapper.class);
	
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	private boolean quartersOfCoverageVerificationRequested;
	private Boolean requestQuartersOfCoverageVerificationIndicator;
	private static com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsdFactory;
	private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	
	public RequestQuartersOfCoverageVerificationIndicatorWrapper(Boolean requestQuartersOfCoverageVerification ){
		this.requestQuartersOfCoverageVerificationIndicator = requestQuartersOfCoverageVerification;
		this.quartersOfCoverageVerificationRequested = this.requestQuartersOfCoverageVerificationIndicator.isValue();
	}
	
	public RequestQuartersOfCoverageVerificationIndicatorWrapper() {
		xsdFactory = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
		extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public void setQuartersOfCoverageVerificationRequested(boolean isVerifyFlag) {
		if(this.requestQuartersOfCoverageVerificationIndicator == null){
			this.requestQuartersOfCoverageVerificationIndicator = xsdFactory.createBoolean();
		}
		this.requestQuartersOfCoverageVerificationIndicator.setValue(isVerifyFlag);
		this.quartersOfCoverageVerificationRequested = isVerifyFlag;
	}
	
	public boolean isQuartersOfCoverageVerificationRequested() {
		if(this.requestQuartersOfCoverageVerificationIndicator == null){
			return false;
		}
		return quartersOfCoverageVerificationRequested;
	}
	
	public static RequestQuartersOfCoverageVerificationIndicatorWrapper 
	
		fromJsonString(String jsonString) throws IOException  {
		
		return gson.fromJson(jsonString, RequestQuartersOfCoverageVerificationIndicatorWrapper.class);
		
	}
	
	public static void main(String[] args) {
		
		try{
			RequestQuartersOfCoverageVerificationIndicatorWrapper wrapper = fromJsonString("{\"quartersOfCoverageVerificationRequested\":\"false\"}");
			SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
			req.setRequestQuartersOfCoverageVerificationIndicator(wrapper.requestQuartersOfCoverageVerificationIndicator);
			//JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
			JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//m.marshal(ssaCompReq, System.out);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}

	public Boolean getSystemRepresentation() {
		return this.requestQuartersOfCoverageVerificationIndicator;
	}
}
