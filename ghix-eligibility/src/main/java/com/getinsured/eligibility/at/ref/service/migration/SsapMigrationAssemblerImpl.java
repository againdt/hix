package com.getinsured.eligibility.at.ref.service.migration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.at.ref.service.ReferralApplicationCompareService;
import com.getinsured.eligibility.at.ref.service.ReferralCompareService;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonEmploymentAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceDocumentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxDependentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.org.nmhix.ssa.person.StateCodeEnum;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.Broker;
import com.getinsured.iex.ssap.ChpInsurance;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.iex.ssap.CitizenshipImmigrationStatus;
import com.getinsured.iex.ssap.ContactPreferences;
import com.getinsured.iex.ssap.CurrentEmployer;
import com.getinsured.iex.ssap.CurrentEmployerInsurance;
import com.getinsured.iex.ssap.EligibleImmigrationDocumentType;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HealthCoverage;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.IncarcerationStatus;
import com.getinsured.iex.ssap.MailingAddress;
import com.getinsured.iex.ssap.MedicaidInsurance;
import com.getinsured.iex.ssap.OtherAddress;
import com.getinsured.iex.ssap.OtherImmigrationDocumentType;
import com.getinsured.iex.ssap.OtherPhone;
import com.getinsured.iex.ssap.Phone;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SpecialCircumstances;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.InsuranceEndReasonEnum;
import com.getinsured.iex.ssap.enums.PhoneTypeEnum;
import com.getinsured.iex.ssap.enums.RaceEnum;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.timeshift.TimeShifterUtil;

@Component("ssapMigrationAssemblerImpl")
public class SsapMigrationAssemblerImpl implements SsapAssembler {

	private static final Logger LOGGER = Logger.getLogger(SsapMigrationAssemblerImpl.class);

	private static final String PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE = "PTF";

	@Autowired
	@Qualifier("dozzerMapper")
	private Mapper dozzerMapper;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	@Qualifier("zipCodeService")
	private ZipCodeService zipCodeService;

	@Autowired
	private SsapUtil ssapUtil;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;


	private String SSAPAPPLICATION_GUID_KEY = "SSAP_APPLICATION_DISPLAY_ID";

	private String SSAPAPPLICANT_GUID_KEY = "SSAP_APPLICANT_DISPLAY_ID";

	private static final int WARD_AGE_LIMIT = 26;

	private String WARD_RELATIONSHIP_CODE = "15";

	private static List<String> relationShipNeedsToBeChanged = Arrays.asList("05", "07", "09", "10", "17", "19");

	private static final String HIPHEN = "-";
	
	@Autowired
	@Qualifier("referralCompareService")
	private ReferralCompareService referralCompareService;

	@Autowired
	@Qualifier("referralApplicationCompareService")
	private ReferralApplicationCompareService referralApplicationCompareService;

	
	
	@Override
	public SingleStreamlinedApplication assembleSsapXmlToJson(AccountTransferRequestDTO accountTransferRequestDTO) {
		final AccountTransferRequestPayloadType source = accountTransferRequestDTO.getAccountTransferRequestPayloadType();
		LOGGER.info("Assembling AccountTransferRequestPayloadType to SingleStreamlinedApplication object starts");

		long startTime = TimeShifterUtil.currentTimeMillis();

		final SingleStreamlinedApplication singleStreamlinedApplication = dozzerMapper.map(source, SingleStreamlinedApplication.class, "dozzerMappingSsapId");

		if (singleStreamlinedApplication == null) {
			LOGGER.error(ERR_NOT_MAPPED);
			throw new GIRuntimeException(ERR_NOT_MAPPED);
		}

		populateApplicationGuid(source, singleStreamlinedApplication);

		populateExternalApplicationId(source, singleStreamlinedApplication);

		populateAuthorizedRepresentativeIndicator(singleStreamlinedApplication);

		populateBrokerFederalTaxIdNumber(source, singleStreamlinedApplication);

		final HouseholdMember responsibleHouseholdMember = populateHouseHoldMembers(source, singleStreamlinedApplication);

		populateLivesAtOtherAddress(responsibleHouseholdMember, singleStreamlinedApplication);

		populateBloodRelationship(source, responsibleHouseholdMember, singleStreamlinedApplication);

		populateTaxHousehold(source, responsibleHouseholdMember, singleStreamlinedApplication);
		
		// populate EligibilityStartDate and EligibilityEndDate
		//populateEligibilityStartAndEndDates(source,singleStreamlinedApplication);

		if (accountTransferRequestDTO.isLCE()) {
			populateLceComparationDetails(responsibleHouseholdMember, singleStreamlinedApplication, accountTransferRequestDTO.getCompareToApplicationId());
		} else if (accountTransferRequestDTO.getCompareToApplicationId() != ReferralConstants.NONE) {
			populateComparationDetails(singleStreamlinedApplication, accountTransferRequestDTO.getCompareToApplicationId());
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("Assembling AccountTransferRequestPayloadType to SingleStreamlinedApplication object Ends");
		LOGGER.info("Assembling Process took " + ((endTime - startTime)) + " milliseconds ");
		return singleStreamlinedApplication;
	}

	private void populateLceComparationDetails(HouseholdMember responsibleHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication, long enrolledApplicationId) {
		referralCompareService.executeCompare(responsibleHouseholdMember, singleStreamlinedApplication, enrolledApplicationId);
	}

	private void populateComparationDetails(SingleStreamlinedApplication singleStreamlinedApplication, long compareApplicationId) {
		referralApplicationCompareService.executeCompare(singleStreamlinedApplication, compareApplicationId);
	}

	@SuppressWarnings("unchecked")
	private String extractPrimaryTaxFilerId(AccountTransferRequestPayloadType source) {
		String primaryTaxFilerId="";
		final int size = ReferralUtil.listSize(source.getTaxReturn());
		if (size != 0) {
			final TaxReturnType taxReturnType = source.getTaxReturn().get(0);
			if (taxReturnType.getTaxHousehold() == null) {
				return "";
			}
			final TaxHouseholdType taxHouseholdType = taxReturnType.getTaxHousehold();


			final List<?> taxFilerList = taxHouseholdType.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent();
			final int tfSize = ReferralUtil.listSize(taxFilerList);
			JAXBElement<?> taxFilerType = null;

			// Populate Primary Taxfiler & Dependents
			for (int i = 0; i < tfSize; i++) {
				taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
				if (taxFilerType != null) {
					if (taxFilerType.getName().getLocalPart().equals(PRIMARYTAXFILER)) {
						JAXBElement<TaxFilerType> primaryTaxFiler = (JAXBElement<TaxFilerType>)taxFilerType;
						if (primaryTaxFiler.getValue() != null && primaryTaxFiler.getValue().getRoleOfPersonReference() != null) {
							primaryTaxFilerId = ((PersonType) primaryTaxFiler.getValue().getRoleOfPersonReference().getRef()).getId();	
						}
					}
				}
			}
		}
		return primaryTaxFilerId;
	}

	private void populateTaxDependent(SingleStreamlinedApplication singleStreamlinedApplication, Map<String, Integer> map, JAXBElement<TaxDependentType> taxFilerType) {
		if (taxFilerType.getValue() != null && taxFilerType.getValue().getRoleOfPersonReference() != null) {
			singleStreamlinedApplication.getTaxFilerDependants().add(new Integer(map.get(((PersonType) taxFilerType.getValue().getRoleOfPersonReference().getRef()).getId())));
		}
	}

	private void populateSpouseTaxFiler(SingleStreamlinedApplication singleStreamlinedApplication, Map<String, Integer> map, JAXBElement<TaxFilerType> taxFilerType) {
		if (taxFilerType.getValue() != null && taxFilerType.getValue().getRoleOfPersonReference() != null) {
			final int spouseId = map.get(((PersonType) taxFilerType.getValue().getRoleOfPersonReference().getRef()).getId());
			final HouseholdMember primaryTaxFiler = ReferralUtil.member(singleStreamlinedApplication, singleStreamlinedApplication.getPrimaryTaxFilerPersonId());
			if (primaryTaxFiler != null) {
				primaryTaxFiler.getTaxFiler().setSpouseHouseholdMemberId(spouseId);
				primaryTaxFiler.setPlanToFileFTRJontlyIndicator(true);
				singleStreamlinedApplication.setJointTaxFilerSpousePersonId(spouseId);
				final HouseholdMember spouse = ReferralUtil.member(singleStreamlinedApplication, spouseId);
				if (spouse != null) {
					spouse.getTaxFiler().setSpouseHouseholdMemberId(primaryTaxFiler.getPersonId());
					spouse.setPlanToFileFTRJontlyIndicator(true);
				}
			}
		}
	}

	private void populatePrimaryTaxFiler(SingleStreamlinedApplication singleStreamlinedApplication, Map<String, Integer> map, JAXBElement<TaxFilerType> taxFilerType) {
		if (taxFilerType.getValue() != null && taxFilerType.getValue().getRoleOfPersonReference() != null) {
			singleStreamlinedApplication.setPrimaryTaxFilerPersonId(map.get(((PersonType) taxFilerType.getValue().getRoleOfPersonReference().getRef()).getId()));
		}
	}

	private void populateHouseholdIncome(TaxHousehold taxHousehold, TaxHouseholdType taxHouseholdType) {
		if (taxHouseholdType.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI() != null) {
			final List<JAXBElement<IncomeType>> householdIncome = taxHouseholdType.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI();
			final int size = ReferralUtil.listSize(householdIncome);
			for (int i = 0; i < size; i++) {
				if (householdIncome.get(i).getName().getLocalPart().equals(HOUSEHOLDINCOME)) {
					if (householdIncome.get(i).getValue() != null && householdIncome.get(i).getValue().getIncomeAmount() != null && householdIncome.get(i).getValue().getIncomeAmount().getValue() != null) {
						taxHousehold.setHouseHoldIncome(householdIncome.get(i).getValue().getIncomeAmount().getValue().doubleValue());
					}
				}
			}
		}
	}

	private void populateBloodRelationship(AccountTransferRequestPayloadType source, HouseholdMember responsibleHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
		if (responsibleHouseholdMember.getBloodRelationship() == null) {
			responsibleHouseholdMember.setBloodRelationship(new ArrayList<BloodRelationship>());
		}
		final Map<String, Integer> map = populateRefIdMap(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember());
		final Map<Integer, Date> dobMap = populateDobMap(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember());
		final List<PersonType> listPersonType = source.getPerson();
		final int size = ReferralUtil.listSize(listPersonType);
		PersonType person = null;
		int size2 = 0;
		List<PersonAssociationType> personAssociationType = null;
		for (int i = 0; i < size; i++) {
			person = listPersonType.get(i);
			if (person.getPersonAugmentation() != null) {
				if (person.getPersonAugmentation().getPersonAssociation() != null) {
					personAssociationType = person.getPersonAugmentation().getPersonAssociation();
					size2 = ReferralUtil.listSize(personAssociationType);
					addSameRelationship(responsibleHouseholdMember.getBloodRelationship(), map, person);
					for (int j = 0; j < size2; j++) {
						if (personAssociationType.get(j).getFamilyRelationshipCode() != null) {
							addOtherRelationship(responsibleHouseholdMember.getBloodRelationship(), map, person, personAssociationType.get(j), dobMap);
						}
					}
				}
			}
		}
	}

	/**
	 * Modified by pravin for HIX-50751. Need to set reverse relationsip. for eg: if P1-P3 relation is 03 from incoming referrals needs to be changed as P3-P1 as 03 for Enrollment.
	 * 
	 * @param bloodRelationships
	 * @param map
	 * @param person
	 */
	private void addOtherRelationship(List<BloodRelationship> bloodRelationships, Map<String, Integer> map, PersonType person, PersonAssociationType personAssociationType, Map<Integer, Date> dobMap) {
		final BloodRelationship bloodRelationship = new BloodRelationship();
		bloodRelationship.setRelatedPersonId("" + map.get(person.getId()));
		bloodRelationship.setIndividualPersonId("" + map.get(((PersonType) personAssociationType.getPersonReference().get(0).getRef()).getId()));
		
		if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("ID") && checkforWardRelationship(personAssociationType.getFamilyRelationshipCode().getValue(), dobMap.get(Integer.parseInt(bloodRelationship.getIndividualPersonId())))) {
			bloodRelationship.setRelation(WARD_RELATIONSHIP_CODE);
		} else {
			String relationship = personAssociationType.getFamilyRelationshipCode().getValue();
			if( StringUtils.isNotBlank(relationship) && StringUtils.contains(relationship, HIPHEN))
			{
				String [] arr = StringUtils.split(relationship,HIPHEN);
				bloodRelationship.setRelation(arr[1]);
			}
			else{
				bloodRelationship.setRelation(relationship);
			}
		}
		bloodRelationships.add(bloodRelationship);
	}

	/**
	 * Added by pravin. if incoming code is "05", "07", "09", "10", "17", "19" needs to be set as 15(ward) if age >=23.
	 * 
	 * @param relationshipCode
	 * @param date
	 * @return
	 */
	private boolean checkforWardRelationship(String relationshipCode, Date date) {
		boolean isWardRelationship = false;
		if (relationShipNeedsToBeChanged.contains(relationshipCode)) {
			if (DateUtil.getAgeFromtodaysDate(date) >= WARD_AGE_LIMIT) {
				isWardRelationship = true;
			}
		}
		return isWardRelationship;
	}

	private void addSameRelationship(List<BloodRelationship> bloodRelationships, Map<String, Integer> map, PersonType person) {
		final BloodRelationship bloodRelationship = new BloodRelationship();
		bloodRelationship.setIndividualPersonId("" + map.get(person.getId()));
		bloodRelationship.setRelatedPersonId("" + map.get(person.getId()));
		bloodRelationship.setRelation(SAME_RELATION_CODE);
		bloodRelationships.add(bloodRelationship);

	}

	private Map<Integer, Date> populateDobMap(List<HouseholdMember> householdMember) {
		final int size = ReferralUtil.listSize(householdMember);
		Map<Integer, Date> dobMap = new HashMap<Integer, Date>();
		for (int i = 0; i < size; i++) {
			dobMap.put(householdMember.get(i).getPersonId(), householdMember.get(i).getDateOfBirth());
		}
		return dobMap;
	}

	private Map<String, Integer> populateRefIdMap(List<HouseholdMember> householdMemberList) {
		final int size = ReferralUtil.listSize(householdMemberList);
		final Map<String, Integer> map = new HashMap<String, Integer>();

		for (int i = 0; i < size; i++) {
			map.put(householdMemberList.get(i).getRequestRef(), householdMemberList.get(i).getPersonId());
		}
		return map;
	}

	private void populateLivesAtOtherAddress(HouseholdMember responsibleHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		final int size = ReferralUtil.listSize(householdMemberList);
		responsibleHouseholdMember.setLivesWithHouseholdContactIndicator(true);
		if (size > 1) // No Need to check if there is one primary
		{
			boolean blnCheck = false;
			boolean blnLivesOther = false;
			Address otherAddres = null;
			for (int i = 0; i < size; i++) {
				if (!householdMemberList.get(i).isPrimaryTaxFiler()) {

					blnCheck = false;
					blnLivesOther = false;
					otherAddres = null;

					if (responsibleHouseholdMember.getHouseholdContact().getHomeAddress() != null && householdMemberList.get(i).getHouseholdContact().getHomeAddress() != null) {
						blnCheck = ReferralUtil.compareAddressData(responsibleHouseholdMember.getHouseholdContact().getHomeAddress(), 
								householdMemberList.get(i).getHouseholdContact().getHomeAddress());
					}

					if (blnCheck) {
						blnLivesOther = false;
					} else if (householdMemberList.get(i).getHouseholdContact().getHomeAddress() != null 
							&& householdMemberList.get(i).getHouseholdContact().getHomeAddress().getPostalCode() != null) {
						blnLivesOther = true;
						otherAddres = householdMemberList.get(i).getHouseholdContact().getHomeAddress();
					} else {
						if (responsibleHouseholdMember.getHouseholdContact().getMailingAddress() != null 
								&& householdMemberList.get(i).getHouseholdContact().getMailingAddress() != null) {
							blnCheck = ReferralUtil.compareAddressData(responsibleHouseholdMember.getHouseholdContact().getMailingAddress(), 
									householdMemberList.get(i).getHouseholdContact().getMailingAddress());
						}

						if (blnCheck) {
							blnLivesOther = false;
						} else if (householdMemberList.get(i).getHouseholdContact().getMailingAddress() != null && householdMemberList.get(i).getHouseholdContact().getMailingAddress().getPostalCode() != null) {
							blnLivesOther = true;
							otherAddres = householdMemberList.get(i).getHouseholdContact().getMailingAddress();
						}
					}

					/* Added for HIX-49310 */
					householdMemberList.get(i).setLivesWithHouseholdContactIndicator(!blnLivesOther);

					if (blnLivesOther) {
						householdMemberList.get(i).setLivesAtOtherAddressIndicator(blnLivesOther);
						if (householdMemberList.get(i).getOtherAddress() == null) {
							householdMemberList.get(i).setOtherAddress(new OtherAddress());
						}
						/* Fix for HIX-49311, HIX-48472 Starts Here */
						/*
						 * if (householdMemberList.get(i).getOtherAddress().getAddress() == null) { householdMemberList.get(i).getOtherAddress().setAddress(new Address()); }
						 */
						if (householdMemberList.get(i).getOtherAddress().getAddress() != null) {
							ReferralUtil.copyProperties(otherAddres, householdMemberList.get(i).getOtherAddress().getAddress());
						}
						householdMemberList.get(i).getOtherAddress().getAddress().setCounty(otherAddres.getPrimaryAddressCountyFipsCode());
						householdMemberList.get(i).getOtherAddress().getAddress().setCountyCode(otherAddres.getPrimaryAddressCountyFipsCode());//HIX-112754: Set county code in other address 
						householdMemberList.get(i).getOtherAddress().getAddress().setPrimaryAddressCountyFipsCode(null);

						/* Fix for HIX-49311, HIX-48472 Ends Here */
					}
				}
			}
		}
	}

	private HouseholdMember populateHouseHoldMembers(AccountTransferRequestPayloadType source, SingleStreamlinedApplication singleStreamlinedApplication) {
		final List<String> insuranceApplicants = extractInsuranceApplicantIds(source);
		final String primaryApplicantId = extractPrimaryApplicantId(source);
		final String primaryTaxFilerId =  extractPrimaryTaxFilerId(source);
		
		PersonType person = null;
		HouseholdMember houseHoldMember = null;
		TaxHousehold taxHouseHold = new TaxHousehold();
		singleStreamlinedApplication.getTaxHousehold().add(taxHouseHold);

		final List<PersonType> listPersonType = source.getPerson();
		final int size = ReferralUtil.listSize(listPersonType);
		int ctr = 2;
		HouseholdMember responsibleHouseholdMember = null;
		
		MailingAddress primaryMailingAddress = new MailingAddress();
		
		for (int i = 0; i < size; i++) {
			person = listPersonType.get(i);
			if (person == null) {
				continue;
			}
			// This method is called just to check if the person is a primary contact. Though household member is null at this time, method will handle null check.
			// The actual persontype will be determined later in code as it is dependent on few other factors. Right now only need to check if this is a primary contact or not.
			SsapApplicantPersonType personType = this.determineApplicantPersonType(primaryApplicantId, primaryTaxFilerId, person, houseHoldMember);
			if (personType != null && personType.getPersonType().contains(PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE)) {
				populatePrimaryMailingAddress(person,primaryMailingAddress);
				break;
			}
		}
		
		String exchangeATGenerateMemberIds = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_AT_GENERATE_MEMBER_IDS);
		
		boolean exchangeATGenerateMemberIdEnabled = "TRUE".equalsIgnoreCase(exchangeATGenerateMemberIds)?true:false;
		
		for (int i = 0; i < size; i++) {
			person = listPersonType.get(i);
			if (person == null) {
				continue;
			}
			
			houseHoldMember = dozzerMapper.map(person, HouseholdMember.class, "dozzerMappingHouseholdId");

			if (houseHoldMember == null) {
				continue;
			}

			dozzerMapper.map(person, houseHoldMember, "dozzerMappingHouseholdMiscId");
			// set applyingForCoverageIndicator
			if(null != person.getPersonSeekingCoverageIndicator() && insuranceApplicants.indexOf(person.getId()) != -1) {
				houseHoldMember.setApplyingForCoverageIndicator(person.getPersonSeekingCoverageIndicator().isValue());
			}else if (insuranceApplicants.indexOf(person.getId()) != -1) {
				houseHoldMember.setApplyingForCoverageIndicator(true);
			}
			
			houseHoldMember.setApplicantPersonType(this.determineApplicantPersonType(primaryApplicantId, primaryTaxFilerId, person, houseHoldMember));
			// Set Id & HouseholdContactIndicator for the houseHoldMember
			if(houseHoldMember.isPrimaryContact()) {
				houseHoldMember.setPersonId(1);
				houseHoldMember.setHouseholdContactIndicator(true);
				houseHoldMember.setRequestRef(person.getId());
				
				if(!exchangeATGenerateMemberIdEnabled) {
					houseHoldMember.setApplicantGuid(getApplicantGuid(person));
				}else {
					houseHoldMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));	
				}
				
				
				
			} else {
				houseHoldMember.setPersonId(ctr);
				houseHoldMember.setHouseholdContactIndicator(false);
				houseHoldMember.setRequestRef(person.getId());
				
				if(!exchangeATGenerateMemberIdEnabled) {
					houseHoldMember.setApplicantGuid(getApplicantGuid(person));
				}else {
					houseHoldMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));	
				}
				
				
				ctr++;
			}

			// Set responsible person to PTF rather than PC. Use person type to determine responsible person.
			if(houseHoldMember.isPrimaryTaxFiler()){
				responsibleHouseholdMember = houseHoldMember;
			}

			populateHouseholdContact(person, houseHoldMember,primaryMailingAddress);

			populateContactPreference(houseHoldMember, source);

			populateEthnicityAndRace(person, houseHoldMember);

			populateHealthCoverage(person, houseHoldMember);

			populateIncarceration(source.getInsuranceApplication(), houseHoldMember);

			populateApplicantId(person, houseHoldMember);

			populateDataFromInsuranceApplication(source.getInsuranceApplication(), houseHoldMember, person.getId());

			populateOtherData(person, houseHoldMember);
			
			populateFederallyRecognizedTribeIndicator(person, houseHoldMember);

			taxHouseHold.getHouseholdMember().add(houseHoldMember);
		}

		return responsibleHouseholdMember;
	}

	private SsapApplicantPersonType determineApplicantPersonType(final String primaryApplicantId, final String primaryTaxFilerId,
		PersonType person, HouseholdMember houseHoldMember) {
		SsapApplicantPersonType personType = null;
		if(person.getId().equalsIgnoreCase(primaryTaxFilerId) && person.getId().equalsIgnoreCase(primaryApplicantId)){
			personType = SsapApplicantPersonType.PC_PTF;
		}
		else if(person.getId().equalsIgnoreCase(primaryTaxFilerId)){
			personType = SsapApplicantPersonType.PTF;
		}
		else if(!person.getId().equalsIgnoreCase(primaryTaxFilerId) && !person.getId().equalsIgnoreCase(primaryApplicantId)){
			personType = SsapApplicantPersonType.DEP;
		}
		else if(person.getId().equalsIgnoreCase(primaryApplicantId) && (houseHoldMember!= null && houseHoldMember.getApplyingForCoverageIndicator())){
			personType = SsapApplicantPersonType.PC_DEP;
		}
		else{
			personType = SsapApplicantPersonType.PC;
		}
		
		return personType;
	}
	
	private String getApplicantGuid(PersonType person){
		return person.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
	}

	private void populateOtherData(PersonType person, HouseholdMember houseHoldMember) {
		if (person.getPersonAugmentation() != null) {
			if (person.getPersonAugmentation().getPersonUSVeteranIndicator() != null) {
				final int size = ReferralUtil.listSize(person.getPersonAugmentation().getPersonUSVeteranIndicator());
				// Check This
				for (int i = 0; i < size; i++) {
					houseHoldMember.getCitizenshipImmigrationStatus().setHonorablyDischargedOrActiveDutyMilitaryMemberIndicator(person.getPersonAugmentation().getPersonUSVeteranIndicator().get(i).isValue());
				}
			}
		}
	}
	
	private void populateFederallyRecognizedTribeIndicator(PersonType person, HouseholdMember houseHoldMember) {
		if (person.getTribalAugmentation() != null
				&& person.getTribalAugmentation().getPersonAmericanIndianOrAlaskaNativeIndicator() != null) {
			houseHoldMember.getAmericanIndianAlaskaNative().setMemberOfFederallyRecognizedTribeIndicator(
					person.getTribalAugmentation().getPersonAmericanIndianOrAlaskaNativeIndicator().isValue());
		}
	}

	private void populateDataFromInsuranceApplication(InsuranceApplicationType insuranceApplication, HouseholdMember houseHoldMember, String id) {
		final List<InsuranceApplicantType> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		final int size = ReferralUtil.listSize(insuranceApplicantList);
		PersonType personType;
		InsuranceApplicantType insuranceApplicant;

		if (houseHoldMember.getCitizenshipImmigrationStatus() == null) {
			houseHoldMember.setCitizenshipImmigrationStatus(new CitizenshipImmigrationStatus());
		}

		if (houseHoldMember.getSpecialCircumstances() == null) {
			houseHoldMember.setSpecialCircumstances(new SpecialCircumstances());
		}

		if (houseHoldMember.getHealthCoverage() == null) {
			houseHoldMember.setHealthCoverage(new HealthCoverage());
		}

		if (houseHoldMember.getIncarcerationStatus() == null) {
			houseHoldMember.setIncarcerationStatus(new IncarcerationStatus());
		}

		for (int i = 0; i < size; i++) {
			insuranceApplicant = insuranceApplicantList.get(i);
			if (insuranceApplicant.getRoleOfPersonReference() != null) {
				if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {
					personType = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
					if (id.equals(personType.getId())) {
						// InsuranceApplicantTemporarilyLivesOutsideApplicationStateIndicator
						if (insuranceApplicant.getInsuranceApplicantTemporarilyLivesOutsideApplicationStateIndicator() != null) {
							if (houseHoldMember.getOtherAddress() == null) {
								houseHoldMember.setOtherAddress(new OtherAddress());
							}
							houseHoldMember.getOtherAddress().setLivingOutsideofStateTemporarilyIndicator(insuranceApplicant.getInsuranceApplicantTemporarilyLivesOutsideApplicationStateIndicator().isValue());
						}

						// Special Circumstances Data
						if (insuranceApplicant.getInsuranceApplicantFosterCareIndicator() != null) {
							houseHoldMember.getSpecialCircumstances().setEverInFosterCareIndicator(insuranceApplicant.getInsuranceApplicantFosterCareIndicator().isValue());
						}

						if (insuranceApplicant.getInsuranceApplicantAgeLeftFosterCare() != null) {
							houseHoldMember.getSpecialCircumstances().setAgeWhenLeftFosterCare(ReferralUtil.convertToInteger(insuranceApplicant.getInsuranceApplicantAgeLeftFosterCare().getValue()));
						}

						if (insuranceApplicant.getInsuranceApplicantFosterCareState() != null) {
							houseHoldMember.getSpecialCircumstances().setFosterCareState(ReferralUtil.enumValueAsString(StateCodeEnum.class, insuranceApplicant.getInsuranceApplicantFosterCareState().getValue().value()));
						}

						if (insuranceApplicant.getInsuranceApplicantHadMedicaidDuringFosterCareIndicator() != null) {
							houseHoldMember.getSpecialCircumstances().setGettingHealthCareThroughStateMedicaidIndicator(insuranceApplicant.getInsuranceApplicantHadMedicaidDuringFosterCareIndicator().isValue());
						}

						// Disability Indicator
						if (insuranceApplicant.getInsuranceApplicantBlindnessOrDisabilityIndicator() != null) {
							houseHoldMember.setDisabilityIndicator(insuranceApplicant.getInsuranceApplicantBlindnessOrDisabilityIndicator().isValue());
						}

						// Living arrangement indicator
						if (insuranceApplicant.getInsuranceApplicantLongTermCareIndicator() != null) {
							houseHoldMember.setLivingArrangementIndicator(insuranceApplicant.getInsuranceApplicantLongTermCareIndicator().isValue());
						}

						// Uninsured within last six months
						if (insuranceApplicant.getInsuranceApplicantCoverageDuringPreviousSixMonthsIndicator() != null) {
							houseHoldMember.getHealthCoverage().setHaveBeenUninsuredInLast6MonthsIndicator(insuranceApplicant.getInsuranceApplicantCoverageDuringPreviousSixMonthsIndicator().isValue());
						}

						// Have health insurance indicator
						if (insuranceApplicant.getInsuranceApplicantNonESICoverageIndicator() != null && insuranceApplicant.getInsuranceApplicantNonESICoverageIndicator().size() > 0
								&& insuranceApplicant.getInsuranceApplicantNonESICoverageIndicator().get(0) != null) {
							houseHoldMember.getHealthCoverage().setCurrentlyHasHealthInsuranceIndicator(insuranceApplicant.getInsuranceApplicantNonESICoverageIndicator().get(0).isValue());
						}

						// Want help paying for medical bills from the last 3
						// months indicator
						if (insuranceApplicant.getInsuranceApplicantRecentMedicalBillsIndicator() != null) {
							if (houseHoldMember.getHealthCoverage().getMedicaidInsurance() == null) {
								houseHoldMember.getHealthCoverage().setMedicaidInsurance(new MedicaidInsurance());
							}

							houseHoldMember.getHealthCoverage().getMedicaidInsurance().setRequestHelpPayingMedicalBillsLast3MonthsIndicator(insuranceApplicant.getInsuranceApplicantRecentMedicalBillsIndicator().isValue());
						}

						// useSelfAttestedIncarceration
						if (insuranceApplicant.getInsuranceApplicantIncarceration() != null && ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantIncarceration()) != 0) {
							if (insuranceApplicant.getInsuranceApplicantIncarceration().get(0).getIncarcerationIndicator() != null) {
								houseHoldMember.getIncarcerationStatus().setUseSelfAttestedIncarceration(insuranceApplicant.getInsuranceApplicantIncarceration().get(0).getIncarcerationIndicator().isValue());
							}
						}

						// chpInsurance:reasonInsuranceEndedOther
						if (insuranceApplicant.getInsuranceApplicantNonESIPolicy() != null && ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantNonESIPolicy()) != 0) {
							if (insuranceApplicant.getInsuranceApplicantNonESIPolicy().get(0).getDisenrollmentActivity() != null
							        && insuranceApplicant.getInsuranceApplicantNonESIPolicy().get(0).getDisenrollmentActivity().getDisenrollmentActivityReasonCode() != null) {
								if (houseHoldMember.getHealthCoverage().getChpInsurance() == null) {
									houseHoldMember.getHealthCoverage().setChpInsurance(new ChpInsurance());
								}

								houseHoldMember.getHealthCoverage().getChpInsurance()
								        .setReasonInsuranceEndedOther(InsuranceEndReasonEnum.value(insuranceApplicant.getInsuranceApplicantNonESIPolicy().get(0).getDisenrollmentActivity().getDisenrollmentActivityReasonCode().getValue().value()));
							}
						}

						// Other & Eligible ImmigrationDocumentType
						populateImmigrationStatus(insuranceApplicant, houseHoldMember);

						break;
					}
				}
			}
		}
	}

	private void populateImmigrationStatus(InsuranceApplicantType insuranceApplicant, HouseholdMember houseHoldMember) {
		if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus() == null) {
			return;
		}

		if (houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipDocument() == null) {
			houseHoldMember.getCitizenshipImmigrationStatus().setCitizenshipDocument(new CitizenshipDocument());
		}

		if (houseHoldMember.getCitizenshipImmigrationStatus().getOtherImmigrationDocumentType() == null) {
			houseHoldMember.getCitizenshipImmigrationStatus().setOtherImmigrationDocumentType(new OtherImmigrationDocumentType());
		}

		if (houseHoldMember.getCitizenshipImmigrationStatus().getEligibleImmigrationDocumentType() == null) {
			houseHoldMember.getCitizenshipImmigrationStatus().setEligibleImmigrationDocumentType(new ArrayList<EligibleImmigrationDocumentType>());
		}

		if (ReferralUtil.listSize(houseHoldMember.getCitizenshipImmigrationStatus().getEligibleImmigrationDocumentType()) == 0) {
			houseHoldMember.getCitizenshipImmigrationStatus().getEligibleImmigrationDocumentType().add(new EligibleImmigrationDocumentType());
		}

		// Same Name Indicator
		populateDocumentSameNameIndicator(insuranceApplicant, houseHoldMember.getCitizenshipImmigrationStatus());

		// Lived in the U.S. since 1996?
		if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusArrivedBefore1996Indicator() != null) {
			houseHoldMember.getCitizenshipImmigrationStatus().setLivedIntheUSSince1996Indicator(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusArrivedBefore1996Indicator().isValue());
		}

		// Eligible immigration status indicator
		if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusEligibility() != null
				&& insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusEligibility().size() > 0
		        && insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusEligibility().get(0) != null
		        && insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusEligibility().get(0).getEligibilityIndicator() != null) {
			houseHoldMember.getCitizenshipImmigrationStatus().setEligibleImmigrationStatusIndicator(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusEligibility().get(0).getEligibilityIndicator().isValue());
		}

		if (ReferralUtil.isTrue(houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator())) {
			return;
		}

		if (ReferralUtil.isTrue(houseHoldMember.getCitizenshipImmigrationStatus().getNaturalizedCitizenshipIndicator())) {
			populateNaturalizedCitizenship(insuranceApplicant, houseHoldMember.getCitizenshipImmigrationStatus());
			return;
		}

		populateEligibleImmigrationDocument(insuranceApplicant, houseHoldMember.getCitizenshipImmigrationStatus());

		populateOtherImmigrationDocument(insuranceApplicant, houseHoldMember.getCitizenshipImmigrationStatus());

	}

	private void populateOtherImmigrationDocument(InsuranceApplicantType insuranceApplicant, CitizenshipImmigrationStatus citizenshipImmigrationStatus) {
		if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument() != null) {
			final int size = ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument());
			if (size > 0) {
				String s;
				for (int i = 0; i < size; i++) {
					if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i).getLawfulPresenceDocumentCategoryCode() != null
					        && insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i).getLawfulPresenceDocumentCategoryCode().getValue() != null) {
						s = ReferralUtil.checkAndValidString(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i).getLawfulPresenceDocumentCategoryCode().getValue().value());

						if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.CUBAN_HAITIAN_ENTRANT.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setCubanHaitianEntrantIndicator(true);
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.ORR_ELIGIBILITY_LETTER.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setORREligibilityLetterIndicator(true);
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.STAY_OF_REMOVAL.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setStayOfRemovalIndicator(true);
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.ORR_CERTIFICATION.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setORRCertificationIndicator(true);
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.AMERICAN_SAMOAN.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setAmericanSamoanIndicator(true);
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.WITHHOLDING_OF_REMOVAL.value())) {
							citizenshipImmigrationStatus.getOtherImmigrationDocumentType().setWithholdingOfRemovalIndicator(true);
						}

					}
				}
			}
		}

	}

	private void populateEligibleImmigrationDocument(InsuranceApplicantType insuranceApplicant, CitizenshipImmigrationStatus citizenshipImmigrationStatus) {
		if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument() != null) {
			final int size = ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument());
			if (size > 0) {
				String s;
				LawfulPresenceDocumentType lawfulPresenceDocumentType = null;
				for (int i = 0; i < size; i++) {
					lawfulPresenceDocumentType = insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i);
					if (lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode().getValue() != null) {
						s = ReferralUtil.checkAndValidString(lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode().getValue().value());

						if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_551.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI551Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_551.value());
							populateCardNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());

						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.DS_2019.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setDS2019Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.DS_2019.value());
							populatForeignPassport(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.UNEXPIRED_FOREIGN_PASSPORT.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setUnexpiredForeignPassportIndicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.UNEXPIRED_FOREIGN_PASSPORT.value());
							populatForeignPassport(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());

							if (lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().size() > 0
									&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0) != null
									&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationJurisdictionISO3166Alpha3Code() != null
							        && lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationJurisdictionISO3166Alpha3Code().getValue() != null) {

								citizenshipImmigrationStatus.getCitizenshipDocument().setForeignPassportCountryOfIssuance(
								        lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationJurisdictionISO3166Alpha3Code().getValue());
							}
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.TEMPORARY_I_551_STAMP.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setTemporaryI551StampIndicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.TEMPORARY_I_551_STAMP.value());
							populatForeignPassport(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.MACHINE_READABLE_VISA.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setMachineReadableVisaIndicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.MACHINE_READABLE_VISA.value());
							populatForeignPassport(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_94_IN_PASSPORT.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI94InPassportIndicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_94_IN_PASSPORT.value());
							populatForeignPassport(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_571.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI571Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_571.value());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_766.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI766Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_766.value());
							populateCardNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_20.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI20Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_20.value());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_94.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI94Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_94.value());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_797.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI797Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_797.value());
							populateI94Number(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						} else if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.I_327.value())) {
							citizenshipImmigrationStatus.getEligibleImmigrationDocumentType().get(0).setI327Indicator(true);
							citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.I_327.value());
							populateAlienNumber(lawfulPresenceDocumentType, citizenshipImmigrationStatus.getCitizenshipDocument());
						}
					}
				}
			}
		}

	}

	private void populatForeignPassport(LawfulPresenceDocumentType lawfulPresenceDocumentType, CitizenshipDocument citizenshipDocument) {
		if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
		        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null) {
			citizenshipDocument.setForeignPassportOrDocumentNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue());

		}
	}

	private void populateCardNumber(LawfulPresenceDocumentType lawfulPresenceDocumentType, CitizenshipDocument citizenshipDocument) {
		if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
		        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null)  {
			citizenshipDocument.setCardNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue());

		}
	}

	private void populateAlienNumber(LawfulPresenceDocumentType lawfulPresenceDocumentType, CitizenshipDocument citizenshipDocument) {
		if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
		        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null)  {

			citizenshipDocument.setAlienNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID().getValue());
		}
	}

	private void populateI94Number(LawfulPresenceDocumentType lawfulPresenceDocumentType, CitizenshipDocument citizenshipDocument) {
		if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
				&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
		        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null)  {

			citizenshipDocument.setI94Number(lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID().getValue());
		}
	}

	private void populateDocumentSameNameIndicator(InsuranceApplicantType insuranceApplicant, CitizenshipImmigrationStatus citizenshipImmigrationStatus) {
		final int size = ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument());
		if (size != 0) {
			for (int i = 0; i < size; i++) {
				if (insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i).getLawfulPresenceDocumentSameNameIndicator() != null) {
					citizenshipImmigrationStatus.getCitizenshipDocument().setNameSameOnDocumentIndicator(
					        insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i).getLawfulPresenceDocumentSameNameIndicator().isValue());
					break;
				}
			}
		}
	}

	private void populateNaturalizedCitizenship(InsuranceApplicantType insuranceApplicant, CitizenshipImmigrationStatus citizenshipImmigrationStatus) {
		final int size = ReferralUtil.listSize(insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument());
		if (size != 0) {
			String s;
			LawfulPresenceDocumentType lawfulPresenceDocumentType = null;
			for (int i = 0; i < size; i++) {
				lawfulPresenceDocumentType = insuranceApplicant.getInsuranceApplicantLawfulPresenceStatus().getLawfulPresenceStatusImmigrationDocument().get(i);
				if (lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode().getValue() != null) {
					s = ReferralUtil.checkAndValidString(lawfulPresenceDocumentType.getLawfulPresenceDocumentCategoryCode().getValue().value());
					if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.CERTIFICATE_OF_CITIZENSHIP.value())) {
						// citizenshipImmigrationStatus.setCertificateOfCitizenshipIndicator(true); // TODO Add this property
						if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
						        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null)  {
							// citizenshipImmigrationStatus.setCertificateOfCitizenshipCertificateNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().getIdentificationID().getValue()); // TODO

						}
						if (lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification() != null 
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().size() > 0
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0) != null
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID() != null
						        && lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID().getValue() != null) {

							// citizenshipImmigrationStatus.setCertificateOfCitizenshipAlienNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().getIdentificationID().getValue());// TODO
						}
						citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.CERTIFICATE_OF_CITIZENSHIP.value());
						break;
					}
					if (s.equals(ImmigrationDocumentCategoryCodeSimpleType.NATURALIZATION_CERTIFICATE.value())) {
						citizenshipImmigrationStatus.setNaturalizedCitizenshipIndicator(true);
						if (lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber() != null && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().size() > 0
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0) != null 
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID() != null
						        && lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue() != null) {
							citizenshipImmigrationStatus.setNaturalizationCertificateNaturalizationNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().get(0).getIdentificationID().getValue());

						}
						if (lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification() != null 
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().size() > 0
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0) != null
								&& lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID() != null
						        && lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID().getValue() != null) {

							citizenshipImmigrationStatus.setNaturalizationCertificateAlienNumber(lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().get(0).getIdentificationID().getValue());
						}
						citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected(ImmigrationDocumentCategoryCodeSimpleType.NATURALIZATION_CERTIFICATE.value());
						break;
					}
				}
			}
		}
	}

	private void populateApplicantId(PersonType personType, HouseholdMember houseHoldMember) {
		//if (isStateId()) {
			String identificationId = null;
			if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
				identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
			}

			if (identificationId == null) {
				if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID().getValue();
				}
			}
			houseHoldMember.setExternalId(identificationId);
		//}
	}

	private void populateIncarceration(InsuranceApplicationType insuranceApplication, HouseholdMember houseHoldMember) {
		if (houseHoldMember.getIncarcerationStatus() == null) {
			houseHoldMember.setIncarcerationStatus(new IncarcerationStatus());
		}

		dozzerMapper.map(insuranceApplication, houseHoldMember.getIncarcerationStatus(), "dozzerMappingIncarcerationId");
	}

	private void populateHealthCoverage(PersonType person, HouseholdMember houseHoldMember) {

		if (person.getPersonAugmentation() == null) {
			return;
		}

		if (person.getPersonAugmentation().getPersonEmploymentAssociation() != null) {
			if (houseHoldMember.getHealthCoverage() == null) {
				houseHoldMember.setHealthCoverage(new HealthCoverage());
			}

			// Current Employer
			final List<PersonEmploymentAssociationType> personEmploymentAssociationList = person.getPersonAugmentation().getPersonEmploymentAssociation();
			final int size = ReferralUtil.listSize(personEmploymentAssociationList);

			for (int i = 0; i < size; i++) {
				houseHoldMember.getHealthCoverage().getCurrentEmployer().add(dozzerMapper.map(personEmploymentAssociationList.get(i), CurrentEmployer.class, "dozzerMappingEmployeerId"));
				if (personEmploymentAssociationList.get(i).getESIAugmentation() != null) {
					if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI() != null) {
						if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESIExpectedChange() != null) {
							if (houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).getCurrentEmployerInsurance() == null) {
								houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).setCurrentEmployerInsurance(new CurrentEmployerInsurance());
							}

							if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESIExpectedChange().getValue().value().equals("Unknown")) {
								houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).getCurrentEmployerInsurance().setExpectedChangesToEmployerCoverageIndicator(true);
							} else if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESIExpectedChange().getValue().value().equals("DoesNotOffer")) {
								houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).getCurrentEmployerInsurance().setEmployerWillNotOfferCoverageIndicator(true);
								if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESIExpectedChangeDate() != null) {
									houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).getCurrentEmployerInsurance()
									        .setEmployerCoverageEndingDate(ReferralUtil.extractDate(personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESIExpectedChangeDate().getDate()));
								}
							}
						}

						if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESILowestCostPlan() != null) {
							if (personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESILowestCostPlan().getMinimumActuarialValueStandardIndicator() != null) {
								houseHoldMember.getHealthCoverage().getCurrentEmployer().get(i).getCurrentEmployerInsurance()
								        .setCurrentPlanMeetsMinimumStandardIndicator(personEmploymentAssociationList.get(i).getESIAugmentation().getESI().getESILowestCostPlan().getMinimumActuarialValueStandardIndicator().isValue());
							}
						}
					}
				}
			}
			if (size != 0) {
				populateParentProperties(houseHoldMember.getHealthCoverage(), person.getPersonAugmentation().getPersonEmploymentAssociation().get(0));
			}
		}
	}

	private void populateParentProperties(HealthCoverage healthCoverage, PersonEmploymentAssociationType personEmploymentAssociationType) {
		if (personEmploymentAssociationType.getESIAugmentation() != null) {
			if (personEmploymentAssociationType.getESIAugmentation().getESI() != null) {
				if (personEmploymentAssociationType.getESIAugmentation().getESI().getESIViaCurrentEmployeeIndicator() != null) {
					healthCoverage.setCurrentlyEnrolledInCobraIndicator(personEmploymentAssociationType.getESIAugmentation().getESI().getESIViaCurrentEmployeeIndicator().isValue());
				}

				healthCoverage.setEmployerWillOfferInsuranceIndicator(true);

				if (personEmploymentAssociationType.getESIAugmentation().getESI().getESIRetireePlanIndicator() != null) {
					healthCoverage.setCurrentlyEnrolledInRetireePlanIndicator(personEmploymentAssociationType.getESIAugmentation().getESI().getESIRetireePlanIndicator().isValue());
				}
			}
		}
	}

	private void populateEthnicityAndRace(PersonType person, HouseholdMember houseHoldMember) {
		final List<TextType> personEthnicityTextList = person.getPersonEthnicityText();
		final List<TextType> personRaceTextList = person.getPersonRaceText();
		final int eSize = ReferralUtil.listSize(personEthnicityTextList);
		final int rSize = ReferralUtil.listSize(personRaceTextList);
		if (houseHoldMember.getEthnicityAndRace() == null) {
			houseHoldMember.setEthnicityAndRace(new EthnicityAndRace());
		}
		boolean flg = false;
		/*if (eSize != 0) {
			if (houseHoldMember.getEthnicityAndRace().getEthnicity() == null) {
				houseHoldMember.getEthnicityAndRace().setEthnicity(new ArrayList<Ethnicity>());
			}
			List<String> ethnicityCodes = new ArrayList<String>();
			for (int i = 0; i < eSize; i++) {
				Ethnicity ethnicity = new Ethnicity();
				EthnicityEnum ethnicityVal = EthnicityEnum.fromValue(personEthnicityTextList.get(i).getValue());
				if (ethnicityVal != null) {
					ethnicity.setCode(ethnicityVal.getCode());
					ethnicity.setLabel(ethnicityVal.getLabel());
				} else {
					ethnicity.setCode(EthnicityEnum.OTHER.getCode());
					ethnicity.setLabel(EthnicityEnum.OTHER.getLabel());
					ethnicity.setOtherLabel(personEthnicityTextList.get(i).getValue());
				}
				if (!ethnicityCodes.contains(ethnicity.getCode())) {
					ethnicityCodes.add(ethnicity.getCode());
					houseHoldMember.getEthnicityAndRace().getEthnicity().add(ethnicity);
				}
			}

			flg = ReferralUtil.listSize(houseHoldMember.getEthnicityAndRace().getEthnicity()) != 0;
		}*/
		List<String> raceCodes = null;
		if(eSize != 0 || rSize != 0) {
			raceCodes = new ArrayList<String>();
		}
		if (eSize != 0) {
			if (houseHoldMember.getEthnicityAndRace().getRace() == null) {
				houseHoldMember.getEthnicityAndRace().setRace(new ArrayList<Race>());
		}

			for (int i = 0; i < eSize; i++) {
				Race race = new Race();
				RaceEnum raceVal = RaceEnum.fromValue(personEthnicityTextList.get(i).getValue());
				if (raceVal != null) {
					race.setCode(raceVal.getCode());
					race.setLabel(raceVal.getLabel());
				} else {
					race.setCode(RaceEnum.OTHER.getCode());
					race.setLabel(RaceEnum.OTHER.getLabel());
					race.setOtherLabel(personEthnicityTextList.get(i).getValue());
				}
				if (!raceCodes.contains(race.getCode())) {
					raceCodes.add(race.getCode());
					houseHoldMember.getEthnicityAndRace().getRace().add(race);
				}
			}

			flg = ReferralUtil.listSize(houseHoldMember.getEthnicityAndRace().getRace()) != 0;
		}

		if (rSize != 0) {
			if (houseHoldMember.getEthnicityAndRace().getRace() == null) {
				houseHoldMember.getEthnicityAndRace().setRace(new ArrayList<Race>());
			}
			
			for (int i = 0; i < rSize; i++) {
				Race race = new Race();
				RaceEnum raceVal = RaceEnum.fromValue(personRaceTextList.get(i).getValue());
				if (raceVal != null) {
					race.setCode(raceVal.getCode());
					race.setLabel(raceVal.getLabel());
				} else {
					race.setCode(RaceEnum.OTHER.getCode());
					race.setLabel(RaceEnum.OTHER.getLabel());
					race.setOtherLabel(personRaceTextList.get(i).getValue());
				}
				if (!raceCodes.contains(race.getCode())) {
					raceCodes.add(race.getCode());
					houseHoldMember.getEthnicityAndRace().getRace().add(race);
				}
			}
		}

		houseHoldMember.getEthnicityAndRace().setHispanicLatinoSpanishOriginIndicator(flg);
	}

	private void populateContactPreference(HouseholdMember houseHoldMember, AccountTransferRequestPayloadType source) {
		if (source.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode() != null && source.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode().getValue() != null) {
			houseHoldMember.getHouseholdContact().getContactPreferences().setPreferredContactMethod(source.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode().getValue().value());
		}
	}

	private void populateHouseholdContact(PersonType person, HouseholdMember houseHoldMember,MailingAddress primaryAddress) {
		if (person.getPersonAugmentation() == null) {
			return;
		}

		List<PersonContactInformationAssociationType> listPersonContactInformationAssociation = person.getPersonAugmentation().getPersonContactInformationAssociation();
		final int size = ReferralUtil.listSize(listPersonContactInformationAssociation);
		HouseholdContact householdContact = houseHoldMember.getHouseholdContact();

		if (householdContact == null) {
			householdContact = new HouseholdContact();
			houseHoldMember.setHouseholdContact(householdContact);
		}

		if (householdContact.getContactPreferences() == null) {
			householdContact.setContactPreferences(new ContactPreferences());
		}

		PersonContactInformationAssociationType personContactInformationAssociationType = null;
		String contactCode;
		String contactEmailId = null;
		String homeContactEmailId = null;
		// System.out.println("houseHoldMember.getPersonId() =========================== " + houseHoldMember.getPersonId());
		for (int i = 0; i < size; i++) {
			personContactInformationAssociationType = listPersonContactInformationAssociation.get(i);

			if (personContactInformationAssociationType.getContactInformation() != null && personContactInformationAssociationType.getContactInformation().getContactEmailID() != null) {
				contactEmailId = personContactInformationAssociationType.getContactInformation().getContactEmailID().getValue();
			}

			if (personContactInformationAssociationType.getContactInformationCategoryCode() != null && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() != null) {
				contactCode = ReferralUtil.validString(personContactInformationAssociationType.getContactInformationCategoryCode().getValue().value());

				if (contactCode.equals(ContactInformationCategoryCodeSimpleType.HOME.value())) // Setting Home Data
				{
					if (personContactInformationAssociationType.getContactInformation() != null) {
						// Address Data
						if (personContactInformationAssociationType.getContactInformation().getContactMailingAddress() != null
						        && personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress() != null) {
							if (householdContact.getHomeAddress() == null) {
								householdContact.setHomeAddress(new HomeAddress());
							}

							pouplateHouseHoldContactAddress(householdContact.getHomeAddress(), personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress());
							if (householdContact.getHomeAddress().getPostalCode() != null) {
								householdContact.setHomeAddressIndicator(true);
							}
						}

						/**
						 * Populate home contact number in other phone and rest phone type (Work,Mobile,Employer,Friend) in Phone section with phone type as CELL.
						 */
						if (personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber() != null
						        && personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber() != null
						        && personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null) {
							if (householdContact.getOtherPhone() == null) {
								householdContact.setOtherPhone(new OtherPhone());
							}

							householdContact.getOtherPhone().setPhoneNumber(personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue());
							householdContact.getOtherPhone().setPhoneType(PhoneTypeEnum.HOME.getValue());
							if (personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID() != null) {
								householdContact.getOtherPhone().setPhoneExtension(personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().getValue());
							}
						}

						if (personContactInformationAssociationType.getContactInformation().getContactEmailID() != null) {
							homeContactEmailId = personContactInformationAssociationType.getContactInformation().getContactEmailID().getValue();
						}
					}
				} /*else if (contactCode.equals(ContactInformationCategoryCodeSimpleType.MAILING.value())) {// Setting Mailing Data
					// Address Data
					if (personContactInformationAssociationType.getContactInformation() != null) {
						if (personContactInformationAssociationType.getContactInformation().getContactMailingAddress() != null
						        && personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress() != null) {
							if (householdContact.getMailingAddress() == null) {
								householdContact.setMailingAddress(new MailingAddress());
							}

							pouplateHouseHoldContactAddress(householdContact.getMailingAddress(), personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress());
						}
					}
				}*/ else if (contactCode.equals(ContactInformationCategoryCodeSimpleType.WORK.value()) || contactCode.equals(ContactInformationCategoryCodeSimpleType.FRIEND.value())
				        || contactCode.equals(ContactInformationCategoryCodeSimpleType.EMPLOYER.value()) || contactCode.equals(ContactInformationCategoryCodeSimpleType.MOBILE.value())
				        || contactCode.equals(ContactInformationCategoryCodeSimpleType.SELF.value()) || contactCode.equals(ContactInformationCategoryCodeSimpleType.BUSINESS.value())) {
					// Phone Data
					if (personContactInformationAssociationType.getContactInformation() != null) {
						if (personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber() != null
						        && personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber() != null
						        && personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null) {
							if (householdContact.getPhone() == null) {
								householdContact.setPhone(new Phone());
							}

							householdContact.getPhone().setPhoneNumber(personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue());
							householdContact.getPhone().setPhoneType(PhoneTypeEnum.MOBILE.getValue());
							if (personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID() != null) {
								householdContact.getPhone().setPhoneExtension(personContactInformationAssociationType.getContactInformation().getContactTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().getValue());
							}
						}
					}
				}
			}
		}
		
		//setting primary contact mailing address for all dependents
	 	if(primaryAddress != null){
	 		householdContact.setMailingAddress(primaryAddress);
	 	}
		if (homeContactEmailId != null) {
			householdContact.getContactPreferences().setEmailAddress(homeContactEmailId);
		} else {
			householdContact.getContactPreferences().setEmailAddress(contactEmailId);
		}

		// Home Address Same as Mailing Address Indicator
		// [mailingAddressSameAsHomeAddressIndicator]
		householdContact.setMailingAddressSameAsHomeAddressIndicator(ReferralUtil.compareAddress(householdContact.getHomeAddress(), householdContact.getMailingAddress()));

		// Preferred Languages Here
		populatePreferredLanguage(person.getPersonAugmentation().getPersonPreferredLanguage(), householdContact.getContactPreferences());

	}

	private void populatePreferredLanguage(List<PersonLanguageType> personPreferredLanguage, ContactPreferences contactPreferences) {
		if (personPreferredLanguage != null) {
			final int lsize = ReferralUtil.listSize(personPreferredLanguage);
			for (int i = 0; i < lsize; i++) {
				if (personPreferredLanguage.get(i).getPersonSpeaksLanguageIndicator() != null && personPreferredLanguage.get(i).getPersonSpeaksLanguageIndicator().isValue()) {
					if (personPreferredLanguage.get(i).getLanguageName() != null && ReferralUtil.listSize(personPreferredLanguage.get(i).getLanguageName()) != 0 && personPreferredLanguage.get(i).getLanguageName().get(0).getValue() != null) {
						contactPreferences.setPreferredSpokenLanguage(personPreferredLanguage.get(i).getLanguageName().get(0).getValue());
					}
				}
				if (personPreferredLanguage.get(i).getPersonWritesLanguageIndicator() != null && personPreferredLanguage.get(i).getPersonWritesLanguageIndicator().isValue()) {
					if (personPreferredLanguage.get(i).getLanguageName() != null && ReferralUtil.listSize(personPreferredLanguage.get(i).getLanguageName()) != 0 && personPreferredLanguage.get(i).getLanguageName().get(0).getValue() != null) {
						contactPreferences.setPreferredWrittenLanguage(personPreferredLanguage.get(i).getLanguageName().get(0).getValue());
					}
				}
			}
		}

	}

	private void pouplateHouseHoldContactAddress(Address address, StructuredAddressType structuredAddress) {

		if (structuredAddress.getLocationStreet() != null && structuredAddress.getLocationStreet().getStreetFullText() != null) {
			address.setStreetAddress1(structuredAddress.getLocationStreet().getStreetFullText().getValue());
		}

		if (structuredAddress.getAddressSecondaryUnitText() != null) {
			address.setStreetAddress2(structuredAddress.getAddressSecondaryUnitText().getValue());
		}

		if (structuredAddress.getLocationCityName() != null) {
			address.setCity(structuredAddress.getLocationCityName().getValue());
		}

		if (structuredAddress.getLocationStateUSPostalServiceCode() != null) {
			address.setState(ReferralUtil.enumValueAsString(StateCodeEnum.class, structuredAddress.getLocationStateUSPostalServiceCode().getValue().value()));
		}

		if (structuredAddress.getLocationPostalCode() != null) {
			address.setPostalCode(structuredAddress.getLocationPostalCode().getValue());
		}

		if (structuredAddress.getLocationCountyName() != null) {
			address.setCounty(structuredAddress.getLocationCountyName().getValue());
		}

		if (structuredAddress.getLocationCountyCode() != null) {
			address.setPrimaryAddressCountyFipsCode(structuredAddress.getLocationCountyCode().getValue());
		}

		// Added for HIX-43201 & HIX-48472
		populateCountyNameAndStateFIPS(address);

	}

	private void populateCountyNameAndStateFIPS(Address address) {

		if (address != null) {
			String countyCode = address.getPrimaryAddressCountyFipsCode();

			String zip = address.getPostalCode();
			String state = address.getState();
			String county = address.getCounty();

			if (ReferralUtil.isNotNullAndEmpty(countyCode) && ReferralUtil.isNotNullAndEmpty(zip) && ReferralUtil.isNotNullAndEmpty(state)) {
				if (county == null) {
					String countyName = zipCodeService.findCountyNameByZipStateAndCountyCD(zip, state, countyCode);
					address.setCounty(countyName);
				}
				// Added for HIX-48472
				String stateFIPS = zipCodeService.findStateFIPSByZipStateAndCountyCD(zip, state, countyCode);
				address.setPrimaryAddressCountyFipsCode(stateFIPS + countyCode);
			}
		}

	}

	private List<String> extractInsuranceApplicantIds(AccountTransferRequestPayloadType source) {
		List<String> insuranceApplicantIdsList = new ArrayList<String>();
		final List<InsuranceApplicantType> insuranceApplicantList = source.getInsuranceApplication().getInsuranceApplicant();
		final int size = ReferralUtil.listSize(insuranceApplicantList);
		InsuranceApplicantType insuranceApplicant;
		PersonType personType;

		for (int i = 0; i < size; i++) {
			insuranceApplicant = insuranceApplicantList.get(i);
			if (insuranceApplicant.getRoleOfPersonReference() != null) {
				if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {
					personType = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
					insuranceApplicantIdsList.add(personType.getId());
				}
			}
		}

		return insuranceApplicantIdsList;
	}

	private String extractPrimaryApplicantId(AccountTransferRequestPayloadType source) {
		final PersonType primaryApplicant = (PersonType) source.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
		
		return primaryApplicant.getId();
	}
	

	private void populateBrokerFederalTaxIdNumber(AccountTransferRequestPayloadType source, SingleStreamlinedApplication singleStreamlinedApplication) {
		final AssisterType assisterType = source.getAssister();
		if (assisterType == null) {
			return;
		}

		final PersonType personType = assisterType.getRolePlayedByPerson();
		if (personType == null) {
			return;
		}

		final PersonAugmentationType personAugmentation = personType.getPersonAugmentation();
		if (personAugmentation == null) {
			return;
		}

		final List<IdentificationType> listIdentificationType = personAugmentation.getPersonIdentification();

		if (listIdentificationType == null) {
			return;
		}

		final int isize = listIdentificationType.size();
		IdentificationType temp;
		for (int i = 0; i < isize; i++) {
			temp = listIdentificationType.get(i);
			if (ReferralUtil.isValidString(ReferralUtil.getValue(temp.getIdentificationCategoryText())) && temp.getIdentificationCategoryText().getValue().equals("Federally-Facilitated Marketplace")) {
				if (singleStreamlinedApplication.getBroker() == null) {
					singleStreamlinedApplication.setBroker(new Broker());
				}
				singleStreamlinedApplication.getBroker().setBrokerFederalTaxIdNumber(ReferralUtil.getValue(temp.getIdentificationID()));
			}
		}
	}

	private void populateAuthorizedRepresentativeIndicator(SingleStreamlinedApplication singleStreamlinedApplication) {
		// Modified condition for HIX-50557
		if (singleStreamlinedApplication.getAuthorizedRepresentative() != null && singleStreamlinedApplication.getAuthorizedRepresentative().getName() != null
		        && singleStreamlinedApplication.getAuthorizedRepresentative().getName().getFirstName() != null) {
			singleStreamlinedApplication.setAuthorizedRepresentativeIndicator(true);
		} else {
			singleStreamlinedApplication.setAuthorizedRepresentativeIndicator(false);
		}
	}

	/*private boolean isStateId() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return ("ID".equals(stateCode) || "CA".equals(stateCode));
	}*/

	@SuppressWarnings("unused")
	private boolean isStateNm() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "NM".equals(stateCode);
	}

	private void populateApplicationGuid(AccountTransferRequestPayloadType source, SingleStreamlinedApplication singleStreamlinedApplication) {
		singleStreamlinedApplication.setApplicationGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICATION_GUID_KEY));
	}

	private void populateExternalApplicationId(AccountTransferRequestPayloadType source, SingleStreamlinedApplication singleStreamlinedApplication) {
		if (source.getInsuranceApplication().getApplicationIdentification() != null && ReferralUtil.listSize(source.getInsuranceApplication().getApplicationIdentification()) != 0) {
			if(ReferralUtil.listSize(source.getInsuranceApplication().getApplicationIdentification()) >1) {
				//HIX-109638
				String externalAppID = null;
				for(IdentificationType appIdentification:source.getInsuranceApplication().getApplicationIdentification()) {
					externalAppID = appIdentification.getIdentificationID().getValue();
					if(ReferralConstants.EXT_APPLICATION_ID.equalsIgnoreCase(appIdentification.getIdentificationCategoryText().getValue())) {
						break;
					}
				}
				singleStreamlinedApplication.setExternalApplicationId(externalAppID);
				
			}else if (source.getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID() != null) {
				singleStreamlinedApplication.setExternalApplicationId(source.getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID().getValue());
			}
			
		}
	}

	@Override
	public String transformSsapToJson(SingleStreamlinedApplication singleStreamlinedApplication) {
		String sReturn = null;
		LOGGER.info("Transforming String to Json Starts");
		sReturn = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
		LOGGER.info("Transforming String to Json Ends");
		return sReturn;
	}

	@Override
	public void buildSsapBeanForJson(SingleStreamlinedApplication singleStreamlinedApplication) {
		LOGGER.info("Building SingleStreamlinedApplication object for Json Specific Changes Starts");
		ssapJsonBuilder.buildForJson(singleStreamlinedApplication);
		LOGGER.info("Building SingleStreamlinedApplication object for Json Specific Changes Ends");
	}
	
	
	private void populatePrimaryMailingAddress(PersonType person, Address primayAddress) {
		 if (person.getPersonAugmentation() == null) {
			 			return;
		 }

		 List<PersonContactInformationAssociationType> listPersonContactInformationAssociation = person.getPersonAugmentation().getPersonContactInformationAssociation();
		 final int size = ReferralUtil.listSize(listPersonContactInformationAssociation);
		 	 	 		                
		 PersonContactInformationAssociationType personContactInformationAssociationType = null;
		 String contactCode;
		 	 	 		                
		 	 	 		                
		 for (int i = 0; i < size; i++) {
		 	 personContactInformationAssociationType = listPersonContactInformationAssociation.get(i);
		 	 	 		 
		 	 if (personContactInformationAssociationType.getContactInformationCategoryCode() != null && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() != null) {
		 	 	contactCode = ReferralUtil.validString(personContactInformationAssociationType.getContactInformationCategoryCode().getValue().value());
		 	 	 		
		 	 	if (contactCode.equals(ContactInformationCategoryCodeSimpleType.MAILING.value())) {// Setting Mailing Data
		 	 	 	 // Address Data
		 	 	 	if (personContactInformationAssociationType.getContactInformation() != null) {
		 	 	 		   if (personContactInformationAssociationType.getContactInformation().getContactMailingAddress() != null
		 	 	 		         && personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress() != null) {
		 	 	 	                                                        
		 	 	 		         pouplateHouseHoldContactAddress(primayAddress, personContactInformationAssociationType.getContactInformation().getContactMailingAddress().getStructuredAddress());
		 	 	 		         break;
		 	 	 		    }
		 	 	 	}
		 	 	} 
		 	 }
		}
		 	 	 		
	}
	
	
		
	@SuppressWarnings("unchecked")
	private void populateTaxHousehold(AccountTransferRequestPayloadType source, HouseholdMember responsibleHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
		final Map<String, Integer> map = populateRefIdMap(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember());

		final int size = ReferralUtil.listSize(source.getTaxReturn());
		if (size != 0) {
			final TaxReturnType taxReturnType = source.getTaxReturn().get(0);
			if (taxReturnType.getTaxHousehold() == null) {
				return;
			}
			final TaxHouseholdType taxHouseholdType = taxReturnType.getTaxHousehold();

			populateHouseholdIncome(singleStreamlinedApplication.getTaxHousehold().get(0), taxHouseholdType);

			final List<?> taxFilerList = taxHouseholdType.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent();
			final int tfSize = ReferralUtil.listSize(taxFilerList);
			JAXBElement<?> taxFilerType = null;
			boolean blnHasSpouseTaxFiler = false;

			// Populate Primary Taxfiler & Dependents
			for (int i = 0; i < tfSize; i++) {
				taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
				if (taxFilerType != null) {
					if (taxFilerType.getName().getLocalPart().equals(PRIMARYTAXFILER)) {
						populatePrimaryTaxFiler(singleStreamlinedApplication, map, (JAXBElement<TaxFilerType>) taxFilerType);
					} else if (taxFilerType.getName().getLocalPart().equals(TAXDEPENDENT)) {
						populateTaxDependent(singleStreamlinedApplication, map, (JAXBElement<TaxDependentType>) taxFilerType);
					}
					if (taxFilerType.getName().getLocalPart().equals(SPOUSETAXFILER)) {
						blnHasSpouseTaxFiler = true;
					}
				}
			}

			if (blnHasSpouseTaxFiler) {
				// Populate Spouse Tax Filer after Primary Done
				for (int i = 0; i < tfSize; i++) {
					taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
					if (taxFilerType != null) {
						if (taxFilerType.getName().getLocalPart().equals(SPOUSETAXFILER)) {
							populateSpouseTaxFiler(singleStreamlinedApplication, map, (JAXBElement<TaxFilerType>) taxFilerType);
						}
					}
				}
			}
		}
	}
	
	@Override
	public long compareAndUpdateStatus(AccountTransferRequestDTO accountTransferRequestDTO) {
		SsapApplication ssapApplication = null;
		long applicationID = 0l;
		if (accountTransferRequestDTO.isLCE()) {
			List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(accountTransferRequestDTO.getCaseNumber());
            if (ssapApplications != null && ssapApplications.size() > 0) {
				ssapApplication = ssapApplications.get(0);
			} else {
				LOGGER.debug("No current applications");
				throw new GIRuntimeException("No ssap application found!");
			}
			
			populateLceComparationDetailsDto(accountTransferRequestDTO);
		} 
		//It seems not needed for this flow
		/*else if (accountTransferRequestDTO.getCompareToApplicationId() != ReferralConstants.NONE) {
			populateComparationDetails(singleStreamlinedApplication, accountTransferRequestDTO.getCompareToApplicationId());
		}*/
		
		LOGGER.info("Update applicant Status");
		if(ssapApplication!=null) {
			applicationID = ssapApplication.getId();
		}
		return applicationID;
	}
	
	private void populateLceComparationDetailsDto(AccountTransferRequestDTO accountTransferRequestDTO) {
		referralCompareService.executeCompareFromDto(accountTransferRequestDTO);
	}

	@Override
	public boolean isATRequestFromFFM(AccountTransferRequestPayloadType accountTransferRequest) {
		return false;
	}

	@Override
	public boolean updateIncomes(AccountTransferRequestPayloadType source,
			SingleStreamlinedApplication singleStreamlinedApplication) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SsapApplicantPersonType determineApplicantPersonType(
			String primaryApplicantId, String primaryTaxFilerId,
			String personId, boolean isApplyingForCoverage) {
		return null;
	}

}

