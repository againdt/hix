<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>Get Insured Batch Admin</title>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
</head>
<body id="user-login">
<br><br><br><br><br><br>

			<form class="form-horizontal box-loose" id="loginform" name="loginform" action="<c:url value='/j_spring_security_check'/>" method="post">
				
					<c:if test="${not empty authfailed}">
						<div align="center" class="errorblock alert alert-info">
							<centrer><p>Your login attempt was not successful, try again</p></centrer>
							<!--  <p>Reason: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}.</p>-->
						</div>
					</c:if>

					<c:if test="${not empty authRoleFailed}">
						<div align="center" class="errorblock alert alert-info">
							<centrer><p>You are not authorized to view this application, try again</p></centrer>
							<!--  <p>Reason: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}.</p>-->
						</div>
					</c:if>
					 <div align="center">
					<center>
					<table border="1" cellspacing="0" cellpadding="3" width="20%" >
					<tr><td>
					<div align="center">
					  <center>
					<table border="0" cellpadding="5" cellspacing="0">
					  <tr>
						<td colspan="2">
						  <p align="center"><font face="Arial" size="2"><b>Get Insured Batch
						  Admin</b></font></td>
					  </tr>
					  <tr>
						<td colspan="2"></td>
					  </tr>
					  <tr>
						<td><font size="2" face="Arial">User Name</font></td>
						<td><font face="Arial"><input type="text" name="j_username" id="j_username" value="" size="20" onkeypress="resetErrorMsg(this)"></font></td>
					  </tr>
					  <tr>
						<td><font size="2" face="Arial">Password</font></td>
						<td><font face="Arial"><input type="password" name="j_password" id="j_password" value="" size="20" onkeypress="resetErrorMsg(this)"></font></td>
					  </tr>
					  <tr>
						<td></td>
					  </center>
					  <td>
						<p align="center"><font size="2" face="Arial"><input type="submit" value="Login" name="B1"></font></td>
					</tr>
					</table>
					  </center>
					</div>
					 </td></tr>
					</table>

				  </div>
					
			</form>

	<link href="<c:url value="/resources/js/ghixcustom.js" />" rel="stylesheet"  type="text/css" />
	
	<script type="text/javascript">
	function validateEmail(sEmail) {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(sEmail)) {
		    return true;
		}
		else {
		    return false;
		}
	}
	function resetErrorMsg(element) {
		var elementId = element.id;
		
		$("#" + elementId + "_error").html('');
	}
	
	jQuery.validator.addMethod("validateLogin", function(value,
			element, param) {
		 var sEmail = $('#j_username').val();
		 if (!validateEmail(sEmail)){
			 return false;
		 }
		 return true;
	});
	var validator = $("#loginform")
	.validate({
		rules:{
			j_username:{
				required : true,
				validateLogin:true
			},
			j_password:{
				required : true
			}
		},
		messages : {
			j_username:{
				required : "<span> <em class='excl'>!</em>Please enter email address.</span>",
				validateLogin:"<span> <em class='excl'>!</em>Please enter valid email address.</span>"
			},
			j_password:{
				required : "<span> <em class='excl'>!</em>Please enter password.</span>"
			}
		},
		onkeyup : false,
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId+ "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	});
	
		
	</script>
</body>
</html>