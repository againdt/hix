package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

/**
 * Verification Processor class is used to return SSAP Application ID to Writer class.
 * 
 * @since August 12, 2019
 */
//@Component("ssapVerificationProcessor") TODO
public class VerificationProcessor implements ItemProcessor<Long, Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationProcessor.class);

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		}

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
			}
		}
	}

	@Override
	public Long process(Long processSsapApplicationID) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Returning SSAP Application ID : {}", processSsapApplicationID);
		}
		return processSsapApplicationID;
	}
}
