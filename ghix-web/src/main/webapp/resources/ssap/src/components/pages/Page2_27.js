import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';


// Constants

import INPUT_TYPES from '../../constants/inputTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';




/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Preganant Details ' Page
 *
 */
const Page2_27= props => {

    const {
        data,
        form: {
            Form: {
                isFormSubmitted,
                isFormOnTopLevelValid
            }
        },
        householdMembers,
        isFirstVisit
    } = props;

    // Local State of Functional Component

    const [localState, setLocalState] = useState({
        noneOfTheAbove: isFirstVisit ? false : householdMembers.every(member => !member.fullTimeStudent)
    });

    const { noneOfTheAbove } = localState;

    const onFieldChange_member = (newData, indexInArray) => {

        const newState = {...data };
        const {
            fullTimeStudent
        } = newData;
        newState.singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[indexInArray]
            .fullTimeStudent = fullTimeStudent;

        props.actions.changeData(newState);
        if(newData.fullTimeStudent) {
            setLocalState(state => ({
                ...state,
                noneOfTheAbove: false
            }));
        }

    };

    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <americanIndianAlaskaNative> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = newData => {

        setLocalState(state => ({
            ...state,
            ...newData
        }));

        const newState = {...data };

        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member.fullTimeStudent = false;
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
    };

    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = (householdMembers) => {

        const combinedMembers = [];
        const {  noneOfTheAbove } = localState;
        householdMembers.forEach((member, i) => {
            const {
                name,
                fullTimeStudent,
                dateOfBirth,

            } = member;
            const memberName = `${normalizeNameOfPerson(name)}`;
            const age = moment().diff(dateOfBirth, 'years');


            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    <ul className="usa-unstyled-list">
                        {
                            age >=18 && age <=22 &&
                            <li>
                                <UniversalInput
                                    label={{
                                        text: `${memberName}`
                                    }}
                                    inputData={{
                                        isInputRequired: false,
                                        type: INPUT_TYPES.CHECKBOX,
                                        name: 'fullTimeStudent',
                                        classes: [''],
                                        checked: fullTimeStudent,
                                        value: fullTimeStudent,
                                        currentData: member,
                                        fields: [],
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_member
                                    }}
                                    indexInArray={i}
                                    errorClasses={['margin-l-0']}
                                />
                            </li>
                        }
                    </ul>
                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    const checkTopLevelValidation = () => {
        return noneOfTheAbove || !!householdMembers.find(member => member.fullTimeStudent);
    };

    return (
        <Fragment>
            <div id="fhMoreInfo" className="fade-in">
                <div className="subsection">
                    <Form topLevelValidity={checkTopLevelValidation()}>
                        <p className="required-true">
                            Are any of these people full time students?
                        </p>
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isFormOnTopLevelValid}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        {
                            getHouseholdMembers(householdMembers)
                        }
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,
    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_27.defaultProps = {
    name: 'Page2_27'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_27);
