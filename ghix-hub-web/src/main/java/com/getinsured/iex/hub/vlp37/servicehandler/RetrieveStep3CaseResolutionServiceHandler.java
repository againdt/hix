package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionResponseType;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep3CaseResolutionRequestTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep3CaseResolutionResponseSetTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep3CaseResolutionResponseTypeWrapper;



@SuppressWarnings("rawtypes")
public class RetrieveStep3CaseResolutionServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RetrieveStep3CaseResolutionServiceHandler.class);
	private Jaxb2Marshaller marshaller;
	
	public Object getRequestpayLoad() throws HubServiceException{
		
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				
				
				processJSONObject(payloadObj);
				
				RetrieveStep3CaseResolutionRequestTypeWrapper obj = (RetrieveStep3CaseResolutionRequestTypeWrapper)getNamespaceObjects().get(RetrieveStep3CaseResolutionRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("required VLP Request parameters not available");
				}
				
				RetrieveStep3CaseResolutionRequestType reqObj = obj.getRequest();
				setJsonInput(obj.toJSONString());
				return reqObj;
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
			
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}

	/**
	 * Method to handle the response
	 * 
	 * author - Nikhil Talreja
	 * since - 14-Jan-2014
	 */
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		String message = "";
		try{
			
			JAXBElement<RetrieveStep3CaseResolutionResponseType> resp= (JAXBElement<RetrieveStep3CaseResolutionResponseType>)responseObject;
			RetrieveStep3CaseResolutionResponseType payload = resp.getValue();
			RetrieveStep3CaseResolutionResponseTypeWrapper response = new RetrieveStep3CaseResolutionResponseTypeWrapper(payload);
			RetrieveStep3CaseResolutionResponseSetTypeWrapper individualResponse = null; 
			
			if(response != null && response.getRetrieveStep3CaseResolutionResponseSet() != null){	
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				/*
				 * To set Response code for VLP, we will use the first response in the set of individual responses
				 * Since VLP service will always be called separately for each applicant, we will receive at most
				 * once response 
				 */
				individualResponse = response.getRetrieveStep3CaseResolutionResponseSet();
				if(response != null && response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
				/*
				 * HIX-34256 - Case needs to be closed as soon as we get step 1 response
				 */
				this.requestStatus = RequestStatus.PENDING_CLOSE;
			}
			
			getResponseData(individualResponse);
			
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+ RetrieveStep3CaseResolutionResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	/**
	 * Method to get Case Number from HUB response
	
	 */
	private void getResponseData(RetrieveStep3CaseResolutionResponseSetTypeWrapper individualResponse){
		
		//Case number
		if(individualResponse != null ){
			this.getResponseContext().put("HUB_CASE_NUMBER", individualResponse.getCaseNumber());
			
		/*	this.getResponseContext().put("NonCitLastName", individualResponse.getNonCitLastName());
			this.getResponseContext().put("NonCitFirstName", individualResponse.getNonCitFirstName());
			this.getResponseContext().put("NonCitMiddleName", individualResponse.getNonCitMiddleName());
			this.getResponseContext().put("NonCitBirthDate", individualResponse.getNonCitBirthDate());
			this.getResponseContext().put("NonCitEntryDate", individualResponse.getNonCitEntryDate());
			this.getResponseContext().put("NonCitAdmittedToDate", individualResponse.getNonCitAdmittedToDate());
			this.getResponseContext().put("NonCitAdmittedToText", individualResponse.getNonCitAdmittedToText());
			this.getResponseContext().put("NonCitCountryBirthCd", individualResponse.getNonCitCountryBirthCd());
			this.getResponseContext().put("NonCitCountryCitCd", individualResponse.getNonCitCountryCitCd());
			this.getResponseContext().put("NonCitCoaCode", individualResponse.getNonCitCoaCode());
			this.getResponseContext().put("NonCitEadsExpireDate", individualResponse.getNonCitEadsExpireDate());
			this.getResponseContext().put("EligStatementCd", individualResponse.getEligStatementCd());
			this.getResponseContext().put("EligStatementTxt", individualResponse.getEligStatementTxt());
			this.getResponseContext().put("WebServSftwrVer", individualResponse.getWebServSftwrVer());
			this.getResponseContext().put("GrantDate", individualResponse.getGrantDate());
			this.getResponseContext().put("GrantDateReasonCd", individualResponse.getGrantDateReasonCd());
			this.getResponseContext().put("ArrayOfSponsorshipData", individualResponse.getArrayOfSponsorshipData());
			this.getResponseContext().put("SponsorshipReasonCd", individualResponse.getSponsorshipReasonCd());
			this.getResponseContext().put("PhotoBinaryAttachment", individualResponse.getPhotoBinaryAttachment());
			this.getResponseContext().put("FiveYearBarApplyCode", individualResponse.getFiveYearBarApplyCode());
			this.getResponseContext().put("QualifiedNonCitizenCode", individualResponse.getQualifiedNonCitizenCode());
			this.getResponseContext().put("FiveYearBarMetCode", individualResponse.getFiveYearBarMetCode());
			this.getResponseContext().put("USCitizenCode", individualResponse.getUsCitizenCode()); */
		}
		
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H101";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		//com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
