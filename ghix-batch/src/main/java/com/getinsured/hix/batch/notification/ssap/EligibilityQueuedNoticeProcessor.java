package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("EligibilityQueuedNoticeProcessor")
@Transactional
public class EligibilityQueuedNoticeProcessor implements QueuedNoticeProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(EligibilityQueuedNoticeProcessor.class);
    private static final List<String> acceptedStatuses = Arrays.asList("ER", "PN", "EN", "CL");

    @Autowired
    private SsapApplicationRepository ssapApplicationRepository;

    @Autowired
    RestTemplate restTemplate;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Override
    public boolean verify(NoticeQueued noticeQueued) {
        try {
            Long ssapAppID = noticeQueued.getColumnValue();
            if (ssapAppID != null) {
                SsapApplication currentApplication = ssapApplicationRepository.findOne(ssapAppID);
                if (currentApplication == null || currentApplication.getCmrHouseoldId() == null ||
                        !acceptedStatuses.contains(currentApplication.getApplicationStatus())) {
                    return false;
                }

                LOGGER.debug("verify::validating application cmr household {} with app id {}", currentApplication.getCmrHouseoldId(), ssapAppID);
                return validApplication(currentApplication.getCmrHouseoldId(), ssapAppID);
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("verify::error processing notice queued", ex);
        }

        return false;
    }

    private boolean validApplication(BigDecimal cmrHouseholdID, Long ssapAppId) {
        try {
            LOGGER.debug("validApplication::building query to validate app id {} for household {}", cmrHouseholdID, ssapAppId);
            Map<String, Object> parameterMap = new HashMap<>();
            parameterMap.put("acceptedStatuses", acceptedStatuses);
            parameterMap.put("cmrHouseholdID", cmrHouseholdID);
            parameterMap.put("ssapAppId", ssapAppId);

            String mostRecentApplication = "SELECT id FROM ssap_applications where application_status in (:acceptedStatuses) " +
                    "AND id = :ssapAppId AND creation_timestamp = (SELECT MAX(creation_timestamp) " +
                    "FROM ssap_applications WHERE cmr_houseold_id = :cmrHouseholdID);";

            return buildAndExecuteQuery(mostRecentApplication, parameterMap).size() > 0;
        } catch (Exception ex) {
            LOGGER.error("validApplication::error querying most recent app for validation", ex);
        }

        return false;
    }

    private List buildAndExecuteQuery(String queryString, Map<String, Object> parameterMap) {
        EntityManager entityManager = null;
        List resultList = null;

        try {
            LOGGER.debug("buildAndExecuteQuery::queryString before replacing {}", queryString);

            entityManager = emf.createEntityManager();
            Query query = entityManager.createNativeQuery(queryString);
            if(parameterMap != null) {
                parameterMap.forEach(query::setParameter);
            }

            LOGGER.debug("buildAndExecuteQuery::queryString {}", query.toString());

            resultList = query.getResultList();
        } catch(Exception ex) {
            LOGGER.error("buildAndExecuteQuery:error executing query", ex);
        } finally {
            // close the entity manager
            if(entityManager !=null && entityManager.isOpen()){
                entityManager.clear();
                entityManager.close();
            }
        }

        return resultList;
    }

    @Override
    public boolean process(NoticeQueued noticeQueued) {
        try {
            if(noticeQueued != null && noticeQueued.getColumnValue() != null) {
                LOGGER.debug("process::initiating eligibility for id {} from column name {} table name {}",
                        noticeQueued.getColumnValue(), noticeQueued.getColumnName(), noticeQueued.getColumnName());
                Long ssapAppID = noticeQueued.getColumnValue();
                if (ssapAppID != null) {
                    SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapAppID);
                    if (ssapApplication != null && acceptedStatuses.contains(ssapApplication.getApplicationStatus())) {
                        ResponseEntity<String> response = restTemplate.exchange(
                                GhixEndPoints.EligibilityEndPoints.UNIVERSAL_ELIGIBILITY_NOTIFICATION_URL + ssapApplication.getCaseNumber(),
                                HttpMethod.GET,
                                null,
                                String.class);
                        LOGGER.debug("process::response {}", response.getBody());
                        return response.getStatusCode().equals(HttpStatus.OK);
                    }
                }
            }
        } catch (RestClientException e) {
            LOGGER.error("process::Exception occurred while processing notice queued", e);
        } catch (Exception ex) {
            LOGGER.error("process::error processing notice queued", ex);
        }

        LOGGER.debug("process::failure processing noticeQueued");
        return false;
    }
}
