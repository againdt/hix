package com.getinsured.hix.tkm.service;

import java.util.List;

import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.model.TkmQuickActionDto;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmUserDTO;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.getinsured.hix.platform.util.exception.GIException;

public interface TkmTicketTaskService {
	/**
	 * @author Kuldeep
	 * @since 02nd May 2013
	 * @param ticketId
	 * @return This method will returns the list of TkmTicketTasks based on
	 *         parameter ticketId
	 * @throws Exception 
	 */
	List<TkmTicketTasks> getTkmTicketTasksList(int ticketId) throws TKMException;
	
	List<HistoryDto> loadTicketAndTaskHistory(Integer ticketId, boolean isDetailedView)throws TKMException;

	/**
	 * 
	 * @param tkmTicketTasks
	 * @return This method will return the newly added TkmTicketTasks object
	 */
	TkmTicketTasks addNewTicketTask(TkmTicketTasks tkmTicketTasks);

	boolean claimTicket(Integer taskId, Integer userId);

	boolean completeTask(Integer taskId, String comments);

	TkmTicketTasks findByTaskId(Integer taskId) throws TKMException;

	boolean updateTasks(List<TkmTicketTasks> tasks);

	List<HistoryDto> loadTicketStatusHistory(Integer ticketId, boolean isDetailedView) throws TKMException;

	List<TkmTicketTasks> getProcessInstanceTaskList(String processInstanceId);

	Integer getTaskTicketId(Integer taskId);
	
	TkmTicketTasks findTkmTasksByActivitiTaskId(String activitiTaskId);
	
	long getTicketTaskCount();
	
	List<TkmTicketTasks> getTicketTaskByAssignee(String assigneeName) throws TKMException;

	TkmTicketTasks getTicketByTaskIdAndActivitiId(String processInstanceId,
			String activitiTaskId) throws TKMException;

	TkmQuickActionDto getDetailsForQuickActionBar(Integer ticketId);

	List<TkmUserDTO> geTaskUserInfoByAssignee(String assigneeName) throws TKMException ;

}
