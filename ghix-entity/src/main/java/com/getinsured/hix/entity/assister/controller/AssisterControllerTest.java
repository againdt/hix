package com.getinsured.hix.entity.assister.controller;

import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.exception.GIException;

public class AssisterControllerTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterControllerTest.class);

	private RestTemplate restTemplate;

	private static final String REST_SERVICE_URL_FOR_DELEGATE_CODE = "http://localhost:8080/ghix-entity/entity/assister/testStub";

	public void testRetrieveEmployerBOBDetails() throws GIException {

		FileSystemXmlApplicationContext applicationContext = new FileSystemXmlApplicationContext(
				"C:\\Users\\ari_g\\Desktop\\RestTemplate\\test.xml");

		restTemplate = (RestTemplate) applicationContext.getBean("restTemplate");

		try {
			String wsResponse = restTemplate.getForObject(REST_SERVICE_URL_FOR_DELEGATE_CODE, String.class);

			LOGGER.info("Rest call ended.");
		}
		catch (Exception exception) {
			LOGGER.error("Excpetion while testing assister rest stub : ", exception);
		}
	}

}
