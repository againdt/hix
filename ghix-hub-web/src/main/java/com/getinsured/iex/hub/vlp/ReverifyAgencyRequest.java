package com.getinsured.iex.hub.vlp;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp.wrapper.ReverifyAgency3InitVerifRequestTypeWrapper;

/**
 * Class to test ReverifyAgencyRequest
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 */
public final class ReverifyAgencyRequest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReverifyAgencyRequest.class);
	
	private ReverifyAgencyRequest(){
		
	}
	
	public static void testReverifyRequest() {
		
		try{
			HubServiceBridge reverifyServiceBridge = HubServiceBridge.getHubServiceBridge("ReverifyAgencyRequest");  
			
			reverifyServiceBridge.getServiceHandler().setJsonInput("{\"AlienNumber\":\"321654987\",\"CategoryCode\":\"C49\",\"RequestedCoverageStartDate\":\"2012-01-29\",\"I94Number\":null,\"SEVISID\":\"0000110429\",\"ReceiptNumber\":\"AAA1200850001\",\"LastName\":\"Villa\",\"NaturalizationNumber\":null,\"CitizenshipNumber\":\"552559412\",\"DateOfBirth\":\"1976-05-05\",\"PassportCountry\":{\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"59N199754\"},\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"FirstName\":\"Rosa\",\"RequesterCommentsForHub\":\"Resubmitting the application\",\"CaseNumber\":\"9901009990000CC\",\"VisaNumber\":\"76254237\"}");
			Object payload = reverifyServiceBridge.getServiceHandler().getRequestpayLoad();
					
			if(payload != null){
				JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav");
				Marshaller m = jc.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new VLPNamespacePrefixMapper());
				//m.marshal(payload, System.out);
			}
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
		
	}
	
	public static void testWrappers() throws HubServiceException{
		
		ReverifyAgency3InitVerifRequestTypeWrapper request = ReverifyAgency3InitVerifRequestTypeWrapper.fromJsonString("{\"caseNumber\":\"0000000000000HY\",\"passportCountry\":{\"passportNumber\":\"J123456\",\"countryOfIssuance\":\"USA\"}}");
		LOGGER.debug(request.toJSONString());
		
	}
	
	public static void main(String[] args) {
		testReverifyRequest();
	}
	
}
