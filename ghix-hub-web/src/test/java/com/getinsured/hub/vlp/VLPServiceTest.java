package com.getinsured.hub.vlp;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hub.platform.GhixBaseTest;
import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.repository.base.GIWSPayloadRepository;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants.VLP_DHS_ID_TYPES;

public class VLPServiceTest extends GhixBaseTest {
	
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private HubResponseRepository hubResponseRepository;
	@Autowired
	private WebServiceTemplate vlpServiceTemplate;
	
	private static Map<Integer,String> payloads = new HashMap<Integer, String>();
	private static Map<Integer,String> idTypes = new HashMap<Integer, String>();
	
	private static final int NO_OF_PAYLOADS = 6; 
	
	static{
		payloads.put(1,"{\"DHSID\":{\"I571DocumentID\":{\"AlienNumber\":\"992222334\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Cody\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Blagg\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		payloads.put(2,"{\"DHSID\":{\"I551DocumentID\":{\"AlienNumber\":\"988122334\",\"ReceiptNumber\":\"azc1122334455\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		payloads.put(3,"{\"DHSID\":{\"I766DocumentID\":{\"AlienNumber\":\"993322355\",\"ReceiptNumber\":\"AXZ1124442341\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C5\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Uyemoto\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/04/1978\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":false,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Curtis\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		payloads.put(4,"{\"DHSID\":{\"I327DocumentID\":{\"AlienNumber\":\"998443112\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Steffani\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"05/05/1976\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jessica\",\"RequesterCommentsForHub\":\"request grant date\"}");
		payloads.put(5,"{\"DHSID\":{\"CertOfCitDocumentID\":{\"AlienNumber\":\"141427771\",\"CitizenshipNumber\":\"199378124\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Justin\",\"RequestSponsorDataIndicator\":true,\"DateOfBirth\":\"02/02/1979\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":false,\"FirstName\":\"Vue\",\"RequesterCommentsForHub\":\"initial verification\"}");
		payloads.put(6,"{\"DHSID\":{\"I94UnexpForeignPassportDocumentID\":{\"I94Number\":\"91402404792\",\"VisaNumber\":\"33421578\",\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"334908756\",\"SEVISID\":\"9889334221\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"Comments\":null,\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Stockman\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"05/05/1976\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Joseph\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		
		idTypes.put(1,VLP_DHS_ID_TYPES.I571.toString());
		idTypes.put(2,VLP_DHS_ID_TYPES.I551.toString());
		idTypes.put(3,VLP_DHS_ID_TYPES.I766.toString());
		idTypes.put(4,VLP_DHS_ID_TYPES.I327.toString());
		idTypes.put(5,VLP_DHS_ID_TYPES.CertOfCit.toString());
		idTypes.put(6,VLP_DHS_ID_TYPES.I94UnexpForeignPassport.toString());
	}
	
	@Test
	public void testPayloads() throws BridgeException, HubServiceException, HubMappingException{
		HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VerifyLawfulPresence");
		ServiceHandler handler = vlpServiceBridge.getServiceHandler();
		Map<String, Object> context = new HashMap<String, Object>();
		
		handler.setJsonInput("{\"DHSID\":{\"I20DocumentID\":{\"I94Number\":\"99222233334\",\"PassportCountry\":{\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"59N199754\"},\"SEVISID\":\"9889334221\", \"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		context.put("objectIdentifier",VLP_DHS_ID_TYPES.I20.toString());
		handler.setContext(context);
		handler.getRequestpayLoad();
		
		handler.setJsonInput("{\"DHSID\":{\"OtherCase1DocumentID\":{\"AlienNumber\":\"992223334\",\"PassportCountry\":{\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"59N199754\"},\"SEVISID\":\"9889334221\",\"DocDescReq\":\"test document\", \"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		context.put("objectIdentifier",VLP_DHS_ID_TYPES.OtherCase1.toString());
		handler.setContext(context);
		handler.getRequestpayLoad();
		
		handler.setJsonInput("{\"DHSID\":{\"OtherCase2DocumentID\":{\"I94Number\":\"99222223334\",\"PassportCountry\":{\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"59N199754\"},\"SEVISID\":\"9889334221\",\"DocDescReq\":\"test document\", \"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		context.put("objectIdentifier",VLP_DHS_ID_TYPES.OtherCase2.toString());
		handler.setContext(context);
		handler.getRequestpayLoad();
		
		handler.setJsonInput("{\"DHSID\":{\"TempI551DocumentID\":{\"AlienNumber\":\"992223334\",\"PassportCountry\":{\"CountryOfIssuance\":\"MEXIC\",\"PassportNumber\":\"59N199754\"},\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"03/03/1979\",\"FiveYearBarApplicabilityIndicator\":true,\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
		context.put("objectIdentifier",VLP_DHS_ID_TYPES.TempI551.toString());
		handler.setContext(context);
		handler.getRequestpayLoad();
	}
	
	//@Test
	public void testVLPService()throws HubMappingException, BridgeException, HubServiceException {
		
		GIWSPayload requestRecord = null;
		GIHUBResponse hubResponse = null;
		
		HubServiceBridge bridge = HubServiceBridge.getHubServiceBridge("VerifyLawfulPresence");
		ServiceHandler handler = bridge.getServiceHandler();
		
		for(int i=1; i<=NO_OF_PAYLOADS;i++){
		
			String inputJSON = payloads.get(i);
			handler.setJsonInput(inputJSON);
			handler.getContext().put("objectIdentifier",idTypes.get(i));
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("VLP");
			requestRecord.setEndpointOperationName("VERIFY_LAWFUL_PRESENCE");
			requestRecord.setAccessIp("0.0.0.0");
			requestRecord.setStatus("PENDING");
			requestRecord.setSsapApplicationId(null);
	
			// Persist initial request
			requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);
			Assert.assertNotSame(0L, requestRecord.getId());
	
			// Creating the payload
			Object requestPayload = handler.getRequestpayLoad();
			Assert.assertNotNull(requestPayload);
	
			// Call HUB service
			long start = System.currentTimeMillis();
			Object webserviceResponse = vlpServiceTemplate.marshalSendAndReceive(requestPayload);
			long timeTaken = System.currentTimeMillis() - start;
			Assert.assertNotNull(webserviceResponse);
	
			// Save final request
			requestRecord.setEndpointUrl(vlpServiceTemplate.getDefaultUri());
			requestRecord.setRequestPayload(handler.getJsonInput());
			requestRecord.setStatus(RequestStatus.SUCCESS.getStatusCode());
			requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);
	
			// Process and save hub response
			String responseStr = handler.handleResponse(webserviceResponse);
			Assert.assertNotNull(responseStr);
			Assert.assertNotSame(responseStr, "");
			hubResponse = new GIHUBResponse();
			hubResponse.setEndPointURL(vlpServiceTemplate.getDefaultUri());
			hubResponse.setGiWspayloadId(requestRecord.getId());
			hubResponse.setClientNodeIp("0.0.0.0");
			hubResponse.setSsapApplicationId(requestRecord.getSsapApplicantId());
			hubResponse.setStatus(RequestStatus.SUCCESS.getStatusCode());
			hubResponse.setElapsedTime(timeTaken);
			hubResponse.setHubResponsePayload(responseStr);
			hubResponse.setHubResponseCode(handler.getResponseCode());
			hubResponse.setHubCaseNumber(""+ handler.getResponseContext().get("HUB_CASE_NUMBER"));
			hubResponse = hubResponseRepository.save(hubResponse);
			Assert.assertNotSame(0L, hubResponse.getId());
			Assert.assertNotNull(hubResponse.getHubResponsePayload());
			Assert.assertNotSame(true, hubResponse.getHubResponsePayload().contains("Exception"));
		}

	}
}
