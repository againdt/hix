package com.getinsured.eligibility.redetermination.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.google.gson.JsonArray;

public interface SsapCloneApplicantService {
	
	List<SsapApplicant> cloneSsapApplicants(SsapApplication currentApplication, List<RequestParamDTO> events,
			Long userId, SepResponseParamDTO sepResponseParamDTO, SsapApplication parentApplication,
			SepRequestParamDTO sepRequestParamDTO, AtomicBoolean isCloneProgramEligibility) throws Exception;
	
	Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(List<RequestParamDTO> events, JsonArray householdMembers) throws Exception;
	
	SsapApplicant saveSsapApplicant(SsapApplicant ssapApplicant);
}
