package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;

import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ReferralActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.ssap.model.SsapApplicant;

@Component
public class MedicaidHouseholdMapper {
	private static Logger lOGGER = Logger.getLogger(MedicaidHouseholdMapper.class);
	
	public MedicaidHouseholdType createMedicaidHouseholdType(Long ssapId, JSONObject householdMemberJSON, int houseHoldsSize){
		lOGGER.info("---- Inside createMedicaidHouseholdType ----");
		try {
			String applicantGuid = (String) householdMemberJSON.get("applicantGuid");
			
			MedicaidHouseholdType medicaidHouseholdType = AccountTransferUtil.insuranceApplicationObjFactory.createMedicaidHouseholdType();
			IncomeType incomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();
			
			List<JSONObject> houseHoldIncome = (List<JSONObject>) householdMemberJSON.get("incomes");
			for (Object income : houseHoldIncome) {
				HashMap jobIncome = (HashMap) income;
				
				FrequencyType frequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
				FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
				frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType(jobIncome.get("frequency").toString()));
				frequency.setFrequencyCode(frequencyCodeType);
				incomeType.setIncomeFrequency(frequency);
				
				BigDecimal amount = AccountTransferUtil.getAmount(jobIncome.get("amount"));
				if(amount.compareTo(BigDecimal.ZERO) != 0){
					amount = amount.divide(BigDecimal.valueOf(100));
				}
				AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
				amountType.setValue(amount);
				incomeType.setIncomeAmount(amountType);
			}
			
			JAXBElement<IncomeType> jaxbhouseholdincome = AccountTransferUtil.insuranceApplicationObjFactory.createHouseholdIncome(incomeType);
			final List<JAXBElement<IncomeType>> householdIncome = medicaidHouseholdType.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI();
			householdIncome.add(jaxbhouseholdincome);
			
			QuantityType quantityType = AccountTransferUtil.niemCoreFactory.createQuantityType();
		    quantityType.setValue(BigDecimal.valueOf(houseHoldsSize));
		    medicaidHouseholdType.setMedicaidHouseholdEffectivePersonQuantity(quantityType);
		    
		    String personId = householdMemberJSON.get("personId").toString();
		    medicaidHouseholdType.setId("MedicaidHousehold-"+applicantGuid+"P"+ personId);

		    return medicaidHouseholdType;
		    /*		    
			SsapApplication ssapApplication = ssapApplicationService.findById(ssapId);
			EligEngineHouseholdCompositionResponse eligEngineHouseholdCompositionResponse = ssapApplicationService.getHouseholdComposition(ssapId);
			if(eligEngineHouseholdCompositionResponse != null && eligEngineHouseholdCompositionResponse.getHouseholdIncome() > 0){
				lOGGER.info("---- Response from eligEngine Household Composition Response ----");
				 List<EligEngineHouseholdCompositionMember>  eligEngineHouseholdCompositionMemberList = eligEngineHouseholdCompositionResponse.getMembers();
				 for (EligEngineHouseholdCompositionMember eligEngineHouseholdCompositionMember : eligEngineHouseholdCompositionMemberList) {
					
					SsapApplicant ssapApplicant = ssapApplicantService.findByApplicantGuidAndSsapApplication(String.valueOf(applicantGuid), ssapApplication);
					if(applicantGuid.equals(ssapApplicant.getApplicantGuid())){
						MedicaidHouseholdType medicaidHouseholdType = AccountTransferUtil.insuranceApplicationObjFactory.createMedicaidHouseholdType();
						
						IncomeType incomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();
							
					    AmountType amount = AccountTransferUtil.niemCoreFactory.createAmountType();
					    amount.setValue(BigDecimal.valueOf( eligEngineHouseholdCompositionMember.getHouseholdIncomeMedicaid()));
					    incomeType.setIncomeAmount(amount);
					    
						List<JSONObject> houseHoldIncome = (List<JSONObject>) householdMemberJSON.get("incomes");
						//if frequency monthly then two incomeType
						//One monthly and second Annually
						for (Object incomes : houseHoldIncome) {
							HashMap jobIncome = (HashMap) incomes;
							FrequencyType frequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
							FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
							frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType( CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,jobIncome.get("frequency").toString())  ));
							frequency.setFrequencyCode(frequencyCodeType);
							incomeType.setIncomeFrequency(frequency);
						}
						JAXBElement<IncomeType> jaxbhouseholdincome = AccountTransferUtil.insuranceApplicationObjFactory.createHouseholdIncome(incomeType);
						final List<JAXBElement<IncomeType>> householdIncome = medicaidHouseholdType.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI();
						householdIncome.add(jaxbhouseholdincome);

						QuantityType quantityType = AccountTransferUtil.niemCoreFactory.createQuantityType();
					    quantityType.setValue(BigDecimal.valueOf(eligEngineHouseholdCompositionMember.getHouseholdSizeMedicaid()));
					    medicaidHouseholdType.setMedicaidHouseholdEffectivePersonQuantity(quantityType);
					    
					    String personId = householdMemberJSON.get("personId").toString();
					    medicaidHouseholdType.setId("MedicaidHousehold-"+applicantGuid+"P"+ personId);
			
					    return medicaidHouseholdType;
					}
				}
				}*/
		} catch (Exception e) {
			lOGGER.error("Error while creating medicaid household type "+e.getMessage());
			return null;
		}
	}
	
}
