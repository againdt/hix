(function (module) {
    'use strict';

    module.controller('DoctorsSidebarController', ['Doctors', '$scope', function (Doctors, $scope) {
        var ds = this;
        // ds.doctors = {};
        ds.getDoctors = getDoctors;
        ds.getStart = getStart;
        ds.onClick = onClick;

        function getDoctors () {
            return Doctors.getDoctors();
        }

        function getStart() {
            return (Doctors.currentPage() - 1) * Doctors.pageSizeRange() + 1;
        }

        function onClick (doctor, index) {
            $scope.$emit('needOpenInfowindow', {
                index: index,
                doctor: doctor
            });
        }
    }]);

    module.directive('doctorsSidebar', ['Doctors', function (Doctors) {
        return {
            restrict: 'E',
            controller: 'DoctorsSidebarController',
            controllerAs: 'ds',
            templateUrl: 'doctorsSidebar.html',
            link: link
        };

        /////////////

        function link (scope, elem, attrs, ctrl) {
            stopOuterElementScroll();
        }
        // stop the outer element scroll when the mouse over the popover div
        function stopOuterElementScroll() {
            $('body').on('mousewheel DOMMouseScroll', '.provider-map .doctor-sb', function(e) {
                var scrollTo = null;

                if (e.type == 'mousewheel') {
                    scrollTo = (e.originalEvent.wheelDelta * -1);
                } else if (e.type == 'DOMMouseScroll') {
                    scrollTo = 40 * e.originalEvent.detail;
                }

                if (scrollTo) {
                    e.preventDefault();
                    $(this).scrollTop(scrollTo + $(this).scrollTop());
                }
            });
        }
    }]);

}(angular.module('ProviderMap.Directive.doctorsSidebar', [
  'ProviderMap.Factory.doctors'
])));