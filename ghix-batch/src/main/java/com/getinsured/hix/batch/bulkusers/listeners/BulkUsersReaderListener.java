package com.getinsured.hix.batch.bulkusers.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.identity.provision.UserRequest;

public class BulkUsersReaderListener implements ItemReadListener<UserRequest>,StepExecutionListener{

	private static final Logger logger = LoggerFactory.getLogger(BulkUsersReaderListener.class);
	private StepExecution stepExecution;
	
	@Override
	public void beforeRead() {
	}

	@Override
	public void afterRead(UserRequest item) {
	}

	@Override
	public void onReadError(Exception exception) {
		String jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
        String exceptionAsString = CommonUtil.convertExceptionToString(exception);
        logger.error("onReadError() jobProcessId : "+jobProcessId+" -->  "+exceptionAsString);	
	}

	

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

}
