package com.getinsured.cap.service;

public interface CallDataService {

	String modifyAndSaveRequest(String rawRequestJson) throws Exception;
	Boolean doesInteractionIdExistInDb(String rawRequestJson);
}
