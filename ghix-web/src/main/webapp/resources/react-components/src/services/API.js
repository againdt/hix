// @flow

type PromiseTypes = {
    error: boolean,
    errorMessage: string | null,
    errorType: string | null,
    data: Object | null
};


/**
 * @async
 * @param {Object} response - data we retrieve from server
 * @returns {Promise<*>}:
 *  {
 *      error {boolean},                - <false> (if we haven't found any error we tried to handle)
 *      errorMessage {null, string },   - message we would like to show on UI for particular error
 *      errorType {null, string},       - GI API report about error type
 *      data {null, Object}             - response payload
 *  }
 */
async function checkResponse(response : Object) : Promise<any> {
    switch (response.status) {
    case 200:
        return {
            error: false,
            errorMessage: null,
            errorType: null,
            data: await response.json()
        };
    case 500:
        return {
            error: true,
            errorMessage: 'Error 500',
            errorType: 'Server internal error',
            data: null
        };
    case 406:
    case 412:
        return {
            error: true,
            errorMessage: await response.json().message,
            errorType: await response.json().type,
            data: null
        };
    }
}

export default {
    /**
     * @async
     * @param {string} url - URL to download from
     * @returns {Promise<*>} - and we pass this Promise to <checkResponse> for:
     * a) handling errors;
     * b) extracting data on success request
     */
    getData: (url : string) : Promise<PromiseTypes> => {
        const fetchConfig : Object = {
            method: 'GET',
            headers: {
                'cookie' : '{cookie}',
                'Content-Type': 'application/json'
            }
        };
        return fetch(url, fetchConfig)
            .then(response => checkResponse(response))
            .catch(() => ({
                error: true,
                errorMessage: 'Fetch error',
                errorType: null,
                data: null
            }));
    },

    /**
     *
     * @param {Array} args - a) URL to download from; b) body
     * @returns {Promise<*>} - and we pass this Promise to <checkResponse> for:
     * a) handling errors;
     * b) extracting data on success request
     */
    postData: (...args : Array<any>) : Promise<PromiseTypes> => {
        const [ url, body, method = 'POST' ] = args;

        console.log(body, method);
        const csrftoken = $('#tokid').val();

        const fetchConfig : Object = {
            method,
            mode: 'cors',
            credentials: 'include',
                headers: {
                'cookie' : '{cookie}',
                'Content-Type': 'application/json',
                'X-Requested-With' : 'XMLHttpRequest',
                'csrftoken': csrftoken
            },
            body: JSON.stringify({...body}),
        };
        return fetch(url, fetchConfig)
            .then(response => checkResponse(response))
            .catch(() => ({
                error: true,
                errorMessage: 'Fetch error',
                errorType: null,
                data: null
            }));
    }
};
