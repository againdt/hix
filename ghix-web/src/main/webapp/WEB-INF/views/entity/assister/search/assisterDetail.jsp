<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<%
String ShowDesignate = (String)request.getAttribute("ShowDesignate");
String isSameAssister = (String)request.getAttribute("isSameAssister");
%>
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<script src="<c:url value="/resources/js/jquery.validate.min.js" />" type="text/javascript"></script>
<%-- <link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" /> --%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
<style>
	body {background-image: none;}
	.table {
		border: none;
	}
</style>  

 <c:set var="encrytedAssisterId" ><encryptor:enc value="${assister.id}"/> </c:set> 
 <c:set var="encrytedEnrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set> 
 <c:set var="encrytedSiteid" ><encryptor:enc value="${site.id}" isurl="true"/> </c:set>  
 <c:set var="encrytedAssisterIdforURL" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set> 
 
 		<div class="gutter10">
			
					<div class="row-fluid" id="main">
						<form class="form-vertical margin20-t" id="frmassisterdetail" name="frmassisterdetail" action="assisterdetail" method="post">
					
								<c:if test="${sessionScope.anonymousFlag != null && sessionScope.anonymousFlag == 'N'}">
									<div class="content-header">
										<h4><spring:message code="label.assister.assisterdetails.header"/></h4>
										<p><spring:message code="label.assister.assisterdetails.headerpara"/></p>
										<div id="btn btn-primary pull-right">
										<%
										 String profileView = request.getParameter("profileView");
											if(ShowDesignate!=null && ShowDesignate.equals("Y") && profileView == null){%>												
															<a class="btn btn-primary large pull-right" href="<c:url value="/entity/assister/eSignature"><c:param name="assisterId" value="${encrytedAssisterId}"/></c:url>"><spring:message code="label.assister.assisterdetails.continuebtn"/></a>
											<%}
											else if(isSameAssister!=null && isSameAssister.equals("Y")){%>
												<h4>${alreadyDesignatedName} is your designated Enrollment Counselor.</h4>
											<%}
											else if(profileView == null){%>
															<a class="btn btn-primary pull-right" onClick="alreadyDesignated('${alreadyDesignatedName}')"><spring:message  code="label.assister.assisterdetails.continuebtn"/></a>															
											<%}
										%>
									 	</div>
									 	<% 
									 	if( profileView == null) { %>		
											<a class="btn btn-primary pull-left" href="<c:url value="/entity/locateassister/viewassisterlist/${encrytedEnrollmentEntityId}/${encrytedSiteid}?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}" />"><spring:message code="label.assister.backbtn"/></a>
										<% } %>
									</div>
								</c:if>
								<%
								String profileView = request.getParameter("profileView");
								if( profileView == null) { %>
								<c:if test="${sessionScope.anonymousFlag == null || sessionScope.anonymousFlag == 'Y'}">
									<div class="content-header gutter10-lr txt-right">
										<a class="btn btn-primary" href="<c:url value="/entity/locateassister/viewassisterlist/${encrytedEnrollmentEntityId}/${encrytedSiteid}?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}" />"><spring:message code="label.assister.backbtn"/></a>
									</div>
								</c:if>
								<% } %>
								<div class="clearfix"></div>
								<div class="row-fluid margin20-t">		
														<div class="span3 txt-center">
															<spring:url value="/entity/assisteradmin/photo/${encrytedAssisterIdforURL}" var="photoUrl" /> <img src="${photoUrl}" alt="<spring:message code='label.assister.profileImageOfAssister'/>" class="profilephoto thumbnail margin20-l inline-block" height="190" width="250" />
														</div>
														
														<div class="span5">
														<h3>${assister.firstName} ${assister.lastName}</h3>
														${enrollmentEntity.entityName}
														<address>
															<c:set var="entityAddress" value=""/>
															<c:if test="${assister.primaryAssisterSite!=null && assister.primaryAssisterSite.physicalLocation!=null}">
																 <c:if test="${assister.primaryAssisterSite.physicalLocation.zip!=null}">
																	<c:if test="${not empty assister.primaryAssisterSite.physicalLocation.address1}">
																		<c:set var="entityAddress" value="${assister.primaryAssisterSite.physicalLocation.address1},"/>	
																	</c:if>
	            													<c:if test="${not empty assister.primaryAssisterSite.physicalLocation.address2}">
																		<c:set var="entityAddress" value="${entityAddress} ${assister.primaryAssisterSite.physicalLocation.address2},"/>
																	</c:if>
																	<c:if test="${not empty assister.primaryAssisterSite.physicalLocation.city}">
																		<c:set var="entityAddress" value="${entityAddress} ${assister.primaryAssisterSite.physicalLocation.city},"/>		
																	</c:if>
																	<c:if test="${not empty assister.primaryAssisterSite.physicalLocation.state}">
																		<c:set var="entityAddress" value="${entityAddress} ${assister.primaryAssisterSite.physicalLocation.state},"/> 
																	</c:if>
																	<c:if test="${not empty assister.primaryAssisterSite.physicalLocation.zip}">
																		<c:set var="entityAddress" value="${entityAddress} ${assister.primaryAssisterSite.physicalLocation.zip}"/>
																	</c:if>
																 </c:if>
															</c:if>
															<input type="hidden" name="entityAddressVal" value='${entityAddress}'>
															<c:choose>
																<c:when test="${not empty entityAddress}">
																	<c:out value="${entityAddress}" /><br> 
																</c:when>
																<c:otherwise>
																	<spring:message code="label.assister.noAddress"/>
																</c:otherwise>
															</c:choose>
															  <c:choose>
															  	<c:when test="${assister.primaryPhoneNumber!=null}"> 
															  		<c:set var="primaryPhoneNumber" value="${assister.primaryPhoneNumber}"></c:set>
															  		<c:set var="formattedPhoneNumber" value="${fn:substring(primaryPhoneNumber,0,3)}-${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
															  		${formattedPhoneNumber}<br>
															  	</c:when>
															  	<c:otherwise>
															  		${assister.primaryPhoneNumber}<br>
															  	</c:otherwise>
															  </c:choose>
															  ${assister.emailAddress}
														</address>
														<spring:message code="label.assister.languagespoken"/>
														<strong>${assisterLanguages.spokenLanguages}</strong>
														</div>
					
														<div class="span4">
														<iframe width="210" height="210" frameborder="0" scrolling="no"
															marginheight="0" marginwidth="0" class="thumbnail"
															src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${assister.primaryAssisterSite.physicalLocation.address1}+${assister.primaryAssisterSite.physicalLocation.city},+${assister.primaryAssisterSite.physicalLocation.state}+${assister.primaryAssisterSite.physicalLocation.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${assister.primaryAssisterSite.physicalLocation.address1}+${assister.primaryAssisterSite.physicalLocation.city},+${assister.primaryAssisterSite.physicalLocation.state}+${assister.primaryAssisterSite.physicalLocation.zip}&amp;gl=us&amp;t=m&amp;<c:if test="${assister.primaryAssisterSite.physicalLocation.lat!=0 && assister.primaryAssisterSite.physicalLocation.lon !=0}">ll=${assister.primaryAssisterSite.physicalLocation.lat},${assister.primaryAssisterSite.physicalLocation.lon}&amp;</c:if>z=13&amp;iwloc=near&amp;output=embed">
														</iframe>
														</div>
											
											
<!-- 												<tr> -->
<%-- 													<td><spring:message code="label.assister.languagespoken"/></td> --%>
<%-- 													<td><b>${assisterLanguages.spokenLanguages}</b></td> --%>
<!-- 													<td></td> -->
<!-- 												</tr> -->
											
							</div>
						</form>
					</div><!-- #row-fluid -->
				</div>
				<div id="alreadyDesignated" class="modal hide" style="top:30%"></div>
<script type="text/javascript">
$('.ttclass').tooltip();
$(function(){$("#getAssistance").click(function(e){e.preventDefault();var href=$(this).attr('href');if(href.indexOf('#')!=0){$('<div id="searchBox" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal" aria-hidden="true">x<\/button><\/div><div class=""><iframe id="search" src="'+href+'" class="searchModal-body"><\/iframe><\/div><\/div>').modal();}});});function closeSearchLightBox(){$("#searchBox").remove();window.location.reload();}
		function closeSearchLightBoxOnCancel(){$("#searchBox").remove();}
		</script>
		<script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,52,new Date().getTime(),"","","","",""]);
		</script>
		
<script type="text/javascript">
function alreadyDesignated(designatedBroker) {
	$('#alreadyDesignated').html('<div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div class="modal-body"><iframe id="success" src="#" style="overflow-x:hidden;width:30%;border:0;margin:0;padding:0;height:20px;" class="hide"></iframe><spring:message code="label.agent.details.designatedto"/> '  + designatedBroker + '.<spring:message code="label.agent.details.designationmessage"/>'+'<br/> <br/> <button class="btn offset2" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div>');
	$('#alreadyDesignated').modal({backdrop:false});
	
}
</script>		
