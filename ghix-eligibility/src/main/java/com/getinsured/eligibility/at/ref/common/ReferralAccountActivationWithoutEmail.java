package com.getinsured.eligibility.at.ref.common;


import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.accountactivation.ReferralAccountActivationEmail;


/**
 *
 * @author raguram_p
 */
@Component("ReferralAccountActivationWithoutEmail")
@DependsOn("dynamicPropertiesUtil")
@Scope("prototype")	
public class ReferralAccountActivationWithoutEmail extends ReferralAccountActivationEmail {


	


}