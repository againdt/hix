import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Components

import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

// ActionCreators

import { hideModal } from '../../actions/actionCreators';

// Constants

const InformToAddIncomeSourceModal = props => {

    let {
        modal: {
            modalOwner,
            data: [
                data,
                index,
                callback
            ]
        }
    } = props;

    const handleBtnClick = () => {
        data[index].healthCoverage.employerWillOfferInsuranceIndicator = false;
        callback(data);
        handleHideClick();
    };

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__lawfully_presence__for__coverage">
                <h2>
                    Employer Information Not Found
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body txt-left">
                <p>
                    There is no Employer information entered into the system.
                    If you have coverage through an employer then, please enter the employer information when reporting your income.
                </p>
                <div className='modal__controls'>
                    <Button
                        className='modal-confirm-btn'
                        onClick={handleBtnClick}
                    >
                        Ok
                    </Button>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    contexts: state.contexts,
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(InformToAddIncomeSourceModal);
