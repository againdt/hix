#!/usr/bin/env python
import sys
import glob
import os
import shutil

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

if len(sys.argv) < 2:
    print(bcolors.FAIL + "You need to supply which server type you are launching. (id1dev, camainqa, nv2dev, etc.)" + bcolors.ENDC)
    sys.exit(-1)

targetServer = sys.argv[1]
print(bcolors.BOLD + "Using {}{}{} for target configuration".format(bcolors.OKGREEN, targetServer, bcolors.ENDC) + bcolors.ENDC)

configPattern = 'configuration*properties*'
fileList = glob.glob(configPattern)

if len(fileList) == 0:
    print(bcolors.FAIL + "There is no configuration.*properties* files found in current directory" + bcolors.ENDC)
    sys.exit(-1)

newconfig = None

for file in fileList:
    if file != "configuration.properties":
      if(targetServer in file):
        newconfig = file
        print(bcolors.BOLD + 'Found matching configuration file: {}{}{}'.format(bcolors.HEADER, newconfig, bcolors.ENDC) + bcolors.ENDC)
        break
        
if newconfig == None:
  print(bcolors.FAIL + "Unable to find config properties with pattern: '{}{}{}{}' in current directory".format(bcolors.OKGREEN, targetServer, bcolors.ENDC, bcolors.FAIL) + bcolors.ENDC)
  sys.exit(-1)

def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)

print("Copying {}{}{} => {}{}{}".format(bcolors.HEADER, newconfig, bcolors.ENDC, bcolors.OKGREEN, "configuration.properties", bcolors.ENDC))
copyFile(newconfig, "../configuration.properties")
if os.path.exists("configuration.properties"):
  print("New {}configuration.properties{} is ready for {}{}{} Tomcat to start".format(bcolors.OKGREEN, bcolors.ENDC, bcolors.OKBLUE, targetServer, bcolors.ENDC))
