package com.getinsured.consumer.service;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"date"})
public class CallMetaData
{
	private String enteredBy;
	private Date dateTime;
	private String wrapCode ;
	private String callType;
	private String duration;
	private Date date;
	
	public CallMetaData(String enteredBy,Date createDate,Date dateTime, String duration, String wrapCode, String callType) {
		super();
		this.enteredBy = enteredBy;
		this.dateTime = dateTime;
		this.wrapCode = wrapCode;
		this.callType = callType;
		this.duration = duration;
		this.date = createDate;
	}
	public CallMetaData(String enteredBy, Date dateTime) {
		this.enteredBy = enteredBy;
		this.dateTime = dateTime;
	}
	public CallMetaData() {
		// TODO Auto-generated constructor stub
	}
	public String getEnteredBy() {
		return enteredBy;
	}
	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getWrapCode() {
		return wrapCode;
	}
	public void setWrapCode(String wrapCode) {
		this.wrapCode = wrapCode;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}