package com.getinsured.hix.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PlanHealth;

public interface ISerffPlanHealthRepository extends JpaRepository<PlanHealth, Integer>, RevisionRepository<PlanHealth, Integer, Integer> {
	
	String PARENT_PLAN_ID = "parentPlanId";
	
	@Query("SELECT planHealth.id,planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    Object[] getPlanHealthIdByParentId(@Param(PARENT_PLAN_ID) String parentPlanId,@Param("costSharing") String costSharing);
	
	@Query("SELECT planHealth.acturialValueCertificate, planHealth.benefitFile, planHealth.costSharing FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId)")
    List<Object[]> getPlanHealthDetails(@Param(PARENT_PLAN_ID) String parentPlanId);
    
    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    List<Integer> getCostSharingPlanIds(@Param(PARENT_PLAN_ID) String parentPlanId,@Param("costSharing") String costSharing);

    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) ")
    List<Integer> getChildPlanIds(@Param(PARENT_PLAN_ID) String parentPlanId);
    
    @Query("SELECT planHealth.id FROM PlanHealth planHealth WHERE planHealth.plan.id = :planId ")
    Integer getPlanHealthId(@Param("planId") Integer planId);
   
    @Query("SELECT ph FROM PlanHealth ph WHERE ph.plan.issuer.id=:issuerID and ph.plan.id = :planID and ph.costSharing=:csType")
    PlanHealth getRequiredPlanHealthObject(@Param("issuerID") int issuerID,@Param("planID") int planID,@Param("csType") String csType);    
    
    @Query("SELECT COUNT(p) FROM PlanHealth p WHERE (LOWER(p.plan.issuerPlanNumber) LIKE LOWER(:planId) || '%') AND p.plan.isDeleted='N'")
    Long getPlanHealthCount(@Param("planId") String planId);    
    
   }


