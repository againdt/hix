<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>


<!--[if IE]>
	<style type="text/css">
		form#commentForm + table.table {
			visibility: hidden;
		}
	</style>
<![endif]-->

	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	

<!--  Latest UI -->
	
<div class="gutter10">
		<!--<h1>${employer.name} <small>&nbsp;Contact Name: ${employer.contactFirstName} ${employer.contactLastName}</small></h1> -->
	<div class="row-fluid">
			<div class="span3">
				<div class="header">
					<h4>About this Employer</h4>
				</div>
						<ul class="nav nav-list">
	<%-- 			                  <li class="active"><a href="<c:url value="/shop/employer/employercasesummary/${employer.id}"/>">Summary</a></li> --%>
	<%-- 			                  <li><a href="<c:url value="/shop/employer/employercasecontactinfo/${employer.id}"/>">Contact Info</a></li> --%>
							<li class="active"><a href="#">Summary</a></li>
		                 	<li><a href="#">Contact Info</a></li>
		                  	<li><a href="#">Messages</a></li>
		                  	<li><a href="#">Notifications</a></li>
		                  	<li><a href="/hix/shop/employer/employercomments/${employer.id}">Comments</a></li>
		                </ul>
				
				<br>
				<div class="header">
					<h4><i class="icon-cog icon-white"></i> Actions</h4>
				</div>
						<ul class="nav nav-list">
		                  <li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> View Employer Account</a></li>
		                  <li><a href="#"><i class="icon-edit"></i> Compose Message</a></li>
		                  <li><a href="#"><i class="icon-comment"></i> New Comment</a></li>
		                </ul>
		</div>
	
	
		<div id="rightpanel" class="span9">
		
			<form class="form-vertical" id="" name="" action="" method="POST">
				<df:csrfToken/>
				<table class="table">
					<thead>
						<tr class="header">
							<th scope="col"><strong>Comments</strong></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="">
								<div class="">
									<p>
										<a href="#">John Assist</a> added a comment - 12/Oct/12 10&#58;58 AM 
										<span class="pull-right"><a href="#" class="btn btn-mini"><i class="icon-pencil"></i></a> <a href="#" class="btn btn-mini"><i class="icon-trash"></i></a></span>
									</p>
									<p>
										Tried to sell them on Aetna super-platinum 5000. They asked for a meeting next week.
									</p>
									<hr>
								</div>
								<div class="">
									<p>
										<a href="#">John Assist</a> added a comment - 12/Oct/12 10&#58;58 AM 
										<span class="pull-right"><a href="#" class="btn btn-mini"><i class="icon-pencil"></i></a> <a href="#" class="btn btn-mini"><i class="icon-trash"></i></a></span>
									</p>
									<p>
										Had initial conversation with client. They seemed ready to buy.
									</p>
									<hr>
								</div>
								<div class="">
									<form class="form-horizontal">
										<df:csrfToken/>
										<div class="control-group">
										<label class="control-label" for="inputnewcomment">New Comment</label>
											<div class="controls">
											 <textarea class="span" rows="3"></textarea>
											</div>
										</div>
									</form>
								</div>
								
								<comment:view targetId="${employer.id}" targetName="EMPLOYER">
								</comment:view>
								
								<jsp:include page="../../platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="EMPLOYER" id="target_name" name="target_name" />
								<input type="hidden" value="${employer.id}" id="target_id" name="target_id" />
								
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		

		</div>
		</div>	
	</div>	
		
		 
		<!-- Modal -->
		<div id="viewempModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="myModalLabel">View Employer Account</h3>
			</div>
			<div class="modal-body">
			<p>Clicking &#34;Employer View&#34; will take you to the Employer&#39;s portal for &#34; &#60;COMPANY NAME&#62; &#34;.
				Through this portal you will be able to take actions on behalf of this employer, such as view and edit employee list, fill out employer eligibility, etc.
			</p>
			<p>
				Proceed to Employer view?
			</p>
			</div>
			<div class="modal-footer">
				<label class="checkbox pull-left">
					<input type="checkbox" value="">
					Don&#39;t show this message again.
				</label>
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				 <a class="btn btn-primary""  type="button"  href="<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>">Employer View</a>
			
			</div>
		</div>	
	
	
<!--  Latest UI -->
	

	