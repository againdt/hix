<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript">
    $('.my-link').click(function(e) { e.preventDefault(); }); 
</script>
<div class="gutter10-lr">
<c:set var="encHouseHoldId" ><encryptor:enc value="${householdId}" isurl="true"/> </c:set>
    <div class="l-page-breadcrumb" style="margin-top:10px;">
        <!--start page-breadcrumb -->
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:history.back()">
                        &lt; 
                        <spring:message code="label.back" />
                    </a>
                </li>
                <li>
                    <a href="
                    <c:url value="/crm/member/viewmember/${encHouseHoldId}"/>
                    ">
                    <spring:message code="label.account" />
                    </a>
                </li>
                <li>${household.firstName}&nbsp;${household.lastName}</li>
            </ul>
            <div style="font-size: 14px; color: red">
                <c:if test="${errorMsg != ''}">
                    <p>
                        <c:out value="${errorMsg}"></c:out>
                    <p />
                </c:if>
                <br>
            </div>
        </div>
        <!--  end of row-fluid -->
    </div>
    <!--  end l-page-breadcrumb -->
    <div class="row-fluid">
         <h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
    </div>
    <div class="row-fluid">
		<div id="sidebar" class="span3">
			<jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
		</div>
        <div class="span9">
            <div class="graydrkaction margin0">
                <h4 class="span10">Ticket Summary</h4>
            </div>
            <div class="row-fluid">
                <form method="POST" action="#" name="frmCrmTicketDetailInfo" id="frmCrmTicketDetailInfo" class="form-horizontal">
                    <table class="table table-border-none verticalThead">
                        <tbody>
                            <tr>
                                <th class="txt-right span4">Task</th>
                                <td><strong>${tkmTicketsObj.subject} </strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right span4">Ticket Type</th>
                                <td><strong>${tkmTicketsObj.tkmWorkflows.type}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right">Requestor</th>
                                <td><strong>${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right">Plan Name</th>
                                <td><strong>${tkmTicketsObj.tkmWorkflows.description}</strong>
                                </td>
                            </tr>
                            <tr>
                                <th class="txt-right">Created On</th>
                                <td><strong>${tkmTicketsObj.created}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right">Description</th>
                                <td><strong>${tkmTicketsObj.description}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <!--  end of row-fluid -->
</div>
<!--  end of gutter10 -->
<script type="text/javascript">
    function saveComment(ticketId)
    {
    	var validateUrl = '<c:url value="/admin/ticketmgmt/savecomment"></c:url>';
    	
    	var commentData = $("#comment_text").val();
    	$.ajax({
    		url: validateUrl,
    		data: {${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
    			ticketId : ticketId,
    			comment : commentData},
    		success: function(response,xhr)
             {
    	        if(isInvalidCSRFToken(xhr))
    		      return;

    			if(response)
    			{
    				$('#markCompleteDialog').modal('hide');
    				$('#markItComplete a').removeAttr('href')
    			}
    		}
    		});
    }
    function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
		rv = true;
		}
		return rv;
	}	

    
    
    
</script>
<form name="dialogForm" id="dialogForm" method="POST" action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
    <div class="markCompleteHeader">
        <div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
    <div class="modal-body">
        <div class="control-group">
            <div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />

        <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
    </div>
</div>
</form>