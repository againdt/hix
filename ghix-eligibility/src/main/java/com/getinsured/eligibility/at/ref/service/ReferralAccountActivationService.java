package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 *
 */
public interface ReferralAccountActivationService {
	void triggerAccountActivation(SingleStreamlinedApplication singleStreamlinedApplication, SsapApplication ssapApplication, AccountTransferRequestDTO accountTransferRequest);
	AccountActivation updateAccountActivation(int ssapApplicationId);
	String retriggerAccountActivation(String caseNumber, String notificationType);
	Boolean triggerAccountActivation(Long ssapApplicationId);
}
