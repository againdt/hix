package com.getinsured.eligibility.benchmark.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.benchmark.service.EligibilityBenchMarkServiceResponse.ResponseCode;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanResponse;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository; 


/**
 * Assumptions:
 * 	All members in the tax household section of ssap json would be send to BenchMark Service.
 *
 *
 */
@Service("benchmarkPlanService")
public class BenchmarkPlanServiceImpl implements BenchmarkPlanService {

	private static final String BENCHMARK_SERVICE_SUCCESS = "SUCCESS";
	private static final String NO_RESPONSE_FROM_BENCHMARK_PREMIUM_WEB_SERVICE = "No Response from benchmark premium web service";
	private static final String BENCHMARK_WS_INVOKE = "BENCHMARK_WS_INVOKE";

	private static final Logger LOGGER = Logger.getLogger(BenchmarkPlanServiceImpl.class);

	@Autowired private BenchmarkPlanServiceHelper benchmarkPlanServiceHelper;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IntegrationLogService integrationLogService;


	/**
	 * @throws - {@link GIRuntimeException}
	 */
	@Override
	@Deprecated
	public EligibilityBenchMarkServiceResponse process(String caseNumber, Date coverageStartDate) {
		EligibilityBenchMarkServiceResponse response = new EligibilityBenchMarkServiceResponse();
		response.setResponseCode(ResponseCode.FAILURE);
		response.setErrorDescription("Not used at application side.");
		return response;
		/*SsapApplication currentApplication = validateIncomingData(caseNumber, coverageStartDate);
		return preProcess(currentApplication, coverageStartDate);*/
	}

	private SsapApplication validateIncomingData(String caseNumber, Date coverageStartDate) {
		validateDate(coverageStartDate);
		SsapApplication currentApplication = fetchCurrentApplication(caseNumber);
		return currentApplication;
	}

	/**
	 * @throws - {@link GIRuntimeException}
	 */
	@Override
	@Deprecated
	public void processAndPersist(String caseNumber, Date coverageStartDate) {
		/*SsapApplication currentApplication = validateIncomingData(caseNumber, coverageStartDate);
		EligibilityBenchMarkServiceResponse svcResponse = preProcess(currentApplication, coverageStartDate);
		updateEHBAmount(currentApplication, svcResponse);*/

	}

	private void validateDate(Date coverageStartDate) {
		if (coverageStartDate == null){
			throw new GIRuntimeException(EligibilityConstants.COVERAGE_START_DATE_NULL);
		}
	}

	private void updateEHBAmount(SsapApplication currentApplication, EligibilityBenchMarkServiceResponse svcResponse) {
		currentApplication.setEhbAmount(svcResponse.getBenchMarkPremiumAmount());
		ssapApplicationRepository.save(currentApplication);
	}

	private EligibilityBenchMarkServiceResponse preProcess(SsapApplication currentApplication, Date coverageStartDate) {
		GetBenchmarkPlanResponse response = null;
		String errorPrintStack = StringUtils.EMPTY;
		try {
			response = benchmarkPlanServiceHelper.invoke(currentApplication, coverageStartDate);
		} catch (Exception e) {
			errorPrintStack = ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorPrintStack);
			throw new GIRuntimeException(errorPrintStack);
		}
		return processResponse(response, currentApplication, errorPrintStack);
	}
	
	/*private EligibilityBenchMarkServiceResponse preProcess(SsapApplication currentApplication) {

		GetBenchmarkPlanResponse response = null;
		String errorPrintStack = StringUtils.EMPTY;
		try {
			response = benchmarkPlanServiceHelper.invoke(currentApplication);
		} catch (Exception e) {
			errorPrintStack = ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorPrintStack);
		}
		return processResponse(response, currentApplication, errorPrintStack);

	}*/

	private EligibilityBenchMarkServiceResponse processResponse(GetBenchmarkPlanResponse response, SsapApplication ssapApplication, String errorPrintStack) {
		EligibilityBenchMarkServiceResponse svcResponse = new EligibilityBenchMarkServiceResponse();
		svcResponse.setCaseNumber(ssapApplication.getCaseNumber());
		svcResponse.setBenchMarkPremiumAmount(null);

		if (response == null){
			svcResponse.setResponseCode(ResponseCode.FAILURE);
			svcResponse.setErrorDescription(NO_RESPONSE_FROM_BENCHMARK_PREMIUM_WEB_SERVICE);
			svcResponse.setErrorPrintStack(errorPrintStack);
			logData(ssapApplication, errorPrintStack, ResponseCode.FAILURE);
		} else {

			if (!StringUtils.equalsIgnoreCase(response.getResponseDescription(), BENCHMARK_SERVICE_SUCCESS)){
				svcResponse.setResponseCode(ResponseCode.WARNING);
				svcResponse.setErrorDescription(response.getResponseDescription());
				logData(ssapApplication, errorPrintStack, ResponseCode.WARNING);
			} else {
				svcResponse.setResponseCode(ResponseCode.SUCCESS);
				logData(ssapApplication, "SUCCESS", ResponseCode.SUCCESS);
			}
			svcResponse.setBenchMarkPremiumAmount(new BigDecimal(response.getBenchmarkPremium()));
		}

		return svcResponse;
	}

	private void logData(SsapApplication ssapApplication, String errorPrintStack, ResponseCode status) {
		integrationLogService.save(errorPrintStack, BENCHMARK_WS_INVOKE, status.toString(), ssapApplication.getId(), null);
	}

	private SsapApplication fetchCurrentApplication(String caseNumber) {
		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(caseNumber);

		if (ssapApplicationList.isEmpty()){
			// Unable to log to db as we have not yet aware of ssap_application_id {primary key}
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES);
		}
		SsapApplication currentApplication = ssapApplicationList.get(0);
		return currentApplication;
	}



}
