/**
 * 
 */
package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095StagingParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author parhi_s
 *
 */
public class EnrollmentAnnualIRSOutWriter implements ItemWriter<Integer> {
	List<Integer> enrollmentIds;
	int applicableYear;
	int partition;
	private ExecutionContext executionContext;
	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private Enrollment1095StagingParams enrollment1095StagingParams;
	private JobService jobService;
	long jobExecutionId = -1;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIRSOutWriter.class);

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		executionContext = stepExecution.getJobExecution().getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			applicableYear=ec.getInt("year",0);
			partition =ec.getInt("partition");
		}
		
	}
	@Override
	public void write(List<? extends Integer> enrollmentIdListToWriter) throws Exception {
		
		LOGGER.info("Inside EnrollmentAnnualIRSOutWriter");
		Enrollment1095 enrollment1095Record=null;
		List<Enrollment1095> enrollment1095List= new ArrayList<Enrollment1095>();
		Map<String, String> skippedEnrollmentMap= new HashMap<String,String>();
		
		String batchJobStatus=null;
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		if(enrollmentIdListToWriter!=null && !enrollmentIdListToWriter.isEmpty()){
			List<Integer> enrollmentIdList= new ArrayList<Integer>(enrollmentIdListToWriter);
			for(Integer enrollmentId : enrollmentIdList){
				if(enrollmentId != null ){
					try{
						LOGGER.debug(Thread.currentThread().getName() + " Processing enrollment : " + enrollmentId );
						enrollment1095Record=enrollmentAnnualIrsReportService.processAnnualIrsEnrollment(enrollmentId, enrollment1095StagingParams.isFreshRun(), skippedEnrollmentMap);
						enrollment1095List.add(enrollment1095Record);
					}catch(GIException e){
						if(e != null && e.getErrorCode() != 0 && e.getErrorCode() == EnrollmentConstants.ERROR_CODE_201){
							//Suppressing the logger for record un-change exception
							LOGGER.debug("Exception Caught in Processing Populate1095 For enrollmentId: "+enrollmentId+ e);
						}
						else{
							LOGGER.error("Exception Caught in Processing Populate1095 For enrollmentId: "+enrollmentId+" "
									+Thread.currentThread().getName() + " Error message :: " + e.getMessage(), e);
						}
						enrollment1095StagingParams.incrementTotalFailedCount(1);
						if(!skippedEnrollmentMap.isEmpty()){
							enrollment1095StagingParams.putAllToSkippedEnrollmentMap(skippedEnrollmentMap);
						}else{
							LOGGER.error(Thread.currentThread().getName() + " Processing failed but skippedRecord not set for enrollmentId : " + enrollmentId );
							enrollment1095StagingParams.putToSkippedEnrollmentMap(enrollmentId.toString(), " Processing failed");
						}
					}finally{
						LOGGER.debug(Thread.currentThread().getName() + " Processing enrollment : " + enrollmentId );
					}
				}
			}
		}
		try{
			for(Enrollment1095 enrollment1095 : enrollment1095List){
				try{
					enrollmentAnnualIrsReportService.saveToEnrollmentStagingTable(enrollment1095);
					if(null != enrollment1095.getIsOverwritten() && !enrollment1095.getIsOverwritten()){
						enrollment1095StagingParams.incrementTotalUpdateCount(1);
					}else if(null != enrollment1095.getIsOverwritten()){
						enrollment1095StagingParams.incrementTotalOverWrittenCount(1);
					}
				}catch(Exception e){
					enrollment1095StagingParams.putToSkippedEnrollmentMap(enrollment1095.getExchgAsignedPolicyId().toString(), EnrollmentUtils.shortenedStackTrace(e, 3));
					enrollment1095StagingParams.incrementTotalFailedCount(1);
					LOGGER.error("EnrollmentAnnualIRSOutWriter :: Table update failed for enrollment Id : " + enrollment1095.getExchgAsignedPolicyId(), e );
				}
			}
		}catch(Exception e){
			LOGGER.error("EnrollmentAnnualIRSOutWriter :: Table update failed, incrementing fail counter ", e);
			enrollment1095StagingParams.incrementTotalFailedCount(enrollment1095List.size());
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
		}
	}
	
	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}
	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}

	public Enrollment1095StagingParams getEnrollment1095StagingParams() {
		return enrollment1095StagingParams;
	}
	public void setEnrollment1095StagingParams(Enrollment1095StagingParams enrollment1095StagingParams) {
		this.enrollment1095StagingParams = enrollment1095StagingParams;
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
}
