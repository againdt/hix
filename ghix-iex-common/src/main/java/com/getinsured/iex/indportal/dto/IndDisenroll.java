/**
 * 
 */
package com.getinsured.iex.indportal.dto;


/**
 * @author Gajulapalli_K
 *
 */
public class IndDisenroll {
	
	public enum TerminationDateChoice {
		
		CURRENT_MONTH("CURRENT_MONTH"), NEXT_MONTH("NEXT_MONTH"), MONTH_AFTER_NEXT_MONTH("MONTH_AFTER_NEXT_MONTH");
		
		private String value;
		
		private TerminationDateChoice(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
		
	}

	private Long applicationId;
	
	private String healthEnrollmentId;
	
	private String dentalEnrollmentId;

	private String reasonCode;
	
	private String otherReason;

	private String terminationDate;
	
	private String caseNumber;

	private String deathDate;
	
	private String userName;
	
	private Long currentEnrolledApplicationId;

	private String currentEnrolledApplicationCaseNumber;
	
	//HIX-58828
	private String coverageStartDate;
	
	private String isRecTribe;
	
	private TerminationDateChoice terminationDateChoice;
	
	//HIX-70835
	private boolean healthActive;
	
	private boolean dentalActive;
	
	//HIX-69287
	private String dentalAptc;
	
	/**
	 * @return the applicationId
	 */
	public Long getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the enrollmentId
	 */
	public String getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	/**
	 * @param enrollmentId
	 *            the enrollmentId to set
	 */
	public void setHealthEnrollmentId(String healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public String getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(String dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	/**
	 * @return the reasonCode
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * @param reasonCode
	 *            the reasonCode to set
	 */
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getOtherReason() {
		return otherReason;
	}

	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}

	/**
	 * @return the terminationDate
	 */
	public String getTerminationDate() {
		return terminationDate;
	}

	/**
	 * @param terminationDate
	 *            the terminationDate to set
	 */
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}	
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getCurrentEnrolledApplicationId() {
		return currentEnrolledApplicationId;
	}

	public void setCurrentEnrolledApplicationId(Long currentEnrolledApplicationId) {
		this.currentEnrolledApplicationId = currentEnrolledApplicationId;
	}

	public String getCurrentEnrolledApplicationCaseNumber() {
		return currentEnrolledApplicationCaseNumber;
	}

	public void setCurrentEnrolledApplicationCaseNumber(
			String currentEnrolledApplicationCaseNumber) {
		this.currentEnrolledApplicationCaseNumber = currentEnrolledApplicationCaseNumber;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getIsRecTribe() {
		return isRecTribe;
	}

	public void setIsRecTribe(String isRecTribe) {
		this.isRecTribe = isRecTribe;
	}

	public TerminationDateChoice getTerminationDateChoice() {
		return terminationDateChoice;
	}

	public void setTerminationDateChoice(TerminationDateChoice terminationDateChoice) {
		this.terminationDateChoice = terminationDateChoice;
	}

	public boolean getHealthActive() {
		return healthActive;
	}

	public void setHealthActive(boolean healthActive) {
		this.healthActive = healthActive;
	}

	public boolean getDentalActive() {
		return dentalActive;
	}

	public void setDentalActive(boolean dentalActive) {
		this.dentalActive = dentalActive;
	}

	public String getDentalAptc() {
		return dentalAptc;
	}

	public void setDentalAptc(String dentalAptc) {
		this.dentalAptc = dentalAptc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndDisenroll [applicationId=");
		builder.append(applicationId);
		builder.append(", healthEnrollmentId=");
		builder.append(healthEnrollmentId);
		builder.append(", dentalEnrollmentId=");
		builder.append(dentalEnrollmentId);
		builder.append(", reasonCode=");
		builder.append(reasonCode);
		builder.append(", otherReason=");
		builder.append(otherReason);
		builder.append(", terminationDate=");
		builder.append(terminationDate);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", deathDate=");
		builder.append(deathDate);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", currentEnrolledApplicationId=");
		builder.append(currentEnrolledApplicationId);
		builder.append(", currentEnrolledApplicationCaseNumber=");
		builder.append(currentEnrolledApplicationCaseNumber);
		builder.append(", coverageStartDate=");
		builder.append(coverageStartDate);
		builder.append(", isRecTribe=");
		builder.append(isRecTribe);
		builder.append(", terminationDateChoice=");
		builder.append(terminationDateChoice);
		builder.append(", healthActive=");
		builder.append(healthActive);
		builder.append(", dentalActive=");
		builder.append(dentalActive);
		builder.append(", dentalAptc=");
		builder.append(dentalAptc);
		builder.append("]");
		return builder.toString();
	}

}
