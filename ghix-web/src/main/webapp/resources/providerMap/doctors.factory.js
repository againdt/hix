(function (module) {
    'use strict';

    module.factory('DoctorModel', [function () {
        return {
            getName: getName,
            getSpecialty: getSpecialty,
            getStreet: getStreet,
            getCityStateZip: getCityStateZip,
            getLat: getLat,
            getLon: getLon
        };

        //////////////

        function getName () {
            return this.name;
        }

        function getSpecialty() {
            return this.specialty;
        }

        function getStreet () {
            return _.first(this.providerAddress).addline1;
        }

        function getCityStateZip () {
            var addr = _.first(this.providerAddress);
            return addr.city + ' ' + addr.state + ', ' + addr.zip;
        }

        function  getLat() {
            return _.first(this.providerAddress).lat;
        }

        function getLon () {
            return _.first(this.providerAddress).lon;
        }
    }]);

    module.factory('Doctors', ['DoctorModel', function (DoctorModel) {
        var doctors = [],
            doctor = {},
            pageSize = 10,
            total = 0,
            startNum = '',
            current = 0;
        return {
            setDoctors: setDoctors,
            getDoctors: getDoctors,
            mouseoverDoctor: mouseoverDoctor,

            totalDoctors: totalDoctors,
            pageSizeRange: pageSizeRange,
            currentPage: currentPage,
            nextStartNum: nextStartNum
        };

        ///////////

        function setDoctors(data) {
            doctors = _.map(data, function (doctor) {
                return angular.extend(doctor, DoctorModel);
            });
        }

        function getDoctors() {
            return doctors;
        }

        function mouseoverDoctor(data) {
            if (angular.isDefined(data)) {
                doctor = data;
            }
            return doctor;
        }

        function totalDoctors(data) {
            if (angular.isDefined(data)) {
                total = parseInt(data, 10);
            }
            return total;
        }

        function pageSizeRange (data) {
            if (angular.isDefined(data)) {
                pageSize = data;
            }
            return pageSize;
        }

        function currentPage (data) {
            if (angular.isDefined(data)) {
                current = parseInt(data, 10);
            }
            return current;
        }

        function nextStartNum (data) {
            if (angular.isDefined(data)) {
                startNum = data;
            }
            return startNum;
        }
    }]);

    module.factory('doctorsService', ['$http', '$q', 'Doctors', function ($http, $q, Doctors) {
    	
        var requestedData = {
                            'searchKey' : '',
                            'userZip' : '',
                            'currentPage' : '',
                            'pageSize' : '10',
                            'language' : 'all',
                            /*'gender' : 'all' ,*/
                            'otherDistance' : '',
                            'searchType' : 'searchType'//'medical',
                            /*'requestType' : 'search',
                            'planId': '',
                            "providerSearchSource": $("#providerSearchSource").val(),
                            "hiosId": ''*/
                        },
            docotrsUrl = '';
        return {
            getDoctors: getDoctors
        };

        /////////

        function updateRequestData (data) {
            requestedData = angular.extend(requestedData, data);
            return requestedData;
        }

        function getDoctors (data, url) {
            var deferred = $q.defer();
            if (url) {
                docotrsUrl = url;
            }
            $http
                .get(docotrsUrl, {params: updateRequestData(data)})
                .success(function (data, status, headers, config) {
                    Doctors.nextStartNum(data.next_start);
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    }]);


}(angular.module('ProviderMap.Factory.doctors', [])));