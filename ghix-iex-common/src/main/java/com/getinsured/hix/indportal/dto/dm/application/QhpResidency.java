package com.getinsured.hix.indportal.dto.dm.application;

public class QhpResidency {
	 private String qhpResidencyStatus;
	 private String qhpResidencyStatusReason;
	 private Boolean taxHouseholdResidencyAppliesIndicator;


	 // Getter Methods 

	 public String getQhpResidencyStatus() {
	  return qhpResidencyStatus;
	 }

	 public String getQhpResidencyStatusReason() {
	  return qhpResidencyStatusReason;
	 }

	 public Boolean getTaxHouseholdResidencyAppliesIndicator() {
	  return taxHouseholdResidencyAppliesIndicator;
	 }

	 // Setter Methods 

	 public void setQhpResidencyStatus(String qhpResidencyStatus) {
	  this.qhpResidencyStatus = qhpResidencyStatus;
	 }

	 public void setQhpResidencyStatusReason(String qhpResidencyStatusReason) {
	  this.qhpResidencyStatusReason = qhpResidencyStatusReason;
	 }

	 public void setTaxHouseholdResidencyAppliesIndicator(Boolean taxHouseholdResidencyAppliesIndicator) {
	  this.taxHouseholdResidencyAppliesIndicator = taxHouseholdResidencyAppliesIndicator;
	 }
	}
