<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
  .dl-horizontal dt {
    font-weight: normal;
  }
  .dl-horizontal dd {
    font-weight: bold;
  }
  #account-settings dt,
  #account-settings dd {margin-bottom: 15px; }

  #acc-settings-info dt {
    width: 160px;
  }
 #acc-settings-info dd {
    margin-left: 180px;
    font-weight: bold;
  }
  #change-email label, #change-email input,
  #change-password label, #change-password input { margin: 10px 0;}
  #change-questions .form-group{
    float: left;
    margin: 5px 0;
  }
  #newpassword_error{
  	position: relative;
    margin-left: -128px;
    width: 407px;
  }

	#lockInfo a,
    #inactiveDormantInfo a{
    text-decoration:underline
    }
</style>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:if test='${stateCode != "ID"}'>
	<div class="form-wrapper">
</c:if>
<c:if test='${stateCode == "ID"}'>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
</c:if>
	<div id="user-login">
	<div class="row-fluid" id="titlebar">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<!-- HIX-28796 :When adding a new role Confirm Your Identity text is missing on login page
		             added confirmId parameter -->

		<c:choose>
			<c:when test="${confirmId == 'Y'}">
				<h1>Confirm Your Identity</h1>
				<script>
					document.title = 'Confirm Your Identity / ' + $('#exchangeName').val();
				</script>
			</c:when>
			<c:otherwise>
				<h1><spring:message  code="label.login"/></h1>
				<script>
					document.title = '<spring:message  code="label.login"/>' + ' / ' + $('#exchangeName').val();
				</script>
			</c:otherwise>
		</c:choose>
	</div>
	</div>
	<div class="row-fluid">
		<div class="box-loose padd_none" id="rightpanel">
			<form class="form-horizontal" id="loginform" name="loginform" action="/hix/j_spring_security_check" method="post" autocomplete="off" >
			<c:if test='${stateCode == "ID"}'>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
			</c:if>
			<div class="row-fluid">
				<div class="gutter10">
					<df:csrfToken/>
					<!-- Alert Message: Start -->
					<c:choose>
						<c:when test="${isAccLocked}">
							<div id="lockInfo" class="alert alert-error" >
							<p>Your account has been locked due to several unsuccessful login attempts. Please use this link to <a href="<c:url value="/account/unlock/email"/>" class="margin30-t">unlock your account.</a></p>
							</div>
						</c:when>
						<c:when test="${isAccInactiveDormant}">
							<div id="inactiveDormantInfo" class="alert alert-error" >
									<p>Your account has been disabled due to inactivity. Please use this link to <a href="<c:url value="/account/unlock/email"/>" class="margin30-t">enable your account.</a></p>
							</div>
						</c:when>
						<c:when test="${isAccInactive}">
							<div id="inactiveInfo" class="alert alert-error" >
									<p>Your account has been disabled by the <%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%> administrator. Please contact the customer service center <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE)%> to enable your account.</p>
							</div>
						</c:when>
					</c:choose>
					<!-- Alert Message: End -->

							<%
					WebApplicationContext webAppContext = RequestContextUtils.getWebApplicationContext(request);
					MessageSource messageSource = (MessageSource)webAppContext.getBean("messageSource");
						String passwordPolicyRegex = "";
						String errorMsg = "";
						if(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE)=="Y"){
							passwordPolicyRegex = DisplayUtil.replaceDynamicValuesInMessageSource(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX));
							errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
						}%>

					<label for="passwordPolicyRegex" aria-hidden="true" class="aria-hidden">passwordPolicyRegex</label>
					<input type="hidden" id="passwordPolicyRegex" name="passwordPolicyRegex" value="<%=passwordPolicyRegex%>"/>
					<label for="passwordPolicyErrorMsg" aria-hidden="true" class="aria-hidden">passwordPolicyErrorMsg</label>
					<input type="hidden" id="passwordPolicyErrorMsg" name="passwordPolicyErrorMsg" value="<%=errorMsg%>"/>

					<c:if test="${not empty authfailed && isAccLocked eq false && isAccInactiveDormant eq false && isAccInactive eq false}">
						<div class="errorblock alert alert-info">
							<p><spring:message  code="label.authfailed"/></p>
						</div>
					</c:if>
				</div>
			</div>
			<div class="row-fluid">
				<div class="gutter10">
					<div class="form-group">
						<label for="j_username" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.userEmail"/></label>
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="text" name="j_username" id="j_username" value="" class="input-large" size="30" onkeypress="resetErrorMsg(this)" />
								<div class="error" id="j_username_error"></div>
								<div id="j_username_valid" style="display:none;">
									<input type="hidden" id="validateEmailAddress" value="" /><label class="error" for="validateEmailAddress"><span><em class="excl">!</em><spring:message  code="label.validateEmailAddress"/></span></label>
								</div>
							</div>
					</div>
					<div class="form-group">
						<label for="j_password" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.password"/></label>
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<input type="password" name="j_password" id="j_password" value="" class="input-large" size="30" onkeypress="resetErrorMsg(this)">
								<div class="error" id="j_password_error" name="j_password_error"></div>
							</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
						<div class="clearfix nmhide mshide">
							<label class="checkbox inline">
								<input type="checkbox" name="_spring_security_remember_me"> <span>
								<spring:message code="label.rememberme"/></span>
							</label>
						</div>
						<input type="submit"  id="submit" value="<spring:message code="label.login"/>" class="btn btn-primary btn-fullwidth-xs margin10-t">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
							<a href="/hix/account/user/forgotpassword"><small><spring:message code="label.forgetpassword"/></small></a>
						</div>
						<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
							<!-- <a href="/hix/account/signup/broker"><small><spring:message code="label.newUser"/></small></a> -->
							<!-- <a href="/hix/"><small><spring:message code="label.newUser"/></small></a> -->
						</div>
					</div>
				</div>
			</div>
				<c:if test='${stateCode == "ID"}'>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
					<div class="gutter20">
						<img src="<c:url value="/resources/img/idaLink_logo.png" />" width="" height="" alt="Welcome to idalink! Your online portal for managing benefits from Idaho's Department of Health and Welfare. idaLink" title="idalink"  />
						<p><small><spring:message code="label.homePage.haveAccountOption"/></small></p>
					</div>
				</div>
				</c:if>

				<input type="hidden" id="authfailedpasswordmaxage" name="authfailedpasswordmaxage" value="<c:out value="${AUTH_FAILED_PASSWORD_MAX_AGE}"/>" />
				<input type="hidden" id="authfailedpasswordusername" name="authfailedpasswordusername" value="<c:out value="${AUTH_FAILED_PASSWORD_USERNAME}"/>" />

			</form>

			<div id="change-password" class="modal hide fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="password-modal" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<span class="span_as_h3" id="password-modal"><c:out value="${PASSWORD_EXPIRED_MESSAGE}"/></span>
				</div>
				<userInfoView:changePassword redirectURL="/account/user/login" submitRedirectURL="/account/user/login">
				</userInfoView:changePassword>
			</div>
		</div><!---box-loose-->
	</div><!--row-fluid-->
	</div><!--#user-login-->
</div>


	<script type="text/javascript">
	 function validateEmail(sEmail) {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(sEmail)) {
		    return true;
		}
		else {
		    return false;
		}
	}




	function resetErrorMsg(element) {
		var elementId = element.id;

		$("#" + elementId + "_error").html('');
	}

	jQuery.validator.addMethod("validateLogin", function(value,
			element, param) {

		 var sEmail = $('#j_username').val();
		 if (!validateEmail(sEmail)){
			 return false;
		 }
		 return true;
	});
	var validator = $("#loginform")
	.validate(
			{
		rules:{
			j_username:{

				required : {
					depends:function(){
						$(this).val($.trim($(this).val()));
						return true;
					}},
				validateLogin:true
			},
			j_password:{
				required : true
			}
		},
		messages : {
			j_username:{
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please enter email address.</span>",
				validateLogin:"<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please enter valid email address.</span>"
			},
			j_password:{
				required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please enter password.</span>"
			}
		},
		onkeyup : false,
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId+ "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	});







	</script>


<script type="text/javascript">
$(document).ready(function(){
  $('div[rel=popover]').popover({trigger:'hover'});
  $('div[rel=popover]').popover({trigger:'focus'});
});

$('.close, .modal-backdrop').live('click',function(e){
	e.preventDefault();
    $('.modal-footer button:first').trigger('click');
});


if($('#authfailedpasswordmaxage').val()=='true' || $('#authfailedpasswordmaxage').val()== true){
	$("#email").attr("value", $("#authfailedpasswordusername").val());
	$("#change-password").modal({show:true});
}else{
	$("#change-password").modal({show:false});
}

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('.error label').hide();
	}
});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/user/userinfologin.js" />"></script>
