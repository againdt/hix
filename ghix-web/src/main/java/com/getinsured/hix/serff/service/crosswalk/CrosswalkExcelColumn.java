package com.getinsured.hix.serff.service.crosswalk;

/**
 * Class is used to generate Enum on the basis of STM Plan Excel Column.
 * 
 * @author Bhavin Parmar
 * @since May 3, 2014
 */
public class CrosswalkExcelColumn {

	private static final int STRING_TYPE = 2;

	public enum CROSSWALKExcelColumnEnum {
		CURRENT_PLAN_ID("Current HIOS Plan ID (Standard Component)", "CURRENT_PLAN_ID", 0, STRING_TYPE),
		COUNTY_NAME_CODE("County Name", "COUNTY_NAME_CODE", 1, STRING_TYPE),
		SERVICE_AREA_ZIP_CPDES("Service Area Zip Code(s)", "SERVICE_AREA_ZIP_CPDES", 2, STRING_TYPE),
		CROSSWALK_LEVEL("Crosswalk Level", "CROSSWALK_LEVEL", 3, STRING_TYPE),
		CROSSWALK_REASON("Reason for Crosswalk", "CROSSWALK_REASON", 4, STRING_TYPE),
		CROSSWALK_PLAN_ID("Crosswalk HIOS Plan ID (Standard Component)", "CROSSWALK_PLAN_ID", 5, STRING_TYPE),
		IS_CATASTROPHIC_OR_CHILD_ONLY("Is this Plan a Catastrophic or Child-Only Plan?", "IS_CATASTROPHIC_OR_CHILD_ONLY", 6, STRING_TYPE),
		AGING_OFF_CROSSWALK_PLAN_ID("Crosswalk Plan ID for Enrollees Aging off for Catastrophic or Child-Only Plan", "AGING_OFF_CROSSWALK_PLAN_ID", 7, STRING_TYPE),
		CUR_PLAN_FORM_NUM("Current Plan ID (Optional at State Discretion) for Associated Policy Form Number(s)", "CUR_PLAN_FORM_NUM", 8, STRING_TYPE),
		CUR_PLAN_NAIC_TRACK_NUM("Current Plan ID (Optional at State Discretion) for NAIC SERFF Tracking Number(s) for Form Filing(s)", "CUR_PLAN_NAIC_TRACK_NUM", 9, STRING_TYPE),
		CW_PLAN_FORM_NUM("Crosswalk Plan ID (Optional at State Discretion) for Associated Policy Form Number(s)", "CW_PLAN_FORM_NUM", 10, STRING_TYPE),
		CW_PLAN_NAIC_TRACK_NUM("Crosswalk Plan ID (Optional at State Discretion) for NAIC SERFF Tracking Number(s) for Form Filing(s)", "CW_PLAN_NAIC_TRACK_NUM", 11, STRING_TYPE),
		CW_AGINGOFF_PLAN_FORM_NUM("Crosswalk Plan ID (Catastrophic or Child-Only) for Associated Policy Form Number(s)", "CW_AGINGOFF_PLAN_FORM_NUM", 12, STRING_TYPE),
		CW_AGINGOFF_PLAN_NAIC_TRKNUM("Crosswalk Plan ID (Catastrophic or Child-Only) for NAIC SERFF Tracking Number(s) for Form Filing(s)", "CW_AGINGOFF_PLAN_NAIC_TRKNUM", 13, STRING_TYPE);
		
		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;
		
		private CROSSWALKExcelColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}
		
		public String getColumnName() {
			return columnName;
		}
		
		public String getColumnConstName() {
			return columnConstName;
		}
		
		public int getColumnIndex() {
			return columnIndex;
		}
		
		public int getDataType() {
			return dataType;
		}
	}
}
