
/****************************

 # Regular buttons types

****************************/


const INITIAL_BTN = 'INITIAL';
const DEFAULT_BTN = 'DEFAULT';

const regularButtonsTypes = {
    INITIAL_BTN,
    DEFAULT_BTN,
};


/****************************

 # Special buttons types

****************************/


const CANCEL_BTN = 'CANCEL';
const COMBINED_BTN = 'COMBINED';
const HIDDEN_BTN = 'HIDDEN';
const SPOUSE_BTN = 'SPOUSE';
const SWITCHED_TO_SAVE_BUTTON_BTN = 'SWITCHED_TO_SAVE_BUTTON';
const SWITCHED_TO_SUBMIT_APPLICATION_BTN = 'SWITCHED_TO_SUBMIT_BUTTON';

const specialButtonsTypes = {
    CANCEL_BTN,
    COMBINED_BTN,
    HIDDEN_BTN,
    SPOUSE_BTN,
    SWITCHED_TO_SAVE_BUTTON_BTN,
    SWITCHED_TO_SUBMIT_APPLICATION_BTN
};

const buttonsTypes = {...regularButtonsTypes, ...specialButtonsTypes};

export default buttonsTypes;