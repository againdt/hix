package com.getinsured.hix.batch.migration.application.nv.processor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.List;
import java.util.function.Predicate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import com.getinsured.hix.indportal.dto.dm.application.ApplicationDTO;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationJSONDTO;
import com.getinsured.hix.indportal.dto.dm.application.result.ApplicationStatusData;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.iex.household.repository.HouseholdRepository;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class AppStatusProcessor implements ItemProcessor<String, ApplicationStatusData>, ApplicationContextAware {
	private final static Logger LOGGER = Logger.getLogger(AppStatusProcessor.class);
	

	private String jobStartTime;
	private static ApplicationContext applicationContext;
	private Gson mapper = new GsonBuilder().disableHtmlEscaping().create();

	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	private static final String LOGGERSTMT1 ="Reading Application JSON :";
	private static final String JSON_START = "{\"application\":{";
	private static final String JSON_END = "}}";
	private static final String APPLICATION_START = "\"applications\":{";
	private String waitInterval;
	private static final int WAIT_COUNT = 3;	
	private static final String SORT_BY_VALUE = "creationTimestamp";
	private HouseholdRepository householdRepository; 
	private SsapApplicationRepository ssapApplicationRepository;
	
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

		} catch (JAXBException e) {
			LOGGER.error("Error while initializing JAXBContext", e);
		}

	}

	public ApplicationStatusData process(String result) throws Exception {
		ApplicationStatusData data = new ApplicationStatusData();
		householdRepository = (HouseholdRepository) applicationContext.getBean("householdRepository");
		ssapApplicationRepository = (SsapApplicationRepository) applicationContext.getBean("ssapApplicationRepository");
		//data.setJsonSource(result);
		if (!StringUtils.isEmpty(result)) {
			
			if(StringUtils.startsWithIgnoreCase(result, APPLICATION_START)) {
				result = result.substring(APPLICATION_START.length(), result.length()-1);
			}
			else if (StringUtils.endsWithIgnoreCase(result, ",")) {
				result = result.substring(0, result.length() - 1);
			}
			result = JSON_START + result + JSON_END;
		}
		try {
			LOGGER.info(LOGGERSTMT1+result);
			JsonReader jsonReader = mapper.newJsonReader(new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
			ApplicationJSONDTO applicationDTO = mapper.fromJson(jsonReader, ApplicationJSONDTO.class);
			ApplicationDTO application = applicationDTO.getApplication().values().iterator().next();
			long waitingTime = getWaitingTimeInterval();
			if(waitingTime!=0) {
				waitForATCompletion(application.getResult().getInsuranceApplicationIdentifier().toString(), data,waitingTime);
			}
			List<SsapApplication> applications = fetchHouseholdByExternalApplicationId(application.getResult().getInsuranceApplicationIdentifier().toString());
			if (applications != null && applications.size()>0 && applications.get(0).getCmrHouseoldId() != null)
			{
				Household household = householdRepository.findOne(applications.get(0).getCmrHouseoldId().intValue());
				if (household != null  && household.getUser() != null)
				{						
					data.setApplicationId(applications.get(0).getId());
				}
			}
		} catch (Exception e) {
			//data.setErrorMessage(ExceptionUtils.getFullStackTrace(e).replaceAll(System.lineSeparator(), " "));
			LOGGER.error("Unable to read next JSON object" + e);
			//throw new ParseException("Unable to read next JSON object", e);
		}
		return data;
	}
	
	private long getWaitingTimeInterval() {
		
		long waitTime=0;
		
		if(!StringUtils.isEmpty(waitInterval)) {
			try {
				waitTime = Long.parseLong(waitInterval);
			}catch(Exception e) {
				LOGGER.error("Error in parsing wait Interval : " + e);
			}
		}
		
		return waitTime;
	}
	
	private List<SsapApplication> fetchHouseholdByExternalApplicationId(String externalApplicationId) {
		PageRequest pageRequest = new PageRequest(0, 1, new Sort(Sort.Direction.DESC, SORT_BY_VALUE));
		return ssapApplicationRepository.findLatestByExternalAppIdCreationTime(externalApplicationId, new Timestamp(Long.parseLong(jobStartTime)), pageRequest);
	}
	
	private boolean waitForATCompletion(String externalApplicationId, ApplicationStatusData data,long waitingTime) {

		int sleepCount = WAIT_COUNT;
		Predicate<String> householdExists = e -> {
			List<SsapApplication> applications = fetchHouseholdByExternalApplicationId(e);
			if (applications != null && applications.size()>0 && applications.get(0).getCmrHouseoldId() != null)
			{
				return true;
			}
			return false;
		};

		try {

			while (householdExists.negate().test(externalApplicationId) && sleepCount > 0) {
				Thread.sleep(waitingTime);
				sleepCount--;
			}

		} catch (InterruptedException ie) {
			LOGGER.error("Got interrupted while waiting for AT push completion" + ie);
		} catch (Exception e) {
			LOGGER.error("Exception while checking for AT push completion" + e);
			throw e;
		}
		return false;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	public void setWaitInterval(String waitInterval) {
		this.waitInterval = waitInterval;
	}

	public void setJobStartTime(String jobStartTime) {
		this.jobStartTime = jobStartTime;
	}
}
