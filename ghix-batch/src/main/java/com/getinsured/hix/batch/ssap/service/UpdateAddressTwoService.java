package com.getinsured.hix.batch.ssap.service;

import com.getinsured.hix.platform.util.exception.GIException;

public interface UpdateAddressTwoService {
    void processUpdateAddressTwo(String ssapAppIdsFilePath, String absPathWithNameAndExt) throws GIException;
}
