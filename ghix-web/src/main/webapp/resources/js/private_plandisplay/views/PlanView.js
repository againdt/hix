(function (views) {
	views.Plans = Backbone.View.extend({
		events : {
			'click input.addToCart': 'addToCart',
			'click .addToCart': 'addToCart',
			'click .compare' : 'compare',
			// 'click .addToCompare' : 'addToCompare',
			// 'click .removeCompare' : 'removeCompare',
			'click .compareNow' : 'comparePlansTrg',
			'click .detail' : 'detailPlanTrg',
			'change input.filter_checkbox' : 'filter',
			'change input.plantype_filter_checkbox' : 'filter',
			'change input.deductible_filter_checkbox' : 'filter',
			'change input.plantype_filter_hsa' : 'filter',
			'change input.plantype_filter_csr' : 'filter',
			'change input.qualityrating_filter_checkbox' : 'filter',
			'change input.doctor_filter_checkbox' : 'filter',
			'click #shopping-modal-update':'dentalCustomGroupUpdate'
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','addToCart', 'compare', 'addToCompare','removeCompare','filter', 'comparePlansTrg', 'detailPlanTrg', 'dentalCustomGroupUpdate');
			var self = this;

			$('#mainSummaryCmpHead').hide();
			$('#benefitHead').hide();
			$('#detailHead').hide();
			self.collection.fetch({
				success : function () {
					if(self.collection.length==0){
						$('#mainSummary').empty();
						$('#mainSummary').hide();
						$('#mainSummary_err').show();
						$('.personalize, #pagination, .first-time').hide();
					}
					//Get disclaimers Info for each issuer.
					self.getDisclaimersInfo();
					var setDefaultSortDropDown = 'false';
					if($('#gpsVersion').val() == 'V1'){
						self.collection.setSort('smartScore', 'desc');
					}
					else {
						self.collection.setSort($('#defaultSort').val(), 'asc');
						setDefaultSortDropDown = 'true';
					}

					if($('#showEmployerPlans').val() && $('#showEmployerPlans').val() == 'YES'){
						self.collection.setSort('premiumAfterCredit', 'asc');
					}
					self.collection.pager();
					self.loadAll();
					self.collection.findMaxTileHeight();

					/* set the default sort label  HIX-61616 */
					if (setDefaultSortDropDown === 'true') {
						var setSortBySelectedVal = $('#defaultSort').val();
						$('#sort input').each(function(){
							var me = $(this);
							if(me.val() == setSortBySelectedVal){
								me.prop('checked', true);
							}else{
								me.prop('checked', false);
							}
						});
					}

					var csr = $('#csr').val();
				    if(csr == 'CS0' || csr == 'CS1'|| csr == ''){
				      	 $('#csrFilter').hide();
				    }
				    else
			    	{
				    	if($('#stateCode').val() == 'CT'){
							$('.plantype_filter_csr').prop('checked', true);
				    		//self.filter();
				    	}
			    	}
					self.filter();

					$('#filter').show();

					if($('#insuranceType').val() == 'DENTAL'){
						var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
						for(var i = 0; i < seekingCoverageDentalObj.length; i++) {
							if(seekingCoverageDentalObj[i].seekingCoverageDental=="Y") {
								$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", true );
							} else {
								$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", false );
							}
						}

						if($('#PERSON_COUNT').val() > 1 && $('#customGroupingOnOff').val()!='OFF'){
							if($('#isDentalPlanSelected').val() == 'N'){
								if($('#closedPopup').val()!='Y'){
									$('#shopping-modal').modal({
										backdrop: 'static',
									  	keyboard: false
									});
								}
								$('.changeLink').attr("data-original-title","");
							}else{
								$('#shopping-modal').modal('hide');
								$('.changeLink').removeClass('active');
								$('.changeLink').attr("data-original-title",plan_display['pd.label.title.removeDentalPlan']);
							}
						}
					}
				},
				silent:true
			});
			var compareTray = $(".compare-widget-position");
			var iframeState = $('#iframe').val();
			//var compareTabElelement = '<div class="compare-tray-tab"><a class="tab phix-theme js-compare-handle" href="javascript:void(0);" data-handle-state="hide"><i class="icon-circle-arrow-down"></i> Hide Compare</a></div>';

		    if(iframeState == 'yes'){
		    	$('.js-compare-tray-tab-iframe--wrap').removeClass('hide');
		    	$('.js-compare-tray-tab-noiframe--wrap').addClass('show');
		    	$('.js-compare-widget-show-tab-position').hide();
		    	compareTray.addClass("iframe-compare-widget-position");
		    	if((compareTray).hasClass("noiframe-compare-widget-position")) {
		    		compareTray.removeClass("noiframe-compare-widget-position");
		    	}
		    	$(".compare-tray-tab-iframe--wrap").append('<div class="compare-tray-tab--iframe js-plan-compare-widget"><a title="Show Compare" class="tab phix-theme js-compare-handle" href="javascript:void(0);" data-handle-state="show"><i class="icon-circle-arrow-left"></i> </a></div>');
		    	$(".compare-tray-tab-iframe--wrap").hide();
		    }
		    else {
		    	$('.js-compare-tray-tab-noiframe--wrap').addClass('show');
		    	$('.js-compare-tray-tab-iframe--wrap').removeClass('hide');
		    	compareTray.addClass("noiframe-compare-widget-position");
		    	if((compareTray).hasClass("iframe-compare-widget-position")){
		    		compareTray.removeClass("iframe-compare-widget-position");
		    	}
		    }

			var filtersApplied = [];
			$('.ps-sidebar').on('change', 'input[type=checkbox]', function() {
				var me = $(this);

				if(me.prop('checked') === true){
					filtersApplied.push(me.attr('data-filter-label'));
				}else {
					filtersApplied = filtersApplied.filter(function(filter){
						return filter !== me.attr('data-filter-label');
					})
				}


				if(filtersApplied.length === 0){
					$('.ps-plans__filters').hide();
					$('.ps-plans__filters-applied').html('');
				}else{
					$('.ps-plans__filters').show();
					var filtersAppliedHtml = '';
					filtersApplied.forEach(function(filter) {
						filtersAppliedHtml += '<span class="ps-plans__filter">' + filter + '</span>';
					})

					$('.ps-plans__filters-applied').html(filtersAppliedHtml);
				}

			});




			/* Show - Hide Compare plan widget */
			$('.js-compare-plans').click(function() {
				var me = $(this);
				if(me.attr("data-show") === 'true') {
					me.find('i').removeClass('icon-minus').addClass('icon-plus');
					me.find('.js-plan-compare-title').text(plan_display['pd.label.title.showCompare']);
					me.attr("data-show", 'false');

					//triggerGoogleTrackingEvent(['_trackEvent', 'Compare Drawer', 'click', 'Hide Compare']);
					window.dataLayer.push({'event': 'plan_display_compare_drawer_state', 'eventCategory': 'Plan Selection - Compare', 'eventAction': 'Click', 'eventLabel': 'Hide Compare'});
				}else {
					me.find('i').removeClass('icon-plus').addClass('icon-minus');
					me.find('.js-plan-compare-title').text(plan_display['pd.label.title.comparePlans']);
					me.attr("data-show", 'true');

					//triggerGoogleTrackingEvent(['_trackEvent', 'Compare Drawer', 'click', 'Show Compare']);
					window.dataLayer.push({'event': 'plan_display_compare_drawer_state', 'eventCategory': 'Plan Selection - Compare', 'eventAction': 'Click', 'eventLabel': 'Show Compare'});
				}

				$( ".cp-compare-plans__body, .cp-compare-plans__footer" ).slideToggle( "slow" );
			});


	 	 	$(document).on('click','.js-removeCompare', function(e){

	 	 		var tempPlanModel = $(this).find('.cp-compare-plans__remove');
				var planInfo = tempPlanModel.data('issuer') + ' | ' + tempPlanModel.data('name') + ' | ' + tempPlanModel.data('level');

				// Remove from Cart Event on X click
				window.dataLayer.push({
					'event': 'compareEvent', 
					'eventCategory': 'Plan Selection - Compare', 
					'eventAction': 'Remove from Compare', 
					'eventLabel': planInfo
				});

	 	 		self.removeCompare(e);
	 	 	});

	 	 	$(document).on('click','.compareButton', function(e){
	 	 		App.views.compareView.comparePlans();
	 	 	 	$('.js-plan-compare-widget').hide();
	 	 	});

	 	 	$(document).on('click','.addPrevYrPlan', function(e){
	 	 		App.views.compareView.addPrevYearPlanToCompare();
	 	 	});

			self.collection.cartSummaryfetched = false;

			self.cartCollection = options.cartCollection;
			self.cartCollection.fetch({
				success : function () {
					self.collection.cartSummaryfetched = true;
				}
			});

			self.cartModel = options.cartModel;

			$('.filter_checkbox').bind('change', this.filter);
			$('.plantype_filter_checkbox').bind('change', this.filter);
			$('.deductible_filter_checkbox').bind('change', this.filter);
			$('.plantype_filter_hsa').bind('change', this.filter);
			$('.plantype_filter_csr').bind('change', this.filter);
			$('.qualityrating_filter_checkbox').bind('change', this.filter);
			$('.doctor_filter_checkbox').bind('change', this.filter);
			$('#shopping-modal-update').bind('click', this.dentalCustomGroupUpdate);

			self.collection.bind('reset', self.addAll);

		},

		comparePlansTrg  : function () {

			var comparePlans = this.collection.models;
			var comparePlansInfo = '';

			for(var i = 0; i < comparePlans.length; i ++) {

				comparePlansInfo += comparePlans[i].get('issuer') + ' | ' + comparePlans[i].get('name') + ' | ' + comparePlans[i].get('level');

				if(i < comparePlans.length - 1) {
					comparePlansInfo += ' :: ';
				}
			}
			
			// When "Compare Now" button is clicked
			window.dataLayer.push({
				'event': 'compareEvent', 
				'eventCategory': 'Plan Selection - Plan Tiles Events', 
				'eventAction': 'Compare Now Click', 
				'eventLabel': comparePlansInfo
			});

			$('#view').val('compare');
			$('.js-plan-compare-widget').hide();
			App.views.compareView.comparePlans();
		},

		detailPlanTrg  : function (e) {
			var self = this;
			if(e.target.id  !== undefined){
				self.collection.addToDetail(e.target.id.replace("detail_",""));

				var planModel = self.collection.searchPlan(this.collection.models, e.target.getAttribute('data-plan-id'));

				if(planModel === '') {
					window.dataLayer.push({
						'event': 'detail',
						'eventCategory': 'Plan Selection - Plan Tiles Events',
						'eventAction': 'View Details',
						'eventLabel': 'Tile View Detail'
					});
				}
				else {
					var planModelLabel = planModel.get('issuer') + ' | ' + planModel.get('name') + ' | ' + planModel.get('level');
					window.dataLayer.push({
						'event': 'detail',
						'eventCategory': 'Plan Selection - Plan Tiles Events',
						'eventAction': 'View Details',
						'eventLabel': planModelLabel,
						'ecommerce': {
							'detail': {
								'products': [{
									'id': planModel.attributes.issuerPlanNumber,
									'name': planModel.attributes.name,
									'brand': planModel.attributes.issuer,
									'category': planModel.attributes.level,
									'variant': planModel.attributes.networkType,
									'price': planModel.attributes.premium
						       }]
					   		}
						}
					});
				}

			}
			$('#view').val('detail');
			App.views.detailView.detailPlan();
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			$('.sort').dropkick();
			self.collection.each (self.addOne);
			self.collection.findMaxTileHeight();
		},

		loadAll : function () {
            var self = this;
            self.collection.loadIssuers();
            self.loadCartSummary();
            self.collection.updateCompareSummary();

            //scrolls page to the top
          	$('.tile-header a.detail').on('click', function(){
    	   		$('html, body').animate({scrollTop: 0}, 0);
    	   	});

            $('.circle-num').each(function(){
            	var scorenum = $(this).html();
            	self.collection.drawPie(scorenum);
            	});

            if ($('#mainSummary_err').is(":visible") || $('#gpsVersion').val() == 'Baseline'){
        	 $('.first-time').hide();
         	} else{
         		//First time user cookie
         		$(function() {
         			$(document).mouseup(function (e) {
         				var hide = 'Hide First time user tooltip'; //cookie value

         				//click outside the popover to close it
         				var container = $(".first-time");
         				var close = $(".first-timer");

         				// if the target of the click isn't the container nor a descendant of the container
         				if (!container.is(e.target) && container.has(e.target).length === 0) {
         					container.hide();
         				}

         				if (close.is(e.target)){container.hide();}

         				//Create tooltip
         				createCookie("firstTimeUser", hide);
         				return false;
         			});

         			//Check if cookie has been created
         			if (readCookie("firstTimeUser") !== null) {
         				$('.first-time').hide();
         			} else {
         				$('.first-time').fadeIn('slow');
         			}
         		});
         	}

            //Create cookie
            function createCookie(name,value,days) {
            	if (days) {
            		var date = new Date();
            		date.setTime(date.getTime()+(30*24*60*60*1000));
            		var expires = "; expires="+date.toGMTString();
            	}
            	else var expires = "";
            	document.cookie = name+"="+value+expires+"; path=/";
            }

            function readCookie(name) {
            	var nameEQ = name + "=";
            	var ca = document.cookie.split(';');
            	for(var i=0;i < ca.length;i++) {
            		var c = ca[i];
            		while (c.charAt(0)==' ') c = c.substring(1,c.length);
            		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            	}
            	return null;
            }

            function eraseCookie(name) {
            	createCookie(name,"",-1);
            }

            $('.sort').dropkick();
		},

		addOne : function (model) {
			var self = this;
			var viewSummary = new Summary({model:model});
			$('#mainSummary').append(viewSummary.render());
		},

		loadCartSummary : function () {
			var self = this;
			var fetchedVal = self.collection.cartSummaryfetched;
			if(fetchedVal == false){
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				self.collection.updateCartSummary(self.cartCollection.models);
				self.collection.updateCartView();
				//Invoke Tile layout styling method:
				this.addingStyleToMiddleTile();
			}
		},

		addingStyleToMiddleTile: function(){
        	/*
        		Clear the tiles after every 3rd Tile and add after new clearfix element to clear the float layout
        		and css class to every second tile:
        	*/
			//console.log('fired from planview js');
//			$('#mainSummary').find('.jsTilesClearFix').remove();
//			$('#mainSummary .tile').css({marginLeft: 0, marginRight:0}); //Required to overwrite .tile:nth-child(3n-1) from css as it is using on other pages
//			$('#mainSummary .tile:nth-of-type(2n)').removeClass('middleTile'); //First remove clearfix and middleTile class as event fires twice so needed these code
//        	$('#mainSummary .tile:nth-of-type(3n)').after('<div class="clearfix jsTilesClearFix"></div>');
//        	$('#mainSummary .tile:nth-of-type(2n)').addClass('middleTile');
		},

		addToCart: function (e) {
			var self = this;
			var cartModel = "";
            var cartEleId = e.target.id;
            if (e.target.className.length === 0 || e.target.className.indexOf('icon') >= 0) {
                cartEleId = $(e.target).parent().attr('id');
            }

            var pageName = document.title;
			if (pageName === undefined) {
				pageName ="Tile";
			}

			itemId = cartEleId.replace("cart_","");


			//Remove From Cart

			if($("#" + cartEleId).hasClass('cp-tile__item--selected')) {
				if ($('#shoppingType').val() == 'POSTSHOPPING'){
					$('#cart-box').show();
				}
				$('.js-planselection-loading-remove').show();
				cartModel = self.collection.searchPlan(self.cartCollection.models, itemId);
				planModel = self.collection.searchPlan(this.collection.models, itemId);
				var planName = planModel.get("name");
				var planInfo = planModel.get('issuer') + ' | ' + planModel.get("name") + ' | ' + planModel.get('level') + ' - ' + pageName;
				orgId = cartModel.id;
				cartModel.set({id: cartModel.get("orderItemId")});
				cartModel.destroy({
					dataType: "text",
					success : function (cartModel, response) {
                        $('.js-planselection-loading-remove').hide();
						cartModel.set({id: orgId});
						self.cartCollection.remove(cartModel);
						self.collection.updateCartSummary(self.cartCollection.models);
						window.dataLayer.push({
						  'event': 'removeFromCart',
  						  'eventCategory': 'Plan Selection - Plan Tiles Events',
  						  'eventAction': 'Remove from Cart',
  						  'eventLabel': planInfo,
						  'ecommerce': {
							'remove': {
							  'products': [{
								'id': planModel.attributes.issuerPlanNumber,
								'name': planModel.attributes.name,
								'brand': planModel.attributes.issuer,
								'category': planModel.attributes.level,
								'variant': planModel.attributes.networkType,
								'price': planModel.attributes.premium
							   }]
						   	}
						  }
					  });
					}
				});

				if(cartModel.get("planType") == "DENTAL"){
				  $('.changeLink').addClass('active');
				  $('.changeLink').attr("data-original-title","");
				  $('.changeLink').attr("rel","");
				}

				if($("#cart_"+itemId).length){
					self.collection.updateCartButtonToAdd(itemId);
				}

			}
			//Add to Cart
			else {
				planModel = self.collection.searchPlan(this.collection.models, itemId);

				var planAddedForGroup = self.collection.searchByPlanType(self.cartCollection.models,planModel.get("planType"));

				if(planAddedForGroup == false){
					self.collection.updateCartButtonToRemove(itemId);
					$('.js-planselection-loading-add').show();
					this.cartModel.save({id: planModel.get("planId"), planId: planModel.get("planId"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), premiumAfterCredit: planModel.get("premiumAfterCredit"), planDetailsByMember: planModel.get("planDetailsByMember"), aptc: planModel.get("aptc"), stateSubsidy: planModel.get("stateSubsidy"), contributionData: planModel.get("contributionData"), encodedPremium: planModel.get("encodedPremium"), doctors: planModel.get("doctors"),benefitsCoverage: planModel.get("benefitsCoverage"),expenseEstimate: planModel.get("expenseEstimate")},
					{
						success : function (model,resp) {
                            $('.js-planselection-loading-add').hide();
							var newModel = new  App.models.Cart({planType: planModel.get("planType"), planId: planModel.get("planId"), orderItemId:model.id});
							self.cartCollection.add(newModel);
							self.collection.updateCartSummary(self.cartCollection.models);
							// Add cart item in cart array

							$('.removeFromCart').bind('click', self.addToCart);

							var planName = planModel.get("name");
							$("#PlanName").text(planName);

							var planStatus = self.showShoppingPopup(self.cartCollection.models);
							if (planStatus === "hasDental") {
								$('#dentalButton').remove();
								if($('#productType').val() == 'D'){
									$('#healthButton').remove();
									$('#healthdentalButton').html('<button type="button" class="btn btn-default btn-primary btn_res txt-center aid-continueCart" id="aid-continueCart_01" data-automation-id="continueCartBtn">'+ plan_display['pd.label.title.continueToCart'] +'</button>');
								}else{
									$('#healthButton').html('<button type="button" id="aid-continueHealthPlan" data-automation-id="continueHealthPlan" class="btn btn-default btn-primary btn_res txt-center pull-left">'+plan_display['pd.label.title.continueToHealthPlans']+'</button> <p class="goToCart pull-right"><a class="pull-right margin10-t gtm_continue_cart" id="aid_continueToCart_01" data-automation-id="continueToCart" href="'+ plan_display['url.showCart'] +'"><u class="uppercase">' + plan_display['pd.label.title.continueToCart'] +'</u></a></p>');
									$('#healthdentalButton').remove();
								}
							} else if (planStatus === "hasHealth") {
								if($("#isSeekingCoverage").val() != "false" && $('#productType').val() != 'H'){
									$('#dentalButton').html('<button type="button" id="aid-continueDentalPlan" data-automation-id="continueDentalPlan" class="btn btn-default btn-primary btn_res txt-center pull-left gtm_continue_dentalplans">'+plan_display['pd.label.title.continueToDentalPlans']+'</button> <p class="goToCart pull-right"><a class="pull-right margin10-t gtm_continue_cart" id="aid_continueToCart_02" href="'+ plan_display['url.showCart'] +'"><u class="uppercase">' + plan_display['pd.label.title.continueToCart'] +'</u></a></p>');
									$('#healthdentalButton').remove();
								}else{
									$('#dentalButton').remove();
									$('#healthdentalButton').html('<button type="button" class="btn btn-default btn-primary btn_res txt-center aid-continueCart gtm_continue_cart" id="aid-continueCart_02" data-automation-id="continueCartBtn">'+ plan_display['pd.label.title.continueToCart'] +'</button>');
								}

								$('#healthButton').remove();
							} else if (planStatus === "hasBoth") {
								$('#healthdentalButton').html('<button type="button" class="btn btn-default btn-primary btn_res txt-center aid-continueCart" id="aid-continueCart_03" data-automation-id="continueCartBtn">'+ plan_display['pd.label.title.continueToCart'] +'</button>');
								$('#healthButton').remove();
								$('#dentalButton').remove();
							}

							$('#shoppingPop-modal').modal('show');

							if(planModel.get("planType") == "DENTAL"){
								$('.changeLink').removeClass('active');
								$('.changeLink').attr("rel","tooltip");
								$('.changeLink').attr("data-original-title",plan_display['pd.label.title.removeDental']);
							}

							var providersInnetworkCount = 0;
							var providersModel = planModel.attributes.doctors;
							if(providersModel.length > 0){
								_.each(providersModel, function(providersModel) {
									if(providersModel.networkStatus == "inNetWork") {
										providersInnetworkCount++;
									}
								});
							}
							
							// if($('#stateCode').val() == 'ID'){
							// 	var planInfo = planModel.get('issuer') + ' | ' + planModel.get("name") + ' | ' + planModel.get('level') + ' - ' + pageName;
							// 	planInfo += ', ' +  planModel.get('expenseEstimate') + ' Estimate ';
							// 	planInfo += ' | ' + $('#medicalUse').val() + ' Medical Utilization, ';
							// 	planInfo += ' | ' + $('#prescriptionUse').val() + ' Prescription Utilization';
							// } 
							// else {
							// 	var planInfo = planModel.get('issuer') + ' | ' + planModel.get("name") + ' | ' + planModel.get('level') + ' - ' + pageName;
							// }

							var planInfo = planModel.get('issuer') + ' | ' + planModel.get("name") + ' | ' + planModel.get('level') + ' - ' + pageName;


							var providerSearchLbl;
							if(providersModel.length >= 1) {
								if (providersInnetworkCount == 0) {
									providerSearchLbl = "Unsuccessful" + '| Providers (' + providersInnetworkCount + '/' + providersModel.length + ') were covered';
								}
								if (providersInnetworkCount != 0 && providersInnetworkCount >= 1 && providersInnetworkCount < providersModel.length) {
									providerSearchLbl = "Partial Success" + '| Providers (' + providersInnetworkCount + '/' + providersModel.length + ') were covered';
								}
								if (providersInnetworkCount == providersModel.length) {
									providerSearchLbl = "Success" + '| Providers (' + providersInnetworkCount + '/' + providersModel.length + ') were covered';
								}
							} else {
								providerSearchLbl = "Null" + '| Providers (' + providersInnetworkCount + '/' + providersModel.length + ') were covered';
							}

							window.dataLayer.push({
							'event': 'addToCart',
  							  'eventCategory': 'Plan Selection - Plan Tiles Events',
  							  'eventAction': 'Add to Cart',
  							  'eventLabel': planInfo,
							  'providerSearch': providerSearchLbl,
							  'ecommerce': {
							    'add': {
							      'products': [{
									'id': planModel.attributes.issuerPlanNumber,
							        'name': planModel.attributes.name,
							        'brand': planModel.attributes.issuer,
							        'category': planModel.attributes.level,
							        'variant': planModel.attributes.networkType,
							        'price': planModel.attributes.premium
							       }]
							    }
							  }
							});
						}
					});
				} else {
					$('#cart-error').modal('show');
				}
			}
		},

		// Called when clicking on tile 'Compare' checkbox
		compare: function (e) {

			var planModel = this.collection.searchPlan(this.collection.models, e.target.parentElement.nextElementSibling.getAttribute('data-plan-id'));
			var planInfo = planModel.get('issuer')+ ' | ' + planModel.get('name') + ' | ' + planModel.get('level');

			if(!$(e.target).closest('label').hasClass('cp-tile__item--selected')) {
				
				this.addToCompare(e);			

				// Add to Cart Event on checkbox click
				window.dataLayer.push({
					'event': 'compareEvent', 
					'eventCategory': 'Plan Selection - Compare', 
					'eventAction': 'Compare Selection', 
					'eventLabel': planInfo
				});
			}

			else {

				this.removeCompare(e);

				// Remove from Cart Event on checkbox click
				window.dataLayer.push({
					'event': 'compareEvent', 
					'eventCategory': 'Plan Selection - Compare', 
					'eventAction': 'Remove from Compare', 
					'eventLabel': planInfo
				});

			}
		},

		addToCompare: function (e) {
			if(this.collection.compareModels.length == 3){
				$(e.target).prop('checked', false);
				$('#more-than-four').modal('show');
				return false;
			}

			var planId = e.target.id.split('_')[1];
			this.collection.addToCompare(planId);
			$('#compareCount').html(this.collection.compareModels.length);

			$(e.target).closest('label').addClass('cp-tile__item--selected');

			$('.js-plan-compare-widget').show();

			App.views.compareView.compareBox();
		},

		removeCompare: function (e) {
			// var self=this;
			// var textToReplace = "remove_";
			// if(e.target.id.indexOf('emoveChk_') >= 0){
			// 	textToReplace = "removeChk_";
			// }

			var planId = e.target.id.split('_')[1];
			/* if not compare view*/
			if(this.collection.showCompare == false){
				this.collection.removeCompare(planId, "pview");
//				var selid = e.target.id.replace(textToReplace,"");
//				$('#compare_'+selid).show();
//				$('#remove_'+selid).hide();
//				$('#compareLbl_'+selid).show();
//				$('#removeLbl_'+selid).hide();
//				$('#compareChk_'+selid).removeAttr('checked');
//				$('#removeChk_'+selid).removeAttr('checked');
//				$('#removeLbl_'+selid).parents('.tile').removeClass('compared-selected');
//				$('#removeLbl_'+selid).parents('.input-append').removeClass('compared');

				$('#compareChk_' + planId).prop('checked', false);
				$('label[for="compareChk_' + planId + '"]').removeClass('cp-tile__item--selected');
			}
			else{ /*if from compare view*/
				this.collection.removeCompare(e.target.id.replace(textToReplace,""),"cview");
				/*if no items in compare array*/
				if(this.collection.compareModels.length < 1)
					this.collection.showCompare == false;
			}
			this.collection.updateCompareSummary();
			App.views.compareView.compareBox();

		},

		getDisclaimersInfo : function(){
		  var self = this;
	      var planModels = self.collection.models;
	      var issuers = new Object();
	      var issueridString = "";
	      var policyLengthArr = [];
	      var issuerNameArr = [];
	      var networkTypeArr = [];
	      var deductibleRangeArr = [];
	      var catastrphicPlansBoolean = "false";
	      if (planModels.length)
    	  {
		    	// --- show other disclaimsers too ---
		    	$('.benefits-summary-disclaimer').show();
		    	if($('#insuranceType').val() != 'DENTAL' && $('#showQualityRating').val() == 'YES')
		    	{
		    		$('.qrs-disclaimer').show();
				}
    	  }
	      if(planModels){
	        _.each(planModels, function(planModel){
	          if(!issuers[planModel.get("issuerId")] || issuers[planModel.get("issuerId")] == "" ) {
	            issuers[planModel.get("issuerId")] = planModel.get("issuer");
	            issuerNameArr.push(planModel.get("issuer"));

	            if(issueridString == ""){
	              issueridString = planModel.get("issuerId");
	            }else{
	              issueridString += "," + planModel.get("issuerId");
	            }
	          }

	          if( jQuery.inArray(planModel.get("policyLength"),policyLengthArr) === -1 ) {
	            policyLengthArr.push( planModel.get("policyLength"));
	          }
	          if( jQuery.inArray(planModel.get("networkType"),networkTypeArr) === -1 ) {
	            networkTypeArr.push( planModel.get("networkType"));
	          }
	          if( planModel.get("level") === 'CATASTROPHIC' ) {
	        	  	catastrphicPlansBoolean = "true";
		      }
	          if(planModel.get("deductible") <= 2501 && jQuery.inArray(2500,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(2500);
		      }
	          if(planModel.get("deductible") > 2501 && planModel.get("deductible") <= 5001 && jQuery.inArray(5000,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(5000);
			  }
	          if(planModel.get("deductible") > 5001 && planModel.get("deductible") <= 7501 && jQuery.inArray(7500,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(7500);
		      }
	          if(planModel.get("deductible") > 7501 && planModel.get("deductible") <= 10001 && jQuery.inArray(10000,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(10000);
			  }
	          if(planModel.get("deductible") > 10001 && planModel.get("deductible") <= 12001 && jQuery.inArray(12000,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(12000);
		      }
	          if(planModel.get("deductible") > 12001 && planModel.get("deductible") <= 15001 && jQuery.inArray(15000,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(15000);
			  }
	          if(planModel.get("deductible") > 15001 && planModel.get("deductible") <= 17501 && jQuery.inArray(17500,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(17500);
			  }
	          if(planModel.get("deductible") > 17501 && planModel.get("deductible") <= 20001 && jQuery.inArray(20000,deductibleRangeArr) === -1 ) {
	        	  	deductibleRangeArr.push(20000);
			  }
	        });

	        var sortedIssuers = [];
	        for (var issuerId in issuers){
	          sortedIssuers.push({"id":issuerId, "name":issuers[issuerId]});
	        }
	        sortedIssuers.sort(function(a, b) { return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : a.name > b.name ? 1 : 0; });
	        if (sortedIssuers.length) {
				var carrierHtml = '';
				_.each(sortedIssuers, function(issuer){
					carrierHtml += '<div class="ps-form__check">' +
						'<input type="checkbox" class="carrier_filter_checkbox gtm_filter ps-form__check-input" name="planFeature" data-filter-label="'+ issuer.name +'" value=\''+issuer.id+'\' id="carrier_filter_checkbox_'+issuer.id+'"/>' +
						'<label for="carrier_filter_checkbox_'+issuer.id+'" class="ps-form__check-label"> ' + issuer.name + ' </label>' +
						'</div>';

					$('body').on('change', 'input.carrier_filter_checkbox', self.filter);
				});

				$('#carrierFilter').append(carrierHtml);
	        }

	        if (policyLengthArr.length) {
	          policyLengthArr.sort();
	          _.each(policyLengthArr, function(policyLength){
	            $('#maxPolicyLenFilter').append('<div class="control"><label class="checkbox" for="maxpolicy_filter_checkbox_'+policyLength+'"><input id="maxpolicy_filter_checkbox_'+policyLength+'" class="maxpolicy_filter_checkbox"  type="checkbox" value=\''+policyLength+'\'>'+policyLength+'</label> </div>');
	            $('body').on('change', 'input.maxpolicy_filter_checkbox', self.filter);
	          });
	        }
	        if (networkTypeArr.length) {
               _.each(networkTypeArr, function(nwTypeLength){

				   var planType = '<div class="ps-form__check"><input id="plantype_filter_checkbox_'+nwTypeLength+'" class="plantype_filter_checkbox gtm_filter ps-form__check-input" type="checkbox" data-filter-label=\''+nwTypeLength+'\' value=\''+nwTypeLength+'\'>';

				   planType +='<label class="ps-form__check-label" for="plantype_filter_checkbox_' + nwTypeLength+'">';

				   if($('#stateCode').val() !== 'ID'){
					   if(nwTypeLength.toUpperCase() === 'PPO'){
						   planType += '<a aria-label="Help text '+plan_display['pd.label.script.networkType.ppo']+' Help text finished." data-original-title="' +   plan_display['pd.label.script.networkType.ppo']  + '" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#">';
					   }else if(nwTypeLength.toUpperCase() === 'HMO'){
						   planType += '<a aria-label="Help text '+plan_display['pd.label.script.networkType.hmo']+' Help text finished." data-original-title="' +   plan_display['pd.label.script.networkType.hmo']  + '" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#">';
					   }else if(nwTypeLength.toUpperCase() === 'EPO'){
						   planType += '<a aria-label="Help text '+plan_display['pd.label.script.networkType.epo']+' Help text finished." data-original-title="' +   plan_display['pd.label.script.networkType.epo']  + '" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#">';
					   }else if(nwTypeLength.toUpperCase() === 'POS'){
						   planType += '<a aria-label="Help text '+plan_display['pd.label.script.networkType.pos']+' Help text finished." data-original-title="' +   plan_display['pd.label.script.networkType.pos']  + '" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#">';
					   }
				   }
				   if($('#stateCode').val() === 'MN'){
					   if(nwTypeLength.toUpperCase() === 'INDEMNITY'){
						   planType += '<a aria-label="Help text '+plan_display['pd.label.script.networkType.indemnity']+' Help text finished." data-original-title="' +   plan_display['pd.label.script.networkType.indemnity']  + '" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#">';
					   }
					}

				   planType += nwTypeLength;

				   if($('#stateCode').val() !== 'ID'){
					   planType += '</a>';
				   }

				   planType += '</label></div>';

				   $('#planTypeFilter').append(planType);

                   $('body').on('change', 'input.plantype_filter_checkbox', self.filter);
               });
            }

	        if($('#insuranceType').val() != 'DENTAL' && deductibleRangeArr.length) {
	        		deductibleRangeArr.sort(function(a, b){return a - b});
	        		_.each(deductibleRangeArr, function(deductibleRange){
        			  var html = '<div class="ps-form__check">'
        				+ '<input type="checkbox" class="deductible_filter_checkbox gtm_filter ps-form__check-input" value="' + deductibleRange + '" data-filter-label="$' + deductibleRange + ' ' + plan_display['pd.label.script.deductible.andLess'] + '" id="deductible_filter_' + deductibleRange + '"/>'
        				+ '<label for="deductible_filter_' + deductibleRange + '" class="ps-form__check-label">'
        				+ '$' + deductibleRange + ' ' + plan_display['pd.label.script.deductible.andLess']
	    				+ '</label>'
	    				+ '</div>';
		            $('#deductibleRangeFilter').append(html);
		            $('body').on('change', 'input.deductible_filter_checkbox', self.filter);
		        });
	        }
	        if(issueridString !== ""){
	          $.ajax({
	            type : "POST",
	            url : "getDisclaimersInfo",
	            data:{"issuersString": issueridString,"csrftoken": $('#csrfToken').val()},
	            dataType:'json',
	            success : function(response) {
	              $('#carrierInfo').empty();
	              $.each(response,function(index,value){
	                if(value != null && value != ""){
	                  $('#carrierInfo').append('<div><h4>'+issuers[index]+'</h4><p>'+value+'</p></div>');
	                }
	              });
	              if($('#carrierInfo').html() != ""){
	                $('.carrier-disclaimers').show();
	              }
	            },
	          });
	        }
	        if(catastrphicPlansBoolean === "true"){
	        	$('#catastrphicFilter').show();
	        }
	      }
	    },

		getFilterRules : function() {
		  var matchLevel = new Array();
		  var matchIssuer = new Array();
		  var matchPlantype = new Array();
		  var hsaPlantype = new Array();
		  var matchDeductible = new Array();
		  var matchPolicyLength = new Array();
		  var csrEligible = new Array();
		  var matchQualityRating = new Array();
		  var matchDoctors = new Array();
		  var matchPlanId = new Array();

		  $(".carrier_filter_checkbox").each(function() {
	        if (this.checked) {
	          matchIssuer.push(Number($(this).val()));
	        }
	      });

	      $(".deductible_filter_checkbox").each(function() {
	        if (this.checked) {
	        	  if($('#insuranceType').val() != 'DENTAL') {
	        		  matchDeductible.push(Number($(this).val()));
	        	  } else {
	        		  var valueRange = $(this).val().split("-");
	    	          var valueRangeObj = new Object();
	    	          valueRangeObj['min'] = valueRange[0];
	    	          valueRangeObj['max'] = valueRange[1];
	    	          matchDeductible.push(valueRangeObj);
	        	  }
	        }
	      });

		  $(".filter_checkbox").each(function() {
			if (this.checked) {
			  matchLevel.push($(this).val());
			}
		  });

		  $(".plantype_filter_checkbox").each(function() {
			if (this.checked) {
			  matchPlantype.push($(this).val());
		    }
	      });

		  $(".plantype_filter_hsa").each(function() {
			if (this.checked) {
			  hsaPlantype.push($(this).val());
			}
	      });

		  $(".plantype_filter_csr").each(function() {
			 if (this.checked) {
				csrEligible.push($(this).val());
			  }
		    });

		  $(".maxpolicy_filter_checkbox").each(function() {
	        if (this.checked) {
	          matchPolicyLength.push($(this).val());
	        }
	      });

		  $(".doctor_filter_checkbox").each(function() {
			if (this.checked) {
				matchDoctors.push($(this).val());
			}
		  });

		  $(".qualityrating_filter_checkbox").each(function() {
			if (this.checked) {
				var valueRange = $(this).val().split("-");
			    var valueRangeObj = new Object();
			    valueRangeObj['min'] = valueRange[0];
			    valueRangeObj['max'] = valueRange[1];
			    matchQualityRating.push(valueRangeObj);
			}
		  });


		  var rules = new Object();
			matchPlanId.push(1);

			if(matchPlanId.length > 0){
			  rules['planId'] = this.buildFilterRule('min', 'planId', matchPlanId);
			}
			if(matchIssuer.length > 0){
			  rules['issuerId'] = this.buildFilterRule('equalTo', 'issuerId', matchIssuer);
			}
			if(matchLevel.length > 0){
			  rules['level'] = this.buildFilterRule('equalTo', 'level', matchLevel);
			}
			if(matchPlantype.length > 0){
			  rules['planType'] = this.buildFilterRule('equalTo', 'networkType', matchPlantype);
			}
			if(matchPolicyLength.length > 0){
			  rules['policyLength'] = this.buildFilterRule('equalTo', 'policyLength', matchPolicyLength);
			}
			if(hsaPlantype.length > 0){
			  rules['hsa'] = this.buildFilterRule('equalTo', 'hsa', hsaPlantype);
			}
			if(csrEligible.length > 0){
				  rules['costSharing'] = this.buildFilterRule('compare', 'costSharing', csrEligible);
			}
			if(matchDeductible.length > 0){
				if($('#insuranceType').val() != 'DENTAL') {
					var maxDeductible = Math.max.apply(Math, matchDeductible);
					rules['deductible'] = this.buildFilterRule('max', 'deductible', maxDeductible);
				} else {
					rules['deductible'] = this.buildFilterRule('range', 'deductible', matchDeductible);
				}
			}
			if(matchQualityRating.length > 0){
			  rules['qualityRating'] = this.buildFilterRule('range', 'qualityRating', matchQualityRating);
			}
			if(matchDoctors.length > 0){
				rules['doctors'] = this.buildFilterRule('jsonObject', 'doctors', matchDoctors);
			}
			return rules;
		},

		buildFilterRule : function(filterType, filterField, filterValue) {
		  var nameObj = new Object();
		  nameObj['type'] = filterType;
		  nameObj['field'] = filterField;
		  nameObj['value'] = filterValue;
		  return nameObj;
		},

		filter : function(e) {
			//e.preventDefault();
			var rules = this.getFilterRules();
			this.collection.setFieldFilter(rules);
			this.collection.preserveOtherOptions();

			//Invoke Tile layout styling method:
			this.addingStyleToMiddleTile();
		},

		dentalCustomGroupUpdate:function() {
			var result = this.validateCustomDentalGroup();
			if(result==false) {
				return result;
			}

			var dataStr=this.getSeekingCoverageDataString();
			$.ajax({
				  type: "POST",
				  url: '/hix/private/updateSeekCoverageDental',
				  data: {dataString:dataStr,"csrftoken": $('#csrfToken').val()},
				  dataType: "text",
				  success:function( response ) {
				    if(response!=null && response=="SUCCESS"){
			       		location.href="planselection?insuranceType=DENTAL&closedPopup=Y";
				    } else {
					    $('#shopping-modal-error').show();
						$('#shopping-modal-error').html(plan_display['pd.label.errorProcessingData']);
					}
				  }
			});
			return false;
		},

		getSeekingCoverageDataString:function(){

			var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
			var dataStr="";
			var seekingCoverageDental="";
			for(var i = 0; i < seekingCoverageDentalObj.length; i++) {
				if($('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==true ) {
					seekingCoverageDental="Y";
				} else {
					seekingCoverageDental="N";
				}

				if(dataStr=="") {
					dataStr=seekingCoverageDentalObj[i].personId+"="+seekingCoverageDental;
				} else {
					dataStr=dataStr+","+seekingCoverageDentalObj[i].personId+"="+seekingCoverageDental;
				}
			}
			return dataStr;
		},

		validateCustomDentalGroup:function() {
			var count =$('input[name=chkSCoverage]:checked').length;

			if(count==0) {
				$('#shopping-modal-error').show();
				$('#shopping-modal-error').html(plan_display['pd.label.selectOneMember']);
				return false;
			}


			var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
			var isAnyMemberOlderThan_19=false;
			var isSelfUnchecked=false;
			var age=0;
			for(var i = 0; i < seekingCoverageDentalObj.length; i++) {
				if(seekingCoverageDentalObj[i].relationship=="Self" && $('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==false ) {
					isSelfUnchecked=true;
				}

				age=parseInt( seekingCoverageDentalObj[i].age,10);

				if(seekingCoverageDentalObj[i].relationship!="Self" && age>19 && $('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==true ) {
					isAnyMemberOlderThan_19=true;
				}
			}

			if(isSelfUnchecked==true && isAnyMemberOlderThan_19==true ) {
				$('#shopping-modal-error').show();
				$('#shopping-modal-error').html(plan_display['pd.label.adultDentalSelection']);
				return false;
			}

			return true;
		},

		showShoppingPopup : function (cartModels) {
			var hasDental = "false";
			var hasHealth = "false";
			if(cartModels.length > 0){
				_.each(cartModels, function(existingCartModel){
					if(existingCartModel.get("planType") == "DENTAL"){
						hasDental = "true";
					}
					else if(existingCartModel.get("planType") == "HEALTH"){
						hasHealth = "true";
					}
				});
			}
			if(hasDental === "true" && hasHealth === "true") {
				return "hasBoth";
			} else if(hasDental === "true") {
				return "hasDental";
			} else if(hasHealth === "true") {
				return "hasHealth";
			}
		}
	});

	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateMainSummary').html()),

		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		render : function () {
			return this.template(this.model.toJSON());
		}

	});
})(App.views);
