//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for an
 * 				assessment of a person's suitability to participate in an exchange
 * 				program based on various criteria.
 * 
 * <p>Java class for ExchangeEligibilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExchangeEligibilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-ee}EligibilityType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ExchangeQHPResidencyEligibilityBasis" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ExchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ExchangeIncarcerationEligibilityBasis" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SpecialEnrollmentPeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeEligibilityType", propOrder = {
    "exchangeQHPResidencyEligibilityBasis",
    "exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis",
    "exchangeIncarcerationEligibilityBasis",
    "specialEnrollmentPeriod"
})
public class ExchangeEligibilityType
    extends EligibilityType
{

    @XmlElement(name = "ExchangeQHPResidencyEligibilityBasis")
    protected MedicaidResidencyEligibilityBasisType exchangeQHPResidencyEligibilityBasis;
    @XmlElement(name = "ExchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis")
    protected EligibilityBasisType exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis;
    @XmlElement(name = "ExchangeIncarcerationEligibilityBasis")
    protected EligibilityBasisType exchangeIncarcerationEligibilityBasis;
    @XmlElement(name = "SpecialEnrollmentPeriod")
    protected SpecialEnrollmentPeriodType specialEnrollmentPeriod;

    /**
     * Gets the value of the exchangeQHPResidencyEligibilityBasis property.
     * 
     * @return
     *     possible object is
     *     {@link MedicaidResidencyEligibilityBasisType }
     *     
     */
    public MedicaidResidencyEligibilityBasisType getExchangeQHPResidencyEligibilityBasis() {
        return exchangeQHPResidencyEligibilityBasis;
    }

    /**
     * Sets the value of the exchangeQHPResidencyEligibilityBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicaidResidencyEligibilityBasisType }
     *     
     */
    public void setExchangeQHPResidencyEligibilityBasis(MedicaidResidencyEligibilityBasisType value) {
        this.exchangeQHPResidencyEligibilityBasis = value;
    }

    /**
     * Gets the value of the exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis property.
     * 
     * @return
     *     possible object is
     *     {@link EligibilityBasisType }
     *     
     */
    public EligibilityBasisType getExchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis() {
        return exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis;
    }

    /**
     * Sets the value of the exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link EligibilityBasisType }
     *     
     */
    public void setExchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis(EligibilityBasisType value) {
        this.exchangeVerifiedCitizenshipOrLawfulPresenceEligibilityBasis = value;
    }

    /**
     * Gets the value of the exchangeIncarcerationEligibilityBasis property.
     * 
     * @return
     *     possible object is
     *     {@link EligibilityBasisType }
     *     
     */
    public EligibilityBasisType getExchangeIncarcerationEligibilityBasis() {
        return exchangeIncarcerationEligibilityBasis;
    }

    /**
     * Sets the value of the exchangeIncarcerationEligibilityBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link EligibilityBasisType }
     *     
     */
    public void setExchangeIncarcerationEligibilityBasis(EligibilityBasisType value) {
        this.exchangeIncarcerationEligibilityBasis = value;
    }

    /**
     * Gets the value of the specialEnrollmentPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialEnrollmentPeriodType }
     *     
     */
    public SpecialEnrollmentPeriodType getSpecialEnrollmentPeriod() {
        return specialEnrollmentPeriod;
    }

    /**
     * Sets the value of the specialEnrollmentPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialEnrollmentPeriodType }
     *     
     */
    public void setSpecialEnrollmentPeriod(SpecialEnrollmentPeriodType value) {
        this.specialEnrollmentPeriod = value;
    }

}
