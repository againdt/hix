
package com.getinsured.hix.batch.enrollment.external.nv.dto;

public class Telephone {

    private String telephoneNumber;
    private String telephonePriorityTypeCodeName;
    private String contactMethodTypeCodeName;

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephonePriorityTypeCodeName() {
        return telephonePriorityTypeCodeName;
    }

    public void setTelephonePriorityTypeCodeName(String telephonePriorityTypeCodeName) {
        this.telephonePriorityTypeCodeName = telephonePriorityTypeCodeName;
    }

    public String getContactMethodTypeCodeName() {
        return contactMethodTypeCodeName;
    }

    public void setContactMethodTypeCodeName(String contactMethodTypeCodeName) {
        this.contactMethodTypeCodeName = contactMethodTypeCodeName;
    }
}
