<div class="span6 info-block">
	<div class="gutter10">
		<div class="header"><h4>Coverage Summary</h4></div>
       	<div class="widget">
        	<h5 class="header-secondary" >$coverageMsg$</h5>
        	
        	<div id="plan-summary" class="gutter10 tile">
          		<p>You can decide what level of coverage to offer and what portion of the premiums you'll contribute.</p>
                <div class="data-display well-noborder">
                     <dl class="dl-horizontal">
                        <dt>Plan Level</dt>
                        <dd>$planTier$</dd>				                        
                        <dt>Employee Contribution</dt>
                        <dd>$empContribution$%</dd>
                        <dt>Dependent Contribution</dt>
						<dd>$depeContribution$%</dd>
                        <dt>Coverage Effective Date</dt>
                        <dd>$coveargeEffDt$</dd>
                      </dl>
                </div> <!-- End /.well -->
          	</div>
       	</div> <!-- End /widget -->
	</div> <!-- End /.gutter10 -->
</div> 