package com.getinsured.eligibility.at.resp.si.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.PersonInfo;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.dto.Verification;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class FetchData {

	private static final Logger LOGGER = Logger.getLogger(FetchData.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	public ERPResponse fetchDataInt(Message<ERPResponse> message){

		ERPResponse erpResponse = message.getPayload();

		return fetchData(erpResponse);
	}

	public ERPResponse fetchData(ERPResponse erpResponse) {

		List<TaxHouseholdMember> ssapApplicantLists = preProcess(erpResponse);
		erpResponse.setSsapApplicantList(ssapApplicantLists);
		return erpResponse;
	}

	private List<TaxHouseholdMember> preProcess(ERPResponse erpResponse) {

		// DTO
		List<TaxHouseholdMember> ssapApplicantLists = new ArrayList<>();

		Map<Long, SsapApplicant> ssapApplicantMap = fetchHousehold(erpResponse);

		List<SsapApplicant> ssapApplicantList = (ssapApplicantMap!= null) ? new ArrayList<>(ssapApplicantMap.values()) : null;
		/*if (ssapApplicantMap == null){
			ssapApplicantList = Collections.emptyList();
		} else {
			ssapApplicantList = new ArrayList<>(ssapApplicantMap.values());
		}*/
		if(ssapApplicantList != null) {
		for (SsapApplicant ssapApplicantResource : ssapApplicantList) {
			if (StringUtils.equalsIgnoreCase(ssapApplicantResource.getApplyingForCoverage(), EligibilityConstants.Y)){
				PersonInfo personInfo = new PersonInfo();
				personInfo.setApplicantIdentificationId(ssapApplicantResource.getExternalApplicantId());


				personInfo.setPrimaryKey(ssapApplicantResource.getId());
				personInfo.setDob(ssapApplicantResource.getBirthDate());
				personInfo.setFirstName(ssapApplicantResource.getFirstName());
				personInfo.setMiddleName(ssapApplicantResource.getMiddleName());
				personInfo.setLastName(ssapApplicantResource.getLastName());
				personInfo.setSsn(ssapApplicantResource.getSsn());

				// Set SSAP_APPLICANT level verification indicators...
				Map<String, Boolean> personVerificationsMap = new HashMap<>();
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.SSN.toString(), isVerified(ssapApplicantResource.getSsnVerificationStatus()));
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.CITIZENSHIP.toString(), isVerified(ssapApplicantResource.getCitizenshipImmigrationStatus()));
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.ELIGIBLE_IMMIGRATION_STATUS.toString(), isVerified(ssapApplicantResource.getCitizenshipImmigrationStatus()));
				//personVerificationsMap.put(Verification.VERIFICATION_TYPE.ANNUAL_INCOME.toString(), ssapApplicantResource.());
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.CURRENT_INCOME.toString(), isVerified(ssapApplicantResource.getIncomeVerificationStatus()));
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.INCARCERATION_STATUS.toString(), isVerified(ssapApplicantResource.getIncarcerationStatus()));
				personVerificationsMap.put(Verification.VERIFICATION_TYPE.ESI_MEC.toString(), isVerified(ssapApplicantResource.getMecVerificationStatus()));
				//personVerificationsMap.put(Verification.VERIFICATION_TYPE.NON_ESI_MEC.toString(), ssapApplicantResource.());

				personInfo.setPersonVerificationsMap(personVerificationsMap);

				TaxHouseholdMember ssapApplicant = new TaxHouseholdMember();
				ssapApplicant.setPersonInfo(personInfo);


				/**
				 * Fetch Verification Data....
				 */

				/*if (personInfo.getPrimaryKey() != null){
					//List<SsapVerification> ssapVerificationsList = reseourceCreatorImpl.getVerificationsByApplicantId();

					// Transform SsapVerification DO to ERP Verification DTO...
					Map<String, Verification> finalVerificationDTOMap = new HashMap<>();

					ssapApplicant.setVerificationMap(finalVerificationDTOMap);

				}*/



				/**
				 * Fetch Eligibility indicators...
				 */
				/*if (personInfo.getPrimaryKey() != null){

					List<EligibilityProgramDTO> programEligibilties = fetchEligibilty(personInfo);

					Map<String, com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram> finalEligibiltyProgramMap = new HashMap<>();
					for (EligibilityProgramDTO eligibilityProgram : programEligibilties) {
						finalEligibiltyProgramMap.put(eligibilityProgram.getEligibilityType(), map(eligibilityProgram));
					}
					ssapApplicant.setEligibilityProgramMap(finalEligibiltyProgramMap);
				}*/

				// add to final list
				ssapApplicantLists.add(ssapApplicant);
			}
		}
		}
		return ssapApplicantLists;
	}

	private boolean isVerified(String status){
		return StringUtils.equalsIgnoreCase(status, "VERIFIED");
	}

	private Map<Long, SsapApplicant> fetchHousehold(ERPResponse erpResponse) {
		Map<Long, SsapApplicant> ssapApplicantMap = null;
		if(!StringUtils.isEmpty(erpResponse.getApplicationID())) {
			List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(erpResponse.getApplicationID());
			if (!ssapApplicationList.isEmpty()){
				SsapApplication ssapApplication = ssapApplicationList.get(0);
	
				List<com.getinsured.iex.ssap.model.SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
				if (!ssapApplicants.isEmpty()){
					ssapApplicantMap = new HashMap<>(ssapApplicants.size());
	
					for (SsapApplicant ssapApplicant : ssapApplicants) {
						ssapApplicantMap.put(ssapApplicant.getId() , ssapApplicant);
					}
	
					erpResponse.setSsapApplicationPrimaryKey(ssapApplication.getId());
				}  else {
					throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICANTS_IN_GI_TABLES + erpResponse.getApplicationID());
				}
	
			} else {
				throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + erpResponse.getApplicationID());
			}
		}
		if(erpResponse.getSsapApplicationPrimaryKey() == null || erpResponse.getSsapApplicationPrimaryKey() == 0)
		{
			erpResponse.setSsapApplicationPrimaryKey(erpResponse.getCompareToApplicationId());
		}
		return ssapApplicantMap;
	}

}

