(function (models) {
	models.dentalPlan = Backbone.Model.extend({
		
		urlRoot : "getDentalPlans",
		
		url : function () {
			if(this.id)
				return 'getDentalPlans/'+this.id+'?'+(Math.random()*10);
			else
				return 'getDentalPlans?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"planId"	: "",
			"name"		: "",
			"level"		: "",
			"premium"	: "",
			"premiumBeforeCredit" : "",
			"premiumAfterCredit" : "",
			"aptc" 			: "",
			"issuer"		: "",
			"issuerText"	: "",
			"networkType"	: "",
			"deductible"	: "",
			"groupId"		: "",
			"groupName"		: "",
			"groupMembers"	: "",
			"orderItemId"	: "",
			"providerLink" : "",
			"planDetailsByMember"	: "",
			"totalContribution" : "",
			"issuerLogo" : ""
		}
	}); 
})(App.models);