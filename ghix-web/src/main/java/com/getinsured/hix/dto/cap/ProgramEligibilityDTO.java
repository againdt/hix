package com.getinsured.hix.dto.cap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;

public class ProgramEligibilityDTO {

	public long ssapApplicationId;
	private String eligibilityStatus;
	private BigDecimal maximumAPTC;
	private String csrLevel;
	private String eligibilityStartDate;
	private String financialAssistanceFlag;

	private List<SsapApplicantDTO> applicantEligibility;

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public List<SsapApplicantDTO> getApplicantEligibility() {
		return applicantEligibility;
	}

	public void setApplicantEligibility(List<SsapApplicantDTO> applicantEligibility) {
		this.applicantEligibility = applicantEligibility;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(String eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}

	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}

}
