function skipFooterToRightpanel() {
    $(window).scrollTop($('#rightpanel').position().top);
    $('#rightpanel').attr("tabindex","0").focus();
}
$(document).ready(function () {

  //auto shift box for phone box and SSN
  $("input[maxlength=3],input[maxlength=4],input[maxlength=2]").bind("keyup",function(event){

    var keyCode = event.keyCode;
    if( keyCode == 16 || keyCode == 9 || keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40){
      return;
    }
    var maxLength = parseInt($(this).attr('maxlength'));
      var nextElement = $(this).nextAll( "input[maxlength]:first");

    if($(this).val().length == maxLength && nextElement !== undefined){

          setTimeout(function(){
              nextElement.focus();
              nextElement.select();
          },300);
    }

  });

  //Triggering click event to carousel's prev and next btns
  $('a[data-slide="next"], a[data-slide="prev"]').bind("keydown",function(e){
    if(e.keyCode == 32 || e.keyCode == 13){
      e.preventDefault();
      $(this).trigger('click');
      $('.active.item').focus();
    }
  });

  //add required to select combo box, JAWS doesn't read "required" for select combo box even it has a img with alt
  $("select").each(function(){
    var selectID = $(this).attr("id");
    $("label").each(function(){
      if($(this).find("img").attr("alt")=="Required!" && $(this).attr("for") == selectID ){
       // $(this).html($(this).html() + " <span class='aria-hidden'>required</span>");
      }
    });
  });


   //add place holder to calendar
  $(".date-picker input").each(function(){
    if(!$(this).attr("value")){
      $(this).attr("placeholder","MM/DD/YYYY");
    }
  });


  //JAWS is prevented from reading out "Graphic"
    $('img').each(function () {
        //clear the alt text
        //assign a variable the value of alt text
        var alttext = $(this).attr("alt");
        if (alttext == "Required!") //add other conditions as necessary
        {
            //make the Required asterisks hidden
            $(this).attr("aria-hidden", "true");
            //span with a label "Required!", such that it is read in place
            //of "Graphic Required"
            $('<a><span class="aria-hidden">Required</span></a>').insertAfter(this);
            var inpId = $(this).parent().attr("for");
            $("#" + inpId).attr("aria-required", "true");
        }
    });



  //read phone 1,2,3
  if($("#phone1")){
       var hasPhoneLabel = false;
       //if it has label, hasLabel is true
       $("label").each(function(){
         if($(this).attr("for") == "phone1"){
           hasPhoneLabel = true;
         }
       });


       //read required if there is require image
       var phoneRequired =  $("#phone1").closest(".control-group").find("img[alt^='Required']");
       var phoneRequiredText = "";
       if(phoneRequired.length > 0){
         phoneRequiredText = " required ";
       }

       if(!hasPhoneLabel){
       //if not, add label for it
         $("#phone1").before('<label for="phone1" class="aria-hidden">Phone first three digits'+ phoneRequiredText /*+ phoneTooltipText*/ +'</label>');
         //if no label for phone, that means no lables for phone2 and phone3
         $("#phone2").before('<label for="phone2" class="aria-hidden">Phone second three digits'+ phoneRequiredText +'</label>');
         $("#phone3").before('<label for="phone3" class="aria-hidden">Phone last four digits'+ phoneRequiredText +'</label>');
       }

     }


  //read SSN 1,2,3
  if($("#ssn1")){
     var hasSSNLabel = false;
     //if it has label, hasLabel is true
     $("label").each(function(){
       if($(this).attr("for") == "ssn1"){
         hasSSNLabel = true;
       }
     });


     //read required if there is require image
     var ssnRequired =  $("#ssn1").closest(".control-group").find("img[alt^='Required']");
     var ssnRequiredText = "";
     if(ssnRequired.length > 0){
       ssnRequiredText = " required ";
     }

     if(!hasSSNLabel){
       $("#ssn1").before('<label for="ssn1" class="aria-hidden">Tax ID Number first three digits'+ ssnRequiredText /*+ ssnTooltipText*/ + '</label>');
       $("#ssn2").before('<label for="ssn2" class="aria-hidden">Tax ID Number second two digits'+ ssnRequiredText + '</label>');
       $("#ssn3").before('<label for="ssn3" class="aria-hidden">Tax ID Number last four digits'+ ssnRequiredText + '</label>');
     }

  }

  //read tooltip content

  $('[rel="tooltip"]').each(function () {

	  	if($(this).attr('data-original-title')){
	  	    var tooltipTitle = $(this).text();
	  		var tooltiptext = $(this).attr('data-original-title').replace(/<\/?[^>]+(>|@=$)/g, "");
	  	    $(this).attr('aria-label', tooltipTitle + ' tooltip text: ' + tooltiptext);
	  	}

      $(this).focus(function(e){
          $(this).tooltip('show');
      });
      $(this).focusout(function(e){
          $(this).tooltip('hide');
          return false;
      });
  });

  //read page
    $(".pagination a").each(function () {
        //find the numerical value, make sure that it is greater
        //than 0 so it's a regular page number
        var thisval = parseInt($(this).text());
        if (thisval > 0) {
            var itxt = $(this).html();
            //if current page, (.active class), add "current page" to description
            if($(this).parent().hasClass("active") || $(this).parent().parent().hasClass("active"))
            {
                itxt = "<span class='aria-hidden'>Page </span>" + itxt + "<span class='aria-hidden'> Current Page </span>";
            }
            //regular description, "Page X"
                else
            {
                    itxt = "<span class='aria-hidden'>Page </span>" + itxt;
            }
            $(this).html(itxt);
        }
    });


    //add sortable span to link in table
    $('th.sortable a').each(function(){
      var linkConten=$(this).text();
      if($('th a[href*="ASC"]').length>0){
        $(this).html(linkConten + '<span class="offScreen">Sortable Ascending</span>');
      }else if($('th a[href*="DESC"]').length>0){
        $(this).html(linkConten + '<span class="offScreen">Sortable Descending</span>');
      }else{
        $(this).html(linkConten + '<span class="offScreen">Sortable</span>');
      }
    });


    //Dropdown fixes, so that JAWS reads it's a dropdown menu and use can press enter to open it and tab through its options
    $("[data-toggle='dropdown']").each(function () {
       $(this).append('<span aria-hidden="true" class="hide"> Dropdown Menu. Press enter to open it and tab through its options</span>');
       var lastItem = $(this).next(".dropdown-menu").find("li").last().find("a");
       lastItem.html(lastItem.html() + "<span class='aria-hidden'> End of dropdown</span>");
     });

    //gear dropdown in table, should be added after Dropdown fixes
    $("table tr").each(function(){
      var lastTdwithDropdown = $(this).find("td").last().find("a.dropdown-toggle span.aria-hidden");
      lastTdwithDropdown.text("Update User Infomation " + lastTdwithDropdown.text());
    });

    setTimeout(function () {
    //read check mark ok as "yes" for address
    $("i.icon-ok").each(function(){
       $(this).append("<span class='aria-hidden'>Yes</span>");
    });
  },500);

    /*for readonly fields*/
    $("input[readonly]").removeAttr("aria-required").attr("aria-disabled","true");

    //for disable link in dashboard
    $("li.disabled a").attr("aria-disabled","true");

    //Read buttons and links in header
  /*  $('div.header').each(function () {
      if($(this).find("input.btn").length){
        var thisHeaderText = $(this).find('h4').text();
        $(this).child('h4').wrapAll("<fieldset></fieldset>").prepend("<legend class='aria-hidden'>"+ thisHeaderText +"</legend>");
      }else if($(this).find("a.btn").length){
        $(this).find('a.btn').each(function () {
          var thisHeader = $(this).closest('div.header, div.graydrkaction').find('h4').text();
            var button = $(this).text();
            $(this).html(' <span class="aria-hidden">' + thisHeader +' ' + '</span>' + button);
        });
      }
    }); */


  //tab order for phone box
    $("input[name*='phone']").focus(function(){
     $(".input-mini[maxlength=3],.input-mini[maxlength=4],.input-small[maxlength=3],.input-small[maxlength=4]").attr("tabindex",0);
     $(".controls").find("input.input-mini[maxlength=3]:first, input.input-small[maxlength=3]:first").attr("tabindex",0);
    });

    $(".input-mini,.input-small").focus(function() {
        $(this).siblings(".input-mini,.input-small").attr("tabindex",0);
        $(this).parent().next().children(".input-mini,.input-small").attr("tabindex",0);
        $(this).parent().prev().children(".input-mini,.input-small").attr("tabindex",0);
    });

    //recaptcha in sign up page
    $("#recaptcha_reload,#recaptcha_switch_audio,#recaptcha_switch_img,#recaptcha_whatsthis").attr("tabindex",0);


  //Read description and placeholder
    /*$("input[type='text']").each(function(){
      var placeholderText = $(this).attr("placeholder");
      if(placeholderText){
        var inputLabel = $(this).attr("id");

        $("label[for="+inputLabel+"]").append("<span class='aria-hidden'> Sample Text " + placeholderText + "</span>");

      };
    });*/

  //Description and placeholder
    $("input[type='text'],select").not(".btn").each(function (index) {
        thisid = $(this).attr("id");
        //console.log($(this),'----',$(this).attr("name"), '******',$(this).attr("id"))
        if (typeof thisid === "undefined") {

            if($(this).attr("name") !== ""){
                //make an id:first based on name, then based on value(if button or checkbox), or based on type
                var newId = $(this).attr("name");
                if (typeof newId === "undefined") {
                    if ($(this).attr('type') == "button" || $(this).attr('type') == "checkbox") {
                        newId = $(this).attr("value");
                    } else {
                        newId = $(this).attr("type");
                    }
                }
                //duplicate
                if ($("#" + newId).is("#" + newId)) {
                    //console.log("duplicated"); //console
                    newId = newId + index;
                }
                //remove spaces, ids cannot have them
                //console.log(newId)
                if(newId === "undefined"){
                  newId = newId.replace(/ +/g, "_");
                }
                $(this).attr("id", newId);
                //console.log("\nId: "+newId);  //console
            }
        }
        //id label not found
        if ($("[for='" + thisid + "']").length == 0) {
            if($("#" + thisid).attr("name")=== "undefined"){
                //console.log('111111')
                var lblname = $("#" + thisid).attr("name");
                if (typeof lblname === "undefined") {
                    lblname = $("#" + thisid).attr("id");
                    if (typeof lblname === "undefined") {
                        lblname = " ";
                    }
                }
                //convert from CamelCase, replace -_. with spaces
                lblname = lblname.replace(/-/g, " ");
                lblname = lblname.replace(/_/g, " ");
                lblname = lblname.replace(/\./g, " ");
                lblname = lblname.replace(/([A-Z])/g, " $1").toLowerCase();
                //check to see if label text is there in another label
                if ($(":contains(" + lblname + ")").length != 0) {
                    lblname = " ";
                }
                //create label
                //if radio, must wrap with no label
                if ($(this).attr("type") == "radio") {
                    var lbl = "<label for='" + thisid + "' />";
                    $("#" + thisid).wrap();
                }
                //if not radio, insert  before
                else {
                    var lbl = "<label for='" + thisid + "' class='hide'>" + lblname + "</label>";
                    $(lbl).insertBefore("#" + thisid);
                }

                //console.log("Label had been added"); //console
            }
        }
        //Tooltip fix
        //selects all tooltips and adds a hidden span to the label
        //of the form element with the tooltip text
//        $(this).filter('[rel="tooltip"]').each(function () {
//            var tooltext = $(this).attr("data-original-title");
//            if (tooltext == "" || typeof tooltext === "undefined") {
//                tooltext = $(this).attr("data-title");
//                if (tooltext == "" || typeof tooltext === "undefined") {
//                    tooltext = $(this).attr("title");
//                }
//            }
//            //   console.log($(this).parent().prop("tagName"));    //console
//            $(this).parent().html($(this).parent().html() + '<span class="aria-hidden">help text as follows. ' + tooltext + 'end help text</span>');
//        });

        var ariaLabelValue = $("[for='" + $(this).attr("id") + "']").text();
        if (typeof ariaLabelValue === 'undefined') {
            ariaLabelValue = "";
        }

        //same as above, for placeholder
        var pld = $(this).attr("placeholder");
        if (typeof pld !== "undefined") //CODE CHANGED HERE
        {
            //start new additions
            //if there are capitals next to each other
            //seperate by a space so that they aren't
            //read as one word. Ex: MM -> M M
            pld = pld.replace(/([A-Z])/g, " $1");
            //replace "-" with the word "dash"
            pld = pld.replace(/-/g, " dash ");
            //replace "/" with the word "slash"
            //   pld = pld.replace(/\//g, " slash ");
            //replace "," with the word "comma"
            pld = pld.replace(/,/g, " comma ");
            //replace "," with the word "comma"
            pld = pld.replace(/\./g, " dot ");
            //end new additions
            if (pld) {
                pld = " Sample text. " + pld;
            }
            if ($(this).val() != "") {
                pld = "";
            }
        } else {
            pld = "";
        }
        if (!(ariaLabelValue + pld == "")) {
            $(this).attr("aria-label", ariaLabelValue + pld);
            //console.log($(this).attr("aria-label"));
        }
    });





/**************************************************************************************************/
/******************************************MODAL FIX**********************************************/
/************************************************************************************************/
   //Let JAWS reads modal shows and hides
   $(".modal").on("hidden",function(){
     $('body').append("<span class='offScreen' role='alert'>Modal is closed</span>");
   });
   $(".modal").on("show",function(){
     $('body').append("<span class='offScreen' role='alert'>Modal is showing</span>");
   });


   //Read modals automatically
   $("[data-toggle='modal']").click(function(){
       var getId = $(this).attr('id');
       $(this).attr('aria-describedby', getId);
       $(this).find('.modal-body').attr('id', 'modal-body');
   });


   //remove "aria-hidden=true" in modal so it can be closed when JAWS opens
  $(".modal *:not(span)").each(function(){
     if($(this).attr("aria-hidden") == "true"){
       $(this).removeAttr("aria-hidden");
     }
   });

  //read close on modal top right, rather than times
  $(".modal-header button").each(function(){
    $(this).wrapInner('<span aria-hidden="true"></span>');
    $(this).append('<span class="aria-hidden">close</span>');
  });

  //When modal shown up search for form and error fields:
  $(".modal").on("shown",function(){

   });

/**************************************************************************************************/
/******************************************MODAL FIX END******************************************/
/************************************************************************************************/



/**************************************************************************************************/
/******************************************SKIP FIX***********************************************/
/************************************************************************************************/
   //add id=rightpanel to span9 if there is a sidebar
   if( $('#sidebar').length > 0 && $('.span9').length > 0 && $('#rightpanel').length == 0){
     $('.span9').attr('id','rightpanel');
   }

   //skip script
   //if there is a menu, nav skip to menu
   if ($('#menu').length > 0){

     $('.skip-nav').attr('href','#menu');
     $('.skip-nav').click(function(){
        $('#menu a:first').focus();
     });

     //if there is a sidebar, menu skip to sidebar
     if($('#sidebar').length > 0){
       $('#menu :first').before('<a href="#sidebar" class="skip-menu" accesskey="m">Skip Menu to Side Bar</a>');
       $('.skip-menu').click(function(){
          $('#sidebar :first').focus();
       });

       //if there is a rightpanel, sidebar skip to rightpanel
       if($('#rightpanel').length > 0){
         $('#sidebar :first').before('<a href="#rightpanel" class="skip-sidebar" accesskey="s">Skip Side Bar to Main Content</a>');
         $('.skip-sidebar').click(function(){
           $('#rightpanel :input:first,#rightpanel a:first').focus();
         });
        }
      //if there is no sidebar and there is a rightpanel, menu skip to rightpanel
      }else if($('#rightpanel').length > 0){
        $('#menu :first').before('<a href="#rightpanel" class="skip-menu" accesskey="m">Skip Menu to Main Content</a>');
        $('.skip-menu').click(function(){
          $('#rightpanel :input:first,#rightpanel a:first').focus();
        });
      }

     //if there is no sidebar and no rightpanel, and there is a footer(always has a footer), menu skip to footer
     $('#footer :first').before('<a href="#" onclick="skipFooterToRightpanel();return false" class="skip-footer" accesskey="f">Skip Footer to Main Content</a>');
     $('.skip-footer').click(function(){
       $('#menu a:first').focus();
     });
   //if there is no menu, and there is a sidebar, nav skip to sidebar
   }else if($('#sidebar').length > 0){
     $('.skip-nav').attr('href','#sidebar');
     $('.skip-nav').click(function(){
        $('#sidebar :first').focus();
     });
     //if there is a rightpanel, sidebar skip to rightpanel
     if($('#rightpanel').length > 0){
      $('#sidebar :first').before('<a href="#rightpanel" class="skip-sidebar" accesskey="s" >Skip Side Bar to Main Content</a>');
      $('.skip-sidebar').click(function(){
        $('#rightpanel :input:first,#rightpanel a:first').focus();
      });
     }

    //if there is no rightpanel, and there is a footer(always has a footer), sidebar skip to footer
    $('#footer :first').before('<a href="#sidebar" class="skip-footer" accesskey="f">Skip Footer to Side Bar</a>');
    $('.skip-footer').click(function(){
      $('#sidebar :first').focus();
    });
  //if there is no menu and sidebar, and there is a rightpanel, nav skip to rightpanel
   }else if($('#rightpanel').length > 0){
    $('.skip-nav').attr('href','#rightpanel');
    $('.skip-nav').click(function(){
      $('#rightpanel :input:first,#rightpanel a:first').focus();
    });

    //if there is no rightpanel, and there is a footer(always has a footer), rightpanel skip to footer
    $('#footer :first').before('<a href="#rightpanel" class="skip-footer" accesskey="f">Skip Footer to Main Content</a>');
    $('.skip-footer').click(function(){
      $('#rightpanel :input:first,#rightpanel a:first').focus();
    });
   }else if($('#slides').length > 0){ //for home page
    $('.skip-nav').attr('href','#slides');
    $('.skip-nav').click(function(){
      $('#slides :input:first,#slides a:first').focus();
    });

    //if there is no rightpanel, and there is a footer(always has a footer), rightpanel skip to footer
    $('#footer :first').before('<a href="#slides" class="skip-footer" accesskey="f">Skip Footer to Main Content</a>');
    $('.skip-footer').click(function(){
      $('#slides :input:first,#slides a:first').focus();
    });
   }else{
     //if there is no menu, sidebar and rightpanel, nav skip to next element
    $('.skip-nav').click(function(){
      $('input:first,a:first,select:first').focus().select();
    });
   }


/**************************************************************************************************/
/******************************************SKIP FIX END*******************************************/
/************************************************************************************************/



/**************************************************************************************************/
/********************************LANGUAGE PLUGIN AND UPLOAD FIX **********************************/
/******************************************FIXED BY SHOVAN***************************************/
/***********************************************************************************************/

    /*for counties served plugin*/
    $('.chzn-choices:not(.providerSelectedListUl)').live('focus',function(){
      if($(this).parents('.control-group').find('.control-label').text()==="Languages"){
        //$(this).attr('aria-hidden','Language');
        if($('.chzn-choices li').length > 1){
          var collection = $(".search-choice");
          jQuery.each( collection, function( i, val ) {

            $('#'+$(val).attr('id')).find('span.selectedLang').text('');
            var selectedLanguage = $('#'+$(val).attr('id')).find('span').text();

            $('#'+$(val).attr('id')).find('a').find('span.selectedLang').remove();

            var anchorTagHTML = $('#'+$(val).attr('id')).find('a');
            anchorTagHTML.html("<span class='selectedLang aria-hidden'>Languages "+selectedLanguage+"</span>");
          });
        }
        if($('.chzn-drop').position().left == 0){
          $('.languageDrop').remove();
          $('.search-field').prepend('<label class="aria-hidden languageDrop">Languages</label>');
        }
      }else if($(this).parents('.selectPlugin').hasClass('selectPlugin')){/*for all the other checkboxes with select dropdown plugin*/
    var titleOfMultipleCheckbox =$(this).parents('.selectPlugin').parents('table').find('thead').text();
    if($('.chzn-drop').position().left == 0){
      $('.languageDrop').remove();
      $('.search-field').prepend('<label class="aria-hidden languageDrop">'+titleOfMultipleCheckbox+' Other Textbox</label>');
    }
  }else{
        $(this).attr('aria-hidden','Counties Served');
        var collection = $(".search-choice");
        jQuery.each( collection, function( i, val ) {

          $('#'+$(val).attr('id')).find('span.selectedLang').text('');
          var selectedLanguage = $('#'+$(val).attr('id')).find('span').text();

          $('#'+$(val).attr('id')).find('a').find('span.selectedLang').remove();

          var anchorTagHTML = $('#'+$(val).attr('id')).find('a');
          anchorTagHTML.html("<span class='selectedLang aria-hidden'>Counties Served "+selectedLanguage+"</span>");
        });
        if($('.chzn-drop').position().left == 0){
          $('.languageDrop').remove();
          $('.search-field').prepend('<label class="aria-hidden languageDrop">Counties Served</label>');
        }
      }

    });


    $('.search-field input').bind('keydown',function(e){
    if(e.keyCode === 13){
      $(this).hide(100).show(100,function(){
        $(this).focus();
      });
    }
  });


    /*trigger input upload button on focus (tab/accessibility)*/
    $("input[type='file']").each(function () {
  $(this).attr('aria-label',"Press Tab and then Space bar to open the choose dialog box.");
    });

/**************************************************************************************************/
/********************************LANGUAGE PLUGIN AND UPLOAD FIX END*******************************/
/******************************************FIXED BY SHOVAN***************************************/
/***********************************************************************************************/






/**************************************************************************************************/
/**********************************ADA FIX WITH TIMEOUT FUNCTION *********************************/
/************************************************************************************************/
    //read all error messages when press button
    //$(".btn").bind("click",function(){
    //Changed to delegate binding
    $(document).on("click", ".btn", function(){

      setTimeout(function(){

      //jump to the error message
      //var errLabel = $('div[id$=_error] label:visible'); changed to below
      var errLabel = $('div[id$=_error] label');

      if(errLabel.length > 0){
        $(window).scrollTop(errLabel.first().offset().top - 150);
      }
      //end jump to the error message

        var errNo = 0;
        var errMsgs = "";

        //select all error message div whose id ends with '_error'
        $("[id$='_error']").each(function () {
          var errMsgHTML = $(this).html().trim();
          var errMsgText = $(this).text().trim();
                  // alert("errMsgHTML" + errMsgHTML + "errMsgText" + errMsgText);
          //if there is error message and it's not hidden
          if(errMsgHTML != '' && errMsgHTML.search("none") == -1 && $(this).css("display") != "none"){

            errNo++;
            errMsgs += "Error message " + errNo + " " + errMsgText;

            //shift focus to first error element
            if(errNo == 1){
              $(this).closest("div.controls").find("input:first,select:first").focus();
            }
          }

          var errorDiv = $(this);
          //Add ARIA-live = ASSERTIVE attribute to label element:
          //errorDiv.find('label').attr('aria-atomic','true');
          errorDiv.find('label').attr('aria-invalid','true');

          //Add ARIA DESCRIBEDBY for ERROR MESSAGE on input required field
          var errorDivId = $(this).attr('id');

          //Add id attribute for label to map aria-describedby on input or select element
          $('#'+errorDivId).find('label').attr('id', errorDivId+'_label');
          var errorDivLabelId = $('#'+errorDivId).find('label').attr('id');

          //If controls are in parent controls element
          //Add aria-describedby
          errorDiv.parents('.controls').find('input[required], input.error, input[aria-required]').attr('aria-describedby',errorDivLabelId);
          errorDiv.parents('.controls').find('select[required], select.error, select[aria-required]').attr('aria-describedby',errorDivLabelId);

          //Add aria-required=true
          errorDiv.parents('.controls').find('input[required], input.error, input[aria-required]').attr('aria-required', 'true');
          errorDiv.parents('.controls').find('select[required], select.error, select[aria-required]').attr('aria-required', 'true');

          //If controls are in parent form-group element
          //Add aria-describedby
          errorDiv.parents('.form-group').find('input[required], input.error, .form-control[aria-required]').attr('aria-describedby',errorDivLabelId);
          errorDiv.parents('.form-group').find('select[required], select.error, .form-control[aria-required]').attr('aria-describedby',errorDivLabelId);

          //Add aria-required=true
          errorDiv.parents('.form-group').find('input[required], input.error, .form-control[aria-required]').attr('aria-required', 'true');
          errorDiv.parents('.form-group').find('select[required], select.error, .form-control[aria-required]').attr('aria-required', 'true');

        });


        if( errNo == 1){
          errMsgs = "Following " + errNo + " error have been found: " + errMsgs;
        }else if( errNo > 1){
          errMsgs = "Following " + errNo + " errors have been found: " + errMsgs;
        }

        //console.log("hehe "+errMsgs);

        //read error messages
        if(errNo > 0){
          //Remove the existing offScreen first and add it again
          $('span.offScreen').remove();
          $('body').append("<span class='offScreen' role='alert'>" + errMsgs + "</span>");
        }

      },400);

    });



   //highlight all phone box if some of them have errors
   $("input,a").bind("click keydown change", function(){
    //this script comes after validation, so use setTimeout function
    setTimeout(
        function(){
          $(".input-mini[maxlength=3],.input-mini[maxlength=4],.input-small[maxlength=3],.input-small[maxlength=4]").each(function(){
            var parentDiv = $(this).closest("div.controls");
            if($(this).hasClass("error") && parentDiv.find("div label").is(":visible")){
              parentDiv.find("input").addClass("error");
            }else if (!parentDiv.find("div label").is(":visible")){
              parentDiv.find("input").removeClass("error");
            }
          });
        },10

    );
   });



   //SHOP EMPLOYER: contribution.jsp
   //html comes from jQuery-UI js, so need to set a timeout
   setTimeout(function(){
     $(".ui-slider-handle").each(function(){
       $(this).attr("role","slider");
       $(this).prepend('<span class="aria-hidden">slider</span>');

       var sliderType = $(this).closest("div.slider").attr('id');
       var percentage  = "Employer pays " + $(this).text();

     //read different sliders
       if(sliderType == "employeeSlider"){
         percentage += "for employees";
       }else if(sliderType == "dependentSlider"){
         percentage += "for dependents";
       }

       $(this).attr("aria-valuetext",percentage);

       //refresh percentage
       $(this).bind("keydown",function(event){

         percentage  = "Employer pays " + $(this).text();

         //read different sliders
         if(sliderType == "employeeSlider"){
           percentage += "for employees";
         }else if(sliderType == "dependentSlider"){
           percentage += "for dependents";
         }

         //no less than 50% for employeeSlider
         if(($(this).text().trim() == "50%") && (sliderType == "employeeSlider")){
           percentage += " In New Mexico minimal contribution towards employee is 50%";
         }

         $(this).attr("aria-valuetext",percentage);


       });

     });

   },1000);

/**************************************************************************************************/
/**********************************ADA FIX WITH TIMEOUT FUNCTION END******************************/
/************************************************************************************************/






});


  /* fix for accordion accessibility issue */

    jQuery(window).load(function () {

    //alert('page is loaded');
    setTimeout(function(){

      $('.accordion-heading').each(function () {

      $(this).click(function(){
      //  console.log($(this).find('.collapsed').length);
        if($(this).find("a.accordion-toggle").attr("aria-expanded") == "false")
        {
          $(this).next().attr("aria-expanded", "true");
          $(this).find("a.accordion-toggle").attr("aria-expanded", "true");

        }
        else {
          $(this).next().attr("aria-expanded", "false");
          $(this).find("a.accordion-toggle").attr("aria-expanded", "false");

        }
      });

      });

    }, 0);

  });

    /* fix for HIX-90632 to put focus back to the previous modal when hitting enter on the Take me back button */

  $(document).ready(function() {

      $(document).on('focus','a.externalModalClose',function(){

            $(this).keydown(function (e) {
               var key = e.which;

              if(key == 13 || key == 32 )  // the enter key code
              {
                console.log("enter key pressed on Take me Back button ");

                $('button.externalModalClose').click();

              }

           });
      });

    //change focus to the ein Modal when the link is pressed on the page

      $(document).on('focus','a[href="#einModal"]',function(){
        $(this).keydown(function (e) {
            var key = e.which;

            if(key == 13 || key == 32)  // the enter key code
            {
              $(this).click();
             }
             //return false;
        });
      });
  });
