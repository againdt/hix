function validateSSN(ssn1,ssn2,ssn3) {

	  	var filter = /^[0-9]+$/;
	  	 
		if ((ssn1 == "" || ssn2 == "" || ssn3 == "") || 
			(isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3)) || 
			(ssn1.length != 3 || ssn2.length != 2 || ssn3.length != 4) || (!filter.test(ssn1) || !filter.test(ssn2) || !filter.test(ssn3))) {
			return false;
		}
		return true;
	}

// validate ssn all zero in 2nd and 3rd field
function notAllowedSSN1(ssn1,ssn2,ssn3) {
	var zero1="000";
 	var zero2="00"; 
 	var zero3="0000"; 
 	
 	//Not be 078-05-1120 (due to the Woolworth's Wallet Fiasco) 
 	var val1="078";
 	var val2="05";
 	var val3="1120";	 	
 	
 	// Not be 219-09-9999 (appeared in an advertisement for the Social Security Administration) 
 	var ssa1="219";
 	var ssa2="09";
 	var ssa3="9999";
 	
 	
 	//code added by samir ends
 	
	if (
			(ssn1 == "" || ssn2 == "" || ssn3 == "") || (isNaN(ssn1))
			|| (isNaN(ssn2)) || (isNaN(ssn3))
			|| (ssn1.length != 3 || ssn2.length != 2 || ssn3.length != 4) 
			
			//code added by samir starts
		    || zero1.match(ssn1)  || zero2.match(ssn2) || zero3.match(ssn3) 
			|| (val1.match(ssn1) && val2.match(ssn2) && val3.match(ssn3))
			|| (ssa1.match(ssn1) && ssa2.match(ssn2) && ssa3.match(ssn3)) 
			//ends
	   ) 
	
	{
		return false;
	}
	return true;
}

//validate ssn1 for area codes. Not allowed are SSNs with Area Numbers (First 3 digits) 000, 666 and 900-999. 
function areaCodeCheck(ssn1){
	var result=false;
	
	var firstThreeDgt1="000";
 	var firstThreeDgt2="666";
 	var firstThreeDgt3=/^9[0-9][0-9]$/;
  	
  	if(  firstThreeDgt1.match(ssn1) ||  firstThreeDgt2.match(ssn1) || firstThreeDgt3.test(ssn1))
  	{
  		result=false;
	}
  	else
  	{
  		result=true;
  	}
	return result; 
}

// Not allowed are SSNs from 987-65-4320 to 987-65-4329. 
function notAllowedSSN2(ssn1,ssn2,ssn3){
	var ssnFilter1="987";
 	var ssnFilter2="65";
 	var ssnFilter3=/^432[0-9]$/;
 	
  	if( ssnFilter1.match(ssn1) &&  ssnFilter2.match(ssn2) && ssnFilter3.test(ssn3)){
		return false;
	}
	return true;
}

//takes in csrfParameter as last parameter of the form '?csrftoken=xc3wvd'
function isUniqueSSN(ssn,employeeId,dependentId,validationFor,csrfParameter){
	var callUrl="";

	if(csrfParameter && csrfParameter.length > 0){
		if(validationFor == 'dependent'){
			callUrl="/hix/shop/validation/isUniqueSSNForDependent" + csrfParameter;
			
		} else if(validationFor == 'employee'){
			callUrl="/hix/shop/validation/isUniqueSSNForEmployee" + csrfParameter;
		}
	}
	else{
		if(validationFor == 'dependent'){
			callUrl="/hix/shop/validation/isUniqueSSNForDependent";
			
		} else if(validationFor == 'employee'){
			callUrl="/hix/shop/validation/isUniqueSSNForEmployee";
		}
	}
	
	// if ssn is not provided don't validate for unique
	if(ssn == null || ssn.length < 9){
		return true;
	}
	
	var result=false;
	$.ajax({
		type: "POST",
		url : callUrl,
		async: false,
		cache : false,
		dataType : 'json',
		data : {
			ssn : ssn,
			employeeId : employeeId,
			dependentId : dependentId
		},
		success : function(response) {
			if(response == true){
				result = true;			
			}
			else{
				result = false;
			}
		}
	});
	return result;
}

//function for employer/employee firstname,lastname validation
function namecheck(value){
//	var regex = /^[a-zA-Z\-]+$/;
	var regex = /^[a-zA-Z '-]+$/;
  	if(value !=""){
		if(!regex.test(value) ){
	  		return false;	
	  	}
	}
	return true;
}

function isInvalidCSRFToken(xhr) {
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}

