/**
 * 
 */
package com.getinsured.hix.batch.enrollment.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.enrollment.service.EnrlReconTestDataCreationService;
import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;

/**
 * Enrollment Reconciliation Test Data Creation Thread
 * @author negi_s
 * @since 08/03/2017
 */
public class EnrlReconTestDataCreationThread implements Callable<List<ReconDetailCsvDto>> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconTestDataCreationThread.class);

	private List<Integer> enrollmentIdList;
	private String hiosIssuerId;
	private int coverageYear;
	private Date batchRunDate;
	private EnrlReconTestDataCreationService enrlReconTestDataCreationService;

	/**
	 * @param enrollmentId
	 * @param hiosIssuerId
	 * @param coverageYear
	 * @param enrlReconTestDataCreationService
	 */
	public EnrlReconTestDataCreationThread(List<Integer> enrollmentIdList, String hiosIssuerId, int coverageYear, Date batchRunDate,
			EnrlReconTestDataCreationService enrlReconTestDataCreationService) {
		this.enrollmentIdList = enrollmentIdList;
		this.hiosIssuerId = hiosIssuerId;
		this.coverageYear = coverageYear;
		this.batchRunDate = batchRunDate;
		this.enrlReconTestDataCreationService = enrlReconTestDataCreationService;
	}

	@Override
	public List<ReconDetailCsvDto> call() throws Exception {
		List<ReconDetailCsvDto> reconDetailList = new ArrayList<>();
		for(Integer enrollmentId : enrollmentIdList){
			LOGGER.info(" Thread Name :: "+ Thread.currentThread().getName()+" HIOS Id ::" + hiosIssuerId+" EnrollmentId ::" + enrollmentId);
			List<ReconDetailCsvDto> partialReconDetailList = enrlReconTestDataCreationService.populateDataForEnrollment(enrollmentId, batchRunDate, hiosIssuerId, coverageYear);
			if(null != partialReconDetailList && !partialReconDetailList.isEmpty()){
				reconDetailList.addAll(partialReconDetailList);
			}
		}
		return reconDetailList;
	}
}
