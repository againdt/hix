/**
 * 
 */
package com.getinsured.hix.account.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

/**
 * MVC Test for /admin/appsecurity administration page.
 * 
 * @author Yevgen Golubenko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AppSecurityMvcTest extends WebTest
{
	private MockMvc mockMvc;

	public AppSecurityMvcTest()
	{
		mockMvc = loginWithEmailAddress(WebTest.EMAIL_EXADMIN);
	}

	@Test
	public void testAccess() throws Exception
	{
		final ResultActions ra = mockMvc.perform(get("/admin/appsecurity").session(getSession()));
		final MvcResult result = ra.andReturn();
		final MockHttpServletResponse response = result.getResponse();
		System.out.println("Status: " + response.getStatus());
		final String responseString = response.getContentAsString();
		System.out.println("Response string: " + responseString);
	}
}
