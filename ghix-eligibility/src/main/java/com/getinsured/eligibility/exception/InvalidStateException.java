package com.getinsured.eligibility.exception;

public class InvalidStateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidStateException() {
		super();
	}

	public InvalidStateException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidStateException(String message) {
		super(message);
	}

	public InvalidStateException(Throwable cause) {
		super(cause);
	}

}
