package com.getinsured.ssap.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.CitizenshipImmigrationStatus;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.hub.vlp.VlpActionResult;
import com.getinsured.ssap.model.hub.vlp.VlpIntegrationLog;
import com.getinsured.ssap.repository.VlpIntegrationLogRepository;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.web.configuration.StateConfiguration;

/**
 * Scheduler for VLP HUB calls.
 * <p>
 *   This scheduler is running at specific hours or intervals,
 *   picks up records and processes them, similar to spring-batch.
 *   <br/>
 *   <br/>
 *   For now scheduler intervals are hardcoded, once we move closer to
 *   production release, we need to sit down and think about intervals for
 *   production.
 * </p>
 * @author Yevgen Golubenko
 * @since 3/7/19
 */
@EnableScheduling
@Component
public class VlpIntegrationLogScheduler
{
  private static final Logger log = LoggerFactory.getLogger(VlpIntegrationLogScheduler.class);
  public static final int MAX_RETRY_COUNT = 5; // We are checking less than this value, so maximum of 5.

  private final VlpIntegrationLogRepository vlpIntegrationLogRepository;
  private final HubIntegration hubIntegration;
  private final SsapApplicationRepository applicationRepository;
  private final SsapApplicantRepository applicantRepository;
  private final StateConfiguration stateConfiguration;

  @Autowired
  public VlpIntegrationLogScheduler(final VlpIntegrationLogRepository vlpIntegrationLogRepository,
                                    final HubIntegration hubIntegration,
                                    final SsapApplicationRepository applicationRepository,
                                    final SsapApplicantRepository applicantRepository,
                                    final StateConfiguration stateConfiguration)
  {
    this.vlpIntegrationLogRepository = vlpIntegrationLogRepository;
    this.hubIntegration = hubIntegration;
    this.applicationRepository = applicationRepository;
    this.applicantRepository = applicantRepository;
    this.stateConfiguration = stateConfiguration;
  }

  @PostConstruct
  public void postConstruct() {
    log.info("VLP Integration Processor for state: {}, supported: {}", stateConfiguration.getStateCode(), isSupportedState());
  }

  @Scheduled(initialDelay = 300_000L, fixedDelay = 3_600_000L)
  private void retry() {
    if(!isSupportedState()) {
      return;
    }

    log.debug("Processing VLP Integration Logs for action result: {} and MAX_RETRY_COUNT less than: {}", VlpActionResult.RETRY, MAX_RETRY_COUNT);
    List<VlpIntegrationLog> integrationLogs = vlpIntegrationLogRepository.findByActionResultAndRetryCountLessThan(VlpActionResult.RETRY, MAX_RETRY_COUNT);
    logIntegrationSummary(integrationLogs);
    log.debug("Need to re-execute VLP verification for {} records", integrationLogs.size());

    final Set<Long> applicationIds = new HashSet<>(integrationLogs.size());
    final Set<Long> applicantIds = integrationLogs.stream().map(VlpIntegrationLog::getSsapApplicantId).collect(Collectors.toSet());

    integrationLogs.forEach(il -> {
      applicationIds.add(il.getSsapApplicationId());
    });

    applicationIds.forEach(applicationId -> hubIntegration.process(applicationId, applicantIds));
  }

  @Scheduled(initialDelay = 600_000L , fixedDelay = 86_400_000L)
  private void checkCaseResolution() {
    if(!isSupportedState()) {
      return;
    }

    log.debug("Processing VLP Integration Logs for action result: {}", VlpActionResult.CHECK_CASE_RESOLUTION);
    List<VlpIntegrationLog> integrationLogs = vlpIntegrationLogRepository.findByActionResult(VlpActionResult.CHECK_CASE_RESOLUTION);
    logIntegrationSummary(integrationLogs);
    log.debug("Checking case resolution for: {} records", integrationLogs.size());
    integrationLogs.forEach(hubIntegration::checkStepTwoCaseResolution);
  }

  private void logIntegrationSummary(final List<VlpIntegrationLog> vlpIntegrationLogs) {
    vlpIntegrationLogs.forEach(il -> {
      log.debug("Integration URL: {}", il.getRemoteUrl());
      log.debug("Previous response code: {} => {}", il.getResponseCode(), il.getResponseMessage());
      log.debug("Previous VLP response: {}", il.getVlpResponse());
      log.debug("VLP Request: {}", il.getVlpRequest());
      log.debug("===========================================\n\n");
    });
  }

  private CitizenshipImmigrationStatus getCitizenshipImmigrationStatus(final long ssapApplicationId, final long ssapApplicantId) {
    final SsapApplication ssapApplication = applicationRepository.findOne(ssapApplicationId);
    if(ssapApplication == null) {
      log.error("Unable to find SSAP application by given application id: {}", ssapApplicationId);
      return null;
    }

    final SingleStreamlinedApplication singleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplication);

    if(singleStreamlinedApplication == null) {
      log.error("Unable to parse application data json from ssap record: {}", ssapApplication.getId());
      return null;
    }

    final SsapApplicant applicant = applicantRepository.findOne(ssapApplicantId);
    final List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();

    Optional<HouseholdMember> householdMemberOptional = householdMembers.stream().filter(hhm ->
        hhm.getApplicantGuid().equals(applicant.getApplicantGuid())).findFirst();

    if(!householdMemberOptional.isPresent()) {
      log.error("Unable to find household member in application: {} by applicant guid: {}", ssapApplicationId, applicant.getApplicantGuid());
      return null;
    }

    HouseholdMember householdMember = householdMemberOptional.get();
    log.info("Found CitizenshipImmigrationStatus document for ssap id: {} and applicant id: {}", ssapApplicationId, ssapApplicantId);
    return householdMember.getCitizenshipImmigrationStatus();
  }

  /**
   * Returns {@code true} if state supports {@link VlpIntegration} or not.
   * Currently:
   * <ul>
   *   <li>CA</li>
   *   <li>MN</li>
   * </ul>
   * do NOT support VLP Integration, every other state does.
   *
   * @return {@code true} if state supports it, {@code false} otherwise.
   */
  private boolean isSupportedState() {
    return !stateConfiguration.getStateCode().equals("CA") && !stateConfiguration.getStateCode().equals("MN");
  }
}
