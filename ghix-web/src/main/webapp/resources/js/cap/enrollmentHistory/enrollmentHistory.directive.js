(function() {
	"use strict";
	
	angular.module("enrollmentHistoryApp").directive('collapseIconPlus', collapseIconPlus);
	angular.module("enrollmentHistoryApp").directive('dateValidation', dateValidation);
	angular.module("enrollmentHistoryApp").directive('dynamicFieldName', dynamicFieldName);
	angular.module("enrollmentHistoryApp").directive('bootstrapTooltip', bootstrapTooltip);
	angular.module("enrollmentHistoryApp").directive('currencyOnly', currencyOnly);
	angular.module("enrollmentHistoryApp").directive('spinningLoader', spinningLoader);
	
	collapseIconPlus.$inject = ['$http'];
	function collapseIconPlus($http){
		var directive = {
			restrict: "A",
			link : function(scope, element, attrs) {
	        	
	        	element.bind('click', function(){
	        		
	        		if(element.find(".icon-plus-sign").length > 0){
	            		element.find("i").removeClass("icon-plus-sign").addClass("icon-minus-sign");
	            		
	            		if(scope.localData.enrollmentHistoryIndicator === false){
	            			var csrftok = {};
	            			csrftok[csrfToken.paramName] =  csrfToken.paramValue;
	            			var data = {
	            					 encHouseholdId : $('#encHouseHoldId').val(),
	            			 }
	            			var promise = $http({
	            				method: 'POST', 
	            				url: '/hix/crm/member/enrollmenthistory',
	            				data:data,
	            				headers: {'Content-Type': 'application/json',"csrftoken": csrfToken.paramValue}
	            				});
	            			
	            			promise.success(function(response){
	                			scope.localData.enrollmentHistoryLoading = false;            				
	            				scope.localData.enrollmentHistoryIndicator = true;
	            				
	            				response = angular.fromJson(response);
	            				
	            				for(var i=0; i<response.length;i++){
	            					var eventJson = JSON.parse(response[i].eventJson);
	            					response[i].eventJson = eventJson;
	            				}
	            				
	            				
	                		});
	            			
	            			promise.error(function(response){
	                		});
	            		}
	            	}else if(element.find(".icon-minus-sign").length > 0){
	            		element.find("i").removeClass("icon-minus-sign").addClass("icon-plus-sign");
	            	}
	        	});
			}
		}
		
		return directive;
	}
	
	
		
 	function dateValidation(){
 		var directive = {
 				restrict : 'AE',
 				require : 'ngModel',
 				link : function(scope, element, attrs, ngModel) {
 					var validationTypeArr = attrs.dateValidation.split('_');
 					
 					function customDateValidation(value){
 						//remove placeholder
 						var date = value.replace(/[A-Za-z]/g, '');
 						var myScope = scope;
 						//remove extra-last-character
 						if(date.length === 11){
 							date = date.substring(0, date.length - 1);
 						}

 						if(date.length === 10){
 							resetValidation();
 							
 							var isValid = true;
 							
 							for(var i = 0; i < validationTypeArr.length; i++){
 								if(validationTypeArr[i] === 'date' && !moment(date,'MM/DD/YYYY').isValid()){
 									isValid = 'date';
 									break;
 								}else if(date !== undefined && scope.vm.enrollment.enrollmentStartDateOriginal !== undefined 
 										&& validationTypeArr[i] === 'sameYear' && date.substring(6,10) !== scope.vm.enrollment.enrollmentStartDateOriginal.substring(6,10)){
 									isValid = 'sameYear';
 									break;
 								}
 							}
 								
 							return isValid;
 						}else{
 							return true;
 						}
 						
 					}
 					
 					function resetValidation(){
 						validationTypeArr.forEach(function(item, index){
 							ngModel.$setValidity(item, true);	
 						});
 					}
 					
 					ngModel.$parsers.unshift(function(value){
 						if(value === undefined || value === null){
 							return;
 						}
 						var dateValidationType = customDateValidation(value);
 						if(dateValidationType !== true){
 							ngModel.$setValidity(dateValidationType, false);
 						}
 						
 						return value;	
 					});
 					
 					 ngModel.$formatters.unshift(function(value) {
 						if(value === undefined || value === null){
 							return;
 						}
 						
 						var dateValidationType = customDateValidation(value);
 						if(dateValidationType !== true){
 							ngModel.$setValidity(dateValidationType, false);
 						}
 						 
 						return value;
 		             });
 					
 					
 				}
 			};

		return directive;
	}
 	
 	
 	
 	
 	function dynamicFieldName(){
		 var directive = {
			restrict : 'A',
			priority : -1,
			require : ['ngModel'],
			// the ngModelDirective has a priority of 0.
			// priority is run in reverse order for postLink functions.
			link : function(scope, iElement, iAttrs, ctrls) {

				var name = iElement[0].name;
				name = name.replace(/\{\{\$index\}\}/g, scope.$index);

				var modelCtrl = ctrls[0];
				modelCtrl.$name = name;
			}
		};

		return directive;
	}
 	
 	
 	
 	function bootstrapTooltip(){
		var directive = {
			restrict : 'AE',
			link : function(scope, elem, attrs) {
				elem.tooltip();
			}
		};
		
		return directive;
	}
 	
 	
 	function currencyOnly(){
		var directive = {
			restrict: 'AE',
			require: 'ngModel',
			link: function (scope, element, attrs, ngModel) {
				function fromUser(userInput) {
	                if (userInput) {
	                    var transformedInput = userInput.replace(/[^0-9\.]/g, ''); 
	                    var pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
	                   
	                    if (transformedInput !== userInput) {
	                        ngModel.$setViewValue(transformedInput);
	                        ngModel.$render();
	                    }
	                    
	                    if(ngModel.$error.required === false){
	                    	ngModel.$setValidity('currency', pattern.test(transformedInput));	
	                    }
	                    
	                    
	                    return transformedInput;
	                }
	                return undefined;
	            }            
	            ngModel.$parsers.push(fromUser);
			}
		};

		return directive;
	}
 	
 	
 	
 	spinningLoader.$inject = ['$timeout'];
 	function spinningLoader($timeout){
 		var directive = {
			restrict: "EA", 
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="/hix/resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;                   
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };           
	            }; 
	         });
	      }	
 		};
 		
 		return directive;
 	}

	
 	
 	
 	
})();