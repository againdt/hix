<script type="text/ng-template" id="income-job.jps">
  <div class="gutter10 jobIncome" ng-controller="incomeJob" >
    <div id="ctrl-as-exmpl" ng-controller="settings">
      <form class="form-horizontal" id="jobIncome" name="jobIncomeForm" novalidate>
        <h3><spring:message code="label.lce.jobincome"/></h3>

      <div ng-repeat="householdMemberIncomeJob in householdMember.detailedIncome.jobIncome" >
        <ng-form name="jobIncomeRepeat" id="jobIncomeRepeat" novalidate>
          <div class="control-group">
            <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.employeename"/>:</label>
            <div class="controls">
              <input type="text" id="employerName_1" class="span5" name="employerName" ng-model="householdMemberIncomeJob.employerName" required ng-trim>
            </div>
          </div>
          <div class="controls margin5-t">
  	        <span class="validation-error-text-container" ng-show="submitted && jobIncomeRepeat.employerName.$error.required"><spring:message code="label.lce.incomejob.error1"/></span>
  	      </div>

         <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getpaidbeforetaxesaretakenout"/>
         <div class="control-group">
           <label class="control-label" for="typeOfRequest"></label>
           <div class="controls margin20-b">
             <div class="input-append input-prepend margin10-t">
               <span class="add-on">$</span>
               <input type="text" class="span4 margin10-r" id="incomeAmountBeforeTaxes_1" name="incomeBeforeTaxes" ng-model="householdMemberIncomeJob.incomeAmountBeforeTaxes" required numbers-Only ng-trim/>
               <select id="incomeFrequency_1" name="incomeFrequency" class="span5" ng-model="householdMemberIncomeJob.incomeFrequency" required>
                 <option><spring:message code="label.lce.weekly"/>Weekly</option>
			           <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			           <option><spring:message code="label.lce.monthly"/>Monthly</option>
			           <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			           <option><spring:message code="label.lce.yearly"/>Yearly</option>
			           <option><spring:message code="label.lce.onceonly"/>Once Only</option>
               </select>
             </div>
           </div>
           <div class="controls margin5-t" ng-show="submitted && jobIncomeRepeat.incomeBeforeTaxes.$error.required">
  		       <span class="validation-error-text-container"><spring:message code="label.lce.incomejob.error2"/></span>
  		     </div>
  		     <div class="controls margin5-t" ng-show="submitted && jobIncomeRepeat.incomeFrequency.$error.required">
  		       <span class="validation-error-text-container"><spring:message code="label.lce.incomejob.error3"/></span>
  		     </div>
         </div>

         <spring:message code="label.lce.howmanyhoursdoes"/> {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}} <spring:message code="label.lce.workperweek"/>
         <div class="control-group">
           <label class="control-label" for="typeOfRequest"></label>
           <div class="controls margin20-b">
             <input type="text" ng-model="householdMemberIncomeJob.workHours" name="workHours" required numbers-only ng-trim numbers-only-max="168"/>
           </div>
           <div class="controls margin5-t">
  	       	 <span class="validation-error-text-container" ng-show="submitted && jobIncomeRepeat.workHours.$error.required"><spring:message code="label.lce.incomejob.error4"/></span>
   	         <span class="validation-error-text-container" ng-show="submitted && jobIncomeRepeat.workHours.$error.maxNumber"><spring:message code="label.lce.incomejob.error5"/></span>
  	       </div>
         </div>

         <!-- Removed jQuery for this function - to add more jobs -->
         <!-- Check out the script.js for the controller called 'contact' -->
         <div class="center">
           <span style="display:none;" class="jobIncomeformValidation">{{jobIncomeRepeat.$valid}}</span>
           <a href="" class="btn btn-primary" ng-show="{{$index == 0}}" ng-click="addIncomeJob()"><spring:message code="label.lce.addanotherjob"/></a>
        	 <a href="" class="btn btn-primary" ng-show="{{$index > 0}}" ng-click="removeIncomeJob(householdMemberIncomeJob)"><spring:message code="label.lce.removethisjob"/></a>
         </div>
  		    <hr ng-hide="{{ ((0 == $index)&&($index == (householdMember.detailedIncome.jobIncome.length-1))) || ($index == (householdMember.detailedIncome.jobIncome.length-1))}}"/>
  		  </ng-form>
  	  </div>
    </form>
  </div>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="isAllFormValid()"  class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!isAllFormValid()" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>