
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.hhs.cms.dsh.sim.ee.vlp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountryOfIssuance_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CountryOfIssuance");
    private final static QName _NonCitCountryBirthCd_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitCountryBirthCd");
    private final static QName _CitizenshipNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CitizenshipNumber");
    private final static QName _Agency3InitVerifIndividualResponse_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifIndividualResponse");
    private final static QName _I327DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I327DocumentID");
    private final static QName _Agency3InitVerifRequest_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifRequest");
    private final static QName _TDSResponseDescriptionText_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "TDSResponseDescriptionText");
    private final static QName _Agency3InitVerifResponse_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifResponse");
    private final static QName _CategoryCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CategoryCode");
    private final static QName _ArrayOfSponsorshipData_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "ArrayOfSponsorshipData");
    private final static QName _OtherCase1DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "OtherCase1DocumentID");
    private final static QName _MacReadI551DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "MacReadI551DocumentID");
    private final static QName _FiveYearBarMetCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "FiveYearBarMetCode");
    private final static QName _PhotoIncludedIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "PhotoIncludedIndicator");
    private final static QName _TempI551DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "TempI551DocumentID");
    private final static QName _WebServSftwrVer_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "WebServSftwrVer");
    private final static QName _Comments_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Comments");
    private final static QName _RequestedCoverageStartDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "RequestedCoverageStartDate");
    private final static QName _DS2019DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DS2019DocumentID");
    private final static QName _EligStatementTxt_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "EligStatementTxt");
    private final static QName _CertOfCitDocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CertOfCitDocumentID");
    private final static QName _Agency3InitVerifIndividualResponseSet_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifIndividualResponseSet");
    private final static QName _PassportNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "PassportNumber");
    private final static QName _NonCitCountryCitCd_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitCountryCitCd");
    private final static QName _USCitizenCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "USCitizenCode");
    private final static QName _ReceiptNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "ReceiptNumber");
    private final static QName _NatrOfCertDocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NatrOfCertDocumentID");
    private final static QName _GrantDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "GrantDate");
    private final static QName _DocExpirationDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DocExpirationDate");
    private final static QName _IAVTypeTxt_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "IAVTypeTxt");
    private final static QName _I571DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I571DocumentID");
    private final static QName _ResponseCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "ResponseCode");
    private final static QName _EligStatementCd_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "EligStatementCd");
    private final static QName _LawfulPresenceVerifiedCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "LawfulPresenceVerifiedCode");
    private final static QName _AlienNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "AlienNumber");
    private final static QName _DHSID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DHSID");
    private final static QName _I20DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I20DocumentID");
    private final static QName _SEVISID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "SEVISID");
    private final static QName _DSHAutoTriggerStepTwo_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DSHAutoTriggerStepTwo");
    private final static QName _UnexpForeignPassportDocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "UnexpForeignPassportDocumentID");
    private final static QName _CaseNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CaseNumber");
    private final static QName _NonCitBirthDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitBirthDate");
    private final static QName _PhotoBinaryAttachment_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "PhotoBinaryAttachment");
    private final static QName _NonCitAdmittedToDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitAdmittedToDate");
    private final static QName _NonCitFirstName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitFirstName");
    private final static QName _RequestSponsorDataIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "RequestSponsorDataIndicator");
    private final static QName _NonCitLastName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitLastName");
    private final static QName _LastName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "LastName");
    private final static QName _NonCitEntryDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitEntryDate");
    private final static QName _GrantDateReasonCd_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "GrantDateReasonCd");
    private final static QName _I551DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I551DocumentID");
    private final static QName _IAVTypeCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "IAVTypeCode");
    private final static QName _SponsorshipReasonCd_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "SponsorshipReasonCd");
    private final static QName _I94UnexpForeignPassportDocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I94UnexpForeignPassportDocumentID");
    private final static QName _DateOfBirth_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DateOfBirth");
    private final static QName _I94DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I94DocumentID");
    private final static QName _SponsorDataFoundIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "SponsorDataFoundIndicator");
    private final static QName _ResponseMetadata_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "ResponseMetadata");
    private final static QName _CaseSentToSecondaryIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "CaseSentToSecondaryIndicator");
    private final static QName _RequesterCommentsForHub_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "RequesterCommentsForHub");
    private final static QName _VisaNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "VisaNumber");
    private final static QName _FirstName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "FirstName");
    private final static QName _NonCitEadsExpireDate_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitEadsExpireDate");
    private final static QName _DocDescReq_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "DocDescReq");
    private final static QName _NonCitAdmittedToText_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitAdmittedToText");
    private final static QName _OtherCase2DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "OtherCase2DocumentID");
    private final static QName _NonCitCoaCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitCoaCode");
    private final static QName _FiveYearBarApplyCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "FiveYearBarApplyCode");
    private final static QName _RequestGrantDateIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "RequestGrantDateIndicator");
    private final static QName _QualifiedNonCitizenCode_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "QualifiedNonCitizenCode");
    private final static QName _FiveYearBarApplicabilityIndicator_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "FiveYearBarApplicabilityIndicator");
    private final static QName _I766DocumentID_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I766DocumentID");
    private final static QName _AKA_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "AKA");
    private final static QName _MiddleName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "MiddleName");
    private final static QName _PassportCountry_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "PassportCountry");
    private final static QName _ResponseDescriptionText_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "ResponseDescriptionText");
    private final static QName _NaturalizationNumber_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NaturalizationNumber");
    private final static QName _NonCitMiddleName_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "NonCitMiddleName");
    private final static QName _Agency3InitVerifRequestSet_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifRequestSet");
    private final static QName _Agency3InitVerifResponseSet_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "Agency3InitVerifResponseSet");
    private final static QName _I94Number_QNAME = new QName("http://vlp.ee.sim.dsh.cms.hhs.gov", "I94Number");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.hhs.cms.dsh.sim.ee.vlp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Agency3InitVerifResponseType }
     * 
     */
    public Agency3InitVerifResponseType createAgency3InitVerifResponseType() {
        return new Agency3InitVerifResponseType();
    }

    /**
     * Create an instance of {@link Agency3InitVerifRequestType }
     * 
     */
    public Agency3InitVerifRequestType createAgency3InitVerifRequestType() {
        return new Agency3InitVerifRequestType();
    }

    /**
     * Create an instance of {@link I327DocumentID3Type }
     * 
     */
    public I327DocumentID3Type createI327DocumentID3Type() {
        return new I327DocumentID3Type();
    }

    /**
     * Create an instance of {@link Agency3InitVerifIndividualResponseType }
     * 
     */
    public Agency3InitVerifIndividualResponseType createAgency3InitVerifIndividualResponseType() {
        return new Agency3InitVerifIndividualResponseType();
    }

    /**
     * Create an instance of {@link UnexpForeignPassportDocumentID30Type }
     * 
     */
    public UnexpForeignPassportDocumentID30Type createUnexpForeignPassportDocumentID30Type() {
        return new UnexpForeignPassportDocumentID30Type();
    }

    /**
     * Create an instance of {@link ResponseMetadataType }
     * 
     */
    public ResponseMetadataType createResponseMetadataType() {
        return new ResponseMetadataType();
    }

    /**
     * Create an instance of {@link TempI551DocumentID21Type }
     * 
     */
    public TempI551DocumentID21Type createTempI551DocumentID21Type() {
        return new TempI551DocumentID21Type();
    }

    /**
     * Create an instance of {@link MacReadI551DocumentID22Type }
     * 
     */
    public MacReadI551DocumentID22Type createMacReadI551DocumentID22Type() {
        return new MacReadI551DocumentID22Type();
    }

    /**
     * Create an instance of {@link I94DocumentID2Type }
     * 
     */
    public I94DocumentID2Type createI94DocumentID2Type() {
        return new I94DocumentID2Type();
    }

    /**
     * Create an instance of {@link OtherCase1DocumentID1Type }
     * 
     */
    public OtherCase1DocumentID1Type createOtherCase1DocumentID1Type() {
        return new OtherCase1DocumentID1Type();
    }

    /**
     * Create an instance of {@link ArrayOfSponsorshipDataType }
     * 
     */
    public ArrayOfSponsorshipDataType createArrayOfSponsorshipDataType() {
        return new ArrayOfSponsorshipDataType();
    }

    /**
     * Create an instance of {@link I94UnexpForeignPassportDocumentID10Type }
     * 
     */
    public I94UnexpForeignPassportDocumentID10Type createI94UnexpForeignPassportDocumentID10Type() {
        return new I94UnexpForeignPassportDocumentID10Type();
    }

    /**
     * Create an instance of {@link I551DocumentID4Type }
     * 
     */
    public I551DocumentID4Type createI551DocumentID4Type() {
        return new I551DocumentID4Type();
    }

    /**
     * Create an instance of {@link OtherCase2DocumentID1Type }
     * 
     */
    public OtherCase2DocumentID1Type createOtherCase2DocumentID1Type() {
        return new OtherCase2DocumentID1Type();
    }

    /**
     * Create an instance of {@link NatrOfCertDocumentID20Type }
     * 
     */
    public NatrOfCertDocumentID20Type createNatrOfCertDocumentID20Type() {
        return new NatrOfCertDocumentID20Type();
    }

    /**
     * Create an instance of {@link Agency3InitVerifIndividualResponseSetType }
     * 
     */
    public Agency3InitVerifIndividualResponseSetType createAgency3InitVerifIndividualResponseSetType() {
        return new Agency3InitVerifIndividualResponseSetType();
    }

    /**
     * Create an instance of {@link CertOfCitDocumentID23Type }
     * 
     */
    public CertOfCitDocumentID23Type createCertOfCitDocumentID23Type() {
        return new CertOfCitDocumentID23Type();
    }

    /**
     * Create an instance of {@link DS2019DocumentID27Type }
     * 
     */
    public DS2019DocumentID27Type createDS2019DocumentID27Type() {
        return new DS2019DocumentID27Type();
    }

    /**
     * Create an instance of {@link Agency3InitVerifRequestSetType }
     * 
     */
    public Agency3InitVerifRequestSetType createAgency3InitVerifRequestSetType() {
        return new Agency3InitVerifRequestSetType();
    }

    /**
     * Create an instance of {@link Agency3InitVerifResponseSetType }
     * 
     */
    public Agency3InitVerifResponseSetType createAgency3InitVerifResponseSetType() {
        return new Agency3InitVerifResponseSetType();
    }

    /**
     * Create an instance of {@link DHSIDType }
     * 
     */
    public DHSIDType createDHSIDType() {
        return new DHSIDType();
    }

    /**
     * Create an instance of {@link I20DocumentID26Type }
     * 
     */
    public I20DocumentID26Type createI20DocumentID26Type() {
        return new I20DocumentID26Type();
    }

    /**
     * Create an instance of {@link I571DocumentID5Type }
     * 
     */
    public I571DocumentID5Type createI571DocumentID5Type() {
        return new I571DocumentID5Type();
    }

    /**
     * Create an instance of {@link PassportCountryType }
     * 
     */
    public PassportCountryType createPassportCountryType() {
        return new PassportCountryType();
    }

    /**
     * Create an instance of {@link I766DocumentID9Type }
     * 
     */
    public I766DocumentID9Type createI766DocumentID9Type() {
        return new I766DocumentID9Type();
    }

    /**
     * Create an instance of {@link SponsorshipDataType }
     * 
     */
    public SponsorshipDataType createSponsorshipDataType() {
        return new SponsorshipDataType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CountryOfIssuance")
    public JAXBElement<String> createCountryOfIssuance(String value) {
        return new JAXBElement<String>(_CountryOfIssuance_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitCountryBirthCd")
    public JAXBElement<String> createNonCitCountryBirthCd(String value) {
        return new JAXBElement<String>(_NonCitCountryBirthCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CitizenshipNumber")
    public JAXBElement<String> createCitizenshipNumber(String value) {
        return new JAXBElement<String>(_CitizenshipNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifIndividualResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifIndividualResponse")
    public JAXBElement<Agency3InitVerifIndividualResponseType> createAgency3InitVerifIndividualResponse(Agency3InitVerifIndividualResponseType value) {
        return new JAXBElement<Agency3InitVerifIndividualResponseType>(_Agency3InitVerifIndividualResponse_QNAME, Agency3InitVerifIndividualResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I327DocumentID3Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I327DocumentID")
    public JAXBElement<I327DocumentID3Type> createI327DocumentID(I327DocumentID3Type value) {
        return new JAXBElement<I327DocumentID3Type>(_I327DocumentID_QNAME, I327DocumentID3Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifRequest")
    public JAXBElement<Agency3InitVerifRequestType> createAgency3InitVerifRequest(Agency3InitVerifRequestType value) {
        return new JAXBElement<Agency3InitVerifRequestType>(_Agency3InitVerifRequest_QNAME, Agency3InitVerifRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "TDSResponseDescriptionText")
    public JAXBElement<String> createTDSResponseDescriptionText(String value) {
        return new JAXBElement<String>(_TDSResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifResponse")
    public JAXBElement<Agency3InitVerifResponseType> createAgency3InitVerifResponse(Agency3InitVerifResponseType value) {
        return new JAXBElement<Agency3InitVerifResponseType>(_Agency3InitVerifResponse_QNAME, Agency3InitVerifResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CategoryCode")
    public JAXBElement<String> createCategoryCode(String value) {
        return new JAXBElement<String>(_CategoryCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSponsorshipDataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "ArrayOfSponsorshipData")
    public JAXBElement<ArrayOfSponsorshipDataType> createArrayOfSponsorshipData(ArrayOfSponsorshipDataType value) {
        return new JAXBElement<ArrayOfSponsorshipDataType>(_ArrayOfSponsorshipData_QNAME, ArrayOfSponsorshipDataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherCase1DocumentID1Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "OtherCase1DocumentID")
    public JAXBElement<OtherCase1DocumentID1Type> createOtherCase1DocumentID(OtherCase1DocumentID1Type value) {
        return new JAXBElement<OtherCase1DocumentID1Type>(_OtherCase1DocumentID_QNAME, OtherCase1DocumentID1Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MacReadI551DocumentID22Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "MacReadI551DocumentID")
    public JAXBElement<MacReadI551DocumentID22Type> createMacReadI551DocumentID(MacReadI551DocumentID22Type value) {
        return new JAXBElement<MacReadI551DocumentID22Type>(_MacReadI551DocumentID_QNAME, MacReadI551DocumentID22Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarMetCode")
    public JAXBElement<String> createFiveYearBarMetCode(String value) {
        return new JAXBElement<String>(_FiveYearBarMetCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "PhotoIncludedIndicator")
    public JAXBElement<Boolean> createPhotoIncludedIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_PhotoIncludedIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TempI551DocumentID21Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "TempI551DocumentID")
    public JAXBElement<TempI551DocumentID21Type> createTempI551DocumentID(TempI551DocumentID21Type value) {
        return new JAXBElement<TempI551DocumentID21Type>(_TempI551DocumentID_QNAME, TempI551DocumentID21Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "WebServSftwrVer")
    public JAXBElement<String> createWebServSftwrVer(String value) {
        return new JAXBElement<String>(_WebServSftwrVer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Comments")
    public JAXBElement<String> createComments(String value) {
        return new JAXBElement<String>(_Comments_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "RequestedCoverageStartDate")
    public JAXBElement<XMLGregorianCalendar> createRequestedCoverageStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RequestedCoverageStartDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DS2019DocumentID27Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DS2019DocumentID")
    public JAXBElement<DS2019DocumentID27Type> createDS2019DocumentID(DS2019DocumentID27Type value) {
        return new JAXBElement<DS2019DocumentID27Type>(_DS2019DocumentID_QNAME, DS2019DocumentID27Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "EligStatementTxt")
    public JAXBElement<String> createEligStatementTxt(String value) {
        return new JAXBElement<String>(_EligStatementTxt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CertOfCitDocumentID23Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CertOfCitDocumentID")
    public JAXBElement<CertOfCitDocumentID23Type> createCertOfCitDocumentID(CertOfCitDocumentID23Type value) {
        return new JAXBElement<CertOfCitDocumentID23Type>(_CertOfCitDocumentID_QNAME, CertOfCitDocumentID23Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifIndividualResponseSetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifIndividualResponseSet")
    public JAXBElement<Agency3InitVerifIndividualResponseSetType> createAgency3InitVerifIndividualResponseSet(Agency3InitVerifIndividualResponseSetType value) {
        return new JAXBElement<Agency3InitVerifIndividualResponseSetType>(_Agency3InitVerifIndividualResponseSet_QNAME, Agency3InitVerifIndividualResponseSetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "PassportNumber")
    public JAXBElement<String> createPassportNumber(String value) {
        return new JAXBElement<String>(_PassportNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitCountryCitCd")
    public JAXBElement<String> createNonCitCountryCitCd(String value) {
        return new JAXBElement<String>(_NonCitCountryCitCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "USCitizenCode")
    public JAXBElement<String> createUSCitizenCode(String value) {
        return new JAXBElement<String>(_USCitizenCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "ReceiptNumber")
    public JAXBElement<String> createReceiptNumber(String value) {
        return new JAXBElement<String>(_ReceiptNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NatrOfCertDocumentID20Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NatrOfCertDocumentID")
    public JAXBElement<NatrOfCertDocumentID20Type> createNatrOfCertDocumentID(NatrOfCertDocumentID20Type value) {
        return new JAXBElement<NatrOfCertDocumentID20Type>(_NatrOfCertDocumentID_QNAME, NatrOfCertDocumentID20Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "GrantDate")
    public JAXBElement<XMLGregorianCalendar> createGrantDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GrantDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DocExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createDocExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DocExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "IAVTypeTxt")
    public JAXBElement<String> createIAVTypeTxt(String value) {
        return new JAXBElement<String>(_IAVTypeTxt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I571DocumentID5Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I571DocumentID")
    public JAXBElement<I571DocumentID5Type> createI571DocumentID(I571DocumentID5Type value) {
        return new JAXBElement<I571DocumentID5Type>(_I571DocumentID_QNAME, I571DocumentID5Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "ResponseCode")
    public JAXBElement<String> createResponseCode(String value) {
        return new JAXBElement<String>(_ResponseCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "EligStatementCd")
    public JAXBElement<BigInteger> createEligStatementCd(BigInteger value) {
        return new JAXBElement<BigInteger>(_EligStatementCd_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "LawfulPresenceVerifiedCode")
    public JAXBElement<String> createLawfulPresenceVerifiedCode(String value) {
        return new JAXBElement<String>(_LawfulPresenceVerifiedCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "AlienNumber")
    public JAXBElement<String> createAlienNumber(String value) {
        return new JAXBElement<String>(_AlienNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DHSIDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DHSID")
    public JAXBElement<DHSIDType> createDHSID(DHSIDType value) {
        return new JAXBElement<DHSIDType>(_DHSID_QNAME, DHSIDType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I20DocumentID26Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I20DocumentID")
    public JAXBElement<I20DocumentID26Type> createI20DocumentID(I20DocumentID26Type value) {
        return new JAXBElement<I20DocumentID26Type>(_I20DocumentID_QNAME, I20DocumentID26Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "SEVISID")
    public JAXBElement<String> createSEVISID(String value) {
        return new JAXBElement<String>(_SEVISID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DSHAutoTriggerStepTwo")
    public JAXBElement<Boolean> createDSHAutoTriggerStepTwo(Boolean value) {
        return new JAXBElement<Boolean>(_DSHAutoTriggerStepTwo_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnexpForeignPassportDocumentID30Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "UnexpForeignPassportDocumentID")
    public JAXBElement<UnexpForeignPassportDocumentID30Type> createUnexpForeignPassportDocumentID(UnexpForeignPassportDocumentID30Type value) {
        return new JAXBElement<UnexpForeignPassportDocumentID30Type>(_UnexpForeignPassportDocumentID_QNAME, UnexpForeignPassportDocumentID30Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CaseNumber")
    public JAXBElement<String> createCaseNumber(String value) {
        return new JAXBElement<String>(_CaseNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitBirthDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitBirthDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitBirthDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "PhotoBinaryAttachment")
    public JAXBElement<byte[]> createPhotoBinaryAttachment(byte[] value) {
        return new JAXBElement<byte[]>(_PhotoBinaryAttachment_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitAdmittedToDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitAdmittedToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitAdmittedToDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitFirstName")
    public JAXBElement<String> createNonCitFirstName(String value) {
        return new JAXBElement<String>(_NonCitFirstName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "RequestSponsorDataIndicator")
    public JAXBElement<Boolean> createRequestSponsorDataIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestSponsorDataIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitLastName")
    public JAXBElement<String> createNonCitLastName(String value) {
        return new JAXBElement<String>(_NonCitLastName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "LastName")
    public JAXBElement<String> createLastName(String value) {
        return new JAXBElement<String>(_LastName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitEntryDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitEntryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitEntryDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "GrantDateReasonCd")
    public JAXBElement<String> createGrantDateReasonCd(String value) {
        return new JAXBElement<String>(_GrantDateReasonCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I551DocumentID4Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I551DocumentID")
    public JAXBElement<I551DocumentID4Type> createI551DocumentID(I551DocumentID4Type value) {
        return new JAXBElement<I551DocumentID4Type>(_I551DocumentID_QNAME, I551DocumentID4Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "IAVTypeCode")
    public JAXBElement<String> createIAVTypeCode(String value) {
        return new JAXBElement<String>(_IAVTypeCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "SponsorshipReasonCd")
    public JAXBElement<String> createSponsorshipReasonCd(String value) {
        return new JAXBElement<String>(_SponsorshipReasonCd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I94UnexpForeignPassportDocumentID10Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I94UnexpForeignPassportDocumentID")
    public JAXBElement<I94UnexpForeignPassportDocumentID10Type> createI94UnexpForeignPassportDocumentID(I94UnexpForeignPassportDocumentID10Type value) {
        return new JAXBElement<I94UnexpForeignPassportDocumentID10Type>(_I94UnexpForeignPassportDocumentID_QNAME, I94UnexpForeignPassportDocumentID10Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DateOfBirth")
    public JAXBElement<XMLGregorianCalendar> createDateOfBirth(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateOfBirth_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I94DocumentID2Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I94DocumentID")
    public JAXBElement<I94DocumentID2Type> createI94DocumentID(I94DocumentID2Type value) {
        return new JAXBElement<I94DocumentID2Type>(_I94DocumentID_QNAME, I94DocumentID2Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "SponsorDataFoundIndicator")
    public JAXBElement<Boolean> createSponsorDataFoundIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SponsorDataFoundIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "ResponseMetadata")
    public JAXBElement<ResponseMetadataType> createResponseMetadata(ResponseMetadataType value) {
        return new JAXBElement<ResponseMetadataType>(_ResponseMetadata_QNAME, ResponseMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "CaseSentToSecondaryIndicator")
    public JAXBElement<Boolean> createCaseSentToSecondaryIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_CaseSentToSecondaryIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "RequesterCommentsForHub")
    public JAXBElement<String> createRequesterCommentsForHub(String value) {
        return new JAXBElement<String>(_RequesterCommentsForHub_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "VisaNumber")
    public JAXBElement<String> createVisaNumber(String value) {
        return new JAXBElement<String>(_VisaNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "FirstName")
    public JAXBElement<String> createFirstName(String value) {
        return new JAXBElement<String>(_FirstName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitEadsExpireDate")
    public JAXBElement<XMLGregorianCalendar> createNonCitEadsExpireDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NonCitEadsExpireDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "DocDescReq")
    public JAXBElement<String> createDocDescReq(String value) {
        return new JAXBElement<String>(_DocDescReq_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitAdmittedToText")
    public JAXBElement<String> createNonCitAdmittedToText(String value) {
        return new JAXBElement<String>(_NonCitAdmittedToText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherCase2DocumentID1Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "OtherCase2DocumentID")
    public JAXBElement<OtherCase2DocumentID1Type> createOtherCase2DocumentID(OtherCase2DocumentID1Type value) {
        return new JAXBElement<OtherCase2DocumentID1Type>(_OtherCase2DocumentID_QNAME, OtherCase2DocumentID1Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitCoaCode")
    public JAXBElement<String> createNonCitCoaCode(String value) {
        return new JAXBElement<String>(_NonCitCoaCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarApplyCode")
    public JAXBElement<String> createFiveYearBarApplyCode(String value) {
        return new JAXBElement<String>(_FiveYearBarApplyCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "RequestGrantDateIndicator")
    public JAXBElement<Boolean> createRequestGrantDateIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestGrantDateIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "QualifiedNonCitizenCode")
    public JAXBElement<String> createQualifiedNonCitizenCode(String value) {
        return new JAXBElement<String>(_QualifiedNonCitizenCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "FiveYearBarApplicabilityIndicator")
    public JAXBElement<Boolean> createFiveYearBarApplicabilityIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_FiveYearBarApplicabilityIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link I766DocumentID9Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I766DocumentID")
    public JAXBElement<I766DocumentID9Type> createI766DocumentID(I766DocumentID9Type value) {
        return new JAXBElement<I766DocumentID9Type>(_I766DocumentID_QNAME, I766DocumentID9Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "AKA")
    public JAXBElement<String> createAKA(String value) {
        return new JAXBElement<String>(_AKA_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "MiddleName")
    public JAXBElement<String> createMiddleName(String value) {
        return new JAXBElement<String>(_MiddleName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PassportCountryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "PassportCountry")
    public JAXBElement<PassportCountryType> createPassportCountry(PassportCountryType value) {
        return new JAXBElement<PassportCountryType>(_PassportCountry_QNAME, PassportCountryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "ResponseDescriptionText")
    public JAXBElement<String> createResponseDescriptionText(String value) {
        return new JAXBElement<String>(_ResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NaturalizationNumber")
    public JAXBElement<String> createNaturalizationNumber(String value) {
        return new JAXBElement<String>(_NaturalizationNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "NonCitMiddleName")
    public JAXBElement<String> createNonCitMiddleName(String value) {
        return new JAXBElement<String>(_NonCitMiddleName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifRequestSetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifRequestSet")
    public JAXBElement<Agency3InitVerifRequestSetType> createAgency3InitVerifRequestSet(Agency3InitVerifRequestSetType value) {
        return new JAXBElement<Agency3InitVerifRequestSetType>(_Agency3InitVerifRequestSet_QNAME, Agency3InitVerifRequestSetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Agency3InitVerifResponseSetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "Agency3InitVerifResponseSet")
    public JAXBElement<Agency3InitVerifResponseSetType> createAgency3InitVerifResponseSet(Agency3InitVerifResponseSetType value) {
        return new JAXBElement<Agency3InitVerifResponseSetType>(_Agency3InitVerifResponseSet_QNAME, Agency3InitVerifResponseSetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", name = "I94Number")
    public JAXBElement<String> createI94Number(String value) {
        return new JAXBElement<String>(_I94Number_QNAME, String.class, null, value);
    }

}
