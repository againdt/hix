<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<style>
#sidebar h4, #sidebar .header, .gray h4, .header{
	max-height: 10%;
}
h4 small.headerSmall{
	font-size:12px;
	color: #666;
}

.grayBgTable{
	background-color:#F8F8F8;
}

.page-breadcrumb{
	display:inline;
}

h4.rule, .span9 .rule{
	border-bottom:1px solid #999;
	padding-bottom:14px;
}

.top30{
	margin-top:30px;
}
</style>
</head>
<body>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#"> Back to FAQ Home</a></li>
                    <li>Information Management for Enrollment Entities</li>
                </ul><!--page-breadcrumb ends-->
		<h1 class="gutter10"><a name="skip"></a>Information Management for Enrollment Entities</h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	
		<h4 class="margin0"></h4>
		
	             <form class="form-vertical grayBgTable" id="frmbrokerreg" name="frmbrokerreg" action="#" method="POST">
							<ul class="nav nav-list" id="nav">
								<li><a href="#entity1">Understand the Role of Enrollment Entity</a></li>
								<li><a href="#entity2">Apply to Become a Registered Enrollment Entity</a></li>
								<li><a href="#entity3">Manage Your Account Information</a></li>
								<li><a href="#entity4">Manage Enrollment Counselors Affiliated With You</a></li>
								<li><a href="#entity5">Manage Individuals Seeking Your Assistance</a></li>
							</ul>
				 </form>
				 
	</div>
    	
       			<div class="span9 " id="rightpanel">
       				<div class="entityFaq1" id="entity1">
					<h4 class="rule" id="role-top">Understand the Role of Enrollment Entity 
					
					<!-- <div class="pull-right">
					<button class="btn btn-small btn-back"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question <i class=" icon-chevron-right"></i></button> </div> -->
					
					<a id="role-top" href="#" class="pull-right">Back to Top</a>
					
					</h4>
					
					<p>An enrollment entity is an entity or organization eligible to be trained and registered to provide in-person assistance to consumers at the health insurance marketplace. A consumer may be an individual or entity seeking information on eligibility and enrollment, or seeking application assistance with a health plan offered by the state health insurance marketplace. To apply to become a registered enrollment entity at the health insurance marketplace, you must be an organization that can demonstrate to the marketplace that you have existing relationships with consumers or self-employed individuals likely to be eligible for enrollment at the marketplace. Examples of such organizations include:</p>

					<ul>
						<li>American Indian tribe or tribal organizations</li>
						<li>Attorneys; for example, family law attorneys who have clients that are experiencing life transitions</li>
						<li>Chambers of commerce</li>
						<li>City government agency</li>
						<li>Commercial fishing industry organizations</li>
						<li>Community clinics</li>
						<li>Community colleges and universities</li>
						<li>County departments of public health, city health departments, or county departments that deliver health services</li>
						<li>Faith-based or other non-profit organizations</li>
						<li>Indian health services facilities</li>
						<li>Licensed health care clinics and providers</li>
						<li>Labor unions</li>
						<li>Ranching and farming organizations</li>
						<li>Resource partners of small businesses</li>
						<li>School districts</li>
						<li>Tax preparers</li>
						<li>Trade, industry, and professional organizations</li>
					</ul>
					
					The health insurance marketplace may require you to provide proof of a current or valid license, authority, certificate, or registration by the appropriate regulatory or licensing entity as a condition of eligibility to be registered as an enrollment entity. According to the state's In-Person Assistance (IPA) Program, once registered by the health insurance marketplace, you may be compensated for successful enrollment of consumers in the marketplace.

						<h4>Learn more:</h4>
						<div class="accordion" id="role-accordian">
							  <div class="accordion-group">
							    <div class="accordion-heading">
							      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian" href="#collapseOne">
							        Responsibilities of an Enrollment Entity
							      </a>
							    </div>
							    <div id="collapseOne" class="accordion-body collapse">
							      <div class="accordion-inner">
															<p>Once registered at the health insurance marketplace, you will be required to:</p>
															
															<ul>
																<li>Conduct public education activities raising awareness about the health insurance plans available through the marketplace</li>
																<li>Distribute information concerning and facilitate enrollment into health plans available through the health insurance marketplace</li>
																<li>Provide referrals to consumer assistance programs that are culturally and linguistically appropriate</li>
																
															</ul>
															
															<p>To learn more, see Apply to Become a Registered Enrollment Entity.</p>
							      </div>
							    </div>
							  </div>
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian" href="#collapseTwo">
								        Certified Enrollment Counselors
								      </a>
								    </div>
								    <div id="collapseTwo" class="accordion-body collapse">
								      <div class="accordion-inner">
																<p>If you are registered at the health insurance marketplace, individuals can affiliate with you to offer in-person assistance to consumers of the marketplace. This will allow you to offer your services to a wider range of population. Certified Enrollment Counselors are individuals affiliated with your organization that have been trained and certified by the marketplace to offer face-to-face, one-on-one consumer assistance. To become a certified enrollment counselor, an individual must:</p>
														
																<ul>
																	<li>Become affiliated with and submit an application to you (enrollment entity)</li>
																	<li>Complete the fingerprinting process</li>
																	<li>Complete required training</li>
																	<li>Not have a conflict of interest with you or the health insurance marketplace</li>
																	<li>Comply with all privacy and security standards set by the marketplace</li>

																	
																</ul>
																
																<p>To learn more, see Apply to Become a Registered Enrollment Entity.</p>
								      </div>
								    </div>
								  </div>
								  
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian" href="#collapseThree">
								        Additional Information
								      </a>
								    </div>
								    <div id="collapseThree" class="accordion-body collapse">
								      <div class="accordion-inner">
								        <p>For detailed and most current information about the state's health insurance marketplace enrollment assistance program, visit http://www.healthexchange.ca.gov/Pages/AssistersProgram.aspx.</p>
								      </div>
								    </div>
								  </div>
								</div>
						</div>
						<!--end of .entityFaq1-->
						
						<div class="entityFaq2" id="entity2">
							<h4 class="rule" id="role-top">Apply to Become a Registered Enrollment Entity 
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-entity2"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity2" href="#entity3">Next Question 2 <i class=" icon-chevron-right"></i></button> </div>
							 -->
							 
							 <a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>As an enrollment entity, you can be registered in the In-Person Assistance Program and receive compensation for successful enrollments made through you. You may register with the health insurance marketplace and maintain an enrollment counselor roster for your account. Once the administrator at the marketplace approves your application, the counselors in your roster will be notified to create an account at the marketplace. If you have counselors that have been certified by the marketplace administrator, individuals searching the marketplace for in-person assistance can find you based on their search criteria.</p>

								<p>Note: Once you have submitted your application, you can make changes only to enrollment counselor information. You cannot make changes to any of the other information, such as subsites, languages, and hours of operation you have provided. You will need to contact the marketplace administrator if you need to do so.</p>

								<p>To apply:</p>
							<ol>

								<li>Navigate to the state's health insurance marketplace website.</li>
								<li>Click Create Account.</li>
								<li>Click Certified Enrollment Entity.</li>
								<li>Complete the account creation process. Once you have created an account at the health insurance marketplace, you can log in to the marketplace at any time and complete your application.</li>
								<li>Once logged in to the health insurance marketplace, on the Entity Information page, provide your Entity Name, Business Legal Name, Email Address, Phone and Fax Number, Preferred Method of Communication, Federal and State Tax ID, Organization Type, Counties Served, and indications of whether you receive payments or have received an Outreach and Education Grant.</li>
								<li>Click Next.</li>
								<li>Specify the people groups you can serve by selecting Languages, Ethnicities, and Industries. For example, to specify languages you can serve in, use the steps that follow. You can select Ethnicities and Industries in a similar manner.
									<ol>
										<li>Click the arrow beside Languages. This expands the screen to display a list of languages.</li>
										<li>Select the languages you can serve in.</li>
										<li>For each language selected, specify the percentage of people in that language group you target to serve as well as the number of your staff that speak the language. Note that the total of your percentage values must add up to 100%.</li>
									</ol>
								</li>
								<li>Click Next.
								<li>Specify your Locations and Hours by entering your Primary Site Name and contact information, your business hours for each day of the week, your mailing and physical addresses, and the languages supported by the staff at your primary site. If your physical and mailing addresses are the same, select Same as Mailing Address.
								<li>Click Next. This opens the Locations and Hours - Subsite page. You may choose not to enter subsite information now, but to add subsites after you submit the application, you will need to contact the marketplace administrator.
									<ul>
										<li>If you do not want to add subsites now, click Next.</li>
										<li>To add subsites now, follow these steps:
											<ol>
												<li>Click Add Subsite.</li>
												<li>Enter the site name, contact information, hours of operation, mailing and physical addresses, and the languages supported at the site. If the physical and mailing addresses are the same, select Same as Mailing Address.</li>
												<li>Click Save Subsite.</li>
												<li>To add another subsite, click Add Subsite.</li>
												<li>When you are finished with adding subsites, click Next.</li>
											</ol>
										</li>
									</ul>
								</li>
								
								<li>Enter your Primary Contact and Financial Contact information.</li>
								<li>Click Next to go to the Enrollment Counselors page. You may choose to add enrollment counselors now or later. See Add an Enrollment Counselor to learn more.
									<ul>
										<li>If you do not want to add enrollment counselors now, click Done.</li>
										<li>To add enrollment counselors to your roster now, follow these steps:
											<ol>
												<li>Click Add Enrollment Counselors.</li>
												<li>Enter the name, contact information, business name, enrollment counselor certification status, enrollment counselor site information--whether primary or secondary, mailing address, and profile information of the enrollment counselor. Profile information includes languages in which the enrollment counselor can provide spoken or written support, level of education, and photo of the enrollment counselor.</li>
												<li>Click Save Enrollment Counselor.</li>
												<li>To add another enrollment counselor, click Add Enrollment Counselor.</li>
												<li>When you are finished with adding all the enrollment counselors, click Done. The Payment Information page opens.</li>
											</ol>
										</li>
									</ul>
								</li>
								<li>Enter your bank account information and the address to which payment should be sent.</li>
								<li>Click Next. The Success screen appears indicating that you have successfully completed the Enrollment Entity application.</li>
								<li>Click OK. The Registration Status page appears showing the Status of your application as Pending.</li>
							</ol>						
							
						</div>
						<!--/.entityFaq2-->
						
						<div class="entityFaq3" id="entity3">
							<h4 class="rule" id="role-top">Manage Your Account Information
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							<a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>You create your profile at the health insurance marketplace when applying to the marketplace to be registered. See Apply to Become a Registered Enrollment Entity for details of the information you can provide. Once you have submitted your application, you can only view your account information. You cannot make changes to any of the information yourself. If you need to make changes to any information, such as, add a subsite or change locations and hours of operation, you will need to contact the marketplace administrator. The only information you can manage is the information related to enrollment counselors in your roster.</p>

							<p>The information you can view include:</p>
							
							<ul>

								<li>Entity Information: Entity number, organization type, name, business legal name, contact information, federal and state tax identification numbers, counties served, whether payments or grants have been received.</li>
								<li>Populations Served: Languages, ethnicities, and industries you serve.</li>
								<li>Locations and Hours: Your primary site and subsite addresses and names, their hours of operation, and languages they support.</li>
								<li>Entity Contacts: Your primary contact and financial contact information.</li>
								<li>Payment Information: The bank account information you provided and your payment address.</li>
								<li>Registration Status: Your current registration status and registration history.</li>
							
							</ul>
							
							<p>To view your entity account information at the health insurance marketplace:</p>

							<ol>
								<li>Log in to the health insurance marketplace using the email and password you used while submitting your application.</li>
								<li>Click the Account tab menu.</li>
								<li>Then click the menu option corresponding to the information you want to view: Entity Information, Populations Served, Sites (Locations and Hours), Entity Contacts, Payment Information, or Registration Status. You may also access these pages using the left navigation pane, which is accessible once you click one of these options.</li>
							</ol>
						</div>
						<!--/.entityFaq3-->
								
										
						<div class="entityFaq4" id="entity4">
							<h4 class="rule" id="role-top">Manage Enrollment Counselors
					<!-- 
							<div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							
							<a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>Enrollment counselors cannot make changes to their own information. You, as the enrollment entity, have to maintain the information about counselors in your roster, such as, languages, demographics served, and address.</p>
							
							
							<div class="accordion" id="role-accordian2">
							  <div class="accordion-group">
							    <div class="accordion-heading">
							      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsOne">
							        Add an Enrollment Counselor
							      </a>
							    </div>
							    <div id="counselorsOne" class="accordion-body collapse">
							      <div class="accordion-inner">
										<p>You may add an enrollment counselor to your roster when:</p>
										
										<ul>
											<li>Submitting your application at the health insurance marketplace</li>
											<li>Registration is complete and you need to add a new enrollment counselor</li>
											
										</ul>
										
										<p>To add an enrollment counselor:</p>
										<ol>
											<li>Log in to the health insurance marketplace using the email and password you used while submitting your application. The Enrollment Counselors page opens.</li>
											<li>Click Add New Enrollment Counselor at the bottom left of the page. This opens the New Enrollment Counselor form. You may also choose to access this page in the following manner:
												<ol>
													<li>Click the Account tab menu.</li>
													<li>Click Enrollment Counselors.</li>
													<li>Click Add Enrollment Counselor.</li>
												</ol>
											</li>
											<li>Enter the name, contact information, business name, enrollment counselor certification information, enrollment counselor site information--whether primary or secondary, mailing address, and profile information of the enrollment counselor. Profile information includes languages in which the enrollment counselor can provide spoken or written support, level of education, and photo of the enrollment counselor.</li>
											<li>Click Save Enrollment Counselor.</li>
											<li>To add another enrollment counselor, click Add Enrollment Counselor and repeat steps 3 and 4.</li>
										</ol>
							      </div>
							    </div>
							  </div>
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsTwo">
								        Find an Enrollment Counselor
								      </a>
								    </div>
								    <div id="counselorsTwo" class="accordion-body collapse">
								      <div class="accordion-inner">
											<p>When you log in to the health insurance marketplace with your enrollment entity access, the web page opens showing the list of enrollment counselors in your roster. You may access the same page any time by clicking the Enrollment Counselors tab menu and then Manage Enrollment Counselors. If you want to refine your search results, you may specify your search criteria in the Refine Results section of the page. You may search by the enrollment counselor's first or last name, status (active or inactive), or based on the date that the enrollment counselor's certification renewal is due. Another way to access the list of enrollment counselors in your roster is to click the Account tab menu and then Enrollment Counselors but you cannot specify your search criteria on this page.</p>
								      </div>
								    </div>
								  </div>
								  
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsThree">
								       View and Change Enrollment Counselor Profile
								      </a>
								    </div>
								    <div id="counselorsThree" class="accordion-body collapse">
								      <div class="accordion-inner">
								       <p>Once your registration has been approved by the health insurance marketplace administrator, the enrollment counselors in your roster create their own accounts at the marketplace. The enrollment counselor's profile information includes the name, contact information, languages handled by, and educational level of the counselor that you provided when adding the counselor to your roster. Enrollment counselors cannot create or edit their own profile information. The counselor has to contact you to make changes to any information.</p>
								       
											<p>To view or change an enrollment counselor's profile information:</p>
											
												<ol>
													<li>Log in to the health insurance marketplace using the email and password you used while submitting your application. The Enrollment Counselors page opens.</li>
													<li>Find the enrollment counselor whose profile you want to view or change.</li>
													<li>Click the enrollment counselor Name link. This opens the Enrollment Counselor Information page.</li>
													<li>To view the profile information, click Enrollment Counselor Information on the left navigation pane.</li>
													<li>Scroll down to the Profile Information section of the page.</li>
													<li>To change enrollment counselor profile information, click Edit.</li>
													<li>Scroll down to the Profile Information section and make the required changes.</li>
													<li>Click Save Enrollment Counselor.</li>
												</ol>
								      </div>
								    </div>
								  </div>
								  
								 
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFour">
								        View and Change Enrollment Counselor Status
								      </a>
								    </div>
								    <div id="counselorsFour" class="accordion-body collapse">
								      <div class="accordion-inner">
								      	<h4>Enrollment Counselor Status</h4>

								        <p>When the health insurance marketplace administrator approves your application, the marketplace notifies enrollment counselors in your roster to create an account. The enrollment counselors must undergo training and a certification exam. You must upload any supporting documents related to the counselor to the health insurance marketplace. The administrator then reviews the enrollment counselor information and approves the counselor based on certification requirements. If the enrollment counselor account is approved, the administrator changes the counselor's status to Certified. If the application is denied, the administrator changes the certification status accordingly.</p>

										<p>An enrollment counselor can be certified only by the health insurance marketplace administrator. As the enrollment entity, you can only mark an enrollment counselor as Active or Inactive. You can do this at any time irrespective of whether or not the counselor has been certified by the marketplace administrator. The Active or Inactive status only indicates whether or not an enrollment counselor is actively working for you. For example, an enrollment counselor who is away on a long-term vacation can be marked as Inactive, so that individuals seeking assistance will not find and designate that particular counselor for assistance.</p>

										<p>An enrollment counselor can have any one of the following certification statuses:</p>

										<ul>
											<li>Pending: This is the default status when the enrollment counselor creates an account at the health insurance marketplace.</li>
											<li>Withdrawn-Enrollment Counselor Request: Enrollment counselor requests to withdraw application.</li>
											<li>Certified: The administrator at the health insurance marketplace certifies an enrollment counselor who has undergone training and passed the certification exam. An enrollment counselor number is generated by the system and the counselor has access to the Enrollment Counselor portal.</li>
											<li>Decertified-Misconduct: The enrollment counselor has been certified in the past, but has been removed from the health insurance marketplace due to misconduct.</li>
											<li>Decertified-Enrollment Counselor Request: The enrollment counselor requests to be decertified.</li>
											<li>Denied-Not Eligible for Training: The enrollment counselor is not eligible for training.</li>
											<li>Denied-Did Not Complete Required Training: The enrollment counselor did not complete required training.</li>
											<li>Denied-Background Check: The enrollment counselor's background check did not meet requirements.</li>
											<li>Denied-Conflict of Interest: The enrollment counselor has a conflict of interest with the health insurance marketplace.</li>
											<li>Denied-Unable to Satisfy Program Requirements: The enrollment counselor does not satisfy the requirements of the health insurance marketplace.</li>
											<li>Denied: The administrator denies the enrollment counselor account for reasons other than those specified.</li>
											<li>Entity Deregistered: The entity has been registered in the past, but has been removed from the health insurance marketplace due to compliance issues. At this time, all enrollment counselors in the entity's roster are deregistered as well.</li>
											<li>Entity Deregistered-Failed to Renew: The entity registration has expired, but the entity has not applied for registration renewal.</li>
											<li>Entity Deregistered-Misconduct: The entity has been registered in the past, but has been removed from the health insurance marketplace due to misconduct.</li>
										</ul>
										
										
										<h4>View and Change Enrollment Counselor Status</h4>
										
										<p>To indicate whether or not an enrollment counselor is actively working with your organization, you may change the status of an enrollment counselor to Active or Inactive at any time. For more information, see Enrollment Counselor Status.</p>

										<p>To view and change the status of an enrollment counselor:</p>
										
										<ol>
											<li>Log in to the health insurance marketplace using the email and password you used while submitting your application. The Enrollment Counselors page opens.</li>
											<li>Find the enrollment counselor whose status you want to view or change.</li>
											<li>Click the enrollment counselor Name link. This opens the Enrollment Counselor Information page.</li>
											<li>Click Status on the left navigation pane.</li>
											<li>To change the status, click Edit.</li>
											<li>Make the required change to the status in the New Status field.</li>
											<li>To add a comment about the status change, use the Comment text box.</li>
											<li>Click Submit.</li>
										</ol>
								      </div>
								    </div>
								  </div>
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFive">
								       View Enrollment Counselor Certification History
								      </a>
								    </div>
								    <div id="counselorsFive" class="accordion-body collapse">
								      <div class="accordion-inner">
								       <p>To indicate whether or not an enrollment counselor is actively working with your organization, you may change the status of an enrollment counselor to Active or Inactive at any time. For more information, see Enrollment Counselor Status.</p>

										<p>To view and change the status of an enrollment counselor:</p>
										
										<ol>

											<li>Log in to the health insurance marketplace using the email and password you used while submitting your application. The Enrollment Counselors page opens.</li>
											<li>Find the enrollment counselor whose status you want to view or change.</li>
											<li>Click the enrollment counselor Name link. This opens the Enrollment Counselor Information page.</li>
											<li>Click Status on the left navigation pane.</li>
											<li>To change the status, click Edit.</li>
											<li>Make the required change to the status in the New Status field.</li>
											<li>To add a comment about the status change, use the Comment text box.</li>
											<li>Click Submit.</li>
										
										</ol>
										</ol>
								      </div>
								    </div>
								  </div>
								  
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsSix">
								      View and Change Enrollment Counselor Information
								      </a>
								    </div>
								    <div id="counselorsSix" class="accordion-body collapse">
								      <div class="accordion-inner">
								        <p>For detailed and most current information about the state's health insurance marketplace enrollment assistance program, visit http://www.healthexchange.ca.gov/Pages/AssistersProgram.aspx.</p>
								      </div>
								    </div>
								  </div>
								</div>

						
						</div>
						<!--/.entityFaq4-->						
														
						<div class="entityFaq5" id="entity5">
							<h4 class="rule" id="role-top">Manage Individuals Seeking Your Assistance
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							<a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>You may view the list of individuals who have designated any of your enrollment counselors to assist with enrollment in the health plans offered by the health insurance marketplace. The designated enrollment counselor in your organization will have access to the individual's account and perform all necessary actions on behalf of the individual. You can use this list to evaluate successful enrollments and quality of service offered by your enrollment counselor.</p>

							<p>To view the list of individuals seeking assistance from your organization:</p>
							
							<ol>
								<li>Log in to the health insurance marketplace.</li>
								<li>Click the Individuals tab menu.</li>
								<li>To view the list of individuals whose request for assistance from your organization are still pending acceptance:
									<ol>
										<li>Click Pending Requests.</li>
										<li>To refine your search, use the Refine Results section. You can search by the individual's name, the name of the enrollment counselor the individual has designated, or by the date on which the request was received.</li>
									</ol>
								</li>
								<li>To view the list of individuals whose requests have been accepted by your enrollment counselors:
									<ol>
										<li>Click Active.</li>
										<li>To refine your search, use the Refine Results section. You can search by the individual's name, the name of the enrollment counselor the individual has designated, the status of the individual in the enrollment process, or by the date since the individual's request has been accepted by the enrollment counselor.</li>
									</ol>
								</li>
								<li>To view the list of individuals who have been marked as inactive by your enrollment counselors:
									<ol>
										<li>Click Inactive.</li>
										<li>To refine your search, use the Refine Results section. You can search by the individual's name, the enrollment counselor's name, the individual's enrollment status, or by the duration during which the individual had been active.</li>
									</ol>
								</li>
							</ol>
						</div>										
																		
																						
						<a id="role-top" href="#" class="pull-right">Back to Top</a>
						
						
				</div>
		</div>		
				