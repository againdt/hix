<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.maskedinput.js" />"></script>

<script type="text/javascript">
	
	$("#existingBankInfo").live('change', function(){
		if($(this).val() == ""){
	    	 $('#bankDataForm').find('input, textarea, button, select, a').attr('readonly',false);
	    	 $('#bankDataForm').find('input, textarea, button, select, a').attr('value','');
	    }else{
	    	var bankInfoData = ${bankJsonData};
			for (var key in bankInfoData) {
			  if (bankInfoData.hasOwnProperty(key)) {
				  if($(this).val() == key){
					  $("#id").val($(this).val());
					  for(var subKey in bankInfoData[key]){
						  for(var field in bankInfoData[key][subKey]){
							  var fieldname = field;
							  var fieldval = bankInfoData[key][subKey][field];
					 		  $("#"+ fieldname).val(fieldval);
					 		  $('#bankDataForm').find('input, textarea, button, select, a').attr('readonly','readonly');
			     		  };
					  };
			      };
			  };
	      };
	   };
	});
	
	
	$('#existingCreditCardInfo').live('change', function(){
		if($(this).val() == ""){
	    	 $('#creditCardDataForm').find('input, textarea, button, select, a').attr('readonly',false);
	    	 $('#creditCardDataForm').find('input, textarea, button, select, a').attr('value','');
	    }else{
	    	 var creditCardInfoData = ${creditJsonData};
			 for (var key in creditCardInfoData) {
				  if (creditCardInfoData.hasOwnProperty(key)) {
					  if($(this).val() == key){
						  $("#id").val($(this).val());
						  for(var subKey in creditCardInfoData[key]){
							  for(var field in creditCardInfoData[key][subKey]){
								  var fieldname = field;
								  var fieldval = creditCardInfoData[key][subKey][field];
						 		  $("#"+ fieldname).val(fieldval);
						 		  $('#creditCardDataForm').find('input, textarea, button, select, a').attr('readonly','readonly');
				     		  };
						 };
					};
				};
			};
	    };
	});
	
	jQuery.validator.addMethod("expirationDateCheck", function(value, element, param) {
		expirationMonth = $("#expirationMonth").val(); 
	  	expirationYear  = $("#expirationYear").val(); 
	  	
	  	if(expirationMonth == "" || expirationYear == ""){ 
	  		return false; 
	  	}
	  	return true;
	});
	
    $("input[name=paymentType]").live('click', function(){
    	var optionVal = $("input[name=paymentType]:checked").val();
    	if(optionVal == 'Bank'){
    		$('#bankDataForm').show();
    		$('#bankOption').show();
    		$('#creditCardDataForm').hide();
    		$('#creditCardOption').hide();
    	}else{
    		$('#creditCardDataForm').show();
    		$('#creditCardOption').show();
    		$('#bankDataForm').hide();
    		$('#bankOption').hide();
    	}
    });
    
    function validateForm(){
    	if( $("#frmcreatefininfo").validate().form() ) {
			 if( $("input[name=paymentType]:checked").val() == undefined ){
				 alert('Please Select Payment Type');
				 return false;
			 }else{
				 if($("input[name=paymentType]:checked").val() == 'CreditCard'){
					 validateCreditInfo();
				 }else{
					 $("#frmcreatefininfo").submit();
				 }
			 }
		 }
	}
	
    function validateCreditInfo() {
	      var validateUrl = 'validatecreditinfo';
	   	  var frm = $(document.frmcreatefininfo);
	      var fields = frm.serializeArray();
	   	   
	   	  var result = new Object();
	      jQuery.each(fields, function(i, field){
	    	   result[field.name] = field.value;
	       });
	       var dat = result;
	   	  
	       $.ajax({
			url:validateUrl, 
			type: "POST", 
			data: dat, 
			success: function(response)
			{
				if(response == 'REJECT'){
					error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please provide valid creditcard Information.</span></label>";
					$('#creditInfoAuth_error').html(error);
				 	return false;
				}else{
				 	$("#frmcreatefininfo").submit();
				}
			},
       });
  }
</script>
<div>		
	<div>
		<h2>Create Financial Information</h2>
	</div>
	<div>
		<form action="${submitUrl}" id="frmcreatefininfo" name="frmcreatefininfo"  method="POST">
			<input type="hidden" id="bankInfoId" name="bankInfo.id" value=""/>
			<input type="hidden" id="creditCardInfoId" name="creditCardInfo.id" value=""/>
			<fieldset class="form-stacked">
			 	<br></br>
			 	<h3><spring:message  code="label.yourFinancialInfo"/></h3>
			 </fieldset>
			 
			<fieldset id="personelinfo" name="personelinfo">
				<legend>Personnel Information</legend>
				<div>
					<table>
						<tr>
							<td class="input">
								<label for="firstName" class="required"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="firstName" id="firstName" value="${personObj.firstName}" size="35">	
							</td>
							<td class="input">
								<label for="LastName" class="required"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="lastName" id="lastName" value="${personObj.lastName}" size="20">
							</td>
							<td class="input">
								<label for="suffix" class="required">Suffix <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<select id="suffix"  name="suffix">
									<option value="" selected="selected">Select</option>
									<option ${personObj.suffix == 'Mr' ? 'selected="selected"' : ''}  value="Mr">Mr</option>
									<option ${personObj.suffix == 'Ms' ? 'selected="selected"' : ''}  value="Ms">Ms</option>
								</select>
							</td>
						</tr>
						<tr>
							<td id="firstName_error"></td>
							<td id="lastName_error"></td>
							<td id="suffix_error"></td>
						</tr>
						<tr>
							<td class="input">
								<c:if test="condition"></c:if>
								<c:choose>
								   <c:when test="${personObj.dob != null && personObj.dob != ''}">
								   		<c:set var="dob" value="${personObj.dob}" />
										<c:set var="dateParts" value="${fn:split(dob,'-')}"/>
										
										<c:set var="dobMM" value="${dateParts[1]}"/>
										<c:set var="dobDD" value="${dateParts[2]}"/>
										<c:set var="dobYY" value="${dateParts[0]}"/>
										
										<c:set var="dob" value="${dobMM}/${dobDD}/${dobYY}" />
								   </c:when>
								   <c:otherwise>
								   		<c:set var="dob" value="" />
								   </c:otherwise>  
								</c:choose>
								<label for="dob" class="required">Date of Birth</label>
								<input type="text" readonly="readonly" name="dob" id="dob" placeholder="mm/dd/yyyy" value="${dob}" size="35">	
							</td>
							<td class="input">
								<c:if test="condition"></c:if>
								<c:choose>
								   <c:when test="${personObj.ssn != null && personObj.ssn != ''}">
								   		<c:set var="ssn1" value="${fn:substring(personObj.ssn, 0, 3)}" />
										<c:set var="ssn2" value="${fn:substring(personObj.ssn, 3, 5)}" />
										<c:set var="ssn3" value="${fn:substring(personObj.ssn, 5, 9)}" />
										
										<c:set var="ssn" value="${ssn1}-${ssn2}-${ssn3}" />
								   </c:when>
								   <c:otherwise>
								   		<c:set var="ssn" value="" />
								   </c:otherwise>  
								</c:choose>
									<label for="ssn" class="required">Social Security No.</label>
									<input type="text" readonly="readonly" name="ssn" id="ssn" value="${ssn}" placeholder="XXX-XX-XXXX" size="35">	
							</td>
							<td class="input">
								<label for="gender" class="required">Gender</label>
								<select id="gender" readonly="readonly"  name="gender">
									<option value="" selected="selected">Select</option>
									<option  ${personObj.gender == 'Female' ? 'selected="selected"' : ''} value="Female">Female</option>
									<option  ${personObj.gender == 'Male' ? 'selected="selected"' : ''} value="Male">Male</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="input">
								<label for="address1" class="required">Home Address Line 1 <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="address1" id="address1" value="${locationObj.address1}" class="large" size="2">
													
							</td>
							<td class="input">
								<label for="address2" class="required">Home Address Line 2 <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="address2" id="address2" value="${locationObj.address2}"  size="20">
							</td>
						</tr>
						<tr>
							<td id="address1_error"></td>
							<td id="address2_error"></td>
						</tr>
						<tr>
							<td class="input">
								<label for="city" class="required">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="city" id="city" value="${locationObj.city}" size="20">
							</td>
							<td class="input">
								<label for="state" class="required">State <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<select size="1"  id="state" name="state">
								<option ${locationObj.state == '' ? 'selected="selected"' : ''} value="" selected="selected">Select</option>
								<option ${locationObj.state == 'AL' ? 'selected="selected"' : ''} value="AL">Alabama</option>
								<option ${locationObj.state == 'AK' ? 'selected="selected"' : ''} value="AK">Alaska</option>
								<option ${locationObj.state == 'AZ' ? 'selected="selected"' : ''} value="AZ">Arizona</option>
								<option ${locationObj.state == 'AR' ? 'selected="selected"' : ''} value="AR">Arkansas</option>
								<option ${locationObj.state == 'CA' ? 'selected="selected"' : ''} value="CA">California</option>
								<option ${locationObj.state == 'C0' ? 'selected="selected"' : ''} value="CO">Colorado</option>
								<option ${locationObj.state == 'CT' ? 'selected="selected"' : ''} value="CT">Connecticut</option>
								<option ${locationObj.state == 'DE' ? 'selected="selected"' : ''} value="DE">Delaware</option>
								<option ${locationObj.state == 'DC' ? 'selected="selected"' : ''} value="DC">District of Columbia</option>
								<option ${locationObj.state == 'FL' ? 'selected="selected"' : ''} value="FL">Florida</option>
								<option ${locationObj.state == 'GZ' ? 'selected="selected"' : ''} value="GA">Georgia</option>
								<option ${locationObj.state == 'HI' ? 'selected="selected"' : ''} value="HI">Hawaii</option>
								<option ${locationObj.state == 'ID' ? 'selected="selected"' : ''} value="ID">Idaho</option>
								<option ${locationObj.state == 'IL' ? 'selected="selected"' : ''} value="IL">Illinois</option>
								<option ${locationObj.state == 'IN' ? 'selected="selected"' : ''} value="IN">Indiana</option>
								<option ${locationObj.state == 'IA' ? 'selected="selected"' : ''} value="IA">Iowa</option>
								<option ${locationObj.state == 'KS' ? 'selected="selected"' : ''} value="KS">Kansas</option>
								<option ${locationObj.state == 'KY' ? 'selected="selected"' : ''} value="KY">Kentucky</option>
								<option ${locationObj.state == 'LA' ? 'selected="selected"' : ''} value="LA">Louisiana</option>
								<option ${locationObj.state == 'ME' ? 'selected="selected"' : ''} value="ME">Maine</option>
								<option ${locationObj.state == 'MD' ? 'selected="selected"' : ''} value="MD">Maryland</option>
								<option ${locationObj.state == 'MA' ? 'selected="selected"' : ''} value="MA">Massachusetts</option>
								<option ${locationObj.state == 'MI' ? 'selected="selected"' : ''} value="MI">Michigan</option>
								<option ${locationObj.state == 'MN' ? 'selected="selected"' : ''} value="MN">Minnesota</option>
								<option ${locationObj.state == 'MS' ? 'selected="selected"' : ''} value="MS">Mississippi</option>
								<option ${locationObj.state == 'MO' ? 'selected="selected"' : ''} value="MO">Missouri</option>
								<option ${locationObj.state == 'MT' ? 'selected="selected"' : ''} value="MT">Montana</option>
								<option ${locationObj.state == 'NE' ? 'selected="selected"' : ''} value="NE">Nebraska</option>
								<option ${locationObj.state == 'NV' ? 'selected="selected"' : ''} value="NV">Nevada</option>
								<option ${locationObj.state == 'NH' ? 'selected="selected"' : ''} value="NH">New Hampshire</option>
								<option ${locationObj.state == 'NJ' ? 'selected="selected"' : ''} value="NJ">New Jersey</option>
								<option ${locationObj.state == 'NM' ? 'selected="selected"' : ''} value="NM">New Mexico</option>
								<option ${locationObj.state == 'NY' ? 'selected="selected"' : ''} value="NY">New York</option>
								<option ${locationObj.state == 'NC' ? 'selected="selected"' : ''} value="NC">North Carolina</option>
								<option ${locationObj.state == 'ND' ? 'selected="selected"' : ''} value="ND">North Dakota</option>
								<option ${locationObj.state == 'ON' ? 'selected="selected"' : ''} value="OH">Ohio</option>
								<option ${locationObj.state == 'OK' ? 'selected="selected"' : ''} value="OK">Oklahoma</option>
								<option ${locationObj.state == 'OR' ? 'selected="selected"' : ''} value="OR">Oregon</option>
								<option ${locationObj.state == 'PA' ? 'selected="selected"' : ''} value="PA">Pennsylvania</option>
								<option ${locationObj.state == 'RI' ? 'selected="selected"' : ''} value="RI">Rhode Island</option>
								<option ${locationObj.state == 'SC' ? 'selected="selected"' : ''} value="SC">South Carolina</option>
								<option ${locationObj.state == 'SD' ? 'selected="selected"' : ''} value="SD">South Dakota</option>
								<option ${locationObj.state == 'TN' ? 'selected="selected"' : ''} value="TN">Tennessee</option>
								<option ${locationObj.state == 'TX' ? 'selected="selected"' : ''} value="TX">Texas</option>
								<option ${locationObj.state == 'UT' ? 'selected="selected"' : ''} value="UT">Utah</option>
								<option ${locationObj.state == 'VT' ? 'selected="selected"' : ''} value="VT">Vermont</option>
								<option ${locationObj.state == 'VA' ? 'selected="selected"' : ''} value="VA">Virginia</option>
								<option ${locationObj.state == 'WA' ? 'selected="selected"' : ''} value="WA">Washington</option>
								<option ${locationObj.state == 'WV' ? 'selected="selected"' : ''} value="WV">West Virginia</option>
								<option ${locationObj.state == 'WI' ? 'selected="selected"' : ''} value="WI">Wisconsin</option>
								<option ${locationObj.state == 'WY' ? 'selected="selected"' : ''} value="WY">Wyoming</option>
							</select>	
							</td>
							<td class="input">
								<label for="zipcode" class="required">Zip <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<input type="text"  name="zipcode" id="zipcode" value="${locationObj.zip}" size="5">
							</td>
						</tr>
						<tr>
							<td id="city_error"></td>
							<td id="state_error"></td>
							<td id="zipcode_error"></td>
						</tr>
					</table>
				</div>
			</fieldset>
				<div>
					<br>
					<p><font size="4">Total Monthly Premium for Plans Selected:</font></p>
				</div>
				<br><br><br>
				<div>
					<p>
						<span><font size="3">Select Payment Type:</font></span>&nbsp;&nbsp;
						<span><input type="radio" name="paymentType" id="paymentType" value="Bank">&nbsp;Bank Account Information</span>&nbsp;&nbsp;&nbsp;&nbsp;
						<span><input type="radio" name="paymentType" id="paymentType" value="CreditCard">&nbsp;Credit/Debit Card</span>&nbsp;&nbsp;&nbsp;&nbsp;
						<span id="bankOption" style="display:none;">
							<select name="existingBankInfo" id="existingBankInfo">
								<option value="">Select to use Bank Account on file</option>
								<c:forEach items="${banks}"  var="bankObj">
									<c:set var="accountNumber" value="${bankObj.value.accountNumber}"/>
									<c:set var="accountNumberString" value="${fn:substring(accountNumber, fn:length(accountNumber) - 4, fn:length(accountNumber))}"/>
									<option value="${bankObj.key}">Chase Account ending...${accountNumberString}</option>
								</c:forEach>
							</select>
						</span>
						<span id="creditCardOption" style="display:none;">
							<select id="existingCreditCardInfo" name="existingCreditCardInfo">
								<option value="">Select to use card on file</option>
								<c:forEach items="${creditCards}" var="creditCardObj">
								<c:set var="cardNumber" value="${creditCardObj.value.cardNumber}"/>
								<c:set var="cardNumberString" value="${fn:substring(cardNumber, fn:length(cardNumber) - 4, fn:length(cardNumber))}"/>
										<option value="${creditCardObj.key}">Card ending...${cardNumberString}</option>
								</c:forEach>
							</select>
						</span>
					</p>
				</div>
				<br>
				<fieldset id="bankDataForm" style="display:none;">
					<table>
						<tr>
							<td>Bank Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></td>
							<td>Bank Account Type <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></td>
						</tr>
						<tr>
							<td><input type="text" name="bankInfo.bankName" id="bankName" size="5"></td>
							<td>
								<select name="bankInfo.accountType" id="accountType">
									<option value="">Select</option>
									<option value="C">Checking</option>
									<option value="S">Saving</option>
									<option value="X">Corporate Checking</option>
								</select>
							</td>
						</tr>
						<tr>
							<td id="accountType_error"></td>
							<td id="bankName_error"></td>
						</tr>
						<tr>
							<td>Bank Routing Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></td>
							<td>Bank Account Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></td>
						</tr>
						<tr>
							<td><input type="text" name="bankInfo.routingNumber" id="routingNumber" size="5" maxlength="9"></td>
							<td><input type="text" name="bankInfo.accountNumber" id="accountNumber" size="5" maxlength="20"></td>
						</tr>
						<tr>
							<td id="routingNumber_error"></td>
							<td id="accountNumber_error"></td>
						</tr>
					</table>
				</fieldset>
				<fieldset style="display:none;" id="creditCardDataForm">
					<table>
						<tr>
							<td>Name on card</td>
							<td>Card Type</td>
						</tr>
						<tr>
							<td><input type="text" name="cardName" id="cardName" size="5"></td>
							<td>
								<select id="cardType" name="creditCardInfo.cardType">
									<option value="">Select</option>
									<option value="mastercard">Master Card</option>
									<option value="visa">Visa</option>
									<option value="americanexpress">American Express</option>
								</select>
							</td>
						</tr>
						<tr>
							<td id="cardName_error"></td>
							<td id="cardType_error"></td>
						</tr>
						<tr>
							<td>Card Number</td>
							<td>Expiration Date</td>
						</tr>
						<tr>
							<td><input type="text" name="creditCardInfo.cardNumber" id="cardNumber" size="5" maxlength="16"></td>
							<td>
								<select name="creditCardInfo.expirationMonth" id="expirationMonth">
									<option value="">Select</option>
									<option value="01">Jan</option>
									<option value="02">Feb</option>
									<option value="03">Mar</option>
									<option value="04">Apr</option>
									<option value="05">May</option>
									<option value="06">Jun</option>
									<option value="07">Jul</option>
									<option value="08">Aug</option>
									<option value="09">Sep</option>
									<option value="10">Oct</option>
									<option value="11">Nov</option>
									<option value="12">Dec</option>
								</select>
								
								<select size="1" id="expirationYear" name="creditCardInfo.expirationYear">
									<option value="">Select</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
								</select>
						</tr>
						<tr>
							<td id="cardNumber_error"></td>
							<td><span id="expirationMonth_error"></span><span id="expirationYear_error"></span></td>
						</tr>
						<tr id="creditInfoAuth_error"></tr>
					</table>
					
				</fieldset>	
			<br><br>
			<fieldset style="float:center;" >
				<div class="actions">
					<span><input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" value="Submit Payment and Enroll" title="Submit Payment and Enroll" class="btn primary"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span><input type="button" name="apply" id="apply" value="Apply for Payment Exemption" title="Apply for Payment Exemption" class="btn primary"/></span>
				</div>
	   		</fieldset>
		</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C) dependant on
					a particular Employer's eligibility to use the SHOP Exchange.</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var validator = $("#frmcreatefininfo").validate({ 
	rules : {
		    'bankInfo.accountType' : {required: true},
		    'bankInfo.bankName' : {required: true},
			'bankInfo.routingNumber' : {required: true, number : true, minlength: 9},	
			'bankInfo.accountNumber' : { required: true, number: true,  minlength: 20},
			'creditCardInfo.cardType' : { required : true},
			'creditCardInfo.cardNumber' : { required: true, number:  true, minlength: 16 },
			'creditCardInfo.expirationYear' : { expirationDateCheck: true },
			'cardName' : {required: true},
			firstName : { required: true },
			lastName : { required: true },
			address1 : { required: true },
			address2 : { required: true },
			city : { required: true },
			state : { required: true },
			zipcode : { required: true, minlength : 5, number : true}
	},
	messages : {
		'bankInfo.accountType': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountType' javaScriptEscape='true'/></span>"},
		'bankInfo.bankName': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBankName' javaScriptEscape='true'/></span>"},
		'bankInfo.routingNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumber' javaScriptEscape='true'/></span>",
						 number : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberFormat' javaScriptEscape='true'/></span>",
						 minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberLength' javaScriptEscape='true'/></span>"
					   },
		'bankInfo.accountNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumber' javaScriptEscape='true'/></span>",
						 number : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberFormat' javaScriptEscape='true'/></span>",
						 minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberLength' javaScriptEscape='true'/></span>"
					   },
		'creditCardInfo.cardType': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardType' javaScriptEscape='true'/></span>"},
		'creditCardInfo.cardNumber': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumber' javaScriptEscape='true'/></span>" ,
					  number : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberFormat' javaScriptEscape='true'/></span>",
					  minlength: "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberLength' javaScriptEscape='true'/></span>"
					},
		'creditCardInfo.expirationYear' : { expirationDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateExpirationDate' javaScriptEscape='true'/></span>"},
		'cardName' : { required : "<span> <em class='excl'>!</em>Please enter name on card</span>"},
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
		address1: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress1' javaScriptEscape='true'/></span>"},
		address2: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress2' javaScriptEscape='true'/></span>"},
		city: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		state: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		zipcode: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
			   number : "<span> <em class='excl'>!</em><spring:message code='label.validateZipNumber' javaScriptEscape='true'/></span>",
			   minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipLength' javaScriptEscape='true'/></span>"
			 }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});

$(function() {
	$('#ssn').mask('999-99-9999',{placeholder:""});
	$('#dob').mask("99/99/9999",{placeholder:""});
});
</script>