package com.getinsured.iex.hub.qac.wrapper;

import java.text.ParseException;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_types.FullyRestrictedSSNType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.ObjectFactory;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.SSNIdentificationType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date;

public class PersonWrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonWrapper.class);
	
	private String personBirthDate;
	private String personGivenName;
	private String personSurName;
	private String personSSN;
	private PersonType personType;
	private com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory nc2Factory = null;
	private com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory = null;
	private com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory niemCoreFactory = null;
	private com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.ObjectFactory extnFactory = null;
	private com.getinsured.iex.hub.qac.cms.hix._0_1.hix_types.ObjectFactory hixTypesFactory = null;

	public PersonWrapper() {
		extnFactory = new ObjectFactory();
		com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ObjectFactory hixFactory = new com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ObjectFactory();
		hixTypesFactory = new com.getinsured.iex.hub.qac.cms.hix._0_1.hix_types.ObjectFactory();
		nc2Factory = new com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.ObjectFactory();
		niemCoreFactory = new com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory();
		this.personType = hixFactory.createPersonType();
	}

	public void setPersonGivenName(String name) {
		PersonNameTextType givenNameTypeText = this.nc2Factory
				.createPersonNameTextType();
		givenNameTypeText.setValue(name);
		PersonNameType nameType = this.personType.getPersonName();
		if (nameType == null) {
			nameType = this.nc2Factory.createPersonNameType();
			this.personType.setPersonName(nameType);
		}
		nameType.setPersonGivenName(givenNameTypeText);
	}

	public String getPersonGivenName() {
		return this.personGivenName;
	}

	public void setPersonSurName(String surname) {
		PersonNameTextType surNameTypeText = this.nc2Factory
				.createPersonNameTextType();
		surNameTypeText.setValue(surname);
		PersonNameType nameType = this.personType.getPersonName();
		if (nameType == null) {
			nameType = this.nc2Factory.createPersonNameType();
			this.personType.setPersonName(nameType);
		}
		nameType.setPersonSurName(surNameTypeText);
	}

	public String getPersonSurName() {
		return this.personSurName;
	}

	public void setPersonBirthDate(String mmddyyyy)  throws HubServiceException {

		if (mmddyyyy != null) {
			try{
				
				Date date = proxyFactory.createDate();
				XMLGregorianCalendar xmlGregorianCalendar = PlatformServiceUtil.stringToXMLDate(mmddyyyy, "MM/dd/yyyy");
				xmlGregorianCalendar.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
				date.setValue(xmlGregorianCalendar);
				
				JAXBElement<Date> personBirthDateJAXBElement = niemCoreFactory
						.createDate(date);
				DateType personBirthDateType = niemCoreFactory.createDateType();
				personBirthDateType
						.setDateRepresentation(personBirthDateJAXBElement);
	
				personBirthDate = mmddyyyy;
	
				if (this.personType != null) {
					personType.setPersonBirthDate(personBirthDateType);
				}
			}
			catch(DatatypeConfigurationException|ParseException e){
				throw  new HubServiceException(e.getMessage(), e);
			}
		}
	}

	public String getPersonBirthDate() {
		return personBirthDate;

	}

	public void setPersonSSN(String personSSN) {
		SSNIdentificationType ssnType = this.extnFactory
				.createSSNIdentificationType();
		FullyRestrictedSSNType frSSNType = this.hixTypesFactory
				.createFullyRestrictedSSNType();
		frSSNType.setValue(personSSN);
		ssnType.setSSNIdentification(frSSNType);
		this.personSSN = personSSN;
		if (this.personType != null) {
			personType.setPersonSSNIdentification(ssnType);
		}

	}

	public String getPersonSSN() {
		return personSSN;
	}
	
	private String getDateString(DateType dateType){
		String dateStr = null;
		Date xsdDate = (Date) dateType.getDateRepresentation().getValue();
		XMLGregorianCalendar xmlDate = xsdDate.getValue();
		try {
			dateStr = PlatformServiceUtil.xmlDateToString(xmlDate, "yyyy-MM-dd");
		} catch (ParseException e) {
			LOGGER.info("Invalid date",e);
		} catch (DatatypeConfigurationException e) {
			LOGGER.info("Invalid date",e);
		}
		return dateStr;
	}
	
	public PersonWrapper(PersonType person) {
		if (person != null) {
			DateType hubBDate = person.getPersonBirthDate();
			this.personBirthDate = getDateString(hubBDate);
			this.personGivenName = person.getPersonName().getPersonGivenName()
					.getValue();
			this.personSurName = person.getPersonName().getPersonSurName()
					.getValue();
			this.personSSN = person.getPersonSSNIdentification().getSSNIdentification().getValue();
		}
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONObject name = new JSONObject();
		name.put("PersonGivenName", this.personGivenName);
		name.put("PersonSurName", this.personSurName);
		
		JSONObject bDate = new JSONObject();
		bDate.put("Date", this.personBirthDate);
		
		JSONObject ssnObj = new JSONObject();
		ssnObj.put("SSNIdentification", this.personSSN);
		
		obj.put("PersonName", name);
		obj.put("PersonBirthDate", bDate);
		obj.put("PersonSSNIdentification", ssnObj);
		return obj.toJSONString();
	}

	public PersonType getSystemRepresentation() {
		return this.personType;
	}

}
