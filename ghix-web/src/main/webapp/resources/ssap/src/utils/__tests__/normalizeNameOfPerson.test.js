

import normalizeNameOfPerson from '../normalizeNameOfPerson';

describe('Test for "normalizeNameOfPerson" function', () => {

    const testObj = {
        firstName: 'John',
        middleName: 'Fitzgerald',
        lastName: 'Kennedy',
        suffix: 'Jr.'
    };

    const testWrongObj = {
        firstName: 'John',
        lastName: 'Kennedy',
        suffix: 'Jr.'
    };

    const wrongValues = [
        12,
        [],
        null,
        true,
        undefined,
        'hello'
    ];

    it('Should throw the error for arguments qty', () => {
        expect(() => {
            normalizeNameOfPerson(testObj, 'additional argument');
        }).toThrow(`Wrong qty of arguments has been passed to function. 
                Expected qty is 1. Was passed 2. 
                Unexpected ones: ["additional argument"]`);
    });

    wrongValues.forEach(v => {
        it('Should throw an error for wrong type', () => {
            expect(() => {
                normalizeNameOfPerson(v);
            }).toThrow('Not an object has been passed to normalizeNameOfPerson function');
        });
    });

    it('Should throw an error for destructure', () => {
        expect(() => {
            normalizeNameOfPerson(testWrongObj);
        }).toThrow('There is no such prop "middleName" in passed for destructure object');
    });

    it('Should normalize person\'s name', () => {
        expect(normalizeNameOfPerson(testObj))
            .toMatch(`${testObj.firstName} ${testObj.middleName} ${testObj.lastName} ${testObj.suffix}`);
    });

});