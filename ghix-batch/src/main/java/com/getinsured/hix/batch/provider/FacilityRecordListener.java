package com.getinsured.hix.batch.provider;

import org.apache.log4j.Logger;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;

public class FacilityRecordListener {

	private static Logger logger = Logger.getLogger(FacilityRecordListener.class);
	
	public FacilityRecordListener(){
	/*	String logFile = GhixConstants.ENCLARITY_DATA_DIR+File.separatorChar+"facility_error_log.txt";
		SimpleLayout layout = new SimpleLayout();    
	    FileAppender appender = null;
		try {
			appender = new FileAppender(layout,logFile,false,true,10240);
		    
		//	appender.setBufferedIO(true);
			//appender.setBufferSize(10240);//10K
			logger = Logger.getLogger(FacilityRecordListener.class);
			logger.addAppender((Appender) appender);//not sure if FileAppender can be cast to Appender. made the chane to go past compilation
			logger.setLevel((Level) Level.ERROR);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	*/
	}
	
	@OnSkipInRead
	public void logValidationError(Throwable t){
		ProviderValidationException pe = null;
		if(t instanceof ProviderValidationException){
			pe = (ProviderValidationException)t;
		}
		if(pe != null){
			logger.error(pe.getMessage());
		}
	}
	
	@OnSkipInWrite
	public void logWriteError(Object obj, Throwable t){
		if(t != null){
			logger.error("[RECORD WRITE]"+t.getMessage());
		}
	}
	
	@OnSkipInProcess
	public void logWriteProcess(Object obj, Throwable t){
		if(t != null){
			logger.error("[RECORD WRITE]"+t.getMessage());
		}
	}
}
