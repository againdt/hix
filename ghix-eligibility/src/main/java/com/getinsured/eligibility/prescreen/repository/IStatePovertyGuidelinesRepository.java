package com.getinsured.eligibility.prescreen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.eligibility.CmsPovertyGuidelines;

@Repository
public interface IStatePovertyGuidelinesRepository extends JpaRepository<CmsPovertyGuidelines, Long> {
	
	/**
	 * Fetches all records
	 * @author Anoop Aravindan
	 * @since 09 July, 2018
	 *
	 */
	List<CmsPovertyGuidelines> findAll();
	
	@Query("SELECT max(year) FROM CmsPovertyGuidelines")	
	Long getLatestCoverageYear();
	
}
