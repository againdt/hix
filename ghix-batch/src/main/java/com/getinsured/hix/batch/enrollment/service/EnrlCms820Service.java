/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * HIX-104958
 * Batch service to load and process CMS 820 files
 * @author negi_s
 * @since 08/02/2018
 * 
 **/
public interface EnrlCms820Service {
	
	/**
	 * Loads CMS 820 files in the 820 data table
	 * @param jobService
	 * @param stepExecution
	 * @throws IOException
	 * @throws GIException
	 * @throws InterruptedException
	 */
	void loadCms820File(JobService jobService, StepExecution stepExecution) throws IOException, GIException, InterruptedException;
	
	/**
	 * Handles loading of a single 820 file. Invoked by a thread.
	 * @param filePath
	 * @param jobId
	 * @param jobService
	 * @param stepExecution
	 * @param archivePath
	 * @return
	 * @throws Exception
	 */
	String populateCms820Data(String filePath, Long jobId, JobService jobService, StepExecution stepExecution,
			File archivePath) throws Exception;
	/**
	 * Process's loaded data and builds the 820 premium table
	 * @param jobService
	 * @param stepExecution
	 * @throws Exception
	 */
	void processCms820(JobService jobService, StepExecution stepExecution) throws Exception;
	
	/**
	 * Handles the processing of a single CMS 820 file. Invoked by a thread.
	 * @param summaryId
	 * @param jobService
	 * @param stepExecution
	 * @return
	 * @throws Exception
	 */
	String processLoadedData(Integer summaryId, JobService jobService, StepExecution stepExecution) throws Exception;
	
	/**
	 * Process's a list of enrollments to form 820 premium table. Invoked by a thread.
	 * @param enrollmentIdList
	 * @param summaryId
	 * @param jobService
	 * @param stepExecution
	 * @return
	 */
	Integer processEnrollmentIdList(List<Long> enrollmentIdList, Integer summaryId, JobService jobService, StepExecution stepExecution);

}
