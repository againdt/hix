@ECHO ON
SET env=mnauto
ECHO Environment: %env%
cd.. & ^
npx cypress run --spec cypress/integration/regression/CiC_8_update_csr_level_CS4_to_CS6_Silver_med_dental.js --env configFile=%env% --reporter mochawesome & ^
npx cypress run --spec cypress/integration/regression/CiC_26_update_csr_level_CS4_to_CS2.js --env configFile=%env% --reporter mochawesome


REM npx cypress run --spec cypress/integration/regression/CiC_9_UQHP_GainingAPTC.js --env configFile=%env% --reporter mochawesome & ^
REM npx cypress run --spec cypress/integration/regression/CiC_11_APTC_Only_Gaining_CSR_silver_med_dental.js --env configFile=%env% --reporter mochawesome & ^
REM npx cypress run --spec cypress/integration/regression/CiC_18_DOB_change_AddMember.js --env configFile=%env% --reporter mochawesome & ^
REM npx cypress run --spec cypress/integration/regression/CiC_20_UQHP_To_APTC_CSR_RemoveMember.js --env configFile=%env% --reporter mochawesome & ^
REM npx cypress run --spec cypress/integration/regression/CiC_22_update_csr_level_CS4_to_CS6_RemoveMember.js --env configFile=%env% --reporter mochawesome & ^
