package com.getinsured.hix.ssap;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.SecurityUtils;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.ssap.models.DocumentCategory;
import com.getinsured.hix.ssap.models.DocumentCategoryDtl;
import com.getinsured.hix.ssap.models.MainApplication;
import com.getinsured.hix.ssap.models.MainApplicationApplicant;
import com.getinsured.hix.ssap.repository.SsapApplicantDocumentRepository;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.client.ResourceCreatorImpl;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.SsapDocumentCategory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.ssap.util.EligibilityConstants;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class VerificationStatusController extends Util {

	private static final String CASE_NUMBER = "caseNumber";

	private static final String FILE_COULD_NOT_BE_UPLOADED = "File could not be uploaded ";

	private static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	
	private static final String DOCUMENT_CATAGORY_VERIFIED_STATUS = "VERIFIED";

	private static final String DOCUMENT_CATEGORY_NOT_VERIFIED_STATUS = "NOT_VERIFIED";

  private static final String DOCUMENT_CATEGORY_NOT_VERIFIED_VARIANT_STATUS = "NOT VERIFIED";

	private static final String DOCUMENT_CATEGORY_PENDING_STATUS = "PENDING";

	private static final String SSAP_APPLICANTS = "SSAP_APPLICANTS";

	private static final String SSAP_DOCUMENTS = "SSAP_DOCUMENTS";

	private static final String DOCUMENT_VERIFICATION = "Document Verification";

	private static final String SSAP_DOCUMENT_TICKET = "SSAP Document Ticket";

	private static final String SSAP_DOCUMENT_VERIFICATION = "SSAP_DOCUMENT_VERIFICATION";

	private static Logger verificationStatusControllerLOGGER = LoggerFactory.getLogger(VerificationStatusController.class);
	
	private static final String VERIFICATION_RESULT_DASHBOARD = "verificationResultDashboard";

	private static final String MAIN_APPLICANT = "MAIN_APPLICANT";
	
	private static final int MAX_COMMENT_LENGTH = 4000;

	public static final String UNSECURE_CHARS_REGEX = "[\n\r]";
	
	@Autowired private UserService userService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private ContentManagementService ecmService;
	@Autowired private TicketMgmtUtils ticketMgmtUtils;
	@Autowired private ResourceCreatorImpl resourceCreatorImpl;
	@Autowired private SsapApplicantDocumentRepository ssapApplicantDocumentRepository;
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private CommentService commentService;
	@Autowired private ConsumerPortalUtil consumerPortalUtil;	
	@Autowired private RoleService roleService;
	@Autowired private  SsapApplicantRepository  ssapApplicantRepository;
	@Autowired private Gson platformGson;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired GhixRestTemplate ghixRestTemplate;
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired private SsapApplicationRepository  ssapApplicationRepository;
	
	@PersistenceUnit 
	private EntityManagerFactory emf;	
	private static final String USER_ACTIVE_ROLE_NAME = "userActiveRoleName".intern();
	
	@RequestMapping(value = "/ssap/verificationResultDashboard",  method = RequestMethod.GET)
	@PreAuthorize(SsapConstants.SSAP_VERIFICATIONS_EDIT)
	public String documentVerificationStatusAndUploadDashboard(@ModelAttribute(CASE_NUMBER) String caseNumberInFlash, Model model, HttpServletRequest request, HttpServletResponse response,RedirectAttributes redirectAttributes) throws InvalidUserException {	
		
		// @step 1 : Pull the Documents from ssap_documents Table, ssap_applicants
		
		// @step 2 : Covert that into a JSON and pass it to the DocumentVerificationAndStatus Dashboard page
		SsapApplicationResource ssapApplicationResource = null;
		long ssapID=0;
		boolean authorizedRepresentativeIndicator = false;
		Map<Long,SsapApplicantResource> applicantMap = null;
		MainApplication objMainApplication = new MainApplication();
		JSONObject jsonObj ;
		String ssapJSON = "";
		
		AccountUser user = userService.getLoggedInUser();
		String userActiveRoleName=null;
		
		if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(user.getDefRole().getName()) != 0){
			userActiveRoleName =user.getSwitchedRoleName();
			request.getSession().setAttribute(USER_ACTIVE_ROLE_NAME, userActiveRoleName);
		}
		
		String caseNumber = deriveCaseNumer(caseNumberInFlash, request,	redirectAttributes);
		
		if(caseNumber == null || "".equals(caseNumber)) {
			   return "redirect:/indportal";
		}
		ssapApplicationResource = getApplicationByCaseNumber(caseNumber);

		verificationStatusControllerLOGGER.debug("STEP1----------------------------------"+caseNumber);
		
		if(ssapApplicationResource != null) {
			try {
				ssapID = Util.getNumerIDAtEndOfLink(ssapApplicationResource.get_links().toString());
				applicantMap = resourceCreatorImpl.getApplicants(ssapID);
				ssapJSON = ssapApplicationResource.getApplicationData();
				jsonObj = (JSONObject)new JSONParser().parse(ssapJSON);
				if(((JSONObject)jsonObj.get(SINGLE_STREAMLINED_APPLICATION)).get("authorizedRepresentativeIndicator")!=null){
					 String value = ((JSONObject)jsonObj.get(SINGLE_STREAMLINED_APPLICATION)).get("authorizedRepresentativeIndicator").toString();
					 if(((JSONObject)((JSONObject)jsonObj.get(SINGLE_STREAMLINED_APPLICATION)).get("authorizedRepresentative")).get("signatureisProofIndicator")!=null){
					
						 String signaturevalue = ((JSONObject)((JSONObject)jsonObj.get(SINGLE_STREAMLINED_APPLICATION)).get("authorizedRepresentative")).get("signatureisProofIndicator").toString();
						 if("true".equalsIgnoreCase(value) && StringUtils.equals("false", signaturevalue)){
							 authorizedRepresentativeIndicator = true;
						 }
					 }
					 
				}
				
				updateApplicantMapWithJsonInfo(applicantMap, jsonObj);
			} catch (Exception e) {
				verificationStatusControllerLOGGER.debug("ERROR LOADING APPLICANT MAP-------> ",e);
			}
		}
		verificationStatusControllerLOGGER.debug("ssapJSON Value----------------------------------"+ssapJSON);
		
		objMainApplication.setApplicationID(ssapID);
		objMainApplication.setCaseId(caseNumber);
		objMainApplication.setAuthorizedRepresentativeIndicator(authorizedRepresentativeIndicator);
			
		if(applicantMap!=null && !applicantMap.isEmpty()){
			setVerificationDashboardResults(ssapApplicationResource, applicantMap,objMainApplication);
		}else{
			MainApplicationApplicant objMainApplicationApplicant1 = new MainApplicationApplicant();
			Map<String,DocumentCategory> documentCategoryStatusMap = getDocumentCatagoryStatusMap();
			objMainApplicationApplicant1.setDocumentCategoryStatusMap(documentCategoryStatusMap);
			objMainApplication.addMainApplicantToList(objMainApplicationApplicant1);
		}
		
		String json = platformGson.toJson(objMainApplication);
		model.addAttribute(MAIN_APPLICANT, json);
		return VERIFICATION_RESULT_DASHBOARD ;
	}

	private void updateApplicantMapWithJsonInfo(
			Map<Long, SsapApplicantResource> applicantMap, JSONObject jsonObj) {
		JSONArray listOfApplicants = (JSONArray) (JSONArray) ((JSONObject) ((JSONArray) ((JSONObject)jsonObj.get(SINGLE_STREAMLINED_APPLICATION)).get("taxHousehold")).get(0)).get("householdMember");

		for(int i=0;i<listOfApplicants.size();i++){
			
			JSONObject householdMember = (JSONObject)listOfApplicants.get(i);
			SsapApplicantResource ssapApplicantResource = (SsapApplicantResource) applicantMap.get(householdMember.get("personId"));
			ssapApplicantResource.setAmericanIndianAlaskaNativeIndicator((boolean)((JSONObject)householdMember.get("specialCircumstances")).get("americanIndianAlaskaNativeIndicator"));

			boolean citizenshipAsAttested = (boolean)((JSONObject)householdMember.get("citizenshipImmigrationStatus")).get("citizenshipAsAttestedIndicator");
			boolean citizenshipStatus = (boolean)((JSONObject)householdMember.get("citizenshipImmigrationStatus")).get("citizenshipStatusIndicator");
			ssapApplicantResource.setCitizenshipAsAttestedIndicator(citizenshipAsAttested || citizenshipStatus);
		}
	}

	private String deriveCaseNumer(String caseNumberInFlash,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
		if(request.getParameter(CASE_NUMBER)!=null && request.getParameter(CASE_NUMBER).trim().length()>0){
    		redirectAttributes.addFlashAttribute(CASE_NUMBER, request.getParameter(CASE_NUMBER).trim());
    	}
		
		String caseNumber = caseNumberInFlash;
		  if(caseNumberInFlash == null  || "".equals(caseNumberInFlash)) {
		   caseNumber = request.getAttribute(CASE_NUMBER)==null?"":request.getAttribute(CASE_NUMBER).toString();
		   
		   if(caseNumber == null || "".equals(caseNumber)) {
		    caseNumber = request.getSession().getAttribute(CASE_NUMBER)==null?"":request.getSession().getAttribute(CASE_NUMBER).toString();
		   }
		  }
		request.getSession().setAttribute("SSAP_APPLICATION_ID", caseNumber);
		request.getSession().setAttribute(CASE_NUMBER, caseNumber);
		return caseNumber;
	}

	private void setVerificationDashboardResults(
			SsapApplicationResource ssapApplicationResource,
			Map<Long, SsapApplicantResource> applicantMap,
			MainApplication objMainApplication) {
		MainApplicationApplicant objMainApplicationApplicant ;
		Iterator<Entry<Long, SsapApplicantResource>> entries = applicantMap.entrySet().iterator();
		final SingleStreamlinedApplication singleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplicationRepository.findOne(ssapApplicationResource.getApplicationId()));
		HouseholdMember primaryMember= HouseholdUtil.getPrimaryTaxHouseholdMember(singleStreamlinedApplication.getTaxHousehold());
		long primaryPersonId= primaryMember!=null && primaryMember.getPersonId() >0  ? primaryMember.getPersonId():1;
		while (entries.hasNext()) {
		  Entry<Long, SsapApplicantResource> thisEntry = (Entry) entries.next();
		  Long key = thisEntry.getKey();
		  SsapApplicantResource ssapApplicantResource = thisEntry.getValue();
		  if("Y".equalsIgnoreCase(ssapApplicantResource.getApplyingForCoverage())){
			  objMainApplicationApplicant= new MainApplicationApplicant();
			  objMainApplicationApplicant.setDocumentCategoryVerified(checkDocumentCategoryVerificationStatus(ssapApplicationResource, ssapApplicantResource,key,primaryPersonId)); 
			  Map<String,DocumentCategory> documentCategoryStatusMap = getDocumentCatagoryStatusMap(ssapApplicationResource,ssapApplicantResource,key,primaryPersonId);
			  loadUploadedDocumentDetails(ssapApplicantResource,documentCategoryStatusMap,objMainApplication);
			  objMainApplicationApplicant.setDocumentCategoryStatusMap(documentCategoryStatusMap);
			  objMainApplicationApplicant.setApplicantID(Util.getNumerIDAtEndOfLink(ssapApplicantResource.get_links().toString()));
			  objMainApplicationApplicant.setAmericanIndianAlaskaNativeIndicator(ssapApplicantResource.isAmericanIndianAlaskaNativeIndicator());
			  objMainApplicationApplicant.setCitizenshipAsAttestedIndicator(ssapApplicantResource.isCitizenshipAsAttestedIndicator());
			  objMainApplicationApplicant.setApplicantDisplayName(ssapApplicantResource.getFirstName()+" "+ssapApplicantResource.getLastName());
			  verificationStatusControllerLOGGER.debug("APPLICANT_ID : --------------"+Util.getNumerIDAtEndOfLink(ssapApplicantResource.get_links().toString()));
			  
			  objMainApplication.addMainApplicantToList(objMainApplicationApplicant);
		  }
		  else 
		  {
			  // Check and set INCOME DMI for the primary not seeking coverage.
			  if(key==primaryMember.getPersonId().longValue()) {
			  objMainApplicationApplicant= new MainApplicationApplicant();
			  objMainApplicationApplicant.setDocumentCategoryVerified(checkDocumentCategoryVerificationStatus(ssapApplicationResource, ssapApplicantResource,key,primaryPersonId)); 
			  Map<String,DocumentCategory> documentCategoryStatusMap = getDocumentCatagoryStatusMap(ssapApplicationResource,ssapApplicantResource,key,primaryPersonId);
			  loadUploadedDocumentDetails(ssapApplicantResource,documentCategoryStatusMap,objMainApplication);
			  objMainApplicationApplicant.setDocumentCategoryStatusMap(documentCategoryStatusMap);
			  objMainApplicationApplicant.setApplicantID(Util.getNumerIDAtEndOfLink(ssapApplicantResource.get_links().toString()));
			  objMainApplicationApplicant.setAmericanIndianAlaskaNativeIndicator(ssapApplicantResource.isAmericanIndianAlaskaNativeIndicator());
			  objMainApplicationApplicant.setCitizenshipAsAttestedIndicator(ssapApplicantResource.isCitizenshipAsAttestedIndicator());
			  objMainApplicationApplicant.setApplicantDisplayName(ssapApplicantResource.getFirstName()+" "+ssapApplicantResource.getLastName());
			  verificationStatusControllerLOGGER.debug("APPLICANT_ID : --------------"+Util.getNumerIDAtEndOfLink(ssapApplicantResource.get_links().toString()));
			  
			  objMainApplication.addMainApplicantToList(objMainApplicationApplicant);
			  }
			}
		 
		}
	} 

	private boolean checkDocumentCategoryVerificationStatus(
			SsapApplicationResource ssapApplicationResource,
			SsapApplicantResource ssapApplicantResource, long personId, long primaryPersoId) {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);  
		boolean isAllDocumentVerified = false;
		  if(!checkNotVerifiedStatus(ssapApplicantResource.getDeathStatus()) &&
        !checkNotVerifiedStatus(ssapApplicantResource.getSsnVerificationStatus()) &&
        !checkNotVerifiedStatus(ssapApplicantResource.getIncarcerationStatus()) &&
        !checkNotVerifiedStatus(ssapApplicantResource.getResidencyStatus())) {

        isAllDocumentVerified = true;
        if("Y".equalsIgnoreCase(ssapApplicationResource.getFinancialAssistanceFlag())){
          if(personId==primaryPersoId && checkNotVerifiedStatus(ssapApplicantResource.getIncomeVerificationStatus()) ){
            isAllDocumentVerified =  false;
          }

          if(checkNotVerifiedStatus(ssapApplicantResource.getMecVerificationStatus()) && isAllDocumentVerified){
            isAllDocumentVerified = false;
          }

          if (checkNotVerifiedStatus(ssapApplicantResource.getNonEsiMecVerificationStatus()) && isAllDocumentVerified) {
            isAllDocumentVerified = false;
          }
        }

        if(isAllDocumentVerified && ssapApplicantResource.isAmericanIndianAlaskaNativeIndicator() && checkNotVerifiedStatus(ssapApplicantResource.getNativeAmericanFlag())){

          if("NM".equalsIgnoreCase(stateCode)){
            isAllDocumentVerified= false;
          }

        }

        if(isAllDocumentVerified && ssapApplicantResource.isCitizenshipAsAttestedIndicator()){
          if(isAllDocumentVerified && checkNotVerifiedStatus(ssapApplicantResource.getCitizenshipImmigrationStatus())){
            isAllDocumentVerified= false;
          }
        } else {
          if(isAllDocumentVerified && checkNotVerifiedStatus(ssapApplicantResource.getVlpVerificationStatus())){
            isAllDocumentVerified = false;
          }
        }
      }

		  return isAllDocumentVerified;
	}

	private void loadUploadedDocumentDetails(
			SsapApplicantResource ssapApplicantResource,
			Map<String, DocumentCategory> documentCategoryStatusMap, MainApplication objMainApplication) {
		AccountUser loggedInUser = SecurityUtils.getLoggedInUser();
		List<ConsumerDocument> consumerDocuments = ssapApplicantDocumentRepository.getDocumentsForSsapApplicantId(Util.getNumerIDAtEndOfLink(ssapApplicantResource.get_links().toString()));
			
		for(ConsumerDocument objconsumerDocuments :  consumerDocuments){
			DocumentCategory objDocumentCategory = populateDocumentCategoryMap(documentCategoryStatusMap, objconsumerDocuments);

			verificationStatusControllerLOGGER.debug("Document Details::::::::"+objconsumerDocuments.getTargetId()+":::::::::::::::"+objconsumerDocuments.getDocumentCategory()+":::::::::::::::::::"+ssapApplicantResource.getPersonId());

			if(null == objconsumerDocuments.getAccepted()) {
				objconsumerDocuments.setAccepted("SUBMITTED");
			} else if(objconsumerDocuments.getAccepted().equals("Y")) {
				objconsumerDocuments.setAccepted("ACCEPTED");
			} else if(objconsumerDocuments.getAccepted().equals("N")) {
				objconsumerDocuments.setAccepted("REJECTED");
			}

			if(ssapApplicantResource!=null && ssapApplicantResource.getPersonId() == 1 && "Authorized Representative Authorization".equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				objMainApplication.addConsumerDocuments(objconsumerDocuments);
			}

			if (objDocumentCategory!=null) {
				DocumentCategoryDtl documentCategoryDtl = new DocumentCategoryDtl(objconsumerDocuments);
				TkmDocuments tkmDocuments = ssapApplicantDocumentRepository.findTkmDocumentForConsumerDocumentID(objconsumerDocuments.getEcmDocumentId());
				if (tkmDocuments != null && tkmDocuments.getTicket() != null) {
					documentCategoryDtl.setEncryptedID(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(tkmDocuments.getTicket().getId())));
					//AccountUser documentCreator = tkmDocuments.getTicket().getCreator();
          Integer documentCreator = tkmDocuments.getUserId();
          if (loggedInUser.getAllUserPermissions().contains("TKM_VIEW_TICKET")) {
            documentCategoryDtl.setTicketNumber(tkmDocuments.getTicket().getNumber());
            documentCategoryDtl.setShowDocument(true);
          } else {
            documentCategoryDtl.setShowDocument(documentCreator == loggedInUser.getId());
          }
				}

				String encryptedDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(objconsumerDocuments.getEcmDocumentId());
				documentCategoryDtl.setEncryptedDocPath(encryptedDocID);
				objDocumentCategory.setDocumentCategoryDtlToList(documentCategoryDtl);
			}
    }
	}

	private DocumentCategory populateDocumentCategoryMap(
			Map<String, DocumentCategory> documentCategoryStatusMap,
			ConsumerDocument objconsumerDocuments) {
		DocumentCategory objDocumentCategory = null;
		if(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
			  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDescription());
		  }else if(SsapDocumentCategory.CITIZENSHIP.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.CITIZENSHIP.getDescription());
		  }
		   else if(SsapDocumentCategory.DEATH.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.DEATH.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.INCARCERATION_STATUS.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.INCARCERATION_STATUS.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.INCOME.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.INCOME.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.VLP.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.VLP.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.MEC.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.MEC.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.RESIDENCY.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.RESIDENCY.getDescription());
					 
				   }
		   else if(SsapDocumentCategory.SSN.getTkmCategory().equalsIgnoreCase(objconsumerDocuments.getDocumentCategory())){
				  objDocumentCategory = documentCategoryStatusMap.get(SsapDocumentCategory.SSN.getDescription());
					 
				   }
		return objDocumentCategory;
	}
	
	private Map<String, DocumentCategory> getDocumentCatagoryStatusMap(SsapApplicationResource ssapApplicationResource,
                                                                     SsapApplicantResource ssapApplicantResource,
                                                                     long personId, long primaryPersonId) {
  
		final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		final Map<String,DocumentCategory> documentCategoryStatusMap = new HashMap<>();

		// Check for financial Indicator :
		// Check for Verification Status :
		documentCategoryStatusMap.put(
		  SsapDocumentCategory.DEATH.getDescription(),
      new DocumentCategory(SsapDocumentCategory.DEATH.getTkmCategory(),
        SsapDocumentCategory.DEATH.getDocumentTypes(),
        !checkNotVerifiedStatus(ssapApplicantResource.getDeathStatus()),
        ssapApplicantResource.getDeathStatus())
    );

    documentCategoryStatusMap.put(SsapDocumentCategory.INCARCERATION_STATUS.getDescription(),
      new DocumentCategory(SsapDocumentCategory.INCARCERATION_STATUS.getTkmCategory(),
        SsapDocumentCategory.INCARCERATION_STATUS.getDocumentTypes(),
        !checkNotVerifiedStatus(ssapApplicantResource.getIncarcerationStatus()),
        ssapApplicantResource.getIncarcerationStatus())
    );

		documentCategoryStatusMap.put(SsapDocumentCategory.SSN.getDescription(),
      new DocumentCategory(SsapDocumentCategory.SSN.getTkmCategory(),
        SsapDocumentCategory.SSN.getDocumentTypes(),
        !checkNotVerifiedStatus(ssapApplicantResource.getSsnVerificationStatus()),
        ssapApplicantResource.getSsnVerificationStatus()
      )
    );

		documentCategoryStatusMap.put(SsapDocumentCategory.RESIDENCY.getDescription(),
      new DocumentCategory(SsapDocumentCategory.RESIDENCY.getTkmCategory(),
        SsapDocumentCategory.RESIDENCY.getDocumentTypes(),
        !checkNotVerifiedStatus(ssapApplicantResource.getResidencyStatus()),
        ssapApplicantResource.getResidencyStatus())
    );

		if("Y".equalsIgnoreCase(ssapApplicationResource.getFinancialAssistanceFlag())){
		 if(personId==primaryPersonId) {
		   documentCategoryStatusMap.put(SsapDocumentCategory.INCOME.getDescription(),
          new DocumentCategory(
            SsapDocumentCategory.INCOME.getTkmCategory(),
            SsapDocumentCategory.INCOME.getDocumentTypes(),
            !checkNotVerifiedStatus(ssapApplicantResource.getIncomeVerificationStatus()),
            ssapApplicantResource.getIncomeVerificationStatus()
          )
        );
			}
			
			documentCategoryStatusMap.put(SsapDocumentCategory.MEC.getDescription(),
        new DocumentCategory(
          SsapDocumentCategory.MEC.getTkmCategory(),
          SsapDocumentCategory.MEC.getDocumentTypes(),
          !checkNotVerifiedStatus(ssapApplicantResource.getMecVerificationStatus()),
          ssapApplicantResource.getMecVerificationStatus()
        )
      );

			documentCategoryStatusMap.put(SsapDocumentCategory.NON_ESI_MEC.getDescription(),
					new DocumentCategory(SsapDocumentCategory.NON_ESI_MEC.getTkmCategory(),
							SsapDocumentCategory.NON_ESI_MEC.getDocumentTypes(),
            !checkNotVerifiedStatus(ssapApplicantResource.getNonEsiMecVerificationStatus()),
            ssapApplicantResource.getNonEsiMecVerificationStatus()
					)
			);
		}
		
		if(ssapApplicantResource.isCitizenshipAsAttestedIndicator()){
			documentCategoryStatusMap.put(
			  SsapDocumentCategory.CITIZENSHIP.getDescription(),
        new DocumentCategory(
          SsapDocumentCategory.CITIZENSHIP.getTkmCategory(),
          SsapDocumentCategory.CITIZENSHIP.getDocumentTypes(),
          !checkNotVerifiedStatus(ssapApplicantResource.getCitizenshipImmigrationStatus()),
          ssapApplicantResource.getCitizenshipImmigrationStatus()
        )
      );
		} else {
		  DocumentCategory documentCategory = new DocumentCategory(
		    SsapDocumentCategory.VLP.getTkmCategory(),
        SsapDocumentCategory.VLP.getDocumentTypes(),
        !checkNotVerifiedStatus(ssapApplicantResource.getVlpVerificationStatus()),
        ssapApplicantResource.getVlpVerificationStatus()
      );

			documentCategoryStatusMap.put(SsapDocumentCategory.VLP.getDescription(), documentCategory);
		}
		
		if(ssapApplicantResource.isAmericanIndianAlaskaNativeIndicator() && ("NM".equalsIgnoreCase(stateCode) || "NV".equalsIgnoreCase(stateCode))){
			documentCategoryStatusMap.put(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDescription(),
        new DocumentCategory(
          SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getTkmCategory(),
          SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDocumentTypes(),
          !checkNotVerifiedStatus(ssapApplicantResource.getNativeAmericanVerificationStatus()),
          ssapApplicantResource.getNativeAmericanVerificationStatus()));
		}
		
		return documentCategoryStatusMap;
	}

  /**
   * NOT_VERIFIED/NOT VERIFIED and PENDING are considered NOT_VERIFIED for verification statuses
   *
   * @param status - Verification Status
   * @return boolean true if input status matches one of the Strings that represents not verified
   */
  private boolean checkNotVerifiedStatus(String status) {
    return status != null && EligibilityConstants.NOT_VERIFIED_STATUS_LIST.stream().anyMatch(status::equalsIgnoreCase);
  }
	
	private Map<String, DocumentCategory> getDocumentCatagoryStatusMap() {
		Map<String,DocumentCategory> documentCategoryStatusMap = new HashMap<String,DocumentCategory>();
		documentCategoryStatusMap.put(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDescription(), new DocumentCategory(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getTkmCategory(),SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.DEATH.getDescription(), new DocumentCategory(SsapDocumentCategory.DEATH.getTkmCategory(),SsapDocumentCategory.DEATH.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.CITIZENSHIP.getDescription(), new DocumentCategory(SsapDocumentCategory.CITIZENSHIP.getTkmCategory(),SsapDocumentCategory.CITIZENSHIP.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.INCARCERATION_STATUS.getDescription(), new DocumentCategory(SsapDocumentCategory.INCARCERATION_STATUS.getTkmCategory(),SsapDocumentCategory.INCARCERATION_STATUS.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.SSN.getDescription(), new DocumentCategory(SsapDocumentCategory.SSN.getTkmCategory(),SsapDocumentCategory.SSN.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.INCOME.getDescription(), new DocumentCategory(SsapDocumentCategory.INCOME.getTkmCategory(),SsapDocumentCategory.INCOME.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.MEC.getDescription(),
				new DocumentCategory(SsapDocumentCategory.MEC.getTkmCategory(),SsapDocumentCategory.MEC.getDocumentTypes(),false));

		documentCategoryStatusMap.put(SsapDocumentCategory.NON_ESI_MEC.getDescription(),
				new DocumentCategory(SsapDocumentCategory.NON_ESI_MEC.getTkmCategory(), SsapDocumentCategory.NON_ESI_MEC.getDocumentTypes(), false));

		documentCategoryStatusMap.put(SsapDocumentCategory.VLP.getDescription(), new DocumentCategory(SsapDocumentCategory.VLP.getTkmCategory(),SsapDocumentCategory.VLP.getDocumentTypes(),false));
		documentCategoryStatusMap.put(SsapDocumentCategory.RESIDENCY.getDescription(), new DocumentCategory(SsapDocumentCategory.RESIDENCY.getTkmCategory(),SsapDocumentCategory.RESIDENCY.getDocumentTypes(),false));
		return documentCategoryStatusMap;
	}

	/**
	 * @param documentCategory
	 * @param case_number
	 * @param documentId
	 * @param user
	 * @param consumerDocumentId
	 */
	private void createOrUpdateSSAPApplicantDocumentTicket(String documentCategory,
			String case_number, String documentId, AccountUser user,
			Long consumerDocumentId, String applicantID) {
		//Set the Alfresco ECM document id 
		List<String> docLinks = new ArrayList<String>();
		try {
			if(consumerDocumentId!=null){
				docLinks.add(documentId);
				   // fill ticket data
				TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
				tkmTicketsRequest.setCreatedBy(user.getId()); // logged in user id
				tkmTicketsRequest.setLastUpdatedBy(user.getId());// logged in user id
				Household household = consumerPortalUtil.getHouseholdRecord(userService.getLoggedInUser().getActiveModuleId());

				String ticketSubject = SSAP_DOCUMENT_VERIFICATION;
				if(household != null && StringUtils.isNotBlank(documentCategory) &&
						StringUtils.isNotBlank(household.getFirstName()) && StringUtils.isNotBlank(household.getLastName())) {
					ticketSubject = documentCategory + " of " + household.getFirstName() + " " + household.getLastName();
				}
				tkmTicketsRequest.setSubject(ticketSubject);
				tkmTicketsRequest.setComment(SSAP_DOCUMENT_TICKET);
			    tkmTicketsRequest.setCategory(DOCUMENT_VERIFICATION); // Ticket category as 'Document Verification'
			    tkmTicketsRequest.setType(documentCategory); //Any of the type from Verify Income, Verify Lawful Presence, Verify Incarceration, Verify SSN, Verify Citizenship
			    tkmTicketsRequest.setUserRoleId(roleService.findRoleByName("INDIVIDUAL").getId());// Role id of logged in user id

			   if(household != null) {
				   tkmTicketsRequest.setRequester(household.getUser()==null?user.getId():household.getUser().getId());// logged in user id
				   tkmTicketsRequest.setModuleId(household.getId());
			   }
			   else {
				   tkmTicketsRequest.setRequester(user.getId());
			   }
			   tkmTicketsRequest.setDocumentLinks(docLinks); // logged in user id
			   tkmTicketsRequest.setModuleName(SSAP_DOCUMENTS); // newly added column to store module name.
			   
			   /*
			    *Below 3 Attributes needs to be set. Curent TicketManagement Team is not providing these data holders in  tkmTicketsRequest obj.
			    */
			   
			   tkmTicketsRequest.setCaseNumber(case_number);
			  // tkmTicketsRequest.setDocumentInfoUrl(urlString);
			  // tkmTicketsRequest.setRedirectUrl("/ssap/verificationResultDashboard?caseNumber="+case_number);
			   Map<String,String> ticketFormPropertiesMap=new HashMap<String,String>();
			   ticketFormPropertiesMap.put("docId", consumerDocumentId.toString()); // SSAP Document id required to call the rest api from ghix-iex-svc
			   ticketFormPropertiesMap.put("docInfoUrl","/ssaptkmpreview?caseNumber="+case_number);//CAP module requires the value.
         ticketFormPropertiesMap.put("applicantID", applicantID);
			   tkmTicketsRequest.setTicketFormPropertiesMap(ticketFormPropertiesMap);
			                        
			       //create a ticket
			   ticketMgmtUtils.createOrUpdateTicket(tkmTicketsRequest);  // TicketMgmtEndPoints.CREATEORUPDATETICKET
			}
		} catch(Exception e) {
			verificationStatusControllerLOGGER.error("Error creating verfication document ticket",e);
			throw new GIRuntimeException(e);
		}
	}

	
	@RequestMapping(value = "/ssap/verifyApplicantDocCategory",  method = RequestMethod.POST)
	@PreAuthorize(SsapConstants.SSAP_APPLICANT_DOC_CATEGORY_VIEW)
	public @ResponseBody boolean verifyApplicantDocCategory(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam long applicantID, @RequestParam String documentCategory, @RequestParam String overrideComments, @RequestParam (required=false) String caseNumber) throws InvalidUserException {
		
		if(!saveCommentsInDB(String.valueOf(applicantID),documentCategory,overrideComments)){
			verificationStatusControllerLOGGER.error("Error Saving Comments ");
			return false;
		}
		SsapApplicant ssapApplicant;
		try {
			ssapApplicant =ssapApplicantRepository.findOne(applicantID);
			
			if(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				  ssapApplicant.setNativeAmericanVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.CITIZENSHIP.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setCitizenshipImmigrationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.DEATH.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setDeathStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.INCARCERATION_STATUS.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setIncarcerationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.INCOME.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setIncomeVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.LEGAL_PRESENCE.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant.setVlpVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.MEC.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setMecVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.NON_ESI_MEC.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setNonEsiMecVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.RESIDENCY.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setResidencyStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.SSN.getDescription().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setSsnVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }
		 } catch (Exception e) {
			verificationStatusControllerLOGGER.error("Error fetching aPplicant Object ",e);
			return false;
		}
		
		
		 try {
			ssapApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			ssapApplicantRepository.save(ssapApplicant);
			
			if(StringUtils.isNotBlank(caseNumber)){
				try {
					AccountUser accountUser = userService.getLoggedInUser();
					String userName = accountUser == null ? "exadmin@ghix.com" : accountUser.getUsername();
					ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/updateEligibilityStatus",
							userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, caseNumber);
		         }catch(Exception e) {
		        	 verificationStatusControllerLOGGER.error("Exception while updating EligibilityStatus", e);
		         } 
			}			
			
		} catch (Exception e) {
			verificationStatusControllerLOGGER.error("Error saving aPplicant Stattus ",e);
			return false;
		}
		
	   return true;
	}
	
	@RequestMapping(value = "/ssap/verifyApplicantDocCategoryForDocId",  method = RequestMethod.POST)
	@PreAuthorize(SsapConstants.SSAP_ACCEPT_VERIFICATION_EDIT)
	public @ResponseBody boolean verifyApplicantDocCategoryForDocId(Model model, HttpServletRequest request, HttpServletResponse response,@RequestParam long docId) throws InvalidUserException {
		
		SsapApplicantResource ssapApplicant = null;
		try {
			
			ConsumerDocument consumerDocument = ssapApplicantDocumentRepository.findOne(docId);
			
			ssapApplicant = resourceCreatorImpl.getSsapApplicantById(consumerDocument.getTargetId());
			
			String documentCategory = consumerDocument.getDocumentCategory().replaceAll("\\s+", "");
			if(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				//ssapApplicant.setNatAmerVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.CITIZENSHIP.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setCitizenshipImmigrationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.DEATH.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setDeathStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.INCARCERATION_STATUS.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setIncarcerationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.INCOME.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setIncomeVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.LEGAL_PRESENCE.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant.setVlpVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.MEC.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setMecVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.RESIDENCY.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setResidencyStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }else if(SsapDocumentCategory.SSN.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant.setSsnVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
		    }
		 } catch (Exception e) {
			verificationStatusControllerLOGGER.error("Error fetching aPplicant Object ",e);
			return false;
		}
		
		
		 try {
			ssapApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			resourceCreatorImpl.updateSsapApplicant(ssapApplicant);
		} catch (Exception e) {
			verificationStatusControllerLOGGER.error("Error saving aPplicant Stattus ",e);
			return false;
		}
		
	   return true;
	}
	
	@RequestMapping(value="ssapdocumentmgmt/documents/uploadDocumentAttachmentsAjax",method = RequestMethod.POST)
	@PreAuthorize(SsapConstants.SSAP_UPLOAD_DOCUMENT_EDIT)
	@ResponseBody
	public String saveApplicantDocumentFilesUsingAjax(@RequestParam String documentCategory, @RequestParam String documentType,
			@RequestParam String case_number,
			@RequestParam String applicantionID,
			@RequestParam String applicantID,
                                                      @RequestParam("files[]") MultipartFile file, HttpServletRequest request) {

		String documentId = "";
        AccountUser user = null;
		String errorMsg = "";
		boolean isSuccess = true;

		try {
			//Get the existing list of document from the db and check if current document already existing in alfresco.
			user = userService.getLoggedInUser();
			String relativePath =  user.getId()+"/ssapdocumentmgmt/documents/attachments";
			String originalFileName = getFileName(file.getOriginalFilename()); 
			if (StringUtils.isNotEmpty(originalFileName)){
                documentId = ecmService.createContent(relativePath, getFileName(file.getOriginalFilename()), file.getBytes(), ECMConstants.Eligibility.DOC_CATEGORY, ECMConstants.Eligibility.SSAP_VERIFICATION, null);

        verificationStatusControllerLOGGER.debug("saveApplicantDocumentFilesUsingAjax::ticket does not exist create one");
        insertIntoCMRDocumentAndUpdateTKMTicket(documentCategory, documentType, case_number, applicantID, file.getOriginalFilename(), documentId, user);
			} else {
				errorMsg = FILE_COULD_NOT_BE_UPLOADED+" - File name cannot be null";
				isSuccess=false;
			}
		} catch(GIRuntimeException e) {
			//request.getSession().setAttribute(UPLOAD_FAILURE, FILE_COULD_NOT_BE_UPLOADED+e.getMessage());
			verificationStatusControllerLOGGER.error("ERROR IN SSAP APPLICANT DOCUMENT UPLOAD:",e);
			errorMsg = FILE_COULD_NOT_BE_UPLOADED+e.getMessage();
			isSuccess=false;
		} catch(Exception e) {
			verificationStatusControllerLOGGER.error("ERROR IN SSAP APPLICANT DOCUMENT UPLOAD:",e);
			//request.getSession().setAttribute(UPLOAD_FAILURE, FILE_COULD_NOT_BE_UPLOADED+e.getMessage());
			errorMsg = FILE_COULD_NOT_BE_UPLOADED+e.getMessage();
			isSuccess=false;
		}
		 
		if (!isSuccess) {
			return errorMsg;
		}
		
        List<DocumentCategoryDtl> documentCategories = new ArrayList<>();
		List<ConsumerDocument> consumerDocuments = ssapApplicantDocumentRepository.getDocumentsForSsapApplicantIdAndDocumentCategory(Long.parseLong(applicantID), documentCategory);
		
		for(ConsumerDocument objconsumerDocuments :  consumerDocuments){
			 if(null == objconsumerDocuments.getAccepted()) {
				  objconsumerDocuments.setAccepted("SUBMITTED");
				} else if(objconsumerDocuments.getAccepted().equals("Y")) {
					 objconsumerDocuments.setAccepted("ACCEPTED");
				} else if(objconsumerDocuments.getAccepted().equals("N")) {
					 objconsumerDocuments.setAccepted("REJECTED"); 
				}

            DocumentCategoryDtl documentCategoryDtl = new DocumentCategoryDtl(objconsumerDocuments);
            try {
                TkmDocuments tkmDocuments = ssapApplicantDocumentRepository.findTkmDocumentForConsumerDocumentID(objconsumerDocuments.getEcmDocumentId());
                if (tkmDocuments != null && tkmDocuments.getTicket() != null) {
                    documentCategoryDtl.setEncryptedID(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(tkmDocuments.getTicket().getId())));
                    //AccountUser documentCreator = tkmDocuments.getTicket().getCreator();
                    Integer documentCreator = tkmDocuments.getUserId();
                    if (user != null) {
                        if (user.getAllUserPermissions().contains("TKM_VIEW_TICKET")) {
                            documentCategoryDtl.setTicketNumber(tkmDocuments.getTicket().getNumber());
                            documentCategoryDtl.setShowDocument(true);
                        } else {
                            documentCategoryDtl.setShowDocument(documentCreator == user.getId());
                        }
                    } else {
                        documentCategoryDtl.setShowDocument(false);
                    }
		}
		
                String encryptedDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(objconsumerDocuments.getEcmDocumentId());
                documentCategoryDtl.setEncryptedDocPath(encryptedDocID);
            } catch (Exception ex) {
                verificationStatusControllerLOGGER.error("saveApplicantDocumentFilesUsingAjax::error updating with ticket info", ex);
            }
		 
            documentCategories.add(documentCategoryDtl);
        }
		 
        return platformGson.toJson(documentCategories);
	}

	private void insertIntoCMRDocumentAndUpdateTKMTicket(String documentCategory, String documentType, String case_number,String applicantID, String fileName,String documentId, AccountUser user) {
		String errorMsg;
		if(!documentId.isEmpty()){
			
			ConsumerDocument consumerDocument = new ConsumerDocument();
			consumerDocument.setDocumentCategory(documentCategory);
			consumerDocument.setDocumentType(documentType);
			consumerDocument.setDocumentName(fileName);
			consumerDocument.setEcmDocumentId(documentId);
			consumerDocument.setTargetName(SSAP_APPLICANTS);
			consumerDocument.setTargetId(Long.parseLong(applicantID));
			consumerDocument.setCreatedDate(new Timestamp(new TSDate().getTime()));
			consumerDocument.setCreatedBy(Long.getLong(String.valueOf(user.getId())));
			try {
				//GhixEndPoints.SsapIntegrationEndpoints.SAVE_SSAP_APPLICANT_DOC_URL
				ResponseEntity<ConsumerDocument> consumerDocumentResponseEntity = restTemplate.postForEntity(GhixEndPoints.ELIGIBILITY_URL+"ssapdocuments/insertSsapDocument", consumerDocument, ConsumerDocument.class);
				consumerDocument = consumerDocumentResponseEntity.getBody();

				if(!"Authorized Representative Authorization".equalsIgnoreCase(documentCategory)){
          createOrUpdateSSAPApplicantDocumentTicket(documentCategory, case_number, documentId, user, consumerDocument.getId(), applicantID);
				}
				
				 verificationStatusControllerLOGGER.debug("SSAP APPLICANT DOCUMENT SAVED:"+documentId);
				
			} catch (RestClientException e) {
				verificationStatusControllerLOGGER.error("ERROR IN INSERTING INTO CMR_DOCUMENTS:",e);
				errorMsg = FILE_COULD_NOT_BE_UPLOADED+e.getMessage();
				throw new GIRuntimeException(errorMsg);
			}
		}
	}
	
	
	private String getFileName(String originalFileName) {

		verificationStatusControllerLOGGER.debug("Old file name : " + originalFileName);
		if (StringUtils.isEmpty(originalFileName)) {
			return originalFileName;
		}

		int lastIndexOfDot = originalFileName.lastIndexOf(".");
		String fileName = null;
		String ext = null;
		if(lastIndexOfDot != -1){
			 fileName = originalFileName.substring(0,lastIndexOfDot);
			 ext = originalFileName.substring(lastIndexOfDot+1);
		}
		
		/*String[] file = originalFileName.split("\\.");
		verificationStatusControllerLOGGER.debug("Length : " + file.length);
		verificationStatusControllerLOGGER.debug("Name : " + file[0]);
		verificationStatusControllerLOGGER.debug("Extension : " + file[1]);*/
		String newFileName = null;
		if (fileName != null && ext != null && !fileName.isEmpty() && !ext.isEmpty()) {
			newFileName = fileName + "-"+new TSDate().getTime()+"." + ext;
		}else{
			newFileName = originalFileName;
		}

		verificationStatusControllerLOGGER.debug("New file name : " + newFileName);
		return newFileName;
	}
	
	public Object getIdFromCaseNumber(String caseNumber){
		EntityManager em = emf.createEntityManager();
		List resultSet = em.createNativeQuery("select id from ssap_applications where case_number='"+caseNumber+"'").getResultList();
		return resultSet.get(0); 
	}
	
	public boolean saveCommentsInDB(String targetId,String targetName, String commentText) {
		
		targetName = targetName.replace("/", "_").replace("-", "");
		verificationStatusControllerLOGGER.debug("Target id : " + targetId);
		verificationStatusControllerLOGGER.debug("Target name : " + targetName);
		verificationStatusControllerLOGGER.debug("Comment text : " + commentText);
			if(commentText != null) {
				commentText = commentText.trim();
			}
			
			try{
				verificationStatusControllerLOGGER.debug("TargetName.valueOf(targetName) : " +TargetName.valueOf(targetName));
				TargetName.valueOf(targetName);
			}catch (Exception e) {
				// TODO: handle exception
				verificationStatusControllerLOGGER.debug("Exception has occured",e);
				return false;
			}
			
					
			CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(targetId), TargetName.valueOf(targetName));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				commentTarget.setTargetId(new Long(targetId));
				commentTarget.setTargetName(TargetName.valueOf(targetName));
			}
			
			List<Comment> comments = null;
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			}
			else{
				comments = commentTarget.getComments();
			}
			
			
			verificationStatusControllerLOGGER.debug("Retrieving logged in user's name");
			
			AccountUser accountUser = null;
			
			try{
				accountUser = userService.getLoggedInUser();
				
				if(accountUser==null){
					throw new InvalidUserException();
				}
			}
			catch(InvalidUserException e){
				verificationStatusControllerLOGGER.error("Exception has occured",e);
				return false;
			}
			
			String commenterName = "";
			
			if(accountUser != null){
				commenterName += (accountUser.getFirstName() == null || accountUser.getFirstName().length() == 0 ) ? "" :  accountUser.getFirstName()+" ";
				commenterName += (accountUser.getLastName() == null || accountUser.getLastName().length() == 0 ) ? "" : accountUser.getLastName();
			}
			
			//Checking if user's name is blank
			if(commenterName == null || commenterName.trim().length() == 0){
				commenterName = "Anonymous User";
			}
			
			verificationStatusControllerLOGGER.debug("Commented by : " + commenterName);
			
			Comment comment = new Comment();
			//comment.setComentedBy(request.getParameter("commented_by").trim());
			
			//Setting logged in user's id
			verificationStatusControllerLOGGER.debug("Commenter's user Id : " + accountUser.getId());
			comment.setComentedBy(accountUser.getId());
			
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			//commentText = commentText.replaceAll("\\n|\\r", " ");
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			
			commentTargetService.saveCommentTarget(commentTarget);
		
			
			return true;
		}
}
