<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.update.companyProfile"/></li>
        </ul><!--page-breadcrumb ends-->
       <%--  <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
       <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
</div>

		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="companyProfile"/>
 	 	 	 	 </jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span9"><spring:message  code="pgheader.update.companyProfile"/></h4>
                    <a class="btn btn-small" href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>"><spring:message  code="label.cancel"/></a> 
					<a class="btn btn-primary btn-small" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                </div>
				<div class="gutter10">

				<form class="form-horizontal" id="frmIssuerAcctProfComInfo" name="frmIssuerAcctProfComInfo" action="<c:url value="/planmgmt/issuer/profile/update/company/save" />" method="POST" enctype="multipart/form-data">
                    <df:csrfToken/>
                    <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                    <div class="control-group">
						<label class="control-label" for="comp-legal-name"><spring:message  code="label.companyLegalName"/></label>
						<div class="controls paddingT5">
							<strong> ${issuerObj.companyLegalName} </strong>
						</div>						
						<input type="hidden" name="companyLegalName" id="companyLegalName" value="${issuerObj.companyLegalName}"  maxlength="60"/>
					</div><!-- end of control-group-->
                    <div class="control-group">
						<label class="control-label" for="companyLogoUI"><spring:message  code="label.companyLogo"/><a class="ttclass" rel="tooltip" href="#" data-original-title="JPEG or PNG file of the Issuer logo"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="file" id="companyLogoUI" name="companyLogoUI" accept=".gif,.jpeg,.jpg,.png" class="input-file" />
							<p class="blur">Recommended dimension: 160px X 60px </p>
							<div id="companyLogo_display" class="gutter10-t">
							 <c:if test="${not empty companyLogoExist}">	
							 	<a href="<c:url value="/planmgmt/issuer/downloadlogo/${issuerObj.hiosIssuerId}"/>" target="_blank" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a>
							 </c:if>	
							</div>
							<div id="companyLogoUI_error" class="" style="color: red">${ERROR}</div>
                            <input type="hidden" id="companyLogo" name="companyLogo" value=""/>
						</div>
					</div><!-- end of control-group-->

					<div class="control-group">
                    	<input type="hidden" id="stateOfDomicile" name="stateOfDomicile" value="${issuerObj.stateOfDomicile}"/>
                    	<label class="control-label"><spring:message code="label.stateOfDomicile"/></label>
                    	<div class="controls paddingT5">
                    		${issuerObj.stateOfDomicile}
                    	</div>
                    </div><!-- end of control-group-->

                    
                   <div class="header">
                        <h4><spring:message  code="pgheader.update.companyAddress"/></h4>
                    </div>           
                    <div class="gutter10-t">        
                    <div class="control-group">
						<label class="control-label" for="bank-address-1"><spring:message  code="label.addressLine1"/></label>
						<div class="controls">
							<input type="text" name="companyAddressLine1" id="companyAddressLine1" class="input-large" value="${issuerObj.companyAddressLine1}" maxlength="200"/>
                            <div id="companyAddressLine1_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-address-2"><spring:message  code="label.addressLine2"/></label>
						<div class="controls">
							<input type="text" name="companyAddressLine2" id="companyAddressLine2" class="input-large" value="${issuerObj.companyAddressLine2}" maxlength="200"/>
                            <div id="companyAddressLine2_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-city"><spring:message  code="label.city"/></label>
						<div class="controls">
							<input type="text" name="companyCity" id="companyCity" class="input-large" value="${issuerObj.companyCity}" maxlength="30"/>
                            <div id="companyCity_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    
                    <div class="control-group">
						<label class="control-label" for="bank-state"><spring:message  code="label.state"/></label>
						<div class="controls">
							<select class="input-medium" name="companyState" id="companyState">
								 <option value="" selected>Select</option>
                            	<c:forEach var="stateObj" items="${stateObj}">
						    			<option <c:if test="${stateObj.code == issuerObj.companyState}"> SELECTED </c:if> value="${stateObj.code}">${stateObj.name}</option>
								</c:forEach>
                            </select>
                            <div id="bank-state-error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="companyZip"><spring:message  code="label.zipCode"/></label>
						<div class="controls">
							<input type="text" name="companyZip" id="companyZip" class="input-large" value="${issuerObj.companyZip}" maxlength="5"/>
                            <div id="companyZip_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="issuer-website"><spring:message  code="label.issuerWebsite"/></label>
						<div class="controls">
							<input type="text" name="siteUrl" id="siteUrl" class="input-large" value="${issuerObj.siteUrl}" maxlength="200"/>
                            <div id="siteUrl_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="facing-website"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls">
							<input type="text" name="companySiteUrl" id="companySiteUrl" class="input-large" value="${issuerObj.companySiteUrl}" maxlength="200"/>
                            <div id="companySiteUrl_error" class=""></div>
						</div>
					</div><!-- end of control-group-->                   
                   	</div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>  
<script type="text/javascript">
var csrValue = $("#csrftoken").val();
function submitForm(){
	 $("#frmIssuerAcctProfComInfo").submit(); 
}
</script>
  
  <script type="text/javascript">
	$('.ttclass').tooltip();
	 
	var validator = $("#frmIssuerAcctProfComInfo").validate({ 
		rules : {
			companyLegalName : {required : false},
			companyLogoUI : {required : false},
			//addressLine1 : { required : false},
			//addressLine2 : { required : true},
			//city : { required : false},
			zipcode : { required : false, number : true, minlength : 5, maxlength : 5},
			//issuerWebsite : {required : false, url:{depends: function(element) {var enteredURL=$("#issuerWebsite").val(); var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");if (urlregex.test(enteredURL)){return (false);}else{return (true);}}}},
			companySiteUrl : {required : false, checkForValidUrl : true},
			siteUrl : {required : false, checkForValidUrl : true}			 		
		},
		messages : {
			companyLegalName : { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLegalName' javaScriptEscape='true'/></span>"},
			companyLogoUI: { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLogo' javaScriptEscape='true'/></span>"},
			//addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
			//addressLine2 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine2' javaScriptEscape='true'/></span>"},
			//city: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
			companyZip : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>",
				        number : "<span><em class='excl'>!</em><spring:message  code='err.issuervalidZip' javaScriptEscape='true'/></span>",
				        minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>",
				        maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>"
			          },
					companySiteUrl : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
						checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
					},
					siteUrl : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
						checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"
					}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
	// Update remaining characters for comments
	function updateCharCount(){	
		var disclaimerMaxLen = 2000;
		var currentLen = $.trim(document.getElementById("disclaimer").value).length;
		var charLeft = disclaimerMaxLen - currentLen;
		if(currentLen > disclaimerMaxLen) {
			$("#disclaimer").val($("#disclaimer").val().substr(0, disclaimerMaxLen));
			$('#chars_left').html('Characters left <b>' + 0 + '</b>' );
	    }
		$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
	}
	
	jQuery.validator.addMethod("checkForValidUrl", function (value, element, param) {		
	    var urlRegExp1 = new RegExp("(((https?:[/][/])((www)))[.]([-]|[a-z]|[A-Z]|[0-9]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)");
	    var urlRegExp2 = new RegExp("^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
	    if(value == '') {
			return true;
		}else{
			if((value.indexOf('.') == -1)){
				 return false;
			}
		    
		    if(value.match("^(http:\/\/www\.|https:\/\/www\.)")){
		    	if(urlRegExp1.test(value)){
		    		return (true);
			    }else{
			    	return (false);
			    }
		    }else{
		    	if(urlRegExp2.test(value)){
		    		return (true);
		    	}else{
		    		return (false);
		    	}
		    }
	    }   	    
	});

 $(function(){
	 $('#companyLogoUI').change(function(){
			var fileObject=this.files[0];
			var excludeTypes = ['image/gif', 'image/jpeg', 'image/png'];

			if (fileObject && !excludeTypes.includes(fileObject.type)) {
				alert("Invalid File type, allowed only " + excludeTypes);
				$("#companyLogoUI").val(null);
			}
		});
	});
</script>
