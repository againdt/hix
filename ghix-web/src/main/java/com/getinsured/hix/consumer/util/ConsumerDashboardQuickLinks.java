package com.getinsured.hix.consumer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.getinsured.hix.model.AccountUser;

public class ConsumerDashboardQuickLinks extends TagSupport {
	private static final long serialVersionUID = 1L;
	
    private JspWriter out;
	private HttpServletRequest request;
    private AccountUser user;
    private Map<String,String> navigation;
    private Map<String,String> linkImage;
    
    private static final String GI_CONCIERGE_LINK = "/memberportal/home#concierge";
    
	private static final String TEMPLATE = "<div class='header' id='list-2'><h4>Quick Links</h4></div>" +
										   " <ul class='nav nav-list' id='quick-links'>" +
										     " <MESSAGE> " +
										    " </ul>";
	
	public AccountUser getUser() {
		return this.user;
	}
	
	public void setUser(AccountUser user) {
		this.user = user;
	}
	
	
	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
	            this.out.println(TEMPLATE.replaceAll("<MESSAGE>", this.createNavigation()));
	        } catch (Exception ex) {
	            throw new JspException("Error in Shop employee dashboard tag", ex);
	        }
	    return SKIP_BODY;
	}
	
	private String getURL() {
    	
		
		
		return this.request.getAttribute("javax.servlet.forward.request_uri").toString();
	}
	/*
	private String createNavigation(){
    	StringBuilder navigationNodes = new StringBuilder();
    	String defaultClass = "";
    	String link = "";
    	String modalView = "";
    	
    	ArrayList<String> pages = new ArrayList<String>();  //page to be disabled for now
    	
   // 	pages.add("Report a Change");
		for (Map.Entry<String, String> entry : this.navigation.entrySet()) {
		    navigationNodes.append(" <li ");
		    link = entry.getValue();
		   	if(pages.contains( entry.getKey()) ){
		    		defaultClass = "disabled";
		    		link = "#";
	    	}else{
	    		defaultClass = "";
	    	}
		   	defaultClass = "";   	
		    navigationNodes.append( (this.getURL().equals(entry.getValue())) ? "class='active'>":"class='"+defaultClass+"'>");
		    navigationNodes.append( "<a href='"+link+"' "+ modalView +">");
		    navigationNodes.append( this.linkImage.get(entry.getKey()));
		    navigationNodes.append( entry.getKey() + "</a></li>");
		}
    	return navigationNodes.toString();
	}*/
	
	
	private String createNavigation(){
    	StringBuilder navigationNodes = new StringBuilder();
    	String defaultClass = "";
    	String link = "";
    	String modalView = "";
    	
    	ArrayList<String> pages = new ArrayList<String>();  //page to be disabled for now
    	
		for (Map.Entry<String, String> entry : this.navigation.entrySet()) {
		    navigationNodes.append(" <li ");
		    link = entry.getValue();
		   	if(pages.contains( entry.getKey()) ){
		    		defaultClass = "disabled";
		    		link = "#";
	    	}else{
	    		defaultClass = "";
	    	}
		   	
		 	modalView = (entry.getKey().equals("Report a Change") || entry.getKey().equals("Getinsured Concierge") ) ? "data-toggle='modal'" : "";
		    
		    navigationNodes.append( (this.getURL().equals(entry.getValue() )) ? "class='active'>":"class='"+defaultClass+"'>");
		    navigationNodes.append( "<a href='"+link+"' "+ modalView +">");
		    navigationNodes.append( this.linkImage.get(entry.getKey()));
		    navigationNodes.append( entry.getKey() + "</a></li>");
		}
    	return navigationNodes.toString();
	}
	
	private void setProperties(){
    	this.out     = pageContext.getOut();
    	this.request = (HttpServletRequest)pageContext.getRequest();
    	this.navigation = new LinkedHashMap<String, String>();
    	this.linkImage = new HashMap<String, String>();
    	//Disabled on 23 October 2013 - We do not have sufficient details right now    	
    	//this.navigation.put("Report a Change", this.request.getContextPath() + REPORT_LINK);
    	this.navigation.put("Getinsured Concierge", this.request.getContextPath() + GI_CONCIERGE_LINK);
        
    	//this.linkImage.put("Report a Change", "<span><i class='icon-flag'></i></span>");
    	this.linkImage.put("Getinsured Concierge", "<span><i class='icon-envelope'></i></span> ");
    	    	
	}
}

