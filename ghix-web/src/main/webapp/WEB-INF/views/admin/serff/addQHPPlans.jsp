<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en">
<head>
<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../../resources/css/ghixcustom.css"/>

<script type="text/javascript">

	/* $(document).ready(function() {
		$(":file").filestyle({'buttonText': 'Browse ...'});
	}); */
	
	function doSubmit(formName, url) {
	
		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
	
	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmUploadPlans"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
			
			var nodeValue = loForm["carrier"].value;
			if (!nodeValue || !loForm["carrierText"].value) {
				laFields[li++] = "Carrier must be required.";
			}
			
			nodeValue = loForm["stateCodes"].value;
			if (!nodeValue) {
				laFields[li++] = "State Code must be required.";
			}
			
			nodeValue = loForm["exchangeType"].value;
			if (!nodeValue) {
				laFields[li++] = "Exchange Type must be required.";
			}
			
			var nodeFileNode = loForm["fileUploadPlans"];
			if (nodeFileNode.files.length == 0) {
				laFields[li++] = "Please attach required Templates XML files.";
			}
			else if (nodeFileNode.files.length < 4 || nodeFileNode.files.length > 5) {
				laFields[li++] = "Please attach only 4/5 Template XMLs.";
			}
			else {
				
				var fileName = null;
				var fileList = "";
				
				for(var fi = 0; fi < nodeFileNode.files.length; fi++) {
					
					if(nodeFileNode.files[fi].type != "text/xml") {
						laFields[li++] = "Attachment file must have XML format.";
						break;
					}
					
					fileName = nodeFileNode.files[fi].name.toLowerCase()
					
					if(fileName.lastIndexOf("${NETWORK_SUFFIX}") > -1) {
						continue;
					}
					else if(fileName.lastIndexOf("${BENEFITS_SUFFIX}") > -1) {
						continue;
					}
					else if(fileName.lastIndexOf("${DRUG_SUFFIX}") > -1) {
						
						if (nodeFileNode.files.length == 4) {
							laFields[li++] = "One required template is missing, only Prescription Drug XML is optional.";
						}
						continue;
					}
					else if(fileName.lastIndexOf("${RATE_SUFFIX}") > -1) {
						continue;
					}
					else if(fileName.lastIndexOf("${SERVICE_AREA_SUFFIX}") > -1) {
						continue;
					}
					else {
						fileList += fileName + ", ";
					}
				}
				
				if(fileList) {
					laFields[li++] = "Attached files name ("+ fileList.substring(0, fileList.length - 2) +")  should be match with templates name.";
				}
			}
			
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">

				<div class="row-fluid">
					<h1 id="">Upload Plan Files</h1>
				</div><!-- end of .row-fluid -->
	
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>About Plan Files</span></div>
						<ul class="unstyled">
							<li><b>Step 1: </b>Please select which carrier you are attemptingto load plan data for.</li>
							<li><b>Step 2: </b>Please select the state you are attempting to load plan data for.</li>
							<!-- <li><b>Step 3: </b>By default, channel should be set to None.</li> -->
							<li><b>Step 3: </b>Please upload the required XML files. Also, please note Prescription Drug XML is optional and Admin(Issuer) XML is not required to upload.</li>
							<li class="gutter10"><b>Click Save:</b>Proceed to clicking <b>Save</b> after all required XML files have been successfully uploaded.</li>
						</ul>	
					</div><!-- end of .span3 -->
					
					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="${pageContext.request.contextPath}/admin/serff/uploadPlans" modelAttribute="uploadPlans" enctype="multipart/form-data" method="POST">
							
				            <div class="control-group">
				            	<c:if test="${ftpStatus != null}">
				            		<div style="color: red; font-weight: bold;">${ftpStatus}<br /><br /></div>
				            	</c:if>
				            	<c:if test="${ftpSucess != null}">
				            		<div style="color: green; font-weight: bold;">${ftpSucess}<br /><br /></div>
				            	</c:if>
				            	
				                <label for="carrier" class="control-label"><b>Step 1:</b> Carrier*&nbsp;&nbsp;</label>
								<select id="carrier" name="carrier" onchange="document.getElementById('carrierText').value=this.options[this.selectedIndex].text">
									<option value="" selected="selected">Select carrier</option>
									<c:if test="${issuerList != null}">
										<c:forEach var="issuer" items="${issuerList}">
											<c:if test="${issuer.hiosIssuerId != null}"> 
												<option value="${issuer.hiosIssuerId}">[${issuer.hiosIssuerId}] ${issuer.shortName}</option>
											</c:if> 
										</c:forEach>
									</c:if>
								</select>
								<input type="hidden" id="carrierText" name="carrierText" />
							</div>

							<div class="control-group">
				                <label for="stateCodes" class="control-label"><b>Step 2:</b> State*&nbsp;&nbsp;</label>
								<select id="stateCodes" name="stateCodes" onchange="document.getElementById('stateCodeText').value=this.options[this.selectedIndex].text">
									<option value="" selected="selected">Select state</option>
									<c:if test="${statesList != null}">
										<c:forEach var="stateName" items="${statesList}">
											<option value="<c:out value="${stateName.code}" />">
												<c:out value="${stateName.name}" />
											</option>
										</c:forEach>
									</c:if>
								</select>
								<input type="hidden" id="stateCodeText" name="stateCodeText" />
							</div>
							
							<div class="control-group">
				                <label for="exchangeType" class="control-label"><b>Step 3:</b> Exchange Type*&nbsp;&nbsp;</label>
								<select id="exchangeType" name="exchangeType" onchange="document.getElementById('exchangeTypeText').value=this.options[this.selectedIndex].text">
									<option value="" selected="selected">Select Exchange Type</option>
									<c:if test="${exchangeTypeList != null}">
									<c:forEach items="${exchangeTypeList}" var="exchangeType">
    									<option value="<c:out value="${exchangeType}" />">
												<c:out value="${exchangeType}" />
										</option>
  									</c:forEach>
                                    </c:if>
								</select>
								<input type="hidden" id="exchangeTypeText" name="exchangeTypeText" />
							</div>
							
							
							<div class="control-group">
				                <label for="tenant" class="control-label"><b>Step 4:</b> Tenant*&nbsp;&nbsp;</label>
								<select id="tenant" name="tenant" onchange="document.getElementById('tenantText').value=this.options[this.selectedIndex].text">
									<option value="" selected="selected">Select Tenant</option>
									<c:if test="${tenantList != null}">
									<c:forEach items="${tenantList}" var="tenant">									
									    <c:if test="${tenant.code == defaultTenantCode}">
    										<option selected="selected" value="<c:out value="${tenant.code}" />">
												<c:out value="${tenant.name}" />
											</option>
										</c:if>
										<c:if test="${tenant.code != defaultTenantCode}">
    										<option value="<c:out value="${tenant.code}" />">
												<c:out value="${tenant.name}" />
											</option>
										</c:if>
  									</c:forEach>
                                    </c:if>
								</select>
								<input type="hidden" id="tenantText" name="tenantText" />
							</div>

							<!-- <div class="control-group">
				                <label for="channel" class="control-label"><b>Step 3:</b> Channel&nbsp;&nbsp;</label>
								<select name="channel" id="channel"  >
									<option value="" selected="selected">None</option>
									<option value="UBA">UBA</option>
								</select>
							</div> -->
							
							<div class="control-group">
								<label class="control-label" for="filePlanAndBenefits"><b>Step 5:</b> Templates XML* </label>
								<div class="controls">
									<input type="file" class="filestyle" multiple="multiple" id="fileUploadPlans" name="fileUploadPlans" data-classButton="btn">
								</div>
							</div>
				            
							<!-- <div class="control-group">
								<label class="control-label" for="filePlanAndBenefits"><b>Step 3:</b> Plan and Benefits XML* </label>
								<div class="controls">
									<input type="file" class="filestyle" id="filePlanAndBenefits" name="filePlanAndBenefits" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileRate">Rate XML* </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileRate" name="fileRate" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileServiceArea">Service Area XML* </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileServiceArea" name="fileServiceArea" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="filePrescriptionDrug">Prescription Drug XML </label>
								<div class="controls">
									<input type="file" class="filestyle" id="filePrescriptionDrug" name="filePrescriptionDrug" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileNetwork">Network XML </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileNetwork" name="fileNetwork" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileEssentialCommunityProviders">Essential Community Providers XML </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileEssentialCommunityProviders" name="fileEssentialCommunityProviders" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileRatingRules">Rating Rules XML </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileRatingRules" name="fileRatingRules" data-classButton="btn">
								</div>
							</div> -->
							
				            <div class="gutter10">
				            	 <a href="javascript:document.forms['frmUploadPlans'].reset()" class="btn offset2" id="cancel">Cancel</a>
				            	 <a href="javascript:doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/serff/uploadPlans');" class="btn btn-primary offset1" type="submit" id="save">Save</a>
				            </div>
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>