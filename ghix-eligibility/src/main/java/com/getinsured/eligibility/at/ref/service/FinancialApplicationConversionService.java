package com.getinsured.eligibility.at.ref.service;

import java.util.Map;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface FinancialApplicationConversionService {
	void convertFinancialAppToNonFinancial(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap);

	void convertFinancialAppToNonFinancial(SsapApplication currentApplication, SsapApplication enrolledApplication);
}
