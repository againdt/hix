import moment from 'moment-timezone'
import jsonQuery from 'json-query'

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
})

Cypress.Commands.add('verifyDashboard',  () => {
    /**
     * Works in OE as of 08/28/2019
     */
    //TODO: refactor for easy read and maintenance

    cy.server()
    cy.route('GET', cy.MN.URL + '/indportal/getDashboardModels?userId=1').as('ssapInfo')

    cy.navigateTo(cy.MN.URL + '/indportal')

    cy.wait('@ssapInfo')

    cy.get('@ssapInfo').then(function (xhr) {
        expect(xhr.status).to.eq(200)
        expect(xhr.method).to.eq('GET')
        cy.appHousehold = xhr.responseBody
        cy.log('XHR Response body: ' + JSON.stringify(xhr.responseBody))
//        cy.writeFile('xhr/XHR_' + cy.h.applicationId + '.json' , JSON.stringify(xhr.responseBody))

        cy.log("App hh: " + cy.appHousehold)

        cy.fixture('mp/dashboard.json')
            .then (dashboard => {
                cy.dashboardStatus = jsonQuery(`oep[status=${cy.h.appStatus}]`, {data: dashboard}).value

                //verify tab shows current coverage year
                cy.get('body').find('[data-tabgroup="first-tab-group"]')
                cy.contains('[data-tabgroup="first-tab-group"]', cy.current_coverage_year)

                //verify Notice Text
                let sDate =  moment(new Date(cy.h.people[0].activityDate)).tz("America/Los_Angeles").startOf('day')
                let oeEndDate = moment(new Date (cy.current_oe_end_date)).tz("America/Los_Angeles").startOf('day')
                let diff = oeEndDate.diff(sDate)
                let dateDiff = Math.ceil(moment.duration(diff).asDays()) + 1

                cy.log("HH status - " + cy.dashboardStatus.status)
                cy.log("OEP notice should exist - " + cy.dashboardStatus.oep_notice)

                if (cy.dashboardStatus.oep_notice === true && !cy.dashboardStatus.isSepOpen) {
                    cy.log("Time sDate  -- " + sDate)
                    cy.log("Time oeEndDate  -- " + oeEndDate)
                    cy.log("Time till OEP ends  -- " + moment.duration(diff).asDays())

                    let noticeText = 'You have ' + dateDiff + ' days left to pick a plan'

                    cy.get('.oep-notice > .ds-text > span').should('contain', noticeText)
                }
                else if (cy.dashboardStatus.oep_notice === true && cy.dashboardStatus.isSepOpen){
                    let noticeText = 'You have 60 days remaining in your special enrollment period. Please promptly follow the next steps below.'
                    cy.get('.oep-notice > .ds-text > span').should('contain', noticeText)
                }
                else{
                    cy.get('.oep-notice > .ds-text > span').should('not.exist')
                }

                //verify Further Action
                cy.contains('#further-action-txt > span', `${cy.dashboardStatus.further_action_txt}`)
                cy.contains('#further-action-btn > span', `${cy.dashboardStatus.further_action_btn}`)

                //verify Welcome note
                cy.get('#container-wrap > .container > #individual-portal > .row-fluid > #skip')
                    .contains("Welcome,")
                    .contains(cy.h.people[0].firstName)
                    .contains(cy.h.people[0].lastName)

                //create name array to verify
                let name = []
                for (let i = 0; i < cy.h.people.length; i++){
                    if (cy.h.people[i].middleName) {
                        name.push(cy.h.people[i].firstName + " " + cy.h.people[i].middleName + " " + cy.h.people[i].lastName)
                    }
                    else{
                        name.push(cy.h.people[i].firstName + " " + cy.h.people[i].lastName)
                    }
                }

                //verify Household members show up properly
                cy.get('div:nth-child(1) > span.ds-u-text-transform--capitalize')
                    .each((el, index, array)=> {
                        cy.log("el: " + el)
                        cy.log("index: " + index)
                        cy.log("array: " + array[index].innerText)

                        let dbNameArr = array[index].innerText.trim().split(' ')

                        let jqExchInd = `people[firstName=${dbNameArr[0]}].exchangeEligibilityIndicator`
                        let jqAPTCInd = `people[firstName=${dbNameArr[0]}].aptcEligibilityIndicator`

                        let eligibility = {}

                        //get exchange and aptc info for each person
                        eligibility.exchange = jsonQuery(jqExchInd, {data: cy.h}).value
                        cy.log("Exchange Elig ind: " + eligibility.exchange)

                        eligibility.aptc = jsonQuery(jqAPTCInd, {data: cy.h}).value
                        cy.log("APTC Elig ind: " + eligibility.aptc)

                        if (eligibility.exchange === false) {
                            cy.get(`#household_elig_member_${index} > span`).should('contain', 'Not eligible')
                        }
                        if (eligibility.aptc === false && eligibility.exchange && cy.h.aptcEligibility.length > 1) {
                            cy.get(`#household_elig_member_${index} > span`).should('contain', 'Not eligible for APTC')
                        }

                        cy.get(array[index]).invoke("text").then(text =>{
                            expect(name).to.include(text.trim())
                            cy.log(text)
                        })
                    })

                // if (cy.h.aptcEligibility.length == 1 &&  cy.h.aptcEligibility[0] == false) {
                //     cy.get('#household_elig_status_0 > span').should('contain', 'You are not eligible for advanced premium tax credits or cost-sharing reductions')
                //     cy.get('input[id="aptc"]').should('have.value', "N/A")
                // }
                // else{
                //     cy.get('#household_elig_status_1').should('contain', formatter.format(cy.h.people[0].aptcMaximumAmount))
                //     cy.get('input[id="aptc"]').should('have.value', formatter.format(cy.h.people[0].aptcMaximumAmount))
                // }

                //TODO: add application and enrollment checks
                let coverageYear = String(cy.h.year)
                cy.get('input[id="firstName"]').should('have.value', cy.h.people[0].firstName)
                cy.get('input[id="lastName"]').should('have.value', cy.h.people[0].lastName)
                cy.get('input[id="coverageYear"]').should('have.value', coverageYear)


                // CSR eligibility check temporarily commented out till find out rules which value should show up
                // when different for different people

                // if (cy.h.people[0].csrEligibilityIndicator) {
                //     let csrCheck = jsonQuery(`[**][v=${cy.h.people[0].csrCategoryAlphaCode}].k`, {data: cy.csrLevel}).value
                //
                //     cy.get('input[id="csr"]').should('have.value', csrCheck)
                //     cy.log("csr check ------  " + csrCheck )
                // }
                // else {
                //     cy.get('input[id="csr"]').should('have.value', "N/A")
                //     cy.log("csr check ------  N/A" )
                // }

                //TODO: handle enrollments
                //Verify Medical enrollment object
                if (cy.dashboardStatus.Health_text) {
                    cy.get('#Health_text').should('contain', cy.dashboardStatus.Health_text)
                }

                //Verify Dental enrollment object
                if (cy.dashboardStatus.Dental_text) {
                    cy.get('#Dental_text').should('contain', cy.dashboardStatus.Dental_text)
                }
            })
    })
})


Cypress.Commands.add('verifyTobaccoPage',  () => {
    /**
     * Works in OE as of 08/28/2019
     */
    cy.navigateTo(cy.MN.URL + '/indportal#/additionalinfo')

    let nameAge = []
    let currentDate = moment(new Date (cy.h.people[0].exchangeEligibilityStartDate)).endOf('month')
    for (let i = 0; i < cy.h.people.length; i++){

        let dob =  moment(new Date(cy.h.people[i].dob)).startOf('month')


        let diff = currentDate.diff(dob)
        let age = Math.floor(moment.duration(diff).asYears())

        let nm = {
            fName: cy.h.people[i].firstName,
            lName: cy.h.people[i].lastName,
            age: age
        }
        nameAge.push(nm)
    }

    for (let i = 0; i < nameAge.length; i++){
        cy.log("Name age json:  " + JSON.stringify(nameAge[i]))
        cy.log("age:  " + nameAge[i].age)
        console.log("Name age json:  " + JSON.stringify(nameAge[i]))

        cy.get(`:nth-child(${i+2}) > .ng-binding`)

        if (nameAge[i].age < 19) {
            cy.get(`:nth-child(${i + 2}) > td.exemptCell > :nth-child(1) > .no-checkedselector > .f > [for="tobaccoUser-no"] > #tobaccoUser-no`).should('be.disabled')
            cy.get(`:nth-child(${i + 2}) > td.exemptCell > :nth-child(1) > .no-checkedselector > .f > [for="tobaccoUser-yes"] > #tobaccoUser-yes`).should('be.disabled')
        }
        else {
            cy.get(`:nth-child(${i + 2}) > td.exemptCell > :nth-child(1) > .no-checkedselector > .f > [for="tobaccoUser-no"] > #tobaccoUser-no`).should('not.be.disabled')
            cy.get(`:nth-child(${i + 2}) > td.exemptCell > :nth-child(1) > .no-checkedselector > .f > [for="tobaccoUser-yes"] > #tobaccoUser-yes`).should('not.be.disabled')
        }

    }
})

Cypress.Commands.add("setMedicalEnrollmentOptions", (options) =>{
    if (options.plantype_filter_checkbox_EPO){
        cy.get('#plantype_filter_checkbox_EPO').check().should('be.checked')
    }
    if (options.plantype_filter_csr){
        cy.get('#plantype_filter_csr').check().should('be.checked')
    }
    if (options.plantype_filter_hsa){
        cy.get('#plantype_filter_hsa').check().should('be.checked')
    }
    if (options.filter_platinum){
        cy.get('#filter_platinum').check().should('be.checked')
    }
    if (options.filter_gold){
        cy.get('#filter_gold').check().should('be.checked')
    }
    if (options.filter_silver){
      cy.get('#filter_silver').check().should('be.checked')
    }
    if (options.filter_bronze){
        cy.get('#filter_bronze').check().should('be.checked')
    }
    if (options.deductible_filter_2500){
        cy.get('#deductible_filter_2500').check().should('be.checked')
    }
    if (options.deductible_filter_5000){
        cy.get('#deductible_filter_5000').check().should('be.checked')
    }
    if (options.deductible_filter_7500){
        cy.get('#deductible_filter_7500').check().should('be.checked')
    }
    if (options.deductible_filter_15000){
        cy.get('#deductible_filter_15000').check().should('be.checked')
    }
})
