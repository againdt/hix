var app = angular.module('cmserrorreportapp', ['ui.grid','ui.grid.exporter', 'ui.bootstrap','ui.router', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('CMSErrorReportCtrl', ['$scope', '$filter', '$timeout', 'CMSErrorReportService', 'uiGridConstants','uiGridExporterConstants','uiGridExporterService', function ($scope,$filter,$timeout,CMSErrorReportService, uiGridConstants,uiGridExporterConstants,uiGridExporterService) {
	var paginationOptions = {
			pageNumber: 1,
			pageSize: 20,
			sort: null
	};	

	$scope.colDef = [];
	$scope.data = null;
	$scope.totalElem = null;
    $scope.externalConfig = {};
    $scope.dropDownColumns = [];
	
	var monthandYearFilter = function(start,end) {
		var selectOptions = [];
		for (var i=start; i<=end; i++) {
			selectOptions.push({ value: i, label : i});
		}
		return selectOptions;
	};
	
	var fileStatus = ['Accepted', 'Accepted with warnings','Accepted with errors','Rejected', 'Approved', 'Approved with warnings', 'Approved with errors', 'Disapproved'];
	var nextAction = ['SENT', 'SUCCESS' , 'HOLD', 'REGEN' ,'INACTIVE'];
	var errorCodes = ['ER-001','ER-003','ER-004','ER-006','ER-008','ER-009','ER-010','ER-011',
	                  'ER-012','ER-013','ER-014','ER-016','ER-017','ER-018','ER-019','ER-020',
	                  'ER-021','ER-022','ER-023','ER-024','ER-025','ER-026','ER-027','ER-028',
	                  'ER-029','ER-030','ER-031','ER-032','ER-033','ER-034','ER-035','ER-036',
	                  'ER-037','ER-038','ER-039','ER-040','ER-041','ER-042','ER-043','ER-044',
	                  'ER-045','ER-046','ER-047','ER-048','ER-049','ER-050','ER-051','ER-052',
	                  'ER-053','ER-054','ER-055','ER-06','ER-057','ER-058','ER-059','ER-060',
	                  'ER-061','ER-062','ER-063','ER-064','ER-997','ER-998','ER-999','WR-001',
	                  'WR-002','WR-003','WR-004','WR-005','WR-006','WR-007','WR-008','WR-009','WR-010'];
	
	var selectOptions = function(data) {
		var selectOption = [];
		for (var i=0; i<data.length; i++) {
			selectOption.push({ value: data[i], label : data[i]});
		}
		return selectOption;
	};
	
	var displayName = {cmsErrorCode : 'CMS Error Code', hoisIssuerId : 'HOIS Issuer ID', subscriberID : 'Subscriber ID', cmsErrorDetail : 'CMS Error Detail'};
	
	var setPropertiesFn = function (elem, key) {
		var filter = { type: uiGridConstants.filter.SELECT, condition : uiGridConstants.filter.EXACT };
		
        switch (key) {
            case 'reportMonth':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(1, 12);
            	$scope.dropDownColumns.push(key);
            	break;
            case 'reportYear':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(2016, new Date().getFullYear());
            	$scope.dropDownColumns.push(key);
                break;
            case 'fileStatus':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(fileStatus);
            	elem.enableSorting = false;
            	$scope.dropDownColumns.push(key);
                break;
            case 'nextAction':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(nextAction);
            	elem.enableSorting = false;
            	$scope.dropDownColumns.push(key);
                break;
            case 'cmsErrorCode':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(errorCodes);
            	$scope.dropDownColumns.push(key);
            case 'hoisIssuerId':
            case 'subscriberID':
            case 'cmsErrorDetail':
            	elem.displayName = displayName[key];
            default:
            	if(key != 'cmsErrorCode' && key != 'subscriberID')
            		elem.enableFiltering = false;
            	if(key != 'hoisIssuerId' && key != 'cmsErrorCode')
            		elem.enableSorting = false;

        }
    };
        
	CMSErrorReportService.getReport(paginationOptions.pageNumber,
			paginationOptions.pageSize, null).success(function(data){
				$scope.data = $scope.gridOptions.data = data.content;				
				if(data.size != 0) {
					var exchangeAssignedPolicyID = null;
					
					angular.forEach(data.content[0], function(value, key) {
						if(key != 'creationTime') {		
	
							var elem = {name:key, width: 150};
							
							if(key == 'exchangeAssignedPolicyID') {
								exchangeAssignedPolicyID = elem;
							} else {
								setPropertiesFn(elem,key);
								$scope.colDef.push(elem);
							}
						}
					});
					
					//Add exchangeAssignedPolicyID after hoisIssuerId column
					var foundItem = $filter('filter')($scope.colDef, { name: 'hoisIssuerId'  }, true)[0];
					$scope.colDef.splice($scope.colDef.indexOf(foundItem) + 1, 0, exchangeAssignedPolicyID);
					
					// Add Creation Time column as last
					$scope.colDef.push({ name: 'creationTime', displayName: 'Creation time', enableSorting: true, enableFiltering: false, cellFilter: 'date:\'yyyy-MM-dd HH:mm\'', width: 200, 
						sort: {
						    direction: uiGridConstants.DESC
						   }
					});
					
				}
				$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;   				         
			});

	$scope.gridOptions = {
			paginationPageSizes: [5, 10, 20],
			paginationPageSize: paginationOptions.pageSize,
			useExternalPagination: true,
			enableGridMenu: true, 
			enableFiltering: true,
			columnDefs:  $scope.colDef,
			exporterMenuPdf: false,
			exporterMenuCsv: false,
			useExternalFiltering: true,
			useExternalSorting: true,
			exporterPdfFilename: 'download.pdf',
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
			onRegisterApi: function(gridApi) {
				$scope.gridApi = gridApi;
				$scope.gridApi.core.on.filterChanged( $scope, function() {
			          var grid = this.grid;
			          var filterExact = {}, filterLike = {};
			          angular.forEach(grid.columns, function (column, index) {
				          if (column.filters && column.filters[0].term) {
				        	  if($scope.dropDownColumns.indexOf(column.field) !== -1)
				        		  filterExact[column.field] = column.filters[0].term;
				        	  else
				        		  filterLike[column.field] = column.filters[0].term;
				          }
		            });
			          $scope.externalConfig.filterExact = filterExact;
			          $scope.externalConfig.filterLike = filterLike;
			          CMSErrorReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
							$scope.data = $scope.gridOptions.data = data.content;
							$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
						});
				 });
				
				$scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){

					var grid = this.grid;
			          var asc = [], desc = [];
			          angular.forEach(grid.columns, function (column, index) {
			        	  console.log(column);
				          if (column.sort && column.sort.direction) {
				        	  if(column.sort.direction == uiGridConstants.ASC)
				        		  asc.push(column.field);
				        	  else if(column.sort.direction == uiGridConstants.DESC)
				        		  desc.push(column.field);
				          }
				      });
			          $scope.externalConfig[uiGridConstants.ASC] = asc;
			          $scope.externalConfig[uiGridConstants.DESC] = desc;
			          CMSErrorReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
							$scope.data = $scope.gridOptions.data = data.content;
							$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
						});
			    });
				
				gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
					paginationOptions.pageNumber = newPage;
					paginationOptions.pageSize = pageSize;
					CMSErrorReportService.getReport(newPage,pageSize,$scope.externalConfig).success(function(data){
						$scope.data = $scope.gridOptions.data = data.content;
						$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
					});
				});
			}
	};
	$scope.gridOptions.gridMenuCustomItems= [
	                                         {
	                                        	 title:'Export all data as CSV',
	                                             action: function ($event) {
	                                            	 CMSErrorReportService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
	                                                     $scope.gridOptions.totalItems = data.totalElements;
	                                                     $scope.gridOptions.data = data.content;
	                                                     $timeout(function()
	                                                             {
	                                                                 var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
	                                                                 $scope.gridApi.exporter.csvExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL, myElement );
	                                                                 $scope.gridOptions.data =  $scope.data;
	        	                                                     $scope.gridOptions.totalItems = $scope.totalElem;
	                                                             }, 1000);
	                                                     

	                                                     });
	                                             },
	                                             order:0
	                                           },
	                                           {
		                                        	 title:'Export all data as PDF',
		                                             action: function ($event) {
		                                            	 CMSErrorReportService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
		                                                     $scope.gridOptions.totalItems = data.totalElements;
		                                                     $scope.gridOptions.data = data.content;
		                                                     $timeout(function()
		                                                             {
		                                                                 $scope.gridApi.exporter.pdfExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL );
		                                                                 $scope.gridOptions.data =  $scope.data;
		        	                                                     $scope.gridOptions.totalItems = $scope.totalElem;
		                                                             }, 1000);
		                                                     

		                                            	});
		                                             },
		                                             order:0
	                                           }
	                                          ];

}]);

app.service('CMSErrorReportService',['$http', function ($http) {
	
	function getReport(pageNumber,size,requestdata) {

		pageNumber = pageNumber > 0?pageNumber - 1:0;
		return  $http({
			method: 'POST',
			url: '/hix/admin/reports/getcmserrorreport?page='+pageNumber+'&size='+size,
			dataType: "json",
			headers: {
				   'Content-Type': "application/json",
				   "csrftoken": $('#tokid').val()
			},
			data: JSON.stringify(requestdata)
		});
	}

	return {
		getReport:getReport
	};

}]);
