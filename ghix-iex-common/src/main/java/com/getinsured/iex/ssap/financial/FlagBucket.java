package com.getinsured.iex.ssap.financial;

/**
 * Describes bit flags for financial SSAP UI flow.
 *
 * <p>
 *   Based on the flags UI will display various UI blocks.
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 5/23/19
 */
public class FlagBucket {
  private long annualTaxIncomeFlags = 18L;
  private long taxHouseholdCompositionFlags = 604_587_154L;
  private long taxHouseholdCompositionProgressFlags = 21_650L;

  public long getAnnualTaxIncomeFlags() {
    return annualTaxIncomeFlags;
  }

  public void setAnnualTaxIncomeFlags(long annualTaxIncomeFlags) {
    this.annualTaxIncomeFlags = annualTaxIncomeFlags;
  }

  public long getTaxHouseholdCompositionFlags() {
    return taxHouseholdCompositionFlags;
  }

  public void setTaxHouseholdCompositionFlags(long taxHouseholdCompositionFlags) {
    this.taxHouseholdCompositionFlags = taxHouseholdCompositionFlags;
  }

  public void setTaxHouseholdCompositionProgressFlags(long taxHouseholdCompositionProgressFlags) {
    this.taxHouseholdCompositionProgressFlags = taxHouseholdCompositionProgressFlags;
  }

  public long getTaxHouseholdCompositionProgressFlags() {
    return taxHouseholdCompositionProgressFlags;
  }
}
