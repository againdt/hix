<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<jsp:include page="/WEB-INF/views/layouts/untiletopbar.jsp" />

<%@ page isELIgnored="false"%>

<h2><a>Static Content from Alfresco</a></h2>
<ecm:staticcontent contentPath="module_consumer/page_landing/field_landing_header"/>
<ecm:staticcontent contentPath="module_consumer/page_landing/field_landing_footer"/>

<div id="maincontainer" class="gutter10">
<div class="">
	<div class="span4">
		<h2>Role Administration</h2>
	</div>
	<div>
		<br><br/>
		<p><a href="create">Create Role</a></p>
	</div>
	<div>
		<form class="form-stacked" id="frmlist" name="frmlist" action="list" method="POST">
			<df:csrfToken/>
			<fieldset>
				<legend></legend>
				<p></p>
				<br />
				<div class="profile">
					<div class="clearfix">
						  <display:table pagesize="5" export="true" name="rolelist" sort="list" id="data" requestURI="list">
							  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
							  
							  <display:setProperty name="export.excel" value="true" />
							  <display:setProperty name="export.csv" value="true" />
							  <display:setProperty name="export.xml" value="true" />
							  
							  <display:setProperty name="export.excel.filename" value="rolelist.xls"/>
							  <display:setProperty name="export.csv.filename" value="rolelist.csv"/>
							  <display:setProperty name="export.xml.filename" value="rolelist.xml"/>
							 
							  <display:column title="Role Id">
							  		<a href='edit/${data.id}'>${data.id}</a>
							  </display:column>
							  <display:column property="name" title="Role Name" sortable="true"/>
							  <display:column property="description" title="Role Description" sortable="true"/>
							  <display:column property="landingPage" title="Landing Page" sortable="true"/>
							  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true"/>
						</display:table>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>
</div>



<h4><a>ECM Demo:- </a></h4>
<h4><a>---------------------</a></h4>
<h4>ObjectByPath.txt content:- </h4> 
<ecm:contentbyfilename contentPath="Automation Tests/testFolder/ObjectByPath.txt"/>
<br> <a HREF="http://www.ghixecm.com/share/page/inline-edit?nodeRef=workspace://SpacesStore/a9c3775c-185b-4612-8e4b-b4fd87611ead" target="_blank" >Edit</a>

<br><br>
<h4>Files under Automation Tests folder :- </h4>
<ecm:getfolderfilesbyname contentPath="Automation Tests"/>

<br><br>                                         
<h4>File Properties :- </h4>
<ecm:getfilepropertiesbyname contentPath="Automation Tests/testFolder/ObjectByPath.txt"/>

<br><br/>
<h4><a>ECM Demo using web scripts:- </a></h4>
<h4><a>---------------------------------------------</a></h4>
<a HREF="http://72.32.227.7:8080/alfresco/service/sample/hello" target="_blank" >ECM logged in user</a>
<br><br/>
<a HREF="http://72.32.227.7:8080/alfresco/service/listdocuments" target="_blank" >List of files under Company home (root) folder</a>
<br><br/>
<a HREF="http://72.32.227.7:8080/alfresco/service/viewdocument/BrokerDesignation" target="_blank" >Content of BrokerDesignation</a>
<br><br/>
</div>
<%@ include file="/WEB-INF/views/layouts/untilefooter.jsp"%>
