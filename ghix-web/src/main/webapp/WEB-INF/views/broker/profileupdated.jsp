<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<link href="../resources/css/bootstrap-responsive.min.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../resources/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/${bootstrap_style}' />" media="screen,print" rel="stylesheet" type="text/css" />
<link href="../resources/css/ghixcustom.css" media="screen" rel="stylesheet" type="text/css" />

<body class="padding0" style="background:#fff;">		
<div id="successbox">
	<div  class="success modal-body">
	<form class="form-vertical" id="profileupdated" name="profileupdated">		
	<p>
	    <c:choose>
			<c:when test="${isAgencyManager}">
				 <spring:message code='label.agency.brokeRegistrationComplete' htmlEscape='false' />
			    <c:choose>
					<c:when test="${CA_STATE_CODE}">
						 ${exchangeName} <spring:message code='label.agency.brokeRegistrationCompleteCA' htmlEscape='false' />
					</c:when>
			    </c:choose>
			</c:when>
			<c:otherwise>
				<spring:message code='label.brokeRegistrationComplete' htmlEscape='false' />
			    <c:choose>
					<c:when test="${CA_STATE_CODE}">
						 ${exchangeName} <spring:message code='label.brokeRegistrationCompleteCA' htmlEscape='false' />
					</c:when>
			    </c:choose>
			</c:otherwise>
	    </c:choose>
	 </p>
	</form>
	</div>
	<div class="modal-footer">
		<c:choose>
			<c:when test="${CA_STATE_CODE}">
				<input name="submitRequest" id="submitRequest" type="submit" data-dismiss="modal" aria-hidden="true" value='<spring:message code="label.button.exit"/>' onClick="window.parent.closeSuccess();" class="btn btn-primary" />			 
			</c:when>
			<c:otherwise>
    	<input name="submitRequest" id="submitRequest" type="submit" data-dismiss="modal" aria-hidden="true" value='<spring:message code="label.button.close"/>' onClick="window.parent.closeSuccess();" class="btn btn-primary" />
			</c:otherwise>
		</c:choose>
    	
	</div>
</div>
</body>