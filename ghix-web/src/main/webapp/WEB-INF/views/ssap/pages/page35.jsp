<%@ include file="../include.jsp" %>
<div id="appscr85" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page35.signAndSubmit" text="Sign & Submit"/>
			</h4>
		</div>
		<form id="appscr85Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<h4>
				<spring:message code="ssap.page35.ReadAndCheck" text="Read and check the box next to each statement if you agree."/>
			</h4>
			
			<div class="non_financial_hide">
				<div class="control-group">
					<div class="margin5-l">
						<label class="capitalize-none checkbox" for="agree_stat1">
							<input type="checkbox" id="agree_stat1" name="agree_stat1" value="medical_expense" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"/>
							<spring:message code="ssap.page35.medicaidPaysCondition" text="I know that if Medicaid pays for a 	medical expense, any money I get from other health insurance or legal settlements will go to Medicaid in an amount equal to what Medicaid pays for the expense."/>
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label>
					</div>
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<div class="margin5-l">
						<label class="capitalize-none checkbox" for="agree_stat2">
							<input type="checkbox" id="agree_stat2" name="agree_stat2" value="medical_expense" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"/>
							<spring:message code="ssap.page35.iWillBeAskedToMedicaidAgency" text=" I know I will be asked to cooperate with the agency that collects medical support from an absent parent. If I think that cooperating to collect medical support will harm me or my children, I can tell the agency and won't have to cooperate."/>
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label>
					</div>
				</div><!-- control-group ends -->
			</div>
			
			<div class="control-group">
			
				<p><spring:message code="ssap.page35.incarceratedQuestion" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>
				<div class="margin5-l">
					<label class="capitalize-none" for="ononeIncarceratedStatus">
						<input type="checkbox" onchange="incarceratedOnChange(this)" id="ononeIncarceratedStatus" name="agree_stat3" value="medical_expense"/>
						<spring:message code="ssap.page35.prisonAndJailCondition" text="No one applying for health insurance on this application is incarcerated (in prison or jail)."/>						
					</label>
					<div id="incarceratedContent85" class="margin30-l"></div>
				</div>
			</div><!-- control-group ends -->
			
			
			
			<div class="control-group">
				<div class="margin5-l">
					<p class="non_financial_hide"><spring:message code="ssap.page35.incomeDataAgreementFA"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>
					<p class="financial_hide"><spring:message code="ssap.page35.incomeDataAgreementNFA"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>
					<label class="capitalize-none">
						<input type="radio" name="useIncomeDataIndicator" value="yes" id="useIncomeDataIndicatorYes" onclick="updateTaxReturnPeriod()" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#useIncomeDataIndicatorYesErrorDiv"> <spring:message code="ssap.page35.agree"/>
					</label>
					<label class="capitalize-none">
						<input type="radio" name="useIncomeDataIndicator"  value="no" id="useIncomeDataIndicatorNo" onclick="updateTaxReturnPeriod()"> <spring:message code="ssap.page35.disagree"/>
					</label>
					<div id="useIncomeDataIndicatorYesErrorDiv"></div>
				</div>
			</div>
			
			
			<div class="control-group hide" id="taxReturnPeriodDiv">
				<div class="margin5-l">
					<p><spring:message code="ssap.page35.permissionPeriod"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>
					<label class="capitalize-none">
						<input type="radio" name="taxReturnPeriod" value="1" id="taxReturnPeriod1" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#taxReturnPeriod1ErrorDiv"> <spring:message code="ssap.page35.1year"/>
					</label>
					<label class="capitalize-none">
						<input type="radio" name="taxReturnPeriod"  value="2" id="taxReturnPeriod2">  <spring:message code="ssap.page35.2years"/>
					</label>
					<label class="capitalize-none">
						<input type="radio" name="taxReturnPeriod"  value="3" id="taxReturnPeriod3">  <spring:message code="ssap.page35.3years"/>
					</label>
					<label class="capitalize-none">
						<input type="radio" name="taxReturnPeriod"  value="4" id="taxReturnPeriod4">  <spring:message code="ssap.page35.4years"/>
					</label>
					<label class="capitalize-none">
						<input type="radio" name="taxReturnPeriod"  value="5" id="taxReturnPeriod5"> <spring:message code="ssap.page35.5years"/>
					</label>
					<div id="taxReturnPeriod1ErrorDiv"></div>
				</div>
			</div>
			
			
			
			<div class="control-group">
				<div class="margin5-l">
					<label class="capitalize-none">
						<input type="checkbox" name="agree_stat4" value="medical_expense" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"/>
						<spring:message code="ssap.page35.tellTheProgramEnrolledIn" text="I know that I must tell the program I'm enrolled in if information I listed on this application changes."/>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
				</div>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<div class="margin5-l">
					<label class="capitalize-none">
						<input type="checkbox" name="agree_stat5" value="medical_expense" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" />
						<spring:message code="ssap.page35.siginginTheApp" text="I'm signing this application under penalty of per jury. This means I've provided true answers to all the questions on this form  to best of my knowledge. I know that if I'm not truthful, there may be a penalty."/>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
				</div>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page35.digitalSign" text="'s Electronic Signature:"/>
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l">
					<div for="appscr85ESignature">
						<input type="text" placeholder="<spring:message code="ssap.page35.ES" text="Electronic Signature"/>" name="Household_sign" class="input-medium" id="appscr85ESignature" name="appscr85ESignature" onblur = "validateSignature()" data-parsley-errors-container="#esignErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"/>
					</div>
				</div>
				<div id="esignErrorDiv"></div>
			</div><!-- control-group ends -->
			
			<%-- <div class="control-group">
				<p>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page35.digitalSign" text="'s Electronic Signature:"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="text" placeholder="<spring:message code="ssap.page35.ES" text="Electronic Signature"/>" name="APTC_sign" class="input-medium" id="appscr85APTCSign" name="appscr85APTCSign"/>
					</label>
				</div>
			</div><!-- control-group ends --> --%>
			
		</form>
	</div>
</div>

<div id="modalMailingAddressNotSameAsHousehold" class="modal hide fade">
	<div class="modal-header">
        <h3><spring:message code="ssap.page35.mailingAddress.title" text=""/></h3>
  	</div>
	<div class="modal-body">
		<p><spring:message code="ssap.page35.mailingAddress.message" text=""/></p>
	</div>
	<div class="modal-footer">
		<a href="#" data-dismiss="modal" class="btn" id="btnModalMailingAddressOk" onClick="$('#modalMailingAddressNotSameAsHousehold').hide();saveDataSubmit();">OK</a>
	</div>
</div>
<div id="modalCSRApplicationType" class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>CSR Application Type</h3>
  </div>
  <div class="modal-body">
    <p><spring:message code="ssap.trackApplication.submittingApplicationOnBefalfApplicant" text="You are submitting an application on behalf of an applicant"/></p>
    <p><spring:message code="ssap.trackApplication.submittingApplicationCaptureSourceOfApplication" text="For reporting purposes, we need to capture the source of the application"/></p>
    <p><spring:message code="ssap.trackApplication.submittingApplicationSelectApplication" text="Please select the source of the application"/> </p>
    <div id="u6" class="ax_radio_button"> 
		<p><input id="optCSRApplicationTypeMail" type="radio" value="WI" name="optCSRApplicationType" 
			onClick="javascript:enableSubmitCSRApplicationType()"
			text="Walk-in (in-person, paper, email or postal mail)" aria-label="Walk-in (in-person, paper, email or postal mail)">
			<span> <spring:message code="ssap.trackApplication.submittingApplicationWalk-in" text="Walk-in (in-person, paper, email or postal mail)"/> &nbsp; </span></p>
		<p><input id="optCSRApplicationTypePhone" type="radio" value="PH" name="optCSRApplicationType" onClick="javascript:enableSubmitCSRApplicationType()"
			text="Telephone" aria-label="Telephone"><span> <spring:message code="ssap.trackApplication.telephone" text="Telephone" />&nbsp; </span></p>
    </div>      
  
  <div id="csrQepEventNameDiv" class="control-group">
		<label class="required control-label capitalize-none"><spring:message code="ssap.page35.qep.eventName"/>:			
			<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
			<span aria-label="Required!"></span>
		</label><!-- end of label -->
		<div class="controls">
		<select id="sepEventCSR" onChange="validateEvent($(this));validateEventDate($('#sepEventDateCSR'));"
		aria-label="SEP Event options">		
			<option value="">Please select event</option>          	
			<option value="CHANGE_IN_LEGAL_PRESENCE">Change in Immigration Status</option>
			<option value="CHANGE_IN_INCARCERATION">Change in Incarceration Status</option>
			<option value="CHANGE_IN_INCOME">Change in Income</option>
			<option value="BIRTH_ADOPTION">Change in Your Dependents</option>			
			<option value="MOVED_INTO_STATE">Change of Address</option>
			<option value="DEATH">Death</option>
			<option value="DIVORCE">Divorce</option>
			<option value="LOST_OTHER_MIN_ESSENTIAL_COVERAGE">Loss of Minimum Essential Coverage</option>
			<option value="MARRIAGE">Marriage</option>
			<option value="CHANGE_IN_AMERICAN INDIAN / ALSAKA NATIVE STATUS">Native Americans</option>
        </select>								
		<div id="sepEventErrorDivCSR" class="dobCustomError"></div>				 
		</div><!-- end of controls-->
	</div><!-- control-group ends -->
	
	<div id="csrQepEventDateDiv" class="control-group">
		<label class="required control-label capitalize-none"> <spring:message code="ssap.page35.qep.eventDate"/>:			
			<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
			<span aria-label="Required!"></span>
		</label><!-- end of label -->
		<div class="controls">
			<div class="input-append date event-date-picker"  data-date="">
				<input type="text" id="sepEventDateCSR" name="sepEventDate" onblur="validateEventDate($(this))" onfocusout="validateEventDate($(this))" class="input-medium dob-input" placeholder="MM/DD/YYYY" data-parsley-errors-container="#sepEventDateErrorDiv" data-parsley-required data-parsley-required-message="Please Enter Valid Date." />				
				<span class="add-on dateSpan js-dateTrigger"><i class="icon-calendar"></i></span>
			</div>			 
		</div><!-- end of controls-->
	</div><!-- control-group ends -->
	</div>
  <div class="modal-footer">
    <a href="#" data-dismiss="modal" class="btn" id="btnCancelCSRApplicationType" onClick="javascript:cancelCSRApplicationType()">Cancel</a>
    <a href="#" onClick="javascript:saveIncarceratedDetailDB()" class="btn btn-primary" id="btnSubmitCSRApplicationType" disabled='disabled'>Submit Application</a>
  </div>
</div>
<div id="eventModal" class="modal hide fade">
  <div class="modal-header">  
    <h3><spring:message code="ssap.page35.qep.title"/></h3>
  </div>
  <div class="modal-body" id="pageSSSAPEvent">
	
	<div class="control-group">
		<label class="required control-label capitalize-none"><spring:message code="ssap.page35.qep.eventName"/>:			
			<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
			<span aria-label="Required!"></span>
		</label><!-- end of label -->
		<div class="controls">
		<select id="sepEvent" onChange="validateEvent($(this));"
		aria-label="SEP event options">		
			<option value="">Please select event</option>                 	
			<option value="CHANGE_IN_LEGAL_PRESENCE">Change in Immigration Status</option>
			<option value="CHANGE_IN_INCARCERATION">Change in Incarceration Status</option>
			<option value="CHANGE_IN_INCOME">Change in Income</option>
			<option value="BIRTH_ADOPTION">Change in Your Dependents</option>			
			<option value="MOVED_INTO_STATE">Change of Address</option>
			<option value="DEATH">Death</option>
			<option value="DIVORCE">Divorce</option>
			<option value="LOST_OTHER_MIN_ESSENTIAL_COVERAGE">Loss of Minimum Essential Coverage</option>
			<option value="MARRIAGE">Marriage</option>
			<option value="CHANGE_IN_AMERICAN INDIAN / ALSAKA NATIVE STATUS">Native Americans</option>                   
        </select>								
		<div id="sepEventErrorDiv" class="dobCustomError"></div>				 
		</div><!-- end of controls-->
	</div><!-- control-group ends -->
	
	<div class="control-group">
		<label class="required control-label capitalize-none"> <spring:message code="ssap.page35.qep.eventDate"/>:			
			<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
			<span aria-label="Required!"></span>
		</label><!-- end of label -->
		<div class="controls">
			<div class="input-append date event-date-picker"  data-date="">
				<input type="text" id="sepEventDate" name="sepEventDate" class="input-medium dob-input" placeholder="MM/DD/YYYY" data-parsley-errors-container="#sepEventDateErrorDiv" data-parsley-required data-parsley-required-message="Please Enter Valid Date." />
				<span class="add-on dateSpan js-dateTrigger"><i class="icon-calendar"></i></span>
			</div>			 
		</div><!-- end of controls-->
	</div><!-- control-group ends -->
	
 </div>
  <div class="modal-footer">   
   	<a href="#" data-dismiss="modal" class="btn" id="btnCancelCSRApplicationType" onClick="javascript:cancelSSAPEvent()()">Cancel</a>
    <a href="#" onClick="javascript:saveSSAPEvent();validateEventDate($('#sepEventDate'));validateEvent($('#sepEvent'));" class="btn btn-primary" id="btnSubmitCSRApplicationType">Submit Application</a>    
  </div>
</div>