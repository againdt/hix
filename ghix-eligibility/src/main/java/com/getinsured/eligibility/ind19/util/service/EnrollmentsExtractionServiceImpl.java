package com.getinsured.eligibility.ind19.util.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

@Service
public class EnrollmentsExtractionServiceImpl implements EnrollmentsExtractionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentsExtractionServiceImpl.class);

	@Autowired 
	private GhixRestTemplate ghixRestTemplate;
	
	@Override
	public Map<String, EnrollmentShopDTO> extractActiveMedicalAndDentalEnrollments(Long ssapApplicationId) throws GIException {
		
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>();
		
		List<EnrollmentShopDTO> healthEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		List<EnrollmentShopDTO> dentalEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		processEnrollments(ssapApplicationId, healthEnrollments, dentalEnrollments);
		if(!healthEnrollments.isEmpty()){
			enrollmentsByTypeMap.put("health", healthEnrollments.get(0));
		}
		if(!dentalEnrollments.isEmpty()){
			enrollmentsByTypeMap.put("dental", dentalEnrollments.get(0));
		}
		
		return enrollmentsByTypeMap;
	}

	public void processEnrollments(Long ssapApplicationId, List<EnrollmentShopDTO> healthEnrollments,
			List<EnrollmentShopDTO> dentalEnrollments) throws GIException {
		List<EnrollmentShopDTO> enrollmentShopDTOs = fetchEnrollmentsById(ssapApplicationId, healthEnrollments,dentalEnrollments);
				
		if(null!=enrollmentShopDTOs && !enrollmentShopDTOs.isEmpty()){
			sortList(healthEnrollments);
			sortList(dentalEnrollments);
		}
	}

	public void sortList(List<EnrollmentShopDTO> enrollments) {
		if(enrollments.size()>1){
			Collections.sort(enrollments, new Comparator<EnrollmentShopDTO>(){
			     public int compare(EnrollmentShopDTO enrollmentId1, EnrollmentShopDTO enrollmentId2){
			         return Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime()));
			     }

			});
		}
	}

	public List<EnrollmentShopDTO> fetchEnrollmentsById(Long ssapApplicationId,
			List<EnrollmentShopDTO> healthEnrollments, List<EnrollmentShopDTO> dentalEnrollments) throws GIException {
		List<EnrollmentShopDTO> enrollmentShopDTOs = getEnrollments(ssapApplicationId);

		if(enrollmentShopDTOs != null && !enrollmentShopDTOs.isEmpty()){
			for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
				if(enrollmentShopDto.getPlanType().equalsIgnoreCase("health")){
					healthEnrollments.add(enrollmentShopDto);
				}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase("dental")){
					dentalEnrollments.add(enrollmentShopDto);
				}
			}
		}
		
		return enrollmentShopDTOs;
	}

	private  List<EnrollmentShopDTO>  getEnrollments(Long ssapApplicationId) throws GIException{
		
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		
		try {
			if(null  != ssapApplicationId){
				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(ssapApplicationId);
				// Pass user name in Ind19ClientRequest and use it here through a new parameter
				ObjectMapper mapper = new ObjectMapper();
				//HIX-59205 Use of JSON API to get enrollment details
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON, 
						"exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, mapper.writeValueAsString(enrolleeRequest)).getBody();
				if (null != response){ 
					Gson gson = new Gson();
					enrolleeResponse = gson.fromJson(response, EnrolleeResponse.class);
					if(enrolleeResponse != null	
						&& enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
					}					
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			throw new GIException("Exception occured while fetching enrollment details.", e);
		}
		return enrollmentShopDTOs;
	}
	
	@Override
	public Map<String, EnrollmentShopDTO> extractActiveEnrollmentsForApplicants(Long ssapApplicationId,List<String> applicationGUIDs) throws GIException {
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>();
		
		List<EnrollmentShopDTO> healthEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		List<EnrollmentShopDTO> dentalEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		List<EnrollmentShopDTO> filteredHealthEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		List<EnrollmentShopDTO> filteredDentalEnrollments = new ArrayList<EnrollmentShopDTO>(0);
		processEnrollments(ssapApplicationId, healthEnrollments, dentalEnrollments);
		
		if(CollectionUtils.isNotEmpty(applicationGUIDs)) {
			if(CollectionUtils.isNotEmpty(healthEnrollments)){
				for(EnrollmentShopDTO enrollmentShopDTO : healthEnrollments){
					if(!"CANCEL".equalsIgnoreCase(enrollmentShopDTO.getEnrollmentStatusValue())) {
						for(EnrolleeShopDTO enrollee: enrollmentShopDTO.getEnrolleeShopDTOList()){
							if (applicationGUIDs.contains(enrollee.getExchgIndivIdentifier()))
							{
								filteredHealthEnrollments.add(enrollmentShopDTO);
							}
						}
					}
				}
			}
			if(CollectionUtils.isNotEmpty(dentalEnrollments)){
				for(EnrollmentShopDTO enrollmentShopDTO : dentalEnrollments){
					if(!"CANCEL".equalsIgnoreCase(enrollmentShopDTO.getEnrollmentStatusValue())) {
						for(EnrolleeShopDTO enrollee: enrollmentShopDTO.getEnrolleeShopDTOList()){
							if (applicationGUIDs.contains(enrollee.getExchgIndivIdentifier()))
							{
								filteredDentalEnrollments.add(enrollmentShopDTO);
							}
						}	
					}
				}
			}
		}
		else
		{
			filteredDentalEnrollments = dentalEnrollments;
			filteredHealthEnrollments = healthEnrollments;
		}
		if(!filteredHealthEnrollments.isEmpty()){
			enrollmentsByTypeMap.put("health", filteredHealthEnrollments.get(0));
		}
		if(!filteredDentalEnrollments.isEmpty()){
			enrollmentsByTypeMap.put("dental", filteredDentalEnrollments.get(0));
		}
		return enrollmentsByTypeMap;
	}

}
