import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

class HouseHold extends Component {

    constructor(props) {
        super(props)
    }

    getEligibilityResults = () => {
        if ($("#stateCode").val() === "CA") {
            if ($("#householdCaseID").val()) {
                window.location = "https://" + calheersEnv + "/static/lw-web/eligibility?householdCaseId=" + $("#householdCaseID").val();

            } else {
                window.location = "https://" + calheersEnv + "/static/lw-web/eligibility";
            }
        } else {
            window.caseNumber = this.props.data.caseNumber;
            window.location = `./indportal#/eligibilityresults?caseNumber=${this.props.data.caseNumber}`;
        }
    };

    reportAChange = () => {
        this.props.onReportChange(this.props);
    };

    uploadDocuments = () => {
        window.location = '/hix/ssap/verificationResultDashboard?caseNumber=' + this.props.data.caseNumber;
    };

    render() {

        const conditionalEligibilityText =
            <span className="ds-u-margin-left--2" style={{ color: '#DB0000' }}>
                <FormattedMessage id="household_section.conditional_elibility_text"/>
                <a className="ds-u-text-decoration--underline ds-u-margin-left--1 ds-u-font-weight--semibold"
                   style={{ color: '#DB0000' }} href="javascript:void(0)"
                   id="household_conditional_eligibility"
                   onClick={() => this.uploadDocuments()}
                >
                    <FormattedMessage id="household_section.conditional_elibility_link_text"/>
                </a>
            </span>;


        const header =
            <div className="ds-l-row">
                <h3 className="ds-h6 ds-u-font-weight--semibold"
                    id="household_elig_header"
                >
                    <FormattedMessage id="household_section.header" /> {this.props.data.isEligibilityConditional ? conditionalEligibilityText : null}
                </h3>
            </div>;

        const members = this.props.data.houseHoldInfo.members.map((item, i) => {
            return <div key={`household_elig_member_${i}`}>
                <span className="ds-u-text-transform--capitalize"> {item.label} </span>
                <span className={item.textType} id={"household_elig_member_" + i}> {item.eligible} </span>
            </div>
        });

        const elibility = this.props.data.houseHoldInfo.status.map((item, i) => {
            return <div key={`household_elig_status_${i}`}>
                <span className={item.textType}
                      id={"household_elig_status_" + i}
                > {item.label}
                </span>
            </div>
        });

        const isMembersAvailable = this.props.data.houseHoldInfo.members && this.props.data.houseHoldInfo.members.length > 0;

        const viewMultiHouseholdLink = this.props.isMultiHousehold ?
            <div className="ds-u-text-decoration--underline">
                <a href="javascript:void(0)"
                   id="household_other_tax_households"
                   onClick={() => window.location = '#multiHousehold'}
                >
                    <FormattedMessage id="household_section.multi_household_link_text"/>
                </a>
            </div>
            : null;

        const assessedEligibilityText = this.props.data.isAssessedMedicaidOrChipEligible ? 
        		<div className="ds-l-row">
        			<p><FormattedMessage id="household_section.assessed_elibility_text"/></p><br/>
        		</div>:null;
        		

        if (this.props.data.houseHoldInfo.type === 'MULTIPLE_COL' && isMembersAvailable) {
            return (
                <div id="auto-household">
                    <div className="ds-u-margin--2 ds-u-font-size--small">
                        {header}
                        <div className="ds-l-row ds-u-margin--1 ds-u-font-size--small">
                        {assessedEligibilityText}
                            <div
                                className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--5 ds-l-lg-col--5 ds-l-xl-col--5">
                                {members}
                            </div>
                            <div
                                className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--4 ds-l-lg-col--4 ds-l-xl-col--4">
                                <div className="ds-u-text ds-u-measure--wide">
                                    {elibility}
                                </div>
                            </div>
                            <div
                                className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">
                                <div className="ds-u-text-decoration--underline">
                                    <a href="javascript:void(0)"
                                       id="household_elig_link"
                                       onClick={() => this.getEligibilityResults()}
                                    >
                                        {this.props.data.houseHoldInfo.linkInfo.linkText}
                                    </a>
                                </div>
                                <br/>
                                <div className="ds-u-text-decoration--underline">
                                    <a href="javascript:void(0)" id="household_report_change_link"
                                       onClick={() => this.reportAChange()}
                                    >
                                        <FormattedMessage id="report_a_change"/>
                                    </a>
                                </div>
                                <br/>
                                {viewMultiHouseholdLink}
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else if (isMembersAvailable) {
            return (
                <div id="auto-household">
                    <div className="ds-u-margin--2">
                        {header}
                        <div className="ds-l-row ds-u-margin--1 ds-u-font-size--small">
                            <div
                                className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">
                                {members}
                            </div>
                            <div
                                className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--9 ds-l-lg-col--9 ds-l-xl-col--9">
                                <p className="ds-u-text ds-u-measure--wide" id="household_elig_content_members">
                                    {this.props.data.houseHoldInfo.content}
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div id="auto-household">
                    <div className="ds-u-margin--2">
                        {header}
                        <div className="ds-l-row ds-u-margin--1 ds-u-font-size--small">
                            <div className="ds-l-col--12 ds-u-text" id="household_elig_content">
                                {this.props.data.houseHoldInfo.content}
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default HouseHold;