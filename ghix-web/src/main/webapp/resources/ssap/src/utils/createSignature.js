

export default fullName => {
    const {
        firstName,
        middleName,
        lastName,
        suffix
    } = fullName;

    return [
        `${firstName} ${lastName}`,
        `${firstName} ${middleName} ${lastName}`,
        `${firstName} ${middleName} ${lastName} ${suffix}`,
        `${firstName} ${lastName} ${suffix}`,
    ];
};

