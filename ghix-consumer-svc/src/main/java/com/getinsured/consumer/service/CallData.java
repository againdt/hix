package com.getinsured.consumer.service;

public class CallData {

	private String wrapCode;
	private String callType;

	public String getWrapCode() {
		return wrapCode;
	}

	public void setWrapCode(String wrapCode) {
		this.wrapCode = wrapCode;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}


}
