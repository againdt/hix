<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="gutter10">	
	<div class="row-fluid margin20-t">
	    <div class="span10 offset1" id="rightpanel">		    	
			<div class="alert alert-info margin20-t">
				<h1 class="margin20-tb margin20-l"><spring:message  code="lable.userCreationSuccessHeader"/></h1>
				<p class="margin20-b margin20-l"><spring:message  code="label.userCreationSuccess"/> <a href="<c:url value="/account/user/login"/>" class="btn btn-primary pull-right"><spring:message  code="label.loginsso"/></a></p>
			</div>
		</div>
	</div>
</div>

