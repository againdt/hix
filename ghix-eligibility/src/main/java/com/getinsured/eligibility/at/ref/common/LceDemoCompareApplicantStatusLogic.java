package com.getinsured.eligibility.at.ref.common;

import org.apache.commons.lang.StringUtils;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
public final class LceDemoCompareApplicantStatusLogic {
	private LceDemoCompareApplicantStatusLogic() {
	}

	public static ApplicantStatusEnum demoChangeApplicantStatus(CompareApplicantDTO applicantDTO) {
		ApplicantStatusEnum status = ApplicantStatusEnum.NO_CHANGE;

		int count = 0;
		if (applicantDTO.isFirstNameChanged()) {
			status = ApplicantStatusEnum.DEMO_FIRSTNAME;
			count++;
		}
		
		if (applicantDTO.isNameSuffixChanged()) {
			status = ApplicantStatusEnum.DEMO_NAMESUFFIX;
			count++;
		}

		if (applicantDTO.isMiddleNameChanged()) {
			status = ApplicantStatusEnum.DEMO_MIDDLENAME;
			count++;
		}

		if (applicantDTO.isLastNameChanged()) {
			status = ApplicantStatusEnum.DEMO_LASTNAME;
			count++;
		}

		if (applicantDTO.isSsnChanged()) {
			status = ApplicantStatusEnum.DEMO_SSN;
			count++;
		}

		if (applicantDTO.isPrimaryAddressChanged()) {
			status = ApplicantStatusEnum.DEMO_PRIMARY_ADDRESS;
			count++;
		}

		if (applicantDTO.isMailAddressChanged()) {
			status = ApplicantStatusEnum.DEMO_MAILING_ADDRESS;
			count++;
		}

		if (applicantDTO.isAddressLine1Changed()) {
			status = ApplicantStatusEnum.DEMO_ADDRESS_LINE1;
			count++;
		}

		if (applicantDTO.isAddressLine2Changed()) {
			status = ApplicantStatusEnum.DEMO_ADDRESS_LINE2;
			count++;
		}
		
		if (applicantDTO.isCityChanged()) {
			status = ApplicantStatusEnum.DEMO_ADDRESS_CITY;
			count++;
		}

		if (applicantDTO.isGenderChanged()) {
			status = ApplicantStatusEnum.DEMO_OTHER;
			count++;
		}

		if (applicantDTO.isEmailAddressChanged()) {
			status = ApplicantStatusEnum.DEMO_OTHER;
			count++;
		}

		if (applicantDTO.isRaceEthnicityChanged()) {
			status = ApplicantStatusEnum.DEMO_OTHER;
			count++;
		}
		if (applicantDTO.isPhoneChanged()) {
			status = ApplicantStatusEnum.DEMO_OTHER;
			count++;
		}
		if (applicantDTO.isPrimaryTaxFilerChange()) {
			status = ApplicantStatusEnum.DEMO_PTF;
			count++;
		}
		if (applicantDTO.isMarriedChanged()) {
			status = ApplicantStatusEnum.DEMO_MARITIAL_STATUS;
			count++;
		}
		if (applicantDTO.isSpokenLanguageChanged()) {
			status = ApplicantStatusEnum.DEMO_SPOKEN_LANGUAGE;
			count++;
		}
		if (applicantDTO.isWrittenLanguageChanged()) {
			status = ApplicantStatusEnum.DEMO_WRITTEN_LANGUAGE;
			count++;
		}
		if (applicantDTO.isExternalApplicantIdChanged()) {
			status = ApplicantStatusEnum.DEMO_OTHER;
			count++;
		}
		if (count > 1) {
			status = ApplicantStatusEnum.DEMO_MULTIPLE;
		}

		return status;
	}

	public static ApplicantStatusEnum lceCompareApplicantStatus(CompareApplicantDTO currentApplicant, CompareApplicantDTO enrolledApplicant) {
		ApplicantStatusEnum status = ApplicantStatusEnum.fromValue(currentApplicant.getStatus());
		boolean applicantNotEligibleBeforeAndNow = false;
        if((StringUtils.equalsIgnoreCase(ReferralConstants.ELIGIBLE_NONE, currentApplicant.getEligibilityStatus()) && 
        		(StringUtils.equalsIgnoreCase(ReferralConstants.ELIGIBLE_NONE, enrolledApplicant.getEligibilityStatus()) || StringUtils.equalsIgnoreCase(ReferralConstants.MEDICAID, enrolledApplicant.getEligibilityStatus())))
        		|| (StringUtils.equalsIgnoreCase(ReferralConstants.MEDICAID, currentApplicant.getEligibilityStatus()) && 
        				(StringUtils.equalsIgnoreCase(ReferralConstants.ELIGIBLE_NONE, enrolledApplicant.getEligibilityStatus()) || StringUtils.equalsIgnoreCase(ReferralConstants.MEDICAID, enrolledApplicant.getEligibilityStatus())))) {
        	applicantNotEligibleBeforeAndNow = true;
        }
		if (!StringUtils.equalsIgnoreCase(currentApplicant.getEligibilityStatus(), enrolledApplicant.getEligibilityStatus())) {
			if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, currentApplicant.getEligibilityStatus())) {
				status = ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE;
			} else if ((StringUtils.equalsIgnoreCase(ReferralConstants.ELIGIBLE_NONE, currentApplicant.getEligibilityStatus()) 
					|| StringUtils.equalsIgnoreCase(ReferralConstants.MEDICAID, currentApplicant.getEligibilityStatus()) ||
					   currentApplicant.getEligibilityStatus() == null)
			        && StringUtils.equalsIgnoreCase(ReferralConstants.QHP, enrolledApplicant.getEligibilityStatus())) {
				status = ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE;
			}
		} else if (!currentApplicant.isSeekQHP()) {
			status = ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE;
		} else if(!applicantNotEligibleBeforeAndNow){
			if (currentApplicant.isCitizenShipStatusChanged()) {
				status = ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS;
			} else if (currentApplicant.isBloodRelationshipChanged()) {
				status = ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION;
			} else if (currentApplicant.isDobChanged()) { 
				status = ApplicantStatusEnum.UPDATED_DOB;
			} else if (currentApplicant.isZipCountyChanged()) {
				status = ApplicantStatusEnum.UPDATED_ZIP_COUNTY;
			} else {
				final CompareApplicationDTO currentCompareApplicationDTO = currentApplicant.getCompareApplicationDTO();
				final CompareApplicationDTO enrolledCompareApplicationDTO = enrolledApplicant.getCompareApplicationDTO();

				//Added below condition to support automatic NF-F conversion
				//Setting csr level to null if it is N/A to handle the NF application created through SSAP flow which has CSR level as N/A
				if("N/A".equalsIgnoreCase(enrolledApplicant.getCsrLevel())) {
					enrolledApplicant.setCsrLevel(null);
				}
				if (!StringUtils.equalsIgnoreCase(currentApplicant.getCsrLevel(), enrolledApplicant.getCsrLevel())) {
				    status = ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL;
				} else if (currentCompareApplicationDTO.getExchangeEligibilityStatus().toString().contains("APTC") 
						   && ("QHP".equalsIgnoreCase(enrolledCompareApplicationDTO.getExchangeEligibilityStatus().toString()) 
						   || "CSR".equalsIgnoreCase(enrolledCompareApplicationDTO.getExchangeEligibilityStatus().toString()))) {
				    status = ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT;
				} else if (enrolledCompareApplicationDTO.getExchangeEligibilityStatus().toString().contains("APTC") 
						   && ("QHP".equalsIgnoreCase(currentCompareApplicationDTO.getExchangeEligibilityStatus().toString()) 
						   || "CSR".equalsIgnoreCase(currentCompareApplicationDTO.getExchangeEligibilityStatus().toString()))) {
				    status = ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT;
				} else if (currentCompareApplicationDTO.getExchangeEligibilityStatus() != enrolledCompareApplicationDTO.getExchangeEligibilityStatus()) {
					status = ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY;
				} else if ((currentCompareApplicationDTO.getMaximumAPTC() == null && enrolledCompareApplicationDTO.getMaximumAPTC() != null)
				        || (currentCompareApplicationDTO.getMaximumAPTC() != null && enrolledCompareApplicationDTO.getMaximumAPTC() == null)
				        || (currentCompareApplicationDTO.getMaximumAPTC() !=null && enrolledCompareApplicationDTO.getMaximumAPTC()!= null  
				        && currentCompareApplicationDTO.getMaximumAPTC().compareTo(enrolledCompareApplicationDTO.getMaximumAPTC()) != 0)) {
					status = ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT;
				} else if ((currentCompareApplicationDTO.getMaximumStateSubsidy() == null && enrolledCompareApplicationDTO.getMaximumStateSubsidy() != null)
						|| (currentCompareApplicationDTO.getMaximumStateSubsidy() != null && enrolledCompareApplicationDTO.getMaximumStateSubsidy() == null)
						|| (currentCompareApplicationDTO.getMaximumStateSubsidy() !=null && enrolledCompareApplicationDTO.getMaximumStateSubsidy()!= null
						&& currentCompareApplicationDTO.getMaximumStateSubsidy().compareTo(enrolledCompareApplicationDTO.getMaximumStateSubsidy()) != 0)) {
					status = ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT;
				} else if(!enrolledApplicant.isAPTCEligible() && currentApplicant.isAPTCEligible()){
					status = ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT;
				}
			}

		}
		return status;
	}
}
