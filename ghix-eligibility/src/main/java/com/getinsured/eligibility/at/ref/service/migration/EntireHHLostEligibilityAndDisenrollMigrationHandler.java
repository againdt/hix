package com.getinsured.eligibility.at.ref.service.migration;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component("entireHHLostEligibilityAndDisenrollMigration")
public class EntireHHLostEligibilityAndDisenrollMigrationHandler extends LceProcessHandlerBaseMigrationService {
	
	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	/**
	 * When all applicant has non Qhp events invoke disenrollment api to disenrol the individual. if success Set ssap to CL. If failed manual process will be invoked. individual needs to disenroll from dashboard
	 */

	public void handleAllApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		ssapApplicationEventMigrationService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		getValidationStatus(currentApplication.getCaseNumber());
		
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		closeSsapApplication(enrolledApplication);
		closeSsapApplication(currentApplication);
	}

}
