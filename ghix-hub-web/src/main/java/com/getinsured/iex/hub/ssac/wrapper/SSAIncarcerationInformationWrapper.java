package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSAIncarcerationInformationType;

public class SSAIncarcerationInformationWrapper implements JSONAware {
	
	private String prisonerIdentification;
	private boolean inmateStatusIndicator;
	private String confinementDate;
	private SupervisionFacilityTypeWrapper supervisionFacility;
	private String reportingPersonText;

	public SSAIncarcerationInformationWrapper(SSAIncarcerationInformationType ssaIncarcerationType) {
		
		if(ssaIncarcerationType != null){
			this.prisonerIdentification = ssaIncarcerationType.getPrisonerIdentification();
			this.inmateStatusIndicator = ssaIncarcerationType.getInmateStatusIndicator().isValue();
			this.confinementDate = ssaIncarcerationType.getPrisonerConfinementDate();
			this.reportingPersonText = ssaIncarcerationType.getReportingPersonText();
			this.supervisionFacility = new SupervisionFacilityTypeWrapper(ssaIncarcerationType.getSupervisionFacility());
		}
	}
	
	public String getPrisonerIdentification() {
		return prisonerIdentification;
	}

	public boolean isInmateStatusIndicator() {
		return inmateStatusIndicator;
	}

	public String getConfinementDate() {
		return confinementDate;
	}

	public SupervisionFacilityTypeWrapper getSupervisionFacility() {
		return supervisionFacility;
	}

	public String getReportingPersonText() {
		return reportingPersonText;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PrisonerIdentification", this.prisonerIdentification);
		obj.put("InmateStatusIndicator", this.inmateStatusIndicator);
		obj.put("ReportingPersonText", this.reportingPersonText);
		obj.put("PrisonerConfinementDate", this.confinementDate);
		obj.put("SupervisionFacility", this.supervisionFacility);
		return obj.toJSONString();
	}

}
