package com.getinsured.hix.planmgmt.web;


import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerRepresentativeService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.IssuerService.RepStatus;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;


@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerRepresentativeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRepresentativeController.class);
	
	@Autowired private ContentManagementService ecmService;
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private LocationService locationService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private IssuerRepresentativeService issuerRepresentativeService;
	
	@Value("#{configProp['appUrl']}") private static String appUrl;
	@Value("#{configProp.uploadPath}") private String uploadPath;
	@Value("#{configProp['ecm.type']}") private String documentConfigurationType;

	private final Boolean isEmailActivation = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION).toLowerCase());

	
	/* newIssueraddRepresentative()  method is to navigate to the add representatives page with the issuer data */
	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/planmgmt/issuer/representatives/add", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADD_REPRESENTATIVE')")
	public String newIssueraddRepresentative(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		try {
		if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
			LOGGER.info("Add representative Page Info ");
			return "planmgmt/issuer/addrepresentatives";
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUER_ADDREPRESENTATIVE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	
	/*
	 * newIssuerSaveRepresentative() takes the representative information &
	 * binds the page info to the accountUser object and save the data in the
	 * their respective tables
	 * 
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/planmgmt/issuer/representatives/save", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'EDIT_REPRESENTATIVE')")
	public String newIssuerSaveRepresentative(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser accountUser, 
		@ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, BindingResult result, Model model, RedirectAttributes redirectAttrs, 
			@RequestParam(value = "redirecTo", required = false) String redirecTo, @RequestParam(value = "returnTo", required = false) String returnTo) 
																															throws InvalidUserException, GIException {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		// To save the Issuer Representative, get the Issuer details for assosiation
		try {
			//server side validation
			 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			 Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative, IssuerRepresentative.AddNewIssuerRepresentative.class);
			 if(violations != null && !violations.isEmpty()){
				//throw exception in case client side validation breached
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			 }
		AccountUser user = userService.getLoggedInUser();
		Issuer currUserIssuerObj = issuerService.getIssuer(user);
		model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, currUserIssuerObj);

		if (accountUser.getUserName() == null) {
			accountUser.setUserName(accountUser.getEmail());
		}

		issuerRepresentative.setIssuer(currUserIssuerObj);
		issuerRepresentative.setUserRecord(null); // Until user objected created in users table, IssureRep's user_id will be null
		issuerRepresentative.setFirstName(accountUser.getFirstName());
		issuerRepresentative.setLastName(accountUser.getLastName());
		issuerRepresentative.setTitle(accountUser.getTitle());
		issuerRepresentative.setEmail(accountUser.getEmail());
		issuerRepresentative.setPhone(accountUser.getPhone());
		issuerRepresentative.setUpdatedBy(user);
		issuerRepresentative.setPrimaryContact(IssuerRepresentative.PrimaryContact.NO);
		issuerRepresentative.setRole(ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString());
		saveIssuerRepresentative(accountUser, issuerRepresentative, model);

		// if representative is duplicate, then it should return to the referrer page
		if (model.asMap().containsKey(PlanMgmtConstants.REP_DUPLICATE_EMAIL)) {
			boolean duplicateEmail = false;
			for (Iterator<String> iterator = model.asMap().keySet().iterator(); iterator.hasNext();) {
				String key = iterator.next();
				redirectAttrs.addFlashAttribute(key, model.asMap().get(key));
				if (key.equalsIgnoreCase(PlanMgmtConstants.REP_DUPLICATE_EMAIL)) {
					duplicateEmail = (model.asMap().get(key).toString().equalsIgnoreCase(PlanMgmtConstants.TRUE_STRING)) ? true : false;
				}
			}
			if (duplicateEmail) {
				return "redirect:" + returnTo; // redirect to referrer page
			}
		}
		if (model.asMap().get(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME) != null
				&& !model.asMap().get(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME).toString().isEmpty()
				&& model.asMap().get(PlanMgmtConstants.SESSION_DELEGATION_CODE) != null
				&& !model.asMap().get(PlanMgmtConstants.SESSION_DELEGATION_CODE).toString().isEmpty()) {
			LOGGER.debug("Received Attribute Delegation Code: " +  SecurityUtil.sanitizeForLogging(model.asMap().get(PlanMgmtConstants.SESSION_DELEGATION_CODE).toString())
							+ " Representative Name: " +  SecurityUtil.sanitizeForLogging(model.asMap().get(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME).toString()) );
			
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.SESSION_DELEGATION_CODE, model.asMap().get(PlanMgmtConstants.SESSION_DELEGATION_CODE));
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME, model.asMap().get(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME));

			if (model.asMap().get(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO) != null && !model.asMap().get(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO).toString().isEmpty()) {
				LOGGER.debug("Received Attribute GENERATEACTIVATIONLINKINFO: " +  SecurityUtil.sanitizeForLogging(model.asMap().get(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO).toString()) );
				redirectAttrs.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, model.asMap().get(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO));
				model.asMap().remove(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO);
			}
			// remove the model attributes after setting the RedirectAttributes
			model.asMap().remove(PlanMgmtConstants.SESSION_DELEGATION_CODE);
			model.asMap().remove(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME);
		} else if (model.asMap().get(PlanMgmtConstants.SESSION_ERROR_INFO) != null && !model.asMap().get(PlanMgmtConstants.SESSION_ERROR_INFO).toString().isEmpty()) {
			LOGGER.debug("Received Attribute SESSION_ERROR_INFO:" +  SecurityUtil.sanitizeForLogging(model.asMap().get(PlanMgmtConstants.SESSION_ERROR_INFO).toString()) );
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.SESSION_ERROR_INFO, model.asMap().get(PlanMgmtConstants.SESSION_ERROR_INFO));
			model.asMap().remove(PlanMgmtConstants.SESSION_ERROR_INFO);
		}
		if (redirecTo != null && !redirecTo.isEmpty()) {
			// From manage representative screen, when issuer adds a representative, then redirect the issuer to manage representative screen
			return "redirect:" + (redirecTo.trim().equalsIgnoreCase("manageRep") ? "/planmgmt/managerepresentative" : redirecTo);
		} else {
			for (Iterator<String> iterator = model.asMap().keySet().iterator(); iterator.hasNext();) {
				String key = iterator.next();
				redirectAttrs.addFlashAttribute(key, model.asMap().get(key));
			}
			return "redirect:/planmgmt/issuer/representatives/add";
		}
		} catch (Exception e) {
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME
							+ PlanMgmtErrorCodes.ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}

	}

	
	// Bug Fix :: HIX-4681 Functional : Multiple Primary contacts can be saved
		// for an issuer.
		@RequestMapping(value = "/planmgmt/issuer/representative/primarycontact/exists", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		@ResponseBody
		public String primaryContactExists(Model model, @RequestParam(value = "id", required = false) Integer repId, @RequestParam(value = "primaryContact", required = false) String primaryContact) {
			try {
				String primaryContactExists = "No";
				Integer issuerId = issuerService.getIssuerId();
				List<IssuerRepresentative> repList = issuerService.findIssuerRepresentative(issuerId);
				for (IssuerRepresentative rp : repList) {
					if ((rp.getPrimaryContact() != null)
							&& (repId != rp.getUserRecord().getId() && rp.getPrimaryContact().equals(IssuerRepresentative.PrimaryContact.YES))) {
						primaryContactExists = "Yes";
						break;
					}
				}
				return primaryContactExists;
			} catch (Exception e) {
				LOGGER.error("Exception in primaryContactExists : ", e);
//				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.PRIMARYCONTACT_EXISTS_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
				giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.PRIMARYCONTACT_EXISTS_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			}
		}
		
		
		// controller for view representative details
		@RequestMapping(value = "/planmgmt/viewrepdetails/{encIssuerRepId}")
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		public String showRepDetails(Model model, @PathVariable("encIssuerRepId") String encIssuerRepId, HttpServletRequest viewRepRequest) throws InvalidUserException {
			String repId = null;
			
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try { 
				repId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerRepId);
				String isFromUser = PlanMgmtConstants.USER;
				String userStatus = null, issuerRepStatus = null;
				
				if (repId != null && !repId.isEmpty()) {
					viewRepRequest.setAttribute(PlanMgmtConstants.REP_ID, repId);
					model.addAttribute(PlanMgmtConstants.REP_ID, repId);
					// pick up only the given representative record from the list
					Integer issuerId = issuerService.getIssuerId();
					// fetch issuer rep data from issuer_representative table
					IssuerRepresentative representativeObj = issuerService.findIssuerRepresentativeById(Integer.parseInt(repId));
					AccountUser repUserObject = representativeObj.getUserRecord();
					/*      As per the comment in JIRA HIX-23920 code changes done*/
					if(representativeObj.getUserRecord()!=null){
						isFromUser=PlanMgmtConstants.USER;
						AccountUser user=userService.findById(representativeObj.getUserRecord().getId());
						userStatus = user.getStatus();
						model.addAttribute("USER_OBJ", user);
						model.addAttribute("isFromUser", isFromUser);
						model.addAttribute(PlanMgmtConstants.ISSUER_REP_OBJ, representativeObj);
					}else{
						model.addAttribute(PlanMgmtConstants.ISSUER_REP_OBJ, representativeObj);
					}
					issuerRepStatus = representativeObj.getStatus();
					model.addAttribute("displayStatus", issuerService.getIssuerRepStatus(userStatus, issuerRepStatus));
					model.addAttribute("repUserCreatedOn", (repUserObject.getCreated() != null) ? DateUtil.dateToString(repUserObject.getCreated(), "MMM d, yyyy") : PlanMgmtConstants.EMPTY_STRING);
					model.addAttribute(PlanMgmtConstants.ISSUERID, issuerId);
					if(representativeObj.getIssuer()!=null) {
						model.addAttribute(PlanMgmtConstants.HIOSISSUERID, representativeObj.getIssuer().getHiosIssuerId());
					}
					model.addAttribute(PlanMgmtConstants.REP_USER, repUserObject);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				}
				return "planmgmt/issuer/viewrepdetails";
			} catch (Exception e) {
				LOGGER.error("Exception in showRepDetails : ", e);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SHOWREPDETAILS_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
			}
		}

		
		// controller for edit representative
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/planmgmt/editrepresentative/{encIssuerRepId}")
		@PreAuthorize("hasPermission(#model, 'EDIT_REPRESENTATIVE')")
		public String editRepresentative(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser repUser, @ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, BindingResult result, Model model, 
				@PathVariable("encIssuerRepId") String encIssuerRepId, HttpServletRequest editRepRequest) throws InvalidUserException {
			String repId = null;
			
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				repId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerRepId);
				// START :: HIX-3850 :: Admin Portal - Nominating an Primary Contact for an Issuer.
				IssuerRepresentative issuerRepObj = null;
				Integer issuerId = issuerService.getIssuerId();
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if (StringUtils.isNotBlank(repId)) {
					issuerRepObj = issuerService.findIssuerRepresentativeById(Integer.parseInt(repId));
					if(null != issuerRepObj)
					{
						editRepRequest.setAttribute(PlanMgmtConstants.REP_ID, repId);
						
						// on submission edit representative form save data in DB
						if (editRepRequest.getParameter("editRep") != null && editRepRequest.getParameter("editRep").trim().toString().equalsIgnoreCase("edit")) {
							// START :: HIX-3850 :: Admin Portal - Nominating an Primary Contact for an Issuer.
							String primaryContactStr = editRepRequest.getParameter("primaryContact");
							// END :: HIX-3850
		
							// If state exchange is CA then update location information
							if (PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) {
								Location location = issuerRepObj.getLocation();
								if (issuerRepresentative != null && issuerRepresentative.getLocation() != null) {
									if (location == null) {
										location = new Location();
									}
									location.setAddress1(issuerRepresentative.getLocation().getAddress1());
									location.setAddress2(issuerRepresentative.getLocation().getAddress2());
									location.setCity(issuerRepresentative.getLocation().getCity());
									location.setState(issuerRepresentative.getLocation().getState());
									location.setZip(issuerRepresentative.getLocation().getZip());
		
									// save issue representative address in location table
									Location newlocation = locationService.save(location);
									issuerRepObj.setLocation(newlocation);
								}
							}
		
							if (StringUtils.isNotBlank(primaryContactStr)) {
								// START :: HIX-3850 :: Admin Portal - Nominating an Primary Contact for an Issuer.
								if (primaryContactStr.equalsIgnoreCase(IssuerRepresentative.PrimaryContact.YES.toString())) {
									// set primary contact from NO for other issuer rep expect current one
									String response = issuerService.updatePrimaryContactToNo(issuerId, issuerRepresentative.getId());
									if(LOGGER.isDebugEnabled()){
										LOGGER.debug("response from updateStatusToNo : " +  SecurityUtil.sanitizeForLogging(response));
									}
									issuerRepObj.setPrimaryContact(IssuerRepresentative.PrimaryContact.YES);
								} else {
									issuerRepObj.setPrimaryContact(IssuerRepresentative.PrimaryContact.NO);
								}
							}	
		
							//server side validation
							 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
							 Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative, IssuerRepresentative.AddNewIssuerRepresentative.class);
							 if(violations != null && !violations.isEmpty()){
								//throw exception in case client side validation breached
								 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
							 }
							 
							issuerRepObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
							issuerRepObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
							issuerRepObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
							issuerRepObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
							issuerRepObj.setEmail(editRepRequest.getParameter(PlanMgmtConstants.EMAIL));
							issuerRepObj.setUpdatedBy(userService.getLoggedInUser());
							
							// save issuer representative data
							issuerService.saveRepresentative(issuerRepObj);
							
							// update user data
							AccountUser accountUserObj = userService.findById(issuerRepObj.getUserRecord().getId());
							if(null != accountUserObj)
							{
								accountUserObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
								accountUserObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
								accountUserObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
								accountUserObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
								userService.updateUser(accountUserObj);
							}	
		
							if(LOGGER.isInfoEnabled()){
								LOGGER.info("Updated Primary Contact Info of Representative");
							}	
								
							// END :: HIX-3850
							return "redirect:/planmgmt/managerepresentative";

						} else {
							
							// pull issuer representative data and populate edit representative form
							// populating the primary contact drop down options.
							List<String> primaryContactOptions = new ArrayList<String>();
							primaryContactOptions.add(IssuerRepresentative.PrimaryContact.YES.toString());
							primaryContactOptions.add(IssuerRepresentative.PrimaryContact.NO.toString());
							model.addAttribute("primaryContactList", primaryContactOptions);
							
							model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
							model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
							model.addAttribute(PlanMgmtConstants.ISSUERID, issuerId);
							model.addAttribute(PlanMgmtConstants.REP_ID, repId);
							if(issuerRepObj.getIssuer()!=null){
								model.addAttribute(PlanMgmtConstants.HIOSISSUERID, issuerRepObj.getIssuer().getHiosIssuerId());
							}

							String isFromUser=null;
							String phoneNumber = null;
							if(issuerRepObj.getUserRecord()!=null){
								isFromUser=PlanMgmtConstants.USER;
								model.addAttribute("isFromUser", isFromUser);
								phoneNumber = issuerRepObj.getUserRecord().getPhone();
							}else{
								phoneNumber = issuerRepObj.getPhone();
							}	
							
							model.addAttribute("primaryContact", (issuerRepObj.getPrimaryContact() != null) ? issuerRepObj.getPrimaryContact() : PlanMgmtConstants.EMPTY_STRING);
							AccountUser repUserObject = issuerRepObj.getUserRecord();
							// END :: HIX-3850
							model.addAttribute("repObjForIssuer", issuerRepObj);
							model.addAttribute(PlanMgmtConstants.PHONE1, (null != phoneNumber) ? phoneNumber.substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE) : null);
							model.addAttribute(PlanMgmtConstants.PHONE2, (null != phoneNumber) ? phoneNumber.substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX) : null);
							model.addAttribute(PlanMgmtConstants.PHONE3, (null != phoneNumber) ?  phoneNumber.substring(PlanMgmtConstants.SIX, PlanMgmtConstants.TEN) : null);
							model.addAttribute(PlanMgmtConstants.REP_USER, repUserObject);
							model.addAttribute("repUserCreatedOn", (repUserObject.getCreated() != null) ? DateUtil.dateToString(repUserObject.getCreated(), "MMM d, yyyy") : PlanMgmtConstants.EMPTY_STRING);
						}
					}
				}	
				return "planmgmt/issuer/editrepresentative";
				
			} catch (Exception e) {
				LOGGER.error("Exception in editRepresentative : ", e);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.PRIMARYCONTACT_EXISTS_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			}
		}
		
		
		/**
		 * @author Kuldeep
		 * @param model
		 * @param repId
		 * @param editRepRequest
		 * @return Below controller is written to view Issuer Representative Details
		 */
		@RequestMapping(value = "/planmgmt/viewdetails/{id}")
		@PreAuthorize(PlanMgmtConstants.VIEW_PLAN_DETAILS)
		public String viewRepresentativeDetails(Model model, @PathVariable("id") String repId, HttpServletRequest request) {
			//LOGGER.info("############ Issuer Representative View Detail Page #############");
			
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				IssuerRepresentative issuerRepresentative = issuerService.findIssuerRepresentativeById(Integer.valueOf(repId));
				/**
				 * Condition check is addded to save null pointer exception if
				 * userRecord is null.
				 */
				model.addAttribute("userInfo", issuerRepresentative != null ? issuerRepresentative.getUserRecord()
						: null);
				model.addAttribute("issuerRep", issuerRepresentative);
			} catch (NumberFormatException nfex) {
				LOGGER.error(" ", nfex);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_REPRESENTATIVE_DETAILS_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(nfex), Severity.LOW);
			}
			return "planmgmt/issuer/viewdetails";
		}

		// send user activation link explicitly
		@RequestMapping(value = "/planmgmt/representative/sendActivationlink/{encIssuerRepId}", method = { RequestMethod.GET })
		@PreAuthorize("hasPermission(#model, 'ADD_REPRESENTATIVE')")
		public String sendActivationLink(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser accountUser, Model model, RedirectAttributes redirectAttr, @PathVariable("encIssuerRepId") String encIssuerRepId) {
			
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
				
			String decryptedIssuerRepId = null;
			Integer repId = null;
			try {
				decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerRepId);
				repId = Integer.parseInt(decryptedIssuerRepId);
				IssuerRepresentative issuerRepresentative = iIssuerRepresentativeRepository.findById(repId);
				accountUser.setFirstName(issuerRepresentative.getFirstName());
				accountUser.setLastName(issuerRepresentative.getLastName());
				accountUser.setEmail(issuerRepresentative.getEmail());
				accountUser.setPhone(issuerRepresentative.getPhone());
				
				issuerService.generateActivationLink(repId, accountUser, ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString());
				redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to " + accountUser.getEmail());
				} catch (Exception e) {
					LOGGER.error("Exception in sendActivationLink ", e);
					redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO,"Activation link sending process failed... Please take appropriate actions. -  " 
										+ e.getMessage());
				}

			return "redirect:/planmgmt/managerepresentative";
		}

		@RequestMapping(value = "/planmgmt/managerepresentative", method = {RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize("hasPermission(#model, 'MANAGE_REPRESENTATIVE')")
		public String manageRepresentative(Model model, @RequestParam(value = "action", required = false) String action, @RequestParam(value = PlanMgmtConstants.REP_ID, required = false) String encIssuerRepId, HttpServletRequest manageRepRequest, RedirectAttributes redirectAttrs) throws InvalidUserException {
			
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
			boolean updateRepStatus = false;
			boolean checkforSuspension = false;
			try {
				Integer issuerId = issuerService.getIssuerId();
				String repName = manageRepRequest.getParameter("repname");
				String repTitle = manageRepRequest.getParameter("reptitle");
				String status = manageRepRequest.getParameter(PlanMgmtConstants.STATUS);
				String sortColumn = manageRepRequest.getParameter(PlanMgmtConstants.SORT_BY);
		
				List<Integer> statusList = new ArrayList<Integer>();
				List<Map<String, String>> repArrList = new ArrayList<Map<String, String>>();
		
				String sortOrder = (manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER) == null) ? PlanMgmtConstants.SORT_ORDER_ASC : ((manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
				sortOrder = (sortOrder.equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.SORT_ORDER_ASC : sortOrder;
				
				String changeOrder = (manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER) == null) ? PlanMgmtConstants.FALSE_STRING : ((manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
				sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.ASC : PlanMgmtConstants.SORT_ORDER_DESC) : sortOrder;
				
				manageRepRequest.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
				manageRepRequest.setAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
		
				AccountUser user = userService.getLoggedInUser();
		
				Issuer userIssuerObj = issuerService.getIssuer(user);
		
				Map<String, String> statusMap = new HashMap<String, String>();
				statusMap.put(PlanMgmtConstants.ZERO_STR, RepStatus.INACTIVE.toString());
				statusMap.put(PlanMgmtConstants.ONE_STR, RepStatus.ACTIVE.toString());
				statusMap.put(PlanMgmtConstants.TWO_STR, RepStatus.SUSPENDED.toString());
		
				model.addAttribute(PlanMgmtConstants.ISSUERID, userIssuerObj.getId());
				model.addAttribute(PlanMgmtConstants.HIOSISSUERID, userIssuerObj.getHiosIssuerId());
				model.addAttribute("loggedIdUserId", user.getId());
				
				// if action = add new representative
				if (action != null && action.equalsIgnoreCase("add")) {
					model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
					return "planmgmt/addauthrepresentative";
				} else if (action != null && (action.equalsIgnoreCase(RepStatus.ACTIVE.toString()) || action.equalsIgnoreCase(RepStatus.INACTIVE.toString()))) {
					// set Activate or Suspend flag for issuer representative
					try {
						Integer issureRepId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerRepId));
						if (null != issureRepId && PlanMgmtConstants.ZERO < issureRepId) {
							checkforSuspension = true;
							updateRepStatus = issuerService.updateRepStatus(issureRepId, action);
						}	
					} catch (InvalidUserException e) {
							LOGGER.error("Invalid User : Exception in : " + this.getClass() + ", Exception : " + e.getMessage());
							throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.INVALID_USER_EXCEPTION, e, null, Severity.HIGH);
					}
					
					if(updateRepStatus){
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.EMPTY_STRING);
						return "redirect:/planmgmt/managerepresentative";
					}else{
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.TRUE_STRING);
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR_MSG, "Unable to "+ (RepStatus.ACTIVE.toString().equalsIgnoreCase(action) ? RepStatus.ACTIVE.toString() : "suspend") +" issuer representative. <br> Please check permission and try again.");
						return "redirect:/planmgmt/managerepresentative";
					}
				}
		
				if ((status != null) && !status.isEmpty()) {
					statusList.add(Integer.parseInt(status));
					model.addAttribute("selectedStatus", status);
				} else {
					statusList.add(0);
					statusList.add(1);
				}	
		
				// get issuer representative records without pagination
				repArrList= issuerService.getIssuerRepresentatives(manageRepRequest, issuerId, true, false);
				model.addAttribute(PlanMgmtConstants.RESULT_SIZE, repArrList.size());
				
				// get issuer representative records with pagination
				repArrList = issuerService.getIssuerRepresentatives(manageRepRequest, issuerId, false, false);
				
				model.addAttribute("statusMap", statusMap);
				model.addAttribute("isEmailActivation", isEmailActivation.toString().toUpperCase());
				model.addAttribute("repName", repName);
				model.addAttribute("repTitle", repTitle);
				model.addAttribute("issuerReps", repArrList);
				
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
				model.addAttribute(PlanMgmtConstants.SORT_ORDER_EXCEPTFIRSTCOLUMN, sortOrder);
				model.addAttribute(PlanMgmtConstants.SORT_BY, sortColumn);
				redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.EMPTY_STRING);
				return "planmgmt/managerepresentative";
			} catch (Exception e) {
				if(!updateRepStatus && checkforSuspension){
					redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.TRUE_STRING);
					redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR_MSG, "Unable to "+ (RepStatus.ACTIVE.toString().equalsIgnoreCase(action) ? RepStatus.ACTIVE.toString() : "suspend") +" issuer representative. <br> Please check permission and try again.");
					return "redirect:/planmgmt/managerepresentative";
				}else{
					LOGGER.error("Exception in manageRepresentative ", e);
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_REPRESENTATIVE_EXCEPTION.getCode(),
							null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
				}
			}
		}

			

		/* HIX-16301 :  Refactor-> add new issuer representative for CA
		 * @author Venkata Tadepalli
		*/
		
		private Model saveIssuerRepresentative(AccountUser accountUser, IssuerRepresentative issuerRepObj, Model model) throws GIException {
			try{
				model.addAttribute(PlanMgmtConstants.IS_ISSUER_REP_ADDED, false);
				model.addAttribute(PlanMgmtConstants.IS_ISSUER_REP_DUPLICATE, false);
				model.addAttribute(PlanMgmtConstants.IS_USER_NAME_EXIST, false);
				model.addAttribute(PlanMgmtConstants.REP_DUPLICATE_EMAIL, false);
				boolean duplicateRep = issuerService.isDuplicate(accountUser.getEmail());
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if (duplicateRep) {
					model.addAttribute(PlanMgmtConstants.IS_USER_NAME_EXIST, true);
					model.addAttribute(PlanMgmtConstants.REP_DUPLICATE_EMAIL, true);
					model.addAttribute(PlanMgmtConstants.REP_USER, accountUser);
					
					if (issuerRepObj.getUserRecord()!=null && issuerRepObj.getUserRecord().getPhone() != null && issuerRepObj.getUserRecord().getPhone().length() >= PlanMgmtConstants.TEN) {
						model.addAttribute(PlanMgmtConstants.PHONE1, issuerRepObj.getUserRecord().getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
						model.addAttribute(PlanMgmtConstants.PHONE2, issuerRepObj.getUserRecord().getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
						model.addAttribute(PlanMgmtConstants.PHONE3, issuerRepObj.getUserRecord().getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
					} else if(issuerRepObj.getPhone() != null && issuerRepObj.getPhone().length() >= PlanMgmtConstants.TEN){
						model.addAttribute(PlanMgmtConstants.PHONE1, issuerRepObj.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
						model.addAttribute(PlanMgmtConstants.PHONE2, issuerRepObj.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
						model.addAttribute(PlanMgmtConstants.PHONE3, issuerRepObj.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
					}
					LOGGER.info("Issuer Portal: Adding New Issuer Rep: Username already exists ");
				}else{
					IssuerRepresentative newIssuerRepresentative = issuerService.saveRepresentative(issuerRepObj);
					if (newIssuerRepresentative != null) {
						model.addAttribute(PlanMgmtConstants.IS_ISSUER_REP_ADDED, true);
					} else {
						model.addAttribute(PlanMgmtConstants.IS_ISSUER_REP_DUPLICATE, true);
						model.addAttribute(PlanMgmtConstants.REP_USER, issuerRepObj);
					}
					// If issuer rep created and email acctivation is on and state exchnage other than CA, generate activation link
					if (newIssuerRepresentative != null && isEmailActivation && !stateCode.equalsIgnoreCase("CA") && !stateCode.equalsIgnoreCase("MS")) {
						try {
							issuerService.generateActivationLink(newIssuerRepresentative.getId(), accountUser, ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString());
							model.addAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to " + accountUser.getEmail());
						} catch (Exception e) {
							LOGGER.error(" " + e);
							model.addAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sending process failed... Please take appropriate actions. -  " + e.getMessage());
						}
					}
					// if CA state exchange call delegation servrice
					if (stateCode.equalsIgnoreCase("CA")) { // if state is CA call API for delegation code
						issuerRepresentativeService.updateIssuerDelegationInfo(newIssuerRepresentative, model, issuerRepObj.getIssuer(), accountUser);
					}
				}
			}catch(Exception ex){
				LOGGER.error("Exception occured in saveIssuerRepresentative:", ex);
			}
			return model;
		}

			
}
