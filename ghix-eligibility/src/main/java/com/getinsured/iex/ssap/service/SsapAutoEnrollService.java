package com.getinsured.iex.ssap.service;

import java.util.Map;
import java.util.Set;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.iex.ssap.model.SsapApplication;


public interface SsapAutoEnrollService {
	
	public String autoEnroll(long ssapApplicationId,Map<String, EnrollmentShopDTO> enrollmentIdMap,Map<String,Set<String>> enrollee,Map<String,Set<String>> disenrollMap);

}
