/*template XHTML for address worksite*/
var require = '<span class="aria-hidden">required</span>',
	zipIdFocused ="";
var application = {
	modal_Template:'<div id="modal" class="modal hide fade" tabindex="-1" aria-labelledby="model-inputs" aria-hidden="true">'+
		'<form class="form-horizontal" id="frmworksites" name="frmworksites" action="addWorksites" method="POST">'+
		//'<input type="hidden" name="id" id="id" class="modal_hidden_id" value=""/>'+
		'<div id="form-input-data">'+
		'<div class="modal-header">'+
		'<button type="button" class="removeModal close"><span aria-hidden="true">×</span> <span class="aria-hidden">close</span></button>'+
		'<h3></h3>'+
		'</div>'+
		'<div class="modal-body">'+
		'<div id="modal-inputs">'+
		'<input type="hidden" name="locations[0].primaryLocation" id="primaryLocation" value="NO">'+
		'<input type="hidden" name="locations[0].location.lat_0" id="lat_0" />'+
		'<input type="hidden" name="locations[0].location.lon_0" id="lon_0" />'+
		'<input type="hidden" name="rdi" id="rdi_0" value="" />'+
		'<div class="profile">'+
		'<div class="control-group">'+
		'<label for="address1_0" class="control-label required">Address 1  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>'+
		'<div class="controls">'+
		'<input type="text" name="locations[0].location.address1" id="address1_0"  class="input-xlarge" size="30" data-val="required">'+
		'<div id="address1_0_error"></div>			'+
		'</div>'+
		'</div>'+
		'<div class="control-group">'+
		'<label for="address2_0" class="control-label required">Address 2</label>'+
		'<div class="controls">'+
		'<input type="text"  name="locations[0].location.address2" id="address2_0"  class="input-xlarge" size="30">'+
		'<div id="address2_0_error"></div>			'+
		'</div>'+
		'</div>'+
		'<div class="control-group">'+
		'<label for="city_0" class="control-label required">City  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>'+
		'<div class="controls">'+
		'<input type="text" name="locations[0].location.city" id="city_0" class="input-xlarge" data-val="required">'+
		'<div id="city_0_error"></div>			'+
		'</div>'+
		'</div>'+
		'<div class="control-group">'+
		'<label for="state_0" class="control-label required">State  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>'+
		'<div class="controls">'+
		'<select class="input-medium" name="locations[0].location.state" id="state_0" data-val="required"><option value="">Select</option></select>'+
		'<div id="state_0_error"></div>'+			
		'</div>'+
		'</div>'+
		'<div class="control-group">'+
		'<label for="zip_0" class="control-label  required ">Zip Code  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>'+
		'<div class="controls">'+
		'<input type="text"  name="locations[0].location.zip" id="zip_0"  maxlength="5" class="zipCode input-mini" data-val="required"><br />'+
		'<div id="zip_0_error"></div>			'+
		'</div>'+
		'</div>'+
		'<div class="control-group">'+
		'<label for="county_0" class="control-label required">County  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>'+
		'<div class="controls">'+
		'<select class="input-large" name="locations[0].location.county"  id="county_0" data-val="required">'+
		'<option value="">Select County...</option>'+
		'</select>'+
		'<div id="county_0_error"></div>'+		
		'</div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'</div>'+
		'<div class="modal-footer">'+
		'<div id="input-footer">'+
		'<input type="button" class="btn removeModal" id="cancelBtnModal" value="Cancel">'+
		'<input type="button" name="submitbutton" id="submitbutton" class="btn btn-primary pull-right" value="Add Address" onClick="javascript:validateLocation();"  />'+
		'</div>'+
		'</div>'+
		'</div></form>'+
		'</div>',
		/*init function on click of add worksite link present on the JSP page*/
		init: function(){
			//var empid = $('#employerId').val();
			var modalContent = $(application.modal_Template);
			$('body').append(modalContent);
			/*call the related function on MODAL SHOW*/
			application.input_content_screen_reset(this);
			application.add_Address_function();
			application.populate_state_dropdown();
			
			/*set the EMPLOYEE ID value*/
			//$('#id').val(empid);
		},
		/*remove the modal*/
		remove_modal:function(){
			if($('#frmworksites').is(':visible')){
				$('#modal, .modal-backdrop').remove();
			}else if($('#frmbrokerCertification').is(':visible')){  
				$('#check-address-error, #address-failure-box, #suggestion-box, #addressProgressPopup, .modal-backdrop').hide();
			}else{
				$('#addressProgressPopup, .modal-backdrop').remove();
			}

			//HIX-112526---
			var parentContainerAddress = $('#addressIFrame', window.parent.document);
			var isParentVisibleAddress = $(parentContainerAddress).is(':visible');

			if(isParentVisibleAddress){
                $('.modal-backdrop', window.parent.document).remove();
				parentContainerAddress.remove();
			}
			//HIX-112526---
			//HIX-113201---
			var parentContainerSuggestion =$('#suggestion-box', window.parent.document);
			var isParentVisibleSuggestion = $(parentContainerSuggestion).is(':visible');

			if(isParentVisibleSuggestion){
                $('.modal-backdrop', window.parent.document).remove();
				parentContainerSuggestion.remove();
			}
			// HIX-113201---
		},
		/*return back to the input FORM div after after error from the SAME PAGE*/
		back_button_to_input_screen : function(){
			$('#back_to_input_modal').live('click',function(event){
				if($('#frmworksites').is(':visible')){
					var formTextBoxId = jQuery('#form-input-data').find('input[type="text"]:first').attr('id'),
						zipCodeVal = jQuery('#form-input-data').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('#form-input-data').find('.zipCode').attr('id');
					
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					parent.postPopulateIndex(indexValue[1], zipCodeVal, zipId);
				}else if($('#frmeditemployee').is(':visible')){
					var formTextBoxId = $('#'+event.target.id).attr('data-rel'),
						zipCodeVal = jQuery('#frmeditemployee').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('#frmeditemployee').find('.zipCode').attr('id');
					
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					parent.postPopulateIndex(indexValue[1], zipCodeVal, zipId);
					application.remove_modal();
				}
				else{
					$('#check-address-error, #address-failure-box').hide();
					application.remove_modal();
				}
			});
		},
		/*return back to the input FORM div from IFRAME WINDOW*/
		back_button_from_iframe:function(modalId){
			//console.log("iframe window", modalId);
			
			var formTextBoxId = jQuery('#form-input-data').find('input[type="text"]:first').attr('id');
			//console.log('formTextBoxIdformTextBoxId----',formTextBoxId);

				if($('#frmworksites').is(':visible')){
					//console.log('modal template ifffffff');

					$('#suggestion-box').hide();
					$('#form-input-data').show();
					//$('#'+formTextBoxId).focus();
					
					var zipCodeVal = jQuery('#form-input-data').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('#form-input-data').find('.zipCode').attr('id');
				
					application.populateCounty(zipId, indexValue, zipCodeVal);
					
					
				}else{
					//console.log('modal template else');
					$('#check-address-error, #address-failure-box, #suggestion-box').hide();
					//$('#'+formTextBoxId).focus();
					application.remove_modal();
					
					
					/*var zipCodeVal = jQuery('form').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('form').find('.zipCode').attr('id');*/
					
					var formId = $(parent.document.forms).attr('id'),
						zipCodeVal = jQuery('#'+formId).find('.zipCode').val(),
						parentElement = jQuery('#'+formId).find('.zipCode').parents('.addressBlock'),
						zipId = parentElement.find('.zipCode').attr('id'),
						indexValue = zipId.split('_');
					
					application.populateCounty(zipId, indexValue, zipCodeVal);
					
					/*if($('#'+zipId).val().length >= 5){
						//console.log('index value before',indexValue[1]);
						parent.postPopulateIndex(indexValue[1], zipCodeVal);
						//console.log('index value after',indexValue[1])
					}*/
				}
		},
		modal_cross_button:function(modalId){
			/*console.log('modal cross button', modalId, $(modalId.document), $('.closeModal'));*/
			$('.closeModal').bind('click',function(e){
				application.remove_modal();
				//application.resetCountyOnAllCallback();
			});
		},
		populateCounty: function(zipid, indexValue, zipCodeVal){
			//console.log(zipid, indexValue, zipCodeVal)
			if($('#'+zipid).val().length >= 5){
				//console.log('index value before',indexValue[1]);
				parent.postPopulateIndex(indexValue[1], zipCodeVal, zipid);
				//console.log('index value after',indexValue[1])
			}
		},
		/*SUBMIT FUNCTION inside IFRAME*/
		submit_button_of_iframe:function(buttonId){
			//console.log('submit button with underscore');
			//console.log('modal tmeplate',buttonId)
			var idss = $('#ids').val();
			var retVal=idss.split("~");	
			
			if ($("input:radio:checked").val() == undefined){
				alert('Please select one address.');
				return false;
			}
			/*get the value of CHECKED RADIO*/
				var actualVal=$("input:radio:checked").val().split(","),
				selectedStateFromWorksite = $("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(),
				selectedStateFromZipcode =  $("#modalData", top.document).parents('body').find('#'+retVal[3]).val();			
			
			 var arrUserEnteredAdd = $("#userdefault").val().split(",");
			 var arrUserSelectedAdd = $("input:radio:checked").val().split(",");			 
			 $("#modalData ", top.document).parents('body').find('#'+retVal[4]+'ErrorDiv').hide();
			 if ($.trim(arrUserEnteredAdd[3]).toLowerCase() != $.trim(arrUserSelectedAdd[3]).toLowerCase()){
				 var msg = "Please enter a valid " + arrUserEnteredAdd[3] + " zip code";
				 $("#modalData ", top.document).parents('body').find('#'+retVal[4]+'ErrorDiv').show();
				 $("#modalData", top.document).parents('body').find('#'+retVal[4]+'ErrorSpan').text(msg);				 	
			}
			
			var temp = retVal[0].split('_');

			if(temp[1] === undefined){
				var currIndex = temp;
			}else{
				var currIndex = temp[1];
			}
			
			//console.log(currIndex,'11111');
			if($('#frmworksites').is(':visible')){
				//console.log('am here');
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[0]).val(actualVal[0]);
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[2]).val($.trim(actualVal[2]));
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(selectedStateFromWorksite);
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[4]).val($.trim(actualVal[4]));
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[5]).val(actualVal[5]);
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[6]).val(actualVal[6]);
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[7]).val(actualVal[7] == undefined ? "" : actualVal[7]);
				$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[8]).val(actualVal[8] == undefined ? "" : actualVal[8]);
				//console.log('modal '+currIndex+' '+actualVal[4]);
				parent.getCountyList(currIndex,$.trim(actualVal[4]), $.trim(actualVal[8]));
				
			}else if($('#editdependent').is(':visible')){
				//console.log('edit dependent visible')
			}
			else{
				//console.log('out of here');
				
				$("#modalData", top.document).parents('body').find('#'+retVal[0]).val(actualVal[0]);
				$("#modalData", top.document).parents('body').find('#'+retVal[2]).val($.trim(actualVal[2]));
				$("#modalData", top.document).parents('body').find('#'+retVal[3]).val(selectedStateFromZipcode);
				$("#modalData", top.document).parents('body').find('#'+retVal[4]).val($.trim(actualVal[4]));
				$("#modalData", top.document).parents('body').find('#'+retVal[5]).val(actualVal[5]);
				$("#modalData", top.document).parents('body').find('#'+retVal[6]).val(actualVal[6]);
				$("#modalData", top.document).parents('body').find('#'+retVal[7]).val(actualVal[7] == undefined ? "" : actualVal[7]);
				$("#modalData", top.document).parents('body').find('#'+retVal[8]).val(actualVal[8] == undefined ? "" : actualVal[8]);
				//console.log(currIndex+' '+actualVal[4]+'----'+actualVal[8]);
				
				/*worksiteZipid = $("#modalData", top.document).parents('body').find('#'+retVal[4]).attr('id');
				application.populateCountyWorksite(currIndex, actualVal[4], actualVal[8], worksiteZipid);*/
				
				//parent.getCountyList($.trim(actualVal[4]), "");
			}
			
			
			//console.log($("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(),'bbbb');
			/*trigger the BACK button*/
			$('#'+buttonId).parent().find("#back_button_from_iframe").trigger('click');
			
			//console.log($("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(),'ccc');
			
			
		},
		/*populateCountyWorksite:function(eIndex, zipCode, county, worksiteZipid){
					
			var parentFormId = $(parent.document.forms[0]).attr('id'),
				//zipCodeVal = jQuery('#'+parentFormId).find('.zipCode').val(),
				parentElement = $('#'+worksiteZipid).parents('.addressBlock').find('.zipCode').attr('id'),
				indexValue = parentElement.split('_');
			
			application.populateCounty(zipId);
		},*/
		/*FOCUS OUT function on ZIP*/
		zipcode_focusout: function(e, that){
			var startindex = (e.target.id).indexOf("_");
			if(startindex > 0){
				var index = (e.target.id).substring(startindex,(e.target.id).length);
			}else{
				var index = "";
			}
			var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
			var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
			address1_e=address1_e+index;
			address2_e=address2_e+index;
			city_e=city_e+index;
			state_e=state_e+index;
			zip_e=zip_e+index;
			lat_e=lat_e+index;
			lon_e=lon_e+index;
			rdi_e+=index;
			county_e+=index;
			var model_address1 = address1_e + '_hidden' ;
			var model_address2 = address2_e + '_hidden' ;
			var model_city = city_e + '_hidden' ;
			var model_state = state_e + '_hidden' ;
			var model_zip = zip_e + '_hidden' ;

			//console.log('zip_e',zip_e);
			if(typeof getCountyList === 'function'){
				
				var countyKey = '';
				if($('#'+ county_e).val() != ''){
					countyKey= ($('#'+ county_e).val()).substring(0,($('#'+ county_e).val()).indexOf("#"));
				}
				getCountyList(index, $('#'+ zip_e).val(), countyKey);
			}
						
			var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
			if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
				if(($('#'+ address2_e).val())==="Address Line 2"){
					$('#'+ address2_e).val('');
				}	
				
				viewValidAddressListNew($('#'+ address1_e).val().replace(/,/g,' '),$('#'+ address2_e).val().replace(/,/g,' '),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);
				
				//console.log(e,'eeeeee', that)
				zipIdFocused = e;
				//console.log(zipIdFocused,'1111');
				zipIdFocused = that;
				//console.log(zipIdFocused,'222222');
			}
		},
		/*RESET the content in the FORM*/
		input_content_screen_reset: function(that){
        	$('.errorAddress, .errorAddressBtn, .suggestedAddress').remove();
        	$('#modal').find('.modal-header h3').text('Additional Worksite');
        	$('#modal-inputs,#input-footer').show();
        },
        /*VALIDATE the FORM elements*/
        add_Address_function:function(){
        	$("#frmworksites").validate({
                errorClass: "error",
                errorPlacement: function(error, element) {
            		var elementId = element.attr('id');
            		error.appendTo( $("#" + elementId + "_error"));
            		$("#" + elementId + "_error").attr('class','error help-inline');
                },
                rules: {
                	'locations[0].location.address1': { 
                        required: true
                    },
                    'locations[0].location.city': {
                        required: true
                    },
                    'locations[0].location.state': {
                        required: true
                    },
                    'locations[0].location.zip': {
                        required: true,
                        zipcheck : true,
                    },
                    'locations[0].location.county': {
                        required: true
                    }
                }
            });
			
            /*error message for address1*/
            $('input[name="locations[0].location.address1"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter address.</span>"
                }
            });
            /*error message for city*/
            $('input[name="locations[0].location.city"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter city.</span>"
                }
            });
            /*error message for state*/
            $('*[name="locations[0].location.state"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter state.</span>"
                }
            });
            
            /*error message for zip*/
            $('input[name="locations[0].location.zip"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter zip code.</span>",
                    zipcheck: "<span><em class='excl'>!</em> Please enter a valid zip code.</span>"
                }
            });
            
            /*error message for county*/
            $('*[name="locations[0].location.county"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter a county.</span>"
                }
            });
            
            /*check the value of ZIP*/
            jQuery.validator.addMethod("zipcheck", function(value, element, param) {
              	elementId = element.id; 
              	var numzip = new Number($("#"+elementId).val());
                var filter = /^[0-9]+$/;
                if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
              		return false;	
              	}return true;
            });
        },
        /*populate STATE DROPDOWN inside MODAL on show*/
        populate_state_dropdown:function(){
        	var stateObj = $('#jsonStateValue').val(),
        		stateJson = $.parseJSON(stateObj);
        	
        	var defaultStateValue = $('#defaultStateValue').val();
        	$.each(stateJson, function(index, item){
        		if(item.code === defaultStateValue){
        			$('<option selected="selected" value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
        		}else{
        			$('<option value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
        		}
        	});
        },
        /*RESIZE the IFRAME HEIGHT*/
        iframeHeight:function(){
        	/*console.log('modal zipcode',$("#modalData", $(window).height()).height(),$('#suggestion-box'));*/
        	$('#modalData').load(function(){
				var iFrameHeight = document.getElementById('modalData').contentDocument.body.scrollHeight + "px";
				/*console.log('iFrameHeight',iFrameHeight)*/
				$('#modalData').height(iFrameHeight);
				$('#modalData').width('100%');
			});
        },
        /*populate LOCATION on PARENT PAGE*/
        populate_location_on_parent_page: function(empDetailsJSON, contactlocationid, locationval){
			if($.browser.msie && $.browser.version <= 8){
				for(var i=0;i<empDetailsJSON.length;i++){
					var obj = empDetailsJSON[i];
					primarylocationid = obj['id'];
				var selected = (obj['id'] == contactlocationid) ? contactlocationid : primarylocationid,
					selectLocationId = ((locationval != ''  && locationval != undefined) || locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
				$('#location').prepend('<option value="'+selectLocationId+'" ' +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
				$('#location').val(selected);
				}
			}else{
				for(var i=0;i<empDetailsJSON.length;i++){
					var obj = empDetailsJSON[i];
					var selected = '';
					var selectedId = (locationval != undefined && locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
					if(contactlocationid == 0){
						if(obj['primary'] == 'YES'){
							selected = 'selected="selected"';
						}
					}else if(selectedId == contactlocationid){
						selected = 'selected="selected"';
					}
					var selectLocationId = (locationval != undefined && locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
                    $('#location').prepend('<option value="'+selectLocationId+'" '+ selected +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
				} 
			}
        },
        /*ADDRESS Failure Message*/
        address_failure_message:function(message){        	
        	$('#address-failure-box').remove();
        	var addresFailureMessage = '<div id="address-failure-box" class="modal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-header clearfix"><h3 class="pull-left">Confirm Address</h3>'+
    			'<button type="button" class="close closeModal">&times;</button></div>'+
				'<div class="modal-body"><div class="suggestedAddress">'+message+'</div></div>'+
				'<div class="modal-footer"><input type="button" class="btn errorAddressBtn" data-rel="withoutTemplate_Home" id="back_to_input_modal" value="OK" /></div></div><div class="modal-backdrop fade in"></div>';
        	if($('#frmworksites').is(':visible')){
				$('#frmworksites').append(addresFailureMessage);
				$('#form-input-data').hide();
        	}else{
				$('body').append(addresFailureMessage);
				$('#address-failure-box').addClass('modal popup-address');
			}
        	
        	application.back_button_to_input_screen();
        },
        clearDataFromModal:function(){
        	$('#id').val('0');
        },
        match_found_proper_address:function(){
			/*console.log(zipIdFocused,'1111');*/
			var getZipIndex = (zipIdFocused).attr('id').split("_"),
				getZipIndexValue = getZipIndex[1];
			
			if(getZipIndexValue != ""){
				$('#county_'+getZipIndexValue).focus();
			}else{
				$('#county').focus();
			}
		}
};
/*CLICK event of worksite link on the JSP page*/
$('#worksite_ModalLink').live('click',function(){
	application.init();
	$('#modal').modal({
		show: true,
		backdrop: 'static',
		keyboard: false
		
	});
});
/*FOCUS OUT event of ZIP CODE*/
$('body').on('focusout','.zipCode', function(e){
	if(!e.target.hasAttribute('disabled') && !e.target.hasAttribute('readonly')){
		application.zipcode_focusout(e, $(this));
	}
});

/*Clear Data From Modal*/
$('.removeModal').live('click',function(){
	application.clearDataFromModal();
	application.remove_modal();
});
