package com.getinsured.ssap.model.hub.vlp.response;

/**
 * Invalid data types ({@code IAVTypeCode}).
 *
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
public enum InvalidDataTypeCode
{
  /**
   * Birth Date mismatch.
   */
  BDMM("Birthday Mismatch"),

  /**
   * Name mismatch.
   */
  NMM("Name Mismatch"),

  /**
   * Record not found.
   */
  RNF("Record Not Found");

  String text;

  InvalidDataTypeCode(String text) {
    this.text = text;
  }
}
