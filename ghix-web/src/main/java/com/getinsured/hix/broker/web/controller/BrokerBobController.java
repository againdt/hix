package com.getinsured.hix.broker.web.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.broker.enums.AEEBookOfBusinessEnums;
import com.getinsured.hix.broker.service.BrokerBOBService;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.broker.HouseholdEligibilityInformationDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
public class BrokerBobController {
	private static final String AGENCY_MANAGER = "agency_manager";

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerBobController.class);

	private static final String DESIG_STATUS = "desigStatus";
	public static final Integer PAGE_SIZE = 10;
	private static final String PAGE_SIZE_STRING = "pageSize";
	private static final String PAGE_TITLE = "page_title";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String SWITCH_TO_MODULE_NAME = "switchToModuleName";
	private static final String ACCESS_IS_DENIED = "Access is denied.";
	private static final String SORT_BY = "sortBy";
	public static final String HOUSEHOLD_COUNT= "count";
	private static final String DUE_DATE_ASC = "DUE_DATE_ASC";
	private static final String FIRST_NAME_ASC = "FIRST_NAME_ASC";
	private static final String PARAMETERS_SET = "parametersSet";
	private static final String NA = "N/A";
	private static final String INDIVIDUAL_IDS = "individualsId";
	private static final String ELIGIBILITY = "Eligibility";
	private static final String HOUSEHOLD = "Household";
	private static final String NOTIFICATION_TEMPLATE_NAME = "IndividualDedesignationTemplateEnrollmentCounselor";
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String ENROLLMENTENTITY_NAME = "EntityName";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	public static final String REASON_FOR_DE_DESIGNATION = "ReasonForDeDesignation";
	public static final String INDIVIDUAL ="individual";
	public static final String ACCEPTED_NOTICE_CLASS_NAME ="IndividualEntityOrCounselorDesignationAcceptedNotice";
	public static final String DECLINED_NOTICE_CLASS_NAME ="IndividualEntityOrCounselorDesignationDeclinedNotice";
	public static final String DATE_FORMAT_PATTERN = "MMMMMMMMM dd, yyyy";
	private static final String ASSISTER_NAME = "counselorName";
	private static final String ENCRYPTED_RECORD_ID = "encryptedRecordId";
	public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	public static final String HOUSEHOLD_MEMBER = "householdMember";
	public static final String TAX_HOUSEHOLD= "taxHousehold";
	public static final String DATEOFBIRTH = "dateOfBirth";
	public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
	public static final String PERSONID = "personId";
	
	@Autowired private Gson platformGson;
	@Autowired
	private UserService userService;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private BrokerBOBService brokerBOBService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private EERestCallInvoker restClassCommunicator;
	@Autowired
	private BrokerIndTriggerService brokerIndTriggerService;
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired
	private DesignateService designateService;
	@Autowired
	private EntityUtils entityUtils;

	@RequestMapping(value = "/broker/bookofbusiness", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	public String getIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) throws GIRuntimeException{
		try {
			String fromModule = ghixJasyptEncrytorUtil.encryptStringByJasypt(ModuleUserService.INDIVIDUAL_MODULE);
			model.addAttribute("encFromModule", fromModule);
			setModelAttributes(model, request);
			return "broker/activeindividuals";
		} catch(Exception exception){
			LOGGER.error("Exception occured while loading broker book of business page : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	@RequestMapping(value = "/broker/loadIssuers", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public String loadIssuers(Model model,HttpServletRequest request) throws GIRuntimeException {

		AccountUser user;
		BrokerBobResponseDTO brokerBobResponseDTO;
		try {
			user = userService.getLoggedInUser();

			brokerBobResponseDTO = brokerBOBService.loadIssuerDetails(user.getUserName());

		}catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while retrieving broker book of business : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving broker book of business : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return platformGson.toJson(brokerBobResponseDTO);

	}

	@RequestMapping(value = "/broker/HouseholdEligibilityInformation", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public String loadHouseholdEligibilityInformation(@RequestBody BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws GIRuntimeException {

		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
		
		try {
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
				brokerBOBSearchParamsDTO.setRecordId(Long.valueOf(brokerBOBDetailsDTO.getIndividualId()));
				brokerBOBSearchParamsDTO.setInformationType(ELIGIBILITY);
				brokerBOBSearchParamsDTO.setApplicationYear(brokerBOBDetailsDTO.getApplicationYear());
				BrokerBobResponseDTO response = brokerIndTriggerService.triggerInd73(brokerBOBSearchParamsDTO);
				hhEligibilityInfoDTO = response.getHouseholdEligibilityInformationDTO();
			} else {
				hhEligibilityInfoDTO = (HouseholdEligibilityInformationDTO) brokerBOBService.getHouseholdEligibilityInformation(brokerBOBDetailsDTO);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while retrieving Household Eligibility Information : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return platformGson.toJson(hhEligibilityInfoDTO);
	}
	
	@RequestMapping(value = "/agency/HouseholdEligibilityInformation", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgencyHouseholdEligibilityInformation(@RequestBody BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws GIRuntimeException {

		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
		
		try {
			BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
			brokerBOBSearchParamsDTO.setRecordId(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(brokerBOBDetailsDTO.getEncryptedIndividualId())));
			brokerBOBSearchParamsDTO.setInformationType(ELIGIBILITY);
			brokerBOBSearchParamsDTO.setApplicationYear(brokerBOBDetailsDTO.getApplicationYear());
			BrokerBobResponseDTO response = brokerIndTriggerService.triggerInd73(brokerBOBSearchParamsDTO);
			hhEligibilityInfoDTO = response.getHouseholdEligibilityInformationDTO();
		} catch (Exception e) {
			LOGGER.error("Exception occured while retrieving Household Eligibility Information : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return platformGson.toJson(hhEligibilityInfoDTO);
	}


	@RequestMapping(value = "/broker/HouseholdMemberInformation", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public String loadHouseholdMemberInformation(@RequestBody BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws GIRuntimeException {
		
		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
		
		try {
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
				brokerBOBSearchParamsDTO.setRecordId(Long.valueOf(brokerBOBDetailsDTO.getIndividualId()));
				brokerBOBSearchParamsDTO.setInformationType(HOUSEHOLD);
				brokerBOBSearchParamsDTO.setApplicationYear(brokerBOBDetailsDTO.getApplicationYear());
				BrokerBobResponseDTO response = brokerIndTriggerService.triggerInd73(brokerBOBSearchParamsDTO);
				hhEligibilityInfoDTO = response.getHouseholdEligibilityInformationDTO();
			} else {
				BrokerBOBDetailsDTO requestDTO = new BrokerBOBDetailsDTO();
				requestDTO.setSsapApplicationId(brokerBOBDetailsDTO.getSsapApplicationId());
				requestDTO.setIndividualId(brokerBOBDetailsDTO.getIndividualId());
				requestDTO.setFirstName(brokerBOBDetailsDTO.getFirstName());
				requestDTO.setLastName(brokerBOBDetailsDTO.getLastName());

				if(brokerBOBDetailsDTO != null && brokerBOBDetailsDTO.getSsapApplicationId() != 0)
				{
					SsapApplication ssapApplication = brokerBOBService.getSsapApplication(new Long(brokerBOBDetailsDTO.getSsapApplicationId()));
					requestDTO.setApplicationData(ssapApplication.getApplicationData());
					hhEligibilityInfoDTO = (HouseholdEligibilityInformationDTO) brokerBOBService.getHouseholdMemberInformation(requestDTO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while retrieving Household Member Information : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return platformGson.toJson(hhEligibilityInfoDTO);
	}
	
	@RequestMapping(value = "/agency/HouseholdMemberInformation", method = RequestMethod.POST)
	@ResponseBody
	public String loadAgencyHouseholdMemberInformation(@RequestBody BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws GIRuntimeException {
		
		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
		
		try {
			BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
			brokerBOBSearchParamsDTO.setRecordId(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(brokerBOBDetailsDTO.getEncryptedIndividualId())));
			brokerBOBSearchParamsDTO.setInformationType(HOUSEHOLD);
			brokerBOBSearchParamsDTO.setApplicationYear(brokerBOBDetailsDTO.getApplicationYear());
			BrokerBobResponseDTO response = brokerIndTriggerService.triggerInd73(brokerBOBSearchParamsDTO);
			hhEligibilityInfoDTO = response.getHouseholdEligibilityInformationDTO();
		} catch (Exception e) {
			LOGGER.error("Exception occured while retrieving Household Member Information : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return platformGson.toJson(hhEligibilityInfoDTO);
	}

	@RequestMapping(value = "/broker/bookofbusinessforpagination", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public String loadIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) throws GIRuntimeException{

		LOGGER.info("Fetch Agent BOB Details : START");

		Integer pageSize = PAGE_SIZE;
		Integer startRecord = 0;
		
		BrokerBOBDetailsDTO requestDTO = new BrokerBOBDetailsDTO();
		String pageNumber = request.getParameter(PAGE_NUMBER);
		BrokerBobResponseDTO brokerBobResponseDTO = null;
		BrokerBOBSearchParamsDTO bobSearchParamsDTO;
		int currentPage = 1;

		if (pageNumber != null) {
			startRecord = (Integer.parseInt(pageNumber) - 1) * pageSize;
			currentPage = Integer.parseInt(pageNumber);
		}

		try {
			desigStatus = XssHelper.stripXSS(desigStatus);
			AccountUser user = userService.getLoggedInUser();

			String switchToModuleName = (String) request.getSession().getAttribute(SWITCH_TO_MODULE_NAME);
			String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);

			if(user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ROLE) || user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ADMIN_ROLE) || user.getActiveModuleName().equalsIgnoreCase(AGENCY_MANAGER) ){
				Broker brokerObj = new Broker();
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					if(isUserSwitchToOtherView != null && switchToModuleName!=null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)
							&& BrokerConstants.BROKER.equalsIgnoreCase(switchToModuleName) && user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ADMIN_ROLE)){
						brokerObj = brokerService.getBrokerDetail(user.getActiveModuleId());
					} else{
						brokerObj = brokerService.findBrokerByUserId(user.getId());
					}
				} else {
					if(isUserSwitchToOtherView != null && switchToModuleName!=null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)
							&& BrokerConstants.BROKER.equalsIgnoreCase(switchToModuleName)){
						brokerObj = brokerService.getBrokerDetail(user.getActiveModuleId());
					} else{
						brokerObj = brokerService.findBrokerByUserId(user.getId());
					}
				}
				
				if(brokerObj!=null) {
					if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(brokerObj)){
						throw new AccessDeniedException(ACCESS_IS_DENIED);
					}
					if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE) && user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ROLE) && BrokerMgmtUtils.isBrokerStatusInActive(brokerObj)){
						throw new AccessDeniedException(ACCESS_IS_DENIED);
					}
					requestDTO.setBrokerId(brokerObj.getId());
				}
			} else if(user.getActiveModuleName().equalsIgnoreCase(RoleService.ASSISTER_ROLE)){
				Assister assister = new Assister();
				if(isUserSwitchToOtherView != null && switchToModuleName!=null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)
						&& BrokerConstants.ASSISTER.equalsIgnoreCase(switchToModuleName)){
					assister = retrieveAssisterById(user.getActiveModuleId());
				} else{
					assister = retrieveAssisterObjectByUserId(user.getId());
				}

				if(assister!=null){
					if(EntityUtils.isAssisterCertified(assister)) {
						throw new AccessDeniedException(ACCESS_IS_DENIED);
					}
					requestDTO.setBrokerId(assister.getId());
				}
			}
			
			bobSearchParamsDTO = getFilterParameters(HtmlUtils.htmlUnescape(request.getParameter("filters")));
			
			if(requestDTO.getBrokerId()!=0){
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					String sortBy =  (request.getParameter(SORT_BY) == null) ? FIRST_NAME_ASC : request.getParameter(SORT_BY);
					bobSearchParamsDTO = setRequestParameters(request, startRecord, sortBy, bobSearchParamsDTO);
					bobSearchParamsDTO.setDesignationStatus(desigStatus);
					brokerBobResponseDTO = setPlanLogo(processIND72(bobSearchParamsDTO, user, currentPage, model, request, requestDTO), user);
				} else {
					String sortBy =  (request.getParameter(SORT_BY) == null) ? DUE_DATE_ASC : request.getParameter(SORT_BY);
					requestDTO.setDesignationStatus(desigStatus);
					requestDTO.setUserName(user.getUserName());
					bobSearchParamsDTO = setRequestParameters(request, startRecord, sortBy, bobSearchParamsDTO);
					brokerBobResponseDTO = brokerBOBService.getBrokerBookOfBusiness(requestDTO, bobSearchParamsDTO, false);
				}
			}
			
			model.addAttribute(PARAMETERS_SET, platformGson.toJson(bobSearchParamsDTO));
			request.setAttribute(PAGE_SIZE_STRING, pageSize);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals");
			model.addAttribute(DESIG_STATUS, desigStatus);
			
		} catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while retrieving broker book of business : ", accessDeniedException);
			throw accessDeniedException;
		} catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while retrieving broker book of business : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving broker book of business : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("Fetch Agent BOB Details : END");

		return platformGson.toJson(brokerBobResponseDTO);
	}
	
	@RequestMapping(value = "/broker/exportcsv", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public void exportBOBAsExcel(HttpServletRequest request, HttpServletResponse response, Model model) {
		LOGGER.info("exportBOBAsExcel: START ");
		final char CHAR_$ = '$', CHAR_NEW_LINE = '\n', CHAR_COMMA = ',', CHAR_HYPHEN = '-';
		Broker broker;
		StringBuffer strWriter;
		BrokerBOBDetailsDTO requestDTO = new BrokerBOBDetailsDTO();
		BrokerBobResponseDTO brokerBobResponseDTO;

		try {
			AccountUser user = userService.getLoggedInUser();
			Validate.notNull(user);

			String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);

			if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)){
				broker = brokerService.getBrokerDetail(user.getActiveModuleId());
			}
			else{
				broker = brokerService.findBrokerByUserId(user.getId());
			}

			Validate.notNull(broker);

			requestDTO.setBrokerId(broker.getId());
			requestDTO.setDesignationStatus(DesignateBroker.Status.Active.toString());
			requestDTO.setUserName(user.getUserName());

			BrokerBOBSearchParamsDTO bobSearchParamsDTO = new BrokerBOBSearchParamsDTO();
			bobSearchParamsDTO.setStartRecord(0);

			brokerBobResponseDTO = brokerBOBService.getBrokerBookOfBusiness(requestDTO, bobSearchParamsDTO, true);
			if (null == brokerBobResponseDTO) {
				strWriter = new StringBuffer("No individuals found in Agent's Book of Business.");
				strWriter.trimToSize();
				response.setContentType("text/plain");

				response.setContentLength(strWriter.toString().length());
			}
			else {
				StringBuffer address = null;
				//brokerBOBService.processHouseholdStatus(brokerBobResponseDTO.getBrokerBobDetailsDTO());
				//brokerBOBService.loadPlanDetails(brokerBobResponseDTO.getBrokerBobDetailsDTO(), user.getUsername());
				strWriter = new StringBuffer();
				strWriter.append("Number,First Name,Last Name,Phone Number,Email,Address,Application Date,Application Type,Current Status,Next Steps,Due Date,Carrier Name,Plan Name,Premium (monthly),APTC (monthly),Individual Contribution (monthly),Office Visits,Generic Drugs,Deductible,Application Year");
				strWriter.append(CHAR_NEW_LINE);

				List<BrokerBOBDetailsDTO> brokerBobDetailsDTOs = brokerBobResponseDTO.getBrokerBobDetailsDTO();

				Validate.notNull(brokerBobDetailsDTOs);
				Validate.notEmpty(brokerBobDetailsDTOs);

				for(BrokerBOBDetailsDTO brokerBobDetailsDTO: brokerBobDetailsDTOs) {
					if(null != brokerBobDetailsDTO) {
						if(brokerBobDetailsDTO.getIndividualId() == 105641 || brokerBobDetailsDTO.getIndividualId() == 35579 || brokerBobDetailsDTO.getIndividualId() == 58226){
							LOGGER.warn("loadPlanDetails indId: "+brokerBobDetailsDTO.getIndividualId());
							LOGGER.warn("loadPlanDetails plId: "+brokerBobDetailsDTO.getPlanId());
							LOGGER.warn("loadPlanDetails spd: "+brokerBobDetailsDTO.getSsapApplicationId());
						}
						strWriter.append(brokerBobDetailsDTO.getIndividualId()).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getFirstName())).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getLastName())).append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getPhoneNumber())) {
							strWriter.append(brokerBobDetailsDTO.getPhoneNumber().substring(0, 3)).append(CHAR_HYPHEN);
							strWriter.append(brokerBobDetailsDTO.getPhoneNumber().substring(3, 6)).append(CHAR_HYPHEN);
							strWriter.append(brokerBobDetailsDTO.getPhoneNumber().substring(6, 10));
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getEmailAddress())) {
							strWriter.append(brokerBobDetailsDTO.getEmailAddress());
						}
						strWriter.append(CHAR_COMMA);

						address = new StringBuffer();

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getAddress1())) {
							address.append(brokerBobDetailsDTO.getAddress1());
						}

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getAddress2())) {
							address.append((", ")).append(brokerBobDetailsDTO.getAddress2());
						}

						final String STR_SPACE = " ";
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getCity())) {
							address.append(STR_SPACE).append(brokerBobDetailsDTO.getCity());
						}

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getState())) {
							address.append(STR_SPACE).append(brokerBobDetailsDTO.getState());
						}

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getZipCode())) {
							address.append(STR_SPACE).append(brokerBobDetailsDTO.getZipCode());
						}

						strWriter.append(StringEscapeUtils.escapeCsv(address.toString()));
						strWriter.append(CHAR_COMMA);
						
						if(null != brokerBobDetailsDTO.getApplicationDate()) {
					       strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getApplicationDate()+""));
					    }else{
					    	strWriter.append(NA);
					    }
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getApplicationType())) {
							strWriter.append(brokerBobDetailsDTO.getApplicationType());
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getCurrentStatus())) {
							strWriter.append(brokerBobDetailsDTO.getCurrentStatus());
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getNextStep())) {
							strWriter.append(brokerBobDetailsDTO.getNextStep());
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						String strDate = brokerBobDetailsDTO.getDueDate();

						if(StringUtils.isNotBlank(strDate)) {
							strWriter.append(StringEscapeUtils.escapeCsv(strDate.trim()));
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						/** Carrier Name */
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getIssuerName())) {
							strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getIssuerName()));
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getPlanName())) {
							strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getPlanName()));
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);
						
						/** Premium Amount */
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getGrossPremiumAmt())) {
							BigDecimal rawGrossPremiumAmt = new BigDecimal(brokerBobDetailsDTO.getGrossPremiumAmt());
							rawGrossPremiumAmt = rawGrossPremiumAmt.setScale(2, RoundingMode.HALF_UP);

							if(null != rawGrossPremiumAmt) {
								strWriter.append(CHAR_$).append(rawGrossPremiumAmt.doubleValue());
							}
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);
						
						/** APTC Amount */
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getAptcAmount()) && !"null".equalsIgnoreCase(brokerBobDetailsDTO.getAptcAmount())) {
							BigDecimal rawAptcAmt = new BigDecimal(brokerBobDetailsDTO.getAptcAmount());
							rawAptcAmt = rawAptcAmt.setScale(2, RoundingMode.HALF_UP);

							if(null != rawAptcAmt) {
								strWriter.append(CHAR_$).append(rawAptcAmt.doubleValue());
							}
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						/** Individual Contribution */
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getPremium())) {
							BigDecimal rawPremium = new BigDecimal(brokerBobDetailsDTO.getPremium());
							rawPremium = rawPremium.setScale(2, RoundingMode.HALF_UP);

							if(null != rawPremium) {
								strWriter.append(CHAR_$).append(rawPremium.doubleValue());
							}
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);
						
						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getOfficeVisit())) {
							strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getOfficeVisit()));
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getGenericDrugs())) {
							strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getGenericDrugs()));
						}else{
							strWriter.append(NA);
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerBobDetailsDTO.getDeductible())) {
							strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getDeductible()));
						}else{
							strWriter.append(NA);
						}
						
					   strWriter.append(CHAR_COMMA);
				       if(null != brokerBobDetailsDTO.getCoverageYear()) {
				    	   strWriter.append(StringEscapeUtils.escapeCsv(brokerBobDetailsDTO.getCoverageYear()+""));
				       }else{
							strWriter.append(NA);
				       }
						strWriter.append(CHAR_NEW_LINE);
					}
				}

				strWriter.trimToSize();

				response.setContentType("text/csv");

				response.addHeader("Content-Disposition", "attachment; filename=Agent_Book_Of_Business.csv");
			}

			FileCopyUtils.copy(strWriter.toString().getBytes(), response.getOutputStream());
		}
		catch(GIRuntimeException runtimeException) {
			LOGGER.error("Runtime exception occurred while exporting BOB as Excel", runtimeException);
			throw runtimeException;
		}
		catch(Exception exception) {
			LOGGER.error("Exception occurred while exporting BOB as Excel", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("exportBOBAsExcel: END ");
		}
	}
	
	/**
	 *
	 * @param encIndividualId
	 * @param request
	 * @return
	 * @throws GIRuntimeException
	 */
	@GiAudit(transactionName = "Decline", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.DEDESIGNATE_AGENT)
	@RequestMapping(value = "/broker/decline/individual/{encIndividualId}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String declineActiveIndividual(@PathVariable("encIndividualId") String encIndividualId,HttpServletRequest request) throws GIRuntimeException{
		LOGGER.info("decline : START");
		AccountUser user;
		String individualId;

		try{
			user = userService.getLoggedInUser();
			individualId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIndividualId);
			
			if(user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ROLE) || user.getActiveModuleName().equalsIgnoreCase(AGENCY_MANAGER)   ){
				declineBrokerActiveIndividual(individualId, user, request);
			} else if(user.getActiveModuleName().equalsIgnoreCase(RoleService.ASSISTER_ROLE)){
				declineAssisterActiveIndividual(individualId, user, request);
			}
		}catch(Exception e){
			LOGGER.error("Exception occured while handling the decline designation request for Individual:", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "ok";
	}
	
	private void setModelAttributes(Model model, HttpServletRequest request) throws InvalidUserException{
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			AccountUser user = userService.getLoggedInUser();
			
			model.addAttribute("applicationTypeList", getLookupValues(lookupService.getLookupValueList("APPLICATION_TYPE")));
			model.addAttribute("currentStatusList", getLookupValues(lookupService.getLookupValueList("CURRENT_STATUS")));
			model.addAttribute("nextStepList", getLookupValues(lookupService.getLookupValueList("NEXT_STEPS")));
			model.addAttribute("coverageYearList", getLookupValues(lookupService.getLookupValueList("COVERAGE_YEAR")));
			model.addAttribute("enrollmentStatuslist", getLookupValues(lookupService.getLookupValueList("ENROLLMENT_STATUS_TYPE")));
			
			String role = user.getActiveModuleName();
			
			if(role.equalsIgnoreCase(BrokerConstants.BROKER)){
				model.addAttribute(ENCRYPTED_RECORD_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(user.getActiveModuleId())));
			} else if(role.equalsIgnoreCase(BrokerConstants.ASSISTER)){
				LOGGER.info("setModelAttributes: userId: "+user.getId());
				if(request.getAttribute("assisterId")!=null){
					LOGGER.info("setModelAttributes: assisterId: "+request.getAttribute("assisterId"));
					model.addAttribute(ENCRYPTED_RECORD_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(request.getAttribute("assisterId"))));
				}else{
					AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
					assisterRequestDTO.setUserId(user.getId());
					String assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
					AssisterResponseDTO assisterResponseDTOFromUserId = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
					if(assisterResponseDTOFromUserId!=null && assisterResponseDTOFromUserId.getAssister()!=null){
						LOGGER.info("setModelAttributes: recordId: "+assisterResponseDTOFromUserId.getAssister().getId());
						model.addAttribute(ENCRYPTED_RECORD_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(assisterResponseDTOFromUserId.getAssister().getId())));
					}
					request.getSession().setAttribute("assisterId", assisterResponseDTOFromUserId.getAssister().getId());
				}
				
			}else{
				LOGGER.info("setModelAttributes: Role is  :"+role);
				model.addAttribute(ENCRYPTED_RECORD_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(user.getActiveModuleId())));
			}
			
			model.addAttribute(BrokerConstants.SWITCH_ACC_URL, GhixConstants.SWITCH_URL_IND66);
			model.addAttribute(BrokerConstants.BOB_DOWNLOAD_URL, GhixConstants.BOB_DOWNLOAD_URL);
			model.addAttribute(BrokerConstants.RECORD_TYPE, user.getActiveModuleName());
		} else {
			model.addAttribute("applicationTypeList", AEEBookOfBusinessEnums.getEnumValues("APPLICATION_TYPE_ID"));
			model.addAttribute("currentStatusList", AEEBookOfBusinessEnums.getEnumValues("CURRENT_STATUS_ID"));
			model.addAttribute("nextStepList", AEEBookOfBusinessEnums.getEnumValues("NEXT_STEPS_ID"));

			//Get Previous, Current, and Next Year
			List<String> coverageYearIdEnumList = AEEBookOfBusinessEnums.getEnumValues("COVERAGE_YEAR_ID");

			//Get Exchange Start Date
            String exchangeStartDateString = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR).trim();

            //Configuration Exists
            if(StringUtils.isNumeric(exchangeStartDateString)) {
				List<String> coverageYearList = new ArrayList<>();

                int exchangeStartDate = Integer.parseInt(exchangeStartDateString);

                for(String yearString : coverageYearIdEnumList){
                    //Only Add Years That Are Greater or Equal to Start Year
                    if(Integer.parseInt(yearString) >= exchangeStartDate){
                        coverageYearList.add(yearString);
                    }
                }

                model.addAttribute("coverageYearList", coverageYearList);
            }
            else{ //Default Case For Non-Configured Start Date
                model.addAttribute("coverageYearList", coverageYearIdEnumList);
            }
		}
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
		model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
		
		 
	}
	
	private List<String> getLookupValues(List<LookupValue> lookupValues){
		List<String> labels = new ArrayList<>();
		
		if(lookupValues!=null){
			for(LookupValue value:lookupValues){
				labels.add(value.getLookupValueLabel());
			}
		}
		
		return labels;
	}
	
	private Assister retrieveAssisterObjectByUserId(Integer userId) throws JsonProcessingException, IOException {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setModuleId(userId);
		String assisterDetailsResponse;
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			assisterDetailsResponse = restClassCommunicator.retrieveAssisterRecordByUserId(assisterRequestDTO);
		} else {
			assisterDetailsResponse = restClassCommunicator.retrieveAssisterObjectByUserId(assisterRequestDTO);
		}
		
		AssisterResponseDTO assisterResponseDTOFromUserId = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		return assisterResponseDTOFromUserId.getAssister();
	}
	
	private Assister retrieveAssisterById(Integer assisterId) throws JsonProcessingException, IOException {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterId);
		String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTOFromUserId = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		return assisterResponseDTOFromUserId.getAssister();
	}
	
	private BrokerBobResponseDTO processIND72(BrokerBOBSearchParamsDTO bobSearchParamsDTO, AccountUser user, int currentPageNubmer, Model model,
			HttpServletRequest request, BrokerBOBDetailsDTO requestDTO){
		int totalCount = 0;
		HttpSession session = request.getSession();
		boolean isSortChanged =  BrokerConstants.TRUE.equalsIgnoreCase(request.getParameter("isSortChanged")) ? true : false;
		
		String role = user.getActiveModuleName();
		LOGGER.info("processIND72: Role is  :"+role);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		if(role.equalsIgnoreCase(BrokerConstants.BROKER) || role.equalsIgnoreCase(AGENCY_MANAGER)){
			bobSearchParamsDTO.setRecordId(Long.valueOf(requestDTO.getBrokerId()));
			bobSearchParamsDTO.setRecordType(BrokerConstants.AGENT);
		} else if(role.equalsIgnoreCase(BrokerConstants.ASSISTER)){
			LOGGER.info("processIND72: userId: "+user.getId());
			assisterRequestDTO.setUserId(user.getId());
			String assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
			AssisterResponseDTO assisterResponseDTOFromUserId = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
			if(assisterResponseDTOFromUserId!=null && assisterResponseDTOFromUserId.getAssister()!=null){
				LOGGER.info("processIND72: recordId: "+assisterResponseDTOFromUserId.getAssister().getId());
				bobSearchParamsDTO.setRecordId(Long.valueOf(assisterResponseDTOFromUserId.getAssister().getId()));
			}
			LOGGER.info("processIND72: recordType: "+user.getActiveModuleName().toUpperCase());
			bobSearchParamsDTO.setRecordType(user.getActiveModuleName().toUpperCase());
		}else{
			LOGGER.info("processIND72: Role is  :"+role);
			bobSearchParamsDTO.setRecordId(Long.valueOf(user.getActiveModuleId()));
			bobSearchParamsDTO.setRecordType(user.getActiveModuleName().toUpperCase());
		}
		
		if(isSortChanged){
			currentPageNubmer = 1;
		} else {
			totalCount =  request.getParameter("totalIndividualCount")!= null ? !request.getParameter("totalIndividualCount").isEmpty()? Integer.parseInt(request.getParameter("totalIndividualCount")) :0:0; 
			
			if(currentPageNubmer!=1 && session.getAttribute(INDIVIDUAL_IDS)!=null){
				List<Long> individualIds = (List<Long>) session.getAttribute(INDIVIDUAL_IDS);
				if(individualIds.size()>0){
					bobSearchParamsDTO.setIndividualIds(processNextPageIndividuals(individualIds, currentPageNubmer));
				}
			}
		}
		
		bobSearchParamsDTO.setPageNumber(currentPageNubmer);
		
		BrokerBobResponseDTO brokerBobResponseDTO = brokerIndTriggerService.triggerInd72(bobSearchParamsDTO);
		
		List<Long> individualIds = brokerBobResponseDTO.getIndividualsId();
		
		if((currentPageNubmer==1)){
			if(null != individualIds && individualIds.size() > 0){
				session.setAttribute(INDIVIDUAL_IDS, individualIds);
				brokerBobResponseDTO.setCount(individualIds.size()+10);
			}else{
				session.setAttribute(INDIVIDUAL_IDS, new ArrayList<Long>());
				brokerBobResponseDTO.setCount(10);
			}
		} 
		
		if(currentPageNubmer!=1){
			brokerBobResponseDTO.setCount(totalCount);
		}
		
		return brokerBobResponseDTO;
	}
	
	private List<Long> processNextPageIndividuals(List<Long> individualsId, int currentPage){
		List<Long> nextIndividualIds = new ArrayList<>();
		int size = individualsId.size();

		if(size <= PAGE_SIZE){
			nextIndividualIds = individualsId;
		} else if(size > (currentPage-1)*PAGE_SIZE){
			nextIndividualIds = individualsId.subList((currentPage-2) * PAGE_SIZE, (currentPage-1) * PAGE_SIZE);
		} else{
			nextIndividualIds = individualsId.subList((currentPage-2) * PAGE_SIZE, individualsId.size());
		}
		
		return nextIndividualIds;
	}

	private BrokerBOBSearchParamsDTO setRequestParameters(HttpServletRequest request, Integer startRecord, String sortBy, BrokerBOBSearchParamsDTO bobSearchParamsDTO) throws Exception{
		bobSearchParamsDTO.setStartRecord(startRecord);
		bobSearchParamsDTO.setEndRecord(startRecord+PAGE_SIZE);
		bobSearchParamsDTO.setSortBy(sortBy);

		return bobSearchParamsDTO;
	}

	private BrokerBOBSearchParamsDTO getFilterParameters(String json) throws Exception{
		JsonParser parser = new JsonParser();
		BrokerBOBSearchParamsDTO bobSearchParamsDTO = new BrokerBOBSearchParamsDTO();

		try {
			if(StringUtils.isNotBlank(json)){
				JsonObject obj = (JsonObject) parser.parse(json);
				bobSearchParamsDTO = platformGson.fromJson(obj, BrokerBOBSearchParamsDTO.class);
			}
		} catch(Exception exception){
			throw exception;
		}

		return bobSearchParamsDTO;
	}
	
	private BrokerBobResponseDTO setPlanLogo(BrokerBobResponseDTO brokerBobResponseDTO, AccountUser user) throws GIException{
		List<BrokerBOBDetailsDTO> listOfBrokerBobDetailsDTO = new ArrayList<>();
		if(brokerBobResponseDTO!=null && brokerBobResponseDTO.getBrokerBobDetailsDTO()!=null && brokerBobResponseDTO.getBrokerBobDetailsDTO().size() > 0){
			for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:brokerBobResponseDTO.getBrokerBobDetailsDTO()){
				if(!BrokerMgmtUtils.isEmpty(brokerBOBDetailsDTO.getHiosPlanId())){
					brokerBOBDetailsDTO.setIssuerLogo(getIssuerLogo(String.valueOf(brokerBOBDetailsDTO.getHiosPlanId()), user));
					if(brokerBOBDetailsDTO.getIssuerLogo()!=null){
						brokerBOBDetailsDTO.setIsEnrolled(BrokerConstants.TRUE);
					}
				}
				
				listOfBrokerBobDetailsDTO.add(brokerBOBDetailsDTO);
			}
			brokerBobResponseDTO.setBrokerBobDetailsDTO(listOfBrokerBobDetailsDTO);
		}
		
		return brokerBobResponseDTO;
	}
	
	private String getIssuerLogo(String hiosPlanId, AccountUser user) throws GIException {
		IssuerByHIOSIdResponseDTO issuerByHIOSIdResponseDTO = null;
		String issuerLogo = null;
		
		try {
			if (hiosPlanId != null ) {
				IssuerByHIOSIdRequestDTO request = new IssuerByHIOSIdRequestDTO();
				String hiosId = StringUtils.substring(hiosPlanId, 0, 5);
				request.setHiosId(hiosId);
				
				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_INFO_BY_HIOS_ID, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, request);
				if(response != null){
					issuerByHIOSIdResponseDTO = platformGson.fromJson(response.getBody(), IssuerByHIOSIdResponseDTO.class);
					issuerLogo = issuerByHIOSIdResponseDTO.getCompanyLogo();
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while retrieving Plan Details : ", exception);
/*			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
*/		}
		
		return issuerLogo;		
	}

	
	private void declineBrokerActiveIndividual(String individualId, AccountUser user, HttpServletRequest request) throws GIRuntimeException{
		LOGGER.info("decline : START");
		Broker broker = null;
		DesignateBroker designateBroker;
		
		try{
			if(BrokerConstants.YES.equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				broker = brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				broker = brokerService.findBrokerByUserId(user.getId());
			}
			if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(broker)){
				throw new AccessDeniedException(ACCESS_IS_DENIED);
			}
			
			LOGGER.info("Individual ID : "+individualId);

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
				designateBroker =  designateService.findBrokerByIndividualId(Integer.parseInt(individualId));
			} else {
				designateBroker =  designateService.findBrokerByExternalIndividualId(Integer.parseInt(individualId));
			}
			
			LOGGER.info("Designate Broker : "+designateBroker);
			
			if (designateBroker != null) {
				designateBroker.setStatus(Status.InActive);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());
				
				//Enrollment update is not required for CA
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					LOGGER.info("Invoking Update Enrollment");
					brokerMgmtUtils.updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getExternalIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG );

				} else {
					brokerMgmtUtils.updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG );
				}

				designateService.saveDesignateBroker(designateBroker);

				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
					Map<String, Object> brokerTemplateData = new HashMap<String, Object>();
					brokerTemplateData = brokerMgmtUtils.createTemplateMap(broker);
					Map<String, Object> searchCriteriaMap = new HashMap<String, Object>();
					searchCriteriaMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.SENDER_BROKER_MODULE);
					searchCriteriaMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_INACTIVE);
					searchCriteriaMap.put(BrokerConstants.SENDER_OBJ, broker);
					searchCriteriaMap.put(BrokerConstants.MODULE_ID, broker.getId());
					searchCriteriaMap.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(designateBroker.getIndividualId()));
					String noticeTemplateName = BrokerConstants.INDIVIDUAL_NOTIFICATION_TEMPLATE_NAME;

					brokerMgmtUtils.sendNotificationsToIndividuals(searchCriteriaMap, noticeTemplateName, brokerTemplateData);
				}
			} else {
				throw new GIRuntimeException("No Designation found for selected Individual");
			}
		} catch(Exception e){
			LOGGER.error("Exception occured while handling the decline designation request for Individual:", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	private void declineAssisterActiveIndividual(String individualId, AccountUser user, HttpServletRequest request) throws GIRuntimeException{
		LOGGER.info("decline: START");

		AssisterResponseDTO assisterResponseDTO = null;
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		Assister assister = null;

		final String EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST = "Exception occured while declining user designation request : ";

		try {
			user = entityUtils.getLoggedInUser();
			
			assisterRequestDTO.setIndividualId(Integer.valueOf(individualId));
			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			Validate.notNull(assisterDesignateResponseDTO);

			DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();

			if(designateAssister!=null){
				designateAssister.setStatus(DesignateAssister.Status.InActive);
				assisterRequestDTO.setDesignateAssister(designateAssister);
				designateAssister.setUpdated(new TSDate());
				designateAssister.setLastUpdatedBy(Long.valueOf(user.getId()));
				restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
					assisterRequestDTO.setModuleId(user.getId());
				    assisterResponseDTO = retrieveAssisterObjectByUserId(assisterRequestDTO);
					assister = assisterResponseDTO.getAssister();
					
					String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
					Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
			        inividualAPIRequestMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_INACTIVE);
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_ID, assister.getId());
			        inividualAPIRequestMap.put(BrokerConstants.SENDER_OBJ, assister);
			        inividualAPIRequestMap.put(BrokerConstants.INDIVIDUAL_ID, individualId);

		            Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
		     		String assistername = assister.getFirstName() +" "+assister.getLastName();
		     		assisterTemplateData.put(ASSISTER_NAME, assistername);
		     		assisterTemplateData.put(ENROLLMENTENTITY_NAME, assister.getEntity().getEntityName());

		     		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		     		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
		     		assisterTemplateData.put(REASON_FOR_DE_DESIGNATION, "Your Counselor "+assistername+ " does not wish to continue this relationship");

		     		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		     		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		     		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		     		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		     		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		     		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));

		     		assisterTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

		     		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		     		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		     		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);

		            brokerMgmtUtils.sendNotificationsToIndividuals(inividualAPIRequestMap, noticeTemplateName, assisterTemplateData);
				}
			} else {
				throw new GIRuntimeException("No Designation found for selected Individual");
			}	
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	private AssisterResponseDTO retrieveAssisterObjectByUserId(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		String assisterDetailsResponse = restClassCommunicator.retrieveAssisterObjectByUserId(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTOFromUserId = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		return assisterResponseDTOFromUserId;
	}
	
	private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
			AssisterRequestDTO assisterRequestDTO) throws GIException {
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		LOGGER.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");

		try {
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

			if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){
					throw new Exception();
			}

			LOGGER.debug("REST call Ends to the GHIX-ENITY module.");
		} catch(Exception ex){
			LOGGER.error("Exception in retrieving designatedAssister details ");
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return assisterDesignateResponseDTO;
	}
	
	@RequestMapping(value = "/broker/contactYHIPopup/{ssapApplicationId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@ResponseBody
	public String contactYHIPopup(@PathVariable("ssapApplicationId") String ssapApplicationId) throws GIRuntimeException {
		
		BrokerBOBDetailsDTO brokerBobDetailsDTO =null;
		if(ssapApplicationId!=null && !BrokerMgmtUtils.isEmpty(ssapApplicationId)){
			SsapApplication ssapApplication = brokerBOBService.getSsapApplication(new Long(ssapApplicationId));
			if(ssapApplication != null){
				brokerBobDetailsDTO = new BrokerBOBDetailsDTO();
				brokerBobDetailsDTO.setApplicationData(ssapApplication.getApplicationData());
				setApplicantsDOBSSN(brokerBobDetailsDTO);
			}
		}
		return platformGson.toJson(brokerBobDetailsDTO);
	}
	
	private void setApplicantsDOBSSN(BrokerBOBDetailsDTO brokerBOBDetailsDTO){
		try{
			JSONObject jsonObj = new JSONObject(brokerBOBDetailsDTO.getApplicationData());
			JSONObject ssapJsonObj = jsonObj.getJSONObject(SINGLE_STREAMLINED_APPLICATION);
			JSONArray taxHouseHold = ssapJsonObj.getJSONArray(TAX_HOUSEHOLD);
			JSONArray householdMembers = ((JSONArray) ((JSONObject) taxHouseHold.get(0)).get(HOUSEHOLD_MEMBER));
			JSONObject householdMember = null;

			for (int i = 0; i < householdMembers.length(); i++) {

				householdMember = (JSONObject) householdMembers.get(i);

				if (householdMember != null&& Integer.parseInt(householdMember.get(PERSONID).toString()) ==1 ) {
					String dateOfBirth  = householdMember.get(DATEOFBIRTH).toString();
					if (dateOfBirth != null) {
						try {
							brokerBOBDetailsDTO.setBirthDateString(dateOfBirth.substring(0, 13));
							}  catch(Exception exception) {
								throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
							}
					}
					brokerBOBDetailsDTO.setSsn(getSSN(householdMember));
					break;
				}
			}
		}catch(Exception exception){
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
	}
	
	private String getSSN(JSONObject householdMember) throws JSONException
	{
		String ssn = null;
		JSONObject applicantsSSN = (JSONObject) householdMember.get(SOCIAL_SECURITY_CARD);
		if (applicantsSSN != null) {
			ssn = applicantsSSN.get(SOCIAL_SECURITY_NUMBER).toString();
			int len = ssn.length();
			if( (ssn != null) && !(ssn.equals("")) && (len > 0 ))
			{
				ssn = ssn.substring((len-4));
			}
		}
		return ssn;
	}
}
