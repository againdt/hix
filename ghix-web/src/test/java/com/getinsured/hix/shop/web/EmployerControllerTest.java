package com.getinsured.hix.shop.web;

import java.util.List;

import org.junit.Test;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Employee;
import com.getinsured.hix.platform.file.reader.FileReader;
import com.getinsured.hix.platform.file.reader.ReaderFactory;

public class EmployerControllerTest extends BaseTest{
	
	@SuppressWarnings("unchecked")
	@Test
	public void testUploadFile(){
		  FileReader<Employee> empReader =  ReaderFactory.<Employee>getReader("C:\\Users\\parekh_d\\AppData\\Local\\Temp\\prefix6949309874489450445csv", ',' , Employee.class); 
		  List<Employee> employees = empReader.readData();
	}
	
}