package com.getinsured.hix.batch.enrollment.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.enrollment.email.Enrollment1095APdfNotification;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Notice.STATUS;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @see com.getinsured.hix.enrollment1095.service.Enrollment1095PdfNotificationService
 * @author Sharma_K
 * @since 16th September 2015 
 *
 */
@Service("enrollment1095PdfNotificationService")
public class Enrollment1095PdfNotificationServiceImpl implements Enrollment1095PdfNotificationService
{
	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeRepository noticeRepository;
	@Autowired private NoticeService noticeService;
	/*@Autowired private EnrollmentExternalRestUtil enrollmentExternalRestUtil;*/
	@Autowired private Enrollment1095APdfNotification enrollment1095APdfNotification;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095PdfNotificationServiceImpl.class);
	
	@Override
	public String send1095ASecureInboxMsg(final Integer noticeId, final String pdfFileName, final byte[] pdfDataInbytes,
			final Integer houseHoldCaseId,final Integer enrollmentId, final String policyIssuerName, final String emailBody)throws GIException
	{
		LOGGER.info("Enrollment1095PdfNotificationService :: IN send1095ASecureInboxMsg() method, generating pdf for EnrollmentId: "+enrollmentId);
		return createAndSendInboxMsg(noticeId, pdfFileName, pdfDataInbytes, houseHoldCaseId, enrollmentId, policyIssuerName, Boolean.FALSE, emailBody);
	}

	@Override
	public String resend1095ASecureInboxMsg(Integer noticeId, String pdfFileName,
			Integer houseHoldCaseId, String previousEcmDocId, final Integer enrollmentId, final String policyIssuerName, final String emailBody) throws GIException {
		try {
			LOGGER.info("Enrollment1095PdfNotificationService :: IN resend1095ASecureInboxMsg() method, resending pdf for EnrollmentId: "+enrollmentId);
			if (StringUtils.isNotEmpty(previousEcmDocId)) {
				byte[] pdfBytes = ecmService.getContentDataById(previousEcmDocId);
				return createAndSendInboxMsg(noticeId, pdfFileName, pdfBytes, houseHoldCaseId, enrollmentId, policyIssuerName, Boolean.TRUE, emailBody);
			} else {
				throw new GIException(EnrollmentConstants.ERROR_CODE_201, "Received Null or Empty ECM_DOC_ID", EnrollmentConstants.HIGH);
			}
		} catch (Exception ex) {
			throw new GIException(ex);
		}
	}
	
	@Override
	public void sendPdfGenerationEmail(final Map<String, String> requestMap) throws GIException
	{
		LOGGER.info("Enrollment1095PdfNotificationService:: 1095A Generated pdf statistics email sending process started ....");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INTERNAL_EMAIL_GROUP_FOR_1095_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("Enrollment1095PdfNotificationService:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("Enrollment1095PdfNotificationService:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			enrollment1095APdfNotification.setEmailData(emailData);
			enrollment1095APdfNotification.setRequestData(requestMap);
			Notice noticeObj = enrollment1095APdfNotification.generateEmail();
			Notification notificationObj = enrollment1095APdfNotification.generateNotification(noticeObj);
			LOGGER.info("Notice body for BusinessDays upload notification."+noticeObj.getEmailBody());
			enrollment1095APdfNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}
	
	/**
	 * 
	 * @param noticeId
	 * @param pdfFileName
	 * @param pdfDataInbytes
	 * @param houseHoldCaseId
	 * @param enrollmentId
	 * @param policyIssuerName
	 * @param isResend
	 * @return
	 * @throws GIException
	 */
	private String createAndSendInboxMsg(final Integer noticeId, final String pdfFileName, final byte[] pdfDataInbytes,
			final Integer houseHoldCaseId, final Integer enrollmentId, final String policyIssuerName, boolean isResend, final String emailBody)throws GIException
	{
		String ecm_doc_id = null;
		try {
			validateRequest(noticeId, pdfFileName, pdfDataInbytes, houseHoldCaseId, enrollmentId, policyIssuerName, emailBody);
			/*Household houseHold = enrollmentExternalRestUtil.getHousehold(houseHoldCaseId);*/
			Household houseHold = cmrHouseholdRepository.findOne(houseHoldCaseId);
			if(houseHold != null){
				/*
				 * Folder Relative Path
				 * /cmr/{HouseHoldCaseId}/1095_PDF/{Current_YEAR}/PDF File
				 */		
					String relativeFilePath = "cmr" + GhixConstants.FRONT_SLASH + houseHold.getId() + GhixConstants.FRONT_SLASH
							+ EnrollmentConstants.PDF1095_ECM_FOLDERNAME + GhixConstants.FRONT_SLASH
							+ DateUtil.getYearFromDate(new java.util.Date());
					ecm_doc_id =  ecmService.createContent(relativeFilePath, pdfFileName, pdfDataInbytes, ECMConstants.ENROLLMENT.DOC_CATEGORY, ECMConstants.ENROLLMENT.ENROLLMENT_1095_PDF, null);
					String emailSubject = getMessageSubject(pdfFileName, enrollmentId, policyIssuerName, isResend);
					saveNoticeAndSendSecureInboxMsg(noticeId, ecm_doc_id, houseHold, pdfFileName, emailSubject, emailBody);
				}
				else {
				throw new GIException(EnrollmentConstants.ERROR_CODE_201,
						"No CMR HouseHold Data Found for HouseHoldCase Id: " + houseHoldCaseId,
						EnrollmentConstants.HIGH);
			}	
		} catch (Exception ex) {
			throw new GIException(ex);
		}
		return ecm_doc_id;
	}
	
	/**
	 * 
	 * @param noticeId
	 * @param pdfFileName
	 * @param pdfDataInbytes
	 * @param houseHoldCaseId
	 * @param enrollmentId
	 * @param policyIssuerName
	 * @throws GIException
	 */
	private void validateRequest(Integer noticeId, String pdfFileName, byte[] pdfDataInbytes,
		Integer houseHoldCaseId, Integer enrollmentId, String policyIssuerName, String emailBody) throws GIException
	{
		if(enrollmentId == null || enrollmentId == 0){
			throw new GIException("ENROLLMENT_1095_MSG:: EnrollmentID: Received null or 0 value, EnrollmentID: " +enrollmentId);
		}
		if (pdfDataInbytes == null || (pdfDataInbytes != null && pdfDataInbytes.length == 0)) {
			throw new GIException("ENROLLMENT_1095_MSG:: PdfData: Received null or empty, EnrollmentId: "+enrollmentId);
		}
		if (noticeId == null || noticeId == 0) {
			throw new GIException("ENROLLMENT_1095_MSG:: NoticeID: Received null or 0 value, EnrollmentId: " + enrollmentId);
		}
		if (StringUtils.isEmpty(pdfFileName)) {
			throw new GIException("ENROLLMENT_1095_MSG:: pdfFileName: Received null or empty pdfFileName, EnrollmentId: " + enrollmentId);
		}
		if (houseHoldCaseId == null || houseHoldCaseId == 0) {
			throw new GIException("ENROLLMENT_1095_MSG:: houseHoldCaseId: Received null or 0 value, EnrollmentId: " + enrollmentId);
		}
		if(StringUtils.isEmpty(policyIssuerName)){
			throw new GIException("ENROLLMENT_1095_MSG:: policyIssuerName: Received null or empty policyIssuerName, EnrollmentId: " + enrollmentId);
		}
		if(StringUtils.isEmpty(emailBody)){
			throw new GIException("ENROLLMENT_1095_MSG:: emailBody: Received null or empty emailBody, EnrollmentId: "+enrollmentId);
		}
	}
	
	/**
	 * Method to save Notice and Sending secure Inbox Message
	 * @param noticeId Integer
	 * @param ecmDocId String
	 * @param houseHold Object[AccountUser]
	 * @param pdfFileName String
	 * @Param emailSubject String
	 * @return Object<Notice>
	 * @throws ContentManagementServiceException
	 * @throws GIException
	 */
	private Notice saveNoticeAndSendSecureInboxMsg(Integer noticeId, String ecmDocId, Household houseHold, String pdfFileName, String emailSubject, String emailBody) throws ContentManagementServiceException, GIException
	{
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
			
		List<String> emailList = new ArrayList<String>();
		String exchangeName =  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		String fullName = EnrollmentUtils.getFullName(houseHold.getFirstName(), null, houseHold.getLastName());
		
		Notice newNoticeObj = createAndSaveNotice(noticeId, ecmDocId, houseHold, moduleName, pdfFileName, emailSubject, emailBody);  
		
		if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.SEND_1095_PDF_TO_INBOX))) {
			noticeService.postToInbox(houseHold.getUser(), emailList,
					moduleName, houseHold.getId(), newNoticeObj,
					pdfFileName, exchangeName, fullName);
		}
		return newNoticeObj;
	}
	
	/**
	 * Save Notice Object to DB
	 * @param noticeId Integer
	 * @param ecmDocId String
	 * @param houseHold Object<Household> 
	 * @param moduleName String
	 * @param pdfFileName String
	 * @Param emailSubject String
	 * @return Object<Notice>
	 */
	private Notice createAndSaveNotice(Integer noticeId, String ecmDocId, Household houseHold, String moduleName, String pdfFileName, String emailSubject, String emailBody)
	{
		Notice noticeObj = new Notice();
		noticeObj.setUser(houseHold.getUser());
		noticeObj.setKeyId(houseHold.getId());
		noticeObj.setKeyName(moduleName);
		noticeObj.setSentDate(new Date());
		noticeObj.setEmailBody(StringEscapeUtils.unescapeJava(emailBody));
		noticeObj.setId(noticeId);
		noticeObj.setEcmId(ecmDocId);
		noticeObj.setSubject(emailSubject);
		noticeObj.setStatus(STATUS.PDF_GENERATED);
		noticeObj.setPrintable(EnrollmentConstants.Y);
		
		return noticeRepository.save(noticeObj);
	}
	

	/**
	 * FILE_NAME = "1095A_Form_Initial_timeStamp.pdf" :: SUBJECT =  "1095A Form"
	 * FILE_NAME = "1095A_Form_Correction_timeStamp.pdf" :: SUBJECT =  "Corrected 1095A Form"
	 * FILE_NAME = "1095A_Form_Void_timeStamp.pdf" :: SUBJECT =  "Invalid 1095A Form"
	 * 
	 * @param fileName String
	 * @param enrollmentId Integer
	 * @param policyIssuerName String
	 * @param isResend boolean
	 * @return String
	 * @throws GIException
	 */
	private String getMessageSubject(String fileName, Integer enrollmentId, String policyIssuerName, boolean isResend)throws GIException
	{
		LOGGER.info("Generating Email Subject for 1095A Pdf ,Received File name: "+fileName+" For EnrollmentID: "+enrollmentId);
		String subject = null;
		String[] fileNameSubsetArray = fileName.split("_");
		if(fileNameSubsetArray != null && fileNameSubsetArray.length >= EnrollmentConstants.TWO){
			String postPrefix =  policyIssuerName + " - " + enrollmentId;
			String pdfType = fileNameSubsetArray[EnrollmentConstants.TWO];
			String resendPrefix = StringUtils.EMPTY;
			if(isResend){
				resendPrefix = EnrollmentConstants.SUBJECT_PREFIX_RESENT;
			}
			if(pdfType.equalsIgnoreCase(EnrollmentConstants.INTIAL_1095A_PDF)){
				subject = resendPrefix + EnrollmentConstants.SUBJECT_1095A_FORM + postPrefix;
			}
			else if(pdfType.equalsIgnoreCase(EnrollmentConstants.CORRECTION_1095A_PDF)){
				subject = resendPrefix + EnrollmentConstants.SUBJECT_1095A_CORRECTED + postPrefix;
			}
			else if(pdfType.equalsIgnoreCase(EnrollmentConstants.VOID_1095A_PDF)){
				subject = resendPrefix + EnrollmentConstants.SUBJECT_1095A_INVALID + postPrefix;
			}
		}
		if(StringUtils.isEmpty(subject)){
			throw new GIException("Invalid file name received: "+fileName);
		}
		return subject;
	}
}
