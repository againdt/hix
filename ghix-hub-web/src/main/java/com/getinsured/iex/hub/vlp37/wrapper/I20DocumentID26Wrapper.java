package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.I20DocumentID26Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;

/**
 * Wrapper Class for I20DocumentID26Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I20DocumentID26Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I20DocumentID26Wrapper.class);
	
	private ObjectFactory factory;
	private I20DocumentID26Type i20DocumentID26Type;
	
	private String i94Number;
	private PassportCountryWrapper passportCountry;
	private String passportNumber;
	private String countryOfIssuance;
	private String sevisid;
	private String expiryDate;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public I20DocumentID26Wrapper(){
		this.factory = new ObjectFactory();
		this.i20DocumentID26Type = factory.createI20DocumentID26Type();
		this.passportCountry = new PassportCountryWrapper();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.i20DocumentID26Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}
	
	public void setPassportCountry(String passportCountry) throws HubServiceException{
		
		if(passportCountry == null){
			throw new HubServiceException("No Passport Country provided");
		}
		
		this.passportCountry = PassportCountryWrapper.fromJsonString(passportCountry);
		this.i20DocumentID26Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public void setPassportCountry(PassportCountryWrapper passportCountry) throws HubServiceException{
		
		if(passportCountry == null){
			throw new HubServiceException("No Passport Country provided");
		}
		
		this.passportCountry = passportCountry;
		this.i20DocumentID26Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public PassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.passportCountry.setPassportNumber(passportNumber);
		this.i20DocumentID26Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.passportCountry.setCountryOfIssuance(countryOfIssuance);
		this.i20DocumentID26Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.i20DocumentID26Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i20DocumentID26Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I20 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public I20DocumentID26Type getI20DocumentID26Type(){
		return this.i20DocumentID26Type;
	}
	
	/**
	 * Converts the JSON String to I20DocumentID26Wrapper object
	 * 
	 * @param jsonString - String representation for I94DocumentID2Wrapper
	 * @return I20DocumentID26Wrapper Object
	 */
	public static I20DocumentID26Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I20DocumentID26Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I20DocumentID26Wrapper wrapper = new I20DocumentID26Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.i20DocumentID26Type.setI94Number(wrapper.i94Number);
		}
		
		tmpObj = obj.get(PASSPORT_COUNTRY_KEY);
		if(tmpObj != null){
			JSONObject passportCountryObj = (JSONObject)tmpObj;
			wrapper.passportCountry = PassportCountryWrapper.fromJSONObject(passportCountryObj);
			wrapper.i20DocumentID26Type.setPassportCountry(wrapper.passportCountry.getPassportCountryType());
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.i20DocumentID26Type.setSEVISID(wrapper.sevisid);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i20DocumentID26Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I20DocumentID26Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I20DocumentID26Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I20DocumentID26Wrapper wrapper = new I20DocumentID26Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		if(jsonObj.get(PASSPORT_COUNTRY_KEY) != null){
			wrapper.setPassportCountry(PassportCountryWrapper.fromJSONObject((JSONObject)jsonObj.get(PASSPORT_COUNTRY_KEY)));
		}
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
