package com.getinsured.eligibility.ssap.integration.notices;

import java.util.List;

import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface AgedOutDisenrollmentNoticeService {

	public String processAgeOutDependents(String caseNumbers);
}