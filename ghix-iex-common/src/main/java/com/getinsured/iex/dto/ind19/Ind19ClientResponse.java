package com.getinsured.iex.dto.ind19;

import java.util.Map;

public class Ind19ClientResponse {
	
	private boolean ind19CallSuccess;
	
	private String planShoppingId;
	
	private Map<String, String> errorCodesAndMessages;

	public boolean isInd19CallSuccess() {
		return ind19CallSuccess;
	}

	public void setIsInd19CallSuccess(boolean ind19CallSuccess) {
		this.ind19CallSuccess = ind19CallSuccess;
	}

	public String getPlanShoppingId() {
		return planShoppingId;
	}

	public void setPlanShoppingId(String planShoppingId) {
		this.planShoppingId = planShoppingId;
	}

	public Map<String, String> getErrorCodesAndMessages() {
		return errorCodesAndMessages;
	}

	public void setErrorCodesAndMessages(Map<String, String> errorCodesAndMessages) {
		this.errorCodesAndMessages = errorCodesAndMessages;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ind19ClientResponse [ind19CallSuccess=");
		builder.append(ind19CallSuccess);
		builder.append(", planShoppingId=");
		builder.append(planShoppingId);
		builder.append(", errorCodesAndMessages=");
		builder.append(errorCodesAndMessages);
		builder.append("]");
		return builder.toString();
	}
	
	

}
