package com.getinsured.ssap.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Permission;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RolePermission;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.ssap.config.SsapEnums;
import com.getinsured.iex.client.ResourceCreatorImpl;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.builder.SsapJsonBuilderImpl;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.ssap.model.eligibility.EligibilityRequest;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;
import com.getinsured.ssap.repository.ApplicantRepository;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.FamilyBuilder;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SSLUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link SingleStreamlinedApplicationServiceImpl}.
 *
 * @author Yevgen Golubenko
 * @since 12/22/18
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DynamicPropertiesUtil.class})
@PowerMockIgnore({"javax.net.ssl.*", "javax.security.*"})
public class SingleStreamlinedApplicationServiceImplTest {
  private static final Logger log = LoggerFactory.getLogger(SingleStreamlinedApplicationServiceImplTest.class);

  private SsapUtil ssapUtil = new SsapUtil();

  @Mock
  private GhixRestTemplate ghixRestTemplate;
  @Mock
  private RestTemplate restTemplate;
  @Mock
  private UserServiceImpl userService;
  @Mock
  private ResourceCreatorImpl resourceCreatorImpl;
  @Mock
  private LocationService locationService;
  @Mock
  private ZipCodeService zipCodeService;
  @Mock
  private SsapApplicationRepository ssapApplicationRepository;

  @Mock
  private SsapApplicantRepository ssapApplicantRepository;

  @Mock
  private ApplicantRepository applicantRepository;
  @Mock
  private LookupService lookupService;
  @Mock
  private EventCreationService eventCreationService;
  @Mock
  private IfsvService ifsvService;
  @Mock
  private NonEsiMecService nmecService;
  @Mock
  private OutboundAccountTransferService outboundAccountTransferService;
  @Mock
  private SsapComparisonService ssapComparisonService;
  @Mock
  private SingleStreamlinedApplicationServiceImpl applicationService;

  private EligibilityEngineIntegrationServiceImpl eligibilityEngineIntegrationService;

  // Eligibility Engine requirements:
  @Mock
  private Properties configProp;

  @Mock
  private EligibilityProgramRepository eligibilityProgramRepository;
  @Mock
  private HubIntegration hubIntegration;
  @Mock
  private SsapNoticeService ssapNoticeService;
  @Mock
  private ApplicationFinalization applicationFinalization;
  
  @Mock
  private AptcHistoryRepository aptcHistoryRepository;

  private ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService = new ExchangeEligibilityDeterminationService();

  private Gson platformGson;

  private static final String TEST_JSON = "hix-117961.json";
  private static final String TEST_SERVER_URL = "https://nv2dev.eng.vimo.com/ghix-eligibility-engine/";

  private static SsapApplication SSAP_APPLICATION = null;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    PowerMockito.mockStatic(DynamicPropertiesUtil.class);
    SSLUtil.turnOffSslChecking();

    final GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
    gsonBuilder.serializeNulls();
    gsonBuilder.setDateFormat("YYYY-MM-DD hh:mm:ss"); // "MMM dd, yyyy hh:mm:ss a"
    gsonBuilder.serializeSpecialFloatingPointValues();

    this.platformGson = gsonBuilder.create();

    ssapComparisonService = new SsapComparisonServiceImpl();
    userService = mock(UserServiceImpl.class);

    SSAP_APPLICATION = getSsapApplication(null);

    when(DynamicPropertiesUtil.getPropertyValue(any(GlobalConfiguration.GlobalConfigurationEnum.class))).thenAnswer(invocation -> {
      GlobalConfiguration.GlobalConfigurationEnum prop = invocation.getArgument(0, GlobalConfiguration.GlobalConfigurationEnum.class);

      if (prop == GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE) {
        log.info("GlobalConfiguration call for state code, returning NV");
        return "NV";
      }

      return null;
    });

    when(configProp.getProperty("endPointEligibilityDetermination")).thenReturn(TEST_SERVER_URL);
    when(userService.getLoggedInUser()).thenReturn(getSampleUser());
    when(ssapApplicationRepository.findByCaseNumber(anyString())).thenReturn(SSAP_APPLICATION);
    when(ghixRestTemplate.getForObject(anyString(), any(Class.class))).thenAnswer(invocation -> {
      RestTemplate restTemplate = new RestTemplate();
      final String url = invocation.getArgument(0, String.class);
      final Class clazz = (Class) invocation.getArguments()[1];

      if (clazz == Household.class) {
        log.info("Called REST endpoint: {}, returning getHousehold()", url);
        return getHousehold();
      }

      return restTemplate.getForObject(url, clazz);
    });

    when(ghixRestTemplate.postForObject(anyString(), any(EligibilityRequest.class), anyObject())).thenAnswer(invocation -> {
      RestTemplate restTemplate = new RestTemplate();

      final String url = invocation.getArgument(0, String.class);
      final EligibilityRequest request = invocation.getArgument(1, EligibilityRequest.class);
      final Class response = invocation.getArgument(2, Class.class);

      return restTemplate.postForObject(url, request, response);
    });

    when(ssapApplicationRepository.getOne(anyLong())).thenReturn(SSAP_APPLICATION);
    when(ssapApplicationRepository.save(any(SsapApplication.class))).thenAnswer(invocation -> {
      final SsapApplication ssapApplication = invocation.getArgument(0, SsapApplication.class);
      log.info("ssapApplicationRepository.save() called, returning saved record: ssapApplication: {}", ssapApplication.getId());
      SSAP_APPLICATION = ssapApplication;
      return ssapApplication;
    });

    when(ssapApplicationRepository.findOne(anyLong())).thenReturn(SSAP_APPLICATION);

    eligibilityEngineIntegrationService =
      new EligibilityEngineIntegrationServiceImpl(
        ghixRestTemplate,
        configProp,
        ssapApplicantRepository,
        eligibilityProgramRepository,
        ssapApplicationRepository,
        exchangeEligibilityDeterminationService,
        hubIntegration,
        eventCreationService,
        ssapNoticeService,
        userService,
        aptcHistoryRepository);

    eligibilityEngineIntegrationService.postConstruct();

    applicationFinalization = new ApplicationFinalizationImpl(
      eligibilityEngineIntegrationService,
      ssapComparisonService,
      ifsvService,
      nmecService,
      ssapApplicationRepository,
      applicantRepository,
      outboundAccountTransferService,
      restTemplate,
      ghixRestTemplate);

    applicationService = new SingleStreamlinedApplicationServiceImpl(
      ghixRestTemplate,
      restTemplate,
      userService,
      ssapUtil,
      resourceCreatorImpl,
      locationService,
      zipCodeService,
      ssapApplicationRepository,
      lookupService,
      applicantRepository,
      eventCreationService,
      applicationFinalization
    );
  }

  @After
  public void tearDown() throws Exception {

  }


  @Test
  public void testSave() throws Exception {
    SsapApplication ssapApplication = getSsapApplication(TEST_JSON);
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    assert application != null : "SingleStreamlinedApplication is null";

    application.setFinalized(true);

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);
    Assert.assertNotNull(savedApplication);
    Assert.assertEquals(ApplicationStatus.CLOSED.getApplicationStatusCode(), SSAP_APPLICATION.getApplicationStatus());
    Assert.assertEquals(EligibilityStatus.DE, SSAP_APPLICATION.getEligibilityStatus());
  }

  @Test
  public void testClosedApplication() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("ssap-closed.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    assert application != null : "SingleStreamlinedApplication is null";

    application.setFinalized(true);

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);
    Assert.assertNotNull(savedApplication);
    Assert.assertEquals(ApplicationStatus.CLOSED.getApplicationStatusCode(), SSAP_APPLICATION.getApplicationStatus());
    Assert.assertEquals(EligibilityStatus.DE, SSAP_APPLICATION.getEligibilityStatus());
  }

  @Test
  public void testClosed2() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("closed.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    assert application != null : "SingleStreamlinedApplication is null";

    application.setFinalized(true);

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);
    Assert.assertNotNull(savedApplication);
  }

  @Test
  public void testCloneStep() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("for-clone.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);

    SsapApplication clonedSsapApplication = cloneWebMethod(ssapApplication);
    SingleStreamlinedApplication clonedApplication = JsonUtil.parseApplicationDataJson(clonedSsapApplication);

    Assert.assertNotNull(application);
    Assert.assertNotNull(clonedApplication);

    HouseholdMember originalMember = application.getTaxHousehold().get(0).getHouseholdMember().get(0);
    HouseholdMember clonedMember = clonedApplication.getTaxHousehold().get(0).getHouseholdMember().get(0);

    Assert.assertNotNull(originalMember.getAnnualTaxIncome());
    Assert.assertNotNull(clonedMember.getAnnualTaxIncome());
    Assert.assertEquals(originalMember.getAnnualTaxIncome().getReportedIncome(), clonedMember.getAnnualTaxIncome().getReportedIncome());
    Assert.assertEquals(originalMember.getAnnualTaxIncome().getProjectedIncome(), clonedMember.getAnnualTaxIncome().getProjectedIncome());

    Assert.assertEquals(originalMember.getTaxHouseholdCompositionFlags(), clonedMember.getTaxHouseholdCompositionFlags());

    Assert.assertNotNull(originalMember.getTaxHouseholdComposition());
    Assert.assertNotNull(clonedMember.getTaxHouseholdComposition());
    Assert.assertEquals(originalMember.getTaxHouseholdComposition().getTaxFilingStatus(), clonedMember.getTaxHouseholdComposition().getTaxFilingStatus());
    Assert.assertEquals(originalMember.getTaxHouseholdComposition().getTaxRelationship(), clonedMember.getTaxHouseholdComposition().getTaxRelationship());
  }

  private SsapApplication cloneWebMethod(SsapApplication ssapApplication) throws Exception {
    SsapJsonBuilder ssapJsonBuilder = new SsapJsonBuilderImpl();

    SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
    singleStreamlinedApplication.setIsEditApplication(true);
    ssapApplication.setApplicationData(ssapJsonBuilder.transformToJson(singleStreamlinedApplication));

    return ssapApplication;
  }

  @Test
  public void testAutomaticVerificationOfMedicaidAssessedUser() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("2-hh-1-medicare.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    Assert.assertNotNull(application);

    HouseholdMember medicaidMember = getHouseholdByGuid(application, "1000039882");
    HouseholdMember qhpMember = getHouseholdByGuid(application, "1000039884");

    Assert.assertNotNull(medicaidMember);
    Assert.assertFalse(medicaidMember.isSeeksQhp());

    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    List<SsapApplicant> savedApplicantList = new ArrayList<>();

    when(applicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {

      SsapApplicant medicaidApplicant = new SsapApplicant();
      medicaidApplicant.setId(medicaidMember.getPersonId());
      medicaidApplicant.setApplicantGuid(medicaidMember.getApplicantGuid());
      medicaidApplicant.setFirstName(medicaidMember.getName().getFirstName());
      medicaidApplicant.setLastName(medicaidMember.getName().getLastName());
      medicaidApplicant.setSsapApplication(ssapApplication);

      SsapApplicant qhpApplicant = new SsapApplicant();
      qhpApplicant.setId(qhpMember.getPersonId());
      qhpApplicant.setApplicantGuid(qhpMember.getApplicantGuid());
      qhpApplicant.setFirstName(qhpMember.getName().getFirstName());
      qhpApplicant.setLastName(qhpMember.getName().getLastName());
      qhpApplicant.setSsapApplication(ssapApplication);

      return Arrays.asList(medicaidApplicant, qhpApplicant);
    });

    when(applicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      savedApplicantList.add(applicant);
      return applicant;
    });

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);

    Assert.assertNotNull(savedApplication);
    Assert.assertFalse(savedApplicantList.isEmpty());

    SsapApplicant savedMedicaidApplicant = savedApplicantList.stream().filter(applicant -> applicant.getId() == medicaidMember.getPersonId()).findFirst().orElse(null);
    Assert.assertNotNull(savedMedicaidApplicant);

    final String VERIFIED_CODE = "VERIFIED";
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getVlpVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getIncomeVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getSsnVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getMecVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getDeathStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getNativeAmericanVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getNonEsiMecVerificationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getIncarcerationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getCitizenshipImmigrationStatus());
    Assert.assertEquals(VERIFIED_CODE, savedMedicaidApplicant.getResidencyStatus());
  }

  @Test
  public void testCase_D() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("testcase-d.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    Assert.assertNotNull(application);
    application.setFinalized(true);

    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    List<SsapApplicant> savedApplicantList = new ArrayList<>();

    HouseholdMember janeDoe = getHouseholdMemberByName(application, "Jane", "Doe");
    HouseholdMember jimDoe = getHouseholdMemberByName(application, "Jim", "Doe");
    HouseholdMember babyDoe = getHouseholdMemberByName(application, "Baby", "Doe");

    SsapApplicant janeDoeApplicant = new SsapApplicant();
    janeDoeApplicant.setId(janeDoe.getPersonId());
    janeDoeApplicant.setApplicantGuid(janeDoe.getApplicantGuid());
    janeDoeApplicant.setFirstName(janeDoe.getName().getFirstName());
    janeDoeApplicant.setLastName(janeDoe.getName().getLastName());
    janeDoeApplicant.setSsapApplication(ssapApplication);

    SsapApplicant jimDoeApplicant = new SsapApplicant();
    jimDoeApplicant.setId(jimDoe.getPersonId());
    jimDoeApplicant.setApplicantGuid(jimDoe.getApplicantGuid());
    jimDoeApplicant.setFirstName(jimDoe.getName().getFirstName());
    jimDoeApplicant.setLastName(jimDoe.getName().getLastName());
    jimDoeApplicant.setSsapApplication(ssapApplication);

    SsapApplicant babyDoeApplicant = new SsapApplicant();
    babyDoeApplicant.setId(babyDoe.getPersonId());
    babyDoeApplicant.setApplicantGuid(babyDoe.getApplicantGuid());
    babyDoeApplicant.setFirstName(babyDoe.getName().getFirstName());
    babyDoeApplicant.setLastName(babyDoe.getName().getLastName());
    babyDoeApplicant.setSsapApplication(ssapApplication);

    List<SsapApplicant> dbApplicants = new ArrayList<>(Arrays.asList(janeDoeApplicant, jimDoeApplicant, babyDoeApplicant));


    when(ssapApplicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      savedApplicantList.add(applicant);
      return applicant;
    });

    Assert.assertEquals(BloodRelationshipCode.DOMESTIC_PARTNER, HouseholdUtil.getRelationOf(Arrays.asList(janeDoe, jimDoe, babyDoe), janeDoe, jimDoe));
    Assert.assertEquals(BloodRelationshipCode.DOMESTIC_PARTNER, HouseholdUtil.getRelationOf(Arrays.asList(janeDoe, jimDoe, babyDoe), jimDoe, janeDoe));
    Assert.assertEquals(BloodRelationshipCode.CHILD, HouseholdUtil.getRelationOf(Arrays.asList(janeDoe, jimDoe, babyDoe), janeDoe, babyDoe));
    Assert.assertEquals(BloodRelationshipCode.COURT_APPOINTED_GUARDIAN, HouseholdUtil.getRelationOf(Arrays.asList(janeDoe, jimDoe, babyDoe), babyDoe, jimDoe));

    Assert.assertTrue("Primary tax filer person id should be Jane doe", janeDoe.getPersonId() == application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId());
    Assert.assertTrue(application.getTaxHousehold().get(0).isReconciledAptc());
    Assert.assertFalse("Failure to reconcile should be false for primary tax household", application.getTaxHousehold().get(0).isFailureToReconcile());

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);

    Assert.assertTrue(janeDoe.getApplyingForCoverageIndicator());
    Assert.assertTrue(jimDoe.getApplyingForCoverageIndicator());
    Assert.assertTrue(babyDoe.getApplyingForCoverageIndicator());

    Assert.assertNotNull(savedApplication);

    HouseholdMemberResponse janeEligibility = janeDoe.getEligibilityResponse();
    HouseholdMemberResponse jimEligibility = jimDoe.getEligibilityResponse();
    HouseholdMemberResponse babyEligibility = babyDoe.getEligibilityResponse();

    Assert.assertNotNull(janeEligibility);
    Assert.assertNotNull(jimEligibility);
    Assert.assertNotNull(babyEligibility);

    Assert.assertTrue(janeEligibility.isExchangeEligible());
    Assert.assertTrue(jimEligibility.isExchangeEligible());
    Assert.assertTrue(babyEligibility.isExchangeEligible());

    Assert.assertTrue("Jane Doe should be QHP eligible", janeEligibility.isExchangeEligible());
    Assert.assertTrue("Jim Doe should be QHP eligible", jimEligibility.isExchangeEligible());
    Assert.assertTrue("Baby Doe should be QHP eligible", babyEligibility.isExchangeEligible());

    Assert.assertFalse("Jane Doe should NOT be CSR eligible", janeEligibility.isCsrEligible());
    Assert.assertFalse("Jim Doe should NOT be CSR eligible", jimEligibility.isCsrEligible());
    Assert.assertFalse("Baby Doe should NOT be CSR eligible", babyEligibility.isCsrEligible());

    Assert.assertFalse("Jane Doe should NOT be Medicaid eligible", janeEligibility.isMedicaid());
    Assert.assertFalse("Jim Doe should NOT be Medicaid eligible", jimEligibility.isMedicaid());
    Assert.assertFalse("Baby Doe should NOT be Medicaid eligible", babyEligibility.isMedicaid());

    Assert.assertFalse("Jane Doe should NOT be CHIP eligible", janeEligibility.isChip());
    Assert.assertFalse("Jim Doe should NOT be CHIP eligible", jimEligibility.isChip());
    Assert.assertFalse("Baby Doe should NOT be CHIP eligible", babyEligibility.isChip());

    Assert.assertTrue("Jane Doe should be APTC eligible", janeEligibility.isAptcEligible());
    Assert.assertFalse("Jim Doe should NOT be APTC eligible", jimEligibility.isAptcEligible());
    Assert.assertTrue("Baby Doe should be APTC eligible", babyEligibility.isAptcEligible());
  }


  @Test
  public void testCase_C() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("testcase-c.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    Assert.assertNotNull(application);
    application.setFinalized(true);

    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    List<SsapApplicant> savedApplicantList = new ArrayList<>();

    HouseholdMember laura = getHouseholdMemberByName(application, "Laura", "Banfield");
    HouseholdMember junior = getHouseholdMemberByName(application, "Junior", "Banfield");
    HouseholdMember john = getHouseholdMemberByName(application, "John", "Curtis");

    SsapApplicant lauraApplicant = new SsapApplicant();
    lauraApplicant.setId(laura.getPersonId());
    lauraApplicant.setApplicantGuid(laura.getApplicantGuid());
    lauraApplicant.setFirstName(laura.getName().getFirstName());
    lauraApplicant.setLastName(laura.getName().getLastName());
    lauraApplicant.setSsapApplication(ssapApplication);

    SsapApplicant juniorApplicant = new SsapApplicant();
    juniorApplicant.setId(junior.getPersonId());
    juniorApplicant.setApplicantGuid(junior.getApplicantGuid());
    juniorApplicant.setFirstName(junior.getName().getFirstName());
    juniorApplicant.setLastName(junior.getName().getLastName());
    juniorApplicant.setSsapApplication(ssapApplication);

    SsapApplicant johnApplicant = new SsapApplicant();
    johnApplicant.setId(john.getPersonId());
    johnApplicant.setApplicantGuid(john.getApplicantGuid());
    johnApplicant.setFirstName(john.getName().getFirstName());
    johnApplicant.setLastName(john.getName().getLastName());
    johnApplicant.setSsapApplication(ssapApplication);

    List<SsapApplicant> dbApplicants = new ArrayList<>(Arrays.asList(lauraApplicant, juniorApplicant, johnApplicant));

    when(ssapApplicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      savedApplicantList.add(applicant);
      return applicant;
    });

    Assert.assertEquals(BloodRelationshipCode.DOMESTIC_PARTNER, HouseholdUtil.getRelationOf(Arrays.asList(laura, junior, john), laura, john));
    Assert.assertEquals(BloodRelationshipCode.CHILD, HouseholdUtil.getRelationOf(Arrays.asList(laura, junior, john), laura, junior));
    Assert.assertEquals(BloodRelationshipCode.CHILD, HouseholdUtil.getRelationOf(Arrays.asList(laura, junior, john), john, junior));
    Assert.assertEquals(BloodRelationshipCode.PARENT_OF_CHILD, HouseholdUtil.getRelationOf(Arrays.asList(laura, junior, john), junior, john));
    Assert.assertEquals(BloodRelationshipCode.PARENT_OF_CHILD, HouseholdUtil.getRelationOf(Arrays.asList(laura, junior, john), junior, laura));

    Assert.assertTrue("Primary tax filer person id should be Jane doe", laura.getPersonId() == application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId());
    Assert.assertTrue(application.getTaxHousehold().get(0).isReconciledAptc());
    Assert.assertFalse("Failure to reconcile should be false for primary tax household", application.getTaxHousehold().get(0).isFailureToReconcile());

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);

    Assert.assertTrue(laura.getApplyingForCoverageIndicator());
    Assert.assertTrue(junior.getApplyingForCoverageIndicator());
    Assert.assertFalse(john.getApplyingForCoverageIndicator());

    Assert.assertNotNull(savedApplication);

    HouseholdMemberResponse lauraEligibility = laura.getEligibilityResponse();
    HouseholdMemberResponse juniorEligibility = junior.getEligibilityResponse();
    HouseholdMemberResponse johnEligibility = john.getEligibilityResponse();

    Assert.assertNotNull(lauraEligibility);
    Assert.assertNotNull(juniorEligibility);
    Assert.assertNull(johnEligibility);

    Assert.assertTrue(lauraEligibility.isExchangeEligible());
    Assert.assertTrue(juniorEligibility.isExchangeEligible());

    Assert.assertTrue(getName(laura) + " should be QHP eligible", lauraEligibility.isExchangeEligible());
    Assert.assertTrue( getName(junior) + " should be QHP eligible", juniorEligibility.isExchangeEligible());

    Assert.assertTrue(getName(laura) + " should NOT be CSR eligible", lauraEligibility.isCsrEligible());
    Assert.assertFalse(getName(junior) +" should NOT be CSR eligible", juniorEligibility.isCsrEligible());

    Assert.assertFalse(getName(laura) +" should NOT be Medicaid eligible", lauraEligibility.isMedicaid());
    Assert.assertFalse(getName(junior) +" should NOT be Medicaid eligible", juniorEligibility.isMedicaid());

    Assert.assertFalse(getName(laura) +" should NOT be CHIP eligible", lauraEligibility.isChip());
    Assert.assertTrue(getName(junior) +" should be CHIP eligible", juniorEligibility.isChip());

    Assert.assertTrue(getName(laura) +" should be APTC eligible", lauraEligibility.isAptcEligible());
    Assert.assertFalse(getName(junior) +" should NOT be APTC eligible", juniorEligibility.isAptcEligible());
  }


  @Test
  public void testCase_SingleNonFiler() throws Exception {
    SsapApplication ssapApplication = getSsapApplication("single-nonfiler.json");
    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);
    Assert.assertNotNull(application);
    application.setFinalized(true);

    String applicationSource = ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode();

    List<SsapApplicant> savedApplicantList = new ArrayList<>();

    HouseholdMember laura = getHouseholdMemberByName(application, "Gerald", "Rivers");

    SsapApplicant lauraApplicant = new SsapApplicant();
    lauraApplicant.setId(laura.getPersonId());
    lauraApplicant.setApplicantGuid(laura.getApplicantGuid());
    lauraApplicant.setFirstName(laura.getName().getFirstName());
    lauraApplicant.setLastName(laura.getName().getLastName());
    lauraApplicant.setSsapApplication(ssapApplication);

    List<SsapApplicant> dbApplicants = new ArrayList<>(Collections.singletonList(lauraApplicant));

    when(ssapApplicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long ssapId = invocation.getArgument(0, Long.class);
      log.info("applicantRepository.findBySsapApplicationId({})", ssapId);
      return dbApplicants;
    });

    when(applicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      savedApplicantList.add(applicant);
      return applicant;
    });

    Assert.assertEquals(BloodRelationshipCode.SELF, HouseholdUtil.getRelationOf(Collections.singletonList(laura), laura, laura));

    Assert.assertEquals("Primary tax filer person id should be Jane doe", 0, application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId());
    Assert.assertTrue(application.getTaxHousehold().get(0).isReconciledAptc());
    Assert.assertFalse("Failure to reconcile should be false for single tax household member", application.getTaxHousehold().get(0).isFailureToReconcile());

    SingleStreamlinedApplication savedApplication =
      applicationService.save(application, false, applicationSource, null, null);

    Assert.assertTrue(laura.getApplyingForCoverageIndicator());

    Assert.assertNotNull(savedApplication);

    HouseholdMemberResponse lauraEligibility = laura.getEligibilityResponse();

    Assert.assertNotNull(lauraEligibility);

    Assert.assertTrue(lauraEligibility.isExchangeEligible());

    Assert.assertTrue(getName(laura) + " should be QHP eligible", lauraEligibility.isExchangeEligible());

    Assert.assertTrue(getName(laura) + " should NOT be CSR eligible", lauraEligibility.isCsrEligible());
    Assert.assertFalse(getName(laura) +" should NOT be Medicaid eligible", lauraEligibility.isMedicaid());

    Assert.assertFalse(getName(laura) +" should NOT be CHIP eligible", lauraEligibility.isChip());

    Assert.assertTrue(getName(laura) +" should be APTC eligible", lauraEligibility.isAptcEligible());
  }

  private HouseholdMember getHouseholdMemberByName(SingleStreamlinedApplication application, String firstName, String lastName) {
    return application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm -> hhm.getName().getFirstName().equals(firstName)
      && hhm.getName().getLastName().equals(lastName)).findFirst().orElse(null);
  }

  private HouseholdMember getHouseholdByGuid(SingleStreamlinedApplication application, String applicantGuid) {
    return application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm -> hhm.getApplicantGuid().equals(applicantGuid)).findFirst().orElse(null);
  }

  private SsapApplication getSsapApplication(String jsonFile) {
    if (jsonFile == null) {
      jsonFile = TEST_JSON;
    }

    Household household = getHousehold();

    String nativeAmericanHh = "N";

    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson(jsonFile);
    SsapApplication ssapApplication = new SsapApplication();
    ssapApplication.setId(Long.parseLong(application.getApplicationGuid()));
    ssapApplication.setCaseNumber(application.getSsapApplicationId());
    ssapApplication.setFinancialAssistanceFlag(application.getApplyingForFinancialAssistanceIndicator() ? "Y" : "N");
    if (application.getApplicationType() != null) {
      ssapApplication.setApplicationType(application.getApplicationType());
    } else {
      System.err.println("Application Type is not set, using default: OE");
      ssapApplication.setApplicationType(SsapEnums.ApplicationTypeEnum.OE.getValue());
    }
    if (application.getApplicationStatus() != null) {
      ssapApplication.setApplicationStatus(application.getApplicationStatus().getApplicationStatusCode());
    } else {
      System.err.println("Application status code is not set, using OP"); // SG
      ssapApplication.setApplicationStatus(ApplicationStatus.OPEN.getApplicationStatusCode());
    }

    ssapApplication.setCmrHouseoldId(BigDecimal.valueOf(household.getId()));
    ssapApplication.setCoverageYear(application.getCoverageYear());
    ssapApplication.setCurrentPageId(String.valueOf(application.getCurrentPage()));
    if (application.getEligibilityStatus() != null) {
      ssapApplication.setEligibilityStatus(application.getEligibilityStatus());
    } else {
      System.err.println("Eligibility status is not set for this application, using default: PE");
      ssapApplication.setEligibilityStatus(EligibilityStatus.PE);
    }

    ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);

    if (application.getApplicationSignatureDate() == null) {
      application.setApplicationSignatureDate(new Date());
    }
    ssapApplication.setEsignDate(new java.sql.Timestamp(application.getApplicationSignatureDate().getTime()));
    ssapApplication.setNativeAmerican(nativeAmericanHh);

    JsonUtil.setApplicationData(ssapApplication, application);

    return ssapApplication;
  }

  private AccountUser getSampleUser() {
    Household household = getHousehold();

    ModuleUser activeModule = new ModuleUser();
    activeModule.setId(household.getId());
    activeModule.setModuleId(household.getId());

    Set<RolePermission> permissions = new HashSet<>();
    Permission p = new Permission();
    p.setIsDefault(Permission.DefaultFlag.Y);
    p.setName("SSAP_EDIT");
    p.setDescription("Default Permission");
    p.setId(1);

    RolePermission permission = new RolePermission();
    permission.setId(1);
    permission.setPermission(p);

    Role role = new Role();
    role.setId(1);
    role.setName("CUSTOMER");
    role.setPrivileged(0);
    role.setRolePermissions(permissions);

    AccountUser user = new AccountUser();
    user.setId(2);
    user.setActiveModule(activeModule);
    user.setAuthenticated(true);
    user.setConfirmed(1);
    user.setRetryCount(0);
    user.setDefRole(role);
    user.setActiveModuleId(household.getId());

    return user;
  }

  private String getName(HouseholdMember member) {
    return String.format("%s %s", member.getName().getFirstName(), member.getName().getLastName());
  }

  private Household getHousehold() {
    Household household = platformGson.fromJson(" {\n" +
      "    \"id\": 1435,\n" +
      "    \"aptc\": null,\n" +
      "    \"countyCode\": null,\n" +
      "    \"createdBy\": null,\n" +
      "    \"created\": \"2019-08-02 01:51:42.128000\",\n" +
      "    \"csr\": null,\n" +
      "    \"elig_lead_id\": 1677,\n" +
      "    \"email\": \"la_aws_test2_dt11022018_tm174513_5807@sharklasers.com\",\n" +
      "    \"familySize\": null,\n" +
      "    \"householdIncome\": null,\n" +
      "    \"updatedBy\": 1,\n" +
      "    \"updated\": \"2019-09-04 15:50:31.958000\",\n" +
      "    \"lastVisited\": null,\n" +
      "    \"numberOfApplicants\": null,\n" +
      "    \"phoneNumber\": \"2025547416\",\n" +
      "    \"premium\": null,\n" +
      "    \"stage\": null,\n" +
      "    \"status\": null,\n" +
      "    \"userId\": null,\n" +
      "    \"zipCode\": \"68501\",\n" +
      "    \"ffm_household_id\": \"1269411579592468486\",\n" +
      "    \"affiliate\": null,\n" +
      "    \"firstName\": \"Firstperson\",\n" +
      "    \"lastName\": \"Jones\",\n" +
      "    \"state\": null,\n" +
      "    \"ffmResponse\": null,\n" +
      "    \"giHouseholdId\": \"8e2a91643d2f475c9feab5698b54a14a\",\n" +
      "    \"ridpVerified\": \"Y\",\n" +
      "    \"dshReferenceNumber\": null,\n" +
      "    \"consentToCallback\": null,\n" +
      "    \"servicedBy\": null,\n" +
      "    \"contact_location_id\": 17235,\n" +
      "    \"middleName\": null,\n" +
      "    \"nameSuffix\": null,\n" +
      "    \"birthDate\": \"1975-01-01 00:00:00.000000\",\n" +
      "    \"ridpSource\": \"REFERRAL\",\n" +
      "    \"ridpDate\": \"2019-09-04 15:50:31.945000\",\n" +
      "    \"healthAdvocate\": null,\n" +
      "    \"mobilePhone\": null,\n" +
      "    \"homePhone\": null,\n" +
      "    \"leadStatus\": null,\n" +
      "    \"location_id\": 17236,\n" +
      "    \"prefSpokenLang\": null,\n" +
      "    \"prefWrittenLang\": null,\n" +
      "    \"prefContactMethod\": 1781,\n" +
      "    \"tenantId\": null,\n" +
      "    \"poaPhone\": null,\n" +
      "    \"household_extension\": null,\n" +
      "    \"ssn\": null,\n" +
      "    \"ssnNotProvidedReason\": null,\n" +
      "    \"textMe\": null,\n" +
      "    \"mobileNumberVerified\": null,\n" +
      "    \"householdCaseId\": \"467601\",\n" +
      "    \"optInPaperless1095\": null,\n" +
      "    \"paperless1095User\": null,\n" +
      "    \"paperless1095Date\": null,\n" +
      "    \"preferencesReviewDate\": \"2019-08-02 01:51:42.128000\"\n" +
      "  }", Household.class);

    return household;
  }
}
