﻿define(function (required) {
    var dropdown = new baseComponent();
    dropdown.type = "dropdown";
    dropdown.getHTML = function (cgfObj) {
        var defCfg = {
            visible: true,
            disabled: false,
            requiredMessage: cgfObj.caption + " required"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var $retobj = $("<div class=\"control-group\"></div>");
        var $label = $("<label class=\"control-label\" for=\"" + compData.name + "\">" + compData.caption + "</label>");
        $retobj.append($label);
        var $controls = $("<div class=\"controls\"></div>");
        $retobj.append($controls);
        var $dropdown = $("<select ng-model=\"" + compData.name + "\"  placeholder=\"'Select'\"  ng-options=\"option.text for option in " + compData.name + "List track by option.value\" ></select>");
        $controls.append($dropdown);
        if (compData.required) {
            $label.append("<img src=\"" + baseURL + "../../images/requiredAsterix.png\" width=\"10\" height=\"10\" alt=\"Required!\" aria-hidden=\"true\">");
            var $errorHolder = $("<span for=\"" + compData.name + "\" class=\"error\"><label class=\"error\">" + compData.requiredMessage + "</label></span>");
            var reqStr = "runValidate && ";
            reqStr += "(##formName##." + compData.name + ".$error.required";
            reqStr += " || " + compData.name + ".value.trim() == '')";
            $errorHolder.attr("ng-show", reqStr);
            $controls.append($errorHolder);
        };
        $dropdown.attr("id", compData.name);
        $dropdown.attr("name", compData.name);
        if (compData.disabled) {
            $dropdown.attr("disabled", "disabled");
        };
        if (compData.onChange) {
            $dropdown.attr("ng-change", compData.onChange);
        };

        if (compData.attrs) {
            for (attr in compData.attrs) {
                $retobj.attr(attr, compData.attrs[attr]);
            };
            //$.each(compData.attrs, function (idx,attr) {
            //    $retobj.attr(attr)
            //});
        };

       

        if (compData.visible == false) {
            $retobj.addClass("hide");
        };

        return $retobj[0].outerHTML;
    };
    return dropdown;
});