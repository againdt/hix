<script type="text/ng-template" id="maritalstatus-income.jsp">
  <div class="gutter10" ng-controller="mainIncomeController" >
    <h3><spring:message code="label.lce.income"/></h3>
    <form class="form-horizontal" id="maritalStatusIncome" name="maritalStatusIncome" novalidate>

      <spring:message code="label.lce.whatis"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.expectedincomefor2015"/>
      <div class="control-group margin30-b">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls margin10-t">
            <div class="input-prepend">
              <span class="add-on">$</span>
              <input type="text" class="span8"  name="expectedYearlyIncome" ng-model="householdMember.expeditedIncome.irsReportedAnnualIncome" required numbers-Only numbers-only-replace-zero="false" format-zero-as-blank="fasle"/>
            </div>
          </div>
          <!-- Error -->
          <div class="controls margin5-t">
            <span class="validation-error-text-container" ng-show="submitted && maritalStatusIncome.expectedYearlyIncome.$error.required">Please enter <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}'s</strong> expected yearly income.</span>
            <span class="validation-error-text-container" ng-show="submitted && maritalStatusIncome.expectedYearlyIncome.$error.number">Please enter a valid expected yearly income.</span>
          </div>
      </div>
	 <%--
      <spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.haveotherincome"/>
      <div class="control-group margin30-b">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls">
          <label class="radio inline" for="otherincome">
            <input type="radio" id="otherincomeYes" name="otherincome"  value="Yes"  ng-model="eventInfoArrayTemp[originalIndexHsHldIndex].hasOtherIncomeTypes"><label for="otherincomeYes"> <spring:message code="label.lce.yes"/></label>
          </label>
          <label class="radio inline">
            <input type="radio" id="otherincomeNo" name="otherincome" value="No" ng-model="eventInfoArrayTemp[originalIndexHsHldIndex].hasOtherIncomeTypes" ng-change="initIncomes();"> <label for="otherincomeNo"><spring:message code="label.lce.no"/></label>
          </label>
        </div>
      </div>

  	 <!-- If user has other income this displays -->
     <div class="control-group" ng-show="eventInfoArrayTemp[originalIndexHsHldIndex].hasOtherIncomeTypes=='Yes'">
       <label class="control-label" for=""><spring:message code="label.lce.incomesources"/>: </label>
       <div class="controls" ng-repeat="income in individualIncomeTypes">
         <!--<input type="checkbox" id="incomeTypes{{income.type}}" name="incomeTypes" value='{{income.type}}' ng-click="selectIncome()"> {{income.type}} -->
         <!-- If you add  value='{{income.type}}' to the input tag the radio buttons will not pre-select-->
         <label class="checkbox inline">
           <input type="checkbox" ng-model="income.value" name="incomeTypes" ng-required="eventInfoArrayTemp[originalIndexHsHldIndex].hasOtherIncomeTypes=='Yes' && selectedIncomeCount()==0"> {{income.type}}
         </label>
       </div>
       <div class="controls margin5-t">
         <span class="validation-error-text-container" ng-show="submitted && maritalStatusIncome.incomeTypes.$error.required"><spring:message code="label.lce.pleaseselectincometypes"/></span>
       </div>
     </div>--%><!-- If user has other income this displays -->
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="loadPreviousHouseHold();back();"><spring:message code="label.lce.back"/></a>
      <a ng-show="maritalStatusIncome.$valid" class="btn btn-primary" ng-click="selectIncome();next();"><spring:message code="label.lce.next"/></a>
      <a ng-show="!maritalStatusIncome.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>