(function(){
	'use strict';
	
	angular.module('resetPasswordApp').config(routerConfig);
	
	routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routerConfig($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise("/");
		
		$stateProvider.state('authenticationInformation', {
			url: "/",
			templateUrl: "/hix/resources/html/cap/resetPassword/authenticationInformationPartial.html",
			controller: 'AuthenticationInformationCtrl',
			controllerAs: 'vm'
		}).state('successEmail', {
			url: "/",
			params : { email: undefined},
			templateUrl: "/hix/resources/html/cap/resetPassword/successEmailPartial.html",
			controller: 'EmailSentCtrl',
			controllerAs: 'vm'
		});
	}
	
})();
