﻿define(['textbox'], function (txtbox) {
    var autocomplete = new baseComponent();
    autocomplete.type = "autocomplete";
    autocomplete.getHTML = function (cgfObj) {
        var defCfg = {
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var vm = this;
        vm.context.registerComponent(txtbox);
        var textbox = vm.context.comp["textbox"].getHTML(compData);
        var $autocomplete = $(textbox);
        var $input = $autocomplete.find("input#"+compData.name);
        $input.attr("kendo-auto-complete", "").removeClass("k-textbox").attr("k-data-source", compData.name + "AutoList");
        return $autocomplete[0].outerHTML;
    };
    return autocomplete;
});