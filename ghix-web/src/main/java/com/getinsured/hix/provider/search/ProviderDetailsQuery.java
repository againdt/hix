package com.getinsured.hix.provider.search;

import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class ProviderDetailsQuery extends ProviderQuery{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderDetailsQuery.class);
	@Value("#{configProp['provider.details.rfields']}")
	private String resultSetFields = "group_key,first_name,last_name,middle_name,address,specialty_code,languages_spoken,provider_class,professional_title,suffix,network_id,network_tier_id,provider_class,facility_name,accepting_new_patients,medical_group_affiliation,hospital_affiliation,accessibility_codes,location_id,sanction_status"; // default

	public ProviderDetailsQuery(){
		super();
	}
	/**
	 * Object returned by this call is provider details
	 * count
	 * @param groupKey: Provider by user
	 * @return Solr Query object
	 * @throws IllegalStateException
	 */
	public SolrQuery getQuery(String groupKey) throws IllegalStateException {
		
		if(this.query == null){
			this.query = new SolrQuery();
			this.query.set("q", "group_key:"+groupKey);
			if(this.providerClassStr != null){
				this.query.setFilterQueries("provider_class:"+this.providerClassStr);
			}			
			if(this.userZipCode != null && this.proxymity > 0){
				this.query.set("sfield", "prac_location");
				this.query.set("pt", this.latLonStr);
				this.query.set("d", Float.toString(this.proxymity));
				this.resultSetFields += ",_dist_:geodist()";
			}else{
				LOGGER.warn("failed to get user geo location for zip code:"+this.userZipCode+" performing query without geo distance");
			}
			this.query.set("fl", resultSetFields);
			this.query.set("wt", this.resultFormat);
		}
		LOGGER.debug("Provider detail Query:"+query);
		return query;
	}
}
