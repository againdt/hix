package com.getinsured.hix.individual.service;

import com.getinsured.hix.model.ExternalIndividual;

/**
 * Encapsulates service layer method call for External Individual
 */
public interface ExternalIndividualService {

	/**
	 * Retrieves external individual based on id
	 * 
	 * @param externalIndId
	 *            external individual id
	 * @return {@link ExternalIndividual}
	 */
	ExternalIndividual findByExternalIndividualID(long externalIndId);
}