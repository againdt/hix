package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.migration.couchbase.ecm.DataConfig.MigrationStepEnum;
import com.getinsured.hix.dto.platform.ecm.CMISErrors;
import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument;

public class LinkDocumentsTask extends BaseEcmCouchMigrationClass {
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkDocumentsTask.class);
	private JdbcTemplate jdbcTemplate;
	private CouchBucketService couchBucketService;

	public LinkDocumentsTask() {
		super(MigrationStepEnum.UPDATE);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setCouchBucketService(CouchBucketService couchBucketService) {
		this.couchBucketService = couchBucketService;
	}

	@Override
	void startExecution() throws Exception {
		Connection con = null;
		try {
			LOGGER.info("Initiate the process of linking documents in db with couchbase");
			con = jdbcTemplate.getDataSource().getConnection();
			for (CouchbaseECMMigrationDTO tableDTO : DataConfig.migrationTables.values()) {
				LOGGER.info("Linking documents from Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName());
				int maxPrimaryRecordId = extractMaximumPrimaryRecordId(con, tableDTO.getTableName(),
						tableDTO.getColumnName(), DataConfig.MigrationStatusEnum.COUCHBASEDONE);
				LOGGER.info("Maximum Primary Id for Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName() + " is - " + maxPrimaryRecordId);
				if (maxPrimaryRecordId != 0) {
					startDocumentLinking(con, tableDTO.getTableName(), tableDTO.getColumnName(),
							tableDTO.getPrimaryIdColumn(), maxPrimaryRecordId);
				}
			}
			LOGGER.info("Process of linking documents in db with couchbase completes successfully");
		} catch (Exception e) {
			LOGGER.error("Encountered exception in process of linking documents in db with couchbase", e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.setAutoCommit(true);
				} catch (Exception e2) {

				}

				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}
	}

	private void startDocumentLinking(Connection con, String tableName, String columnName, String primaryIdColumn,
			int maxPrimaryRecordId) throws Exception {
		Pagination paging = new Pagination(maxPrimaryRecordId);
		List<Map<String, String>> data;
		int iSize;
		while (paging.isNext()) {
			LOGGER.info("Link records for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			data = fetchRecordsToLink(con, tableName, columnName, paging.getStartRecordNumber(),
					paging.getEndRecordNumber());
			iSize = CouchMigrationUtil.listSize(data);

			if (iSize == 0) {
				continue;
			}
			LOGGER.info(iSize + " - records found for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			linkDocument(con, tableName, columnName, primaryIdColumn, data);
		}
	}

	private void linkDocument(Connection con, String tableName, String columnName, String primaryIdColumn,
			List<Map<String, String>> data) {
		// boolean blnCheck;
		for (Map<String, String> map : data) {
			if (CouchMigrationUtil.isValidString(map.get(DataConfig.COUCHBASE_ID))) {
				/** Dont update couchbase */
				/*
				 * blnCheck = updateCouchbase(map); if (blnCheck) {
				 * updateDb(con, map, tableName, columnName, primaryIdColumn); }
				 */
				updateDb(con, map, tableName, columnName, primaryIdColumn);
			}
		}
	}

	@SuppressWarnings("unused")
	private void updateDb(Connection con, Map<String, String> map, String tableName, String columnName,
			String primaryIdColumn) {
		try {
			con.setAutoCommit(false);
			StringBuilder st1 = new StringBuilder(50);
			st1.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
					.append(DataConfig.MigrationStatusEnum.MIGRATIONDONE.name()).append("' where MIGRATION_ID = ")
					.append(map.get(DataConfig.MIGRATION_ID));
			StringBuilder st2 = new StringBuilder(50);
			st2.append("update ").append(tableName).append(" set ").append(columnName).append(" = '")
					.append(map.get(DataConfig.COUCHBASE_ID)).append("' where ").append(primaryIdColumn).append(" = ")
					.append(map.get(DataConfig.PRIMARY_ID));

			int[] ireturn = JDBCUtil.executeBatchUpdate(con, new String[] { st1.toString(), st2.toString() });

			con.commit();
			st1 = null;
			st2 = null;
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception e2) {
				/** TODO should we eat the exception */
			}
		}
	}

	@SuppressWarnings("unused")
	private boolean updateCouchbase(Map<String, String> map) {
		boolean blnReturn = false;
		try {
			CouchEcmDocument document = couchBucketService.getDocumentById(map.get(DataConfig.COUCHBASE_ID),
					CouchEcmDocument.class);
			if (null == document) {
				throw new ContentManagementServiceException(CMISErrors.COUCH_DOCUMENT_NOT_FOUND);
			}
			//document.getCustomMetaData().put(DataConfig.MIGRATION_STATUS, DataConfig.DONE);
			final String documentId = couchBucketService.updateDocument(document);
			blnReturn = true;
		} catch (Exception e) {
			/** TODO should we eat the exception */
		}
		return blnReturn;
	}

	private List<Map<String, String>> fetchRecordsToLink(Connection con, String tableName, String columnName,
			int startRecordNumber, int endRecordNumber) throws SQLException {
		StringBuilder st = new StringBuilder(50);
		st.append("select MIGRATION_ID, PRIMARY_ID, COUCHBASE_ID from ").append(DataConfig.COUCH_MIGRATION)
				.append(" where TABLE_NAME = '").append(tableName).append("' and COLUMN_NAME = '").append(columnName)
				.append("' and MIGRATION_STATUS in ('").append(DataConfig.MigrationStatusEnum.COUCHBASEDONE.name())
				.append("')").append(" and PRIMARY_ID between ").append(startRecordNumber).append(" and ")
				.append(endRecordNumber);

		return JDBCUtil.records(con, st.toString(),
				new String[] { DataConfig.MIGRATION_ID, DataConfig.PRIMARY_ID, DataConfig.COUCHBASE_ID });
	}

}
