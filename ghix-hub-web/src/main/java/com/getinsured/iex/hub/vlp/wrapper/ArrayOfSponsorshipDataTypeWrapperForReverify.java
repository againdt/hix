package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ArrayOfSponsorshipDataType;

/**
 * Wrapper Class for ArrayOfSponsorshipDataType
 * 
 * @author Nikhil Talreja
 * @since  14-Feb-2014 
 * 
 */
public class ArrayOfSponsorshipDataTypeWrapperForReverify implements JSONAware{
	
private List<SponsorshipDataTypeWrapperForReverify> sponsorshipData;
	
	private static final String SPONSORSHIP_DATE_KEY = "SponsorshipData";
	
	public ArrayOfSponsorshipDataTypeWrapperForReverify(ArrayOfSponsorshipDataType arrayOfSponsorshipDataType){
		
		if(arrayOfSponsorshipDataType == null){
			return;
		}
		
		for(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.SponsorshipDataType sponsorshipDataType : arrayOfSponsorshipDataType.getSponsorshipData()){
			this.getSponsorshipData().add(new SponsorshipDataTypeWrapperForReverify(sponsorshipDataType));
		}
	}

	public List<SponsorshipDataTypeWrapperForReverify> getSponsorshipData() {
		
		if(this.sponsorshipData == null){
			this.sponsorshipData = new ArrayList<SponsorshipDataTypeWrapperForReverify>();
		}
		
		return sponsorshipData;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray sponsorshipDataArray = new JSONArray();
		if(this.sponsorshipData != null){
			for(SponsorshipDataTypeWrapperForReverify data : this.sponsorshipData){
				if(data != null){
					sponsorshipDataArray.add(data);
				}
			}
		}
		obj.put(SPONSORSHIP_DATE_KEY,sponsorshipDataArray);
		return obj.toJSONString();
	}
}
