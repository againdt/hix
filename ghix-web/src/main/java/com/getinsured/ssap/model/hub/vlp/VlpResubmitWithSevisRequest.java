package com.getinsured.ssap.model.hub.vlp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * TODO: Missing JavaDoc.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-29
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(value = {"applicationId", "clientIp", "CasePOCFullName", "CasePOCPhoneNumber", "payload"})
public class VlpResubmitWithSevisRequest {
  private long applicationId;
  private String clientIp;

  @JsonProperty("CasePOCFullName")
  private String casePocFullName;

  @JsonProperty("CasePOCPhoneNumber")
  private String casePocPhoneNumber;

  private Payload payload;

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonPropertyOrder(value = {"CaseNumber", "SEVISID", "FiveYearBarApplicabilityIndicator", "RequesterCommentsForHub"})
  public static class Payload {
    @JsonProperty("CaseNumber")
    private String caseNumber;
    @JsonProperty("SEVISID")
    private String sevisId;
    @JsonProperty("FiveYearBarApplicabilityIndicator")
    private boolean fiveYearBar;
    @JsonProperty("RequesterCommentsForHub")
    private String comments;

    public String getCaseNumber() {
      return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
      this.caseNumber = caseNumber;
    }

    public String getSevisId() {
      return sevisId;
    }

    public void setSevisId(String sevisId) {
      this.sevisId = sevisId;
    }

    public boolean isFiveYearBar() {
      return fiveYearBar;
    }

    public void setFiveYearBar(boolean fiveYearBar) {
      this.fiveYearBar = fiveYearBar;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    @Override
    public String toString() {
      return "Payload{" +
        "caseNumber='" + caseNumber + '\'' +
        ", sevisId='" + sevisId + '\'' +
        ", fiveYearBar=" + fiveYearBar +
        ", comments='" + comments + '\'' +
        '}';
    }
  }

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(long applicationId) {
    this.applicationId = applicationId;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public String getCasePocFullName() {
    return casePocFullName;
  }

  public void setCasePocFullName(String casePocFullName) {
    this.casePocFullName = casePocFullName;
  }

  public String getCasePocPhoneNumber() {
    return casePocPhoneNumber;
  }

  public void setCasePocPhoneNumber(String casePocPhoneNumber) {
    this.casePocPhoneNumber = casePocPhoneNumber;
  }

  public Payload getPayload() {
    return payload;
  }

  public void setPayload(Payload payload) {
    this.payload = payload;
  }

  @Override
  public String toString() {
    return "VlpResubmitWithSevisRequest{" +
      "applicationId=" + applicationId +
      ", clientIp='" + clientIp + '\'' +
      ", casePocFullName='" + casePocFullName + '\'' +
      ", casePocPhoneNumber='" + casePocPhoneNumber + '\'' +
      ", payload=" + payload +
      '}';
  }
}
