package com.getinsured.hix.ssap;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.client.SsapApplicationAccessor;
import com.getinsured.iex.dto.SsapApplicationResource;

@Controller("ReviewSummaryController")
@RequestMapping("/reviewSummarydownload")
public class ReviewSummaryController {
	
	@Autowired private ContentManagementService ecmService;
	@Autowired private RestTemplate restTemplate;
	//@Autowired private ReviewSummaryService reviewSummaryService;@Autowired private RestTemplate restTemplate;
	@Autowired private SsapApplicationAccessor ssapApplicationAccessorImpl;
	@Autowired private UserService userService;
	
	@RequestMapping(value = "/ssap/filedownloadbyid", method = RequestMethod.GET)
	public ModelAndView downloadFileById(HttpServletResponse response,
			@RequestParam("caseNumber") String caseNumber) throws ContentManagementServiceException, IOException {

		byte[] bytes = new byte[0];
		String ecmId;
		try {
			
			SsapApplicationResource ssapApplicationResource = ssapApplicationAccessorImpl.getSsapApplicationsByCaseNumberId(caseNumber);
			if(userService.getLoggedInUser().getActiveModuleId() != ssapApplicationResource.getCmrHouseoldId().intValueExact()) {
					throw new GIRuntimeException("Permission denied to download summary of other user.");
			}
			
			 ecmId = restTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL+"reviewSummary/pdfgeneraion?caseNumber="+caseNumber,String.class);
			 
			 if(ecmId!=null)
			 {
			bytes = ecmService.getContentDataById(ecmId);
			 }
		} catch (ContentManagementServiceException e) {
			throw new ContentManagementServiceException("Error downloading file from ecm - " + e);
		}catch (Exception e) {
			throw new GIRuntimeException(e.getMessage());
		}

		streamData(response, bytes,"ReviewSummary.pdf");
		return null;
	}

	private void streamData(HttpServletResponse response, byte[] bytes, String fileName) throws IOException {
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
		response.setContentType("application/pdf");
		FileCopyUtils.copy(bytes, response.getOutputStream());
	}
	
}
