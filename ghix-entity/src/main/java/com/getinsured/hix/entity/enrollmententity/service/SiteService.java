package com.getinsured.hix.entity.enrollmententity.service;

import java.util.List;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;

/**
 * Encapsulates service layer method call for Enrollment Entity SubSite
 */
public interface SiteService {
	/**
	 * Retrieves subsite based on id
	 * 
	 * @param <Site>
	 * 			  {@link Site}
	 * @param siteId
	 *            site Id
	 */
	Site findByEESiteId(int siteId);
	/**
	 * Saves Site 
	 * 
	 * @param Site
	 *            {@link Site}
	 */

	Site saveSite(Site site);
	/**
	 * Find Site based on entity 
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 */

	List<Site> findSiteByEntity(EnrollmentEntity enrollmentEntity);
	/**
	 * Find Site based on entity and site type
	 * 
	 * @param enrollmentEntityId
	 *            {@link EnrollmentEntity}
	 *  @param siteType
	 */

	List<Site> findByEnrollmentEntityAndSiteType(int enrollmentEntityId, String siteType);

}
