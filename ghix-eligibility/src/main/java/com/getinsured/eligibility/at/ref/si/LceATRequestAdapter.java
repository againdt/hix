package com.getinsured.eligibility.at.ref.si;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

/**
 * @author chopra_s
 * 
 */
@Component("lceATRequestAdapter")
@Scope("singleton")
@DependsOn("dynamicPropertiesUtil")
public class LceATRequestAdapter extends ATRequestAdapter {
	private static final Logger LOGGER = Logger.getLogger(LceATRequestAdapter.class);
	private String stateCode;

	@PostConstruct
	public void createStateContext() {
		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	@Override
	void populateAccountTransferProperties(AccountTransferRequestDTO accountTransferRequestDTO, ReferralResponse input) {
		LOGGER.info("LceATRequestAdapter - populateAccountTransferProperties ");
		accountTransferRequestDTO.setLCE(true);
		accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.LCE.value());
		accountTransferRequestDTO.setEnrolledApplicationId((long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID));
	}

	@Override
	String getStateCode() {
		return stateCode;
	}
}
