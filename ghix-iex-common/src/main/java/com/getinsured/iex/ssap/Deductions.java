
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Deductions {

    @Expose
    private Double alimonyDeductionAmount;
    @Expose
    private Double otherDeductionAmount;
    @Expose
    private String alimonyDeductionFrequency;
    @Expose
    private String studentLoanDeductionFrequency;
    @Expose
    private String deductionFrequency;
    @Expose
    private Double studentLoanDeductionAmount;
    @Expose
    private List<String> deductionType = new ArrayList<String>();
    @Expose
    private String otherDeductionFrequency;
    @Expose
    private Double deductionAmount;

    public Double getAlimonyDeductionAmount() {
        return alimonyDeductionAmount;
    }

    public void setAlimonyDeductionAmount(Double alimonyDeductionAmount) {
        this.alimonyDeductionAmount = alimonyDeductionAmount;
    }

    public Double getOtherDeductionAmount() {
        return otherDeductionAmount;
    }

    public void setOtherDeductionAmount(Double otherDeductionAmount) {
        this.otherDeductionAmount = otherDeductionAmount;
    }

    public String getAlimonyDeductionFrequency() {
        return alimonyDeductionFrequency;
    }

    public void setAlimonyDeductionFrequency(String alimonyDeductionFrequency) {
        this.alimonyDeductionFrequency = alimonyDeductionFrequency;
    }

    public String getStudentLoanDeductionFrequency() {
        return studentLoanDeductionFrequency;
    }

    public void setStudentLoanDeductionFrequency(String studentLoanDeductionFrequency) {
        this.studentLoanDeductionFrequency = studentLoanDeductionFrequency;
    }

    public String getDeductionFrequency() {
        return deductionFrequency;
    }

    public void setDeductionFrequency(String deductionFrequency) {
        this.deductionFrequency = deductionFrequency;
    }

    public Double getStudentLoanDeductionAmount() {
        return studentLoanDeductionAmount;
    }

    public void setStudentLoanDeductionAmount(Double studentLoanDeductionAmount) {
        this.studentLoanDeductionAmount = studentLoanDeductionAmount;
    }

    public List<String> getDeductionType() {
        return deductionType;
    }

    public void setDeductionType(List<String> deductionType) {
        this.deductionType = deductionType;
    }

    public String getOtherDeductionFrequency() {
        return otherDeductionFrequency;
    }

    public void setOtherDeductionFrequency(String otherDeductionFrequency) {
        this.otherDeductionFrequency = otherDeductionFrequency;
    }

    public Double getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(Double deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

}
