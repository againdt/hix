package com.getinsured.hix.referral.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.iex.util.ReferralUtil;

@Component("fileNameValidator")
@Scope("singleton")
public class FileNameValidator {
	
	private static List<String> fileExtnAllowed=new ArrayList<String>();
	
	public FileNameValidator() {
		fileExtnAllowed.add("pdf");
		fileExtnAllowed.add("jpeg");
		fileExtnAllowed.add("png");
		fileExtnAllowed.add("jpg");
		fileExtnAllowed.add("txt");
		fileExtnAllowed.add("doc");
		fileExtnAllowed.add("docx");
	}
	

	public boolean validate(String fileName) {
	
		boolean isvalid = false;
		if (ReferralUtil.isNotNullAndEmpty(fileName)) {
			String fileExtn = fileName.substring(fileName.lastIndexOf('.') + 1);
			System.out.println("Extn" + fileExtn);
			if (fileExtnAllowed.contains(fileExtn.toLowerCase())) {
				isvalid = true;
			}
		}
		return isvalid;
	}

}
