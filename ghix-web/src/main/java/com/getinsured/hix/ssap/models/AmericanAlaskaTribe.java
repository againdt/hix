package com.getinsured.hix.ssap.models;

public class AmericanAlaskaTribe {

	private String tribeName;
	private String tribeCode;
	
	public AmericanAlaskaTribe(){
		
	}
	
	public AmericanAlaskaTribe(String tribeCode, String tribeName){
		this.tribeCode = tribeCode;
		this.tribeName = tribeName;
	}
	
	public String getTribeName() {
		return tribeName;
	}
	public void setTribeName(String tribeName) {
		this.tribeName = tribeName;
	}
	public String getTribeCode() {
		return tribeCode;
	}
	public void setTribeCode(String tribeCode) {
		this.tribeCode = tribeCode;
	}
}
