import React from 'react';
import Controls from '../components/controls/Controls';

const ControlsContainer = props => {
    return (
        <div>
            <Controls  {...props}/>
        </div>
    );
};

export default ControlsContainer;