package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class AuthorizedRepresentative {

	@Expose
	private Boolean signatureisProofIndicator = null; // UI doesn't want anything preselected until user picks.
	@Expose
	private Phone[] phone = new Phone[3];
	@Expose
	private Boolean partOfOrganizationIndicator = null; // UI doesn't want anything preselected until user picks.
	@Expose
	private Address address = new Address();
	@Expose
	private Name name;
	@Expose
	private String emailAddress;
	@Expose
	private String organizationId;
	@Expose
	private String companyName;

	public AuthorizedRepresentative() {
		phone[0] = new Phone("CELL");
		phone[1] = new Phone("HOME");
		phone[2] = new Phone("WORK");
	}

	public Boolean getSignatureisProofIndicator() {
		return signatureisProofIndicator;
	}

	public void setSignatureisProofIndicator(Boolean signatureisProofIndicator) {
		this.signatureisProofIndicator = signatureisProofIndicator;
	}

	public Phone[] getPhone() {
		Phone[] newPhone = new Phone[phone.length];
		System.arraycopy(phone,0,newPhone,0,phone.length);
		return newPhone;
	}

	public void setPhone(Phone[] phone) {
		Phone[] newPhone = new Phone[phone.length];
		System.arraycopy(phone,0,newPhone,0,phone.length);
		this.phone = newPhone;
	}

	public Boolean getPartOfOrganizationIndicator() {
		return partOfOrganizationIndicator;
	}

	public void setPartOfOrganizationIndicator(Boolean partOfOrganizationIndicator) {
		this.partOfOrganizationIndicator = partOfOrganizationIndicator;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
