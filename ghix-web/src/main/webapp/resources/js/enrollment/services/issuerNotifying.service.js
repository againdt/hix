(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .factory('issuerNotifyingService', issuerNotifyingService);

    issuerNotifyingService.$inject = ['$rootScope'];

    function issuerNotifyingService ($rootScope) {
        return {
            subscribe: function(scope, callback) {
                var handler = $rootScope.$on('issuerChanged', callback);
                scope.$on('$destroy', handler);
            },

            notify: function(issuerData) {
                $rootScope.$emit('issuerChanged', issuerData);
            }
        };
    }
})();