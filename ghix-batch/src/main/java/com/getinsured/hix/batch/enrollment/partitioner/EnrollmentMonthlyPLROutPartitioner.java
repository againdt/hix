package com.getinsured.hix.batch.enrollment.partitioner;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPLRBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentPLROut;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrollmentMonthlyPLROutPartitioner")
@Scope("step")
public class EnrollmentMonthlyPLROutPartitioner implements Partitioner {
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private  String monthlyPLROutboundCommitInterval;
	private EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService;
	private EnrollmentPLROut enrollmentPLROut;
	private String strMonth;
	private String strYear;
	int applicableMonth;
	int applicableYear;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLROutPartitioner.class);
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		List<BatchJobExecution> batchExecutionList = enrollmentMonthlyPLRBatchService.getRunningBatchList("monthlyPLROutJob");
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			ExecutionContext executionContext = null;
			
			try{
				boolean status = EnrollmentUtils.cleanDirectory(EnrollmentUtils
						.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.PLR.toString(),
								EnrollmentConstants.TRANSFER_DIRECTION_OUT)
						.append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME).toString());
				LOGGER.info("Folder cleaning status in PLR Individual pertitioner is :: "+status);
				setApplicableMonthYear();
				if(stepExecution!=null){
					executionContext=stepExecution.getJobExecution().getExecutionContext();
					if(executionContext!=null){
						executionContext.put("month", applicableMonth);
						executionContext.put("year", applicableYear);
					}
				}
			enrollmentPLROut.resetAllFields();
			List<String> houseHoldIdList=enrollmentMonthlyPLRBatchService.getUniqueHouseHolds(applicableMonth,applicableYear);
			
					if(houseHoldIdList!=null && !houseHoldIdList.isEmpty()&& houseHoldIdList.size()>0){
						enrollmentPLROut.addAllToHouseholdCaseIds(houseHoldIdList);
						int maxHouseholdsPerFile = 1;
						int size = houseHoldIdList.size();
						if(this.monthlyPLROutboundCommitInterval !=null && !this.monthlyPLROutboundCommitInterval.isEmpty()){
							maxHouseholdsPerFile=Integer.valueOf(this.monthlyPLROutboundCommitInterval.trim());
						}
						int numberOfFiles = size / maxHouseholdsPerFile;
						if (size % maxHouseholdsPerFile != 0) {
							numberOfFiles++;
						}
						int firstIndex = 0;
						int lastIndex = 0;
						for (int i = 0; i < numberOfFiles; i++) {
							firstIndex = i * maxHouseholdsPerFile;
							lastIndex = (i + 1) * maxHouseholdsPerFile;
							if (lastIndex > size) {
								lastIndex = size;
							}

							ExecutionContext value = new ExecutionContext();
							value.putInt("startIndex", firstIndex);
							value.putInt("endIndex", lastIndex);
							value.putInt("partition",  i);
							value.putInt("month", applicableMonth);
							value.putInt("year", applicableYear);
							partitionMap.put("partition - " + i, value);
							LOGGER.debug("EnrollmentMonthlyPLROutPartitioner : partition():: partition map entry end for partition :: "+i);

						}
					}
					
			}catch(Exception e){
				LOGGER.error("EnrollmentMonthlyPLROutPartitioner failed to execute : "+ e.getMessage(), e);
				throw new RuntimeException("EnrollmentMonthlyPLROutPartitioner failed to execute : " + e.getMessage()) ;
			}
		return partitionMap;
		}
		return null;
	}
	public String getMonthlyPLROutboundCommitInterval() {
		return monthlyPLROutboundCommitInterval;
	}
	public void setMonthlyPLROutboundCommitInterval(
			String monthlyPLROutboundCommitInterval) {
		this.monthlyPLROutboundCommitInterval = monthlyPLROutboundCommitInterval;
	}
	public EnrollmentMonthlyPLRBatchService getEnrollmentMonthlyPLRBatchService() {
		return enrollmentMonthlyPLRBatchService;
	}
	public void setEnrollmentMonthlyPLRBatchService(
			EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService) {
		this.enrollmentMonthlyPLRBatchService = enrollmentMonthlyPLRBatchService;
	}
	public EnrollmentPLROut getEnrollmentPLROut() {
		return enrollmentPLROut;
	}
	public void setEnrollmentPLROut(EnrollmentPLROut enrollmentPLROut) {
		this.enrollmentPLROut = enrollmentPLROut;
	}
	
	
	private void setApplicableMonthYear() throws GIException{
		int intMonth = 0;
		int intYear = 0;

		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		int currentMonth = calObj.get(Calendar.MONTH);
		if(NumberUtils.isNumber(strMonth) && NumberUtils.isNumber(strYear)){
			intMonth = Integer.parseInt(strMonth);
			intYear = Integer.parseInt(strYear);
			if(((intMonth>0 && intMonth<=12) || intMonth==-1) && intYear <= currentYear){
				applicableMonth = intMonth;
				applicableYear = intYear;
				if(intYear < currentYear && intMonth == -1){
					applicableMonth = 0;
					applicableYear = intYear+1;
				}else if(intMonth == -1){
					applicableMonth = currentMonth;
					applicableYear = intYear;
				}else{
					applicableMonth--;
				}


			}else{
				throw new GIException("Please provide valid month(mm) in the range [1,12] (Jan - Dec) and year <= current year");
			}
		}else{
			throw new GIException("Please provide valid month(mm) and year(yyyy)");
		}

	}



	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = ""+Calendar.getInstance().get(Calendar.YEAR);
		} else {
			this.strYear = strYear;
		}
	}
	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		if ((null == strMonth) || (strMonth.equalsIgnoreCase("null")) || strMonth.isEmpty()) {
			this.strMonth = "-1";
		} else {
			this.strMonth = strMonth;
		}
	}

}
