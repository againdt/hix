package com.getinsured.iex.hub.rest.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.vlp37.model.VLP37CaseLog;
import com.getinsured.iex.hub.vlp37.repository.IVLP37CaseLogRepository;
import com.getinsured.iex.hub.vlp37.service.VLP_PostProcessor;
import com.getinsured.iex.hub.vlp37.servicehandler.ResubmitServiceHandler;
import com.getinsured.iex.hub.vlp37.servicehandler.RetrieveStep3CaseResolutionServiceHandler;


@Controller
public class VLP37Controller extends MessageProcessor  {
	
	@Autowired private WebServiceTemplate vlp37InitialVerifyServiceTemplate;
	@Autowired private WebServiceTemplate vlp37ReverifyServiceTemplate;
	@Autowired private WebServiceTemplate vlp37ResubServiceTemplate;
	@Autowired private WebServiceTemplate vlp37RetrieveStep2CaseResolutionServiceTemplate;
	@Autowired private WebServiceTemplate vlp37RetrieveStep3CaseResolutionServiceTemplate;
	@Autowired private WebServiceTemplate vlp37closeCaseServiceTemplate;
	@Autowired private VLP_PostProcessor vlpPostProcessor;
	@Autowired private IVLP37CaseLogRepository caseRepo;
	
	
	private  Logger LOGGER = LoggerFactory.getLogger(VLP37Controller.class);
	private static final String CASE_NUMBER_REGEX = "^([a-zA-Z0-9]){15}+$";
	private Pattern caseNumberPattern = Pattern.compile(CASE_NUMBER_REGEX);
	private static final String TRUE = "true";
	private static final String CASE_NUMBER = "caseNumber";
	private static final String SERVICE_NAME = "VLP";
	
	/**
	 * Step 1
	 * @param vlpRequestJSON
	 * @return
	 */
	@RequestMapping(value="/invokeInitialVerifRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeVerifiedLawfulPresence(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-1-H92");
		request.setEndpointOperationName("VLP_INITIAL_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_InitialVerification");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for VLP Step 1");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for VLP Step 1 service");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",objectIdentifier);
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlp37InitialVerifyServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	/**
	 * Step 1A
	 * @param vlpRequestJSON
	 * @return
	 */
	@RequestMapping(value="/invokeReverifyRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeReverify(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-1A-H92A");
		request.setEndpointOperationName("VLP_REVERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_ReVerification");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			String casePOCFullName = (String) requestObj.get("CasePOCFullName");
			String casePOCPhoneNumber = (String) requestObj.get("CasePOCPhoneNumber");
			if(casePOCFullName == null ||casePOCPhoneNumber == null) {
				throw new HubServiceException("Case POC full name and phone number are mandatory");
			}
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for VLP Step 1a");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for VLP Step 1a service");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put(APPLICATION_ID_KEY, requestObj.get(APPLICATION_ID_KEY));
			context.put("CasePOCFullName", casePOCFullName);
			context.put("CasePOCPhoneNumber", casePOCPhoneNumber);
			context.put("objectIdentifier",objectIdentifier);
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlp37ReverifyServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	/**
	 * Step 1B
	 * @param vlpRequestJSON
	 * @return
	 */
	@RequestMapping(value="/invokeResubmitRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeResubmit(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-1B-H92B");
		request.setEndpointOperationName("VLP_RESUBMIT_WITH_SEVISID");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_Resubmit");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			String casePOCFullName = (String) requestObj.get("CasePOCFullName");
			String casePOCPhoneNumber = (String) requestObj.get("CasePOCPhoneNumber");
			if(casePOCFullName == null ||casePOCPhoneNumber == null) {
				throw new HubServiceException("Case POC fiull name and phone number are mandatory");
			}
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for VLP Step 1b");
			}
			request.setAccessIp(clientIp);
			ResubmitServiceHandler handler = (ResubmitServiceHandler) vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for VLP Step 1b service");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put(APPLICATION_ID_KEY, requestObj.get(APPLICATION_ID_KEY));
			context.put("CasePOCFullName", casePOCFullName);
			context.put("CasePOCPhoneNumber", casePOCPhoneNumber);
			context.put("objectIdentifier",objectIdentifier);
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlp37ResubServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeRetrieveStep2CaseResolution", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeRetrieveStep2CaseResolution(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-H101");
		request.setEndpointOperationName("VLP_RETRIEVE_ADDTL_VERF_RESP");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_RetrieveStep2CaseResolution");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for Retreive Step 2 case resolution");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for  Retreive Step 2 case resolution service");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",objectIdentifier);
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlp37RetrieveStep2CaseResolutionServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeRetrieveStep3CaseResolution", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeRetrieveStep3CaseResolution(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-H101");
		request.setEndpointOperationName("VLP_RETRIEVE_THIRD_VERF_RESP");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_RetrieveStep3CaseResolution");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for Retreive Step 3 case resolution");
			}
			request.setAccessIp(clientIp);
			RetrieveStep3CaseResolutionServiceHandler handler = (RetrieveStep3CaseResolutionServiceHandler) vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for  Retreive Step 3 case resolution service");
			}
			//handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",objectIdentifier);
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlp37RetrieveStep3CaseResolutionServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	
	
	/**
	 * Provides the VLP Case log
	 * @param caseNumber
	 * @return
	 */
	@RequestMapping(value="/case/status/{caseNumber}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getCurrentStatus(@PathVariable String caseNumber) {
		ResponseEntity<String> response = null;
		try{
			Matcher matcher = this.caseNumberPattern.matcher(caseNumber);
			if(matcher.matches()) {
				VLP37CaseLog caseLog = this.caseRepo.getCaseLog(caseNumber);
				if(caseLog != null) {
					response = PlatformServiceUtil.getResponseEntity(caseLog.toJSONString(), HttpStatus.OK, null, MediaType.APPLICATION_JSON);
				}else {
					response = PlatformServiceUtil.getResponseEntity(
							getErrorJson(caseNumber, "No record Found"), HttpStatus.NOT_FOUND, null, MediaType.APPLICATION_JSON);
				}
			}else {
				response = PlatformServiceUtil.getResponseEntity(
						getErrorJson(null, "Empty or Invalid case number provider, A valid case number must be alpha nuberic value of exactly 15 characters"), 
						HttpStatus.NOT_FOUND, null, MediaType.APPLICATION_JSON);
			}
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value="/case/history/{caseNumber}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getCaseHistory(@PathVariable String caseNumber) {
		ResponseEntity<String> response = null;
		try{
			Matcher matcher = this.caseNumberPattern.matcher(caseNumber);
			if(matcher.matches()) {
				VLP37CaseLog caseLog = this.caseRepo.getCaseLog(caseNumber);
				if(caseLog != null) {
					response = PlatformServiceUtil.getResponseEntity(caseLog.toJSONString(), HttpStatus.OK, null, MediaType.APPLICATION_JSON);
				}else {
					response = PlatformServiceUtil.getResponseEntity(
							getErrorJson(caseNumber, "No record Found"), HttpStatus.NOT_FOUND, null, MediaType.APPLICATION_JSON);
				}
			}else {
				response = PlatformServiceUtil.getResponseEntity(
						getErrorJson(null, "Empty or Invalid case number provider, A valid case number must be alpha nuberic value of exactly 15 characters"), 
						HttpStatus.NOT_FOUND, null, MediaType.APPLICATION_JSON);
			}
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private String getErrorJson(String caseNumber, String message) {
		String caseN = "Not Available";
		JSONObject obj = new JSONObject();
		if(caseNumber != null) {
			caseN = caseNumber;
		}
		obj.put("CaseNumber", caseN);
		obj.put("Error", message);
		return obj.toJSONString();
	}
	
	

	
}
