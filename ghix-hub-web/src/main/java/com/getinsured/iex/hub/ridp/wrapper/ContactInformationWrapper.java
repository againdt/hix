package com.getinsured.iex.hub.ridp.wrapper;


import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.PrimaryRequestInformationType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.ObjectFactory;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.TelephoneNumberType;
import com.google.gson.Gson;

/**
 * This class represents the wrapper for ContactInformation element in the
 * PrimaryRequest for RIDP service.
 * 
 * 
 * @author Sunil Desu
 * @since 01/09/2014
 * @see JIRA HIX-26786
 * 
 */
public class ContactInformationWrapper {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContactInformationWrapper.class);
	
	private static ObjectFactory factory;
	private ContactInformationType contactInformationType = null;
	private String fullTelephoneNumber;
	
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	public ContactInformationWrapper() {
		factory = new ObjectFactory();
		this.contactInformationType = factory.createContactInformationType();
		
	}

	public void setFullTelephoneNumber(String contactTelephoneNumber) {
		if(contactTelephoneNumber != null && contactTelephoneNumber.trim().length() > 0){
		TelephoneNumberType telephoneNumberType = factory.createTelephoneNumberType();		
		telephoneNumberType.setFullTelephoneNumber(contactTelephoneNumber);
		contactInformationType.setContactTelephoneNumber(telephoneNumberType);
		this.fullTelephoneNumber = contactTelephoneNumber;
		}
	}

	public String getFullTelephoneNumber() {
		if(fullTelephoneNumber != null){
			return this.fullTelephoneNumber;
		}
		if (this.contactInformationType != null
				&& this.contactInformationType.getContactTelephoneNumber() != null
				&& this.contactInformationType.getContactTelephoneNumber().getFullTelephoneNumber() != null
				&& this.contactInformationType.getContactTelephoneNumber().getFullTelephoneNumber().trim().length() > 0) {
			
			return this.contactInformationType.getContactTelephoneNumber().getFullTelephoneNumber();
			
		}
		return null;
	}

	@JsonIgnore
	public ContactInformationType getSystemRepresentation() {
		return this.contactInformationType;
	}

	public static ContactInformationWrapper fromJsonString(String jsonString) throws IOException{
		return gson.fromJson(jsonString, ContactInformationWrapper.class);
	}
	
	public String toJsonString() throws JsonProcessingException{
		ContactInformationWrapper contactInformation = new ContactInformationWrapper();
		contactInformation.setFullTelephoneNumber(this.getFullTelephoneNumber());
		return gson.toJson(contactInformation, ContactInformationWrapper.class);
		
	}
	public static void main(String[] args) {
		com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory fac = new com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory();
		ContactInformationWrapper contactInformation = new ContactInformationWrapper();
		contactInformation.setFullTelephoneNumber("1234567890");

		PrimaryRequestInformationType req = fac.createPrimaryRequestInformationType();
		req.setContactInformation(contactInformation.getSystemRepresentation());

		JAXBElement<PrimaryRequestInformationType> pReq = fac.createPrimaryRequest(req);
		RequestPayloadType reqPayLoad = fac.createRequestPayloadType();
		reqPayLoad.setRequest(pReq);

		//JAXBElement<Object> reqObj = fac.createRequest(reqPayLoad);
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//m.marshal(reqObj, System.out);
			LOGGER.debug("\nPhoneNumber :"+ contactInformation.getFullTelephoneNumber());
			
			LOGGER.debug(contactInformation.toJsonString());
			ContactInformationWrapper contactInformation2 = ContactInformationWrapper.fromJsonString(contactInformation.toJsonString());
			LOGGER.debug(contactInformation2.getFullTelephoneNumber());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
		}
		
	}
}
