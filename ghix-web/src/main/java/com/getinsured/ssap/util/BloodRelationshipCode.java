package com.getinsured.ssap.util;

import java.util.HashSet;
import java.util.Set;

public enum BloodRelationshipCode
{
  OTHER("00"),
  SPOUSE("01"),
  PARENT_OF_CHILD("03"),
  PARENT_OF_ADOPTED_CHILD("03a"),
  PARENT_OF_FOSTER_CHILD("03f"),
  PARENT_CARETAKER_OF_THE_WARD("03w"),
  GRANDPARENT("04"),
  GRANDCHILD("05"),
  UNCLE_AUNT("06"),
  NEPHEW_NIECE("07"),
  FIRST_COUSIN("08"),
  ADOPTED_CHILD_SON_DAUGHTER("09"),
  FOSTER_CHILD_SON_DAUGHTER("10"),
  SON_OR_DAUGHTER_IN_LAW("11"),
  BROTHER_SISTER_IN_LAW("12"),
  MOTHER_FATHER_IN_LAW("13"),
  SIBLING_BROTHER_SISTER("14"),
  WARD_OF_GUARDIAN("15"),
  WARD_OF_COURT_APPOINTED_GUARDIAN("15c"),
  WARD_OF_PARENT_CARETAKER("15p"),
  STEPPARENT("16"),
  STEPCHILD("17"),
  SELF("18"),
  CHILD("19"),
  SPONSORED_DEPENDENT("23"),
  DEPENDENT_OF_MINOR_DEPENDENT("24"),
  FORMER_SPOUSE("25"),
  GUARDIAN("26"),
  COURT_APPOINTED_GUARDIAN("31"),
  COLLATERAL_DEPENDENT("38"),
  DOMESTIC_PARTNER("53"),
  ANNUITANT("60"),
  TRUSTEE("D2"),
  UNSPECIFIED_RELATIONSHIP("G8"),
  UNSPECIFIED_RELATIVE("G9"),
  PARENTS_DOMESTIC_PARTNER("03-53"),
  CHILD_OF_DOMESTIC_PARTNER("53-19");

  String code;

  BloodRelationshipCode(final String code)
  {
    this.code = code;
  }

  public String getCode()
  {
    return this.code;
  }

  /**
   * Returns blood relationship for given code.
   *
   * @param code {@link BloodRelationshipCode#getCode()}
   * @param eligibilityEngine if true, only returns {@link BloodRelationshipCode} supported by eligibility engine.
   * @return {@link BloodRelationshipCode}.
   */
  public static BloodRelationshipCode getBloodRelationForCode(final String code, final boolean eligibilityEngine) {
    BloodRelationshipCode bloodRelationshipCode = getBloodRelationForCode(code);

    // Eligibility engine only supports 4 types, SELF, SPOUSE, CHILD, OTHER.
    final Set<String> supportedCodes = new HashSet<>();
    supportedCodes.add(BloodRelationshipCode.SELF.getCode());
    supportedCodes.add(BloodRelationshipCode.SPOUSE.getCode());
    supportedCodes.add(BloodRelationshipCode.CHILD.getCode());
    supportedCodes.add(BloodRelationshipCode.STEPCHILD.getCode());

    if(eligibilityEngine && !supportedCodes.contains(code)) {
      bloodRelationshipCode = BloodRelationshipCode.OTHER;
    }

    if(bloodRelationshipCode == BloodRelationshipCode.STEPCHILD) {
      bloodRelationshipCode = BloodRelationshipCode.CHILD;
    }

    return bloodRelationshipCode;
  }

  public static BloodRelationshipCode getBloodRelationForCode(final String code)
  {
    for(BloodRelationshipCode relationshipCode : BloodRelationshipCode.values()) {
      if(relationshipCode.getCode().equals(code)) {
        return relationshipCode;
      }
    }

    return BloodRelationshipCode.SELF;
  }
}
