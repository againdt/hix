package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataField;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class LatLonAggregateProcessor implements ProviderDataFieldProcessor {

	private int index;
	private ValidationContext validationContext;
	private static Logger logger = LoggerFactory
			.getLogger(FieldAggregatingProcessor.class);

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.validationContext = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.validationContext;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException, InvalidOperationException {
		HashMap<String, String> zipcodeMap = null;
		String zip = null;
		String fieldSet = this.validationContext
				.getNamedConstraintField("field_set");
		@SuppressWarnings("unchecked")
		ArrayList<ProviderDataField> allFields = (ArrayList<ProviderDataField>) this.validationContext
				.getContextField("record_fields");
		String[] fields = null;
		String tmp = "";
		String tmpVal = null;
		if (fieldSet != null) {
			fields = fieldSet.split(",");
			for (String field : fields) {
				tmpVal = this.getFieldValue(allFields, field);
				if (tmpVal != null && tmpVal.length() > 0) {
					tmp += tmpVal + ",";
				}
			}
			if (tmp.length() == 0) {
				logger.debug("Lattitude and longitude data not provided, attempting to fix");
				zipcodeMap = (HashMap<String, String>) this.validationContext.getContextField("zipcodeMap");
				if(zipcodeMap == null){
					logger.warn("Context does not includes zip code map, can not fix the record");
					return "0.0,0.0";
				}
				zip = this.getFieldValue(allFields, "zip");
				if(zip == null || (tmp = zipcodeMap.get(zip)) == null){
					logger.warn("No zip code available in the context (zip code must appear before the location field in the data file) or in the map ["+zip+"], not attempting to fix geo location data");
					return "0.0,0.0";
				}
				this.validationContext.addContextInfo("field_modified", true);
				logger.info("Inserted zip code geo data for zip:"+zip+" LAT,LON:"+tmp);
			}else{
				tmp = tmp.substring(0, tmp.length() - 1);
			}
			String outputField = (String) this.validationContext
					.getContextField("output_field");
			if (outputField == null) {
				outputField = "prac_location";
			}
			this.validationContext.addContextInfo("output_field",
					outputField);
			this.validationContext.addContextInfo(outputField, tmp);
			logger.debug("Aggregation completed with value " + tmp);
		}
		return tmp;
	}

	private String getFieldValue(ArrayList<ProviderDataField> fieldList,
			String name) throws InvalidOperationException {
		String val;
		for (ProviderDataField pd : fieldList) {
			if (pd.getName().equalsIgnoreCase(name)) {
				val = (String) pd.getValue();
				return val;
			}
		}
		logger.debug("Failed Lookig up field with name:" + name);
		throw new InvalidOperationException("Failed to lookup field with name:"
				+ name);
	}

	private Object prepareOutput(Object value) {
		Object returnObj = null;
		String outputType = this.validationContext
				.getNamedConstraintField("output_type");
		if (outputType.equalsIgnoreCase("list")) {
			logger.debug("Packaging aggregation output [" + value + "] as list");
			ArrayList<Object> list = new ArrayList<Object>();
			list.add(value);
			returnObj = list;
		} else {
			returnObj = value;
		}
		logger.debug("Aggregate value output class"
				+ returnObj.getClass().getName() + " VALUE:" + returnObj);
		return returnObj;
	}
}
