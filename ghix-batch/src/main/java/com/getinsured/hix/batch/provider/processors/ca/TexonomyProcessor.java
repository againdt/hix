package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class TexonomyProcessor implements ProviderDataFieldProcessor {

//	private static HashMap<String, String> texonomyMap = new HashMap<String, String>();
	private static boolean sourceAvailable = false;
	private ValidationContext context;
	private int index;

	private Logger logger = LoggerFactory.getLogger(TexonomyProcessor.class);

	@SuppressWarnings("unchecked")
	@Override
	public Object process(Object objToBeValidated) throws ProviderValidationException {

		/* Existing Validation had the checked but new does not confirmed to this validation so, commented out.
		Object source = this.context.getContextField("texonomies");
		if(source == null){
			throw new ProviderValidationException("Can not process the texonomy field code, source data not available");
		}
		texonomyMap = (HashMap<String, String>)source;
		logger.debug("Source Texonomy Map has "+ texonomyMap.size()+" Mappings");*/

		String s = objToBeValidated.toString().trim();
		String[] fieldTaxonomies = s.split("\\|");
		ArrayList<String> taxonomyList = new ArrayList<String>();

		if (logger.isDebugEnabled()) {
			logger.debug("Splitting ["+s+"] for all texonomies");
			logger.debug("Received ["+fieldTaxonomies.length+"] texonomies");
		}
//		String desc;

		for(String taxonomy : fieldTaxonomies){

			if(taxonomy.length() == 0){
				logger.warn("Empty value for texonomy, skipping");
				continue;
			}

			/*desc = texonomyMap.get(texonomy.trim());
			if(desc == null){
				printDebugLog(texonomyMap, texonomy);
				throw new ProviderValidationException("No mapping available for texonomy code ["+texonomy+"]");
			}*/
			taxonomyList.add(taxonomy);
		}

		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null)? null: (String)tmp;

		if(outputField == null){
			//Not available in metadata file, create a default one
			outputField = "taxonomy_code";
			this.context.addContextInfo("output_field", outputField);
		}

		if(taxonomyList.size() > 0){

			if (logger.isDebugEnabled()) {
				logger.debug("Extracted "+taxonomyList.size()+" texonomies for field:"+outputField);
			}
			context.addContextInfo(outputField, taxonomyList);
		}
		return s;
	}

	/*private void printDebugLog(HashMap<String, String> texMap, String lookedUpvalue) {
		Set<Map.Entry<String, String>> s = texMap.entrySet();
		Iterator<Map.Entry<String, String>> cursor = s.iterator();
		while(cursor.hasNext()){
			Map.Entry<String, String> me = cursor.next();
			logger.debug("Looking for "+lookedUpvalue+" from Texonomy Key:"+me.getKey()+" Mapped to:"+me.getValue());
		}
	}*/

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
		
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
