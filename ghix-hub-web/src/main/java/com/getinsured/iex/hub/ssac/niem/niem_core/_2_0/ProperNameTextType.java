
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.extension.RestrictedGivenNameType;
import com.getinsured.iex.hub.ssac.extension.RestrictedMiddleNameType;
import com.getinsured.iex.hub.ssac.extension.RestrictedSurNameType;


/**
 * A data type for a word or phrase by which a person or thing is known, referred, or addressed.
 * 
 * <p>Java class for ProperNameTextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProperNameTextType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://niem.gov/niem/niem-core/2.0>TextType">
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProperNameTextType")
@XmlSeeAlso({
    RestrictedSurNameType.class,
    RestrictedMiddleNameType.class,
    RestrictedGivenNameType.class,
    PersonNameTextType.class
})
public class ProperNameTextType
    extends TextType
{


}
