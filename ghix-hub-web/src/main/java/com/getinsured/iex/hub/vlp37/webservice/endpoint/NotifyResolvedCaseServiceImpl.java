package com.getinsured.iex.hub.vlp37.webservice.endpoint;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp37.model.VLP37CaseLog;
import com.getinsured.iex.hub.vlp37.repository.IVLP37CaseLogRepository;
import com.getinsured.iex.hub.vlp37.webservice.CaseListArrayType;
import com.getinsured.iex.hub.vlp37.webservice.CaseListType;
import com.getinsured.iex.hub.vlp37.webservice.DHSVerifiedCasesRequestType;
import com.getinsured.iex.hub.vlp37.webservice.DHSVerifiedCasesResponseType;
import com.getinsured.iex.hub.vlp37.webservice.ObjectFactory;
import com.getinsured.iex.hub.vlp37.webservice.ResponseCodeType;
import com.getinsured.iex.hub.vlp37.webservice.ResponseMetadataType;


@Endpoint
public class NotifyResolvedCaseServiceImpl { 
	private static final String CASE_NUMBER_REGEX = "^([a-zA-Z0-9]){15}+$";
	private static Pattern caseNumberPattern = Pattern.compile(CASE_NUMBER_REGEX);
	private Logger logger = LoggerFactory.getLogger(NotifyResolvedCaseServiceImpl.class);
	private final String TARGET_NAMESPACE = "http://vlpdvc.ee.sim.dsh.cms.hhs.gov";
	private static final String SUCCESS = "HS000000";
	private static final String FAILED_VALIDATION = "HS000100";
	private static final String FAILED_NO_CASE = "HS000200";
	private static final String FAILED_UPDATE_DB = "HS000300";
	private static final String UNKNOWN_TYPE = "HS000400";
	private static final ObjectFactory factory = new ObjectFactory();
	
	
	@Autowired
	private IVLP37CaseLogRepository vlp37CaseRepo;
	
	@PayloadRoot(localPart = "DHSVerifiedCasesRequest", namespace = TARGET_NAMESPACE)
	@WebMethod(operationName = "dhsVerifiedCases", action = "http://localhost:8081/ghix/dhsVerifiedCases")
	@WebResult(name = "DHSVerifiedCasesResponse", targetNamespace = TARGET_NAMESPACE, partName = "parameters")
	public @ResponsePayload DHSVerifiedCasesResponseType notifyResolvedCases(@RequestPayload DHSVerifiedCasesRequestType request) {
		CaseListArrayType caseListArray = request.getCaseListArray();
		List<CaseListType> caseList = caseListArray.getCaseList();
		if(logger.isDebugEnabled()) {
			logger.debug("Received {} cases for resolution", caseList.size());
		}
		boolean success = false;
		int i = 0;
		for(CaseListType caseInfo: caseList) {
			if(logger.isDebugEnabled()) {
				logger.debug("Processing case {} for index {}", caseInfo.getCaseNumber(), i++);
			}
			success = this.processCase(caseInfo);
			if(!success) {
				logger.info("Failed to process the case log from callback...exiting");
				break;
			}
		}
		DHSVerifiedCasesResponseType response = factory.createDHSVerifiedCasesResponseType();
		ResponseMetadataType metadata = factory.createResponseMetadataType();
		JAXBElement<ResponseCodeType> responseCode = null;
		if(success) {
			responseCode = factory.createResponseCode(ResponseCodeType.HS_000000);
			metadata.setResponseDescriptionText("Success");
		}else {
			responseCode = factory.createResponseCode(ResponseCodeType.HX_009000);
			metadata.setResponseDescriptionText("Error processing the case info");
		}
		metadata.setResponseCode(responseCode.getValue());
		JAXBElement<ResponseMetadataType> meta = factory.createResponseMetadata(metadata);
		response.setResponseMetadata(meta.getValue());
		return response;
	}
	
	private boolean processCase(CaseListType request) {
		String caseNumber = request.getCaseNumber();
		String caseType = request.getCaseType();
		String responseCode = SUCCESS;
		Matcher matcher = caseNumberPattern.matcher(caseNumber);
		if(!matcher.matches()) {
			responseCode = FAILED_VALIDATION;
		}
		if(responseCode == SUCCESS) {
			VLP37CaseLog caseLog = vlp37CaseRepo.getCaseLog(caseNumber);
			if(caseLog == null) {
				logger.info("Case Number {} is not available in the case logs", caseNumber);
				responseCode = FAILED_NO_CASE;
			}
			if(responseCode == SUCCESS) {
				if(logger.isInfoEnabled()) {
					logger.info("Received Notification for Case {} for type {}, Existing Status ",caseNumber, caseType, caseLog.getStatus());
				}
				switch(caseType) {
					case "2":{
						logger.info("Case {} resolved at Step 2", caseNumber);
						caseLog.setStatus(RequestStatus.ADDL_VERIFY_RESPONSE_AVAILABLE.toString());
						break;
					}
					case "3":{
						logger.info("Case {} resolved at Step 3", caseNumber);
						caseLog.setStatus(RequestStatus.THIRD_VERIFY_RESPONSE_AVAILABLE.toString());
						break;
					}
					default :{
						responseCode = UNKNOWN_TYPE;
					}
				}
				if(responseCode == SUCCESS) {
					try {
						VLP37CaseLog updatedLog = this.vlp37CaseRepo.saveAndFlush(caseLog);
						if(updatedLog == null) { 
							responseCode = FAILED_UPDATE_DB;
						}
					}catch(Exception e) {
						logger.error("Error saving the log for case {}",caseNumber, e);
						responseCode = FAILED_UPDATE_DB;
					}
				}
			}
		}
		logger.info("Processed callback for case {} with status {}", caseNumber, responseCode);
		if(responseCode == SUCCESS ) {
			return true;
		}
		return false;
	}
	
}