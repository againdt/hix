<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.List, com.getinsured.hix.model.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
#menu {
	display:none;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}

.no-health-plan {
	background: #f9f7c1;
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.no-dental-plan {
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.cart {
	border: 1px solid #c9c9c9;
}

h3 {
	line-height: 25px;
	margin: 0
}

.darkblue {
	background: #0c9bd2;
	color: #fff;
}

.darkgreen {
	background: #008080;
	color: #fff;
}

.SILVER {
	background: url(../resources/img/silver-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.GOLD {
	background: url(../resources/img/gold-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.BRONZE {
	background: url(../resources/img/bronze-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.CATASTROPHIC {
	background: url(../resources/img/catastrophic-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.PLATINUM {
	background: url(../resources/img/platinum-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.planimg {
	margin-top: 20px;
	margin-left: 25px;
	padding-bottom: 10px;
}

.closeout {
	position: relative;
	top: 24px;
	right: 10px;
	margin: -19px 0 0 0;
}

.mini-cart {
	width: 380px;
	border: 4px solid #4f4f4f;
	background: #fff;
	position: absolute;
	z-index: 10;
}

.mini-cart h3 {
	color: #fff;
}

.mini-cart .header {
	background: #4f4f4f;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.info').tooltip();
		$('.remove').hide();
		$('.close').hover(function() {
			$(this).prev('div').toggle();
		});
	});

	function gotoparent() {
		window.top.location.href = document.getElementById("redirectLink").value;
	}
</script>





<div class="row-fluid">
	<div class="gutter10">
		<div class="row-fluid">
			<%-- <div class="span3" id="sidebar">

				<h4 class="graydrkbg gutter10">
					<spring:message code='enroll.chkOut' />
				</h4>
				<ol class="nav nav-list">
					<li class="done"> <i class="icon-ok"></i>
						<spring:message code='enroll.cart'/>
					</li>
					<li class="done"> <i class="icon-ok"></i>
						<spring:message code='enroll.eSignature'/>
					</li>
					<li class="active">
						<spring:message code='enroll.conf'/>
					</li>
				</ol>
			</div> --%>
			<!--sidebar-->

			<div class="" id="rightpanel">
				<c:if test="${errorMsg == ''}">
					<div class="gutter10">

						<h3>
							<spring:message code='enroll.orderConfirm.confirm' />
						</h3>
						<c:choose>
							<c:when test="${enrollment_type == enrollment_type_individual}">
								<p>
									<spring:message
										code='enroll.orderConfirm.confirm.individual.value' />
								</p>
							</c:when>
							<c:when test="${enrollment_type == enrollment_type_shop}">
								<p>
									<spring:message
										code='enroll.orderConfirm.confirm.employee.value'
										arguments="XXXXXX" />
								</p>
							</c:when>
						</c:choose>

						<form name="frmorderconfirm" id="frmorderconfirm"
							action="orderconfirmsubmit" method="post">
							<input type="hidden" name="cart_id" id="cart_id"
								value="${cartId}"> <input type="hidden"
								name="redirectLink" id="redirectLink" value="${redirectLink}">


							<c:if test="${fn:length(enrollmentMap) gt 0}">
								<c:forEach var="enrollmentMapEntry" items="${enrollmentMap}">
									<c:set var="enrollmentlist" value="${enrollmentMapEntry.value}"></c:set>
									<c:if test="${fn:length(enrollmentlist) gt 0}">
										<div class="cart">
											<div class="header">
												<h4><spring:message code='enroll.orderConfirm.${enrollmentMapEntry.key}' /></h4>
											</div>

											<c:forEach items="${enrollmentlist}" var="enrollmentObj">
												<fmt:formatDate pattern="MM/dd/yyyy"
													value="${enrollmentObj.benefitEffectiveDate}"
													var="effDate" />
												<div class="gutter10">
													<div class="health-plan">
														<c:choose>
															<c:when
																test="${enrollment_type == enrollment_type_individual}">
																<p>
																	<strong>${enrolleeNameMap[enrollmentObj.id]}</strong><span
																		class="pull-right"><spring:message
																			code='enroll.orderConfirm.effDate' /> &nbsp;<strong>${effDate}</strong>
																	</span>
																</p>
															</c:when>
															<c:when test="${enrollment_type == enrollment_type_shop}">
																<p>
																	${enrollmentObj.sponsorName}
																	<spring:message
																		code='enroll.orderConfirm.sponsoredPlanFor' />
																	&nbsp; <strong>
																		${enrolleeNameMap[enrollmentObj.id]}</strong><span
																		class="pull-right"><spring:message
																			code='enroll.orderConfirm.effDate' /> &nbsp; <strong>${effDate}</strong>
																	</span>
																</p>
															</c:when>
														</c:choose>
														<c:if test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
														<div class="pull-right closeout">
															<div class="remove pull-left">
																<spring:message code='enroll.orderConfirm.remove' />
															</div>
															<button type="button" title="x" class="close" data-dismiss="modal"
																aria-hidden="true">�</button>

														</div>
														</c:if>
														<table class="table table-border" >
															<tbody>
																<tr>
																	<c:choose>
																			<c:when
																				test="${enrollmentObj.planLevel == 'GOLD'}">
																				<td class="padding0 GOLD">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'SILVER'}">
																				<td class="padding0 SILVER">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'BRONZE'}">
																				<td class="padding0 BRONZE">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'PLATINUM'}">
																				<td class="padding0 PLATINUM">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'CATASTROPHIC'}">
																				<td class="padding0 CATASTROPHIC">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'HIGH' || enrollmentObj.planLevel == 'LOW'}">
																				<!-- <td class="padding0 CATASTROPHIC"> -->
																				<td class="padding0">
																					<div class="planimg">
																						<img class="carrierlogo" src='${enrollmentObj.issuerLogo}' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																						<br>
																					</div>
																				</td>
																			</c:when>
																	</c:choose>
																	<td><strong>${enrollmentObj.insurerName}</strong><br>
																		${enrollmentObj.planName}</td>
																	<td class="txt-right"><spring:message
																			code='enroll.orderConfirm.mnthlyPremium' /><br>
																		<c:choose>
																			<%-- <c:when
																				test="${enrollment_type == enrollment_type_individual}">
																				<spring:message code='enroll.orderConfirm.taxCredit' />
																			</c:when> --%>
																			<c:when
																				test="${enrollment_type == enrollment_type_shop}">
																				<spring:message
																					code='enroll.orderConfirm.employerContribution' />
																			</c:when>
																		</c:choose></td>



																	<td class="txt-right"><strong>$ <fmt:formatNumber
																				value="${enrollmentObj.grossPremiumAmt}"
																				minFractionDigits="2" />   
																				<c:choose>
																				<%-- <c:when
																					test="${enrollment_type == enrollment_type_individual}">
																					<fmt:formatNumber value="${enrollmentObj.aptcAmt}"
																						minFractionDigits="2" />
																				</c:when> --%>
																				<c:when
																					test="${enrollment_type == enrollment_type_shop}">
																					<br>-$
																					<fmt:formatNumber
																						value="${enrollmentObj.employerContribution}"
																						minFractionDigits="2" />
																				</c:when>
																			</c:choose>
																	</strong></td>
<!-- 																	<td></td> -->
																</tr>
																<c:if test="${fn:length(enrollmentMap) gt 0 || fn:length(enrollmentlist) gt 0}">
																	<%-- <tr class="blue">
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		
																		<td>
																		<c:choose>
																				<c:when
																					test="${enrollment_type == enrollment_type_individual}">
																					<spring:message code='enroll.orderConfirm.payment' />
																				</c:when>
																				<c:when
																					test="${enrollment_type == enrollment_type_shop}">
																					<spring:message
																						code='enroll.orderConfirm.payrollDeduction' />
																				</c:when>
	
																			</c:choose></td>
																			
																		<td colspan="2" class="txt-right"><h3>
																				$
																				<fmt:formatNumber
																					value="${enrollmentObj.netPremiumAmt}"
																					minFractionDigits="2" />
																				/<small>mo</small>
																			</h3></td>
																	</tr> --%>
																	
																	
																	
																	<tr class="blue">
																		<td colspan="5">
																			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="totalTable">
																				<tbody>
																					<tr>
																				    	<td width="75%" class="txt-right">
																				    		<c:choose>
																								<c:when test="${enrollment_type == enrollment_type_individual}">
																									<spring:message code='enroll.orderConfirm.payment' />
																								</c:when>
																								<c:when test="${enrollment_type == enrollment_type_shop}">
																									<spring:message code='enroll.orderConfirm.payrollDeduction' />
																								</c:when>
					
																							</c:choose>
																						</td>
																				    	<td width="25%" class="txt-right">
																				    		<h3>
																				    			$<fmt:formatNumber value="${enrollmentObj.netPremiumAmt}" minFractionDigits="2" />
																								/<small>mo</small>
																				    		</h3>
																				    	</td>
																					</tr>
																					<tr>
																						<td colspan="5" class="txt-right">
																							<c:choose>
																								<c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key != 'DENTAL'}">
																									<c:url value="/finance/paymentRedirect?finEnrlDataKey=${finEnrlHltDataKey}" var="healthPaymentURL"/>
																									<a href='${healthPaymentURL}' title="<spring:message code='enroll.orderConfirm.makePayment' />" class="btn"></i><spring:message code='enroll.orderConfirm.makePayment'/></a> 
																								</c:when>
																								<c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key == 'DENTAL'}">
																									<c:url value="/finance/paymentRedirect?finEnrlDataKey=${finEnrlDntDataKey}" var="dentalPaymentURL"/>
																									<a href='${dentalPaymentURL}' title="<spring:message code='enroll.orderConfirm.makePayment' />" class="btn"></i><spring:message code='enroll.orderConfirm.makePayment'/></a>
																								</c:when>
																							</c:choose>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</c:if>
															</tbody>
														</table>

													</div>

												</div>
											</c:forEach>
										</div>
									</c:if>
								</c:forEach>

								<c:choose>
									<%--Totals Table for Individual  --%>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<table class="table table-border-none table-condensed totalTable">
											<tr class="darkblue">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.allPlans" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalMonthlyPremium' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalMonthlyPremium}"
															minFractionDigits="2" />

													</h3></td>
											</tr>
											<tr class="darkblue">
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.mnthlyTaxCredit'/></td>
												<td class="txt-right"><h3>
														-$
														<fmt:formatNumber value="${totalAptcContribution}"
															minFractionDigits="2" />

													</h3></td>
											</tr>

											<tr class="darkgreen">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.cartTotal" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><a href="#" rel="tooltip" data-placement="top" data-original-title="You will use pre-tax dollars to pay for your insurance premiums."><spring:message code='enroll.orderConfirm.totalPayment' /></a></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalCost}"
															minFractionDigits="2" />
														/<small>mo</small>
													</h3></td>
											</tr>
										</table>
									</c:when>

									<%--Totals Table for Employee  --%>
									<c:when test="${enrollment_type == enrollment_type_shop}">
										<table class="table table-border-none table-condensed totalTable">
											<tr class="darkblue">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.allPlans" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalMonthlyPremium' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalMonthlyPremium}"
															minFractionDigits="2" />

													</h3></td>
											</tr>
											<tr class="darkblue">
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalEmployerContribution' /></td>
												<td class="txt-right"><h3>
														-$
														<fmt:formatNumber value="${totalEmployerContribution}"
															minFractionDigits="2" />

													</h3></td>
											</tr>

											<tr class="darkgreen">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.cartTotal" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><a href="#" rel="tooltip" data-placement="top" data-original-title="You will use pre-tax dollars to pay for your insurance premiums."><spring:message code='enroll.orderConfirm.totalPayrollDeduct' /></a></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalCost}"
															minFractionDigits="2" />
														/<small>mo</small>
													</h3></td>
											</tr>
										</table>
									</c:when>
								</c:choose>
							</c:if>
							<br>
							<c:if test="${showChngPlans==true}">

								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.mngPlanChng' />
								</h4>
								<div class="gutter10">
									<c:choose>
										<c:when
											test="${enrollment_type == enrollment_type_individual}">
											<p>
												<spring:message
													code='enroll.orderConfirm.individual.mngPlanChng.value' />
											</p>
										</c:when>
										<c:when test="${enrollment_type == enrollment_type_shop}">
											<p>
												<spring:message
													code='enroll.orderConfirm.employee.mngPlanChng.value' />
											</p>
										</c:when>
									</c:choose>
								</div>
							</c:if>
							<c:if test="${fn:length(disEnrollmentMap) gt 0}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disenrollmentsFromPlanTitle' />
								</h4>
								<div class="gutter10">
									<p>
										<spring:message code='enroll.orderConfirm.disenrollmentsFromPlan' />
									</p>
									
									<p>
									<c:forEach var="disEnrollment" items="${disEnrollmentMap}">
  										&nbsp<b><c:out value="${disEnrollment.key}"/>: &nbsp <c:out value="${disEnrollment.value}"/></b><br>
									</c:forEach>
									</p>
								</div>
							</c:if>
							<c:if test="${showDisc==true}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disclaimer' />
								</h4>
								<div class="gutter10">
									<c:choose>
										<c:when
											test="${enrollment_type == enrollment_type_individual}">
											<p>
												<spring:message
													code='enroll.orderConfirm.individual.disclaimer.value' />
											</p>
										</c:when>
										<c:when test="${enrollment_type == enrollment_type_shop}">
											<p>
												<spring:message
													code='enroll.orderConfirm.employee.disclaimer.value' />
											</p>
										</c:when>
									</c:choose>
								</div>
							</c:if>
							
							
							
							<div class="form-actions margin-top50">
								<div class="controls">
									<a href="#" title="<spring:message code='enroll.orderConfirm.overView' />" class="btn" onclick="window.print()"> <i class="icon-print"></i>
										 <spring:message
											code='enroll.orderConfirm.print' /> </a> <a href="#"
										onclick="gotoparent()" title="<spring:message
											code='enroll.orderConfirm.overView' />"class="btn btn-primary pull-right"><spring:message
											code='enroll.orderConfirm.overView' /></a>
								</div>
							</div>
						</form>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
function resizeimage() {
	$(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
		
		if($(this).parents('td')){
    		$(this).parents('td').width(maxWidth);
    	}
	    
		$(this).css('display','block');
    });
}
</script>