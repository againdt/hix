import React, {useEffect, useState} from 'react';
import ReactModal from 'react-modal';
import { FormattedMessage } from 'react-intl';

const POLLING_INTERVAL = 1000 * 10;
const APP_STATUS_CLOSED_STATE = 'CL';

function AlertAppStatusClosedContainer ({ssapApplicationId}) {
    let [isShowModal, setIsShowModal] = useState(false);
    let [savedInterval, setSavedInterval] = useState(null);

    const fetchAppStatus = async (ssapApplicationId) => {
        if (!ssapApplicationId) return;

        try {
            let response = await fetch(`/hix/indportal/getapplicationstatusbyid/${ssapApplicationId}`);
            // let response = await fetch('http://localhost:3000/getapplicationstatusbyid');
            const data = await response.json();
            console.error(`In AlertAppClosed: ssapApplicationId (${ssapApplicationId}), response from API = `);
            console.info(data);
            if (data.applicationStatus === APP_STATUS_CLOSED_STATE && !isShowModal) {
                setIsShowModal(true);
            }
    
            if (!savedInterval) {
                console.error('In AlertAppClosed: start interval only ONCE');
                savedInterval = setInterval(() => {
                    console.error('In AlertAppClosed: check if application status is closed');
                    fetchAppStatus(ssapApplicationId);
                }, POLLING_INTERVAL);
                setSavedInterval(savedInterval);
            }
        } catch (err) {
            console.error(`In AlertAppStatusClosedContainer: error in calling API = `);
            console.info(err);
        }
    }

    useEffect(() => {
        // console.error(`In AlertAppStatusClosedContainer: ssapApplicationId (${ssapApplicationId})`);
        fetchAppStatus(ssapApplicationId)
        return () => clearInterval(savedInterval);
    }, [ssapApplicationId]);

    /**.
     * Handle closing modal when user clicks on OK button
     */
    const handleCloseModal = () => {
        clearInterval(savedInterval);
        setSavedInterval(null);
        setIsShowModal(false);
        window.location = "./indportal#";
    };

    return (
        <ReactModal isOpen={isShowModal}
            onRequestClose={handleCloseModal}
            className="react-modal modal"
            overlayClassName="react-overlay">
            <div className="modal-body react-modal-body">
                <p>
                    <FormattedMessage id="appstatus.closed.warning"/>
                </p>
            </div>
            <div className="modal-footer react-modal-footer">
                <button onClick={handleCloseModal} className="ds-c-button ds-c-button--primary ds-u-margin--1">
                    <FormattedMessage id="dashboard.ok"/>
                </button>
            </div>
        </ReactModal>
    );
}

export default AlertAppStatusClosedContainer;