<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="stateCode" value="${StateConfiguration.stateCode}" />
<c:if test="${stateCode == 'CA'}">
  <jsp:include page="./ca/leftNav.jsp" />
</c:if>
<c:if test="${stateCode == 'CT'}">
  <jsp:include page="./common/leftNav.jsp" />
</c:if>
<c:if test="${stateCode eq 'ID'}">
  <jsp:include page="./id/leftNav.jsp" />
</c:if>
<c:if test="${stateCode == 'MN'}">
  <jsp:include page="./mn/leftNav.jsp" />
</c:if>
<c:if test="${stateCode == 'NV'}">
  <jsp:include page="./nv/leftNav.jsp" />
</c:if>
<c:if test="${stateCode == 'WA'}">
  <jsp:include page="./common/leftNav.jsp" />
</c:if>
