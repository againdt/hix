package com.getinsured.eligibility;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.eligibility.at.dto.ProgramEligibilityResponseDTO;
import com.getinsured.eligibility.at.outbound.service.OutboundATProcessingService;
import com.getinsured.eligibility.at.ref.service.LceEditApplicationService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneService;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneServiceImpl;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.eligibility.ssap.integration.at.client.service.AccountTransferMapper;
import com.getinsured.eligibility.ssap.integration.at.client.service.AccountTransferOutboundSoapWebClient;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.dto.PortalResponse;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;

@RestController
@Validated
@RequestMapping("/ssapapplication")
public class ATXMLConverterController{
	private static Logger lOGGER = Logger.getLogger(ATXMLConverterController.class);
	
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired AccountTransferMapper accountTransferMapper;
	@Autowired private AccountTransferOutboundSoapWebClient accountTransferSoapWebClient;
	@Autowired private OutboundATProcessingService outboundATProcessingService;
	@Autowired private EligibilityProgramRepository eligibilityProgramRepository;
	@Autowired private Gson platformGson;
	@Autowired private GIWSPayloadRepository giWsPayloadRepository;
	@Autowired private ApplicationCloneService applicationCloneService;
	@Autowired private LceEditApplicationService lceEditApplicationService;
	
	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	public static final List<String> nonCloeasblestatuses = Arrays.asList(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(), 
																		  ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode());
	
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
    private static final String MEDICAID_ELIGIBILITY_TYPE = "MedicaidEligibilityType";
    private static final String CHIP_ELIGIBILITY_TYPE = "CHIPEligibilityType";
    private static final String ASSESSED_CHIP_ELIGIBILITY_TYPE = "AssessedCHIPEligibilityType";
    private static final String ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidMAGIEligibilityType";
    private static final String ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidNonMAGIEligibilityType";
	
	@RequestMapping(value = "/hasMedicaidChipEligibleMember", method = RequestMethod.GET)
    public String hasMedicaidChipEligibleMember(HttpServletRequest request){
		ProgramEligibilityResponseDTO programEligibilityResponseDTO = new ProgramEligibilityResponseDTO();
		try {
			Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
			List<String> elgibilitytypes = new ArrayList<String>();
			elgibilitytypes.add(CHIP_ELIGIBILITY_TYPE);
			elgibilitytypes.add(MEDICAID_ELIGIBILITY_TYPE);
			
			long count = eligibilityProgramRepository.getChipAndMedicaidEligibilityByApplicant(applicationId, elgibilitytypes);
			
			if(count > 0){
				programEligibilityResponseDTO.setResult(true);
			}else{
				programEligibilityResponseDTO.setResult(false);
			}
			programEligibilityResponseDTO.setStatus("SUCCESS");
		} catch (Exception e) {
			lOGGER.error(e.getStackTrace());
			programEligibilityResponseDTO.setResult(null);
			programEligibilityResponseDTO.setStatus("FAILURE");
			programEligibilityResponseDTO.setErrorMsg(e.getMessage());
		}
		return platformGson.toJson(programEligibilityResponseDTO);
	}
	
	@RequestMapping(value = "/hasAssessedMedicaidChipEligibleMember", method = RequestMethod.GET)
    public String hasAssessedMedicaidChipEligibleMember(HttpServletRequest request){
		ProgramEligibilityResponseDTO programEligibilityResponseDTO = new ProgramEligibilityResponseDTO();
		try {
			Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
			List<String> elgibilitytypes = new ArrayList<String>();
			elgibilitytypes.add(ASSESSED_CHIP_ELIGIBILITY_TYPE);
			elgibilitytypes.add(ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE);
			elgibilitytypes.add(ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE);
			
			long count = eligibilityProgramRepository.getChipAndMedicaidEligibilityByApplicant(applicationId, elgibilitytypes);
			if(count > 0){
				programEligibilityResponseDTO.setResult(true);
			}else{
				programEligibilityResponseDTO.setResult(false);
			}
			programEligibilityResponseDTO.setStatus("SUCCESS");
		} catch (Exception e) {
			lOGGER.error(e.getStackTrace());
			programEligibilityResponseDTO.setResult(null);
			programEligibilityResponseDTO.setStatus("FAILURE");
			programEligibilityResponseDTO.setErrorMsg(e.getMessage());
		}
		return platformGson.toJson(programEligibilityResponseDTO);
	}
	
	
	@RequestMapping(value = "/generateAndPushAT", method = RequestMethod.GET)
    public String generateAndPushAT(HttpServletRequest request){
		Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
		String applicationData = ssapApplicationRepository.getApplicationDataById(applicationId);
		AccountTransferResponsePayloadType atResponse = null;
		StringBuilder errorResponse = new StringBuilder();
		lOGGER.info("generateAndPushAT STARTED "+applicationId);
		try {
			AccountTransferRequestPayloadType accountTransferJaxb = accountTransferMapper.populateAccountTransferRequest("", applicationData);
			lOGGER.info("AccountTransferRequestPayloadType created for "+applicationId);
			Map<String,Object> outboundAtDetailsMap = outboundATProcessingService.prepareOutboundAtDBLoggingDetails(applicationId);
			
			StringWriter responseString =  new StringWriter();
				try {
					
					if(outboundATProcessingService.compareNewAndEnrolledApplication(applicationId) == true){
						lOGGER.info("compareNewAndEnrolledApplication done for "+applicationId);
						populateOutboundMapWithMarshalledRequest(outboundAtDetailsMap,accountTransferJaxb);
						lOGGER.info("outboundAtDetailsMap created for "+applicationId);
						//first create payload record for request
						GIWSPayload giWsPayload =  giWsPayloadRepository.save(outboundATProcessingService.populateGiWSPayload(applicationId,outboundAtDetailsMap));

						atResponse = accountTransferSoapWebClient.send(accountTransferJaxb);
						if(atResponse != null) {
						  marshaller.marshal(atResponse, responseString);
						}
					    String op1 = responseString != null ? responseString.toString() : "";
					    
					    outboundAtDetailsMap.put("response",op1);
					    outboundAtDetailsMap.put("responseObj",atResponse);
					   
					    if(null!=outboundAtDetailsMap.get("caseNumber") && null!=outboundAtDetailsMap.get("applicants") && null!=outboundAtDetailsMap.get("request")) {
					    	try {
					    		giWsPayload.setResponsePayload(outboundAtDetailsMap.get("response").toString());
					    		giWsPayload.setResponseCode(atResponse.getResponseMetadata().getResponseDescriptionText().getValue());
					    		String status = ("Success".equalsIgnoreCase(giWsPayload.getResponseCode())) ? "SUCCESS" : "FAILURE";
					    		giWsPayload.setStatus(status);
					    		giWsPayloadRepository.save(giWsPayload);

					    		if ("SUCCESS".equals(status)) {
									lOGGER.info("atResponse was success log outbound transfer");
									outboundATProcessingService.logOutboundAccountTransfer(applicationId, outboundAtDetailsMap, giWsPayload.getId(), status);
								} else {
					    			lOGGER.error("atResponse failed for applicationId: " + applicationId);
								}
					    	}catch (Exception e) {
					    		lOGGER.error(e.getStackTrace());
					    		errorResponse.append("Error while auditing outbound request - response:").append(e.getMessage());
								return this.formResponse(AccountTransferConstants.ERROR_CODE_FOUR, errorResponse.toString());
							}
					    }
					    return op1;
					} else{
						lOGGER.error("Outbound validation failed for HH and Income chnaged");
			    		errorResponse.append("Outbound validation failed for HH and Income chnaged");
						return this.formResponse(AccountTransferConstants.ERROR_CODE_SIX, errorResponse.toString());
					}
				} catch (WebServiceIOException wsIOE) {
					lOGGER.error(wsIOE);
					errorResponse.append("Error calling Account Transfer Endpoint:").append(wsIOE.getMessage());
					return this.formResponse(AccountTransferConstants.ERROR_CODE_ONE, errorResponse.toString());
				} catch (SoapFaultClientException sfce) {
					lOGGER.error(sfce);
					errorResponse.append("Error calling Account Transfer Endpoint:").append(sfce.getMessage());
					return this.formResponse(AccountTransferConstants.ERROR_CODE_TWO, errorResponse.toString());
				}
		} catch (Exception e) {
			lOGGER.error(e);
			errorResponse.append("Error calling Account Transfer Endpoint:").append(e.getMessage());
			return this.formResponse(AccountTransferConstants.ERROR_CODE_THREE, errorResponse.toString());
		}
	}
	
	private void populateOutboundMapWithMarshalledRequest(Map<String,Object> outboundAtDetailsMap,AccountTransferRequestPayloadType request) throws JAXBException {
		
		String marshalledRequest = null;
		if(request!=null) {
			StringWriter requestString = new StringWriter();
			marshaller.marshal(request, requestString);
			marshalledRequest =requestString!=null ? requestString.toString() : null;
		}
		
		outboundAtDetailsMap.put("request",marshalledRequest);
		
	}
	
	private String formResponse(String responseValue, String error) {
		try {
			AccountTransferResponsePayloadType atresponse = new AccountTransferResponsePayloadType();
			atresponse.setAtVersionText(AccountTransferConstants.AT_VERSION_TXT_2);

			ResponseMetadataType value = new ResponseMetadataType();
			TextType tt = new TextType();
			tt.setValue(responseValue);
			value.setResponseCode(tt);
			
			if(StringUtils.isNotBlank(error)){
				List<TextType> tdsResponseDescriptionTextList = new ArrayList<>();
				
				TextType responseErrorDescrip = new TextType();
				responseErrorDescrip.setValue("Account Transfer request failed");
				value.setResponseDescriptionText(responseErrorDescrip);
				
				TextType errorDescrip = new TextType();
				errorDescrip.setValue(error);
				tdsResponseDescriptionTextList.add(errorDescrip);
				
				value.getTDSResponseDescriptionText().addAll(tdsResponseDescriptionTextList);
			}
			atresponse.setResponseMetadata(value);
			StringWriter responseString =  new StringWriter();
			if(atresponse != null) {
			  marshaller.marshal(atresponse,responseString);
			}
		    String response = responseString != null ? responseString.toString() : "";
		    return response;
		} catch (Exception e) {
		 String response = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+
			"<ns6:AccountTransferResponse ns5:atVersionText=\"2.4\" xmlns:ns6=\"http://at.dsh.cms.gov/exchange/1.0\" xmlns:ns5=\"http://at.dsh.cms.gov/extension/1.0\" xmlns:ns8=\"http://hix.cms.gov/0.1/hix-pm\" xmlns:ns7=\"http://niem.gov/niem/domains/screening/2.1\" xmlns:ns2=\"http://hix.cms.gov/0.1/hix-core\" xmlns:ns1=\"http://niem.gov/niem/structures/2.0\" xmlns:ns4=\"http://hix.cms.gov/0.1/hix-ee\" xmlns:ns3=\"http://niem.gov/niem/niem-core/2.0\">"+
			    "<ns5:ResponseMetadata>"+
			        "<ns2:ResponseCode>"+AccountTransferConstants.ERROR_CODE_FIVE+"</ns2:ResponseCode>"+
			        "<ns2:ResponseDescriptionText>Account Transfer request failed</ns2:ResponseDescriptionText>"+
			        "<ns2:TDSResponseDescriptionText>"+e.getMessage()+"</ns2:TDSResponseDescriptionText>"+
			    "</ns5:ResponseMetadata>"+
			"</ns6:AccountTransferResponse>";
		  return response;
		}
	}

	@RequestMapping(value = "/cloneApplicationWithEligibilities", method = RequestMethod.GET)
	public PortalResponse cloneApplicationWithEligibilities(HttpServletRequest request){
		PortalResponse response = new PortalResponse();

		response.setStatus("SUCCESS");
		response.setExistingAppId(request.getParameter("applicationId"));
		lOGGER.info("Started Processing application : "+request.getParameter("applicationId"));

		try
		{
			Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
			SsapApplication existingApplication = ssapApplicationRepository.findOne(applicationId);

			lOGGER.info("Clone application START");

			Map<String, String> responseData = applicationCloneService.cloneSsap(existingApplication,false, new AtomicBoolean(true), true);

			response.setClonedAppId(responseData.get(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID));
			response.setClonedAppCaseNumber(responseData.get(ApplicationCloneServiceImpl.CLONED_APPLICATION_CASENUMBER));

			lOGGER.info("Clone application END");

			lOGGER.info("Updating existing application status STARTED");
			if(!nonCloeasblestatuses.contains(existingApplication.getApplicationStatus()))
			{
				if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(existingApplication.getApplicationStatus())){
					if(existingApplication.getApplicationDentalStatus() != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(existingApplication.getApplicationDentalStatus())){
						return response;
					}
				}
				lceEditApplicationService.closeApplications(existingApplication);
				lOGGER.info("Updating existing application status is closed now.");
			}

			lOGGER.info("Updating existing application status ENDED");
		} catch (Exception e) {
			lOGGER.error("Error while cloning application: ", e);
			response.setStatus("FAILED");
			response.setErrorMessage("Error while cloning application");
		}
		return response;
	}
	
	@RequestMapping(value = "/cloneApplication", method = RequestMethod.GET)
    public PortalResponse cloneApplication(HttpServletRequest request){
		PortalResponse response = new PortalResponse();
		try 
		{
			response.setStatus("SUCCESS");
			response.setExistingAppId(request.getParameter("applicationId"));
			lOGGER.info("Started Processing application : "+request.getParameter("applicationId"));
			Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
			SsapApplication existingApplication = ssapApplicationRepository.findOne(applicationId);
			lOGGER.info("Found application : "+applicationId);
			lOGGER.info("Clone application STARTED ");
			Map<String, String> responseData = applicationCloneService.cloneSsap(existingApplication,true, new AtomicBoolean(false), true);
			response.setClonedAppId(responseData.get(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID));
			response.setClonedAppCaseNumber(responseData.get(ApplicationCloneServiceImpl.CLONED_APPLICATION_CASENUMBER));
			lOGGER.info("Clone application ENDED ");

			lOGGER.info("Updating existing application status STARTED");
			if(!nonCloeasblestatuses.contains(existingApplication.getApplicationStatus()))
			{
				if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(existingApplication.getApplicationStatus())){
					if(existingApplication.getApplicationDentalStatus() != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(existingApplication.getApplicationDentalStatus())){
						return response;
					}
				}
				lceEditApplicationService.closeApplications(existingApplication);
				lOGGER.info("Updating existing application status is closed now.");
			}
			
			lOGGER.info("Updating existing application status ENDED");
		} catch (Exception e) {
			lOGGER.error("Error while cloning application: ", e);
			response.setStatus("FAILED");
			response.setErrorMessage("Error while cloning application");
		}
		return response;
	}
}