import React, { Fragment, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

// Components

import Form from './Form';
import PageHeader from '../../components/headers/PageHeader';
import SourceSummary from './pageIncludes/summary/SourceSummary';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData, clearMethods, setMethods, showModal } from '../../actions/actionCreators';

// Utils

import incomeCalculator from '../../utils/incomeCalculator';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import INPUT_TYPES from '../../constants/inputTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import InstructionalText from '../notifications/InstructionalText';



const Page3_34_E1 = props => {

    const {
        data,
        form: {
            Form: {
                isFormOnTopLevelValid,
                areAllInputsValid,
                isFormSubmitted
            }
        },
        householdMembers,
        page
    } = props;

    const {
        buttonContainer: ButtonContainer,
        isFirstVisit
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        expenses,
        name
    } = currentHhm;

    const [ localState, setLocalState ] = useState({
        isAnyDeduction: isFirstVisit ? null : !!expenses.length
    });

    const {
        isAnyDeduction
    } = localState;

    const handleDeductionSourceDeletion = (member, deductionIndex) => {
        const newState = { ...data };

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .expenses
            .splice(deductionIndex, 1);

        const {
            incomes: currentIncomes,
            expenses: currentExpenses
        } = newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member];

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .annualTaxIncome
            .reportedIncome = incomeCalculator.getTotalIncome(currentIncomes, currentExpenses);

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .expenses
            .length === 0
        && setLocalState(state => ({
            ...state,
            isAnyDeduction: null
        }));


        props.actions.changeData(newState);
    };

    const handleDeductionSourceEditing = deductionIndex => {
        props.actions.showModal(
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            [
                expenses[deductionIndex],
                deductionIndex,
                'expenses',
                page
            ]
        );
    };

    const handleAddDeductionSourceClick = () => {
        props.actions.showModal(
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            [
                {},
                null,
                'expenses',
                page
            ]
        );
    };

    useEffect(() => {
        props.actions.setMethods('Page3_34_E1',
            {
                handleDeductionSourceDeletion,
                handleDeductionSourceEditing,
                handleAddDeductionSourceClick
            });

        return () => {
            props.actions.clearMethods('Page3_34_E1');
        };
    }, [ expenses.map(expense => Object.values(expense).toLocaleString()).toLocaleString() ]);

    const onFieldChange_local = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
    };

    const checkIfDisabled = () => {
        return expenses.length > 0 ? false : !isAnyDeduction;
    };

    const checkTopLevelValidation = () => {
        switch(true) {
        case isAnyDeduction === true:
            return expenses.length > 0;
        case isAnyDeduction === false:
            return true;
        case isAnyDeduction === null:
            return false;
        }
    };

    return (
        <Fragment>
            <div id="iiDeductions" className="">
                <div className="subsection">
                    <PageHeader
                        text={`Deductions for ${normalizeNameOfPerson(name)}`}
                        headerClassName={'usa-heading-alt'}
                    />
                    <p>
                        <FormattedMessage
                            id="ssap.deductionSourcesInitial.msg.one"
                            defaultMessage="Telling us about things that can be deducted on an income tax return could
                            make the cost of health insurance a little lower"
                        />
                    </p>


                    <Form topLevelValidity={checkTopLevelValidation()}>

                        {
                            expenses.length === 0 ?

                                <div className="usa-width-one-whole">
                                    <ValidationErrorMessage
                                        isVisible={isFormSubmitted && !isFormOnTopLevelValid && areAllInputsValid}
                                        messageText={'You should add at least one deduction source or select \'No\''}
                                        errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                                    />

                                    <UniversalInput
                                        legend={{
                                            legendText: <div>

                                                <InstructionalText
                                                    Component={
                                                        <p data-instructions='deductions'>
                                                            Does { normalizeNameOfPerson(name) } pay any of these deductions?<span className="required-true"></span>
                                                        </p>
                                                    }
                                                />
                                                <ul className="usa-width-one-whole">
                                                    <li>
                                                        <FormattedMessage
                                                            id="ssap.deductions.msg.deductionTypeOne"
                                                            defaultMessage="Alimony"
                                                        />
                                                    </li>


                                                    <li>
                                                        <FormattedMessage
                                                            id="ssap.deductions.msg.deductionTypeTwo"
                                                            defaultMessage="Student loan interest"
                                                        />
                                                    </li>


                                                    <li>
                                                        <FormattedMessage
                                                            id="ssap.deductions.msg.deductionTypeThree"
                                                            defaultMessage="Other deductions"
                                                        />
                                                    </li>

                                                </ul>
                                            </div>
                                        }}
                                        label={{
                                            labelClass: ['usa-width-one-whole', 'layout--02', 'margin-t-0'],
                                            ulClass:['usa-width-one-whole'],
                                            showRequiredIcon: false
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'isAnyDeduction',
                                            value: isAnyDeduction,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: localState,
                                            fields: []
                                        }}
                                        errorClasses={['margin-l-20-ve']}
                                        inputActions={{
                                            onFieldChange: onFieldChange_local
                                        }}
                                    />

                                </div> :

                                <div className="usa-width-one-whole  margin-tb-20">
                                    <p>
                                        <FormattedMessage
                                            id="ssap.deductionSources.msg.three"
                                            defaultMessage="Add another type of deduction or continue to review a summary of your current deductions."
                                        />
                                    </p>
                                    <SourceSummary
                                        buttonContainer={ButtonContainer}
                                        sources={expenses}
                                        member={member}
                                        dropdownSourcesList={DROPDOWN_OPTIONS.DEDUCTION_SOURCES}
                                        dropdownFrequencyList={DROPDOWN_OPTIONS.FREQUENCY}
                                        summaryType={'Deduction'}
                                    />
                                </div>
                        }
                    </Form>

                    <ButtonContainer
                        customButtonClassName={checkIfDisabled() ? 'btn-disabled' : ''}
                        type="addDeductionSourceButton"
                        args={[householdMembers, member]}
                        isDisabled={checkIfDisabled()}
                    />

                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearMethods,
        showModal,
        setMethods
    }, dispatch)
});

Page3_34_E1.defaultProps = {
    name: 'Page3_34_E1'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page3_34_E1);
