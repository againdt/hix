package com.getinsured.hix.batch.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class ExitJobListener implements JobExecutionListener{

    @Override
    public void beforeJob(JobExecution jobExecution) {
        // no-op
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        // change exit message
        ExitStatus es = jobExecution.getExitStatus();
        if(es.getExitDescription().indexOf("JobInterruptedException") >= 0){
        	jobExecution.setExitStatus(new ExitStatus(es.getExitCode(), "Job has been stopped through an external command"));
        }
    }
    
}