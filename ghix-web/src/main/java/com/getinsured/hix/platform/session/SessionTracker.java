package com.getinsured.hix.platform.session;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;

@Controller
public class SessionTracker {
	
	private static Logger LOGGER = LoggerFactory.getLogger(SessionTracker.class);
	@Autowired
	private SessionTrackerService sessionService;
	private static Timer sessionCleanupThread = null;
	private SessionCleanup cleaner = null;
	@PostConstruct
	public void initiateTime(){
			LOGGER.info("Initiating Session clean up");
			if(sessionCleanupThread == null){
				sessionCleanupThread = new Timer("SESSION CLEAN UP");
				cleaner = new SessionCleanup();
				sessionCleanupThread.scheduleAtFixedRate(cleaner, 10000, 300*1000);
			}
	}
	
	@PreDestroy
	public void stopTimer(){
		sessionCleanupThread.cancel();
	}
	
	@RequestMapping(value = "/invalidateSession", method = RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public ResponseEntity<String> invalidateUserSession(@RequestParam(value ="sessionId", required = true) String sessionId, HttpServletRequest req ){
		//Session Id is encrypted
		boolean success = false;
		HttpStatus status = HttpStatus.OK;
		try {
			LOGGER.info("Received remote session ["+sessionId+"] invalidate request from:"+req.getRemoteAddr());
			String dec_sessionId = GhixAESCipherPool.decrypt(sessionId);
			success = sessionService.markUserSessionClosed(dec_sessionId);
			LOGGER.info("Retrieved remote session ["+dec_sessionId+"], Invadated:"+success);
		} catch (Exception e) {
			status = HttpStatus.BAD_REQUEST;
		}
		if(success){
			return makeResponse("SUCCESS", HttpStatus.OK);
		}
		if(status.value() != HttpStatus.BAD_REQUEST.value()){
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return makeResponse("FAILED", status);
	}
	
	private ResponseEntity<String> makeResponse(String responseStr,
			HttpStatus requestStatus) {
		 HttpHeaders x = new HttpHeaders();
	     x.setContentType(MediaType.valueOf("text/plain"));
		 ResponseEntity<String> response = new ResponseEntity<String>(responseStr, x, requestStatus);
		 return response;
	}
	
	private class SessionCleanup extends TimerTask {
		@Override
		public void run() {
			Thread.currentThread().setName(
					"Session Cleanup[" + Thread.currentThread().getId()
							+ "]");
			try {
				sessionService.cleanupMemoryMappedSessions();
			} catch (Exception e) {
				LOGGER.error(
						"Exception occured during Session Clean up", e);
			}
		}
	}
}
