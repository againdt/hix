package com.getinsured.iex.dto;

import java.sql.Timestamp;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.getinsured.hix.model.LookupValue;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicationEventResource extends ResourceSupport {

	private Timestamp coverageStartDate;
	private Timestamp coverageEndDate;
	private String verification;
	private String actionEnabled;
	private Timestamp createdTimeStamp;
	private Timestamp eventDate;
	private Timestamp reportStartDate;
	private Timestamp reportEndDate;
	private Timestamp enrollmentStartDate;
	private Timestamp enrollmentEndDate;
	private LookupValue changeEventType;
	@JsonInclude(Include.NON_NULL)
	private String ssapApplication;
	private String keepOnly;
	private Integer createdBy;
	private Integer lastUpdatedBy;
	private Timestamp lastUpdateTimestamp;
	private String eventType;
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}
	
	public String getSsapApplication() {
		return ssapApplication;
	}

	public void setSsapApplication(String ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	public SsapApplicationEventResource() {
		
	}

	public LookupValue getChangeEventType() {
		return changeEventType;
	}

	public void setChangeEventType(LookupValue changeEventType) {
		this.changeEventType = changeEventType;
	}

	public Object _links;

	public Timestamp getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Timestamp coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Timestamp getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(Timestamp coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}

	public String getActionEnabled() {
		return actionEnabled;
	}

	public void setActionEnabled(String actionEnabled) {
		this.actionEnabled = actionEnabled;
	}

	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public Timestamp getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(Timestamp reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public Timestamp getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(Timestamp reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}
}
