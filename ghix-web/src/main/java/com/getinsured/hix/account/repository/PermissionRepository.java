package com.getinsured.hix.account.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Permission;

@Repository
//@Transactional(readOnly = true)
public class PermissionRepository {
	@Autowired
	private IPermissionRepository permissionRepository;
	
	public Permission findById(Integer id) {
		if( permissionRepository.exists(id) ){
			return permissionRepository.findOne(id);
		}
		return null;
    }

	public List<Permission> findAll() {
       return permissionRepository.findAll();
    }
	
	public Page<Permission> findAll(Pageable pageable) {
	       return permissionRepository.findAll(pageable);
	}
	
    @Transactional
    public Permission save(Permission permission) {
            return permissionRepository.save(permission);
    }
    
    
    public Permission findIdByName(String nm){
    	return permissionRepository.findIdByName(nm);
    }
}
