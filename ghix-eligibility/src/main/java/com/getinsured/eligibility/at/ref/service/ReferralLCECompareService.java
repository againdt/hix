package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;

/**
 * @author chopra_s
 * 
 */
public interface ReferralLCECompareService {
	CompareMainDTO executeCompare(long currentApplicationId, long enrolledApplicationId) throws Exception;

	CompareMainDTO executeCompareReadOnly(long currentApplicationId, long enrolledApplicationId) throws Exception;

	CompareMainDTO execute(long currentApplicationId, long enrolledApplicationId, boolean readOnly) throws Exception;
}
