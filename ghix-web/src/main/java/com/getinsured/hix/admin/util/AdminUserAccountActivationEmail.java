package com.getinsured.hix.admin.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;

@Component
public class AdminUserAccountActivationEmail extends NotificationAgent {

	@Value("#{configProp.exchangename}")
	private String exchangeName;


	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation) notificationContext.get("ACCOUNT_ACTIVATION");
		Map<String,String> employeeData = new HashMap<String, String>();
		String activationUrl = GhixConstants.APP_URL + "account/user/activation/" + accountActivation.getActivationToken();
		employeeData = activationJsonObj.getCreatedObject().getCustomeFields();
		employeeData.put("exchangename", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME) );
		employeeData.put("activationUrl", activationUrl );
		employeeData.put("userName", StringUtils.isNotEmpty(activationJsonObj.getCreatedObject().getFullName()) ? activationJsonObj.getCreatedObject().getFullName(): "");
		employeeData.put("host", GhixConstants.APP_URL );
		employeeData.put("exchangeFullName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		employeeData.put("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		return employeeData;
	}

	
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson) notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "You have been invited to create an account on " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		}
		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}