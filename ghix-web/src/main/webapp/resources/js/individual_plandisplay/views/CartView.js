(function (views) {
	views.Carts = Backbone.View.extend({
		events : {
			'click a.removeItems': 'removeItems',
			'click a.validateCart': 'validateCart'
		},
		el : '#cart',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','removeItems','validateCart');
			var self = this;
			var ehb= "";
			self.hasSrNo = new Array();
			self.collection.fetch({
				success : function () {
					self.collection.setCartSummary(false,false);
					
				}
			});
			
			self.cartModel = options.cartModel;
			
			$('.validateCart').bind('click', this.validateCart);
			self.collection.bind('reset', self.addAll);
		},
		
		addAll : function (models) {
			var self = this;
			this.updateCartSummary();
			this.updatePremium();
			$('#cart').html('');
			$("table[id^='table_'] tbody").empty();
			$("table[id^='table_dental_'] tbody").empty();

			self.collection.each (self.addOne);
			
			$('#loadDiv').hide();
			$("table[id^='table_']").show();
			$("table[id^='table_tbody_']").show();
			$('#summary_table').show();
		},
		
		addOne : function (model) {
			var self = this;
			var pgrmType = $('#PGRM_TYPE').val();
			var exchangeType = $('#exchangeType').val();
			var stateCode = $('#stateCode').val();
			var ehb= "";
			
			model.set({"PGRM_TYPE":pgrmType});
			
			if(jQuery.inArray(model.get("id"),self.hasSrNo) === -1){
				self.hasSrNo.push(model.get("id"));
				model.set({srNo: self.hasSrNo.length});
			} 
			
			ehb =  model.get("ehb");
			
			/*if(exchangeType == 'STATE'  && ehb < '9.5' ){
				$('.validateCart').hide();
			}*/
			var view = new Cart({model:model});
			if(model.get("planType") == "DENTAL"){
				$('#table_dental_'+model.get("groupId")).append(view.render().el);
				//$('#health_cart').hide();
				$('#dental_cart').show();
			}else{
				$('#table_'+model.get("groupId")).append(view.render().el);
				//$('#dental_cart').hide();
				$('#health_cart').show();
			}
			$('.removeItems').bind('click', this.removeItems);
			$("#cart").empty();
		},
		
		updateCartSummary : function () {
			var self = this;
			$('#planCount').html(self.collection.cartSummary.planCount);
			var detailSummary = self.collection.cartSummary.platinum ? self.collection.cartSummary.platinum+" Platinum Plans, ":"";
			detailSummary += self.collection.cartSummary.gold ? self.collection.cartSummary.gold+" Gold Plans, " : "";
			detailSummary += self.collection.cartSummary.silver ? self.collection.cartSummary.silver+" Silver Plans, " : "";
			detailSummary += self.collection.cartSummary.bronze ? self.collection.cartSummary.bronze+" Bronze Plans, " : "";
			detailSummary += self.collection.cartSummary.catastrophic ? self.collection.cartSummary.catastrophic+" Catastrophic Plans, " : "";
			
			$('#detailSummary').html(detailSummary.substring(0,detailSummary.length-2));
		},
		
		updatePremium : function () {
			var totalPremiumBeforeCredit = this.collection.getTotalPremium()
			var totalAptc = this.collection.getTotalAptc();
			var totalPremiumAfterCredit = totalAptc < totalPremiumBeforeCredit ? totalPremiumBeforeCredit - totalAptc : 0;
			//var totalContribution = this.collection.getTotalContri();
			if($('#PGRM_TYPE').val() == "Shop"){
				$('#totalContribution').html("- $"+totalAptc.toFixed(2));
				totalPremiumAfterCredit = totalAptc < totalPremiumBeforeCredit ? totalPremiumBeforeCredit - totalAptc : 0;
			}
			$('#totalAptc').html("- $"+totalAptc.toFixed(2));
			$('#totalPremiumBeforeCredit').html("$"+totalPremiumBeforeCredit.toFixed(2));
			$('#totalPremiumAfterCredit').html("$"+totalPremiumAfterCredit.toFixed(2));
			$('#totalPremiumAfterCreditHeader').html("$"+totalPremiumAfterCredit.toFixed(2));
			$('#totalPremiumBeforeCreditVal').val(totalPremiumBeforeCredit.toFixed(2));
			$('#netpremium').val(totalPremiumAfterCredit.toFixed(2));
			
		},
		
		removeItems : function (e) {
			var itemIdArr = e.target.id.replace("remove_","").split("_");
			var planId = itemIdArr[1];
			var groupId = itemIdArr[0];
			var planType = this.collection.getPlanType(planId,groupId);
			
			if(planType == 'DENTAL'){
				$.ajax({
			 		type : "POST",
			 	 	url : "updateSADPEnrollID",
			 	 	success : function(response) {
			 	 		if(response == "Updated"){
			 	 			$('#dental_cart').empty();
			 	 			 $('#dental_cart').append('<div><h4>Dental Plans</h4><p>You will be disenrolled from your existing dental plan.'+  
									 'If you want dental coverage, please add a dental plan to your cart.'+
									 'To shop for a dental plan <a href="renewal" class="btn margin10-l">click here.</a> </p></div>');
			 	 		  }
			 	 	},error : function(e) {
			 	 		                   }
			});
			}
			

			
			this.collection.removeSelectedItems(planId,groupId);
			this.updateCartSummary();
		},

		validateCart : function(){
			if(this.collection.models.length <= 0){
				alert('There are no items in this cart');
				return false;
			}
			var enrollmentType = $('#enrollmentType').val();
			var elegibilityComplete = $('#elegibilityComplete').val();
			
			if(enrollmentType == 'S' && (elegibilityComplete==null || elegibilityComplete =="")){
				$('#checkout').hide();
				$.ajax({
				  type: "GET",
					url: './personDataUpdate?key='+(Math.random()*10)
			    }).done(function( msg ) {
			    	$('#checkout').show();
			    	if(msg == "SUCCESS"){
			    		$("#showEsign").submit();
			    	}
			    });
			}else{
				$("#showEsign").submit();
			}
		}
	});
	var Cart = Backbone.View.extend({
		tagName : 'tbody',
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			var template = _.template($('#tmpCart').html());
			$(this.el).html(template(this.model.toJSON()));
			
			return this;
		}
	});
})(App.views);