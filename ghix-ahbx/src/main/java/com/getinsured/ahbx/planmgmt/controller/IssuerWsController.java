package com.getinsured.ahbx.planmgmt.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import com.getinsured.ahbx.planmgmt.issuer.sync.IssuerInfoDTO;
import com.getinsured.ahbx.planmgmt.issuer.sync.UpdateIssuerInfo;
import com.getinsured.ahbx.planmgmt.issuer.sync.UpdateIssuerInfoResponse;
import com.getinsured.ahbx.util.GIException;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.platform.webclient.SoapWebClient;

@Controller
@RequestMapping(value = "/issuer")
public class IssuerWsController {
	
	private static final Logger LOGGER = Logger.getLogger(IssuerWsController.class);
	
	@Value("#{configProp.AHBXIssuerSyncWSDL}")
	private String aHBXIssuerSyncWSDL;
	
	@Autowired private WebServiceTemplate webServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<UpdateIssuerInfoResponse>> soapWebClient;
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome()
	{
		LOGGER.info("Welcome to GHIX-AHBX SOAP ISSUER Service module");		
		return "Welcome to GHIX-AHBX SOAP ISSUER Service module";
	}
	
	
	@RequestMapping(value ="/syncWithAHBX", method = RequestMethod.POST)
	@ResponseBody public String syncWithAHBX(@RequestBody Map<String, Object> requestData)  throws GIException
	{
		LOGGER.info("==================== inside syncWithAHBX response =======================");
		LOGGER.info("Received Criteria:=== "+ requestData );		 
		
		if (requestData == null || requestData.size() == 0)		{
			LOGGER.info("Invalid input: criteria data is null or empty");			
			return GhixAhbxConstants.RESPONSE_FAILURE;			
		}else{			
				IssuerInfoDTO issuerInfoDTO	 = new IssuerInfoDTO();
				issuerInfoDTO.setAccountName(requestData.get("accountName").toString());
				issuerInfoDTO.setAccountNumber(requestData.get("accountNumber").toString());
				issuerInfoDTO.setAddressLine1(requestData.get("addressLine1").toString());
				issuerInfoDTO.setAddressLine2(requestData.get("addressLine2").toString());
				issuerInfoDTO.setBankName(requestData.get("bankName").toString());
				issuerInfoDTO.setCity(requestData.get("city").toString());
				issuerInfoDTO.setEffectiveEndDate(requestData.get("effectiveEndDate").toString());				
				issuerInfoDTO.setEffectiveStartDate(requestData.get("effectiveStartDate").toString());
				issuerInfoDTO.setIssuerHiosId(Integer.parseInt(requestData.get("issuerId").toString()));
				issuerInfoDTO.setIssuerName(requestData.get("issuerName").toString());
				issuerInfoDTO.setIssuerRecordIndicator(requestData.get("issuerRecordIndicator").toString());
				issuerInfoDTO.setPaymentType(requestData.get("paymentType").toString());
				issuerInfoDTO.setRoutingNumber(requestData.get("routingNumber").toString());
				issuerInfoDTO.setState(requestData.get("state").toString());
				issuerInfoDTO.setZipCode(Integer.parseInt(requestData.get("zipCode").toString()));
				
								
				List<IssuerInfoDTO> list = new ArrayList<IssuerInfoDTO>();
				list.add(issuerInfoDTO);
				UpdateIssuerInfo updateIssuerInfo = null;
				
				com.getinsured.ahbx.planmgmt.issuer.sync.ObjectFactory objectFactory = new com.getinsured.ahbx.planmgmt.issuer.sync.ObjectFactory();
				updateIssuerInfo = objectFactory.createUpdateIssuerInfo();
				updateIssuerInfo.setArg0(list);
				
				JAXBElement<UpdateIssuerInfo> requestElement = objectFactory.createUpdateIssuerInfo(updateIssuerInfo);
				
				UpdateIssuerInfoResponse updateIssuerInfoResponse = new UpdateIssuerInfoResponse();
				webServiceTemplate.setDefaultUri(aHBXIssuerSyncWSDL);
				
				try{
					@SuppressWarnings("unchecked")
					//JAXBElement<UpdateIssuerInfoResponse> ahbxResponse = (JAXBElement<UpdateIssuerInfoResponse>)webServiceTemplate.marshalSendAndReceive(requestElement);	
					JAXBElement<UpdateIssuerInfoResponse> ahbxResponse = soapWebClient.send(requestElement, aHBXIssuerSyncWSDL, "IND05", webServiceTemplate);
					updateIssuerInfoResponse = ahbxResponse.getValue();
					
					if (updateIssuerInfoResponse.getReturn() != null && !updateIssuerInfoResponse.getReturn().isEmpty() && updateIssuerInfoResponse.getReturn().size() > 0) {
						/*LOGGER.info("Sync issuer data with AHBX service called successfully. Received following response from AHBX: " + 
								"Issuer Id: " + updateIssuerInfoResponse.getReturn().get(0).getIssuerId()+
								" Response Code: " + updateIssuerInfoResponse.getReturn().get(0).getResponseCode()+
								" Response Desc: " + updateIssuerInfoResponse.getReturn().get(0).getResponseDescription());*/
						
						return GhixAhbxConstants.RESPONSE_SUCCESS;
					}	else{
						return GhixAhbxConstants.RESPONSE_FAILURE;
					}
					
				}
				catch (WebServiceIOException webServiceIOException) {
					LOGGER.info("Exception occurred while sync issuer data with AHBX: "
							+ webServiceIOException.getMessage());	
					return GhixAhbxConstants.RESPONSE_FAILURE;
				} catch (Exception exception) {
					LOGGER.info("Exception occurred while sync issuer data with AHBX: "
							+ exception.getMessage());		
					return GhixAhbxConstants.RESPONSE_FAILURE;
				}
				
		}
		
		
	}	
	
}
