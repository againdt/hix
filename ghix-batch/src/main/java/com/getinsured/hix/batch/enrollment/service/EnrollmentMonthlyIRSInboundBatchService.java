/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface EnrollmentMonthlyIRSInboundBatchService {
	/**
	 * Move and Process CMS and IRS response files
	 * @author negi_s
	 * @param jobExecutionId 
	 * @since 24/09/2015
	 * @throws GIException
	 */
	void processInboundResponse(Long jobExecutionId, JobLauncher jobLauncher, Job job) throws GIException;
	
	/**
	 * Job Execution list
	 * @param jobName
	 * @return
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);
}
