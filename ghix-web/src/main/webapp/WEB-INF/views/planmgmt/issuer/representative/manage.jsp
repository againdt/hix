<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<!-- end of secondary navbar -->
<div class="gutter10">
<div class="row-fluid">
	<!-- start of third navbar
	<ul class="nav nav-pills">
		<li><a href="#"><spring:message code='label.accountInfo' /></a></li>
		<li class="active"><a href="#"><spring:message code='label.manageAuthRep' /></a></li>
	<li><a href="managefinancialinfo"><spring:message code='pgheader.financialInformation' /></a></li>
	</ul>
	<!-- start of third navbar -->
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
       	<h1>
       		<img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${hiosissuerid}"/>"/> <spring:message code='label.manageAuthRep' /> <small>${resultSize} <spring:message code='label.totalMembers' /></small>
       		<!-- userActiveRoleName is defined in masthead-->
       		<c:if test="${userActiveRoleName != 'issuer_representative'}" >
       			<a class="btn btn-primary pull-right nmhide" href="<c:url value='/planmgmt/managerepresentative?action=add'/>"><i class="icon icon-plus icon-white"></i> <spring:message code='pgheader.addRepresentative' /></a>
       		</c:if>
       	</h1>
</div>
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<div class="header">
					<h4><spring:message code='pgheader.searchRep'/></h4>
					</div>
					<form class="form-vertical" id="frmManageRep" name="frmManageRep" action="<c:url value="/planmgmt/managerepresentative"/>" method="POST">
						<df:csrfToken/>
						<input type="hidden" id="action" name="action" value="search" />
						<div class="gutter10 lightgray">
								<div class="control-group">
									<label for="planName" class="control-label"><spring:message code='label.planIssuerName' /></label>
									<div class="controls">
										<input type="text" name="repname" id="repname" class="span12" value="${repName}" />
									</div> <!-- end of controls-->
								</div><!-- end of control-group -->
								<div class="control-group">
									<label for="networkName" class="required control-label"><spring:message code='label.title' /> <!-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --> </label>
									<div class="controls">
										<input type="text" name="reptitle" id="reptitle" class="span12" value="${repTitle}" />
									</div> <!-- end of controls-->
								</div> <!-- end of control-group -->
                            	<div class="control-group">
                                    <label for="planname" class="control-label"><spring:message  code='label.status.plan'/></label>
                                    <div class="controls">
                                        <select  name="status" id="status" class="span12">
                                            <option value=""><spring:message code='label.selectUpperCase'/></option>
                                            <c:forEach items="${statusMap}" var="statusMap">
                                                <option ${statusMap.key == selectedStatus ? 'selected="selected"' : ''} value="${statusMap.key}"> ${statusMap.value} </option>
                                            </c:forEach>
                                        </select>
                                    </div> <!-- end of controls-->
                                </div><!-- end of control-group -->
                                <div class="txt-center">
									<input type="submit" class="btn btn-primary" value="<spring:message code='label.go'/>" title="<spring:message code='label.go'/>" />
								</div>
						</div>
				</form>
				<!-- show Add Issuer Representative button --> 
				<!-- Hiding Add Rep as per HIX-106935 -->
				<%-- <a class="btn btn-primary nmhide" href="<c:url value='/planmgmt/managerepresentative?action=add'/>"><i class="icon icon-plus icon-white"></i><spring:message code='pgheader.addRepresentative' /></a> --%>
			</div>
			<c:set var="newSortOrder" value="${sortOrder}"></c:set>
			<div class="span9" id="rightpanel">
				<div class="row-fluid">
					<c:choose>
						<c:when test="${fn:length(issuerReps) > 0}">
							<spring:message  code="label.nameOnly" var="nameValue" />
							<spring:message  code="label.title" var="titleValue" />
							<spring:message  code="label.lastUpdated" var="lastUpdated" />
							<spring:message  code="label.status.plan" var="statusValue" />
							<spring:message  code="label.updatedBy" var="updatedBy" />
							<spring:message  code="label.primaryContact" var="primarycontact" />
							<table class="table table-striped">
								<thead>
									<tr class="header">							
								 		<th scope="col" class="sortable"><dl:sort title="${nameValue}" sortBy="firstName" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${titleValue}" sortBy="title" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${lastUpdated}" sortBy="updated" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${statusValue}" sortBy="status" sortOrder="${newSortOrder}"></dl:sort></th>								     	
								     	<th scope="col" class="sortable"><dl:sort title="${updatedBy}" sortBy="updatedBy.firstName" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${primarycontact}" sortBy="primarycontact" sortOrder="${newSortOrder}"></dl:sort></th>
									 	<th scope="col" class="sortable"><i class="icon-cog"></i></th>
									</tr>
								</thead>
								<c:forEach var="issuerRepsObj" items="${issuerReps}">
									<c:set var="encIssuerRepId" ><encryptor:enc value="${issuerRepsObj.id}" isurl="true"/> </c:set>
									<tr>
										<td class="name-column">
											<c:choose>
												<c:when test="${issuerRepsObj.haveUserId == 'Y' && ((STATE_CODE != 'CA' && fn:toUpperCase(issuerRepsObj.displayStatus) != 'INACTIVE') || (STATE_CODE == 'CA' && fn:toUpperCase(issuerRepsObj.displayStatus) == 'INACTIVE'))}">
													<a href="<c:url value='/planmgmt/viewrepdetails/${encIssuerRepId}'/>">${issuerRepsObj.firstName} ${issuerRepsObj.lastName}</a>
												</c:when>
												<c:otherwise>
													${issuerRepsObj.firstName} ${issuerRepsObj.lastName}
												</c:otherwise>	
											</c:choose>		
										</td>
										<td>${issuerRepsObj.title}</td>
										<td>${issuerRepsObj.updated}</td>
										<td>
											<%-- <c:choose>
												<c:when test="${fn:toUpperCase(issuerRepsObj.status) =='INACTIVE'}"><spring:message  code='label.inactive'/></c:when>
												<c:otherwise><spring:message  code='label.active'/></c:otherwise>
											</c:choose>  --%>
											 ${issuerRepsObj.displayStatus}
										</td>
										<td>${issuerRepsObj.updatedby}</td>
										<td>${issuerRepsObj.primarycontact}</td>
										<td>
											<div class="controls">
												<div class="dropdown">
													<c:choose>
														<c:when test="${issuerRepsObj.haveUserId == 'Y' && ((STATE_CODE != 'CA' && fn:toUpperCase(issuerRepsObj.displayStatus) != 'INACTIVE') || (STATE_CODE == 'CA' && ((issuerRepsObj.primarycontact != 'YES' && loggedIdUserId != issuerRepsObj.repUserId && fn:toUpperCase(issuerRepsObj.displayStatus) != 'INACTIVE') || (fn:toUpperCase(issuerRepsObj.displayStatus) == 'INACTIVE'))))}">
															<!--  show gear menu start -->
															<a class="dropdown-toggle" id="dLabel" role="button"  data-toggle="dropdown" data-target="#" href="/page.html">
																<i class="icon-cog"></i> <b class="caret"></b>
															</a>
															<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">

																<c:if test="${STATE_CODE != 'CA' || (STATE_CODE == 'CA' && fn:toUpperCase(issuerRepsObj.displayStatus) == 'INACTIVE')}">
																	<li><a href="<c:url value='/planmgmt/editrepresentative/${encIssuerRepId}'/>"><i class="icon-pencil"></i> <spring:message code='label.edit'/></a></li>
																</c:if>

																<c:if test="${issuerRepsObj.primarycontact != 'YES'}">
																	<li>
																		<%-- a. Do not show suspend option for Primary Contact.
																			 b. Show Activate option for Inactive issuer representative
																			 c. Logged-in issuer rep can't suspend himself/herself --%>
																		<c:choose>
																			<c:when test="${fn:toUpperCase(issuerRepsObj.displayStatus) == 'SUSPENDED'}"><a href="<c:url value='/planmgmt/managerepresentative?action=active&repId=${encIssuerRepId}'/>"><i class="icon-activate"></i> <spring:message  code='label.activate'/></a></c:when>
																			<c:when test="${loggedIdUserId != issuerRepsObj.repUserId && fn:toUpperCase(issuerRepsObj.displayStatus) == 'ACTIVE'}"><a href="#" onclick="doSuspend('${issuerRepsObj.firstName}', '${issuerRepsObj.lastName}','${encIssuerRepId}')"><i class="icon-trash"></i> <spring:message  code='label.suspend'/></a></c:when>
																		</c:choose>
																	</li>
																</c:if>
															</ul>
														</c:when>
														<%-- Show 'Send email activation' link for unregistered issuer representative only on ID state exchange  --%>
														<c:when test="${issuerRepsObj.haveUserId == 'N' && isEmailActivation == true && fn:toUpperCase(STATE_CODE) == 'ID' }">
															<a href="<c:url value='/planmgmt/representative/sendActivationlink/${encIssuerRepId}'/>"><spring:message  code='label.sendactivatelink'/></a>
														</c:when>
														<c:otherwise>
															<span class="dropdown-toggle" id="dLabel" role="button"  data-toggle="dropdown" data-target="#">
																<i class="icon-cog"></i>
															</span>
														</c:otherwise>
													 </c:choose>	
												</div>
											</div>
										</td>
									</tr>
								</c:forEach>
							</table>
							<div class="center">
							 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>
						</c:when>
						<c:otherwise>
							<div class="alert alert-info"><spring:message code='label.recordsNotFound'/></div>
						</c:otherwise>
					</c:choose>
					
				</div>
			</div><!-- end of .span9 -->
		</div>
	</div>
</div>



<div id="dialog-modal" style="display: none;">
  <p>
  	<c:if test="${not empty sessionRepresentativeName}">
    	Representative Name: ${sessionRepresentativeName} <br/>
	</c:if>
	<c:if test="${not empty sessionDelegationCode}">
    	Delegation Code is: ${sessionDelegationCode} <br/>
	</c:if>
	<c:if test="${not empty generateactivationlinkInfo}">
    	${generateactivationlinkInfo}
	</c:if>
	<c:if test="${not empty ERROR}">
    	${Error}
	</c:if>	
 <%--  Representative Name: ${sessionRepresentativeName} <br/>
  		Delegation Code is: ${sessionDelegationCode} <br/>
  		${generateactivationlinkInfo} --%>
  </p>
</div>
<div id="dialog-modal_error" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>

<script type="text/javascript">
$(document).ready(function()
{
	/* alert("Delegation code is: "+'${sessionDelegationCode}'); */
	if('${sessionDelegationCode}' != null && '${sessionDelegationCode}' != '' && '${sessionRepresentativeName}' != null && '${sessionRepresentativeName}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "<spring:message  code='label.ahbxInfo'/>",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	else if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
	{
		$( "#dialog-modal_error" ).dialog({
		      height: 140,
		      modal: true,
		      title: "<spring:message  code='label.ahbxInfo'/>",
		      buttons: {
					Ok: function() {
					$( this ).dialog( "close" );
					}
					}
		    });			
	}
	else if('${generateactivationlinkInfo}' != null && '${generateactivationlinkInfo}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		  		modal: true,
		    	title: "<spring:message  code='label.registrationStatus'/>",
		    	buttons: {
		    	Ok: function() {
		    		$( this ).dialog( "close" );
		    	}
		    	}
		    });
	}
	else if('${ERROR}' != null && '${ERROR}' != ''){
		$( "#dialog-modal" ).dialog({
			modal: true,
			title: "Permission Error",
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}
});

function doSuspend(firstName, lastName, encIssuerRepId){
	var didConfirm = confirm("Are you sure you want to suspend the authorized representative '" + firstName + " " + lastName + "' ?");
	if (didConfirm == true) {
		location.href="<c:url value='/planmgmt/managerepresentative?action=Inactive&repId=" + encIssuerRepId + "'/>";
	} else {
		return false;
	}
}
</script>
