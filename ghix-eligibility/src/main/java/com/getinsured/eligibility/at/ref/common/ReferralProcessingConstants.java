package com.getinsured.eligibility.at.ref.common;

/**
 * @author chopra_s
 * 
 */
public interface ReferralProcessingConstants {
	String KEY_GI_WS_PAYLOAD_ID = "GI_WS_PAYLOAD_ID";

	String KEY_SSAP_APPLICATION_ID = "SSAP_APPLICATION_ID";

	String KEY_APPLICATIONS_WITH_SAME_ID = "APPLICATIONS_WITH_SAME_ID";

	String KEY_APPLICATION_EXTENSION = "APPLICATION_EXTENSION";

	String KEY_ENROLLED_APPLICATION_ID = "ENROLLED_APPLICATION_ID";

	String KEY_COMPARED_TO_APPLICATION_ID = "COMPARED_TO_APPLICATION_ID";

	String KEY_ACCOUNT_TRANSFER_CATEGORY = "ACCOUNT_TRANSFER_CATEGORY";

	String KEY_DEMO_COMPARE_ADMINUPDATE = "DEMO_COMPARE_ADMINUPDATE";
	
	String UPDATED_MAILING_ADDRESS = "UPDATED_MAILING_ADDRESS";

	String KEY_DEMO_COMPARE_DOBCHANGED = "DEMO_COMPARE_DOBCHANGED";

	String MEMBER_GUID_LIST = "MEMBER_GUID_LIST";

	String REFERRAL_AUTOLINKING = "REFERRAL_AUTOLINKING";

	String KEY_ENROLLED_ATTRIBUTES = "ENROLLED_APPLICATION_ATTRIBUTES";

	String KEY_RENEWAL_COMPARED_TO_APPLICATION_ID = "RENEWAL_COMPARED_TO_APPLICATION_ID";
	
	String KEY_REFERRAL_CMR_MULTIPLE = "REFERRAL_CMR_MULTIPLE";

	String KEY_NON_FINANCIAL_CMR = "HAS_NON_FINANCIAL_CMR";
	
	String KEY_NON_FINANCIAL_CMR_ID = "NON_FINANCIAL_CMR_ID";
	
	String KEY_NF_MATCHING_CMR_ID = "NF_MATCHING_CMR_ID";
	
	String KEY_NF_APPLICATION_ID = "NF_APPLICATION_ID";	

	String KEY_AUTH_REPS_ORGA_IDEN_ID = "KEY_AUTH_REPS_ORGA_IDEN_ID";
	
	String CASE_NUMBER = "CASE_NUMBER";

	String REASON = " - Reason - ";

	String AUTO_LINK = "AUTO_LINK";

	String NON_FINANCIAL_CMR = "NON_FINANCIAL_CMR";
	
	String NON_FINANCIAL_CMR_ID = "NON_FINANCIAL_CMR_ID";

	String MULTIPLE_CMR = "MULTIPLE_CMR";
	
	// added for externalAssisterID constant
	String ASSISTER_EXTERNALID = "Assister ExternalID";

	// added for agentNpn constant
	String BROKER_NPN = "Broker NPN";

	String SUCCESS_CREATING_REFERRAL_APPLICATION = "Referral Ssapapplication created successfully for GI_WS_PAYLOAD_ID - ";

	String ERROR_CREATING_REFERRAL_APPLICATION = "Error creating Referral for GI_WS_PAYLOAD_ID - ";

	String SUCCESS_EXECUTING_DEMOGRAPHIC_COMPARE = "Successfull execution of demographic compare for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_DEMOGRAPHIC_COMPARE = "Error executing demographic compare ";

	String SUCCESS_EXECUTING_ADMIN_UPDATE = "Successfull execution of admin update for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_ADMIN_UPDATE = "Error executing admin update ";

	String SUCCESS_EXECUTING_LINK_CMR = "Successfull execution of cmr linking process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LINK_CMR = "Error executing cmr linking process ";

	String ERROR_EXECUTING_AT_ADAPTER = "Error executing At adapter";

	String SUCCESS_EXECUTING_ELIGIBILITY_DECISION = "Successfull execution of Eligibility decision process for SSAP_APPLICATION_CASE_NUMBER - ";

	String ERROR_EXECUTING_ELIGIBILITY_DECISION = "Error executing Eligibility decision process";

	String ERROR_EXECUTING_LCE_AT_TRANSFORM = "Error executing LCE At transformer";

	String ERROR_EXECUTING_LCE_COMPARE = "Error executing LCE compare process for SSAP_APPLICATION_CASE_NUMBER - ";

	String LCECOMPARE_PROCESSING_RESULT = "LCECOMPARE_PROCESSING_RESULT";

	String SUCCESS_EXECUTING_LCE_REFERRAL_DESCISION = "Successfull execution of LCE decision process for SSAP_APPLICATION_CASE_NUMBER - ";

	String ERROR_EXECUTING_LCE_REFERRAL_DESCISION = "Error executing LCE decision process";

	String SUCCESS_EXECUTING_QE_REFERRAL_DESCISION = "Successfull execution of QE cmr linking for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_QE_REFERRAL_DESCISION = "Error executing QE cmr linking and decision process";

	String ERROR_EXECUTING_QE_AT_TRANSFORM = "Error executing QE At transformer";

	String SUCCESS_EXECUTING_LCE_DEMO_HANDLER = "Successfull execution of LCE Demo handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_DEMO_HANDLER = "Error executing LCE Demo handler process";

	String SUCCESS_EXECUTING_LCE_DEMO_DOB_HANDLER = "Successfull execution of LCE Demo Dob handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_DEMO_DOB_HANDLER = "Error executing LCE Demo Dob handler process";

	String SUCCESS_EXECUTING_LCE_ELG_LOST_HANDLER = "Successfull execution of LCE Eligibility Lost handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_ELG_LOST_HANDLER = "Error executing LCE Eligibility Lost handler process";

	String SUCCESS_EXECUTING_LCE_APTC_ONLY_HANDLER = "Successfull execution of LCE APTC Only handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_APTC_ONLY_HANDLER = "Error executing LCE APTC Only handler process";

	String SUCCESS_EXECUTING_LCE_CITIZENSHIP_HANDLER = "Successfull execution of LCE Citizenship handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_CITIZENSHIP_HANDLER = "Error executing LCE Citizenship handler process";

	String SUCCESS_EXECUTING_LCE_RELATIONSHIP_HANDLER = "Successfull execution of LCE Relationship handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_RELATIONSHIP_HANDLER = "Error executing LCE Relationship handler process";

	String SUCCESS_EXECUTING_LCE_ALL_CHANGES_HANDLER = "Successfull execution of LCE All Changes handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_ALL_CHANGES_HANDLER = "Error executing LCE All Changes handler process";

	String SUCCESS_EXECUTING_LCE_NO_CHANGES_HANDLER = "Successfull execution of LCE No Changes handler process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_LCE_NO_CHANGES_HANDLER = "Error executing LCE No Changes handler process";

	String SUCCESS_EXECUTING_RENEWAL_DECISION = "Successfull execution of Renwal decision process for SSAP_APPLICATION_CASE_NUMBER - ";

	String ERROR_EXECUTING_RENEWAL_DECISION = "Error executing Renwal decision process";

	String SUCCESS_EXECUTING_NF_DETERMINATION  = "Successfull execution of non financial determination process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_NF_DETERMINATION = "Error executing non financial determination process ";
	
	String SUCCESS_EXECUTING_NF_CONVERSION  = "Successfull execution of non financial conversion process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_NF_CONVERSION = "Error executing non financial conversion process ";

	String SUCCESS_EXECUTING_NF_APTC_UPDATE  = "Successfull execution of non financial APTC Update process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_NF_APTC_UPDATE = "Error executing non financial APTC Update process ";

	String SUCCESS_EXECUTING_REN_NF_DETERMINATION  = "Successfull execution of renewal non financial determination process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_REN_NF_DETERMINATION = "Error executing renewal non financial determination process ";
	
	String SUCCESS_EXECUTING_REN_NF_CONVERSION  = "Successfull execution of renewal non financial conversion process for SSAP_APPLICATION_ID - ";

	String ERROR_EXECUTING_REN_NF_CONVERSION = "Error executing renewal non financial conversion process";
	
	String ERROR_CALLING_EXTERNAL_ASSISTER = "Error calling external assister designate api - ";
	
	String REQUESTER_AT = "AT";
	
	String REQUESTER_MES_BATCH = "MES_BATCH";
	
	String ERROR_DTERMINING_PROCESSOR_STEP = "Error in determining processor step for GI_WS_PAYLOAD_ID - ";
	
	String ACCOUNT_TRANSFER_REQUEST_DTO = "ACCOUNT_TRANSFER_REQUEST_DTO";

	String ERROR_IN_MES_PROCESSING = "Error in MES processing.";
	
	String KEY_CURRENT_SPAN = "CURRENT_SPAN";
	
	String KEY_TOTAL_SPAN = "TOTAL_SPAN";
	
	String SPAN_INFO_DTO = "SPAN_INFO_DTO";
	
	String REFERRAL_RESPONSE_MAP = "REFERRAL_RESPONSE_MAP";
	
	String PROCESSOR_REQUESTER = "PROCESSOR_REQUESTER";

	String BATCH_SUCCESS = "BATCH_SUCCESS";
	
	String BATCH_FAILURE = "BATCH_FAILURE";
	
	String AT_SUCCESS = "AT_SUCCESS";
	
	String AT_FAILURE = "AT_FAILURE";
	
	int ADDRESS_LINE_1_MAX_LENGTH = 88;
	
	int ADDRESS_LINE_2_MAX_LENGTH = 75;
	
	int CITY_MAX_LENGTH = 100;
}
