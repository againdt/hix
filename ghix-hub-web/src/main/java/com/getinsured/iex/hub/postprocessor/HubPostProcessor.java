package com.getinsured.iex.hub.postprocessor;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.getinsured.iex.hub.platform.messaging.PostProcessor;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;
import com.rabbitmq.client.Channel;

/**
 * Post Processor kicks in after a HUB service invocation is complete. Post processing will be called irrespective of the 
 * Service invocation results
 * @author chaudhary_a
 *
 */
public class HubPostProcessor extends PostProcessor implements ChannelAwareMessageListener{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HubPostProcessor.class);
	
	private RestTemplate restTemplate;
	
	
	private HubResponseRepository hubResponseRepository;
	
	@SuppressWarnings("unchecked")
	@Override
	public void onMessage(Message message, Channel channel) {
		JsonMessageConverter conv = new JsonMessageConverter();
		
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		LOGGER.debug("Routing key: "+ routingKey);
		
		Object payLoadObj = conv.fromMessage(message);
		
		//Message is for invoking rules engine
		if(StringUtils.equalsIgnoreCase(routingKey, RULES_ROUTING_KEY)){
			Map<String, Object> payload = (HashMap<String, Object>) payLoadObj;
			String input = (String) payload.get(INPUT_KEY);
			String response = (String)payload.get(RESPONSE_KEY);
			String ruleName = (String)payload.get(RULE_NAME_KEY);
			LOGGER.info("Processing Rule:"+ruleName);
			this.invokeRulesEngine(input, response, ruleName);
		}
		//Message is for persisting
		else if (StringUtils.equalsIgnoreCase(routingKey, PERSISTENCE_ROUTING_KEY)){
			GIHUBResponse response = (GIHUBResponse) payLoadObj;
			response  = hubResponseRepository.save(response);
			LOGGER.info("Done with persisting response with ID:"+response.getId());
		}
	}
	
	public void invokeRulesEngine(String jsonInput, String hubResponse,String ruleName){
		 MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	     map.add("ssapJson", jsonInput);
	     map.add("hubResponse", hubResponse);
	     
	     if(ruleName.equalsIgnoreCase(PostProcessor.SSN_VERIFICATION_RULE)){
	    	 LOGGER.debug(restTemplate.postForObject("http://localhost:8080/ghix-eligibility-svc/verification/rule/ssn", map, String.class));
	     }
	     if(ruleName.equalsIgnoreCase(PostProcessor.INCARCERATION_VERIFICATION_RULE)){
	    	 LOGGER.debug(restTemplate.postForObject("http://localhost:8080/ghix-eligibility-svc/verification/rule/incarceration", map, String.class));
	     }
	     if(ruleName.equalsIgnoreCase(PostProcessor.DEATH_VERIFICATION_RULE)){
	    	 LOGGER.debug(restTemplate.postForObject("http://localhost:8080/ghix-eligibility-svc/verification/rule/death", map, String.class));
	     }
		
	}
	
	
}
