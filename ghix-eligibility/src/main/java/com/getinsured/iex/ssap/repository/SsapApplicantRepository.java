package com.getinsured.iex.ssap.repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

@RepositoryRestResource(path="SsapApplicant")
public interface SsapApplicantRepository extends JpaRepository<SsapApplicant, Long> {

	@RestResource(path="find-by-id")
	public SsapApplicant findSsapApplicantById(@Param("id") long id);

	@RestResource(path="findByExternalApplicantId")
	List<SsapApplicant> findByExternalApplicantId(@Param("externalApplicantId") String externalApplicantId);

	@RestResource(path="findByApplicantGuidAndSsapApplication")
	SsapApplicant findByApplicantGuidAndSsapApplication(String applicantGuid, SsapApplication ssapApplication);
	/*
	@RestResource(path="findByPersonId")
	SsapApplicant findByPersonId(@Param("personId") long personId);*/

	List<SsapApplicant> findBySsapApplication(SsapApplication ssapApplication);

	@RestResource(path="findBySsapApplicationIdAndPersonId")
	@Query("from SsapApplicant where ssapApplication.id = :applicationId and personId = :personId and onApplication='Y'")
	SsapApplicant findByPersonId(@Param("applicationId") long applicationId, @Param("personId") long personId);

	@Query("select externalApplicantId from SsapApplicant where ssapApplication.id = :applicationId and personId = :personId ")
	String getExternalIdBySsapApplicationIdandPersonId(@Param("applicationId") long applicationId, @Param("personId") long personId);

	@Query("from SsapApplicant where ssapApplication.id = :applicationId and onApplication='Y'")
	List<SsapApplicant> findBySsapApplicationId(@Param("applicationId") long applicationId);
	
	@Query("from SsapApplicant where ssapApplication.id = :applicationId and onApplication='Y' and eligibilityStatus='QHP' and applyingForCoverage='Y' ")
	List<SsapApplicant> findEligibleMembersBySsapApplicationId(@Param("applicationId") long applicationId);
	
	
    @Query("from SsapApplicant where applicantGuid = :applicantGuid and ssapApplication.id = :applicationId")
    SsapApplicant findBySsapApplicantGuidAndSsapApplicationId(@Param("applicantGuid") String applicantGuid, @Param("applicationId") long applicationId);
    
    @Query("from SsapApplicant where externalApplicantId = :externalApplicantId and ssapApplication.id = :applicationId")
    SsapApplicant findByExternalApplicantIdAndSsapApplicationId(@Param("externalApplicantId") String externalApplicantId, @Param("applicationId") long applicationId);

	@Modifying
	@Transactional
	@Query("update SsapApplicant set onApplication = :on_application, applyingForCoverage = :applyingForCoverage  where ssapApplication.id = :ssapApplicationId "
			+ "and applicantGuid = :applicant_guid")
	void updateApplicantOnApplicationFlag(@Param("on_application") String on_application,
			@Param("ssapApplicationId") long ssapApplicationId, 
			@Param("applicant_guid")  String applicant_guid,
			@Param("applyingForCoverage")  String applyingForCoverage);

	/**
	 * Added to return selected columns for verification
	 * @param ssapApplicantId
	 * @param personId
	 * @return
	 */
	@RestResource(path="findBySsapApplicationIdAndPersonIdSelectedColumns")
	@Query("select sat.firstName,sat.lastName,sat.ssn,sat.birthDate from SsapApplicant sat where ssapApplication.id = :applicationId "
			+ "and personId = :personId and onApplication='Y'")
	Object[] findByPersonIdSelectedColumns(@Param("applicationId") long applicationId, @Param("personId") long personId);

	@RestResource(path="getApplicants")
	@Query("Select ssapApplicant " +
			" FROM SsapApplicant as ssapApplicant "+
			" inner join fetch ssapApplicant.ssapVerifications as ssapVerifications "+
			" where ssapApplicant.id = :id")
	List<SsapApplicant> getApplicants(@Param("id") long id);


	@RestResource(path="getEligibilitiesForApplicationId")
	@Query("Select ssapApplicant " +
			" FROM SsapApplicant as ssapApplicant "+
			" inner join fetch ssapApplicant.eligibilityProgram as eligibilityProgram"+
			" inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
			" where ssapApplication.id = :ssapApplicationId ")
	List<SsapApplicant> getEligibilitiesForApplicationId(@Param("ssapApplicationId") long ssapApplicationId);

	@RestResource(path="getApplicantEligibilitiesForApplicationId")
	@Query("Select ssapApplicant " +
			" FROM SsapApplicant as ssapApplicant "+
			" left join fetch ssapApplicant.eligibilityProgram as eligibilityProgram"+
			" inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
			" where ssapApplication.id = :ssapApplicationId ")
	List<SsapApplicant> getApplicantEligibilitiesForApplicationId(@Param("ssapApplicationId") long ssapApplicationId);

	@Modifying
	@Transactional
	@Query("update SsapApplicant set tobaccouser = :tobaccoUsage, isEnrldCessPrgrm= :cessationProgram, maritialStatusId= :maritalStatus  where id = :id")
	void updateApplicantFlags(@Param("tobaccoUsage") String tobaccoUsage, @Param("cessationProgram") String cessationProgram, @Param("maritalStatus") BigDecimal maritalStatus,@Param("id") long id);

	/*@Modifying
	@Transactional
	@Query("update SsapApplicant set tobaccouser = :tobaccoUsage, isEnrldCessPrgrm= :cessationProgram, maritialStatusId= :maritalStatus , ecnNumber= :ecnNumber , hardshipExempt= :hardshipExempt where applicantGuid = :applicantGuid")
	public void updateApplicantFlagsForApplicant(@Param("tobaccoUsage") String tobaccoUsage, @Param("cessationProgram") String cessationProgram, @Param("maritalStatus") BigDecimal maritalStatus,@Param("applicantGuid") String applicantGuid,@Param("ecnNumber") String ecnNumber,@Param("hardshipExempt") String hardshipExempt);
	 */

	@Modifying
	@Transactional
	@Query("update SsapApplicant set tobaccouser = :tobaccoUsage, isEnrldCessPrgrm= :cessationProgram, maritialStatusId= :maritalStatus, lastUpdateTimestamp = :lastUpdateTimestamp  where id = :id")
	void updateApplicantFlags(@Param("tobaccoUsage") String tobaccoUsage, @Param("cessationProgram") String cessationProgram, 
			@Param("maritalStatus") BigDecimal maritalStatus,
			@Param("id") long id,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@Modifying
	@Transactional
	@Query("update SsapApplicant set tobaccouser = :tobaccoUsage, isEnrldCessPrgrm= :cessationProgram, maritialStatusId= :maritalStatus , ecnNumber= :ecnNumber , hardshipExempt= :hardshipExempt, "
			+ "lastUpdateTimestamp = :lastUpdateTimestamp where applicantGuid = :applicantGuid and ssapApplication.id = (select id from SsapApplication where caseNumber = :caseNumber)")
	public void updateApplicantFlagsForApplicant(@Param("tobaccoUsage") String tobaccoUsage, @Param("cessationProgram") String cessationProgram, 
			@Param("maritalStatus") BigDecimal maritalStatus,@Param("applicantGuid") String applicantGuid,
			@Param("ecnNumber") String ecnNumber,@Param("hardshipExempt") String hardshipExempt,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp,
			@Param("caseNumber") String caseNumber);


	@Query("select count(id) from SsapApplicant where ssapApplication.id = :applicationId and status in (:statusList)")
	long countofApplicantsWithDemoUpdates(@Param("applicationId") long applicationId, @Param("statusList") List<String> statusList);


	@Query("select max(personId) from SsapApplicant where ssapApplication.id = (select id from SsapApplication where caseNumber = :caseNumber)")
	public Integer getMaxPersonId(@Param("caseNumber") String caseNumber);

	@Modifying
	@Transactional
	@Query("update SsapApplicant set ssnVerificationStatus = 'VERIFIED', citizenshipImmigrationStatus = 'VERIFIED', vlpVerificationStatus='VERIFIED', "
			+ " incomeVerificationStatus='VERIFIED', incarcerationStatus = 'VERIFIED' , mecVerificationStatus = 'VERIFIED', "
			+ " deathStatus = 'VERIFIED' , residencyStatus = 'VERIFIED' , nativeAmericanVerificationStatus = 'VERIFIED'"
			+ "  where id = :id ")
	int updateApplicantVerificationStatus(@Param("id") long id);

	@Query("from SsapApplicant sa join fetch sa.ssapApplicantEvents as sae where sa.ssapApplication.caseNumber = :caseNumber and sae.sepEvents.gated in :gatedStatus")
	public Set<SsapApplicant> findApplicantForApplication(@Param("caseNumber") String caseNumber,@Param("gatedStatus") List<Gated> gatedStatus);
	
	@Query("SELECT COUNT(*) FROM SsapApplication ssap, SsapApplicant applicant"
			+ " WHERE applicant.ssapApplication.id = ssap.id"
			+ " AND ssap.cmrHouseoldId = :householdCaseId"
			+ " AND applicant.personId = :personId"
			+ " AND applicant.ssn LIKE '%' || :ssn")
	long verifyIfSSNMatches(@Param("personId") long personId, @Param("householdCaseId") BigDecimal householdCaseId, @Param("ssn") String ssn);
	
	@Query("from SsapApplicant s where s.ssn = :ssn and s.birthDate = :birthDate and s.personId = 1 and s.ssapApplication.cmrHouseoldId <> null order by s.creationTimestamp desc")
	List<SsapApplicant> findBySsnAndDob(@Param("ssn") String ssn, @Param("birthDate") Date birthDate);

	/**
	 * This method returns matching SSAP_Applicants based on SSN AND DOB. Results also include UnClaimed Applications so these can be closed.
	 * @param ssn
	 * @param birthDate
	 * @return
	 */
	@Query("from SsapApplicant s where s.ssn = :ssn and s.birthDate = :birthDate and s.personId = 1  order by s.creationTimestamp desc")
	List<SsapApplicant> findBySsnAndDobWithAllCMR(@Param("ssn") String ssn, @Param("birthDate") Date birthDate);

	
	@Query("from SsapApplicant s where s.ssn = :ssn and s.birthDate = :birthDate and s.personId = 1 and s.ssapApplication.cmrHouseoldId = :cmrHouseoldId order by s.creationTimestamp desc")
	List<SsapApplicant> findBySsnAndDobAndCmrHousehold(@Param("ssn") String ssn, @Param("birthDate") Date birthDate,@Param("cmrHouseoldId") BigDecimal cmrHouseoldId);
	
	@Query("from SsapApplicant s where lower(s.firstName) = :firstName and lower(s.lastName) = :lastName and s.birthDate = :birthDate and s.personId = 1  order by s.creationTimestamp desc")
	List<SsapApplicant> findByNameAndDobWithAllCMR(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("birthDate") Date birthDate);
	
	@Query("from SsapApplicant s where s.personId = 1 and ssapApplication.id = :applicationId order by s.creationTimestamp desc")
	List<SsapApplicant> findPrimaryByApplication(@Param("applicationId") long applicationId);
	
	@Query("from SsapApplicant where ssapApplication.id = :applicationId and personType in ('PTF','PC_PTF') and onApplication='Y'")
	SsapApplicant findPTF(@Param("applicationId") long applicationId);

	@Query("Select ssapApplicant " +
			" FROM SsapApplicant as ssapApplicant "+
			" inner join fetch ssapApplicant.eligibilityProgram as eligibilityProgram"+
			" inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
			" where ssapApplication.id = :ssapApplicationId" +
			" and eligibilityProgram.eligibilityType in (:eligibilityTypes)" +
			" and eligibilityProgram.eligibilityIndicator = 'TRUE' ")
	List<SsapApplicant> getEligibilitiesForApplicantsByAppId(@Param("ssapApplicationId") long ssapApplicationId, @Param("eligibilityTypes") List<String> eligibilityTypes);

	@Query("SELECT sas " +
			"FROM SsapApplicant sas inner join fetch sas.ssapApplication as ssapApplication " +
			"WHERE ssapApplication.id IN (:applicationId) " +
			"ORDER BY sas.ssapApplication.id")
	List<SsapApplicant> findAllEligibleBasedOnApplicationId(@Param("applicationId") List<Long> applicationId);

	@Query("SELECT sas " +
			"FROM SsapApplicant sas inner join fetch sas.ssapApplication as ssapApplication " +
			"WHERE ssapApplication.id IN (:applicationId) " +
			"ORDER BY sas.ssapApplication.id")
	List<SsapApplicant> findAllEligibleBasedOnApplicationIds(@Param("applicationId") List<? extends Long> applicationId);
}