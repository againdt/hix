package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionResponse;
import com.getinsured.iex.ssap.model.SsapApplication;


public interface SsapApplicationService {

	List<SsapApplication> findByCaseNumber(String caseNumber);

	List<SsapApplication> findByExternalCaseNumber(String caseNumber);

	void closeApplication(SsapApplication application);

	void closeApplication(long id);
	
	List<SsapApplication> findByHHIdAndCoverageYear(BigDecimal householdId,Long coverageYear);
	
	List<SsapApplication> findByHHCaseIdAndCoverageYear(String caseNumber,Long coverageYear);

	boolean checkIsEditApplicationFlow(long ssapId) throws Exception;

	void updateIsEditApplicationFlag(String caseNumber, Boolean isEditApplicationFlag);

	EligEngineHouseholdCompositionResponse getHouseholdComposition(Long ssapApplicationId);
	
	SsapApplication findById(long ssapId);
}
