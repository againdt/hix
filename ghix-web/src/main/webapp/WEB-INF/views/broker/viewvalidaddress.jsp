<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	
	
	<div class="row-fluid">
	<div class="span3" id="rightpanel"></div>
		<form class="form-vertical" id="frmviewAddressList" name="frmviewAddressList" action="viewvalidaddress" method="POST" target="_parent">
			<div class="span9" id="rightpanel">
				<div class="gutter10">
					<div>
						<c:if test="${enteredLocation != null}">
							<h4><a name="skip" class="skip"><spring:message  code="label.brkaddressentered"/></br></a></h4>
							<table class="table table-border-none">
									<tbody>
										<tr>
											<td><input type="radio" value="${enteredLocation.address1}, ${enteredLocation.address2}, ${enteredLocation.city}, ${enteredLocation.state}, ${enteredLocation.zip}" name="addr" id="userdefault"/></td>
											<td class="txt-right"><spring:message  code="label.brkAddress1"/></td>
											<td><strong>${enteredLocation.address1}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkAddress2"/></td>
											<td><strong>${enteredLocation.address2}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkCity"/></td>
											<td><strong>${enteredLocation.city}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkState"/></td>
											<td><strong>${enteredLocation.state}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkZip"/></td>
											<td><strong>${enteredLocation.zip}</strong></td>
										</tr>
									</tbody>
							</table>
						</c:if>
					</div>
					<br/><br/>
					<div class="page-header">
						<h4><a name="skip" class="skip">Valid Address List</a></h4> 
					</div>		
					<div class="row-fluid">
						<div id="brokerlist">
							<input type="hidden" name="id" id="id" value="">
							<c:if test="${fn:length(validAddressList) > 0}">
								<c:forEach items="${validAddressList}" var="address" begin="0" varStatus="rowCounter">
									<table class="table table-border-none">
									<tbody>
										<tr>
											<td><input type="radio" value="${address.address1},${address.address2},${address.city},${address.state},${address.zip}" name="addr" id="${rowCounter.index}"/></td>
											<td class="txt-right"><spring:message  code="label.brkAddress1"/></td>
											<td><strong>${address.address1}</strong></td>
										</tr>
										<tr>
											<td></td>										
											<td class="txt-right"><spring:message  code="label.brkAddress2"/></td>
											<td><strong>${address.address2}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkCity"/></td>
											<td><strong>${address.city}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkState"/></td>
											<td><strong>${address.state}</strong></td>
										</tr>
										<tr>
											<td></td>
											<td class="txt-right"><spring:message  code="label.brkZip"/></td>
											<td><strong>${address.zip}</strong></td>
										</tr>
									</tbody>
							</table>
								</c:forEach>
							</c:if>
							</br>
							<input name="submitAddr" id="submitAddr" type="button" onClick="fun();" value='Submit' title="Submit" class="btn btn-primary pull-right" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div><!-- .span9 -->
	
	<script>
		function fun() {
			$("#frmviewAddressList").submit();
		}
	</script>
