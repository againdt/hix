<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="stateCode" value="${StateConfiguration.stateCode}" />
<c:if test="${stateCode == 'CA'}">
	<jsp:include page="./ca/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'CT'}">
	<jsp:include page="./ct/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'ID'}">
	<jsp:include page="./id/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'MN'}">
	<jsp:include page="./mn/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'MS'}">
	<jsp:include page="./ms/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'NM'}">
	<jsp:include page="./nm/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'NV'}">
	<jsp:include page="./nv/employeeOpenEnrollSideBar.jsp" />
</c:if>
<c:if test="${stateCode == 'WA'}">
	<jsp:include page="./wa/employeeOpenEnrollSideBar.jsp" />
</c:if>
