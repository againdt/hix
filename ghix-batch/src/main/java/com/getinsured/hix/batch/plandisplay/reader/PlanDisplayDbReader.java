package com.getinsured.hix.batch.plandisplay.reader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class PlanDisplayDbReader implements ItemReader<PlanDisplayHealthBenefitMapper>, ItemStream {
	
	private static Logger logger = LoggerFactory.getLogger(PlanDisplayDbReader.class);


	private JdbcTemplate jdbcTemplate;
	private ResultSet resultSet = null;
	Connection connection = null;
	private Statement statement=null;
	@Override
	public PlanDisplayHealthBenefitMapper read() throws SQLException  {
		PlanDisplayHealthBenefitMapper mapper = new PlanDisplayHealthBenefitMapper();
		try{
			 if (resultSet.next()) {
				
				mapper = mapper.mapRow(this.resultSet,0);
			}
		}catch(Exception e){
			logger.error("Encountered Exception PlanDisplayMapper record",e );
			e.printStackTrace();
			throw e;
		}
		return mapper;
	}

	@Override
	public void open(ExecutionContext executionContext) {
		try {
			this.connection  =  jdbcTemplate.getDataSource().getConnection();
			this.statement = connection.createStatement();
			//this.resultSet = statement.executeQuery(PlanDisplayMapper.QUERY);
			
			System.out.println("size--"+resultSet.getFetchSize());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		logger.info("Opened Item stream for reading providers");
		
	}

	@Override
	public void update(ExecutionContext executionContext)
			throws ItemStreamException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() {
		if(this.connection != null){
			try 
			{
				this.connection.close();
				logger.info("closing connection-------------->");
			} catch (SQLException e) {
				logger.warn("Exception "+e.getMessage()+" encountered while closing the connection, Ignoring");
			}
		}
		logger.info("Closing provider read stream");

	}
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public static void main(String[] args) throws UnexpectedInputException,
	ParseException, NonTransientResourceException, Exception {
		String[] config = { "applicationContext.xml", "spring-batch.xml"  };
		ApplicationContext context = new ClassPathXmlApplicationContext(config);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job providerDatajob = (Job) context.getBean("ProviderDbDirectorySyncJob");

		try {
			/*
			 * Added this line of code to set new parameters to job instance
			 */
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();

			// JobExecution fac_execution = jobLauncher.run(facilityDataJob,
			// jobParameters);
			// System.out.println("Exit Status [Facility] : " +
			// fac_execution.getStatus());
			JobExecution idv_execution = jobLauncher.run(providerDatajob,jobParameters);
			logger.info("Exit Status [Individual] : "+ idv_execution.getStatus());

		} catch (Exception e) {
			logger.error("Error :",e);
		}
	}


}
