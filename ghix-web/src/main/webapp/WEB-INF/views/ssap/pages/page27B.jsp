<%@ include file="../include.jsp" %>
<div id="appscr77Part2" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page27B" text="Employer Health Coverage Information for " />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr77Part2Form" class="form-horizontal">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.whatTheNameOfTheLowestCostSelfOnlyHealthPlanOfferedTo" text="What&#39;s the name of the lowest cost self-only health plan offered to" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.QM" text="?" />
					<small>
						<spring:message code="ssap.page27B.onlyIncludePlans" text="(Only include plans that meet the &#39;minimum value standard&#39; set by the Affordable Care Act.)" />
					</small>
					<a href="#"><spring:message code="ssap.page27B.employerCoverageForm" text="Employer Coverage Form" /></a>
				</p><!-- end of label -->
				<div class="margin5-l">
					<div for="currentLowestCostSelfOnlyPlanName">
						<input placeHolder="<spring:message code="ssap.page27B.healthPlanName" text="Health Plan Name"/> " class="input-medium" type="text" id="currentLowestCostSelfOnlyPlanName" name="appscr77p2HealthPlanName" />
					</div>
					
					<label class="checkbox">
						<input type="checkbox" name="appscr77p1dontKnow" value="0"><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know" />
					</label>
					
					<label class="checkbox">
						<input type="checkbox" id="currentPlanMeetsMinimumStandard" name="currentPlanMeetsMinimumStandard" value="0" checked="checked" />
						<span><spring:message code="ssap.page27B.noPlansMeetTheMinimumValuStandard" text="No plans meet the minimum value standard" /></span>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.whatTheNameOfTheLowestCostSelfonlyHealthPlanThatWillBeOfferedTo" text="What&#39;s the name of the lowest cost self-only health plan that will be offered to" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.in" text="in" /> <span class='coverageYear'></span> <spring:message code="ssap.page27B.QM" text="?" />
					<small>
						<spring:message code="ssap.page27B.onlyIncludePlans" text="(Only include plans that meet the &#39;minimum value standard&#39; set by the Affordable Care Act.)" />
					</small>
					<a href="#"><spring:message code="ssap.page27B.employerCoverageForm" text="Employer Coverage Form" /></a>
				</p><!-- end of label -->
				<div class="margin5-l">
					<div for="comingLowestCostSelfOnlyPlanName">
						<input placeHolder="<spring:message code="ssap.page27B.healthPlanName" text="Health Plan Name"/> " class="input-medium" type="text" id="comingLowestCostSelfOnlyPlanName" name="appscr77p2HealthPlanNameCoverageYear" />
					</div>
					
					<label class="checkbox">
						<input type="checkbox" name="appscr77p2CoverageForm" value="0"><spring:message code="ssap.page27B.dontKnow" text="I	don&#39;t know" />
					</label>
					
					<label class="checkbox">
						<input type="checkbox" id="comingPlanMeetsMinimumStandard" name="appscr77p2CoverageForm" value="0" />
						<spring:message code="ssap.page27B.noPlansMeetTheMinimumValuStandard" text=" No plans meet the minimum value standard" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.howMuchWould" text="How much would" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.payInPremiumsToEnrollInThisPlan" text=" pay in premiums to enroll in this plan?" />
				</p>
				<label class="control-label">
					<spring:message code="ssap.page27B.amount" text="Amount:" />
					<strong>$</strong>
				</label><!-- end of label -->
				<div class="controls">
					<input placeHolder="<spring:message code="ssap.page27B.dollerAmount" text=" Dollar Amount" />" type="text" id="lowestCostSelfOnlyPremiumAmount" name="appscr77p2Payment" class="input-medium" />
					<label class="checkbox">
						<input type="checkbox" name="appscr77p2PaymentDontKnow" value="0" /><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.howMuchWould" text="How much would" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.payInPremiumsToEnrollInThisPlan" text="pay in premiums to enroll in this plan?"/>
				</p>
				<label class="control-label">
					<spring:message code="ssap.page27B.amount" text="Amount:" />
					<strong>$</strong>
				</label><!-- end of label -->
				<div class="controls">
					<input placeHolder="<spring:message code="ssap.page27B.fr" text="Frequency:"/>" type="text" id="lowestCostSelfOnlyPremiumFrequency" name="appscr77p2PremiumsPayment" class="input-medium" />
					<label class="checkbox">
						<input type="checkbox" name="naturalizedCitizen" value="0" /><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.does" text="Does" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.thinkThisCoverageIsAffordable" text="think this coverage is affordable?" />
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="appscr77p2IsAffordable" value="1" /><spring:message code="ssap.page27B.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" name="appscr77p2IsAffordable" checked="checked" value="0" /><spring:message code="ssap.page27B.no" text=" No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
		</form>
	</div>
</div>