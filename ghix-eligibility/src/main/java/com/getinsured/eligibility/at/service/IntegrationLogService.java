package com.getinsured.eligibility.at.service;

import com.getinsured.iex.ssap.model.SsapIntegrationLog;



/**
 * Encapsulates service layer method calls for IntegrationLogService.
 *
 */
public interface IntegrationLogService{

	SsapIntegrationLog save(SsapIntegrationLog integrationLog);

	void save(String payload, String serviceName, String status, Long ssap_pk, Long giWsPayloadId);
	
	SsapIntegrationLog findByGiWsPayloadId(long giWsPayloadId);

}
