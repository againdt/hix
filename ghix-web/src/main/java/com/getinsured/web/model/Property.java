package com.getinsured.web.model;

/**
 * Describes environment properties.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public class Property
{
  private PropertyType type;
  private String value;

  public PropertyType getType()
  {
    return type;
  }

  public void setType(final PropertyType type)
  {
    this.type = type;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(final String value)
  {
    this.value = value;
  }
}
