<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>


<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>
	
<div class="gutter10-lr">
	<div class="row-fluid">
		<c:choose>
			<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<div class="l-page-breadcrumb hide"></div>
			</c:when>				
			<c:otherwise>	
				<div class="l-page-breadcrumb">
				<!--start page-breadcrumb -->
					<div class="row-fluid">
						<ul class="page-breadcrumb">
							<li><a href="javascript:history.back()">&lt; <spring:message  code="label.back"/></a></li>
							<li><a href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
							<li><spring:message code="label.paymentinfo" /></li>
						</ul>
					</div>
					<!--  end of row-fluid -->
				</div>
			</c:otherwise>
	    </c:choose>
	</div>
	<div class="row-fluid">		
		<c:choose>
				<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus =='' || broker.certificationStatus == 'Incomplete'}">	
					<h1><spring:message code="label.newBrokerReg"/></h1>
				</c:when>
				<c:otherwise>	
					<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1> 
				</c:otherwise>
		 </c:choose>			 
	</div>

	<div class="row-fluid">
		<!-- #sidebar -->      
			<jsp:include page="brokerLeftNavigationMenu.jsp" />
		<!-- #sidebar ENDS -->
			
		<div class="span9" id="rightpanel">
			<div class="header">
				<c:choose>
				<c:when test="${paymentMethodsObj != null}">
					<h4><spring:message code="label.paymentinfo"/></h4>						
					<a class="btn btn-small pull-right"  title="Cancel" href="/hix/broker/viewpaymentinfo"><spring:message code="label.brkCancel"/></a> 
					<a class="btn btn-primary btn-small pull-right" type="button"  title="Save" id="save" name="save"><spring:message code="label.saveBtn"/></a>			
				</c:when>
				<c:otherwise>					
					<h4><spring:message code="label.brlsteppaymentinfo"/></h4>								
				</c:otherwise>
				</c:choose>		
			</div>
				
						<p class="gutter10-t"><strong><spring:message code="label.brkbankaccntdetails"/></strong></p>
						<p><spring:message code="label.brkbankaccountinfo"/></p>
	
				
			<form class="form-horizontal gutter10 brokerAddressValidation" id="frmbrokereditpayment" name="frmbrokereditpayment" action="paymentinfo" method="POST">
			<df:csrfToken/>
		    <input type="hidden" id="paymentId" name="paymentId" value="${paymentMethodsObj.id}"/>
			  
			   <c:choose>
				   <c:when test="${paymentMethodsObj.id == null}">
					<input type="hidden" id="redirectTo" name="redirectTo" value="addpaymentinfo"/>
				</c:when>
				<c:otherwise>
					<input type="hidden" id="redirectTo" name="redirectTo" value="editpaymentinfo"/>					
				</c:otherwise>
			  </c:choose>	
			  	
			  <div class="control-group">
				<label for="paymentMethod" class="control-label"><spring:message code="label.paymentMethod"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
					<div class="controls">
					<label class="radio inline" for="CHECK">
							<input type="radio"	${paymentMethodsObj.paymentType== 'CHECK' ? 'checked="checked"' : ''}	name="paymentType" id="CHECK" value="CHECK"><spring:message code="label.check"/>
						</label>
						<label class="radio inline" for="ACH">
							<input type="radio" ${paymentMethodsObj.paymentType== 'ACH' ? 'checked="checked"' : ''}	name="paymentType" id="ACH" value="ACH"><spring:message code="label.ach"/> 
						</label>
					
						<div id="CHECK_error"></div>
					</div>
				</div>
				<div class="row-fluid" id="eftDetailsDiv" style="display:none;">
	  			   <div class="control-group">
                                   <label for="bankName" class="control-label"><spring:message  code="label.bankName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                                   <div class="controls">
                                   	<input type="text" class="input-xlarge" id="bankName" name="financialInfo.bankInfo.bankName" size="30" maxlength="50" value="${paymentMethodsObj.financialInfo.bankInfo.bankName}">
                                   	<div id="bankName_error"></div>
                                   </div>
                               </div>
                               
                               <div class="control-group">
                                    <label for="bankABARoutingNumber" class="control-label"><spring:message  code="label.routingNumber"/> <a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover"  href="#">									<i class="icon-question-sign"></i></a><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                                   <div class="controls">
                                   	<input type="text" class="input-xlarge" id="bankABARoutingNumber" name="financialInfo.bankInfo.routingNumber"  value="${paymentMethodsObj.financialInfo.bankInfo.routingNumber}" maxlength="9" size="30">
                                   	<div id="bankABARoutingNumber_error"></div>
                                   </div>
                               </div>
                               
                               <div class="control-group">
                                   <label for="accountNumber" class="control-label"><spring:message  code="label.accountNumber"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                                   <div class="controls">
                                   	<input type="text" class="input-xlarge" id="bankAccountNumber" name="financialInfo.bankInfo.accountNumber" maxlength="20" size="30" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}">
                                   	<div class="" id="bankAccountNumber_error"></div>
                                   </div>
                               </div>
                               
                               <div class="control-group">
                                   <label for="bankAccountName" class="control-label"><spring:message  code="label.nameofaccount"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                                   <div class="controls">
                                   	<input type="text" class="input-xlarge" id="bankAccountName" name="financialInfo.bankInfo.nameOnAccount" placeholder="Last name, First name" size="30" value="${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}">
                                   	<div id="bankAccountName_error"></div>
                                   </div>
                               </div>
                               
                                <div class="control-group">
                                   <label for="bankAccountType" class="control-label"><spring:message  code="label.accountType"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                                   <div class="controls">
                                  <select name="financialInfo.bankInfo.accountType" id="bankAccountType">
							<option value="">Select</option>
							<option value="C" <c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>>Checking</option>
							<option value="S" <c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>>Savings</option>
						  	</select>
							<div id="bankAccountType_error"></div>
                            	</div>
							</div>
						</div>
						
				<div class="row-fluid" id="paymentAddressDetailsDiv" style="display:none;">
					<div class="control-group">
						<label for="address1_mailing" class="control-label required"><spring:message
								code="label.streetaddress" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text"
								placeholder="Street Name, P.O. Box, Company, c/o"
								name="financialInfo.address1" id="address1_mailing"
								value="${paymentMethodsObj.financialInfo.address1}"
								class="input-xlarge" size="30" /> <input type="hidden"
								id="address1_mailing_hidden" name="address1_mailing_hidden"
								value="${paymentMethodsObj.financialInfo.address1}">
							<div id="address1_mailing_error"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->
	
					<div class="control-group">
						<label for="address2_mailing" class="control-label"><spring:message
								code="label.suite" /></label>
						<div class="controls">
							<input type="text"
								placeholder="Apt, Suite, Unit, Bldg, Floor, etc"
								name="financialInfo.address2" id="address2_mailing"
								value="${paymentMethodsObj.financialInfo.address2}"
								class="input-xlarge" size="30" /> <input type="hidden"
								id="address2_mailing_hidden" name="address2_mailing_hidden"
								value="${paymentMethodsObj.financialInfo.address2}">
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->
	
					<div class="control-group">
						<label for="city_mailing" class="control-label"><spring:message
								code="label.brkCity" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" placeholder="City, Town"
								name="financialInfo.city" id="city_mailing"
								value="${paymentMethodsObj.financialInfo.city}"
								class="input-xlarge" size="30" /> <input type="hidden"
								id="city_mailing_hidden" name="city_mailing_hidden"
								value="${paymentMethodsObj.financialInfo.city}">
							<div id="city_mailing_error"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->
	
					
	
					<div class="control-group">
						<label for="state_mailing" class="control-label"><spring:message
								code="label.brkState" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<select size="1" id="state_mailing" name="financialInfo.state"
								path="statelist" class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}"
										<c:if test="${state.code == paymentMethodsObj.financialInfo.state}"> selected="selected" </c:if>
										value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select> <input type="hidden" id="state_mailing_hidden"
								name="state_mailing_hidden"
								value="${paymentMethodsObj.financialInfo.state}">
							<div id="state_mailing_error"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->
					
					<div class="control-group">
						<label for="zip_mailing" class="control-label"><spring:message
								code="label.brkZipCode" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" placeholder="" name="financialInfo.zipcode"
								id="zip_mailing"
								value="${paymentMethodsObj.financialInfo.zipcode}"
								class="input-mini zipCode" maxlength="5" /> <input
								type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden"
								value="${paymentMethodsObj.financialInfo.zipcode}">
							<div id="zip_mailing_error"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->
				
					<input type="hidden" name="mailingLocation.lat" id="lat_mailing" value="${broker.mailingLocation.lat != null ? broker.mailingLocation.lat : 0.0}" />
					<input type="hidden" name="mailingLocation.lon" id="lon_mailing" value="${broker.mailingLocation.lon != null ? broker.mailingLocation.lon : 0.0}" />
					<input type="hidden" name="mailingLocation.rdi" id="rdi_mailing" value="${broker.mailingLocation.rdi != null ? broker.mailingLocation.rdi : ''}" />
					<input type="hidden" name="mailingLocation.county" id="county_mailing" value="${broker.mailingLocation.county != null ? broker.mailingLocation.county : ''}" />
					
				</div>
				<div>
						<c:if test="${paymentMethodsObj==null}">
							<input type="button" name="back" id="back" value="<spring:message code="label.brkBack"/>" class="btn pull-left" onClick="window.location.href='/hix/broker/buildprofile'"/>
							<input type="submit" name="finish" id="finish" value="<spring:message code="label.brkFinish"/>" class="btn btn-primary pull-right"/>	
						</c:if>																							
				</div><!-- end of form-actions -->
			</form>
		</div><!-- end of span9 -->
	</div><!--  end of row-fluid -->
 </div>
		
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p><spring:message code="label.brkstaticnotes"/></p>
				</div>
			</div>
		</div><!--  end of .notes -->
	
<script type="text/javascript">
$('a[rel=popover]').popover({
	  html: true,
	  trigger: 'hover',
	  content: function () {
	    return '<img src="'+$(this).data('img') + '" />';
	  }
	});

	
	/*Radio button event on educationGrant*/	
    $("input[name=paymentType]").click(function() {
    	
    	var optionVal = $("input[name=paymentType]:checked").val();
    	if(optionVal == 'CHECK') {
    		$('#eftDetailsDiv').hide();
    		$('#paymentAddressDetailsDiv').show();
    	} else if(optionVal == 'ACH') {		
    		$('#eftDetailsDiv').show();
    		$('#paymentAddressDetailsDiv').hide();
    	}
	});
	
	$(document).ready(function() {	
		
		$(document).ready(function() {
			var paymentError = '${paymentInfoValidated}';
			var paymentErrorMsg = '${paymentErrorMsg}';					
			if ('true' == paymentError) {							

			    $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${paymentErrorMsg} </p>'
					       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
			   }
			
		});
		
		$("#brkPaymentInfo").removeClass("link");
		$("#brkPaymentInfo").addClass("active");
		
		//Added this code so that if education grant exists, it displays this block
		var optionVal = '${paymentMethodsObj.paymentType}';    	
		if(optionVal == 'CHECK') {
			$('#eftDetailsDiv').hide();
			$('#paymentAddressDetailsDiv').show();
		} else if(optionVal == 'ACH') {		
			$('#eftDetailsDiv').show();
			$('#paymentAddressDetailsDiv').hide();
		}
    	
		
		var brkId = ${broker.id};

		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};
		
		
		var test = document.createElement('input');
		if (!('placeholder' in test)) {
		    $('input').each(function () {
		        if ($(this).attr('placeholder') != "" && this.value == "") {
		            $(this).val($(this).attr('placeholder'))
		                   .css('color', 'grey')
		                   .on({
		                       focus: function () {
		                         if (this.value == $(this).attr('placeholder')) {
		                           $(this).val("").css('color', '#000');
		                         }
		                       },
		                       blur: function () {
		                         if (this.value == "") {
		                           $(this).val($(this).attr('placeholder'))
		                                  .css('color', 'grey');
		                         }
		                       }
		                   });
		        }
		    });
		}
	});
	
	jQuery.validator.addMethod("notDefaultText", function (value, element) {
		if (value != $(element).attr('placeholder')) {
			return true;
		}
	});

	jQuery.validator.addMethod("validZip", function(value, element, param) {
		var optionVal = $("input[name=paymentType]:checked").val();
    	if(optionVal == 'CHECK') {
		if((isNaN(value) ||(value=="") || (value.length < 5 ) || (value == '00000')))
		{
				return false; 
		}
    	}
		return true;
	});
	
	jQuery.validator.addMethod("AccountNumberCheck", function(value, element, param) {
		var accountNumber = $("#bankAccountNumber").val();
		var accountNumberPriv = '${paymentMethodsObj.financialInfo.bankInfo.accountNumber}';
		if(accountNumber == accountNumberPriv){
			return true;
		}
		var chechF=(/^[0-9]*$/.test(accountNumber));
		if(!chechF){
			return false;
		}
		if(accountNumber == "" || isNaN(accountNumber) || accountNumber.length < 9){
			return false;
		}
		return true;
	});
	
	jQuery.validator.addMethod("disallowAllZero", function(value, element, param) {
		var bankRoutingNumber = $('#bankABARoutingNumber').val();
		var disallowAllZeroPtr = /^[0]{1,9}$/;
		if(disallowAllZeroPtr.test(bankRoutingNumber)){
			return false;
		}
		return true;
	});
	
	$(function() {
		$("#save").click(function(e){
			if($("#frmbrokereditpayment").validate().form()) {
				$("#frmbrokereditpayment").submit();
			}
		});
	});
	
	var validator = $("#frmbrokereditpayment").validate({ 
		onkeyup : false,
		onclick : false,
		rules : {
			paymentType : {required : true},
			"financialInfo.bankInfo.bankName" : {required : true},
			"financialInfo.bankInfo.nameOnAccount" : {required : true, notDefaultText : true},				
			"financialInfo.bankInfo.accountNumber" : {required : true, AccountNumberCheck : true},
			"financialInfo.bankInfo.routingNumber" : {required : true, number : true, minlength : 9, disallowAllZero : true},
			"financialInfo.bankInfo.accountType" : {required : true},
			"financialInfo.address1":{required:true, notDefaultText : true},
			"financialInfo.city":{required:true, notDefaultText : true},
			"financialInfo.state":{required:true},
			"financialInfo.zipcode":{required:true,number:true,validZip:true}
		},
		messages : {
			paymentType : { required : "<span><em class='excl'>!</em>Please select Payment Method</span>"},
			"financialInfo.bankInfo.bankName" : { required : "<span><em class='excl'>!</em>Please enter bank name</span>"},
			"financialInfo.bankInfo.nameOnAccount" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctName' javaScriptEscape='true'/></span>",
				notDefaultText : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctName' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.accountNumber" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumber' javaScriptEscape='true'/></span>",
			AccountNumberCheck : "<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidAccountNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.routingNumber" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankABARoutingNumber' javaScriptEscape='true'/></span>",
				disallowAllZero : "<span><em class='excl'>!</em><spring:message  code='err.bankABARoutingNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.accountType" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAccountType' javaScriptEscape='true'/></span>"},
			"financialInfo.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>", 
				notDefaultText : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
			"financialInfo.city" : {  required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>", 
				notDefaultText : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
			"financialInfo.state" : {  required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
			"financialInfo.zipcode" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
				                        validZip: "<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});	
	

	/*focus out event for the text input to call the lightbox*/
	$('.zipCode').focusout(function(e) {
		if ($(this).val().length == 5){	
			var startindex = (e.target.id).indexOf("_");
			var index = (e.target.id).substring(startindex,(e.target.id).length);
			var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
			var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
			address1_e=address1_e+index;
			address2_e=address2_e+index;
			city_e=city_e+index;
			state_e=state_e+index;
			zip_e=zip_e+index;
			lat_e=lat_e+index;
			lon_e=lon_e+index;
			rdi_e+=index;
			county_e+=index;
			var model_address1 = address1_e + '_hidden' ;
			var model_address2 = address2_e + '_hidden' ;
			var model_city = city_e + '_hidden' ;
			var model_state = state_e + '_hidden' ;
			var model_zip = zip_e + '_hidden' ;
			
			var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
			if(($('#'+ address1_e).val() != "Street Name, P.O. Box, Company, c/o")&&($('#'+ city_e).val() != "City, Town")&&($('#'+ state_e).val() != "State")&&($('#'+ zip_e).val() != '')){
				if(($('#'+ address2_e).val())==="Apt, Suite, Unit, Bldg, Floor, etc"){
					$('#'+ address2_e).val('');
				}	
				viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);	
			}
		}
	});
</script>

