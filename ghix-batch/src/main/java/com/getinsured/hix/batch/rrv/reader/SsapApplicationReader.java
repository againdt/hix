package com.getinsured.hix.batch.rrv.reader;

import com.getinsured.eligibility.rrv.service.RrvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;

@SuppressWarnings("Duplicates")
public class SsapApplicationReader implements ItemReader<Long>, ItemStream{

    private static final Logger logger = LoggerFactory.getLogger(SsapApplicationReader.class);

    private static final int FETCH_SIZE = 500;

    @Autowired
    private DataSource batch_dataSource;

    @Autowired
    RrvService rrvService;

    private ResultSet resultSet;
    private Connection con;
    private PreparedStatement preparedStatement;

    private long coverageYear;
    private String applicationStatus;

    private static final String ssap_application_id = "ssap_application_id";

    private static final String QUERY =
            "SELECT " +
            "   DISTINCT sa.id                          AS ssap_application_id " +
            "FROM " +
            "    ssap_applications       sa " +
            "WHERE " +
            "    sa.application_status = ? " +
            "            AND sa.coverage_year = ? " +
            "               AND sa.CMR_HOUSEOLD_ID NOT " +
            "in " +
            " " +
            "( SELECT " +
            "    household_id " +
            "FROM " +
            "    rrv_submission " +
            "WHERE " +
            "    coverage_year = ? " +
            ")";

    @PostConstruct
    public void initSettings() throws NamingException {
        if(batch_dataSource == null){
            Context initialContext = new InitialContext();
            this.batch_dataSource = (DataSource)initialContext.lookup("java:comp/env/jdbc/ghixBatchDS");
        }
    }

    @Override
    public Long read() {
        logger.info("Reading record from query");
        try{
            if(this.resultSet.next()){

                return resultSet.getLong(ssap_application_id);
            }
        }
        catch(SQLException e){
            logger.error("Failed to get records from DB",e);
        }
        logger.info("SSAP Application Reader Returning NULL.");
        return null;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

        try {
            this.con = this.batch_dataSource.getConnection();
            this.preparedStatement = con.prepareStatement(QUERY);
            logger.info("SSAP APPLICATION READER QUERY: ");
            logger.info(QUERY);
            this.preparedStatement.setFetchSize(FETCH_SIZE);
            this.preparedStatement.setString(1, applicationStatus);
            this.preparedStatement.setLong(2, coverageYear);
            this.preparedStatement.setLong(3, coverageYear);
            this.resultSet = this.preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new ItemStreamException(e.getMessage(),e);
        }
        logger.info("Opened Item stream for reading HUB responses");
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        logger.info("Closing DB connection");
        if(this.con != null){
            try {
                this.preparedStatement.close();
                this.con.close();
            } catch (SQLException e) {
                logger.error("Failed to close DB connection, Ignoring",e);
            }
        }
    }

    public long getCoverageYear() {
        return coverageYear;
    }

    public void setCoverageYear(long coverageYear) {
        this.coverageYear = coverageYear;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public DataSource getBatch_dataSource() {
        return batch_dataSource;
    }

    public void setBatch_dataSource(DataSource batch_dataSource) {
        this.batch_dataSource = batch_dataSource;
    }

    public RrvService getRrvService() {
        return rrvService;
    }

    public void setRrvService(RrvService rrvService) {
        this.rrvService = rrvService;
    }
}
