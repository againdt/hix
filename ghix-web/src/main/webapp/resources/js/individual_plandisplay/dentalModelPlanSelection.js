(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.dentalModelPlans();
		var cartCollection = new App.collections.Carts();
		var cartModel = new App.models.Cart();
		App.views.dentalModelPlans = new App.views.dentalModelPlans({collection:collection,cartModel:cartModel,cartCollection:cartCollection});
		
	});

})();