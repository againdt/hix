package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class AutoRenewalValidationHandler implements ErrorHandler {

	final List<SAXParseException> exceptions = new ArrayList<SAXParseException>();

	public String getErrors() {
		return exceptions.stream().map(ex -> ex.toString()).collect(Collectors.joining("\n"));
	}
	
	public void warning(SAXParseException ex) {
		exceptions.add(ex);
	}

	public void error(SAXParseException ex) {
		exceptions.add(ex);
	}

	public void fatalError(SAXParseException ex) throws SAXException {
		exceptions.add(ex);
	}


}
