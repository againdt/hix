<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Review, Declare, & File</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/bootstrap.css">
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghixcustom.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghix-blue-theme.css"> -->
<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ms-custom.css">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/eligibilitynm/css/planView-v2.css" type="text/css">
<link rel="stylesheet" href="../resources/eligibilitynm/css/planViewStyles-v2.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/datepicker.css" />
<style type="text/css">
.row-fluid .span12 {
	width:99.9%;
}
.clear {
	clear:both;
	display:block;
}
.form-vertical hr {
	margin: 10px 0;
	clear: both;
	display: block;
}
h4 {
	font-size:18px;
	line-height:30px;
	padding:10px 0 0;
}
h5 {
	padding:10px 0 0;
}
input.no-margin {
	margin:0;
}
input.txt-center {
	text-align:center;
}
</style>
</head>

<body>

<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="gutter10 span3">
        <ul class="nav nav-pills">
          <li> <a href="e1-1-start-app">Back </a> </li>
        </ul>
      </div>
      <div class="span9 pull-left">
        <h3>Review, Declare, &amp; File</h3>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li><a href="e1-1-start-app">Start Your Application <i class="icon-chevron-right"></i></a></li>
        <li><a href="e3-build-your-household">Build Your Household <i class="icon-chevron-right"></i></a></li>
        <li><a href="e4-household-income">Your Household Income <i class="icon-chevron-right"></i></a></li>
        <li><a href="e5-additional-questions">Additional Questions <i class="icon-chevron-right"></i></a></li>
        <li><a href="e6-review-declare-file">Review, Declare &amp; File <i class="icon-chevron-right"></i></a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="gutter10">
         <p>Records with the New Mexico Department of Corrections indicate that Cynthia Manning is currently incarcerated, which means she cannot enroll in a health plan through the New Mexico Exchange.  If you believe this to be an error, Cynthia Manning can attest to the fact that she is not currently incarcerated and we will continue with the eligibility determination, plan shopping and enrollment process. However, after submitting the attestation, you will likely need to provide documentary evidence to the Exchange to support it. </p>
        <form class="form-vertical">

        <div class="control-group">
              <p>Would Cynthia Manning like to submit an attestation to the Exchange?</p>
              <label class="radio inline">
                <input type="radio" name="attestation" value="Yes" checked>
                Yes </label>
              <label class="radio inline">
                <input type="radio" name="attestation" value="No">
                No </label>
            </div>


            <div class="control">
              <label class="control-label" for="inputInitials"></label>
              <div class="controls">
                <input type="text" id="inputInitials" placeholder="" class="span1">
                By including my initials, I, Cynthia Manning, hereby attest that I am not currently incarcerated.
              </div>
            </div>
            <button class="btn pull-right">Submit</button>

    </form>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->
</div>


<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
</script>
</body>
</html>
