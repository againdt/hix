package com.getinsured.hix.indportal.dto;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.hix.broker.utils.ApplicationStatus;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;



/**
 * DTO used on MyApplications tab
 * @author Sunil Desu
 *
 */
public class MyApplicationsEntry {

	private long coverageYear;

	private String applicationName;

	private String applicationStatus;
	
	private ApplicationValidationStatus validationStatus;

	private String caseNumber;

	private String enableEnrollButton;

	private String enableVerificationResultsButton="true";

	private String enableEligibilityResultsButton;

	private String enableCancelButton="true";

	private boolean enableCancelOfEnrolledApp;

	private boolean isApplicationCurrent;

	private boolean isApplicationPast;

	private boolean applicationInProgress;
	private boolean isNonFinancial = true;
	
	private boolean isSepApp;
	
	private boolean isQep;
	
	private static List<String> statusForCancel = new ArrayList<String>();
	
	private boolean isSepQepDenied = false;
	
	//HIX-58446
	private String lastUpdated;
	
	private String createdDate;
	
	//HIX-86254
	private String appType;
	
//	/HIX-89105
    private String showEnrollButton;
    
    //HIX-100825
    private String aptc;

	//HIX-113026
    private String stateSubsidy;

    private String csr;
    
    /* HIX-101101 */
    private String eligibilityStatus;
    
    private String ptfFirstName;    
    private String ptfLastName;
    private String ptfMiddleName;
    
    private boolean isApplicationQueued = false;

    //HIX-113602
    private String dueDate;
	private String eligibilityStartDate;
    private String eligibilityEndDate;

    private String queuedStatus;
    private String applicationDentalStatus;
    
    private SimpleDateFormat formator = new SimpleDateFormat(IndividualPortalConstants.MEDIUM_DATE_TIME_FORMAT);
    private SimpleDateFormat dateFormator = new SimpleDateFormat(IndividualPortalConstants.MEDIUM_DATE_FORMAT);

    private boolean editable;


	public void setApplicationQueued(boolean isApplicationQueued) {
		this.isApplicationQueued = isApplicationQueued;
	}

	public String getQueuedStatus() {
		return queuedStatus;
	}

	public void setQueuedStatus(String queuedStatus) {
		this.queuedStatus = queuedStatus;
	}

	static{
		statusForCancel.add("OP");
		statusForCancel.add("SG");
		statusForCancel.add("SU");
		statusForCancel.add("ER");
	}
	
	public MyApplicationsEntry(SsapApplicationResource ssapApplicationResource, boolean isIndividual) {

		this.setCoverageYear(ssapApplicationResource.getCoverageYear());

		this.setApplicationName("Individual and Family Coverage");
		
		String status = ssapApplicationResource.getApplicationStatus();
		String applicationSource = ssapApplicationResource.getSource();
		String financialAssitanceFlag = ssapApplicationResource.getFinancialAssistanceFlag();
		String allowEnrollment = ssapApplicationResource.getAllowEnrollment();
		String qStatus = ssapApplicationResource.getQueuedStatus();
		this.setApplicationStatus(ApplicationStatus.valueOf(status).getDescription());
		this.setValidationStatus(ssapApplicationResource.getValidationStatus());

		this.setCaseNumber(ssapApplicationResource.getCaseNumber());

		this.setEnableEnrollButton((StringUtils.equalsIgnoreCase(status,"ER")
				&& StringUtils.equalsIgnoreCase(allowEnrollment,"Y")) ? "true" : "false");
		
		if(StringUtils.equalsIgnoreCase(applicationSource,"RF") && 
				StringUtils.equalsIgnoreCase(financialAssitanceFlag,"Y") && 
				IndividualPortalConstants.CONDITIONAL_ELIGIBILITIES.contains(ssapApplicationResource.getEligibilityStatus())){
			this.setEnableVerificationResultsButton("false");
		}
		
		// FIXME: Set it based on application status
		this.setEnableEligibilityResultsButton("true");
		
		/*HIX-58830: 
		 * Added the condition to set the EnableCancelButton flag to false,
		 * on all other cases by default the value is true.
		 */
		
		if (StringUtils.equalsIgnoreCase(financialAssitanceFlag,"Y") && isIndividual) {
		   this.setEnableCancelButton("false");
		}else if (!statusForCancel.contains(status.toUpperCase())) {
		   this.setEnableCancelButton("false");
		}

		if (StringUtils.equalsIgnoreCase(financialAssitanceFlag,"Y") && isIndividual) {
			   this.setEnableCancelOfEnrolledApp(false);
		}else if ("EN".equalsIgnoreCase(status)) {
			   this.setEnableCancelOfEnrolledApp(true);
		}


		determineCurrentOrPastApplication(ssapApplicationResource);
		
		if(StringUtils.equalsIgnoreCase(status, "OP") || StringUtils.equalsIgnoreCase(status, "UC")){
			this.setApplicationInProgress(true);
		}
		if(StringUtils.equalsIgnoreCase(financialAssitanceFlag,"Y")){
			this.setIsNonFinancial(false);
			
			//HIX-100825
			if(ssapApplicationResource.getMaximumAPTC() != null)
				this.setAptc(String.valueOf(ssapApplicationResource.getMaximumAPTC()));

			if(ssapApplicationResource.getMaximumStateSubsidy() != null){
				this.setStateSubsidy(String.valueOf(ssapApplicationResource.getMaximumStateSubsidy()));
			}

			this.setCsr(ssapApplicationResource.getCsrLevel());
		}
		
		/*
		 * HIX-55772 - Check if application is in SEP
		 * @author - Nikhil Talreja
		 */
		if(StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationType(), "SEP")){
			this.setIsSepApp(true);
		}
		
		//Check for QEP
		if(StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationType(), "QEP")){
			this.setIsQep(true);
		}
		
		//HIX-58446
		this.setLastUpdated(formator.format(new Date(ssapApplicationResource.getLastUpdateTimestamp().getTime())));
		
		//HIX-63251 SEP/QEP denied
		if ((this.getIsQep() || this.getIsSepApp()) && 
				StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationStatus(), "CL") &&
				StringUtils.equalsIgnoreCase(ssapApplicationResource.getSepQepDenied(), "Y"))
		{
			this.setIsSepQepDenied(true);
		}
		
		//HIX-82146
		this.setCreatedDate(formator.format(new Date(ssapApplicationResource.getCreationTimestamp().getTime())));

		
		// HIX-86254
		this.setAppType(SsapApplicationTypeEnum.value(ssapApplicationResource.getApplicationType()));
		
		if(StringUtils.equalsIgnoreCase(ssapApplicationResource.getNativeAmerican(),"Y")) {
			this.setShowEnrollButton("Y");
		} else {
			this.setShowEnrollButton("N");
		}
		
		/* HIX-101101 */
		if(ssapApplicationResource.getEligibilityStatus() != null){
			this.setEligibilityStatus(ssapApplicationResource.getEligibilityStatus());
		}
		
		this.setPtfFirstName(ssapApplicationResource.getPtfFirstName());
		this.setPtfLastName(ssapApplicationResource.getPtfLastName());
		this.setPtfMiddleName(ssapApplicationResource.getPtfMiddleName());
		
		if(ssapApplicationResource.getDueDate() != null) {
    	   this.setDueDate(dateFormator.format(ssapApplicationResource.getDueDate()));
		}
		if(ssapApplicationResource.getEligibilityStartDate() != null) {
			this.setEligibilityStartDate(dateFormator.format(ssapApplicationResource.getEligibilityStartDate()));
		}
		if(ssapApplicationResource.getEligibilityEndDate() != null) {
			this.setEligibilityEndDate(dateFormator.format(ssapApplicationResource.getEligibilityEndDate()));
		}
		
		if(ssapApplicationResource.getQueuedStatus() != null) {
		this.setQueuedStatus(AccountTransferQueueStatus.valueOf(qStatus).getDescription());
		}
		
		this.setApplicationDentalStatus(ssapApplicationResource.getApplicationDentalStatus());
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	
	public ApplicationValidationStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(ApplicationValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getEnableEnrollButton() {
		return enableEnrollButton;
	}

	public void setEnableEnrollButton(String enableEnrollButton) {
		this.enableEnrollButton = enableEnrollButton;
	}

	public String getEnableVerificationResultsButton() {
		return enableVerificationResultsButton;
	}

	public void setEnableVerificationResultsButton(
			String enableVerificationResultsButton) {
		this.enableVerificationResultsButton = enableVerificationResultsButton;
	}

	public String getEnableEligibilityResultsButton() {
		return enableEligibilityResultsButton;
	}

	public void setEnableEligibilityResultsButton(
			String enableEligibilityResultsButton) {
		this.enableEligibilityResultsButton = enableEligibilityResultsButton;
	}

	public String getEnableCancelButton() {
		return enableCancelButton;
	}

	public void setEnableCancelButton(String enableCancelButton) {
		this.enableCancelButton = enableCancelButton;
	}

	public boolean isEnableCancelOfEnrolledApp() {
		return enableCancelOfEnrolledApp;
	}

	public void setEnableCancelOfEnrolledApp(boolean enableCancelOfEnrolledApp) {
		this.enableCancelOfEnrolledApp = enableCancelOfEnrolledApp;
	}

	public boolean getIsApplicationCurrent() {
		return isApplicationCurrent;
	}

	public void setIsApplicationCurrent(boolean isApplicationCurrent) {
		this.isApplicationCurrent = isApplicationCurrent;
	}

	public boolean getIsApplicationPast() {
		return isApplicationPast;
	}

	public void setIsApplicationPast(boolean isApplicationPast) {
		this.isApplicationPast = isApplicationPast;
	}

	public boolean getApplicationInProgress() {
		return applicationInProgress;
	}

	public void setApplicationInProgress(boolean applicationInProgress) {
		this.applicationInProgress = applicationInProgress;
	}

	public String getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(String stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}

	private void determineCurrentOrPastApplication(SsapApplicationResource ssapApplicationResource) {
		 String applicationStatus = ssapApplicationResource.getApplicationStatus();
		if (applicationStatus.equalsIgnoreCase("CC") || applicationStatus.equalsIgnoreCase("CL") || applicationStatus.equalsIgnoreCase("DQ")) {
			this.setIsApplicationCurrent(false);
			this.setIsApplicationPast(true);
		}
		else if (applicationStatus.equalsIgnoreCase("OP") || applicationStatus.equalsIgnoreCase("SU") || applicationStatus.equalsIgnoreCase("SG")
				 		|| applicationStatus.equalsIgnoreCase("SU")	|| applicationStatus.equalsIgnoreCase("ER") || applicationStatus.equalsIgnoreCase("EN")	
				 		|| applicationStatus.equalsIgnoreCase("PN")	|| applicationStatus.equalsIgnoreCase("UC")){
			
			this.setIsApplicationCurrent(true);
			this.setIsApplicationPast(false);
		}
		else if (ssapApplicationResource.getApplicationStatus().equalsIgnoreCase("QU")) {
			this.setIsApplicationQueued(true);
			this.setIsApplicationCurrent(false);
			this.setIsApplicationPast(false);
		} 
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MyApplicationsEntry [coverageYear=");
		builder.append(coverageYear);
		builder.append(", applicationName=");
		builder.append(applicationName);
		builder.append(", applicationStatus=");
		builder.append(applicationStatus);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", enableEnrollButton=");
		builder.append(enableEnrollButton);
		builder.append(", enableVerificationResultsButton=");
		builder.append(enableVerificationResultsButton);
		builder.append(", enableEligibilityResultsButton=");
		builder.append(enableEligibilityResultsButton);
		builder.append(", enableCancelButton=");
		builder.append(enableCancelButton);
		builder.append(", isApplicationCurrent=");
		builder.append(isApplicationCurrent);
		builder.append(", isApplicationPast=");
		builder.append(isApplicationPast);
		builder.append(", isApplicationQueued=");
		builder.append(isApplicationQueued);
		builder.append(", applicationInProgress=");
		builder.append(applicationInProgress);
		builder.append("]");
		return builder.toString();
	}


	public boolean getIsNonFinancial() {
		return isNonFinancial;
	}

	public void setIsNonFinancial(boolean isNonFinancial) {
		this.isNonFinancial = isNonFinancial;
	}

	public boolean getIsSepApp() {
		return isSepApp;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void setIsSepApp(boolean isSepApp) {
		this.isSepApp = isSepApp;
	}

	public boolean getIsQep() {
		return isQep;
	}

	public void setIsQep(boolean isQep) {
		this.isQep = isQep;
	}

	public boolean getIsSepQepDenied() {
		return isSepQepDenied;
	}

	public void setIsSepQepDenied(boolean isSepQepDenied) {
		this.isSepQepDenied = isSepQepDenied;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the appType
	 */
	public String getAppType() {
		return appType;
	}

	/**
	 * @param appType the appType to set
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}

	/**
	 * @return the showEnrollButton
	 */
	public String getShowEnrollButton() {
		return showEnrollButton;
	}

	/**
	 * @param showEnrollButton the showEnrollButton to set
	 */
	public void setShowEnrollButton(String showEnrollButton) {
		this.showEnrollButton = showEnrollButton;
	}

	/**
	 * @return the aptc
	 */
	public String getAptc() {
		return aptc;
	}

	/**
	 * @param aptc the aptc to set
	 */
	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	/**
	 * @return the csr
	 */
	public String getCsr() {
		return csr;
	}

	/**
	 * @param csr the csr to set
	 */
	public void setCsr(String csr) {
		this.csr = csr;
	}

	/**
	 * @return the eligibilityStatus
	 */
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	/**
	 * @param eligibilityStatus the eligibilityStatus to set
	 */
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getPtfFirstName() {
		return ptfFirstName;
	}

	public void setPtfFirstName(String ptfFirstName) {
		this.ptfFirstName = ptfFirstName;
	}

	public String getPtfLastName() {
		return ptfLastName;
	}

	public void setPtfLastName(String ptfLastName) {
		this.ptfLastName = ptfLastName;
	}

	public String getPtfMiddleName() {
		return ptfMiddleName;
	}

	public void setPtfMiddleName(String ptfMiddleName) {
		this.ptfMiddleName = ptfMiddleName;
	}
	
	public void setIsApplicationQueued(boolean isApplicationQueued) {
		this.isApplicationQueued = isApplicationQueued;
	}

	public boolean getIsApplicationQueued() {
		return isApplicationQueued;
	}

	//HIX-113602

	 public String getDueDate() {
			return dueDate;
		}

		public void setDueDate(String dueDate) {
			this.dueDate = dueDate;
		}

		public String getEligibilityStartDate() {
			return eligibilityStartDate;
		}

		public void setEligibilityStartDate(String eligibilityStartDate) {
			this.eligibilityStartDate = eligibilityStartDate;
		}

		public String getEligibilityEndDate() {
			return eligibilityEndDate;
		}

		public void setEligibilityEndDate(String eligibilityEndDate) {
			this.eligibilityEndDate = eligibilityEndDate;
		}

		public String getApplicationDentalStatus() {
			return applicationDentalStatus;
		}

		public void setApplicationDentalStatus(String applicationDentalStatus) {
			this.applicationDentalStatus = applicationDentalStatus;
		}

  public boolean isEditable() {
    return editable;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }
}
