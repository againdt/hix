package com.getinsured.iex.hub.vlp37.service;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.vlp37.servicehandler.CloseCaseServiceHandler;

@Service
public class VLP37Service extends MessageProcessor {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private VLP_PostProcessor vlpPostProcessor;
	@Autowired private WebServiceTemplate vlp37Step2ServiceTemplate;
	@Autowired private WebServiceTemplate vlp37Step3ServiceTemplate;
	@Autowired private WebServiceTemplate vlp37closeCaseServiceTemplate;
	
	/**
	 * Invokes Step 2 Request
	 * @param vlpRequestJSON
	 * @return
	 */
	@Transactional
	public RequestStatus invokeInitiateAddVerifRequest(String vlpRequestJSON) {
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-2-H93");
		request.setEndpointOperationName("VLP_STEP2");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_InitiateAdditionalVerif");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided Additional verification");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for  invoke Step 2 (initiate Additional verification");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			this.executeRequest(request, handler, vlp37Step2ServiceTemplate);
			return handler.getRequestStatus();
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return RequestStatus.ADDL_VERIFY_FAILED;

	}
	@Transactional
	public RequestStatus invokeInitiateThirdVerifRequest(String vlpRequestJSON) {
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP-2-H94");
		request.setEndpointOperationName("VLP_STEP3");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_InitiateThirdVerif");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided Additional verification");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for  invoke Step 3 (initiate Third verification");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			this.executeRequest(request, handler, vlp37Step3ServiceTemplate);
			return handler.getRequestStatus();
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return RequestStatus.THIRD_VERIFY_FAILED;

	}
	
	public ResponseEntity<String> invokeCloseCaseRequest(@RequestBody String vlpRequestJSON) {
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("VLP");
		request.setEndpointOperationName("VLP_CLOSE_CASE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VLP37_CloseCaseRequest");
			//vlpServiceBridge.processInputParameter(CASE_NUMBER, "6700000000156AX");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for Close case ");
			}
			
			CloseCaseServiceHandler handler = (CloseCaseServiceHandler) vlpServiceBridge.getServiceHandler();
			Map<String,Object> context = new HashMap<String, Object>();
			context.put(APPLICATION_ID_KEY, requestObj.get(APPLICATION_ID_KEY));
			context.put(ServiceHandler.RESPONSE_HANDLER, vlpPostProcessor);
			handler.setContext(context);
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, vlp37closeCaseServiceTemplate );
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		return response;
	}
	
//	public List<VLP37CaseLog> getCaseHistory(String caseNumber){
//		Session session = factory.unwrap(org.hibernate.Session.class);
//				List history = AuditReaderFactory.get(session)
//				.createQuery()
//				.forRevisionsOfEntity( VLP37CaseLog.class, true, true )
//				.add(AuditEntity.property("case_number").eq(caseNumber) )
//				.getResultList();
//		for(Object obj:history) {
//			
//		}
//	}

}