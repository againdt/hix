package com.getinsured.hix.batch.indportal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.batch.agency.repository.IBrokerRepository;
import com.getinsured.hix.model.Broker;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * BrokerSeriveImpl is the implementation of {@link BrokerService}
 *
 */
@Service("brokerService")
@Repository
@Transactional
public class BrokerServiceImpl implements BrokerService {	
	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String AGENT_STATUS = "agentStatus";
	public static final String LANGUAGE_SPOKEN_PARAM = "languageSpoken_";
	public static final String DISTANCE = "distance";
	public static final String ZIP = "zip";
	public static final String ZIP_SELECT = "zip1";
	public static final String BROKER_LIST = "brokerlist";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final Integer PAGE_SIZE = 10;

	@Autowired
	private IBrokerRepository brokerRepository;

	/**
	 * @see BrokerService#saveBrokerWithLocation(Broker, boolean)
	 */
	@Override
	@Transactional
	public Broker saveBrokerWithLocation(Broker broker) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveBrokerWithLocation : START-> "+startTime);
		Broker brokerObj = null;
		
		try {
			brokerObj = brokerRepository.saveAndFlush(broker);

			// Generate Broker Number if does not exist
			if (brokerObj != null && brokerObj.getId() != 0
					&& (brokerObj.getBrokerNumber() == null || brokerObj.getBrokerNumber() == 0)) {
				//generateBrokerNumber(brokerObj);
			}
			brokerObj = brokerRepository.saveAndFlush(brokerObj);
			

		LOGGER.info("saveBrokerWithLocation: END");
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveBrokerwithLocation method", exception);

		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("saveBrokerWithLocation : END->"+endTime);
		return brokerObj;
	}

	@Override
	public Broker findBrokerByLicenseNumber(String licenseNumber) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public Broker findBrokerByNpn(String npn) {
		Broker brokerObj = null;

		try {
			brokerObj = brokerRepository.findByNpn(npn);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in findBrokerByNpn method", exception);
		}
		return brokerObj;
	}
}
