package com.getinsured.iex.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SSAP_APPLICANTS database table.
 * 
 */
public class SsapApplicant implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private String applyingForCoverage;

	private Date birthDate;

	private String citizenshipImmigrationStatus;

	private BigDecimal citizenshipImmigrationVid;

	private Timestamp creationTimestamp;

	private String deathStatus;

	private BigDecimal deathVerificationVid;

	private String externalApplicantId;

	private String firstName;

	private String gender;

	private String householdContactFlag;

	private BigDecimal householdContactVid;

	private String incarcerationStatus;

	private BigDecimal incarcerationVid;

	private String incomeVerificationStatus;

	private BigDecimal incomeVerificationVid;

	private String lastName;

	private Timestamp lastUpdateTimestamp;

	private BigDecimal mailiingLocationId;

	private String married;

	private String mecVerificationStatus;

	private BigDecimal mecVerificationVid;

	private String middleName;

	private BigDecimal otherLocationId;

	private String residencyStatus;

	private BigDecimal residencyVid;

	private String ssnVerificationStatus;

	private BigDecimal ssnVerificationVid;

	private String tobaccouser;
	
	private SsapApplication ssapApplication;
	
	private long personId;
	
	private String vlpVerificationStatus;

	private long vlpVerificationVid;
	
	private String nativeAmericanFlag;

	
	public String getNativeAmericanFlag() {
		return nativeAmericanFlag;
	}

	public void setNativeAmericanFlag(String nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public SsapApplication getSsapApplication() {
		return ssapApplication;
	}

	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	/*@JsonManagedReference("ssapApplication-ssapApplicant")
	@JsonBackReference
	private SsapApplication ssapApplication;
*/
	private List<SsapVerification> ssapVerifications;

	public SsapApplicant() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplyingForCoverage() {
		return this.applyingForCoverage;
	}

	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

/*	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
*/
	public String getCitizenshipImmigrationStatus() {
		return this.citizenshipImmigrationStatus;
	}

	public void setCitizenshipImmigrationStatus(String citizenshipImmigrationStatus) {
		this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
	}

	public BigDecimal getCitizenshipImmigrationVid() {
		return this.citizenshipImmigrationVid;
	}

	public void setCitizenshipImmigrationVid(BigDecimal citizenshipImmigrationVid) {
		this.citizenshipImmigrationVid = citizenshipImmigrationVid;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getDeathStatus() {
		return this.deathStatus;
	}

	public void setDeathStatus(String deathStatus) {
		this.deathStatus = deathStatus;
	}

	public BigDecimal getDeathVerificationVid() {
		return this.deathVerificationVid;
	}

	public void setDeathVerificationVid(BigDecimal deathVerificationVid) {
		this.deathVerificationVid = deathVerificationVid;
	}

	public String getExternalApplicantId() {
		return this.externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHouseholdContactFlag() {
		return this.householdContactFlag;
	}

	public void setHouseholdContactFlag(String householdContactFlag) {
		this.householdContactFlag = householdContactFlag;
	}

	public BigDecimal getHouseholdContactVid() {
		return this.householdContactVid;
	}

	public void setHouseholdContactVid(BigDecimal householdContactVid) {
		this.householdContactVid = householdContactVid;
	}

	public String getIncarcerationStatus() {
		return this.incarcerationStatus;
	}

	public void setIncarcerationStatus(String incarcerationStatus) {
		this.incarcerationStatus = incarcerationStatus;
	}

	public BigDecimal getIncarcerationVid() {
		return this.incarcerationVid;
	}

	public void setIncarcerationVid(BigDecimal incarcerationVid) {
		this.incarcerationVid = incarcerationVid;
	}

	public String getIncomeVerificationStatus() {
		return this.incomeVerificationStatus;
	}

	public void setIncomeVerificationStatus(String incomeVerificationStatus) {
		this.incomeVerificationStatus = incomeVerificationStatus;
	}

	public BigDecimal getIncomeVerificationVid() {
		return this.incomeVerificationVid;
	}

	public void setIncomeVerificationVid(BigDecimal incomeVerificationVid) {
		this.incomeVerificationVid = incomeVerificationVid;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

/*	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
*/
	public BigDecimal getMailiingLocationId() {
		return this.mailiingLocationId;
	}

	public void setMailiingLocationId(BigDecimal mailiingLocationId) {
		this.mailiingLocationId = mailiingLocationId;
	}

	public String getMarried() {
		return this.married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getMecVerificationStatus() {
		return this.mecVerificationStatus;
	}

	public void setMecVerificationStatus(String mecVerificationStatus) {
		this.mecVerificationStatus = mecVerificationStatus;
	}

	public BigDecimal getMecVerificationVid() {
		return this.mecVerificationVid;
	}

	public void setMecVerificationVid(BigDecimal mecVerificationVid) {
		this.mecVerificationVid = mecVerificationVid;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public BigDecimal getOtherLocationId() {
		return this.otherLocationId;
	}

	public void setOtherLocationId(BigDecimal otherLocationId) {
		this.otherLocationId = otherLocationId;
	}

	public String getResidencyStatus() {
		return this.residencyStatus;
	}

	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}

	public BigDecimal getResidencyVid() {
		return this.residencyVid;
	}

	public void setResidencyVid(BigDecimal residencyVid) {
		this.residencyVid = residencyVid;
	}

	public String getSsnVerificationStatus() {
		return this.ssnVerificationStatus;
	}

	public void setSsnVerificationStatus(String ssnVerificationStatus) {
		this.ssnVerificationStatus = ssnVerificationStatus;
	}

	public BigDecimal getSsnVerificationVid() {
		return this.ssnVerificationVid;
	}

	public void setSsnVerificationVid(BigDecimal ssnVerificationVid) {
		this.ssnVerificationVid = ssnVerificationVid;
	}

	public String getTobaccouser() {
		return this.tobaccouser;
	}

	public void setTobaccouser(String tobaccouser) {
		this.tobaccouser = tobaccouser;
	}

	/*public SsapApplication getSsapApplication() {
		return this.ssapApplication;
	}

	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}*/

	public List<SsapVerification> getSsapVerifications() {
		return this.ssapVerifications;
	}

	public void setSsapVerifications(List<SsapVerification> ssapVerifications) {
		this.ssapVerifications = ssapVerifications;
	}

	public SsapVerification addSsapVerification(SsapVerification ssapVerification) {
		getSsapVerifications().add(ssapVerification);
		ssapVerification.setSsapApplicant(this);

		return ssapVerification;
	}

	public SsapVerification removeSsapVerification(SsapVerification ssapVerification) {
		getSsapVerifications().remove(ssapVerification);
		ssapVerification.setSsapApplicant(null);

		return ssapVerification;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getVlpVerificationStatus() {
		return vlpVerificationStatus;
	}

	public void setVlpVerificationStatus(String vlpVerificationStatus) {
		this.vlpVerificationStatus = vlpVerificationStatus;
	}

	public long getVlpVerificationVid() {
		return vlpVerificationVid;
	}

	public void setVlpVerificationVid(long vlpVerificationVid) {
		this.vlpVerificationVid = vlpVerificationVid;
	}

}