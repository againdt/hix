package com.getinsured.eligibility.ssap.integration.lce.splitter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.messaging.Message;
import org.springframework.integration.annotation.Splitter;
import org.springframework.stereotype.Component;

import com.getinsured.iex.ssap.model.SsapApplicationEvent;

@Component(value = "lceSplitter")
public class LCESplitter {
	
	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	@Splitter
	public List<SsapApplicationEvent> split(final Message message) {
		if(message.getPayload() instanceof SsapApplicationEvent) {
			List<SsapApplicationEvent> ssapApplicationEvents = new ArrayList<SsapApplicationEvent>() {
				{
					add((SsapApplicationEvent)message.getPayload());
				}
			};
			return ssapApplicationEvents;
		}
		return (List<SsapApplicationEvent>) message.getPayload();
	}
}
