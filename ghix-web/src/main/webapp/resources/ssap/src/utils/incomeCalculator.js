import { FREQUENCY_TYPES } from '../constants/frequencyTypes';

const adjustAmountByFrequency = (amount, frequency, cyclesPerFrequency) => {
    switch (frequency) {
    case FREQUENCY_TYPES.QUARTERLY:
        return amount * 4;
    case FREQUENCY_TYPES.MONTHLY:
        return amount * 12;
    case FREQUENCY_TYPES.WEEKLY:
        return amount * 52;
    case FREQUENCY_TYPES.BIMONTHLY:
        return amount * 24;
    case FREQUENCY_TYPES.BIWEEKLY:
        return amount * 26;
    case FREQUENCY_TYPES.DAILY:
        return amount * (cyclesPerFrequency ? cyclesPerFrequency : 5) * 52;
    case FREQUENCY_TYPES.HOURLY:
        return amount * (cyclesPerFrequency ? cyclesPerFrequency : 40) * 52;
    default:
        return amount;
    }
};

const getTotal = (sources, isTribal = undefined) => {
    return sources.reduce((total, source) => {
        const {
            amount,
            frequency,
            tribalAmount,
            cyclesPerFrequency
        } = source;
        total += adjustAmountByFrequency(isTribal ? tribalAmount : amount, frequency, cyclesPerFrequency);
        return total;
    }, 0);
};

const getTotalIncome = (incomes, expenses) => getTotal(incomes) - getTotal(expenses);

const getIncomeOnMonthBase = (incomes, expenses) => {
    const incomeTotal = getTotalIncome(incomes, expenses) - getTotal(incomes, true);
    return Math.round(incomeTotal / 12 * 100) / 100;
};

export default {
    adjustAmountByFrequency,
    getTotalIncome,
    getIncomeOnMonthBase
};