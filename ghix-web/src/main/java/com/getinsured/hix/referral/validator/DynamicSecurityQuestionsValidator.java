package com.getinsured.hix.referral.validator;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component("dynamicSecurityQuestionsValidator")
@Scope("singleton")
public class DynamicSecurityQuestionsValidator implements Validator {

	
	private String FIRST_NAME = "firstName";
	private String LAST_NAME = "lastName";
	private String DOB = "birthDate";
	private java.text.SimpleDateFormat DATE_FORMATTER = new java.text.SimpleDateFormat("MM/dd/yyyy");

	@Override
	public boolean supports(Class<?> arg0) {
		return DynamicSecurityQuestionsDTO.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		DynamicSecurityQuestionsDTO dynamicSecurityQuestionsDTO = (DynamicSecurityQuestionsDTO) arg0;

		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, FIRST_NAME, " Please enter first name.");		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, LAST_NAME, " Please enter last name.");
		
		if(StringUtils.isEmpty(dynamicSecurityQuestionsDTO.getBirthDate()) )
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(arg1, DOB, " Please enter birth date.");
		}
		else if(!isValidFormat(dynamicSecurityQuestionsDTO.getBirthDate()))
		{
			arg1.rejectValue(DOB, "Please enter Date of Birth in required format.");
		}	
	}
	private boolean isValidFormat(String dateValue) {
        Date date = null;
        try {            
            date = DATE_FORMATTER.parse(dateValue);
            if (!dateValue.equals(DATE_FORMATTER.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            return false;
        }
        return date != null;
    }

}
