package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.batch.ssap.renewal.service.CloneService;
import com.getinsured.hix.batch.ssap.renewal.util.ClonePartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;

/**
 * Clone Partitioner class is used to get read SSAP Application IDs from database and do Partitioner.
 * 
 * @since July 30, 2019
 */
@Component("ssapClonePartitioner")
@Scope("step")
public class ClonePartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClonePartitioner.class);

	private Long batchSize;
	private Long coverageYear;
	private Long renewalYear;
	private String cloneToNFA;
	private String applicationStatuses; // List of Application Status separated by semicolon(;)
	private String isCloneProgramEligibility;
	private String isOTREnabled;
	private String cloneCommitInterval;
	private CloneService cloneService;
	private ClonePartitionerParams clonePartitionerParams;
	private BatchJobExecutionService batchJobExecutionService;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();

		try {

			if (!hasRunningBatchSizeOne()) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}

			String defaultBatchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			isOTREnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_YEAR);
			coverageYear = Long.valueOf(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
			renewalYear = Long.valueOf(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			clonePartitionerParams.setRenewalYear(renewalYear);

			if (null == batchSize || 0l == batchSize) {
				batchSize = Long.valueOf(defaultBatchSize);
			}

			if (!validateParams(errorMessage)) {
				return partitionMap;
			}

			List<Long> partitionerSsapApplicationIdList = null;
			clonePartitionerParams.clearApplicationStatusList();

			if (null != applicationStatuses) {
				clonePartitionerParams.addAllToApplicationStatusList(Arrays.asList(applicationStatuses.replaceAll("\\s+", "").toUpperCase().split(",")));
			}
			clonePartitionerParams.setIsCloneProgramEligibility(new AtomicBoolean("Y".equalsIgnoreCase(isCloneProgramEligibility)));
			clonePartitionerParams.setCloneToNFA(new AtomicBoolean("TRUE".equalsIgnoreCase(cloneToNFA)));

			if (isOTREnabled == null || isOTREnabled.trim().equals("")) {
				partitionerSsapApplicationIdList = cloneService.getEnrolledSsapApplicationsIdByCoverageYear(coverageYear, clonePartitionerParams.getApplicationStatusList(), batchSize, null);
			} else {
				partitionerSsapApplicationIdList = cloneService.getEnrolledSsapApplicationsIdByCoverageYear(coverageYear, clonePartitionerParams.getApplicationStatusList(), batchSize, "OTR");
			}

			if (CollectionUtils.isNotEmpty(partitionerSsapApplicationIdList)) {

				LOGGER.info("Number of Renewal Application-ID to generate Clone: {}", partitionerSsapApplicationIdList.size());
				partitionMap = new HashMap<String, ExecutionContext>();
				clonePartitionerParams.clearSsapApplicationIdList();
				clonePartitionerParams.addAllToSsapApplicationIdList(partitionerSsapApplicationIdList);

				int maxRenewalApplicationsCommitInterval = 1;
				int size = partitionerSsapApplicationIdList.size();

				if (StringUtils.isNumeric(this.cloneCommitInterval)) {
					maxRenewalApplicationsCommitInterval = Integer.valueOf(this.cloneCommitInterval.trim());
				}

				int numberOfRenewalApplicationIdToCommit = size / maxRenewalApplicationsCommitInterval;
				if (size % maxRenewalApplicationsCommitInterval != 0) {
					numberOfRenewalApplicationIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfRenewalApplicationIdToCommit; i++) {
					firstIndex = i * maxRenewalApplicationsCommitInterval;
					lastIndex = (i + 1) * maxRenewalApplicationsCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				errorMessage.append("SSAP Application data is not found to generate Clone.");
			}
		}
		catch (Exception ex) {
			errorMessage.append("ClonePartitioner failed to execute : ").append(ex.getMessage());
			LOGGER.error(errorMessage.toString(), ex);
		}
		finally {

			if (0 < errorMessage.length()) {
				cloneService.saveAndThrowsErrorLog(errorMessage.toString());
			}
		}
		return partitionMap;
	}

	private boolean validateParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == batchSize || 0l >= batchSize) {
			errorMessage.append("Invalid Batch Size: ");
			errorMessage.append(batchSize);
			hasValidParams = false;
		}

		if (StringUtils.isBlank(cloneToNFA) || (!"TRUE".equalsIgnoreCase(cloneToNFA)
				&& !"FALSE".equalsIgnoreCase(cloneToNFA))) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}

			if (StringUtils.isBlank(cloneToNFA)) {
				errorMessage.append("Clone to NFA is mandatory parameter");
			}
			else {
				errorMessage.append("Invalid Clone to NFA: ");
				errorMessage.append(cloneToNFA);
			}
			errorMessage.append(". It should be TRUE/FALSE.");
			hasValidParams = false;
		}

		if (null == renewalYear || 2000 > renewalYear || 2099 < renewalYear) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}
			errorMessage.append("Invalid Current Coverage Year: ");
			errorMessage.append(renewalYear);
			hasValidParams = false;
		}

		if (null == coverageYear || 2000 > coverageYear || 2099 < coverageYear) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}
			errorMessage.append("Invalid Previous Coverage Year: ");
			errorMessage.append(coverageYear);
			hasValidParams = false;
		}

		if (StringUtils.isNotBlank(isCloneProgramEligibility) && !"Y".equalsIgnoreCase(isCloneProgramEligibility)
				&& !"N".equalsIgnoreCase(isCloneProgramEligibility)) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}
			errorMessage.append("Invalid Is Clone Program Eligibility: ");
			errorMessage.append(isCloneProgramEligibility);
			errorMessage.append(". It should be Y/N.");
			hasValidParams = false;
		}

		boolean hasValidApplicationList = false;
		if (StringUtils.isNotBlank(applicationStatuses)) {

			List<String> applicationStatusList = new CopyOnWriteArrayList<String>(Arrays.asList(applicationStatuses.replaceAll("\\s+", "").toUpperCase().split(",")));

			if (CollectionUtils.isNotEmpty(applicationStatusList)) {
				hasValidApplicationList = applicationStatusList.stream()
						.allMatch(applicationStatus -> (null != ApplicationStatus.fromString(applicationStatus)));
			}
		}

		if (!hasValidApplicationList) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}

			if (StringUtils.isBlank(applicationStatuses)) {
				errorMessage.append("Application Status List is mandatory parameter");
			}
			else {
				errorMessage.append("Invalid Application Status List: ");
				errorMessage.append(applicationStatuses);
			}
			errorMessage.append(".");
			hasValidParams = false;
		}

		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(CloneService.AUTO_ENROLL_JOB_NAME);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Long batchSize) {
		this.batchSize = batchSize;
	}

	public String getCloneToNFA() {
		return cloneToNFA;
	}

	public void setCloneToNFA(String cloneToNFA) {
		this.cloneToNFA = cloneToNFA;
	}

	public String getApplicationStatuses() {
		return applicationStatuses;
	}

	public void setApplicationStatuses(String applicationStatuses) {
		this.applicationStatuses = applicationStatuses;
	}

	public String getIsCloneProgramEligibility() {
		return isCloneProgramEligibility;
	}

	public void setIsCloneProgramEligibility(String isCloneProgramEligibility) {
		this.isCloneProgramEligibility = isCloneProgramEligibility;
	}

	public String getCloneCommitInterval() {
		return cloneCommitInterval;
	}

	public void setCloneCommitInterval(String cloneCommitInterval) {
		this.cloneCommitInterval = cloneCommitInterval;
	}

	public CloneService getCloneService() {
		return cloneService;
	}

	public void setCloneService(CloneService cloneService) {
		this.cloneService = cloneService;
	}

	public ClonePartitionerParams getClonePartitionerParams() {
		return clonePartitionerParams;
	}

	public void setClonePartitionerParams(ClonePartitionerParams clonePartitionerParams) {
		this.clonePartitionerParams = clonePartitionerParams;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}
}
