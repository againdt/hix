<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Upload PDFs</title>
</head>
<body>
	<p align="center">Upload Brochure/SBC PDFs</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<table border="0" cellpadding="0" cellspacing="0" width="30%">
				<tr>
					<td>
						<form action="${pageContext.request.contextPath}/admin/serff/uploadBrochure" onsubmit="document.getElementById('submit').disabled=true;" method="GET">
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Upload Brochure / SBC PDFs"
									style="font-size: 18pt; color: #0066CC">
							</p>
						</form>
						<p align="center">&nbsp;
					</td>
				</tr>

				<c:if test="${uploadBrochureMessage != null}">
					<tr>
						<td>
							<p align="center">
								<c:out value="${uploadBrochureMessage}" />
								<c:if test="${uploadFileCount == 0}">
									Please upload Brochure/SBC PDFs manually at FTP server to start this process.
								</c:if>
							</p>
						</td>
					</tr>
				</c:if>
			</table>
		</center>
	</div>
</body>

</html>
