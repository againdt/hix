package com.getinsured.hix.planmgmt.service.jpa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.ZipCountyRatingArea;
import com.getinsured.hix.planmgmt.repository.AdminDocumentRepository;
import com.getinsured.hix.planmgmt.repository.ZipCountyRatingAreaRepository;
import com.getinsured.hix.planmgmt.service.ZipCountyRatingAreaService;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.timeshift.TimeShifterUtil;

@Service("zipCountyRatingAreaService")
@Repository
@Transactional
public class ZipCountyRatingAreaServiceImpl implements ZipCountyRatingAreaService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZipCountyRatingAreaServiceImpl.class);
	@Autowired private ZipCountyRatingAreaRepository zipCountyRatingAreaRepository;
	private static final String UNDERSCORE = "_";
	private static final String DOT = ".";
	@Autowired private AdminDocumentRepository adminDocumentRepository;
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	
	@Override
	public Integer getAllCountyAreaRatingCount() {
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
		String buildquery = "select id from pm_zip_county_rating_area";
			List<?> countyRatingAreasList = entityManager.createNativeQuery(buildquery).getResultList();
		if (countyRatingAreasList.size() > 0) {
			return countyRatingAreasList.size();
		}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getAllCountyAreaRatingCount", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
		return 0;	
	}

	@Override
	public boolean saveZipCountyRatingArea(ZipCountyRatingArea zipCountyRatingArea) {
		boolean ratingAreaResponse = true;
		try {
			zipCountyRatingAreaRepository.save(zipCountyRatingArea);
			return ratingAreaResponse;
		} catch (Exception e) {
			LOGGER.error("Exception occured in save : " ,e);
			ratingAreaResponse = false;
			return ratingAreaResponse;
		}
	}

	@Override
	public void removeAllContentsFromZipCounty() {
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
		String buildquery = "delete from pm_zip_county_rating_area";
			entityManager.getTransaction().begin();
			entityManager.createNativeQuery(buildquery).executeUpdate();
			entityManager.getTransaction().commit();
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in removeAllContentsFromZipCounty", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		LOGGER.info("All Removed Sucessfully");
	}

	@Override
	public String saveFile(String fileLocation, MultipartFile mfile, String suggestedFileName) {

		LOGGER.info("saveFile()");

		InputStream inputStream = null;
		OutputStream outputStream = null;
		String actualFileName = null;
		try {
			boolean isValidPath = planMgmtUtils.isValidPath(fileLocation);
			if(isValidPath){
				if(mfile.getSize() > 0){
					LOGGER.debug("Location of the file : " + SecurityUtil.sanitizeForLogging(String.valueOf(fileLocation)));
					File fileDir = new File(fileLocation);
					// create Directory if not exists.
					if (!fileDir.exists()) {
						fileDir.mkdir();
					}
					String fileName = null;
					// fileName would be a.txt then it will be come a_123456789.txt
					// i.e. a_TIMESTAMP.EXTENSION
					if (suggestedFileName == null) {
						actualFileName = FilenameUtils.getBaseName(mfile.getOriginalFilename()) + UNDERSCORE + TimeShifterUtil.currentTimeMillis() + DOT
								+ FilenameUtils.getExtension(mfile.getOriginalFilename());
					} else {
						actualFileName = suggestedFileName;
					}
					fileName = fileLocation + GhixConstants.FRONT_SLASH + actualFileName;

					inputStream = mfile.getInputStream();
					outputStream = new FileOutputStream(fileName);
					int readBytes = 0;
					byte[] buffer = new byte[10000];
					while ((readBytes = inputStream.read(buffer, 0, 10000)) != -1) {
						outputStream.write(buffer, 0, readBytes);
					}
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			LOGGER.error("Error while saving the file. Error Descirption : "
					+ e.getMessage());
			actualFileName = null;
		}finally{
			IOUtils.closeQuietly(outputStream);
			IOUtils.closeQuietly(inputStream);
		}
		return actualFileName;
	}

	@Override
	public List<AdminDocument> getAllAdminDocument(String documentType) {
		List<AdminDocument> documentsList = adminDocumentRepository.findAll();
		List<AdminDocument> returnedList = new ArrayList<AdminDocument>();
		for (AdminDocument adminDocument : documentsList) {
			if (adminDocument.getDocumentType().equalsIgnoreCase(documentType)) {
				returnedList.add(adminDocument);
			}
		}
		LOGGER.debug("returnedList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnedList.size())));

		Collections.sort(returnedList, new Comparator<AdminDocument>() {
			public int compare(AdminDocument ad1, AdminDocument ad2) {
				return ad2.getLastUpdateTimestamp().compareTo(ad1.getLastUpdateTimestamp());
			}
		});
		return returnedList;
	}

	@Override
	public boolean saveDocument(AdminDocument adminDocument) {
		adminDocumentRepository.save(adminDocument);
		return true;
	}

}
