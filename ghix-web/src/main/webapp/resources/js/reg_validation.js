function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

function populateDate(zipCode){
	$.ajax({
	  url: "signup",
	  type: "GET",
	  data: {op : 'GETZIPDATA', zipCode : zipCode},
	  success: function(data){
		  var arr = data.split(" # ");
		  $('#city').val(arr[1]);
		  $('#state').val(arr[0]);
	  }
	});
}

jQuery.validator.addMethod("passwordcheck", function(value, element, param) {
	password = $("#password").val(); retype_password = $("#retype_password").val(); 
  	if(password !=  retype_password ){ 
  	  	return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	ssn1 = $("#ssn1").val(); ssn2 = $("#ssn2").val(); ssn3 = $("#ssn3").val();
  	if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3))  ){ 
  	  	return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	phone1 = $("#phone1").val(); phone2 = $("#phone2").val(); phone3 = $("#phone3").val();
  	if( (phone1 == "" || phone2 == "" || phone3 == "")  || (isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3))  ){ 
  	  	return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
  	dob_mm = $("#dob_mm").val(); dob_dd = $("#dob_dd").val(); dob_yy = $("#dob_yy").val(); 
  
  	if( (dob_mm == "" || dob_dd == "" || dob_yy == "")  || (isNaN(dob_mm)) || (isNaN(dob_dd)) || (isNaN(dob_yy))  ){ return false; }
  		
	var today=new Date()
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

var validator = $("#frmsignup").validate({ 
	rules: { email: { required: true, email: true },password:{ required: true },
			 retype_password: { required: true, passwordcheck : true }, firstname:{ required: true },
			 lastname:{ required: true }, dob_yy:{ dobcheck: true },
			 ssn3:{ ssncheck: true  }, address: { required: true },
			 phone3:{ phonecheck: true }, zip:{ required: true },
			 city:{ required: true }, state:{ required: true }
	       },
    messages: { 
           email: { 
               required: "<span> <em class='excl'>!</em> Enter your email address.</span>", 
               email: "<span> <em class='excl'>!</em> Please enter a valid email address.</span>" 
           },
           password: "<span> <em class='excl'>!</em> Enter value for password.</span>" ,
           retype_password: { 
        	   required: "<span> <em class='excl'>!</em> Enter value for password.</span>" ,
        	   passwordcheck: "<span> <em class='excl'>!</em> Passwords do not match.</span>" 
           },
           firstname: "<span> <em class='excl'>!</em> Enter your first name.</span>" ,
           lastname: "<span> <em class='excl'>!</em> Enter your last name.</span>" ,
           dob_yy: "<span> <em class='excl'>!</em> Enter valid date of birth.</span>" ,
           ssn3: "<span> <em class='excl'>!</em> Enter valid SSN number.</span>" ,
           address: "<span> <em class='excl'>!</em> Enter your address.</span>" ,
           phone3: "<span> <em class='excl'>!</em> Enter valid phone number.</span>" ,
           zip: "<span> <em class='excl'>!</em> Enter your zip code.</span>" ,
           city: "<span> <em class='excl'>!</em> Enter your city.</span>" ,
           state: "<span> <em class='excl'>!</em> Enter your state.</span>"
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
	}
});

$(function() {
	$('#zip').mask('99999', {placeholder:""});
	$('#phone1').mask('999', {placeholder:""});
	$('#phone2').mask('999',{placeholder:""});
	$('#phone3').mask('9999',{placeholder:""});
	$('#ssn1').mask('999',{placeholder:""});
	$('#ssn2').mask('99',{placeholder:""});
	$('#ssn3').mask('9999',{placeholder:""});
	$('#dob_mm').mask('99',{placeholder:""});
	$('#dob_dd').mask('99',{placeholder:""});
	$('#dob_yy').mask('9999',{placeholder:""});
});