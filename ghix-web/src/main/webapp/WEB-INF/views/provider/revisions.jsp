<!--  Revisions Page -->

V.  Revisions Page (8): Should be titled "Preferred Provider Details Revision"
a.  {Programmed} Select the following information that is incorrect with {Provider Name}'s information.
b.  Check all that apply:
c.  (<checkbox>) Address
d.  (<checkbox>) Phone
e.  (<checkbox>) Gender
f.  (<checkbox>) Specialty
g.  (<checkbox>) No Longer Affiliated with Medical/Dental Group
h.  (<checkbox>) No Longer Accepting New Patients
i.  (<checkbox>) No Longer Participating with my Plan
j.  (<checkbox>) Retired - No Longer in Practice
k.  (<checkbox>) Deceased
l.  (<checkbox>) Other (<textarea>)
m.  (<input button>) <<click to submit to Details Page (IV) and pop up confirmation>> Submit

n.  (<link>) <<click to cancel>> Close Modal