package com.getinsured.eligibility.ssap.integration.ridp.transformer;

import java.io.StringReader;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.prescreen.calculator.FplCalculator;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanResponse;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.jayway.jsonpath.JsonPath;

@Component(value = "aPTCCalcultionActivator")
public class APTCCalcultionActivator {

	@Autowired
	FplCalculator fplCalculator;
	private static Logger LOGGER = Logger.getLogger(APTCCalcultionActivator.class);
	
	@Autowired	private SsapApplicationRepository ssapApplicationRepository;

	@ServiceActivator
	public String createAPTCRequest(Message<String> message) throws GIException {

		LOGGER.debug(" BenchmarkResponse " + message.getPayload());
		String ssapJSON = (String) message.getHeaders().get("SSAP_JSON");
		String ssapApplicationId = "";
		String requestAPTCJson = "";
		
		ssapJSON = ssapJSON.replace("\n", "");
		String houseHoldIncome = JsonPath.read(ssapJSON,"singleStreamlinedApplication.taxHousehold[0].houseHoldIncome").toString();
		try {
			Pattern p = Pattern.compile("(.*)(\\\"ssapApplicationId\\\":\\s*\\\")(\\d*)(.*)$");
			Matcher m = p.matcher(ssapJSON);
			if (m.matches()) {
				ssapApplicationId = m.group(3);
			}
			
			//SsapApplication ssapAp = ssapApplicationRepository.findOne(Long.valueOf(ssapApplicationId));
			SsapApplication ssapAp = ssapApplicationRepository.findOne(new Long(ssapApplicationId));
			Long coverageYear = ssapAp.getCoverageYear();
			
			String state = JsonPath.read(ssapJSON,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[0].householdContact.homeAddress.state").toString();
			List<JSONObject> family = JsonPath.read(ssapJSON,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
			int familySize = family.size();
			LOGGER.debug(" ssapApplicationId :" + ssapApplicationId+ " houseHoldIncome :" + houseHoldIncome);
			requestAPTCJson = "{\"applicationId\":"
					+ ssapApplicationId
					+ ",\"clientIp\": \""
					+ InetAddress.getLocalHost().getHostName()
					+ "\","
					+ "\"operation\": \"\",\"payload\":"
					+ "{\"Income\": {"
					+ "\"incomeAmount\": \""
					+ houseHoldIncome
					+ "\",\"incomeDate\": \""
					+ coverageYear
					+ "\","
					+ " \"incomeFederalPovertyLevelPercent\": \""
					+ getFPLPercent(new Double(houseHoldIncome), familySize,
							state, coverageYear) + "\"" + "},\"SLCSPPremium\": {"
					+ "  \"insurancePremiumAmount\": \""
					+ getBenchMarkPremium(message.getPayload()).toString()
					+ "\"" + "},\"requestID\": \""+ssapApplicationId+"\"}}";
			LOGGER.debug(" /invokeAPC JSON Request" + requestAPTCJson);
		} catch (Exception e) {
			LOGGER.error(" exception while creating APTC HUB calculation Request ",e);
			throw new GIException(e);
		}
		ssapJSON = null;
		return requestAPTCJson;
	}

	public double getFPLPercent(double familyIncome, int familySize,
			String state, Long coverageYear) {

		FplRequest fplRequest = new FplRequest();
		fplRequest.setFamilyIncome(familyIncome);
		fplRequest.setFamilySize(familySize);
		fplRequest.setState(state);
		fplRequest.setCoverageYear(coverageYear);
		FplResponse fplResponse = fplCalculator.calculateFpl(fplRequest,false,0);
		DecimalFormat f = new DecimalFormat("##.00");
		return Double.parseDouble(f.format(fplResponse.getFplPercentage()));
	}

	public Float getBenchMarkPremium(String benchmarkServiceResponse)
			throws GIException {

		GetBenchmarkPlanResponse response = null;

		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(GetBenchmarkPlanResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(benchmarkServiceResponse);
			response = (GetBenchmarkPlanResponse) unmarshaller
					.unmarshal(reader);

		} catch (JAXBException e) {
			LOGGER.error("error while parsing response "
					+ benchmarkServiceResponse, e);
			throw new GIException(e);
		}
		return response.getBenchmarkPremium();
	}

	public String changeDateFormatString(String inputDateStr) {
		final String OLD_FORMAT = "MMM dd, yyyy hh:ww:ss";
		final String NEW_FORMAT = "MM/dd/yyyy";

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(inputDateStr);
		} catch (Exception e) {
			LOGGER.error("error while changing date format ", e);
		}
		sdf.applyPattern(NEW_FORMAT);
		return sdf.format(d);
	}
}