package com.getinsured.ssap.model.financial;

/**
 * IFSV verification response types.
 * @author Yevgen Golubenko
 * @since 7/16/19
 */
public enum IfsvVerification {
  /**
   * IFSV verified that MAGI household income is within the threshold.
   */
  VERIFIED,

  /**
   * IFSV service returned that MAGI household income is not within the threshold.
   */
  NOT_VERIFIED,

  /**
   * IFSV service responded with an error (502, 500, etc.)
   */
  ERROR,

  /**
   * Unknown IFSV verification status.
   */
  UNKNOWN;

  /**
   * Returns enum from given string value (case-insensitive).
   * @param value string value.
   * @return {@link IfsvVerification} enum.
   */
  public IfsvVerification fromValue(final String value) {
    switch (value.toLowerCase()) {
      case "verified":
        return IfsvVerification.VERIFIED;
      case "not_verified":
        return IfsvVerification.NOT_VERIFIED;
      case "error":
        return IfsvVerification.ERROR;
      default:
        return IfsvVerification.UNKNOWN;
    }
  }
}
