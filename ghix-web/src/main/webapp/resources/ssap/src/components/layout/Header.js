import React, { Fragment } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// ActionCreators

import {
    fetchHeaderFooterData,
    showModal
} from '../../actions/actionCreators';

// Constants

import CONFIG from '../../.env';
import MODAL_TYPES from '../../constants/modalTypes';

const Header = props => {
    const { anonymous, impersonation, impersonationDetails, stateCode, exchangeName } = props.headerLinks;
    //const {activeModuleName} = props.headerLinks.user
    const {
        SETTINGS: accountSettings,
        ASSISTANCE: assistance,
        DASHBOARD: dashboard,
        HELP: help,
        // HOME: home,
        LOGIN: login,
        LOGOUT: logout,
        PHONE: phone,
        UNREAD_MESSAGES: unReadMsg,
    } = props.headerLinks.links;

    const { LOGO: logo } = props.headerLinks.assets;
    // const {
    //     GOOGLE_ANALYTICS_CODE: trackingCode,
    //     GOOGLE_ANALYTICS_CONTAINER_ID: gtmContainerID
    // } = props.headerLinks.properties;

    const logoForDevelopment = '../../../assets/images/logo_nv.png';

    const handleOpeningModal = () => {
        props.actions.showModal(MODAL_TYPES.LOCATE_ASSISTANCE_MODAL);
    };

    return(
        <Fragment>
            <a className="usa-skipnav" href="#main-content">Skip to main content</a>
            <header className="usa-header usa-header-basic" id="masthead" role="banner">
                <div className="usa-nav-container masthead__container">
                    <div className="usa-navbar">
                        <div className="usa-logo logo" id="masthead-logo-wrap">
                            {
                                stateCode === 'CA' ?
                                    < img id="masthead-logo-img_01" className="masthead__logo__img" src={logo} alt={exchangeName} width="150"/>
                                    :<a href="#" className="masthead__logo__img">
                                        < img id="masthead-logo-img_02" className="masthead__logo__img" src={CONFIG.ENV.DEVELOPMENT ? logoForDevelopment : logo} alt={exchangeName} width="150"/>
                                    </a>
                            }
                        </div>
                        <div className="usa-menu-btn masthead__nav__btn--collapse">
                            {/*<FontAwesomeIcon icon="bars" />*/} Menu
                        </div>
                    </div>

                    <nav role="navigation" className="usa-nav main__nav">
                        <button className="usa-nav-close">
                            <img src="../../../assets/images/close.svg" alt="close" />
                        </button>
                        {
                            anonymous ?
                                <ul id="navbar-links-container" className="usa-nav-primary usa-accordion main__nav__links_container">
                                    <li><a id="authentication_01" href={login}>Login</a></li>
                                    <li>
                                        <button id="get-assistance_01" className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--01" aria-controls="basic-nav-section-one" aria-expanded="false">
                                            <span>Get Assistance</span>
                                        </button>
                                        <ul id="basic-nav-section-one" className="usa-nav-submenu main__nav__submenu" aria-hidden="true">
                                            <li>
                                                <a href={assistance} id="find-assistance_01" onClick={handleOpeningModal}>Find Local Assistance</a>
                                            </li>
                                            {
                                                stateCode === 'ID' &&
                                                <li>
                                                    <a href={help} id="help_01">Help</a>
                                                </li>
                                            }
                                            <li>
                                                <a href={`tel:${phone}`} id="phone_01">Consumers  <FontAwesomeIcon icon="phone" flip="horizontal"/> {phone}</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                : <ul className="usa-nav-primary usa-accordion main__nav__links_container">
                                    <li><a id="home" className="usa-nav-link" href={dashboard}>
                                        <FontAwesomeIcon icon="home" />
                                    </a></li>
                                    <li><a id="secured-mail_01" className="usa-nav-link secured-mail" href={unReadMsg}>
                                        <FontAwesomeIcon className="lock" icon="lock" /><FontAwesomeIcon className="envelope" icon="envelope" />
                                    </a></li>
                                    <li>
                                        <button id="get-assistance_02" className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--01" aria-expanded="false" aria-controls="basic-nav-section-one">
                                            <span>Get Assistance</span>
                                        </button>
                                        <ul id="basic-nav-section-one" className="usa-nav-submenu main__nav__submenu" aria-hidden="true">
                                            <li>
                                                <a href={assistance} id="find-assistance_02" onClick={handleOpeningModal}>Find Local Assistance</a>
                                            </li>
                                            {stateCode === 'ID' &&
                                                <li>
                                                    <a href={help} id="help_02">Help</a>
                                                </li>
                                            }
                                            <li>
                                                <a href={`tel:${phone}`} id="phone_02">Consumers  <FontAwesomeIcon icon="phone"  flip="horizontal"/> {phone}</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <button id="my-account" className="usa-accordion-button usa-nav-link dropdown__pos dropdown__pos--02" aria-expanded="false" aria-controls="basic-nav-section-two">
                                            <span>My Account</span>
                                        </button>
                                        <ul id="basic-nav-section-two" className="usa-nav-submenu  main__nav__submenu" aria-hidden="true">
                                            <li>
                                                <a href={accountSettings} id="account-settings_01">Account Settings</a>
                                            </li>
                                            <li>
                                                <a href={dashboard} id="dashboard_01">Dashboard</a>
                                            </li>
                                            <li>
                                                <a href={logout} id="logout_01">Logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                        }
                    </nav>
                </div>
            </header>
            {impersonation &&
                <div id="impersonationSubHeader" className="usa-width-one-whole header">
                    <div>
                        <h4>
                            Viewing Individual Account ({impersonationDetails.name})
                            <span>
                                <a href={impersonationDetails.switchBackLink} className="ds-c-button usa-button btn-primary">My Account</a>
                            </span>
                        </h4>
                    </div>
                </div>
            }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    headerLinks: state.headerLinks
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        fetchHeaderFooterData,
        showModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
