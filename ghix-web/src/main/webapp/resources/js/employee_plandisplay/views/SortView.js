(function (views) {
	views.SortView = Backbone.View.extend({
		
		events : {
			'click a.sort_monthlyPayrollDeduction': 'sort',
			'click a.sort_totalEstimatedCost': 'sort',
			'click a.sort_planScore': 'sort',
			'click a.sort_oopMax': 'sort'
		},
		
		el : '#sort',
		initialize : function () {
			_.bindAll (this, 'render');
			var self = this;

			self.tmpl = _.template($('#tmpSort').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self;
            self = this;

			var html = this.tmpl(self.collection.info());
			$(this.el).html(html);
		},
		
		sort: function (e) {
			e.preventDefault();
			var currentSort = e.target.id;
			if(currentSort != this.collection.lastSortColumn){
				this.collection.sortDirection = "";
			}
			if(this.collection.sortDirection == 'asc'){
				this.collection.setSort(currentSort, 'desc');
			}else if(this.collection.sortDirection == 'desc'){
				this.collection.setSort(currentSort, 'asc');
			}else if(currentSort == 'planScore' && this.collection.sortDirection == ""){
				this.collection.setSort(currentSort, 'desc');
			}else this.collection.setSort(currentSort, 'asc');
			
			this.collection.preserveOtherOptions();
		},
		
	});
})(App.views);