package com.getinsured.ssap.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;
import com.getinsured.ssap.model.hub.vlp.*;
import com.getinsured.ssap.model.hub.vlp.documents.*;
import com.getinsured.ssap.model.hub.vlp.response.IndividualDetails;
import com.getinsured.ssap.model.hub.vlp.response.ResponseMetadata;
import com.getinsured.ssap.model.hub.vlp.response.VlpResponse;
import com.getinsured.ssap.repository.ApplicantRepository;
import com.getinsured.ssap.repository.VlpIntegrationLogRepository;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;

/**
 * Hub integration service implementation.
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
@Service("hubIntegration")
public class HubIntegrationImpl implements HubIntegration
{
  private static final Logger log = LoggerFactory.getLogger(HubIntegrationImpl.class);

  private final VlpIntegration vlpIntegration;
  private final SsapApplicationRepository ssapApplicationRepository;
  private final VlpIntegrationLogRepository vlpIntegrationLogRepository;
  private final ApplicantRepository applicantRepository;
  private final SsapNoticeService ssapNoticeService;

  @Value("#{configProp['fdh.vlp.casepoc.fullname'] != null ? configProp['fdh.vlp.casepoc.fullname'] : 'DHS Tester'}")
  private String casePocFullName;

  @Value("#{configProp['fdh.vlp.casepoc.phonenumber'] != null ? configProp['fdh.vlp.casepoc.phonenumber'] : '5555555555'}")
  private String casePocPhoneNumber;

  @Value("#{configProp['fdh.cms_partner'] != null ? configProp['fdh.cms_partner'] : 'SSHIX'}")
  private String cmsPartner;

  @Autowired
  public HubIntegrationImpl(final VlpIntegration vlpIntegration, final SsapApplicationRepository ssapApplicationRepository,
                            final VlpIntegrationLogRepository vlpIntegrationLogRepository, final ApplicantRepository applicantRepository,
                            final SsapNoticeService ssapNoticeService)
  {
    this.vlpIntegration = vlpIntegration;
    this.ssapApplicationRepository = ssapApplicationRepository;
    this.vlpIntegrationLogRepository = vlpIntegrationLogRepository;
    this.applicantRepository = applicantRepository;
    this.ssapNoticeService = ssapNoticeService;

    if(casePocFullName == null || "".equals(casePocFullName)) {
      casePocFullName = "DHS Tester";
    }

    if(casePocPhoneNumber == null || "".equals(casePocPhoneNumber)) {
      casePocPhoneNumber = "5555555555";
    }

    if(cmsPartner == null || "".equals(cmsPartner)) {
      cmsPartner = "SSHIX";
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void process(final long applicationId) {
    process(applicationId, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void process(final long applicationId, final Set<Long> applicantIds)
  {
    if(applicantIds != null) {
      log.info("Processing SsapApplication id: {} for {} applicants", applicationId, applicantIds.size());
    } else {
      log.info("Processing SsapApplication id: {} for all applicants", applicationId);
    }

    final SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);

    if(ssapApplication == null) {
      log.error("Unable to process SSAP with given id: {}, application not found", applicationId);
      return;
    }

    final SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);

    if(application == null) {
      log.error("Unable to make Hub integration calls: application is null for given application id: {}", applicationId);
      return;
    }
    else if(application.getTaxHousehold() == null) {
      log.error("Unable to make Hub integration calls: tax household for given applicationId: {} is null", applicationId);
      return;
    }

    // TODO: Handle multiple tax households when the time comes.
    // TODO: Here we will filter households and figure out if they belong to primary tax household or not, if not, we will only conditionally send some verifications
    final List<HouseholdMember> householdMemberList = HouseholdUtil.filterHouseholdMembersForVerifications(application);
    List<SsapApplicant> applicantList = applicantRepository.findBySsapApplicationId(applicationId);

    if(applicantIds != null && applicantIds.size() > 0) {
      log.info("Only processing {} applicants out of {} total",  applicantIds.size(), applicantList.size());
      applicantList = applicantList.stream().filter(applicant -> applicantIds.contains(applicant.getId())).collect(Collectors.toList());
    }

    final Map<Long, VlpRequest> vlpRequestMap = new HashMap<>(householdMemberList.size());

    List<SsapApplicant> finalApplicantList = applicantList;
    householdMemberList.forEach(householdMember -> {
      // If user selected that they are citizen, don't run VLP integration.
      // If user is medicaid/chip eligible (and not seeking QHP), don't run VLP, verify them automatically HIX-118046
      final HouseholdMemberResponse memberEligibility = householdMember.getEligibilityResponse();
      boolean autoVerification = false;

      if(memberEligibility != null) {
        autoVerification = (memberEligibility.isMedicaid() || memberEligibility.isChip()) && !householdMember.isSeeksQhp();
      }

      if(
        autoVerification ||
        (householdMember.getCitizenshipImmigrationStatus() != null &&
          householdMember.getCitizenshipImmigrationStatus().getCitizenshipAsAttestedIndicator() == Boolean.TRUE) ||
          (householdMember.getCitizenshipImmigrationStatus() != null ?
            householdMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() : null) == Boolean.TRUE)
      {
        log.info("User is citizen, not doing VLP call: applicant guid: {}", householdMember.getApplicantGuid());

        Optional<SsapApplicant> applicantOptional = finalApplicantList.stream().filter(a ->
          a.getApplicantGuid().equals(householdMember.getApplicantGuid())).findFirst();

        if(applicantOptional.isPresent()) {
          SsapApplicant applicant = applicantOptional.get();
          applicant.setVlpVerificationStatus(ApplicantVerificationStatus.VERIFIED.getValue());
          applicant.setVlpVerificationVid(new BigDecimal(0));
          applicantRepository.save(applicant);
          log.info("Saved applicant with VLP self-attestation/for Citizen applicant id: {}", applicant.getId());
        }
      }
      else {
        final DhsDocument dhsDocument = fillDocument(householdMember);
        Optional<SsapApplicant> applicantOptional = finalApplicantList.stream().filter(a ->
          a.getApplicantGuid().equals(householdMember.getApplicantGuid())).findFirst();

        if(applicantOptional.isPresent()) {
          if (dhsDocument != null) {
            final VlpPayload vlpPayload = createPayload(householdMember, dhsDocument);

            final VlpRequest vlpRequest = new VlpRequest();
            vlpRequest.setApplicationId(applicationId);
            vlpRequest.setPayload(vlpPayload);
            vlpRequest.setDocumentType(dhsDocument.getDocumentType());
            vlpRequest.setClientIp(application.getClientIp());
            vlpRequest.setCmsPartnerId(cmsPartner);

            vlpRequestMap.put(applicantOptional.get().getId(), vlpRequest);
          } else {
            log.error("Unable to create VLP Request for applicant guid: {}, applicant not found", householdMember.getApplicantGuid());
          }
        }
      }
    });

    final List<VlpIntegrationLog> integrationList = vlpIntegrationLogRepository.findBySsapApplicationId(applicationId);
    final Map<Long, ApplicantVerificationStatus> applicantVerificationStatusMap = new HashMap<>();

    vlpRequestMap.forEach((applicantId, vlpRequest) -> {
      log.info("Processing VLP Payload for applicant id: {} payload: {}", applicantId, vlpRequest.toString());

      Optional<VlpIntegrationLog> existingLog = integrationList.stream().filter(il -> il.getSsapApplicantId() == applicantId).findFirst();
      VlpIntegrationLog vlpIntegrationLog = existingLog.orElseGet(VlpIntegrationLog::new);

      VlpResponse vlpResponse = null;

      // If it's new request, integration log will not be present and id will be 0.
      if(vlpIntegrationLog.getId() == 0L) {
        // If there was no VLP for this request, we always start with initial verification first.
        vlpResponse = vlpIntegration.verify(vlpRequest);
        vlpIntegrationLog.setAction(VlpAction.VERIFY);
      } else {
        // If we have case number already from previous requests, add that one to the document payload
        if (vlpIntegrationLog.getCaseNumber() != null) {
          vlpRequest.getPayload().getDhsDocument().setCaseNumber(vlpIntegrationLog.getCaseNumber());
        }

        switch(vlpIntegrationLog.getActionResult()) {
          case RESUBMIT_WITH_SEVISID:
            vlpResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);
            vlpIntegrationLog.setAction(VlpAction.RESUBMIT_WITH_SEVISID);
            break;
          case CHECK_CASE_RESOLUTION:
            vlpResponse = vlpIntegration.getStepTwoCaseResolution(vlpRequest);
            vlpIntegrationLog.setAction(VlpAction.CHECK_CASE_RESOLUTION);
            break;
          case REVERIFY:
            vlpResponse = vlpIntegration.reverify(vlpRequest);
            vlpIntegrationLog.setAction(VlpAction.REVERIFY);
            break;
          case RETRY:
          default:
            // In case of retry, we need to look at the action instead.
            switch(vlpIntegrationLog.getAction()) {
              case RESUBMIT_WITH_SEVISID:
                vlpResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);
                vlpIntegrationLog.setAction(VlpAction.RESUBMIT_WITH_SEVISID);
                break;
              case CHECK_CASE_RESOLUTION:
                vlpResponse = vlpIntegration.getStepTwoCaseResolution(vlpRequest);
                vlpIntegrationLog.setAction(VlpAction.CHECK_CASE_RESOLUTION);
                break;
              case REVERIFY:
                vlpResponse = vlpIntegration.reverify(vlpRequest);
                vlpIntegrationLog.setAction(VlpAction.REVERIFY);
                break;
              case VERIFY:
              default:
                vlpResponse = vlpIntegration.verify(vlpRequest);
                vlpIntegrationLog.setAction(VlpAction.VERIFY);
                break;
            }
            break;
        }
      }

      vlpIntegrationLog.setRemoteUrl(vlpResponse.getRemoteUrl());
      vlpIntegrationLog.setSsapApplicationId(applicationId);
      vlpIntegrationLog.setSsapApplicantId(applicantId);

      applicantVerificationStatusMap.putAll(processVlpResponse(vlpIntegrationLog, vlpRequest, vlpResponse));
    });

    final boolean needToSendAdditionalInformationRequiredNotice = applicantVerificationStatusMap
      .values()
      .stream()
      .anyMatch(verificationStatus -> verificationStatus == ApplicantVerificationStatus.MORE_INFO_NEEDED);

    if (needToSendAdditionalInformationRequiredNotice) {
        final String caseNumber = ssapApplication.getCaseNumber();
        log.info("Sending notification to primary applicant that VLP requires more info for one of the applicants, case number: {}", caseNumber);
        ssapNoticeService.sendAdditionalInformationNeeded(caseNumber);
    }

    log.info("Processing for SsapApplication id: {} has been finished.", applicationId);
  }

  /**
   * {@inheritDoc}
   * @return
   */
  @Override
  public Map<Long, ApplicantVerificationStatus> checkStepTwoCaseResolution(final VlpIntegrationLog integrationLog) {
    log.info("Checking case resolution for ssap id: {}, applicant id: {}, case number: {}",
      integrationLog.getSsapApplicationId(),
      integrationLog.getSsapApplicantId(),
      integrationLog.getCaseNumber()
    );

    VlpRequest vlpRequest = integrationLog.getVlpRequest();
    vlpRequest.getPayload().getDhsDocument().setCaseNumber(integrationLog.getCaseNumber());

    vlpRequest.setCasePOCFullName(vlpRequest.getPayload().getCasePOCFullName());
    vlpRequest.setCasePOCPhoneNumber(vlpRequest.getPayload().getCasePOCPhoneNumber());

    VlpResponse vlpResponse = vlpIntegration.getStepTwoCaseResolution(vlpRequest);
    integrationLog.setAction(VlpAction.CHECK_CASE_RESOLUTION);

    return processVlpResponse(integrationLog, vlpRequest, vlpResponse);
  }

  @Override
  public Map<Long, ApplicantVerificationStatus> resubmitWithSevisId(final Long integrationLogId, final CitizenshipDocument citizenshipDocument) {
    VlpIntegrationLog integrationLog = vlpIntegrationLogRepository.findOne(integrationLogId);

    if(integrationLog == null) {
      log.error("Unable to find Vlp Integration Log for given id: {}", integrationLogId);
      return null;
    }

    log.info("Resubmitting with SEVIS id: {} ssap id: {}, applicant id: {}, case number: {}",
      citizenshipDocument.getSevisId(),
      integrationLog.getSsapApplicationId(),
      integrationLog.getSsapApplicantId(),
      integrationLog.getCaseNumber()
    );

    VlpRequest vlpRequest = integrationLog.getVlpRequest();

    DhsDocument dhsDocument = getDhsDocumentFromCitizenshipDocument(citizenshipDocument, vlpRequest.getDocumentType());
    vlpRequest.getPayload().setDhsDocument(dhsDocument);

    vlpRequest.setCasePOCFullName(vlpRequest.getPayload().getCasePOCFullName());
    vlpRequest.setCasePOCPhoneNumber(vlpRequest.getPayload().getCasePOCPhoneNumber());

    VlpResponse vlpResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);
    integrationLog.setAction(VlpAction.RESUBMIT_WITH_SEVISID);

    return processVlpResponse(integrationLog, vlpRequest, vlpResponse);
  }

  private Map<Long, ApplicantVerificationStatus> processVlpResponse(VlpIntegrationLog integrationLog, VlpRequest vlpRequest, VlpResponse vlpResponse) {
    integrationLog.setRemoteUrl(vlpResponse.getRemoteUrl());
    integrationLog.setActionResult(vlpResponse.getActionResult());
    integrationLog.setVlpRequest(vlpRequest);
    integrationLog.setVlpResponse(vlpResponse);

    if(vlpResponse.getIndividualDetails() != null && !vlpResponse.getIndividualDetails().isEmpty())
    {
      IndividualDetails individualDetails = vlpResponse.getIndividualDetails().get(0);

      if(individualDetails.getCaseNumber() != null) {
        integrationLog.setCaseNumber(individualDetails.getCaseNumber());
      }

      integrationLog.setEligibilityStatementCode(individualDetails.getEligibilityStatementCode());

      if(individualDetails.getResponseMetadata() != null)
      {
        ResponseMetadata responseMetadata = individualDetails.getResponseMetadata();
        integrationLog.setResponseCode(responseMetadata.getResponseCode());
        integrationLog.setResponseMessage(responseMetadata.getResponseDescriptionText() + "; " + responseMetadata.getTdsResponseDescriptionText());
      }
    }
    else {
      integrationLog.setResponseCode(vlpResponse.getResponseCode());
    }

    // Set case number into original VLP request.
    if(integrationLog.getCaseNumber() != null) {
      VlpPayload vlpPayload = vlpRequest.getPayload();
      if(vlpPayload != null) {
        DhsDocument doc = vlpPayload.getDhsDocument();
        if(doc != null) {
          doc.setCaseNumber(integrationLog.getCaseNumber());
        }
        vlpPayload.setDhsDocument(doc);
        vlpRequest.setPayload(vlpPayload);
        integrationLog.setVlpRequest(vlpRequest);
      }
    }

    switch(vlpResponse.getResponseCode()) {
      case SUCCESS:
        integrationLog.setRetryCount(0);
        break;
      case COMMUNICATION_ERROR: // When we are unable to reach the server
      case NONE:                // When we reached server, but no valid response known to us is returned
        // For CHECK_CASE_RESOLUTION we retry indefinitely? At least for now.
        if(integrationLog.getAction() == VlpAction.CHECK_CASE_RESOLUTION ||
          integrationLog.getActionResult() == VlpActionResult.CHECK_CASE_RESOLUTION) {
          integrationLog.setRetryCount(0);
        } else {
          integrationLog.setRetryCount(integrationLog.getRetryCount() + 1);
        }
        break;
      case CASE_RESULT_NOT_AVAILABLE:
        integrationLog.setRetryCount(0);
        // If return status is case results not available, reset action result for scheduler.
        integrationLog.setActionResult(VlpActionResult.CHECK_CASE_RESOLUTION);
        //integrationLog.setAction(VlpAction.REVERIFY);
        break;
    }

    if(integrationLog.getRetryCount() == VlpIntegrationLogScheduler.MAX_RETRY_COUNT) {
      integrationLog.setActionResult(VlpActionResult.VALIDATION_FAILED);

      log.info("Reached max retry count of: {}, marking user[{}] vlp: {}",
        VlpIntegrationLogScheduler.MAX_RETRY_COUNT,
        integrationLog.getSsapApplicantId(),
        integrationLog.getActionResult());
    }

    final VlpIntegrationLog updatedIntegrationLog = vlpIntegrationLogRepository.save(integrationLog);
    log.info("Saved VLP Integration log for application id: {},  applicant id: {}", integrationLog.getSsapApplicationId(), integrationLog.getSsapApplicantId());

    final List<SsapApplicant> applicantList = applicantRepository.findBySsapApplicationId(integrationLog.getSsapApplicationId());

    return updateApplicantVerificationStatus(applicantList, updatedIntegrationLog);
  }

  private Map<Long, ApplicantVerificationStatus> updateApplicantVerificationStatus(List<SsapApplicant> applicantList, VlpIntegrationLog... integrationLogs) {
    List<SsapApplicant> updatedApplicantVerificationList = new ArrayList<>();
    final Map<Long, ApplicantVerificationStatus> verificationStatuses = new HashMap<>();

    Stream.of(integrationLogs).forEach(vlpIntegrationLog -> {
      SsapApplicant applicant = applicantList.stream().filter(ssapApplicant -> ssapApplicant.getId() == vlpIntegrationLog.getSsapApplicantId()).findFirst().orElse(null);

      if(applicant != null) {
        applicant.setVlpVerificationVid(new BigDecimal(vlpIntegrationLog.getId()));

        switch(vlpIntegrationLog.getActionResult()) {
          case VALIDATION_FAILED:
            applicant.setVlpVerificationStatus(ApplicantVerificationStatus.NOT_VERIFIED.getValue());
            break;
          case RETRY:
          case VERIFY:
          case CLOSE_CASE:
          case CHECK_CASE_RESOLUTION:
            applicant.setVlpVerificationStatus(ApplicantVerificationStatus.PENDING.getValue());
            break;
          case REVERIFY:
          case RESUBMIT_WITH_SEVISID:
            applicant.setVlpVerificationStatus(ApplicantVerificationStatus.MORE_INFO_NEEDED.getValue());
            break;
          default:
            applicant.setVlpVerificationStatus(ApplicantVerificationStatus.VERIFIED.getValue());
            break;
        }
        log.info("Updating SSAP Applicant: {} response code: {}, result of verification: {}",
          applicant.getId(), vlpIntegrationLog.getResponseCode(), applicant.getVlpVerificationStatus());

        updatedApplicantVerificationList.add(applicant);
        verificationStatuses.put(applicant.getId(), ApplicantVerificationStatus.fromValue(applicant.getVlpVerificationStatus()));
      }
    });
    if(!updatedApplicantVerificationList.isEmpty()) {
      log.info("Updating SsapApplicant VLP verification status, total Applicants to be updated: {}", updatedApplicantVerificationList.size());
      final List<SsapApplicant> updatedApplicantList = applicantRepository.save(updatedApplicantVerificationList);
      log.info("Updated {} SsapApplicant records", updatedApplicantList.size());
    } else {
      log.info("There was no SSAP Applicants that have VLP verification status. Vlp request had {} members", integrationLogs.length);
    }

    return verificationStatuses;
  }

  /**
   * Returns {@link DocumentType} from given {@link CitizenshipImmigrationStatus}.
   *
   * @param citizenshipImmigrationStatus {@link CitizenshipImmigrationStatus}.
   * @return {@link DocumentType}
   */
  private DocumentType getSelectedDocumentType(final CitizenshipImmigrationStatus citizenshipImmigrationStatus)
  {
    List<EligibleImmigrationDocumentType> immigrationDocumentTypes = citizenshipImmigrationStatus.getEligibleImmigrationDocumentType();

    EligibleImmigrationDocumentType eligibleImmigrationDocument = immigrationDocumentTypes.get(0);

    if (eligibleImmigrationDocument.getDS2019Indicator())
    {
      return DocumentType.DS2019;
    }
    else if (eligibleImmigrationDocument.getI20Indicator())
    {
      return DocumentType.I20;
    }
    else if (eligibleImmigrationDocument.getI94Indicator())
    {
      return DocumentType.I94;
    }
    else if (eligibleImmigrationDocument.getI94InPassportIndicator())
    {
      return DocumentType.I94_IN_PASSPORT;
    }
    else if (eligibleImmigrationDocument.getI327Indicator())
    {
      return DocumentType.I327;
    }
    else if (eligibleImmigrationDocument.getI551Indicator())
    {
      return DocumentType.I551;
    }
    else if (eligibleImmigrationDocument.getI571Indicator())
    {
      return DocumentType.I571;
    }
    else if (eligibleImmigrationDocument.getI766Indicator())
    {
      return DocumentType.I766;
    }
    else if (eligibleImmigrationDocument.getMachineReadableVisaIndicator())
    {
      return DocumentType.MACHINE_READABLE_IMMIGRANT_VISA;
    }
    else if (eligibleImmigrationDocument.getOtherDocumentTypeIndicator())
    {
      return DocumentType.OTHER_ALIEN;
    }
    else if (eligibleImmigrationDocument.getTemporaryI551StampIndicator())
    {
      return DocumentType.TEMPORARY_I551_STAMP;
    }
    else if (eligibleImmigrationDocument.getUnexpiredForeignPassportIndicator())
    {
      return DocumentType.UNEXPIRED_FOREIGN_PASSPORT;
    }
    else if (eligibleImmigrationDocument.getI797Indicator() || eligibleImmigrationDocument.isNoneOfThese())
    {
      log.warn("'None of These' or 'I797 Document' is not supported by the system, returning DocumentType: {}", DocumentType.UNSUPPORTED);
    }

    return DocumentType.UNSUPPORTED;
  }

  @Override
  public DhsDocument fillDocument(HouseholdMember householdMember)
  {
    final CitizenshipImmigrationStatus citizenshipImmigrationStatus = householdMember.getCitizenshipImmigrationStatus();
    final DocumentType documentType = getSelectedDocumentType(citizenshipImmigrationStatus);
    final CitizenshipDocument citizenshipDocument = householdMember.getCitizenshipImmigrationStatus().getCitizenshipDocument();

    return getDhsDocumentFromCitizenshipDocument(citizenshipDocument, documentType);
  }

  private DhsDocument getDhsDocumentFromCitizenshipDocument(final CitizenshipDocument citizenshipDocument, DocumentType documentType) {

    if (documentType != null)
    {
      switch (documentType)
      {
        case CERTIFICATE_OF_CITIZENSHIP:
          return new CertificateOfCitizenshipDocument().from(citizenshipDocument);
        case DS2019:
          return new Ds2019Document().from(citizenshipDocument);
        case I20:
          return new I20Document().from(citizenshipDocument);
        case I94:
          return new I94Document().from(citizenshipDocument);
        case I94_IN_PASSPORT:
          return new I94InPassportDocument().from(citizenshipDocument);
        case I327:
          return new I327Document().from(citizenshipDocument);
        case I551:
          return new I551Document().from(citizenshipDocument);
        case I571:
          return new I571Document().from(citizenshipDocument);
        case I766:
          return new I766Document().from(citizenshipDocument);
        case MACHINE_READABLE_IMMIGRANT_VISA:
          return new MachineReadableImmigrantVisaDocument().from(citizenshipDocument);
        case NATURALIZATION_CERTIFICATE:
          return new NaturalizationCertificateDocument().from(citizenshipDocument);
        case OTHER_ALIEN:
          return new OtherAlienDocument().from(citizenshipDocument);
        case OTHER_I94:
          return new OtherI94Document().from(citizenshipDocument);
        case TEMPORARY_I551_STAMP:
          return new TemporaryI551StampDocument().from(citizenshipDocument);
        case UNEXPIRED_FOREIGN_PASSPORT:
          return new UnexpiredForeignPassportDocument().from(citizenshipDocument);
        case UNSUPPORTED:
          return new UnsupportedDocument();
        default:
          break;
      }
    }

    log.error("DocumentType is not supported: documentType={}, citizenshipDocument={}", documentType, citizenshipDocument);
    return null;
  }

  private VlpPayload createPayload(HouseholdMember householdMember, DhsDocument document)
  {
    VlpPayload vlpPayload = new VlpPayload();

    final CitizenshipDocument citizenshipDocument = householdMember.getCitizenshipImmigrationStatus().getCitizenshipDocument();

    String firstName = householdMember.getName().getFirstName();
    String lastName = householdMember.getName().getLastName();
    String middleName = householdMember.getName().getMiddleName();
    String aka = getAka(firstName, lastName, middleName);

    if (citizenshipDocument.getNameSameOnDocumentIndicator() == Boolean.FALSE)
    {
      firstName = citizenshipDocument.getNameOnDocument().getFirstName();
      lastName = citizenshipDocument.getNameOnDocument().getLastName();
      middleName = citizenshipDocument.getNameOnDocument().getMiddleName();
      aka = getAka(firstName, lastName, middleName);
      // TODO: Do we need to include suffix for VLP Payload?
    }

    vlpPayload.setFirstName(firstName);
    vlpPayload.setLastName(lastName);

    if(middleName != null && !"".equals(middleName.trim())) {
      vlpPayload.setMiddleName(middleName);
    }

    vlpPayload.setAka(aka);

    vlpPayload.setDhsDocument(document);
    vlpPayload.setDateOfBirth(householdMember.getDateOfBirth());

    // TODO: Where do we take these from or we just make them true/false always?
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setRequestSponsorDataIndicator(false);

    vlpPayload.setCasePOCFullName(casePocFullName);
    vlpPayload.setCasePOCPhoneNumber(casePocPhoneNumber);

    return vlpPayload;
  }

  private String getAka(final String firstName, String lastName, String middleName)
  {
    StringBuilder sb = new StringBuilder();
    sb.append(firstName);
    sb.append(" ");
    if (middleName != null && !middleName.trim().equals(""))
    {
      sb.append(middleName);
      sb.append(" ");
    }

    sb.append(lastName);
    return sb.toString();
  }
}
