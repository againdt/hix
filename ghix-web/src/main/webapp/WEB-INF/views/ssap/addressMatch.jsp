<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="row-fluid" style="margin-top: -20px">
	<c:if test="${enteredLocation != null}">
		<div class="span6">
			<h4>You entered:</h4>
			<div class="margin10-l">
				${enteredLocation.address1} <c:if test="${enteredLocation.address2 != 'null'}">${enteredLocation.address2}</c:if><br>
				${enteredLocation.city}, ${enteredLocation.state} ${enteredLocation.zip}
			</div>
		</div>
	</c:if>
	
	<c:if test="${fn:length(validAddressList) > 0}">
		<div class="span6">
			<h4>We found:</h4>
			<c:forEach items="${validAddressList}" var="address" begin="0" varStatus="rowCounter">
				<div class="margin10-l">
					${address.address1} <c:if test="${address.address2 != 'null'}">${address.address2}</c:if><br>
					${address.city}, ${address.state} ${address.zip}
				</div>
			</c:forEach>
		</div>
	</c:if>
</div>

<c:if test="${fn:length(validAddressList) > 0}">
	<div id="addressSelection" class="ssap__form__margin2-t">
		<h4>Select your address:</h4>
		<c:forEach items="${validAddressList}" var="address" begin="0" varStatus="rowCounter">
			<div class="margin10-l">
				<label> 
					<input name="address" checked type="radio" class="margin0" value="${address.address1},${address.address2},${address.city},${address.state},${address.zip}"  /> 
					<span class="margin10-l">${address.address1}<c:if test="${address.address2 != null && address.address2 != 'null'}"> ${address.address2}</c:if>, ${address.city}, ${address.state} ${address.zip}</span>
				</label>
			</div>
		</c:forEach>
		
		<div class="margin10-l">
			<label> 
				<input name="address" type="radio" class="margin0" value="${enteredLocation.address1},${enteredLocation.address2},${enteredLocation.city},${enteredLocation.state},${enteredLocation.zip}" /> 
				<span class="margin10-l">${enteredLocation.address1}<c:if test="${enteredLocation.address2 != 'null'}"> ${enteredLocation.address2}</c:if>, ${enteredLocation.city}, ${enteredLocation.state} ${enteredLocation.zip}</span>
			</label>
		</div>
	</div>
</c:if>