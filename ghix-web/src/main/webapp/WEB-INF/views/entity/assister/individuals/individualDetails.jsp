<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<c:set var="externalIndividualFrameNewId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
<c:set var="isUserCreated" value="${isIndividualUserExists}" ></c:set>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<script type="text/javascript">
	
	$(document).ready(function() {
	});	
	 $(function() {
		    $(".newcommentiframe").click(function(e){
		          e.preventDefault();
		          var href = $(this).attr('href');
		          if (href.indexOf('#') != 0) {
		             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:"static"});
		      }
		    });
		  });
		  
		  function closeIFrame() {
		    $("#newcommentiframe").remove();
		    $(".modal-backdrop").remove();
		    //var encId = "${externalIndividualFrameNewId}";
		    //window.location.href = '/hix/entity/assister/individualcomments/'+encId;
		  }
		  
		  function closeCommentBox() {
			  $("#newcommentiframe").remove();
			  $(".modal-backdrop").remove();
			  var encId = "${externalIndividualFrameNewId}";
			  var url = '/hix/entity/assister/individualcomments/'+ encId;
			  window.location.assign(url);
		  }
		  
		  function setPopupValue() {
				if(document.getElementById("frmPopup").elements['indchkshowpopup'].checked) {
					document.frmPopup.showPopupInFuture.value = "N";
				}
		  }
</script>
<div class="gutter10-lr" role="main" style="margin-top:10px;">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/entity/assister/individuals?desigStatus=Pending"/>"><spring:message code="label.assister.manage"/></a></li>
				<li>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1 tabindex="0" aria-flowto="sidebar rightpanel"><a name="skip"></a>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</h1>
	</div>
	
	<div class="row-fluid">
		<form class="form-vertical" id="frmviewindividualprofile"
			name="frmviewindividualprofile" action="individualinfo" method="POST">
			<div id="sidebar" class="span3">
				   <ul class="nav nav-list">         
				  <li class="active"><spring:message code="label.assister.summary"/></li>
				  <c:set var="externalIndividualId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
				  <li><a href="/hix/entity/assister/individualcomments/${externalIndividualId}"><spring:message code="label.assister.comments"/></a></li>
				  </ul>
				 <br>
				 <div class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.assister.actions"/></div>
				<c:if test="${!CA_STATE_CODE}">
					<ul class="nav nav-list">
						<c:set var="externalIndividualNewId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
						<li class="navmenu"><a name="addComment" href="<c:url value="/entity/assister/newcomment?target_id=${externalIndividualNewId}&target_name=DESIGNATEASSISTER&individualName=${externalIndividual.firstName} ${externalIndividual.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.assister.newComment"/></a></li>
						<c:if test="${checkDilog == null}">
						<li class="navmenu"><a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
						</c:if>
						<c:if test="${checkDilog != null}">
						<li class="navmenu"><a href="/hix/switchToIndividualView/dashboard?switchToModuleName=<encryptor:enc  value="individual"/>&switchToModuleId=${externalIndividualNewId}&switchToResourceName=${externalIndividual.firstName} ${externalIndividual.lastName}" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
						</c:if>
					  <c:if test="${isConsumerActivated == false}">
					  	<li><a name ="send-activation-link"  href="javascript:void(0)"  id="send-activation-link"><i class="icon-envelope"></i><spring:message code="label.assister.resendActivationEmail"/></a></li>
					  </c:if>
					</ul>
				</c:if>
				
				
			</div>
			<!-- end of span3 -->
			<div id="rightpanel" class="span9" role="contentinfo" tabindex="0">
				<div class="content-header header"><h4 class="pull-left"  tabindex="0"><spring:message code="label.assister.summary"/></h4>
				<a class="btn btn-medium pull-right ind-edit-btn"  <%-- href="<c:url value="/entity/assister/indsummaryinfo/${externalIndividualId}"/>" --%>><spring:message  code="label.edit"/></a>
				</div>
				<div class="row-fluid gutter10-tb">
					
						<table class="table table-border-none" tabindex="0">
							<tbody  tabindex="0">
								<tr>
									<td class="txt-left"><spring:message code="label.assister.primaryApplicant"/></td>
									<td><strong>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</strong></td>
								</tr>
								 
								<tr>
									<td class="txt-left"><spring:message code="label.assister.address"/></td>
									<td><strong>${externalIndividual.address1} <br/>${externalIndividual.address2} ${externalIndividual.city}&nbsp;${externalIndividual.state}</strong></td>
								</tr>
								<tr>
								 <td class="txt-left"><spring:message code="label.assister.PhoneNumber"/></td>
									<td>
										<strong>
											<c:choose>
											<c:when test="${externalIndividual.phoneNumber!=null}">
											<c:set var="primaryPhoneNumber" value="${externalIndividual.phoneNumber}"></c:set>
											<c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)})${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
											${formattedPrimaryPhoneNumber}
											</c:when>
											<c:otherwise>
											${externalIndividual.phoneNumber}
											</c:otherwise>
											</c:choose>
										</strong>
									</td>
								</tr>
								<tr>
									<td class="txt-left"><spring:message code="label.brkEmail"/></td>
									<td><strong>${externalIndividual.emailAddress}</strong></td>
								</tr>
								 <tr>
									<td class="txt-left"><spring:message code="label.assister.indEligibilityStatus"/></td>
									<td><strong>${externalIndividual.eligibilityStatus}</strong></td>
								</tr>
								<tr>
									<td class="txt-left"><spring:message code="label.ApplicationStatus"/></td>
									<td><strong>${externalIndividual.applicationStatus}</strong></td>
								</tr> 
								
							</tbody>
						</table>
					
				</div>
				<div class="form-actions">
						<!-- <a class="btn btn-medium pull-right ind-edit-btn">Edit</a> -->
						<span id="processing" class="margin10-l hide"> <spring:message code="ssap.footer.processing" text="Processing please wait"/><img class="margin10-l" src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></span>
				</div>
				<!-- Showing comments -->
				<%-- 							<comment:view targetId="${broker.id}" targetName="BROKER"> --%>
				<%-- 							</comment:view> --%>
			</div>
			<df:csrfToken/>
		</form>
		
		<!-- Modal (Commented modal code till switch-account implementation) -->
			<jsp:include page="individualViewModal.jsp" />
		<!-- Modal end -->
	</div>
	<!-- #row-fluid -->
</div>


<script type="text/javascript">

$("document").ready(function (){
	var csrValue = $("#csrftoken").val();
	
	$(".ind-edit-btn").click(function (){
				
		 if(${isUserCreated}) {
			$('<div id="ind-dialogue" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
						+ '<div class="modal-header">'
							+ '<h3 class="pull-left">Failure</h3>'
							+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
							
						+ '</div>'
						+ '<div class="modal-body">'
							+ '<p>This action is only allowed for individual consumers who have not completed the sign up process.'
							+ '${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already completed the sign up process.</p>'
						+ '</div>'
						+'<div class="modal-footer">'
						+ '<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>'
						+ '</div>'
					+ '</div>').modal();
			
		}
		else {
			$(this).attr("href", "<c:url value='/entity/assister/indsummaryinfo/${externalIndividualId}'/>")
			
		} 
		
		
		
	});
	
	$("#send-activation-link").click(function (){
		if(${isConsumerActivated}) {
			
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	            + ' activation email cannot be sent.</p></div><div class="modal-footer txt-center"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        } 
		
		else if(${isUserCreated}) {
			
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	            + ' activation email cannot be sent.</p></div><div class="modal-footer txt-center"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        } else {
	        	
	        	 $("#processing").show();
	        	 var  strPath =  "<%=  request.getContextPath()  %>";
                var  pathURL=strPath+"/assister/individual/sendactivationlink/${externalIndividualId}";
                $.ajax({ 	type: "POST",
               	 		url: pathURL,
               	 		 data:{ csrftoken:'<df:csrfToken plainToken="true"/>' },
               	 		success: function (data,status) {
               	 			if(status === 'success') {
   	              				if(-1 == data.search("SUCCESS")) { 
   	              					$("#processing").hide();
   	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
   	              				}else {
   	              					$("#processing").hide();
   	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Success!</h3></div><div class="modal-body" style="max-height:470px; "><p>A new activation email has been sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
   	              				}
   	             			}else {
   	             				$("#processing").hide();
   	              				$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
   	             			}
               	           
               	        },
               	        error: function(e) {
               	            console.log("Error:" + e);
               	        }
                });
	        }
	  });
	
});
</script>