
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.GYear;


/**
 * 
 *                 A data type created to restrict Year from 1937 to 2025
 *             
 * 
 * <p>Java class for QuarterYearType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuarterYearType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;http://niem.gov/niem/proxy/xsd/2.0>gYear">
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuarterYearType")
public class QuarterYearType
    extends GYear
{


}
