package com.getinsured.hix.planmgmt.mapper;

public class StateAreaRatingPojo {
	private String zip;
	private String county;
	private String ratingArea;
	private String state;
	private String countyFIPS;
	private String errorFlag;
	
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getRatingArea() {
		return ratingArea;
	}
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountyFIPS() {
		return countyFIPS;
	}
	public void setCountyFIPS(String countyFIPS) {
		this.countyFIPS = countyFIPS;
	}
	public String getErrorFlag() {
		return errorFlag;
	}
	public void setErrorFlag(String errorFlag) {
		this.errorFlag = errorFlag;
	}
	
}
