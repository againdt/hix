(function (module) {
    'use strict';

    module.controller('PaginationCtrl', ['$scope', 'Doctors', 'doctorsService', function ($scope, Doctors, doctorsService) {
        var pg = this,
            pages = [];

        pg.getPages = getPages;
        pg.getStyle = getStyle;
        pg.getCurrentPage = getCurrentPage;
        pg.selectPage = selectPage;
        pg.isFirstPage = isFirstPage;
        pg.isEndPage = isEndPage;
        pg.selectPrePage = selectPrePage;
        pg.selectNextPage = selectNextPage;
        pg.totalPage = 0;

        ///////////

        function getPages () {
            var total = Doctors.totalDoctors(),
                pageSize = Doctors.pageSizeRange(),
                totalPages = Math.ceil(total / pageSize);
            if (totalPages > 10) {
            	totalPages = 10;
            }
            pg.totalPages = totalPages;
            pages = _.range(1, totalPages+1);
            
            return pages;
        }

        function getStyle() {
            return {
                'display': 'block'
            };
        }

        function getCurrentPage () {
            return Doctors.currentPage();
        }

        function selectPage(page) {
            doctorsService
                .getDoctors({currentPage: page})
                .then(function (data) {
                    Doctors.setDoctors(data.providers);
                    Doctors.currentPage(page);
                    $scope.$emit('doctorsChange');
                });
        }

        function isFirstPage () {
            return getCurrentPage() === 1;
        }

        function isEndPage () {
            return getCurrentPage() == pg.totalPages;
        }

        function selectPrePage () {
            selectPage(getCurrentPage() -  1);
        }

        function selectNextPage () {
            selectPage(getCurrentPage() + 1);
        }
    }]);

    module.factory('PaginationModel', [function () {
        return {
        };

        //////////
    }])

    module.directive('providerMapPagination', ['Doctors', function (Doctors) {
        return {
            restrict: 'E',
            controller: 'PaginationCtrl',
            controllerAs: 'pg',
            templateUrl: 'pagination.html',
            link: link
        };

        //////////////

        function link (scope, elem, attrs, ctrl) {
        }
    }]);

}(angular.module('ProviderMap.Directive.pagination', [
  'ProviderMap.Factory.doctors'
])));