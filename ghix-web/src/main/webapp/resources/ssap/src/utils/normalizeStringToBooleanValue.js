export default (value, returnAsNumber) => {
    let modifiedValue;
    switch (value) {
    case 'true':
        modifiedValue = true; break;
    case 'false':
        modifiedValue = false; break;
    case 'null':
        modifiedValue = null; break;
    default:
        modifiedValue = returnAsNumber ? +value : value;
    }
    return modifiedValue;
};