package com.getinsured.iex.ssap;

import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;

/**
 * Definition for other state or federal program type.
 *
 * @author Yevgen Golubenko
 * @since 5/14/19
 */
public class OtherStateOrFederalProgram {
  private String name;
  private OtherStateOrFederalProgramType type;
  private boolean eligible;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OtherStateOrFederalProgramType getType() {
    return type;
  }

  public void setType(OtherStateOrFederalProgramType type) {
    this.type = type;
  }

  public boolean isEligible() {
    return eligible;
  }

  public void setEligible(boolean eligible) {
    this.eligible = eligible;
  }
}
