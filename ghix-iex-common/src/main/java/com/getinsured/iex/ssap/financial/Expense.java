package com.getinsured.iex.ssap.financial;

import com.getinsured.iex.ssap.financial.type.ExpenseType;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.google.gson.annotations.Expose;

/**
 * Describes expense for {@link com.getinsured.iex.ssap.HouseholdMember}.
 * Used in SSAP Financial Flow.
 *
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public class Expense {

  @Expose
  private ExpenseType type = ExpenseType.UNSPECIFIED;
  @Expose
  private long amount;
  @Expose
  private Frequency frequency = Frequency.UNSPECIFIED;

  public ExpenseType getType() {
    return type;
  }

  public void setType(ExpenseType type) {
    this.type = type;
  }

  public long getAmount() {
    return amount;
  }

  public void setAmount(long amount) {
    this.amount = amount;
  }

  public Frequency getFrequency() {
    return frequency;
  }

  public void setFrequency(Frequency frequency) {
    this.frequency = frequency;
  }
}
