package com.getinsured.eligibility.rrv.model;

public class RrvMetadata {
    private Long batchId;
    private int householdCount;

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public int getHouseholdCount() {
        return householdCount;
    }

    public void setHouseholdCount(int householdCount) {
        this.householdCount = householdCount;
    }
}
