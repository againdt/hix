package com.getinsured.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.HouseholdEnrollment;

public interface IHouseholdEnrollmentRepository extends JpaRepository<HouseholdEnrollment, Integer> {
	
	/**
	 * Fetches the HouseholdEnrollment for a given Household Id
	 * @param householdId 
	 * @return HouseholdEnrollment associated with that Household Id
	 */
	@Query("FROM HouseholdEnrollment he where he.householdId = :householdId")
	List<HouseholdEnrollment> findHouseholdEnrollmentByHouseholdId(@Param("householdId") Integer householdId); 
	
}
