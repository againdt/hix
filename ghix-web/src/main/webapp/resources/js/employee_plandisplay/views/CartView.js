(function (views) {
	views.Carts = Backbone.View.extend({
		events : {
			'click button.removeFromCart': 'removeFromCart',
			'click a.checkout' : 'checkout'
		},
		el : '#cart',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','removeFromCart','checkout');
			var self = this;
			self.hasSrNo = new Array();
			self.collection.fetch({
				success : function () {
					
					if(self.collection.length==0){
						$('#tbody_cart').html('<tr><td></td><h3>No Plans are available in Cart</h3></tr>');
					}
					self.collection.setCartSummary(false,false);
					
				}
			});
			
			self.cartModel = options.cartModel;
			
			
			$('.removeFromCart').bind('click', this.removeFromCart);
			$('.checkout').bind('click', this.checkout);
			self.collection.bind('reset', self.addAll);
		},
		
		addAll : function (models) {
			var self = this;
			this.updateCartSummary();
			$('#tbody_cart').html('');

			self.collection.each (self.addOne);
		},
		
		addOne : function (model) {
			
			var viewSummary = new Cart({model:model});
			$('#tbody_cart').append(viewSummary.render().el);
			$('#totalMonthlyPayrollDeduction').html("$"+model.get("monthlyPayrollDeduction")+"/month");
			$('.removeFromCart').bind('click', this.removeFromCart);
		},
		
		updateCartSummary : function () {
			var self = this;
			$('#planCount').html(self.collection.cartSummary.planCount);
		},
		
		removeFromCart : function (e) {
			var itemId = e.target.id.replace("remove_","");
			this.collection.removeSelectedItems(itemId);
			$('#totalMonthlyPayrollDeduction').html("");
			this.updateCartSummary();
		},
		
		checkout : function(e){
			if(this.collection.cartItems.length > 0){
				location.href="signapplication";				
			}
			else{
				alert('Can not checkout as cart is empty'+'\n'+'Please add any plan in Cart');
			}
		}

	});
	var Cart = Backbone.View.extend({
		tagName : 'tbody_cart',
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			var template = _.template($('#tmpCart').html());
			$(this.el).html(template(this.model.toJSON()));
			
			return this;
		}
	});
})(App.views);