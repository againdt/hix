package com.getinsured.hix.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.Utils;

public class BatchNodes {
	
	private static Logger LOGGER = LoggerFactory.getLogger(BatchNodes.class);
	private static HashMap<String, String> nodeMapping = new HashMap<String,String>(15);
	
	static{
		loadBatchNodes();
	}
	
	public static HashMap<String, String>  getNodeMapping(){
		return nodeMapping;
	}
	
	@SuppressWarnings("unchecked")
	public static synchronized JSONObject getBatchNodes(){
		JSONArray nodes = new JSONArray();
		JSONObject node = null;
		Set<Entry<String, String>> entries = nodeMapping.entrySet();
		Iterator<Entry<String, String>> cursor = entries.iterator();
		Entry<String, String> nodeEntry = null;
		while(cursor.hasNext()){
			nodeEntry = cursor.next();
			node = new JSONObject();
			node.put("alias", nodeEntry.getKey());
			node.put("server_base_url", nodeEntry.getValue());
			nodes.add(node);
		}
		JSONObject allNodes = new JSONObject();
		allNodes.put("nodes",nodes);
		return allNodes;
	}
	
	private static void loadBatchNodes(){
		String ghixHome =  System.getProperty("GHIX_HOME");
		if(ghixHome == null){
			throw new RuntimeException("Expected GHIX_HOME property to be available to load the alias mapping, found none");
		}
		String batchNodesConfig = ghixHome+File.separatorChar+"ghix-setup"+File.separatorChar+"conf"+File.separatorChar+"batch_nodes.xml";
		LOGGER.info("Loading batch nodes from file:"+batchNodesConfig);
		Document doc = null;
		InputStream is = null;
		try {
			DocumentBuilderFactory factory = Utils.getDocumentBuilderFactoryInstance();
			is = new FileInputStream(batchNodesConfig);
			doc = factory.newDocumentBuilder().parse(is);
			buildAliasMap(doc);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			LOGGER.error("Failed to load the attribute map", e);
		}finally{
			IOUtils.closeQuietly(is);
		}
	}
	
	private static void buildAliasMap(Document doc) {
		NodeList aliasMaps = doc.getElementsByTagName("batch_node");
		Node aliasMapNode = null;
		String alias = null;
		String server = null;
		Node tmp = null;
		NamedNodeMap attributesList = null;
		int len = aliasMaps.getLength();
		for(int i = 0; i < len; i++){
			aliasMapNode = aliasMaps.item(i);
			attributesList = aliasMapNode.getAttributes();
			tmp = attributesList.getNamedItem("alias");
			if(tmp != null){
				alias = tmp.getNodeValue();
			}else{
				LOGGER.error("No alias found for alias entry, skipping");
				continue;
			}
			tmp = attributesList.getNamedItem("server_base_url");
			if(tmp != null){
				server = tmp.getNodeValue();
			}else{
				LOGGER.error("No server entry found for alias:"+alias+", skipping");
				continue;
			}
			if(alias.length() > 0 && server.length() > 0){
				nodeMapping.put(alias, server);
				LOGGER.info("Added alias:"+alias+" for server:"+server);
			}else{
				LOGGER.error("Invalid alias mapping entry:[alias="+alias+" -> server="+server+"], Ignoring");
			}
		}
	}
	
	public static void main(String args[]){
		BatchNodes batchNodes = new BatchNodes();
		HashMap<String, String> nodemapping = batchNodes.getNodeMapping();
		for (HashMap.Entry<String, String> entry : nodeMapping.entrySet()) {
		    String alias = entry.getKey();
		    Object server_url = entry.getValue();
		    System.out.println("alias = " + alias + " url : " + server_url.toString());
		}
	}
}
