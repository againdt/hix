package com.getinsured.iex.hub.vlp37.wrapper;

import java.math.BigInteger;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionResponseSetType;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;


public class RetrieveStep2CaseResolutionResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RetrieveStep2CaseResolutionResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String userField;
	private String responseDate;
	
	private BigInteger versionId;
    private String majorCd;
    private String majorStatement;
    private String minorCd;
    private String minorStatement;
    private Boolean sponsoredIndicator;
    private String serviceReceiptDate;
    private String lprStatusDate;
    private ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData;
    private String grantDate;
    private AdditionalFieldsTypeWrapper resolutionData;
	
	private String webServSftwrVer;
	private String fiveYearBarApplyCode;
	private String lawfulPresenceVerifiedCode;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
	
	    
    public RetrieveStep2CaseResolutionResponseSetTypeWrapper(RetrieveStep2CaseResolutionResponseSetType retrieveStep2CaseResolutionResponseSetType){
    	
    	if (retrieveStep2CaseResolutionResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = retrieveStep2CaseResolutionResponseSetType.getCaseNumber();
    	this.userField = retrieveStep2CaseResolutionResponseSetType.getUserField();
    	this.versionId = retrieveStep2CaseResolutionResponseSetType.getVersionId();
    	this.majorCd = retrieveStep2CaseResolutionResponseSetType.getMajorCd();
    	this.majorStatement = retrieveStep2CaseResolutionResponseSetType.getMajorStatement();
    	this.minorCd = retrieveStep2CaseResolutionResponseSetType.getMinorCd();
    	this.minorStatement = retrieveStep2CaseResolutionResponseSetType.getMinorStatement();
    	this.sponsoredIndicator = retrieveStep2CaseResolutionResponseSetType.isSponsoredIndicator();
		this.arrayOfSponsorshipData = new ArrayOfSponsorshipDataTypeWrapper(retrieveStep2CaseResolutionResponseSetType.getArrayOfSponsorshipData());
		this.resolutionData = new AdditionalFieldsTypeWrapper(retrieveStep2CaseResolutionResponseSetType.getResolutionData());
    	this.webServSftwrVer = retrieveStep2CaseResolutionResponseSetType.getWebServSftwrVer();
    	this.fiveYearBarApplyCode = retrieveStep2CaseResolutionResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = retrieveStep2CaseResolutionResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = retrieveStep2CaseResolutionResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = retrieveStep2CaseResolutionResponseSetType.getUSCitizenCode();
    	this.lawfulPresenceVerifiedCode = retrieveStep2CaseResolutionResponseSetType.getLawfulPresenceVerifiedCode();
    	
    	try{
	    	if(retrieveStep2CaseResolutionResponseSetType.getServiceReceiptDate() != null){
	    		this.lprStatusDate = VLPServiceUtil.xmlDateToString(retrieveStep2CaseResolutionResponseSetType.getLPRStatusDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(retrieveStep2CaseResolutionResponseSetType.getServiceReceiptDate() != null){
	    		this.grantDate = VLPServiceUtil.xmlDateToString(retrieveStep2CaseResolutionResponseSetType.getLPRStatusDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(retrieveStep2CaseResolutionResponseSetType.getServiceReceiptDate() != null){
	    		this.serviceReceiptDate = VLPServiceUtil.xmlDateToString(retrieveStep2CaseResolutionResponseSetType.getServiceReceiptDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(retrieveStep2CaseResolutionResponseSetType.getServiceReceiptDate() != null){
	    		this.responseDate = VLPServiceUtil.xmlDateToString(retrieveStep2CaseResolutionResponseSetType.getResponseDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}

	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}


	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}

	public void setFiveYearBarApplyCode(String fiveYearBarApplyCode) {
		this.fiveYearBarApplyCode = fiveYearBarApplyCode;
	}

	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public void setQualifiedNonCitizenCode(String qualifiedNonCitizenCode) {
		this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public void setFiveYearBarMetCode(String fiveYearBarMetCode) {
		this.fiveYearBarMetCode = fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	public void setUsCitizenCode(String usCitizenCode) {
		this.usCitizenCode = usCitizenCode;
	}



	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}



	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}



	public String getUserField() {
		return userField;
	}



	public void setUserField(String userField) {
		this.userField = userField;
	}



	public String getResponseDate() {
		return responseDate;
	}



	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}



	public BigInteger getVersionId() {
		return versionId;
	}



	public void setVersionId(BigInteger versionId) {
		this.versionId = versionId;
	}



	public String getMajorCd() {
		return majorCd;
	}



	public void setMajorCd(String majorCd) {
		this.majorCd = majorCd;
	}



	public String getMajorStatement() {
		return majorStatement;
	}



	public void setMajorStatement(String majorStatement) {
		this.majorStatement = majorStatement;
	}



	public String getMinorCd() {
		return minorCd;
	}



	public void setMinorCd(String minorCd) {
		this.minorCd = minorCd;
	}



	public String getMinorStatement() {
		return minorStatement;
	}



	public void setMinorStatement(String minorStatement) {
		this.minorStatement = minorStatement;
	}


	public Boolean getSponsoredIndicator() {
		return sponsoredIndicator;
	}

	public void setSponsoredIndicator(Boolean sponsoredIndicator) {
		this.sponsoredIndicator = sponsoredIndicator;
	}

	public String getServiceReceiptDate() {
		return serviceReceiptDate;
	}



	public void setServiceReceiptDate(String serviceReceiptDate) {
		this.serviceReceiptDate = serviceReceiptDate;
	}



	public String getLprStatusDate() {
		return lprStatusDate;
	}



	public void setLprStatusDate(String lprStatusDate) {
		this.lprStatusDate = lprStatusDate;
	}



	public String getGrantDate() {
		return grantDate;
	}



	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}



	public AdditionalFieldsTypeWrapper getResolutionData() {
		return resolutionData;
	}



	public void setResolutionData(AdditionalFieldsTypeWrapper resolutionData) {
		this.resolutionData = resolutionData;
	}

	public ArrayOfSponsorshipDataTypeWrapper getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}

	public void setArrayOfSponsorshipData(
			ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData) {
		this.arrayOfSponsorshipData = arrayOfSponsorshipData;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();

		if(this.caseNumber != null){
			obj.put("CaseNumber",this.caseNumber);
		}

		if(this.webServSftwrVer != null){
			obj.put("WebServSftwrVer",this.webServSftwrVer);
		}

		if(this.fiveYearBarApplyCode != null){
			obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		}

		if(this.qualifiedNonCitizenCode != null){
			obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		}

		if(this.fiveYearBarMetCode != null){
			obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		}

		if(this.usCitizenCode != null){
			obj.put("USCitizenCode",this.usCitizenCode);
		}

		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode", this.lawfulPresenceVerifiedCode);
		}

		return obj.toJSONString();
	}

}
