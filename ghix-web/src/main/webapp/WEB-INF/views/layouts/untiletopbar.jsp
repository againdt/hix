<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${page_title}</title>
<meta name="description" content="" />
<meta name="author" content="" />
 <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>
 
 <link href="<c:url value="/resources/css/slideshow-css.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/theme-eli.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/gi-sp-MN.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/docs.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/jquery-ui-1.8.16.custom.css" />" rel="stylesheet"  type="text/css" />		
 <link href="//fonts.googleapis.com/css?family=Droid+Serif:400,700" media="screen" rel="stylesheet" type="text/css" />
 
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.7.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/slideshow.js" />"></script>	
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.16.custom.min.js" />"></script>	
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap-twipsy.js" />"></script>	
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap-popover.js" />"></script>	
 <script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>	
 <script type="text/javascript" src="<c:url value="/resources/js/verticaltabs.pack.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery.tools.min.js" />"></script>
 <!--[if lt IE 9]>
	<script type="text/javascript" src="<c:url value="/resources/js/html5.js" />"></script>
 <![endif]-->

 <script>
	$(function () {
		$("a[rel=twipsy]").twipsy({
		live: true
		});
	});

	$(function () {
    	$("a[rel=popover]")
        	.popover({
            	offset: 10
            })
            .click(function(e) {
            	e.preventDefault()
			});
	});
	
	$(document).ready(function(){
	  $("#myTable").tablesorter();
	  $("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});
	});

	$(document).ready(function() {
		var $dialog = $('<div></div>').append($('div.notes').html()).append('<br/><p class="help-inline span13"><strong>NOTE:</strong> All data represented in the Minnesota Exchange prototype was provided to Getinsured.com for sample purposes only. The information displayed, and all graphs and reports are illustrative only and Getinsured.com makes no representations as to the validity or accuracy of this information.</p>').dialog({
				autoOpen: false,
				title: 'About this page'
			});

		$('#feedback-badge-right').click(function() {
			$dialog.dialog('open');
			// prevent the default action, e.g., following a link
			return false;
		});
	});
 </script>
 
 <style type="text/css">
   body { padding-top: 60px; }
 </style>
</head>
<body>
	<div id="topbar" class="topbar">
		<div class="fill">
			<div class="container">
				<ul class="nav">
					<li><a href="/" class="brand">GetInsured Health Exchange</a></li>
				</ul>
				<ul class="nav right">
					<li class="helpline"><a href="#">Help Line: 800-999-3333</a></li>
					<li class="chat"><a href="#">Chat</a></li>
				</ul>
			</div> <!-- container ends -->
		</div> <!-- fill ends -->
	</div>
</body>