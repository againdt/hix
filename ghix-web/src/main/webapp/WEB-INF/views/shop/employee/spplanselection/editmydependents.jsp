<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ajaxfileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>

<script type="text/javascript">

var uploadedDocId;
var uploadedDocDtls='';
var input = $('#fileupload');
var fileDetails;
var rowno=0;
var responseText="";

var locationJSON = ${locationJSON};
var empDetailsJSON = ${empDetailsJSON};
var dependentCount = ${dependentCount};
var wardDocumentRequired = '${wardDocumentRequired}';

$(document).ready(function(){
var validator = $('#edit-mydependents').validate({
	ignore: ':hidden:not(".wardDocument")', 
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});
	showWardDoc();
});
function clearvars(index){
	if($('#wardDocument_' +index).val() && $('#wardDocument_' +index).val().indexOf("workspace") != -1){
		deleteFileOnCancel(index);	
	}
	orgSSNvalAssigned=false;
}
function deleteFileOnCancel(index){
	var subUrl = '<c:url value="/shop/employee/spplanselection/removeSupportDocument">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {uploadedDocId : $('#wardDocument_' +index).val()}, 
		dataType:'text',
		success : function(response, xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;

			$('#wardDocument_' +index).val(null);
			var newText = '<input type="file" class="input-file" id="' +  ' "fileupload_" +index '
			+' onchange=""  name="wardDoc" />&nbsp;';
			$('#file_upload_container_' +index).html('');
			$('#file_upload_container_' +index).html(newText);
			 setAjaxPlugin(index);
		},error : function(e) {
		}
	});
} 
function setAjaxPlugin(cnt){

var subUrl =  '<c:url value="/shop/employee/spplanselection/uploadSupportDocument"/>';
	$('#fileupload_' + cnt).ajaxfileupload({			
	    'action': subUrl,
	    'params': {empDetailsId : $('#employeeMemberId_' + cnt).val()}, 		    
	    'onComplete': function(response) {
	    	responseText=response;
	    	if(response.status == false){       
	    	   alert(response.message);
  	      	}
	    	else if(responseText.indexOf("Error") != -1){
	    		 errorDetails=responseText.split('#');
	    		 alert(errorDetails[1]);
	    	}
	    	else if(responseText){
	    		fileDetails=responseText.split('#');
	    		
	    		if(fileDetails.length == 2){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	$('#wardDocument_'+ cnt).val(uploadedDocId);
	            	rowno++;
	            	appendFileDetails(cnt);	
	            	$( "#fileupload_" + cnt).hide();
	            	$('#wardDocument_' + cnt + '_error').hide();
	            	$('#fileupload_error_' + cnt).hide();
	            }
	        	else{
	        		$('#fileupload_error_' + cnt).show();
	        	}
	    	}
	    	else{
	    		$('#fileupload_error_' + cnt).show();
	    	}
	    	
	    	$('#uploaddiv_' + cnt).hide();
	    },
	    'onStart': function() {
	    	$('#uploaddiv_' + cnt).show();
	    	$('#fileupload_' + cnt + '_error').hide();
	    },
	    'onCancel': function() {
	      
	    },
	    'valid_extensions' : ['gif','png','jpg','jpeg','pdf']
	  });
}
function deleteFile(fileId,  index){
	var memberId = $('#employeeMemberId_'+cnt);
	$('#fileupload_'  +index +'_error').hide();
	$('#deletediv_' +index).show();
	var subUrl = '<c:url value="/shop/employee/spplanselection/removeSupportDocument">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {uploadedDocId : fileId, memberId: memberId}, 
		dataType:'text',
		success : function(response, xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			var newText = '<input type="file" class="input-file" id='
				+ "fileupload_"+ index + 'onchange=""  name="wardDoc" />&nbsp;';
			$('#file_upload_container_'+index).html('');
			$('#file_upload_container_'+index).html(newText);
			 //$('#fileupload').replaceWith($('#fileupload').clone(true).val(''));
			 setAjaxPlugin(index);
			$('#wardDocument_'+index).val(null);
			$('#deletediv_'+index).hide();
			$('#upload_files_container_'+index).html("");
		},error : function(e) {
			alert('error in deleting document.');
			$('#deletediv_'+index).hide();
		}
	});
}
function appendFileDetails( cnt){
	orgHtml=$('#upload_files_container_' +cnt).html();
	strHtml='<tr id="' +rowno  +'">'+
	'<td colspan="1">'+fileDetails[1]+'</td>'+
	'<td colspan="1"></td>'+
	'<td colspan="1"><a class="btn" href="javascript:downloadFile(\''+uploadedDocId+'\')">download</a></td>'+
	'<td colspan="1"><a class="btn btn-danger" href="javascript:deleteFile(\''+uploadedDocId+'\','+cnt+')">delete</a></td>'
	+'</tr>';
	$('#upload_files_container_' + cnt).html(strHtml);
	 
}
function downloadFile(fileId){
	var  contextPath =  "<%=request.getContextPath()%>";

	var url = contextPath+"/shop/employee/spplanselection/downloadDocument?udId="+fileId;
	var subUrl =  '<c:url value="'+url+'"/>';
	window.open(subUrl,"Supported Document");
}
function showWardDoc(){
	if(dependentCount < empDetailsJSON.length){
   	  dependentCount = empDetailsJSON.length;
   	} 
   	for(var index =0, i=0;i<dependentCount;i++, index++){
   		obj = empDetailsJSON[i];
   		var wardDoc = obj['wardDoc'];
   		var type = obj['type']; 
   		if( type == 'WARD'){
   			//var wardDoc = $('#fileupload_'+index).attr('value');
	        if( wardDoc!='none' && wardDoc!='' && !(wardDoc === undefined)){
	        	$('#uploadWardDoc_'+index).show();
	        	fileDetails=wardDoc.split('#');
	    		if(fileDetails.length == 2){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	$('#wardDocument_'+index).val(uploadedDocId);
	            	rowno++;
	            	appendFileDetails(index);		
	            	$( "#fileupload_"+index ).hide();
	        	}
	        }
	        else{
	        	$('#wardDocument_'+index).val(null);
	        	 if(type=='WARD'){ 
	        		 $('#uploadWardDoc_' + index).show();
	        	 } else{
	            	$('#uploadWardDoc_' +index).hide();
	             }
	        	$('#upload_files_container_'+index).html('');
	        }	
   		}
   	}
}


function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue, zipId);
	//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( indexValue,zipCodeValue, '' );
}


	function getCountyList(eIndex, zip, county) {
		var subUrl =  '<c:url value="/shop/employee/spplanselection/addCounties" />';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response, xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateCounties(response, eIndex, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, eIndex, county) {
        var optionsstring = '<option value="">Select County...</option>';
			$.each(response, function(key, value) {
				var selected = (county == key) ? 'selected' : '';
				var optionVal = key+'#'+value;
				var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
				optionsstring = optionsstring + options;
			});
			$('#county_'+eIndex).html(optionsstring);
	} // end populateCounties

	/*focus out event for the text input to call the lightbox*/
			
	function validateAddress(eindex){
		var address1_e='address1_'; var address2_e='address2_'; var city_e= 'city_'; var state_e='state_'; var zip_e='zip_';
		var lat_e='lat_';var lon_e='lon_'; var rdi_e='rdi_'; var county_e='county_';
		
		var model_address1 = address1_e + 'hidden'+'_'+eindex ;
		var model_address2 = address2_e + 'hidden'+'_'+eindex ;
		var model_city = city_e + 'hidden'+'_'+eindex ;
		var model_state = state_e + 'hidden'+'_'+eindex ;
		var model_zip = zip_e + 'hidden'+'_'+eindex ;
		
		address1_e=address1_e+eindex;
		address2_e=address2_e+eindex;
		city_e=city_e+eindex;
		state_e=state_e+eindex;
		zip_e=zip_e+eindex;
		lat_e=lat_e+eindex;
		lon_e=lon_e+eindex;
		rdi_e=rdi_e+eindex;
		county_e=county_e+eindex;
		
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}	
			
		 	 viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
				idsText);  
		}
		getCountyList(eindex, $('#'+ zip_e).val(), '');
		 $('#'+address1_e).focusin(function(e) {
			if($('#'+zip_e).val().length >= 5){
				getCountyList(eindex, $('#'+ zip_e).val(), '');
				$('#'+address1_e).unbind( "focusin" );
			 }   
		}); 
	}
	
</script>
<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}</span></h1>
  <div class="row-fluid">
	    <div class="span3" id="sidebar">
	      	<util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
	      	<util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
	    </div><!--/span3-->
	    <div class="span9" id="rightpanel">  
	    <form id="edit-mydependents" class="form-horizontal" action="<c:url value='/shop/employee/spplanselection/editmydependents'/>" method="post" name="edit-mydependetns">
              <df:csrfToken/>
	    		<input type='hidden' name='applicationId' value='${employeeApplication.id}'/>    
		      <div class="header">
		      	<h4 class="pull-left">My Dependents</h4>
		        <input type="submit" id="save" class="btn btn-primary btn-small pull-right" alt="Save" value="Save" />
		        <a href="<c:url value='/shop/employee/spplanselection/mydependents/${employeeApplication.id}'/>" id="cancel" class="btn btn-small pull-right" alt="Cancel" onClick="javascript:clearvars();">Cancel</a>
		      </div>    
	        <div class="gutter10">
	          <div class="row-fluid">
	            <p>The following information was entered by your employer and verified by you.</p>
	      
	      		<c:forEach items="${employeeDetailsList}" var="empDtls"  varStatus="cnt">
	          	<c:if test="${empDtls.type == 'SPOUSE' || empDtls.type == 'LIFE_PARTNER'}"> <h4 class="bottom-border">Spouse</h4> </c:if>
	          	<c:if test="${empDtls.type == 'CHILD' || empDtls.type == 'WARD' }"><h4 class="bottom-border">Child </h4> </c:if>
	          	<div id="spouse_${cnt.index}" class="row-fluid offset1 addressBlock">
	                    <div class="control-group">                     
	                    	<label class="control-label" for="firstName_${cnt.index}"><spring:message  code='label.emplFirstName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                      	<div class="controls">
	                       	<input type="text" name="employeeDetailsApplication[${cnt.index}].firstName" id="firstName_${cnt.index}" tabindex="0" value="${empDtls.firstName}">
	                       	<span id="firstName_${cnt.index}_error"></span>
	                          <input type="hidden" name="firstName_${cnt.index}_hidden" id="firstName_${cnt.index}_hidden" tabindex="0"> 
	                    	</div>
		                 </div>
		                 <div class="control-group">                     
	                    	<label class="control-label" for="lastName_${cnt.index}"><spring:message  code='label.emplLastName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                      	<div class="controls">
	                       	<input type="text" name="employeeDetailsApplication[${cnt.index}].lastName" id="lastName_${cnt.index}" tabindex="0" value="${empDtls.lastName}">
	                       	<span id="lastName_${cnt.index}_error"></span>
	                          <input type="hidden" name="lastName_${cnt.index}_hidden" id="lastName_${cnt.index}_hidden" tabindex="0"> 
	                    	</div>
		                 </div>
		                  <div class="control-group">                     
	                    	<label class="control-label" for="suffix_${cnt.index}">Suffix</label>
	                      	<div class="controls">
	                      		<select class="input-medium"  name="employeeDetailsApplication[${cnt.index}].suffix" id="suffix_${cnt.index}" >
									<option value=""></option>
									<c:forEach var="suffix" items="${suffixlist}">
										<option id="${suffix}" <c:if test="${suffix == empDtls.suffix}"> SELECTED </c:if>
										value="${suffix}">${suffix}</option>
									</c:forEach>
								</select>
								<span id="suffix_${cnt.index}_error"></span>
	                      		<input type="hidden" name="suffix_${cnt.index}_hidden" id="suffix_${cnt.index}_hidden" tabindex="0">
	                    	</div>
		                 </div>
						 <div class="control-group">
							<label class="span4 txt-right">Relationship</label>
							<div class="controls">
								<c:choose>
								    <c:when test="${empDtls.type == null || empDtls.type == ''}">
								       <weak>N/A</weak>
								    </c:when>
								    <c:otherwise>
								       <c:set var="relation" value="${fn:replace(empDtls.type,'_', ' ')}"/>
								       <strong>${fn:toUpperCase(fn:substring(relation, 0, 1))}${fn:toLowerCase(fn:substring(relation, 1, -1))}</strong>
								    </c:otherwise>
							    </c:choose>
							</div>
						</div>
		                 </div>
	                    <div class="control-group">
		                      <label for="employeeAddress" class="control-label"><spring:message  code="label.emplAddress"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
		                      <div class="controls">
		                        <input type="radio" id="isEmployeeAddressYes_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].isEmployeeAddress" ${fn:contains(empDtls.isEmployeeAddress, 'Y') ? 'checked="checked"' : ''} class="input-medium error" tabindex="0" value="Y" onclick="showHideAddressDiv('${cnt.index}','Y','');">YES
		                        <input type="radio" id="isEmployeeAddressNo_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].isEmployeeAddress" ${fn:contains(empDtls.isEmployeeAddress, 'N') ? 'checked="checked"' : ''} class="input-medium error" tabindex="0" value="N" onclick="showHideAddressDiv('${cnt.index}','N','${empDtls.id}');" >NO
								<div id="employeeAddress_${cnt.index}_error" class="error help-inline"></div>	
		                      </div>
		                    </div>
	                    <div id="addressDiv_${cnt.index}" ${fn:contains(empDtls.isEmployeeAddress, 'Y') ? 'style="display:none;"' : 'style="display:;"'} class="addressValidator">
	                    <label for="county" class="hide"></label>
	                    <input type="hidden" name="employeeDetailsApplication[${cnt.index}].id" id="id_${cnt.index}" value="${empDtls.id}" />
	                    <input type="hidden" name="employeeDetailsApplication[${cnt.index}].employeeMemberId" id="employeeMemberId_${cnt.index}" value="${empDtls.employeeMemberId}" />
	                    <input type="hidden" name="employeeDetailsApplication[${cnt.index}].type" id="type_${cnt.index}" value="${empDtls.type}"/>
	                    <input type="hidden" value="0.00" id="lat_${cnt.index}" name="lat" />
	                    <input type="hidden" value="0.00" id="lon_${cnt.index}" name="lon" />
	                    <input type="hidden" value="" id="rdi_${cnt.index}" name="rdi" />
	                    <input type="hidden" value="${empDtls.location.id}" id="locationId_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].location.id" />
	                    <input type="hidden" value="" id="wardDocument_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].wardDocument" class = "wardDocument"  >
	                      <div class="control-group">                     
	                          <label class="control-label" for="address1"><spring:message  code="label.homeAddress1"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"> </label>
	                            <div class="controls">
	                              <input type="text" class="address_county" value="${empDtls.location.address1}" placeholder="Address Line 1" name="employeeDetailsApplication[${cnt.index}].location.address1" id="address1_${cnt.index}">
	                              <span id="address1_${cnt.index}_error"></span>
	                              
	                          </div>
	                      </div>
	                      <div class="control-group">
	                        <label class="control-label" for="address2"><spring:message  code="label.homeAddress2"/> </label>
	                        <div class="controls">
	                          <input type="text" value="${empDtls.location.address2}" placeholder="Address Line 2" name="employeeDetailsApplication[${cnt.index}].location.address2" id="address2_${cnt.index}" tabindex="0">
	                          <span id="address2_${cnt.index}_error"></span>
	                        </div>
	                      </div>
	                      <div class="control-group">
	                        <label class="control-label" for="city"><spring:message  code='label.emplCity'/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
	                        <div class="controls">
	                          <input type="text" value="${empDtls.location.city}" placeholder="City" name="employeeDetailsApplication[${cnt.index}].location.city" id="city_${cnt.index}" tabindex="0" aria-required="true">
	                          <span id="city_${cnt.index}_error"></span>
	                        </div>
	                      </div>
	                      <div class="control-group">
	                        <label class="control-label" for="state"><spring:message  code='label.emplState'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
	                        <div class="controls">
	                          <select id="state_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].location.state" class="input-medium" tabindex="0" aria-required="true">
									 <option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
								<option id="${state.code}_${cnt.index}" <c:if test="${state.code == empDtls.location.state}"> SELECTED </c:if>
									value="${state.code}">${state.name}</option>
								</c:forEach>
	  						 </select>
	              			 <span id="state_${cnt.index}_error"></span>
	              
	                        </div>
	                      </div>
	                      <div class="control-group">
	                        <label class="control-label"  for="zip"><spring:message  code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
	                        <div class="controls">
	                            <input type="text" value="${empDtls.location.zip}" maxlength="5" id="zip_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].location.zip" placeholder="Zip Code" class="input-small zipCode" tabindex="0" onblur="validateAddress('${cnt.index}');" >
	                            <span id="zip_${cnt.index}_error"></span>
	                        </div>
	                      </div>
	                      <div class="control-group">
								<label class="control-label" for="county" class="control-label required"><spring:message  code='label.county'/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/> </label>
								<div class="controls">
									<select class="input-large" name="employeeDetailsApplication[${cnt.index}].location.county"  id="county_${cnt.index}">
									</select>
									<div id="county_${cnt.index}_error"></div>		
								</div>
							</div>
						 </div><!--END ADDRESS DIV   -->
						<div class="control-group">
	                        <label class="control-label" for="gender"><spring:message  code='label.gender'/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
	                        <div class="controls">
	                          <%-- <label class="radio inline"><input aria-required="true" type="radio" id="genderMale_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].gender" ${fn:contains(empDtls.gender, 'MALE') ? 'checked="checked"' : ''} value="MALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.male"/> </label>
		                      <label class="radio inline"><input aria-required="true" type="radio" id="genderFemale_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].gender" ${fn:contains(empDtls.gender, 'FEMALE') ? 'checked="checked"' : ''}  value="FEMALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.female"/> </label> --%>
		                      <label class="radio inline"><input aria-required="true" type="radio" id="gender_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].gender" ${fn:contains(empDtls.gender, 'MALE') ? 'checked="checked"' : ''} value="MALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.male"/> </label>
		                      <label class="radio inline"><input aria-required="true" type="radio" id="gender_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].gender" ${fn:contains(empDtls.gender, 'FEMALE') ? 'checked="checked"' : ''}  value="FEMALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.female"/> </label>
	                         <%-- <span id="genderMale_${cnt.index}_error"></span> 
		                    <span id="genderFemale_${cnt.index}_error"></span> --%>
		                    <span id="gender_${cnt.index}_error"></span> 
		                    
	                        </div>
	                     </div>
						
	                    <dl class="dl-horizontal">
			            <dt>Date of Birth</dt>
			            <dd id="d-o-b_${cnt.index}">
			            	<c:choose>
			               	  	<c:when test="${empDtls.dob == null || empDtls.dob ==''}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
									<fmt:formatDate pattern="MM/dd/yyyy" value="${empDtls.dob}" var="dob" /> 
			               			<strong>${dob}</strong>
			               		</c:otherwise>
							</c:choose>
			            </dd>
			            <dt>Tax ID Number</dt>
			            <dd id="tax-id_${cnt.index}">
							<c:choose>
			               	  	<c:when test="${empDtls.ssn == null || empDtls.ssn =='' || empDtls.ssn == '--'}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
									<c:set var="ssnParts" value="${fn:split(empDtls.ssn,'-')}" />
									***-**-${ssnParts[2]}
			               		</c:otherwise>
							</c:choose>
						</dd>
			          </dl>
	          
	          		<div class="control-group">
		               <label class="control-label" for="email">Email Address</label>
		               <div class="controls">
		                       <input type="text" value="${empDtls.email}" id="email_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].email" class="input-xlarge" tabindex="0" aria-required="true">
		                       <span id="email_${cnt.index}_error"></span>
		               </div>
	               </div>
	               <div class="control-group">
	                   <label class="control-label" for="phone">Phone Number</label>
	                   <div class="controls">
	                   <c:choose>
								<c:when
									test="${empDtls.contactNumber != null && empDtls.contactNumber != ''}">
									<c:set var="phone" value="${empDtls.contactNumber}" />
									<c:set var="phoneParts" value="${fn:split(phone,'-')}" />
	
									<c:set var="phone1" value="${phoneParts[0]}" />
									<c:set var="phone2" value="${phoneParts[1]}" />
									<c:set var="phone3" value="${phoneParts[2]}" />
								</c:when>
							</c:choose>
	                   	   <input type="text" class="span2 inline" value="${phone1}" name="phone1_${cnt.index}" id="phone1_${cnt.index}" maxlength="3" onkeyup="shiftbox(this, 'phone2_${cnt.index}')">
	                        <input type="text" class="span2 inline" value="${phone2}" name="phone2_${cnt.index}" id="phone2_${cnt.index}" maxlength="3" onkeyup="shiftbox(this, 'phone3_${cnt.index}')">
	                        <input type="text" class="span2 inline" value="${phone3}" name="phone3_${cnt.index}" id="phone3_${cnt.index}" maxlength="4">
	                        	
	                       <input type="hidden"  value="${empDtls.contactNumber}" id="contactNumber_${cnt.index}" name="employeeDetailsApplication[${cnt.index}].contactNumber" class="input-xlarge" tabindex="0" aria-required="true" maxlength="10">
	                       <span id="phone3_${cnt.index}_error"></span>
	                       
	                   </div>
	               </div>
	               <dl class="dl-horizontal">
	                 <dt>Tobacco User</dt>
			         <dd id="Tobacco-user_${cnt.index}">
			         	<c:choose>
		               	  	<c:when test="${empDtls.smoker == null}">
								<weak>N/A</weak>
							</c:when>
							<c:otherwise>
		               			<strong>${fn:toUpperCase(fn:substring(empDtls.smoker, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.smoker, 1, -1))}</strong>
		               		</c:otherwise>
						</c:choose>
			         </dd>
			         <dt><spring:message  code="label.tobaccoCessationPrg"/></dt>
					<dd  id="tobaccoCessationPrg_${cnt.index}">
						<c:choose>
						    <c:when test="${empDtls.tobaccoCessPrg == null || empDtls.tobaccoCessPrg == ''}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDtls.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.tobaccoCessPrg, 1, -1))}</strong>
						    </c:otherwise>
					    </c:choose>
					</dd>
			         <dt>Member of Federally Recognized Tribe</dt>
			         <dd id="fedral-tribe_${cnt.index}">
			         	<c:choose>
						    <c:when test="${empDtls.nativeAmr == null}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDtls.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.nativeAmr, 1, -1))}</strong>
						    </c:otherwise>
						</c:choose>
			         </dd>
			        </dl> 
	        
	                <div class="control-group" id="uploadWardDoc_${cnt.index}" style='display:none;'>
	 					<label class="control-label" for="fileupload_${cnt.index}"><spring:message  code="label.uploadDoc"/></label>
	                    <div class="span4">
	                    	<div id="file_upload_container_${cnt.index}">
	                    	
								<input type="file" class="input-file" id="fileupload_${cnt.index}" onchange="setAjaxPlugin(${cnt.index})" value=""  name="wardDoc" />&nbsp;	
							
							<div id="fileupload_error_${cnt.index}" class="error" style="display:none;">File could not be uploaded. Selected file name already exists on the server.</div>
							<div id="uploaddiv_${cnt.index}" style="display:none;">Uploading..... Please wait</div>
	       					<div id="deletediv_${cnt.index}" style="display:none;">Deleting..... Please wait</div>
	       					<span id="wardDocument_${cnt.index}_error"></span> 
					    <!-- The table listing the files available for attaching/removing -->
	       					<div id="" class="files" >
	       						<table id="upload_files_container_${cnt.index}"></table>
	       					</div>
	                    	</div>
	                    </div>
	               </div>
	
	        
		        <script type="text/javascript">
		        $(document).ready(function(){
			        var cntr = "${cnt.index}";
			        var zip = '${empDtls.location.zip}';
			        var county = '${empDtls.location.county}';
			        var isEmployeeAddress = '${empDtls.isEmployeeAddress}';
			        if(isEmployeeAddress=='N'){
			        	getCountyList(cntr,zip, county);
			        }
			         	
			        jQuery("#firstName_"+cntr).rules("add", { required : true,namecheck : true, messages: {required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>", namecheck: "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>" } } );
			        jQuery("#lastName_"+cntr).rules("add", { required : true,namecheck : true, messages: {required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>", namecheck: "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>" } } );
			        jQuery("#address1_"+cntr).rules("add", { required: true,address1Check:true, messages: {required: "<span><em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>",address1Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>" } } );
			        jQuery("#address2_"+cntr).rules("add", { address2Check:true,address2Match: { cntr:cntr}, messages: {address2Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>", address2Match: "<span> <em class='excl'>!</em><spring:message code='label.validateaddressline12' javaScriptEscape='true'/></span>" } } );
			        jQuery("#city_"+cntr).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>" } } );
			        jQuery("#state_"+cntr).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>" } } );
			        jQuery("#zip_"+cntr).rules("add", { required: true,zipcheck : true, messages: {required: "<span><em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>" } } );
			        jQuery("#county_"+cntr).rules("add", { required: true, messages: {required: "<span><em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>" } } );
			        jQuery("#email_"+cntr).rules("add", { email : true, messages: {email : "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>" } } );
			        jQuery("#phone3_"+cntr).rules("add", { phonecheck:{ cntr:cntr}, messages: {phonecheck: "<span><em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>" } } );
			        jQuery("#gender_"+cntr).rules("add", { required : true, messages: {required : "<span><em class='excl'>!</em><spring:message code='label.validategender' javaScriptEscape='true'/></span>" } } );
			        jQuery("#wardDocument_"+cntr).rules("add", { warddoccheck : true, messages: {warddoccheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefile' javaScriptEscape='true'/></span>" } } );
		        });
			        </script>
	        </c:forEach> 
	        </div><!-- .row-fluid -->
	        </div>
	    </form>
	    </div><!--/rightpanel-->
          
      
      </div><!--/row-fluid-->      
  </div>  <!--/main .gutter10-->


<c:if test="${isEnrollmentEnded }">
<jsp:include page="../terminateemployercoverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
	<jsp:param name="nativeAmr" value="${nativeAmr}" />
</jsp:include>  
</c:if>
<c:if test="${!isEnrollmentEnded }">
<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>
</c:if>

<script>
jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}
	return true;
});
jQuery.validator.addMethod("address1Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
	if(!regex.test($("#"+elementId).val()) ){
  		return false;	
  	}
	return true;
});

jQuery.validator.addMethod("address2Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
  	if($("#"+elementId).val()!=""){
		if(!regex.test($("#"+elementId).val()) ){
	  		return false;	
	  	}
	}
	return true;
});
jQuery.validator.addMethod("address2Match", function(value, element, options) {
	var cntr = options.cntr;
	elementId = element.id;	  	
  	if($.trim($("#"+elementId).val())!="" && $.trim($("#address1_"+cntr).val())!=""){	  		  	
  		if($("#"+elementId).val() == $("#address1_"+cntr).val()){
	  		return false;	
		}
  	}
	return true;
});	

jQuery.validator.addMethod("namecheck", function(value, element, param) {
  	if(namecheck(value) == false){
	  	return false;	
	}
	return true;
});


   jQuery.validator.addMethod("warddoccheck", function(value, element,param ){
	elementId = element.id;
	var index = elementId.split("_");
	var fileName =  $("#"+elementId).val();
	
	if(wardDocumentRequired == "YES" && $('#type_' +index[1]).val()== "WARD" && (fileName === undefined || fileName == 'none' || fileName == null || fileName == "" ) ){
		return false;
	}
	return true;
});   

jQuery.validator.addMethod("phonecheck",function(value, element, options) {
	var cntr = options.cntr;
	phone1 = $("#phone1_"+cntr).val();
	phone2 = $("#phone2_" +cntr).val();
	phone3 = $("#phone3_" +cntr).val();

	if((phone1 != "" || phone2 != "" || phone3 != ""))
	{
	if ((isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) || (phone1.length != 3 || phone2.length != 3 || phone3.length != 4)) 
	{
		return false;
	}
	}
	var phone = $("#phone1_"+cntr).val() + "-" + $("#phone2_"+cntr).val() + "-" + $("#phone3_"+cntr).val();
	
	$("#contactNumber_"+cntr).val(phone);
	return true;
});

$(".inactive").click(function(e) {
	e.preventDefault();
});

$(document).ready(function(){
	$('.info').tooltip();
});

function shiftbox(element, nextelement) {
	maxlength = parseInt(element.getAttribute('maxlength'));
	if (element.value.length == maxlength) {
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}

function showHideAddressDiv(cntr, val,id){
	if(val=='Y'){
		$('#addressDiv_'+cntr).hide();
		//$('#locationId_'+cntr).val(locationJSON['id']);
	}
	else
	{
		$('#address1_'+cntr).val('');
		$('#address2_'+cntr).val('');
		$('#city_'+cntr).val('');
		$('#zip_'+cntr).val('');
		$('#state_'+cntr).prop('selectedIndex', 0);
		$('#county_'+cntr).prop('selectedIndex', 0);
		if($('#locationId_'+cntr).val()==locationJSON['id']){
			$('#locationId_'+cntr).val(0);
		}
		else if($('#locationId_'+cntr).val()>0){ //if prev set location id is not employee home locatiuon id
			
			if(dependentCount < empDetailsJSON.length){
		    	 dependentCount = empDetailsJSON.length;
		     }
			  for(var i=0;i<dependentCount;i++){
				  obj = empDetailsJSON[i];
				  if(id==obj['id'] && obj['isEmployeeAddress']=='N'){
					  $('#locationId_'+cntr).val(obj['locationid']);
					  $('#address1_'+cntr).val(obj['address1']);
					  $('#address2_'+cntr).val(obj['address2']);
					  $('#city_'+cntr).val(obj['city']);
					  $('#state_'+cntr).val(obj['state']);
					  $('#zip_'+cntr).val(obj['zip']);
					  getCountyList(cntr, obj['zip'], obj['county']);
			      }//if
			  }//for	  
		}//if
		
		$('#addressDiv_'+cntr).show();
	}//else
}
</script>