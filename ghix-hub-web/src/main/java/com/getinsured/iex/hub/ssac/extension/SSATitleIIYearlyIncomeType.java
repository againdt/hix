
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A Data Type for TitleII yearly income details
 *             
 * 
 * <p>Java class for SSATitleIIYearlyIncomeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSATitleIIYearlyIncomeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}TitleIIRequestedYearInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSATitleIIYearlyIncomeType", propOrder = {
    "titleIIRequestedYearInformation"
})
public class SSATitleIIYearlyIncomeType
    extends ComplexObjectType
{

    @XmlElement(name = "TitleIIRequestedYearInformation")
    protected SSATitleIIRequestedYearInformationType titleIIRequestedYearInformation;

    /**
     * Gets the value of the titleIIRequestedYearInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIRequestedYearInformationType }
     *     
     */
    public SSATitleIIRequestedYearInformationType getTitleIIRequestedYearInformation() {
        return titleIIRequestedYearInformation;
    }

    /**
     * Sets the value of the titleIIRequestedYearInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIRequestedYearInformationType }
     *     
     */
    public void setTitleIIRequestedYearInformation(SSATitleIIRequestedYearInformationType value) {
        this.titleIIRequestedYearInformation = value;
    }

}
