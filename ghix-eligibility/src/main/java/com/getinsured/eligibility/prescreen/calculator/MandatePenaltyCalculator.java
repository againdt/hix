/**
 * 
 */
package com.getinsured.eligibility.prescreen.calculator;

import java.text.ParseException;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.eligibility.estimator.mini.util.MiniEstimatorUtil;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest;

/**
 * Mandate Penalty calculator for the mini-estimator
 * JIRA: HIX-12786
 * 
 * Updates to the mandate penalty calculator are done as per JIRA: HIX-16293
 * 
 * @author Nikhil Talreja
 * @since 10 July, 2013
 *
 */
@Component
public class MandatePenaltyCalculator{
	
	private static final Logger LOGGER = Logger.getLogger(MandatePenaltyCalculator.class);
	
	//Penalties for 2013, 2014
	private static final double PENALTY_FOR_ADULT_2014 = 95.0;
	private static final double PENALTY_FOR_CHILD_2014 = 47.50;
	private static final double PERCENTAGE_PENALTY_2014 = 0.01;
	private static final double MAX_PENALTY_FOR_FAMILY_2014 = 3 * PENALTY_FOR_ADULT_2014;
	
	//Penalties for 2015
	private static final double PENALTY_FOR_ADULT_2015 = 325.0;
	private static final double PENALTY_FOR_CHILD_2015 = 162.50;
	private static final double PERCENTAGE_PENALTY_2015 = 0.02;
	private static final double MAX_PENALTY_FOR_FAMILY_2015 = 3 * PENALTY_FOR_ADULT_2015;
	
	//Penalties for 2016 and beyond
	private static final double PENALTY_FOR_ADULT_2016 = 695.0;
	private static final double PENALTY_FOR_CHILD_2016 = 347.50;
	private static final double PERCENTAGE_PENALTY_2016 = 0.025;
	private static final double MAX_PENALTY_FOR_FAMILY_2016 = 3 * PENALTY_FOR_ADULT_2016;
	
	//Filing Threshold
	private static final double FILING_THRESHOLD_SINGLE = 10150.0;
	private static final double FILING_THRESHOLD_HOH = 13050.0;
	private static final double FILING_THRESHOLD_JOINT = 20300.0;
	
	//Fields used to compute total penalty
	private double penaltyForAdult = 0.0;
	private double penaltyForChild = 0.0;
	private double maxPenaltyForFamily = 0.0;
	private double percentagePenalty = 0.0;
	
	private static final int ADULT_AGE = 18;
	private static final int CURRENT_YEAR = TSCalendar.getInstance().get(Calendar.YEAR);
	private static final int YEAR_2014 = 2014;
	private static final int YEAR_2015 = 2015;
	private static final int YEAR_2016 = 2016;
	private static final int JOINT_FILERS = 2;
	
	private MiniEstimatorUtil util = new MiniEstimatorUtil();
	
	/**
	 * Calculated mandate penalty for a household as per following rules:
	 * 1. A flat dollar amount (set by the ACA) on each taxpayer and dependent: 
	 * <ul>
	 *	<li> For 2013,2014: $95 for an adult, $47.50 for dependents under the age of 18, capped at three times the individual penalty (=$285).</li> 
	 *	<li> For 2015: $325 for an adult, $162.50 for dependents under 18 </li>
	 *	<li> For 2016 and beyond: $695 for an adult, $347.50 for dependents under 18 </li>
	 *	<li> Limited for a household to three times the fixed dollar amount for adults or </li>
	 * </ul>
	 * 2. A percentage of household income (MAGI) that exceeds the income tax filing threshold 
	 * <ul>	
	 *	<li> For 2013,2014: 1.0% </li>
	 *	<li> For 2015: 2.0% </li>
	 *	<li> For 2016 and beyond: 2.5% </li>
	 * </ul>	
	 * The Penalty cannot be greater than the national average premium for "Bronze" level coverage in an Exchange. 
	 * @author Nikhil Talreja
	 * @throws ParseException 
	 * @since 10 July, 2013
	 *
	*/
	public double calculateMandatePenalty(MiniEstimatorRequest request) throws ParseException{
		
		LOGGER.debug("Calculating Mandate Penalty for household");
		
		setPenaltiesForCurrentYear();
		
		double totalPenalty = 0.0;
		
		if(CollectionUtils.isEmpty(request.getMembers())){
			return totalPenalty;
		}
		else{
			int age = 0;
			int noOfAdults = 0;
			int noOfApplicants = 0;
			for(Member member : request.getMembers()){
				
				//if DOB is empty, do not consider member for calculating penalty
				if(StringUtils.isBlank(member.getDateOfBirth())){
					continue;
				}
				else{
					noOfApplicants++;
					age = util.getAgeFromDob(member.getDateOfBirth(), request);
					if(age > ADULT_AGE){
						totalPenalty += penaltyForAdult;
						noOfAdults++;
					}
					else{
						totalPenalty += penaltyForChild;
					}
				}
			}
				
			//Checking max penalty condition
			if(totalPenalty > maxPenaltyForFamily){
				totalPenalty = maxPenaltyForFamily;
			}
			
			//Calculate filing threshold
			double filingThreshold = calculateFilingThreshold(noOfAdults,noOfApplicants);
			double hhIncome = request.getHhIncome();
			//Checking HH Income condition
			double penaltyBasedOnIncome = percentagePenalty * (hhIncome - filingThreshold);
			if( penaltyBasedOnIncome > totalPenalty){
				totalPenalty = penaltyBasedOnIncome;
			}
			
		}
		
		LOGGER.debug("Mandate Penalty for household is " + totalPenalty);
		return totalPenalty;
	}
	
	/**
	 * Sets the values for penalty types for current year
	 * 
	 * @author Nikhil Talreja
	 * @since 13 September, 2013
	 *
	*/
	public void setPenaltiesForCurrentYear(){
		
		if(CURRENT_YEAR <= YEAR_2014){
			penaltyForAdult = PENALTY_FOR_ADULT_2014;
			penaltyForChild = PENALTY_FOR_CHILD_2014;
			maxPenaltyForFamily = MAX_PENALTY_FOR_FAMILY_2014;
			percentagePenalty = PERCENTAGE_PENALTY_2014;
		}
		else if(CURRENT_YEAR == YEAR_2015){
			penaltyForAdult = PENALTY_FOR_ADULT_2015;
			penaltyForChild = PENALTY_FOR_CHILD_2015;
			maxPenaltyForFamily = MAX_PENALTY_FOR_FAMILY_2015;
			percentagePenalty = PERCENTAGE_PENALTY_2015;
		}
		else if(CURRENT_YEAR >= YEAR_2016){
			penaltyForAdult = PENALTY_FOR_ADULT_2016;
			penaltyForChild = PENALTY_FOR_CHILD_2016;
			maxPenaltyForFamily = MAX_PENALTY_FOR_FAMILY_2016;
			percentagePenalty = PERCENTAGE_PENALTY_2016;
		}
	}
	
	/**
	 * 
	 * Calculate tax filing threshold : 
	 *  1) In the case of >= 2 adults, we treat them as Married Filling Jointly 
	 *	2) In the case of < 2 adults: 
	 *		2.1) No dependents = Single 
	 *		2.2) Dependents = Head of Household
	 * 
	 * Filing Thresholds:
	 * Single : $10,000
	 * Head of household : $12,850
	 * Married Filling jointly : $20,000
	 *
	 * Filing threshold should be subtracted from MAGI to determine penalty
	 * 
	 * @author Nikhil Talreja
	 * @since  13 September, 2013
	 * 
	 */
	private double calculateFilingThreshold(int noOfAdults, int noOfApplicants){
		
		double filingThreshold = 0.0;
		
		//No. of adults is 2 or more. Should be considered as joint filers.
		if(noOfAdults >= JOINT_FILERS){
			filingThreshold = FILING_THRESHOLD_JOINT;
		}
		//No. of adults is 1 and is the only applicant (no dependents). 
		else if (noOfAdults == 1 && noOfApplicants == 1){
			filingThreshold = FILING_THRESHOLD_SINGLE;
		}
		//No. of adults is 1 and there is at least one child
		else if(noOfAdults == 1 && noOfApplicants > 1){
			filingThreshold = FILING_THRESHOLD_HOH;
		}
		
		LOGGER.debug("Filing Threshold : " + filingThreshold);
		return filingThreshold;
	}
	
	/**
	 * Penalty calculator for admin page
	 * @author Nikhil Talreja
	 * @since 10 July, 2013
	 *
	 */
	public double calculateMandatePenalty(int year, int adults, int children, double hhIncome){
		
		setPenaltiesForCurrentYear();
		double penalty = (adults * penaltyForAdult) + (children * penaltyForChild);
		
		//Checking max penalty condition
		if(penalty > maxPenaltyForFamily){
			penalty = maxPenaltyForFamily;
		}
		
		LOGGER.debug("Penalty for adult : " + penaltyForAdult);
		LOGGER.debug("Penalty for child : " + penaltyForChild);
		LOGGER.debug("Total penalty based on houshold : " + penalty);
		LOGGER.debug("Cap on total Penalty for family based on household : " + maxPenaltyForFamily);
		LOGGER.debug("Household income : " + hhIncome);
		
		//Calculate filing threshold
		double filingThreshold = calculateFilingThreshold(adults,adults+children);
		
		//Checking HH Income condition
		if((percentagePenalty * (hhIncome - filingThreshold)) > penalty){
			penalty = (percentagePenalty * (hhIncome - filingThreshold));
		}
		
		LOGGER.debug("Final Mandate Penalty : " + penalty);
		
		return penalty;
		
	}
	
}
