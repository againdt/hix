package com.getinsured.ahbx.util;


public class GIException extends Exception {
	
	/**
	 * 
	 */

	private int errorCode;
	private String errorMsg;
	private String errorSeverity;
	
	public enum GhixErrors {
		/**
		 * 101, "Coverage Date not valid."
		 */
		INVALID_COVERAGE_DATE(101, "Coverage Date not valid."),
		/**
		 * 102, "Date of Birth not valid."
		 */
		INVALID_BIRTH_DATE(102, "Date of Birth not valid."),
		/**
		 * 103, "Zip Code is either missing or invalid."
		 */
		INVALID_ZIPCODE(103, "Zip Code is either missing or invalid."),
		/**
		 * 104, "Zip Code is either missing or invalid."
		 */
		INVALID_TOBACCO_VALUE(104, "Specified tobacco value is invalid."),
		/**
		 * 105, "Zip Code is either missing or invalid."
		 */
		MEMBER_SELF_NOT_SPECIFIED(105, "Member of type Self not speicified. Can not process the request."),
		/**
		 * 106, "Zip Code is either missing or invalid."
		 */
		INVALID_CHILD_AGE(106, "Child Age can not be higher than self/parent.Can not process the request."),
		/**
		 * 107, "Zip Code is either missing or invalid."
		 */
		INVALID_COUNTY(107, "Invalid County Specified.Can not process the request."),
		/**
		 * 107, "Zip Code is either missing or invalid."
		 */
		INVALID_ZIP(108, "Invalid Zip Specified.Can not process the request."),
		/**
		 * 107, "Zip Code is either missing or invalid."
		 */
		INVALID_AGE(109, "Age can not be negative.Can not process the request."),
		/**
		 * 201, "No Data found."
		 */
		NO_DATA_FOUND(201, "No Benchmark Plan found."),
		/**
		 * 202, "Error while processing."
		 */
		ERROR_WHILE_PROCESSING(202, "Error while processing."),
		/**
		 * 200, "Service ran successfully."
		 */
		SUCCESS(200, "Service ran successfully."),
		/**
		 * 300, "Invalid Reuqest.One othe the required field is missing in the request."
		 */
		INVALID_REQUEST(300, "Invalid Reuqest.One of the required field is missing in the request.");
		//... add more cases here ...

		private final int id;
		private final String message;

		GhixErrors(int id, String message) {
			this.id = id;
			this.message = message;
		}

		public int getId() { return id; }
		public String getMessage() { return message; }
	}
	
	public GIException() {
		super();
	}
	public GIException(String message, Throwable cause) {
		super(message, cause);
	}
	public GIException(String message) {
		super(message);
	}
	public GIException(Throwable cause) {
		super(cause);
	}
	
	public GIException(int errorCode, String errorMsg, String errorSeverity) {
		super(errorMsg);
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		this.errorSeverity = errorSeverity;
	}
	
	public GIException(GhixErrors ghixError) 
	{
		this.errorCode = ghixError.getId();
		this.errorMsg = ghixError.getMessage();
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getErrorSeverity() {
		return errorSeverity;
	}
	public void setErrorSeverity(String errorSeverity) {
		this.errorSeverity = errorSeverity;
	}
	
}
