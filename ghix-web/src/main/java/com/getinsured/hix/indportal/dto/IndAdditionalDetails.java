package com.getinsured.hix.indportal.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class IndAdditionalDetails {
    private Boolean householdExempt;
    private Map<String, List<IndPortalApplicantDetails>> applicantDetails;
    private Boolean isSep;
    //HIX-63743 Change plan option for native Americans
    private boolean isChangePlanFlow;
    //HIX-67563 - Change Plan and Enroll for SEP
    private boolean isSEPPlanFlow;

    //HIX-72082
    private Date coverageStartDate;
    private boolean coverageYearOver;

    private String errCode;
    private Group group;
    private String applicationStatus;

    public Boolean getHouseholdExempt() {
        return householdExempt;
    }

    public void setHouseholdExempt(Boolean householdExempt) {
        this.householdExempt = householdExempt;
    }

    public Map<String, List<IndPortalApplicantDetails>> getApplicantDetails() {
        return applicantDetails;
    }

    public void setApplicantDetails(Map<String, List<IndPortalApplicantDetails>> applicantDetails) {
        this.applicantDetails = applicantDetails;
    }

    public Boolean getIsSep() {
        return isSep;
    }

    public void setIsSep(Boolean isSep) {
        this.isSep = isSep;
    }
    public boolean isChangePlanFlow() {
        return isChangePlanFlow;
    }

    public void setIsChangePlanFlow(boolean isChangePlanFlow) {
        this.isChangePlanFlow = isChangePlanFlow;
    }

    public boolean isSEPPlanFlow() {
        return isSEPPlanFlow;
    }

    public void setIsSEPPlanFlow(boolean isSEPPlanFlow) {
        this.isSEPPlanFlow = isSEPPlanFlow;
    }

    public Date getCoverageStartDate() {
        return coverageStartDate;
    }

    public void setCoverageStartDate(Date coverageStartDate) {
        this.coverageStartDate = coverageStartDate;
    }

    public boolean getCoverageYearOver() {
        return coverageYearOver;
    }

    public void setCoverageYearOver(boolean coverageYearOver) {
        this.coverageYearOver = coverageYearOver;
    }
    /**
     * @return the errCode
     */
    public String getErrCode() {
        return errCode;
    }
    /**
     * @param errCode the errCode to set
     */
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IndAdditionalDetails [householdExempt=");
        builder.append(householdExempt);
        builder.append(", applicantDetails=");
        builder.append(applicantDetails);
        builder.append("]");
        return builder.toString();
    }
}
