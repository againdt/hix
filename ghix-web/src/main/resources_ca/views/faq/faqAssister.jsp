<%@page session="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<div class="row-fluid">
    	
		<h1 class="gutter10"><a name="skip"></a><spring:message code="label.FAQ.Assister.FAQ" htmlEscape="false"/></h1>
 		<%-- <small class="gutter10"><a href="<c:url value='/faqhome'/>" class="pull-right"><spring:message code="label.faq.faqHome" htmlEscape="false"/></a></small> --%>
    </div><!--  end of row-fluid -->

    <div class="row-fluid">
    	<div class="span3" id="sidebar">		
			<ul class="nav nav-list" id="nav">
				<li><a href="#entity1"><spring:message code="label.FAQ.Assister.entity1" htmlEscape="false"/></a></li>
				<li><a href="#entity2"><spring:message code="label.FAQ.Assister.entity2" htmlEscape="false"/></a></li>
				<li><a href="#entity3"><spring:message code="label.FAQ.Assister.entity3" htmlEscape="false"/></a></li>
				<li><a href="#entity4"><spring:message code="label.FAQ.Assister.entity4" htmlEscape="false"/></a></li>
				
			</ul>				 
	</div>
    	
       			<div class="span9 " id="rightpanel">
       				<div class="entityFaq1" id="entity1">
						<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Assister.entity1" htmlEscape="false"/></h4>
					<p><spring:message code="label.FAQ.Assister.entity1.understandRole1" htmlEscape="false"/></p>
						
						<ul>
							<li><spring:message code="label.FAQ.Assister.entity1.understandRole1.setp1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Assister.entity1.understandRole1.setp2" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Assister.entity1.understandRole1.setp3" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Assister.entity1.understandRole1.setp4" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Assister.entity1.understandRole1.setp5" htmlEscape="false"/></li>
							
						</ul>
						<p><spring:message code="label.FAQ.Assister.entity1.understandRole2" htmlEscape="false"/></p>

						</div>
						<!--end of .entityFaq1-->
						
						
						<div class="entityFaq2" id="entity2">
							<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Assister.entity2" htmlEscape="false"/>
						
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-entity2"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity2" href="#entity3">Next Question 2 <i class=" icon-chevron-right"></i></button> </div>
							 -->
							 
							 <a id="role-top" href="#" class="pull-right"><spring:message code="label.faq.backToTop" htmlEscape="false"/></a>
							</h4>
							<p><spring:message code="label.FAQ.Assister.entity2.applyCounselor1" htmlEscape="false"/></p>

							<p><spring:message code="label.FAQ.Assister.entity2.applyCounselor2" htmlEscape="false"/></p>
							
						</div>
						<!--/.entityFaq2-->
						
						<div class="entityFaq3" id="entity3">
							<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Assister.entity3" htmlEscape="false"/>
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							<a id="role-top" href="#" class="pull-right"><spring:message code="label.faq.backToTop" htmlEscape="false"/></a>
							</h4>
							
							<p><spring:message code="label.FAQ.Assister.entity3.manageProfile1" htmlEscape="false"/></p>

							<p><spring:message code="label.FAQ.Assister.entity3.manageProfile2" htmlEscape="false"/></p>
							
							<ol>
								<li><spring:message code="label.FAQ.Assister.entity3.manageProfile2.setp1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.Assister.entity3.manageProfile2.setp2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.Assister.entity3.manageProfile2.setp3" htmlEscape="false"/></li>
							</ol>
						</div>
						<!--/.entityFaq3-->
								
										
						<div class="entityFaq4" id="entity4">
							<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Assister.entity4" htmlEscape="false"/>
							<!-- 
							<div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							
							<a id="role-top" href="#" class="pull-right"><spring:message code="label.faq.backToTop" htmlEscape="false"/></a>
							</h4>
							
							<p><spring:message code="label.FAQ.Assister.entity4.manageIndividualAccounts1" htmlEscape="false"/></p>
							
							
							<div class="accordion" id="role-accordian2">
							  <div class="accordion-group">
							    <div class="accordion-heading">
							      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsOne">
							      <spring:message code="label.FAQ.Assister.entity4.viewList" htmlEscape="false"/>
							      </a>
							    </div>
							    <div id="counselorsOne" class="accordion-body collapse">
							      <div class="accordion-inner">
										<p><spring:message code="label.FAQ.Assister.entity4.viewList1" htmlEscape="false"/></p>
										
										<ol>
											<li><spring:message code="label.FAQ.Assister.entity4.viewList1.setp1" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.viewList1.setp2" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.viewList1.setp3" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.viewList1.setp4" htmlEscape="false"/></li>
											
										</ol>
							      </div>
							    </div>
							  </div>
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsTwo">
								        <spring:message code="label.FAQ.Assister.entity4.findIndividual" htmlEscape="false"/>
								      </a>
								    </div>
								    <div id="counselorsTwo" class="accordion-body collapse">
								      <div class="accordion-inner">
											<p><spring:message code="label.FAQ.Assister.entity4.findIndividual1" htmlEscape="false"/></p>
											
											<ol>
											<li><spring:message code="label.FAQ.Assister.entity4.findIndividual1.setp1" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.findIndividual1.setp2" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.findIndividual1.setp3" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.findIndividual1.setp4" htmlEscape="false"/></li>
											</ol>
								      </div>
								    </div>
								  </div>
								  
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsThree">
								       <spring:message code="label.FAQ.Assister.entity4.changeStatus" htmlEscape="false"/>
								      </a>
								    </div>
								    <div id="counselorsThree" class="accordion-body collapse">
								      <div class="accordion-inner">
								       <p><spring:message code="label.FAQ.Assister.entity4.changeStatus1" htmlEscape="false"/></p>
								       
											<p><spring:message code="label.FAQ.Assister.entity4.changeStatus2" htmlEscape="false"/></p>
											
												<ol>
													<li><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step1" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step2"    htmlEscape="false"/>
													<p><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step2.note" htmlEscape="false"/></p>
													</li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step3" htmlEscape="false"/>
													<p>
													<spring:message code="label.FAQ.Assister.entity4.changeStatus2.step3.ifPending" htmlEscape="false"/></p>
													<ol>
														<li><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step3.ifPending.step1" htmlEscape="false"/></li>
														<li><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step3.ifPending.step2" htmlEscape="false"/>
														<p><spring:message code="label.FAQ.Assister.entity4.changeStatus2.step3.ifPending.step2.p" htmlEscape="false"/></p></li>
													</ol>
												
													</li>
													
												</ol>
								      </div>
								    </div>
								  </div>
								  
								 
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFour">
								        <spring:message code="label.FAQ.Assister.entity4.viewInfo" htmlEscape="false"/>
								      </a>
								    </div>
								    <div id="counselorsFour" class="accordion-body collapse">
								      <div class="accordion-inner">
								      	
								      	<ol>
									        <li><spring:message code="label.FAQ.Assister.entity4.viewInfo.step1" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.viewInfo.step2" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.viewInfo.step3" htmlEscape="false"/>
											<p><spring:message code="label.FAQ.Assister.entity4.viewInfo.step3.note" htmlEscape="false"/></p></li>
										</ol>
								      </div>
								    </div>
								  </div>
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFive">
								       <spring:message code="label.FAQ.Assister.entity4.changeIndividual" htmlEscape="false"/>
								      </a>
								    </div>
								    <div id="counselorsFive" class="accordion-body collapse">
								      <div class="accordion-inner">
								      <p><spring:message code="label.FAQ.Assister.entity4.changeIndividual.maintain" htmlEscape="false"/></p>
								      <ol>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.maintain.step1" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.maintain.step2" htmlEscape="false"/>
											<p><spring:message code="label.FAQ.Assister.entity4.changeIndividual.maintain.step2.note" htmlEscape="false"/></p></li>

											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.maintain.step3" htmlEscape="false"/>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.add" htmlEscape="false"/>
												<ol>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.add.step1" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.add.step2" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.add.step3" htmlEscape="false"/></li>
												</ol>
											</li>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.view" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.update" htmlEscape="false"/>
												<ol>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.update.step1" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.update.step2" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.update.step3" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.update.step4" htmlEscape="false"/></li>
												</ol>
											</li>
											<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.delete" htmlEscape="false"/>
												<ol>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.delete.step1" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.delete.step2" htmlEscape="false"/></li>
													<li><spring:message code="label.FAQ.Assister.entity4.changeIndividual.delete.step3" htmlEscape="false"/></li>
												</ol>
											</li>
											</ol>
								      </div>
								    </div>
								  </div>
								  
								</div>

						
						</div>
						<!--/.entityFaq4-->						
														
														
																						
						<a id="role-top" href="#" class="pull-right"><spring:message code="label.faq.backToTop" htmlEscape="false"/></a>
						
						
				</div>
				
				
				
    </div><!--  end of row-fluid -->
    
<script>

$(document).ready(function() {
 $('a[href*=#]:not([href=#]):not([class=accordion-toggle])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>
