(function(){
	'use strict';

	angular.module('brokerAdminApp')
	.config(routerConfig)
	.run(RoutesRootScope);


	routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routerConfig($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise('/');

		$stateProvider.state('manageAgencies', {
			url: '/manageAgencies',
			templateUrl: '/hix/resources/html/agency/admin/manageAgencies.html',
			controller: 'ManageAgenciesCtrl',
            controllerAs: 'vm',
			bindToController: true
		})
        .state('viewAdminStaffList',{
            url: '/viewAdminStaffList',
            templateUrl: '/hix/resources/html/agency/admin/manageStaff.html',
            controller: 'ViewAdminStaffCtrl',
            controllerAs: 'vm'
        })
		.state('adminstaff', {
			url: '',
			abstract: true,
			templateUrl: '/hix/resources/html/agency/staffTemplate.html',
			controller: 'adminStaffAppController',
			controllerAs: 'vm',
			bindToController: true
		})
		.state('adminstaff.addstaff', {
		  url: '/addadminstaff',
		  templateUrl: '/hix/resources/html/agency/addAdminStaff.html',
		  controller: 'AdminStaffCtrl',
		  controllerAs: 'vm',
		  params: {assistantId: undefined}
		})
		.state('adminstaff.approvalstatus', {
			url: '/adminstaffapprovalstatus',
			templateUrl: '/hix/resources/html/agency/adminStaffApprovalStatus.html',
			controller: 'AdminStaffApprovalCtrl',
			controllerAs: 'vm',
			params: {assistantId: undefined}
		})
		.state('adminstaff.status', {
		  url: '/adminstaffstatus',
		  templateUrl: '/hix/resources/html/agency/adminStaffStatus.html',
		  controller: 'AdminStaffStatusCtrl',
		  controllerAs: 'vm',
		})
		.state('agency', {
			url: '',
            abstract: true,
            templateUrl: '/hix/resources/html/agency/admin/agencyContainer.html',
            controller: 'AgencyCtrl',
            controllerAs: 'vm',
        })
        .state('agency.agencyInformation', {
            url: '/agencyInformation',
            templateUrl: '/hix/resources/html/agency/agencyInformation.html',
            controller: 'AgencyInformationCtrl',
            controllerAs: 'vm',
			params: {agencyId: undefined}
        })
        .state('agency.locationHour', {
        	url: '/locationHour',
            templateUrl: '/hix/resources/html/agency/agencyLocation.html',
            controller: 'LocationHourCtrl',
            controllerAs: 'vm'
        })
        .state('agency.documentUpload', {
            url: '/documentUpload',
            templateUrl: '/hix/resources/html/agency/documentUpload.html',
            controller: 'DocumentUploadCtrl',
            controllerAs: 'vm'
        })
		.state('agency.certificationStatus', {
            url: '/certificationStatus',
            templateUrl: '/hix/resources/html/agency/certificationStatus.html',
            controller: 'CertificationStatusCtrl',
            controllerAs: 'vm',
            params: {agencyId: undefined}
        })
	}
	RoutesRootScope.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$timeout', '$translate','dataStorageService'];
	function RoutesRootScope ($rootScope, $state, $stateParams, $window, $timeout, $translate, dataStorageService) {
	    $rootScope.$on('$stateChangeStart',
	        function(event, toState, toParams, fromState, fromParams, $state, $stateParams) {
	            if (toState.external) {
	                event.preventDefault();
	                $window.open(toState.url, '_self');
	            }
	    });
	    $rootScope.$state = $state;
	    $rootScope.$stateParams = $stateParams;

		$state.transitionTo('manageAgencies', { reload: false, inherit: false, notify: true });
	}


})();
