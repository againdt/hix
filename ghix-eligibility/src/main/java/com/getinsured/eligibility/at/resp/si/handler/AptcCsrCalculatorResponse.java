package com.getinsured.eligibility.at.resp.si.handler;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AptcCsrCalculatorResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class AptcCsrCalculatorResponse {

	@XmlElement(name = "MaxAPTCAmount", required = true)
	private BigDecimal maxAPTCAmount;

	@XmlElement(name = "AvailableAPTCAmount", required = true)
	private BigDecimal availableAPTCAmount;

	@XmlElement(name = "CsrLevel", required = true)
	private String csrLevel;

	@XmlElement(name = "IsNativeAmerican", required = true)
	private boolean isNativeAmerican;

	@XmlElement(name = "FplAmount", required = true)
	private BigDecimal fplAmount;

	public BigDecimal getMaxAPTCAmount() {
		return maxAPTCAmount;
	}

	public void setMaxAPTCAmount(BigDecimal maxAPTCAmount) {
		this.maxAPTCAmount = maxAPTCAmount;
	}

	public BigDecimal getAvailableAPTCAmount() {
		return availableAPTCAmount;
	}

	public void setAvailableAPTCAmount(BigDecimal availableAPTCAmount) {
		this.availableAPTCAmount = availableAPTCAmount;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public boolean isNativeAmerican() {
		return isNativeAmerican;
	}

	public void setNativeAmerican(boolean isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}

	public BigDecimal getFplAmount() {
		return fplAmount;
	}

	public void setFplAmount(BigDecimal fplAmount) {
		this.fplAmount = fplAmount;
	}

}
