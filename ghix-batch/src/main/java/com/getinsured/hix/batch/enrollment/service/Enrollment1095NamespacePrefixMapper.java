package com.getinsured.hix.batch.enrollment.service;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class Enrollment1095NamespacePrefixMapper extends NamespacePrefixMapper {
	private static final Map<String, String> PREFIXES = new HashMap<String, String>();

	
	
	/*
	 * air5.0	urn:us:gov:treasury:irs:ext:aca:air:5.0
	 * irs	urn:us:gov:treasury:irs:common
	 * batchreq	urn:us:gov:treasury:irs:msg:form1095atransmissionupstreammessage
	 * batchresp	urn:us:gov:treasury:irs:msg:form1095atransmissionexchrespmessage
	 * reqack	urn:us:gov:treasury:irs:msg:form1095atransmissionexchackngmessage
	 *
	 */
	
	static{
		PREFIXES.put("urn:us:gov:treasury:irs:ext:aca:air:5.0", "air5.0");
		PREFIXES.put("urn:us:gov:treasury:irs:common", "irs");
		PREFIXES.put("urn:us:gov:treasury:irs:msg:form1095atransmissionupstreammessage", "batchreq");
		PREFIXES.put("urn:us:gov:treasury:irs:msg:form1095atransmissionexchrespmessage", "batchresp");
		PREFIXES.put("urn:us:gov:treasury:irs:msg:form1095atransmissionexchackngmessage", "reqack");
	}
	
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix != null){
			return prefix;
		}
		else{
			return suggestion;
		}
	}
}
