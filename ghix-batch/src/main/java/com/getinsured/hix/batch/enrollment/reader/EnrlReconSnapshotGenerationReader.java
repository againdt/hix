package com.getinsured.hix.batch.enrollment.reader;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.EnrlReconSnapshotGenerationData;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrlReconSnapshotGenerationReader implements ItemReader<Integer> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconSnapshotGenerationReader.class);
	String jobName;
	private ExecutionContext executionContext;
	int loopCount;
	long jobExecutionId = -1;
	private JobService jobService;
	private List<Long> issuerPolicyIds;
	private EnrlReconSnapshotGenerationData enrlReconSnapshotGenerationData;
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {

		try{
			LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
			jobExecutionId = stepExecution.getJobExecution().getId();
			ExecutionContext ec = stepExecution.getExecutionContext();
			if(ec != null){
				executionContext = stepExecution.getJobExecution().getExecutionContext();
				String partition = ec.getString("partition");
				//jobName=(String)stepExecution.getJobExecution().getJobInstance().getJobName();
				
				issuerPolicyIds=enrlReconSnapshotGenerationData.getEnrollmentIdMap().get(partition);
			}
		}catch(Exception e){
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
			throw e;
		}
		
	
	}
	
	@Override
	public Integer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		
		String batchJobStatus=null;
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
	if(executionContext != null){
			Object jobExecutionStatus = executionContext.get("jobExecutionStatus");
			String jobStatus = null;
			
			if(jobExecutionStatus != null){
				jobStatus = (String)jobExecutionStatus;
			}
			
			if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
				LOGGER.info(Thread.currentThread().getName() +  " : Terminating thread " + Thread.currentThread().getName() + " due to failure of other thread execution");
				throw new Exception("Terminating thread " + Thread.currentThread().getName() + "due to failure of other thread execution");
			}
		}
		
	if(issuerPolicyIds!=null && !issuerPolicyIds.isEmpty()){
		if(loopCount<issuerPolicyIds.size()){
			loopCount++;
		}else{
			return null;
		}
	}
		return null;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}


	public EnrlReconSnapshotGenerationData getEnrlReconSnapshotGenerationData() {
		return enrlReconSnapshotGenerationData;
	}

	public void setEnrlReconSnapshotGenerationData(EnrlReconSnapshotGenerationData enrlReconSnapshotGenerationData) {
		this.enrlReconSnapshotGenerationData = enrlReconSnapshotGenerationData;
	}
	
	
	
}
