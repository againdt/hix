const inputHouseholds = ["regression/APTC_2HH_2_groups"]
before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC10: DOB change with 2 enrollment groups for ${file} enrolled into medical/dental`, function() {

        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical and Dental - ${file}`, function () {
            cy.enrollIntoMedicalAndDental();
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
        it(`Updating the DoB for Person2 in the AT for ${file}`, function () { //Updating the DoB for Member 2
            cy.h.people[1].dob = "1988-10-10"
            cy.h.appStatus = 'enroll_med_dent_demo_update'
        })
        it(`AT update for ${file}`, function () {
            cy.updateAccountTransfer(cy.h)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})









