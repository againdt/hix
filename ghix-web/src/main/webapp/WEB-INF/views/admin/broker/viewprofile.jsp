<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<script type="text/javascript" src="<c:url value="/resources/js/phoneUtils.js" />"></script>
<%
Broker mybroker = (Broker)request.getAttribute("broker");
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();

}
%>
<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>	
<script type="text/javascript">
	
	$(document).ready(function() {
		var brkId = ${broker.id};
		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};
	
		var updatedProfile = '${updatedProfile}';
		
		if('true' == updatedProfile) {
			var href = "/hix/broker/profileupdated";
	   	   	$('<div id="success" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe id="success" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:310px;"></iframe></div></div>').modal({backdrop:false});
		}
	});
	
	function closeSuccess() {
		$("#success").remove();
		$('.modal-backdrop').remove();
		//window.location.replace = "/hix/broker/viewprofile";
	}	
	$(".comma").text(function(i, val) {
	    return val.replace(/,/g, ", ");
	});
	
	$(document).ready(function(){
			formatAllPhoneNumbers(["sPhoneNumber"]);
		});
	
	function sendActivationEmail() {
		
		$("#ajaxloader").show();
		$("#btnSendMail").attr("disabled", true);
		$("#btnCancelMail").attr("disabled", true);
		
		var url = '<c:url value="/admin/broker/resendactivationlink"></c:url>';
		
		$.ajax({
			url : url,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				brokerid:'${encBrokerId}'
			},
			success: function(response,xhr) {
			    if(isInvalidCSRFToken(xhr))
			 	  return;
			    
			    $("#ajaxloader").hide();
				$("#mailStatusModal").modal('hide');
				$('#btnSendMail').removeAttr("disabled");
				$('#btnCancelMail').removeAttr("disabled");
				
			    if(response === 'SUCCESS'){
			    	alert("Activation Link Sent");
			    }else{
			    	alert("Unable to send activation link");	
			    }
			},
			error : function(error) {
				$("#ajaxloader").hide();
				$("#mailStatusModal").modal('hide');
				$('#btnSendMail').removeAttr("disabled");
				$('#btnCancelMail').removeAttr("disabled");
				alert("Unable to send activation link");				
			}
		}); 


}

	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {	
			$("#ajaxloader").hide();
			$("#mailStatusModal").modal('hide');
			alert($('Session is invalid').text());
		rv = true;
		}
		return rv;
	}	
	
</script>
<div class="gutter10" role="main">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message
							code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.firstName}</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1 tabindex="0" aria-flowto="sidebar rightpanel"><a name="skip"></a>
		<c:choose>
			<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
				${broker.user.firstName}&nbsp;${broker.user.lastName}
			</c:when>
			<c:otherwise>
				${broker.firstName}&nbsp;${broker.lastName}
			</c:otherwise>
		</c:choose>
		</h1>
	</div>
	
	<div class="row-fluid">
		<form class="form-vertical" id="frmviewbrokerprofile"
			name="frmviewbrokerprofile" action="viewprofile" method="POST">
			<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
			<div id="sidebar" class="span3" role="navigation" tabindex="0">
					<div class="header"></div>
					<ul class="nav nav-list">
						<li><a
							href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
						<li class="active"><a href="#"><spring:message code="label.profile"/></a></li>
						<li><a
							href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
						<c:if test="${ca_statecode}">
							<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
						</c:if>
						
						<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
						<c:if test="${ca_statecode}">
							<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
						</c:if>
						<c:if test="${!ca_statecode && broker.user != null}">	
							<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
							<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
						</c:if>	
					</ul>
					  <br>
			<c:if test="${!ca_statecode}">	
				<div class="header">
					<h4><i class="icon-cog icon-white"></i><spring:message code="label.brkactions"/></h4>
				</div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">  
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">  
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${broker.certificationStatus!=null && broker.certificationStatus == 'Incomplete'}">
					<li class="navmenu">
						<a href="#mailStatusModal" role="button" data-toggle="modal"><i class="icon-envelope-alt"></i>Send Account Activation Email 
						</a>
					</li>
				</c:if>
	            </ul>   
	        </c:if>
			</div>
			<!-- end of span3 -->
			<div id="rightpanel" class="span9" role="contentinfo" tabindex="0">
				<div class="header">
					<h4 class="pull-left"  tabindex="0"><spring:message code="label.profile"/></h4>
					<%-- Integrating Edit button feature in CA but in Idaho broker admin doesn't have privilege to edit broker's profile page --%>
					<c:if test="${ca_statecode || (stateCode == 'NV' && ((broker.certificationStatus == 'Certified') || (broker.certificationStatus == 'Incomplete')) )}">
						<a class="btn btn-small pull-right" type="button" title="<spring:message code="label.brkEdit"/>" href="<c:url value='/admin/broker/editprofile/${encBrokerId}'/>"><spring:message code="label.brkEdit"/></a>
					</c:if>
				</div>
				<div class="gutter10">
					<div class="span">
						<table class="table table-border-none span11" tabindex="0">
							<tbody  tabindex="0">
								<tr>
									<td class="txt-right span3"><spring:url value="/broker/photo/${encBrokerId}"
											var="photoUrl" /> <img src="${photoUrl}" alt="<spring:message code="label.bkr.ProfileImageofBroker"/>"
										class="profilephoto thumbnail pull-right" />
									</td>
									<td class="span5">
										<h4>
										<c:choose>
											<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
												${broker.user.firstName}&nbsp;${broker.user.lastName}
											</c:when>
											<c:otherwise>
												${broker.firstName}&nbsp;${broker.lastName}
											</c:otherwise>
										</c:choose>
										</h4>
										<p>
											${broker.location.address1}
											<c:if
												test="${broker.location.address2!=null && broker.location.address2!=\"\"}">
												<br>${broker.location.address2} 
										</c:if>
											<br/> ${broker.location.city} ${broker.location.state}
											${broker.location.zip} <br />
										</p>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.brkPhoneNumber"/></td>
									<td><strong id="sPhoneNumber">${broker.contactNumber}</strong></td>
								</tr>
								
								<tr>
									<td class="txt-right" style="width: 30%"><spring:message  code="label.brkPublicEmailAdress"/></td>
									<td style="width: 70%"><strong>${broker.yourPublicEmail}</strong></td>
								</tr>
								
								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkClientsServed"/></td>
											<td><strong>${broker.clientsServed}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:if test="${broker.clientsServed != '' && broker.clientsServed != null}">
											<tr>
												<td class="txt-right"><spring:message code="label.brkClientsServed"/></td>
												<td><strong>${broker.clientsServed}</strong></td>
											</tr>
										</c:if>
									</c:otherwise>
								</c:choose>
								
								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkLanguageSpoken"/></td>
											<td><strong>${broker.languagesSpoken}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:if test="${broker.languagesSpoken != '' && broker.languagesSpoken != null}">
											<tr>
												<td class="txt-right"><spring:message code="label.brkLanguageSpoken"/></td>
												<td><strong>${broker.languagesSpoken}</strong></td>
											</tr>
										</c:if>
									</c:otherwise>
								</c:choose>
								
								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkProductExpertise"/></td>
											<td><strong class="comma">${broker.productExpertise}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:if test="${broker.productExpertise != '' && broker.productExpertise != null}">
											<tr>
												<td class="txt-right"> 
													<spring:message code="label.brkAreaExpertise"/> 
												</td>
												<td><strong class="comma">${broker.productExpertise}</strong></td>
											</tr>
										</c:if>
									</c:otherwise>
								</c:choose>
						
								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message  code="label.brkWebsite"/></td>
											<td><strong>${broker.yourWebSite}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:if test="${broker.yourWebSite != '' && broker.yourWebSite != null}">
											<tr>
												<td class="txt-right">
													<spring:message  code="label.brkWebsite"/>    
												</td>
												<td><strong>${broker.yourWebSite}</strong></td>
											</tr>
										</c:if>
									</c:otherwise>
								</c:choose>
						
								<c:if test="${broker.education != '' && broker.education != null}">
									<tr>
										<td class="txt-right"><spring:message code="label.brkEducation"/></td>
										<td><strong>${broker.education}</strong></td>
									</tr>
								</c:if>
								

								<c:if test="${broker.aboutMe != '' && broker.aboutMe != null}">
									<tr>
										<td class="txt-right"><spring:message code="label.brkAboutMe"/></td>
										<td class="input-xlarge"><strong>${broker.aboutMe}</strong></td>
									</tr>
								</c:if>
							</tbody>
						</table>
<!-- 						<div class="span5 gutter10"> -->
<!-- 							<iframe width="190" height="190" frameborder="0" scrolling="no" -->
<!-- 								marginheight="0" marginwidth="0" class="thumbnail" -->
<%-- 								src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;ll=${broker.location.lat},${broker.location.lon}&amp;z=13&amp;iwloc=A&amp;output=embed" title="Broker Location">Broker Location</iframe> --%>
<!-- 						</div> -->
					</div>
				</div>
				<!-- Showing comments -->
				<%-- 							<comment:view targetId="${broker.id}" targetName="BROKER"> --%>
				<%-- 							</comment:view> --%>
			</div>
			
			<div id="mailStatusModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="mailStatusModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
				<div class="modal-header">
					<h4 class="margin0 pull-left">Send Activation Link</h4>
					<button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button">x</button>
				</div>

				<div class="modal-body">
					<div class="control-group txt-center">
						<h4 class="margin0" id="mailStatus">Send activation email ?</h4>
					</div>
				</div>
				<div class="modal-footer clearfix">
					<button id="btnCancelMail" class="btn pull-left" data-dismiss="modal" aria-hidden="true"> <spring:message code="label.cancelButton" /></button>
					<button id="btnSendMail" class="btn btn-primary" onclick="sendActivationEmail(); return false;">
						<spring:message code="label.sendButton" />
					</button>
				</div>

				<div id="ajaxloader"
					style="display: none; width: 80px; height: 80px; position: absolute; top: 50%; left: 50%; padding: 2px; margin-top: -52px; margin-left: -47px;">
					<img src="<c:url value="/resources/images/Eli-loader.gif" />"
						alt="Required!" width="80" height="80" />
				</div>
			</div>
			
		</form>
	</div>
	<!-- #row-fluid -->
</div>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>
 
  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>