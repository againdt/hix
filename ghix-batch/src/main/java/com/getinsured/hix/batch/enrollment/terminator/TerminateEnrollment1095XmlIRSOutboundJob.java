package com.getinsured.hix.batch.enrollment.terminator;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.util.exception.GIException;

public class TerminateEnrollment1095XmlIRSOutboundJob  implements Tasklet{

	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollment1095XmlIRSOutboundJob.class);

	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	//	private EnrollmentEmailUtils enrollmentEmailUtils;
	//	private SkippedRecords skippedRecords;
	//	private EnrollmentIds enrollmentIdList;

	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {

		LOGGER.info("Inside TerminateEnrollment1095XmlIRSOutboundJob");
		ExecutionContext je = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();

		Object jobExecutionStatus = je.get("jobExecutionStatus");
		String jobStatus = null;

		if(jobExecutionStatus != null){
			jobStatus = (String)jobExecutionStatus;
		}
		clearParentFolders();
		if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
			String errorMsg = "";
			/*		String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
	    	String irsFailureFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_FAILURE_FOLDER_PATH);
	    	String timeStampIrsXMLFailureFolderPath = irsFailureFolderPath + File.separator + batchDate +  File.separator + "IncompleteIRSReports";
	    	LOGGER.error("Error occured during IRS Report Generation, Files moved to Failure folder");
			EnrollmentUtils.moveFiles(filePayloadXMLFolderPath,timeStampIrsXMLFailureFolderPath,EnrollmentConstants.FILE_TYPE_XML,null);
			LOGGER.error("Error occured during IRS Report Generation, cleaning IRSContentFileXMLFolder");
			boolean status = EnrollmentUtils.cleanDirectory(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH));
			if(!status){
				LOGGER.info("Directory cleaning failed");
			}*/
			if(null != je.get("errorMessage")){
				errorMsg = (String)je.get("errorMessage");
				LOGGER.error(errorMsg);
			}
			throw new GIException("Error occured during IRS Report Generation" + errorMsg);
		}
		/*String batchDate= enrollmentIrsReportService.getDateTime();		
		if(skippedRecords!=null){
			LOGGER.debug("Skipped Household List :: "+skippedRecords.getSkippedHouseholdMap());
			enrollmentIrsReportService.logSkippedHouseholds(skippedRecords.getSkippedHouseholdMap(), batchDate);
			skippedRecords.clearSkippedHouseholdMap();
		}
		if(householdIdList!=null){
			householdIdList.clearHouseHoldIdList();
		}
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLEXTRACTPATH));

		if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
			String filePayloadXMLFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH);
	    	String irsFailureFolderPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_REPORT_FAILURE_FOLDER_PATH);
	    	String timeStampIrsXMLFailureFolderPath = irsFailureFolderPath + File.separator + batchDate +  File.separator + "IncompleteIRSReports";
	    	LOGGER.error("Error occured during IRS Report Generation, Files moved to Failure folder");
			EnrollmentUtils.moveFiles(filePayloadXMLFolderPath,timeStampIrsXMLFailureFolderPath,EnrollmentConstants.FILE_TYPE_XML,null);
			LOGGER.error("Error occured during IRS Report Generation, cleaning IRSContentFileXMLFolder");
			boolean status = EnrollmentUtils.cleanDirectory(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTCONTENTFILEXMLPATH));
			if(!status){
				LOGGER.info("Directory cleaning failed");
			}
			throw new GIException("Error occured during IRS Report Generation");

		}*/
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

	}
	
	/**
	 * Cleans the temporary manifest and zip folder locations
	 * 
	 * @author negi_s
	 */
	private void clearParentFolders() {
		EnrollmentUtils.cleanDirectory(EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT)
				.append(File.separatorChar).append(EnrollmentConstants.MANIFEST_FOLDER).toString());
		EnrollmentUtils.cleanDirectory(EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT)
				.append(File.separatorChar).append(EnrollmentConstants.ZIP_FOLDER).toString());
	}
	
	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}

	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}
}
