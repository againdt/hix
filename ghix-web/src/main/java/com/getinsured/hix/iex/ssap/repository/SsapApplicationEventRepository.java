package com.getinsured.hix.iex.ssap.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

/**
 * Story HIX-41023 - CSR Overrides: CSR can choose to open a special
 * enrollment period for the household
 * 
 * Sub-Task HIX-54283 - Create getSepDetails API
 * 
 * Clone for the repository inisde ghix-eligibility
 * 
 * @author Nikhil Talreja
 * 
 * 
 */
@Repository
public interface SsapApplicationEventRepository  extends JpaRepository<SsapApplicationEvent, Long> {

	List<SsapApplicationEvent> findBySsapApplication(SsapApplication ssapApplication);

	@Query("from SsapApplicationEvent sae  where sae.ssapApplication.id = :applicationId and sae.eventType = 'OE')")
	public SsapApplicationEvent findEventForOpenEnrollment(@Param("applicationId") long applicationId);

	@Query("from SsapApplicationEvent where id = :applicationId)")
	public SsapApplicationEvent findBySssapApplicationEventId(@Param("applicationId") long applicationId);

	@Query("from SsapApplicationEvent where ssapApplication.id = :applicationId)")
	public SsapApplicationEvent findEventBySsapApplication(@Param("applicationId") long applicationId);

	@Query("Select event " +
			" FROM SsapApplicationEvent as event "+
			" inner join fetch event.ssapApplication as application"+
			" where application.id = :applicationId ")
	List<SsapApplicationEvent> findEventBySsapApplicationId(@Param("applicationId") long applicationId);
	
	@Modifying
	@Transactional
	@Query("Update SsapApplicationEvent event "
			+ "Set event.enrollmentStartDate=:enrollmentStartDate, event.enrollmentEndDate=:enrollmentEndDate, event.coverageStartDate=:coverageStartDate "
			+ "Where id=:id")
	Integer updateDatesForEvent(@Param("id") long id, @Param("enrollmentStartDate") Timestamp enrollmentStartDate, 
			@Param("enrollmentEndDate") Timestamp enrollmentEndDate, @Param("coverageStartDate") Timestamp coverageStartDate );

}
