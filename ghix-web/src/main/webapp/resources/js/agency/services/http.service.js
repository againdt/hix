(function() {
    'use strict';
    angular.module('agencyApp')
    .service('httpService',httpService);

    httpService.$inject = ['$http'];
    function httpService($http) {
        var httpservice = this;
        var data = [];
        httpservice.apiGetRequest = function (url) {
            var response = $http({
                method: "GET",
                url: url
            });
            return response;
        };

        httpservice.apiPostRequest = function (url, data, csrfToken){
            if(!csrfToken){ csrfToken = $("#tokid").val();}
            var response = $http({
                method: "POST",
                url: url,
                data: JSON.stringify(data)
            });
            return response;
        };
    }
})();
