package com.getinsured.hix.broker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.repository.IExternalIndividualRepository;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Implements {@link ExternalIndividualService} to find and save external
 * individuals
 * 
 */

@Service("externalIndividualService")
@Transactional
public class ExternalIndividualServiceImpl implements ExternalIndividualService {

	@Autowired
	private IExternalIndividualRepository externalIndividualRepository;

	/**
	 * Finds external individual based on individual case id
	 * 
	 * @see ExternalIndividualService#findByExternalIndividualID(long)
	 * 
	 * @param external_ind_Id
	 * @return ExternalIndividual
	 * 
	 */

	@Override
	@Transactional
	public ExternalIndividual findByExternalIndividualID(long external_ind_Id) {
		return externalIndividualRepository
				.findByIndividualCaseId(external_ind_Id);
	}
}