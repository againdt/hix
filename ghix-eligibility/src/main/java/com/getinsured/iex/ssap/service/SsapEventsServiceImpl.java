/**
 * Creates ApplicationEvent and ApplicantEvents for QEP/OE
 */
package com.getinsured.iex.ssap.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.getinsured.hix.platform.config.IEXConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.sep.repository.SepEventsRepository;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.repository.Ind19SsapApplicationEventRepository;
import com.getinsured.eligibility.ssap.validation.service.QleValidationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.EventPrecedenceIndicator;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author karnam_s
 *
 */
@Service("SsapEventsService")
public class SsapEventsServiceImpl implements SsapEventsService {
  private static final Logger log = LoggerFactory.getLogger(SsapEventsServiceImpl.class);

  private static final String Y = "Y";
  @Autowired
  SsapApplicationRepository ssapApplicationRepository;
  @Autowired
  SsapApplicationEventRepository ssapApplicationEventRepository;
  @Autowired
  SsapApplicantEventRepository ssapApplicantEventRepository;
  @Autowired
  Ind19SsapApplicationEventRepository ind19SsapApplicationEventRepository;
  @Autowired
  private SepEventsRepository sepEventsRepository;
  @Autowired
  @Qualifier("referralLceNotificationService")
  private ReferralLceNotificationService referralLceNotificationService;
  @Autowired
  private QleValidationService qleValidationService;
  
  @Autowired private ReferralOEService referralOEService;
  //	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
//	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
  public static final int QEP_WINDOW = 60;
  public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
  
  @Override
  @Transactional
  public void createSsapEvents(QepEventRequest qepEventRequest) throws Exception {
    List<SsapApplication> ssapApplicationsList = ssapApplicationRepository.findByAppId(qepEventRequest.getSsapApplicationId());
    SsapApplication ssapApplication = ssapApplicationsList.get(0);
    SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(ssapApplication.getId());

    if (null == ssapApplicationEvent) {
      ssapApplicationEvent = createApplicationEvent(ssapApplication, qepEventRequest);
    }
    createApplicantevents(ssapApplicationEvent, ssapApplication, qepEventRequest);

    if (SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(qepEventRequest.getApplicationType())) {
      if(!isQepAccepted(qepEventRequest)) {
        // call qle service to process qle before closing application
        qleValidationService.processQleForSepDenial(ssapApplication.getCaseNumber(), qepEventRequest.getUserId());
        //fetch the latest updated application
        ssapApplication = ssapApplicationRepository.findOne(ssapApplication.getId());
        ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
        ssapApplication.setSepQepDenied(Y);
        try {
          referralLceNotificationService.generateSEPDenialNotice(qepEventRequest.getCaseNumber());
        } catch (Exception tnfe) {
          log.error("Unable to generate SEP Denial Notice (most likely template not found): " + tnfe.getMessage(), tnfe);
        }
      } else {
        ssapApplication.setAllowEnrollment("N"); // TODO: Follow-up and make sure this is used and if this is correct behaviour.
      }
    }

    // TODO: Check with ID to make sure if new eligibility engine is used then do nothing, otherwise decide if we need to
    // set status here.
    // We use new eligibility-engine to set the status: ssapApplication.setEligibilityStatus(com.getinsured.eligibility.enums.EligibilityStatus.PE);
    ssapApplicationRepository.save(ssapApplication);
  }

  @Override
  public void triggerQleValidation(String caseNumber, int userId)
  {
    qleValidationService.triggerQleValidation(caseNumber, userId);
  }

  /**
   * creates a new record in ApplciationEvent
   * Event Type is set same as the application type which can be OE or QEP
   * @param ssapApplication
   * @return
   * @throws Exception
   */
  private SsapApplicationEvent createApplicationEvent(SsapApplication ssapApplication, QepEventRequest qepEventRequest) throws Exception {
	  boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

    SsapApplicationEvent ssapApplicationEvent = getApplicationEventByApplication(ssapApplication.getId());
    ssapApplicationEvent.setEventType(getEventType(ssapApplication.getApplicationType()));
    ssapApplicationEvent.setCreatedTimeStamp(new Timestamp(new TSDate().getTime()));
    ssapApplicationEvent.setEnrollmentStartDate(getEnrollmentStart(qepEventRequest));
    DateTime endDate = new DateTime(getEnrollmentEnd(qepEventRequest,isNextYearsOE)).withTime(23, 59, 59, 999);
    ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
    ssapApplicationEvent.setSsapApplication(ssapApplication);
    Date eventDate = null;
    if (SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(qepEventRequest.getApplicationType())) {
      eventDate = parseQepEventDate(qepEventRequest.getQepEventDate());
      if (eventDate != null) {
        ssapApplicationEvent.setEventDate(new Timestamp(eventDate.getTime()));
      } else {
        ssapApplicationEvent.setEventDate(null);
      }
    } else {
      ssapApplicationEvent.setEventDate(null);
    }

    ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

    return ssapApplicationEvent;
  }

  /**
   * creates a new record for ApplciantEvent for those applicants which doesn't exist a record
   * @param ssapApplicationEvent
   * @param ssapApplication
   * @throws ParseException
   */
  private void createApplicantevents(SsapApplicationEvent ssapApplicationEvent, SsapApplication ssapApplication, QepEventRequest qepEventRequest) throws ParseException {
    Map<Long, SsapApplicantEvent> applicantsMap = getAppliicantsFromApplicantEvents(ssapApplicationEvent);
    List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
    SepEvents sepEvents = null;
    boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

    if (SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(qepEventRequest.getApplicationType())) {
      sepEvents = sepEventsRepository.findByEventNameAndNonFinancial(qepEventRequest.getQepEvent(), qepEventRequest.getChangeType(), SepEvents.Financial.N);
    }
    for (SsapApplicant ssapApplicant : ssapApplicants) {
      SsapApplicantEvent applicantEvent = null;
      if (applicantsMap.containsKey(ssapApplicant.getId())) {
        applicantEvent = applicantsMap.get(ssapApplicant.getId());
      } else {
        applicantEvent = new SsapApplicantEvent();
      }
      applicantEvent.setCreatedTimeStamp(new Timestamp(new TSDate().getTime()));
      applicantEvent.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
      applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
      applicantEvent.setSsapApplicant(ssapApplicant);
      applicantEvent.setEnrollmentStartDate(getEnrollmentStart(qepEventRequest));
      DateTime endDate = new DateTime(getEnrollmentEnd(qepEventRequest,isNextYearsOE)).withTime(23, 59, 59, 999);
      applicantEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
      applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
      applicantEvent.setSepEvents(sepEvents);

      Date eventDate = parseQepEventDate(qepEventRequest.getQepEventDate());

      if (eventDate != null) {
        applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
      } else {
        applicantEvent.setEventDate(null);
      }

      ssapApplicantEventRepository.save(applicantEvent);
    }
  }

  /**
   * returns EventType QEP or OE
   * @param applicationEventType
   * @return SsapApplicationEventTypeEnum
   */
  private SsapApplicationEventTypeEnum getEventType(String applicationEventType) {

    if ("QEP".equalsIgnoreCase(applicationEventType)) {
      return SsapApplicationEventTypeEnum.QEP;
    } else {
      return SsapApplicationEventTypeEnum.OE;
    }
  }

  /**
   * Calculate the enrollment start date based on the application type
   * @param ssapApplication
   * @return Timestamp
   * @throws ParseException
   */
  private Timestamp getEnrollmentStart(QepEventRequest qepEventRequest) throws ParseException {
    Date oeStart = null;

    if ("QEP".equalsIgnoreCase(qepEventRequest.getApplicationType())) {
      //return ssapApplication.getEsignDate();
      oeStart = parseQepEventDate(qepEventRequest.getQepEventDate());
    } else {
      oeStart = parseQepEventDate(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));
    }

    return new Timestamp(oeStart.getTime());
  }

  /**
   * Calculate the enrollment end date based on the application type
   * @param ssapApplication
   * @return Timestamp
   * @throws ParseException
   */
  private Timestamp getEnrollmentEnd(QepEventRequest qepEventRequest, boolean isNextYearsOE) throws ParseException {
    if ("QEP".equalsIgnoreCase(qepEventRequest.getApplicationType())) {
      int enrollmentGracePeriod = 9;
      String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
      if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
        enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
      }
      Date qepEventDate = parseQepEventDate(qepEventRequest.getQepEventDate());
      DateTime qepEventEndDateTime = new DateTime(qepEventDate.getTime());
      qepEventEndDateTime = qepEventEndDateTime.plusDays(QEP_WINDOW + enrollmentGracePeriod);
      //Date tempDT = qepEventEndDateTime.toDate();
      return new Timestamp(qepEventEndDateTime.getMillis());
    }  
	else if (isNextYearsOE)
	{
		try {
			return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
					parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
		} catch (ParseException e) {
			return null;
		}
	}
    else {
      Date oeEnd = parseQepEventDate(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE));
      return new Timestamp(oeEnd.getTime());
    }
  }

  /**
   * Fetch all the Applicants associated with the Applicant Event
   * @param ssapApplicationEvent
   * @return Map<Long, Long>
   */
  private Map<Long, SsapApplicantEvent> getAppliicantsFromApplicantEvents(SsapApplicationEvent ssapApplicationEvent) {
    List<SsapApplicantEvent> applicantevents = ssapApplicationEvent.filteredPreferredSsapApplicantEvents();
    Map<Long, SsapApplicantEvent> applicantsMap = new HashMap<Long, SsapApplicantEvent>();

    for (SsapApplicantEvent ssapApplicantEvent : applicantevents) {
      applicantsMap.put(ssapApplicantEvent.getSsapApplicant().getId(), ssapApplicantEvent);
    }

    return applicantsMap;
  }

  private boolean isQepAccepted(QepEventRequest qepEventRequest) {
    Date date1 = parseQepEventDate(qepEventRequest.getQepEventDate());

    if (date1 != null) {
      int enrollmentGracePeriod = 9;
      String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
      if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
        enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
      }
      Date date2 = new TSDate();
      long diff = date2.getTime() - date1.getTime();

      if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > (enrollmentGracePeriod + QEP_WINDOW)) {
        return false;
      } else {
        return true;
      }
    }

    return false;
  }

  private SsapApplicationEvent getApplicationEventByApplication(long applicationId) {
    SsapApplicationEvent ssapApplicationEvent = null;
    ssapApplicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(applicationId);
    if (ssapApplicationEvent == null) {
      ssapApplicationEvent = new SsapApplicationEvent();
    }
    return ssapApplicationEvent;
  }

  public SsapApplicationEvent findSsapApplicationEventByApplicationId(long applicationId) {
    SsapApplicationEvent ssapApplicationEvent = null;
    ssapApplicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(applicationId);
    return ssapApplicationEvent;
  }

  private Date parseQepEventDate(String qepEventDate) {
    Date date = null;

    if (qepEventDate != null && !"".equals(qepEventDate)) {
      try {
        date = DateUtils.parseDate(qepEventDate,
            "yyyy-MM-dd",
            "MM/dd/yyyy",
            "MMM dd, yyyy HH:mm:ss",
            "MMM dd, yyyy HH:mm:ss a");
      } catch (ParseException e) {
        log.warn("Unable to parse QEP Event Date value: {}, error: {}", qepEventDate, e.getMessage());
      }
    } else {
      log.warn("QEP Event Date is null or blank, returning null value. QEP Event Date: {}", qepEventDate);

    }

    return date;
  }
}
