package com.getinsured.hix.batch.agency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Broker;

/**
 * Spring-JPA repository that encapsulates methods that interact with the Broker
 * table
 */
public interface IBrokerRepository extends RevisionRepository<Broker, Integer, Integer>, JpaRepository<Broker, Integer> {

	/**
	 * Fetches the Broker record against the given broker Id
	 * 
	 * @param brokerId
	 *            the broker id
	 * @return the Broker record against the broker Id
	 */
	Broker findById(int brokerId);
	
	@Query("select count(*) from Broker b join b.user.userRole ur where b.agencyId = :agencyId and ur.role.name = :agencyManagerRole")
	int getAgencyManagerCount(@Param("agencyId") Long agencyId,@Param("agencyManagerRole") String agencyRole);
	
	@Query("from Broker broker where broker.npn = :npn")
	Broker findByNpn(@Param("npn") String npn);
	
	@Query("Select broker.id from Broker broker where broker.certificationStatus = 'Certified'  and broker.npn = :npn ")
	List<Integer> getBrokerIdsByNpn(@Param("npn") String npn);
}