
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Agency3InitVerifResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Agency3InitVerifResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}Agency3InitVerifResponseSet"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency3InitVerifResponseType", propOrder = {
    "responseMetadata",
    "agency3InitVerifResponseSet"
})
public class Agency3InitVerifResponseType {

    @XmlElement(name = "ResponseMetadata")
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "Agency3InitVerifResponseSet")
    protected Agency3InitVerifResponseSetType agency3InitVerifResponseSet;

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the agency3InitVerifResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link Agency3InitVerifResponseSetType }
     *     
     */
    public Agency3InitVerifResponseSetType getAgency3InitVerifResponseSet() {
        return agency3InitVerifResponseSet;
    }

    /**
     * Sets the value of the agency3InitVerifResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Agency3InitVerifResponseSetType }
     *     
     */
    public void setAgency3InitVerifResponseSet(Agency3InitVerifResponseSetType value) {
        this.agency3InitVerifResponseSet = value;
    }

}
