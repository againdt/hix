<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript">
function openFindBrokerModal(){
	$('#findbroker').modal('show');
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
});
</script>


<div class="gutter10">  	
	<div class="row-fluid margin30-t">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage Setup</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li class="active"><a href="#">1 Getting Started</a></li>
	      		<li><a href="#">2 Health Coverage Setup</a></li>
	      		<c:if test="${isDentalAvailable=='YES' }"><li><a href="#">3 Dental Coverage Setup</a></li></c:if>
	      		<li><a href="#">4 Review and Confirm</a></li>
	      	</ul>

			<div class="header margin30-t">
	      		<h4>Quick Links</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/broker/search' />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
	      	</ul>
	    </div>
    
	    <div class="span9" id="rightpanel">
	    	<div class="header">
	    		<h4 class="pull-left">Getting Started</h4>
	    		<a href="<c:url value="/shop/employer/setupcoverage/healthcoverage" />" id="nextbutton" class="btn btn-small btn-primary pull-right">Next</a>
	    		<a href="<c:url value="/shop/employer/dashboard" />" class="btn btn-small pull-right">Back</a>
	    	</div>
			
	    	<div class="gutter10">
		    	<form class="form-horizontal">
				<df:csrfToken/>
<!-- RENEWAL SETUP -->		    	
		    		<div class="gutter10" id="renewalSetup">
					  	<p>Renewals can be easy.  The following page will display the choices that are closest to what you have now. You can either confirm those settings or make changes as needed.</p>
					  	<p>If you do not complete this renewal process, coverage for your employees will end on ${coverageEndDate}.</p>
					</div>
<!-- RENEWAL SETUP ENDS -->

<!-- NEW SETUP -->					
				  	<div class="gutter10 hide" id="newSetup">
					  	<p>You will be making choices regarding the health <c:if test="${isDentalAvailable=='YES'}"> and dental </c:if>coverage options that you will be offering to your employees.  To help you, we'll make suggestions based on your objectives and/or how other companies of your size in your area have set up their health coverage.</p>
					  	<p>If you would prefer to work with someone directly, you can <a href="#">find an agent</a> to assist you.</p>
					</div>
<!-- NEW SETUP ENDS-->

					<div class="txt-center margin20-t">
						<a href="<c:url value="/shop/employer/setupcoverage/healthcoverage" />" class="btn btn-primary btn-large">Next</a>
					</div>
				</form>
	    	</div><!-- .gutter10 -->
	    	
		</div><!-- .span9 #rightpanel -->
	</div><!-- .row-fluid -->
</div><!-- .gutter10 -->

