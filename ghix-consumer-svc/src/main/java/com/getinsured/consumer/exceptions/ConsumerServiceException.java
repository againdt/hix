package com.getinsured.consumer.exceptions;

public class ConsumerServiceException extends Exception {
	private String message;
	
	public ConsumerServiceException(String message) {
		super();
		this.message = message;
	}

	private static final long serialVersionUID = 1L;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
