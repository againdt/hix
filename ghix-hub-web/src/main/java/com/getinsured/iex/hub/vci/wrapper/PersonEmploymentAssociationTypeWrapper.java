package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.PersonEmploymentAssociationType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.PersonEmploymentAssociationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class PersonEmploymentAssociationTypeWrapper implements JSONAware {
	
	private String employmentOriginalHireDate;
	private String associationBeginDate;
    private String associationEndDate;
    
    private static final String HIRE_DATE_KEY = "EmploymentOriginalHireDate";
    private static final String BEGIN_DATE_KEY = "AssociationBeginDate";
    private static final String END_DATE_KEY = "AssociationEndDate";
	 
    public PersonEmploymentAssociationTypeWrapper(PersonEmploymentAssociationType personEmploymentAssociationType){
		
		if(personEmploymentAssociationType == null){
			return;
		}
		
		if(personEmploymentAssociationType != null){
			setDates(personEmploymentAssociationType);
		}
		
	}
	
    private void setDates(PersonEmploymentAssociationType personEmploymentAssociationType){
    	
    	if(personEmploymentAssociationType.getEmploymentOriginalHireDate() != null
    			&& personEmploymentAssociationType.getEmploymentOriginalHireDate().getDateRepresentation() != null 
    			&& personEmploymentAssociationType.getEmploymentOriginalHireDate().getDateRepresentation().getValue() != null){
    		Date date = (Date)personEmploymentAssociationType.getEmploymentOriginalHireDate().getDateRepresentation().getValue();
			this.employmentOriginalHireDate = date.getValue().toString();
		}
    	if(personEmploymentAssociationType.getAssociationBeginDate() != null
    			&& personEmploymentAssociationType.getAssociationBeginDate().getDateRepresentation() != null 
    			&& personEmploymentAssociationType.getAssociationBeginDate().getDateRepresentation().getValue() != null){
    		Date date = (Date)personEmploymentAssociationType.getAssociationBeginDate().getDateRepresentation().getValue();
			this.associationBeginDate = date.getValue().toString();
		}
    	if(personEmploymentAssociationType.getAssociationEndDate() != null
    			&& personEmploymentAssociationType.getAssociationEndDate().getDateRepresentation() != null 
    			&& personEmploymentAssociationType.getAssociationEndDate().getDateRepresentation().getValue() != null){
    		Date date = (Date)personEmploymentAssociationType.getAssociationEndDate().getDateRepresentation().getValue();
			this.associationEndDate = date.getValue().toString();
		}
    	
    }
    
	public String getEmploymentOriginalHireDate() {
		return employmentOriginalHireDate;
	}

	public String getAssociationBeginDate() {
		return associationBeginDate;
	}

	public String getAssociationEndDate() {
		return associationEndDate;
	}
	
	@SuppressWarnings("unchecked")
		public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(HIRE_DATE_KEY, this.employmentOriginalHireDate);
		obj.put(BEGIN_DATE_KEY, this.associationBeginDate);
		obj.put(END_DATE_KEY, this.associationEndDate);
		return obj.toJSONString();
	}
}
