package com.getinsured.hix.admin.web;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class AdminIssuerEnrollmentRepresentativeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminIssuerEnrollmentRepresentativeController.class);
	
	@Autowired private IssuerService issuerService;
	@Autowired private LocationService locationService;
	@Autowired private UserService userService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	private static final String ROLE_NAME ="activeRoleName";
	//private final String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
	//private final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
	private final Boolean isEmailActivation = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION).toLowerCase());

	
	/*  ##### controller for get enrollment representative data   ##### */
	@RequestMapping(value = "/admin/issuer/enrollmentrepresentative/edit/{encRepId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editEnrollmentRepresentative(@PathVariable("encRepId") String encRepId, Model model, HttpServletRequest manageRepRequest) {
		String decryptedIssuerRepId = null;
		try
		{
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encRepId);
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findById(Integer.parseInt(decryptedIssuerRepId));
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, representativeObj.getIssuer());
			model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
			model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
			if(representativeObj.getUserRecord()!=null) {
				model.addAttribute("repUser", representativeObj.getUserRecord());
				model.addAttribute(PlanMgmtConstants.PHONE1, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
				model.addAttribute(PlanMgmtConstants.PHONE2, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
				model.addAttribute(PlanMgmtConstants.PHONE3, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
			}else {
				model.addAttribute("repUser", representativeObj);
				model.addAttribute(PlanMgmtConstants.PHONE1, representativeObj.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
				model.addAttribute(PlanMgmtConstants.PHONE2, representativeObj.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
				model.addAttribute(PlanMgmtConstants.PHONE3, representativeObj.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
			}
			
			model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());
			model.addAttribute("representativeObj", representativeObj);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
		}
		catch(Exception ex)
		{
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.EDIT_REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/enrollmentrepresentative/edit";
	}
	
	
	/*  ##### controller for save enrollment representative data  ##### */
	@RequestMapping(value = "/admin/issuer/enrollmentrepresentative/update", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editEnrollmentRepresentative(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser repUser, 
			@ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, 
			Model model, HttpServletRequest editRepRequest,  
			@RequestParam(value = "issuerRepId", required = false) Integer issuerRepId, RedirectAttributes redirectAttr) throws InvalidUserException {

		Integer issuerId = 0;
		try {
			// server side validation
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative,
					IssuerRepresentative.AddNewIssuerRepresentative.class);
			if (violations != null && !violations.isEmpty()) {
				// throw exception in case client side validation breached
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findById(issuerRepId);

			if (null != issuerRepObj) {
				issuerId = issuerRepObj.getIssuer().getId();

				// If state exchange is CA then update location information
				if (PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) {
					Location location = issuerRepObj.getLocation();
					if (issuerRepresentative != null && issuerRepresentative.getLocation() != null) {
						if (location == null) {
							location = new Location();
						}
						location.setAddress1(issuerRepresentative.getLocation().getAddress1());
						location.setAddress2(issuerRepresentative.getLocation().getAddress2());
						location.setCity(issuerRepresentative.getLocation().getCity());
						location.setState(issuerRepresentative.getLocation().getState());
						location.setZip(issuerRepresentative.getLocation().getZip());

						// save issue representative address in location table
						Location newlocation = locationService.save(location);
						issuerRepObj.setLocation(newlocation);
					}
				}
				else if (issuerRepObj.getUserRecord()==null && PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode) &&
						!issuerRepObj.getEmail().equalsIgnoreCase(editRepRequest.getParameter(PlanMgmtConstants.EMAIL))) {   //HIX-103663

					boolean duplicateEmailFlag = issuerService.isDuplicate(editRepRequest.getParameter(PlanMgmtConstants.EMAIL));

					if (duplicateEmailFlag) {
						issuerRepresentative.setId(issuerRepId);
						model.addAttribute("duplicateError", PlanMgmtConstants.YES);
						model.addAttribute("representativeObj", issuerRepresentative);
						model.addAttribute("repUser", issuerRepresentative);
						model.addAttribute(PlanMgmtConstants.REP_DUPLICATE_EMAIL, true);

						if (issuerRepresentative.getPhone() != null
								&& issuerRepresentative.getPhone().length() >= PlanMgmtConstants.TEN) {
							model.addAttribute(PlanMgmtConstants.PHONE1, issuerRepresentative.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
							model.addAttribute(PlanMgmtConstants.PHONE2, issuerRepresentative.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
							model.addAttribute(PlanMgmtConstants.PHONE3, issuerRepresentative.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
						}
						AccountUser currentAccountUser = userService.getLoggedInUser();
						String activeRoleName = currentAccountUser.getActiveModuleName().toUpperCase();
						model.addAttribute(ROLE_NAME, activeRoleName);
						model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerRepObj.getIssuer());
						model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
						model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
						model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
						model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
						return "admin/issuer/enrollmentrepresentative/edit";
					}
				}
				issuerRepObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
				issuerRepObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
				issuerRepObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
				issuerRepObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
				issuerRepObj.setEmail(editRepRequest.getParameter(PlanMgmtConstants.EMAIL));
				issuerRepObj.setUpdatedBy(userService.getLoggedInUser());

				// save issuer enrollment representative data
				IssuerRepresentative newIssuerRepresentative = issuerService.saveRepresentative(issuerRepObj);

				// update user data
				if (issuerRepObj.getUserRecord() != null) {

					AccountUser accountUserObj = userService.findById(issuerRepObj.getUserRecord().getId());

					if (null != accountUserObj) {
						accountUserObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
						accountUserObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
						accountUserObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
						accountUserObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
						userService.updateUser(accountUserObj);
					}
				}
				else {

					if (newIssuerRepresentative != null && isEmailActivation 
							&& !PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)
							&& !PlanMgmtConstants.STATE_CODE_MS.equalsIgnoreCase(stateCode)) {

						try {
							String role = ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString();
							if(StringUtils.isNotBlank(newIssuerRepresentative.getRole())){
								role = newIssuerRepresentative.getRole();
							}
							issuerService.generateActivationLink(newIssuerRepresentative.getId(), repUser, role);
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to "
									+ repUser.getEmail());
						}
						catch (Exception e) {
							LOGGER.error(" " + e);
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO,
									"Activation link sending process failed... Please take appropriate actions. -  " + e.getMessage());
						}
					}
				}
			 }
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		String encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerId));
		return PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encryptedIssuerId;
	}

	/*  ##### controller for add enrollment representative   ##### */
	@RequestMapping(value = "/admin/issuer/enrollmentrepresentative/add/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String addEnrollmentRepresentative(@PathVariable("encIssuerId") String encIssuerId, Model model) {
		 String decryptedIssuerId = null;
		try{
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
			model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
			model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ADD_REPRESENTATIVE_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "admin/issuer/enrollmentrepresentative/add";
	}

}
