package com.getinsured.eligibility.at.resp.service;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.resp.mapper.EligibilityProgramNotificationDtoMapper;
import com.getinsured.eligibility.notification.dto.SsapApplicationEligibilityDTO;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ui.dto.EligibilityProgramDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.NoticeTmplHelper;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;


@Service("eligibilityNotificationService")
@DependsOn("dynamicPropertiesUtil")
@Transactional(readOnly = true)
public class EligibilityNotificationServiceImpl implements EligibilityNotificationService {


	@Autowired private EligibilityService eligibilityService;
	@Autowired private NoticeService noticeService;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private ContentManagementService ecmService;
	@Autowired private PreferencesService preferencesService;

	public static final String PRIVACY_CONTENT_LOCATION = "notificationTemplate/privacyContent.html";
	public static final String APPEALS_CONTENT_LOCATION="notificationTemplate/eligibilityAppealsStaticContent.html";
	public static final String HEARING_RIGHTS_LOCATION="notificationTemplate/HearingRights.html";
	public static final String PRIVACY_CONTENT="privacyContent";
	public static final String ELIGIBILITY_APPEALS="eligibilityAppeals";
	public static final String HEARING_RIGHTS="hearingRights";
	private static final String TEMPLATE_DATE_FORMAT = "MM/dd/yyyy";

	@Autowired private ApplicationContext appContext;
	@Autowired private GhixDBSequenceUtil ghixDBSequenceUtil;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private ILocationRepository iLocationRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(EligibilityNotificationServiceImpl.class);

	@Override
	public String generateEligibilityNotificationInInbox(String caseNumber) throws Exception{
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (stateCode.equals("NV")) {
			return generate(caseNumber, "UniversalEligibilityNotification");
		} else {
			return generate(caseNumber, "ApplicationEligibilityResults");
		}
	}

	@Override
	public String generateEligibilityNotificationInInbox(String caseNumber, String noticeTemplateName) throws Exception{
		LOGGER.debug("generateEligibilityNotificationInInbox::generating with caseNumber {} and template {}", caseNumber, noticeTemplateName);
		return generate(caseNumber, noticeTemplateName);
	}

	@Override
	public String generateEligibilityNotificationUpdatedInInbox(String caseNumber) throws Exception{

		return generate(caseNumber, "ApplicationEligibilityResultsUpdated");
	}

	private String generate(String caseNumber, String noticeTemplateName) throws NoticeServiceException {
		SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = getApplicationData(caseNumber);
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);
		Household household = fetchConsumer(ssapApplicationEligibilityDTO);
		int moduleId = household.getId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		Location location = null;
		PreferencesDTO preferencesDTO  = preferencesService.getPreferences(ssapApplication.getCmrHouseoldId().intValue(), false);
		location = getLocationFromDTO(preferencesDTO.getLocationDto());
		
		String relativePath = "cmr/" + moduleId + "/";
		String ecmFileName = noticeTemplateName + "_" + moduleId
				+ (new TSDate().getTime()) + ".pdf";

		String emailId = preferencesDTO.getEmailAddress();
		List<String> validEmails = emailId != null ? Arrays.asList(emailId)
				: null;

		
		Notice notice = noticeService
				.createModuleNotice(
						noticeTemplateName,
						GhixLanguage.US_EN,
						getReplaceableObjectData(ssapApplicationEligibilityDTO, ssapApplication),
						relativePath,
						ecmFileName,
						moduleName,
						moduleId,
						validEmails,
						DynamicPropertiesUtil
								.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
						fullName, location, preferencesDTO.getPrefCommunication());

		return notice.getEcmId();
	}

	private void udapteSsapApplicationEligibilityDTO(com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {
		// find all applicant
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplicationEligibilityDTO.getPrimaryKey());
		//create Map of Eligible and non eligible persons
		List<com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO> ssapApplicantEligibleEligibilityDTO =  ssapApplicationEligibilityDTO.getSsapApplicantEligibleEligibilityDTO();
		List<com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO> ssapApplicantIneligibleEligibilityDTO =  ssapApplicationEligibilityDTO.getSsapApplicantIneligibleEligibilityDTO();
		
		Set<Long> eligibilePersonId = getSetOfPersonId(ssapApplicantEligibleEligibilityDTO);
		Set<Long> ineligibilePersonId = getSetOfPersonId(ssapApplicantIneligibleEligibilityDTO);
		for(SsapApplicant ssapApplicant : ssapApplicants){
			if(ssapApplicant.getApplyingForCoverage() != null && ssapApplicant.getApplyingForCoverage().equals("N")
			 && !eligibilePersonId.contains(ssapApplicant.getPersonId()) && !ineligibilePersonId.contains(ssapApplicant.getPersonId())
			 && "Y".equals(ssapApplicant.getOnApplication())){
				com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO = new com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO();
				ssapApplicantEligibilityDTO.setFirstName(ssapApplicant.getFirstName() != null ? ssapApplicant.getFirstName() : StringUtils.EMPTY);
				ssapApplicantEligibilityDTO.setMiddleName(ssapApplicant.getMiddleName() != null ? ssapApplicant.getMiddleName() : StringUtils.EMPTY);
				ssapApplicantEligibilityDTO.setLastName(ssapApplicant.getLastName() != null ? ssapApplicant.getLastName() : StringUtils.EMPTY);
				ssapApplicantEligibilityDTO.setPersonId(ssapApplicant.getPersonId());
				ssapApplicantEligibilityDTO.setVerificationCompleted(false);
				ssapApplicantEligibilityDTO.setEligibilities(new HashSet<EligibilityProgramDTO>());
				ssapApplicantEligibilityDTO.getEligibilities().add(new EligibilityProgramDTO());
				ssapApplicantIneligibleEligibilityDTO.add(ssapApplicantEligibilityDTO);
			}
		}
		
		
	}

	private Set<Long> getSetOfPersonId(List<com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO> ssapApplicantEligibleEligibilityDTO) {
		Set<Long> personIds = new HashSet<Long>();
		if(ssapApplicantEligibleEligibilityDTO!=null){
			for(com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO:ssapApplicantEligibleEligibilityDTO){
				personIds.add(ssapApplicantEligibilityDTO.getPersonId());
			}
		}
		return personIds;
	}

	
	public Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}

	

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName()+ " "+ssapApplicant.getLastName();
			}
		}

		return null;
	}

	private SsapApplication getSSAPApplicationData(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository
				.findByCaseNumber(caseNumber);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException(
				"SSAP Id is null.Unable to fetch household id. Email cannot be triggered for"
						+ caseNumber);
	}

	private Household fetchConsumer(
			SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {

		int cmrId = fetchModuleId(ssapApplicationEligibilityDTO);
		return cmrHouseholdRepository.findOne(cmrId);
	}

	private int fetchModuleId(SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {

		int cmrId = ssapApplicationEligibilityDTO.getCmrHouseholdId() != null ? ssapApplicationEligibilityDTO.getCmrHouseholdId().intValue() : 0;

		if (cmrId == 0){
			throw new GIRuntimeException("Cannot generate notification! CMR Household ID not found for case number - " + ssapApplicationEligibilityDTO.getCaseNumber());
		}
		return cmrId;
	}

	@Override
	public SsapApplicationEligibilityDTO getApplicationData(String caseNumber){

		com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = eligibilityService.getApplicationData(caseNumber);
		//udapteSsapApplicationEligibilityDTO(ssapApplicationEligibilityDTO);
		return EligibilityProgramNotificationDtoMapper.map(ssapApplicationEligibilityDTO);
	}

	private Map<String, Object> getReplaceableObjectData(
			SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO,
			SsapApplication ssapApplication) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("ssapApplicationEligibilityDTO",
				ssapApplicationEligibilityDTO);
		tokens.put("primaryApplicantName", getName(ssapApplication));
		if (ssapApplicationEligibilityDTO != null
				&& ssapApplicationEligibilityDTO.getEligibilityReceivedDate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY",
					new Locale("es", "ES"));
			tokens.put("spanishDate", formatter
					.format(ssapApplicationEligibilityDTO
							.getEligibilityReceivedDate()));
		} else {
			tokens.put("spanishDate", " ");
		}
		tokens.put(PRIVACY_CONTENT, getStaticTemplate(PRIVACY_CONTENT_LOCATION));
		tokens.put(ELIGIBILITY_APPEALS,
				getStaticTemplate(APPEALS_CONTENT_LOCATION));
		tokens.put(HEARING_RIGHTS, getStaticTemplate(HEARING_RIGHTS_LOCATION));
		tokens.put(
				TemplateTokens.EXCHANGE_FULL_NAME,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(
				TemplateTokens.EXCHANGE_PHONE,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(
				TemplateTokens.EXCHANGE_ADDRESS_1,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(
				TemplateTokens.EXCHANGE_ADDRESS_2,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put(
				"exgCityName",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put(
				"exgStateName",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put(
				"zip",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(
				TemplateTokens.EXCHANGE_URL,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put(
				"exchangeFax",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put(
				"exchangeAddressEmail",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", ssapApplication.getId());

		// Tokens needed for Universal Eligibility Notice
		Date esignDate = new Date(ssapApplication.getEsignDate().getTime());
		Calendar cal = TSCalendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		cal.setTime(esignDate);
		cal.add(Calendar.DATE, 90);

		tokens.put("submitDate", DateUtil.dateToString(DateUtil.removeTime(esignDate), TEMPLATE_DATE_FORMAT));
		tokens.put("submitPlus90", DateUtil.dateToString(DateUtil.removeTime(cal.getTime()), TEMPLATE_DATE_FORMAT));
		tokens.put("todaysDate", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		tokens.put("planYear", Long.toString(ssapApplication.getCoverageYear()));
		tokens.put("eligibilityID", ssapApplication.getCaseNumber());
		tokens.put("currentYear", currentYear);

		List<SsapApplicationEvent> ssapApplicationEvents = ssapApplicationEventRepository.findBySsapApplication(ssapApplication);
		if (ssapApplicationEvents != null && !ssapApplicationEvents.isEmpty() && ssapApplicationEvents.get(0).getEnrollmentEndDate() != null) {
			Date enrollmentEndDate = new Date(ssapApplicationEvents.get(0).getEnrollmentEndDate().getTime());
			LOGGER.debug("getReplaceableObjectData::enrollmentEndDate {}", enrollmentEndDate);
			tokens.put("enrollByDate", DateUtil.dateToString(DateUtil.removeTime(enrollmentEndDate), TEMPLATE_DATE_FORMAT));
		}

		return tokens;
	}


	private String getStaticTemplate(String templateLocation) throws NoticeServiceException {

		InputStream inputStreamUrl = null;

		try {
			if("Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))){
				inputStreamUrl = getTemplateFromECM(templateLocation);
			}
			else{
				Resource resource = appContext.getResource("classpath:"+templateLocation);
				inputStreamUrl = resource.getInputStream();
			}
			return populateStaticTemplateContent(IOUtils.toString(inputStreamUrl, "UTF-8"));
		} catch (FileNotFoundException fnfe) {
			LOGGER.error("Error in reading template from:",templateLocation);
			throw new NoticeServiceException(fnfe);
		} catch (IOException ioe) {
			LOGGER.error("Error in reading template from:",templateLocation);
			throw new NoticeServiceException(ioe);
		}
		catch (Exception e) {
			LOGGER.error("Error while populating the template:",e);
			throw new NoticeServiceException(e);
		}
		finally
		{
			IOUtils.closeQuietly(inputStreamUrl);
		}

	}

	@Autowired private NoticeTmplHelper noticeTmplHelper;
	
	private InputStream getTemplateFromECM(String location) throws ContentManagementServiceException  {
		String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH)
			 	+ location;
			
		return new ByteArrayInputStream(noticeTmplHelper.readBytesByPath(ecmTemplateFolderPath));
		//return new ByteArrayInputStream(ecmService.getContentDataByPath(ecmTemplateFolderPath + location));
	}


	private String populateStaticTemplateContent(String templateContent) throws NoticeServiceException
	{
		Map<String, Object> replaceableObj = new HashMap<String, Object> ();
		replaceableObj.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObj.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		replaceableObj.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		replaceableObj.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		replaceableObj.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		replaceableObj.put("exgCityName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		replaceableObj.put("exgStateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		replaceableObj.put("zip", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		replaceableObj.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		replaceableObj.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		replaceableObj.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		replaceableObj.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		String noticeSeqId = ghixDBSequenceUtil.getNextSequenceFromDB(Notice.NOTICESEQUENCE.notices_seq.toString());
		replaceableObj.put(TemplateTokens.NOTICE_UNIQUE_ID, StringUtils.leftPad(noticeSeqId, GhixPlatformConstants.TEN, GhixPlatformConstants.ZERO));
		replaceableObj.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);
		replaceableObj.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();

		try {
			stringLoader.putTemplate("noticeTemplate", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			Template tmpl = templateConfig.getTemplate("noticeTemplate");
			tmpl.process(replaceableObj, sw);

		} catch (Exception e) {
			LOGGER.error("Exception found while populating Static Template", e);
			throw new NoticeServiceException(e);
		}
		finally{
			IOUtils.closeQuietly(sw);
		}

		return sw.toString();
	}

}
