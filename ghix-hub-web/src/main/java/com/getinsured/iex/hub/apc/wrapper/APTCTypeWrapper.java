package com.getinsured.iex.hub.apc.wrapper;

import java.math.BigDecimal;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_ee.APTCType;

public class APTCTypeWrapper implements JSONAware{

    private BigDecimal aptcMaximumAmount;
    private BigDecimal aptcRemainingBHCAmount;

	public APTCTypeWrapper(APTCType aPTCType)
	{
		aptcMaximumAmount = aPTCType.getAPTCMaximumAmount().getValue();
		aptcRemainingBHCAmount = aPTCType.getAPTCRemainingBHCAmount().getValue();
	}
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("APTCMaximumAmount", this.aptcMaximumAmount);
		obj.put("APTCRemainingBHCAmount", this.aptcRemainingBHCAmount);
		return obj.toJSONString();
	}
}
