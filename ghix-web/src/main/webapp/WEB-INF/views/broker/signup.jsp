<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!-- <body id="broker-signup"> -->
		<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
	<!-- 	<ul class="nav nav-tabs">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#">Find Insurance</a></li>
			<li><a href="#">Learn More</a></li>   
			<li><a href="#">Get Assistance</a></li>   
		</ul> -->
		<!-- end of secondary navbar -->
<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip"></a>Register with State Health Benefit Exchange</h1>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
			<!--  beginning of side bar -->
			<div class="header">
				<h4>Registrations</h4>
			</div>
			<!-- end of side bar -->
			</div><!-- end of span3 -->
			<div class="span9" id="rightpanel">
				
					
					<p class="gutter10">Please fill out this form to start the registration process with the State Health Benefit Exchange.
					<br />On submission of the form an email will be sent to you to activate your account.
					</p>
				
				<form class="form-vertical" id="frmbrokerreg" name="frmbrokerreg" action="signup" method="POST">
				<div class="gutter10">
					<div class="control-group">
						<label for="email" class="required control-label"><spring:message  code="label.brkEmail"/></label><!-- end of label -->
						<div class="controls">
							<input type="email" name="email" id="email" value="" class="xlarge" size="30"  onblur = "checkEmail()" />
							<div id="email_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="confirmEmail" class="required control-label"><spring:message  code="label.brkConfirmEmail1"/></label>
						<div class="controls">
							<input type="email" name="confirmEmail" id="confirmEmail" value="" class="xlarge" size="30" />
							<div id="confirmEmail_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="firstName" class="required control-label"><spring:message  code="label.brkFirstName"/></label><!-- end of label -->
						<div class="controls">
							<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="30" />
							<div id="firstName_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="lastName" class="required control-label"><spring:message  code="label.brkLastName"/></label><!-- end of label -->
						<div class="controls">
							<input type="text" name="lastName" id="lastName" value="" class="xlarge" size="30" />
							<div id="lastName_error" class="help-inline"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<%-- <div class="control-group">
						<label for="userName" class="required control-label"><spring:message  code="label.brkUserName"/></label>
						<div class="controls">
							<input type="text" name="userName" id="userName" value="" class="xlarge" size="30" onkeyup="chkUserName()" />
							<div id="userName_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group --> --%>

					<input type="hidden" name="userNameCheck" id="userNameCheck" value="" />
					<input type="hidden" name="emailCheck" id="emailCheck" value="" />
				</div><!-- end of gutter10 -->

				<div class="form-actions">
					<input type="submit" name="submit" id="submit" value="<spring:message  code='label.signupBtn'/>" class="btn btn-primary" title="<spring:message  code='label.signupBtn'/>"/> 
				</div><!-- end of form-actions -->
				</form>
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
	</div>
	<script type="text/javascript">
	jQuery.validator.addMethod("userNameCheck", function(value, element, param) {
		return $("#userNameCheck").val() == 0 ? false : true;
	
	});
	
	jQuery.validator.addMethod("emailCheck", function(value, element, param) {
		return $("#emailCheck").val() == 0 ? false : true;
	
	});
	
	jQuery.validator.addMethod("confirmEmail", function(value, element, param) {
		 password   = $("#email").val().toLowerCase(); 
		 confirmpwd = $("#confirmEmail").val().toLowerCase(); 
		 
		if( password != confirmpwd ){ return false; }
		return true;
	});
	
	function chkUserName(){
		$.get('signup/chkUserName',
				{userName: $("#userName").val()},
				function(response){
					if(response == "EXIST"){
						$("#userNameCheck").val(0);
					}else{
						$("#userNameCheck").val(1);
					}
				}				
			);
	}
	function checkEmail(){
		$.get('signup/checkEmail',
				{email: $("#email").val()},
				function(response){
					if(response == "EXIST"){
						$("#emailCheck").val(0);
					}else{
						$("#emailCheck").val(1);
					}
				}
			);
	}
	
	var validator = $("#frmbrokerreg").validate({ 
		rules : { firstName : { required : true},
			lastName : { required : true},
			confirmEmail : { required : true, confirmEmail : true},
		   email : { required : true,
			   emailCheck : true},
		   userName : { required: true,
						userNameCheck : true}
		},
		messages : {
			
			firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
			lastName : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
	
			confirmEmail :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailConfirm' javaScriptEscape='true'/></span>",
				confirmEmail: "<span> <em class='excl'>!</em><spring:message code='label.validateEmailConfirm' javaScriptEscape='true'/></span>",
				validEmailCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"},
	
			email :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
				emailCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailCheck' javaScriptEscape='true'/></span>"},
	
			userName :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateUserName' javaScriptEscape='true'/></span>",
						userNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateUserNameCheck' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class',
					'error help-inline');
		} 
		
	});
	
	
	</script>
