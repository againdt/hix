package com.getinsured.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.Member;



@Repository
public interface IMemberRepository extends JpaRepository<Member, Integer> {
	
	List<Member> findByHousehold(Household household);
	
	/**
	 * Method is used to modify smoker information for a given member
	 * @param smoker - possible values YES, NO
	 * @param ffeApplicantIdList - ffeApplicantId(s) for the members for which the smoker info is being persisted
	 * @return
	 */
	@Modifying
	@Transactional
	@Query("UPDATE Member SET smoker = :smoker WHERE ffeApplicantId IN (:ffeApplicantIdList)")
	void updateSmokerInfo(@Param("smoker") Member.MemberBooleanFlag smoker, @Param("ffeApplicantIdList") List<Long> ffeApplicantIdList);
	
}
