package com.getinsured.ssap.model.financial.nmec.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes exception cause for {@link NonEsiMecExceptionCause}
 *
 * @author Suresh Kancherla
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NonEsiMecExceptionCause {
  @JsonProperty("Exception Type")
  private String exceptionType;

  @JsonProperty("ExceptionMessage")
  private String exceptionMessage;

  @JsonProperty("ExceptionCause")
  private NonEsiMecExceptionCauseDetails exceptionCauseDetails;

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public NonEsiMecExceptionCauseDetails getExceptionCauseDetails() {
    return exceptionCauseDetails;
  }

  public void setExceptionCauseDetails(NonEsiMecExceptionCauseDetails exceptionCauseDetails) {
    this.exceptionCauseDetails = exceptionCauseDetails;
  }

  @Override
  public String toString() {
    return "IfsvExceptionCause{" +
        "exceptionType='" + exceptionType + '\'' +
        ", exceptionMessage='" + exceptionMessage + '\'' +
        ", exceptionCauseDetails=" + exceptionCauseDetails +
        '}';
  }
}
