package com.getinsured.hix.agency.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.getinsured.hix.agency.service.AgencyDocumentService;
import com.getinsured.hix.agency.service.AgencyService;
import com.getinsured.hix.agency.service.AgencySiteService;
import com.getinsured.hix.agency.util.AgencyDtoMapper;
import com.getinsured.hix.agency.util.DesignateBrokerAuditTask;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessExportDTO;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessExportResponseDTO;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyCertificationStatusDto;
import com.getinsured.hix.dto.agency.AgencyDelegationListDTO;
import com.getinsured.hix.dto.agency.AgencyDocumentDto;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencyManagerProfileDto;
import com.getinsured.hix.dto.agency.AgencyProfileDto;
import com.getinsured.hix.dto.agency.AgencyResponse;
import com.getinsured.hix.dto.agency.AgencySiteDto;
import com.getinsured.hix.dto.agency.AgentBulkTransferDTO;
import com.getinsured.hix.dto.agency.DelegatedIndividualDTO;
import com.getinsured.hix.dto.agency.SearchAgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.SearchAgencyDelegationDTO;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.broker.BrokerSiteLocationDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.Agency.CertificationStatus;
import com.getinsured.hix.model.agency.AgencyDocument;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.webservice.entity.endpoints.GenerateEntityRecordIdEndPoint;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdRequest;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse;
import com.getinsured.hix.webservice.entity.generateentityrecordid.RecordType;
import com.getinsured.hix.webservice.entity.service.GenerateEntityRecordIdService;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
@RequestMapping(value="/agency")
public class AgencyController {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AgencyController.class);
	
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	private static final String DATE_FORMAT_1 = "MMM dd, yyyy";
	private static final String AGENCY_CERTIFICATION_NUM_SEQ = "AGENCY_CERTIFICATION_NUM_SEQ";
	private static final String RESPONSE_CODE = "responseCode";
	private static final String INVALID_INPUT_PARAMETERS = "Record Type or Record Id is missing in request";
	private static final String NO_AGENCY_FOUND = "No Agency found";
	private static final String INVALID_RECORD_TYPE = "Invalid Record Type";
	
	@Autowired
	AgencyService agencyService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	AgencyDtoMapper agencyDtoMapper;
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	GenerateEntityRecordIdEndPoint generateEntityRecordIdEndPoint;
	@Autowired
	BrokerService brokerService;
	@Autowired
	private ContentManagementService ecmService;
	@Autowired
	private AgencySiteService agencySiteService;
	@Autowired
	private AgencyDocumentService agencyDocumentService;
	@Autowired
	private GenerateEntityRecordIdService generateEntityRecordIdService;
	@Autowired 
	private GhixDBSequenceUtil ghixDBSequenceUtil;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private DesignateService designateService;
	@Autowired
	private GIWSPayloadService giwsPayloadService;
	@Autowired
	private DesignateBrokerAuditTask designateBrokerAuditTask;
	
	@Value("#{configProp['agency.agent.transfer']}")
	private String agentBulkTransferUrl;
	
	@ResponseBody
	@RequestMapping(value={"/information/view/{agencyId}"},method=RequestMethod.GET)
	public AgencyInformationDto viewAgencyInformation(@PathVariable("agencyId") String agencyId,@RequestParam(value="useModuleUser",required=false) String useModuleUser){
		Agency agency = null;
		AgencyInformationDto agencyInformationDto = null;
		
		try {
			if(useModuleUser == null){
				agency = agencyService.findById(Long.parseLong(agencyId));
			}else{
				agency = agencyService.findByBrokerId(Integer.parseInt(agencyId));
			}
			agencyInformationDto = agencyService.loadAgencyManagers(agencyDtoMapper.convert(AgencyInformationDto.class,agency), agency.getId().intValue());
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing agency information response.",ex);
			throw new GIRuntimeException("Exception occured while processing agency information response.",ex);
		}
		
		return agencyInformationDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/information",method=RequestMethod.POST)
	public AgencyResponse updateAgencyInformation(@RequestBody @Valid AgencyInformationDto agencyInformationDto){
		Agency agency = null;
		AgencyResponse agencyResponse = null;
		try {
			agency = agencyDtoMapper.convert(Agency.class,agencyInformationDto);
			agency = agencyService.saveAgencyInformation(agency);
			agencyResponse = buildSuccessAgencyResponse(String.valueOf(agency.getId()));
		}catch(DataIntegrityViolationException e) {
			//e.getMessage();//could not execute statement; SQL [n/a]; constraint [CAMAINQA.UK_FEDERAL_TAX_ID]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement
			LOGGER.error("Exception occured while saving agency information response.",e);
			if(e.getMessage().indexOf("FEDERAL_TAX_ID")>0) {
				agencyResponse = new AgencyResponse();
				agencyResponse.setStatusCode("500");
				agencyResponse.setStatusMessage("An Agency with this FEIN already exists in the system.");	
			}else {
				agencyResponse = buildFailureAgencyResponse();
			}
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing agency information response.",ex);
			agencyResponse = buildFailureAgencyResponse();
		}
		return agencyResponse; 
	}

	private AgencyResponse buildSuccessAgencyResponse(String id) {
		AgencyResponse agencyResponse = new AgencyResponse();
		agencyResponse.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(id));
		agencyResponse.setStatusCode("200");
		agencyResponse.setStatusMessage("SUCCESS");
		return agencyResponse;
	}
	
	private AgencyResponse buildFailureAgencyResponse() {
		AgencyResponse agencyResponse = new AgencyResponse();
		agencyResponse.setStatusCode("500");
		agencyResponse.setStatusMessage("FAILURE");
		return agencyResponse;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/document/{agencyDocumentId}"}, method = RequestMethod.GET)
	public AgencyDocumentDto getAgencyDocument(@PathVariable("agencyDocumentId") String agencyDocumentId){
		
		AgencyDocument agencyDocument = null;
		AgencyDocumentDto agencyDocumentDto = new AgencyDocumentDto();
		
		try {
			if (agencyDocumentId != null) {
				agencyDocument = agencyDocumentService.findById(Long.valueOf(agencyDocumentId));
				if(agencyDocument!=null){
					byte[]  document = ecmService.getContentDataById(agencyDocument.getEcmDocumentId());
					
					if(document!=null){
						agencyDocumentDto.setDocument(document);
						agencyDocumentDto.setDocumentName(agencyDocument.getDocumentName());
						agencyDocumentDto.setDocumentMime(agencyDocument.getDocumentMime());
					} else {
						agencyDocumentDto.setDocumentMime("text/plain");
						agencyDocumentDto.setDocument("No Document found".getBytes());
					}
				}
				agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_SUCCESS);
			}
		} catch (ContentManagementServiceException exception) {
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Unable to download file" , exception);
			agencyDocumentDto.setDocument("Technical Error".getBytes());
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} catch(Exception exception) {
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Unable to download file" , exception);
			agencyDocumentDto.setDocument("Technical Error".getBytes());
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return agencyDocumentDto;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/document/upload"}, method = RequestMethod.POST)
	public AgencyDocumentDto uploadDocument(@RequestBody @Valid AgencyDocumentDto agencyDocumentDto) throws ContentManagementServiceException {
		AgencyDocument agencyDocument = null;
		String documentId = null;
		AgencyDocumentDto response = new AgencyDocumentDto();
		
		try {
			AccountUser user = userService.getLoggedInUser();
			
		 	// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(agencyDocumentDto.getOriginalFilename()) + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(agencyDocumentDto.getOriginalFilename());
			// upload file to ECM
			documentId = ecmService.createContent(BrokerConstants.AGENCY_DOC_PATH + BrokerConstants.SLASH + agencyDocumentDto.getAgencyId() + BrokerConstants.SLASH + BrokerConstants.FOLDER_NAME,
					modifiedFileName, agencyDocumentDto.getDocument(), ECMConstants.Agency.DOC_CATEGORY, ECMConstants.Agency.AGENCY_DOCUMENT, null);
			
			if(!StringUtils.isEmpty(documentId)){
				MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
				String mimeType = mimeTypesMap.getContentType(agencyDocumentDto.getOriginalFilename());
				
				Agency agency = agencyService.findById(agencyDocumentDto.getAgencyId());
				
				agencyDocument = new AgencyDocument();
				agencyDocument.setAgency(agency);
				agencyDocument.setDocumentName(agencyDocumentDto.getOriginalFilename());
				agencyDocument.setCreatedBy(user.getEmail());
				agencyDocument.setCreatedDate(new TSTimestamp());
				agencyDocument.setDocumentMime(mimeType);
				agencyDocument.setDocumentType(agencyDocumentDto.getDocumentType());
				agencyDocument.setEcmDocumentId(documentId);
				agencyDocument.setActiveFlag("Y");
				
				agencyDocument = agencyDocumentService.saveAgencyDocument(agencyDocument);
				
				response.setDocumentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencyDocument.getId())));
				response.setCreatedDate(new SimpleDateFormat(DATE_FORMAT_1).format(TSDate.getNoOffsetTSDate(agencyDocument.getCreatedDate().getTime())));
				response.setDocumentName(agencyDocument.getDocumentName());
				response.setStatus(BrokerConstants.RESPONSE_SUCCESS);
			}
		} catch (ContentManagementServiceException cms) {
			response.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Unable to upload file" , cms);
			throw new ContentManagementServiceException("Unable to upload file" ,cms);
		}catch (Exception exception){
			response.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Exception occured in getAssisterAdminDocDetailsbyId." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/documents/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AgencyDocumentDto> getAgencyDocuments(@PathVariable("agencyId") String agencyId){
		List<AgencyDocument> listOfAgencyDocs = null;
		List<AgencyDocumentDto> list = new ArrayList<>();
		
		try {
			if (agencyId != null) {
				listOfAgencyDocs = agencyDocumentService.findByAgencyId(Long.valueOf(agencyId));
				
				if(listOfAgencyDocs != null && listOfAgencyDocs.size()>0){
					for(AgencyDocument agencyDocument:listOfAgencyDocs){
						AgencyDocumentDto agencyDocumentDto = new AgencyDocumentDto();
						agencyDocumentDto.setDocumentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencyDocument.getId())));
						agencyDocumentDto.setCreatedDate(new SimpleDateFormat(DATE_FORMAT_1).format(TSDate.getNoOffsetTSDate(agencyDocument.getCreatedDate().getTime())));
						agencyDocumentDto.setDocumentName(agencyDocument.getDocumentName());
						
						list.add(agencyDocumentDto);
					}
				}
			}
		} catch(Exception exception) {
			LOGGER.error("Error occurred while retrieving agency documents : " , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return list;
	}
	
	@ResponseBody
	@RequestMapping(value = "/document/delete/{documentId}", method = RequestMethod.POST)
	public AgencyDocumentDto deleteDocument(@PathVariable("documentId") String documentId) {
		AgencyDocumentDto agencyDocumentDto = new AgencyDocumentDto();
		try {
			AgencyDocument agencyDocument = agencyDocumentService.findById(Long.valueOf(documentId));
			agencyDocument.setActiveFlag("N");
			agencyDocumentService.saveAgencyDocument(agencyDocument);
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_SUCCESS);

		} catch (Exception exception) {
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Exception occured while deleting Agency document " , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return agencyDocumentDto;
	}

	@ResponseBody
	@RequestMapping(value="/ind65",method=RequestMethod.GET)
	public AgencyResponse ind65(){
		AgencyResponse agencyResponse = new AgencyResponse();
		try {
			AccountUser user = userService.getLoggedInUser();
			Broker broker = brokerService.getBrokerDetailByUserId(user.getId());
				if(broker !=null){
					agencyResponse.setId(String.valueOf(broker.getId()));
					return agencyResponse;
				}
			GenerateEntityRecordIdRequest request = new GenerateEntityRecordIdRequest();
			request.setAddressLine1("1120 N Street ");
			request.setCity("Sacramento");
			request.setState("CA");
			request.setZipCode(GenerateRandomNumber(5));
			
			request.setAgentLicenseNo(GenerateRandomNumber(7));
			request.setFederalEIN(GenerateRandomNumber(9));
			request.setBusinessLegalName("Michell Clark"+(int) (Math.random() * (4 - 1)) + 1);
			request.setEmail("john@ca.com");
			request.setFirstName("Test");
			request.setLastName("Agency"+(int) (Math.random() * (5 - 3)) + 3);
			request.setRecordType(RecordType.AGENCY_MANAGER);
//			request.setStateEIN(GenerateRandomNumber(9));
			GenerateEntityRecordIdResponse response = generateEntityRecordIdService.createAgencyAndAgentRecords(request, new GenerateEntityRecordIdResponse());
			if(response!=null){
				Broker brkObj = brokerService.findById(response.getRecordID());
				brkObj.setUser(user);
				brokerService.saveBroker(brkObj);
			}
			agencyResponse.setId(String.valueOf(response.getRecordID()));
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing agency information response.",ex);
			agencyResponse = buildFailureAgencyResponse();
		}
		
		return agencyResponse; 
	}
	
	String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }
	
	@ResponseBody
	@RequestMapping(value={"/certification/view/{agencyId}"},method=RequestMethod.GET)
	public AgencyCertificationStatusDto viewCertificationStatus(@PathVariable("agencyId") String agencyId){
		Agency agency = null;
		AgencyCertificationStatusDto agencyCertificationStatusDto = null;
		AccountUser user = null;
		try {
			if(agencyId!=null){
				agency = agencyService.findById(Long.parseLong(agencyId));
				if(agency!=null){
					user = userService.getLoggedInUser();
					//Change Certification status to PENDING for Agency and Agency manager if Certification status is INCOMPLETE
					if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) &&
							(agency.getCertificationStatus().equals(CertificationStatus.INCOMPLETE) || agency.getCertificationStatus() == null)){
						agency.setCertificationStatus(CertificationStatus.PENDING);
						agency.setLastUpdatedBy(Long.valueOf(userService.getLoggedInUser().getId()));
						agency.setLastUpdateTimestamp(new TSTimestamp());
						agencyService.saveAgencyAgentCertificationStatus(agency);
					}
					
					agencyCertificationStatusDto = new AgencyCertificationStatusDto();
					if (agency.getCertificationStatus()!=null){
						agencyCertificationStatusDto.setCertificationStatus(agency.getCertificationStatus().toString());
					}
					if (agency.getCertificationNumber() != null) {
						agencyCertificationStatusDto.setCertificationNumber(agency.getCertificationNumber().toString());
					}
					SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
					if(agency.getCertificationDate()!=null && agency.getCertificationStatus().equals(CertificationStatus.CERTIFIED)){
						agencyCertificationStatusDto.setCertificationDate(dateFormat.format(agency.getCertificationDate()));
					}
					if(agency.getRecertifyDate()!=null){
						agencyCertificationStatusDto.setRecertifyDate(dateFormat.format(agency.getRecertifyDate()));
					}
					if (agency.getCreationTimestamp() != null) {
						agencyCertificationStatusDto.setCreationDate(dateFormat.format(agency.getCreationTimestamp()));
					}
					agencyCertificationStatusDto.setStatus(BrokerConstants.RESPONSE_SUCCESS);
				}
			}
		} catch (Exception ex) {
			agencyCertificationStatusDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Exception occured while processing agency certification status.",ex);
			throw new GIRuntimeException("Exception occured while processing agency certification status.",ex);
		}
		
		return agencyCertificationStatusDto;
	}
	
	@ResponseBody
	@RequestMapping(value={"/getAgencyForBroker/{userId}"},method=RequestMethod.GET)
	public AgencyInformationDto getAgencyForBroker(@PathVariable("userId") String userId){
		Agency agency = null;
		Broker broker = null;
		AgencyInformationDto agencyInformationDto = null;
		try {
			if(userId != null){
				broker = brokerService.getBrokerDetailByUserId(Integer.parseInt(userId));
				if(broker!=null){
					agency = agencyService.findByBrokerId(broker.getId());
					agencyInformationDto = agencyDtoMapper.convert(AgencyInformationDto.class,agency);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while retrieving agency response.",ex);
			throw new GIRuntimeException("Exception occured while retrieving agency response.",ex);
		}
		
		return agencyInformationDto;
	}
	
	
	@ResponseBody
	@RequestMapping(value={"/site/list/{agencyId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AgencySiteDto> getAgencySiteList(@PathVariable("agencyId") String agencyId){
		List<AgencySiteDto> agencySiteDtos = null;
		try {
			List<AgencySite> agencySites = agencySiteService.getSiteByAgencyId(Long.parseLong(agencyId));
			agencySiteDtos = agencyDtoMapper.convertCollection(AgencySiteDto.class, agencySites);
			return agencySiteDtos;
		}catch(Exception ex){
			LOGGER.error("Exception occured while fetching agency site information.",ex);
			throw new GIRuntimeException("Exception occured while fetching agency site information.",ex);
		}
		
	}
	
	
	@ResponseBody
	@RequestMapping(value={"/site/view/{siteId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencySiteDto getAgencySiteList(@PathVariable("siteId") Long siteId){
		AgencySiteDto agencySiteDto = null;
		try {
			AgencySite agencySite= agencySiteService.findSiteById(siteId);
			agencySiteDto = agencyDtoMapper.convert(AgencySiteDto.class, agencySite);
			return agencySiteDto;
		}catch(Exception ex){
			LOGGER.error("Exception occured while fetching agency site information.",ex);
			throw new GIRuntimeException("Exception occured while fetching agency site information.",ex);
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value={"/site"},method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencySiteDto getAgencySiteList(@RequestBody @Valid AgencySiteDto agencySiteDto){
		AgencySite agencySite = null;
		AgencySiteDto responseDto = null;
		try {
			agencySite = agencyDtoMapper.convert(AgencySite.class, agencySiteDto);
			agencySite= agencySiteService.saveOrUpdate(agencySite);
			responseDto = agencyDtoMapper.convert(AgencySiteDto.class, agencySite);
			return responseDto;
		}catch(Exception ex){
			LOGGER.error("Exception occured while saving agency site information.",ex);
			throw new GIRuntimeException("Exception occured while saving agency site information.",ex);
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value={"/broker/location"},method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public BrokerResponse populateBrokerWithLocation(@RequestBody BrokerSiteLocationDTO brokerSiteLocationDTO){
		Broker broker = null;
		BrokerResponse br = null;
		try {
			AgencySite agencySite= agencySiteService.findSiteById(brokerSiteLocationDTO.getSiteId());
			if(agencySite!=null && agencySite.getLocation()!=null){
				broker = brokerService.findById(brokerSiteLocationDTO.getId());
				if(broker!=null){
				broker.setLocation(agencySite.getLocation());
				brokerService.saveBroker(broker);
			}
				br = buildSuccessBrokerResponse(brokerSiteLocationDTO.getId());
			br.setLocationId(agencySite.getLocation().getId());
			}
			
			
			return br;
		}catch(Exception ex){
			LOGGER.error("Exception occured while save broker object with agency site location information.",ex);
			return buildFailureBrokerResponse();
		}
		
	}
	
	private BrokerResponse buildSuccessBrokerResponse(long id) {
		BrokerResponse brokerResponse = new BrokerResponse();
		brokerResponse.setBrokerId(id);
		brokerResponse.setResponseCode(200);
		brokerResponse.setResponseDescription("SUCCESS");
		return brokerResponse;
	}
	
	private BrokerResponse buildFailureBrokerResponse() {
		BrokerResponse brokerResponse = new BrokerResponse();
		brokerResponse.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
		brokerResponse.setResponseDescription(BrokerConstants.RESPONSEDESCFAILURE);
		return brokerResponse;
	}
	
	@ResponseBody
	@RequestMapping(value={"/certification"},method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyCertificationStatusDto updateCertificationStatus(@RequestBody @Valid AgencyCertificationStatusDto agencyCertificationStatusDto){
		try {
			Long agencyId = Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyCertificationStatusDto.getId()));
			Agency agency = agencyService.findById(agencyId);
			agency.setCertificationStatus(CertificationStatus.getCertificationStatus(agencyCertificationStatusDto.getCertificationStatus()));
			
			if(CertificationStatus.CERTIFIED.toString().equalsIgnoreCase(agencyCertificationStatusDto.getCertificationStatus())){
				agency.setCertificationNumber(Long.valueOf(generateAgencyCertificationNumber()));
				agency.setCertificationDate(new TSTimestamp());
			}
			
			if(!BrokerUtils.isEmpty(agencyCertificationStatusDto.getDeclarationDocId())){
				agency.setEoDeclarationDocumentId(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyCertificationStatusDto.getDeclarationDocId())));
			} else {
				agency.setEoDeclarationDocumentId(null);
			}
			if(!BrokerUtils.isEmpty(agencyCertificationStatusDto.getSupportDocId())){
				agency.setSupportDocumentId(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyCertificationStatusDto.getSupportDocId())));
			} else {
				agency.setSupportDocumentId(null);
			}
			if(!BrokerUtils.isEmpty(agencyCertificationStatusDto.getContractDocId())){
				agency.setContractDocumentId(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyCertificationStatusDto.getContractDocId())));
			} else {
				agency.setContractDocumentId(null);
			}
			
			if(!BrokerUtils.isEmpty(agencyCertificationStatusDto.getComment())){
				Comment comment = agencyService.saveComments(String.valueOf(agencyId), agencyCertificationStatusDto.getComment());
				if(comment!=null){
					agency.setCommentsId(Long.valueOf(comment.getId()));
				}
			}else {
				agency.setCommentsId(null);
			}
			AccountUser user = userService.getLoggedInUser();
			agency.setLastUpdatedBy(Long.valueOf(user.getId()));
			agency.setLastUpdateTimestamp(new TSTimestamp());

			agencyService.updateAgencyCertificationStatus(agency);
			
			return agencyService.loadAgencyCertificationHistory(agencyId);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving agency certification status.", ex);
			throw new GIRuntimeException("Exception occured while saving agency certification status.", ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value={"/certificationhistory/{agencyId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyCertificationStatusDto getAgencyCertificationHistory(@PathVariable("agencyId") Long agencyId){
		return agencyService.loadAgencyCertificationHistory(agencyId);
	}
	
	private String generateAgencyCertificationNumber() {
		return ghixDBSequenceUtil.getNextSequenceFromDB(AGENCY_CERTIFICATION_NUM_SEQ);
	}
	
	@ResponseBody
	@RequestMapping(value = {"/detail/{recordType}/{encryptedId}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AgencyProfileDto getAgencyProfileInformation(@PathVariable("encryptedId") String encryptedId, 
														@PathVariable("recordType") String recordType) {
		
		LOGGER.info("Inside AGENCY/DETAIL API START ");
		AgencyProfileDto agencyProfileDto = new AgencyProfileDto();
		String id = null;

		try {
			if (encryptedId != null & encryptedId.length() > 0 && StringUtils.isNotEmpty(recordType)) {
				try {					
					id = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
				} catch(Exception ex){
					LOGGER.error("Exception occured while decrypting broker id.", ex);
					throw new GIRuntimeException("Exception occured while decrypting broker id", ex);
				}
				
				LOGGER.info("Inside AGENCY/DETAIL API , Broker/Staff Id : " + id);
				if(id != null && id.length() > 0){
					if(BrokerConstants.RECORD_TYPE_BROKER.equalsIgnoreCase(recordType)){
						Agency agency = agencyService.findByBrokerId(Integer.parseInt(id));
						agencyProfileDto = processAgencyProfile(agency, id);
					} else if (BrokerConstants.RECORD_TYPE_STAFF.equalsIgnoreCase(recordType)){
						Agency agency = agencyService.findByAgencyAssistantId(Long.valueOf(id));
						agencyProfileDto = processAgencyProfile(agency, id);
					} else {
						setResponseCode(BrokerConstants.RESPONSECODEFAILURE, INVALID_RECORD_TYPE, agencyProfileDto);
					}
				}
			} else {
				LOGGER.error("Record Type or Record Id is missing : Record Type : "+recordType + " : Record ID : "+encryptedId);
				setResponseCode(BrokerConstants.RESPONSECODEFAILURE, INVALID_INPUT_PARAMETERS, agencyProfileDto);
			}
		} catch (Exception ex) {
			setResponseCode(BrokerConstants.RESPONSECODEFAILURE, BrokerConstants.RESPONSEDESCFAILURE, agencyProfileDto);
			LOGGER.error("Exception occured while retrieving agency response.", ex);
		}
		
		LOGGER.info("Inside AGENCY/DETAIL API END ");
		return agencyProfileDto;
	}
	
	private AgencyProfileDto processAgencyProfile(Agency agency, String id){
		AgencyProfileDto agencyProfileDto = new AgencyProfileDto();
		if (agency != null && agency.getId() != 0) {
			List<Broker> agencyManagers = brokerService.findByAgencyManagers(agency.getId());
			LOGGER.info("Inside AGENCY/DETAIL API , BusinessLegalName : " + agency.getBusinessLegalName());
			if(agencyManagers!=null && !agencyManagers.isEmpty()){
				List<AgencyManagerProfileDto> managers = new ArrayList<>();
				for(Broker broker:agencyManagers){
					AgencyManagerProfileDto agencyManagerProfileDto =  agencyDtoMapper.convert(AgencyManagerProfileDto.class, broker);
					managers.add(agencyManagerProfileDto);
				}
				agencyProfileDto = agencyDtoMapper.convert(AgencyProfileDto.class, agency);
				agencyProfileDto.setAgencyManagers(managers);
			} else{
				LOGGER.info("Inside AGENCY/DETAIL API , NO AGENCY MANAGER EXISTS FOR BrokerId : " + id);
				agencyProfileDto =  agencyDtoMapper.convert(AgencyProfileDto.class, agency);
			}
			
			setResponseCode(BrokerConstants.RESPONSECODESUCCESS, BrokerConstants.RESPONSEDESCSUCCESS, agencyProfileDto);
		} else {
			LOGGER.info("Inside AGENCY/DETAIL API , NO AGENCY EXISTS FOR BrokerId : " + id);
			setResponseCode(BrokerConstants.RESPONSECODEFAILURE, NO_AGENCY_FOUND, agencyProfileDto);
		}
		
		return agencyProfileDto;
	}
	
	private AgencyProfileDto setResponseCode(int responseCode, String responseDescription, AgencyProfileDto agencyProfileDto){
		agencyProfileDto.setResponseCode(responseCode);
		agencyProfileDto.setResponseDescription(responseDescription);
		
		return agencyProfileDto;
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public void handleValidationException(MethodArgumentNotValidException ex){
		try{
		 AccountUser user = userService.getLoggedInUser();
		 giMonitorService.saveOrUpdateErrorLog(GIRuntimeException.ERROR_CODE_UNKNOWN, new TSDate(), this.getClass().getName(),ExceptionUtils.getFullStackTrace(ex),
				 user, getUrl(), GIRuntimeException.Component.AEE.getComponent(),null);
		}catch(Exception e){
			LOGGER.error("Exception occured while saving into gi_monitor.",e);
		}
	}

	private String getUrl() {
		 RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		 HttpServletRequest servletRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
		 return servletRequest.getRequestURI();
	}
	
	@ResponseBody
	@RequestMapping(value = {"/detail/ver2/{encryptedBrokerId}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AgencyProfileDto getAgencyProfileInformationVer2(
	@PathVariable("encryptedBrokerId") String encryptedBrokerId) {
		
		LOGGER.info("Inside AGENCY/DETAIL API START ");
		AgencyProfileDto agencyProfileDto = null;
		String brokerId = null;

		try {

			if (encryptedBrokerId != null & encryptedBrokerId.length() > 0) {
				brokerId = encryptedBrokerId;
//				try{					
//						brokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedBrokerId);
//				}catch(Exception ex){
//					LOGGER.error("Exception occured while decrypting broker id.", ex);
//					throw new GIRuntimeException("Exception occured while decrypting broker id", ex);
//				}
//				
				LOGGER.info("Inside AGENCY/DETAIL API , BrokerId : " + brokerId);
				if(brokerId != null && brokerId.length() > 0){
					
					//Broker brkObj = brokerService.findById(Integer.parseInt(brokerId));
					
					Agency agency = agencyService.findByBrokerId(Integer.parseInt(brokerId));
					
					if (agency != null && agency.getId() != 0) {
						Broker agencyManager = brokerService.findByAgencyManagerInfo(agency.getId());
						LOGGER.info("Inside AGENCY/DETAIL API , BusinessLegalName : " + agency.getBusinessLegalName());
						agencyProfileDto =  agencyDtoMapper.convert(AgencyProfileDto.class, agency, agencyManager);
						//agencyProfileDto.setBrokerId(String.valueOf(agencyManager.getId())); // This is agencyManagerId of broker for which request is made.
						
					} else {
						LOGGER.info("Inside AGENCY/DETAIL API , NO AGENCY EXISTS FOR BrokerId : " + brokerId);
					}
				}
			}
			LOGGER.info("Inside AGENCY/DETAIL API END ");
		} catch (Exception ex) {
			LOGGER.error("Exception occured while retrieving agency response.", ex);
			throw new GIRuntimeException("Exception occured while retrieving agency response.", ex);
		}
		return agencyProfileDto;
	}
	
	@ResponseBody
	@RequestMapping(value={"/approveRequest/{brokerId}/{individualId}"}, method=RequestMethod.POST)
	public String approveRequest(@PathVariable("brokerId") Integer brokerId, @PathVariable("individualId") Integer individualId){
		String status = BrokerConstants.RESPONSE_FAILURE;
		try {
			if(brokerId!=null && brokerId!=0 && individualId!=null && individualId!=0){
				AccountUser user = userService.getLoggedInUser();
				status = agencyService.approveRequest(brokerId, individualId, user);
			} 
		} catch (Exception ex) {
			LOGGER.error("Exception occured while approving Individual request by Agency.", ex);
		}
		
		return status;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/delegations/{designationStatus}"}, method = { RequestMethod.GET, RequestMethod.POST },produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyDelegationListDTO getAllDelegations( @PathVariable("designationStatus") String designationStatus,@RequestBody  SearchAgencyDelegationDTO searchAgencyDelegationDTO)  {
		 
		try {
			return agencyService.findAllDelegations(searchAgencyDelegationDTO);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while retrieving delegation list for agency.", ex);
			throw new GIRuntimeException("Exception occured while retrieving delegation list for agency.", ex);
		}
		 
	}
	
	@RequestMapping(value = "/agentbulktransfer", method = RequestMethod.POST)
	@ResponseBody
	public String agentBulkTransfer(@RequestBody AgentBulkTransferDTO agentBulkTransferDTO) {
		JsonParser parser = new JsonParser();
		String status = BrokerConstants.RESPONSEDESCFAILURE;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			SimpleModule module = new SimpleModule();
			module.addSerializer(AgentBulkTransferDTO.class, new AgentBulkTransferDTOSerializer( ));
			mapper.registerModule(module);
			String request = mapper.writeValueAsString(agentBulkTransferDTO);
			
			LOGGER.info("URL : "+agentBulkTransferUrl+" : Request : "+ request);
			
			String response = ghixRestTemplate.postForObject(agentBulkTransferUrl, request, String.class);
			
			giwsPayloadService.save(populateGiWSPayload(request, response, agentBulkTransferUrl, BrokerConstants.ENDPOINT_FUNCTION));
			
			LOGGER.info(" Bulk Transfer Response : "+ response);
			
			AccountUser user = userService.getLoggedInUser();
			
			JsonObject object = parser.parse(response).getAsJsonObject();
			if(object!=null && object.get(RESPONSE_CODE)!=null && "200".equalsIgnoreCase(object.get(RESPONSE_CODE).getAsString())){
				if(agentBulkTransferDTO.getAgentIds()!=null && !agentBulkTransferDTO.getAgentIds().isEmpty() && agentBulkTransferDTO.getTargetAgentId()!=null){
					int brokerId = Integer.valueOf( getEncryptedId(   agentBulkTransferDTO.getAgentIds().get(0) ,"brokerId")   );
					int targetBrokerId = Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agentBulkTransferDTO.getTargetAgentId()));
					designateService.updateByBrokerId(brokerId,targetBrokerId, user);
					designateBrokerAuditTask.processAudit(targetBrokerId, user);
					designateService.sendAgentTransferToEnrollment(brokerId, targetBrokerId, user.getUserName());
				} else if(agentBulkTransferDTO.getTargetAgentId()!=null && agentBulkTransferDTO.getIndividualIds()!=null){
					List<DelegatedIndividualDTO> delegatedIndividuals = agentBulkTransferDTO.getIndividualIds();
					for(DelegatedIndividualDTO delegation:delegatedIndividuals){
						designateService.updateByBrokerIdAndIndividualId(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(delegation.getAgentId())), 
								Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(delegation.getIndividualId())), 
								Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agentBulkTransferDTO.getTargetAgentId())), user);
					}
				}
				status = BrokerConstants.RESPONSEDESCSUCCESS;
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while transferring consumers.", ex);
		}
		
		return status;
	}
	
	private GIWSPayload populateGiWSPayload(String request, String response, String endpointUrl, String endpointFunction) {
		GIWSPayload newGiWSPayload = new GIWSPayload();
		newGiWSPayload.setCreatedTimestamp(new TSDate());
		newGiWSPayload.setEndpointUrl(endpointUrl);
		newGiWSPayload.setRequestPayload(request);
		newGiWSPayload.setResponsePayload(response);
		newGiWSPayload.setEndpointFunction(endpointFunction);
		
		return newGiWSPayload;
	}
	
	@RequestMapping(value = {"/ahbx/manageDelegate-service/v1/manageDelegate/alt/transfer"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String test(@RequestBody String string) {
		String payload = null;
		
		try {
			System.out.println(string);
			
			JsonObject data = new JsonObject();
			data.addProperty(RESPONSE_CODE, 200);

			Gson gson = new Gson();
			payload = gson.toJson(data);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while transferring consumers.", ex);
		}
		
		return payload;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/bookofbusiness"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyBookOfBusinessDTO loadAgencyBookOfBusiness(@RequestBody SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO,@RequestParam(value="aManCertStatus",required=false) String aManCertStatus ){
		
		try {
			if(StringUtils.isEmpty(searchAgencyBookOfBusinessDTO.getSortBy())  ){
			    searchAgencyBookOfBusinessDTO.setSortBy("FIRSTNAME , LASTNAME ");
			    searchAgencyBookOfBusinessDTO.setSortOrder("ASC");
			   } else if("FIRSTNAME".equalsIgnoreCase(searchAgencyBookOfBusinessDTO.getSortBy())){
			    searchAgencyBookOfBusinessDTO.setSortBy("FIRSTNAME "+searchAgencyBookOfBusinessDTO.getSortOrder()+" , LASTNAME ");
			   } else if("LASTNAME".equalsIgnoreCase(searchAgencyBookOfBusinessDTO.getSortBy())){
			    searchAgencyBookOfBusinessDTO.setSortBy("LASTNAME "+searchAgencyBookOfBusinessDTO.getSortOrder()+" , FIRSTNAME ");
			   }
			AgencyBookOfBusinessDTO tAgencyBookOfBusinessDTO=agencyService.findAllIndividuals(searchAgencyBookOfBusinessDTO);
			tAgencyBookOfBusinessDTO.setAgencyManagerCertificationStatus(aManCertStatus);
			return tAgencyBookOfBusinessDTO;
		} catch (Exception ex) {
			LOGGER.error("Exception occured while retrieving Agency BOB-Details.", ex);
			throw new GIRuntimeException("Exception occured while retrieving Agency BOB-Details.", ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/searchagents"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyBrokerListDTO searchAgents(@RequestBody SearchBrokerDTO searchBrokerDTO ){
	
		try {
			return agencyService.searchAgents(searchBrokerDTO);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while searching Agency agents.", ex);
			throw new GIRuntimeException("Exception occured while searching Agency agents.", ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/noofenrollees/{enrollmentId}"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public int findNoOfEnrolless(@PathVariable("enrollmentId") String enrollmentId){
	
		try {
			enrollmentId = ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentId);
			return agencyService.findNoOfEnrollees(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentId)));
		} catch (Exception ex) {
			LOGGER.error("Exception occured while finding number of enrollees.", ex);
			throw new GIRuntimeException("Exception occured while finding number of enrollees.", ex);
		}
	}
	
	private String getEncryptedId(String map,String keyId){
		Map<String, String> encParams = GhixAESCipherPool.decryptParameterMap(map);
		return encParams.get(keyId);
	}
	
	
	class AgentBulkTransferDTOSerializer extends StdSerializer<AgentBulkTransferDTO>{
		
		 public AgentBulkTransferDTOSerializer( ) {
		        this(null );
		    }
		   
		    public AgentBulkTransferDTOSerializer(Class<AgentBulkTransferDTO> t ) {
		        super(t);
		    }
		    
		    @Override
		    public void serialize(  AgentBulkTransferDTO value, JsonGenerator jgen, SerializerProvider provider)   throws IOException, JsonProcessingException {
		    	jgen.writeStartObject();
		    	
		    	if(null != value.getAgentIds()) {
		    		jgen.writeArrayFieldStart("agentIds");
		    		
		    		for(String agentIdsStr :value.getAgentIds()) {
		    			jgen.writeNumber(Integer.parseInt(getEncryptedId(agentIdsStr,"brokerId")));
		    		}
		    		
		    		jgen.writeEndArray();
		    	}
		    	 
		    	if(null != value.getTargetAgentId()) {
		    		jgen.writeNumberField("targetAgentId",  Integer.parseInt( ghixJasyptEncrytorUtil.decryptStringByJasypt(value.getTargetAgentId()) ) );
		    	}
		        jgen.writeStringField("action", value.getAction());
		        
		        if(null != value.getIndividualIds()) {
		        	jgen.writeArrayFieldStart("individualIds");
		        	
		        	
		        	for(DelegatedIndividualDTO localDelegatedIndividualDTO :  value.getIndividualIds()) {
		        		jgen.writeStartObject();
		        		
		        		if(null != localDelegatedIndividualDTO.getAgentId()) {
		        			jgen.writeNumberField("agentId", Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(localDelegatedIndividualDTO.getAgentId())));
		        		}
		        		
		        		if(null != localDelegatedIndividualDTO.getIndividualId()) {
		        			jgen.writeNumberField("individualId", Integer.parseInt( ghixJasyptEncrytorUtil.decryptStringByJasypt(localDelegatedIndividualDTO.getIndividualId())));
		        		}
		        		
		        		jgen.writeStringField("designationStatus", localDelegatedIndividualDTO.getDesignationStatus());
		        		
		        		jgen.writeEndObject();
		        	}
		        	
		        	jgen.writeEndArray();
		        }
		        
		        jgen.writeEndObject();
		    }
		    
	}
	
	@ResponseBody
	@RequestMapping(value = {"/ahbx/manageDelegate-service/v1/manageDelegate/alt/bookofbusiness"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public AgencyBookOfBusinessExportResponseDTO test(@RequestBody AgencyBookOfBusinessExportDTO agencyBookOfBusinessExportDTO){
		AgencyBookOfBusinessExportResponseDTO testDto = new AgencyBookOfBusinessExportResponseDTO();
		StringBuffer strWriter;
		try {
			strWriter = new StringBuffer("No individuals found in Agent's Book of Business.");
			strWriter.trimToSize();
			
			testDto.setResponseCode(200);
			testDto.setStatus("success");
			testDto.setByteStream(getBytes());
		} catch (Exception ex) {
			LOGGER.error("Exception occured while creating agency bob csv.", ex);
			throw new GIRuntimeException("Exception occured while creating agency bob csv.", ex);
		}
		
		return testDto;
	}
	
	private byte[] getBytes(){
		final String COMMA_DELIMITER = ",";
		String NEW_LINE_SEPARATOR = "\n";
		final String FILE_HEADER = "id,firstName,lastName,gender,age";
			
		FileWriter fileWriter = null;
		byte[] data = null;
		
		try {
			fileWriter = new FileWriter("d:\\test.csv");
			fileWriter.append(FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			fileWriter.append(String.valueOf(9899));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Chetan");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Bhagat");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Male");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf(23));
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.flush();
			
			Path fileLocation = Paths.get("d:\\test.csv");
			data = Files.readAllBytes(fileLocation);
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		
		return data;
	}
}

