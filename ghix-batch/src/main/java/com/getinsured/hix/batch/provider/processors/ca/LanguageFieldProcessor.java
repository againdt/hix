package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class LanguageFieldProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException {
		List<String> providerLanguage = new ArrayList<String>();
		String languageData = objToBeValidated.toString();

		if(languageData.contains("|")){
			String[] languagesSpoken = languageData.split("\\|");
			for(int i=0;i<languagesSpoken.length;i++){
				String[] languageAndLocation = languagesSpoken[i].split("-");
				if(languageAndLocation.length<=2){
					providerLanguage.add(languageAndLocation[0].trim());
				}else{
					for(int j =0;j<languageAndLocation.length-1;j++){
						providerLanguage.add(languageAndLocation[j].trim());
					}
				}
			}
		}else{
			String[] languageAndLocation = languageData.split("-");
			if(languageAndLocation.length<=2){
				providerLanguage.add(languageAndLocation[0].trim());
			}else{
				for(int j =0;j<languageAndLocation.length-1;j++){
					providerLanguage.add(languageAndLocation[j].trim());
				}
			}
		}
		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null)? null: (String)tmp;
		if(outputField == null){
			//Not available in metadata file, create a default one
			outputField = "languages_spoken";
			this.context.addContextInfo("output_field", outputField);
		}
		if(providerLanguage.size() > 0){
			context.addContextInfo(outputField, providerLanguage);
		}

		return objToBeValidated;

	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
