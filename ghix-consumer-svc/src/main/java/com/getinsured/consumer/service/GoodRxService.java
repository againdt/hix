package com.getinsured.consumer.service;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;
import com.getinsured.hix.platform.util.exception.GIException;

public interface GoodRxService {
	String API_KEY = "d6de803087";
	String API_SECRET = "pf8d4wma7SLmuPS31ZDsuw==";
	String FAIR_PRICE_SERVICE_ENDPOINT = "https://api.goodrx.com/fair-price?";
	String LOW_PRICE_SERVICE_ENDPOINT = "https://api.goodrx.com/low-price?";
	String COMPARE_PRICE_SERVICE_ENDPOINT = "https://api.goodrx.com/compare-price?";
	String DRUG_SEARCH_SERVICE_ENDPOINT = "https://api.goodrx.com/drug-search?";
	
	String getFairPrice(String args);
	String getLowPrice(String args);
	String comparePrice(String args);
	String drugSearch(String args);
	/**
	 * Following calls are internal representation
	 * of above api's
	 * @param args
	 * @return
	 * @throws ConsumerServiceException 
	 */
	GoodRxResponseDTO search(String args) throws ConsumerServiceException;
	GoodRxResponseDTO details(String args);

}
