<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link rel="stylesheet" href="/hix/resources/entity/css/bootstrap.css">
<style>
	.container {
    	background-image: none;
    	background: #fff;
	}
</style>


<!-- Content -->
    <div class="row-fluid main">
      
      <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          
          <div class="item active">
            <img src="/hix/resources/entity/images/family.png" alt="Family">
            <div class="carousel-caption">
              <h2><spring:message code="label.entity.enrollmentEntities"/></h2>
              <p><spring:message code="label.entity.helpingCaliforniaEnrollInTheHighestQualHealthPlans"/></p>
              <p><a class="btn btn-primary large" href="<c:url value="/account/signup/enrollmententity" />"><spring:message code="label.entity.firstTimeHereRegistUrEntity"/></a></p>
              <p class="small"><spring:message code="label.entity.alreadyAMember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.entity.signIn"/></a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
          
          <div class="item">
            <img src="/hix/resources/entity/images/employer.png" alt="Employer">
            <div class="carousel-caption">
              <h2><spring:message code="label.entity.enrollmentEntities"/></h2>
              <p><spring:message code="label.entity.helpingCaliforniaEnrollInTheHighestQualHealthPlans"/></p>
              <p><spring:message code="label.entity.firstTimeHere"/> <a class="btn btn-primary large" href="#"><spring:message code="label.entity.registerYourEntity"/></a></p>
              <p class="small"><spring:message code="label.entity.alreadyAMember"/><a href="#"><spring:message code="label.entity.signIn"/></a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
          
          <div class="item">
            <img src="/hix/resources/entity/images/brokers.png" alt="Brokers">
            <div class="carousel-caption">
              <h2><spring:message code="label.entity.enrollmentEntities"/></h2>
              <p><spring:message code="label.entity.helpingCaliforniaEnrollInTheHighestQualHealthPlans"/></p>
              <p><spring:message code="label.entity.firstTimeHere"/><a class="btn btn-primary large" href="#"><spring:message code="label.entity.registerYourEntity"/></a></p>
              <p class="small"><spring:message code="label.entity.alreadyAMember"/><a href="#"><spring:message code="label.entity.signIn"/></a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
        </div><!-- .carousel-inner -->
        
        <ul>
          <li class="menuItem inact act"><a href=""><spring:message code="label.entity.enrollmentEntities"/></a></li>
          <li class="menuItem inact"><a href=""><spring:message code="label.assister.assister1"/></a></li>
          <li class="menuItem inact"><a href=""><spring:message code="label.entity.agents"/></a></li>
        </ul>
        
      </div><!-- #myCarousel .carousel slide -->
    </div><!-- .row-fluid main -->
<!-- Content ENDS -->
