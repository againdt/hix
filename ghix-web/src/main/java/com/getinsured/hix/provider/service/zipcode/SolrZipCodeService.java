package com.getinsured.hix.provider.service.zipcode;

import java.util.List;


public interface SolrZipCodeService {
	
	public ZipCode findByZipCode(String zip);
	
	public List<ZipCode> getCityList(String city);
	
	public List<ZipCode> getZipList(String zipCode);
	
	public boolean validateZipAndCountyCode(String zipCode, String countyCode);
	
	public List<ZipCode> findByZip(String zip);

	public List<String> getZipByCountyCodeAndZips(List<String> zips, String countyCode);
}
