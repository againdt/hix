package com.getinsured.eligibility.ssap.integration.household.service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.util.SsapIntegrationUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

@Component(value = "vlpRuleServiceActivator")
public class VLPRuleServiceActivator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VLPRuleServiceActivator.class);
	
	@ServiceActivator
	public Message<Map<String, String>> receive(Message<String> message) throws UnsupportedEncodingException, ParseException {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		if(!stateCode.equalsIgnoreCase("CA")) {
			// Only let go further for ID
			LOGGER.info("Not doing old VLP Integration calls because state code is not CA: {}", stateCode);
			return null;
		}

		JSONParser parser = new JSONParser();
		String vlpResponse = message.getPayload().toString();
		Message<Map<String, String>> responseMessage = null;
		LOGGER.debug(">>>>>>>>>>>>>>>>>> VLP Response:" + vlpResponse);
		String ssapMember = message.getHeaders().get(SSAPIntegrationConstants.SSAP_MEMBER).toString();
		if(null != vlpResponse && vlpResponse.contains("Agency3InitVerifResponseSet")) {
			// Get the applicationIdpersonid and set it in the response as needed by the rule
			// Send SSAP and VLP JSON to rule
			JSONObject vlpResponseJSON = (JSONObject) parser.parse(vlpResponse);
			JSONObject vlpIndResponseSet = (JSONObject) vlpResponseJSON.get("Agency3InitVerifResponseSet");
			if(null != vlpIndResponseSet) {
				JSONArray vlpIndResponses = (JSONArray) vlpIndResponseSet.get("Agency3InitVerifIndividualResponse");
				if(null != vlpIndResponses && !vlpIndResponses.isEmpty()) {
					JSONObject vlpIndResponse = (JSONObject) vlpIndResponses.get(0);
					if(null != vlpIndResponse.get("ResponseMetadata")) {
						JSONObject vlpResponseMetadata = (JSONObject) vlpIndResponse.get("ResponseMetadata");
						if(null != vlpResponseMetadata.get("ResponseCode") && !SsapIntegrationUtil.isSuccessOrNonRetryableResponseCode(vlpResponseMetadata.get("ResponseCode").toString())) {
							vlpResponse = null;
						}
					}
				}
			}
			responseMessage = createVLPRuleMessage(message, vlpResponse, ssapMember);
		} else if("NO_DOCUMENT".equals(vlpResponse)) {
			responseMessage = createVLPRuleMessage(message, vlpResponse, ssapMember);
		} else {
			// Unknown error calling VLP service, still call the rule with vlp response as 'null' 
			// to mark vlp verification status as PENDING
			responseMessage = createVLPRuleMessage(message, null, ssapMember);
		}
		return responseMessage;
	}

	private Message<Map<String, String>> createVLPRuleMessage(
			Message<String> message, String vlpResponse, String ssapMember) {
		Message<Map<String, String>> responseMessage;
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("ssapJson", ssapMember);
		payload.put("vlpJson", vlpResponse);
		payload.put(
				SSAPIntegrationConstants.SSAP_APPLICATION_ID,
				message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString());
		responseMessage = MessageBuilder.withPayload(payload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
		return responseMessage;
	}
}
