package com.getinsured.hix.broker.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.repository.IDesignateBrokerRepository;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;

/**
 * Implements {@link DesignateService} to designate / de-designate employers /
 * individuals
 * 
 * @author kanthi_v
 * 
 */
@Service("designateService")
@Transactional
public class DesignateBrokerServiceImpl implements DesignateService {

	private static final String EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT = "Exception occurred while designating Agent : ";

	private static final String EXCEPTION_OCCURRED_WHILE_DE_DESIGNATING_AGENT = "Exception occurred while de-designating Agent : ";

	private static final String DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_END = "deDesignateIndividualForBroker: END";

	private static final String DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START = "deDesignateIndividualForBroker: START";
	
	private static final DateTimeFormatter MM_DD_YYYY = DateTimeFormatter.ofPattern("MM/dd/yyyy");

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignateBrokerServiceImpl.class);

	@Autowired
	private IDesignateBrokerRepository brokerDesignateRepository;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private BrokerUtils brokerUtils;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private Gson platformGson;
	@Autowired
	private IBrokerRepository iBrokerRepository;

	/**
	 * @throws Exception 
	 * @see DesignateService#deDesignateEmployerForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker deDesignateEmployerForBroker(int brokerId, int extEmployerId) throws GIException {
		
		LOGGER.info("deDesignateEmployerForBroker: START");
		
		LOGGER.debug("De-designating external employer : " + extEmployerId);
		DesignateBroker designateBroker = null;
		DesignateBroker dedesignatedBroker = null;

		try {
			// Finds record before de-designate
			designateBroker = brokerDesignateRepository.findDesigBrokerByExternalEmployerId(extEmployerId);

			// If record is found, in-activates the employer. Else returns null
			// object.
			if (designateBroker != null) {
				designateBroker.setBrokerId(brokerId);
				designateBroker.setStatus(Status.InActive);
				dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DE_DESIGNATING_AGENT,exception);
		}
	
		LOGGER.info("deDesignateEmployerForBroker: END");
		
		return dedesignatedBroker;
	}

	/**
	 * @throws Exception 
	 * @see DesignateService#deDesignateIndividualForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker deDesignateIndividualForBroker(int brokerId, int extIndividualId) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START);
		
		LOGGER.debug("De-designating external individual : " + extIndividualId);
		DesignateBroker dedesignatedBroker = null;
		DesignateBroker designateBroker = null;

		try {
			// Finds record before de-designate
			designateBroker = brokerDesignateRepository.findDesigBrokerByExternalIndividualId(extIndividualId);

			// If record is found, in-activates the individual. Else returns null
			// object.
			if (designateBroker != null) {
				Broker broker = brokerService.findById(brokerId);
				
				designateBroker.setBrokerId(brokerId);
				designateBroker.setStatus(Status.InActive);
				designateBroker.setUpdatedBy(broker.getUser().getId());
				designateBroker.setUpdated(new TSDate());
				dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
				
				EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO =  new EnrollmentBrokerUpdateDTO();
				enrollmentBrokerUpdateDTO.setAgentBrokerName(broker.getUser().getFirstName() + " " + broker.getUser().getLastName());
				enrollmentBrokerUpdateDTO.setStateEINNumber(broker.getFederalEIN());
				enrollmentBrokerUpdateDTO.setStateLicenseNumber(broker.getLicenseNumber());
				if (broker.getNpn() != null) {
					enrollmentBrokerUpdateDTO.setAgentNPN(broker.getNpn());
				}
				brokerUtils.updateEnrollmentDetails(enrollmentBrokerUpdateDTO, extIndividualId, brokerId, BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG);
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DE_DESIGNATING_AGENT, exception);
		} 
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return dedesignatedBroker;
	}
	
	/**
	 * @throws Exception 
	 * @see DesignateService#designateIndividualForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker designateIndividualForBroker(int brokerId, int extIndividualId) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START);
		
		LOGGER.debug("De-designating external individual : " + extIndividualId);
		DesignateBroker dedesignatedBroker = null;
		DesignateBroker designateBroker = null;

		try{
			Broker broker = brokerService.findById(brokerId);

			if (broker != null && broker.getUser()!=null) {
				designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndIndividualId(brokerId, extIndividualId);
				
				if(designateBroker==null){
					designateBroker = new DesignateBroker();
				}
				designateBroker.setBrokerId(brokerId);
				designateBroker.setExternalIndividualId(extIndividualId);
				designateBroker.setStatus(Status.Active);
				designateBroker.setCreatedBy(broker.getUser().getId());
				designateBroker.setCreated(new TSDate());
				designateBroker.setEsignBy(broker.getUser().getFirstName() + ' ' + broker.getUser().getLastName());
				designateBroker.setEsignDate(new TSDate());
				designateBroker.setShow_switch_role_popup('Y');
				dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
		}

		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return dedesignatedBroker;
	}
	
	/**
	 * @throws Exception 
	 * @see DesignateService#designateEmployerForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker designateEmployerForBroker(int brokerId, int extEmployerId) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START);
		
		LOGGER.debug("De-designating external employer : " + extEmployerId);
		DesignateBroker dedesignatedBroker = null;
		DesignateBroker designateBroker = null;

		try {
			Broker broker = brokerService.findById(brokerId);

			if (broker != null && broker.getUser()!=null) {
				designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndEmployerId(brokerId, extEmployerId);
				
				if(designateBroker==null){
					designateBroker = new DesignateBroker();
				}
				designateBroker.setBrokerId(brokerId);
				designateBroker.setExternalEmployerId(extEmployerId);
				designateBroker.setStatus(Status.Active);
				designateBroker.setEsignBy(broker.getUser().getFirstName() + ' ' + broker.getUser().getLastName());
				designateBroker.setEsignDate(new TSDate());
				designateBroker.setShow_switch_role_popup('Y');
				dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
		}
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return dedesignatedBroker;
	}

	/**
	 * @see DesignateService#findDeDesignateRecordForBrokerAndIndividualId(int, int)
	 */
	@Override
	public DesignateBroker findDeDesignateRecordForBrokerAndIndividualId(int brokerId, int extIndividualId) {
		LOGGER.info("findDeDesignateRecordForBrokerAndIndividualId: START");
		return brokerDesignateRepository.findDesigBrokerByAgentAndIndividualId(brokerId, extIndividualId);
	}
	
	/**
	 * @see DesignateService#findDeDesignateRecordForBrokerAndEmployerId(int, int)
	 */
	@Override
	public DesignateBroker findDeDesignateRecordForBrokerAndEmployerId(int brokerId, int extEmployerId) {
		LOGGER.info("findDeDesignateRecordForBrokerAndEmployerId: START");
		return brokerDesignateRepository.findDesigBrokerByAgentAndEmployerId(brokerId, extEmployerId);
	}
	
	/**
	 * @see DesignateService#findByIndividualAndAgentIdAndStatus(int, int, Status)
	 */
	@Override
	public DesignateBroker findByIndividualAndAgentIdAndStatus(int agentId, int extIndividualId, Status status) {
		LOGGER.info("findByIndividualAndAgentIdAndStatus: START");
		return brokerDesignateRepository.findDesigBrokerByAgentAndIndividualAndStatus(agentId, extIndividualId, status);
	}

	/**
	 * @see DesignateService#findByEmployerAndAgentIdAndStatus(int, int, Status)
	 */
	@Override
	public DesignateBroker findByEmployerAndAgentIdAndStatus(int agentId, int externalEmployerId, Status status) {
		LOGGER.info("findByEmployerAndAgentIdAndStatus: START");
		return brokerDesignateRepository.findDesigBrokerByAgentAndEmployerIdAndStatus(agentId, externalEmployerId, status);
	}
	
	

	/**
	 * @throws Exception 
	 * @see DesignateService#designateEmployerForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker designateEmployerByBroker(int brokerId, int employerId) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START);
		
		LOGGER.debug("Designating employer : " + employerId);
		DesignateBroker designatedBroker = null;
		DesignateBroker designateBroker = null;

		try {
			Broker broker = brokerService.findById(brokerId);

			if (broker != null && broker.getUser()!=null) {
				designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndEmployerId(brokerId, employerId);
				
				if(designateBroker==null){
					designateBroker = new DesignateBroker();
				}
				designateBroker.setBrokerId(brokerId);
				designateBroker.setEmployerId(employerId);
				designateBroker.setStatus(Status.Active);
				designateBroker.setEsignBy(broker.getUser().getFirstName() + ' ' + broker.getUser().getLastName());
				designateBroker.setEsignDate(new TSDate());
				designateBroker.setShow_switch_role_popup('Y');
				designatedBroker = brokerDesignateRepository.save(designateBroker);
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
		}
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return designatedBroker;
	}
	
	/**
	 * @see DesignateService#findBrokerByEmployerId(int)
	 */
	@Override
	@Transactional
	public DesignateBroker findBrokerByEmployerId(int employerId) {
		LOGGER.info("findBrokerByEmployerId : START");
		return brokerDesignateRepository.findBrokerByEmployerId(employerId);
	}
	
	@Override
	@Transactional
	public DesignateBroker findBrokerByEmployerIdAndStatus(int employerId,Status status) {
		LOGGER.info("findBrokerByEmployerIdAndStatus : START");
		return brokerDesignateRepository.findDesigBrokerByEmployerIdAndStatus(employerId,status);
	}

	@Override
	public DesignateBroker findBrokerByIndividualId(Integer individualId) {
		return brokerDesignateRepository.findDesigBrokerByIndividualId(individualId);
	}

	@Override
	public DesignateBroker saveDesignateBroker(DesignateBroker designateBroker) {
		LOGGER.info("saveDesignateBroker : START");
		return brokerDesignateRepository.save(designateBroker);
	}
	
	@Override
	@Transactional
	public void updateByBrokerId(int brokerId, int targetBrokerId, AccountUser user) {
		brokerDesignateRepository.updateDesignateBrokerByBrokerId(brokerId, targetBrokerId, new TSDate(), user.getId());
	}
	
	@Async
	@Override
	public void sendAgentTransferToEnrollment(int brokerId, int targetBrokerId, String userName) {

		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO =  new EnrollmentBrokerUpdateDTO();
		Broker broker = iBrokerRepository.findById(targetBrokerId);
		enrollmentBrokerUpdateDTO.setRoleType(BrokerConstants.AGENT_ROLE);
		enrollmentBrokerUpdateDTO.setAgentBrokerName(broker.getUser().getFirstName() + " " + broker.getUser().getLastName());
		enrollmentBrokerUpdateDTO.setStateEINNumber(broker.getFederalEIN());
		enrollmentBrokerUpdateDTO.setStateLicenseNumber(broker.getLicenseNumber());
		// ID of agent/broker or assister
		enrollmentBrokerUpdateDTO.setAssisterBrokerId(broker.getId());
		enrollmentBrokerUpdateDTO.setAddremoveAction(BrokerConstants.ADDACTIONFLAG);
		enrollmentBrokerUpdateDTO.setMarketType(BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL);
		enrollmentBrokerUpdateDTO.setBrokerTPAFlag(null);
		enrollmentBrokerUpdateDTO.setOldAssisterBrokerId(brokerId);
		LocalDate startDate = LocalDate.now().minusYears(1).with(TemporalAdjusters.firstDayOfYear());
		LocalDate endDate = LocalDate.now();
		enrollmentBrokerUpdateDTO.setTransferStartDate(MM_DD_YYYY.format(startDate));
		enrollmentBrokerUpdateDTO.setTransferEndDate(MM_DD_YYYY.format(endDate));
		enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
		callEnrollmentUpdate(enrollmentRequest,GhixEndPoints.EnrollmentEndPoints.AGENT_BOB_TRANSFER, userName);
	}

	@Override
	@Transactional
	public void updateByBrokerIdAndIndividualId(int brokerId, int externalIndividualId, int targetBrokerId, AccountUser user) throws GIException {
		
		DesignateBroker designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndIndividualId(brokerId, externalIndividualId);
		
		if (designateBroker != null) {
			designateBroker.setBrokerId(targetBrokerId);
			designateBroker.setUpdated(new TSDate());
			designateBroker.setUpdatedBy(user.getId());
			
			brokerDesignateRepository.save(designateBroker);
			
			Broker broker = iBrokerRepository.findById(targetBrokerId);
			updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, externalIndividualId, BrokerConstants.AGENT_ROLE, BrokerConstants.ADDACTIONFLAG, user.getUserName());
		}
	}
	
	
	@Override
	@Transactional
	public void updateEnrollmentDetails(Broker brokerObj,String marketType,int householdId,String roleType,String actionFlag, String userName) {
		LOGGER.info("Update Enrollment Start : Broker :  "+brokerObj+" : Market Type : "+marketType+" : Individual ID : "+householdId+" : Role Type : "+roleType+" : Action : "+actionFlag);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO =  new EnrollmentBrokerUpdateDTO();

		if(BrokerUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(null);
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(String.valueOf(householdId));
		}else{
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(String.valueOf(householdId));
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(null);
		}
		
		enrollmentBrokerUpdateDTO.setRoleType(roleType);
		enrollmentBrokerUpdateDTO.setAgentBrokerName(brokerObj.getUser().getFirstName() + " " + brokerObj.getUser().getLastName());
		enrollmentBrokerUpdateDTO.setStateEINNumber(brokerObj.getFederalEIN());
		enrollmentBrokerUpdateDTO.setStateLicenseNumber(brokerObj.getLicenseNumber());
		// ID of agent/broker or assister
		enrollmentBrokerUpdateDTO.setAssisterBrokerId(brokerObj.getId());
		enrollmentBrokerUpdateDTO.setAddremoveAction(actionFlag);
		enrollmentBrokerUpdateDTO.setMarketType(marketType);
		enrollmentBrokerUpdateDTO.setBrokerTPAFlag(null);
		if (brokerObj.getNpn() != null) {
		enrollmentBrokerUpdateDTO.setAgentNPN(brokerObj.getNpn());
		}
		enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
		callEnrollmentUpdate(enrollmentRequest,GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS, userName);
	}

	private EnrollmentResponse callEnrollmentUpdate(EnrollmentRequest enrollmentRequest,String url, String userName) {
		String response = null;
		EnrollmentResponse enrollmentResponse = null;
		try {
			LOGGER.info("GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS:"+url);
			response = ghixRestTemplate.exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentRequest).getBody();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Update Enrollment API Response : " + SecurityUtil.sanitizeForLogging(response));
			}
			
			if (null != response) {

				 enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
				 LOGGER.info("enrollmentResponse->"+enrollmentResponse);
				 if(enrollmentResponse != null	&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
						LOGGER.error("Failed to update enrollment:  Error Code : " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse.getErrCode())) + " Error Message : " + SecurityUtil.sanitizeForLogging(enrollmentResponse.getErrMsg()));
						//throw new GIRuntimeException(null, null, Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
					}
			}

		} catch(GIRuntimeException giexception) {
			LOGGER.error("Exception occured while updating enrollment details : ", giexception);
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating enrollment details : ", e);
		}
		return enrollmentResponse;
	}
	
	@Override
	public List<DesignateBroker> findByBrokerIdAndUpdatedTime(int brokerId, Date updatedDate) {
		return brokerDesignateRepository.findByBrokerIdAndUpdatedTime(brokerId, updatedDate);
	}
}
