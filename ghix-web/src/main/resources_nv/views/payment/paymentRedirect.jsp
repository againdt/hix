<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Hide all the element on the page except loader icon and message below -->
<style>
	body{background:#fff;}
	/* body > div{display:none}; */
</style>

<%-- <c:if test="${payRedirectErrorMsg != ''}">
	<div style="font: italic 14px/30px Georgia, serif;">
		<div id="errorMsgPayment" class="errorblock alert alert-error"></div>
	</div>
</c:if> --%>

<!--<section style="text-align:center; display:block !important; border:30px solid red; position:absolute; z-index:1000; top:30%; left:50%; margin-left:-250px; width:500px;" >
	<h3 id="message">Redirecting to Carrier Web site for Payment...</h3>
	<img id="image" src="../resources/img/loader.gif"/>
</section>-->

<!-- <div style="font: italic 14px/30px Georgia, serif;">
<div id="errorMsgPayment" class="errorblock alert alert-error hide" style="font: italic 14px/30px Georgia, serif;"></div>
</div> -->
<div id="errorMsgPayment" class="errorblock alert alert-error hide" style="font: italic 14px/30px Georgia, serif;"></div>
<form id="postformExternal" name="postform" method="post" action="<c:url value="${ffmEndpointURL}"/>">
<input type="hidden" name="SAMLResponse" value="${SAMLResponse}"/>
	<c:if test="${not empty relayStateUrl}">
		<input type="hidden" name="RelayState" value="${relayStateUrl}"/>
	</c:if>
</form>

<script type="text/javascript">
$( document ).ready(function() {
	if('${payRedirectErrorMsg}'!=null && '${payRedirectErrorMsg}'!='')
		{
		$("#errorMsgPayment").text('${payRedirectErrorMsg}').show();
		}
	else{
	var t=setTimeout($("#postformExternal").submit(),0);
	}
});
</script>