package com.getinsured.hix.batch.enrollment.service;

import java.io.Serializable;
import org.apache.commons.collections.Predicate;

public class EnrollmentPremiumMonthPredicate implements Predicate, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer monthNumber;
	
	/**
	 * 
	 * @param monthNumber
	 */
	public EnrollmentPremiumMonthPredicate(Integer monthNumber){
		this.monthNumber = monthNumber;
	}
	

	/**
	 * 
	 */
	@Override
	public boolean evaluate(Object object) {
		if (object instanceof Integer) {
		      int value = Integer.parseInt(object.toString());
		      return monthNumber == value ? true : false;
		    } else {
		      return false;
		    }
	}

}
