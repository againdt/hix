package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class AttestationApplication {
	 private int coverageYear;
	 private Boolean requestingFinancialAssistanceIndicator;
	 private String insuranceApplicationSecurityQuestionType;
	 private String insuranceApplicationSecurityQuestionAnswer;
	 private ArrayList < ApplicationSignature > applicationSignatures;
	 private String coverageState;
	 private LegalAttestations legalAttestations;
	 private String contactMemberIdentifier;
	 private ContactInformation contactInformation;
	 private String spokenLanguageType;
	 private String writtenLanguageType;
	 private ArrayList<Assister> applicationAssistors;
	 private ArrayList < Object > contactMethod;


	 // Getter Methods 

	 public ArrayList<Object> getContactMethod() {
		return contactMethod;
	}

	public void setContactMethod(ArrayList<Object> contactMethod) {
		this.contactMethod = contactMethod;
	}

	public int getCoverageYear() {
	  return coverageYear;
	 }

	 public Boolean getRequestingFinancialAssistanceIndicator() {
	  return requestingFinancialAssistanceIndicator;
	 }

	 public String getCoverageState() {
	  return coverageState;
	 }

	 public LegalAttestations getLegalAttestations() {
	  return legalAttestations;
	 }

	 public String getContactMemberIdentifier() {
	  return contactMemberIdentifier;
	 }

	 public ContactInformation getContactInformation() {
	  return contactInformation;
	 }

	 public String getSpokenLanguageType() {
	  return spokenLanguageType;
	 }

	 public String getWrittenLanguageType() {
	  return writtenLanguageType;
	 }

	 // Setter Methods 

	 public void setCoverageYear(int coverageYear) {
	  this.coverageYear = coverageYear;
	 }

	 public void setRequestingFinancialAssistanceIndicator(Boolean requestingFinancialAssistanceIndicator) {
	  this.requestingFinancialAssistanceIndicator = requestingFinancialAssistanceIndicator;
	 }

	 public void setCoverageState(String coverageState) {
	  this.coverageState = coverageState;
	 }

	 public void setLegalAttestations(LegalAttestations legalAttestationsObject) {
	  this.legalAttestations = legalAttestationsObject;
	 }

	 public void setContactMemberIdentifier(String contactMemberIdentifier) {
	  this.contactMemberIdentifier = contactMemberIdentifier;
	 }

	 public void setContactInformation(ContactInformation contactInformation) {
	  this.contactInformation = contactInformation;
	 }

	 public void setSpokenLanguageType(String spokenLanguageType) {
	  this.spokenLanguageType = spokenLanguageType;
	 }

	 public void setWrittenLanguageType(String writtenLanguageType) {
	  this.writtenLanguageType = writtenLanguageType;
	 }

	public String getInsuranceApplicationSecurityQuestionType() {
		return insuranceApplicationSecurityQuestionType;
	}

	public void setInsuranceApplicationSecurityQuestionType(String insuranceApplicationSecurityQuestionType) {
		this.insuranceApplicationSecurityQuestionType = insuranceApplicationSecurityQuestionType;
	}

	public String getInsuranceApplicationSecurityQuestionAnswer() {
		return insuranceApplicationSecurityQuestionAnswer;
	}

	public void setInsuranceApplicationSecurityQuestionAnswer(String insuranceApplicationSecurityQuestionAnswer) {
		this.insuranceApplicationSecurityQuestionAnswer = insuranceApplicationSecurityQuestionAnswer;
	}

	 public ArrayList<Assister> getApplicationAssistors() {
		return applicationAssistors;
	}

	public void setApplicationAssistors(ArrayList<Assister> applicationAssistors) {
		this.applicationAssistors = applicationAssistors;
	}

	public ArrayList<ApplicationSignature> getApplicationSignatures() {
		return applicationSignatures;
	}

	public void setApplicationSignatures(ArrayList<ApplicationSignature> applicationSignatures) {
		this.applicationSignatures = applicationSignatures;
	}
	}
