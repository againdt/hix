
package com.getinsured.iex.ssap.enums;



/**
 * 
 * @author Dhananjay
 *
 *
 */

public enum LanguageEnum {
	
	eng("English"),
	ara("Arabic"),
	hye("Armenian"),
	fas("Farsi"),
	khmr("Cambodian"),
	cesm("Cantonese"),
	cmn("Mandarin"),
	zho("Traditional Chinese character"),
	hmn("Hmong"),
	kor("Korean"),
	rus("Russian"),
	spa("Spanish"),
	tgl("Tagalog"),
	vie("Vietnamese");
    
	private String  value;
	
	private LanguageEnum(String s) {
		this.value = s;
	}
	
	public String value() {
		return value;
	}
	
	public static String value(String v) {
		if (v != null) {
			final LanguageEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}
	
	public static String getCode(String v) {
		String  value = "";
		for (LanguageEnum c : LanguageEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				value = c.toString();
				break;
			}
		}
		return value;
	}
	
	private static LanguageEnum readValue(String v) {
		LanguageEnum data = null;
		for (LanguageEnum c : LanguageEnum.class.getEnumConstants()) {
			if (c.toString().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
	
	public static void main(String[] args) {
		System.out.println(LanguageEnum.getCode("Spanish"));
		System.out.println(LanguageEnum.spa.value());
	}
}
