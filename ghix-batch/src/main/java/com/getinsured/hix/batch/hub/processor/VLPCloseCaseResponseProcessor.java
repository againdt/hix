package com.getinsured.hix.batch.hub.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.batch.hub.exceptions.HubServiceException;
import com.getinsured.hix.batch.hub.model.HubResponse;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Component("VLPCloseCaseResponseProcessor")
public class VLPCloseCaseResponseProcessor implements ItemProcessor<HubResponse, HubResponse> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VLPCloseCaseResponseProcessor.class);
	
	private static final int MAX_REATTEMPTS = 2;
	
	@Autowired RestTemplate restTemplate;
	
	@Override
	public HubResponse process(HubResponse response) throws HubServiceException{
		
		validateResponse(response);
		
		/*
		 * If case is already closed, don't call the HUB service 
		 * This check prevents re-calling of HUB services for an already 
		 * closed case during re-processing of batch records
		 */
		if(response.getStatus().equalsIgnoreCase("CASE_CLOSED")){
		   	LOGGER.info("Case for record " + response.getId() +" is already closed. Skippping this record");
			return response;
		}
		
		StringBuilder request = new StringBuilder(); 
		request.append("{\"caseNumber\":\"");
		request.append(response.getCaseNumber());
		request.append("\"}");
		String restResponse = null;
		
		//If the REST call fails, update the retry count and skip the record
		try{
			restResponse = restTemplate.postForObject(GhixEndPoints.HubIntegrationEndpoints.VLP_CLOSE_CASE_URL, request.toString(), String.class);
			checkResponseForSuccess(restResponse);
			response.setStatus("CASE_CLOSED");
		}
		//This will catch non-success response from HUB
		catch (HubServiceException e){
			LOGGER.error(e.getMessage(),e);
			throw e;
		}
		//This will catch REST call exceptions
		catch(Exception e){
			/*
			 * Max Re-attempts to invoke the close case job is 2
			 * after which the record is put in SERVICE_NOT_REACHABLE status
			 */
			if(response.getRetryCount() >= MAX_REATTEMPTS){
				response.setStatus("SERVICE_NOT_REACHABLE");
			}
			else{
				response.setRetryCount(response.getRetryCount() + 1);
			}
		}
		
		return response;
	}
	
	/**
	 * Validates incoming HubResponse object
	 */
	private void validateResponse(HubResponse response) throws HubServiceException{
		
		String caseNumber = response.getCaseNumber();
		
		//Skip records without case numbers
		if(caseNumber == null || caseNumber.trim().length() == 0){
			LOGGER.info("Case number is null for record " + response.getId() +". Skippping this record");
			throw new HubServiceException("Case number is missing");
		}
	}
	
	/**
	 * Checks REST response for success
	 */
	private void checkResponseForSuccess(String restResponse) throws HubServiceException{
		
		//All other responses other than one with response code HS000000 are failures
		if(restResponse == null || !restResponse.contains("HS000000")){
			throw new HubServiceException("Hub Response was not a success one");
		}
		
	}

}
