<script type="text/ng-template" id="nextincarceration.jsp">
  <div class="gutter10">
    <h3><spring:message code="label.lce.incarceration"/>: <spring:message code="label.lce.completed"/></h3>
    <spring:message code="label.lce.yourinformationwillbesaved"/>

    <dl class="dl-horizontal pull-right">
      <input type="button" value="<spring:message code='label.lce.back'/>" class="btn btn-secondary" onclick="history.go(-1);return false;">
      <a class="btn btn-primary" ng-click="nextIncarceration()"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>