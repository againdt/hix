<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<c:set var="isEmailChangeEnabled" value="<%=DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_CHANGE_ENABLED)%>" />
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<style>
  .dl-horizontal dt {
    font-weight: normal;
  }
  .dl-horizontal dd {
    font-weight: bold;
  }
  #account-settings dt,
  #account-settings dd {margin-bottom: 15px; }

  #acc-settings-info dt {
    width: 160px;
  }
 #acc-settings-info dd {
    margin-left: 180px;
    font-weight: bold;
  }
  #change-email label, #change-email input,
  #change-password label, #change-password input { margin: 10px 0;}
  #change-questions .form-group{
    float: left;
    margin: 5px 0;
  }
</style>
<c:url value="/account/user/changeemail" var="theUrl">
</c:url>
<div class="gutter10" id="account-settings">
<br>
<div class="row-fluid">
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">

			<div class="errorblock alert alert-info">
							<p>${errorMsg}.</p>
			</div>


			</c:if>
			<br>
		</div>
</div>

<div class="row-fluid">
		<div style="font-size: 14px; color: green">
			<c:if test="${not empty successMsg}">
			<div class="errorblock alert alert-info">
							<p>${successMsg}.</p>
							<% session.removeAttribute("successMsg"); %>
			</div>


			</c:if>
			<br>
		</div>
</div>

  <div class="row-fluid">

    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3" id="rightpanel">
		<div class="mar-btm-20">
			<div class="header">
				<h4 style="margin-bottom:0px" class="pull-left">Account Settings</h4>
			</div>
		</div>
    	<%
    	WebApplicationContext webAppContext = RequestContextUtils.getWebApplicationContext(request);
    	MessageSource messageSource = (MessageSource)webAppContext.getBean("messageSource");
    		String passwordPolicyRegex = "";
    		String errorMsg = "";
    		if(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE)=="Y"){
    			passwordPolicyRegex = DisplayUtil.replaceDynamicValuesInMessageSource(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX));
    			errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
    		}%>
    	<input type="hidden" id="passwordPolicyRegex" name="passwordPolicyRegex" value="<%=passwordPolicyRegex%>"/>
        <input type="hidden" id="passwordPolicyErrorMsg" name="passwordPolicyErrorMsg" value="<%=errorMsg%>"/>

		<!--row1-->
		<div class="">
			<div class="row-fluid" id="acc-settings-info">
				<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 txt-right">
					<div class="acc-row">
						Password
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
					<div class="acc-row">
						&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
					<div class="acc-row">
						<a id="pwdChange" href="#change-password" data-toggle="modal" class="btn btn-small btn-fullwidth-xs" title="Press enter/space to change your password">Change Your Password</a>
					</div>
				</div>
			</div>
	<%
      		String noOfSecQstn = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.NO_OF_SECURITY_QUESTIONS);
      		pageContext.setAttribute("noOfSecQstn", noOfSecQstn);
      	%>
		</div>
		<hr>
		<!--row2-->
		<div class="">
          <div class="row-fluid">
          	<c:forEach var="currSecQstnAns" items="${userSecQstnAnsList}" varStatus="status">
				<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 txt-right">
					<div class="acc-row">
						Security Question
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
					<div class="acc-row">
						<strong class="strong">${currSecQstnAns.question}</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 txt-right">
					<div class="acc-row">
						Answer
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
					<div class="acc-row">
						&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;
					</div>
				</div>
	        </c:forEach>
          </div>
		<div class="row-fluid">
			<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
				<div class="acc-row">
					<c:choose>
						<c:when test="${noOfSecQstn == '1' }">

							<a href="#change-questions" data-toggle="modal" class="btn btn-small btn-fullwidth-xs" title="Press enter/space to change your security question">Change Your Security Question</a>
						</c:when>
						<c:otherwise>

							<a href="#change-questions" data-toggle="modal" class="btn btn-small btn-fullwidth-xs">Change Your Security Question</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<!--row3-->
	<div class="">
		<div class="row-fluid" id="acc-email-settings-info">
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 txt-right">
				<div class="acc-row">
					Email Address
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
				<div class="acc-row">
					<strong class="strong">${email}</strong>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
				<div class="acc-row">
					<c:if test="${isEmailChangeEnabled == 'TRUE'}"><a href="#change-email" data-toggle="modal" class="btn btn-small btn-fullwidth-xs">Change Your Email Address</a></c:if>
				</div>
			</div>
		</div>
	</div>
	<hr>
	</div><!--rightpanel-->
  </div><!--/row-fluid-->
</div><!--/main .gutter10-->

<!-- Modal Code Begins Here-->
<div id="change-password" class="modal hide fade  bigger-modal" tabindex="-1" role="dialog" aria-labelledby="password-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="password-modal">Change Your Password</h3>
    </div>
 	<small class="margin20-l" style="position:relative; top: 10px;">All fields on this form marked with an asterisk (*) are required unless otherwise indicated.</small>
      <userInfoView:changePassword redirectURL="/account/user/accountsettings">
      </userInfoView:changePassword>
</div><!--/.modal change password-->

<div id="change-questions" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="questions-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

      <c:choose>
      	<c:when test="${noOfSecQstn == '1' }">
      		<h3 id="questions-modal">Change Your Security Question</h3>
      	</c:when>
      	<c:otherwise>
      		<h3 id="questions-modal">Change Your Security Questions</h3>
      	</c:otherwise>
      </c:choose>
    </div>
     <userInfoView:securityQuestions redirectURL="/account/user/accountsettings"></userInfoView:securityQuestions>
</div><!--/.modal change questions-->

<div id="change-email" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="email-address-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="email-address-modal">Change Your Email Address</h3>
    </div>
    <div class="modal-body">
    	<small class="margin20-l" style="position:relative; top: 10px;">All fields on this form marked with an asterisk (*) are required unless otherwise indicated.</small>
    	<form id="change-email-address" name="change-email-address" role="form" class="form-horizontal">

			<div class="control-group">
				<label class="control-label" for="old-email-address">Current
					Email Address</label>
				<div class="controls">
					<span id="old-email-address"
						class="display_block  gutter10-t margin5-t"><strong>${email}</strong>
					</span>
				</div>
			</div>

			<div class="control-group">
				<label class="required control-label margin0" for="currentpassword">Current Password <img
					src="<c:url value="/resources/img/requiredAsterisk.png"/>"
					alt="Required!" aria-hidden="true">
				</label>

				<div class="controls">
					<input type="password" name="currentpassword" id="currentpassword"
						class="input-xlarge margin0" />
					<div id='currentpassword_error' class=''></div>
				</div>
			</div>

			<div class="control-group">
				<label class="required control-label margin0" for="newEmail">New
					Email Address <img
					src="<c:url value="/resources/img/requiredAsterisk.png"/>"
					alt="Required!" aria-hidden="true">
				</label>

				<div class="controls">
					<input type="email" name="newEmail" id="newEmail"
						class="input-xlarge margin0" />
					<div id='newEmail_error' class=''></div>
				</div>
			</div>
			<div class="control-group">
				<label class="required control-label margin0" for="confirmEmail">Confirm
					New Email Address <img
					src="<c:url value="/resources/img/requiredAsterisk.png"/>"
					alt="Required!" aria-hidden="true">
				</label>

				<div class="controls">
					<input type="email" name="confirmEmail" id="confirmEmail"
						class="input-xlarge margin0" />
					<div id='confirmEmail_error' class=''></div>
				</div>
			</div>
		</form>
    </div>
	<div class="modal-footer">
		<button class='btn pull-left' id='cancelbutton' name='cancelbutton'
			data-dismiss="modal" onclick="window.location.reload();">Cancel</button>
		<button type='button' name='changeEmail' id='changeEmail'
			aria-hidden='true' onClick='javascript:checkCurrentPasswordAndEmail();'
			value='Send Confirmation Mail' class='btn btn-primary pull-right'
			style='margin: 0px;'>Send Confirmation Mail</button>
	</div>
</div><!--/.modal change email address-->


<script type="text/javascript">
$(document).ready(function(){
// 	$('.clearForm').click(function(){
//         $(this).parents("form").find("input").val("");
// 	});


  $('div[rel=popover]').popover({trigger:'hover'});
  $('div[rel=popover]').popover({trigger:'focus'});
//  $('#pwdChange').click(function(){$(":input").val('');});
});

$('.close, .modal-backdrop').live('click',function(e){
	e.preventDefault();
    $('.modal-footer button:first').trigger('click');
});

$('.modal').on('hidden.bs.modal', function (e) {
	  $(this)
	    .find("input,textarea,select")
	       .val('')
	       .end()
	    .find("input[type=checkbox], input[type=radio]")
	       .prop("checked", "")
	       .end();
});

function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
           alert($('Session is invalid').text());;
           rv = true;
    }
    return rv;
}


var d = new Date();
$('#today_date').val((d.getMonth()+1) +'/'+ d.getDate() + '/' + d.getFullYear());

$('input[name=optionsWaive]').click(function(){
  if($('input[value=option5]').is(":checked")) {
    if ($('p#deny-insurance').length < 1){
        $('label[for=optionsWaive5]').after ('<p id="deny-insurance" class="alert-error"><small>Not having health insurance may subject you to tax penalities due to non-compliance with the individual mandate of the Affordable Care Act.</small></p>');
    }
  }

  else {
    if ($('p#deny-insurance').length >= 1){
      $('p#deny-insurance').remove();
    }
  }
});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('.error label').hide();
	}
});
</script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/user/userinfo.js" />"></script>