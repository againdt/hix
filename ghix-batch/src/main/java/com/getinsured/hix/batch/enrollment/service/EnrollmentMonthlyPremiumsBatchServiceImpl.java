/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrollmentRequotingService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
@Service("enrollmentMonthlyPremiumsBatchService")
@Transactional
public class EnrollmentMonthlyPremiumsBatchServiceImpl implements EnrollmentMonthlyPremiumsBatchService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPremiumsBatchServiceImpl.class);
	
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private EnrollmentRequotingService enrollmentRequotingService;
	@Autowired private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired private EnrollmentService enrollmentService;
	@Autowired private UserService userService;
	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPremiumsBatchService#getEnrollmentIdsForYear(java.lang.Integer)
	 */
	@Override
	public List<Integer> getEnrollmentIdsForYear(Integer year) {
		Date nextYearStartDate = EnrollmentUtils.getStartDateForYear(year+1);
		Date currentYearStartDate = EnrollmentUtils.getStartDateForYear(year);
		List<Integer> enrollmentIds = enrollmentRepository.getEnrollmentsForPremiumUpdate(DateUtil.dateToString(currentYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(nextYearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		return enrollmentIds;
	}
	
	@Override
	public List<Integer> getDeltaEnrollmentIdsForYear(Integer year) {
		String currentYearStartDate = DateUtil.dateToString(EnrollmentUtils.getStartDateForYear(year), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
		String nextYearStartDate = DateUtil.dateToString(EnrollmentUtils.getStartDateForYear(year+1), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
		
		List<Integer> enrollmentIds = enrollmentRepository.getDeltaEnrollmentsForPremiumUpdate(currentYearStartDate,nextYearStartDate,year);
		
		return enrollmentIds;
	}
	
	@Override
	public List<Integer> getEmptySLCSPEnrollmentIdsForYear(Integer year){
		String currentYearStartDate = DateUtil.dateToString(EnrollmentUtils.getStartDateForYear(year), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
		String nextYearStartDate = DateUtil.dateToString(EnrollmentUtils.getStartDateForYear(year+1), EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY);
		
		List<Integer> enrollmentIds = enrollmentPremiumRepository.getEmptySLCSPEnrollmentIdsForYear(currentYearStartDate,nextYearStartDate,year);
		return enrollmentIds;
	}
	
	@Override
	public void populateMonthlyEnrolmentPremium(Integer enrollmentId, String fillOnlySLCSP, boolean updateLastSlice) throws Exception{
		try{
			Enrollment enrollment= enrollmentRepository.findById(enrollmentId);
			AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			boolean populateMonthlyPremium= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.MONTHLY_PREMIUM_POPULATION_FLAG));
			
			if(enrollment!=null && populateMonthlyPremium){
				if(updateLastSlice){
					createEnrollmentEvents(enrollment, user);
				}
				enrollmentRequotingService.updateMonthlyPremiumForEnrollment(enrollment, updateLastSlice, true, fillOnlySLCSP, true);
				enrollment.setUpdatedOn(new Date());
				enrollment.setUpdatedBy(user);
				enrollmentRepository.save(enrollment);
			}
		}catch(GIException e){
			LOGGER.error("Error while updating monthly premium for enrollment ID :: "+enrollmentId+" "+e.getCause().getMessage());
			throw new Exception("Error while updating monthly premium for enrollment ID :: "+enrollmentId+" "+e.getCause().getMessage());
		}catch(Exception e){
			LOGGER.error("Error while updating monthly premium for enrollment ID :: "+enrollmentId+" "+e.getMessage());
			throw new Exception("Error while updating monthly premium for enrollment ID :: "+enrollmentId+" "+e.getMessage());
		}
	}
	
	private void createEnrollmentEvents(Enrollment enrollment, AccountUser user){
		for (Enrollee enrollee: enrollment.getEnrollees()){
			if(enrollee.getEnrolleeLkpValue()!=null 
					&& (EnrollmentConstants.ENROLLMENT_STATUS_PENDING.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())
							||EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode())
							|| (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollee.getEnrolleeLkpValue().getLookupValueCode()) && enrollee.getEffectiveEndDate()!=null && enrollee.getEffectiveEndDate().after(new Date())  )
						)
				){
				enrollee.setUpdatedBy(user);
				enrollmentService.createEnrolleeEvent(enrollee, EnrollmentConstants.EVENT_TYPE_CHANGE, EnrollmentConstants.EVENT_REASON_AI, user, true,false, EnrollmentEvent.TRANSACTION_IDENTIFIER.POPULATE_PREMIUM_BATCH_JOB);
			}
		}
	}

}
