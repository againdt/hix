package com.getinsured.hix.entity.assister.service;

import java.util.List;

import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.entity.DesignateAssister;

/**
 * Encapsulates service layer method calls for Assister
 */
public interface AssisterDesignateService {
	/**
	 * Saves designated assister with Pending status when individual sends
	 * designate request.
	 * 
	 * @param designateBroker
	 */
	EntityResponseDTO saveDesignateAssister(DesignateAssister designateAssister, int existingAssisterId);

	/**
	 * De-Designate Assister
	 * 
	 * @param assisterId
	 *            assister ID
	 * @return Number of de-designated individuals
	 */
	int deDesignateAssister(int assisterId);

	/**
	 * Finds Assister by individual id
	 * 
	 * @param individualId
	 *            individual ID
	 * @return DesignateAssister
	 */
	DesignateAssister findAssisterByIndividualId(int individualId);

	/**
	 * De-designates assister for external individual id.
	 * 
	 * @param assisterId
	 *            Identification number for assister
	 * @return count of dedesignated records
	 */
	int deDesignateIndividualsForAssister(int assisterId);

	/**
	 * Retrieving individuals designated to assister
	 * 
	 * @param assisterId
	 *            {@link AssisterDesignateService} identification number
	 * @return {@link List} of individual ids
	 */
	List<DesignateAssister> getDesignateIndividualsByAssisterId(int assisterId);

}
