package com.getinsured.hix.enrollment.service;

import java.util.Map;

import com.getinsured.hix.dto.enrollment.Enrollment1095DTO;
import com.getinsured.hix.dto.enrollment.Enrollment1095SearchFilter;
import com.getinsured.hix.model.enrollment.Enrollment1095;


public interface Enrollment1095EditService {

	Map<String , Object> getEnrollment1095SearchResult(Enrollment1095SearchFilter enrollment1095SearchFilter);
	Enrollment1095 getById(Integer id);
	Enrollment1095DTO getDetailsById (Integer id);
	boolean updateResendFlag(Integer id);
	boolean updateEnrollment1095(Enrollment1095DTO enrollment1095Dto);
}
