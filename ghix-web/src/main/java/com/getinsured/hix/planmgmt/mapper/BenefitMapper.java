package com.getinsured.hix.planmgmt.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.BaseBenefit;

/**
 * Mapper class for all Benefit Objects.
 */
public class BenefitMapper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BenefitMapper.class);
	
	
	/**
	 * 
	 * This converts key-value paired data to 
	 * 		Each new object of specified benefit class {@code benefitCLS}.
	 * 		Sets its corresponding {@code parent Plan object} into each Benefit object.
	 * 		Forms a Map object as required for it to be persisted in DB. 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <benefitCLS> Map<String, benefitCLS> mapBenfits(Map<String, String> data, Class<? extends BaseBenefit> benefitCLS, Object parent) 
	{
		Map<String, benefitCLS> benefits = new HashMap<String, benefitCLS>();
		BaseBenefit<Object> baseBenefit;
		try {
			Set<String> keyset = data.keySet();
			for(String key : keyset )
			{
				baseBenefit = (BaseBenefit<Object>) benefitCLS.newInstance();
				//baseBenefit.setKeyAttrib(key);
				//baseBenefit.setValue(data.get(key));
				baseBenefit.setParent(parent);
				benefits.put(key, (benefitCLS) baseBenefit);
			}
		} catch (InstantiationException e) {
			LOGGER.error("Error while instantiating benefit class. Cause : " + e.getMessage());
		} catch (IllegalAccessException e) {
			LOGGER.error("Error while instantiating benefit class. Cause : " + e.getMessage());
		}
		return benefits;
	}
}
