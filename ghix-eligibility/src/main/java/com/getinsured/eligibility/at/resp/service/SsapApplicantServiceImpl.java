package com.getinsured.eligibility.at.resp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Service("ssapApplicantService")
@Transactional
public class SsapApplicantServiceImpl implements SsapApplicantService {


	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	//private static final Logger LOGGER = Logger.getLogger(SsapApplicantServiceImpl.class);

	@Override
	public SsapApplicant findByExternalCaseNumber(String caseNumber)
			throws GIException {
		SsapApplicant ssapApplicant = null;
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findByExternalApplicantId(caseNumber);

		if (ssapApplicants != null && ssapApplicants.size() > 0) {

			if (ssapApplicants.size() > 1) {
				throw new GIException("More than one ssap applicants found for external case number - " + caseNumber);
			}

			ssapApplicant = ssapApplicants.get(0);
		}

		return ssapApplicant;
	}

	@Override
	public List<SsapApplication> findSsapApplicationForExternalApplicantId(String caseNumber) {

		return ssapApplicationRepository.findByExternalApplicantIdSortDesc(caseNumber);
		//return ssapApplicantRepository.findApplicationForPrimaryByExternalApplicantId(caseNumber);
	}

	@Override
	public List<SsapApplication> findSsapApplicationForHHCaseId(String hhCaseId) {
		return ssapApplicationRepository.findByHHCaseId(hhCaseId);
	}
	
	@Override
	public SsapApplicant findById(long id){
		return ssapApplicantRepository.findOne(id);
	}

	@Override
	public SsapApplicant findByApplicantGuidAndSsapApplication(String applicantGuid, SsapApplication ssapApplication) {
		return ssapApplicantRepository.findByApplicantGuidAndSsapApplication(applicantGuid, ssapApplication);
	}
	
	
}
