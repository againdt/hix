package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlReconSnapshotGenerationService;

public class EnrlReconSummaryUpdateTask implements Tasklet {
	private static  Logger LOGGER = LoggerFactory.getLogger(EnrlReconSummaryUpdateTask.class);
	private EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService;
	private String fileId;
	private Job job;
	private JobLauncher jobLauncher; 
	long jobExecutionId = -1;
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("EnrlReconSummaryUpdateTask.execute : Start");
		if(fileId!=null && !fileId.trim().equalsIgnoreCase("")){
			jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			enrlReconSnapshotGenerationService.updateSummary(Integer.valueOf(fileId),job, jobLauncher, jobExecutionId);
		}
		LOGGER.info("EnrlReconSummaryUpdateTask.execute : END");
		return RepeatStatus.FINISHED;
	}
	public EnrlReconSnapshotGenerationService getEnrlReconSnapshotGenerationService() {
		return enrlReconSnapshotGenerationService;
	}
	public void setEnrlReconSnapshotGenerationService(
			EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService) {
		this.enrlReconSnapshotGenerationService = enrlReconSnapshotGenerationService;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}
	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}
}
