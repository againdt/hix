<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>



<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<%@ page isELIgnored="false"%>
<c:set var="PAGE_NAME" value="manageflow"/>

<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/affiliate/manageflow" />">&lt; <spring:message  code="label.back"/></a></li>
            <li>Manage Flow</li>
        </ul><!--page-breadcrumb ends-->
            <h1>Manage Flow</h1>
    </div>	
    <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
   </div>
    
    <div class="row-fluid">
		<div id="sidebar" class="span12">
			<div class="gray">
            	<form class="form-vertical" id="frmsearchaffiliate" name="frmsearchaffiliate" action="<c:url value="/admin/affiliate/manageflow" />" method="POST">
                	<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
                	
                    <div class="gutter10 graybg">
                    	<div class="row-fluid">
                         <div class="control-group span4">
                            <label for="affiliateflowId" class="control-label">Flow ID</label>
                            <div class="controls">
                                <input class="input-medium" type="text" value="${searchCriteria.affiliateflowId}"  name="affiliateflowId" id="affiliateflowId" maxlength="20"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                    	<div class="control-group span4">
                            <label for="flowName" class="control-label">Flow Name</label>
                            <div class="controls compnayAutoComp">
                                <input class="input-medium" type="text" value="${searchCriteria.flowName}"  name="flowName" id="flowName"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->

						  <div class="control-group span4">
                            <label for="status" class="control-label">Status</label>
                            <div class="controls">
                                <c:set var="counter" value="0" />
                                 <select  name="flowStatus" id="flowStatus" class="input-large">
                                    <option ${searchCriteria.flowStatus == 'SELECT' ? 'selected="selected"' : ''} value="SELECT">SELECT</option>
                                    <option ${searchCriteria.flowStatus == 'ACTIVE' ? 'selected="selected"' : ''} value="ACTIVE" selected >Active</option>
                                    <option ${searchCriteria.flowStatus == 'INACTIVE' ? 'selected="selected"' : ''} value="INACTIVE">Inactive</option>
                                    <option ${searchCriteria.flowStatus == 'REGISTERED' ? 'selected="selected"' : ''} value="REGISTERED">Registered</option>
                                    <option ${searchCriteria.flowStatus == 'SUSPENDED' ? 'selected="selected"' : ''} value="SUSPENDED">Suspended</option>
                                   
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        </div>
                        
                        <div class="row-fluid">
                        <div class="control-group span4">
                            <label for="typeOfTraffic" class="control-label">Type of Traffic</label>
                            <div class="controls">
                                <select  name="typeOfTraffic" id="typeOfTraffic" class="input-large">
                                    <option value="">SELECT</option>
                                     <c:forEach var="typeOfTraffic" items="${typeOfTrafficList}"> 
                                       <option <c:if test="${typeOfTraffic == searchCriteria.typeOfTraffic}"> SELECTED </c:if> value="${typeOfTraffic}">${typeOfTraffic.description}</option>
                                    </c:forEach>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                               <fieldset class="span4">
							<label for="ivr" class="control-label">IVR #
						    </label>
						<div class="control-group">
							<div class="controls">
								<label for="ivr1" class="hidden">&nbsp;</label>
								<input type="text" class="span4 inline" value="${searchCriteria.ivr1}" name="ivr1" id="ivr1" data-type="number" class="parsley-validated parsley-error" size="3" maxlength="3" onKeyUp="shiftbox(this,'ivr2')"/>
								
								<label for="ivr2" class="hidden">&nbsp;</label>
								<input type="text" class="span4 inline" value="${searchCriteria.ivr2}"  name="ivr2" id="ivr2" data-type="number" class="parsley-validated parsley-error" size="3" maxlength="3" onKeyUp="shiftbox(this,'ivr3')"/>
								
								<label for="ivr3" class="hidden">&nbsp;</label>
								<input type="text" class="span4 inline" value="${searchCriteria.ivr3}"  name="ivr3" id="ivr3" data-type="number" class="parsley-validated parsley-error" size="4" maxlength="4"/>
									<span id="ivr3_error"></span>
							</div>
						</div>
						</fieldset>
                       
              
  
                        <div class="control-group span4">
	                        &nbsp;
	                        <div class="controls">
 								<input type="submit" class="btn" value="<spring:message  code='label.submit'/>" id="submit-parsely">
 							</div>
 						</div>
                        </div>
                    </div>
                </form>
               <%--  <a href="<c:url value="/admin/affiliate/addaffiliate"/>" class="btn btn-primary span11" id="addAffiliate" name="addAffiliate"><spring:message  code='label.addAffiliate'/></a> --%>
            </div>
		</div><!-- span3 ends-->
		</div><!-- /.row-fluid -->
        
        <div class="row-fluid">
        <div class="span12 " id="rightpanel">
        	<form class="form-horizontal" id="frmsearchaffiliate" name="frmsearchaffiliate" method="POST">
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(affiliateflow) > 0}">
                                <table class="table">
                                    <thead>
                                        <tr class="graydrkbg">
                                             <th scope="col" style="width:23px"><dl:sort title="ID" sortBy="affiliateflowId" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col" style="width:88px"><dl:sort title="Flow" sortBy="flowName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                              <th scope="col" style="width:88px"><dl:sort title="Company" sortBy="affiliatecompanyName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col" style="width:63px"><dl:sort title="Status" sortBy="flowStatus" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                              <th scope="col" style="width:143px"><dl:sort title="Type ofTraffic" sortBy="trafficType" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                              <th scope="col" style="width:83px"><dl:sort title="IVR #" sortBy="ivr" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col" style="width:53px"><dl:sort title="Web Pricing <br/>Model" sortBy="webPricingModel" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                              <th scope="col" style="width:88px"><dl:sort title="Web Payout($/%)" sortBy="webPayoutvalue" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											 <th scope="col" style="width:63px"><dl:sort title="Call Pricing Model" sortBy="callPricingModel" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											 <th scope="col" style="width:88px"><dl:sort title="Call Payout($/%)" sortBy="callPayoutvalue" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                            <th style="width:30px"></th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${affiliateflow}" var="affiliateflow">
                                            <tr>
                                            
                                                 <td>${affiliateflow.affiliateflowId}</td>
                                                 <td>${affiliateflow.flowName} </td>
                                                 <td>${affiliateflow.affiliatecompanyName}</td>
                                                 <td>${affiliateflow.flowStatus} </td>
                                                 <td>${affiliateflow.trafficType} </td>
                                                 <td>${affiliateflow.ivrNumber} </td>
                                                 <td>${affiliateflow.webPricingModel} </td>
                                                 <td>
	                                                <c:if test="${affiliateflow.webPayoutType == 'FIXED' }">
	                                                 $ ${affiliateflow.webPayoutvalue}
	                                                </c:if>
	                                                <c:if test="${affiliateflow.webPayoutType == 'PERCENTAGE' }">
	                                                ${affiliateflow.webPayoutvalue} %
	                                                </c:if>
                                                </td>
                                                 <td>${affiliateflow.callPricingModel} </td>
                                                 <td>
	                                                <c:if test="${affiliateflow.callPayoutType == 'FIXED_PAYOUT' }">
	                                                  $ ${affiliateflow.callPayoutvalue}
	                                                </c:if>
	                                                <c:if test="${affiliateflow.callPayoutType == 'PERCENTAGE' }">
	                                                ${affiliateflow.callPayoutvalue} %
	                                                </c:if>
                                                </td>

                                                <td>
			                                        <div class="dropdown">
			                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
			                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			                                                <li><a href="<c:url value="/admin/affiliate/viewflow/${affiliateflow.affiliateaffiliateId}/${affiliateflow.affiliateflowId}" />"><i class="icon-refresh"></i><spring:message  code='label.viewAffiliate'/></a></li>
			                                                <li><a href="<c:url value="/admin/affiliate/editflow/${PAGE_NAME}/${affiliateflow.affiliateaffiliateId}/${affiliateflow.affiliateflowId}" />"><i class="icon-pencil"></i><spring:message  code='label.editAffiliate'/></a></li>
			                                                <li><a href="javascript:void(0)"> <i class="icon-pencil"></i>Portal</a></li>
			                                                <li> <a href="<c:url value="/admin/affiliate/addcampaign/${PAGE_NAME}/${affiliateflow.affiliateaffiliateId}/${affiliateflow.affiliateflowId}"/>"  id="createView" name="createView"> <i class="icon-pencil"></i> Campaign </a></li>
			                                            </ul>
			                                        </div>
				                                 
				                                </td>
                                            </tr>
                                        </c:forEach>
                                </table>
                                <div class="center">
                                    <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
            </form>
		</div><!--span9 ends-->
	</div>	

    <div class="notes" style="display: none">
        <div class="row">
            <div class="span">

                <p>The prototype showcases three scenarios (A, B and C) dependent on
                    a particular Employer's eligibility to use the SHOP Exchange.</p>
            </div>
        </div>
    </div>

</div> <!-- row-fluid -->


<script type="text/javascript">



$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});

function shiftbox(element,nextelement) {
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength) {
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}


var flowName_list ='${flowNameList}';
var flowNameArr = flowName_list.slice(1,flowName_list.length-1);
var flowNameData = flowNameArr.split(',');
$( "#flowName" ).autocomplete({
    source: flowNameData
});


var affiliateflowId_list ='${flowIdList}';
var affiliateflowIdArr = affiliateflowId_list.slice(1,affiliateflowId_list.length-1);
var affiliateflowIdData = affiliateflowIdArr.split(',');
$( "#affiliateflowId" ).autocomplete({
    source: affiliateflowIdData
});

</script>

<script type="text/javascript">

	/*disable all the keypress except for numbers*/
	$('#ivr1, #ivr2, #ivr3, #affiliateflowId').keypress(function(e){
	  	if (window.event) { var charCode = window.event.keyCode; }
	    else if (e) { var charCode = e.which; }
	    else { return true; }
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
	    return true;
	  
	});
</script>