<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>

<%

String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);

%>



<c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(ticketList) > 0}">
								<table class="table table-striped margin10-t">
									<thead>
										<tr class="header ">
											<!--  span9 header -->
<!-- 		Commented as per HIX-12845 		<th><input id="selectall" type="checkbox"> </th> -->
											<!-- <th></th> -->
											<%-- <th class="sortable" scope="col"><dl:sort
													title="Task" sortBy="id"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<th class="sortable" scope="col" style="width:150px"><dl:sort title="Ticket" sortBy="tkm_ticket_id" sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" ></dl:sort>
												<c:choose>
													<c:when
														test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'tkm_ticket_id'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
															alt="State sort ascending" />
													</c:when>
													<c:when
														test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'tkm_ticket_id'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
															alt="State sort descending" />
													</c:when>
													<c:otherwise></c:otherwise>
												</c:choose>
											</th>
											
											<th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Requested By" sortBy="role" sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" ></dl:sort>
												<c:choose>
													<c:when
														test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'role'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
															alt="State sort ascending" />
													</c:when>
													<c:when
														test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'role'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
															alt="State sort descending" />
													</c:when>
													<c:otherwise></c:otherwise>
												</c:choose>													
											</th>
											
											<th class="" scope="col" style="width: 120px;">Created for</th>
											
											<th class="sortable" scope="col" style="width: 208px;"><dl:sort title="Workgroup" sortBy="tkmQueues" sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" > <i class="icon-sort"></i></dl:sort>
													<c:choose>
														<c:when
															test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'tkmQueues'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																alt="State sort ascending" />
														</c:when>
														<c:when
															test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'tkmQueues'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																alt="State sort descending" />
														</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>
											</th>
											
											<th class="sortable" scope="col" style="width:120px;"><dl:sort title="Ticket Status" sortBy="status"
													sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" > <i class="icon-sort"></i></dl:sort>
													<c:choose>
														<c:when
															test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'status'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																alt="State sort ascending" />
														</c:when>
														<c:when
															test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'status'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																alt="State sort descending" />
														</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>
											</th>
											<th class="sortable" scope="col" style="width: 75px;"><dl:sort title="Priority" sortBy="priority_order" sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" > <i class="icon-sort"></i></dl:sort>
												<c:choose>
													<c:when
														test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'priority_order'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
															alt="State sort ascending" />
													</c:when>
													<c:when
														test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'priority_order'}">
														<img
															src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
															alt="State sort descending" />
													</c:when>
													<c:otherwise></c:otherwise>
												</c:choose>		
											</th>
											<th class="sortable" scope="col" style="width: 130px;"><dl:sort title="Date of Request" sortBy="creation_timestamp"
													sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" > <i class="icon-sort"></i></dl:sort>
													<c:choose>
														<c:when
															test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'creation_timestamp'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																alt="State sort ascending" />
														</c:when>
														<c:when
															test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'creation_timestamp'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																alt="State sort descending" />
														</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>	
											</th>
												<th class="sortable" scope="col" style="width:100px;"><dl:sort title="Due Date" sortBy="due_date"
													sortOrder="${searchCriteria.sortOrder}" customFunctionName="sorting" > <i class="icon-sort"></i></dl:sort>
													<c:choose>
														<c:when
															test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'due_date'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																alt="State sort ascending" />
														</c:when>
														<c:when
															test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'due_date'}">
															<img
																src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																alt="State sort descending" />
														</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>	
											</th>
											<th>
											<input type="button" class="btn  btn-primary " id="btnExportToExcel" value="Export" style="margin-top:16px;" >
											</th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${ticketList}" var="ticket" varStatus="vs">
									<tr class="jsRow_${ticket[0]}">
<%-- 		Commented as per HIX-12845 		<td><input class="case" value="${ticket.id}" type="checkbox"> </td> --%>
											<%-- <td> <button type="button" title="<spring:message  code='label.vButtonDetail'/>" id ="dropdown" onclick="javascript:getTicketTaskData(${ticket.id});"><b>v</b></button></td> --%>
											<%-- <td> <a type="" title="<spring:message  code='label.vButtonDetail'/>" id ="dropdown" onclick="javascript:getTicketTaskData(${ticket.id});"><i class="icon-plus-sign"></i></a></td> --%>
											<%-- <span onClick="getTicketTaskData(${ticket.id});"><b>v</b></span> --%>
											<c:set var="encTicketid" scope="request"><encryptor:enc value="${ticket[0]}" isurl="true"/> </c:set>
											<td class="TDnumber"><a href="<c:url value="/ticketmgmt/ticket/ticketdetail/${encTicketid}" />" ><c:out value="${ticket[2]}" /></a>
												<br><a href="<c:url value="/ticketmgmt/ticket/ticketdetail/${encTicketid}" />" ><c:out value="${ticket[1]}" /></a></br>
											</td>
											<td> ${ticket[11]}</td> <!-- Requested By -->
											<td> ${ticket[3]}</td>	<!-- Created For -->	
											<!-- Remove Queue text from JSP -->									
											<td id ="ticketQueueName-${ticket[0]}">
											<c:set var="initQueuetxt" value="${ticket[5]}"/>
											<c:choose>											 
												<c:when test="${fn:contains(initQueuetxt, 'Queue')}">
													<c:set var="finalQueuetxt" value="${fn:replace(initQueuetxt, 'Queue', '')}" />
												   	<c:out value="${finalQueuetxt}" />
												</c:when>
												<c:otherwise>
													<c:out value="${ticket[5]}" />
												</c:otherwise>
											</c:choose>
											
											
											</td>
											<td id="ticketTaskStatusId-${ticket[0]}">${ticket[7]}</td><!-- Ticket Status Status Table Info -->
											<td>${ticket[6]}</td><!-- Priority Table Info -->
											<td><fmt:formatDate value="${ticket[4]}" pattern="MM-dd-yyyy" timeZone="<%=timeZone%>"/></td>
											<c:choose>
												<c:when test="${(ticket[9]!='')}">
												<td><fmt:formatDate	value="${ticket[9]}" pattern="MM-dd-yyyy" timeZone="<%=timeZone%>" />
													<br><fmt:formatDate	value="${ticket[9]}" pattern="hh:mm aa" timeZone="<%=timeZone%>" /></br>
												</td>
												</c:when>
												<c:otherwise>
												   <td><c:out value="NA" /> </td>					
					  							</c:otherwise>
					  						</c:choose>
											<td style ="white-space:nowrap"><div class="controls">
													<div class="dropdown">
														<a href="#" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"> <i
															class="icon-cog"></i> <b class="caret"></b>
														</a>
														<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right">
									                      <c:set var="encTicketid" scope="request"><encryptor:enc value="${ticket[0]}" isurl="true"/> </c:set>						
															<li><a href="<c:url value="/ticketmgmt/ticket/ticketdetail/${encTicketid}"/>"
																class=""><i class="icon-eye-open"></i><spring:message code="text_tkt.viewDetails"/></a></li>
																<c:if test="${! isSimplifiedView}">
																	<c:choose>
																		<c:when test="${(ticket[7]=='Completed' ||ticket[7] =='Canceled' || ticket[7]=='Resolved')}">
																			<li class="navmenu disabled" id="editTicket"><a href="#"><i class="icon-pencil"></i><spring:message code="option_tktTask.editSummary"/></a></li>
																		</c:when>
																		<c:otherwise>
																			<li class="navmenu" id="editTicket-${ticket[0]}">
																			<a href="<c:url value="/ticketmgmt/ticket/edit/${encTicketid}"/>"><i class="icon-pencil"></i><spring:message code="option_tktTask.editSummary"/></a></li>
											
						  												</c:otherwise>
						  											</c:choose>
																
																	<c:choose>
																	<c:when test="${(ticket[7]=='Completed' ||ticket[7] =='Canceled' || ticket[7]=='Resolved')}">
																		<li class="navmenu disabled" id="cancelTicket"><a href="#"><i class="icon-remove"></i> <spring:message code="lbl.cancelTicket"/></a></li>
																	</c:when>
																	<c:otherwise>
																		<li class="navmenu" id="cancelTicket-${ticket[0]}">
																		<a href="javascript:cancelFunction(${ticket[0]})"><i class="icon-remove"></i> <spring:message code="lbl.cancelTicket"/></a></li>
																		
					  												</c:otherwise>
					  												</c:choose>
				  												</c:if>
				  												<c:choose>
																<c:when test="${(ticket[7]!='Completed' &&ticket[8] !='Canceled' &&ticket[7]!='Resolved')}">
																	<c:if test="${ticket[8]=='Claimed'}">
																	<li class="navmenu" id="completeTicket-${ticket[0]}">
																	<a href="javascript:completeTicket(${ticket[0]}, ${ticket[16]})"><i class="icon-refresh"></i> <spring:message code="option_tktTask.markAsComplete"/> </a></li>
																	</c:if>
																	<c:if test="${ticket[8]=='UnClaimed'}">
																	<li class="navmenu" id="completeTicket-${ticket[0]}">
																	<a href="javascript:completeTicket(${ticket[0]}, ${ticket[16]})"><i class="icon-refresh"></i> <spring:message code="option_tktTask.claim"/></a></li>
																	</c:if>
																</c:when>
				  												</c:choose>
				  												<c:if test="${! isSimplifiedView}">
					  												<c:choose>
																	<c:when test="${(ticket[7]=='Completed' ||ticket[7] =='Canceled' || ticket[7]=='Resolved')}">
																		<li class="navmenu disabled" id="reassignTicket"><a href="#"><i class="icon-exchange"></i> <spring:message code="option_tktTask.reAssignTkt"/></a></li>
																	</c:when>
																	<c:otherwise>
																		
																		<c:if test="${ticket[8]=='Claimed'}">
																			<li class="navmenu" id="reassignTicket-${ticket[0]}">	<a href="javascript:reassignFunction(${ticket[0]})""><i class="icon-exchange"></i> <spring:message code="option_tktTask.reAssignTkt"/></a></li>
																		</c:if>
																		<c:if test="${ticket[8]=='UnClaimed'}">
																		  <c:if test="${isAssignAllowed == true }">
																			<c:set var="assignTicketid" scope="request"><encryptor:enc value="${ticket[0]}" isurl="true"/> </c:set>
																			<input type ="hidden" name="encAssignTicketid" id="encAssignTicketid_${ticket[0]}" value=${assignTicketid} />
																			<li class="navmenu" id="assignTicket-${ticket[0]}">	<a href="javascript:assignFunction(${ticket[0]})"><i class="icon-exchange"></i> Assign Ticket</a></li>
																		  </c:if>	
																		</c:if>
																		<li id="reassign" class="navmenu" style="display: none;"><a href="#reassignDialog" id="displayReassignDialog" role="button" data-toggle="modal"><i class="icon-ok"></i><spring:message code="option_tktTask.reAssign"/></a></li>
																		
					  												</c:otherwise>
					  												</c:choose>
																	<%-- <li class="navmenu disabled" id="archiveTicket"><a href="#" class=""><i class="icon-archive"></i> <spring:message code="option_tktTask.archiveTkt"/></a></li> --%>
																</c:if>	
																<c:if  test="${(ticket[7] =='Canceled' || ticket[7]=='Resolved')}">
																	<c:choose>
																		<c:when test="${ticket[10]=='Y'}">
																			<li class="navmenu disabled" id="reopen-${encTicketid}">	<a><i class="icon-folder-open"></i> Reopen</a></li>
																			<li class="navmenu disabled" id="restart-${encTicketid}">	<a><i class="icon-refresh"></i> Restart</a></li>
																		</c:when>
																		<c:otherwise>
																			<li class="navmenu" id="reopen-${encTicketid}">	<a href="javascript:reopenTicketFunction('${encTicketid}')"><i class="icon-folder-open"></i> Reopen</a></li>
																			<li class="navmenu" id="restart-${encTicketid}">	<a href="javascript:restartTicketFunction('${encTicketid}')"><i class="icon-refresh"></i> Restart</a></li>
																		</c:otherwise>
																	</c:choose>
																</c:if>
																
																
																
																
															    <c:if test="${isArchivedAllowed == true }">
																  <c:choose>
																	<c:when test="${(ticket[7]!='Completed' && ticket[7] !='Canceled' && ticket[7]!='Resolved') ||ticket[10]=='Y' }">
																		<li class="navmenu disabled" id="archiveTicket-${ticket[0]}" ><a class=""><i class="icon-archive"></i> <spring:message code="option_tktTask.archiveTkt"/></a></li>
																	</c:when>
																	<c:otherwise>
																	   <li class="navmenu" id="archiveTicket-${ticket[0]}">
																		<a href="javascript:archiveFunction(${ticket[0]})"><i class="icon-archive"></i> <spring:message code="option_tktTask.archiveTkt"/></a></li>	
					  												</c:otherwise>
					  											  </c:choose>
					  										   </c:if>
														 </ul>
													</div>
												</div></td>
												
											
										</tr>
										<tr style="display:none;" id="hide${ticket[0]}">
										<td></td>
										<td colspan="7">
										<table  id="excelDataTable${ticket[0]}">
													<!-- <thead>
													class="table table-striped"
														
														<tr class="graydrkbg ">
															<th><input type="checkbox"> </th>
															<th>TaskName</th>
															<th>CreatedBy</th>
															<th>Asignee</th>
															<th>Creation</th>
															<th>LastUpdate</th>
															<th>Status</th>
														</tr>
														
													</thead> -->
													
														<%-- <c:forEach items="${ticket.tkmTicketsTaksList}" var="ticketTask" varStatus="vs">
														<tr>
															<td><input type="checkbox"> </td>
															<td>${ticketTask.taskName}</td>											
															<td>${ticketTask.creator}</td>
															<td>${ticketTask.assignee.firstName}</td>
															<td><fmt:formatDate value="${ticketTask.created}" pattern="MM-dd-yyyy" /></td>
															<td><fmt:formatDate value="${ticketTask.updated}" pattern="MM-dd-yyyy" /></td>
															<td>${ticketTask.status}</td>
														</tr>
													</c:forEach> --%>
												</table>
												</td>
										</tr>				
									</c:forEach>
									</tbody>
								</table>
                                <div class="pagination txt-center">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" customFunctionName="goToPage" />
								</div>
						</c:when>
						<c:otherwise>
							<h4 class="alert alert-info">
								<spring:message code='label.norecords' />
							</h4>
						</c:otherwise>
					</c:choose>
				</c:when>
				
			</c:choose>
			
			 <input type ="hidden" name="totTickets"
                                                                id="totTickets" value="${resultSize}" >
                                                                
                                                                
                                                                
<script text="text/javascript">
$("#btnExportToExcel").on("click",function(){
		$("#exportToExcelModal").modal();
	});
function notifyReassignResponse(status, ticketId,assignee,queueName,action) {
	if(action == true){
		var encTicketId  = 	document.getElementById("encAssignTicketid_"+ticketId).value;
		var redirectUrl ='<c:url value="/ticketmgmt/ticket/ticketdetail/'+encTicketId+'"> </c:url>';
		window.location.href = redirectUrl;
	}
	else{
		if(status == true) {
			var rowId = '#ticketQueueName-'+ticketId;
			$(rowId).text(queueName);
		} else {
			alert("Ticket Reassign failed");
		}	
	}
}
</script>
