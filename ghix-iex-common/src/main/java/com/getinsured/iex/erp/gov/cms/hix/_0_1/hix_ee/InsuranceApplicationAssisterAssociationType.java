//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AssociationType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;


/**
 * A relationship A data
 * 				type for a relationship between an insurance application and a
 * 				certified assister (e.g., counselor or navigator).
 * 			
 * 
 * <p>Java class for InsuranceApplicationAssisterAssociationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceApplicationAssisterAssociationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}AssociationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}InsuranceApplicationAssisterReference" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceApplicationAssisterAssociationType", propOrder = {
    "insuranceApplicationAssisterReference"
})
public class InsuranceApplicationAssisterAssociationType
    extends AssociationType
{

    @XmlElement(name = "InsuranceApplicationAssisterReference")
    protected ReferenceType insuranceApplicationAssisterReference;

    /**
     * Gets the value of the insuranceApplicationAssisterReference property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceType }
     *     
     */
    public ReferenceType getInsuranceApplicationAssisterReference() {
        return insuranceApplicationAssisterReference;
    }

    /**
     * Sets the value of the insuranceApplicationAssisterReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceType }
     *     
     */
    public void setInsuranceApplicationAssisterReference(ReferenceType value) {
        this.insuranceApplicationAssisterReference = value;
    }

}
