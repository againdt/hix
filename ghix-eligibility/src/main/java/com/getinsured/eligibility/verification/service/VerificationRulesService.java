package com.getinsured.eligibility.verification.service;

/**
 * Encapsulates service layer method calls for Verification Rules
 */
public interface VerificationRulesService {
	
	/**
	 * Updates Verification Rule Status for Household Members
	 * 
	 * * @param applicationId
	 *            SSAP Application ID
	 * @param personId
	 *            HouseholdMember/Applicant Id
	 * @param status            
	 *            Verification Rule Status 
	 * @param rule
	 *            Rule for which the status will be updated
	 * @return true/false
	 * @throws Exception 
	 */
	boolean updateVerificationStatus(long applicationId, long personId, String status, int rule) throws Exception;

	/**
	 * Updates SSAP json in ssap_applications
	 * 
	 * @param applicationId
	 *            SSAP Application ID
	 * @param ssapJson
	 *            updated SSAP Json
	 * @throws Exception 
	 */
	void updateSsapJson(long applicationId, String ssapJson) throws Exception;

	boolean checkVerificationStatus(long applicationId, long personId, int rule)
			throws Exception;

}
