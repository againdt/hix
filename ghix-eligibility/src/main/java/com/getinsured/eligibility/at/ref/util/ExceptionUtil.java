package com.getinsured.eligibility.at.ref.util;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapMonitorPayloadService;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.model.SsapMonitorPayload;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.util.TSDate;

@Component
public class ExceptionUtil {
	
	private static final Logger LOGGER = Logger.getLogger(ExceptionUtil.class);
	
	private static final String COMPONENT = "ACCOUNT_TRANSFER";
	
	@Autowired 
	private GIWSPayloadService giwsPayloadService;
	@Autowired 
	private GIMonitorService giMonitorService;
	@Autowired 
	private SsapMonitorPayloadService ssapMonitorPayloadService;
	
	
	
	
	public void persistGiMonitorId(AccountTransferRequestPayloadType request, Throwable throwable){
		try 
		{
			Long giMonitorId = persistToGiMonitor(throwable);
			String houseHoldCaseId = extractHouseholdCaseId(request);
			String correlationId = extractCorrelationId(request);
			if(houseHoldCaseId != null && giMonitorId>0){
				SsapMonitorPayload ssapMonitorPayload = new SsapMonitorPayload();
				ssapMonitorPayload.setGiMonitorId(giMonitorId);
				ssapMonitorPayload.setGiWsPayloadId(null);
				ssapMonitorPayload.setHouseHoldCaseId(houseHoldCaseId);
				ssapMonitorPayload.setCorrelationId(correlationId);
				ssapMonitorPayload.setCreationTimestamp(new TSDate());
				ssapMonitorPayload.setLastUpdateTimestamp(new TSDate());
				ssapMonitorPayloadService.save(ssapMonitorPayload);
				LOGGER.info("ID = " + ssapMonitorPayload.getId() + " HouseHoldCaseId = "+ houseHoldCaseId + " CorrelationId = "+ ssapMonitorPayload.getCorrelationId() + " GiMonitorId = "+ ssapMonitorPayload.getGiMonitorId());
			}
		} 
		catch(Exception exception){
			LOGGER.error("Error occurred while saving account transfer exception in GIWSPAYLOAD table : ", exception);
		}
	}
	
	private String extractHouseholdCaseId(AccountTransferRequestPayloadType accountTransferRequest)
	{
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		String hhCaseId = null;
		if((null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId))){
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = accountTransferRequest.getInsuranceApplication().getApplicationIdentification();
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if(ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue())) {
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
			}
		}
		return hhCaseId;
	}
	
	private String extractCorrelationId(AccountTransferRequestPayloadType accountTransferRequest) {

		return accountTransferRequest!= null ? accountTransferRequest.getTransferHeader() != null ? 
				accountTransferRequest.getTransferHeader().getTransferActivity() != null ? 
						accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification() != null ? 
								accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID() != null ?
								accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue() 
				 : StringUtils.EMPTY  : StringUtils.EMPTY : StringUtils.EMPTY : StringUtils.EMPTY : StringUtils.EMPTY;

	}
	public void persistGiMonitorId(long applicationId, Throwable throwable){
		try 
		{
			Long giMonitorId = persistToGiMonitor(throwable);
			if(applicationId>0 && giMonitorId>0){
				List<GIWSPayload> atPayloads = giwsPayloadService.findBySSAPID(applicationId);
				
				if(atPayloads!=null){
					for(GIWSPayload payload : atPayloads){
						SsapMonitorPayload ssapMonitorPayload = new SsapMonitorPayload();
						ssapMonitorPayload.setSsapApplicationId(applicationId);
						ssapMonitorPayload.setGiMonitorId(giMonitorId);
						ssapMonitorPayload.setGiWsPayloadId(Long.valueOf(payload.getId().longValue()));
						ssapMonitorPayload.setHouseHoldCaseId(payload.getHouseHoldCaseId());
						ssapMonitorPayload.setCorrelationId(payload.getCorrelationId());
						ssapMonitorPayload.setCreationTimestamp(new TSDate());
						ssapMonitorPayload.setLastUpdateTimestamp(new TSDate());
						ssapMonitorPayloadService.save(ssapMonitorPayload);
						LOGGER.info("ID = " + ssapMonitorPayload.getId() + " SsapApplicationId = "+ applicationId + " GiWsPayloadId = "+ ssapMonitorPayload.getGiWsPayloadId() + " GiMonitorId = "+ ssapMonitorPayload.getGiMonitorId());
					}
				}
			}
		} 
		catch(Exception exception){
			LOGGER.error("Error occurred while saving account transfer exception in GIWSPAYLOAD table : ", exception);
		}
	}
	
	private Long persistToGiMonitor(Throwable throwable){
		Integer giMonitorId=null;
		try 
		{
			GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog(GIRuntimeException.ERROR_CODE_UNKNOWN, new TSDate(), throwable.getClass().getName(), 
					ExceptionUtils.getStackTrace(throwable), null, null, COMPONENT, null);
			giMonitorId = giMonitor.getId();
		}
		catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
		}
		return giMonitorId != null ? Long.valueOf(giMonitorId.longValue()) : 0;
	}
	
}
