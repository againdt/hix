package com.getinsured.hix.batch.node;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.Nodes;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;


@Controller
public class NodeController {
	
	@Autowired
	private NodePublisher nodePublisher;
	
	@RequestMapping(value = "/node/info", method = RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getNodeInfo(){
		Nodes  localNode = nodePublisher.getLocalNode();
		if(localNode != null){
			return PlatformServiceUtil.getResponseEntity(localNode.toJSONString(),HttpStatus.OK, null,MediaType.APPLICATION_JSON);
		}else{
			return PlatformServiceUtil.getResponseEntity("Failed to get local Node Info", HttpStatus.NOT_FOUND, null, MediaType.APPLICATION_JSON);
		}
	}
}
