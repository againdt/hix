package com.getinsured.hix.indportal.dto;

/**
 * @author vishwanath_s
 * Class to store Applicants Events in case of Override SEP
 */
public class ApplicantEventsDTO {

	private Integer eventId;
	private String eventDate;
	private Integer applicantEventId;
	private String eventNameLabel;
	private String firstName;
	private String middleName;
	private String lastName;
	private Integer previousEventId;
	private String previousEventNameLabel;
	
	/**
	 * @return the eventId
	 */
	public Integer getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	/**
	 * @return the eventDate
	 */
	public String getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	/**
	 * @return the applicantEventId
	 */
	public Integer getApplicantEventId() {
		return applicantEventId;
	}
	/**
	 * @param applicantEventId the applicantEventId to set
	 */
	public void setApplicantEventId(Integer applicantEventId) {
		this.applicantEventId = applicantEventId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the eventNameLabel
	 */
	public String getEventNameLabel() {
		return eventNameLabel;
	}
	/**
	 * @param eventNameLabel the eventNameLabel to set
	 */
	public void setEventNameLabel(String eventNameLabel) {
		this.eventNameLabel = eventNameLabel;
	}
	/**
	 * @return the eventNameLabel
	 */
	public Integer getPreviousEventId() {
		return previousEventId;
	}
	/**
	 * @param eventNameLabel the eventNameLabel to set
	 */
	public void setPreviousEventId(Integer previousEventId) {
		this.previousEventId = previousEventId;
	}
	/**
	 * @return the eventNameLabel
	 */
	public String getPreviousEventNameLabel() {
		return previousEventNameLabel;
	}
	/**
	 * @param eventNameLabel the eventNameLabel to set
	 */
	public void setPreviousEventNameLabel(String previousEventNameLabel) {
		this.previousEventNameLabel = previousEventNameLabel;
	}
}
