package com.getinsured.hix.batch.enrollment.writer;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.enrollment.service.EnrollmentAdminEffectuationService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrollmentAdminEffactuation;

import static com.getinsured.hix.enrollment.util.EnrollmentConstants.*;

import com.getinsured.hix.platform.util.exception.GIException;

public class EnrollmentAdminEffectuationWriter  implements ItemWriter<String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAdminEffectuationWriter.class);
	private EnrollmentAdminEffectuationService enrollmentAdminEffectuationService ;
	private String wipFolderDir = null;
	private JobService jobService;
	long jobExecutionId = -1;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader EnrollmentAdminEffectuationReader");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		if(ec != null){
			wipFolderDir  =ec.getString("wIPFolderDir");

		}
	}
	@Override
	public void write(List<? extends String> items) throws Exception {
		Map<String,List<String[]>> effectuationMap = new HashMap<>();
		List<String[]> effectuationList = null;
		
		if(items != null && items.size() > 0 ){
			for (String filePath : items) {
				
				
				List<String> allEnrollment = Files.readAllLines(Paths.get(filePath),Charset.defaultCharset());
				
				//allEnrollment.remove(0); // Remove the header row
				
				for (String line : allEnrollment) {
				    	String[] effectuationData = line.split(CSVS_SPLITE_BY);
				    	 if(effectuationData != null && effectuationData.length > 1 && EnrollmentUtils.isNotNullAndEmpty( effectuationData[Exchange_Assigned_Policy_ID] != null)){
				    			 if(effectuationMap.containsKey(effectuationData[Exchange_Assigned_Policy_ID])){
				    				 effectuationList = effectuationMap.get(effectuationData[Exchange_Assigned_Policy_ID]);
				    				 effectuationList.add(effectuationData);
				    			 }else{
				    				 effectuationList = new ArrayList<>();
				    				 effectuationList.add(effectuationData);
				    				 effectuationMap.put(effectuationData[Exchange_Assigned_Policy_ID], effectuationList);
				    			 }
				    	 }else{
				    		 //call to make failure entry
				    		 List<String[]> effectFalureList = new ArrayList<>();
				    		 effectFalureList.add(effectuationData);
				    		 enrollmentAdminEffectuationService.insertAdminEffactuation(effectFalureList, new File(filePath).getName(), EnrollmentAdminEffactuation.PROCESSING_STATUS.FAILURE.toString(), "Exchange_Assigned_Policy_ID not provided");
				    	 }
				    	 
				}
				
			    if(effectuationMap != null && effectuationMap.size() > 0){
			    	
			    	try {
			    		
			    		String batchJobStatus=null;
			    		
			    		if(jobService != null && jobExecutionId != -1){
			    			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			    		}
			    		
			    		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			    			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			    		}
			    		
			    		boolean contailsBadEnrollment = enrollmentAdminEffectuationService.updateEnrollmentByCSV(effectuationMap, new File(filePath).getName(), wipFolderDir);
						
						moveToFolder(new File(filePath).getName() , wipFolderDir, contailsBadEnrollment );
						

					} catch (GIException e) {
						LOGGER.error("Exception in EnrollmentAdminEffectuationWriter  : " + e.getMessage());
					}
			    }
				}
			}
	}
	
	public EnrollmentAdminEffectuationService getEnrollmentAdminEffectuationService() {
		return enrollmentAdminEffectuationService;
	}
	public void setEnrollmentAdminEffectuationService(
			EnrollmentAdminEffectuationService enrollmentAdminEffectuationService) {
		this.enrollmentAdminEffectuationService = enrollmentAdminEffectuationService;
	}
	
	private void moveToFolder(String fileName, String fileDirectory, boolean contailsBadEnrollment) {
		try {
		if(contailsBadEnrollment){
				EnrollmentUtils.moveFiles(fileDirectory, fileDirectory+ File.separator +"BAD_CSV",fileName, null);
		}else{
			EnrollmentUtils.moveFiles(fileDirectory, fileDirectory+ File.separator +"GOOD_CSV",fileName, null);
		}
		} catch (GIException e) {
			LOGGER.error("Error in moveToFolder "+e.toString(),e);
		}
		
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
}
