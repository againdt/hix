<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>

<jsp:include page="sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>

<div class="gutter10-lr">
    <div class="l-page-breadcrumb" style="margin-top:10px;">
        <!--start page-breadcrumb -->
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li>
                    <a href="javascript:history.back()">
                        &lt; 
                        <spring:message code="label.back" />
                    </a>
                </li>
                <li>
                    <a href="
                    <c:url value="/crm/member/viewmember/${householdId}"/>">
                    <spring:message code="label.account" />
                    </a>
                </li>
                <li>${household.firstName}&nbsp;${household.lastName}</li>
            </ul>
            <div style="font-size: 14px; color: red">
                <c:if test="${errorMsg != ''}">
                    <p>
                        <c:out value="${errorMsg}"></c:out>
                    <p />
                </c:if>
                <br>
            </div>
        </div>
        <!--  end of row-fluid -->
    </div>
    <!--  end l-page-breadcrumb -->
    <div class="row-fluid">
       <h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})
       <div class="controls margin10-r pull-right" id="createTicketDiv">
						<a href="<c:url value="/ticketmgmt/ticket/createpage?prefillUser=true&roleName=INDIVIDUAL&moduleId=${householdId}&requestorId=${household.user.id}&requestorName=${household.firstName} ${household.lastName}&moduleName=HOUSEHOLD"/>" class="margin10-r btn btn-small btn-primary pull-right" id="addNewTicket" name="addNewTicket" ><spring:message  code="label.addNewTicket"/></a>
				</div>
       </h1> 
       
    </div>
    <div class="row-fluid">
        <div id="sidebar" class="span3">
		           <jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
        </div>
        <div class="span9">
     <%--    <div class="controls margin10-r pull-right" id="createTicketDiv">
	
						<a href="<c:url value="/ticketmgmt/ticket/createpage?prefillUser=true&userId=${householdId}&requestorName=${household.firstName} ${household.lastName}"/>" class="margin10-r btn btn-small btn-primary pull-right" id="addNewTicket" name="addNewTicket" ><spring:message  code="label.addNewTicket"/></a>
						 <input type="hidden" name="prefillUser" id="prefillUser" value="true" />
        				 <input type="hidden" name="userId" id="userId" value="<encryptor:enc value="${householdId}"/>" />
        				 <input type="hidden" name="requestorRole" id="requestorRole" value="${household.firstName} ${household.lastName}" />

				</div> --%>
            <c:choose>
                <c:when test="${fn:length(ticketHistoryList) > 0}">
                    <display:table id="ticketHistory" name="ticketHistoryList"  pagesize="${pageSize}" requestURI="" sort="list" class="table table-condensed table-border-none table-striped">
                        <display:column title="Ticket Id" sortable="true" headerClass="graydrkbg">
                            <c:set var="id" value="${ticketHistory.id}" />
                             <c:set var="encTicketId" ><encryptor:enc value="${ticketHistory.id}" isurl="true"/> </c:set>
                             <c:set var="encHouseHoldIdUrl" ><encryptor:enc value="${householdId}" isurl="true"/> </c:set>
                            <a href="/hix/ticketmgmt/ticket/ticketdetail/${encTicketId}?memberId=${encHouseHoldIdUrl}">${ticketHistory.number} </a>
                        </display:column>
                        <display:column property="subject" title="Subject" sortable="true" headerClass="graydrkbg" />
                        <display:column property="tkmWorkflows.type" title="Ticket Type" sortable="true" headerClass="graydrkbg" />
                        <display:column property="status" title="Status" sortable="true" headerClass="graydrkbg" />
                        <display:column property="created" title="Created Date" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
                        <display:column property="updated" title="Close Date" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
                        <display:setProperty name="paging.banner.placement" value="bottom" />
                        <display:setProperty name="paging.banner.some_items_found" value=''/>
                        <display:setProperty name="paging.banner.all_items_found" value=''/>
                        <display:setProperty name="paging.banner.group_size" value='50'/>
                        <display:setProperty name="paging.banner.last" value=''/>
                        <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
                        <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
                        <display:setProperty name="paging.banner.onepage" value=''/>
                        <display:setProperty name="paging.banner.one_item_found" value=''/>
                        <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
                            <div class="pagination center">
                            <ul>
                            <li>{0}</li>
                            <li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
                            </ul>
                            </div>
                            </span>'/>
                        <display:setProperty name="paging.banner.last" value='<span class="pagelinks">
                            <div class="pagination center">
                            <ul>
                            <li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
                            <li>{0}</li>
                            </ul>
                            </div>
                            </span>'/>
                        <display:setProperty name="paging.banner.full" value='
                            <div class="pagination center">
                            <ul>
                            <li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
                            <li>{0}</li>
                            <li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
                            </ul>
                            </div>
                            '/>
                    </display:table>
                </c:when>
                <c:otherwise>
                    <h4 class="alert alert-info">
                        <spring:message code='label.norecords' />
                    </h4>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <!--  end of row-fluid -->
</div>
<!--  end of gutter10 -->
<script type="text/javascript">
	$(document).ready(function() {
		if(window.location.pathname.indexOf("appealhistory")>0){
			$("#appealhistoryMenu").addClass("active navmenu");
		}else if(window.location.pathname.indexOf("tickethistory")>0){
				$("#tktHistoryMenu").addClass("active navmenu");
			}
	});
    function saveComment(ticketId)
    {
    	
    	var validateUrl = '<c:url value="/admin/ticketmgmt/savecomment"> <c:param name="${df:csrfTokenParameter()}"><df:csrfToken plainToken="true" /></c:param></c:url>';
    	var commentData = $("#comment_text").val();
    	$.ajax({
    		url: validateUrl,
    		data: {ticketId : ticketId,
    			comment : commentData},
    		success: function(response,xhr)
            {
    	        if(isInvalidCSRFToken(xhr))
    		      return;

    			if(response)
    			{
    				$('#markCompleteDialog').modal('hide');
    			}
    		}
    		});
    }
    
    function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
		rv = true;
		}
		return rv;
	}	   
</script>
<form name="dialogForm" id="dialogForm"  method="POST" action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
    <div class="markCompleteHeader">
        <div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
    <div class="modal-body clearfix gutter10-lr">
        <div class="control-group">
            <div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />

        <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
    </div>
</div>
</form>