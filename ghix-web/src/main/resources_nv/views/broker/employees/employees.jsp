<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/tablesorter.css" />" />

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<%-- File upload scripts --%>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<%
	//Code for pagination starts
	Integer pageSizeInt = (Integer) request.getAttribute("pageSize");
	String reqURI = (String) request.getAttribute("reqURI");
	Integer totalResultCount = (Integer) request
			.getAttribute("resultSize");
	if (totalResultCount == null) {
		totalResultCount = 0;
	}
	Integer currentPage = (Integer) request.getAttribute("currentPage");
	int noOfPage = 0;
	 
	if (totalResultCount > 0) {
		if ((totalResultCount % pageSizeInt) > 0) {
			noOfPage = (totalResultCount / pageSizeInt) + 1;
		} else {
			noOfPage = (totalResultCount / pageSizeInt);
		}
	}
	//Code for pagination ends
%>
<!--  -->
<style>
#rightpanel tbody tr td:first-child{padding-left: 11px;}
.page-breadcrumb {margin-top: 0 !important;}
.page-breadcrumb li i.icon-circle-arrow-left {top: 0px;}
</style>
<div class="gutter10-lr">
	<%--page-breadcrumb starts--%>
	<div class="l-page-breadcrumb">
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/broker/employees"/>"><spring:message
							code="label.employees" /></a></li>
				<li>${desigStatus}</li>
			</ul>
		</div>
	</div>
	<%--page-breadcrumb ends--%>
	<div class="row-fluid">

		<c:if test="${message != null}">
			<div class="errorblock alert alert-info">
				<p>${message}</p>
			</div>
		</c:if>
	<%--Employee search result count message starts--%>
		<div id="sidebar" class="span3" style="width: 100%;">
	            <div class="nav nav-list">
					<h1>
						<a name="skip"></a>
						<spring:message code="label.employees" /> <small>${resultSize} ${tableTitle}<c:if test="${resultSize > 1}">s</c:if></small>
					</h1>
				</div>
		</div>
	<%--Employee search result count message ends--%>
	</div>		
	<div class="row-fluid">
		<div class="gutter10-lr">
			<%--Employee search filter section on left side of page starts--%>
			<div id="sidebar" class="span3">
				<div class="header">
                    <h4>
                         <span id="rrb"><spring:message code="label.search.sectionHeader.refineResults" /></span>
                         <a class="pull-right margin5-r" href="#" onclick="resetAll()">(<spring:message code="label.brkResetAll"/>)</a>
                    </h4>
                </div>
				<div class="graybg">
					<form class="form-vertical gutter10" id="employeesearch"
						name="employeesearch" action="employees" method="POST">
						<input type="hidden" name="status" id="status"
							value="${param.desigStatus}" /> <input type="hidden"
							name="fromModule" id="fromModule" value="employees" />
	
						<div class="control-group">
							<label class="control-label" for="firstName" title="" ><spring:message code="label.search.firstName" /></label>
							<div class="controls">
								<input class="span" type="text" id="firstName" name="firstName"
									value="${searchCriteria.firstName}" placeholder="" tabindex="1" title="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="lastName" title="" ><spring:message code="label.search.lastName" /></label>
							<div class="controls">
								<input class="span" type="text" id="lastName" name="lastName"
									value="${searchCriteria.lastName}" placeholder="" tabindex="2" title="" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="companyName" title="" ><spring:message code="label.search.companyName" /></label>
							<div class="controls">
								<input class="span" type="text" id="companyName" name="companyName"
									value="${searchCriteria.companyName}" placeholder="" tabindex="3" title="" />
							</div>
						</div>
	
						<c:if test="${(desigStatus == 'InActive')}">
						<fieldset>
						<legend><spring:message code="label.search.inactiveSince" /></legend>
							<div class="control-group box-tight margin0">
								<div class="control-group">
									<label class="control-label" for="inactiveDateFrom" title="" ><spring:message code="label.search.from" /></label>
									<div class="controls">
										<input type="text" id="inactiveDateFrom"
											name="inactiveDateFrom" class="datepick input-small"
											title="MM/dd/yyyy" value="${searchCriteria.inactiveDateFrom}" tabindex="4" title="" />
										<div class="help-inline" id="inactiveDateFrom_error"></div>
									</div>
									<!-- end of controls-->
								</div>
	
								<div class="control-group">
									<label class="control-label" for="inactiveDateTo" title="" ><spring:message code="label.search.to" /></label>
									<div class="controls">
										<input type="text" id="inactiveDateTo" name="inactiveDateTo"
											class="datepick input-small" title="MM/dd/yyyy"
											value="${searchCriteria.inactiveDateTo}" tabindex="5" title="" />
										<div class="help-inline" id="inactiveDateTo_error"></div>
									</div>
									<!-- end of controls-->
								</div>
							</div>
							</fieldset>
						</c:if>
	
						<c:if test="${(desigStatus == 'Active')}">
						<fieldset>
							<legend title=""><spring:message code="label.search.enrollmentStatus" /></legend>
							<div class="control-group box-tight margin0">
								<select size="1" id="enrollment_status" name="enrollment_status" class="input-medium" tabindex="6" title="" >
									<option value=""><spring:message code="label.search.dropdown.option.select" /></option>
									<c:forEach var="enrollmentStatus" items="${enrollmentStatuslist}">
										<option value="${enrollmentStatus.lookupValueLabel}" <c:if test="${enrollmentStatus.lookupValueLabel == searchCriteria.status}"> selected="selected"</c:if> class="hourclass">
											${enrollmentStatus.lookupValueLabel}
										</option>																													
									</c:forEach>
								</select>
							</div>
						</fieldset>
						</c:if>
						<div class="gutter10">
							<input type="submit" name="submit" id="submit"
								class="btn input-small btn-primary offset3" value="<spring:message code="label.search.button.go" />" title="<spring:message code="label.search.button.go" />"/>
						</div>
					</form>
				</div>
			</div>
			<%--Employee search filter section on left side of page ends--%>
			<%--Employee table section of the page starts--%>
			<div id="rightpanel" class="span9">
	
				<form class="form-vertical" id="brokeremployees"
					name="brokeremployees" action="employees" method="POST">
					<input type="hidden" name="prevStatus" id="prevStatus"
						value="${desigStatus}"> <input type="hidden" name="ids"
						id="ids" value=""> <input type="hidden" name="fromModule"
						id="fromModule" value="employees">
	
					<div id="employeelist">
	
						<input type="hidden" name="id" id="id" value="">
							<c:if test="${fn:length(employeelist) > 0}">
								<table class="table table-striped" id="employeeListTable"
									name="employeeListTable">
									<thead>
										<tr>
											<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.employeeName" /></th>
											<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.company" /></th>
											<c:if test="${(desigStatus == 'Active')}">
												<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.enrollmentStatus" /></th>
												<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.effectiveDate" /></th>
												<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.renewalDate" /></th>
												<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.actions" /></th>
											</c:if>
											<c:if test="${(desigStatus == 'InActive')}">
												<th class="header" scope="col"><spring:message code="label.employeeList.columnHeader.inactiveSince" /></th>
											</c:if>
										</tr>
									</thead>
									<c:forEach items="${employeelist}" var="employee">
										<tr>
											<td>
												<c:if test="${(desigStatus == 'Active')}">
													<a id="employeedetailpagelink"  href="/hix/broker/employeedetails/${employee.id}">${employee.employeeName}</a>
												</c:if>
												<c:if test="${(desigStatus == 'InActive')}">
													<a id="employeesummarypopuplink" class="employeesummarypopupclass" href="<c:url value="/broker/employeesummarypopup?employeeName=${employee.employeeName}&employeeCompanyName=${employee.companyName}&employeePhoneNumber=${employee.employeePhoneNumber}&employeeEmail=${employee.employeeEmail}"/>">${employee.employeeName}</a>
												</c:if>
											</td>
											<td>${employee.companyName}</td>
											<c:if test="${(desigStatus == 'Active')}">
												<td class="txt-center">${employee.enrollmentStatus}</td>
												<td class="txt-center"><fmt:formatDate value="${employee.effectiveDate}" /></td>
												<td class="txt-center"><fmt:formatDate value="${employee.renewalDate}" /></td>
												<td class="txt-center">
													<div class="controls">
														<div class="dropdown">
															<a href="/page.html" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle">
																<i class="icon-gear"></i> <b class="caret"></b>
															</a>
															<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right">
																<li>
																	<a href="<c:url value="/broker/employeedetails/${employee.id}"/>"><spring:message code="label.employeeList.column.actions.details"/></a>
																</li>
															</ul>	
														</div>
													</div>				
												</td>
											</c:if>
											<c:if test="${(desigStatus == 'InActive')}">
												<td	><fmt:formatDate value="${employee.inactiveSinceDate}" /></td>
											</c:if>
										</tr>
									</c:forEach>
								</table>
							</c:if>
						<c:choose>
							<c:when
								test="${fn:length(employeelist) > 0 }">
								<div class="center">
								 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
								 </div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-info"><spring:message code="label.errorMessage.noMatchingRecordsFound" /></div>
							</c:otherwise>
						</c:choose>
					</div>
				</form>
			</div>
			<%--Employee table section of the page ends--%>
		</div>
	</div>
</div>

<%-- Secure Inbox Start--%>
<%-- <jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
<%-- Secure Inbox End--%>
	
<script type="text/javascript">

$(document).ready(function() {
	
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx + '/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});

	$("#employeesearch input:text").first().focus();
	
	$(".employeesummarypopupclass").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.indexOf('#') != 0) {
           $('<div id="employeesummarypopup" class="modal"><div class="modal-body"><iframe id="employeesummarypopupIframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:390px;"></iframe></div></div>').modal({backdrop:false});
		}
	});
	
	var employeeSearchForm =  $("#employeesearch");
	var employeeSearchformAction = employeeSearchForm.attr("action");
	var hDesigStatus = $("#status");

	employeeSearchformAction = employeeSearchformAction + "?desigStatus=" + hDesigStatus.val();
	
	employeeSearchForm.attr("action",employeeSearchformAction);
	
	var desigStatus = '${param.desigStatus}';
	
	try {
		if(desigStatus =="Active"){
			$('table[name="employeeListTable"]').tablesorter({
				sortList : [ [ 1, 0 ]]
			});
		}
		else{
			$('table[name="employeeListTable"]').tablesorter({
				sortList : [ [ 3, 0 ]]
			});	
		}
	}catch (err) {
		;
	}
	
});
	/* var validator = $("#employeesearch")
			.validate(
					{
						onkeyup : false,
						onclick : false,
						rules : {
							inactiveDateFrom : {
								required : false,
								chkDateFormat : true
							},
							inactiveDateTo : {
								required : false,
								chkDateFormat : true
							}
						},
						messages : {
							inactiveDateFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"
							},
							inactiveDateTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"
							}
						}
					}); */
	
	jQuery.validator.addMethod("chkDateFormat",
			function(value, element, param) {
				var isValid = false;
				var reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
				if (reg.test(value)) {
					var splittedDate = value.split('/');
					var mm = parseInt(splittedDate[0], 10);
					var dd = parseInt(splittedDate[1], 10);
					var yyyy = parseInt(splittedDate[2], 10);
					var newDate = new Date(yyyy, mm - 1, dd);
					if ((newDate.getFullYear() == yyyy)
							&& (newDate.getMonth() == mm - 1)
							&& (newDate.getDate() == dd))
						isValid = true;
					else
						isValid = false;
				} else
					isValid = false;

				return isValid;

			});
	
	function closeEmployeeSummaryPopup(){
		$("#employeesummarypopup").remove();
	}
	
	/**
	* Function to reset search condition data. 
    */
	function resetAll() {
		var  contextPath =  "<%=request.getContextPath()%>";
        var documentUrl = contextPath + "/broker/employees/resetall?desigStatus=${desigStatus}";
       	window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
    }
</script>
