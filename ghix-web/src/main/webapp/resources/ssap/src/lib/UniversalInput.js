import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Components

import Checkbox from './Checkbox';
import DatePicker from './DatePicker';
import Dropdown from './Dropdown';
import Input from './Input';
import InputWithLRBtn from './InputWithLRBtn';
import MoneyInput from './MoneyInput';
import PhoneNumberInput from './PhoneNumberInput';
import RadioButtons from './RadioButtons';
import RadioCheckBoxes from './RadioCheckBoxes';
import SocialSecurityNumberInput from './SocialSecurityNumberInput';
import ValidationErrorMessage from '../components/notifications/ValidationErrorMessage';

// ActionCreators

import { changeInputStatus, registerInput, reRegisterInput, removeInput } from '../actions/actionCreators';

// Utils

import mergeRecursively from '../utils/mergeRecursively';
import { pageNumberAdjuster } from '../utils/pageNumberAdjuster';

// Constants

import validators from '../utils/validators';
import INPUT_TYPES from '../constants/inputTypes';



const UniversalInput = props => {

    const {
        inputData,
        inputData: {
            currentData,
            disabled,
            isInputRequired,
            fields,
            name,
            type,
            validationErrorMessage,
            value,
            valuesToMatch,
            validationType,
            isToggleReRegistration = undefined
        },
        indexInArray,
        inputActions,
        inputActions: {
            onFieldBlur,
            onFieldChange
        },
        // indicator,
        errorClasses,
        legend
    } = props;

    const {
        parentForm: {
            name: formName
        },
        form,
        page
    } = props;

    const {
        isFormSubmitted,
        registeredInputs
    } = form[formName];

    const getLastField = arr => {
        return arr && arr.length >= 1 ? `_${arr[arr.length - 1]}` : '';
    };

    const getHouseholdMemberIndex = index => {
        return index !== undefined ? `_hhm${index}` : '';
    };

    const createInputID = () => {
        return [...getChunksForID()].join('');
    };

    const getChunksForID = function * () {

        yield pageNumberAdjuster(page);
        yield getHouseholdMemberIndex(indexInArray);
        yield getLastField(fields);
        yield `_${name}`;
    };

    const validate = (value, valuesToMatch) => {
        const {
            isValidatorsGeneratorResultsShown : isGenShown
        } = props.console;

        if (!validator) {
            return true;
        }

        if(!isInputRequired && (value === null || value === '')) {
            return true;
        } else if(isInputRequired && (value === null || value === '')){
            return false;
        } else {
            return !!(validator && validator.verifier !== undefined && validator.verifier({
                value,
                valuesToMatch,
                isGenShown
            }));
        }
    };

    const ID = createInputID();
    const validator = validators.find(v => v.validatorType === props.inputData.validationType);

    useEffect(() => { // Mounting and unmounting inputs causes registration and removing
        props.actions.registerInput({
            inputId: ID,
            initialInputValue: value,
            isInputTouched: null,
            inputName: name,
            inputValue: value,
            isInputRequired: isInputRequired,
            inputIsValid: validate(value, valuesToMatch),
            messages: validator && validator.messages,
            inputValidationType: validationType ? validationType : null,
            valueOf() {
                return this.inputIsValid;
            }
        }, formName);
        return () => {
            props.actions.removeInput({ inputId: ID }, formName);
        };
    }, []);

    useEffect(() => {
        if(isToggleReRegistration !== undefined) {
            props.actions.reRegisterInput({
                inputId: ID,
                initialInputValue: value,
                isInputTouched: null,
                inputName: name,
                inputValue: value,
                isInputRequired: isInputRequired,
                inputIsValid: validate(value, valuesToMatch),
                messages: validator && validator.messages,
                inputValidationType: validationType ? validationType : null,
                valueOf() {
                    return this.inputIsValid;
                }
            }, formName);
        }
    }, [ isToggleReRegistration ]);

    useEffect(() => { // Here we watch on any change in registered input and validate it
        const isValid = validate(value, valuesToMatch);

        props.actions.changeInputStatus({
            inputId: ID,
            inputValue: value,
            inputIsValid: isValid
        }, formName);

    }, [value]);

    useEffect(() => {
        isFormSubmitted !== null && validate(value, valuesToMatch);
    }, [isFormSubmitted]);

    const getNewData = updatedValue => {
        const resolvedName = name[0] === '_' ? name.slice(1) : name;
        const injectedProp = {[resolvedName]: updatedValue};
        return mergeRecursively(currentData, injectedProp, [], fields, updatedValue, resolvedName);
    };

    const handleFieldBlur = updatedValue => {
        onFieldBlur && onFieldBlur(getNewData(updatedValue), indexInArray, fields, [name, updatedValue, disabled]);
    };

    const handleFieldChange = (updatedValue, text = undefined) => {
        onFieldChange && onFieldChange(getNewData(updatedValue), indexInArray, fields, [name, updatedValue, disabled, text]);
    };

    const getInput = () => {

        const injectedProps = {
            ...props,
            inputData:{
                ...inputData,
                type: type.toLowerCase(),
                id: ID
            },
            inputActions: {
                ...inputActions,
                handleFieldChange: handleFieldChange, // Injecting handleFieldChange()
                handleFieldBlur: handleFieldBlur      // Injecting handleFieldBlur()
            }
        };

        // Normalizing <inputData> object to prevent UI fall, if <legend> property isn't passed

        if(!legend) {
            injectedProps.legend = {
                legendText: ''
            };
        }

        switch (type) {
        case INPUT_TYPES.CHECKBOX:
            return <Checkbox {...injectedProps}/>;
        case INPUT_TYPES.DATE_PICKER:
            return <DatePicker {...injectedProps} />;
        case INPUT_TYPES.DROPDOWN:
            return <Dropdown {...injectedProps} />;
        case INPUT_TYPES.REGULAR_INPUT:
            return <Input {...injectedProps} />;
        case INPUT_TYPES.INPUT_LR_BUTTONS:
            return <InputWithLRBtn {...injectedProps} />;
        case INPUT_TYPES.MONEY_INPUT:
            return <MoneyInput {...injectedProps} />;
        case INPUT_TYPES.PHONE_NUMBER_INPUT:
            return <PhoneNumberInput {...injectedProps}/>;
        case INPUT_TYPES.RADIO_BUTTONS:
            return <RadioButtons {...injectedProps} />;
        case INPUT_TYPES.RADIO_CHECKBOXES:
            return <RadioCheckBoxes {...injectedProps}/>;
        case INPUT_TYPES.SSN_INPUT:
            return <SocialSecurityNumberInput {...injectedProps}/>;
        default:
            return null;
        }
    };

    const inputFromForm = registeredInputs.length > 0 && registeredInputs.find(input => input.inputId === ID);

    const isInputValid = inputFromForm && inputFromForm.inputIsValid;

    const message = validationErrorMessage ? validationErrorMessage : inputFromForm && inputFromForm.messages && inputFromForm.messages[0];

    return (
        <Fragment>
            <ValidationErrorMessage
                isVisible={ isFormSubmitted && !isInputValid }
                messageText={ message }
                errorClasses={ errorClasses }
            />
            {
                getInput()
            }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    console: state.console,
    form: state.form,
    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeInputStatus,
        registerInput,
        reRegisterInput,
        removeInput
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversalInput);