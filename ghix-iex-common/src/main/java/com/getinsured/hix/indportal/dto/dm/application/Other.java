package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class Other {
	 private String reconcilePtcIndicatorType;

	 private Map<String,ChangeInCircumstance> changeInCircumstance;
	 private AmericanIndianAlaskanNative americanIndianAlaskanNative;
	 private Boolean veteranIndicator;
	 private String incarcerationType;
	 private Boolean appliedDuringOeOrLifeChangeIndicator;


	 // Getter Methods 

	

	 public String getIncarcerationType() {
	  return incarcerationType;
	 }

	 public Map<String, ChangeInCircumstance> getChangeInCircumstance() {
		return changeInCircumstance;
	}

	public void setChangeInCircumstance(Map<String, ChangeInCircumstance> changeInCircumstance) {
		this.changeInCircumstance = changeInCircumstance;
	}

	public Boolean getAppliedDuringOeOrLifeChangeIndicator() {
	  return appliedDuringOeOrLifeChangeIndicator;
	 }

	 // Setter Methods 

	 public void setIncarcerationType(String incarcerationType) {
	  this.incarcerationType = incarcerationType;
	 }

	 public void setAppliedDuringOeOrLifeChangeIndicator(Boolean appliedDuringOeOrLifeChangeIndicator) {
	  this.appliedDuringOeOrLifeChangeIndicator = appliedDuringOeOrLifeChangeIndicator;
	 }
	 public String getReconcilePtcIndicatorType() {
		return reconcilePtcIndicatorType;
	}

	public void setReconcilePtcIndicatorType(String reconcilePtcIndicatorType) {
		this.reconcilePtcIndicatorType = reconcilePtcIndicatorType;
	}

	public AmericanIndianAlaskanNative getAmericanIndianAlaskanNative() {
		return americanIndianAlaskanNative;
	}

	public void setAmericanIndianAlaskanNative(AmericanIndianAlaskanNative americanIndianAlaskanNative) {
		this.americanIndianAlaskanNative = americanIndianAlaskanNative;
	}


	public Boolean getVeteranIndicator() {
		return veteranIndicator;
	}

	public void setVeteranIndicator(Boolean veteranIndicator) {
		this.veteranIndicator = veteranIndicator;
	}
	}