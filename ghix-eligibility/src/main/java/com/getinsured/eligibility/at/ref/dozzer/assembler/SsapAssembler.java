/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.assembler;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * @author chopra_s
 * 
 */
public interface SsapAssembler {

	String ERR_NOT_MAPPED = "The mapped SingleStreamlinedApplication object cannot be null";

	String SAME_RELATION_CODE = "18";
	String SELF_RELATION_CODE = "18";

	String PRIMARYTAXFILER = "PrimaryTaxFiler";

	String SPOUSETAXFILER = "SpouseTaxFiler";

	String TAXDEPENDENT = "TaxDependent";
	
	String HOUSEHOLDINCOME = "HouseholdIncome";

	public SingleStreamlinedApplication assembleSsapXmlToJson(AccountTransferRequestDTO accountTransferRequestDTO);

	public void buildSsapBeanForJson(SingleStreamlinedApplication singleStreamlinedApplication);

	public String transformSsapToJson(SingleStreamlinedApplication singleStreamlinedApplication);
	
	public long compareAndUpdateStatus(AccountTransferRequestDTO accountTransferRequestDTO);
	
	boolean isATRequestFromFFM(AccountTransferRequestPayloadType accountTransferRequest);
	
	boolean updateIncomes(AccountTransferRequestPayloadType source, SingleStreamlinedApplication singleStreamlinedApplication);
	
	SsapApplicantPersonType determineApplicantPersonType(final String primaryApplicantId, final String primaryTaxFilerId,
			String personId, boolean isApplyingForCoverage);
}
