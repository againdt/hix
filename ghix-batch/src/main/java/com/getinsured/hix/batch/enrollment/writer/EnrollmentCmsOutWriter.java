package com.getinsured.hix.batch.enrollment.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrollmentCmsOutWriter  implements ItemWriter<Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCmsOutWriter.class);
	private EnrollmentCmsOutService enrollmentCmsOutService;
	private JobService jobService;
	//long jobExecutionId = -1;
	List<Integer> enrollmentIdList;	
	Integer partition;
	Integer issuerId;
	Integer year;
	Integer month;
	Integer batchYear;
	long batchStartTime;
	Long batchExecutionId;
	
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			issuerId=ec.getInt("issuerId");
			year=ec.getInt("year");
			month =ec.getInt("month");
			batchYear = ec.getInt("batchYear");
			batchStartTime = ec.getLong("batchStartTime");
			batchExecutionId = stepExecution.getJobExecutionId();
			//jobExecutionId=stepExecution.getJobExecution().getId();
			LOGGER.info("EnrollmentCmsOutWriter : Thread Name: "+Thread.currentThread().getName() +" IssuerId: "+issuerId+" Year: "+ year );
			}
	}

	@Override
	public void write(List<? extends Integer> enrollmentIdList) throws Exception {
		String batchJobStatus=null;
		try{
			if(jobService != null && batchExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(batchExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				enrollmentCmsOutService.deleteFileFromWip();
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			if(null != enrollmentIdList && !enrollmentIdList.isEmpty()){
				
				enrollmentCmsOutService.makeAndWriteCMSXml((List<Integer>) enrollmentIdList, year, month, issuerId, batchYear, batchExecutionId, partition, batchStartTime);
			}	
		}catch(Exception e){
			if(e.getMessage()!=null && e.getMessage().contains(EnrollmentConstants.BATCH_STOP_MSG)){
				enrollmentCmsOutService.deleteFileFromWip();
				enrollmentCmsOutService.deleteFromEnrollmentCmsOut(batchExecutionId);
				throw e;
			}
		}
		
		
			
	}

	public EnrollmentCmsOutService getEnrollmentCmsOutService() {
		return enrollmentCmsOutService;
	}

	public void setEnrollmentCmsOutService(
			EnrollmentCmsOutService enrollmentCmsOutService) {
		this.enrollmentCmsOutService = enrollmentCmsOutService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
