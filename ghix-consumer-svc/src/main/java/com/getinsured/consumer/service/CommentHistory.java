package com.getinsured.consumer.service;

import com.getinsured.hix.consumer.history.ConsumerHistoryBase;

public class CommentHistory extends ConsumerHistoryBase {

	private MainData mainData;
	private NoteMetaData metadata;
	private String type;

	/**
	 * @param mainData
	 * @param metadata
	 * @param type
	 */

	public CommentHistory() {

	}

	public CommentHistory(MainData mainData, NoteMetaData metadata, String type) {
		super(metadata.getDate());
		this.mainData = mainData;
		this.metadata = metadata;
		this.type = type;
	}

	public MainData getMainData() {
		return mainData;
	}

	public void setMainData(MainData mainData) {
		this.mainData = mainData;
	}

	public NoteMetaData getMetadata() {
		return metadata;
	}

	public void setMetadata(NoteMetaData metadata) {
		this.metadata = metadata;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
