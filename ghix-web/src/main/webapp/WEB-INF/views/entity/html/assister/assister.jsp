<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/addressutils.js" />"></script>
<script src="../resources/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../resources/js/vimo.assister.js" type="text/javascript"></script>
<script src="../resources/js/bootstrap-typeahead.js" type="text/javascript"></script>
<link href="../resources/css/assister.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../resources/css/accessibility.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../resources/css/autoSuggest.css" media="screen" rel="stylesheet" type="text/css" />
<script src="../resources/js/jquery.autoSuggest.js" type="text/javascript"></script>
<script src="../resources/js/jquery.autoSuggest.minified.js" type="text/javascript"></script>

 <div id="main">
 <div class="container">
<div class="row-fluid">
	
	<div class="span9">
			<div class="graydrkaction">
				<h4 class="span10">NEW ASSISTER FORM</h4>
			</div>
				<form class="form-horizontal gutter10" id="frmAssister" name="frmAssister" action="assister" method="POST">
				
				<div class="control-group">
					<label for="name" class="required control-label" id="name">Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<c:choose>
							<c:when test="${assister.name != null && assister.name != ''}">
								<input type="text" name="name_readonly" id="name" value="${assister.name}" readonly="readonly" class="input-xlarge" size="30" />
								<input type="hidden" name="name" id="name" value="${assister.name}" />
							</c:when>
							<c:otherwise>
								<input type="text" name="name" id="name" value="${assister.name}" class="input-xlarge" size="30" />
							</c:otherwise>
						</c:choose>
						<div id="name_error"></div>
			       </div>
				 </div>
				 
				 <div class="control-group">
					<label for="name" class="required control-label" id="name">Email Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<c:choose>
							<c:when test="${assister.emailAddress != null && assister.emailAddress != ''}">
								<input type="text" name="emailAddress_readonly" id="name" value="${assister.emailAddress}" readonly="readonly" class="input-xlarge" size="30" />
								<input type="hidden" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" />
							</c:when>
							<c:otherwise>
								<input type="text" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" class="input-xlarge" size="30" />
							</c:otherwise>
						</c:choose>
						<div id="emailAddress_error"></div>
			       </div>
				 </div>
				<div class="control-group phone-group">
							<label for="phone1" class="required control-label">Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<label for="phone2" class="required hidden">Phone Number</label>
							<label for="phone3" class="required hidden">Phone Number</label>
							<div class="controls">
								<input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" placeholder="408" class="area-code input-mini" onKeyUp="shiftbox(this, 'phone2')" />
								<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" placeholder="833" class="input-mini" onKeyUp="shiftbox(this, 'phone3')" />
								<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" placeholder="1861" class="input-small" />
								<div id="phone3_error"></div>
							</div>
				</div>
				<div class="control-group phone-group">
							<label for="phone11" class="required control-label">Secondary Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<label for="phone22" class="required hidden">Secondary Phone Number</label>
							<label for="phone33" class="required hidden">Secondary Phone Number</label>
							<div class="controls">
								<input type="text" name="phone11" id="phone11" value="${phone11}" maxlength="3" placeholder="408" class="area-code input-mini" onKeyUp="shiftbox(this, 'phone22')" />
								<input type="text" name="phone22" id="phone22" value="${phone22}" maxlength="3" placeholder="833" class="input-mini" onKeyUp="shiftbox(this, 'phone33')" />
								<input type="text" name="phone33" id="phone33" value="${phone33}" maxlength="4" placeholder="9978" class="input-small" />
								<div id="phone33_error"></div>
							</div>
				</div>
				
				 <div class="control-group">
					<label for="name" class="required control-label" id="name">Preferred Method of Communication <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<c:choose>
							<c:when test="${assister.communicationPreference != null && assister.communicationPreference != ''}">
								<input type="text" name="communicationPreference_readonly" id="name" value="${assister.communicationPreference}" readonly="readonly" class="input-xlarge" size="30" />
								<input type="hidden" name="communicationPreference" id="communicationPreference" value="${assister.communicationPreference}" />
							</c:when>
							<c:otherwise>
								<input type="text" name="communicationPreference" id="communicationPreference" value="${assister.communicationPreference}" class="input-xlarge" size="30" />
							</c:otherwise>
						</c:choose>
						<div id="communicationPreference_error"></div>
			       </div>
				 </div>
				 
				  <div class="control-group">
					<label for="name" class="required control-label" id="name">Preferred Method of Communication <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<c:choose>
							<c:when test="${assister.certificationStatus != null && assister.certificationStatus != ''}">
								<input type="text" name="certificationStatus_readonly" id="name" value="${assister.certificationStatus}" readonly="readonly" class="input-xlarge" size="30" />
								<input type="hidden" name="certificationStatus" id="certificationStatus" value="${assister.certificationStatus}" />
							</c:when>
							<c:otherwise>
								<input type="text" name="certificationStatus" id="certificationStatus" value="${assister.certificationStatus}" class="input-xlarge" size="30" />
							</c:otherwise>
						</c:choose>
						<div id="certificationStatus_error"></div>
			       </div>
				 </div>
				 
				<div class="control-group">
					<label for="isAssisterCertified" class="required control-label">Is this Assister Certified? <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="radio" name="isAssisterCertified" value="Phone" id="selectNo">No
						<input type="radio" name="isAssisterCertified" value="Email" id="selectYes" checked="checked">Yes
						<div id="isAssisterCertified_error" class="help-inline"></div>
					</div>
				</div>
				<div class="control-group">
						<label for="assisterCertification" class="required control-label">Assister Certification # <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">		
								<input type="text" name="assisterCertification_readonly" id="assisterCertification" value=""  readonly="readonly" class="input-xlarge" size="30" />
								<input type="hidden" name="assisterCertification" id="assisterCertification" value="assisterCertification" />
							<div id="assisterCertification_error"></div>
						</div>
				</div>
				<div class="control-group">
					<label for="state" class="control-label">Assister Sub-Site <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<select size="1" id="state" name="primaryAssisterSite" path="sitelist" class="input-medium">
							<option value="">Select</option>
							<c:forEach var="site" items="${sitelist}">
								<option id="${site.id}" <c:if test="${site.id == assister.primaryAssisterSite.id}"> selected="selected"</c:if>  value="<c:out value="${assister.primaryAssisterSite.id}" />">
									<c:out value="${assister.primaryAssisterSite.primarySiteLocationName}" />
								</option>
							</c:forEach>
						</select>
						<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.primaryAssisterSite}">
						<div id="site_error"></div>
					</div>	
				</div>
				
				<div class="graydrkaction">
					<h4 class="span10">Mailing Address</h4>
				</div>
				<div class="control-group">
					<label for="address1_mailing" class="control-label required">Street Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">							
						<input type="text" placeholder="Street Name, P.O. Box, Company, c/o" name="mailingLocation.address1" id="address1_mailing" value="${assister.mailingLocation.address1}" class="input-xlarge" size="30" />
						<input type="hidden" id="address1_mailing_hidden" name="address1_mailing_hidden" value="${assister.mailingLocation.address1}">
						<div id="address1_mailing_error"></div>
					</div>	
				</div>	
				
				<div class="control-group">
					<label for="address2_mailing" class="control-label">Suite/Apartment Number</label>
					<div class="controls">
						<input type="text" placeholder="Apt, Suite, Unit, Bldg, Floor, etc" name="mailingLocation.address2" id="address2_mailing" value="${assister.mailingLocation.address2}" class="input-xlarge" size="30" />
						<input type="hidden" id="address2_mailing_hidden" name="address2_mailing_hidden" value="${assister.mailingLocation.address2}">
					</div>
				</div>
				 
				<div class="control-group">
					<label for="city" class="required control-label" id="city">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
		 					<input type="text" name="city_readonly" id="city" class="input-xlarge" size="30" />
							<input type="hidden" name="city" id="city" />
							<div id="city_error"></div>
				       </div>
				 </div>
				 
				 <div class="control-group">
						<label for="city_mailing" class="control-label">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" placeholder="City, Town" name="mailingLocation.city" id="city_mailing" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" />
							<input type="hidden" id="city_mailing_hidden" name="city_mailing_hidden" value="${assister.mailingLocation.city}">
						<div id="city_mailing_error"></div>
						</div>	<!-- end of controls-->
					</div>	
				 
				 <div class="control-group">
					<label for="zip" class="control-label">Zip Code <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" placeholder="" name="zip" id="zip" value="${assister.location.zip}" class="input-mini zipCode" maxlength="5"	 />
						<input type="hidden" id="zip_hidden" name="zip_hidden" value="${assister.location.zip}">	
						<div id="zip_error"></div>
					</div>
				</div>	
				<div class="control-group">
					<label for="state" class="control-label">State <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<select size="1" id="state" name="mailingLocation.state" path="statelist" class="input-medium">
							<option value="">Select</option>
							<c:forEach var="state" items="${statelist}">
								<option id="${state.code}" <c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>  value="<c:out value="${state.code}" />">
									<c:out value="${state.name}" />
								</option>
							</c:forEach>
						</select>
						<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.mailingLocation.state}">
						<div id="state_error"></div>
					</div>	
				</div>	
				
			
				<div class="graydrkaction">
					<h4 class="span10">Profile Information</h4>
				</div>
				
              <div id="languagesSupported" >
               <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Language</th>
                      <th>Spoken Languages Supported</th>
                      <th>Written Languages Supported</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="language" items="${languagelist}">
	                  	<tr>
	                      <td><label><c:out value="${language.lookupValueLabel}" /></label></td>
	                      <td><input type="checkbox" id="${language.lookupValueId}" value="${language.lookupValueCode}"></td>
	                      <td><input type="checkbox" id="${language.lookupValueId}" value="${language.lookupValueCode}"></td>
	                    </tr>
					</c:forEach>
						<tr>
	                      <td>Other</td>
	                      <td><input type="text" id="" placeholder=""></td>
	                      <td><input type="text" id="" placeholder=""></td>
	                    </tr>
                   </tbody>
                </table>
              </div>
              
            	<div class="control-group">
					<label for="education" class="control-label required">Education <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<select id="education" class="input-xlarge" name="education">
							<option value="">--</option>
							<option value="Did Not Complete High School/GED">Did Not Complete High School/GED</option>
							<option value="Completed GED/HSED">Completed GED/HSED</option>
							<option value="Graduated From High School">Graduated From High School</option>
							<option value="Some College, No Degree">Some College, No Degree</option>
							<option value="One Year Vocational Diploma">One Year Vocational Diploma</option>
							<option value="Two Year Associate Degree">Two Year Associate Degree</option>
							<option value="Bachelor Degree">Bachelor Degree</option>
							<option value="Some Graduate Degree Courses">Some Graduate Degree Courses</option>
							<option value="Graduate College Degree">Graduate College Degree</option>
						</select>
						<div id="education_error"></div>
					</div>	
				</div>
				
				<div class="control-group">
						<label class="control-label" for="uploadPhoto">Upload Photo <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls paddingT5">
							<input type="file" class="input-file" id="uploadPhoto" name="uploadPhoto">&nbsp;
			   <!--              <input type="submit" id="btnUploadPhoto" value="Browse" class="btn">
			    -->             <div id="uploadPhoto_error" class="help-inline"></div>
						</div>
				</div>
					
			</div>
			
			<div class="form-actions">
				<input type="submit" name="SaveAssister" id="SaveAssister" value="Save Assister" class="btn btn-primary" />							
			</div>
		</form>
	</div>	
</div>
</div>
</div>		
