import _ from 'lodash';

export const _destructureProxy = obj => {
    const handler = {
        get: (target, prop) => {
            if(prop in target) {
                if(_.isPlainObject(target[prop])) {
                    return new Proxy(target[prop], handler);
                } else {
                    return Reflect.get(target, prop);
                }
            } else {
                throw new Error(`There is no such prop "${prop}" in passed for destructure object`);
            }
        }
    };

    return new Proxy(obj, handler);
};

export const _oneArgumentProxy = array => {

    return new Proxy(array, {
        get: (target, prop) => {
            if(target.length !== 1) {
                throw new Error(
                    `Wrong qty of arguments has been passed to function. 
                Expected qty is 1. Was passed ${target.length}. 
                Unexpected ones: ${JSON.stringify(target.slice(1))}`
                );
            }
            return Reflect.get(target, prop);
        }
    });
};

