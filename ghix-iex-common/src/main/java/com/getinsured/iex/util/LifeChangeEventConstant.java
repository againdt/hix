package com.getinsured.iex.util;

import org.springframework.stereotype.Component;

/**
 * Used for defining all the constants for Life Change Event
 */
@Component
public class LifeChangeEventConstant {
	
	public enum Status {
		LCE_ADD, LCE_UPDATE, LCE_DELETE, LCE_NO_CHANGE;
	}
	
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	
	public static final String FAILURE = "FAILURE";
	public static final String ENDPOINT_FUNCTION = "LCE";
	public static final String ENDPOINT_OPERATION_SHOW = "SHOW_LCE_DATA";
	public static final String ENDPOINT_OPERATION_SAVE = "SAVE_LCE_DATA";
	public static final String ENDPOINT_OPERATION_INVOKE_SSAP_INTEGRATION = "INVOKE_SSAP_INTEGRATION";
	public static final String FAILED = "Failed";
	public static final String IS_DEMOGRAPHIC_CHANGE = "IS_DEMOGRAPHIC_CHANGE";
	public static final String IS_DOB_CHANGE = "IS_DOB_CHANGE";
	public static final String IS_OTHER_CHANGE = "IS_OTHER_CHANGE";
	public static final String IS_ADDRESS_DEMOGRAPHIC_CHANGE = "IS_ADDRESS_DEMOGRAPHIC_CHANGE";
	public static final String HREF = "href";
	public static final String SELF = "self";
	public static final String SLASH = "/";
	public static final String YES = "Y";
	public static final String PARTIAL = "P";
	public static final String NO = "N";
	public static final Integer SIXTY_FIVE = 65;
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String SUBMITTED = "SUBMITTED";
	public static final String COMPLETED = "C";
	public static final String APPLICATION_TYPE = "SEP";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final long PRIMARY_APPLICANT_PERSON_ID = 1;
	public static final String ON = "ON";
	public static final String ELIGIBILITY_STATUS_PE = "PE";
	public static final String ELIGIBILITY_STATUS_DE = "DE";
	public static final String NONE = "NONE";
	public static final String MIN = "MIN";
	public static final String MAX = "MAX";
	public static final String EXADMIN_USERNAME = "exadmin@ghix.com";
	
	public static final String MARITAL_STATUS = "MARITAL_STATUS";
	public static final String MARITAL_STATUS_MARRIAGE = "MARRIAGE";
	public static final String MARITAL_STATUS_CIVIL_UNION = "CIVIL_UNION";
	public static final String MARITAL_STATUS_DEATH_OF_SPOUSE = "DEATH";
	public static final String MARITAL_STATUS_DIVORCE_OR_ANULLMENT = "DIVORCE_OR_ANULLMENT";
	public static final String MARITAL_STATUS_LEGAL_SEPERATION = "LEGAL_SEPERATION";
	
	public static final String INCARCERATION = "INCARCERATION";
	public static final String INCARCERATION_CHANGE = "INCARCERATION_CHANGE";
	public static final String INCARCERATED_PRIMARY = "INCARCERATED_PRIMARY";
	public static final String INCARCERATED_DEPENDENTS = "INCARCERATED_DEPENDENTS";
	public static final String NOT_INCARCERATED = "NOT_INCARCERATED";
	public static final String NOT_INCARCERATED_OTHER = "NOT_INCARCERATED_OTHER";
	
	public static final String UPDATE_DEPENDENTS = "UPDATE_DEPENDENTS";
	public static final String UPDATE_DEPENDENTS_ADD_DEPENDENT = "ADD_DEPENDENT(S)";
	public static final String UPDATE_DEPENDENTS_DEATH_OF_PRIMARY_APPLICANT = "DEATH_PRIMARY";
	public static final String UPDATE_DEPENDENTS_DEATH_OF_DEPENDENTS = "DEATH_DEPENDENTS";
	public static final String UPDATE_DEPENDENTS_DEATH = "DEATH";
	public static final String UPDATE_DEPENDENTS_REMOVE_DEPENDENT = "REMOVE_DEPENDENT";
	public static final String UPDATE_DEPENDENTS_ADOPTION = "ADOPTION";
	public static final String UPDATE_DEPENDENTS_DEPENDENT_CHILD_AGES_OUT = "DEPENDENT_CHILD_AGES_OUT";
	public static final String UPDATE_DEPENDENTS_BIRTH = "BIRTH";
	public static final String UPDATE_DEPENDENTS_GAIN_COURT_APPOINTED_DEPENDENT = "GAIN_COURT_APPOINTED_DEPENDENT";
	
	public static final String ADDRESS_CHANGE = "ADDRESS_CHANGE";
	public static final String ADDRESS_CHANGE_WITHIN_STATE = "CHANGE_IN_ADDRESS_WITHIN_STATE";
	public static final String ADDRESS_CHANGE_MOVED_INTO_STATE = "MOVED_INTO_STATE";
	public static final String ADDRESS_CHANGE_MOVED_OUT_OF_STATE = "MOVED_OUT_OF_STATE";
	public static final String ADDRESS_CHANGE_MOVED_OUT_OF_STATE_DEPENDENTS = "MOVED_OUT_OF_STATE_DEPENDENTS";
	public static final String ADDRESS_CHANGE_MOVED_INTO_STATE_OTHER = "MOVED_INTO_STATE_OTHER";
	public static final String ADDRESS_CHANGE_MOVED_OUT_OF_STATE_OTHER  ="MOVED_OUT_OF_STATE_OTHER";
	
	public static final String TRIBE_STATUS = "TRIBE_STATUS";
	public static final String TRIBE_STATUS_GAIN_IN_TRIBE_STATUS = "GAIN_IN_TRIBE_STATUS";
	
	public static final String LOSS_OF_MEC = "LOSS_OF_MEC";
	public static final String LOSS_OF_MEC_LOSE_EMPLOYER_COVERAGE = "LOSE_EMPLOYER_COVERAGE";
	public static final String LOSS_OF_MEC_LOSE_OTHER_PUBLIC_MEC = "LOSE_OTHER_PUBLIC_MEC";
	public static final String LOSS_OF_MEC_LOSE_COVERAGE_NON_PAYMENT = "LOSE_COVERAGE_NON_PAYMENT";
	
	public static final String GAIN_MEC = "GAIN_MEC";
	public static final String GAIN_MEC_GAIN_EMPLOYER_COVERAGE_THRU_SPOUSE = "GAIN_EMPLOYER_COVERAGE_THRU_SPOUSE";
	public static final String GAIN_MEC_GAIN_OTHER_PUBLIC_MEC = "GAIN_OTHER_PUBLIC_MEC";
	
	public static final String IMMIGRATION_STATUS_CHANGE = "IMMIGRATION_STATUS_CHANGE";
	public static final String IMMIGRATION_STATUS_CHANGE_GAIN_CITIZENSHIP = "GAIN_CITIZENSHIP";
	public static final String IMMIGRATION_STATUS_CHANGE_GAIN_LPR = "GAIN_LPR";
	public static final String IMMIGRATION_STATUS_CHANGE_LOSE_CITIZENSHIP = "LOSE_CITIZENSHIP";
	public static final String IMMIGRATION_STATUS_CHANGE_CHANGE_LPR = "CHANGE_LPR";
	
	public static final String DEMOGRAPHICS = "DEMOGRAPHICS";
	public static final String DEMOGRAPHICS_NAME_CHANGE = "NAME_CHANGE";
	public static final String DEMOGRAPHICS_SSN_CHANGE = "SSN_CHANGE";
	public static final String DEMOGRAPHICS_MAILING_ADDRESS_CHANGE = "MAILING_ADDRESS_CHANGE";
	public static final String DEMOGRAPHICS_DOB_CHANGE = "DOB_CHANGE";
	public static final String DEMOGRAPHICS_ETHNICITY_RACE_CHANGE = "ETHNICITY_RACE_CHANGE";
	public static final String DEMOGRAPHICS_PHONE_NUMBER_CHANGE = "PHONE_NUMBER_CHANGE";
	public static final String DEMOGRAPHICS_GENDER_CHANGE = "GENDER_CHANGE";
	public static final String DEMOGRAPHICS_EMAIL_CHANGE = "EMAIL_CHANGE";
	public static final String DEMOGRAPHICS_RELATIONSHIP_CHANGE = "RELATIONSHIP_CHANGE";
	public static final String DEMOGRAPHICS_RELATIONSHIP_CHANGE_REMOVE = "RELATIONSHIP_CHANGE_REMOVE";
	public static final String DEMOGRAPHICS_DOB_CHANGE_REMOVE = "DOB_CHANGE_REMOVE";
	
	public static final String OTHER = "OTHER";
	public static final String OTHER_OTHER_EXCEPTIONAL_CIRCUMSTANCES = "OTHER_EXCEPTIONAL_CIRCUMSTANCES";
	public static final String OTHER_OTHER_MISREPRESENTATION = "OTHER_MISREPRESENTATION";
	public static final String OTHER_OTHER_ENROLLMENTERROR = "OTHER_ENROLLMENTERROR";
	public static final String OTHER_OTHER_CANCEL_QHP = "OTHER_CANCEL_QHP";
	public static final String OTHER_OTHER_CHANGE_COVERAGE = "OTHER_CHANGE_COVERAGE";
	public static final String OTHER_ENROLLMENT_BY_MARKETPLACE_OR_CONSUMER_CONNECTOR_AFTER_DAY_FREE_LOOK_PERIOD = "OTHER_ENROLLMENT_BY_MARKETPLACE_OR_CONSUMER_CONNECTOR_AFTER_DAY_FREE_LOOK_PERIOD";
	public static final String OTHER_OTHER_CONTRACT = "OTHER_CONTRACT";
	public static final String OTHER_OTHER_DOMESTIC_ABUSE = "OTHER_DOMESTIC_ABUSE";
	public static final String OTHER_OTHER_SYSTEM_ERRORS = "OTHER_SYSTEM_ERRORS";
	public static final String OTHER_ELIG_EXEMPTION = "OTHER_ELIG_EXEMPTION";
	
	public static final String INCOME = "INCOME";
	public static final String CHANGE_IN_NAME = "CHANGE_IN_NAME";

	public static final String SEP = "SEP";
	public static final String APPLICATION_GUID = "applicationGuid";
	public static final String APPLICANT_GUID = "applicantGuid";
	public static final String SSAP_APPLICATION_ID = "ssapApplicationId";
	public static final String EVENT_INFO = "eventInfo";
	public static final String REVIEWED_AND_SIGNED = "REVIEWED_AND_SIGNED";
	public static final String ESIGN_DATE = "ESIGN_DATE";
	public static final String ESIGN_FIRST_NAME = "ESIGN_FIRST_NAME";
	public static final String ESIGN_MIDDLE_NAME = "ESIGN_MIDDLE_NAME";
	public static final String ESIGN_LAST_NAME = "ESIGN_LAST_NAME";
	public static final String COVERAGE_YEAR = "coverageYear";
	public static final String SIGNED = "SIGNED";
	public static final String ADD_TAX_DEPENDENT_REASON = "ADD_TAX_DEPENDENT_REASON";
	
	public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	public static final String HOUSEHOLD_MEMBER= "householdMember";
	public static final String TAX_HOUSEHOLD= "taxHousehold";
	public static final String APPLYING_FOR_COVERAGE_INDICATOR= "applyingForCoverageIndicator";
	public static final String HOUSEHOLD_CONTACT_INDICATOR = "householdContactIndicator";
	public static final String TOBACCO_USER_INDICATOR = "tobaccoUserIndicator";
	public static final String MARRIED_INDICATOR = "marriedIndicator";
	public static final String MARRIED_INDICATOR_CODE = "marriedIndicatorCode";
	public static final String GENDER = "gender";
	public static final String NAME = "name";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String MIDDLE_NAME = "middleName";
	public static final String SUFFIX = "suffix";
	public static final String PERSON_ID = "personId";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String BLOOD_RELATIONSHIP = "bloodRelationship";
	public static final String RELATIONSHIP_CODE = "relationshipCode";
	public static final String RELATED_PERSON_ID = "relatedPersonId";
	public static final String INDIVIDUAL_PERSON_ID = "individualPersonId";
	public static final String RELATION = "relation";
	public static final String SPOUSE_RELATIONSHIP_CODE = "01";
	public static final String INCARCERATION_STATUS = "incarcerationStatus";
	public static final String INCARCERATION_STATUS_INDICATOR = "incarcerationStatusIndicator";
	public static final String ACTIVE = "active";
	public static final String SEEKING_COVERAGE = "applyingForCoverageIndicator";
	public static final String DEATH_INDICATOR = "deathIndicator";
	public static final String HOUSEHOLD_CONTACT = "householdContact";
	public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
	public static final String SOCIAL_SECURITY_CARD_HOLDER_INDICATOR = "socialSecurityCardHolderIndicator";
	public static final String SPECIAL_CIRCUMSTANCES = "specialCircumstances";
	public static final String AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR = "americanIndianAlaskaNativeIndicator";
	public static final String OTHER_PHONE = "otherPhone";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String PHONE_EXTENSTION = "phoneExtension";
	public static final String PHONE = "phone";
	public static final String CONTACT_PREFERENCES = "contactPreferences";
	public static final String EMAIL_ADDRESS = "emailAddress";
	public static final String HOME_ADDRESS = "homeAddress";
	public static final String MAILING_ADDRESS = "mailingAddress";
	public static final String MAILING_ADDRESS_SAME_AS_HOME_ADDRESS_INDICATOR ="mailingAddressSameAsHomeAddressIndicator";
	public static final String STREET_ADDRESS_1 = "streetAddress1";
	public static final String STREET_ADDRESS_2 = "streetAddress2";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String POSTAL_CODE = "postalCode";
	public static final String COUNTY = "county";
	public static final String PRIMARY_ADDRESS_COUNTY_FIPS_CODE = "primaryAddressCountyFipsCode";
	public static final String LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR = "livesWithHouseholdContactIndicator";
	public static final String OTHER_ADDRESS = "otherAddress";
	public static final String ADDRESS = "address";
	public static final String CATEGORY = "category";
	public static final String SEP_EVENTS = "sepEventss";
	public static final String NAME_SAME_ON_SSN_INDICATOR = "nameSameOnSSNCardIndicator";
	public static final String FIRST_NAME_ON_SSN_CARD = "firstNameOnSSNCard";
	public static final String MIDDLE_NAME_ON_SSN_CARD = "middleNameOnSSNCard";
	public static final String LAST_NAME_ON_SSN_CARD = "lastNameOnSSNCard";
	public static final String SUFFIX_ON_SSN_CARD = "suffixOnSSNCard";
	public static final String REASONABLE_EXPLANATION_FOR_NO_SSN = "reasonableExplanationForNoSSN";
	public static final String INDIVIDUAL_MANDATE_EXCEPTION_INDICATOR = "individualMandateExceptionIndicator";
	public static final String INDIVIDUAL_MANDATE_EXCEPTION_ID = "individualMandateExceptionID";
	
	
	public static final String SSAP_JSON = "ssapJson";
	public static final String IS_NEW = "isNew";
	public static final String CONTENT_TYPE = "application/json";
	
	public static final String LCE_DEMO_CHANGE_NOTIFICATION_TEMPLATE_NAME = "lceDemographicChangesNotification";
	public static final String LCE_DEMO_CHANGE_ECM_RELATIVE_PATH = "lcedemographicchangesnotification";
	public static final String LCE_DEMO_CHANGE_NOTIFICATION_FILE_NAME = "lceDemographicChangesNotification_";
	
	public static final String LCE_DOB_CHANGE_NOTIFICATION_TEMPLATE_NAME = "lceDobChangeNotification";
	public static final String LCE_DOB_CHANGE_ECM_RELATIVE_PATH = "lcedobchangenotification";
	public static final String LCE_DOB_CHANGE_NOTIFICATION_FILE_NAME = "lceDobChangeNotification_";
	
	public static final String LCE_DEMOGRAPHICS_ACTION_REQUIRED_NOTIFICATION_ECM_RELATIVE_PATH = "lceEE030NFDemographicChangesNotification";
	public static final String LCE_DEMOGRAPHICS_ACTION_REQUIRED_NOTIFICATION_FILE_NAME = "lceEE030NFDemographicChangesNotification_";
	
	public static final String LCE_SEP_NOTIFICATION_TEMPLATE_NAME = "lceSepNotification";
	public static final String LCE_SEP_ECM_RELATIVE_PATH = "lcesepnotification";
	public static final String LCE_SEP_NOTIFICATION_FILE_NAME = "lceSepNotification_";
	
	public static final String LCE_DISENROLL_NOTIFICATION_TEMPLATE_NAME = "lceDisenrollNotification";
	public static final String LCE_DISENROLL_ECM_RELATIVE_PATH = "lcedisenrollnotification";
	public static final String LCE_DISENROLL_NOTIFICATION_FILE_NAME = "lceDisenrollNotification_";
	
	public static final String LCE_AMERICAN_STATUS_CHANGE_NOTIFICATION_TEMPLATE_NAME = "lceAmericanStatusChangeNotification";
	public static final String LCE_AMERICAN_STATUS_CHANGE_ECM_RELATIVE_PATH = "lceamericanstatuschangenotification";
	public static final String LCE_AMERICAN_STATUS_CHANGE_NOTIFICATION_FILE_NAME = "lceAmericanStatusChangeNotification_";
	
	public static final String LCE_SEP_DENIAL_NOTIFICATION_TEMPLATE_NAME = "lceSepDenialNotification";
	public static final String LCE_SEP_DENIAL_ECM_RELATIVE_PATH = "lceSepDenialNotification";
	public static final String LCE_SEP_DENIAL_NOTIFICATION_FILE_NAME = "lceSepDenialNotification_";
	
	public static final String PDF = ".pdf";
	public static final String INDIVIDUAL = "individual";
	public static final String NOTIFICATION_SEND_DATE = "notificationDate";
	public static final String GAIN_IN_TRIBE_STATUS = "GAIN_IN_TRIBE_STATUS";

	public static final String CHANGE_TYPE_OTHER_CONCAT_STRING = "_OTHER";
	public static final String CHANGE_TYPE_REMOVE = "REMOVE";
	public static final String CHANGE_TYPE_ADD = "ADD";
	public static final String SEP_DENIED = "sepDenied";
	public static final String Y = "Y";
	
}
