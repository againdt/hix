<%@ include file="../include.jsp" %>
<div id="appscr81C" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page31C.chpSpecificQuestions" text="CHP+ Specific Questions" />
			</h4>
		</div>
		<form id="appscr81CForm" class="form-horizontal">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page31C.did" text="Did" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31C.haveHealthInsuranceFrom" text="have health insurance from a job that ended in the last [number of months of waiting period]" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="didHaveAHealthInsuranceFromLastJob" value="yes" id="didHaveAHealthInsuranceFromLastJobYes" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" name="didHaveAHealthInsuranceFromLastJob" value="no" id="didHaveAHealthInsuranceFromLastJobNo"><spring:message code="ssap.no" text="No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page31C.whyDidThat" text="Why did that insurance end" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="PARENT_NO_LONGER_EMPLOYED" id="P81CParentNoLongerEmployed" /><spring:message code="ssap.page31C.theParentNoLonger" text="The parent no longer works for the employer that offered the insurance" /> <br>
					</label>
					
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="EMPLOYER_STOPPED_COVERAGE" id="P81CEmployerStoppedCoverage" /><spring:message code="ssap.page31C.theEmployerStopped" text="The employer stopped offering insurance" />  <br>
					</label>
					
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="EMPLOYER_STOPPED_DEPENDANT_COVERAGE" id="P81CEmployerStoppedDependant" /><spring:message code="ssap.page31C.theEmployerStoppedOfferings" text="The employer stopped offering insurance to dependents (kids)" />  <br>
					</label>
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="INSURANCE_TOO_EXPENSIVE" id="P81CInsuranceTooExpensive" /><spring:message code="ssap.page31C.theInsuranceWasTooExpensive" text="The insurance was too expensive" /><br>
					</label>
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="UNINSURED_MEDICAL_NEEDS" id="P81CUninsuredMedicalNeeds" /><span class="nameOfHouseHold"></span> <spring:message code="ssap.page31C.hadMedicalNeeds" text="had medical needs not covered by the other insurance" /><br>
					</label>
					<label>
						<input type="radio" name="whyDidThatInsuranceEnd" value="OTHER" id="P81COther" checked="checked" /><spring:message code="ssap.county.Other" text="Other" />
					</label>
					<textarea id="descriptionTextArea" name="" class="margin20-l" cols="" rows="" placeHolder="<spring:message code='ssap.page31C.explanatiopn' text='Explanation' />" ></textarea>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.is" text="Is" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31C.offeredTheColoradoState" text="offered the New Mexico state employee health benefit plan through a job, or a family member's job" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="isHealthPlanFromSelfJob" value="Yes" id="isHealthPlanFromSelfJobYes" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" name="isHealthPlanFromSelfJob" value="No" id="isHealthPlanFromSelfJobNo"><spring:message code="ssap.no" text="No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
		</form>
	</div>
</div>