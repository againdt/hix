import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';


// Constants

import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS, { getDynamicDropdownNumberOption, getOptionsForHardcodedState } from '../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';




/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Preganant Details ' Page
 *
 */
const Page2_24= props => {

    const {
        data,
        envVariables,
        form: {
            Form: {
                isFormSubmitted,
                isFormOnTopLevelValid
            }
        },
        householdMembers,
        isFirstVisit
    } = props;

    // Local State of Functional Component

    const [localState, setLocalState] = useState({
        noneOfTheAbove: isFirstVisit ? false : householdMembers.every(member => !member.specialCircumstances.everInFosterCareIndicator)
    });

    const { noneOfTheAbove } = localState;

    const onFieldChange_member = (newData, indexInArray) => {

        const newState = {...data };
        const { dateOfBirth } = householdMembers[indexInArray];
        const {
            stateCode
        } = envVariables;

        const {
            ageWhenLeftFosterCare,
            fosterCareState
        } = newData.specialCircumstances;
        const age = moment().diff(dateOfBirth, 'years');
        const ss = newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances;
        if (!newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances.everInFosterCareIndicator) {
            newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances = {
                ...ss,
                ...cloneObject(EMPTY_OBJECTS.FOSTER_CARE)
            };
        } else {
            newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
            if(ageWhenLeftFosterCare > 0 && fosterCareState) {
                if((age <= 25 && ageWhenLeftFosterCare === 18 && stateCode === fosterCareState) || (age <= 21 && ageWhenLeftFosterCare === 18 && stateCode!== fosterCareState)) {
                    newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances.metFosterCareConditions = true;
                } else {
                    newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances.metFosterCareConditions = false;
                }
            } else {
                newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances.metFosterCareConditions = false;
            }

        }
        props.actions.changeData(newState);
        if(newData.specialCircumstances.everInFosterCareIndicator) {
            setLocalState(state => ({
                ...state,
                noneOfTheAbove: false
            }));
        }
    };

    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <americanIndianAlaskaNative> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = newData => {

        setLocalState(state => ({
            ...state,
            ...newData
        }));

        const newState = {...data };

        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member.specialCircumstances = {
                        ...member.specialCircumstances,
                        ...cloneObject(EMPTY_OBJECTS.FOSTER_CARE)
                    };
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
    };

    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = (householdMembers) => {

        const combinedMembers = [];
        const {  noneOfTheAbove } = localState;
        householdMembers.forEach((member, i) => {
            const {
                name,
                specialCircumstances: {
                    fosterCareState,
                    everInFosterCareIndicator,
                    gettingHealthCareThroughStateMedicaidIndicator,
                    ageWhenLeftFosterCare
                },
                dateOfBirth,
                applyingForCoverageIndicator,
                gender
            } = member;
            const memberName = `${normalizeNameOfPerson(name)}`;
            const age = moment().diff(dateOfBirth, 'years');
            const [ stateAbbr ] = fosterCareState ? getOptionsForHardcodedState(fosterCareState) : [{}];

            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    {applyingForCoverageIndicator &&
                    <Fragment>
                        <ul className="usa-unstyled-list">
                            {
                                age >= 18 && age <= 25 &&
                                <li>
                                    <UniversalInput
                                        label={{
                                            text: `${memberName}`
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            name: 'everInFosterCareIndicator',
                                            classes: [''],
                                            checked: everInFosterCareIndicator,
                                            value: everInFosterCareIndicator,
                                            currentData: member,
                                            fields: ['specialCircumstances'],
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        indexInArray={i}
                                        errorClasses={['margin-l-0']}
                                    />
                                </li>
                            }
                        </ul>
                        {/*****************************************************************************************
                         Begin - Were any of these people ever in foster care? (YES)
                         *****************************************************************************************/}
                        {
                            everInFosterCareIndicator &&
                            <Fragment>
                                <div className="usa-width-one-whole margin-tb-20 gi-dropdown-layout--02">
                                    <fieldset>
                                        <legend>
                                            In what state was&nbsp;
                                            <span><strong>{memberName}</strong>&nbsp;in the foster care system?</span>
                                        </legend>
                                        <UniversalInput
                                            label={{
                                                text: 'State',
                                                isLabelRequired: true
                                            }}
                                            inputData={{
                                                isInputRequired: true,
                                                type: INPUT_TYPES.DROPDOWN,
                                                name: 'fosterCareState',
                                                value: fosterCareState,
                                                options: DROPDOWN_OPTIONS.STATE,
                                                validationType: VALIDATION_TYPES.CHOSEN,
                                                currentData: member,
                                                fields: ['specialCircumstances']
                                            }}
                                            errorClasses={['margin-b-0']}
                                            inputActions={{
                                                onFieldChange: onFieldChange_member
                                            }}
                                            indexInArray={i}
                                        />
                                    </fieldset>
                                    <UniversalInput
                                        legend={{
                                            legendText:
                                                <span>
                                                Was&nbsp;
                                                    <strong>
                                                        {normalizeNameOfPerson(name)}&nbsp;
                                                    </strong>
                                                getting health care through <strong>{stateAbbr.text}</strong> (Medicaid)?
                                                </span>
                                        }}
                                        label={{
                                            text: '',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-whole margin-b-20'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'gettingHealthCareThroughStateMedicaidIndicator',
                                            value: gettingHealthCareThroughStateMedicaidIndicator,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: member,
                                            fields: ['specialCircumstances']
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        indexInArray={i}
                                    />
                                    <fieldset>
                                        <legend>
                                            How old was&nbsp;
                                            <span><strong>{memberName}</strong>&nbsp;when&nbsp;
                                                {gender === 'female' ? 'she' : 'he'}&nbsp;
                                                left the foster care system?</span>
                                        </legend>
                                        <UniversalInput
                                            label={{
                                                text: 'Age  ',
                                                isLabelRequired: true
                                            }}
                                            inputData={{
                                                isInputRequired: true,
                                                type: INPUT_TYPES.DROPDOWN,
                                                name: 'ageWhenLeftFosterCare',
                                                value: ageWhenLeftFosterCare,
                                                options: getDynamicDropdownNumberOption(age, 21),
                                                validationType: VALIDATION_TYPES.CHOSEN,
                                                currentData: member,
                                                handleAsNumbers: true,
                                                fields: ['specialCircumstances']
                                            }}
                                            errorClasses={['margin-b-0']}
                                            inputActions={{
                                                onFieldChange: onFieldChange_member
                                            }}
                                            indexInArray={i}
                                        />
                                    </fieldset>
                                </div>
                            </Fragment>
                        }
                        {/*****************************************************************************************
                         End - Were any of these people ever in foster care? (YES)
                         *****************************************************************************************/}
                    </Fragment>
                    }

                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    const checkTopLevelValidation = () => {
        return noneOfTheAbove || !!householdMembers.find(member => member.specialCircumstances.everInFosterCareIndicator);
    };

    return (
        <Fragment>
            <div id="fhMoreInfo" className="fade-in">
                <div className="subsection">
                    <Form topLevelValidity={checkTopLevelValidation()}>
                        <p className="required-true">
                            Were any of these people ever in foster care?
                        </p>
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isFormOnTopLevelValid}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        {
                            getHouseholdMembers(householdMembers)
                        }
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,
    envVariables: state.headerLinks,
    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_24.defaultProps = {
    name: 'Page2_24'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_24);
