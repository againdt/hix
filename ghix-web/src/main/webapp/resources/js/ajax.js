function setCookie(name, value){
	document.cookie = name+"="+value
}
	
function delete_cookie(cookie_name){
  var cookie_date = new Date ( );  // current date & time
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

function getCookieByName (name) {
	var nvPairs = document.cookie.split ("; ");
	var cookieval;
	for (var i = 0; i < nvPairs.length; i = i + 1) {
		var seped = nvPairs [i].split ("=");
		if (seped [0] == name) {
			this.name = seped [0];
			cookieval = seped [1];
		}
	}
	return cookieval;
}

function cookieExist(name) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) && ( name != document.cookie.substring( 0, name.length ) ) ){
		return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}
 
 function compareplans(PlanType){  	
  	var tempPlanid = document.cookie;
	
  	if(PlanType =='cb_short_plan_'){  		
  		if(document.getElementById('short_plan_id_' + getCookieByName('shortplanid'))) {
  			var planIds = document.getElementById('short_plan_id_' + getCookieByName('shortplanid')).value;
  		} else {
  			var planIds = document.applySelectedForm.shortPlanIds.value ;	 
  		}
  	}else{	
  		if(document.getElementById('major_plan_id_' + getCookieByName('majorplanid'))) {
  			var planIds = document.getElementById('major_plan_id_' + getCookieByName('majorplanid')).value;
  		} else {
  			var planIds = document.applySelectedForm.majorPlanIds.value ;
  		}
  	}
  	var checkbox_choices = 0;
	appendString ="";
	
  	var planIdsArray = planIds.split(",");   	
  	for (var x = 0; x < planIdsArray.length; x++){
  		//var PlanCheckBox = "cb_major_plan_" + planIdsArray[x] ;
  		var PlanCheckBox = PlanType + planIdsArray[x] ;  		
		if(document.getElementById(PlanCheckBox)) {  
		  	if(document.getElementById(PlanCheckBox).checked){
		  		checkbox_choices = checkbox_choices + 1;
		  		appendString = appendString + document.getElementById(PlanCheckBox).value + ",";
		  	}
		}
  	}
  	if(checkbox_choices == 0){
  		alert("Please select atleast 1 plan to compare.");
  		return false ;
  	}	
  	appendString = appendString.substr(0, appendString.length - 1);
  	var locationurl = "/brokercenter/plan-detail.php?plan_ids="+appendString;
 	window.open(locationurl, 'ComaparePlans', 'width=1200px, height=800px, scrollbars=yes, resizable=yes');
 }
 
function isValid(parm,val) {
		if (parm == "") return true;
		for (i=0; i<parm.length; i++) {
		if (val.indexOf(parm.charAt(i),0) == -1) return false;
		}
		return true;
}
 
function isNum(parm) {
 	var numb = '0123456789';
 	return isValid(parm,numb);
}

function formatNumber(num,dec,thou,pnt,curr1,curr2,n1,n2) {
	var x = Math.round(num * Math.pow(10,dec));
	if (x >= 0) n1=n2='';
	var y = (''+Math.abs(x)).split('');
	var z = y.length - dec; 
	if (z<0) z--; 
	for(var i = z; i < 0; i++) 
		y.unshift('0');
	y.splice(z, 0, pnt); 
	if(y[0] == pnt) y.unshift('0'); 
	while (z > 3) {
		z-=3; 
		y.splice(z,0,thou);
	}
	var r = curr1+n1+y.join('')+n2+curr2;
	return r;
}

function getTotalPremium(e,planId,premium){
  	
  	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	
    if(keycode==13){
	  	var rate_up_field="";
	  	var rate_up_value = 0;
	  	var premium ;
	  	var total_premium ;
	  	var total_saving_field;
	  	var total_saving;
	  	var Current_Monthly_Premium;
	  	
	  	rate_up_field= "rt_" + planId;
	  	total_premium_field = "tp_" + planId;  	
	  	
	  	rate_up_value = document.getElementById(rate_up_field).value;
	  	if(!isNum(rate_up_value)){alert("Please Enter Numeric Value."); return false;}
	  	total_premium = ((premium*1) + (1*(premium * (rate_up_value/100))));	  	
	  	document.getElementById(total_premium_field).innerHTML= "<b>$" + total_premium.toFixed(2) + "</b><BR /><span>monthly</span>";  	
	  	total_saving_field = "ts_" + planId;
	  	if(document.getElementById('CurrentMonthlyPremium') && document.getElementById(total_saving_field)) {
		  	Current_Monthly_Premium = document.getElementById('CurrentMonthlyPremium').value;
		  	Current_Monthly_Premium = Current_Monthly_Premium.toString().replace(",", "") + 0;
		  	total_saving = (((Current_Monthly_Premium*1) - total_premium) * 12);
		  	if(total_saving < 0) {
		  		total_saving = formatNumber((((Current_Monthly_Premium*1) - total_premium) * 12),2,',','.','','','-','');
		  		total_saving = total_saving.toString().replace("-", "");
		  		document.getElementById(total_saving_field).innerHTML= "(<font color=Red>$<strong>" + total_saving + "</strong></font>)";
		  	} else {
		  		total_saving = formatNumber((((Current_Monthly_Premium*1) - total_premium) * 12),2,',','.','','','-','');
		  		document.getElementById(total_saving_field).innerHTML= "<font color=Green>$<strong>" + total_saving + "</strong></font>";
		  	}	
		}
  	}
  }
 
  
var rowIdInGetquote;
	  
function showResponse(request){
	benefitSuccFlag();
	var response = request.responseText;
	response = response.replace(/[\r\n\t\s]+/, '');
}

function initPlanBenefits(insurancetype, leadid, key_id, cr_id, rowid, insured, premium, plan_id) {
	 var totalPremium = 0;
	 var prevPremium;
	 var prevPremiumId;
	 if(insurancetype == "Health") {
		chkboxId = "cb_major_plan_" + plan_id;
		prevPremiumId = "premium_major_plan_" + plan_id;
	} else if(insurancetype == "ShortTerm") {
		chkboxId = "cb_short_plan_" + plan_id;
		prevPremiumId = "premium_short_plan_" + plan_id;
	} else if(insurancetype == "Life") {
		if(insured == "Applicant") {
			if(premium == "NONROP") {
				chkboxId = "life_applicant_nonrop_" + plan_id;
				prevPremiumId = chkboxId  + "_premium";
			} else {
				chkboxId = "life_applicant_rop_" + plan_id;
				prevPremiumId = chkboxId  + "_premium";
			}
		} else if(insured == "Spouse") {
			if(premium == "NONROP") {
				chkboxId = "life_spouse_nonrop_" + plan_id;
				prevPremiumId = chkboxId  + "_premium";
			} else {
				chkboxId = "life_spouse_rop_" + plan_id;
				prevPremiumId = chkboxId  + "_premium";
			}
		}
	}
		
	if(prevPremiumId != "" && $(prevPremiumId)!= null) {		
		var prevPremium = ($(chkboxId).checked) ?  eval($(prevPremiumId)).value : 0;							
		if(insurancetype == "Health" && $(chkboxId).checked == true) {
			var grlifecheckbox = 'GR_Life_' + plan_id;
			var grdentalcheckbox = 'GR_Dental_' + plan_id;
			var grcarecheckbox = 'GR_Care_' + plan_id;
			var grrxcheckbox = 'GR_Rx_' + plan_id;			
			var amecheckbox = 'discount_product_' + plan_id;
			var lifecheckbox = 'life_applicant_nonrop_' + plan_id;			
			var dentalcheckbox = 'Addon_Dental_' + plan_id;
			
			/*if($(grlifecheckbox) != null && $(grlifecheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(grlifecheckbox).value); 								
			if($(grdentalcheckbox) != null && $(grdentalcheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(grdentalcheckbox).value); 								
			if($(grcarecheckbox) != null && $(grcarecheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(grcarecheckbox).value); 								
			if($(grrxcheckbox) != null && $(grrxcheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(grrxcheckbox).value); 
			*/											
			if($(amecheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(amecheckbox+ '_premium').value); 								
			if($(lifecheckbox) != null && $(lifecheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(lifecheckbox+ '_premium').value);
			if($(dentalcheckbox) != null && $(dentalcheckbox).checked == true)
				prevPremium = eval(prevPremium) + eval($(dentalcheckbox).value);	  					
			
		}	 
		
		var total = eval($('totalpremiumamt').innerHTML).toFixed(2);						
		totalPremium = eval(total) - eval(prevPremium); 
	    displayTotalPremium(totalPremium.toFixed(2));  				  

	}
	rowIdInGetquote = rowid;	
	var parmeterVal = 'request=init&instype='+ insurancetype + '&lead_id=' + leadid + '&keyid=' + key_id + '&crid=' + cr_id + '&rowid=' + rowid+ '&insured=' + insured + '&premium=' + premium ;
	new Ajax.Request('/brokercenter/getPrevNextPlan.php', 
		{ 
		method: 'GET',
		parameters: parmeterVal,  				
		onComplete: showResponse  				
		}
	);
}
  
function benefitSuccFlag(){
	new Ajax.Updater(rowIdInGetquote, '/brokercenter/getPrevNextPlan.php', {method: 'GET',parameters: 'request=fetch'} );
}

function chkUnchkLifeMembers(chkBox) {
	var chkboxIdArr;
	var chkboxId;	
	chkboxId = chkBox.id;
	chkboxIdArr = chkboxId.split("_");
	if(chkboxIdArr[2] == "child") {
		if(document.getElementById(chkboxId).checked){
			if(!document.getElementById("life_medical_applicant").checked){
				document.getElementById("life_medical_applicant").checked = true;
			} 
		} 
	} else {
		if(!document.getElementById("life_medical_applicant").checked){
			chkboxIdArr = document.getElementById("childMemberStr").value.split(",");
			for(var i=0; i < chkboxIdArr.length; i++) {
				if(document.getElementById(chkboxIdArr[i]))
					document.getElementById(chkboxIdArr[i]).checked = false;
			}
		}
	}	
}

function checkData(element,alertMsg){	
	 if(document.getElementById(element).value ==''){
		alert(alertMsg);  				
  		document.getElementById(element).focus();
  		return false;
  	}
  	else
  	 return true;
}

function checkNumericData(element,alertMsg){
	
	var numberformat = /^[0-9\.]+$/;
	if(!document.getElementById(element).value.match(numberformat)){
		alert(alertMsg);  				
  		document.getElementById(element).focus();
  		return false;
  	}
  	else
  	 return true;
}
	
function ApplyValidate(validation, mmId, ameId, lifeId, premiumId, typeInId){  	
  	 
  	 deleteCookie();
  	   	
  	/* =================== when user click Apply button ===================== */
  	  	
  	if(validation == false){  	 	  		  	

  		if(typeInId != null) {  			
  			if(!checkData("cb_Health_" + typeInId + "_PlanName", "Enter plan name")) return false;
	  		if(!checkData("cb_Health_" + typeInId + "_Deductible", "Enter deductible amount")) return false;
	  		if(!checkNumericData("cb_Health_" + typeInId + "_Deductible", "Enter numeric value for deductible amount")) return false;
			if(!checkData("cb_Health_" + typeInId + "_CoInsurance", "Enter Co-Insurance value")) return false;			
			if(!checkData("cb_Health_" + typeInId + "_BasePremium", "Enter premium amount")) return false;
			if(!checkNumericData("cb_Health_" + typeInId + "_BasePremium", "Enter numeric value for premium")) return false; 		 	 							  	   			  	  				
	  	}
	  		
	  	/* ================= uncheck radio buttons for Major Medical plans start here ==================== */
	  		  	
	  	var tempAMEId = '';
	  	var tempLifeId = '';
	  	if(ameId != '' && $(ameId) != null)  tempAMEId = ($(ameId).checked) ? ameId : '';
	  	if(lifeId != '' && $(lifeId) != null) tempLifeId = ($(lifeId).checked) ? lifeId : '';
	  	  	
	  		  		  		  	
	  	if($('major_plan_id_' + getCookieByName('majorplanid'))) 
	  		var planIds = $('major_plan_id_' + getCookieByName('majorplanid')).value;
	  	else
	  		var planIds = ($('majorPlanIds')) ? $('majorPlanIds').value : "";			
	  	
	  	if(planIds !=""){			
			var planIdsArray = planIds.split(",");   	
		  	for (var x = 0; x < planIdsArray.length; x++){
		  		var PlanCheckBox = "cb_major_plan_" + planIdsArray[x];
		  		var AMEPlanCheckBox = "discount_product_" + planIdsArray[x];
		  		var LifePlanCheckBox = "life_applicant_nonrop_" + planIdsArray[x];		  		

				if($(PlanCheckBox) != null && $(PlanCheckBox).checked)   				  	
				  	$(PlanCheckBox).checked = false;	
  				if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked)
					$(AMEPlanCheckBox).checked = false;	  			
				if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked)   				  	
				  	$(LifePlanCheckBox).checked = false;
				  	
				if($(mmId).value != planIdsArray[x]){ 	
					var DentalPlanCheckBox = "Addon_Dental_" + planIdsArray[x]; 
			  		var GR_LifePlanCheckBox = "GR_Life_" + planIdsArray[x];
			  		var GR_DentalPlanCheckBox = "GR_Dental_" + planIdsArray[x];
			  		var GR_CarePlanCheckBox = "GR_Care_" + planIdsArray[x];
		  			var GR_RxPlanCheckBox = "GR_Rx_" + planIdsArray[x];
		  			
					if($(DentalPlanCheckBox) != null && $(DentalPlanCheckBox).checked)   				  	
						$(DentalPlanCheckBox).checked = false;  	
					if($(GR_LifePlanCheckBox) != null && $(GR_LifePlanCheckBox).checked)   				  	
					  	$(GR_LifePlanCheckBox).checked = false;
					if($(GR_DentalPlanCheckBox) != null && $(GR_DentalPlanCheckBox).checked)   				  	
				  		$(GR_DentalPlanCheckBox).checked = false;
					if($(GR_CarePlanCheckBox) != null && $(GR_CarePlanCheckBox).checked)   				  	
					  	$(GR_CarePlanCheckBox).checked = false;					  	
					if($(GR_RxPlanCheckBox) != null && $(GR_RxPlanCheckBox).checked)   				  	
					  	$(GR_RxPlanCheckBox).checked = false;  	  	  	  									  	
				}	  	
		  	}
	  	}	
	  	  	
	  	if($('counter')) {
			var counter = $('counter').value;			
		  	for(var ctr=0; ctr<counter; ctr++){
		  	  if($("cb_Health_"+ctr) != null && $("cb_Health_"+ctr).checked)
				 $("cb_Health_"+ctr).checked = false;					  		
		  	}
	  	}
	  		  	
	  	/* ================= uncheck radio buttons for Major Medical plans end here ==================== */
	  	
	  	/* ================= uncheck radio buttons for AME & Standalone plans start here ==================== */
	  		  	
	   	var discountplanIds = ($('discountPlanIds')) ? $('discountPlanIds').value : "";
			  	
	  	if(discountplanIds !=""){		
		var discountplanIdsArray = discountplanIds.split(",");   	
	  	for (var x = 0; x < discountplanIdsArray.length; x++){
	  		var LifePlanCheckBox = "life_applicant_nonrop_" + discountplanIdsArray[x];
	  		var AMEPlanCheckBox = "discount_product_companion_" + discountplanIdsArray[x];
			var discountPlanStanaloneCheckBox = "discount_product_standalone_" + discountplanIdsArray[x] ;
			var discountPlanStanaloneCompanionCheckBox = "discount_product_standalone_companion_" + discountplanIdsArray[x] ;
			
			if($(discountPlanStanaloneCheckBox) != null && $(discountPlanStanaloneCheckBox).checked)
				$(discountPlanStanaloneCheckBox).checked = false;				
			if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked)   				  	
				$(LifePlanCheckBox).checked = false;		
			if($(discountPlanStanaloneCompanionCheckBox) != null && $(discountPlanStanaloneCompanionCheckBox).checked)
				$(discountPlanStanaloneCompanionCheckBox).checked = false;	  		
			if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked)
				$(AMEPlanCheckBox).checked = false;	  			
		  }
	  	}
	  		  
	   /* ================= uncheck radio buttons for AME & Standalone plans end here ==================== */
	   
	   /* ================= uncheck radio buttons for Short Trem plans start here ==================== */
	  
	   if($('short_plan_id_' + getCookieByName('shortplanid'))) 
	  		var shortPlanIds = $('short_plan_id_' + getCookieByName('shortplanid')).value;
	   else 
	  		var shortPlanIds = ($('shortPlanIds')) ? $('shortPlanIds').value : "";
	  	
	  	if(shortPlanIds !=""){		
		var planIdsArray = shortPlanIds.split(",");   	
	  		for (var x = 0; x < planIdsArray.length; x++){
	  			var PlanCheckBox = "cb_short_plan_" + planIdsArray[x] ;
				if($(PlanCheckBox) != null && $(PlanCheckBox).checked)  			  	
			  		$(PlanCheckBox).checked = false;			  	
			}	  	
	  	}	  		  	
		if($('counter')){
			for(var ctr=0; ctr<counter; ctr++){
				if($("cb_ShortTerm_"+ctr) != null && $("cb_ShortTerm_"+ctr).checked)
		  			$("cb_ShortTerm_"+ctr).checked = false;
		  	}
		}	
	  	
	  	/* ================= uncheck radio buttons for Short Trem plans end here ==================== */
	  	
	  	/* ================= uncheck radio buttons for Standalone Life plans start here ==================== */
	  	
	  	for(var i=0; i<=1; i++) {
	  		if($('life_applicant_nonrop_'+i) != null && $('life_applicant_nonrop_'+i).checked) 
	  		$('life_applicant_nonrop_'+i).checked = false;
	  		
	  		if($('life_spouse_nonrop_'+i) != null && $('life_spouse_nonrop_'+i).checked) 
	  		$('life_spouse_nonrop_'+i).checked = false;	  	
	  	}
	  	
	  	 /* uncheck type-in checkbox */
	  	 	  	 
	  	if($('life_applicant_typein_checkbox') != null && $('life_applicant_typein_checkbox').checked) 
	  		$('life_applicant_typein_checkbox').checked = false;
	  		 
	  	if($('life_spouse_typein_checkbox') != null && $('life_spouse_typein_checkbox').checked) 
	  		$('life_spouse_typein_checkbox').checked = false;
	  	
	  	/* ================= uncheck radio buttons for Standalone Life plans end here ==================== */
	  		  	 		  		 		
  		$(mmId).checked = true;
  		var totalPremium = eval($(premiumId).value);
  		if(tempAMEId != ''){
  			$(ameId).checked = true;
  			totalPremium = eval(totalPremium) + eval($(ameId + '_premium').value); 
  		}
  		if(tempLifeId != '' && $(lifeId) != null){	
	  		$(lifeId).checked = true;
	  		totalPremium = eval(totalPremium) + eval($(lifeId + '_premium').value); 
  		}	  		
  		
 		if($('assurantplanid') != null && $($('assurantplanid').value).checked == true){  			
			if($('hpaplanid') != null && $($('hpaplanid').value).checked == false){
				var confirmans = confirm("Before you apply for Assurant Major Medical plan for this user, please choose an HPA short term medical plan as well. Add the Short Term Plan now? Click OK to proceed without adding STM else click Cancel and add STM");
				if(confirmans == false) return false;
			}  				
  		}
  		
  		displayTotalPremium(totalPremium.toFixed(2));  		
	  	var submitpage = ($('policy_exists').value == 'Yes') ? 'policy_present.php' :  'submitquotes.php';
	  		  	  		
  		document.applySelectedForm.target="_self";
	  	document.applySelectedForm.action = submitpage;
	  	document.applySelectedForm.agent_action.value='apply';
	  	document.applySelectedForm.gold_platinum.value='y'; 
	 	document.applySelectedForm.submit();
	  	
  	}	
  	else{  	
	  	var tempPlanid = document.cookie;
	  	var checkbox_choices = 0;
	  	var short_checkbox_choices = 0;
	  	var ame_checkbox_choices = 0;
	  	var discount_standalone_checkbox_choices = 0;
	  	var discount_standalone_companion_checkbox_choices = 0;	  	
	  	var life_checkbox_choices = 0;
	  	var ame_without_mm_choices = 0;
	  	var life_applicant_typein_checkbox_choices = 0;
	  	var life_spouse_checkbox_choices = 0;
	  	var life_spouse_typein_checkbox_choices = 0;
		
		/* ================= counting checked radio buttons for Major Medical plans start here ==================== */
			
	  	if($('major_plan_id_' + getCookieByName('majorplanid'))) 
	  		var planIds = $('major_plan_id_' + getCookieByName('majorplanid')).value;
	  	else
	  		var planIds = ($('majorPlanIds')) ? $('majorPlanIds').value : "";			
		
	  	if(planIds !=""){
			appendString ="";
			var planIdsArray = planIds.split(",");   	
		  	for (var x = 0; x < planIdsArray.length; x++){
		  		var PlanCheckBox = "cb_major_plan_" + planIdsArray[x] ;
		  		var AMEPlanCheckBox = "discount_product_" + planIdsArray[x];
		  		var LifePlanCheckBox = "life_applicant_nonrop_" + planIdsArray[x];
		  		
				if($(PlanCheckBox) != null && $(PlanCheckBox).checked == true) {  
				 	checkbox_choices = checkbox_choices + 1;
				 	appendString = appendString + $(PlanCheckBox).value + ",";
				}
				if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked == true){
					ame_checkbox_choices = ame_checkbox_choices + 1;					
					if($(PlanCheckBox).checked == false)
						ame_without_mm_choices = ame_without_mm_choices + 1;
				}
				if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked) 
					life_checkbox_choices = life_checkbox_choices + 1;
		  	}
	  	}
	  	
	  		  	
	  	if($('counter')) {
			var counter = $('counter').value;
		  	for(var ctr=0; ctr<counter; ctr++){
		  	  if($("cb_Health_" + ctr) != null && $("cb_Health_" + ctr).checked == true){
					if(!checkData("cb_Health_" + ctr + "_PlanName", "Enter plan name")) return false;
		  			if(!checkData("cb_Health_" + ctr + "_Deductible", "Enter deductible amount")) return false;
		  			if(!checkNumericData("cb_Health_" + ctr + "_Deductible", "Enter numeric value for deductible amount")) return false;
					if(!checkData("cb_Health_" + ctr + "_CoInsurance", "Enter Co-Insurance value")) return false;			
					if(!checkData("cb_Health_" + ctr + "_BasePremium", "Enter premium amount")) return false;
					if(!checkNumericData("cb_Health_" + ctr + "_BasePremium", "Enter numeric value for premium")) return false; 		 	 
					
					checkbox_choices = eval(checkbox_choices) + 1;  		
		  	    }		  	    
		  	    		  	    
		  	   	if($('discount_product_cb_Health_' + ctr) != null && $('discount_product_cb_Health_'+ctr).checked == true){
		  	     	ame_checkbox_choices = ame_checkbox_choices + 1;
		  	     	if($("cb_Health_" + ctr).checked == false)
						ame_without_mm_choices = ame_without_mm_choices + 1;
		  	   	}	
   		  	   	if($('life_applicant_nonrop_cb_Health_' + ctr) != null && $('life_applicant_nonrop_cb_Health_' + ctr).checked == true) 	
			  	    life_checkbox_choices = life_checkbox_choices + 1;		  	    	
		  	}
	  	}
	  	
	  	if(checkbox_choices >1){
	  		alert("You can apply only for one Major Medical plan.");
	  		return false ;
	  	}
	  	
		/* ================= counting checked radio buttons for Major Medical plans end here ==================== */
		
		/* ================= counting checked radio buttons for AME & Standalone plans start here ==================== */
	  	
	  	var discountplanIds = ($('discountPlanIds')) ? $('discountPlanIds').value : "";
	  	
	  	if(discountplanIds !=""){
		appendString ="";
		var discountplanIdsArray = discountplanIds.split(",");   	
	  	for (var x = 0; x < discountplanIdsArray.length; x++){
	  		var AMEPlanCheckBox = "discount_product_companion_" + discountplanIdsArray[x] ;			
			var discountPlanStanaloneCheckBox = "discount_product_standalone_" + discountplanIdsArray[x] ;
			var discountPlanStanaloneCompanionCheckBox = "discount_product_standalone_companion_" + discountplanIdsArray[x] ;
			var LifePlanCheckBox = "life_applicant_nonrop_" + discountplanIdsArray[x];			
			if($(discountPlanStanaloneCheckBox) != null && $(discountPlanStanaloneCheckBox).checked == true) {  
			 	discount_standalone_checkbox_choices = discount_standalone_checkbox_choices + 1;
			 	appendString = appendString + $(discountPlanStanaloneCheckBox).value + ",";			  	
			}
			if($(discountPlanStanaloneCompanionCheckBox) != null && $(discountPlanStanaloneCompanionCheckBox).checked)
				discount_standalone_companion_checkbox_choices = discount_standalone_companion_checkbox_choices +1;	
			if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked)   				  	
				life_checkbox_choices = life_checkbox_choices + 1;				
			if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked)  	
				ame_checkbox_choices = ame_checkbox_choices + 1;	
	  	}
	  	}	  	
	  		  	
	  	if(ame_checkbox_choices >1){
	  		alert("You can apply only for one AME Add-on plan.");
	  		return false ;
	  	}
	  		  	
	  	/*if(ame_without_mm_choices >0){
	  		alert("You can apply AME Add-on plan along with related Major Medical plan.");
	  		return false ;
	  	}*/
	  		  	
	    /* ================= counting checked radio buttons for AME & Standalone plans end here ==================== */	  	  	
	    
	    /* ================= uncheck radio buttons for Standalone Life plans start here ==================== */
	  	
	  	for(var i=0; i<=1; i++) {
	  		if($('life_applicant_nonrop_'+i) != null && $('life_applicant_nonrop_'+i).checked) 
	  		life_checkbox_choices = life_checkbox_choices + 1;	  	 	
	  	}
	  	
	  	/* ================= uncheck radio buttons for Standalone Life plans end here ==================== */
	  	
	  	if(life_checkbox_choices >1){
	  		alert("You can apply only for one Life plan.");
	  		return false ;
	  	}	  	
	  	if(discount_standalone_checkbox_choices > 1 || discount_standalone_companion_checkbox_choices > 1){
	  		alert("You can apply only for one Guaranteed Issue plan.");
	  		return false ;
	  	}  		  
	  	if((discount_standalone_checkbox_choices >= 1) && (discount_standalone_companion_checkbox_choices >= 1) ){
	  		alert("You can apply only for one Guaranteed Issue plan.");
	  		return false ;
	  	}  
	  	if((ame_checkbox_choices >= 1) && (discount_standalone_checkbox_choices >= 1) ){
	  		alert("You can not apply AME Add-on with Guaranteed Issue plan of carrier CBA or Fairmont.");
	  		return false ;
	  	}  	  			
	  	
	  	/* ================= counting checked radio buttons for Short Term plans start here ==================== */
	  	
	  	if($('short_plan_id_' + getCookieByName('shortplanid'))) 
	  		var shortPlanIds = $('short_plan_id_' + getCookieByName('shortplanid')).value;
	  	 else 
	  		var shortPlanIds = ($('shortPlanIds')) ? $('shortPlanIds').value : "";
			
	  	if(shortPlanIds !=""){
		appendStringShort ="";
		var planIdsArray = shortPlanIds.split(",");   	
	  	for (var x = 0; x < planIdsArray.length; x++){
	  		var PlanCheckBox = "cb_short_plan_" + planIdsArray[x] ;
			if($(PlanCheckBox)) {  
			  	if($(PlanCheckBox).checked){
			  		short_checkbox_choices = short_checkbox_choices + 1;
			  		appendStringShort = appendStringShort + $(PlanCheckBox).value + ",";
			  	}
			}
	  	}
	  	}
	  	
		if($('counter')){
			for(var ctr=0; ctr<counter; ctr++){
				if($("cb_ShortTerm_" + ctr) != null && $("cb_ShortTerm_" + ctr).checked == true){			  	 
		  			if(!checkData("cb_ShortTerm_" + ctr + "_PlanName", "Enter plan name")) return false;
			  		if(!checkData("cb_ShortTerm_" + ctr + "_Deductible", "Enter deductible amount")) return false;
		  			if(!checkNumericData("cb_ShortTerm_" + ctr + "_Deductible", "Enter numeric value for deductible amount")) return false;
			  		if(!checkData("cb_ShortTerm_" + ctr + "_CoInsurance", "Enter Co-Insurance value")) return false;
		  			if(!checkData("cb_ShortTerm_" + ctr + "_BasePremium", "Enter premium amount")) return false;
			  		if(!checkNumericData("cb_ShortTerm_" + ctr + "_BasePremium", "Enter numeric value for premium")) return false;
			  		
				  	short_checkbox_choices = eval(short_checkbox_choices) + 1;  			  						   	
				}   
		  	}
		}
		
	  	if(short_checkbox_choices >1){
	  		alert("You can apply only for one Short term plan.");
	  		return false ;
	  	}
	  	
	  	/* ================= counting checked radio buttons for Short Term plans end here ==================== */
	  		  	
	  	/* ================= counting checked radio buttons for Life Insurance plans of Applicant & Child start here ==================== */
	  /*	
	  	var life_applicant_plan = "";
	  	var life_applicant_plan_id = "";
	  	var life_plan_err_msg = "";
	  	for(var i=0; i<=17; i++) {
	  		if(document.getElementById('life_applicant_rop_'+i))
	  		if(document.getElementById('life_applicant_rop_'+i).checked) {
	  			life_applicant_plan = document.getElementById('life_applicant_rop_'+i).value;
	  			if(life_applicant_plan_id == "") {
	  				life_applicant_plan_id = life_applicant_plan;
	  			} else {
	  				life_plan_err_msg = "Please select only one plan for Applicant/Child from 'with Return of Premium' or 'without Return of Premium' plans!";
	  			}
	  		}
	  	}
	  	if(life_plan_err_msg != "") {
	  		alert(life_plan_err_msg);
	  		return false ;
	  	}  	  	
	  	
	  	for(var i=0; i<=17; i++) {
	  		if(document.getElementById('life_applicant_nonrop_'+i)) 
	  		if(document.getElementById('life_applicant_nonrop_'+i).checked) {
	  			life_applicant_plan = document.getElementById('life_applicant_nonrop_'+i).value;
	  			if(life_applicant_plan_id == "") {
	  				life_applicant_plan_id = life_applicant_plan;
	  			} else {
	  				life_plan_err_msg = "Please select only one plan for Applicant/Child from 'with Return of Premium' or 'without Return of Premium' plans!";
	  			}
	  		}
	  	}
	  	if(life_plan_err_msg != "") {
	  		alert(life_plan_err_msg);
	  		return false ;
	  	}
	  	*/
		/* ================= counting checked radio buttons for Life Insurance plans of Applicant & Child end here ==================== */
		
		/* ================= counting checked radio buttons for Life Insurance plans of Spouse start here ==================== */
		/*
	  	var life_spouse_plan = "";
	  	var life_spouse_plan_id = "";
	  	var life_plan_err_msg = "";
	  	for(var i=0; i<=17; i++) {
	  		if(document.getElementById('life_spouse_rop_'+i))
	  		if(document.getElementById('life_spouse_rop_'+i).checked) {
	  			life_spouse_plan = document.getElementById('life_spouse_rop_'+i).value;
	  			if(life_spouse_plan_id == "") {
	  				life_spouse_plan_id = life_spouse_plan;
	  			} else {
	  				life_plan_err_msg = "Please select only one plan for Spouse from 'with Return of Premium' or 'without Return of Premium' plans!";
	  			}
	  		}
	  	}
	  	if(life_plan_err_msg != "") {
	  		alert(life_plan_err_msg);
	  		return false ;
	  	}  	  	
	  	
	  	for(var i=0; i<=17; i++) {
	  		if(document.getElementById('life_spouse_nonrop_'+i))
	  		if(document.getElementById('life_spouse_nonrop_'+i).checked) {
	  			life_spouse_plan = document.getElementById('life_spouse_nonrop_'+i).value;
	  			if(life_spouse_plan_id == "") {
	  				life_spouse_plan_id = life_spouse_plan;
	  			} else {
	  				life_plan_err_msg = "Please select only one plan for Spouse from 'with Return of Premium' or 'without Return of Premium' plans!";
	  			}
	  		}
	  	}
	  	if(life_plan_err_msg != "") {
	  		alert(life_plan_err_msg);
	  		return false ;
	  	}
	  	*/
	  	 
	  	if($('life_applicant_typein_checkbox') != null && $('life_applicant_typein_checkbox').checked){
	  		if(!checkData("life_applicant_typein_planname", "Enter plan name")) return false;
	  		if(!checkData("life_applicant_typein_duration", "Enter duration")) return false;
  			if(!checkNumericData("life_applicant_typein_duration", "Enter numeric value for duration")) return false;
	  		if(!checkData("life_applicant_typein_insamount", "Enter Insured amount")) return false;
  			if(!checkData("life_applicant_typein_premium", "Enter premium amount")) return false;
	  		if(!checkNumericData("life_applicant_typein_premium", "Enter numeric value for premium")) return false;			  		
	  		life_applicant_typein_checkbox_choices = 1;	  		
	  	}
	  	
	  	if(life_checkbox_choices > 0 && life_applicant_typein_checkbox_choices == 1){
	  		alert('Select either regular or type-in Life Insurance plan for Applicant');
	  		return false;
	  	}
	  	
	  	for(var i=0; i<=17; i++) {
	  		if($('life_spouse_nonrop_'+i) != null && $('life_spouse_nonrop_'+i).checked)
	  			life_spouse_checkbox_choices = life_spouse_checkbox_choices + 1;
	  	}
	  	 
	  	if($('life_spouse_typein_checkbox') != null && $('life_spouse_typein_checkbox').checked){
	  		if(!checkData("life_spouse_typein_planname", "Enter plan name")) return false;
	  		if(!checkData("life_spouse_typein_duration", "Enter duration")) return false;
  			if(!checkNumericData("life_spouse_typein_duration", "Enter numeric value for duration")) return false;
	  		if(!checkData("life_spouse_typein_insamount", "Enter Insured amount")) return false;
  			if(!checkData("life_spouse_typein_premium", "Enter premium amount")) return false;
	  		if(!checkNumericData("life_spouse_typein_premium", "Enter numeric value for premium")) return false;			  		
	  		life_spouse_typein_checkbox_choices = 1;	  		
	  	}	
	  		
	  	if(life_spouse_checkbox_choices > 0 && life_spouse_typein_checkbox_choices == 1){
	  		alert('Select either regular or type-in Life Insurance plan for spouse');
	  		return false;
	  	}	  		  		
	  			 	
		/* ================= counting checked radio buttons for Life Insurance plans of Spouse end here ==================== */
	  	
	  	
	  	
	  	/* VMW-6137*/
	  	if((checkbox_choices == 0) && (short_checkbox_choices == 0) && (discount_standalone_checkbox_choices == 0) && (discount_standalone_companion_checkbox_choices == 0) ){
	  		alert("Please select atleast one Major/Guaranteed Issue/Short Term to apply.");
	  		return false ;
	  	} 
	  	
	  	if(ame_checkbox_choices == 1 && checkbox_choices == 0 && discount_standalone_checkbox_choices == 0 && discount_standalone_companion_checkbox_choices == 0){
	  		alert("You can apply AME Add-on plan along with Major Medical or Guaranteed Issue plan only.");
	  		return false;
	  	} 
	  	
	  	if($('assurantplanid') != null && $($('assurantplanid').value).checked == true){  			
			if($('hpaplanid') != null && $($('hpaplanid').value).checked == false){
				var confirmans = confirm("Before you apply for Assurant Major Medical plan for this user, please choose an HPA short term medical plan as well. Add the Short Term Plan now? Click OK to proceed without adding STM else click Cancel and add STM");
				if(confirmans == false) return false;
			}  				
  		}
  			  	
	  	var submitpage = ($('policy_exists').value == 'Yes') ? 'policy_present.php' :  'submitquotes.php';
	  	
	  	document.applySelectedForm.target="_self";
	  	document.applySelectedForm.action = submitpage;
	  	document.applySelectedForm.agent_action.value='apply'; 
	  	document.applySelectedForm.submit();
	}	  	
 }
 
  function SendEmailValidate(agent_action){  	
  	var tempPlanid = document.cookie;
  	var checkbox_choices = 0;
  	var short_checkbox_choices = 0;
  	var ame_checkbox_choices = 0;
  	var discount_standalone_checkbox_choices = 0;
	var discount_standalone_companion_checkbox_choices = 0;	  
  	var life_checkbox_choices = 0;
  	var life_spouse_checkbox_choices = 0;
  	var life_applicant_typein_checkbox_choices =0;
  	var life_spouse_typein_checkbox_choices =0;
  	
  	if($('major_plan_id_' + getCookieByName('majorplanid'))) 
		var planIds = $('major_plan_id_' + getCookieByName('majorplanid')).value;
	else
		var planIds = ($('majorPlanIds')) ? $('majorPlanIds').value : "";	
  	
  	if(planIds !=""){
	appendString ="";
	var planIdsArray = planIds.split(",");   	
  	for (var x = 0; x < planIdsArray.length; x++){
  		var PlanCheckBox = "cb_major_plan_" + planIdsArray[x] ;
  		var AMEPlanCheckBox = "discount_product_" + planIdsArray[x];
  		var LifePlanCheckBox = "life_applicant_nonrop_" + planIdsArray[x];
  		
  		if($(PlanCheckBox) != null && $(PlanCheckBox).checked == true) {  
			checkbox_choices = checkbox_choices + 1;
		  	appendString = appendString + $(PlanCheckBox).value + ",";
		}
		
		if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked == true)
			ame_checkbox_choices = ame_checkbox_choices + 1;					
		if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked) 
			life_checkbox_choices = life_checkbox_choices + 1;
  		}
	}
  	
  	if($('counter')){
		var counter = $('counter').value;
	  	for(var ctr=0; ctr<counter; ctr++){
	  	  if($("cb_Health_" + ctr) != null && $("cb_Health_" + ctr).checked == true){
			if(!checkData("cb_Health_" + ctr + "_PlanName", "Enter plan name")) return false;
	  		if(!checkData("cb_Health_" + ctr + "_Deductible", "Enter deductible amount")) return false;
	  		if(!checkNumericData("cb_Health_" + ctr + "_Deductible", "Enter numeric value for deductible amount")) return false;
			if(!checkData("cb_Health_" + ctr + "_CoInsurance", "Enter Co-Insurance value")) return false;			
			if(!checkData("cb_Health_" + ctr + "_BasePremium", "Enter premium amount")) return false;
			if(!checkNumericData("cb_Health_" + ctr + "_BasePremium", "Enter numeric value for premium")) return false; 		 	 
				
			checkbox_choices = eval(checkbox_choices) + 1;  			  	    
	  	  }  	

		if($('discount_product_cb_Health_' + ctr) != null && $('discount_product_cb_Health_'+ctr).checked == true)
			ame_checkbox_choices = ame_checkbox_choices + 1;
		if($('life_applicant_nonrop_cb_Health_' + ctr) != null && $('life_applicant_nonrop_cb_Health_' + ctr).checked == true) 	
			life_checkbox_choices = life_checkbox_choices + 1;	
	  	}
  	}
  
  	
  	if($('short_plan_id_' + getCookieByName('shortplanid'))) 
  		var shortPlanIds = $('short_plan_id_' + getCookieByName('shortplanid')).value;
  	 else
  		var shortPlanIds = ($('shortPlanIds')) ? $('shortPlanIds').value : "";
	
  	if(shortPlanIds !=""){
		appendStringShort ="";
		var planIdsArray = shortPlanIds.split(",");
		if(planIdsArray.length == 0) {
			shortPlanIds = shortPlanIds + ",";
			planIdsArray = shortPlanIds.split(",");
		}   	
  		for (var x = 0; x < planIdsArray.length; x++){
  			var PlanCheckBox = "cb_short_plan_" + planIdsArray[x] ;
			if($(PlanCheckBox) != null && $(PlanCheckBox).checked == true) {		  	
				short_checkbox_choices = short_checkbox_choices + 1;
		  		appendStringShort = appendStringShort + $(PlanCheckBox).value + ",";		  	
			}
  		}
  	}
  	
  	if($('counter')){
	  	for(var ctr=0; ctr<counter; ctr++){
			if($("cb_ShortTerm_" + ctr) != null && $("cb_ShortTerm_" + ctr).checked ==true){
	  			if(!checkData("cb_ShortTerm_" + ctr + "_PlanName", "Enter plan name")) return false;
		  		if(!checkData("cb_ShortTerm_" + ctr + "_Deductible", "Enter deductible amount")) return false;
	  			if(!checkNumericData("cb_ShortTerm_" + ctr + "_Deductible", "Enter numeric value for deductible amount")) return false;
		  		if(!checkData("cb_ShortTerm_" + ctr + "_CoInsurance", "Enter Co-Insurance value")) return false;
	  			if(!checkData("cb_ShortTerm_" + ctr + "_BasePremium", "Enter premium amount")) return false;
		  		if(!checkNumericData("cb_ShortTerm_" + ctr + "_BasePremium", "Enter numeric value for premium")) return false;
		  		
			  	short_checkbox_choices = eval(short_checkbox_choices) + 1;  			  					  
			}   
	  	}
  	}  		
  	
  	//===================== START =====================	LIFE INSURANCE PLANS FOR APPLICANT/CHILD
  /*	var life_applicant_plan = "";
  	var life_applicant_plan_id = "";
  	var life_plan_err_msg = "";
  	for(var i=0; i<=17; i++) {
  		if(document.getElementById('life_applicant_rop_'+i))
  		if(document.getElementById('life_applicant_rop_'+i).checked) {
  			life_applicant_plan = document.getElementById('life_applicant_rop_'+i).value;
  			if(life_applicant_plan_id == "") {
  				life_applicant_plan_id = life_applicant_plan;
  			} else {
  				life_plan_err_msg = "Please select only one plan for Applicant/Child from 'with Return of Premium' or 'without Return of Premium' plans!";
  			}
  		}
  	}
  	if(life_plan_err_msg != "") {
  		alert(life_plan_err_msg);
  		return false ;
  	}  	  	
  	
  	for(var i=0; i<=17; i++) {
  		if(document.getElementById('life_applicant_nonrop_'+i)) 
  		if(document.getElementById('life_applicant_nonrop_'+i).checked) {
  			life_applicant_plan = document.getElementById('life_applicant_nonrop_'+i).value;
  			if(life_applicant_plan_id == "") {
  				life_applicant_plan_id = life_applicant_plan;
  			} else {
  				life_plan_err_msg = "Please select only one plan for Applicant/Child from 'with Return of Premium' or 'without Return of Premium' plans!";
  			}
  		}
  	}
  	if(life_plan_err_msg != "") {
  		alert(life_plan_err_msg);
  		return false ;
  	}
	//====================	END =======================	LIFE INSURANCE PLANS FOR APPLICANT/CHILD
	//====================	START ============================	LIFE INSURANCE PLANS FOR SPOUSE
  	var life_spouse_plan = "";
  	var life_spouse_plan_id = "";
  	var life_plan_err_msg = "";
  	for(var i=0; i<=17; i++) {
  		if(document.getElementById('life_spouse_rop_'+i))
  		if(document.getElementById('life_spouse_rop_'+i).checked) {
  			life_spouse_plan = document.getElementById('life_spouse_rop_'+i).value;
  			if(life_spouse_plan_id == "") {
  				life_spouse_plan_id = life_spouse_plan;
  			} else {
  				life_plan_err_msg = "Please select only one plan for Spouse from 'with Return of Premium' or 'without Return of Premium' plans!";
  			}
  		}
  	}
  	if(life_plan_err_msg != "") {
  		alert(life_plan_err_msg);
  		return false ;
  	}  	  	
  	
  	for(var i=0; i<=17; i++) {
  		if(document.getElementById('life_spouse_nonrop_'+i))
  		if(document.getElementById('life_spouse_nonrop_'+i).checked) {
  			life_spouse_plan = document.getElementById('life_spouse_nonrop_'+i).value;
  			if(life_spouse_plan_id == "") {
  				life_spouse_plan_id = life_spouse_plan;
  			} else {
  				life_plan_err_msg = "Please select only one plan for Spouse from 'with Return of Premium' or 'without Return of Premium' plans!";
  			}
  		}
  	}
  	if(life_plan_err_msg != "") {
  		alert(life_plan_err_msg);
  		return false ;
  	}
  	*/
  		if($('life_applicant_typein_checkbox') != null && $('life_applicant_typein_checkbox').checked){
	  		if(!checkData("life_applicant_typein_planname", "Enter plan name")) return false;
	  		if(!checkData("life_applicant_typein_duration", "Enter duration")) return false;
  			if(!checkNumericData("life_applicant_typein_duration", "Enter numeric value for duration")) return false;
	  		if(!checkData("life_applicant_typein_insamount", "Enter Insured amount")) return false;
  			if(!checkData("life_applicant_typein_premium", "Enter premium amount")) return false;
	  		if(!checkNumericData("life_applicant_typein_premium", "Enter numeric value for premium")) return false;			  		
	  		life_applicant_typein_checkbox_choices = 1;	  		
	  	}  	 	
	  	
	  	for(var i=0; i<=17; i++) {
	  		if($('life_spouse_nonrop_'+i) != null && $('life_spouse_nonrop_'+i).checked)
	  			life_spouse_checkbox_choices = life_spouse_checkbox_choices + 1;
	  	}
	  	 
	  	if($('life_spouse_typein_checkbox') != null && $('life_spouse_typein_checkbox').checked){
	  		if(!checkData("life_spouse_typein_planname", "Enter plan name")) return false;
	  		if(!checkData("life_spouse_typein_duration", "Enter duration")) return false;
  			if(!checkNumericData("life_spouse_typein_duration", "Enter numeric value for duration")) return false;
	  		if(!checkData("life_spouse_typein_insamount", "Enter Insured amount")) return false;
  			if(!checkData("life_spouse_typein_premium", "Enter premium amount")) return false;
	  		if(!checkNumericData("life_spouse_typein_premium", "Enter numeric value for premium")) return false;			  		
	  		life_spouse_typein_checkbox_choices = 1;	  		
	  	}	
	  		
	  	if(life_spouse_checkbox_choices > 0 && life_spouse_typein_checkbox_choices == 1){
	  		alert('Select either regular or type-in Life Insurance plan for spouse');
	  		return false;
	  	}
  	
	//====================	END =======================	LIFE INSURANCE PLANS FOR SPOUSE
	
	 
	//====================	START =======================	DISCOUNT PLANS
	var discountplanIds = ($('discountPlanIds')) ? $('discountPlanIds').value : "";
  	
  	if(discountplanIds !=""){
	appendString ="";
	var discountplanIdsArray = discountplanIds.split(",");   	
  	for (var x = 0; x < discountplanIdsArray.length; x++){
	  	var AMEPlanCheckBox = "discount_product_companion_" + discountplanIdsArray[x] ;
		var discountPlanStanaloneCheckBox = "discount_product_standalone_" + discountplanIdsArray[x] ;
		var discountPlanStanaloneCompanionCheckBox = "discount_product_standalone_companion_" + discountplanIdsArray[x] ;
		var LifePlanCheckBox = "life_applicant_nonrop_" + discountplanIdsArray[x];
		if($(discountPlanStanaloneCheckBox) != null && $(discountPlanStanaloneCheckBox).checked == true) {  
	  		discount_standalone_checkbox_choices = discount_standalone_checkbox_choices + 1;
	  		appendString = appendString + $(discountPlanStanaloneCheckBox).value + ",";
		}	
		if($(discountPlanStanaloneCompanionCheckBox) != null && $(discountPlanStanaloneCompanionCheckBox).checked)
			discount_standalone_companion_checkbox_choices = discount_standalone_companion_checkbox_choices +1;
		if($(LifePlanCheckBox) != null && $(LifePlanCheckBox).checked)   				  	
			life_checkbox_choices = life_checkbox_choices + 1;
		if($(AMEPlanCheckBox) != null && $(AMEPlanCheckBox).checked == true)
			ame_checkbox_choices = ame_checkbox_choices + 1;			
  	  }
  	}
  	  	
	for(var i=0; i<=1; i++) {
		if($('life_applicant_nonrop_'+i) != null && $('life_applicant_nonrop_'+i).checked) 
		life_checkbox_choices = life_checkbox_choices + 1;	  	 	
	}
	
  	if(ame_checkbox_choices >1){
  		alert("You can send email only for one AME Add-on plan.");
  		return false ;
  	}     	  	
	/*if(discount_standalone_checkbox_choices > 1 || discount_standalone_companion_checkbox_choices > 1){
	  	alert("You can send email only for one Guaranteed Issue plan.");
	  	return false ;
	}  		  
	if((discount_standalone_checkbox_choices >=1) && (discount_standalone_companion_checkbox_choices >= 1) ){
		alert("You can send email only for one Guaranteed Issue plan.");
		return false ;
	}*/ 
  	if((ame_checkbox_choices >=1) && (discount_standalone_checkbox_choices >= 1) ){
  		alert("You can not send email for AME Add-on with Guaranteed Issue plan of carrier CBA or Fairmont.");
  		return false ;
  	}	  		
  	if(short_checkbox_choices >1){
  		alert("You can send email only for one Short term plan.");
  		return false ;
  	}
  	if((checkbox_choices == 0) && (short_checkbox_choices == 0) && (discount_standalone_checkbox_choices == 0) && (discount_standalone_companion_checkbox_choices == 0)){
  		alert("Please select atleast one Major/Guaranteed Issue/Short Term to send email.");
  		return false ;
  	}
  	if(ame_checkbox_choices == 1 && checkbox_choices == 0 && discount_standalone_checkbox_choices == 0 && discount_standalone_companion_checkbox_choices == 0){	
	 	alert("You can send email for AME Add-on plan along with Major Medical or Guaranteed Issue plan only.");
	 	return false;
	}
	if(life_checkbox_choices >1){
	  	alert("You can send email only for one Life plan.");
	  	return false ;
	}	
	if(life_checkbox_choices > 0 && life_applicant_typein_checkbox_choices == 1){
	  	alert('Select either regular or type-in Life Insurance plan for Applicant');
	  	return false;
	 }
	 
  	document.applySelectedForm.target="_blank";
  	document.applySelectedForm.action = "email_quote.php";  	
  	document.applySelectedForm.agent_action.value= agent_action; 
  	document.applySelectedForm.submit();
 }
 
 
  function DeadValidate(e){  	
  	var tempPlanid = document.cookie;
  	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
 	if(keycode==13){
 	return false;
	}
 	 	
  	
  	if(document.applySelectedForm.disposition_code.value == ""){
  		alert("Please select reason code for dead lead.");
  		return false ;
  	}
  	document.applySelectedForm.action = "submitquotes.php";
  	document.applySelectedForm.agent_action.value='dead';
  	document.applySelectedForm.submit();
  }
  
  function NoteValidate(e){  	
  	var tempPlanid = document.cookie;
  	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
 	if(keycode==13){
 	return false;
	}
 	 	
  	
  	if(document.addNoteForm.notes.value == ""){
  		alert("Please add note for lead.");
  		return false ;
  	}
  	document.addNoteForm.action = "submitquotes.php";
  	document.addNoteForm.agent_action.value='add_note';
  	document.addNoteForm.submit();
  }
  
  
  function calculate_premium(chkboxId, premiumFieldId, action, oldPremium, showPremium){ 	
	  var totalpremium = 0;
	  var total = eval(($('totalpremiumamt').innerHTML).replace(",","")).toFixed(2);
 	  var numberformat = /^[0-9\.]+$/;
	if(document.getElementById(chkboxId)) {						
		var premium = eval($(premiumFieldId)).value.replace(",","");
		if(premium.match(numberformat)){
			var planId = $(chkboxId).value;				 
			if($(chkboxId).checked == true) {
				$(premiumFieldId).disabled = false;								 
				if(action == "sub")
		   			totalpremium = eval(total) - eval(premium);
		   		else if(action == 'showpremium')
			   		totalpremium =   eval(total) + eval(($('PlanPremium_'+planId).innerHTML).replace(",",""));
				else 
					totalpremium = eval(total) + eval(premium);		
			} else {
				$(premiumFieldId).disabled = true;
				 if(action == 'showpremium')
		   			totalpremium = eval(total) - eval(($('PlanPremium_'+planId).innerHTML).replace(",",""));
		   		 else		
					totalpremium = eval(total) - eval(premium);					
			}		
		}
		else
		 totalpremium = eval(total);
		 
		 if(showPremium == 'true' && $(chkboxId + '_PlanPremium') != null)
		 	$(chkboxId + '_PlanPremium').innerHTML = totalpremium.toFixed(2);
		
	} else if(oldPremium != "") {
		totalpremium = oldPremium; 
	}	   	
   	
	if(totalpremium < 0)
		totalpremium = 0;
	
	displayTotalPremium(totalpremium.toFixed(2));  		   
  }
  
 function refreshParent() {
 	window.opener.location.href = window.opener.location.href;
	if (window.opener.progressWindow) {
    	window.opener.progressWindow.close()
	}
  	window.close();
}
//========================================================================================
var insurance_Type;
var Currently_Insured;
var Current_MonthlyPremium;
var add_ChildROP;
var carrier_Name;
var lead_id;
//showSelectedPlans('viewAddtionalPlans', '{$leadid}', 'chk_sendEmail_{$plan->getinsuranceType()}_{$plan->getId()}', '{$plan->getinsuranceType()}', '{$CurrentlyInsured}', '{$CurrentMonthlyPremium}', '{$carrierName}', '');
function showSelectedPlans(formName, leadid, chkbox_Id, insuranceType, CurrentlyInsured, CurrentMonthlyPremium, carrierName, addChildROP) {
	var selectedPlansHtmlText;		
	var selectedPlanId;
	var chkStatus;
	var rowIdInGetquote;	
	insurance_Type = insuranceType;
	Currently_Insured = CurrentlyInsured;
	Current_MonthlyPremium = CurrentMonthlyPremium;
	add_ChildROP = addChildROP;
	carrier_Name = carrierName;
	lead_id = leadid;
	
	if(document.getElementById(chkbox_Id)) {
		selectedPlanId = eval("document." + formName + "." + chkbox_Id).value;
		if($(chkbox_Id).checked==true) {
			chkStatus = "checked";
		} else {
			chkStatus = "unchecked";
		} 			
		var parmeterVal = 'request=init&leadid=' + lead_id + '&selectedPlanId='+ selectedPlanId + '&chkStatus=' + chkStatus + '&insuranceType=' + insuranceType + '&CurrentlyInsured=' + CurrentlyInsured + '&CurrentMonthlyPremium=' + CurrentMonthlyPremium + '&addChildROP=' + addChildROP;
		new Ajax.Request('/brokercenter/selectedPlans_sendEmail.php', { 
			method: 'GET',
			parameters: parmeterVal,
			onComplete: printSelectedPlans	
			}
		);				  
	} else {
		// when navigated using previous/next link, then on page load, this funciton is called.
		var parmeterVal = 'request=init&leadid=' + lead_id + '&selectedPlanId=&chkStatus=&insuranceType=' + insuranceType + '&CurrentlyInsured=' + CurrentlyInsured + '&CurrentMonthlyPremium=' + CurrentMonthlyPremium+ '&addChildROP=' + addChildROP;
		new Ajax.Request('/brokercenter/selectedPlans_sendEmail.php', { 
			method: 'GET',
			parameters: parmeterVal,
			onComplete: printSelectedPlans	
			}
		);		
	}
}

function printSelectedPlans(request){
	new Ajax.Updater("div_SelectedPlans", '/brokercenter/selectedPlans_sendEmail.php', {method: 'GET',parameters: 'request=fetch&leadid=' + lead_id + '&carrier_Name='+ carrier_Name + '&insuranceType=' + insurance_Type + '&CurrentlyInsured=' + Currently_Insured + '&CurrentMonthlyPremium=' + Current_MonthlyPremium + '&addChildROP=' + add_ChildROP} );
	var response = request.responseText;
	response = response.replace(/[\r\n\t\s]+/, '');
}

function checkFilterForm(){
	var mindeductible = $('mindeductible').value;
	var maxdeductible = $('maxdeductible').value;
	
	if(isNaN(mindeductible)){
		alert("Enter numeric value");
		$('mindeductible').focus();
		return false;
	}	
	if(mindeductible < 0){
		alert("Enter value > 0");
		$('mindeductible').focus();
		return false;
	}		
	if(isNaN(maxdeductible)){
		alert("Enter numeric value");
		$('maxdeductible').focus();
		return false;
	}
	if(maxdeductible < 0){
		alert("Enter value > 0");
		$('mindeductible').focus();
		return false;
	}	

	if(mindeductible !='' && maxdeductible !='' && (eval(mindeductible) > eval(maxdeductible))){
		alert("Minimum value should be lower than maximum value");
		$('mindeductible').focus();
		return false;
	}
  
   $('filterfrm').submit();
}

function hideDiscountPlan(plantype){	 
	
	if(document.getElementById('discount_product_' + getCookieByName('discountplanid'))) {
  		var discountplanIds = document.getElementById('discount_product_' + getCookieByName('discountplanid')).value;
  	}else if(document.getElementById('discount_product_standalone_' + getCookieByName('discountplanid'))){
  		var discountplanIds = document.getElementById('discount_product_standalone_' + getCookieByName('discountplanid')).value;  		  	
  	}else {
  		if(document.getElementById('discountPlanIds')){	
			var discountplanIds = document.applySelectedForm.discountPlanIds.value ;			  
		}else{
			var discountplanIds = "";
		}	
  	}
  	
  		
	if(plantype == 'DiscountPlan'){
			if(discountplanIds !=""){	
				var discountplanIdsArray = discountplanIds.split(",");   	
				for (var x = 0; x < discountplanIdsArray.length; x++){
					var discountPlanStanaloneCheckBox = "discount_product_standalone_" + discountplanIdsArray[x] ;
					var discountPlanStanaloneCompanionCheckBox = "discount_product_standalone_companion_" + discountplanIdsArray[x] ;
					if($(discountPlanStanaloneCheckBox) != null && $(discountPlanStanaloneCheckBox).checked == true)  
						$(discountPlanStanaloneCheckBox).checked = false;					
					if($(discountPlanStanaloneCompanionCheckBox) != null && $(discountPlanStanaloneCompanionCheckBox).checked == true)   
						$(discountPlanStanaloneCompanionCheckBox).checked = false;					
	  		  	}
	  		}
			
	} else if(plantype == 'MajorPlan'){		 		
		var hideplan = true;
		var totalChildren = $('totalChildren').value;		
				
		if($('HealthMembersCount').value > 0)
		{
			if($('major_medical_applicant').checked)
				hideplan = false;
			else if($('major_medical_spouse') != null)
				if($('major_medical_spouse').checked)
					hideplan = false;	
			else {
			 	for(var i=1; i<=totalChildren; i++){
	 				 if($('major_medical_children_'+i).checked){ 					 	
						hideplan = false;	
						break;
	 				 }	
			 	 }
		 	 }
	
			if(hideplan){	
				if(discountplanIds !=""){	
					var discountplanIdsArray = discountplanIds.split(",");   					
					for (var x = 0; x < discountplanIdsArray.length; x++){
				  		var discountPlanCheckBox = "discount_product_" + discountplanIdsArray[x] ;
						if(document.getElementById(discountPlanCheckBox)) {  
						  	if(document.getElementById(discountPlanCheckBox).checked)		  		
								document.getElementById(discountPlanCheckBox).checked = false;
						}
		  			}
		  		}			
				
				if($('addonplan') != null)
					$('addonplan').hide(); 
			}	
			 else{							
				if($('addonplan') != null)
					$('addonplan').show(); 
			 }	
		}			 
	 }
}

function noSpaces(str) {
	return str.replace(/\s*/g, '')
}

function checkpremium(planctr){
	$('note_' + planctr).style.display = 'none';
	$('add_note_' + planctr).style.display = 'block';	
	if(noSpaces($('mm_' + planctr + '_premium').value) == '' || isNaN(noSpaces($('mm_' + planctr + '_premium').value)) || noSpaces($('mm_' + planctr + '_premium').value) <= 0){
		alert('Enter premium for Major Madical plan');
		$('mm_' + planctr + '_premium').focus();		
	}
	else
	{
		$total = $('total_premium').value;
		$old_pre = $('mm_' + planctr + '_oldpremium').value;
		$new_pre = noSpaces($('mm_' + planctr + '_premium').value);
		$prediff = eval($old_pre)-eval($new_pre);
		$total = (eval($total) - eval($prediff));
		$('mm_' + planctr + '_oldpremium').value = eval($new_pre);	
		$('total_premium').value = $total.toFixed(2); 		
		$('disptotal').innerHTML = $total.toFixed(2); 		
	}
}

function checkapplicantdata(){
	for(i=1; i<=$('totalmember').value; i++){		
		if(noSpaces($(i+'_first_name').value) == '' || noSpaces($(i+'_last_name').value) == '' || noSpaces($(i+'_ss1').value) == '' || noSpaces($(i+'_ss2').value) == '' || noSpaces($(i+'_ss3').value) == ''){
			$('appinfo').className = "left red";
			$('saveTR').style.display = 'none';
			break;
		}
		if(noSpaces($(i+'_ss1').value) != ''){
			if(noSpaces($(i+'_ss1').value).length != 3 || isNaN(noSpaces($(i+'_ss1').value))){
				$('appinfo').className = "left red";
				$('saveTR').style.display = 'none';
				break;	
			}
		}
		if(noSpaces($(i+'_ss2').value) != ''){
			if(noSpaces($(i+'_ss2').value).length != 2 || isNaN(noSpaces($(i+'_ss2').value))){
				$('appinfo').className = "left red";
				$('saveTR').style.display = 'none';
				break;	
			}
		}
		if(noSpaces($(i+'_ss3').value) != ''){
			if(noSpaces($(i+'_ss3').value).length != 4 || isNaN(noSpaces($(i+'_ss3').value))){
				$('appinfo').className = "left red";
				$('saveTR').style.display = 'none';
				break;	
			}
		}
		if(noSpaces($(i+'_ss1').value) == '000' && noSpaces($(i+'_ss2').value) == '00' && noSpaces($(i+'_ss3').value) == '0000'){
			$('appinfo').className = "left red";
			$('saveTR').style.display = 'none';			
			break;	
	 	}
	}

	if((i-1) == $('totalmember').value){
		$('saveTR').style.display = 'block';
		$('appinfo').className = "left grn";
	}
	
	if($('appinfo').className == "left grn" && $('financeinfo').className == "left grn" && $('scheduleinfo').className == "left grn" && $('wrapupinfo').className == "left grn")
	 $('submitBtn').style.display = 'block'; 
	else 
	 $('submitBtn').style.display = 'none';
}

function validateForm(){
	if($('mm_premium') != null){
		if(noSpaces($('mm_premium').value) == '' || isNaN(noSpaces($('mm_premium').value)) || noSpaces($('mm_premium').value) == 0){
			alert('Enter premium for Major Madical plan');	
			return false;
		}	
		if(noSpaces($('mm_premium').value) != noSpaces($('mm_oldpremium').value) && noSpaces($('major_note').value) == ''){
			alert('Put note why you change premium amount');	
			return false;
		}
	}
	$('submitBtn').style.display = 'none';
    $('submitBtn2').style.display = 'block';	
	$('financeInfoForm').submit();	
}

function savePremium(planctr){
	setCookie('mm_' + planctr + '_premium', noSpaces($('mm_' + planctr + '_premium').value));
	setCookie('major_' + planctr + '_note', $('major_' + planctr + '_note').value);
	$('add_note_' + planctr).style.display = 'none';	
	$('note_' + planctr).style.display = 'block';
	$('noteHTML_' + planctr).innerHTML = $('major_' + planctr + '_note').value;
	setCookie('total_premium',$('disptotal').innerHTML);
}

function pouplatePremium(){	
	if($('totalmajormedicalplan') != null){
		$totalmmplan = $('totalmajormedicalplan').value;
		setCookie('totalmmplan',$totalmmplan);
		for($i=1; $i<=$totalmmplan; $i++){
			if(cookieExist('mm_' + $i + '_premium'))
				$('mm_' + $i + '_premium').value = getCookieByName('mm_' + $i + '_premium');
			if(cookieExist('total_premium'))	
				$('disptotal').innerHTML = getCookieByName('total_premium');
			
			if(cookieExist('major_' + $i + '_note') && getCookieByName('major_' + $i + '_note') != ''){
				$('note_' + $i).style.display = 'block';		
				$('major_' + $i + '_note').value = $('noteHTML_' + $i).innerHTML = getCookieByName('major_' + $i + '_note');
			}	
		}	
	}	
}

function saveApplicantInfo(){ 
 $totalapplicant = $('totalmember').value;
 for($i=1; $i<=$totalapplicant; $i++){
 	setCookie($i+'_first_name',$($i+'_first_name').value);
 	setCookie($i+'_last_name',$($i+'_last_name').value);
 	setCookie($i+'_ss1',noSpaces($($i+'_ss1').value));
 	setCookie($i+'_ss2',noSpaces($($i+'_ss2').value));
 	setCookie($i+'_ss3',noSpaces($($i+'_ss3').value));
  }	
  setCookie('totalmember',$totalapplicant); 
}

function populateApplicantInfo(){
 $totalapplicant = $('totalmember').value;
 for($i=1; $i<=$totalapplicant; $i++){
 	if(cookieExist($i+'_first_name'))
 		$($i+'_first_name').value = getCookieByName($i+'_first_name');
 	if(cookieExist($i+'_last_name'))	 
 		$($i+'_last_name').value = getCookieByName($i+'_last_name');
 	if(cookieExist($i+'_ss1'))
 		$($i+'_ss1').value = getCookieByName($i+'_ss1');
 	if(cookieExist($i+'_ss2'))
 		$($i+'_ss2').value = getCookieByName($i+'_ss2');
 	if(cookieExist($i+'_ss3'))
 		$($i+'_ss3').value = getCookieByName($i+'_ss3'); 	
  }	
  checkapplicantdata();
}

function deleteCookie(){
	$totalmmplan = getCookieByName('totalmmplan');
	if($totalmmplan != null){
		for($i=1; $i<=$totalmmplan; $i++){
			delete_cookie('mm_' + $i + '_premium');
			delete_cookie('major_' + $i + '_note');
		}	
	}	
	delete_cookie('total_premium');	
	if(cookieExist('totalmember')){
		$totalapplicant = getCookieByName('totalmember');
		for($i=1; $i<=$totalapplicant; $i++){
 			delete_cookie($i+'_first_name'); 
 			delete_cookie($i+'_last_name');
 			delete_cookie($i+'_ss1');
 			delete_cookie($i+'_ss2');
 			delete_cookie($i+'_ss3');	
   		}
	} 	
}

function validateWrapInfo(FRM){		
	for(i=0; i<FRM.wrapoptions.length; i++){
	 if(FRM.wrapoptions[i].checked == false){
	 	alert("You have not check all options");
	 	return false;
	 }			
	}	
	FRM.submit();
}

function chkUnchkOptBenefit(benefitChkBox, planPremiumFld, planPremiumVal, optionalPremium, planKey, planName, optionalPayOut, checkBoxId) {
	var chkboxVal, planPremiumVal, planPlatinumPremiumVal, premiumWithBenefit, premiumWithBenefitMouseOver;
	var benefitChkBoxId = benefitChkBox.id;		
	var totalPremium = 0;
	var existingTotalPremium = eval($('totalpremiumamt').innerHTML).toFixed(2);
	chkboxVal = (benefitChkBoxId.indexOf('GR_') == 0 || benefitChkBoxId.indexOf('Addon_') == 0) ? parseFloat(benefitChkBox.value) : optionalPremium ;	  
	//planPremiumVal = ($(planPremiumFld)) ? parseFloat($(planPremiumFld).value) : planPremiumVal = 0;
	planPremiumVal = ($(planPremiumFld)) ? parseFloat($(planPremiumFld).value.replace(",","")) : planPremiumVal = 0; 
	if(benefitChkBox.checked){
		//premiumWithBenefit = parseFloat(eval($('PlanPremium_'+planKey).innerHTML) + chkboxVal);
		premiumWithBenefit = parseFloat(eval($('PlanPremium_'+planKey).innerHTML.replace(",","")) + chkboxVal);  
		premiumWithBenefitMouseOver = (benefitChkBoxId.indexOf('GR_') == 0 || benefitChkBoxId.indexOf('Addon_') == 0 ) ? parseFloat(planPremiumVal + chkboxVal) : parseFloat(planPremiumVal);	
		totalPremium = eval(existingTotalPremium) + eval(chkboxVal);
	}else if(planPremiumVal > 0) {
		//premiumWithBenefit = parseFloat(eval($('PlanPremium_'+planKey).innerHTML) - chkboxVal);
		premiumWithBenefit = parseFloat(eval($('PlanPremium_'+planKey).innerHTML.replace(",","")) - chkboxVal); 
		premiumWithBenefitMouseOver = (benefitChkBoxId.indexOf('GR_') == 0 || benefitChkBoxId.indexOf('Addon_') == 0 ) ? parseFloat(planPremiumVal - chkboxVal) : parseFloat(planPremiumVal);		
		totalPremium = eval(existingTotalPremium) - eval(chkboxVal);
	}
	var num = new Number(premiumWithBenefit);
	$('PlanPremium_'+planKey).innerHTML = num.toFixed(2);

	num = new Number(premiumWithBenefitMouseOver);
	var showPlanDetailLink = true;
	if(planKey.length > 6) {
		var GRplanKeyPrefix = planKey.substring(0,6);
		if(GRplanKeyPrefix == 'GR_Id_')
			showPlanDetailLink = false;
	} 
	if($('premiumMouseOver_'+planKey) != null) {
		if(showPlanDetailLink) {
			$('premiumMouseOver_'+planKey).innerHTML = '<a target="_blank" href="/brokercenter/plan-detail.php?id='+planKey+'" onmouseover="Tip(\'Premium $<strong>'+ num.toFixed(2)+'</strong> monthly\')" onmouseout="UnTip()">'+planName+'</a>';
		} else {
			$('premiumMouseOver_'+planKey).innerHTML = '<a onmouseover="Tip(\'Premium $<strong>'+ num.toFixed(2)+'</strong> monthly\')" onmouseout="UnTip()">'+planName+'</a>';
		}
	}
	if($(planPremiumFld))
		$(planPremiumFld).value = num.toFixed(2); 
	
	if($(checkBoxId) != null && $(checkBoxId).checked == true)
		displayTotalPremium(totalPremium.toFixed(2));  


	//calculatePayout(benefitChkBox,planKey,optionalPayOut);
}

function typeinBoxChkUnchkOptBenefit(benefitChkBox, planPremiumFld, optionalPremium, planId, planType, optionalPayOut){
	var totalPremium = 0;
	var existingTotalPremium = eval($('totalpremiumamt').innerHTML).toFixed(2);	
	if(planType == 'AME')
		optionalPremium = eval($('displabel_'+planId).innerHTML);
		
	if(benefitChkBox.checked){
		premiumWithBenefit = eval($(planId + '_PlanPremium').innerHTML) + eval(optionalPremium); 
		totalPremium = eval(existingTotalPremium) + eval(optionalPremium); //eval($(planPremiumFld).value) + 
	}
	else{
		premiumWithBenefit = eval($(planId + '_PlanPremium').innerHTML) - eval(optionalPremium); 
		totalPremium = eval(existingTotalPremium) - eval(optionalPremium); //eval($(planPremiumFld).value) + 
	}
	
	$(planId + '_PlanPremium').innerHTML = premiumWithBenefit.toFixed(2);
	displayTotalPremium(totalPremium.toFixed(2));
	//calculatePayout(benefitChkBox,planId,optionalPayOut);
}

function calculatePayout(benefitChkBox, planKey, optionalPayOut){
	optionalPayOut = typeof(optionalPayOut) == 'undefined' ? 0 : optionalPayOut;
	var payout = (benefitChkBox.checked) ?  eval($('payout_'+planKey).innerHTML) + eval(optionalPayOut) : eval($('payout_'+planKey).innerHTML) - eval(optionalPayOut);
	$('payout_'+planKey).innerHTML = payout.toFixed(2);
}

function calculateAME(currentObj, typeinId){
	var totalPremium = 0;
	var existingTotalPremium = eval($('totalpremiumamt').innerHTML).toFixed(2);	
	if(currentObj.value <= 5000){
		$amePlanId = 'amePlanId_5000';
		$amePlanCost = 'amePlanCost_5000';
		$ameDeductable = '$5000';		
	}else{
		$amePlanId = 'amePlanId_10000';
		$amePlanCost = 'amePlanCost_10000';
		$ameDeductable = '$10,000';		
	}
	
	$('discount_product_' + typeinId).setAttribute('name','discount_product_'+$($amePlanId).value);	
	$('tooltip_' + typeinId).setAttribute('onmouseover','Tip(\'AME deductible: '+$ameDeductable+'\')');	
	$('discount_product_' + typeinId).value = $($amePlanId).value;
	$('discount_product_' + typeinId+'_premium').setAttribute('name','discount_product_'+$($amePlanCost).value+'_premium');
	$('discount_product_' + typeinId+'_premium').value = $($amePlanCost).value;
		
	if($('discount_product_' + typeinId).checked){
		var newAMECost = eval($(typeinId + '_PlanPremium').innerHTML) - eval($('ameCost_' + typeinId).value);				
		if(newAMECost < 0)
			newAMECost = 0;
		newAMECost = eval(newAMECost) + eval($($amePlanCost).value);		
		$(typeinId + '_PlanPremium').innerHTML = newAMECost.toFixed(2); 
	
		if($(typeinId).checked){
			totalPremium = (eval(existingTotalPremium) - eval($('ameCost_' + typeinId).value)) + eval($($amePlanCost).value);	
			displayTotalPremium(totalPremium.toFixed(2));
		}
	}
		
	$('ameId_' + typeinId).value = $($amePlanId).value;
	$('ameCost_' + typeinId).value = $($amePlanCost).value;	
	$('displabel_' + typeinId).innerHTML = $($amePlanCost).value;				
}

function displayTotalPremium(totalPremium){
	if(totalPremium < 0)
		totalPremium = 0;
		
	$('totalpremiumamt').innerHTML = totalPremium;	  	 
	if(document.getElementById('totalpremiumamt2'))
		$('totalpremiumamt2').innerHTML = totalPremium;
	if(document.getElementById('totalpremiumamt3'))
		$('totalpremiumamt3').innerHTML = totalPremium;
	$('total_pre').value = totalPremium;	
}

function resetPlanPremium(currentElement, mmplanId){
	if(isNaN(currentElement.value)){
		alert("Please enter Numeric value");
		currentElement.focus();
	}	
	else if(currentElement.value < 0){
		alert("Premium amount should be more than 0");
		currentElement.focus();
	}
	else{
		var lifePlanCost = 0;
		var amePlanCost = 0;
		var dentalPlanCost = 0;
		var totalOptionalBenifitPremium = 0;
		
		if($('cb_major_plan_' + mmplanId) != null && $('cb_major_plan_' + mmplanId).checked == true){
		var prevMajorPremium = $('premium_major_plan_' + mmplanId).value;
		var existingTotalPremium = eval($('totalpremiumamt').innerHTML).toFixed(2);	
			totalPremium = (eval(existingTotalPremium) - eval(prevMajorPremium)) + eval(currentElement.value);
			displayTotalPremium(totalPremium.toFixed(2));
		}				
		if($('life_applicant_nonrop_' + mmplanId) != null && $('life_applicant_nonrop_' + mmplanId).checked == true)
			lifePlanCost = $('life_applicant_nonrop_' + mmplanId + '_premium').value;
		if($('discount_product_' + mmplanId) != null && $('discount_product_' + mmplanId).checked == true)
			amePlanCost = $('discount_product_' + mmplanId + '_premium').value;	
		if($('Addon_Dental_' + mmplanId) != null && $('Addon_Dental_' + mmplanId).checked == true)
			dentalPlanCost = $('Addon_Dental_' + mmplanId).value;			
				
		totalOptionalBenifitPremium = eval(currentElement.value) + eval(amePlanCost) + eval(dentalPlanCost) + eval(lifePlanCost);
		$('premium_major_plan_' + mmplanId).value = currentElement.value;		
		$('PlanPremium_' + mmplanId).innerHTML  = totalOptionalBenifitPremium.toFixed(2)
	}	
}
