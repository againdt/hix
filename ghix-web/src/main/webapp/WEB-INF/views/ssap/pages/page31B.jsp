<%@ include file="../include.jsp" %>         
<div id="appscr81B"  style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page31B.medicaidSpecificQuestions" text="Medicaid Specific Questions" />
			</h4>
		</div>
		<form id="appscr81BForm" class="form-horizontal" data-parsley-excluded=":hidden">
		<div class="header">
			<h4><spring:message code="ssap.pageHeader.additionalQuestions" text="Additional Questions" /></h4>
		</div>
			<div class="gutter10">
			<div class="control-group">
				<p>
					<spring:message code="ssap.does" text="Does" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31B.wantHelpPaying" text="want help paying for medical bills from the last 3 months" />?
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="doesWantHelpForPayingBIllP81BYes">
						<input type="radio" name="doesWantHelpForPayingBIllP81B" value="Yes" id="doesWantHelpForPayingBIllP81BYes" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#doesWantHelpForPayingBIllP81BYesErrorDiv"/><spring:message code="ssap.yes" text="Yes"/>
					</label>
					
					<label class="radio" for="doesWantHelpForPayingBIllP81BNo">
						<input type="radio" name="doesWantHelpForPayingBIllP81B" value="No" id="doesWantHelpForPayingBIllP81BNo"><spring:message code="ssap.no" text="No" />
					</label>
					<div id="doesWantHelpForPayingBIllP81BYesErrorDiv"></div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<%-- <div class="control-group">
				<p>
					<spring:message code="ssap.page31B.doAnyOfTheFollowing" text="Do any of the following people have health insurance now" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input  type="checkbox" id="peopleHealthInsurDependChild" name="peopleHealthInsur" value="Yes" onclick="showOrHide81BInfoPart2()" checked="checked"><strong class="nameOfTheChild"></strong>&nbsp;<strong class="nameOfHouseHold"></strong>
					</label>
					
					<label>
						<input type="checkbox" id="peopleHealthInsurNone" name="peopleHealthInsurNone" value="No" onclick="showOrHide81BInfo()"><spring:message code="ssap.noneOfTheAbove" text="None of the above" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends --> --%>
			<%-- <p>
				<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31B.couldGetFreeOrLowText1" text="could get free or low-cost health coverage if [he/she] has enough of a work history in the U.S. on [his/her] own or through a family member." />&nbsp;<spring:message code="ssap.page31B.couldGetFreeOrLowText2" text="We checked [FNLNS]'s work history already because you gave us" />&nbsp;<strong id="appscr81SSNHisHer"></strong>&nbsp;<spring:message code="ssap.page31B.couldGetFreeOrLowText3" text="Social Security number (SSN). We can also check the work history for [FNLNS]'s family members if you give us the SSN of" />&nbsp;<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page31B.couldGetFreeOrLowText4" text="'s parent or spouse." />
			</p> --%>
			
			<div id="showOrHide_divAt81B" class="hide">
				<div class="control-group">
					<label class="control-label" for="othercounty">
						<spring:message code="ssap.page31B.name" text="Name" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="input-medium" id="othercounty">
				        	<option><spring:message code="ssap.county.Other" text="Other" /></option>
				        	<option><spring:message code="ssap.page31B.india" text="India" /></option>
				          	<option><spring:message code="ssap.page31B.india2" text="India2" /></option>
					    </select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="appscr81BFirstName">
						<spring:message code="ssap.page11.placeholder.firstName" text="First Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" value="<spring:message code="ssap.page11.placeholder.firstName" text="First Name" />" class="input-medium" id="appscr81BFirstName" name="appscr81BFirstName" />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="appscr81BMiddleName">
						<spring:message code="ssap.page11.placeholder.firstName" text="First Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" value="<spring:message code="ssap.page11.placeholder.middleName" text="Middle Name" />" class="input-medium" id="appscr81BMiddleName" name="appscr81BMiddleName" />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="appscr81BLastName">
						<spring:message code="ssap.lastName" text="Last Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" value="<spring:message code="ssap.lastName" text="Last Name" />" class="input-medium" id="appscr81BLastName" name="appscr81BLastName" />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="suffix2">
						<spring:message code="ssap.suffix" text="Suffix" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="select_row_options" id="suffix2">
			       			<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
							<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
							<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
							<option value="III"><spring:message code="ssap.III" text="III" /></option>
							<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
				    	</select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="appscr81BDOB">
						<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" value="MM/DD/YYYY" class="input-medium" id="appscr81BDOB" name="appscr81BDOB"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="appscr81BSSN1">
						<spring:message code="ssap.page31B.ssn" text="SSN" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" value="xxxx" class="input-mini" id="appscr81BSSN1" name="appscr81BSSN1"/>
						<label class="control-label" for="appscr81BSSN2">
						<spring:message code="ssap.page31B.ssn" text="SSN" />
					</label><!-- end of label -->
						<input type="text" value="xxxx" class="input-mini" id="appscr81BSSN2" name="appscr81BSSN2"/>
						<label class="control-label" for="appscr81BSSN3">
						<spring:message code="ssap.page31B.ssn" text="SSN" />
					</label><!-- end of label -->
						<input type="text" value="xxx" class="input-mini" id="appscr81BSSN3" name="appscr81BSSN3"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="relationship2">
						<spring:message code="ssap.page31B.relationship" text="Relationship" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="input-medium" id="relationship2">
							<option><spring:message code="ssap.page31B.relationship" text="Relationship" /></option>
							<option><spring:message code="ssap.page31B.parent" text="Parent" /></option>
							<option><spring:message code="ssap.page31B.spouse" text="Spouse" /></option>
						</select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page31B.doesHaveParentLiving" text="Does [Child name] have a parent living outside the home" />?
					</p><!-- end of label -->
					<div class="margin5-l">
						<label class="radio" for="male_0">
							<input type="radio" name="doesChildHaveParentLivingOutside" value="Yes" id="male_0" checked="checked"><spring:message code="ssap.yes" text="Yes" />
						</label>
						
						<label class="radio" for="male_1">
							<input type="radio" name="doesChildHaveParentLivingOutside" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page31B.howManyHoursPer" text="How many hours per week do [Child's name]'s parents work" />?
					</p><!-- end of label -->
					<div class="control-group">
						<label class="control-label" for="appscr81Parent1">
							<spring:message code="ssap.page31B.parent1" text="Parent 1" />
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" value="#" class="input-medium" id="appscr81Parent1" name="appscr81Parent1"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					<div class="control-group">
						<label class="control-label" for="appscr81Parent2">
							<spring:message code="ssap.page31B.parent2" text="Parent 2" />
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" value="#" class="input-medium" id="appscr81Parent2" name="appscr81Parent2"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				</div><!-- control-group ends -->
				
				
			</div><!-- showOrHide_divAt81B ends -->
			</div><!-- .gutter10 -->
		</form>
	</div>
</div>