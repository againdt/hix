package com.getinsured.eligibility.ssap.model;

/**
 * Eligibility response status.
 *
 * @author Yevgen Golubenko
 * @since 5/8/19
 */
public enum ResponseStatus
{
  /**
   * Eligibility response had a successful status.
   */
  success,

  /**
   * Eligibility response had a failure status, {@code errors}
   * needs to be check for exact reason.
   */
  failure
}
