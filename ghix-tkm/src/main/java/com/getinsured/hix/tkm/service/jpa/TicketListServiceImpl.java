package com.getinsured.hix.tkm.service.jpa;

import java.io.FileWriter;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.dto.tkm.ReportDto.CATEGORIES;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmQueueUsersRepository;
import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.tkmException.TKMException;
import com.getinsured.tkm.enums.TicketStatus;

import au.com.bytecode.opencsv.CSVWriter;

@Service("TicketListService")
public class TicketListServiceImpl implements TicketListService
{
	private static final String DUE_DATE = "Due Date";

	private static final String CREATED_DATE = "Created Date";

	private static final String BODY_OF_TICKET = "Body of ticket";

	private static final String SUBJECT = "Subject";

	private static final String REQUESTER = "Requester";

	private static final String TICKET_TYPE = "Ticket type";

	private static final String SORT_BY_TKM_QUEUES = "tkmQueues";

	@Autowired 
	private ITicketRepository iTicketRepository;
	
	@Autowired
	private ITkmQueueUsersRepository iTkmQueueUsersRepository;
	
	private static final Logger LOGGER = Logger.getLogger(TicketListServiceImpl.class);
	
	private static final String SEARCH_CRITERIA_TICKET_STATUS = "status";
	
	private static final String SEARCH_CRITERIA_TICKET_SUBJECT = "ticketSubject";
	
	private static final String TASKSTATUS = "taskStatus";
	
	private static final String TASKROLE = "role";
	
	private static final String SEARCH_CRITERIA_TICKET_QUEUES = "ticketQueues";
	
	private static final String ANY = "any";
	
	private static final String UNCHECKED ="unchecked";
	
	private static final String PARAM_TYPE ="type";
	
	private static final String CREATION_DATE="created";
	
	private static final int TYPE = 0;
	private static final int IN_1_DAY = 1;
	private static final int IN_1_3_DAY = 2;
	private static final int IN_3_4_DAY = 3;
	private static final int IN_1_3_WEEK = 4;
	private static final int OUT_OF_3_WEEK = 5;
	private static final int ARRAY_INITIAL_SIZE=12;
	private static final String SUBMITTED_BEFORE = "submittedBefore";
	private static final String SUBMITTED_AFTER = "submittedAfter";
	
	private static final int ONE_TO_THREE_DAYS_OLD_START = -1;
	private static final int ONE_TO_THREE_DAYS_OLD_END = -2;
	
	private static final int THREE_TO_SEVEN_DAYS_OLD_START = -4;
	private static final int THREE_TO_SEVEN_DAYS_OLD_END = -3;
	
	private static final int ONE_WEEK_TO_THREE_WEEKS_OLD_START = -8;
	private static final int ONE_WEEK_TO_THREE_WEEKS_OLD_END = -13;
	
	private static final int MORETHAN_THREE_WEEKS_START = -22;
	
	private static final int FIVE_COLUMNS = 5;
	
	private static final String USER_ID ="userId";
	
	
	@Value("#{configProp.outputpath}")
	private String outputPath;
	
	@Value("#{configProp['database.type']}")
	private String DB_TYPE;
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private RoleService roleService;
	
	@Override
	public Map<String, Object> searchTicketsFromTicketAndTask(Map<String, Object> searchCriteria)  {
		Map<String, Object> mapTicketRecCount = new HashMap<String, Object>();
		Map<String, Object> mapQueryParam = new HashMap<>();
			
		
		String selectClause = "SELECT distinct (t.id), t.subject, t.number, t.role.firstName, t.role.lastName, t.created, t.tkmQueues.name, t.priority, t.status, tt.status,t.dueDate,t.isArchived,t.archivedBy ";
		String selectCountClause = "SELECT count(*) ";
		String fromClause = "FROM TkmTickets as t left join t.tkmtickettasks as tt ";
		String whereClause = "  WHERE t.id=tt.ticket.id "
							+ " and tt.taskId in (select max(tt.taskId)from tt ,t" 
							+ " where tt.ticket.id=t.id "
							+ " group by t.id) ";
				
		StringBuilder query = new StringBuilder(selectClause);
		query.append(fromClause);
		query.append(whereClause);
		
		// apply the filters and create parameter map
		applyGeneralFilters(query, searchCriteria, mapQueryParam);
		// apply the filters for super or non-super users
		Map<String, Object> mapRoleFilter = applyRoleFilters(query, searchCriteria);
		if(!mapRoleFilter.isEmpty()) {
			mapQueryParam.putAll(mapRoleFilter);
		}
		
		// apply sort filters
		addSortFilters(query, searchCriteria);
		
		LOGGER.info("Search TkmTicketTask Query: " + query.toString());
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			// Set the paging criteria		
			filterByPaging(hqlQuery, (Integer) searchCriteria.get("startRecord"), (Integer) searchCriteria.get("pageSize"));
			
			@SuppressWarnings("unchecked")
			List<Object> resultList = hqlQuery.getResultList();
			for (Object row : resultList) {
				Object[] rowArray = (Object[]) row;
				if (rowArray[ARRAY_INITIAL_SIZE] == null) {
					rowArray[ARRAY_INITIAL_SIZE] = StringUtils.EMPTY;
				}
				if(rowArray[7]!=null){
					rowArray[7] = TicketStatus.valueOf((String)rowArray[7]).getDescription() ;
				}
			}
			 
			mapTicketRecCount.put("ticketList", resultList);
			
			query.replace(query.indexOf(selectClause), selectClause.length(), selectCountClause);
			Query hqlCountQuery = em.createQuery(query.toString());
			setNamedQueryParams(hqlCountQuery, mapQueryParam);		
			mapTicketRecCount.put("recordCount", hqlCountQuery.getSingleResult());
			
			updateDateParameterBasedOnPeriodSearch(searchCriteria, mapTicketRecCount);
		} catch (Exception e) {
			LOGGER.error("Unable to search tickets", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return mapTicketRecCount;
	}
	
	/**
	 * @param hqlQuery
	 * @param mapQueryParam
	 * This method will add the values in the queries as parameters and will avoid SQL Injection.
	 */
	private void setNamedQueryParams(Query hqlQuery, Map<String, Object> mapQueryParam) {
		for(String param : mapQueryParam.keySet()){
			hqlQuery.setParameter(param, mapQueryParam.get(param));
		}
	}


	/**
	 * @param query
	 * @param searchCriteria
	 * This method will add the sorting criteria on the ticket list query.
	 */
	private void addSortFilters(StringBuilder query, Map<String, Object> searchCriteria) {		
		String columnName = searchCriteria.get("sortBy").toString();				
		String sortOrder = searchCriteria.get("sortOrder").toString();
		
		sortOrder = (("DESC").compareToIgnoreCase(sortOrder) == 0) ? "DESC" : "ASC";
		
		List<String> sortByCols = Arrays.asList("t.id", "t.subject", "t.number", "t.role.firstName","t.role.lastName",  "t.created", 
		                            "t.tkmQueues.name", "t.priorityOrder", "t.status", "tt.status", "t.dueDate"); 
		String sortBy = "t.created";
		if(!StringUtils.isEmpty(columnName)) {
			if((TASKSTATUS).equalsIgnoreCase(columnName)) {
				sortBy = "tt.status";
			} else if((TASKROLE).equalsIgnoreCase(columnName)){
				sortBy = "t.role.firstName";
			} else if((SORT_BY_TKM_QUEUES).equalsIgnoreCase(columnName)){
				sortBy = "t.tkmQueues.name";
			} else {
				sortBy = "t." + columnName;
			}
		}
		
		sortBy = sortByCols.contains(sortBy) ? sortBy : sortByCols.get(FIVE_COLUMNS);
		query.append("ORDER BY ").append(sortBy).append(" ").append(sortOrder);
	}

	
	@Override
	public void generateCsvForTickets(){
		FileWriter fw = null;
		try {
			List<TkmTickets> ticktes = iTicketRepository.getUnprocessedTkmTickets();
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
	
			
		
			//SimpleDateFormat sdfForFileName = new SimpleDateFormat("MM-dd-yyyy");
			String csv = outputPath+"/TICKET_REPORT_"+sdf.format(new TSDate()).replaceAll(":", "-")+".csv";
			fw = new FileWriter(csv);
			CSVWriter writer = new CSVWriter(fw);
			List<String[]> data = new ArrayList<String[]>();
			data.add(new String[] {TICKET_TYPE, REQUESTER, SUBJECT, BODY_OF_TICKET, CREATED_DATE, DUE_DATE});
			List<Integer> ticketIds= new ArrayList<>();
			if(!ticktes.isEmpty()){
				
				for(TkmTickets ticket :  ticktes){
					data.add(new String[] {ticket.getTkmWorkflows().getType(), ticket.getRole().getFirstName()+" "+ticket.getRole().getLastName(),ticket.getSubject(), ticket.getDescription(), sdf.format(ticket.getCreated()), ticket.getDueDate() != null ? sdf.format(ticket.getDueDate()): ""});
					ticketIds.add(ticket.getId());
				}				
				writer.writeAll(data);
				 
				writer.close();
				
				if(!ticketIds.isEmpty()){
				iTicketRepository.updateExtractedTicketBatchFlag(ticketIds);
				}				
			}
		} catch (Exception e) {
			LOGGER.error("Unable to generate tickets report", e);
		}	finally{
			IOUtils.closeQuietly(fw);
		}
	}
	
	/**
	 * @param query
	 * @param searchCriteria
	 * @return
	 * This method will apply the filters for Super-Admin and other admins.
	 */
	private Map<String, Object> applyRoleFilters(StringBuilder query, Map<String, Object> searchCriteria) {
		Map<String, Object> mapQueryParam = null;
		
		// filter Assignee id, task status and queue user id
		Integer userId = (Integer) searchCriteria.get(USER_ID);
		String taskStatus = (String) searchCriteria.get(TASKSTATUS);
		@SuppressWarnings("unchecked")
		List<Integer> queueIdList = (List<Integer>)searchCriteria.get("userQueueId");	
		queueIdList = (queueIdList != null) ? queueIdList : new ArrayList<Integer>();
		if(userId != null) {
			// user id will be available only for non-super user
			// assignee will be the logged in user	
			mapQueryParam = filterForNonSuperUser(query, taskStatus, userId, queueIdList);
		} else { 
			// Super user should have access to all ticket filtered by assignee id if available
			mapQueryParam = filterByTaskStatusForSuperUser(query, taskStatus);
			Map<String, Object> mapAssigneeFilter = filterByAssigneeIdForSuperUser(query, (String) searchCriteria.get("assigneeId"));
			
			mapQueryParam.putAll(mapAssigneeFilter);
		}
		
		return mapQueryParam;
	}

	/**
	 * @param query
	 * @param searchCriteria
	 * @param mapQueryParam
	 * This will apply the generic filters on the query created for ticket search.
	 */
	private void applyGeneralFilters(StringBuilder query, Map<String, Object> searchCriteria, Map<String, Object> mapQueryParam) {
		// filter by subject
		String ticketSubject = (String) searchCriteria.get(SEARCH_CRITERIA_TICKET_SUBJECT);
		if (StringUtils.isNotEmpty(ticketSubject) && !ticketSubject.equalsIgnoreCase(ANY)) {
			query.append("AND t.subject like :").append(SEARCH_CRITERIA_TICKET_SUBJECT).append(" ");
			mapQueryParam.put(SEARCH_CRITERIA_TICKET_SUBJECT, "%" + ticketSubject + "%");
		}
		
		// filter by requested by
		String requestedBy = (String) searchCriteria.get("requestedBy");
		if(!StringUtils.isEmpty(requestedBy) && ((ANY).compareToIgnoreCase(requestedBy) !=0)) {
			query.append("AND t.role.id=:requestedBy ");
			mapQueryParam.put("requestedBy", Integer.parseInt(requestedBy));
		}
		
		// filter by Queue
		@SuppressWarnings("unchecked")
		List<String>  queueList = (List<String>)  searchCriteria.get(SEARCH_CRITERIA_TICKET_QUEUES);
		if (queueList != null && !queueList.isEmpty() && !queueList.contains(ANY)) {
			query.append("AND t.tkmQueues.name in :").append(SEARCH_CRITERIA_TICKET_QUEUES).append(" ");
			mapQueryParam.put(SEARCH_CRITERIA_TICKET_QUEUES, queueList);
		}
		
		String userRole = (String) searchCriteria.get("userRole");
		
		
		
		
		// filter by Created For
		String rolename=null;
		String moduleId = (String) searchCriteria.get("moduleId");
		if(!StringUtils.isEmpty(moduleId) && ((ANY).compareToIgnoreCase(moduleId) !=0)) {
			if(!StringUtils.isEmpty(userRole)) {
				Role role = roleService.findById(Integer.parseInt(userRole));
				 rolename=role.getName();
			}
			
			if("INDIVIDUAL".equalsIgnoreCase(rolename))
			{	
			query.append("AND t.moduleId=:moduleId ");
			}
			else
			{
			 query.append("AND t.role.id=:moduleId ");	
			}
			
			mapQueryParam.put("moduleId", Integer.parseInt(moduleId));
		}
		
		
		
		// filter by priority
		String ticketPriority = (String) searchCriteria.get("ticketPriority");
		if (StringUtils.isNotEmpty(ticketPriority) && !ticketPriority.equalsIgnoreCase(ANY)) {
			query.append("AND t.priority like :").append("ticketPriority").append(" ");
			mapQueryParam.put("ticketPriority","%" + ticketPriority+ "%");
		}
		
		// filter by ticket number
		String ticketNumber = (String) searchCriteria.get("ticketNumber");
		if (StringUtils.isNotEmpty(ticketNumber) && !ticketNumber.equalsIgnoreCase(ANY)) {
			query.append("AND t.number like :").append("ticketNumber").append(" ");
			mapQueryParam.put("ticketNumber", "%" + ticketNumber + "%");
		}
		
		// filter by ticket status
		@SuppressWarnings("unchecked")
		List<String> ticketStatus = (List<String>) searchCriteria.get("ticketStatus");
		if (ticketStatus !=null && !ticketStatus.isEmpty() && !ticketStatus.contains("Any")) {
            query.append("AND t.status in :").append("ticketStatus").append(" ");
            mapQueryParam.put("ticketStatus", ticketStatus);
		}
		
		// Add period filter : needed for ageing report drill down 
		if (searchCriteria.get("period") != null ){
			filterByPeriod(searchCriteria, (String)searchCriteria.get("period"));
		}
		
		String archiveFlagValue = (String) searchCriteria.get("archiveFlagValue");
		boolean isNonSuperUser = Boolean.parseBoolean((searchCriteria.get("isNonSuperUser")).toString());
	
		if (isNonSuperUser){
			query.append("AND t.isArchived =:").append("archiveFlagValue").append(" ");
			if(archiveFlagValue.equalsIgnoreCase("Y"))
			{	
			query.append("AND t.archivedBy =:").append("userId").append(" ");
			mapQueryParam.put("userId",searchCriteria.get("userId").toString());
			}
			mapQueryParam.put("archiveFlagValue",archiveFlagValue);
			
		}else{
			query.append("AND t.isArchived =:").append("archiveFlagValue").append(" ");
			mapQueryParam.put("archiveFlagValue",archiveFlagValue);
		}
		// filter by date (start date, end date)
		String endDate = (String) searchCriteria.get("submittedBefore");
		if(StringUtils.isNotEmpty(endDate)) {
			query.append("AND t.created <=").append(":submittedBefore").append(" ");
			Date submittedBeforeDate = prepareDateCriteria(endDate, true);
			submittedBeforeDate = (submittedBeforeDate != null) ? submittedBeforeDate : new TSDate();
			mapQueryParam.put("submittedBefore", submittedBeforeDate);
		}
		String createdDate = (String) searchCriteria.get("submittedAfter");
		if(StringUtils.isNotEmpty(createdDate)) {
			query.append("AND t.created >=").append(":submittedAfter").append(" ");
			Date submittedAfterDate = prepareDateCriteria(createdDate, false);
			submittedAfterDate = (submittedAfterDate != null) ? submittedAfterDate : new TSDate();
			mapQueryParam.put("submittedAfter", submittedAfterDate);
		}
	}

	private void updateDateParameterBasedOnPeriodSearch(
			Map<String, Object> searchCriteria,
			Map<String, Object> mapTicketRecCount) {
		if (searchCriteria.get("period") != null ){
			if(searchCriteria.get(SUBMITTED_BEFORE) != null){
				mapTicketRecCount.put(SUBMITTED_BEFORE , searchCriteria.get(SUBMITTED_BEFORE));
			}
			if(searchCriteria.get(SUBMITTED_AFTER) != null ){
				mapTicketRecCount.put(SUBMITTED_AFTER , searchCriteria.get(SUBMITTED_AFTER));
			}
		}
	}
	
	
	private Timestamp getSystemTimeStampFromDB() {
		return  (Timestamp)emf.createEntityManager().createNativeQuery("select now()").getSingleResult();
		
	}
	
	
	private void filterByPeriod(Map<String, Object> searchCriteria, String period) {

		CATEGORIES category = ReportDto.CATEGORIES.getValue(period);
		if (category != null) {
			
			Calendar cal = TSCalendar.getInstance();
			
			cal.setTimeInMillis(getSystemTimeStampFromDB().getTime());
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			switch (category) {
			case LESSTHAN_ONE_DAY_OLD:
				String date = dateFormat.format(cal.getTime());
				searchCriteria.put(SUBMITTED_BEFORE, date);
				searchCriteria.put(SUBMITTED_AFTER, date);
				break;
			case ONE_TO_THREE_DAYS_OLD:
				cal.add(Calendar.DATE, ONE_TO_THREE_DAYS_OLD_START);
				searchCriteria.put(SUBMITTED_BEFORE, dateFormat.format(cal.getTime()));
				cal.add(Calendar.DATE, ONE_TO_THREE_DAYS_OLD_END);
				searchCriteria.put(SUBMITTED_AFTER, dateFormat.format(cal.getTime()));
				break;
			case THREE_TO_SEVEN_DAYS_OLD:
				cal.add(Calendar.DATE, THREE_TO_SEVEN_DAYS_OLD_START);
				searchCriteria.put(SUBMITTED_BEFORE, dateFormat.format(cal.getTime()));
				cal.add(Calendar.DATE, THREE_TO_SEVEN_DAYS_OLD_END);
				searchCriteria.put(SUBMITTED_AFTER, dateFormat.format(cal.getTime()));
				break;
			case ONE_WEEK_TO_THREE_WEEKS_OLD:
				cal.add(Calendar.DATE, ONE_WEEK_TO_THREE_WEEKS_OLD_START);
				searchCriteria.put(SUBMITTED_BEFORE, dateFormat.format(cal.getTime()));
				cal.add(Calendar.DATE, ONE_WEEK_TO_THREE_WEEKS_OLD_END);
				searchCriteria.put(SUBMITTED_AFTER, dateFormat.format(cal.getTime()));
				break;
			case MORETHAN_THREE_WEEKS:
				cal.add(Calendar.DATE, MORETHAN_THREE_WEEKS_START);
				searchCriteria.put(SUBMITTED_BEFORE, dateFormat.format(cal.getTime()));
				break;
			}
		}
	
	}

	@Override
	public List<ReportDto> getAgeingReport(Map<String, Object> reportCriteria) throws TKMException{
		List<ReportDto> reportDtoList = new ArrayList<ReportDto>();
		if(reportCriteria.isEmpty() ){
			reportCriteria.put(PARAM_TYPE, "priority");
		}
		if(reportCriteria.get(PARAM_TYPE).equals("TYPE")){
			reportCriteria.put(PARAM_TYPE, "tkmWorkflows.type");
		} 
		StringBuilder query = new StringBuilder("SELECT ");
		query.append(reportCriteria.get(PARAM_TYPE));

		query.append(", sum((case when (cast(now() AS date) - cast(created AS date)) < 1 then 1 else 0 end)) AS IN_1_DAY, ");
		query.append("sum((case when (cast(now() AS date) - cast(created AS date)) BETWEEN 1 AND 3 then 1 else 0 end)) AS IN_1_3_DAY, ");
		query.append("sum((case when (cast(now() AS date) - cast(created AS date)) BETWEEN 4 AND 7 then 1 else 0 end)) AS IN_3_4_DAY, ");
		query.append("sum((case when (cast(now() AS date) - cast(created AS date)) BETWEEN 8 AND 21 then 1 else 0 end)) AS IN_1_3_WEEK, ");
		query.append("sum((case when (cast(now() AS date) - cast(created AS date)) > 21 then 1 else 0 end)) AS OUT_OF_3_WEEK ");
		query.append(" FROM TkmTickets tkt where EXISTS ( FROM TkmTicketTasks tt WHERE tt.ticket.id = tkt.id) ");
		query.append(" AND isArchived = 'N' ");
		query.append(" AND status in ('Open', 'UnClaimed')  GROUP BY ");

		query.append(reportCriteria.get(PARAM_TYPE));
		
		
		LOGGER.info("Report  Query: " + query.toString());
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());

					
			@SuppressWarnings("unchecked")
			List<Object> ageingCountList = hqlQuery.getResultList();
			for(Object ageingCount : ageingCountList){
				Object[] objArrayReportDto = (Object[]) ageingCount;
				ReportDto reportDto = new ReportDto();
				reportDto.setName(objArrayReportDto[TYPE].toString());
				List<Integer> data = new ArrayList<Integer>();
				data.add(Integer.parseInt(objArrayReportDto[IN_1_DAY].toString()));
				data.add(Integer.parseInt(objArrayReportDto[IN_1_3_DAY].toString()));
				data.add(Integer.parseInt(objArrayReportDto[IN_3_4_DAY].toString()));
				data.add(Integer.parseInt(objArrayReportDto[IN_1_3_WEEK].toString()));
				data.add(Integer.parseInt(objArrayReportDto[OUT_OF_3_WEEK].toString()));
				reportDto.setData(data);
				reportDtoList.add(reportDto);
			}
			
			
			
		} catch (Exception e) {
			LOGGER.error("Unable to generate tickets report", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}

		return reportDtoList;

	}

	private void filterByPaging(Query hqlQuery, Integer startRecord, Integer pageSize) {
		if (pageSize != -1) {
			hqlQuery.setFirstResult((startRecord !=null) ? startRecord : 0);
			hqlQuery.setMaxResults(pageSize);
		}		
	}
		 
	/**
	 * @author hardas_d
	 * @param query
	 * @param taskStatus
	 * @param userId
	 * @param queueIdList
	 * @return  Non-super user should view only tickets of which tasks are directly assigned to them 
	 *	 	 	or tasks for unclaimed tickets assigned to queue
	 */
	private Map<String, Object> filterForNonSuperUser(StringBuilder query, String taskStatus, Integer userId, List<Integer> queueIdList) {
		Map<String, Object> mapQueryParam = new HashMap<>();
		
		if(StringUtils.isEmpty(taskStatus) || taskStatus.equalsIgnoreCase(ANY)) {
			//Query: Get tickets which are directly assigned and unclaimed from queue list
			query.append("AND (tt.assignee.id=:").append(USER_ID).append(" "); // tickets directly assigned to the user
			mapQueryParam.put(USER_ID, userId);
			
			if(queueIdList !=null && !queueIdList.isEmpty()) {  // Unclaimed tickets from queue list
				query.append(" OR ");
				query.append("( tt.queue.id IN ").append(":queueIdList");
				query.append(" AND tt.status=:").append(TASKSTATUS).append(") ");
				mapQueryParam.put("queueIdList", queueIdList);
				mapQueryParam.put(TASKSTATUS, TkmTicketTasks.task_status.UnClaimed.toString());
			}
			query.append(" ) ");
		} else if(StringUtils.isNotEmpty(taskStatus) 
			&& taskStatus.equalsIgnoreCase(TkmTicketTasks.task_status.UnClaimed.toString())) {
			//Query: Get only unclaimed tickets from queue list
			if(queueIdList ==null || queueIdList.isEmpty()) {  // Unclaimed tickets from queue list
				queueIdList =  new ArrayList<>();
				queueIdList.add(0); // dummy queue id to resolve HIX-53562
			}
				query.append("AND (tt.queue.id in(:queueIdList)");
				query.append("AND tt.status=:").append(TASKSTATUS).append(") ");
				mapQueryParam.put("queueIdList", queueIdList);
				mapQueryParam.put(TASKSTATUS, TkmTicketTasks.task_status.UnClaimed.toString());
					
		} else {
			//Query: Get only assigned tickets with specified task status
			query.append("AND tt.assignee.id=:").append(USER_ID).append(" ");// tickets directly assigned to the user
			query.append("AND tt.status=:").append(TASKSTATUS).append(" ");
			mapQueryParam.put(USER_ID, userId);
			mapQueryParam.put(TASKSTATUS, taskStatus);
		}	
		
		return mapQueryParam;
	}

	/**
	 * @author hardas_d
	 * @param query
	 * @param assigneeId
	 */
	private Map<String, Object> filterByAssigneeIdForSuperUser(StringBuilder query, String assigneeId) {
		Map<String, Object> mapQueryParam = new HashMap<>();
		if(!StringUtils.isEmpty(assigneeId) && ((ANY).compareToIgnoreCase(assigneeId) !=0)) {
			query.append("AND tt.assignee.id=:").append("assigneeId").append(" ");
			mapQueryParam.put("assigneeId", Integer.parseInt(assigneeId));
		}	
		return mapQueryParam;
	}


	/**
	 * @author hardas_d
	 * @param query
	 * @param taskStatus
	 */
	private Map<String, Object> filterByTaskStatusForSuperUser(StringBuilder query, String taskStatus) {
		Map<String, Object> mapQueryParam =  new HashMap<>();
		if (StringUtils.isNotEmpty(taskStatus) && !taskStatus.equalsIgnoreCase(ANY)) {
			query.append("AND tt.status=:").append(TASKSTATUS).append(" ");
			mapQueryParam.put(TASKSTATUS, taskStatus);
		}
		return mapQueryParam;
	}

	private Date prepareDateCriteria(String submittedDateString, boolean maxtime) {
		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		if(StringUtils.isEmpty(submittedDateString)) {
			return effectiveEndDate;
		}
		
		try {
			effectiveEndDate = formatter.parse(submittedDateString);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			return effectiveEndDate;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		if(maxtime){
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));
		}else{
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMinimum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMinimum(Calendar.SECOND));
		}
		//query.applyWhere("created", calendarDate.getTime(), DataType.DATE, ComparisonType.LE);
		return calendarDate.getTime();
	}
	
	@Override	
	@Transactional(readOnly=true)
	public
	TkmTickets findTkmTicketsById(Integer ticketId, boolean statusDisplayFlag) 
	{
		TkmTickets tkmTickets = null;

		if(iTicketRepository.exists(ticketId))
		{
			tkmTickets = iTicketRepository.findOne(ticketId);
			if(statusDisplayFlag){
				tkmTickets.setStatus(TicketStatus.valueOf(tkmTickets.getStatus()).getDescription());
			}
		}
		
		return tkmTickets;
	}
	
	@Override	
	@Transactional(readOnly=true)
	public TkmTickets findTkmTicketsById(int id) 
	{
		TkmTickets tkmTickets = null;

		if(iTicketRepository.exists(id))
		{
			tkmTickets = iTicketRepository.findOne(id);
		}
	
		return tkmTickets;
	}
	
	@Override
	public List<String> getDistinctSubject(Integer id) 
	{
		List<String> subjectList = id==null?iTicketRepository.getDistinctSubject():iTicketRepository.getTkmTicketSubjectListByGroupIdAndAssigneeId(iTkmQueueUsersRepository.findGroupsByUserId(id), id);
		
		return subjectList;
	}
	
	@Override
	public TkmTickets addNewTicket(TkmTickets tkmTickets) {
		TkmTickets ticket = iTicketRepository.save(tkmTickets);
		
		return ticket;
	}

	@Override
	public TkmTickets updateTicket(String ticNumber, int tickId)
	{
		TkmTickets tkmTickets = iTicketRepository.findOne(tickId);
		if(tkmTickets != null) {
			tkmTickets.setNumber(ticNumber);
			tkmTickets = iTicketRepository.save(tkmTickets);
		}
		
		return tkmTickets; 		
	}

	@Override
	public TkmTickets saveComments(int ticketId, String comment) 
	{
		TkmTickets savedTicket = null;
		TkmTickets tkmTickets = iTicketRepository.findOne(ticketId);
		
		/*if(tkmTickets != null)*/ 
			tkmTickets.setClosingComment(comment);
			tkmTickets.setStatus(TkmTickets.ticket_status.Resolved.toString());
			savedTicket = iTicketRepository.save(tkmTickets);
		
		return savedTicket;
	}
	
	@Override
	public List<TkmTickets> getTicketByUser(List<Integer> queueId,Integer assigneeId) 
	{   List<TkmTickets> ticketByUser = null;
		List<Integer> queueList = ((queueId != null) && (!queueId.isEmpty())) ? queueId : null; 

		ticketByUser =  iTicketRepository.getTkmTicketListByGroupIdAndAssigneeId(queueList, assigneeId);
	
		return ticketByUser;
	}
	
	@Override
	public TkmTickets updateStatus(Integer ticketId, String status, Integer userId, String comments) {
		TkmTickets savedTicket = null;

		TkmTickets tkmTickets = iTicketRepository.findOne(ticketId);
		if(tkmTickets != null) {
			tkmTickets.setStatus(TkmTickets.ticket_status.valueOf(status).toString());
			
			if(!StringUtils.isEmpty(comments)) {
				tkmTickets.setClosingComment(comments);
			}
			
			AccountUser user = new AccountUser();
			user.setId(userId);
			tkmTickets.setUpdator(user);
			
			savedTicket = iTicketRepository.save(tkmTickets);
		}
	
		return savedTicket;
	}
	
	@Override
	public TkmTickets saveEditedTicket(TkmTickets tkmTickets) {
			
		if(tkmTickets == null || !iTicketRepository.exists(tkmTickets.getId())) {
			return null;
		}
	
		TkmTickets saveEditedTicket = iTicketRepository.save(tkmTickets);
	
	return saveEditedTicket;
		
	}
	
	@Override
	public List<TkmTickets> searchTicketsByCriteria(TkmTicketsRequest ticketRequest) {
		List<TkmTickets> ticketList  = null;
		
		if(ticketRequest == null) {
			return ticketList;
		}	
		
		@SuppressWarnings(UNCHECKED)
		QueryBuilder<TkmTickets> query = delegateFactory.getObject();
		query.buildObjectQuery(TkmTickets.class);
		
		//Requester Role and user id filter
		filterByRoleUserId(query, ticketRequest.getUserRoleId(), ticketRequest.getRequester());
		
		//ticket type and category filter
		filterByWorkflow(query, ticketRequest.getType(), ticketRequest.getCategory());
		
		//ticket status filter
		filterByStatus(query, ticketRequest.getTicketStatus());
		
		filterByDate(query, ticketRequest.getSubmittedBefore(), ticketRequest.getSubmittedAfter());
		//module id filter
		filterByModuleId(query, ticketRequest.getModuleId());
		
		addSortCriteria(query, ticketRequest);
		
		Integer startRecord = ticketRequest.getStartRecord();
		if(startRecord == null || startRecord <= 0) {
			startRecord = 1;
		}
		
		Integer pageSize = ticketRequest.getPageSize();
		if(pageSize == null || pageSize <= 0) {
			pageSize = -1;
		}
		
		//Execute the query

			query.setFetchDistinct(true);
			ticketList = query.getRecords(startRecord, pageSize);
		
		return ticketList;
	}
	
	/**
	 * @author hardas_d
	 * @return void
	 * @param query
	 * @param ticketRequest
	 */
	private void filterByModuleId(QueryBuilder<TkmTickets> query, Integer moduleId) {
		if(moduleId != null) {
			query.applyWhere("moduleId", moduleId, DataType.NUMERIC, ComparisonType.EQ);
		}
	}

	private void filterByStatus(QueryBuilder<TkmTickets> query,	String tickStatus) {
		if(!StringUtils.isEmpty(tickStatus)) {
			TkmTickets.ticket_status status = TkmTickets.ticket_status.valueOf(tickStatus);
			if(status != null) {
				query.applyWhere(SEARCH_CRITERIA_TICKET_STATUS, status.toString(), DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
			}
		}
	}
	
	private void filterByDate(QueryBuilder<TkmTickets> query, Date submittedBefore, Date submittedAfter) {
		if(submittedBefore != null) {
			//Date submittedBeforeDate = prepareDateCriteria(submittedBefore.toString(), true);
			query.applyWhere(CREATION_DATE, submittedBefore,DataType.DATE, ComparisonType.LE);
			
		}
		if(submittedAfter != null) {
			//Date submittedAfterDate = prepareDateCriteria(submittedAfter.toString(), false);
			query.applyWhere(CREATION_DATE, submittedAfter, DataType.DATE, ComparisonType.GE);
			
		}
	}

	/**
	 * 
	 * @param query
	 * @param ticketRequest
	 * This method will add the sorting criteria 
	 * Default: Sort the list as per the creation date in DESC order
	 */
	private void addSortCriteria(QueryBuilder<TkmTickets> query, TkmTicketsRequest ticketRequest) {
		String sortBy = ticketRequest.getSortBy();
		String sortOrder = ticketRequest.getSortOrder();
		
		if(!StringUtils.isEmpty(sortBy) && !StringUtils.isEmpty(sortOrder)) {
			query.applySort(sortBy,	SortOrder.valueOf(sortOrder));
		} else {
			//Apply sort to get the list of tickets as per the creation date in DESC order
			query.applySort("created", SortOrder.DESC);
		}		
	}

	/**
	 * 
	 * @param query
	 * @param ticketRequest
	 * Adds a filter for workflow category and type
	 */
	private void filterByWorkflow(QueryBuilder<TkmTickets> query, String type, String category) {
		if(!StringUtils.isEmpty(type)) {
			query.applyWhere("tkmWorkflows.type", type, DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
		}
		
		if(!StringUtils.isEmpty(category)) {
			query.applyWhere("tkmWorkflows.category", category, DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
		}
		
	}

	/**
	 * 
	 * @param query
	 * @param ticketRequest
	 * Adds a filter for user id and role id
	 */
	private void filterByRoleUserId(QueryBuilder<TkmTickets> query, Integer userRoleId, Integer roleId) {
		if(userRoleId != null) {
			query.applyWhere("userRole.id", userRoleId, DataType.NUMERIC, ComparisonType.EQ);
		}
		
		if(roleId != null) {
			query.applyWhere("role.id", roleId, DataType.NUMERIC, ComparisonType.EQ);
		}
	}
	
	
	@Override
	public boolean setUpdator(Integer ticketId, Integer updatorId) {
		boolean bStatus = false; 
		
		TkmTickets tkmTickets = iTicketRepository.findOne(ticketId);
		if(tkmTickets != null) {
			AccountUser user = new AccountUser();
			user.setId(updatorId);
			tkmTickets.setUpdator(user);

				TkmTickets savedTicket = iTicketRepository.save(tkmTickets);
				bStatus = (savedTicket != null) ? true : bStatus;
						
		}
		
		return bStatus;
	}
	
	/**
	 * @author hardas_d
	 * @return
	 */
	@Override
	public List<String> getDistinctNumbers() 
	{
		List<String> numberList = iTicketRepository.getDistinctNumbers();
		
		return numberList;
	}
	
	/**
	 * @author hardas_d
	 * @return
	 */
	@Override
	public List<AccountUser> getDistinctRequesters() 
	{
		List<AccountUser> requesters = iTicketRepository.getDistinctRequesters();
		
		return requesters;
	}

	@Override
	public List<TkmTickets> getTkmTicketsByActivitiTaskId(String activitiTaskId) throws TKMException {
		return iTicketRepository.getTkmTicketsByActivitiTaskId(activitiTaskId);
	}
	
	
	@Override
	public List<Object[]>  getTkmTicketsForPieReport(String criteria) throws TKMException {
		
		if(PARAM_TYPE.equalsIgnoreCase(criteria)) {
			
			return iTicketRepository.getTkmTicketsByType();
		} else {
			return iTicketRepository.getTkmTicketsByPriority();
		}
		
		
	}
	
	@Override
	public long getCntByModuleAndRoleId(int moduleId, int roleId) {
		return iTicketRepository.findByModuleIdAndUserRoleId(moduleId, roleId);
	}
	
	/**
	 * @author hardas_d
	 * @return {@link TkmTickets}
	 */
	@Override
	public TkmTickets findByTicketNumber(String ticNum) {
		return iTicketRepository.findByNumber(ticNum);
	}
	
	
	/**
	 * @author makhija_n
	 * @return
	 */
	@Override
	public Household getNameCreatedFor(int moduleId ) 
	{
		Household household = iTicketRepository.getNameCreatedFor(moduleId);
		
		return household;
	}
	
	
	
	
	/**
	 * @author makhija_n
	 * @return
	 */
	/*@Override
	public List<Household> getHouseholdNamesForCreatedFor(String username ) throws TKMException
	{
		List<Household> householdlist = null;
		try {
			householdlist = iTicketRepository.getHouseholdNamesForCreatedFor();
		} catch (Exception e) {
			LOGGER.error("Error in getting requester list: ", e);
			throw (TKMException)e;
		}
		
		return householdlist;
	}*/
	
	@Override
	public String getCreatedForIndv(String userText) {
		List<Object[]> householdList = iTicketRepository.getCreatedForIndv1(userText);
		
		Map<Integer, String> mapHHIdName =  new HashMap<>();
		if(householdList != null) {
			for (Object[] user : householdList) {
				String firstName = null==user[1]?StringUtils.EMPTY:(String)user[1];
				String lastName = null==user[2]?StringUtils.EMPTY:(String)user[2];
				mapHHIdName.put((Integer)user[0],firstName+ " "+ lastName);
			}
		}
		
		String result=StringUtils.EMPTY;
		try {
			result = JacksonUtils.getJacksonObjectWriterForHashMap(Integer.class, String.class).writeValueAsString(mapHHIdName);
		} catch (JsonProcessingException e) {
			LOGGER.error("ERROR CONVERTING mapHHIdName TO JSON IN getCreatedForIndv METHOD" , e);
		}
		
		//System.out.println("Household id list: " +result);
		return result;
	}
	
	@Override
	public String getCreatedForUser(String roleId, String userText) {
		Integer roleIdInt=null;
		if(!StringUtils.isEmpty(roleId))
		{
			 roleIdInt=Integer.parseInt(roleId);
		}
		
		
		
		Map<Integer, String> mapUserIdName =  new HashMap<>();
	/*	List<AccountUser> userList = iTicketRepository.getCreatedForUser(roleIdInt);
		if(userList != null) {
			for (AccountUser user : userList) {
				mapUserIdName.put(user.getId(), user.getFirstName() + " "+ user.getLastName());
			}
		}*/
		List<Object[]> userList1=null; 
		if(StringUtils.isNotEmpty(userText)){
		userList1 = iTicketRepository.getCreatedForUser1(userText,roleIdInt);
		}
		else{
			userList1 = iTicketRepository.getCreatedForUser1(roleIdInt);
		}
		for (Object[] user : userList1) {
			String firstName = null==user[1]?StringUtils.EMPTY:(String)user[1];
			String lastName = null==user[2]?StringUtils.EMPTY:(String)user[2];
			mapUserIdName.put((Integer)user[0],firstName+ " "+ lastName);
		}
	
		String result=StringUtils.EMPTY;
		try {
			result = JacksonUtils.getJacksonObjectWriterForHashMap(Integer.class, String.class).writeValueAsString(mapUserIdName);
		} catch (JsonProcessingException e) {
			LOGGER.error("ERROR CONVERTING mapUserIdName TO JSON IN getCreatedForUser METHOD" , e);
		}
		
		return result;
	}

	@Override
	public List<Household> getHouseholdByuserId(int houseHoldUserId) {
		List<Household> householdList = iTicketRepository.getHouseholdByuserId(houseHoldUserId);
		return householdList;
	}
	
	@Override
	public boolean archiveTicket(Integer ticNumber, Integer archivedBy)
	{   
		boolean bStatus = false;
		TkmTickets tkmTickets = iTicketRepository.findOne(ticNumber);
		if(tkmTickets != null){
			tkmTickets.setIsArchived("Y");
			tkmTickets.setArchivedBy(archivedBy);
			tkmTickets.setArchivedOn(new TSDate());
			tkmTickets = iTicketRepository.save(tkmTickets);
			bStatus = (tkmTickets != null) ? true : bStatus;
		}
		
		return bStatus; 		
	}
	
//	@Override
//	public Map<String, Object> searchTicketsWithNativeQuery(
//			Map<String, Object> searchCriteria) {
//
//		Map<String, Object> mapTicketRecCount = new HashMap<String, Object>();
//		Map<String, Object> mapQueryParam = new HashMap<>();
//		
//		Object reportSearch = searchCriteria.get("reportSearch");
//		boolean isReportSearch =  (boolean) ((reportSearch !=null ) ? reportSearch : false);	
//		Integer pageSize = (isReportSearch) ? 1000 : (Integer) searchCriteria.get("pageSize");
//		String selectClause = "SELECT DISTINCT tkt.tkm_ticket_id AS tktid,tkt.subject AS subject,tkt.ticket_number AS tktNum, ( CASE WHEN tkt.module_name='HOUSEHOLD'  THEN (SELECT cmr_household.FIRST_NAME ||  ' ' ||cmr_household.LAST_NAME FROM cmr_household WHERE cmr_household.id= tkt.module_id) "
//							 +" ELSE au.first_name||  ' ' ||au.last_name  END) AS name ,"
//							 +" tkt.creation_timestamp AS tktcreation, queues.queue_name AS queueName,tkt.priority AS tktPriority, tkt.status AS tktStatus, tkttask.status AS taskStatus, "
//							 +" tkt.due_date  AS tktDueDt, tkt.archive_flag, (au2.first_name||  ' ' ||au2.last_name) as requestedBy , tkt.priority_order as priorityOrder , au2.first_name as firstName, tkt.created_by AS createdBy ";
//		String selectCountClause = "SELECT tkt.tkm_ticket_id ";
//		String fromClause = " FROM tkm_tickets tkt,     tkm_ticket_tasks tkttask, users au, tkm_queues queues,  users au2, tkm_workflows tw ";
//		String whereClause = " WHERE tkt.tkm_workflow_id = tw.tkm_workflow_id AND tkttask.TKM_TICKET_TASK_ID = (select max(tt.TKM_TICKET_TASK_ID) FROM tkm_ticket_tasks tt WHERE tt.tkm_ticket_id = tkt.tkm_ticket_id) AND tkt.tkm_ticket_id = tkttask.tkm_ticket_id AND tkt.user_id=au.id AND tkt.created_by=au2.id AND tkt.assigned_queue=queues.tkm_queue_id ";
//				
//		String fromUnionClause =  " FROM tkm_tickets tkt,     tkm_ticket_tasks tkttask, users au, tkm_queues queues,  users au2,   comments cm,  comment_targets cmt ";
//		String whereUnionClause = " WHERE tkttask.TKM_TICKET_TASK_ID = (select max(tt.TKM_TICKET_TASK_ID) FROM tkm_ticket_tasks tt WHERE tt.tkm_ticket_id = tkt.tkm_ticket_id) AND tkt.tkm_ticket_id = tkttask.tkm_ticket_id AND tkt.user_id=au.id AND tkt.created_by=au2.id AND tkt.assigned_queue=queues.tkm_queue_id "+
//								  " AND cm.comment_target_id = cmt.id  AND cmt.target_id=tkt.tkm_ticket_id   and cmt.target_name='TICKET' ";
//		StringBuilder reportSelectClause = new StringBuilder("SELECT DISTINCT tkt.tkm_ticket_id AS tktid, tkt.ticket_number AS tktNum, tw.ticket_category as category, tw.ticket_type as ticketType, "); 
//		reportSelectClause.append(" queues.queue_name AS queueName, tkt.subject AS subject, tkt.description, tkt.status AS tktStatus, (au2.first_name||  ' ' ||au2.last_name) as requestedBy, ");
//		reportSelectClause.append(" NVL(( CASE WHEN tkt.module_name='HOUSEHOLD'  THEN (SELECT cmr_household.FIRST_NAME ||  ' ' ||cmr_household.LAST_NAME FROM cmr_household WHERE cmr_household.id= tkt.module_id) ELSE au.first_name||  ' ' ||au.last_name  END), 'NA') AS name, ");
//		reportSelectClause.append(" tkt.priority, tkt.creation_timestamp AS tktcreation, NVL(tkt.due_date, to_date('19700101','YYYYMMDD')) AS tktDueDt ");
//		StringBuilder query = (isReportSearch) ? new StringBuilder(reportSelectClause) :  new StringBuilder(selectClause);
//		query.append(fromClause);
//		query.append(whereClause);
//		
//		
//		StringBuilder unionQuery = new StringBuilder(selectClause);
//		unionQuery.append(fromUnionClause);
//		unionQuery.append(whereUnionClause);
//		
//		// apply the filters and create parameter map
//		applyGeneralNativeQueryFilters(query, searchCriteria, mapQueryParam);
//		applyGeneralNativeQueryFilters(unionQuery, searchCriteria, mapQueryParam);
//		// filter by ticket text
//				String ticketComments = (String) searchCriteria.get("ticketComments");
//				if (StringUtils.isNotEmpty(ticketComments) && !ticketComments.equalsIgnoreCase(ANY)) {
//					unionQuery.append(" AND lower(cm.comment_text) like :").append("ticketComments");
//					query.append(" AND (lower(tkt.description) like :").append("ticketComments").append(" OR lower(tkt.subject) like :").append("ticketComments").append(")");
//					mapQueryParam.put("ticketComments", "%" + ticketComments.toLowerCase() + "%");
//				}
//		// apply the filters for super or non-super users
//		Map<String, Object> mapRoleFilter = applyNativeQueryRoleFilters(query, searchCriteria);
//		applyNativeQueryRoleFilters(unionQuery, searchCriteria);
//		if(!mapRoleFilter.isEmpty()) {
//			mapQueryParam.putAll(mapRoleFilter);
//		}
//		if(!isReportSearch){
//			query =  query.append( " UNION " ).append(unionQuery.toString());	
//		}
//		// apply sort filters
//		addNativeQuerySortFilters(query, searchCriteria);
//		LOGGER.info("Search TkmTicketTask Query: " + query.toString());
//		EntityManager em = null;
//		try {					
//			//Execute the query, get the records as per search criteria.
//			em = emf.createEntityManager();
//			
//			Query hqlQuery = em.createNativeQuery(query.toString());
//					
//			// Set query parameters
//			setNamedQueryParams(hqlQuery, mapQueryParam);
//			// Set the paging criteria		
//			filterByPaging(hqlQuery, (Integer) searchCriteria.get("startRecord"), pageSize);
//			
//			@SuppressWarnings("unchecked")
//			List<Object> resultList = hqlQuery.getResultList();
//			if(!isReportSearch) {
//			for (Object row : resultList) {
//				Object[] rowArray = (Object[]) row;
//				if (rowArray[9] == null) {
//					rowArray[9] = StringUtils.EMPTY;
//				}
//				
//				if (rowArray[3] == null) {
//		 	 	 rowArray[3] = StringUtils.EMPTY;
//				}	
//				
//				if(rowArray[7]!=null){
//					rowArray[7] = TicketStatus.valueOf((String)rowArray[7]).getDescription() ;
//				}
//			}
//			
//			 
//				//mapTicketRecCount.put("ticketList", resultList);
//			String finalQry = query.toString();
//			finalQry = finalQry.replace(selectClause, selectCountClause);// replace first select before union
//			finalQry = finalQry.replace(selectClause, selectCountClause);//replace second select after union
//			finalQry = finalQry.substring(0,finalQry.indexOf("ORDER BY")); // remove order by as it wont work with union
//			finalQry = "select count(*) from ("+finalQry +")";//create a subquery to get count from union
//			Query hqlCountQuery = em.createNativeQuery(finalQry);
//			setNamedQueryParams(hqlCountQuery, mapQueryParam);		
//			mapTicketRecCount.put("recordCount", hqlCountQuery.getSingleResult());
//			
//			updateDateParameterBasedOnPeriodSearch(searchCriteria, mapTicketRecCount);
//			}
//			mapTicketRecCount.put("ticketList", resultList);
//		} catch (Exception e) {
//			LOGGER.error("Unable to search tickets", e);
//		} finally {
//			if (em != null && em.isOpen()) {
//				em.clear();
//				em.close();
//			}
//		}
//		
//		return mapTicketRecCount;
//	}

	/**
	 * Get the sorting order and column to sort on.
	 * @param query
	 * @param searchCriteria
	 */
	private void addNativeQuerySortFilters(StringBuilder query,
			Map<String, Object> searchCriteria) {		
		String columnName = searchCriteria.get("sortBy").toString();				
		String sortOrder = searchCriteria.get("sortOrder").toString();
		
		sortOrder = (("DESC").compareToIgnoreCase(sortOrder) == 0) ? "DESC" : "ASC";
		
		Map<String, String> sortMap = new HashMap<>();
		sortMap.put("tkm_ticket_id", "tkt.tkm_ticket_id");
		sortMap.put(TASKROLE, "req.first_name");
		sortMap.put(SORT_BY_TKM_QUEUES, "queues.queue_name");
		sortMap.put("status", "tkt.status");
		sortMap.put("priority_order", "tkt.priority_order");
		sortMap.put("creation_timestamp", "tkt.creation_timestamp");
		sortMap.put("created", "tkt.creation_timestamp");
		sortMap.put("due_date", "tkt.due_date");
		
		
		String sortBy = sortMap.get(columnName);
		if(StringUtils.isEmpty(sortBy)) {
			LOGGER.warn("Ticket list sorting ignored, as sorting not supported for column ".intern() + columnName);
		}
		sortBy = !StringUtils.isEmpty(sortBy) ? sortBy : "tkt.creation_timestamp";
		query.append(" ORDER BY ").append(sortBy).append(" ").append(sortOrder);
	}
	
//	@Deprecated
//	protected void addNativeQuerySortFiltersOld(StringBuilder query,
//			Map<String, Object> searchCriteria) {		
//		String columnName = searchCriteria.get("sortBy").toString();				
//		String sortOrder = searchCriteria.get("sortOrder").toString();
//		
//		sortOrder = (("DESC").compareToIgnoreCase(sortOrder) == 0) ? "DESC" : "ASC";
//		
//		List<String> sortByCols = Arrays.asList("tkt.tkm_ticket_id", "tkt.subject", "tkt.number", "au.first_name","au.last_name", "tkt.creation_timestamp", 
//		                            "queues.queue_name", "tkt.priority_order", "tkt.status", "tt.status", "tkt.due_date","au2.first_name");
//		
//		Map<String,String> columnAliasMap = new HashMap<String,String>();
//		List<String> sortByColsAlias = Arrays.asList("tktid","subject","tktNum","name","name","tktcreation","queueName","priorityOrder",
//									   "tktStatus","taskStatus","tktDueDt","firstName");
//		//create a map of column names and their aliases, as union will work only with aliases
//		int i=0;
//		for(String column :sortByCols){
//			columnAliasMap.put(column, sortByColsAlias.get(i));
//			i++;
//		}
//		String sortBy = "tkt.creation_timestamp";
//		if(!StringUtils.isEmpty(columnName)) {
//			if((TASKSTATUS).equalsIgnoreCase(columnName)) {
//				sortBy = columnAliasMap.get("tt.status");
//			} else if((TASKROLE).equalsIgnoreCase(columnName)){
//				sortBy = columnAliasMap.get("au.first_name");
//			} else if((SORT_BY_TKM_QUEUES).equalsIgnoreCase(columnName)){
//				sortBy = columnAliasMap.get("queues.queue_name");
//			} else {
//				String name = "tkt."+columnName;
//				sortBy = columnAliasMap.get(name);
//			}
//		}
//		
//		sortBy = columnAliasMap.containsValue(sortBy) ? sortBy : "tkt.creation_timestamp";
//		query.append(" ORDER BY ").append(sortBy).append(" ").append(sortOrder);
//	}

	private Map<String, Object> applyNativeQueryRoleFilters(
			StringBuilder query, Map<String, Object> searchCriteria) {
		Map<String, Object> mapQueryParam = null;
		
		// filter Assignee id, task status and queue user id
		Integer userId = (Integer) searchCriteria.get(USER_ID);
		String taskStatus = (String) searchCriteria.get(TASKSTATUS);
		@SuppressWarnings("unchecked")
		List<Integer> queueIdList = (List<Integer>)searchCriteria.get("userQueueId");	
		queueIdList = (queueIdList != null) ? queueIdList : new ArrayList<Integer>();
		if(userId != null) {
			// user id will be available only for non-super user
			// assignee will be the logged in user	
			mapQueryParam = filterForNativeQueryNonSuperUser(query, taskStatus, userId, queueIdList);
		} else { 
			// Super user should have access to all ticket filtered by assignee id if available
			mapQueryParam = filterByNativeQueryTaskStatusForSuperUser(query, taskStatus);
			Map<String, Object> mapAssigneeFilter = filterByNativeQueryAssigneeIdForSuperUser(query, (String) searchCriteria.get("assigneeId"));
			
			mapQueryParam.putAll(mapAssigneeFilter);
		}
		
		return mapQueryParam;
	}

	private Map<String, Object> filterByNativeQueryAssigneeIdForSuperUser(
			StringBuilder query, String assigneeId) {
		Map<String, Object> mapQueryParam = new HashMap<>();
		if(!StringUtils.isEmpty(assigneeId) && ((ANY).compareToIgnoreCase(assigneeId) !=0)) {
			query.append("AND tkttask.assignee=:").append("assigneeId").append(" ");
			mapQueryParam.put("assigneeId", Integer.parseInt(assigneeId));
		}	
		return mapQueryParam;
	}

	private Map<String, Object> filterByNativeQueryTaskStatusForSuperUser(
			StringBuilder query, String taskStatus) {
		Map<String, Object> mapQueryParam =  new HashMap<>();
		if (StringUtils.isNotEmpty(taskStatus) && !taskStatus.equalsIgnoreCase(ANY)) {
			query.append("AND tkttask.status=:").append(TASKSTATUS).append(" ");
			mapQueryParam.put(TASKSTATUS, taskStatus);
		}
		return mapQueryParam;
	}

	private Map<String, Object> filterForNativeQueryNonSuperUser(
			StringBuilder query, String taskStatus, Integer userId,
			List<Integer> queueIdList) {
		Map<String, Object> mapQueryParam = new HashMap<>();
		
		if(StringUtils.isEmpty(taskStatus) || taskStatus.equalsIgnoreCase(ANY)) {
			//Query: Get tickets which are directly assigned and unclaimed from queue list
			query.append("AND (tkttask.assignee=:").append(USER_ID).append(" "); // tickets directly assigned to the user
			mapQueryParam.put(USER_ID, userId);
			
			if(queueIdList !=null && !queueIdList.isEmpty()) {  // Unclaimed tickets from queue list
				query.append(" OR ");
				query.append("( tkttask.tkm_queue_id IN ").append(":queueIdList");
				query.append(" AND tkttask.status=:").append(TASKSTATUS).append(") ");
				mapQueryParam.put("queueIdList", queueIdList);
				mapQueryParam.put(TASKSTATUS, TkmTicketTasks.task_status.UnClaimed.toString());
			}
			query.append(" ) ");
		} else if(StringUtils.isNotEmpty(taskStatus) 
			&& taskStatus.equalsIgnoreCase(TkmTicketTasks.task_status.UnClaimed.toString())) {
			//Query: Get only unclaimed tickets from queue list
			if(queueIdList ==null || queueIdList.isEmpty()) {  // Unclaimed tickets from queue list
				queueIdList =  new ArrayList<>();
				queueIdList.add(0); // dummy queue id to resolve HIX-53562
			}
				query.append("AND (tkttask.tkm_queue_id in(:queueIdList)");
				query.append("AND tkttask.status=:").append(TASKSTATUS).append(") ");
				mapQueryParam.put("queueIdList", queueIdList);
				mapQueryParam.put(TASKSTATUS, TkmTicketTasks.task_status.UnClaimed.toString());
					
		} else {
			//Query: Get only assigned tickets with specified task status
			query.append("AND tkttask.assignee=:").append(USER_ID).append(" ");// tickets directly assigned to the user
			query.append("AND tkttask.status=:").append(TASKSTATUS).append(" ");
			mapQueryParam.put(USER_ID, userId);
			mapQueryParam.put(TASKSTATUS, taskStatus);
		}	
		
		return mapQueryParam;
	}

	private void applyGeneralNativeQueryFilters(StringBuilder query,
			Map<String, Object> searchCriteria,
			Map<String, Object> mapQueryParam) {
		// filter by subject
		String ticketSubject = (String) searchCriteria.get(SEARCH_CRITERIA_TICKET_SUBJECT);
		if (StringUtils.isNotEmpty(ticketSubject) && !ticketSubject.equalsIgnoreCase(ANY)) {
			query.append("AND tkt.subject like :").append(SEARCH_CRITERIA_TICKET_SUBJECT).append(" ");
			mapQueryParam.put(SEARCH_CRITERIA_TICKET_SUBJECT, "%" + ticketSubject + "%");
		}
		
		// filter by requested by
		String requestedBy = (String) searchCriteria.get("requestedBy");
		if(!StringUtils.isEmpty(requestedBy) && ((ANY).compareToIgnoreCase(requestedBy) !=0)) {
			query.append("AND tkt.created_by=:requestedBy ");
			mapQueryParam.put("requestedBy", Integer.parseInt(requestedBy));
		}
		
		// filter by Queue
		@SuppressWarnings("unchecked")
		List<String>  queueList = (List<String>)  searchCriteria.get(SEARCH_CRITERIA_TICKET_QUEUES);
		if (queueList != null && !queueList.isEmpty() && !queueList.contains(ANY)) {
			query.append("AND queues.queue_name in :").append(SEARCH_CRITERIA_TICKET_QUEUES).append(" ");
			mapQueryParam.put(SEARCH_CRITERIA_TICKET_QUEUES, queueList);
		}
		
		String userRole = (String) searchCriteria.get("userRole");
		
		
		
		
		// filter by Created For
		String rolename=null;
		String moduleId = (String) searchCriteria.get("moduleId");
		if(!StringUtils.isEmpty(moduleId) && ((ANY).compareToIgnoreCase(moduleId) !=0)) {
			if(!StringUtils.isEmpty(userRole)) {
				Role role = roleService.findById(Integer.parseInt(userRole));
				 rolename=role.getName();
			}
			
			if("INDIVIDUAL".equalsIgnoreCase(rolename))
			{	
			query.append("AND tkt.module_id=:moduleId ");
			query.append("AND tkt.module_name='HOUSEHOLD' ");
			}
			else
			{
			 query.append("AND au.id=:moduleId ");	
			 query.append("AND tkt.module_name!='HOUSEHOLD'");	
			 
			}
			
			mapQueryParam.put("moduleId", Integer.parseInt(moduleId));
		}
		
		
		
		// filter by priority
		@SuppressWarnings("unchecked")
		List<String>  ticketPriority = (List<String>)  searchCriteria.get("ticketPriority");
		if (ticketPriority != null && !ticketPriority.isEmpty() && !ticketPriority.contains(ANY)) {
			query.append("AND tkt.priority in :").append("ticketPriority").append(" ");
			mapQueryParam.put("ticketPriority",ticketPriority);
		}
		
		// filter by ticket number
		String ticketNumber = (String) searchCriteria.get("ticketNumber");
		if (StringUtils.isNotEmpty(ticketNumber) && !ticketNumber.equalsIgnoreCase(ANY)) {
			query.append("AND tkt.ticket_number like :").append("ticketNumber").append(" ");
			mapQueryParam.put("ticketNumber", "%" + ticketNumber + "%");
		}
		
		// filter by ticket status
		@SuppressWarnings("unchecked")
		List<String> ticketStatus = (List<String>) searchCriteria.get("ticketStatus");
		if (ticketStatus !=null && !ticketStatus.isEmpty() && !ticketStatus.contains("Any")) {
            query.append("AND tkt.status in :").append("ticketStatus").append(" ");
            mapQueryParam.put("ticketStatus", ticketStatus);
		}
		
		// Add period filter : needed for ageing report drill down 
		if (searchCriteria.get("period") != null ){
			filterByPeriod(searchCriteria, (String)searchCriteria.get("period"));
		}
		
		String archiveFlagValue = (String) searchCriteria.get("archiveFlagValue");
		boolean isNonSuperUser = Boolean.parseBoolean((searchCriteria.get("isNonSuperUser")).toString());
	
		if (isNonSuperUser){
			query.append("AND tkt.archive_flag =:").append("archiveFlagValue").append(" ");
			if(archiveFlagValue.equalsIgnoreCase("Y"))
			{	
			query.append("AND tkt.archived_by =:").append("userId").append(" ");
			mapQueryParam.put("userId", searchCriteria.get("userId").toString());
			}
			mapQueryParam.put("archiveFlagValue",archiveFlagValue);
			
		}else{
			query.append("AND tkt.archive_flag =:").append("archiveFlagValue").append(" ");
			mapQueryParam.put("archiveFlagValue",archiveFlagValue);
		}
		
		// filter by date (start date, end date)
		String endDate = (String) searchCriteria.get("submittedBefore");
		if(StringUtils.isNotEmpty(endDate)) {
			query.append("AND tkt.creation_timestamp <=").append(":submittedBefore").append(" ");
			Date submittedBeforeDate = prepareDateCriteria(endDate, true);
			submittedBeforeDate = (submittedBeforeDate != null) ? submittedBeforeDate : new TSDate();
			mapQueryParam.put("submittedBefore", submittedBeforeDate);
		}
		String createdDate = (String) searchCriteria.get("submittedAfter");
		if(StringUtils.isNotEmpty(createdDate)) {
			query.append("AND tkt.creation_timestamp >=").append(":submittedAfter").append(" ");
			Date submittedAfterDate = prepareDateCriteria(createdDate, false);
			submittedAfterDate = (submittedAfterDate != null) ? submittedAfterDate : new TSDate();
			mapQueryParam.put("submittedAfter", submittedAfterDate);
		}
	}
	
	@Override
	@Transactional
	public Map<String, Object> searchTicketsWithNativeQuery(Map<String, Object> searchCriteria) {

		Map<String, Object> mapTicketRecCount = new HashMap<String, Object>();
		Map<String, Object> mapQueryParam = new HashMap<>();
		
		Object reportSearch = searchCriteria.get("reportSearch");
		boolean isReportSearch =  (boolean) ((reportSearch !=null ) ? reportSearch : false);	
		Integer pageSize = (isReportSearch) ? 1000 : (Integer) searchCriteria.get("pageSize");
		
		String selectClause = "SELECT DISTINCT tkt.tkm_ticket_id AS tktid,tkt.subject AS subject,tkt.ticket_number AS tktNum, ( CASE WHEN tkt.module_name='HOUSEHOLD'  THEN (SELECT cmr_household.FIRST_NAME ||  ' ' ||cmr_household.LAST_NAME FROM cmr_household WHERE cmr_household.id= tkt.module_id) "
				 +" ELSE au.first_name||  ' ' ||au.last_name  END) AS name ,"
				 +" tkt.creation_timestamp AS tktcreation, queues.queue_name AS queueName,tkt.priority AS tktPriority, tkt.status AS tktStatus, tkt.status AS taskStatus, "
				 +" tkt.due_date  AS tktDueDt, tkt.archive_flag, (req.first_name||  ' ' ||req.last_name) as requestedBy , tkt.priority_order as priorityOrder , req.first_name as firstName, tkt.created_by AS createdBy, workflow.ticket_type as ticketType, workflow.ticket_category as ticketCategory ";
		
		String fromClause = " FROM tkm_tickets tkt, users au, users req, tkm_ticket_tasks tkttask, tkm_queues queues, tkm_workflows workflow "; 
		String whereClause = " WHERE tkt.tkm_ticket_id = tkttask.tkm_ticket_id "
							+ " AND tkt.user_id=au.id " 
							+ " AND tkt.created_by = req.id "
							+ " AND tkt.assigned_queue=queues.tkm_queue_id "
							+ " AND tkt.tkm_workflow_id = workflow.tkm_workflow_id " ;
		String selectCountClause = "SELECT count(DISTINCT tkt.tkm_ticket_id) ";
		
		StringBuilder reportSelectClause = new StringBuilder("SELECT DISTINCT tkt.tkm_ticket_id AS tktid, tkt.ticket_number AS tktNum, tw.ticket_category as category, tw.ticket_type as ticketType, ");
		reportSelectClause.append(" queues.queue_name AS queueName, tkt.subject AS subject, tkt.description, tkt.status AS tktStatus, (req.first_name||  ' ' ||req.last_name) as requestedBy, ");
		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase("POSTGRESQL")) {
			reportSelectClause.append(" COALESCE(( CASE WHEN tkt.module_name='HOUSEHOLD'  THEN (SELECT cmr_household.FIRST_NAME ||  ' ' ||cmr_household.LAST_NAME FROM cmr_household WHERE cmr_household.id= tkt.module_id) ELSE au.first_name||  ' ' ||au.last_name  END), 'NA') AS name, ");
		}else {
			reportSelectClause.append(" NVL(( CASE WHEN tkt.module_name='HOUSEHOLD'  THEN (SELECT cmr_household.FIRST_NAME ||  ' ' ||cmr_household.LAST_NAME FROM cmr_household WHERE cmr_household.id= tkt.module_id) ELSE au.first_name||  ' ' ||au.last_name  END), 'NA') AS name, ");
		}
		reportSelectClause.append(" tkt.priority, tkt.creation_timestamp AS tktcreation, ");
		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase("POSTGRESQL")) {
			reportSelectClause.append(" COALESCE(tkt.due_date, to_date('19700101','YYYYMMDD')) AS tktDueDt ");
		}else {
			reportSelectClause.append(" NVL(tkt.due_date, to_date('19700101','YYYYMMDD')) AS tktDueDt ");
		}
		
		
		StringBuilder query = (isReportSearch) ? new StringBuilder(reportSelectClause) :  new StringBuilder(selectClause);
		StringBuilder fromQuery = new StringBuilder(fromClause);
		StringBuilder whereQuery = new StringBuilder(whereClause);
		if(isReportSearch) {
			fromQuery.append(", tkm_workflows tw ");
			whereQuery.append(" AND tkt.tkm_workflow_id = tw.tkm_workflow_id ");
		}

		// Ticket comment search
		String ticketComments = (String) searchCriteria.get("ticketComments");
		if (StringUtils.isNotEmpty(ticketComments) && !ticketComments.equalsIgnoreCase(ANY)) {
			fromQuery.append(", comments cm,  comment_targets cmt  ");
			
			whereQuery.append(" AND cm.comment_target_id = cmt.id  AND cmt.target_id=tkt.tkm_ticket_id   and cmt.target_name='TICKET' ");
			whereQuery.append(" AND ( lower(cm.comment_text) like :").append("ticketComments");
			whereQuery.append(" OR lower(tkt.description) like :").append("ticketComments").append(" OR lower(tkt.subject) like :").append("ticketComments").append(")");
			mapQueryParam.put("ticketComments", "%" + ticketComments.toLowerCase() + "%");
		}
		
		query.append(fromQuery);
		query.append(whereQuery);
		
		// apply the filters and create parameter map
		applyGeneralNativeQueryFilters(query, searchCriteria, mapQueryParam);
		// apply the filters for super or non-super users
		Map<String, Object> mapRoleFilter = applyNativeQueryRoleFilters(query, searchCriteria);
		if(!mapRoleFilter.isEmpty()) {
			mapQueryParam.putAll(mapRoleFilter);
		}
		
		// apply sort filters
		addNativeQuerySortFilters(query, searchCriteria);
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			
			Query hqlQuery = em.createNativeQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			// Set the paging criteria		
			filterByPaging(hqlQuery, (Integer) searchCriteria.get("startRecord"), pageSize);
			
			@SuppressWarnings("unchecked")
			List<Object> resultList = hqlQuery.getResultList();
			if(!isReportSearch) {
				for (Object row : resultList) {
					Object[] rowArray = (Object[]) row;
					if (rowArray[9] == null) {
						rowArray[9] = StringUtils.EMPTY;
					}
					
					if (rowArray[3] == null) {
			 	 	 rowArray[3] = StringUtils.EMPTY;
					}	
					
					if(rowArray[7]!=null){
						rowArray[7] = TicketStatus.valueOf((String)rowArray[7]).getDescription() ;
					}
				}
				
				// Count query
				query.replace(query.indexOf(selectClause), selectClause.length(), selectCountClause);
				Query hqlCountQuery = em.createNativeQuery(query.substring(0,query.lastIndexOf("ORDER BY")).toString());
				setNamedQueryParams(hqlCountQuery, mapQueryParam);		
				mapTicketRecCount.put("recordCount", hqlCountQuery.getSingleResult());
			}

			mapTicketRecCount.put("ticketList", resultList);
			updateDateParameterBasedOnPeriodSearch(searchCriteria, mapTicketRecCount);
		} catch (Exception e) {
			LOGGER.error("Unable to search tickets", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return mapTicketRecCount;
	}
	
	/**
	 * @author hardas_d
	 * @return
	 */
	@Transactional
	@Override
	public List<AccountUser> getRequesters(String name) 
	{
		List<AccountUser> requesters = new ArrayList<>(); 
		AccountUser user = null;
		
		try {
			List<Object[]> reqList = iTicketRepository.getRequesters(name);
			for (Object[] req : reqList) {			
				user = new AccountUser();
				user.setId((Integer) req[0]);
				user.setFirstName((null==req[1]) ? StringUtils.EMPTY : (String)req[1]);
				user.setLastName((null==req[2]) ? StringUtils.EMPTY : (String)req[2]);
				
				requesters.add(user);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the requester list", e);
		}
		
		return requesters;
	}

	@Override
	public void saveTicketEvent(Integer ticketId, Integer eventId, String subModuleName) {
		TkmTickets tkmTickets = iTicketRepository.findOne(ticketId);
		if(tkmTickets != null) {
			tkmTickets.setSubModuleId(eventId);
			tkmTickets.setSubModuleName(subModuleName);
			tkmTickets = iTicketRepository.save(tkmTickets);
		}
		
	}
	
	@Override
	public TkmTickets findTicketWithEventId(Integer subModuleId) {
		if (null!=subModuleId && subModuleId>0) {
				return iTicketRepository.findBySubModuleId(subModuleId);
		}
		return null;
	}
	
	@Override
	public TkmTickets findTicketWithEventIdAndName(Integer subModuleId, String subModuleName){
		if (null!=subModuleId && subModuleId>0 && null != subModuleName && subModuleName.length() >0) {
			return iTicketRepository.findBySubModuleIdAndName(subModuleId,subModuleName);
	}
		return null;
	}
}
