/*
 * Custom script for Income Page
 */

$(function(){
	
	//Income page model
	IncomeModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			memberDesc: "",
			jobIncome: 0.0,
			selfEmployment: 0.0,
			socialSecurityBenefits: 0.0,
			unemployment: 0.0,
			retirementPension: 0.0,
			capitalGains: 0.0,
			investmentIncome: 0.0,
			alimonyReceived: 0.0,
			rentalRoyaltyIncome: 0.0,
			farmingFishingIncome: 0.0,
			otherIncome: 0.0	
      	}
	});
	
	//Incomes Collection
	IncomeCollection = Backbone.Collection.extend({
		model : IncomeModel
	});
	
	incomes = new IncomeCollection();
});