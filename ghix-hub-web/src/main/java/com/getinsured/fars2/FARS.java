package com.getinsured.fars2;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.ridpv2.RIDPNamespaceMapper;


public final class FARS {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FARS.class);
	
	private FARS(){
		
	}
	
	public static void main(String[] args) {
		try{
			HubServiceBridge bridge = HubServiceBridge.getHubServiceBridge("FARSRequest");
			bridge.getServiceHandler().setJsonInput("{\"dshReferenceNumber\":\"c3c8-9e-6097\"}");
			
			Object payLoad = bridge.getServiceHandler().getRequestpayLoad();
			
			if(payLoad != null){
				JAXBContext jc = JAXBContext.newInstance("com.getinsured.fars2.cms.dsh.ridp.exchange._1");
				Marshaller m = jc.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new RIDPNamespaceMapper());
				m.marshal(payLoad, System.out);
			}
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}

}
