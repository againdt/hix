<%-- <%@page isELIgnored="false"  %> --%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<c:set var="CHECKED_RADIO" value="checked"/>
<div class="gutter10">
	<h2>New Account Set-Up</h2>
	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div>

	<div class="row-fluid">		
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>You should know</h4>
			</div>
		    <ul class="nav nav-list">
		       <c:choose>
		        <c:when test="${fn:containsIgnoreCase(creatorType, 'Broker') || fn:containsIgnoreCase(creatorType, 'Assister')}">
			    <li>${creatorName} has created an account for you on the ${exchangeName}. In order to access your account, please follow this simple process.</li>
		       </c:when>
		      	<c:otherwise>
		             <li><spring:message code="account_setup.customerSupportText" arguments = "${exchangeName}" htmlEscape="false"/></li>
		        </c:otherwise>
			 </c:choose>
		    </ul>	
		</div>

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="margin0">Your information</h4>
			</div>
				<form class="form-horizontal" id="frmactivation" name="frmactivation" action="<c:url value="/account/phoneverification/sendcode" />" method="POST">
				<df:csrfToken/>
					<div class="account_activation">
						<div class="control-group">
							<p><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME_ABBR)%> Customer Support Staff has provided the following phone numbers for you:</p>
							<div class="controls">
								<c:forEach items="${phoneNumberList}" var="phoneNumber">
									<c:if test="${CHECKED_RADIO != '' }">
										<label class="radio"> 
											<input type="radio" name="phoneNumber" id="phoneNumber" checked='checked'  value="<encryptor:enc value="${phoneNumber}"/>">
											(***)***-${fn:substring(phoneNumber, fn:length(phoneNumber) - 4, fn:length(phoneNumber))}
										</label> 
									</c:if>
									<c:if test="${CHECKED_RADIO == '' }">
										<label class="radio"> 
											<input type="radio" name="phoneNumber" id="phoneNumber" value="<encryptor:enc value="${phoneNumber}"/>">
											(***)***-${fn:substring(phoneNumber, fn:length(phoneNumber) - 4, fn:length(phoneNumber))}
										</label> 
									</c:if>
									<br>
									<c:set var="CHECKED_RADIO" value="" />
								</c:forEach>				
								
							</div>			
							<p>In order to verify your identity, we will send you a verification code to the selected number, using a voice call or text message</p>
						</div>	
						<div class="control-group">
							<div class="controls">
								<select name="codeType"id="select-codetye" class="input-medium">
									<option ${codeType == 'sms' ? 'selected="selected"' : ''} value="sms">Text Message</option>
                                    <option ${codeType == 'call' ? 'selected="selected"' : ''} value="call">Voice Call</option>
									<!-- <option value="sms">SMS</option>
									<option value="call">CALL</option> -->                                                                         
								</select>
								
								<input type="submit" class="btn btn-primary" value="Send code" id="submitBtn" name="submitBtn" title="Send code">
							</div>
						</div>
					</div>
				</form>
				<a href="#callErrorModal" role="button" data-toggle="modal">This is not my phone number</a>
<!-- 				<a href="#" id="callError">This is not my phone number</a> -->
				<form class="form-horizontal" id="frmVerifyCode" name="frmVerifyCode" action="<c:url value="/account/phoneverification/sendcode" />" method="POST">
					<df:csrfToken/>
					<div class="control-group verifyCodeDiv hide">
						<label class="control-label" for="verificationCode">Verification Code</label>
						<div class="controls">
							<input type="text" class="input-small"  maxlength="8" name="verificationCode" id="verificationCode"  value="${verificationCode}">
							<input type="submit" class="btn btn-primary" value="Verify" id="verifycode-btn" name="verifycode-btn" title="Verify">
						</div>
					</div>		
					
					<input type="hidden" name="codeType" id="codeType" value="${codeType}">			
				</form>
				<input type="hidden" name="createdType" id="createdType"  value="${createdType}" />
				<input type="hidden" name="emailId" id="emailId"  value="${emailId}" />
				<input type="hidden" name="activationId" id="activationId"  value="${activationId}" />
				<input type="hidden" name="username" id="username"  value="${username}" />
				<input type="hidden" name="codeSent" id="codeSent"  value="${codeSent}" />
			</div>
	</div>
</div>	

<!-- Modal Phone Number Error -->
<div id="callErrorModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">&nbsp;</h3>
  </div>
  <div class="modal-body">
    <c:choose>
        <c:when test="${exchangeName == 'Your Health Idaho'}">
              <p>Please contact your Agent or Enrollment Counselor, if you have one. Otherwise, please contact Your Health Idaho by calling 1-855-YH-IDAHO (${exchangePhone}) to correct your phone number.</p>
       </c:when>
      	<c:otherwise>
             <p>Please call ${exchangePhone} to change the phone number we have on record for you.</p>
        </c:otherwise>
	 </c:choose>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<!-- Modal Phone Number Error Ends-->

<script type="text/javascript">	
	if($('#codeSent').val()==="true"){
		$(".verifyCodeDiv:hidden").show();
		$('#submitBtn').val('Send Again');
		//$('#callError').hide();
	}

	$('#frmVerifyCode').submit(function(){
		var intRegex = /^\d+$/;

		var str = $('#verificationCode').val();
		if(str==="" || str.length < 8 || !(intRegex.test(str))) {
		   alert('Please enter valid 8 digit numeric activation code.');
		   $('#verificationCode').focus();
		   return false;
		   
		}
	});	
</script>