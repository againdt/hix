package com.getinsured.iex.hub.ifsv.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IFSVApplicantType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_types.TaxFilerCategoryCodeSimpleType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IFSVApplicantTypepe
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class IFSVApplicantTypeWrapper implements JSONAware{
	
	private ObjectFactory factory;
	
	private IFSVApplicantType ifsvApplicantType;
	
	private PersonTypeWrapper person;
	private String taxFilerCategoryCode;
	
	public static final String PERSON_KEY = "person";
	public static final String TAX_FILER_CATEGORY_CODE_KEY = "taxFilerCategoryCode";
	
	public IFSVApplicantTypeWrapper(){
		this.factory = new ObjectFactory();
		this.ifsvApplicantType = factory.createIFSVApplicantType();
	}

	public IFSVApplicantType getIfsvApplicantType() {
		return ifsvApplicantType;
	}

	public PersonTypeWrapper getPerson() {
		return person;
	}

	public void setPerson(PersonTypeWrapper person) {
		this.ifsvApplicantType.setPerson(person.getPerson());
		this.person = person;
	}

	public String getTaxFilerCategoryCode() {
		return taxFilerCategoryCode;
	}

	public void setTaxFilerCategoryCode(String taxFilerCategoryCode) {
		this.ifsvApplicantType.setTaxFilerCategoryCode(TaxFilerCategoryCodeSimpleType.fromValue(taxFilerCategoryCode));
		this.taxFilerCategoryCode = taxFilerCategoryCode;
	}

	/**
	 * Converts the JSON String to IFSVApplicantTypeWrapper object
	 * 
	 * @param jsonString - String representation for IFSVApplicantTypeWrapper
	 * @return IFSVApplicantTypeWrapper Object
	 */
	public static IFSVApplicantTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create IFSVApplicantTypeWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		IFSVApplicantTypeWrapper wrapper = new IFSVApplicantTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(PERSON_KEY);
		if(tmpObj != null){
			JSONObject person = (JSONObject)tmpObj;
			wrapper.person = PersonTypeWrapper.fromJSONObject(person);
			wrapper.setPerson(wrapper.person );
		}
		
		tmpObj = obj.get(TAX_FILER_CATEGORY_CODE_KEY);
		if(tmpObj != null){
			wrapper.taxFilerCategoryCode = (String)tmpObj;
			wrapper.setTaxFilerCategoryCode(wrapper.taxFilerCategoryCode);
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.person != null){
			obj.put(PERSON_KEY, this.person.toJSONObject());
		}
		obj.put(TAX_FILER_CATEGORY_CODE_KEY, this.taxFilerCategoryCode);
		return obj.toJSONString();
	}
	
	public static IFSVApplicantTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		IFSVApplicantTypeWrapper wrapper = new IFSVApplicantTypeWrapper();
		if(jsonObj.get(PERSON_KEY) != null){
			wrapper.setPerson(PersonTypeWrapper.fromJSONObject((JSONObject)jsonObj.get(PERSON_KEY)));
		}
		wrapper.setTaxFilerCategoryCode((String) jsonObj.get(TAX_FILER_CATEGORY_CODE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		if(this.person != null){
			obj.put(PERSON_KEY, this.person.toJSONObject());
		}
		if(this.taxFilerCategoryCode != null){
			obj.put(TAX_FILER_CATEGORY_CODE_KEY, this.taxFilerCategoryCode);
		}
		return obj;
	}
	
}
