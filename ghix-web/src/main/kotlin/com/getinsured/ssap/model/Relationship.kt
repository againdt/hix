package com.getinsured.ssap.model

/**
 * Describes relationship from state configuration json.
 * @author Yevgen Golubenko
 * @since 6/20/19
 */
data class Relationship(val text : String, val type: String, val code: String, val mirrorType: String)
