package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfSponsorshipDataType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.SponsorshipDataType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper Class for ArrayOfSponsorshipDataTypeGCDWrapper
 * 
 * @author Vishaka Tharani
 * @since  18-Feb-2014 
 * 
 */
public class ArrayOfSponsorshipDataTypeGCDWrapper implements JSONAware{
	
	private List<SponsorshipDataTypeGCDWrapper> sponsorshipDataWrapperList;
	
	private static final String SPONSORSHIP_DATA_KEY = "sponsorshipData";
	
	public ArrayOfSponsorshipDataTypeGCDWrapper(ArrayOfSponsorshipDataType sponsorshipData) throws HubServiceException {
		
		if(sponsorshipData == null){
			throw new HubServiceException("No sponsorshipData provided");
		}
		
		List<SponsorshipDataType> sponsorshipDataList = sponsorshipData.getSponsorshipData();
		if(sponsorshipDataList != null && !sponsorshipDataList.isEmpty())
		{
			sponsorshipDataWrapperList = new ArrayList<SponsorshipDataTypeGCDWrapper>();
		}
		for(SponsorshipDataType sponsorshipDataType : sponsorshipDataList){
			SponsorshipDataTypeGCDWrapper sponsorshipDataTypeGCDWrapper = new SponsorshipDataTypeGCDWrapper(sponsorshipDataType); 
			sponsorshipDataWrapperList.add(sponsorshipDataTypeGCDWrapper);
		}

	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray sponsorshipData = null;
		
		if(sponsorshipDataWrapperList != null && !sponsorshipDataWrapperList.isEmpty()){
			sponsorshipData = new JSONArray();
			for(SponsorshipDataTypeGCDWrapper data : this.sponsorshipDataWrapperList){
				sponsorshipData.add(data.toJSONString());
			}
		}
		
		obj.put(SPONSORSHIP_DATA_KEY,sponsorshipData);
		return obj.toJSONString();
	}
}
