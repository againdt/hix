package com.getinsured.iex.hub.mec.wrapper;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ResponsePersonType;
import com.getinsured.iex.hub.mec.niem.niem.fbi._2.SEXCodeType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class ResponsePersonWrapper implements JSONAware {
	private Logger logger = LoggerFactory.getLogger(ResponsePersonWrapper.class);
	private Object birthDate;
	private String personGivenName;
	private String personSurName;
	private String personMiddleName;
	private String personSex;
	private String personSSN;
	
	public ResponsePersonWrapper(ResponsePersonType responsePerson) {
		DateType dateType = responsePerson.getPersonBirthDate();
		this.birthDate = this.getDateString(dateType);
		PersonNameType name = responsePerson.getPersonName();
		if(name != null){
			this.personGivenName = name.getPersonGivenName().getValue();
			this.personSurName = name.getPersonSurName().getValue();
			this.personMiddleName = name.getPersonMiddleName().getValue();
		}
		SEXCodeType x = responsePerson.getPersonSexCode();
		if(x != null){
			this.personSex = responsePerson.getPersonSexCode().getValue().value();
		}
		IdentificationType ssnIdtype = responsePerson.getPersonSSNIdentification();
		if(ssnIdtype != null){
			this.personSSN  = ssnIdtype.getIdentificationID().get(0).getValue();
		}
	}
	
	public String getPersonSSN() {
		return personSSN;
	}

	private String getDateString(DateType dateType){
		if(dateType == null){
			return null;
		}
		String dateStr = null;
		Date xsdDate = (Date) dateType.getDateRepresentation().getValue();
		XMLGregorianCalendar xmlDate = xsdDate.getValue();
		try {
			dateStr = PlatformServiceUtil.xmlDateToString(xmlDate, "yyyy-MM-dd");
		} catch (ParseException e) {
			logger.info("Invalid date",e);
		} catch (DatatypeConfigurationException e) {
			logger.info("Invalid date",e);
		}
		return dateStr;
	}

	public Object getBirthDate() {
		return birthDate;
	}

	public String getPersonGivenName() {
		return personGivenName;
	}

	public String getPersonSurName() {
		return personSurName;
	}

	public String getPersonMiddleName() {
		return personMiddleName;
	}

	public String getPersonSex() {
		return personSex;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
	JSONObject obj = new JSONObject();
	obj.put("SSN", this.personSSN);
	obj.put("GivenName", this.personGivenName);
	obj.put("MiddleName", this.personMiddleName);
	obj.put("SurName", this.personSurName);
	obj.put("BirthDate", this.birthDate);
	obj.put("Sex", this.personSex);
	return obj.toJSONString();
	}
}
