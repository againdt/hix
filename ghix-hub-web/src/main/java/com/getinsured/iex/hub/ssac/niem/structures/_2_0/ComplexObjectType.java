
package com.getinsured.iex.hub.ssac.niem.structures._2_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;




import com.getinsured.iex.hub.ssac.extension.FacilityContactInformationType;
import com.getinsured.iex.hub.ssac.extension.FacilityLocationType;
import com.getinsured.iex.hub.ssac.extension.QualifyingYearAndQuarterType;
import com.getinsured.iex.hub.ssac.extension.ResponseMetadataType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualResponseType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeRequestType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeResponseType;
import com.getinsured.iex.hub.ssac.extension.SSAIncarcerationInformationType;
import com.getinsured.iex.hub.ssac.extension.SSAQuartersOfCoverageType;
import com.getinsured.iex.hub.ssac.extension.SSAResponseType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIMonthlyIncomeType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIMonthlyInformationType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIRequestedYearInformationType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIYearlyIncomeType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIYearlyInformationType;
import com.getinsured.iex.hub.ssac.extension.SupervisionFacilityType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.AddressType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.ContactInformationType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.DateType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.EntityType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.FacilityType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.FullTelephoneNumberType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.IdentificationType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.LocationType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.PersonNameType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.PersonType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.StreetType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.StructuredAddressType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.TelephoneNumberType;



/**
 * <p>Java class for ComplexObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplexObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}id"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}metadata"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}linkMetadata"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexObjectType")
@XmlSeeAlso({
    com.getinsured.iex.hub.ssac.extension.PersonNameType.class,
    SSATitleIIMonthlyIncomeType.class,
    QualifyingYearAndQuarterType.class,
    ResponseMetadataType.class,
    SupervisionFacilityType.class,
    SSATitleIIYearlyInformationType.class,
    SSATitleIIMonthlyInformationType.class,
    SSATitleIIYearlyIncomeType.class,
    SSATitleIIRequestedYearInformationType.class,
    FacilityContactInformationType.class,
    SSACompositeIndividualResponseType.class,
    SSACompositeIndividualRequestType.class,
    SSAQuartersOfCoverageType.class,
    SSAResponseType.class,
    SSAIncarcerationInformationType.class,
    com.getinsured.iex.hub.ssac.extension.PersonType.class,
    FacilityLocationType.class,
    SSACompositeResponseType.class,
    SSACompositeRequestType.class,
    PersonNameType.class,
    EntityType.class,
    FullTelephoneNumberType.class,
    DateType.class,
    TelephoneNumberType.class,
    PersonType.class,
    FacilityType.class,
    IdentificationType.class,
    ContactInformationType.class,
    AddressType.class,
    StreetType.class,
    StructuredAddressType.class,
    LocationType.class
})
public abstract class ComplexObjectType {

    @XmlAttribute(name = "id", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "metadata", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> metadata;
    @XmlAttribute(name = "linkMetadata", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> linkMetadata;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getMetadata() {
        if (metadata == null) {
            metadata = new ArrayList<Object>();
        }
        return this.metadata;
    }

    /**
     * Gets the value of the linkMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getLinkMetadata() {
        if (linkMetadata == null) {
            linkMetadata = new ArrayList<Object>();
        }
        return this.linkMetadata;
    }

}
