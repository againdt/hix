
package com.getinsured.iex.ssap.enums;

/**
 * 
 * @author raguram_p
 *
 *
 */
public enum InsuranceEndReasonEnum {

    PARENT_NO_LONGER_EMPLOYED,
    EMPLOYER_STOPPED_COVERAGE,
    EMPLOYER_STOPPED_DEPENDANT_COVERAGE,
    INSURANCE_TOO_EXPENSIVE,
    UNINSURED_MEDICAL_NEEDS,
    OTHER;

    public String value() {
        return name();
    }

    public static String value(String v) {
		if (v != null) {
			final InsuranceEndReasonEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static InsuranceEndReasonEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static InsuranceEndReasonEnum readValue(String v) {
		InsuranceEndReasonEnum data = null;
		for (InsuranceEndReasonEnum c : InsuranceEndReasonEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

}
