(function() {
'use strict';

angular.module('agencyApp').controller('AgencyDelegationCtrl',AgencyDelegationCtrl);

	AgencyDelegationCtrl.$inject = ['$http', '$state', 'StatusService'];

	function AgencyDelegationCtrl($http, $state, StatusService) {
		var vm = this;
        var agencyDelegationListUrl = '/hix/agency/delegations/Pending';

        vm.init = init;
        vm.getPagination = getPagination;
        vm.acceptDelegation = acceptDelegation;
        vm.getSorting = getSorting;
        vm.reset = reset;
        vm.isResultsFound = true;
		vm.agencyCertificationStatus = StatusService.CertificationStatus;
        function init(){
			if(vm.agencyCertificationStatus !== 'CERTIFIED') {
				$state.go("viewagentlist");
			}
        	vm.paginationData = {
				totalRecords: 0
			}

        	vm.inputData = {
        		'individualFirstName' : '',
        		'individualLastName' : '',
	        	'agentFirstName' : '',
	        	'agentLastName' : '',
	        	'fromDate' : '',
	        	'toDate' : '',
	        	'sortOrder ': undefined,
				'sortBy ': undefined,
				'pageNumber ': 1
        	}
        	vm.filterData = {};
			getPagination();
		}


        function getPagination(){
			vm.loader = true;

			if(vm.updateRefineResult){
				var properties = ['individualFirstName', 'individualLastName', 'agentFirstName', 'agentLastName', 'fromDate', 'toDate'];

				vm.filterData = {};

				for (var i = 0; i < properties.length; i ++) {
					if(vm.inputData[properties[i]] !== '' && vm.inputData[properties[i]] !== undefined) {
						vm.filterData[properties[i]] = vm.inputData[properties[i]];
					}
				}

				vm.inputData.pageNumber = 1;
			}else {
				if(vm.inputData.sortOrder && vm.inputData.sortOrder !== undefined) {
					vm.filterData.sortOrder = vm.inputData.sortOrder;
				}
				if(vm.inputData.sortBy && vm.inputData.sortBy !== undefined) {
					vm.filterData.sortBy = vm.inputData.sortBy;
				}
			}
			vm.filterData.pageNumber = vm.inputData.pageNumber;


			var promise = $http.post(agencyDelegationListUrl, vm.filterData);

			promise.success(function(response){
				vm.loader = false;
				vm.updateRefineResult = false;
				if (response.count > 0) {
					vm.delegations = response.delegations;

					vm.delegations.forEach(function(delegation){
						delegation.receivedOn = delegation.receivedOn? delegation.receivedOn.split(' ')[0] : delegation.receivedOn;
					})

					vm.paginationData.totalRecords =  response.count;
					vm.isResultsFound = true;
				}
				else {
					vm.isResultsFound = false;
				}
			})


			promise.error(function(response){
				vm.loader = false;
				vm.updateRefineResult = false;
			})
		}

		function acceptDelegation(delegations) {
			vm.loader = true;

			var acceptDelegationUrl = '/hix/agency/approveRequest/' + delegations.brokerId + '/' +  delegations.individualId;

			vm.consumerName = delegations.individualFirstName + ' ' + delegations.individualLastName;
			vm.agentName = delegations.agentFirstName + ' ' + delegations.agentLastName;
			var promise = $http.post(acceptDelegationUrl);
			promise.success(function(response){
				vm.loader = false;
				$('#successfullyAcceptedDelegation').modal();
			})
			promise.error(function(response){
				vm.loader = false;
				//$('#successfullyAcceptedDelegation').modal();
			})

		}

		function getSorting(column, $event){
			var id = $event.target.id;
			if (id.substr(id.length - 3) == "asc") {
				id = $event.target.id;
			} else if (id.substr(id.length - 4) == "desc"){
				id = $event.target.id;
			} else {
				$event.target.id = id + '_asc';
				id = $event.target.id;
			}
			vm.inputData.sortBy = column;
			if(vm.inputData.sortOrder === undefined || vm.inputData.sortOrder === null || vm.inputData.sortOrder === 'desc'){
				vm.inputData.sortOrder = 'asc';
				if(id.substr(id.length - 5) == '_desc'){
					$event.target.id = id.substr(0, id.length - 5) + '_asc';
				}
				id = $event.target.id;
			}else{
				vm.inputData.sortOrder = 'desc';
				if(id.substr(id.length - 4) == '_asc'){
					$event.target.id = id.substr(0, id.length - 4) + '_desc';
				}
				id = $event.target.id;
			}
			getPagination();
		}

		function reset() {
			vm.inputData = {};
			vm.filterData = {};
			getPagination();
		}
    }

})();
