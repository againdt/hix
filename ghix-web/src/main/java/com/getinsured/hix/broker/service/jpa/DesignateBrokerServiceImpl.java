package com.getinsured.hix.broker.service.jpa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.getinsured.hix.broker.repository.IDesignateBrokerRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.employees.EmployeeDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Service("designateService")
@Repository
public class DesignateBrokerServiceImpl implements DesignateService {
	private static final String QUERY_STRING_SEPERATOR = ", '";

	private static final int ELEVEN = 11;

	private static final int TEN = 10;

	private static final int NINE = 9;

	private static final int EIGHT = 8;

	private static final int SEVEN = 7;

	private static final int THREE = 3;

	private static final int FOUR = 4;

	private static final int FIVE = 5;

	private static final int SIX = 6;

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignateBrokerServiceImpl.class);

	private static final String INACTIVE_DATE_TO = "inactiveDateTo";
	private static final String DATE_FORMAT_MM_DD_YYYY = "mm/dd/yyyy";
	private static final String INACTIVE_DATE_FROM = "inactiveDateFrom";
	private static final String REQUEST_SENT_TO = "requestSentTo";
	private static final String REQUEST_SENT_FROM = "requestSentFrom";
	private static final String LAST_NAME = "lastName";
	private static final String FIRST_NAME = "firstName";
	private static final String BROKER_LIST = "brokerlist";
	private static final String HOUSEHOLD_LIST="householdlist";
	private static final String RECORD_COUNT = "recordCount";
	private static final String STATUS = "status";
	private static final String BROKER_ID = "brokerId";
	private static final String COMPANY_NAME = "companyName";
	private static final String ZERO = "0";
	private static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	private static final String EMPLOYER_ID = "employerId";
	private static final String ELIGIBILITY_STATUS = "eligibilityStatus";
 	private static final String EMP_STATUS = "empStatus";
 	private static final String IND_STATUS = "indStatus";

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String ACTIVE = "Active";
	private static final String EMPTY_STRING = "";
	private static final String BROKER_ID_PARAMETER = "brokerId";
	private static final String DESIGNATION_STATUS_PARAMETER = "designationStatus";
	private static final String EMPLOYEE_FIRST_NAME_PARAMETER = "employeeFirstName";
	private static final String EMPLOYEE_LAST_NAME_PARAMETER = "employeeLastName";
	private static final String EMPLOYEE_COMPANY_NAME_PARAMETER = "companyName";
	private static final String ENROLLMENT_STATUS_PARAMETER = "enrollmentStatus";
	private static final String INACTIVE_FROM_PARAMETER = "inactiveSinceDateFrom";
	private static final String INACTIVE_TO_PARAMETER = "inactiveSinceDateTo";
	private static final String PERCENT_SIGN = "%";
	private static final String NULL_STRING = "null";
	public static final String ENROLLMENT_STATUS = "enrollment_status";

	@Autowired
	private IDesignateBrokerRepository brokerDesignateRepository;

	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Autowired
	private BrokerIndTriggerService brokerIndTriggerService ;

	@Autowired
	private UserService userService;


	/**
	 * @see DesignateService#saveDesignateBroker(DesignateBroker)
	 */
	@Override
	public EntityResponseDTO saveDesignateBroker(DesignateBroker designateBroker) {
		EntityResponseDTO entityResponseDTO = null;
		
		try {
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				synchronized (this) {
					LOGGER.warn("Designate broker synchronized");
					DesignateBroker dBinDB= brokerDesignateRepository.findDesigBrokerByAgentAndIndividualAndStatus(designateBroker.getBrokerId(), designateBroker.getExternalIndividualId(), designateBroker.getStatus()) ;
				    LOGGER.warn("Designate broker record exists: "+ dBinDB);
				    if (   dBinDB == null  ) {
					     entityResponseDTO = brokerIndTriggerService.triggerInd47(designateBroker);
					     LOGGER.warn("Triggered IND47 response: "+ entityResponseDTO);
					     LOGGER.warn("IND47 response code: "+ entityResponseDTO.getResponseCode() + ", Status: "+entityResponseDTO.getStatus());

					     if(entityResponseDTO!=null && (entityResponseDTO.getResponseCode()==200 || BrokerConstants.RESPONSE_SUCCESS.equalsIgnoreCase(entityResponseDTO.getStatus()))){
						      LOGGER.warn("Pre updating Designate broker ...");
						      brokerDesignateRepository.save(designateBroker);
						      LOGGER.warn("Post updating Designate broker...");
					     }
				    }
				}
			} else {
				brokerDesignateRepository.save(designateBroker);
			}
			
		} catch(Exception exception){
			LOGGER.error("Exception occurred while designating Broker", exception);
		}
		
		return entityResponseDTO;
	}

	/**
	 * @see DesignateService#findByEmployerId(int)
	 */
	@Override
	@Transactional
	public long findByEmployerId(int employerId) {
		LOGGER.info("findByEmployerId : START");
		QueryBuilder<DesignateBroker> brokerQuery = delegateFactory.getObject();
		brokerQuery.buildObjectQuery(DesignateBroker.class);

		if (employerId != 0) {
			brokerQuery.applyWhere(EMPLOYER_ID, employerId, DataType.NUMERIC, ComparisonType.EQ);
		}
		LOGGER.info("findByEmployerId : END");
		return brokerQuery.getRecordCount();
	}

	/**
	 * @see DesignateService#findBrokerByEmployerId(int)
	 */
	@Override
	@Transactional
	public DesignateBroker findBrokerByEmployerId(int employerId) {
		LOGGER.info("findBrokerByEmployerId : START");
		LOGGER.info("findBrokerByEmployerId : END");
		return brokerDesignateRepository.findBrokerByEmployerId(employerId);
	}

	/**
	 * @see DesignateService#findDesignatedBroker(DesignateBroker, Integer,
	 *      Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findDesignatedBroker(DesignateBroker broker, Integer startRecord, Integer pageSize) {
		LOGGER.info("findDesignatedBroker : START");
		QueryBuilder<DesignateBroker> brokerQuery = delegateFactory.getObject();
		brokerQuery.buildObjectQuery(DesignateBroker.class);

		int brkId = (broker != null && broker.getBrokerId() != 0) ? broker.getBrokerId() : 0;
		if (brkId != 0) {
			brokerQuery.applyWhere(BROKER_ID, brkId, DataType.NUMERIC, ComparisonType.EQ);
		}

		String status = (broker != null && broker.getStatus() != null) ? broker.getStatus().toString() : "";
		if (!"".equals(status)) {
			brokerQuery.applyWhere(STATUS, status.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		List<DesignateBroker> brokers = brokerQuery.getRecords(startRecord, pageSize);

		Map<String, Object> brokerListAndRecordCount = new HashMap<String, Object>();
		brokerListAndRecordCount.put(BROKER_LIST, brokers);
		brokerListAndRecordCount.put(RECORD_COUNT, brokerQuery.getRecordCount());
		LOGGER.info("findDesignatedBroker : END");
		return brokerListAndRecordCount;
	}

	/**
	 * @see DesignateService#findDesignatedBroker(DesignateBroker, Integer,
	 *      Integer, String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findDesignatedBroker(DesignateBroker broker, Integer startRecord, Integer pageSize,
			String empStatus) {
		LOGGER.info("findDesignatedBroker : START");
		QueryBuilder<DesignateBroker> brokerQuery = delegateFactory.getObject();
		brokerQuery.buildObjectQuery(DesignateBroker.class);

		int brkId = (broker != null && broker.getBrokerId() != 0) ? broker.getBrokerId() : 0;
		if (brkId != 0) {
			brokerQuery.applyWhere(BROKER_ID, brkId, DataType.NUMERIC, ComparisonType.EQ);
		}
		if (empStatus != null) {
			brokerQuery.applyWhere(STATUS, Status.valueOf(empStatus), DataType.ENUM, ComparisonType.EQ);
		} else {
			brokerQuery.applyWhere(STATUS, Status.Pending, DataType.ENUM, ComparisonType.EQ);
		}
		List<DesignateBroker> brokers = brokerQuery.getRecords(startRecord, pageSize);
		Map<String, Object> brokerListAndRecordCount = new HashMap<String, Object>();
		brokerListAndRecordCount.put(BROKER_LIST, brokers);
		brokerListAndRecordCount.put(RECORD_COUNT, brokerQuery.getRecordCount().intValue());
		LOGGER.info("findDesignatedBroker : END");
		return brokerListAndRecordCount;
	}

	/**
	 * @see DesignateService#findEmployers(Map, String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findEmployers(Map<String, Object> searchCriteria, String empStatus) {
		LOGGER.info("findEmployers : START");
		List<DesignateBroker> brokers = new ArrayList<DesignateBroker>();
		Map<String, Object> employersListAndRecordCount = new HashMap<String, Object>();
		EntityManager em = null;
		try {

			String brkId = getValueFromSearchCriteria(searchCriteria, BROKER_ID);
			String firstName = getValueFromSearchCriteria(searchCriteria, FIRST_NAME);
			String lastName = getValueFromSearchCriteria(searchCriteria, LAST_NAME);
			String companyName = getValueFromSearchCriteria(searchCriteria, COMPANY_NAME);
			String requestSentFrom = getValueFromSearchCriteria(searchCriteria, REQUEST_SENT_FROM);
			String requestSentTo = getValueFromSearchCriteria(searchCriteria, REQUEST_SENT_TO);
			String inactiveDateFrom = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_FROM);
			String inactiveDateTo = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_TO);
			String status = getValueFromSearchCriteria(searchCriteria, STATUS);

			StringBuilder buildquery = buildQueryUsingSearchCriteria(empStatus, brkId, companyName, firstName, lastName, status);

			appendDateToQuery(requestSentFrom, requestSentTo, inactiveDateFrom, inactiveDateTo, buildquery);

			em = emf.createEntityManager();
			
			Query query = em.createNativeQuery(buildquery.toString());
			
			setQueryParameterForSearchCriteria(query, empStatus, brkId, companyName, firstName, lastName, status);
			
			setDateParameterToQuery(query, requestSentFrom, requestSentTo, inactiveDateFrom, inactiveDateTo);
			
			List rsList = query.getResultList();
			
			createBrokersList(brokers, rsList);

			employersListAndRecordCount.put(BROKER_LIST, brokers);
			employersListAndRecordCount.put(RECORD_COUNT, brokers.size());
		} 
		catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occurred while searching for Employers : ", giexception);
            throw giexception;
 		} 
		catch (Exception e) {
			LOGGER.error("Exception occurred while searching for Employers", e);			
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		LOGGER.info("findEmployers : END");
		return employersListAndRecordCount;
	}

	private String getValueFromSearchCriteria(Map<String, Object> searchCriteria, String id) {
		if (isSearchCritetiaAvailable(searchCriteria, id)) {
			return (String) searchCriteria.get(id);
		}
		return null;
	}

	private boolean isSearchCritetiaAvailable(Map<String, Object> searchCriteria, String id) {
		return searchCriteria != null && searchCriteria.get(id) != null
				&& !searchCriteria.get(id).equals("");
	}

	/**
	 * Builds search employers query
	 *
	 * @param empStatus
	 *            Employer status
	 * @param brokerId
	 *            Broker ID
	 * @param companyName
	 *            Employer company name
	 * @param firstName
	 *            Employer First Name
	 * @param lastName
	 *            Employer Last Name
	 * @param status
	 *            Eligibility status
	 * @return StringBuilder search query
	 */
	private StringBuilder buildQueryUsingSearchCriteria(String empStatus, String brokerId, String companyName,
			String firstName, String lastName, String eligibilityStatus) {
		LOGGER.info("buildQueryUsingSearchCriteria : START");

		StringBuilder buildquery = new StringBuilder();
		
		buildquery.append("SELECT DISTINCT EMPLOYERID, DA.CREATED, DA.UPDATED, DA.STATUS, DA.INDIVIDUALID, DA.ID FROM DESIGNATE_BROKER DA , EMPLOYERS EMP ");
		
		if (!StringUtils.isBlank(firstName) || !StringUtils.isBlank(lastName) || !StringUtils.isBlank(companyName)) {
			//HIX-33558
			//buildquery.append(" ,MODULE_USERS MU ");
			//buildquery.append(" WHERE DA.EMPLOYERID = MU.MODULE_ID ");
			buildquery.append(" WHERE DA.EMPLOYERID = EMP.ID ");
			//buildquery.append(" AND MU.MODULE_NAME = '" + ModuleUserService.EMPLOYER_MODULE + "'");
			
			if (!StringUtils.isBlank(brokerId)) {
				buildquery.append(" AND BROKERID = :" + BROKER_ID);	
			}
			
			BrokerMgmtUtils.appendToQuery(buildquery, "EMP.CONTACT_FIRST_NAME", firstName, FIRST_NAME, "LIKE", true, false);
			BrokerMgmtUtils.appendToQuery(buildquery, "EMP.CONTACT_LAST_NAME", lastName, LAST_NAME, "LIKE", true, false);
			BrokerMgmtUtils.appendToQuery(buildquery, "EMP.NAME", companyName, COMPANY_NAME, "LIKE", true, false);

		} 
		else {
			if (!StringUtils.isBlank(brokerId)) {
				buildquery.append(" WHERE BROKERID = :" + BROKER_ID);
			}
		}
		
		if (!StringUtils.isBlank(empStatus)) {
			buildquery.append(" AND DA.STATUS = :" + EMP_STATUS );
		}
		
		buildquery.append(" AND DA.EMPLOYERID = EMP.ID ");

		if (!StringUtils.isBlank(eligibilityStatus)) {
			buildquery.append(" AND NEW_COLUMN = :"+ELIGIBILITY_STATUS);
		}
		
		LOGGER.info("buildQueryUsingSearchCriteria : END");
		return buildquery;
	}

	/**
	 * Appends date criteria to builds query.
	 *
	 * @param requestSentFrom
	 *            Designate Request sent date
	 * @param requestSentTo
	 *            Designate request sent date
	 * @param inactiveDateFrom
	 *            Decline designation date
	 * @param inactiveDateTo
	 *            Decline designation date
	 * @param buildquery
	 *            query to which date criteria should be appended
	 */
	private void appendDateToQuery(String requestSentFrom, String requestSentTo, String inactiveDateFrom,
			String inactiveDateTo, StringBuilder buildquery) {
		LOGGER.info("appendDateToQuery : START");
		// Search For pending employer requests
		if (requestSentFrom != null && requestSentTo != null) {
			buildquery.append(" AND DA.CREATED BETWEEN TO_DATE(:" + REQUEST_SENT_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY+"') AND TO_DATE(:"
	 	 	 	 		      + REQUEST_SENT_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY+"') +1");
		} 
		else if (requestSentFrom != null && requestSentTo == null) {
			buildquery.append(" AND DA.CREATED >= TO_DATE(:" + REQUEST_SENT_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')");
		} 
		else if (requestSentFrom == null && requestSentTo != null) {
			buildquery.append(" AND DA.CREATED <= TO_DATE(:" + REQUEST_SENT_TO + QUERY_STRING_SEPERATOR+DATE_FORMAT_MM_DD_YYYY+"')+1");
		}

		searchInactiveEmployerRequest(inactiveDateFrom, inactiveDateTo,
				buildquery);
		
		LOGGER.info("appendDateToQuery : END");
	}

	private void searchInactiveEmployerRequest(String inactiveDateFrom,
			String inactiveDateTo, StringBuilder buildquery) {
		// Search For Inactive employer requests
		if (inactiveDateFrom != null && inactiveDateTo != null) {
			buildquery.append(" AND DA.UPDATED BETWEEN TO_DATE(:" + INACTIVE_DATE_FROM + QUERY_STRING_SEPERATOR+DATE_FORMAT_MM_DD_YYYY+"') AND TO_DATE(:"
								+ INACTIVE_DATE_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')+1");
		} 
		else if (inactiveDateFrom != null && inactiveDateTo == null) {
			buildquery.append(" AND DA.UPDATED >= TO_DATE(:" + INACTIVE_DATE_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')");
		} 
		else if (inactiveDateFrom == null && inactiveDateTo != null) {
			buildquery.append(" AND DA.UPDATED <= TO_DATE(:" + INACTIVE_DATE_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')+1");
		}
	}

	/**
	 * Creates Brokers list
	 *
	 * @param brokers
	 * @param rsList
	 */
	private void createBrokersList(List<DesignateBroker> brokers, List rsList) {
		LOGGER.info("createBrokersList : START");
		Iterator rsIterator = rsList.iterator();
		try{
			while (rsIterator.hasNext()) {
				DesignateBroker da = new DesignateBroker();
				Object[] objArray = (Object[]) rsIterator.next();
	
				da.setEmployerId(Integer.parseInt(objArray[0] != null ? objArray[0].toString() : ZERO));
	
				setDateForDesignateBroker(da, objArray);
	
				da.setStatus(Status.valueOf(objArray[THREE].toString()));
	
				da.setIndividualId(Integer.parseInt(objArray[FOUR] != null ? objArray[FOUR].toString() : ZERO));
	
				da.setId(Integer.parseInt(objArray[FIVE] != null ? objArray[FIVE].toString() : ZERO));
				brokers.add(da);
			}
		}catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occurred while creating broker list : ", giexception);
            throw giexception;
 		}
		catch(Exception exception)
		{
			LOGGER.error("Exception occurred while creating broker list: ",exception);
		}
		LOGGER.info("createBrokersList : END");
	}

	private void setDateForDesignateBroker(DesignateBroker da, Object[] objArray) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
		Date convertedDate = null;
		String s1 = objArray[1].toString();
		s1 = s1.substring(0, s1.indexOf(' ')).trim();
		String s2 = objArray[2].toString();
		s2 = s2.substring(0, s2.indexOf(' ')).trim();

		try {
			convertedDate = dateFormat.parse(s1);
		} catch (ParseException parseException) {
			LOGGER.error("Exception occured in createBrokersList while parsing date", parseException);
		}
		da.setCreated(convertedDate);

		try {
			convertedDate = dateFormat.parse(s2);
		} catch (ParseException parseException) {
			LOGGER.error("Exception occured in createBrokersList while parsing date", parseException);
		}
		da.setUpdated(convertedDate);
	}

	

	/**
	 * @see DesignateService#deDesignateEmployersForBroker(int)
	 */
	@Override
	@Transactional
	public int deDesignateEmployersForBroker(int brokerId) {
		LOGGER.info("deDesignateEmployersForBroker : START");
		LOGGER.info("deDesignateEmployersForBroker : END");
		return brokerDesignateRepository.deleteDesignateBrokerByBrokerId(brokerId);
	}

	/**
	 * @see DesignateService#findIndividuals(Map, String)
	 */
	@Override
	public Map<String, Object> findIndividuals(Map<String, Object> searchCriteria, String empStatus) {
		LOGGER.info("findIndividuals : START");
		List<DesignateBroker> brokers = new ArrayList<DesignateBroker>();
		List<Household> houseHoldList= new ArrayList<Household>();
		Map<String, Object> employersListAndRecordCount = new HashMap<String, Object>();
		EntityManager em = null;
		try {

			String brkId = getValueFromSearchCriteria(searchCriteria, BROKER_ID);
			String firstName = getValueFromSearchCriteria(searchCriteria, FIRST_NAME);
			String lastName = getValueFromSearchCriteria(searchCriteria, LAST_NAME);
			String requestSentFrom = getValueFromSearchCriteria(searchCriteria, REQUEST_SENT_FROM);
			String requestSentTo = getValueFromSearchCriteria(searchCriteria, REQUEST_SENT_TO);
			String inactiveDateFrom = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_FROM);
			String inactiveDateTo = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_TO);
			String status = getValueFromSearchCriteria(searchCriteria, STATUS);

			StringBuilder buildquery = buildQueryForIndividualsUsingSearchCriteria(empStatus, brkId, firstName, lastName, status);

			appendDateToQuery(requestSentFrom, requestSentTo, inactiveDateFrom, inactiveDateTo, buildquery);

			em = emf.createEntityManager();
			
			Query query = em.createNativeQuery(buildquery.toString());
	 	 	 		                
            setParameterizedQueryForIndividuls(query, empStatus, brkId, firstName, lastName, status);
            
            setDateParameterToQuery(query,requestSentFrom, requestSentTo, inactiveDateFrom, inactiveDateTo);
            
            List rsList = query.getResultList();
            
			createBrokersList(brokers, rsList);
			createHouseholdList(houseHoldList,rsList);
			employersListAndRecordCount.put(HOUSEHOLD_LIST, houseHoldList);
			employersListAndRecordCount.put(BROKER_LIST, brokers);
			employersListAndRecordCount.put(RECORD_COUNT, brokers.size());
		} 
		catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occurred while searching for individuals : ", giexception);
           throw giexception;
		}
		catch (Exception e) {
			LOGGER.error("Exception occurred while searching for individuals", e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		LOGGER.info("findIndividuals : END");
		return employersListAndRecordCount;
	}

	private void createHouseholdList(List<Household> houseHoldList, List rsList) {
		LOGGER.info("createHouseholdList : START");
		Iterator rsIterator = rsList.iterator();
		try{
			while (rsIterator.hasNext()) {
				Household da = new Household();
				Object[] objArray = (Object[]) rsIterator.next();

				da.setId(Integer.parseInt(objArray[FOUR] != null ? objArray[FOUR].toString() : ZERO));
				
				da.setFirstName( objArray[SIX]!=null?  objArray[SIX].toString():null );//6
				da.setLastName(objArray[SEVEN]!=null?objArray[SEVEN].toString():null );//7
				da.setPhoneNumber(objArray[EIGHT]!=null ?objArray[EIGHT].toString():null );//8
				da.setEmail(objArray[NINE] !=null?objArray[NINE].toString():null );//9
				da.setHouseHoldIncome(Double.parseDouble(objArray[TEN]!=null?objArray[TEN].toString():ZERO));//10
				da.setFamilySize(Integer.parseInt(objArray[ELEVEN] != null ? objArray[ELEVEN].toString() : ZERO));//11
				
				houseHoldList.add(da);
			}
		}catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occurred while creating createHouseholdList: ", giexception);
            throw giexception;
 		}catch(Exception exception){
			LOGGER.error("Exception occurred while creating createHouseholdList: ",exception);
		}		
		LOGGER.info("createHouseholdList : END");
		
	}

	/**
	 * @see DesignateService#findEmployees(Map, String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeDTO> findEmployees(Map<String, Object> searchCriteria, String desigStatus) {

		LOGGER.info("findEmployees : START");

		List<EmployeeDTO> employeeList = new ArrayList<EmployeeDTO>();

		EntityManager em = emf.createEntityManager();

		List<Object[]> employeeListResult = null;
		
		String brokerId = getValueFromSearchCriteria(searchCriteria, BROKER_ID);
		String employeeFirstName = getValueFromSearchCriteria(searchCriteria, FIRST_NAME);
		String employeeLastName = getValueFromSearchCriteria(searchCriteria, LAST_NAME);
		String inactiveSinceDateFrom = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_FROM);
		String inactiveSinceDateTo = getValueFromSearchCriteria(searchCriteria, INACTIVE_DATE_TO);
		String enrollmentStatus = getValueFromSearchCriteria(searchCriteria, ENROLLMENT_STATUS);
		String companyName = getValueFromSearchCriteria(searchCriteria, COMPANY_NAME);
		//broker id criteria ends

		//inactive since criteria ends

		//Make Employee Object
		EmployeeDTO employee = new EmployeeDTO();

		employee.setBrokerId(Integer.valueOf(brokerId));
		employee.setDesignationStatus(desigStatus);
		employee.setEmployeeFirstName(employeeFirstName);
		employee.setEmployeeLastName(employeeLastName);
		employee.setCompanyName(companyName);
		employee.setEnrollmentStatus(enrollmentStatus);
		employee.setInactiveSinceFromDate(inactiveSinceDateFrom);
		employee.setInactiveSinceToDate(inactiveSinceDateTo);

		//build query to find employees and set parameters
		Query  query = buildQueryObjectForEmployees(employee, em);

		try {

			employeeListResult = query.getResultList();

		} catch(GIRuntimeException giexception) {
    		LOGGER.error("Exception occurred while searching for Employees : ", giexception);
            throw giexception;
 		} catch (Exception e) {

			LOGGER.error("Exception occurred while searching for Employees", e);

		} finally {

			if ( query != null ) {

				query = null;
			}

			if ( em != null && em.isOpen() ) {
				em.clear();
				em.close();
			}
		}

		employeeList = populateEmployeeList(employeeList, employeeListResult, desigStatus);

		LOGGER.info("findEmployees : END");

		return employeeList;
	}

	/**
	 * Method to build Query object to search employees
	 *
	 * @param employee Object of EmployeeDTO
	 * @param em EntityManager
	 *
	 * @return Query JPA query object
	 */
	private Query buildQueryObjectForEmployees(EmployeeDTO employee, EntityManager em) {

		LOGGER.info("buildQueryObjectForEmployees : START");

		Query query = null;
		StringBuilder buildquery = new StringBuilder();
		/**
		 * @author sahay_b
		 * Change in Query to show record even if the employee is not enrolled. 
		 * HIX-22790
		 */
		buildquery.append("	SELECT distinct(e.id),	\n").append("	  db.brokerid,	\n");
		buildquery.append("	  md.first_name	\n").append("	  ||' '	\n").append("	  ||md.last_name,	\n");
		
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			buildquery.append("	  er.name,	\n").append("	  COALESCE(lv.lookup_value_label, 'NOT ENROLLED'),	\n").append("	  ee.effective_start_date,	\n");
		}else{
			buildquery.append("	  er.name,	\n").append("	  NVL(lv.lookup_value_label, 'NOT ENROLLED'),	\n").append("	  ee.effective_start_date,	\n");
		}
		
		
		buildquery.append("	  ee.effective_end_date,	\n").append("	  db.updated,	\n").append("	  md.contact_phone_number,	\n");
		buildquery.append("	  md.email_address	\n").append("	FROM employees e	\n").append("	JOIN employers er	\n").append("	ON e.employer_id = er.id	\n");
		buildquery.append("	JOIN designate_broker db	\n");
		buildquery.append("	ON er.id = db.employerid	\n");
		buildquery.append("	left outer JOIN enrollment en	\n");
		buildquery.append("	ON en.employee_id = e.id	\n");
		buildquery.append("	left outer JOIN enrollee ee	\n");
		buildquery.append("	ON ee.enrollment_id = en.id and ee.effective_start_date is not null		\n");
		buildquery.append("	left outer JOIN lookup_value lv	\n");
		buildquery.append("	ON lv.lookup_value_Id = en.enrollment_status_lkp	\n");
		buildquery.append("	left outer JOIN lookup_type lt	\n");
		buildquery.append("	ON lt.lookup_type_Id = lv.lookup_type_Id	\n");
		buildquery.append("	AND upper(trim(lt.name))   = upper(trim('enrollment_status'))	\n");
		buildquery.append("	JOIN employee_member_details md	\n");
		buildquery.append("	ON md.employee_id            = e.id	\n");
		buildquery.append("	WHERE upper(trim(md.type))   = upper(trim('EMPLOYEE'))	\n");
		buildquery.append("AND e.status <> 'DELETED' \n ");
		if ( isEmptyOrNull(EMPTY_STRING + employee.getBrokerId()) ) {
			buildquery.append("and db.brokerid = :brokerId ");
		}

		if ( isEmptyOrNull(employee.getDesignationStatus()) ) {
			buildquery.append("and  UPPER(db.status) =  UPPER(:designationStatus) ");
		}

		if ( isEmptyOrNull(employee.getEmployeeFirstName()) ) {
			buildquery.append("and UPPER(md.first_name) like UPPER(:employeeFirstName) ");
		}

		if ( isEmptyOrNull(employee.getEmployeeLastName()) ) {
			buildquery.append("and UPPER(md.last_name) like UPPER(:employeeLastName) ");
		}

		if ( isEmptyOrNull(employee.getCompanyName()) ) {
			buildquery.append("and UPPER(er.name) like UPPER(:companyName) ");
		}

		//when designation status of employee is Active
		if ( isEmptyOrNull(employee.getEnrollmentStatus()) ) {
			buildquery.append("and TRIM(UPPER(lv.lookup_value_label)) = TRIM(UPPER(:enrollmentStatus)) ");
		}

		//when designation status of employee is InActive
		if ( employee.getInactiveSinceFromDate() != null && employee.getInactiveSinceToDate() != null ) {
			buildquery.append("and db.updated between TO_DATE(:inactiveSinceDateFrom, 'mm/dd/yyyy') ");
			buildquery.append("and TO_DATE(:inactiveSinceDateTo, 'mm/dd/yyyy') + interval '86399' second ");
		}		
		buildquery.append("and ( en.id is null or en.id in (select max(enr.id) from enrollment enr where enr.employee_id = en.employee_id )) ");				
		buildquery.append("order by e.id ");

		query = em.createNativeQuery(buildquery.toString());

		query = applyQueryParametersForEmployeesQuery(employee, query);

		return query;
	}

	private boolean isEmptyOrNull(String desigStatus) {
		return desigStatus != null && !desigStatus.isEmpty();
	}

	/**
	 * Method to apply query parameters to employee search query
	 * @param employee employee dto object
	 * @param query object
	 * @return query object after applying parameters
	 */
	private Query applyQueryParametersForEmployeesQuery(EmployeeDTO employee, Query query) {

		LOGGER.info("applyQueryParametersForEmployeesQuery : START");

		String brokerId = ""+employee.getBrokerId();
		String desigStatus = employee.getDesignationStatus();
		String employeeFirstName = employee.getEmployeeFirstName();
		String employeeLastName = employee.getEmployeeLastName();
		String companyName = employee.getCompanyName();
		String enrollmentStatus = employee.getEnrollmentStatus();
		String inactiveSinceDateFrom = employee.getInactiveSinceFromDate();
		String inactiveSinceDateTo = employee.getInactiveSinceToDate();

		if ( isEmptyOrNull(brokerId) ) {
			query.setParameter(BROKER_ID_PARAMETER, Integer.valueOf(brokerId));
		}

		if ( isEmptyOrNull(desigStatus) ) {
			query.setParameter(DESIGNATION_STATUS_PARAMETER, desigStatus);
		}

		if ( isEmptyOrNull(employeeFirstName) ) {
			query.setParameter(EMPLOYEE_FIRST_NAME_PARAMETER, PERCENT_SIGN+employeeFirstName+PERCENT_SIGN);
		}

		if ( isEmptyOrNull(employeeLastName) ) {
			query.setParameter(EMPLOYEE_LAST_NAME_PARAMETER, PERCENT_SIGN+employeeLastName+PERCENT_SIGN);
		}

		if ( isEmptyOrNull(companyName) ) {
			query.setParameter(EMPLOYEE_COMPANY_NAME_PARAMETER, PERCENT_SIGN+companyName+PERCENT_SIGN);
		}

		//when designation status of employee is Active
		if ( isEmptyOrNull(enrollmentStatus) ) {
			query.setParameter(ENROLLMENT_STATUS_PARAMETER, enrollmentStatus);
		}

		//when designation status of employee is InActive
		if ( inactiveSinceDateFrom != null && inactiveSinceDateTo != null ) {
			query.setParameter(INACTIVE_FROM_PARAMETER, inactiveSinceDateFrom);
			query.setParameter(INACTIVE_TO_PARAMETER, inactiveSinceDateTo);
		}

		LOGGER.info("applyQueryParametersForEmployeesQuery : END");

		return query;
	}


	/**
	 * Method to populate employee list from query result
	 *
	 * @param employeeList list of designated employees for broker
	 * @param employeeListQueryResult employee list returned by query
	 * @param desigStatus designation Status
	 * @return list of designated employees for broker
	 */
	private List<EmployeeDTO> populateEmployeeList(List<EmployeeDTO> employeeList , List<Object[]> employeeListQueryResult, String desigStatus) {

		LOGGER.info("populateEmployeeList : START");

		EmployeeDTO employee = null;

		//Query returns each row in this order:
		//employee id, broker id, employee name, company name, enrollment status,
		//effective date, renewal date, inactive since date, phone number, email
		String strEnrollmentStatus =  null;
		String strEffectiveDate = null;
		String strRenewalDate = null;
		String strInactiveSinceDate = null;
		String strEmployeePhoneNumber = null;
		String strEmployeeEmail = null;

		if ( employeeListQueryResult != null && !employeeListQueryResult.isEmpty() ) {

			for ( Object[] eachRowInResultSet: employeeListQueryResult ) {

				employee = new EmployeeDTO();

				String strEmployeeId = getResultSetValue(eachRowInResultSet, 0);
				String strBrokerId = getResultSetValue(eachRowInResultSet, 1);
				String strEmployeeName = getResultSetValue(eachRowInResultSet, 2);
				String strCompanyName = getResultSetValue(eachRowInResultSet, THREE);
				
				if ( desigStatus != null && desigStatus.equalsIgnoreCase(ACTIVE)) {

					strEnrollmentStatus = getResultSetValue(eachRowInResultSet, FOUR);
					strEffectiveDate = getResultSetValue(eachRowInResultSet, FIVE);
					strRenewalDate = getResultSetValue(eachRowInResultSet, SIX);
				}
				
				strInactiveSinceDate = getResultSetValue(eachRowInResultSet, SEVEN);
				strEmployeePhoneNumber = getResultSetValue(eachRowInResultSet, EIGHT);
				strEmployeeEmail = getResultSetValue(eachRowInResultSet, NINE);
				
				employee.setId(getEmployeeID(strEmployeeId));
				employee.setBrokerId(getBrokerID(strBrokerId));
				employee.setEmployeeName(getEmployeeName(strEmployeeName));
				employee.setCompanyName(getCompanyName(strCompanyName));
				employee.setEnrollmentStatus(getEnrollmentStatus(strEnrollmentStatus));
				employee.setEffectiveDate(getEffectiveDate(strEffectiveDate));
				employee.setRenewalDate(getRenewalDate(strRenewalDate));
				employee.setInactiveSinceDate(getInactiveSinceDate(strInactiveSinceDate));
				employee.setEmployeePhoneNumber(getEmployeePhoneNumber(strEmployeePhoneNumber));
				employee.setEmployeeEmail(getEmployeeEmail(strEmployeeEmail));

				employeeList.add(employee);
			}
		}

		LOGGER.info("populateEmployeeList : END");

		return employeeList;
	}

	private String getResultSetValue(Object[] eachRowInResultSet, int index) {
		return eachRowInResultSet[index] != null ? eachRowInResultSet[index].toString() : null;
	}

	/**
	 * Method to get Employee Id
	 * @param strEmployeeId string employee id
	 * @return integer employee id
	 */
	private int getEmployeeID(String strEmployeeId) {

		int employeeId = -1;

		if ( isEmptyOrNull(strEmployeeId) && !strEmployeeId.trim().equalsIgnoreCase(NULL_STRING)) {
			employeeId = Integer.valueOf(strEmployeeId);
		}
		else {
			employeeId = -1;
		}

		return employeeId;
	}

	/**
	 * Method to get Broker Id
	 * @param strBrokerId string broker id
	 * @return integer broker id
	 */
	private int getBrokerID(String strBrokerId) {

		int brokerId = -1;

		if ( isEmptyOrNull(strBrokerId) && !strBrokerId.trim().equalsIgnoreCase(NULL_STRING)) {
			brokerId = Integer.valueOf(strBrokerId);
		}
		else {
			brokerId = -1;
		}
		return brokerId;
	}


	/**
	 * Method to get Employee Name
	 * @param  strEmployeeName
	 * @return strEmployeeName
	 */
	private String getEmployeeName(String strEmployeeName) {

		if ( isEmptyOrNull(strEmployeeName) && strEmployeeName.trim().equalsIgnoreCase(NULL_STRING)) {
			return "";
		}

		return strEmployeeName;
	}

	/**
	 * Method to get Company Name
	 * @param  strCompanyName
	 * @return strCompanyName
	 */
	private String getCompanyName(String strCompanyName) {

		if ( isEmptyOrNull(strCompanyName) && strCompanyName.trim().equalsIgnoreCase(NULL_STRING)) {
			return "";
		}

		return strCompanyName;
	}

	/**
	 * Method to get Enrollment Status
	 * @param  strEnrollmentStatus
	 * @return strEnrollmentStatus
	 */
	private String getEnrollmentStatus(String strEnrollmentStatus) {

		if ( isEmptyOrNull(strEnrollmentStatus) && strEnrollmentStatus.trim().equalsIgnoreCase(NULL_STRING)) {
			return "";
		}

		return strEnrollmentStatus;
	}

	/**
	 * Method to get Effective Date
	 * @param  strEffectiveDate
	 * @return effectiveDate
	 */
	private Date getEffectiveDate(String strEffectiveDate) {

		Date effectiveDate = null;

		if ( isEmptyOrNull(strEffectiveDate) && !strEffectiveDate.trim().equalsIgnoreCase(NULL_STRING)) {
			try {
				effectiveDate = formatDate(strEffectiveDate);
			} catch(GIRuntimeException giexception) {
	    		LOGGER.error("Exception occurred while searching for Employees : effective date: ", giexception);
	            throw giexception;
	 		}catch (Exception e) {
				LOGGER.error("Exception occurred while searching for Employees : effective date", e);
			}
			//strEffectiveDate = effectiveDate.toString();
		}

		return effectiveDate;
	}

	/**
	 * Method to get Renewal Date
	 * @param  strRenewalDate
	 * @return renewaLDate
	 */
	private Date getRenewalDate(String strRenewalDate) {

		Date renewalDate = null;

		if ( isEmptyOrNull(strRenewalDate) && !strRenewalDate.trim().equalsIgnoreCase(NULL_STRING)) {
			try {
				renewalDate = formatDate(strRenewalDate);
			} catch(GIRuntimeException giexception) {
	    		LOGGER.error("Exception occurred while searching for Employees : renewal date: ", giexception);
	            throw giexception;
	 		}catch (Exception e) {
				LOGGER.error("Exception occurred while searching for Employees : renewal date", e);
			}
			//strRenewalDate = renewalDate.toString();
		}

		return renewalDate;
	}

	/**
	 * Method to get Inactive Since Date
	 * @param  strInactiveSinceDate
	 * @return inactiveSinceDate
	 */
	private Date getInactiveSinceDate(String strInactiveSinceDate) {

		Date inactiveSinceDate = null;

		if ( isEmptyOrNull(strInactiveSinceDate) && !strInactiveSinceDate.trim().equalsIgnoreCase(NULL_STRING)) {
			try {
				inactiveSinceDate = formatDate(strInactiveSinceDate);
			}catch(GIRuntimeException giexception) {
	    		LOGGER.error("Exception occurred while searching for Employees : inactive since date: ", giexception);
	            throw giexception;
	 		} catch (Exception e) {
				LOGGER.error("Exception occurred while searching for Employees : inactive since date", e);
			}


			//strInactiveSinceDate = (inactiveSinceDate != null ? inactiveSinceDate.toString() : "");
		}
/*		else {
			//strInactiveSinceDate = "";
		}*/

		return inactiveSinceDate;
	}


	/**
	 * Method to get employee phone number
	 * @param  strEmployeePhoneNumber
	 * @return strEmployeePhoneNumber
	 */
	private String getEmployeePhoneNumber(String strEmployeePhoneNumber) {

		if ( isEmptyOrNull(strEmployeePhoneNumber) && strEmployeePhoneNumber.trim().equalsIgnoreCase(NULL_STRING)) {
			return "";
		}

		return strEmployeePhoneNumber;
	}

	/**
	 * Method to get employee email
	 * @param  strEmployeeEmail
	 * @return strEmployeeEmail
	 */
	private String getEmployeeEmail(String strEmployeeEmail) {

		if ( isEmptyOrNull(strEmployeeEmail) && strEmployeeEmail.trim().equalsIgnoreCase(NULL_STRING)) {
			return "";
		}

		return strEmployeeEmail;
	}

	/**
	 * Method to formatDate
	 *
	 *      Formats the given date into format defined in
	 *      DATE_FORMAT
	 *
	 * @param date
	 *            the date to be formatted
	 * @return java.util.Date formatted date
	 * @throws ParseException 
	 */
	private Date formatDate(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		return format.parse(date);
	}


	/**
	 * Builds search individuals query
	 *
	 * @param indStatus
	 *            Individual status
	 * @param brokerId
	 *            Broker ID
	 * @param firstName
	 *            Individual First Name
	 * @param lastName
	 *            Individual Last Name
	 * @param eligibilityStatus
	 *            Eligibility status
	 * @return StringBuilder search query
	 */
	private StringBuilder buildQueryForIndividualsUsingSearchCriteria(String indStatus, String brokerId,
			String firstName, String lastName, String eligibilityStatus) {
		LOGGER.info("buildQueryForIndividualsUsingSearchCriteria : START");

		StringBuilder buildquery = new StringBuilder();
		buildquery.append("SELECT DISTINCT EMPLOYERID, DA.CREATED, DA.UPDATED, DA.STATUS, DA.INDIVIDUALID, DA.ID,CMR.FIRST_NAME, CMR.LAST_NAME, CMR.PHONE_NUMBER, CMR.EMAIL_ADDRESS, CMR.HOUSEHOLD_INCOME, CMR.FAMILY_SIZE FROM DESIGNATE_BROKER DA , CMR_HOUSEHOLD CMR ");
		
		if (!StringUtils.isBlank(brokerId)) {
			buildquery.append(" WHERE BROKERID = :" + BROKER_ID);
		}

		if (!StringUtils.isBlank(indStatus)) {
			buildquery.append(" AND DA.STATUS = :" + IND_STATUS);
		}
		
		if (!StringUtils.isBlank(firstName)) {
			buildquery.append(" AND UPPER(CMR.FIRST_NAME) LIKE UPPER(:" + FIRST_NAME + ")");
		}
		
		if (!StringUtils.isBlank(lastName)) {
			buildquery.append(" AND UPPER(CMR.LAST_NAME) LIKE UPPER(:" + LAST_NAME + ")");
		}
		
		buildquery.append(" AND DA.INDIVIDUALID = CMR.ID ");

		if (!StringUtils.isBlank(eligibilityStatus)) {
			buildquery.append(" AND UPPER(EXTIND.ELIGIBILITY_STATUS) = UPPER(:" + ELIGIBILITY_STATUS + ")");
		}
		
		LOGGER.info("buildQueryForIndividualsUsingSearchCriteria : END");
		return buildquery;

	}

	/**
	 * @see DesignateService#findBrokerByIndividualId(int)
	 */
	@Override
	@Transactional
	public DesignateBroker findBrokerByIndividualId(int individualId) {
		LOGGER.info("findBrokerByIndividualId : START");
		LOGGER.info("findBrokerByIndividualId : END");
		return brokerDesignateRepository.findBrokerByIndividualId(individualId);
	}

	@Override
	public DesignateBroker findBrokerByExternalEmployerId(int extEmployerId) {
		LOGGER.info("findBrokerByExternalEmployerId : START");
		LOGGER.info("findBrokerByExternalEmployerId : END");
		return brokerDesignateRepository.findBrokerByExternalEmployerId(extEmployerId);
	}

	@Override
	public DesignateBroker findBrokerByExternalIndividualId(int extIndividualId) {
		LOGGER.info("findBrokerByExternalIndividualId : START");
		LOGGER.info("findBrokerByExternalIndividualId : END");
		return brokerDesignateRepository.findBrokerByExternalIndividualId(extIndividualId);
	}

	/**
	 * @see DesignateService#updateUserPreference(Character, DesignateBroker)
	 */
	@Override
	public void updateUserPreference(Character showPopupInFuture, DesignateBroker designateBrokerObj) {
		// If value for showPopupInFuture is 'N' means Don't show pop-up message
		// to designated broker
		// again for Employer / Individual while switching.
		LOGGER.info("updateUserPreference : START");
		Map<String, Object> brokerListAndRecordCount = null;
		if (showPopupInFuture != null) {
			Map<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(BROKER_ID, String.valueOf(designateBrokerObj.getBrokerId()));

			// Checking if broker is switching for Employer / Individual and
			// then fetching all the Employers/ Individuals assigned to that
			// broker.
			if (designateBrokerObj.getEmployerId() != null) {
				brokerListAndRecordCount = findEmployers(searchCriteria, DesignateBroker.Status.Active.toString());
			} else if (designateBrokerObj.getIndividualId() != null) {
				brokerListAndRecordCount = findIndividuals(searchCriteria, DesignateBroker.Status.Active.toString());
			}

			updateShowPopUpInDB(showPopupInFuture, brokerListAndRecordCount);
		}
		LOGGER.info("updateUserPreference : END");
	}

	private void updateShowPopUpInDB(Character showPopupInFuture,
			Map<String, Object> brokerListAndRecordCount) {
		try {
			// Iterating over the associated Employer / Individual Designate
			// Broker records to set flag to 'N'
			if (brokerListAndRecordCount != null) {
				List<DesignateBroker> designateBrokersList = brokerListAndRecordCount.get(BROKER_LIST) != null ? (List<DesignateBroker>) brokerListAndRecordCount
						.get(BROKER_LIST) : null;
				if (designateBrokersList != null) {
					Iterator<DesignateBroker> itr = designateBrokersList.iterator();
					
					AccountUser user = userService.getLoggedInUser();
					
					while (itr.hasNext()) {
						DesignateBroker designateBroker = itr.next();
						// Retrieving designateBroker record based on primary
						// key, id.
						designateBroker = brokerDesignateRepository.findDesignateBrokerById(designateBroker.getId());

						designateBroker.setUpdated(new TSDate());
						designateBroker.setUpdatedBy(user.getId());
						designateBroker.setShow_switch_role_popup(showPopupInFuture);
						
						saveDesignateBroker(designateBroker);
					}
				}
			}
		}
		catch (InvalidUserException ex) {
			LOGGER.error("Invalid User Exception occurred", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
	}

	/**
	 * @see DesignateService#findActiveIndividualForBroker(int, Status)
	 */
	@Override
	public List<DesignateBroker> findActiveIndividualForBroker(int brokerId, DesignateBroker.Status status) {
		LOGGER.info("findActiveIndividualForBroker : START");
		LOGGER.info("findActiveIndividualForBroker : END");
		return brokerDesignateRepository.findDesignateBrokerForExternalInd(brokerId, status);
	}
	
	@Override
	public List<DesignateBroker> findActiveAndPendingDesignateBrokers(int brokerId, List<DesignateBroker.Status> listOfStatus) {
		return brokerDesignateRepository.findActiveAndPendingDesignateBrokers(brokerId, listOfStatus);
	}

	/**
	 * @see DesignateService#findDesigBrokerByAgentId(int brokerId)
	 */
	@Override
	public List<DesignateBroker> findDesigBrokerByAgentId(int brokerId) {
		LOGGER.info("findDesigBrokerByAgentId : START");
		LOGGER.info("findDesigBrokerByAgentId : END");
		return brokerDesignateRepository.findDesigBrokerByAgentId(brokerId);
	}
	
	 /**
	  * Function to set query parameters for search criteria.
	  *  
	  * @param query The Query instance
	  * @param empStatus The employer status
	  * @param brokerId The Broker identifier.
	  * @param companyName The company name.
	  * @param firstName First Name.
	  * @param lastName Last name.
	  * @param eligibilityStatus The eligibility status.
	  * @param enrollmentStatus The enrollment status.
	 */
	private void setQueryParameterForSearchCriteria(Query query, String empStatus, String brokerId, String companyName, String firstName, String lastName, String eligibilityStatus) {
	 	 	 		                
            LOGGER.info("setQueryParameterForSearchCriteria: START");

            if (!StringUtils.isBlank(brokerId)) {
            	query.setParameter(BROKER_ID, brokerId);  
            }
            
            if (!StringUtils.isBlank(firstName) || !StringUtils.isBlank(lastName)
                            || !StringUtils.isBlank(companyName)) {
                    if (!StringUtils.isBlank(firstName)) {
                            query.setParameter(FIRST_NAME, PERCENT_SIGN+firstName.trim()+PERCENT_SIGN);
                    }
                    if (!StringUtils.isBlank(lastName)) {
                            query.setParameter(LAST_NAME, PERCENT_SIGN+lastName.trim()+PERCENT_SIGN);
                    }
                    if (!StringUtils.isBlank(companyName)) {
                            query.setParameter(COMPANY_NAME, PERCENT_SIGN+companyName.trim()+PERCENT_SIGN);
                    }
                    
            }
            
            if (!StringUtils.isBlank(empStatus)) {
            	query.setParameter(EMP_STATUS, empStatus.trim());
            }

            if (!StringUtils.isBlank(eligibilityStatus)) {
                query.setParameter(ELIGIBILITY_STATUS, eligibilityStatus.trim());
            }
            
            LOGGER.info("setQueryParameterForSearchCriteria: END");
 	 }
	 
	 /**
	  * Function to set date parameters in query.
	  * 
	  * @param query The Query instance.
	  * @param requestSentFrom The requested sender information.
	  * @param requestSentTo The requested receiver information.
	  * @param inactiveDateFrom The inactive date begin information.
	  * @param inactiveDateTo The inactive date end information.
	  */
	private void setDateParameterToQuery(Query query, String requestSentFrom, String requestSentTo, String inactiveDateFrom, String inactiveDateTo){
            LOGGER.info("setDateParameterToQuery: START");
            // set parameters for pending employer requests
            BrokerMgmtUtils.setQueryParameter(query, REQUEST_SENT_FROM, requestSentFrom, false);
            BrokerMgmtUtils.setQueryParameter(query, REQUEST_SENT_TO, requestSentTo, false);
            BrokerMgmtUtils.setQueryParameter(query, INACTIVE_DATE_FROM, inactiveDateFrom, false);
            BrokerMgmtUtils.setQueryParameter(query, INACTIVE_DATE_TO, inactiveDateTo, false);

            LOGGER.info("setDateParameterToQuery: END");
    }
	
	/**
	 * Function to set parameters to Individual's query.
	 * 
	 * @param indStatus The Individual's Status.
	 * @param brkId The Broker identifier.
	 * @param firstName The first name of individual. 
	 * @param lastName The last name of individual.
	 * @param eligibilityStatus  The eligibility status
	 * @param query The Query instance.
	 * @return query The modified Query instance.
	 */
	private Query setParameterizedQueryForIndividuls(Query query, String indStatus, String brkId, String firstName,
			String lastName, String eligibilityStatus) {	 	 	 		                
        LOGGER.info("setParameterizedQueryForIndividuls: START");

        if(!StringUtils.isBlank(brkId)) {
        	query.setParameter(BROKER_ID, brkId);
        }
        
        if(!StringUtils.isBlank(indStatus)) {
        	query.setParameter(IND_STATUS, indStatus.trim());
        }
        
        if(!StringUtils.isBlank(firstName)) {
                query.setParameter(FIRST_NAME, PERCENT_SIGN + firstName.trim() + PERCENT_SIGN);
        }
        
        if(!StringUtils.isBlank(lastName)) {
                query.setParameter(LAST_NAME, PERCENT_SIGN + lastName.trim() + PERCENT_SIGN);
        }

		if(!StringUtils.isBlank(eligibilityStatus)) {
		        query.setParameter(ELIGIBILITY_STATUS, eligibilityStatus.trim());
		}
		
		LOGGER.info("setParameterizedQueryForIndividuls: ENDS");		        
		return query;
	}
	
	public Integer deDesignateIndividualForBroker(DesignateBroker designateBroker) throws InvalidUserException
	{
		AccountUser user = userService.getLoggedInUser();

		designateBroker.setUpdated(new TSDate());
		designateBroker.setUpdatedBy(user.getId());
		designateBroker.setStatus(DesignateBroker.Status.InActive);
		saveDesignateBroker(designateBroker);
		return designateBroker.getIndividualId();
		
	}
}
