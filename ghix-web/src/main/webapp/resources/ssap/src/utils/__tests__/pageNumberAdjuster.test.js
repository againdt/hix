import { pageNumberAdjuster }  from '../pageNumberAdjuster';

describe('Test for page number adjuster: ', () => {

    it('Page number is not a number (actually string)', () => {
        expect(pageNumberAdjuster('hello')).toBe('page_00');
    });

    it('Page number is not a number (actually array)', () => {
        expect(pageNumberAdjuster(['hello'])).toBe('page_00');
    });

    it('Page number less than 0', () => {
        expect(pageNumberAdjuster(-5)).toBe('page_00');
    });

    it('Page number less than 10', () => {
        expect(pageNumberAdjuster(3)).toBe('page_03');
    });

    it('Page number is equal to 10', () => {
        expect(pageNumberAdjuster(10)).toBe('page_10');
    });

    it('Page number more than 10', () => {
        expect(pageNumberAdjuster(11)).toBe('page_11');
    });
});

