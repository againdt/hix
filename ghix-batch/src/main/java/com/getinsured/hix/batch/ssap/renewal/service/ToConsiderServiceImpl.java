package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.renewal.service.SsapToConsiderService;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.util.TSDate;

/**
 * Class is used to provide services for ToConsider renewals.
 */
@Service("toConsiderService")
public class ToConsiderServiceImpl implements ToConsiderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToConsiderServiceImpl.class);
	
	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	
	@Autowired
	private GIMonitorService giMonitorService;
	
	@Autowired
	private SsapToConsiderService ssapToConsiderService;
	
	
	@Override
	public List<Long> getSsapApplicationIdsForRenewal(Long renewalYear,Long batchSize) {
		return ssapCloneApplicationService.getSsapApplicationIdsForRenewal(renewalYear,batchSize);
	}
	
	@Override
	public List<Long> getSsapApplicationIdsByServerName(Long renewalYear, int serverCount, int serverName, Long batchSize) {
		return ssapCloneApplicationService.getSsapApplicationIdsByServerName(renewalYear, serverCount, serverName, batchSize);
	}
	
	@Override
	public List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList) {
		return ssapCloneApplicationService.getSsapApplicationListByIds(applicationIdList);
	}
	
	@Override
	public void executeToConsiderLogicForRenewal(SsapApplication currentApplication,Long renewalYear, Integer serverName) throws GIException, Exception {
		ssapToConsiderService.executeToConsiderLogicForRenewal(currentApplication, renewalYear, serverName);
	}
	
	@Override
	public void processErrorApplications(Long renewalYear, int serverCount, int serverName) throws Exception {
		List<Long> list =  ssapCloneApplicationService.getSsapApplicationIdsForErrorProcessing(renewalYear, serverCount, serverName);
		for(Long ssapApplicationId : list)
		{
			LOGGER.info("Started Error Processing for application id = " + ssapApplicationId);
			ssapToConsiderService.processErrorApplications(ssapApplicationId);
		}
	}
	
	public void saveAndThrowsErrorLog(String errorMessage, String errorCode) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog(errorCode, new TSDate(), this.getClass().getName(),errorMessage, null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}
	
	public Integer logToGIMonitor(String errorMessage, String errorCode) {
		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog(errorCode, new TSDate(), this.getClass().getName(),errorMessage, null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
		if (giMonitor != null) {
			giMonitorId = giMonitor.getId();
		}
		return giMonitorId;
	}
}
