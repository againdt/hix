package com.getinsured.ahbx.enrollment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerException;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.getinsured.ahbx.enrollment.adminupdateconfirmation.IndividualPlanConfirmationRequestDTO;
import com.getinsured.ahbx.enrollment.adminupdateconfirmation.IndividualPlanConfirmationResponseDTO;
import com.getinsured.ahbx.enrollment.adminupdateconfirmation.SaveIndvPlanConfirm;
import com.getinsured.ahbx.enrollment.adminupdateconfirmation.SaveIndvPlanConfirmResponse;
import com.getinsured.ahbx.enrollment.indvplanselection.AcceptanceStatusType;
import com.getinsured.ahbx.enrollment.indvplanselection.DisenrolledMembers;
import com.getinsured.ahbx.enrollment.indvplanselection.EnrollmentStatusType;
import com.getinsured.ahbx.enrollment.indvplanselection.EnrollmentTypeValue;
import com.getinsured.ahbx.enrollment.indvplanselection.IndvidualPlanSelectionRequestDTO;
import com.getinsured.ahbx.enrollment.indvplanselection.IndvidualPlanSelectionRequestDTO.Disenrollments;
import com.getinsured.ahbx.enrollment.indvplanselection.IndvidualPlanSelectionRequestDTO.SelectedPlans;
import com.getinsured.ahbx.enrollment.indvplanselection.MemberDetailsType;
import com.getinsured.ahbx.enrollment.indvplanselection.PlanSelectionDetails;
import com.getinsured.ahbx.enrollment.indvplanselection.PlanSelectionDetails.AssociatedMemberDetails;
import com.getinsured.ahbx.enrollment.indvplanselection.PlanTierIDType;
import com.getinsured.ahbx.enrollment.indvplanselection.ProductCategoryType;
import com.getinsured.ahbx.enrollment.indvplanselection.ProductType;
import com.getinsured.ahbx.enrollment.indvplanselection.RoleType;
import com.getinsured.ahbx.enrollment.indvplanselection.SavePlanSelectionInfo;
import com.getinsured.ahbx.enrollment.indvplanselection.SavePlanSelectionInfoResponse;
import com.getinsured.ahbx.enrollment.indvplanselection.SavePlanSelectionInfoResponseDTO;
import com.getinsured.ahbx.enrollment.sendupdatedenrollment.IndvEnrollmentConfirmationRequest;
import com.getinsured.ahbx.enrollment.sendupdatedenrollment.SaveEnrollmentConfirmation;
import com.getinsured.ahbx.enrollment.sendupdatedenrollment.SaveEnrollmentConfirmationResponse;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.thoughtworks.xstream.XStream;


/**
 * @author panda_p
 *
 */
@Controller
@RequestMapping(value = "/enrollment")
public class EnrollmentWsController {
	
	private static final Logger LOGGER = Logger.getLogger(EnrollmentWsController.class);
	
	private static final String CASE_ID= "caseId";
	//private static final String USER_ID= "userId";
	//private static final String PIN= "pin";
	private static final String PROGRAM_TYPE= "programType";
	
	private final int errCode_1100 = 1100;
	private final int errCode_1101 = 1101;
	private final int errCode_1102 = 1102;
	private final int errCode_11111 = 11111;
	private final String IND20_FAILED = "IND20 Failed ~";
	//private final int errCode_11112 = 11112;
	
	private final String statusCode_e006 = "E-006";
	
	
	//@Autowired private WebServiceTemplate enrlEpinWebServiceTemplate;
	
	//IND20
	@Autowired private WebServiceTemplate enrlPsWebServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<SavePlanSelectionInfoResponse>> soapWebClientInd20;
	//IND21
	@Autowired private WebServiceTemplate webServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<SaveEnrollmentConfirmationResponse>> soapWebClientInd21;
	
	@Autowired private SoapWebClient<JAXBElement<SaveIndvPlanConfirmResponse>> soapWebClientInd70;
	@Autowired private WebServiceTemplate enrlAdminUpdateWebServiceTemplate; 
	
	@Value("#{configProp['enrollment.sendCarrierUpdatedEnrolleeToAHBXResponseUrl']}")
	private String sendCarrierUpdatedEnrolleeToAHBXResponseUrl;
	

	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome()throws HTTPException{
		LOGGER.info("Welcome to Enrollment WebService module");
		
		boolean ENRL_AHBX_WSDL_CALL_ENABLE= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(GhixAhbxConstants.AHBXWSDLCALLENABLE));
		if(ENRL_AHBX_WSDL_CALL_ENABLE){
			LOGGER.info(" ENRL_AHBX_WSDL_CALL_ENABLE : "+ ENRL_AHBX_WSDL_CALL_ENABLE);
		}
		return "Welcome to Enrollment SOAP Service module";
	}
	
/*	
	
	@RequestMapping(value = "/psdatatest", method = RequestMethod.GET)
	@ResponseBody public String psdatatest(){
		Map<String,Object> orderMap  = new HashMap<String,Object>();
		orderMap.put("caseId", "201");
		//orderMap.put("orderId", "162");
		orderMap.put("enrollmentId", "162");
		
		List<Map<String,Object>> planList = new ArrayList<Map<String,Object>>();

		Map<String,Object> planMap = new HashMap<String,Object>();
		planMap.put("planId", "38");
		planMap.put("planTierId","BRONZE");
		planMap.put("programType", "FI");
		planMap.put("coverageStartDate", "12/12/2012");
		planMap.put("grossPremiumCost", "74.0");
		planMap.put("employeeContribution",0);
		planMap.put("employerContribution", 0);
		planMap.put("aptcAppliedAmount", "50");
		planMap.put("netPremiumAmount", "51");
		planMap.put("enrollmentStatus", "PENDING");

		List<Map<String,Object>> memberList = new ArrayList<Map<String,Object>>();
		Map<String,Object> memberMap = new HashMap<String,Object>(); 
		memberMap.put("memberId", "14");
		memberMap.put("subscriberFlag", "Y");
		memberMap.put("grossPremiumCost", "74.0");
		memberList.add(memberMap);

		planMap.put("memberDetails", memberList);
		planList.add(planMap);
		orderMap.put("planDetails", planList);


		String verifyResponse = restTemplate.postForObject("http://localhost:8080/ghix-ahbx/enrollment/individualplanselection", orderMap, String.class);
		XStream xStream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xStream.fromXML(verifyResponse);

		if(enrollmentResponse!=null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixAhbxConstants.RESPONSE_SUCCESS)){
			//LOGGER.info("FIXME - Empty if block");
		}

		return enrollmentResponse.getErrMsg()!=null?enrollmentResponse.getErrMsg():enrollmentResponse.getStatus();
	}*/

	

	/**
	 * @author panda_p
	 * @since 07-Mar-2013
	 *
	 * This method will send the Plan and Member Details to AHBX 
	 * 
	 * @param inputVal
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@RequestMapping(value = "/individualplanselection", method = RequestMethod.POST)
	@ResponseBody public String individualPlanSelection(@RequestBody Map<String,Object> inputVal){
		Integer tempEnrollmentId = 0;
		//String tempHouseHoldCaseID= "";
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		
		try{
			IndvidualPlanSelectionRequestDTO objReq = new IndvidualPlanSelectionRequestDTO();
			
			Map<String, String> headerMap = new HashMap<String, String>();
			if(null != inputVal.get("logGlobalId")){
				headerMap.put("LOG_GLOBAL_ID", (String)inputVal.get("logGlobalId"));
			}
			
			if(isNotNullOrEmpty(inputVal.get("assisterBrokerId"))){
				objReq.setSubmitByPersonId(inputVal.get("assisterBrokerId").toString());
			}
			if(isNotNullOrEmpty(inputVal.get("assisterBrokerRole"))){
				objReq.setSubmitByPersonRole(RoleType.valueOf(inputVal.get("assisterBrokerRole").toString()));
			}
			if(isNotNullOrEmpty(inputVal.get(CASE_ID))){
				objReq.setHouseHoldCaseId(Long.valueOf(inputVal.get(CASE_ID).toString()));
				//tempHouseHoldCaseID = inputVal.get(CASE_ID).toString();
			}
			
			List<PlanSelectionDetails> planSelectionList = new ArrayList<PlanSelectionDetails>();
			List<DisenrolledMembers> disenrolledMembersList = new ArrayList<DisenrolledMembers>();
	
			for(Object objPlan: (ArrayList<Map<String,Object>>)inputVal.get("planDetails")){
	
				Map<String,Object> planData = (Map<String,Object>) objPlan;
				
				PlanSelectionDetails planSelectionDetails = new PlanSelectionDetails();
				/*
				if(isNotNullOrEmpty(inputVal.get("sadp"))){
					planSelectionDetails.setProSadpFlag(inputVal.get("sadp").toString());
				}*/
				
				if(isNotNullOrEmpty(planData.get("employerCaseId"))){
					planSelectionDetails.setEmployerCaseId(planData.get("employerCaseId").toString());
				}
				
				if(isNotNullOrEmpty(planData.get("enrollmentId"))){
					planSelectionDetails.setEnrollmentId(planData.get("enrollmentId").toString());
					tempEnrollmentId = Integer.parseInt(planData.get("enrollmentId").toString());
				}
				
				if(isNotNullOrEmpty(inputVal.get("AUTO_RENEWAL_FLAG"))){
					planSelectionDetails.setAutoRenewalFlag(inputVal.get("AUTO_RENEWAL_FLAG").toString());
				}
				if(isNotNullOrEmpty(inputVal.get("ENROLLMENT_TYPE"))){
					planSelectionDetails.setEnrollmentType(EnrollmentTypeValue.valueOf(inputVal.get("ENROLLMENT_TYPE").toString()));
				}
				//PriorEnrollmentId in case of renewal flow.
				if(isNotNullOrEmpty(planData.get("PRIOR_ENROLLMENT_ID"))){
					planSelectionDetails.setRenewTermEnrollment(planData.get("PRIOR_ENROLLMENT_ID").toString()) ;
				}
				LOGGER.info("EnrollmentWsController :: individualPlanSelection() IND20  Processing Enrollment Id :: "+tempEnrollmentId);
				if(isNotNullOrEmpty(planData.get("planId"))){
					planSelectionDetails.setHiosPlanId(planData.get("planId").toString());
				}
				if(isNotNullOrEmpty(planData.get("planTierId"))){
					planSelectionDetails.setPlanTierId(PlanTierIDType.valueOf(planData.get("planTierId").toString()));
				}
				if(isNotNullOrEmpty(planData.get("insuranceType")) && "HEALTH".equalsIgnoreCase(planData.get("insuranceType").toString())){
					planSelectionDetails.setProductType(ProductType.H);
				}else{
					planSelectionDetails.setProductType(ProductType.D);
				}
				if(isNotNullOrEmpty(planData.get("subscriberEventId"))){
					planSelectionDetails.setSubscriberEventId(Long.valueOf(planData.get("subscriberEventId").toString()));
				}else{
					planSelectionDetails.setSubscriberEventId(new Long(0));
				}
				
				
				if(planData.get(PROGRAM_TYPE)!=null){
					if(planData.get(PROGRAM_TYPE).equals(GhixAhbxConstants.MARKET_TYPE_SHOP)){
						planSelectionDetails.setProgramType(planData.get(PROGRAM_TYPE).toString());
						planSelectionDetails.setProductCategory(ProductCategoryType.H);
						if(isNotNullOrEmpty(planData.get("employeeContribution"))){
							planSelectionDetails.setEmployeeContribution(Double.valueOf(planData.get("employeeContribution").toString()));
						}
						if(isNotNullOrEmpty(planData.get("employerContribution"))){
							planSelectionDetails.setEmployerContribution(Double.valueOf(planData.get("employerContribution").toString()));
						}
					}else if(planData.get(PROGRAM_TYPE).equals(GhixAhbxConstants.MARKET_TYPE_INDIVIDUAL)){
						planSelectionDetails.setProgramType(GhixAhbxConstants.MARKET_TYPE_INDIVIDUAL);
						planSelectionDetails.setProductCategory(ProductCategoryType.H);
					}else if(planData.get(PROGRAM_TYPE).equals(GhixAhbxConstants.MARKET_TYPE_MEDICAL)){
						planSelectionDetails.setProgramType(GhixAhbxConstants.MARKET_TYPE_INDIVIDUAL);
						planSelectionDetails.setProductCategory(ProductCategoryType.M);
					}
				}
				
				if(isNotNullOrEmpty(planData.get("coverageStartDate"))){
					planSelectionDetails.setCoverageStartDate(planData.get("coverageStartDate").toString());
				}
				if(isNotNullOrEmpty(planData.get("grossPremiumCost"))){
					planSelectionDetails.setGrossPremiumCost(Double.parseDouble(planData.get("grossPremiumCost").toString()));
				}
				if(isNotNullOrEmpty(planData.get("aptcAppliedAmount"))){
					planSelectionDetails.setAPTCAppliedAmount(Double.valueOf(planData.get("aptcAppliedAmount").toString()));
				}else{
					planSelectionDetails.setAPTCAppliedAmount(new Double(0.00f));
				}
				if(isNotNullOrEmpty(planData.get("netPremiumAmount"))){
					planSelectionDetails.setNetPremiumAmount(Double.parseDouble(planData.get("netPremiumAmount").toString()));
				}
				if(isNotNullOrEmpty(planData.get("csrAmount"))){
					planSelectionDetails.setCsrAmount(Double.valueOf(planData.get("csrAmount").toString()));
				}
				
				if(isNotNullOrEmpty(planData.get("grossPremEffDate"))){
					planSelectionDetails.setFinancialEffectiveDate(planData.get("grossPremEffDate").toString());
				}
				
				if(isNotNullOrEmpty(planData.get("enrollmentStatus"))){
					planSelectionDetails.setEnrollmentStatus(EnrollmentStatusType.valueOf(planData.get("enrollmentStatus").toString()));
				}
				
				
				List<MemberDetailsType> memberDetailsList = new ArrayList<MemberDetailsType>();
				for(Object objMember: (ArrayList<Map<String,Object>>)planData.get("memberDetails")){
	
					Map<String,Object> memberData = (Map<String,Object>) objMember;
					MemberDetailsType memberDetailsType = new MemberDetailsType();
					if(isNotNullOrEmpty(memberData.get("grossPremiumCost"))){
						memberDetailsType.setGrossPremiumCost(Double.parseDouble(memberData.get("grossPremiumCost").toString()));
					}
					if(isNotNullOrEmpty(memberData.get("memberId"))){
						memberDetailsType.setMemberId(Long.valueOf(memberData.get("memberId").toString()));
					}
					if(memberData.get("subscriberFlag")!=null && memberData.get("subscriberFlag").equals("Y")){
						memberDetailsType.setSubscriberFlag(memberData.get("subscriberFlag").toString());
					}else{
						memberDetailsType.setSubscriberFlag("N");
					}
					
					if(isNotNullOrEmpty(memberData.get("existingQhpEnrollmentId"))){
						memberDetailsType.setDisenrollmentQHPEnrollmentId(memberData.get("existingQhpEnrollmentId").toString());
					}
					if(isNotNullOrEmpty(memberData.get("QHPDisenrollmentEndDate"))){
						memberDetailsType.setQHPDisenrollmentEndDate(memberData.get("QHPDisenrollmentEndDate").toString());
					}
					if(isNotNullOrEmpty(memberData.get("existingSadpEnrollmentId"))){
						memberDetailsType.setDisenrollmentSADPEnrollmentId(memberData.get("existingSadpEnrollmentId").toString());
					}
					if(isNotNullOrEmpty(memberData.get("SADPDisenrollmentEndDate"))){
						memberDetailsType.setSADPDisenrollmentEndDate(memberData.get("SADPDisenrollmentEndDate").toString());
					}
					memberDetailsList.add(memberDetailsType);
				}
				AssociatedMemberDetails amd = new AssociatedMemberDetails();
				amd.getMemeberDetail().addAll(memberDetailsList);
				planSelectionDetails.setAssociatedMemberDetails(amd) ;
	
				planSelectionList.add(planSelectionDetails);
				
				
			}
			if(isNotNullOrEmpty(inputVal.get("disEnrolledMemberDetails"))){
			for(Object objMember: (ArrayList<Map<String,String>>)inputVal.get("disEnrolledMemberDetails")){
				Map<String,String> memberData = (Map<String,String>) objMember;
				
				DisenrolledMembers disEnrolledMember = new DisenrolledMembers();
				disEnrolledMember.setDisenrollMemberId(Long.valueOf(memberData.get("memberId").toString()));
				
				List<String> disEnrollmentIdList = new ArrayList<String>();
				disEnrollmentIdList.add(memberData.get("disEnrolledEnrollmentId").toString());
				disEnrolledMember.getDisenrollmentId().addAll(disEnrollmentIdList);
				
				disenrolledMembersList.add(disEnrolledMember);
			}
			}
			SelectedPlans selectedPlans = new SelectedPlans();
			selectedPlans.getPlanDetail().addAll(planSelectionList);
			
			/***** Add disEnrolled Data *****/
			Disenrollments disenrollments = new Disenrollments();
			disenrollments.getDisenrolledMembersList().addAll(disenrolledMembersList);
			
			objReq.setSelectedPlans(selectedPlans);
			objReq.setDisenrollments(disenrollments);
	
			SavePlanSelectionInfo savePlanSelectionInfo = new SavePlanSelectionInfo();
			savePlanSelectionInfo.setIndvidualPlanSelectionRequest(objReq);
	
			com.getinsured.ahbx.enrollment.indvplanselection.ObjectFactory of = new com.getinsured.ahbx.enrollment.indvplanselection.ObjectFactory();
			JAXBElement<SavePlanSelectionInfo> savePlanSelectionInfoReq = of.createSavePlanSelectionInfo(savePlanSelectionInfo);
			JAXBElement<SavePlanSelectionInfoResponse> objResponse = null;
		
			enrlPsWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND20);
			LOGGER.info("EnrollmentWsController :: individualPlanSelection()  IND20  Sent for Enrollment Id :: "+tempEnrollmentId );
			
			/*objResponse = soapWebClientInd20.send(savePlanSelectionInfoReq, GhixAhbxConstants.WSDL_URL_IND20, "IND20_EnrollmentID: " + tempEnrollmentId + ": CaseID: "+ tempHouseHoldCaseID, enrlPsWebServiceTemplate);*/
			
			/**
			 * Jira Id:HIX-72207 Pass LOG_GLOBAL_ID in IND20 Header
			 * Changes made to add header LOG_GLOBAL_ID to SaopRequest 
			 */
			if(headerMap != null && !headerMap.isEmpty()){
				objResponse = soapWebClientInd20.send(savePlanSelectionInfoReq, GhixAhbxConstants.WSDL_URL_IND20, "IND20", enrlPsWebServiceTemplate, new EnrollmentHeaderRequestWebServiceMessageCallback(of.getSaveplanselectioninfoQname().getNamespaceURI(), headerMap));
			}
			else{
				objResponse = soapWebClientInd20.send(savePlanSelectionInfoReq, GhixAhbxConstants.WSDL_URL_IND20, "IND20", enrlPsWebServiceTemplate);
			}
			
			if(objResponse==null){
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setModuleStatusCode("E-004");       // TODO: IND20 objResponse  is null : Could not marshal IND20 wsdl call
				enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode());
				LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +" IND20 objResponse  is null : Could not marshal IND20 wsdl call Response");
			}else{
				SavePlanSelectionInfoResponseDTO savePlanSelectionInfoResponseDTO = objResponse.getValue().getSavePlanSelectionInfoResponse();
				if(savePlanSelectionInfoResponseDTO==null || savePlanSelectionInfoResponseDTO.getResponseDetail()==null ){

					enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
					enrollmentResponse.setModuleStatusCode("E-005");       // TODO: IND20 savePlanSelectionInfoResponseDTO is null : Incorrect response received from AHBX
					//enrollmentResponse.setErrMsg("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +" IND20 Incorrect response received from AHBX : Could not marshal IND20 wsdl call Response");
					enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode());
					LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +" IND20 Incorrect response received from AHBX : Could not marshal IND20 wsdl call Response");

				}else if(savePlanSelectionInfoResponseDTO.getPlanSelectionResponse()!=null){
					
					if(savePlanSelectionInfoResponseDTO.getPlanSelectionResponse().equals(AcceptanceStatusType.C)){
						enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
						enrollmentResponse.setModuleStatusCode("E-000");
						enrollmentResponse.setErrMsg(savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseDescription());
					}else{
						enrollmentResponse.setErrCode(errCode_11111);
						enrollmentResponse.setModuleStatusCode("E-001");
						enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg(IND20_FAILED+savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseDescription()+" ~  Error Code : "+savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseCode());
						LOGGER.error(IND20_FAILED+savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseDescription()+" ~  Error Code : "+savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseCode());
					}
				}else{
					enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
					enrollmentResponse.setModuleStatusCode(statusCode_e006);
					//enrollmentResponse.setErrMsg("IND20  Failed : Error Code : "+savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseDescription());
					enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode());
					LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +savePlanSelectionInfoResponseDTO.getResponseDetail().getResponseDescription());
				}
			}
		}catch(WebServiceIOException wsIOE){
			enrollmentResponse.setErrCode(errCode_1100);     // TODO: Need to have some unique error code
			enrollmentResponse.setModuleStatusCode(statusCode_e006);
			enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode()+"\n"+wsIOE.getMessage());
			LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +wsIOE);
		}catch(SoapFaultClientException se){
			enrollmentResponse.setErrCode(errCode_1101);     // TODO: Need to have some unique error code
			enrollmentResponse.setModuleStatusCode(statusCode_e006);
			enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode()+"\n"+se.getMessage());
			LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +se);
		}catch(Exception e) {
			enrollmentResponse.setErrCode(errCode_1102);     // TODO: Need to have some unique error code
			enrollmentResponse.setModuleStatusCode(statusCode_e006);
			enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg(IND20_FAILED+"Your enrollment could not be processed"+" ~ "+" Error Code : "+enrollmentResponse.getModuleStatusCode()+"\n"+e.getMessage());
			LOGGER.error("EnrollmentWsController :: individualPlanSelection()  IND20  Failed for Enrollment Id :: "+tempEnrollmentId +e);
		}

		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(enrollmentResponse);
	}

	
	@RequestMapping(value = "/sendCarrierUpdatedData", method = RequestMethod.POST)
	@ResponseBody public String sendCarrierUpdatedData(@RequestBody Map<String,Object> inputVal){
		//LOGGER.info("inside WS sendCarrierUpdatedData ");
		
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		boolean ENRL_AHBX_WSDL_CALL_ENABLE= Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(GhixAhbxConstants.AHBXWSDLCALLENABLE));
		if(!ENRL_AHBX_WSDL_CALL_ENABLE){
		//if(!combinedConfig.getBoolean("enrollment.ahbxWsdlCallEnable")){
			enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			XStream xstream = GhixUtils.getXStreamStaxObject();
			return xstream.toXML(enrollmentResponse);
		}
		
		Integer tempEnrollmentId=null;
		String tempCaseId=null;
		
		//LOGGER.info("=======IND 21 Requrst Params =============");
		//LOGGER.info("IND 21 REQUEST RARAMS   : "+inputVal);
		//LOGGER.info("==========================================");
		List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList =new ArrayList<SendUpdatedEnrolleeResponseDTO>();
		SaveEnrollmentConfirmation saveEnrollmentConfirmation = new SaveEnrollmentConfirmation();
		//LOGGER.info("Processing IND 21 Request Parameters");
		for(Object objPlan: (ArrayList<Map<String,Object>>)inputVal.get("planList")){
			Map<String,Object> planData = (Map<String,Object>) objPlan;
			IndvEnrollmentConfirmationRequest objReq = new IndvEnrollmentConfirmationRequest();
			
			if(isNotNullOrEmpty(planData.get(CASE_ID))){
				objReq.setCaseId(Long.parseLong(planData.get(CASE_ID).toString()));
				tempCaseId = planData.get(CASE_ID).toString();
			}
			if(isNotNullOrEmpty(planData.get("enrollmentId"))){
				objReq.setEnrollmentId(planData.get("enrollmentId").toString());
				tempEnrollmentId = Integer.valueOf(planData.get("enrollmentId").toString());
			}
			/*if(isNotNullOrEmpty(planData.get("planId"))){
				objReq.setExchangePlanId(Long.parseLong(planData.get("planId").toString()));
			}*/
			if(isNotNullOrEmpty(planData.get("cmsPlanId"))){
				objReq.setHiosPlanId(planData.get("cmsPlanId").toString());
			}
			if(isNotNullOrEmpty(planData.get("memberId"))){
				objReq.setMemberId(Long.parseLong(planData.get("memberId").toString()));
			}
			if(isNotNullOrEmpty(planData.get("policyId"))){
				objReq.setPolicyId(planData.get("policyId").toString());
			}
			if(isNotNullOrEmpty(planData.get("effectiveEndDate"))){
				objReq.setEffectiveEndDate(planData.get("effectiveEndDate").toString());
			}
			if(isNotNullOrEmpty(planData.get("effectiveStartDate"))){
				objReq.setEffectiveStartDate(planData.get("effectiveStartDate").toString());
			}
			//if(planData.get("status")!=null)
			if(isNotNullOrEmpty(planData.get(CASE_ID))){
				objReq.setCaseId(Long.parseLong(planData.get(CASE_ID).toString()));
			}
			if(isNotNullOrEmpty(planData.get("enrollmentId"))){
				objReq.setEnrollmentId(planData.get("enrollmentId").toString());
			}
			objReq.setEnrollmentStatus(com.getinsured.ahbx.enrollment.sendupdatedenrollment.EnrollmentStatusType.valueOf(planData.get("status").toString()));
			
			if(isNotNullOrEmpty(planData.get("lastPremiumPaidDate"))){
				objReq.setLastPremiumPaidDate(planData.get("lastPremiumPaidDate").toString());
			}
			
			if(isNotNullOrEmpty(planData.get("enrollmentConfirmationDate"))){
				objReq.setEnrollmentConfirmationDate(planData.get("enrollmentConfirmationDate").toString());
			}
			
			if(isNotNullOrEmpty(planData.get("maintenanceReasonCode"))){
				objReq.setMaintenanceReasonCode(planData.get("maintenanceReasonCode").toString());
			}
			saveEnrollmentConfirmation.setEnrollmentConfirmationRequest(objReq);
			//LOGGER.info("IND 21 REQUEST 		 	: "+ objReq.toString());
            //LOGGER.info("IND 21 CaseId 				: "+ objReq.getCaseId());
            //LOGGER.info("IND 21 enrollmentId 		: "+ objReq.getEnrollmentId());
            //LOGGER.info("IND 21 EffectiveEndDate 	: "+ objReq.getEffectiveEndDate());
            //LOGGER.info("IND 21 EffectiveStartDate 	: "+ objReq.getEffectiveStartDate());
            //LOGGER.info("IND 21 ExchangePlanId 		: "+ objReq.getExchangePlanId());
            //LOGGER.info("IND 21 LastPremiumPaidDate : "+ objReq.getLastPremiumPaidDate());
            //LOGGER.info("IND 21 MemberId 			: "+ objReq.getMemberId());
            //LOGGER.info("IND 21 PolicyId 			: "+ objReq.getPolicyId());
            //LOGGER.info("IND 21 EnrollmentStatus 	: "+ objReq.getEnrollmentStatus());
            //LOGGER.info("IND 21 EnrollmentConfirmationDate 	: "+ objReq.getEnrollmentConfirmationDate());
			
			com.getinsured.ahbx.enrollment.sendupdatedenrollment.ObjectFactory of = new com.getinsured.ahbx.enrollment.sendupdatedenrollment.ObjectFactory();
			JAXBElement<SaveEnrollmentConfirmation> saveEnrollmentConfirmationReq = of.createSaveEnrollmentConfirmation(saveEnrollmentConfirmation);
			JAXBElement<SaveEnrollmentConfirmationResponse> objResponse = null;
			SendUpdatedEnrolleeResponseDTO sendUpdatedEnrolleeResponseDTO = new SendUpdatedEnrolleeResponseDTO();
			try{
				
				//LOGGER.info("WSDL_URL_IND21 : "+GhixAhbxConstants.WSDL_URL_IND21);
				webServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND21);
				//objResponse = (JAXBElement<SaveEnrollmentConfirmationResponse>) webServiceTemplate.marshalSendAndReceive(saveEnrollmentConfirmationReq);
				objResponse = soapWebClientInd21.send(saveEnrollmentConfirmationReq, GhixAhbxConstants.WSDL_URL_IND21, "IND21_EnrollmentID: " + tempEnrollmentId + ": CaseID: "+ tempCaseId, webServiceTemplate);
				
				if(objResponse!=null && objResponse.getValue().getSaveEnrollmentConfirmationResponse()!=null){
					
					com.getinsured.ahbx.enrollment.sendupdatedenrollment.ResponseDTO responseDTO = objResponse.getValue().getSaveEnrollmentConfirmationResponse();
					
					if(responseDTO!=null){
						
						sendUpdatedEnrolleeResponseDTO.setAhbxStatusCode(responseDTO.getResponseCode());
						sendUpdatedEnrolleeResponseDTO.setAhbxStatusDescription(responseDTO.getResponseDescription());
						sendUpdatedEnrolleeResponseDTO.setEnrolleeId(planData.get("enrolleeId").toString());
						sendUpdatedEnrolleeResponseDTO.setExchgIndivIdentifier(""+objReq.getMemberId());
						sendUpdatedEnrolleeResponseDTO.setEnrolleeStatus(objReq.getEnrollmentStatus().toString());
						
						//LOGGER.info(" WSDL_URL_IND21 Response : "+sendUpdatedEnrolleeResponseDTO.toString());
						
						
					}else{
						LOGGER.info("IND 21 Got NULL Response for Member id : "+objReq.getMemberId());
					}
				}
			}catch(WebServiceIOException wsIOE){
				//LOGGER.info("Fault Message : " + wsIOE.getMessage());
				//LOGGER.info("Fault Cause : " + wsIOE.getCause());
				enrollmentResponse.setErrCode(errCode_1100);	// TODO: Need to have some unique error code
				enrollmentResponse.setModuleStatusCode(statusCode_e006);
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND21 : "+wsIOE.getMessage());
				
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusCode(""+errCode_1100);
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusDescription(GhixAhbxConstants.RESPONSE_FAILURE);
				sendUpdatedEnrolleeResponseDTO.setEnrolleeId(planData.get("enrolleeId").toString());
				sendUpdatedEnrolleeResponseDTO.setExchgIndivIdentifier(""+objReq.getMemberId());
				sendUpdatedEnrolleeResponseDTO.setEnrolleeStatus(objReq.getEnrollmentStatus().toString());
			}catch(SoapFaultClientException se){
				//LOGGER.info("Fault Code : " + se.getFaultCode());
				//LOGGER.info("Fault Reason : " + se.getFaultStringOrReason());
				//LOGGER.info("Fault Role : " + se.getSoapFault().getFaultActorOrRole());
				enrollmentResponse.setErrCode(errCode_1101);	// TODO: Need to have some unique error code
				enrollmentResponse.setModuleStatusCode(statusCode_e006);
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND21 : "+se.getMessage());
				
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusCode(""+errCode_1101);
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusDescription(GhixAhbxConstants.RESPONSE_FAILURE);
				sendUpdatedEnrolleeResponseDTO.setEnrolleeId(planData.get("enrolleeId").toString());
				sendUpdatedEnrolleeResponseDTO.setExchgIndivIdentifier(""+objReq.getMemberId());
				sendUpdatedEnrolleeResponseDTO.setEnrolleeStatus(objReq.getEnrollmentStatus().toString());
			}catch(Exception e) {
				//LOGGER.info("Fault Message : " + e.getMessage());
				//LOGGER.info("Fault Cause : " + e.getCause());
				enrollmentResponse.setErrCode(errCode_1102);	// TODO: Need to have some unique error code
				enrollmentResponse.setModuleStatusCode(statusCode_e006);
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND21 : "+e.getMessage());
				
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusCode(""+errCode_1102);
				sendUpdatedEnrolleeResponseDTO.setAhbxStatusDescription(GhixAhbxConstants.RESPONSE_FAILURE);
				sendUpdatedEnrolleeResponseDTO.setEnrolleeId(planData.get("enrolleeId").toString());
				sendUpdatedEnrolleeResponseDTO.setExchgIndivIdentifier(""+objReq.getMemberId());
				sendUpdatedEnrolleeResponseDTO.setEnrolleeStatus(objReq.getEnrollmentStatus().toString());
			}
			sendUpdatedEnrolleeResponseDTOList.add(sendUpdatedEnrolleeResponseDTO);
			
		}//End of list
		
		//LOGGER.info("Returing with Response Status and description");
		enrollmentResponse.setSendUpdatedEnrolleeResponseDTOList(sendUpdatedEnrolleeResponseDTOList);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return  xstream.toXML(enrollmentResponse);
	}
	
	@RequestMapping(value = "/sendAdminUpdateConfirmation", method = RequestMethod.POST)
	@ResponseBody public String sendAdminUpdateConfirmation(@RequestBody String enrollmentId){
		//LOGGER.info("AHBX IND70 :: sendAdminUpdateConfirmation : STARTED");
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		if(isNotNullOrEmpty(enrollmentId)){
			try{
				IndividualPlanConfirmationRequestDTO individualPlanConfirmationRequest = new IndividualPlanConfirmationRequestDTO();
				individualPlanConfirmationRequest.setEnrollmentId(enrollmentId);
				
				SaveIndvPlanConfirm saveIndvPlanConfirm = new SaveIndvPlanConfirm();
				saveIndvPlanConfirm.setIndividualPlanConfirmationRequest(individualPlanConfirmationRequest);

				com.getinsured.ahbx.enrollment.adminupdateconfirmation.ObjectFactory objectFactory = new com.getinsured.ahbx.enrollment.adminupdateconfirmation.ObjectFactory();

				JAXBElement<SaveIndvPlanConfirm> saveIndvPlanConfirmReq = objectFactory.createSaveIndvPlanConfirm(saveIndvPlanConfirm);
				JAXBElement<SaveIndvPlanConfirmResponse> objResponse = null;

				//LOGGER.info("WSDL_URL_IND70 : "+GhixAhbxConstants.WSDL_URL_IND70);
				enrlAdminUpdateWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND70);

				objResponse = soapWebClientInd70.send(saveIndvPlanConfirmReq, GhixAhbxConstants.WSDL_URL_IND70, "IND70_enrollmentId="+enrollmentId, enrlAdminUpdateWebServiceTemplate);

				if(objResponse != null && objResponse.getValue().getSaveIndvPlanConfirmResponse() != null){
					IndividualPlanConfirmationResponseDTO individualPlanConfirmationResponseDTO = objResponse.getValue().getSaveIndvPlanConfirmResponse();
					if(individualPlanConfirmationResponseDTO == null || individualPlanConfirmationResponseDTO.getResponseDetail() == null || individualPlanConfirmationResponseDTO.getAcceptanceStatus() == null){
						//LOGGER.info("IND70 individualPlanConfirmationResponseDTO is null : Incorrect response received from AHBX");
						enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
						enrollmentResponse.setErrMsg("IND70 individualPlanConfirmationResponseDTO is null : Incorrect response received from AHBX");
					}
					else if(individualPlanConfirmationResponseDTO.getResponseDetail().getResponseCode()!=null &&
							individualPlanConfirmationResponseDTO.getResponseDetail().getResponseCode().equalsIgnoreCase("200")){
						if(individualPlanConfirmationResponseDTO.getAcceptanceStatus().value().equals(AcceptanceStatusType.C.value())){
							//LOGGER.info("IND 70 Acceptance Status : " + individualPlanConfirmationResponseDTO.getAcceptanceStatus());
							enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
						}else{
							//LOGGER.info("AHBX IND70 ResponseCode 			: " + individualPlanConfirmationResponseDTO.getResponseDetail().getResponseCode());
							//LOGGER.info("AHBX IND70 ResponseDescription 	: " + individualPlanConfirmationResponseDTO.getResponseDetail().getResponseDescription());
							//LOGGER.info("AHBX IND70 Acceptance Status   : " + individualPlanConfirmationResponseDTO.getAcceptanceStatus());
							enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
							enrollmentResponse.setErrMsg("IND70 Failure" + individualPlanConfirmationResponseDTO.getResponseDetail().getResponseDescription());
						}
					}
					else{
						//LOGGER.info("AHBX IND70 ResponseCode 			: " + individualPlanConfirmationResponseDTO.getResponseDetail().getResponseCode());
						//LOGGER.info("AHBX IND70 ResponseDescription 	: " + individualPlanConfirmationResponseDTO.getResponseDetail().getResponseDescription());
						//LOGGER.info("AHBX IND70 Acceptance Status   : " + individualPlanConfirmationResponseDTO.getAcceptanceStatus());
						enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
					}
				}
			}
			catch(WebServiceIOException wsIOE){
				//LOGGER.error("Fault Message : " + wsIOE.getMessage());
				//LOGGER.error("Fault Cause : " + wsIOE.getCause());
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND70 : " + wsIOE.getMessage());
			}catch(SoapFaultClientException se){
				//LOGGER.error("Fault Message : " + se.getMessage());
				//LOGGER.error("Fault Cause : " + se.getCause());
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND70 : " + se.getMessage());
			}catch(Exception e) {
				//LOGGER.error("Fault Message : " + e.getMessage());
				//LOGGER.error("Fault Cause : " + e.getCause());
				enrollmentResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				enrollmentResponse.setErrMsg("IND70 : " + e.getMessage());
			}
		}
		return  xstream.toXML(enrollmentResponse);
	}
	
	/**
	 * @author panda_p
	 * @since 17-Apr-2013
	 * 
	 * This method Check whether input object is null or empty   
	 * 
	 * @param obj
	 * @return boolean
	 */
	private boolean isNotNullOrEmpty(Object obj){
	    boolean isNotNull = false;
	    if(obj != null ){
           if(obj instanceof String){
                 if(!obj.toString().equalsIgnoreCase("") && !obj.toString().equalsIgnoreCase("null")){
                	 isNotNull = true;
                 }
           }
           else{
        	   isNotNull = true;
           }
	    }
	    return isNotNull;
	}
	
	/**
	 * Implements WebServiceMessageCallback
	 */
	private class EnrollmentHeaderRequestWebServiceMessageCallback implements WebServiceMessageCallback{
		public String namespaceURI;
		public Map<String, String> headerMap;
		
		public EnrollmentHeaderRequestWebServiceMessageCallback(String namespaceURI, Map<String, String> headerMap){
			this.namespaceURI = namespaceURI;
			this.headerMap = headerMap;
		}
		
		
		 @Override
		 public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException 
		 {
			 try
			 {
				 SaajSoapMessage saajSoapMessage = (SaajSoapMessage) message;
				 SOAPMessage soapMessage = saajSoapMessage.getSaajMessage();
				 SOAPPart soapPart = soapMessage.getSOAPPart();
				 SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
				 SOAPHeader soapHeader = soapEnvelope.getHeader();
				 if(headerMap != null && !headerMap.isEmpty())
				 {
					 for(String headerName : headerMap.keySet())
					 {
						 QName qName = new QName(namespaceURI, headerName);
						 SOAPHeaderElement soapHeaderElement = soapHeader.addHeaderElement(qName);
						 soapHeaderElement.addTextNode(headerMap.get(headerName));
					 }
				 }
			 }
			 catch (SOAPException soapException)
			 {
				 throw new RuntimeException("EnrollmentHeaderRequestWebServiceMessageCallback", soapException);
			 } 
		 }	
	}
}

