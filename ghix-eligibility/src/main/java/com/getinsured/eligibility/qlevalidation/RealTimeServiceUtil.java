package com.getinsured.eligibility.qlevalidation;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.model.SepEvents.ManualVerification;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.eligibility.EventVerificationDetailsDTO;
import com.getinsured.hix.dto.eligibility.EventVerificationDetailsListDTO;
import com.getinsured.hix.dto.eligibility.RealTimeHMSResponse;
import com.getinsured.hix.dto.eligibility.RealTimeVerificationDTO;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.JWTTokenizer;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.ssap.model.SsapApplicantEventVerification;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantEventVerificationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;


@Service("RealTimeServiceUtil")
public class RealTimeServiceUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RealTimeServiceUtil.class);
	
	private static final String SUCCESS = "Success";
	private static final String FAILURE = "Failure";
	private static final String ENDPOINT_FUNCTION = "HMS";
	public static final String YES = "Y";
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String HEADER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	private static final String RETRY_FLAG = "retryFlag";
	private static final String APP_ID = "app-id";
	private static final String CLIENT_ID = "client-id";
	private static final String CASE_ID = "case-id";
	private static final String CORRELATION_ID = "x-correlation-id";
	private static final String TIMESTAMP = "timeStamp";
	private static final String AUTHORIZATION = "authorization";
	private static final String SSAP_APPLICANT_EVENT_ID = "SSAP_APPLICANT_EVENT_ID";
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	private static final String ACTION_SUBMIT = "SUBMIT";
	private static final String ACTION_SKIP = "SKIP";
	private static final String NO_EVENTS_FOUND_FOR_APPLICANT = "No events found for applicant";
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	
	@Value("#{configProp['hms.realTimeEndPoint']}")
	private String realTimeEndPoint;
	@Value("#{configProp['hms.additionalInfoRequired']}")
	private String additionalInfoRequired;
	@Value("#{configProp['hms.realTimeEnabled']}")
	public String realTimeEnabled;
	@Value("#{configProp['hms.jwt.tokenExpirationTime']}")
	private String tokenExpirationTime;
	@Value("#{configProp['hms.jwt.id']}")
	private String tokenId;
	@Value("#{configProp['hms.jwt.issuer']}")
	private String tokenIssuer;
	@Value("#{configProp['hms.jwt.subject']}")
	private String tokenSubject;
	@Value("#{configProp['hms.jwt.token.key']}")
	public String tokenKey;
	@Value("#{configProp['hms.ahbx.apigateway.username']}")
	private String username;
	@Value("#{configProp['hms.ahbx.apigateway.password']}")
	private String password;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private JWTTokenizer jWTTokenizer;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantEventVerificationRepository ssapApplicantEventVerificationRepository;
	@Autowired
	private EntityManagerFactory emf;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	private LookupService lookupService;

	public enum HmsStatus {
		NO_MATCH("NO MATCH"), VERIFIED("VERIFIED"), NOT_VERIFIED("NOT VERIFIED"), PENDING("PENDING"), INCONCLUSIVE("INCONCLUSIVE"), ERROR("ERROR");
		
		private String	value;
		private HmsStatus(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
	}
	
	public void mapApplicant(SsapApplicantEvent ssapApplicantEvent, Integer lastUpdatedBy){
		updateValidationStatus(ssapApplicantEvent, lastUpdatedBy);
		RealTimeVerificationDTO realTimeVerificationDTO = mapApplicantToDto(ssapApplicantEvent);
		triggerRealTimeVerification(realTimeVerificationDTO, ssapApplicantEvent, FALSE);
	}
	
	public RealTimeHMSResponse triggerRealTimeVerification(RealTimeVerificationDTO realTimeVerificationDTO, SsapApplicantEvent ssapApplicantEvent, String retryFlag) {
		String response =null;
		RealTimeHMSResponse realTimeHMSResponse = null;
		String status = FAILURE;
		String request = null;
		HttpHeaders headers = null;
		String headerJson = null;
		long startTime = 0;
		
		try {
			LOGGER.info("realTimeEndPoint : "+realTimeEndPoint);
			if(realTimeEndPoint!=null){
				startTime = TimeShifterUtil.currentTimeMillis();
				request = JacksonUtils.getJacksonObjectWriter(RealTimeVerificationDTO.class).writeValueAsString(realTimeVerificationDTO);

				headers = populateHeaders(ssapApplicantEvent, retryFlag);
				if(headers!=null){
					headerJson = JacksonUtils.getJacksonObjectWriter(HttpHeaders.class).writeValueAsString(headers);
					response = ghixRestTemplate.postForObject(realTimeEndPoint,request,String.class, headers);
					
					LOGGER.info("response : "+response);
					if(response != null && StringUtils.isNotBlank(response)){
						realTimeHMSResponse = JacksonUtils.getJacksonObjectReader(RealTimeHMSResponse.class).readValue(response);
						processHMSResponse(realTimeHMSResponse, ssapApplicantEvent);
						status = SUCCESS;
					}
				} else {
					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.OVERRIDDEN);
				}
			} 
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while triggerring HMS", ex);
			throw new GIRuntimeException("Exception occurred while triggerring HMS.", ex);
		} finally {
			LOGGER.info("Headers : "+headerJson);
			logServicePayload(ssapApplicantEvent, "HMS Execution Time in ms : " +String.valueOf(TimeShifterUtil.currentTimeMillis()-startTime)+ " Headers : "+headerJson+"  Body : "+request, realTimeEndPoint, response, status);
		}
			
		return realTimeHMSResponse;	
	}
	
	private HttpHeaders populateHeaders(SsapApplicantEvent ssapApplicantEvent, String retryFlag){
		HttpHeaders headers = new HttpHeaders();
		try {
			String token = jWTTokenizer.getJWTECToken(tokenId, tokenIssuer, tokenSubject, Long.valueOf(tokenExpirationTime));
			LOGGER.info("JWT Token : "+ token);

			headers.add(tokenKey, token);
			headers.add(RETRY_FLAG, retryFlag);
			headers.add(APP_ID, "HCV-SEP");
			headers.add(CLIENT_ID, "CCA");
			headers.add(CORRELATION_ID, UUID.randomUUID().toString());
			
			SimpleDateFormat sdf = new SimpleDateFormat(HEADER_DATE_FORMAT);
			headers.add(TIMESTAMP, sdf.format(new Date()));
			
			String externalHouseholdCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId().intValue());
			headers.add(CASE_ID, externalHouseholdCaseId);
			
			String userCredentials = username + ":"+ password;
			String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
			headers.add(AUTHORIZATION, basicAuth);
			LOGGER.info("basicAuth : "+ basicAuth);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while setting headers", ex);
			return null;
		}
		
		return headers;
	}
	
	public  RealTimeVerificationDTO mapApplicantToDto(SsapApplicantEvent ssapApplicantEvent){
		RealTimeVerificationDTO realTimeVerificationDTO = null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		if(ssapApplicantEvent != null){
			SsapApplicant ssapApplicant = ssapApplicantEvent.getSsapApplicant();
			if(ssapApplicant != null){
				realTimeVerificationDTO = new RealTimeVerificationDTO();
				realTimeVerificationDTO.setFirstName(ssapApplicant.getFirstName());
				realTimeVerificationDTO.setLastName(ssapApplicant.getLastName());
				realTimeVerificationDTO.setMiddleName(ssapApplicant.getMiddleName());
				realTimeVerificationDTO.setSuffix(ssapApplicant.getNameSuffix());
				realTimeVerificationDTO.setSsn(ssapApplicant.getSsn());
				realTimeVerificationDTO.setBirthDate(sdf.format(ssapApplicant.getBirthDate()));
				realTimeVerificationDTO.setSex(ssapApplicant.getGender());
				realTimeVerificationDTO.setInquiryDate(sdf.format(new TSDate()));
				populateHomeAddress(ssapApplicantEvent, realTimeVerificationDTO);
			}
		}

		return realTimeVerificationDTO;
	}
	
	private void populateHomeAddress(SsapApplicantEvent ssapApplicantEvent, RealTimeVerificationDTO realTimeVerificationDTO){
		SsapApplication ssapApplication = ssapApplicationRepository.findById(ssapApplicantEvent.getSsapApplicant().getSsapApplication().getId());
		
		SingleStreamlinedApplication applicationData = null;
		if(StringUtils.isNotBlank(ssapApplication.getApplicationData())){
			applicationData = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());	
			
			if (applicationData != null) {			
				List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId() == ssapApplicantEvent.getSsapApplicant().getPersonId()) {
						if(householdMember.getHouseholdContact() != null){
							HomeAddress homeAddress = householdMember.getHouseholdContact().getHomeAddress();
							if( homeAddress != null) {
								realTimeVerificationDTO.setStreetName1(homeAddress.getStreetAddress1());
								realTimeVerificationDTO.setStreetName2(homeAddress.getStreetAddress2());
								realTimeVerificationDTO.setCityName(homeAddress.getCity());
								realTimeVerificationDTO.setStateCode(homeAddress.getState());
								realTimeVerificationDTO.setZipCode(homeAddress.getPostalCode());							
							}
						}	
						break;
					}
				}
			}
		}	
	}
	
	public void processHMSResponse(RealTimeHMSResponse realTimeHMSResponse, SsapApplicantEvent ssapApplicantEvent){
		if(realTimeHMSResponse!=null){
			ssapApplicantEvent.setRealTimeStatus(realTimeHMSResponse.getResponseCode());
			if(ssapApplicantEvent.getSepEvents().getManualVerificationReqd() == ManualVerification.Y && !("CA").equalsIgnoreCase(stateCode)){
				ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING);
			} else {
				if(StringUtils.isNotEmpty(realTimeHMSResponse.getResponseCode()) 
						&& realTimeHMSResponse.getResponseCode().equalsIgnoreCase(HmsStatus.VERIFIED.getValue())){
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.VERIFIED);
				} else if(StringUtils.isNotEmpty(realTimeHMSResponse.getResponseCode()) 
						&& realTimeHMSResponse.getResponseCode().equalsIgnoreCase(HmsStatus.NO_MATCH.getValue()) 
						&& YES.equalsIgnoreCase(additionalInfoRequired)){
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING_INFO);
				} else {
					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.OVERRIDDEN);
				}
			}
		}
	}
	
	private void logServicePayload(SsapApplicantEvent ssapApplicantEvent, String request, String endpointUrl, String response, String status) {
		Query query = null;
		EntityManager em = null;
		StringBuilder strQuery = new StringBuilder();
		
		try {
			em = emf.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			
			String externalHouseholdCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId().intValue());
	
			strQuery.append("insert into GI_WS_PAYLOAD ");
			strQuery.append("(GI_WS_PAYLOAD_ID,SSAP_APPLICATION_ID,SSAP_APPLICANT_ID,CUSTOM_KEY_ID1,CUSTOM_KEY_VALUE1,CREATED_TIMESTAMP,ENDPOINT_FUNCTION,ENDPOINT_URL,");
			strQuery.append("REQUEST_PAYLOAD,RESPONSE_PAYLOAD,STATUS,HOUSEHOLD_CASE_ID) values ");
			strQuery.append( "(?,?,?,?,?,?,?,?,?,?,?,?)");
			
			query = em.createNativeQuery("SELECT GI_WS_PAYLOAD_SEQ.nextval FROM DUAL");
			BigDecimal sequenceValue = (BigDecimal) query.getSingleResult();
			
			query = em.createNativeQuery(strQuery.toString());
			query.setParameter(1, sequenceValue);
			query.setParameter(2, ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getId());
			query.setParameter(3, ssapApplicantEvent.getSsapApplicant().getId());
			query.setParameter(4, SSAP_APPLICANT_EVENT_ID);
			query.setParameter(5, String.valueOf(ssapApplicantEvent.getId()));
			query.setParameter(6, new TSDate(), TemporalType.TIMESTAMP);
			query.setParameter(7, ENDPOINT_FUNCTION);
			query.setParameter(8, endpointUrl);
			query.setParameter(9, request);
			query.setParameter(10, response);
			query.setParameter(11, status);
			query.setParameter(12, externalHouseholdCaseId);
			
			query.executeUpdate();

			em.getTransaction().commit();
		}catch(Exception e){
			LOGGER.error("Exception occurred while inserting records in payload table", e);
			throw new GIRuntimeException(e);
		} finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
	}

	public static ApplicationValidationStatus extractApplicationStatus(List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents) {
		ApplicationValidationStatus status = null;
		
		if (applicantEvents != null && applicantEvents.size() > 0){
			Set<ApplicantEventValidationStatus> validationStatus = new HashSet<>();
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				validationStatus.add(ssapApplicantEvent.getValidationStatus());
			}
			if(validationStatus.contains(ApplicantEventValidationStatus.INITIAL)){
				status = ApplicationValidationStatus.INITIAL;
			} else if(validationStatus.contains(ApplicantEventValidationStatus.PENDING_INFO)){
				status = ApplicationValidationStatus.PENDING_INFO;
			} else if((validationStatus.contains(ApplicantEventValidationStatus.PENDING) || validationStatus.contains(ApplicantEventValidationStatus.SUBMITTED) 
					|| validationStatus.contains(ApplicantEventValidationStatus.REJECTED)) && !("CA").equalsIgnoreCase(stateCode)){
				status = ApplicationValidationStatus.PENDING;
			} else if(validationStatus.contains(ApplicantEventValidationStatus.OVERRIDDEN) || validationStatus.contains(ApplicantEventValidationStatus.VERIFIED) 
					|| validationStatus.contains(ApplicantEventValidationStatus.NOTREQUIRED)){
				status = ApplicationValidationStatus.NOTREQUIRED;
			} 
		} else{
			status = ApplicationValidationStatus.NOTREQUIRED;
		}
		
		return status;
	}
	
	public EventVerificationDetailsListDTO findEventVerificationDetails(String caseNumber) {
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		EventVerificationDetailsDTO eventVerificationDetailsDTO = null;
		List<EventVerificationDetailsDTO> listOfVerifications = new ArrayList<>();
		EventVerificationDetailsListDTO eventVerificationDetailsListDTO = new EventVerificationDetailsListDTO();
		
		try {
			queryString.append(" SELECT se.ID, sa.FIRST_NAME, sa.LAST_NAME, sep.EVENT_NAME, se.VALIDATION_STATUS, vrf.ISSUER_CODE, "
					+ "vrf.POLICY_NUMBER, vrf.GROUP_NUMBER FROM SSAP_APPLICANT_EVENTS se "
					+ "INNER JOIN SSAP_APPLICANTS sa ON se.SSAP_APPLICANT_ID = sa.id "
					+ "INNER JOIN SSAP_APPLICATIONS app ON app.id = sa.SSAP_APPLICATION_ID "
					+ "INNER JOIN SEP_ENROLLMENT_EVENTS sep ON se.SEP_ENROLLMENT_EVENTS_ID=sep.ID "
					+ "LEFT JOIN  SSAP_APPLICANT_EVENT_VRF vrf ON vrf.SSAP_APPLICANT_EVENT_ID = se.ID "
					+ "WHERE se.VALIDATION_STATUS='PENDING_INFO' AND CASE_NUMBER = :caseNumber");
					
			em = entityManagerFactory.createEntityManager();
			Query query = em.createNativeQuery(queryString.toString());
			query.setParameter("caseNumber", caseNumber);
			
			List<Object[]> resultList = query.getResultList();
			
			for (Object[] data : resultList) {
				eventVerificationDetailsDTO = new EventVerificationDetailsDTO();
				if(null != data[0]) {
					eventVerificationDetailsDTO.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[0].toString()));
				}
				if(null != data[1]) {
					eventVerificationDetailsDTO.setFirstName(data[1].toString());
				}
				if(null != data[2]) {
					eventVerificationDetailsDTO.setLastName(data[2].toString());
				}
				if(null != data[3]) {
					eventVerificationDetailsDTO.setEventName(data[3].toString());
				}
				if(null != data[4]) {
					eventVerificationDetailsDTO.setValidationStatus(getValidationApplicantEventStatus(data[4].toString()));
				}
				if(null != data[5]) {
					eventVerificationDetailsDTO.setIssuerCode(data[5].toString());
				}
				if(null != data[6]) {
					eventVerificationDetailsDTO.setPolicyNumber(data[6].toString());
				}
				if(null != data[7]) {
					eventVerificationDetailsDTO.setGroupNumber(data[7].toString());
				}
				
				listOfVerifications.add(eventVerificationDetailsDTO);
			}
			eventVerificationDetailsListDTO.setEventVerificationDetailsDTO(listOfVerifications);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while finding event verification details.", e);
			throw new GIRuntimeException("Exception occurred while finding event verification details.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return eventVerificationDetailsListDTO;
	}
	
	public String processRealTimeVerification(EventVerificationDetailsDTO request){
		SsapApplicantEvent ssapApplicantEvent = null;
		try {
			ssapApplicantEvent = ssapApplicantEventRepository.findBySsapApplicantIdAndValidationStatus(
					Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getId())), ApplicantEventValidationStatus.PENDING_INFO);
			if (ssapApplicantEvent == null) {
				throw new GIException(NO_EVENTS_FOUND_FOR_APPLICANT);
			}
			
			if (ACTION_SUBMIT.equalsIgnoreCase(request.getAction())) {
				RealTimeVerificationDTO realTimeVerificationDTO = mapApplicantToDto(ssapApplicantEvent);
				realTimeVerificationDTO.setIssuerCode(request.getIssuerCode());
				realTimeVerificationDTO.setPolicyNumber(request.getPolicyNumber());
				realTimeVerificationDTO.setGroupNumber(request.getGroupNumber());
				List<LookupValue> values = lookupService.getLookupValueList("HMS_ISSUERS");
				
				String issuerCode = request.getIssuerCode();
				for(LookupValue value:values){
					if(null != issuerCode && issuerCode.equalsIgnoreCase(value.getLookupValueCode())) {
						realTimeVerificationDTO.setCarrierName(value.getLookupValueLabel());
						break;
					}
				}
			
				RealTimeHMSResponse realTimeHMSResponse = triggerRealTimeVerification(realTimeVerificationDTO, ssapApplicantEvent, TRUE);
				if(realTimeHMSResponse!=null){
					processHMSResponse(realTimeHMSResponse, ssapApplicantEvent);
				}
				
				persistEventVerificationDetails(ssapApplicantEvent, request);
			} else if (ACTION_SKIP.equalsIgnoreCase(request.getAction())) {
				ssapApplicantEvent.setRealTimeStatus(null);
				ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.OVERRIDDEN);
			} else {
				persistEventVerificationDetails(ssapApplicantEvent, request);
				ssapApplicantEvent.setRealTimeStatus(null);
				ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.OVERRIDDEN);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while processing real time verification", ex);
			ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.OVERRIDDEN);
			return updateApplicantEventAndApplication(ssapApplicantEvent, request);
		}
		
		return updateApplicantEventAndApplication(ssapApplicantEvent, request);
	}
	
	private String updateApplicantEventAndApplication(SsapApplicantEvent ssapApplicantEvent, EventVerificationDetailsDTO request){
		ssapApplicantEvent = ssapApplicantEventRepository.save(ssapApplicantEvent);
		
		List<SsapApplicantEvent> ssapApplicantEvents = ssapApplicantEventRepository.findBySsapAplicationEvent(ssapApplicantEvent.getSsapAplicationEvent());
		
		SsapApplication ssapApplication = ssapApplicationRepository.findById(ssapApplicantEvent.getSsapApplicant().getSsapApplication().getId());
		ssapApplication.setValidationStatus(extractApplicationStatus(ssapApplicantEvents));
		ssapApplication.setLastUpdatedBy(new BigDecimal(request.getUserId()));
		ssapApplication.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
		ssapApplicationRepository.save(ssapApplication);
		
		return ssapApplicantEvent.getValidationStatus().toString();
	}
	
	private void updateValidationStatus(SsapApplicantEvent ssapApplicantEvent, Integer lastUpdatedBy){
		EntityManager em = null;

		try {
			em = emf.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			
			SsapApplication ssapApplication = ssapApplicationRepository.findById(ssapApplicantEvent.getSsapApplicant().getSsapApplication().getId());
			
			Query query = em
					.createNativeQuery("UPDATE SSAP_APPLICATIONS SET VALIDATION_STATUS = ?, LAST_UPDATED_BY = ?, LAST_UPDATE_TIMESTAMP = ? WHERE ID = ?");
			
			query.setParameter(1, ApplicationValidationStatus.HMS_INTRANSIT.toString());
			query.setParameter(2, lastUpdatedBy);
			query.setParameter(3, new TSDate(), TemporalType.TIMESTAMP);
			query.setParameter(4, ssapApplication.getId());
			query.executeUpdate();

			em.getTransaction().commit();
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating validation status to hms intransit", exception);
			throw new GIRuntimeException("Exception occurred while updating validation status to hms intransit.", exception);
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
	}
	
	private String getValidationApplicantEventStatus(String status){
		Map<String, String> applicantEventStatus = new HashMap<>();
		applicantEventStatus.put("PENDING_INFO", "Pending Info");
		applicantEventStatus.put("OVERRIDDEN", "Overridden");
		applicantEventStatus.put("CANCELLED", "Cancelled");
		applicantEventStatus.put("VERIFIED", "Verified");
		applicantEventStatus.put("REJECTED", "Rejected");
		applicantEventStatus.put("SUBMITTED", "Submitted");
		applicantEventStatus.put("PENDING", "Pending");
		applicantEventStatus.put("NOTREQUIRED", "Not Required");
		applicantEventStatus.put("INITIAL", "Initial");
		
		return applicantEventStatus.get(status);
	}
	
	private void persistEventVerificationDetails(SsapApplicantEvent ssapApplicantEvent, EventVerificationDetailsDTO request){
		SsapApplicantEventVerification ssapApplicantEventVerification= null;
		try {
			ssapApplicantEventVerification = ssapApplicantEventVerificationRepository.findBySsapApplicantEventId(ssapApplicantEvent.getId());
			if(ssapApplicantEventVerification==null){
				ssapApplicantEventVerification = new SsapApplicantEventVerification();
				ssapApplicantEventVerification.setCreatedBy(Long.valueOf(request.getUserId()));
			} else {
				ssapApplicantEventVerification.setLastUpdatedBy(Long.valueOf(request.getUserId()));
			}
			ssapApplicantEventVerification.setSsapApplicantEvent(ssapApplicantEvent);
			ssapApplicantEventVerification.setIssuerCode(request.getIssuerCode());
			ssapApplicantEventVerification.setGroupNumber(request.getGroupNumber());
			ssapApplicantEventVerification.setPolicyNumber(request.getPolicyNumber());
			
			ssapApplicantEventVerificationRepository.save(ssapApplicantEventVerification);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while persisting event verification details", ex);
			throw new GIRuntimeException("Exception occurred while persisting event verification details.", ex);
		}
	}
	
}
