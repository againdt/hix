package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.ssap.renewal.service.VerificationService;
import com.getinsured.hix.batch.ssap.renewal.util.VerificationPartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

/**
 * Writer class is used to process Verification data.
 * 
 * @since August 12, 2019
 */
public class VerificationWriter implements ItemWriter<Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private VerificationService verificationService;
	private VerificationPartitionerParams verificationPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends Long> ssapApplicationIdListToWrite) throws Exception {

		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		if (CollectionUtils.isNotEmpty(ssapApplicationIdListToWrite)) {

			for (Long ssapApplicationId : ssapApplicationIdListToWrite) {

				try {
					verificationService.verificationSsap(ssapApplicationId);
				}
				catch (Exception e) {
					verificationService.logToGIMonitor(e, Long.toString(ssapApplicationId));
				}
			}
		}
		LOGGER.info("Successfully execute Verification Job.");
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public VerificationService getVerificationService() {
		return verificationService;
	}

	public void setVerificationService(VerificationService verificationService) {
		this.verificationService = verificationService;
	}

	public VerificationPartitionerParams getVerificationPartitionerParams() {
		return verificationPartitionerParams;
	}

	public void setVerificationPartitionerParams(VerificationPartitionerParams verificationPartitionerParams) {
		this.verificationPartitionerParams = verificationPartitionerParams;
	}
}
