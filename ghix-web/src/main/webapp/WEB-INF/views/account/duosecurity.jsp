<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript"
	src="<c:url value="/resources/js/duosecurity/Duo-Web-v1.bundled.js" />"></script>
	<style>
		.duoform {
		    left: -310px;
		    margin: 30px 0 0 50%;
		    position: relative;
		}
	</style>
	
	<script>
	  Duo.init({
	    //'host': 'api-053e5501.duosecurity.com',
	    'host': '${duoHostName}',
	    'sig_request': '${sig_request}',
	    'post_action': '/hix/account/user/login'
	  });
	</script>
</head>
<body>
	<div class="row-fluid">
		<div class="span12" id="rightpanel">
			<form method="POST" class="duoform" id="duo_form">
			<iframe id="duo_iframe" width="620" height="330" frameborder="0"></iframe>
			  <df:csrfToken/>
			  <input type="hidden" name="isDuoSecurityCleared" id="isDuoSecurityCleared" value="true" />
			</form>
		</div>
	</div>
</body>
</html>