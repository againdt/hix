import React, { Fragment } from 'react';
import _ from 'lodash';

// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Constants

import INPUT_TYPES from '../../../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../constants/validationTypes';


const MaritalStatusBlock = props => {

    const {
        auxData: {
            ButtonContainer,
            livesWithSpouse,
            personName,
            spouseName
        },
        currentData,
        indicatorsValues: {
            married
        },
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    const getNotification = () => {
        if(spouseName) {
            return (
                <div className="usa-alert usa-alert-info alert--info">
                    Information previously entered on your application indicates that &nbsp;
                    <strong>
                        { spouseName }
                    </strong>  &nbsp;
                    is spouse of &nbsp;
                    <strong>
                        { personName }
                    </strong>  &nbsp; and they live &nbsp;
                    <strong>
                        { livesWithSpouse ? 'together' : 'separately' }
                    </strong>.
                    If that is incorrect, please update your information.
                </div>
            );
        } else {
            return (
                <div className="usa-alert usa-alert-info alert--info">
                    Information previously entered on your application indicates that &nbsp;
                    <strong>
                        { personName }
                    </strong>  &nbsp;
                    is single.
                    If that is incorrect, please update your information.
                </div>
            );
        }
    };

    return (
        <Fragment>

            {
                getNotification()
            }

            <div className="subsection">
                <UniversalInput
                    legend={{
                        legendText: 'Do you want make any changes?'
                    }}
                    label={{
                        text: '',
                        labelClass: ['usa-width-one-whole', 'layout--02'],
                        ulClass: ['usa-width-one-whole margin-b-20'],
                        showRequiredIcon: true
                    }}
                    inputData={{
                        isInputRequired: true,
                        type: INPUT_TYPES.RADIO_BUTTONS,
                        classes:['disp-styl-none'],
                        name: 'married',
                        value: married,
                        options: DROPDOWN_OPTIONS.YES_NO,
                        validationType: VALIDATION_TYPES.CHOSEN,
                        currentData: currentData,
                        fields: []
                    }}
                    {...cleanedRestProps}

                />

                {
                    married &&

                    <ButtonContainer
                        type="updateInformationButton"
                        args={[]}
                    />
                }

            </div>

        </Fragment>
    );
};

export default MaritalStatusBlock;
