package com.getinsured.eligibility.at.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("mainDetermination")
@Scope("singleton")
public class MainDetermination extends BaseDeterminationClass {
	private static final Logger LOGGER = Logger.getLogger(MainDetermination.class);

	@Override
	void processLogicToDetermine(AccountTransferRequestDTO accountTransferRequestDTO, List<SsapApplication> ssapApplications) {
		List<SsapApplication> applicationsToBeClosed = new ArrayList<SsapApplication>();
		List<SsapApplication> applicationsThatAreEnrolled = new ArrayList<SsapApplication>();
		List<SsapApplication> otherYearApplications = new ArrayList<SsapApplication>();
		List<SsapApplication> sameYearApplications = new ArrayList<SsapApplication>();
		List<ApplicationsWithSameIdDTO> applicationsWithSameId = new ArrayList<ApplicationsWithSameIdDTO>();
		Set<BigDecimal> enrolledAppCmrIds = new HashSet<>();
		Set<BigDecimal> allAppCmrIds = new HashSet<>();
		boolean isLCETrue = false;
		boolean bCheck;
		boolean hasEnrolledApplication = false;
		boolean isQEDetermined = false;
		long enrolledApplicationId = 0l;
		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		
		for (SsapApplication ssapApplication : ssapApplications) {
			ApplicationsWithSameIdDTO fieldData = populateDataInDto(ssapApplication);
			if (ReferralUtil.convertToInt(ssapApplication.getCmrHouseoldId()) != ReferralConstants.NONE) {
				allAppCmrIds.add(ssapApplication.getCmrHouseoldId());
			}
			if (ssapApplication.getCoverageYear() == accountTransferRequestDTO.getCoverageYear()) {
				sameYearApplications.add(ssapApplication);
				/**
				 * Treat AT as LCE if either of following is true.
				 * application status = EN OR 
				 * application status = PN OR 
				 * application status = ER AND application dental status = EN  // no health and only dental enrollment is completed.
				 * 
				 * */
				if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus()) ||
						ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus()) ||
						(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationDentalStatus()) 
								&& ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus()))
						) {
					//As part of HIX-106461 we are allowing Old NF applications pass through LCE
					//if (ReferralConstants.Y.equals(ssapApplication.getFinancialAssistanceFlag())) {// For LCE/QEP check only for enrolled financial application
						bCheck = isEnrollmentActive(ssapApplication);
						fieldData.setEnrolledActive(bCheck);
						if (bCheck) {
							hasEnrolledApplication = true;
							isLCETrue = true;
							isQEDetermined = false;
							applicationsThatAreEnrolled.add(ssapApplication);
							enrolledAppCmrIds.add(ssapApplication.getCmrHouseoldId());
						} else {
							if (!hasEnrolledApplication) {
								isQEDetermined = true;
							}
						}
					//}
				} else if (super.mesApplicationsAllowedToClose(ssapApplication.getApplicationStatus())// handle MES applications
						&& !ApplicationStatus.CLOSED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())
				        && !ApplicationStatus.CANCELLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					applicationsToBeClosed.add(ssapApplication);
				}
			} else {
				otherYearApplications.add(ssapApplication);
			}
			applicationsWithSameId.add(fieldData);
		}

		accountTransferRequestDTO.setApplicationsWithSameId(applicationsWithSameId);

		if (ReferralUtil.listSize(applicationsToBeClosed) != ReferralConstants.NONE) {
			closeApplications(applicationsToBeClosed);
		}

		if (isLCETrue) {
			enrolledApplicationId = retrieveComparedApplicationId(applicationsThatAreEnrolled, enrolledAppCmrIds);
			LOGGER.info("Lce Comparison to be done with Enrolled Application  - " + enrolledApplicationId);
			accountTransferRequestDTO.setEnrolledApplicationId(enrolledApplicationId);
			accountTransferRequestDTO.setCompareToApplicationId(enrolledApplicationId);
			accountTransferRequestDTO.setLCE(true);
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.LCE.value());
		} else if (isQEDetermined || isQEPDuringOE(accountTransferRequestDTO.getCoverageYear()) || isQEPeriod(accountTransferRequestDTO.getCoverageYear())) {
			accountTransferRequestDTO.setQE(true);
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.QE.value());
			if (ReferralUtil.listSize(sameYearApplications) != ReferralConstants.NONE) {
				accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(sameYearApplications, accountTransferRequestDTO.getCoverageYear()));
				LOGGER.info("QE Comparison to be done with Application  - " + accountTransferRequestDTO.getCompareToApplicationId());
			}
		} else {
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.OE.value());
			if (ReferralUtil.collectionSize(allAppCmrIds) <= ReferralConstants.ONE && isOEPeriod(accountTransferRequestDTO.getCoverageYear())) {
				long coverageYear = accountTransferRequestDTO.getCoverageYear();
				List<SsapApplication> tmpYearApplications = new ArrayList<SsapApplication>();
				
				if (ReferralUtil.listSize(sameYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear();
					tmpYearApplications.addAll(sameYearApplications);
				} else if (ReferralUtil.listSize(otherYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear() - ReferralConstants.ONE;
					tmpYearApplications.addAll(otherYearApplications);
				}
				
				accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(tmpYearApplications, coverageYear));
				LOGGER.info("OE Comparison to be done with Application  - " + accountTransferRequestDTO.getCompareToApplicationId());
			}
		}

	}

}
