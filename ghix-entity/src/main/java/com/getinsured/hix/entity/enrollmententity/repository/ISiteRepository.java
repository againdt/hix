package com.getinsured.hix.entity.enrollmententity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;

public interface ISiteRepository extends JpaRepository<Site, Long> {

	Site findSiteById(int siteId);

	List<Site> findSiteByEntity(EnrollmentEntity enrollmentEntity);

	@Query("FROM Site s where s.entity.id = :enrollmentEntityId and s.siteType = :siteType")
	List<Site> findByEnrollmentEntityAndSiteType(@Param("enrollmentEntityId") int enrollmentEntityId,
			@Param("siteType") String siteType);
}
