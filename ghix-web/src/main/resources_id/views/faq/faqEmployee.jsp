<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a>Employee FAQs</h1>
		<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
			<df:csrfToken/>
			<ul class="nav nav-list faq-nav" id="nav">
						<li><a href="#employeeGroup1"><spring:message code="label.employeeGroup1" htmlEscape="false"/></a></li>
						<li><a href="#employeeGroup2"><spring:message code="label.employeeGroup2" htmlEscape="false"/></a></li>
						<li><a href="#employeeGroup3"><spring:message code="label.employeeGroup3" htmlEscape="false"/></a></li>
						<li><a href="#employeeGroup4"><spring:message code="label.employeeGroup4" htmlEscape="false"/></a></li>
						<li><a href="#employeeGroup5"><spring:message code="label.employeeGroup5" htmlEscape="false"/></a></li>
			</ul>
		</div>
		<div class="span9 " id="rightpanel">
		
		
			<!-------------------------------group 1----------------------------------------------->
			<div id="employeeGroup1">
				<div class="header">
					<h4><spring:message code="label.employeeGroup1" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 1 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeHowGetIntoAccount" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeGetIntoAccount1" htmlEscape="false"/></p>	  			
	  			<p><spring:message code="label.employeeGetIntoAccount2" htmlEscape="false"/></p>
	  			
	  			<!--------------------------- question 2 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeHowChangeAccount" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeChangeAccount" htmlEscape="false"/></p>	  			
				
				<!--------------------------- question 3 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatAfterLogin" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeAfterLogin1" htmlEscape="false"/></p>	
	  			<ul>
	  				<li><spring:message code="label.employeeAfterLogin1.action1" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeAfterLogin1.action2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeAfterLogin1.action3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeAfterLogin1.action4" htmlEscape="false"/></li>
	  			</ul>
	  			<p><spring:message code="label.employeeAfterLogin2" htmlEscape="false"/></p>	
				
				
			</div>
			
			
			
			
			<!-------------------------------group 2----------------------------------------------->
			<div id="employeeGroup2">
				<div class="header">
					<h4><spring:message code="label.employeeGroup2" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 4 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatEmrollmentPeriod" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeEmrollmentPeriod1" htmlEscape="false"/></p>	  			
	  			<p><spring:message code="label.employeeEmrollmentPeriod2" htmlEscape="false"/></p>
	  			
	  			<!--------------------------- question 5 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeCanChangeAfterEnrollment" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeChangeAfterEnrollment" htmlEscape="false"/></p>	
	  			
	  			<!--------------------------- question 6 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatSpecialEnrollment" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeSpecialEnrollment" htmlEscape="false"/></p>	  			
				
			</div>
			
			<!-------------------------------group 3----------------------------------------------->
			<div id="employeeGroup3">
				<div class="header">
					<h4><spring:message code="label.employeeGroup3" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 7 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatQulifyingEven" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeQulifyingEven" htmlEscape="false"/></p>	  			
	  			<p><spring:message code="label.employeeQulifyingEvenList" htmlEscape="false"/></p>
	  			<ul>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item1" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item5" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item6" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item7" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeQulifyingEvenList.item8" htmlEscape="false"/></li>
	  			</ul>
	  			<p><spring:message code="label.employeeQulifyingEvenDate" htmlEscape="false"/></p>
	  			<p><spring:message code="label.employeeQulifyingEvenSpecial" htmlEscape="false"/></p>
	  			
			</div>
			
			<!-------------------------------group 4----------------------------------------------->
			<div id="employeeGroup4">
				<div class="header">
					<h4><spring:message code="label.employeeGroup4" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 8 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatWaiveCoverage" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeWaiveCoverage" htmlEscape="false"/></p>	  			
	  			
	  			<!--------------------------- question 9------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatHappenWaiveCoverage" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeWaiveCoverageHappen1" htmlEscape="false"/></p>	  			
	  			<p><spring:message code="label.employeeWaiveCoverageHappen2" htmlEscape="false"/></p>	  			
				
			</div>
			
			<!-------------------------------group 5----------------------------------------------->
			<div id="employeeGroup5">
				<div class="header">
					<h4><spring:message code="label.employeeGroup5" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 10 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatEmployerContribution" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeEmployerContribution" htmlEscape="false"/></p>	  			
	  			
	  			<!--------------------------- question 11------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeHowAddDependent" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeAddDependent" htmlEscape="false"/></p>
	  			
	  			<!--------------------------- question 12 - How do I purchase dental coverage for my family?------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeDentalForFamilyQ" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeDentalForFamilyA" htmlEscape="false"/></p>						
	  			
	  			<!--------------------------- question 13------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeCanDifferentPlans" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeDifferentPlans" htmlEscape="false"/></p>
	  			
	  			<!--------------------------- question 14------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhereSeeSummary" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeSeeSummary" htmlEscape="false"/></p>
	  			<ul>
	  				<li><spring:message code="label.employeeSeeSummary.place1" htmlEscape="false"/></li>
	  				<li><spring:message code="label.employeeSeeSummary.place2" htmlEscape="false"/></li>	  				
	  			</ul>
	  			
	  			<!--------------------------- question 15 - What if I need help completing the Open Enrollment process?------------------------>
				<%-- <div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatIfNeedHelp" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeNeedHelp" htmlEscape="false"/></p> --%>
	  			
	  			<!--------------------------- question 16 - What is an e-signature? ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.employeeWhatESignature" htmlEscape="false"/></h4>
	  			</div>
	  			<p><spring:message code="label.employeeESignature" htmlEscape="false"/></p>	  	
	  			
				<p class="pull-right gutter10"><small><a id="role-top" href="#">Back to Top</a></small></p>			
			</div>

		</div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		