package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.platform.util.exception.GIException;

public interface NoticeQueuedService {
    void processNoticesQueued(String scheduledDate) throws GIException;
}
