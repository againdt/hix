<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>

<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>

<style type="text/css">
    .modal-body{overflow-y:inherit !important;}

    .form-horizontal .controls{margin-left:0 !important;}


    .modal .header {
        border-radius: 6px 6px 0 0;
        padding: 10px;
    }

    .uploadDialogHeader .header {
        margin: 0 !important;
    }

    .Ajaxloader{
        background-color: #F5F5F5;
        background-image: url("/hix/resources/images/Eli-loader.gif");
        background-position: center center;
        background-repeat: no-repeat;
        height: 769px;
        margin: -10% 0;
        opacity: 0.75;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
    }
</style>
<script type="text/javascript">
    var showAndHideRowNum = 0;
</script>

<%

    String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>
<div class="gutter10">
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><spring:message code="lbl.viewTickets"/></li>
        </ul><!--page-breadcrumb ends-->

        <div class="row-fluid">
            <h1>${Subject}</h1>
            <div class="row-fluid">
                <jsp:include page="ticketQuickActionBar.jsp">
                    <jsp:param value="${ticketId}" name="ticketId"/>
                    <jsp:param value="${tkmTicketsObj.tkmWorkflows.type}" name="ticketType"/>
                    <jsp:param value="${tkmTicketsObj.tkmWorkflows.category}" name="ticketCategory"/>
                </jsp:include>
            </div>
        </div>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <c:set var="tkmTicketsObj" value="${tkmTicketsObj}" scope="request" />
            <jsp:include page="ticketsummary.jsp">
                <jsp:param value="${ticketId}" name="ticketId"/>
                <jsp:param value="../ticketlist?pageNumber=${gotoPageNumber}" name="backButtonUrl"/>
            </jsp:include>
        </div>
        <div class="span9" id="rightpanel">
            <div style="font-size: 14px; color: red" id="tkmErrorDiv">
                <c:if test="${errorMsg != null && errorMsg != ''}">
                    <p><c:out value="${errorMsg}"></c:out><p/>
                    <c:remove var="errorMsg"/>
                </c:if>
            </div>
            <c:choose>
                <c:when test="${fn:length(DOC_LIST) > 0}">
                    <table class="table table-striped" id="attachedDocument">
                        <thead>
                        <tr class="header">
                            <th><spring:message code="tblheader_attachment.docName"/></th>
                            <th><spring:message code="tblheader_attachment.dateUploaded"/></th>
                            <th><spring:message code="tblheader_attachment.size"/></th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${DOC_LIST}" var="doc">
                            <tr>
                                <td>${doc.DocName}</td>

                                    <%-- <td>${doc.UpdatedDate}</td>
                              --%>

                                <fmt:parseDate value="${doc.UpdatedDate}" type="date"
                                               pattern="EEE MMM dd HH:mm:ss z yyyy" var="formatedDate" />
                                <td class="dataField"><fmt:formatDate
                                        value="${formatedDate}" type="date"
                                        pattern="MM-dd-yyyy hh:mm aa" timeZone="<%=timeZone%>" /></td>
                                <td>${doc.DocSize}</td>
                                <td style="white-space: nowrap"><div class="controls">
                                    <div class="dropdown">
                                        <a href="#" data-target="#" data-toggle="dropdown"
                                           role="button" id="dLabel" class="dropdown-toggle"> <i
                                                class="icon-cog"></i> <b class="caret"></b>
                                        </a>


                                        <ul id='action_${doc.index}' aria-labelledby="dLabel"
                                            role="menu" class="dropdown-menu pull-right">
                                            <c:set var = "tempDocPath"><encryptor:enc value = "${doc.DocPath}"/></c:set>
                                            <li><a style='cursor: pointer;' href='#'
                                                   onclick="javascript:showdetail('${tempDocPath}');"
                                                   class=""><i class="icon-eye-open"></i> <spring:message code="text_tkt.view"/></a></li>

                                            <c:choose>
                                                <c:when
                                                        test="${(TICKET_STATUS=='Completed' ||TICKET_STATUS =='Canceled' || TICKET_STATUS=='Resolved')}">

                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${doc.DocUploader== 'true'}">
                                                        <li><a style='cursor: pointer;' href='#'
                                                               onclick="javascript:cnfrmDelete('${doc.DocId}','${doc.DocPath}');"><i
                                                                class="icon-remove"></i> <spring:message code="text_tkt.delete"/></a></li>
                                                    </c:if>

                                                </c:otherwise>
                                            </c:choose>
                                        </ul>
                                    </div>

                                </div></td>

                            </tr>

                        </c:forEach>
                        </tbody>
                        <div id="fileupload_error" class="error"></div>
                    </table>
                </c:when>
                <c:otherwise>
                    <h4 class="alert alert-info">
                        <spring:message code='label.norecords' />
                    </h4>
                </c:otherwise>
            </c:choose>
            <jsp:include page="attachToTicket.jsp">
                <jsp:param value="${ticketId}" name="ticketId"/>
            </jsp:include>
            <div class="gutter10-tb">
                <div class="clearfix txt-center margin10">
                    <c:choose>
                        <c:when
                                test="${(TICKET_STATUS=='Completed' ||TICKET_STATUS =='Canceled' || TICKET_STATUS=='Resolved')}">

                        </c:when>
                        <c:otherwise>
                            <a href="#uploadDocumentDialog" id="displayuploadDialog" role="button" data-toggle="modal" class="btn primary"><spring:message code='link_tkt.addAttachment' /></a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="delete-doc-dialog-modal" style="display: none;">
    <p>
        <c:if test="${not empty ticketAttachmentDeleted}">
            ${ticketAttachmentDeleted}
            <input type="hidden" id="ticketAttachmentDeleted" name="ticketAttachmentDeleted" value="${ticketAttachmentDeleted}">
            <c:remove var="ticketAttachmentDeleted"/>
        </c:if>
    </p>
</div>
<div id="delete-cnfrm-dialog-modal" style="display: none;">
    <p>
        Are you sure you want to delete?
    </p>
</div>
<script  type="text/javascript" >
    $(document).ready(function() {
        $("#doclist").addClass("active navmenu");
        if ("${Upload_Failure}"!='') {
            $('#fileupload_error').html('<label class="error" style=""><span><em class="excl">!</em>'+"${Upload_Failure}"+'</span></label>');
        }

        var ticketStatus = "${TICKET_STATUS}";
        if(ticketStatus == 'Completed' || ticketStatus=='Canceled' || ticketStatus=='Resolved') {
            $('#btnAddAttachment').attr('style', 'display:none');
        }

        if(document.getElementById('ticketAttachmentDeleted') != null && document.getElementById('ticketAttachmentDeleted').value != '')
        {
            $( "#delete-doc-dialog-modal" ).dialog({
                modal: true,
                title:"Attachment Delete Confirmation",
                buttons: {
                    Ok: function() {
                        $( this ).dialog( "close" );
                    }
                },
            });
        }

    });

    function cnfrmDelete(docId,docPath){
        $( "#delete-cnfrm-dialog-modal" ).dialog({
            modal: true,
            title:"Attachment Delete Confirmation",
            buttons: {
                Yes: function() {
                    $( this ).dialog( "close" );
                    deletedoc(docId,docPath)
                },
                Cancel:function() {
                    $( this ).dialog( "close" );
                }
            },
        });
    }

    function showdetail(docpath) {
        docpath = encodeURI(docpath);
        var documentUrl = "<c:url value='/ticketmgmt/ticket/viewDocument?documentId="+docpath+"'/>";
        open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
    }
    function deletedoc(docid,docPath) {
        var validateUrl = "<c:url value='/ticketmgmt/ticket/deleteTicketAttachments'/>";
        var csrftoken;
        csrftoken= '${sessionScope.csrftoken}';
        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
        $.ajax({
            url : validateUrl,
            type : "POST",
            data : {
                documentId : docid,
                documentPath : docPath,
                csrftoken : csrftoken
            },
            success: function(response){
                if(response != "" && response != null){
                    $('#tkmErrorDiv').html(response);
                    $('.Ajaxloader').remove();
                    $('body').css('overflow','auto');
                }else{
                    window.location.reload(true);
                }
            },
            error: function(e){
                alert("Failed to delete attachment");
                $('.Ajaxloader').remove();
                $('body').css('overflow','auto');
            },
        });
    }
</script>
