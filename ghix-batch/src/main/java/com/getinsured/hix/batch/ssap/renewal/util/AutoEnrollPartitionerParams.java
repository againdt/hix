package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

/**
 * DTO class is used to add Auto Enroll Job parameters.
 * 
 * @since July 19, 2019
 */
@Component("autoEnrollPartitionerParams")
public class AutoEnrollPartitionerParams {

	private List<Long> renewalApplicationIdList = null;
	private AtomicLong renewalYear;
	private AtomicBoolean processErrorRecordFlag;
	private int serverName;

	public AutoEnrollPartitionerParams() {
		this.renewalApplicationIdList = new CopyOnWriteArrayList<Long>();
	}

	public List<Long> getRenewalApplicationIdList() {
		return renewalApplicationIdList;
	}

	public synchronized void clearRenewalApplicationIdList() {
		this.renewalApplicationIdList.clear();
	}

	public synchronized void addAllToRenewalApplicationIdList(List<Long> enrollmentIdList) {
		this.renewalApplicationIdList.addAll(enrollmentIdList);
	}

	public synchronized void addToRenewalApplicationIdList(Long enrollmentId) {
		this.renewalApplicationIdList.add(enrollmentId);
	}

	public AtomicLong getRenewalYear() {
		return renewalYear;
	}

	public void setRenewalYear(long renewalYear) {
		this.renewalYear = new AtomicLong(renewalYear);
	}

	public AtomicBoolean getProcessErrorRecordFlag() {
		return processErrorRecordFlag;
	}

	public void setProcessErrorRecordFlag(boolean processErrorRecordFlag) {
		this.processErrorRecordFlag = new AtomicBoolean(processErrorRecordFlag);
	}

	public int getServerName() {
		return serverName;
	}

	public void setServerName(int serverName) {
		this.serverName = serverName;
	}
	
}
