<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>


<style type="text/css">

.left-panel
    {        
       
        width:50%;
        height:500px;
        float:left;            
    }
    .right-panel
    {        
        
        width:50%;
        height:500px;
        float:left;
    }

</style>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<form class="form-horizontal" id="frmeditCampaign" name="frmeditCampaign" action="<c:url value="/admin/affiliate/editcampaign/${affiliateFlow.affiliateflowId}/${affiliate.affiliateId}" />" method="POST">
<div class="gutter10">
<div class="rowfluid">
		<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/affiliate/managecampaign"/>">&lt; <spring:message  code="label.back"/></a></li>
			<li><a href="<c:url value="/admin/affiliate/managecampaign"/>">Manage Campaign</a></li>
			 <li>Edit Campaign</li>
        </ul><!--page-breadcrumb ends-->
</div>

<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
 <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:forEach items="${FieldError}" var="element"> 
			  <tr>
			    <td><p><c:out value="${element}"></c:out><p/></td>
			  </tr>
			</c:forEach>
			<br>
		</div>
</div>
		<input  id="campaignId" name="campaignId" type="hidden" value="${campaign.campaignId}" class="parsley-validated parsley-error">
		<fieldset id="individual_member_0">
		<legend class="removemember" id="">Edit Campaign Information</legend>
		<div class="left-col-w" style="width:470px; float:left;">
			<table class="table table-border-none calenderTable">
				<tbody>
					<tr class="">
						<td class="txt-right span4"><label>Affiliate Id &nbsp;</label>
						</td>
						<td>
							<label id="affiliateIdValue">${affiliate.affiliateId}</label>
						</td>							 
					</tr>
					<tr>
						<td class="txt-right span4"><label>Flow Name&nbsp;</label></td>
						<td><label id ="flownameValue">${affiliateFlow.flowName}</label></td>
					</tr>  							   
					<tr>
						<td class="txt-right span4"><label>Account&nbsp;Executive&nbsp;</label></td>
						<td><label id="accountExecutiveValue">${affiliate.accountExecutive}</label></td>
					</tr>									  
					<tr>
						<td class="txt-right span4"><label>Affiliate&nbsp;Company&nbsp;</label></td>
						<td><label id = "affiliateCompanyValue">${affiliate.companyName}</label></td>
					</tr>										
					<tr class="">
						<td class="txt-right span4">
							<label>Affiliate&nbsp;Manager&nbsp;</label>
						</td>
						<td>
							<label id="affiliateManagerValue">${affiliate.managerName}</label>
						</td>
					</tr>
					<tr>
						<td class="txt-right span4"><label>Campaign&nbsp;Name</label></td>
						<td class="span9"><label id = "campaignNameValue">${campaign.campaignName}</label></td>
					</tr>
					<tr>
						<td class="txt-right span4"><label for="startDate">Start&nbsp;Date<img src="<c:url value="/resources/images/requiredAsterix.png"/>" />&nbsp;</label></td>
						<td id="startDateTd">
							<div class="abc pull-left width210">
								<input type="text" title="mm/dd/yyyy" data-type="dateIso" data-required="true" value="${startDate}" class="input-medium parsley-validated parsley-error"   name="startDate" id="startDate" />
								<div id="startDate_error" class="help-inline"></div>
							</div>
							<input class="datepick pull-left startDatePick" value="${startDate}">
						</td>
					</tr>
					<tr>
						<td class="txt-right span4"><label for="endDate">End&nbsp;Date<img src="<c:url value="/resources/images/requiredAsterix.png"/>" />&nbsp;</label></td>
						<td id="endDateTd">
							<div class="dateDiv pull-left width210">
								<input type="text" title="mm/dd/yyyy" data-type="dateIso" data-required="true" value="${endDate}" class="input-medium parsley-validated parsley-error"   name="endDate" id="endDate" />
								<div id="endDate_error" class="help-inline"></div>
							</div>
							<input class="datepick pull-left endDatePick" value="${endDate}">
						</td>
					</tr>
					<tr>
						<td class="txt-right span4"><label>Amount<img src="<c:url value="/resources/images/requiredAsterix.png"/>" />&nbsp;</label></td>
						<td><input maxlength="9" type="text" id="amountValue" name="amount" data-range="[0,999999.99]" data-required="true" value="${campaign.amount}" data-type="number" class="parsley-validated parsley-error"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="btn btn-primary btn-small" id="saveCampaign" value="Save">
						&nbsp;<input type="button" class="btn btn-primary btn-small" id="cancel" value="Cancel"></td>
					</tr>
				</tbody>
			</table>
		</div>
		</fieldset>
</div>
</form>
<script type="text/javascript">


$(function() {
	
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			minDate: '0',
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true,
			option : $.datepicker.regional["en-GB"],
			onSelect: function(selected) {
				$('#endDate').val($('.endDatePick').val());
				$('#startDate').val($('.startDatePick').val());
				
				var campaignValue ='<c:out value="${affiliate.companyName},${affiliateFlow.flowName},"/>';
				$('#campaignNameValue').text(campaignValue +$('.startDatePick').val()+','+$('#endDate').val());
			}
		});
	});
	
	
	$('#cancel').click(function (){
		  window.location='<c:url value="/admin/affiliate/managecampaign"/>';
	});
	
	
	 $('#frmeditCampaign').parsley( 'addListener', {
			onFormSubmit: function (isFormValid, event, ParsleyForm) {
				// if field is not visible, do not apply Parsley validation!
				if(isFormValid === "true"){
					var url='<c:url value="/admin/affiliate/editcampaign"/>';
					 $.post(url,function() {
						location.href='<c:url value="/admin/affiliate/editcampaign" />';
					 });
				}
				else if(isFormValid === false){
					//$('.calenderTable td#endDateTd img.ui-datepicker-trigger').css({'margin':'-52px 115px 0 0','float':'right'});
				}
				else{
					//$('.calenderTable td#endDateTd img.ui-datepicker-trigger').css({'margin':'8px 115px 0 0','float':'right'});
				}
			} 
		});
});
var validator = $("#frmeditCampaign").validate({ 
	rules : {
		    
		    startDate:{checkDuration : true},
		    endDate:{checkDuration : true},
    },
	messages : {
		
	    
	    'startDate' : { 
			checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationStart' javaScriptEscape='true'/></span>",				
		},
		'endDate' : {
			checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationEnd' javaScriptEscape='true'/></span>",
	 	}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10').show();
	},
	success:function(error,element){
		/*hides the error message on a valid input*/
		var elementId = $(element).attr('id');
		$('#'+elementId).parent().find('#'+elementId+'_error').hide();
	}
});

$('#frmeditCampaign').submit(function(){
	
	var re = /^\d{0,6}(\.\d{0,2})?$/;
	if (document.getElementById('amountValue').value.match(re)) {
		$('#amountValue').parent().find('.requiredError').remove();
	} else {
		$('#amountValue').parent().find('.requiredError').remove();
		$('#amountValue').parent().append('<p class="requiredError">Amount should be in form of xxxxxx.xx</p>');
		return false;
	}
	//checkDuration();
});

jQuery.validator.addMethod("checkDuration", function (value, element, param) {
	 var effectiveStartDate = new Date($("#startDate").val());
	 var effectiveEndDate = new Date($("#endDate").val());
	 
	 if(new Date(effectiveStartDate) >= new Date(effectiveEndDate)){
		 $('#startDate_error').show();
		 return false;
	 }else{
		 //return true;
	}
	 return true;
	 
});

$(document).ready(function(){
	$('.hasDatepicker').css('visibility','hidden');
});
</script>
