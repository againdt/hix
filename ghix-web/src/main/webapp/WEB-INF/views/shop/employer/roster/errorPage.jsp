<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row-fluid">
	<div class="span9" id="rightpanel">
		<!--titlebar-->
		<div class="gutter10 clearfix">
			<div style="font-size: 14px; color: red">
					<p>${errorMsg}<p/>
					<p><a href="<c:url value="${redirectURL}" />">Click here</a></p>
				<br>
			</div>
		</div>
		<!--gutter10-->
	</div>
	<!-- rightpanel -->
</div>
<!-- row-fluid -->
