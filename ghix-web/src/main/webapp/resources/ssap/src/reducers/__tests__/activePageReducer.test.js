import activePageReducer, { initialState } from '../activePageReducer';
import { activePageActionTypes } from '../../constants/actionTypes';

let state;
let exactValue;

describe('Test for activePageReducer', () => {

    beforeEach(() => {
        state = 8;
        exactValue = 12;
    });

    it('Invalid value has been passed (not "increase", "decrease" or number)', () => {
        expect(() => {
            activePageReducer(
                initialState,
                {
                    type: activePageActionTypes.CHANGE_PAGE,
                    page: []
                }
            );
        }).toThrow('Invalid value (not "increase", "decrease" or number) has been passed in activePageReducer');
    });

    it('This reducer hasn\'t been invoked', () => {
        expect(activePageReducer(
            initialState,
            {
                type : 'ANOTHER_ACTION_TYPE'
            }))
            .toEqual(initialState);
    });

    it('Updated state should be +1 than previous state in case of increasing', () => {
        expect(activePageReducer(state,
            {
                type : activePageActionTypes.CHANGE_PAGE,
                page: 'increase'
            }))
            .toBe(state + 1);
    });

    it('Updated state should be -1 than previous state in case of decreasing', () => {
        expect(activePageReducer(
            state,
            {
                type : activePageActionTypes.CHANGE_PAGE,
                page: 'decrease'
            }))
            .toBe(state - 1);
    });

    it('Updated state should be equal to action.page value', () => {
        expect(activePageReducer(
            state,
            {
                type : activePageActionTypes.CHANGE_PAGE,
                page: exactValue
            }
        ))
            .toBe(exactValue);
    });
});