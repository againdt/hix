package com.getinsured.hix.broker;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
@Scope("prototype")
public class BrokerProfileUpdateEmail extends NotificationAgent {
	private AccountUser userObj;
	private Map<String, String> singleData;
	
	public void setUserObj(AccountUser userObj) {
		this.userObj = userObj;
	}

	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();
		bean.put("name",userObj.getFirstName()+" "+userObj.getLastName() );
		bean.put("id",Integer.toString(userObj.getId()));
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To", this.userObj.getEmail());
		data.put("UserId", Integer.toString(userObj.getId()));

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
}