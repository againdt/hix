<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style type="text/css">
.form-border{
	    border: 1px solid #d3d3d3;
    border-top: none;
}
.search-applicant-results th, .search-applicant-results td {
  word-break: normal;
}
.effectiveYearHolder{
	overflow: hidden;
	height: 39px;
	margin-top:-12px;
}
.effectiveLabel{
	font-weight: normal;
	font-size: 12px;
}
.effectiveYearHolder select{
	margin-top:8px;
}
#consumerSearch .control-group{
	margin-bottom:5px !important;
}
</style>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<div class="gutter10"  ng-app="enrollmentApp" ng-controller="enrollmentAppCtrl">
	<div class="row-fluid">		
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out></p>
			</c:if>
			<br>
		</div>
		<div class="">
			<h1>
				1095-A Records &nbsp;
				<!--  
				<c:if test="${onPageLoad=='onSearchTrue'}">
				<small> <fmt:formatNumber
							type="number" value="${totalApplicants}" /> total records</small>
				</c:if>	-->	
			</h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="header">
					<h4>
						Search
						<div class="pull-right effectiveYearHolder">
							<span class="effectiveLabel">Coverage Year</span>
							<span><select class="input-small"  id="coverageYear" ng-model="editToolJson.coverageYear" ng-options="year for year in coverageYears">							
							</select></span>
						</div>
					</h4>
			</div>
		</div>
		<div class="row-fluid">
			<form  method="POST" action="<c:url value="/crm/member/searchapplicants" />" id="consumerSearch" name="consumerSearch" class="form-vertical gutter10 lightgray form-border" >
				<df:csrfToken/>
				<input type="hidden" id="effectiveYear" name="effectiveYear" value="${searchCriteria.effectiveYear}"></input>
				<div class="row-fluid">
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">First Name</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="First Name" ng-model="editToolJson.firstName" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">Last Name</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="Last Name" ng-model="editToolJson.lastName" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label">SSN(last 4)</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="SSN(last 4)" ng-model="editToolJson.ssn" maxlength="4" />
							</div>
						</div>
					</div>					
				</div>
				<div class="row-fluid">					
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">Marketplace Assigned Policy Number</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="Policy Number" ng-model="editToolJson.policyNumber" maxlength="10" />
							</div>
						</div>
					</div>	
					<div class="span4">
						<div class="control-group">
							<label class="control-label" >&nbsp;</label>
							<div class="controls">
								<button type="button" ng-click="resetSearchForm()" class="btn">Reset All</button>								
								<button type="button" ng-click="search1095()" class="btn btn-primary margin10-l"><spring:message  code='label.go'/></button>
							</div>
						</div>
					</div>				
				</div>				
			</form>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12" id="rightpanel">
			<table class="table table-striped search-applicant-results">
				<thead>
					<tr class="graydrkbg ">	
						<th style="cursor: pointer;"><div ng-click="setSortOrder()">Recipient Name <span ng-if="editToolJson.sortOrder=='ASC'"><img src="/hix/resources/images/i-aro-blu-sort-up.png" alt="State sort ascending" /></span> <span ng-if="editToolJson.sortOrder=='DESC'"> <img src="/hix/resources/images/i-aro-blu-sort-dwn.png" alt="State sort descending" /></span></div></th>
						<th>Recipient SSN</th>
						<th>Spouse Name</th>	
						<th>Spouse SSN</th>
						<th>Cov. Year</th>
						<th>Policy No.</th>
						<th>1095-A</th>												
					</tr>					
				</thead>
				<tr  ng-repeat="singleResult in searchResult">
						<td>{{singleResult.recepientFirstName}}&nbsp;{{singleResult.recepientLastName}}</td>													
						<td>{{singleResult.recepientSsn}}</td>
						<td>{{singleResult.spouseFirstName}}&nbsp;{{singleResult.spouseLastName}}</td>													
						<td>{{singleResult.spouseSsn}}</td>
						<td>{{singleResult.coverageYear}}</td>
						<td>{{singleResult.policyNumber}}</td>						
						<td>
							<a href="<c:url value="/enrollment/1095A/getById/{{singleResult.id}}"/>">View </a>															
						</td>
													
					</tr>
			</table>
			
		</div>		
	</div>
	
	</div>
	<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
	
	<div ng-show="modalForm.processing">
	<div id="processingModal" modal-show="modalForm.processing" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  		<div class="modal-body">
  		<p align ="center">Searching, please wait </p>
  		<img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/>
    		
  		</div>
	</div>
	</div>
	<!-- CSR Modal-->
</div>
<script src="/hix/resources/angular/angular-file-upload-shim.min.js"></script>
<script src="/hix/resources/angular/angular-file-upload.min.js"></script>
<script src="/hix/resources/angular/mask.js"></script>
<script src="/hix/resources/angular/ui-bootstrap-tpls-0.9.0.min.js"></script>
<script type="text/javascript" src="/hix/resources/js/enrollment1095/enrollment1095.js"></script>
<script src="/hix/resources/js/angular-sanitize.min.js"></script>
<script>var baseURL = "${pageContext.request.contextPath}";</script>