package com.getinsured.iex.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Component
public class SsapUtil {

	private static final Logger ssapUtilLogger = LoggerFactory.getLogger(SsapUtil.class);

	public static final String CONSUMER_ID = "consumerId";

	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";

	public static final String GENDER = "gender";

	public static final String LAST_NAME = "lastName";

	public static final String FIRST_NAME = "firstName";

	public static final String BIRTH_DATE = "birthDate";

	public static final String APPLICANT_GUID = "APPLICANT_GUID";

	public static final String EXTERNAL_APPLICANT_ID = "EXTERNAL_APPLICANT_ID";
	
	private static final String PRIMARY_PERSON_ID = "1";
	private static final String BLOOD_RELATIONSHIP = "bloodRelationship";
	private static final String INDIVIDUAL_PERSON_ID = "individualPersonId";
	private static final String RELATED_PERSON_ID = "relatedPersonId";
	private static final String RELATION = "relation";


	public enum SSAP_TABLE_NAMES {
		SSAP_APPLICATIONS, SSAP_APPLICANTS, SSAP_APPLICANT_EVENTS, SSAP_APPLICATION_EVENTS
  }

	@PersistenceUnit private EntityManagerFactory emf;

	public  String getNextSequenceFromDB(String sequenceName) {
	    Number nextSequenceNo = null;
	    EntityManager em = null;
	    try {
	    	em = emf.createEntityManager();
	    	
	    	if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
	    		nextSequenceNo = (Number) em.createNativeQuery("select nextval ( '"+sequenceName+"')"  ).getSingleResult();
	    	}else{
			nextSequenceNo = (Number) em.createNativeQuery("select "+sequenceName+".nextval from dual").getSingleResult();
	    	}
	    	
			
			if(nextSequenceNo!=null){
				return nextSequenceNo.toString();
			}else{
				throw new GIRuntimeException("Could not generate Sequence no for>>>>>>>>>>"+sequenceName);
			}
		} catch (Exception e) {
			ssapUtilLogger.error("ERROR GENERATING SEQUENCE>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("Could not generate Sequence no for>>>>>>>>>>"+sequenceName+">>>>Error>>>>>>>>"+e.getMessage());
		}finally{
			if(em!=null && em.isOpen()){
				em.close();
			}
		}

	}

	/**
	 * @see LifeChangeEventUtil#isEmpty(String)
	 *
	 *      Checks for null or blank String objects
	 *
	 * @param value
	 *            the string to check
	 * @return true or false
	 */
	public static boolean isEmpty(String value) {
		return value == null || "".equals(value.trim());
	}

	/**
	 * Retrieves applicant guid and external applicant id for given household member (inside of member data).
	 *
	 * @param memberData Map that contains first name, last name, dob, consumer id, social security number and gender of
	 *                   the member.
	 * @return record from database or empty Map.
	 * @throws Exception if exception occurred during SQL query or query creation/formatting.
	 */
	public Map<String, String> getApplicantIds(Map<String, Object> memberData) throws Exception {
		Validate.notNull(memberData);

		Map<String, String> applicantIds = new HashMap<>();
		Query query = null;
		StringBuilder buildQuery = new StringBuilder();

		buildQuery.append("SELECT applicant_guid, external_applicant_id from (	");
		buildQuery.append("	SELECT s_ap.APPLICANT_GUID applicant_guid, s_ap.EXTERNAL_APPLICANT_ID external_applicant_id FROM ssap_applicants s_ap	");
		buildQuery.append("		JOIN ssap_applications s_app on s_ap.ssap_application_id = s_app.id	");
		buildQuery.append("		JOIN cmr_household cmr_hh on s_app.cmr_houseold_id = cmr_hh.id	");
		buildQuery.append("			AND cmr_hh.id = :").append(CONSUMER_ID);
		buildQuery.append("			AND UPPER(s_ap.first_name) = UPPER(:").append(FIRST_NAME).append(")");
		buildQuery.append("			AND UPPER(s_ap.last_name) = UPPER(:").append(LAST_NAME).append(")");

		if(memberData.get(BIRTH_DATE) != null)
		{
			buildQuery.append("			AND s_ap.birth_date = TO_DATE(:").append(BIRTH_DATE).append(", '").append("Mon DD, YYYY").append("')");
		}

		if(memberData.get(GENDER) != null)	{
			buildQuery.append("	AND s_ap.gender = :").append(GENDER);
		}

		if(memberData.get(SOCIAL_SECURITY_NUMBER) != null)	{
			buildQuery.append("	AND s_ap.ssn = :").append(SOCIAL_SECURITY_NUMBER);
		}

		buildQuery.append("	ORDER BY s_ap.LAST_UPDATE_TIMESTAMP DESC) SUBQ6 ");

		EntityManager em = emf.createEntityManager();

		try {
			query = em.createNativeQuery(buildQuery.toString());
			query.setMaxResults(1);

			query.setParameter(FIRST_NAME, memberData.get(FIRST_NAME));
			query.setParameter(LAST_NAME, memberData.get(LAST_NAME));

			if(memberData.get(GENDER) != null) {
				query.setParameter(GENDER, memberData.get(GENDER));
			}

			if(memberData.get(CONSUMER_ID) != null) {
				query.setParameter(CONSUMER_ID,  memberData.get(CONSUMER_ID));
			}

			if(memberData.get(SOCIAL_SECURITY_NUMBER) != null) {
				query.setParameter(SOCIAL_SECURITY_NUMBER, memberData.get(SOCIAL_SECURITY_NUMBER));
			}

			if(memberData.get(BIRTH_DATE) != null) {
				final Date birthDate = (Date) memberData.get(BIRTH_DATE);
				try
				{
					DateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
					query.setParameter(BIRTH_DATE, sdf.format(birthDate));
				}
				catch(Exception sdfe) {
					ssapUtilLogger.warn("Unable to parse date {} with format of MMM dd, yyyy, trying yyyy-MM-dd format. (error: {})", birthDate, sdfe.getMessage());
					DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					query.setParameter(BIRTH_DATE, sdf.format(birthDate));
				}
			}

			List<Object[]> rawResult = query.getResultList();

			if(null != rawResult && !rawResult.isEmpty()) {
				Object[] rawData = rawResult.get(0);

				if(null != rawData[0]) {
					applicantIds.put(APPLICANT_GUID, rawData[0].toString());
				}

				if(null != rawData[1]) {
					applicantIds.put(EXTERNAL_APPLICANT_ID, rawData[1].toString());
				}
			}
		}
		catch (Exception e) {
			ssapUtilLogger.error("Exception occurred while searching for member applicant ids", e);
			throw e;
		}
		finally {
			if ( query != null ) {
				query = null;
			}

			if ( em != null && em.isOpen() ) {
				em.clear();
				em.close();
			}
		}

		return applicantIds;
	}

	/**
	 * Method to retrieve applicant IDs.
	 *
	 * @param memberData The Map collection of member data.
	 * @return applicantIDs The Map collection of member's applicant IDs data.
	 */
	public Map<String, String> retrieveApplicantIDs(Map<String, Object> memberData) {
		Validate.notNull(memberData);

		String applicantGUID = null, externalApplicantID = null, gender = null, socialSecurityNumber = null;
		Date birthDate = null;
		Map<String, String> applicantIDs = new HashMap<String, String>();
		Query query = null;
		StringBuilder buildquery = new StringBuilder();
		EntityManager em = emf.createEntityManager();

		Object rawValue = memberData.get(GENDER);

		if(null != rawValue) {
			gender = String.valueOf(rawValue);
		}

		rawValue = memberData.get(SOCIAL_SECURITY_NUMBER);

		if(null != rawValue) {
			socialSecurityNumber = String.valueOf(memberData.get(SOCIAL_SECURITY_NUMBER));
		}

		buildquery.append("select applicant_guid, external_applicant_id from (	");
		buildquery.append("	select s_ap.APPLICANT_GUID applicant_guid, s_ap.EXTERNAL_APPLICANT_ID external_applicant_id from ssap_applicants s_ap	");
		buildquery.append("		join ssap_applications s_app on s_ap.ssap_application_id = s_app.id	");
		buildquery.append("		join cmr_household cmr_hh on s_app.cmr_houseold_id = cmr_hh.id	");
		buildquery.append("			and cmr_hh.id = :").append(CONSUMER_ID);
		buildquery.append("			and UPPER(s_ap.first_name) = UPPER(:").append(FIRST_NAME).append(")");
		buildquery.append("			and UPPER(s_ap.last_name) = UPPER(:").append(LAST_NAME).append(")");
		buildquery.append("			and s_ap.birth_date = to_date(:").append(BIRTH_DATE).append(", '").append("Mon DD, YYYY").append("')");

		if(StringUtils.isNotBlank(gender))	{
			buildquery.append("			and s_ap.gender = :").append(GENDER);
		}

		if(StringUtils.isNotBlank(socialSecurityNumber))	{
			buildquery.append("			and s_ap.ssn = :").append(SOCIAL_SECURITY_NUMBER);
		}

		buildquery.append("		order by s_ap.LAST_UPDATE_TIMESTAMP desc) SUBQ6 ");
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			buildquery.append(" LIMIT 1 ");
		}else{
		buildquery.append("where ROWNUM <= 1");
		}
		


		try {
			query = em.createNativeQuery(buildquery.toString());

			query.setParameter(FIRST_NAME, memberData.get(FIRST_NAME));
			query.setParameter(LAST_NAME, memberData.get(LAST_NAME));

			if(null == memberData.get(BIRTH_DATE)) {
				query.setParameter(BIRTH_DATE, memberData.get(BIRTH_DATE));
			}
			else {
				birthDate = (Date) memberData.get(BIRTH_DATE);
				DateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
				query.setParameter(BIRTH_DATE, sdf.format(birthDate));
			}

            if(StringUtils.isNotBlank(gender))	{
            	query.setParameter(GENDER, memberData.get(GENDER));
            }

            if(null != memberData.get(CONSUMER_ID) && memberData.get(CONSUMER_ID) instanceof String){
            	query.setParameter(CONSUMER_ID, Long.parseLong((String) memberData.get(CONSUMER_ID)));
            }else  if(null != memberData.get(CONSUMER_ID)){
            	query.setParameter(CONSUMER_ID,  memberData.get(CONSUMER_ID));
            }

			if(StringUtils.isNotBlank(socialSecurityNumber))	{
				query.setParameter(SOCIAL_SECURITY_NUMBER, memberData.get(SOCIAL_SECURITY_NUMBER));
			}

			List<Object[]> rawResult = query.getResultList();

			if(null != rawResult && !rawResult.isEmpty()) {
				Object[] rawData = rawResult.get(0);

				if(null != rawData[0]) {
					applicantGUID = rawData[0].toString();
				}
				applicantIDs.put(APPLICANT_GUID, applicantGUID);

				if(null != rawData[1]) {
					externalApplicantID = rawData[1].toString();
				}
				applicantIDs.put(EXTERNAL_APPLICANT_ID, externalApplicantID);
			}
		}
		catch (Exception e) {
			ssapUtilLogger.error("Exception occurred while searching for Employees", e);
			throw e;
		}
		finally {
	         if ( query != null ) {
	                 query = null;
	         }

	         if ( em != null && em.isOpen() ) {
	                 em.clear();
	                 em.close();
	         }
		 }

		return applicantIDs;
	}
	
	public JsonObject getBloodRelationship(JsonArray householdMembers){
		if(householdMembers!=null){
			String personId;
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				JsonObject householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				if(PRIMARY_PERSON_ID.equals(personId)){
					return householdMember;
				}
			}
		}
		return null;
	}
	
	public String getApplicantRelation(JsonObject primaryHouseholdMember, String personId){
		if(primaryHouseholdMember!=null){
			JsonArray bloodRelationships = primaryHouseholdMember.get(BLOOD_RELATIONSHIP).getAsJsonArray();
			for(int i=0;i<bloodRelationships.size();i++){
				JsonObject relationship = bloodRelationships.get(i).getAsJsonObject();
				String individualPersonId = relationship.get(INDIVIDUAL_PERSON_ID).getAsString();
				String relatedPersonId = relationship.get(RELATED_PERSON_ID).getAsString();
				if(StringUtils.isNotEmpty(individualPersonId) && individualPersonId.equals(personId) 
						&& StringUtils.isNotEmpty(relatedPersonId) && PRIMARY_PERSON_ID.equals(relatedPersonId)){
					return relationship.get(RELATION).getAsString();
				}
			}
		}
		return null;
	}
	
	public List<BloodRelationship> getBloodRelationship(List<HouseholdMember> householdMemberList){
		if(householdMemberList!=null){
			final int size = householdMemberList.size();
			for (int i = 0; i < size; i++) {
				HouseholdMember member = householdMemberList.get(i);
				if(member.isPrimaryTaxFiler()){
					return member.getBloodRelationship();
				}
			}
		}
		return null;
	}
	
	public String getApplicantRelation(List<BloodRelationship> bloodRelationship, Long currentApplicantPersonId,String responsibleMemberPersonId){
		if(bloodRelationship!=null && bloodRelationship.size()>0){
			for(BloodRelationship relation:bloodRelationship){
				String individualPersonId = relation.getIndividualPersonId();
				String relatedPersonId = relation.getRelatedPersonId();
				if(StringUtils.isNotEmpty(individualPersonId) && individualPersonId.equals(currentApplicantPersonId.toString()) 
						&& StringUtils.isNotEmpty(relatedPersonId) && responsibleMemberPersonId.equals(relatedPersonId)){
					return relation.getRelation();
				}
			}
		}
		return null;
	}
	
	public SsapApplicant getPrimaryContactApplicantFromApplication(SsapApplication application) {
		SsapApplicant primaryContactApplicant = null;
		for (SsapApplicant applicant : application.getSsapApplicants()) {
			if (applicant.isPrimaryContact()) {
				primaryContactApplicant = applicant;
				break;
			}
		}
		if(primaryContactApplicant == null){
			for (SsapApplicant applicant : application.getSsapApplicants()) {
				if (applicant.getPersonId() == 1){
					primaryContactApplicant = applicant;
					break;
				}
			}
		}
		return primaryContactApplicant;
	}
	
	public SsapApplicant getPrimaryTaxFilerApplicantFromApplication(SsapApplication application) {
		SsapApplicant primaryTaxFilerApplicant = null;
		for (SsapApplicant applicant : application.getSsapApplicants()) {
			if (applicant.isPrimaryTaxFiler()) {
				primaryTaxFilerApplicant = applicant;
				break;
			}
		}
		if(primaryTaxFilerApplicant == null){
			for (SsapApplicant applicant : application.getSsapApplicants()) {
				if (applicant.getPersonId() == 1){
					primaryTaxFilerApplicant = applicant;
					break;
				}
			}
		}
		return primaryTaxFilerApplicant;
	}
	
	public HouseholdMember getApplicantJson(List<HouseholdMember> members, SsapApplicant applicantDB) {
		HouseholdMember applicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) applicantDB.getPersonId()) == 0) {
				applicantJson = householdMember;
				break;
			}
		}
		return applicantJson;
	}
	

	public String getPrimaryTaxFilerPersonIdFromHouseholdMembers(List<HouseholdMember> householdMemberList){
		String personId = PRIMARY_PERSON_ID;
		if(householdMemberList!=null){
			final int size = ReferralUtil.listSize(householdMemberList);
			for (int i = 0; i < size; i++) {
				HouseholdMember member = householdMemberList.get(i);
				if(member.isPrimaryTaxFiler()){
					personId = member.getPersonId().toString();
					break;
				}
			}
		}
		return personId;
	}
	
	public String getFiveDigitZipCode(String postalCode) {
		postalCode = StringUtils.isBlank(postalCode) ? null : postalCode;
		if(postalCode != null && postalCode.length() > 5){
			postalCode = postalCode.substring(0, 5);
		}
		return postalCode;
	}
}
