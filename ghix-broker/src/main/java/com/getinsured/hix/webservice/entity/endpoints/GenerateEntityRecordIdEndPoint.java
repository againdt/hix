package com.getinsured.hix.webservice.entity.endpoints;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.entity.service.EnrollmentEntityService;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdRequest;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault.Detail;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault.Faultstring;
import com.getinsured.hix.webservice.entity.generateentityrecordid.RecordType;
import com.getinsured.hix.webservice.entity.service.GenerateEntityRecordIdService;

/**
 * This is the EndPoint Class which will be used by third party to create record
 * id for assister/agent/enrollment entity based on record type
 * 
 */
@Endpoint
public class GenerateEntityRecordIdEndPoint {
	private static final String RECORD_ID_GENERATION_ERROR = "Record ID generation Error : ";

	private static final Logger LOGGER = Logger.getLogger(GenerateEntityRecordIdEndPoint.class);

	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/account/generateentityrecordid";
	private static final int NINE_DIGIT_NUMBER = 9;
	private static final int SEVEN_CHARACTER_STRING = 7;

	@Autowired
	private EnrollmentEntityService enrollmentEntityService;
	@Autowired
	private GenerateEntityRecordIdService generateEntityRecordIdService;

	/**
	 * This method will create record id for assister/broker/enrollment entity
	 * based on record type
	 * 
	 * @param request
	 *            the object encapsulating record type and information related
	 *            to the given record type which may be
	 *            broker/assister/enrollment entity
	 * @return response record id, response code and description
	 */
	public @ResponsePayload
	@PayloadRoot(localPart = "generateEntityRecordIdRequest", namespace = TARGET_NAMESPACE)	
	GenerateEntityRecordIdResponse generateEntityRecordId(@RequestPayload GenerateEntityRecordIdRequest request) {
		LOGGER.info("IND65 : Record ID generation EndPoint Starts");
		GenerateEntityRecordIdResponse response = new GenerateEntityRecordIdResponse();
		RecordType record = request.getRecordType();
		String recordType = record.value();
		int recordId = 0;

		//LOGGER.info("IND65 : Record ID generation Request Object : " + request);

		try {
			if (!BrokerUtils.isEmpty(recordType)) {
				if (BrokerConstants.BROKERRECORDTYPE.equalsIgnoreCase(recordType)) {
					String businessLegalName = request.getBusinessLegalName();
					String brokerLicenseNo = request.getAgentLicenseNo();
					String firstName = request.getFirstName();
					String lastName = request.getLastName();
					String federalEIN = request.getFederalEIN();
					String stateEIN = null;
					String email = request.getEmail();
					
					if(request.getStateEIN()!= null && !request.getStateEIN().equals("")){
						stateEIN = request.getStateEIN();
					}

					if (BrokerUtils.isEmpty(brokerLicenseNo) || !BrokerUtils.checkLengthOfString(brokerLicenseNo, SEVEN_CHARACTER_STRING)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDLICENSENO);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDLICENSENO, BrokerConstants.RESPONSEDESCINVALIDLICENSENO, response);
					} else if (BrokerUtils.isEmpty(firstName)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGFIRSTNAME);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGFIRSTNAME, BrokerConstants.RESPONSEDESCMISSINGFIRSTNAME, response);
					} else if (!BrokerUtils.isEmpty(email) && !BrokerUtils.validate(email)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDEMAILADDRESS, BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS, response);
					} else if (BrokerUtils.isEmpty(lastName)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGLASTNAME);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGLASTNAME, BrokerConstants.RESPONSEDESCMISSINGLASTNAME, response);
					}  else if (!BrokerUtils.isEmpty(federalEIN) && (!BrokerUtils.isNumeric(federalEIN) || !BrokerUtils.checkLengthOfString(federalEIN, NINE_DIGIT_NUMBER))) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDFEDERALEIN, BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN, response);
					} else if (BrokerUtils.isEmpty(stateEIN)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDSTATEEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDSTATEEIN, BrokerConstants.RESPONSEDESCINVALIDSTATEEIN, response);
					} else {
						// This call generate partial record in Broker table and
						// returns the generated record id
						recordId = generateEntityRecordIdService.generateBrokerRecordId(brokerLicenseNo, federalEIN, businessLegalName,
								email, stateEIN, null);
						setSuccessResponse(recordId, response);
						//LOGGER.info("IND65 : Record ID generation for Broker is successful : ID :" + recordId);
					}
				} else if(BrokerConstants.AGENCY_MANAGER.equalsIgnoreCase(recordType)){
					String brokerLicenseNo = request.getAgentLicenseNo();
					String federalEIN = request.getFederalEIN();
					String email = request.getEmail();
					
					if (BrokerUtils.isEmpty(brokerLicenseNo) || !BrokerUtils.checkLengthOfString(brokerLicenseNo, SEVEN_CHARACTER_STRING)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDLICENSENO);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDLICENSENO, BrokerConstants.RESPONSEDESCINVALIDLICENSENO, response);
					} else if (!BrokerUtils.isEmpty(email) && !BrokerUtils.validate(email)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDEMAILADDRESS, BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS, response);
					} else if (BrokerUtils.isEmpty(federalEIN) || !BrokerUtils.isNumeric(federalEIN) || !BrokerUtils.checkLengthOfString(federalEIN, NINE_DIGIT_NUMBER) 
							|| !generateEntityRecordIdService.checkAgencyFederalTaxId( federalEIN   )    ) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDFEDERALEIN, BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN, response);
					} else if (BrokerUtils.isEmpty(request.getAddressLine1())) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGADDRESS1);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGADDRESS1, BrokerConstants.RESPONSEDESCMISSINGADDRESS1, response);
					} else if (BrokerUtils.isEmpty(request.getCity())) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGCITY);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGCITY, BrokerConstants.RESPONSEDESCMISSINGCITY, response);
					} else if (BrokerUtils.isEmpty(request.getState())) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGSTATE);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGSTATE, BrokerConstants.RESPONSEDESCMISSINGSTATE, response);
					} else if (BrokerUtils.isEmpty(request.getZipCode())) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCMISSINGZIPCODE);
						setFailureResponse(BrokerConstants.RESPONSECODEMISSINGZIPCODE, BrokerConstants.RESPONSEDESCMISSINGZIPCODE, response);
					} else {
						generateEntityRecordIdService.createAgencyAndAgentRecords(request, response);
						response.setRecordType(RecordType.AGENCY_MANAGER);
					}
				} else if(BrokerConstants.ENROLLMENTENTITYRECORDTYPE.equalsIgnoreCase(recordType)) {
					String businessLegalName = request.getBusinessLegalName();
					String federalEIN = request.getFederalEIN();
					String stateEIN = request.getStateEIN();
					String email = request.getEmail();

					if (BrokerUtils.isEmpty(businessLegalName)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCBUSINESSLEGALNAMEMISSING);
						setFailureResponse(BrokerConstants.RESPONSECODEBUSINESSLEGALNAMEMISSING, BrokerConstants.RESPONSEDESCBUSINESSLEGALNAMEMISSING, response);
					} else if (!BrokerUtils.isEmpty(email) && !BrokerUtils.validate(email)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDEMAILADDRESS, BrokerConstants.RESPONSEDESCINVALIDEMAILADDRESS, response);
					} else if (BrokerUtils.isEmpty(federalEIN) || !BrokerUtils.isNumeric(federalEIN) || !BrokerUtils.checkLengthOfString(federalEIN, NINE_DIGIT_NUMBER)) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDFEDERALEIN, BrokerConstants.RESPONSEDESCINVALIDFEDERALEIN, response);
					} else if (stateEIN == null) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDSTATEEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDSTATEEIN, BrokerConstants.RESPONSEDESCINVALIDSTATEEIN, response);
					} else if (stateEIN.equals("")) {
						LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDSTATEEIN);
						setFailureResponse(BrokerConstants.RESPONSECODEINVALIDSTATEEIN, BrokerConstants.RESPONSEDESCINVALIDSTATEEIN, response);
					} else {
						// This call generates partial record in Enrollment
						// Entity table and
						// returns the generated record id
						recordId = generateEnrollmentEntityRecordId(businessLegalName, federalEIN, stateEIN, email);
						setSuccessResponse(recordId, response);
						//LOGGER.info("IND65 : Record ID generation for Enrollment Entity is successful : ID :" + recordId);
					}
				} else if (BrokerConstants.ASSISTERRECORDTYPE.equalsIgnoreCase(recordType)) {
						LOGGER.info("For assister partial record creation");
				} else {
					LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCINVALIDRECORDTYPE);
					setFailureResponse(BrokerConstants.RESPONSECODEINVALIDRECORDTYPE, BrokerConstants.RESPONSEDESCINVALIDRECORDTYPE, response);
				}
			} else {
				LOGGER.error(RECORD_ID_GENERATION_ERROR + BrokerConstants.RESPONSEDESCRECORDTYPEMISSING);
				setFailureResponse(BrokerConstants.RESPONSECODERECORDTYPEMISSING, BrokerConstants.RESPONSEDESCRECORDTYPEMISSING, response);
			}
		} catch (Exception exception) {

			LOGGER.error("Exception occurred while generating record id for : " + recordType + ": " , exception);

			setFailureResponse(BrokerConstants.EXCEPTIONFAULTCODE, BrokerConstants.RESPONSEDESCRECORDIDGENERATIONERROR, response);

			LOGGER.info("IND65 : Response Code : " + response.getResponseCode() + " : Response Description : " + response.getResponseDescription());
			LOGGER.info("IND65 :Record ID generation EndPoint Ends");

			return response;
		}

		LOGGER.info("IND65 : Response Code : " + response.getResponseCode() + " : Response Description : " + response.getResponseDescription());
		LOGGER.info("IND65 : Record ID generation EndPoint Ends");

		return response;
	}

	private int generateEnrollmentEntityRecordId(String businessLegalName, String federalEIN, String stateEIN, String email) {
		EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
		enrollmentEntity.setBusinessLegalName(businessLegalName);
		enrollmentEntity.setPrimaryEmailAddress(email);
		if (federalEIN != null) {
			enrollmentEntity.setFederalTaxID(Long.parseLong(federalEIN));
		}
		if (stateEIN != null) {
			enrollmentEntity.setStateTaxID(stateEIN);
		}

		return enrollmentEntityService.generateEnrollmentEntityRecordId(enrollmentEntity);
	}

	private GenerateEntityRecordIdResponse setFailureResponse(int faultCode, String str, GenerateEntityRecordIdResponse response) {

		Fault fault = new Fault();
		Faultstring faultString = new Faultstring();
		fault.setFaultcode(String.valueOf(faultCode));

		faultString.setValue(str);
		fault.setFaultstring(faultString);

		response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
		response.setResponseDescription(BrokerConstants.RESPONSEDESCFAILURE);
		response.setFault(fault);

		LOGGER.info("Fault Code : " + fault.getFaultcode() + " : Fault String : " + fault.getFaultstring().getValue());

		Detail detail = new Detail();
		List<String> list = new ArrayList<String>();
		list.add(str);
		detail.setValidationError(list);
		fault.setDetail(detail);
		return response;
	}

	private GenerateEntityRecordIdResponse setSuccessResponse(int recordId, GenerateEntityRecordIdResponse response) {
		response.setRecordID(recordId);
		response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
		response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
		return response;
	}
}
