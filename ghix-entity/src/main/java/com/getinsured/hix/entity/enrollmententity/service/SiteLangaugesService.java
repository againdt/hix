package com.getinsured.hix.entity.enrollmententity.service;


import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;

public interface SiteLangaugesService {
	
/**
 * Retrieves siteLanguages based on id
 * @param siteLanguagesId
 * @return
 */
	SiteLanguages findBySiteLanguagesId(long siteLanguagesId);
	/**
	 * Saves siteLanguages  
	 * @param siteLanguages
	 * @return saved siteLanguages 
	 */
	
	SiteLanguages saveSiteLanguage(SiteLanguages siteLanguages);
	
	/**
	 * Retrieves siteLanguages by site
	 * @param site
	 * @return siteLanguages
	 */
	SiteLanguages findSiteLanguagesBySite(Site site);
}
