package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Temporary I-551 Stamp Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class TemporaryI551StampDocument extends DhsDocument
{
  public TemporaryI551StampDocument()
  {
    super.setDocumentType(DocumentType.TEMPORARY_I551_STAMP);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("PassportCountry")
  private String passportCountry;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    if(!validPassportInformation(passportCountry, passportNumber, countryOfIssuance)) {
      log.error("When passportCountry supplied: {}, passport number: {} and county of issuance: {} are required",
          this.passportCountry, this.passportNumber, this.countryOfIssuance);
      return false;
    }

    return notBlank(this.alienNumber);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final TemporaryI551StampDocument document = new TemporaryI551StampDocument();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setPassportCountry(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getPassportCountry()
  {
    return passportCountry;
  }

  public void setPassportCountry(final String passportCountry)
  {
    this.passportCountry = passportCountry;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", TemporaryI551StampDocument.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("passportCountry='" + passportCountry + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
