<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<div id="sidebar" class="span3">
							<div class="header"></div>
							<ul class="nav nav-list">
								<li><a href="/hix/entity/entityadmin/assisterinformation?assisterId=${assister.id}"><spring:message code="label.assister.certifiedCounselor" /></a></li>
								<li><a href="/hix/entity/entityadmin/assisterprofile?assisterId=${assister.id}">Profile</a></li>
								<li class="active"><a href="#">Status</a></li>
							</ul>
						</div><!-- end of span3 -->
						<div class="span9">
							<form class="form-horizontal" id="frmupdatecertification" enctype="multipart/form-data" name="frmupdatecertification" method="post" action="editassisterstatus">
								<input type="hidden" id="brokerId" name="brokerId" value="194"><input type="hidden" id="licenseNumber" name="licenseNumber" value="3242183"><input type="hidden" id="documentId" name="documentId" value="">
								<div class="row-fluid">
									<div class="header">
										<h4 class="pull-left">Status</h4>
										<a class="btn btn-small pull-right" type="button" href="/hix/entity/entityadmin/assisterinformation?assisterId=${assister.id}">Cancel</a>
									</div>
									<div class="gutter10">
										<table class="table table-border-none table-condensed table-auto">
											<tbody>
												<tr>
													<td class="span4 txt-right">Status</td>
													<td><strong>${assister.status }</strong></td>
												</tr>
												
												<input type ="hidden" id="assisterId" name="assisterId" value="${assister.id}" />
												<tr>
													<td class="span4 txt-right"><label for="certificati	onStatus">New Status</label></td>
													<td>
														<select size="1" id="status" name="status" class="input-medium">
															<option value="">Select</option>
															<option id="active" value="Active">Active</option>
															<option id="inactive"value="Inactive">InActive</option>															
														</select>
														<input type="hidden" name="assisterStatus" id="assisterStatus" val=""/>
														<div id="certificationStatus_error"></div>
													</td>
												</tr>
												<tr>
													<td class="span4 txt-right"><label for="comments">Comment</label></td>
													<td><textarea id="comments" name="comments">${assister.activityComments}</textarea></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="form-actions">
									<input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"><input type="submit" class="btn btn-primary pull-right" name="save" id="save" value="Submit">
								</div>
							</form><!-- Showing comments -->
							<div class="gutter10">
								<table id="brokerStatusHistory" class="table table-condensed table-border-none table-striped">
									<thead>
										<tr>
											<th>Date</th>
											<th>Previous status</th>
											<th>New Status</th>
											<th>Comment</th>
										</tr>
									</thead>
									<tbody>
										<tr class="odd">
											<td>May 06, 2013</td>
											<td>Active</td>
											<td>Inactive</td>
											<td><a href="#">View Comment</a></td>
										</tr>
										<tr class="">
											<td>April 20, 2013</td>
											<td>Pending</td>
											<td>Active</td>
											<td><a href="#">View Comment</a></td>
										</tr>
										<tr class="odd">
											<td>June 06, 2013</td>
											<td>Inactive</td>
											<td>Pending</td>
											<td><a href="#">View Comment</a></td>
										</tr>
									</tbody>
								</table>
							</div><!-- end of .gutter10 -->
						</div><!-- end of span9 -->
					</div>

		<script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,92,new Date().getTime(),"","","","",""]);
		</script>
	<script type="text/javascript">
	$("#save").click(function(e){
		var selectedValue = $('#status option:selected').val();		
		$("#assisterStatus").val(selectedValue);
	})
	
	</script>
	