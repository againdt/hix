<%@ include file="../include.jsp" %>
<div id="appscr75A" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="page25AandB.additionalQuestions" text="Additional Questions"/>
			</h4>
		</div>
		
		<div class="container-fluid">
			<div class="control-group">
				<div class="alert alert-info">
				<p class="margin10-t">
					<spring:message code="ssap.page25AandB.somethingAboutYourFamily" text="We need to know a few more things about you and your family to make sure we are matching you accurately with the best available programs. "/> 
				</p>
				</div>
				<label>
					<em>
						<spring:message code="ssap.page25AandB.mandatoryFields" text="All fields on this Additional Questions section are required unless otherwise indicated."/>  
					</em>
				</label>
				<hr>
			</div>
                    
			<div class="control-group margin10-t">
				<strong><spring:message code="ssap.page2.youMayNeed" text="You may need:" /></strong> 
				<ul class="checklist gutter10">
					<li><spring:message code="ssap.page25A.currentHealthInsuranceInfo" text="Information about your current health insurance (if you have it)"/></li>
					<li><spring:message code="ssap.page25A.anyJobRelatedInsuranceInfo" text="Information about any job-related insurance you or your family may be able to get, even if "/>
					<spring:message code="ssap.page25A.enroll" text="you are not enrolled in it."/> </li>
				</ul>
			</div>
			
			
			
			<div class="control-group margin30-t hide">
				<label>
					<img alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
					<spring:message code="ssap.page25AandB.estimateTime" text="Estimated time for this section:"/>&nbsp;
					<strong><spring:message code="ssap.page25A.tenMinutesAndfifteenMinutes" text="10 Minutes - 15 Minutes"/></strong>
				</label>
			</div>
		</div>
	</div>
</div>
        
         
        