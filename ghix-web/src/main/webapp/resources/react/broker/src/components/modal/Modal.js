import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { removeErrorDetails } from '../../actions/actionCreators';

const Modal = props => {
  const { modalSize, titleText, bodyText } = props;
  let modalType = modalSize ? 'ds-c-dialog--' + modalSize : '';
  const styles = {
    minHeight: '450px'
  };
  return (
    <div data-demo="This div is for demo purposes only" style={styles}>
      <div
        className="ds-c-dialog-wrap"
        aria-labelledby="dialog-title"
        role="dialog"
      >
        <div className={classNames('ds-c-dialog', modalType)} role="document">
          <header className="ds-c-dialog__header" role="banner">
            <h1 className="ds-h2" id="dialog-title">
              {titleText}
            </h1>
            <button
              className="ds-c-button ds-c-button--transparent ds-c-dialog__close"
              aria-label="Close modal dialog"
              onClick={() => props.actions.removeErrorDetails()}
            >
              CLOSE
            </button>
          </header>
          <main role="main">
            <p className="ds-text gutter10">{bodyText}</p>
          </main>
          <aside className="ds-c-dialog__actions" role="complementary">
            <button
              className="ds-c-button ds-c-button--transparent"
              onClick={() => props.actions.removeErrorDetails()}
            >
              CANCEL
            </button>
            <button
              className="ds-c-button ds-c-button--danger"
              onClick={() => props.actions.removeErrorDetails()}
            >
              OK
            </button>
          </aside>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      removeErrorDetails
    },
    dispatch
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal);
