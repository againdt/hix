<%@ include file="../include.jsp" %>
<div id="appscr71" style="display: none; ">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<strong class="nameOfHouseHold"></strong>
				<spring:message code="ssap.page21.currentIncomeDetails" text="'s Current Income Details"/>
			</h4>
		</div>
		
		<form id="appscr71Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="hide" id="Financial_Job_Div"> 
				<div class="header margin10-b">
					<h4><spring:message code="ssap.page21.jobIncome" text="Job Income"/></h4>
				</div>
				<div class="gutter10">
					<div class="control-group">
						<label class="control-label" for="Financial_Job_EmployerName_id">
							<spring:message code="ssap.page21.nameOfEmployer" text="Name of employer:"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input id="Financial_Job_EmployerName_id" name="" type="text" value="" placeholder="Employer Name" class="input-medium"  data-parsley-length="[8, 256]" data-parsley-length-message="<spring:message code='ssap.error.employerNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.employerNameRequired'/>"/>
		                    <span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				
					<div class="control-group">
						<p>
							<spring:message code="ssap.page21.howMuchDoes" text="How much does"/>
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page21.getPaidPart1" text="get paid (before taxes are taken out)? You should also tell us here about a one-time amount you got from a current or former employer this month."/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<label class="control-label" for="Financial_Job_id">
							<spring:message code="ssap.page21.amount" text="Amount:"/>
							<strong>$</strong>
						</label><!-- end of label -->
						<div class="controls">
							<input name="" type="text" placeholder="Dollar Amount" id="Financial_Job_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
							<!-- <ul id="parsley-financial-amount" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				
					<div class="control-group">
						<p>
							<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text=" get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<label class="aria-hidden" for="Financial_Job_select_id"><spring:message code="ssap.page21.getThisAmount" text=" get this amount?"/></label>
						<div class="controls">
							<select id="Financial_Job_select_id" class="input-large" onchange="howOftenGetThisAmount(this)" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
							aria-label="Frequency options">
										<option value="">Select Frequency</option>
										<option value="Hourly">Hourly </option>
										<option value="Daily">Daily </option>
										<option value="Weekly">Weekly </option>
										<option value="EveryTwoWeeks">Every 2 week </option>
										<option value="TwiceAMonth">Twice a month  </option>
										<option value="Monthly">Monthly  </option>
										<option value="Yearly">Yearly  </option>
										<option value="OneTimeOnly">One time only  </option>
							</select>
							<!-- <ul id="parsley-jobIncomeFrequency" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
						</div>
					</div><!-- control-group ends -->
				
					<div class="control-group hide" id="howMuchPerWeek">
						<p>
							<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page21.workPerWeekAtThisJob" text="usually work per week at this job?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						
						<div class="control-label" for="Financial_Job_HoursOrDays_id">
						
							<div class="controls">
								<input name="" id="Financial_Job_HoursOrDays_id" type="text" placeholder="Hours Or Days per week" 
								value="" class="input-medium" data-parsley-pattern-message="<spring:message code='ssap.error.hoursValid'/>" 
								data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>" aria-label="Hours Or Days per week"/>
								<span class="errorMessage"></span>
							</div>
						</div><!-- end of label -->
					</div><!-- control-group ends -->
				</div><!-- .gutter10 -->
			</div><!-- Financial_Job_Div ends -->
			
			<div class="hide" id="Financial_Self_Employment_Div">
				<h4><spring:message code="ssap.page21.selfEmploymentIncome" text="Self-employment Income"/></h4>
				<div class="gutter10">
				
					<div class="control-group">
						<label class="control-label" for="Financial_Self_Employment_TypeOfWork">
							<spring:message code="ssap.page21.workType" text="Type of work:"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input id="Financial_Self_Employment_TypeOfWork" type="text" value="" placeholder="Type of work" class="input-medium" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.typeOfWorkRequired' />"/>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				
					<div class="control-group">
						<p>
							<spring:message code="ssap.page21.howMuchNetIncomeWill" text="How much net income (profits once expenses are paid) will"/>
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page21.selfEmploymentThisMonth" text="get from this self-employment this month? If the costs for this self-employment are more than the amount"/>
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page21.expectsToEarnCanWriteNegative" text="expects to earn, you can write a negative number."/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<label class="control-label" for="Financial_Self_Employment_id">
							<spring:message code="ssap.page21.amount" text="Amount:"/>
							<strong>$</strong>
						</label><!-- end of label -->
						<div class="controls">
							<input name="" type="text" placeholder="Dollar Amount" id="Financial_Self_Employment_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
							<!-- <ul id="parsley-selfEmployment-amount" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						
					</div><!-- control-group ends -->
				</div><!-- gutter10 -->
			</div><!-- Financial_Self_Employment_Div ends -->
			
			
			<div class="hide" id="Financial_SocialSecuritybenefits_Div"> 
				<h4><spring:message code="ssap.page21.SSBI" text="Social Security Benefits Income"/></h4>
				<div class="gutter10">
					<div class="control-group">
						<p>
							<spring:message code="ssap.page21.howMuchDoes" text="How much does"/>
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page21.getFromSocialSecurityRetirementDisabilitySurvivorsBenefits" text="get from Social Security retirement, disability, or survivors benefits?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<div class="margin20-t"></div>
						<label class="control-label" for="Financial_SocialSecuritybenefits_id">
							<spring:message code="ssap.page21.amount" text="Amount:"/>
							<strong>$</strong>
						</label><!-- end of label -->
						<div class="controls">
							<input name="" type="text" placeholder="Dollar Amount" id="Financial_SocialSecuritybenefits_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
							<!-- <ul id="parsley-ssb-amount" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
				
					<div class="control-group">
						<p>
							<spring:message code="ssap.page21.howMuchDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</p>
						<div class="controls margin20-t">
							<select id="Financial_SocialSecuritybenefits_select_id" class="input-medium" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
							aria-label="Frequency options">
								<option value="">Select Frequency</option>								
								<option value="Monthly">Monthly</option>
								<option value="Yearly">Yearly</option>
								<option value="OneTimeOnly">One time only</option>
							</select>
							<!-- <ul id="parsley-ssbFrequency" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
							<span class="errorMessage"></span>
						</div>
					</div><!-- control-group ends -->
				</div><!-- .gutter10 -->
			</div><!-- Financial_SocialSecuritybenefits_Div ends -->
			
			
			<div class="hide" id="Financial_Unemployment_Div">
				<h4><spring:message code="ssap.page21.unemploymentIncome" text="Unemployment Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.fromWhatStateGovernmentFormerEmployerDoes" text="From what state government or former employer does"/>
						<strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getUnemploymentBenefits" text="get unemployment benefits?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="aria-hidden" for="Financial_Unemployment_StateGovernment"><spring:message code="ssap.page21.fromWhatStateGovernmentFormerEmployerDoes" text="From what state government or former employer does"/></label>
					<div class="controls">
						<input name="" id="Financial_Unemployment_StateGovernment" type="text" placeholder="State Government/Employer Name" value="" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.unemploymentBenefitsRequired' />"/>
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.get" text="get?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_Unemployment_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_Unemployment_id" value="" class="input-medium curreny" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-unemployment-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="aria-hidden" for="Financial_Unemployment_select_id"><spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_Unemployment_select_id" class="input-large" onchange="displayDateUnemploymentExpire(this)" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>							
							<option value="Weekly">Weekly </option>							
							<option value="Monthly">Monthly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-unemploymentFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				
				<div class="control-group hide" id="isDateUnemploymentSet">
					<%-- <p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.usuallyWorkPerWeekAtThisJob" text="usually work per week at this job?"/>
					</p> --%>
					<p>
						<spring:message code="ssap.page21.Unemployment.benefitsExpire" text="Is there a date that the unemployment benifits are set to expire?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<div class="controls">
						<label>
							<input type="radio" value="Yes" name="payPerWeekRadio" onchange="payPerWeekRadioCheck()" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
							<spring:message code="ssap.page21.yes" text="Yes"/>
						</label>
						<label>
							<input type="radio" value="No" name="payPerWeekRadio" onchange="payPerWeekRadioCheck()" />
							<spring:message code="ssap.page21.no" text="No"/>
						</label>
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				
				<div class="control-group hide" id="dateEmployementExpire">
					<p>
						<spring:message code="ssap.page21.whatIsTheDate" text="What is the date?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label for="unemploymentIncomeDate" class="aria-hidden">						<spring:message code="ssap.page21.whatIsTheDate" text="What is the date?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<input name="" id="unemploymentIncomeDate" type="text" placeholder="MM/DD/YYYY" value="" class="input-medium" required />
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				
				</div>
			</div><!-- Financial_Unemployment_Div ends -->
			
			
			<div class="hide" id="Financial_Retirement_pension_Div">
				<h4><spring:message code="ssap.page21.retirementPensionIncome" text="Retirement/pension Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getFromThisRetirementAccountOrPension" text="get from this retirement account or pension? Include amounts received as a distribution from a retirement investment even if"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.isntRequired" text="isn't retired."/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_Retirement_pension_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_Retirement_pension_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-retirement-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label for="Financial_Retirement_pension_select_id" class="aria-hidden">			
								<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_Retirement_pension_select_id" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>							
							<option value="Weekly">Weekly </option>
							<option value="EveryTwoWeeks">Every 2 week </option>
							<option value="TwiceAMonth">Twice a month  </option>
							<option value="Monthly">Monthly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-retirementFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_Retirement_pension_Div ends -->
			
			
			<div class="hide" id="Financial_Capitalgains_Div">
				<h4><spring:message code="ssap.page21.capitalGainsIncome" text="Capital Gains Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.expectToGetFromNetCapitalGainsThisYear" text="expect to get from net capital gains (the profit after subtracting capital losses) this month?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_Capitalgains_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_Capitalgains_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-capitalGains-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_Capitalgains_Div ends -->
			
			
			<div class="hide" id="Financial_InvestmentIncome_Div">
				<h4><spring:message code="ssap.page21.investmentIncome" text="Investment Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getFromInvestmentIncomeLikeInterestAndDividends" text="get from investment income, like interest and dividends?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_InvestmentIncome_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_InvestmentIncome_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-investmentIncome-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label for="Financial_InvestmentIncome_select_id" class="aria-hidden">						<spring:message code="ssap.page21.howMuchDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_InvestmentIncome_select_id" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>							
							<option value="Weekly">Weekly </option>							
							<option value="Monthly">Monthly  </option>
							<option value="Quarterly">Quarterly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-investmentFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_InvestmentIncome_Div ends -->
			
			<div class="hide" id="Financial_RentalOrRoyaltyIncome_Div">
				<h4><spring:message code="ssap.page21.rentalOrRoyaltyIncome" text="Rental or Royalty Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code=".page21.getFromNetRentalIncome" text="get from net rental income (the profit after subtracting costs)?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_RentalOrRoyaltyIncome_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_RentalOrRoyaltyIncome_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-rentalRoyalty-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label for="Financial_RentalOrRoyaltyIncome_select_id" class="aria-hidden">						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_RentalOrRoyaltyIncome_select_id" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>							
							<option value="Weekly">Weekly </option>
							<option value="EveryTwoWeeks">Every 2 week </option>
							<option value="TwiceAMonth">Twice a month  </option>
							<option value="Monthly">Monthly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-rentalRoyaltyFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_RentalOrRoyaltyIncome_Div ends -->
			
			<div class="hide" id="Financial_FarmingOrFishingIncome_Div">
				<h4><spring:message code="ssap.page21.farmingOrFishingIncome" text=" Farming or Fishing Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getFromNetFarmingOrFishingIncome" text="get from net farming or fishing income (the profit after subtracting costs)?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_FarmingOrFishingIncome_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_FarmingOrFishingIncome_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/> " />
						<!-- <ul id="parsley-farmingFishing-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label for="Financial_FarmingOrFishingIncome_select_id" class="aria-hidden">						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_FarmingOrFishingIncome_select_id" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>
							<option value="Weekly">Weekly </option>
							<option value="EveryTwoWeeks">Every 2 week </option>
							<option value="TwiceAMonth">Twice a month  </option>
							<option value="Monthly">Monthly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-farmingFishingFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_FarmingOrFishingIncome_Div ends -->
			
			<div class="hide" id="Financial_AlimonyReceived_Div">
				<h4><spring:message code="ssap.page21.alimonyReceived" text="Alimony Received"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howMuchDoes" text="How much does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getFromAlimony" text="get from alimony?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="control-label" for="Financial_AlimonyReceived_id">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label><!-- end of label -->
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_AlimonyReceived_id" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
						<!-- <ul id="parsley-alimonyReceived-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<label class="aria-hidden" for="Financial_AlimonyReceived_select_id">						<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label>
					<div class="controls">
						<select id="Financial_AlimonyReceived_select_id" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
						aria-label="Frequency options">
							<option value="">Select Frequency</option>							
							<option value="Weekly">Weekly </option>
							<option value="EveryTwoWeeks">Every 2 week </option>
							<option value="TwiceAMonth">Twice a month  </option>
							<option value="Monthly">Monthly  </option>
							<option value="Yearly">Yearly  </option>
							<option value="OneTimeOnly">One time only  </option>
						</select>
						<!-- <ul id="parsley-alimonyFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul> -->
						<span class="errorMessage"></span>
					</div>
				</div><!-- control-group ends -->
				</div>
			</div><!-- Financial_AlimonyReceived_Div ends -->
			
			<div class="hide" id="Financial_OtherIncome_Div" >
				<h4><spring:message code="ssap.page21.otherIncome" text="Other Income"/></h4>
				<div class="gutter10">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page21.whichOtherTypeOfIncomeDoes" text="Which other type of income does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.get" text="get?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<div class="margin5-l">
						<div class="otherIncomeDiv">
							<label class="checkbox" for="appscr71CanceleddebtsCB">
								<input id="appscr71CanceleddebtsCB" type="checkbox" onchange="deductionCheck('appscr71CanceleddebtsCB')" name="otherIncome" parsley-mincheck="1" data-parsley-errors-container="#appscr71CanceleddebtsCBDivErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.checkBoxRequired'/>"/>
								<span><spring:message code="ssap.page21.canceledDebts" text="Canceled debts"/></span>
							</label>
							<div class="control-group hide" id="cancelled_Debts">
								<label class="control-label" for="cancelled_Debts_amount">
									<spring:message code="ssap.page21.amount" text="Amount:"/>
									<strong>$</strong>
								</label><!-- end of label -->
								<div class="controls">
									<input name="" type="text" placeholder="Dollar Amount" id="cancelled_Debts_amount" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
									<!-- <ul id="parsley-cancelled-Debts-amount" class="parsley-error-list hide">
										<li class="required" style="display: list-item;">
											<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
										</li>
									</ul> -->
									<span class="errorMessage"></span>
								</div><!-- end of controls-->
							</div><!-- control-group ends -->
							
							
						<div class="control-group hide" id="cancelledDebtsFrequencyDiv">
							<p>
								<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</p>
							<label for="cancelledDebtsFrequency" class="aria-hidden">								<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</label>
							<div class="controls">
								<select id="cancelledDebtsFrequency" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.option' text='Please Select a Option.' />"
								aria-label="Frequency options">
									<option value="">Select Frequency</option>									
									<option value="Weekly">Weekly </option>
									<option value="EveryTwoWeeks">Every 2 week </option>
									<option value="TwiceAMonth">Twice a month  </option>
									<option value="Monthly">Monthly  </option>
									<option value="Yearly">Yearly  </option>
									<option value="OneTimeOnly">One time only  </option>
								</select>
								<span class="errorMessage"></span>
							</div>
						</div><!-- control-group ends -->	
						</div>
						
						<div class="otherIncomeDiv">
							<label class="checkbox" for="appscr71CourtAwardsCB">
								<input id="appscr71CourtAwardsCB" type="checkbox" onchange="deductionCheck('appscr71CourtAwardsCB')" name="otherIncome"/>
								<span><spring:message code="ssap.page21.courtAwards" text="Court awards"/></span>
							</label>
							<div class="control-group hide" id="court_awrds">
								<label class="control-label">
									<spring:message code="ssap.page21.amount" text="Amount:"/>
									<strong>$</strong>
								</label><!-- end of label -->
								<div class="controls">
									<input name="" type="text" placeholder="Dollar Amount" id="court_awards_amount" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.please' text='Please Enter' /> <spring:message code='ssap.required.dollar.amount' text='the amount.' />" />
									<!-- <ul id="parsley-court-awards-amount" class="parsley-error-list hide">
										<li class="required" style="display: list-item;">
											<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
										</li>
									</ul> -->
									<span class="errorMessage"></span>
								</div><!-- end of controls-->
							</div><!-- control-group ends -->
							
							<div class="control-group hide" id="courtAwrdsFrequencyDiv">
								<p>
									<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
								</p>
								<div class="controls">
									<select id="courtAwrdsFrequency" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
									aria-label="Frequency options">
										<option value="">Select Frequency</option>										
										<option value="Weekly">Weekly </option>
										<option value="EveryTwoWeeks">Every 2 week </option>
										<option value="TwiceAMonth">Twice a month  </option>
										<option value="Monthly">Monthly  </option>
										<option value="Yearly">Yearly  </option>
										<option value="OneTimeOnly">One time only  </option>
									</select>
									<span class="errorMessage"></span>
								</div>
							</div><!-- control-group ends -->
						</div>
						
						<div class="otherIncomeDiv">
							<label class="checkbox" for="appscr71JuryDutyPayCB">
								<input id="appscr71JuryDutyPayCB" type="checkbox" onchange="deductionCheck('appscr71JuryDutyPayCB')" name="otherIncome"/>
								<span><spring:message code="ssap.page21.juryDutyPay" text="Jury duty pay"/></span>
							</label>
							<div id="appscr71CanceleddebtsCBDivErrorDiv"></div>
							<div class="control-group hide" id="jury_duty_pay">
								<label class="control-label" for="jury_duty_pay_amount">
									<spring:message code="ssap.page21.amount" text="Amount:"/>
									<strong>$</strong>
								</label><!-- end of label -->
								<div class="controls">
									<input name="" type="text" placeholder="Dollar Amount" id="jury_duty_pay_amount" value="" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
									<!-- <ul id="parsley-jury-duty-pay-amount" class="parsley-error-list hide">
										<li class="required" style="display: list-item;">
											<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
										</li>
									</ul> -->
									<span class="errorMessage"></span>
								</div><!-- end of controls-->
							</div><!-- control-group ends -->
							
							<div class="control-group hide" id="juryDutyPayFrequencyDiv">
								<p>
									<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
								</p>
								<label for="juryDutyPayFrequency" class="aria-hidden">									<spring:message code="ssap.page21.howOftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
								</label>
								<div class="controls">
									<select id="juryDutyPayFrequency" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"
									aria-label="Frequency options">
										<option value="">Select Frequency</option>										
										<option value="Weekly">Weekly </option>
										<option value="EveryTwoWeeks">Every 2 week </option>
										<option value="TwiceAMonth">Twice a month  </option>
										<option value="Monthly">Monthly  </option>
										<option value="Yearly">Yearly  </option>
										<option value="OneTimeOnly">One time only  </option>
									</select>
									<span class="errorMessage"></span>
								</div>
							</div><!-- control-group ends -->
						</div>
						<span class="errorMessage"></span>
						<%-- <div>
							<label class="checkbox">
								<input id="appscr71OtherCB" type="checkbox" checked/>
								<span><spring:message code="ssap.page21.other" text="Other"/></span>
							</label>
							
						</div> --%>
					</div>
				</div><!-- control-group ends -->
				
				<%-- <h4><spring:message code="ssap.page21.youDontNeedToTellUsAboutAnything" text="You don't need to tell us about child support, veteran's payments, or Supplemental Security Income (SSI)."/></h4> --%>
				
				<%-- <div class="control-group">
					<p>
						<spring:message code="ssap.page21.whatOtherTypeIncomeDoes" text=" What other type of income does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.have" text=" have?"/>
					</p>
					<div class="margin5-l">
						<input name="" id="Financial_OtherTypeIncome" type="text" placeholder="Other Income Type" value="" class="input-medium">
					</div>
				</div> --%><!-- control-group ends -->
				
				<%-- <div class="control-group">
					<label class="control-label">
						<spring:message code="ssap.page21.amount" text="Amount:"/>
						<strong>$</strong>
					</label>
					<div class="controls">
						<input name="" type="text" placeholder="Dollar Amount" id="Financial_OtherIncome_id" value="" class="input-medium amountMask" parsley-error-message="<spring:message code='ssap.required.please' text='Please Enter' /> <spring:message code='ssap.required.dollar.amount' text='the amount.' />"/>
						<ul id="parsley-other-amount" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul>
					</div>
				</div> --%><!-- control-group ends -->
				
				<%-- <div class="control-group">
					<p>
						<spring:message code="ssap.page21.howoftenDoes" text="How often does"/> <strong class="nameOfHouseHold"></strong> <spring:message code="ssap.page21.getThisAmount" text="get this amount?"/>
					</p>
					<div class="margin5-l">
						<select id="Financial_OtherIncome_select_id" class="input-large" parsley-error-message="<spring:message code='ssap.required.option' text='Please Select a Option.' />">
							<option value="SelectFrequency"><spring:message code="ssap.page21.selectFrequency" text="Select Frequency"/></option>
							<option value="OneTimeOnly"><spring:message code="ssap.page21.oneTimeOnly" text="One time only"/>  </option>
							<option value="Weekly"><spring:message code="ssap.page21.weekly" text="Weekly "/>  </option>
							<option value="EveryTwoWeeks"><spring:message code="ssap.page21.every2Week" text="Every 2 weeks" /> </option>
							<option value="TwiceAMonth"><spring:message code="ssap.page21.twiceAMonth" text="Twice a month" /> </option>
							<option value="Monthly"><spring:message code="ssap.page21.monthly" text="Monthly" />  </option>
							<option value="Yearly"><spring:message code="ssap.page21.yearly" text="Yearly" />   </option>
						</select>
						<ul id="parsley-otherFrequency" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul>
					</div>
				</div> --%><!-- control-group ends -->
				</div>
			</div><!-- Financial_OtherIncome_Div ends -->
		</form>
	</div>
</div>