package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

/**
 * DTO class is used to add Clone Job parameters.
 * 
 * @since July 30, 2019
 */
@Component("clonePartitionerParams")
public class ClonePartitionerParams {

	private List<Long> ssapApplicationIdList = null;
	private List<String> applicationStatusList;
	private AtomicLong renewalYear;
	private AtomicBoolean cloneToNFA;
	private AtomicBoolean isCloneProgramEligibility;

	public ClonePartitionerParams() {
		this.ssapApplicationIdList = new CopyOnWriteArrayList<Long>();
		this.applicationStatusList = new CopyOnWriteArrayList<String>();
		this.isCloneProgramEligibility = new AtomicBoolean(false);
	}

	public List<Long> getSsapApplicationIdList() {
		return ssapApplicationIdList;
	}

	public synchronized void clearSsapApplicationIdList() {
		this.ssapApplicationIdList.clear();
	}

	public synchronized void addAllToSsapApplicationIdList(List<Long> ssapApplicationList) {
		this.ssapApplicationIdList.addAll(ssapApplicationList);
	}

	public synchronized void addToSsapApplicationIdList(Long ssapApplicationId) {
		this.ssapApplicationIdList.add(ssapApplicationId);
	}

	public AtomicLong getRenewalYear() {
		return renewalYear;
	}

	public void setRenewalYear(long renewalYear) {
		this.renewalYear = new AtomicLong(renewalYear);
	}

	public List<String> getApplicationStatusList() {
		return applicationStatusList;
	}

	public synchronized void clearApplicationStatusList() {
		this.applicationStatusList.clear();
	}

	public synchronized void addAllToApplicationStatusList(List<String> applicationStatusList) {
		this.applicationStatusList.addAll(applicationStatusList);
	}

	public synchronized void addToApplicationStatusList(String applicationStatus) {
		this.applicationStatusList.add(applicationStatus);
	}

	public AtomicBoolean getCloneToNFA() {
		return cloneToNFA;
	}

	public void setCloneToNFA(AtomicBoolean cloneToNFA) {
		this.cloneToNFA = cloneToNFA;
	}

	public AtomicBoolean getIsCloneProgramEligibility() {
		return isCloneProgramEligibility;
	}

	public void setIsCloneProgramEligibility(AtomicBoolean isCloneProgramEligibility) {
		this.isCloneProgramEligibility = isCloneProgramEligibility;
	}
}
