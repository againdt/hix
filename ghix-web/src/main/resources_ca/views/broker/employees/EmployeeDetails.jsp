<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<script type="text/javascript">
	
	$(document).ready(function() {		
		$("#emplNewcomment").removeClass("link");
		$("#emplViewEmployerAccount").removeClass("link");
		$("#emplViewEmployeeAccount").removeClass("link");
		$("#emplComment").removeClass("link");
		$("#emplEnrollment").removeClass("link");
		$("#emplSummary").addClass("active");
	});
</script>


<!--start page-breadcrumb -->
<div class="gutter10-lr">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/employers"/>"><spring:message code="label.employers"/></a></li>
			<li><spring:message code="label.brkactive"/></li>
			<li>Employee Name ${employer.name} Employee Name</li>
			<li><spring:message code="label.agent.employers.Summary"/></li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->
</div>
<!-- gutter10 -->

	<div class="row-fluid">
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>


	<!--  Latest UI -->
	<c:if test="${message != null}">
		<div class="errorblock alert alert-info">
			<p>${message}</p>
		</div>
	</c:if>
     <div class="nav nav-list">
		<h1>${objEmployee.name} <small>${objEmployee.employer.name}</small></h1>
	</div> 
	
	<div class="row-fluid">
		<!-- #sidebar -->			
	    <jsp:include page="employeeLeftNavigationMenu.jsp" />
	   	<!-- #sidebar ENDS -->	
		
		<c:set var="todaysDate" scope="page" value="<%=new java.util.Date()%>" />
		<div class="span9" id="rightpanel">
			 <div class="header">
			       <h4 class="span11"><spring:message code="label.employeeDetails.Summary"/></h4>	
		     </div>
		    <form class="form-horizontal" id="" name="" action="" method="POST">
				<div class="gutter10">
				 <table class="table table-border-none">	
					<tbody>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.PrimaryApplicant"/></th>
							<td><strong>${objEmployee.name}</strong></td>
						</tr>
						<c:forEach var="memeber" items="${listEmployeeDetails}">
							<c:if test="${memeber.type == 'EMPLOYEE'}" >
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Age"/></th>
									<c:set var="age" scope="page" value="${(todaysDate.time - memeber.dob.time)/(1000*60*60*24*365)}" />
									<td><strong><fmt:formatNumber type="number" pattern="00" value="<%=Math.floor(Double.parseDouble(String.valueOf(pageContext.getAttribute(\"age\")))) %>" ></fmt:formatNumber></strong></td>
								</tr>
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Address"/></th>
									<td>
										<strong>
													${memeber.location.address1}<br/>
													<c:if test="${memeber.location.address2 != null}" >
														${memeber.location.address2}<br/>
													</c:if>
													
													<c:if test="${!empty fn:trim(memeber.location.city) }">
													${memeber.location.city},
													</c:if>
													 ${memeber.location.state} ${memeber.location.zip}
													
										</strong>
									</td>
								</tr>
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.PhoneNumber"/></th>
									<td><strong>${memeber.contactNumber}</strong></td>
								</tr>
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.EmailAddress"/></th>
									<td><strong>${memeber.email}</strong></td>
								</tr>
							</c:if>
						</c:forEach>
						
						<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.EmploymentStatus"/></th>
								<td>
									<strong>
										<c:choose>
											<c:when test="${objEmployee.status == 'ENROLLED'}">Enrolled</c:when>
											<c:when test="${objEmployee.status == 'TERMINATED'}">Terminated</c:when>
											<c:when test="${objEmployee.status == 'WAIVED'}">Waived</c:when>
											<c:when test="${objEmployee.status == 'NOT_ELIGIBLE'}">Not Eligible</c:when>
											<c:when test="${objEmployee.status == 'NOT_ENROLLED'}">Not Enrolled</c:when>
											<c:when test="${objEmployee.status == 'DELETED'}">Deleted</c:when>
											<c:when test="${objEmployee.status == 'ELIGIBLE'}">Eligible</c:when>
											<c:otherwise>${objEmployee.status}</c:otherwise>
										</c:choose>
									</strong>
								</td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.EnrollmentStatus"/></th>
							<td><strong>${enrollmentStatus}</strong></td>
						</tr>
											
					</tbody>
				</table>
				</div>
<%-- 				<div class="header">
			       <h4 class="span11"><spring:message code="label.employeeDetails.Financial"/></h4>	
		     	</div>
		     	<div class="gutter10">
				 <table class="table table-border-none">	
					<tbody>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.EstimatedAnnualSalary"/></th>
							<td><strong>${objEmployee.yearlyIncome}</strong></td>
						</tr>
					</tbody>
				</table>
				</div> --%>
				<div class="header">
			       <h4 class="span11"><spring:message code="label.employeeDetails.Dependents"/></h4>	
		     	</div>
		     	<div class="gutter10">
				 <table class="table table-border-none">	
					<tbody>
						<c:forEach var="memeber" items="${listEmployeeDetails}">
							<c:if	test="${memeber.type == 'SPOUSE'}" >
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Spouse"/></th>
										<td><strong>${memeber.firstName} ${memeber.lastName}</strong></td>
									</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Age"/></th>
										<c:set var="age" scope="page" value="${(todaysDate.time - memeber.dob.time)/(1000*60*60*24*365)}" />
										<td><strong><fmt:formatNumber type="number" pattern="00" value="<%=Math.floor(Double.parseDouble(String.valueOf(pageContext.getAttribute(\"age\")))) %>" ></fmt:formatNumber></strong></td>
									</tr>
									<c:set var="isSpouse" scope="page" value="${true}"/>
							</c:if>
						</c:forEach>
						<c:if test = "${!isSpouse}">
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Spouse"/></th>
								<td><strong>N/A</strong></td>
							</tr>
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Age"/></th>
								<td><strong>N/A</strong></td>
							</tr>
						</c:if>	
						
						<c:forEach var="memeber" varStatus="i" items="${listEmployeeDetails}">
							<c:if	test="${memeber.type == 'CHILD'}" >
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Child"/> ${i.index} </th>
										<td><strong>${memeber.firstName} ${memeber.lastName}</strong></td>
									</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.employeeDetails.Age"/></th>
										<c:set var="age" scope="page" value="${(todaysDate.time - memeber.dob.time)/(1000*60*60*24*365)}" />
										<td><strong><fmt:formatNumber type="number" pattern="00" value="<%=Math.floor(Double.parseDouble(String.valueOf(pageContext.getAttribute(\"age\")))) %>" ></fmt:formatNumber></strong></td>
									</tr>
							</c:if>
						</c:forEach>
						
					</tbody>
				</table>
				</div>
				<input type="hidden" name="employerName" id="employerName"
					value="${employer.name}" />
			</form>

			<!-- Modal -->	
				<c:set var="employerName" scope="request" value="${objEmployee.employer.name}"/>
				<c:set var="employerID" scope="request" value="${objEmployee.employer.id}"/>
				<c:set var="brokerFirstName" value="${brokerFirstName}"/>
				
				<jsp:include page="switch_employer_confirmation.jsp"/>			
			<!-- Modal end -->
		</div>
	</div>
	<!-- row-fluid -->

<!--  Latest UI -->
<!-- code for employee switch -->
<form name="employeeViewForm" id="employeeViewForm" action="<c:url value="/broker/employee/dashboard" />" novalidate="novalidate">
<div id="employeeViewDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="employeeViewDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"><spring:message code="label.employee.viewemployeeaccount"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employee view" will take you to the Employee's portal for ${objEmployee.name}.<br/>
					Through this portal you will be able to take actions on behalf of the Employee.<br/>
					Proceed to Employee view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkAgentView" name="checkAgentView"  > 
    <div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/> </div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">  <spring:message code="label.employee.cancle"/>  </button>
   <button class="btn btn-primary" type="submit">  <spring:message code="label.employee.employeeview"/>  </button>
   <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employee" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${objEmployee.id}" />
	
	<c:forEach var="memeber" items="${listEmployeeDetails}">
		<c:if test="${memeber.type == 'EMPLOYEE'}" >
		 	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${memeber.firstName}" />
		</c:if>
	</c:forEach>
	
  </div>
</div>
</form>