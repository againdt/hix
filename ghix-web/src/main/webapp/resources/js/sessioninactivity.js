var minuteCounter = 0;
var clientThreshold = 15;
var popupThreshold = 10;
var clientActivity = false;

$(document).ready(
function() {
	clientThreshold = parseInt(clientInactivityThreshold) || 15;
	popupThreshold = parseInt(popupThresholdValue) || clientThreshold - 5;

	pingServer();
	
	setInterval(function(){scheduledTasks();}, 60000); // Fire this every minute
		
	$(this).keydown (function (e) {
		clientActivity = true;
	});

	$(this).click(function (e) {
		clientActivity = true;
	});
});

function scheduledTasks() {
	minuteCounter++;
	
	//based on client inactivity, notify client of session expire. Extend session on client action
	if(minuteCounter >= popupThreshold){
		checkClientActivity();
	}

}

function pingServer() {
	if (pingUrl != "" ) {
		$.ajax({
			type: 'GET',
			url: pingUrl,
			dataType: 'json',
			cache:false,
			asynch:true
		}).done(function(data, statusText, xhr){
			  var status = xhr.status;                
			  var head = xhr.getAllResponseHeaders(); 
			  console.log(status);
			  console.log(statusText);
			  console.log(head);
			  console.log(data);
		});
		
	}
}

function sessionRenew() {
	$.ajax({
		type : "GET",
		url : "/hix/preeligibility/session/ping",
		cache:false,
		asynch:true,
		success : function(response) {
			clientActivity = false;
			minuteCounter = 0;
			pingServer();
			$('#activityModal').modal('hide');
		}
	});
}

function checkClientActivity() {
	
	if (clientActivity == true && minuteCounter < clientThreshold) {
		sessionRenew();
	}
	
	if (clientActivity == false &&  minuteCounter < clientThreshold) { 
		//alert("Your session is about to expire. Click OK to extend the session");
		$('#activityModal').modal();
		$('#timeout-warning').focus();
		console.log("focus!");
	}
	// Complete time out
	if (clientActivity == false && minuteCounter >= clientThreshold) { // 29 minutes
		if(authenticatedPage == "true") {
            localStorage.removeItem('currentTab');
			window.location.href=logoutUrl;
		}
		else {
			window.location.reload();
		}
	}
}
