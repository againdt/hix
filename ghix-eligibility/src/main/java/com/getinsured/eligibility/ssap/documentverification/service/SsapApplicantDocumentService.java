package com.getinsured.eligibility.ssap.documentverification.service;

import com.getinsured.eligibility.ssap.documentverification.exceptions.SsapDocumentsServiceException;
import com.getinsured.hix.model.ConsumerDocument;

public interface SsapApplicantDocumentService {
	ConsumerDocument saveSsapApplicantDocument(ConsumerDocument consumerDocument) throws   SsapDocumentsServiceException;
	String handleException(Throwable exception);
}
