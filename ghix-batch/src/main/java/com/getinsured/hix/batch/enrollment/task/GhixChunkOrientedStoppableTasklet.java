package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.item.*;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.batch.repeat.support.RepeatTemplate;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by song_s on 8/30/17.
 */

@Scope("step")
public class GhixChunkOrientedStoppableTasklet<T> implements ChunkListener {
    private static final Logger logger = LoggerFactory.getLogger(GhixChunkOrientedStoppableTasklet.class);

    private static final RepeatTemplate repeatTemplate = new RepeatTemplate();

    static {
        // It's only for testing, and we don't want any infinite loops...
        repeatTemplate.setCompletionPolicy(new SimpleCompletionPolicy(6));
    }

    private static volatile long started = 0;
    private StepExecution stepExecution;


    public void resetStepExecution(StepExecution stepExecution) {
        if (this.stepExecution == null || !this.stepExecution.getId().equals(stepExecution.getId())) {
            logger.warn("Step Id mismatch: expected: " + stepExecution.getId() + ", actual: " + this.stepExecution == null ? " null" : this.stepExecution.getId().toString());
        }
        else {
            this.stepExecution = null;
        }
    }

    @Override
    public void beforeChunk(ChunkContext context) {
        StepExecution stepExecution = context.getStepContext().getStepExecution();
        if (this.stepExecution != null) {
            logger.warn("Unexpected step execution: " + this.stepExecution.getId());
        }
        this.stepExecution = stepExecution;
        started = System.currentTimeMillis();
        logger.info("Set started: " + started);
    }

    @Override
    public void afterChunk(ChunkContext context) {
        StepExecution stepExecution = context.getStepContext().getStepExecution();
        resetStepExecution(stepExecution);
    }

    @Override
    public void afterChunkError(ChunkContext context) {
        StepExecution stepExecution = context.getStepContext().getStepExecution();
        resetStepExecution(stepExecution);
    }

    public static class Reader implements ItemReader<Long> {
        @Override
        public Long read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
            while (started == 0) {
                logger.info("wait for started is set");
                sleep(5*1000);
            }
            sleep(1 * 1000);
            long now = System.currentTimeMillis();
            if (now > started + (60 * 1000)) {
                logger.info("Read: finished. now=" + now + ", end=" + (started + (60*1000)));
                return null;
            }
            else {
                long value = now - started;
                logger.info("Read: " + value);
                return value;
            }
        }
    }

    public static class Processor implements ItemProcessor<Long,Long> {
        @Override
        public Long process(Long item) throws Exception {
            return item;
        }
    }

    public static class Writer implements ItemWriter<Long> {
        @Override
        public void write(List<? extends Long> items) throws Exception {
            sleep(2 * 1000);
            logger.info("Write: " + items.toString());
        }
    }

    public static class Partitioner implements org.springframework.batch.core.partition.support.Partitioner {
        public Partitioner() {}

        @Override
        public Map<String, ExecutionContext> partition(int gridSize) {
            Map<String,ExecutionContext> map = new HashMap<>();
            map.put("p1", new ExecutionContext());
            return map;
        }
    }

    private static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            //
        }
    }
}
