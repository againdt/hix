package com.getinsured.batch.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * JSON utility helper methods.
 *
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
public class JsonUtil
{
  private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);

  private static Gson gsonParser;
  private static ObjectMapper objectMapper;

  static {
    final GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
    gsonBuilder.serializeNulls();
    // gsonBuilder.setDateFormat(DhsDocument.DATE_FORMAT);
    gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT); // "MMM dd, yyyy hh:mm:ss a"
    gsonBuilder.serializeSpecialFloatingPointValues();
    gsonParser = gsonBuilder.create();

    objectMapper = new ObjectMapper();
  }

  public static SingleStreamlinedApplication parseApplicationDataJson(final SsapApplication ssapApplication) {
    if(ssapApplication.getApplicationData() != null && !ssapApplication.getApplicationData().equals("")) {
      LinkedHashMap m = gsonParser.fromJson(ssapApplication.getApplicationData(), LinkedHashMap.class);
      SingleStreamlinedApplication application = gsonParser.fromJson(gsonParser.toJson(m.get("singleStreamlinedApplication")), SingleStreamlinedApplication.class);
      return application;
    }

    return null;
  }

  /**
   * Reads given {@link ClassPathResource} as {@code String}.
   * @param resource {@link ClassPathResource} object.
   * @return contents of the given resource as {@code String}.
   */
  public static String readFile(final ClassPathResource resource) {
    try
    {
      return new String(Files.readAllBytes(Paths.get(resource.getURI())));
    }
    catch (IOException e)
    {
      log.error("Error occurred while reading: {} file: {}", resource.getPath(), e.getMessage());
    }

    return null;
  }

  public static SingleStreamlinedApplication getSingleStreamlinedApplicationFromJson(String json) {
    final Map<String, SingleStreamlinedApplication> data = gsonParser.fromJson(json, new TypeToken<Map<String, SingleStreamlinedApplication>>() {}.getType());
    return data.get("singleStreamlinedApplication");
  }

  /**
   * Sets given {@link SingleStreamlinedApplication} as {@code applicationData} in {@link SsapApplication}.
   * @param ssapApplication {@link SsapApplication} object
   * @param singleStreamlinedApplication {@link SingleStreamlinedApplication} object.
   * @return updated {@link SsapApplication} object.
   */
  public static SsapApplication setApplicationData(final SsapApplication ssapApplication, final SingleStreamlinedApplication singleStreamlinedApplication) {
    Map<String, SingleStreamlinedApplication> jsonMap = new HashMap<>();
    jsonMap.put("singleStreamlinedApplication", singleStreamlinedApplication);
    String appDataJson = gsonParser.toJson(jsonMap);

    ssapApplication.setApplicationData(appDataJson);
    return ssapApplication;
  }
}
