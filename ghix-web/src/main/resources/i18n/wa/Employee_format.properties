# Employee FAQ
#----------------------------------------------------------------------
label.employeeGroup1 = Manage your account
label.employeeGroup2 = Enrollment period
label.employeeGroup3 = Qualifying event
label.employeeGroup4 = Waive coverage
label.employeeGroup5 = Other issues


label.employeeHowGetIntoAccount = How do I get into my account?
label.employeeGetIntoAccount1 = Your employer will send you an email that includes a link to the Employee Portal, as well as a user name. Click on the link to open the Employee Portal in an Internet browser window. Then input your user name and follow a simple process to log in to your account.
label.employeeGetIntoAccount2 = After you log in, you should see a welcome greeting with your name in the upper left corner of the Dashboard screen.

label.employeeHowChangeAccount = How do I change my password?
label.employeeChangeAccount = To change your password, click the <strong>My Account</strong> tab on the top of the screen. Then click on the <strong>Account Settings</strong> tab in the dropdown. Click on the <strong>"Change Your Password"</strong> button then make the desired changes and click <strong>Save.</strong>

label.employeeWhatAfterLogin = What do I do after I log in?
label.employeeAfterLogin1 = After you log in to your account, you must complete four main steps to enroll in a healthcare plan. These steps include:
label.employeeAfterLogin1.action1 = Verifying your employee information
label.employeeAfterLogin1.action2 = Adding or updating information for your dependents
label.employeeAfterLogin1.action3 = Evaluating the available health care plans and making a selection
label.employeeAfterLogin1.action4 = Confirming your selection and enrolling in the plan
label.employeeAfterLogin2 = The <strong>Dashboard</strong> walks you through these four steps and indicates which steps have already been completed. Click the corresponding blue buttons to complete the steps.

label.employeeWhatEmrollmentPeriod = What is an open enrollment period?
label.employeeEmrollmentPeriod1 = Open enrollment is a designated period of time each year when employees can make changes to their health insurance coverage. An open enrollment period is 30 days. During this time you can evaluate the available health insurance plans and make your selections. You can also make changes to your selections up to the last day of the open enrollment period. And, if desired, you can also waive coverage.
label.employeeEmrollmentPeriod2 = On the <strong>Employee Portal</strong>, the dates of the open enrollment period are shown at the top of the <strong>Dashboard</strong> screen.

label.employeeCanChangeAfterEnrollment = Can I make changes to my health coverage after the open enrollment period is over?
label.employeeChangeAfterEnrollment = After the open enrollment period ends, you cannot make changes to your health coverage until the next open enrollment period unless you qualify for a special enrollment period due to a qualifying life event.

label.employeeWhatSpecialEnrollment = What is a special enrollment period?
label.employeeSpecialEnrollment = A special enrollment period is also available to current employees who experience changes in their lives such as marriage or divorce or the birth or adoption of a baby. These changes are referred to as qualifying life events. You can let your employer know if you experience a qualifying event by indicating the change on the <strong>Employee Special Enrollment</strong> screen.

label.employeeWhatQulifyingEven = What is a qualifying event?
label.employeeQulifyingEven = A qualifying event is an event that results in a life change for the employee, such as the birth of a child. These life changes can also make it necessary for an employee to make changes to their health coverage.
label.employeeQulifyingEvenList = Qualifying events include the following:
label.employeeQulifyingEvenList.item1 = Birth
label.employeeQulifyingEvenList.item2 = Adoption
label.employeeQulifyingEvenList.item3 = Marriage
label.employeeQulifyingEvenList.item4 = Divorce
label.employeeQulifyingEvenList.item5 = Lost eligibility for other coverage
label.employeeQulifyingEvenList.item6 = Gained eligibility for other coverage
label.employeeQulifyingEvenList.item7 = Death of a dependent
label.employeeQulifyingEvenList.item8 = Legal Separation
label.employeeQulifyingEvenDate = If you experience a qualifying event, you have 30 days to report the event and make the change to your enrollment.
label.employeeQulifyingEvenSpecial = Members of federally recognized tribes are eligible for special enrollment once every month.

label.employeeWhatWaiveCoverage = What does it mean to waive coverage?
label.employeeWaiveCoverage = If you choose to waive coverage during the open enrollment period, it means that you give up the opportunity to enroll in the health coverage provided by your employer for the upcoming year. You will not eligible for health coverage through your employer until the next open enrollment period.  

label.employeeWhatHappenWaiveCoverage = What happens if I waive coverage?
label.employeeWaiveCoverageHappen1 = You can choose to waive coverage during the open enrollment period by selecting the <strong>Waive Employer Coverage</strong> option. If you select this option, you will be asked to provide the reason why you want to give up employer-sponsored health coverage. You may be covered under another plan or you may be eligible for a government plan such as Medicaid.
label.employeeWaiveCoverageHappen2 = If you waive coverage, you will be asked to verify this choice and then provide your electronic signature. If you do not have any health coverage and you still choose to waive your employer coverage, you may be subject to a tax penalty.

label.employeeWhatEmployerContribution = What is an Employer Contribution?
label.employeeEmployerContribution = This is the amount your employer pays for a portion of your health insurance premium.. This contribution may or may not also apply to your dependent premiums.



label.employeeHowAddDependent = How do I add a dependent?
label.employeeAddDependent = You can add a dependent either during open enrollment or special enrollment by completing the <strong>Update Dependents</strong> step.


label.employeeCanDifferentPlans = Can I select different plans for different family members?
label.employeeDifferentPlans = The employee and all dependents must be enrolled in the same health insurance plan. Children can be enrolled in an additional plan for dental coverage. 

label.employeeWhereSeeSummary = Where can I see a summary of my selected plans?
label.employeeSeeSummary = There are two ways to see the details of your selected plans: 
label.employeeSeeSummary.place1 = By clicking the <strong>My Plans</strong> tab on the left-hand side of the screen.
label.employeeSeeSummary.place2 = By clicking the <strong>View Benefit</strong> Details button under <strong>Plan Summary</strong>.

label.employeeWhatIfNeedHelp = What if I need help completing the Open Enrollment process? 
label.employeeNeedHelp = Help is available by phone <strong><1-800-234-1234></strong> or online (by clicking the <strong>Chat</strong> button). You can also send an email to a Customer Support representative by clicking the <strong>Contact Customer Support</strong> button located under <strong>Quick Links</strong> on the left-hand side of the screen. 


label.employeeWhatESignature = What is an e-signature? 
label.employeeESignature = An e-signature is the electronic version of a handwritten signature and is a simple and legal way to give consent on electronic forms. When you enroll in the selected health care plan, you provide an e-signature by typing your name on the Enrollment screen.

label.employeeDentalForFamilyQ = How do I purchase dental coverage for my family?
label.employeeDentalForFamilyA = As part of the shopping experience on YHI, you are currently offered the opportunity to purchase dental coverage for your minor children only. Family dental coverage will be available for you to select in the near future. In the meantime, if you'd like to shop for dental coverage outside of YHI, click <a href="#" target="_blank">here</a> for a list of dental insurance carriers in Idaho. 

label.terminatedEmployee1 = Your account is not currently available. Please contact
label.terminatedEmployee2 =at
label.terminatedEmployee3 =if you need assistance.