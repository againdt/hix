
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * A data type for the TitleII monthly income.
 *             
 * 
 * <p>Java class for SSATitleIIMonthlyIncomeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSATitleIIMonthlyIncomeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonDisabledIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}OngoingMonthlyBenefitCreditedAmount" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}OngoingMonthlyOverpaymentDeductionAmount" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}OngoingPaymentInSuspenseIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestedMonthInformation" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestedMonthMinusOneInformation" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestedMonthMinusTwoInformation" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestedMonthMinusThreeInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSATitleIIMonthlyIncomeType", propOrder = {
    "personDisabledIndicator",
    "ongoingMonthlyBenefitCreditedAmount",
    "ongoingMonthlyOverpaymentDeductionAmount",
    "ongoingPaymentInSuspenseIndicator",
    "requestedMonthInformation",
    "requestedMonthMinusOneInformation",
    "requestedMonthMinusTwoInformation",
    "requestedMonthMinusThreeInformation"
})
public class SSATitleIIMonthlyIncomeType
    extends ComplexObjectType
{

    @XmlElement(name = "PersonDisabledIndicator", required = true)
    protected Boolean personDisabledIndicator;
    @XmlElement(name = "OngoingMonthlyBenefitCreditedAmount")
    protected BigDecimal ongoingMonthlyBenefitCreditedAmount;
    @XmlElement(name = "OngoingMonthlyOverpaymentDeductionAmount")
    protected BigDecimal ongoingMonthlyOverpaymentDeductionAmount;
    @XmlElement(name = "OngoingPaymentInSuspenseIndicator")
    protected Boolean ongoingPaymentInSuspenseIndicator;
    @XmlElement(name = "RequestedMonthInformation")
    protected SSATitleIIMonthlyInformationType requestedMonthInformation;
    @XmlElement(name = "RequestedMonthMinusOneInformation")
    protected SSATitleIIMonthlyInformationType requestedMonthMinusOneInformation;
    @XmlElement(name = "RequestedMonthMinusTwoInformation")
    protected SSATitleIIMonthlyInformationType requestedMonthMinusTwoInformation;
    @XmlElement(name = "RequestedMonthMinusThreeInformation")
    protected SSATitleIIMonthlyInformationType requestedMonthMinusThreeInformation;

    /**
     * Gets the value of the personDisabledIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonDisabledIndicator() {
        return personDisabledIndicator;
    }

    /**
     * Sets the value of the personDisabledIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonDisabledIndicator(Boolean value) {
        this.personDisabledIndicator = value;
    }

    /**
     * Gets the value of the ongoingMonthlyBenefitCreditedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOngoingMonthlyBenefitCreditedAmount() {
        return ongoingMonthlyBenefitCreditedAmount;
    }

    /**
     * Sets the value of the ongoingMonthlyBenefitCreditedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOngoingMonthlyBenefitCreditedAmount(BigDecimal value) {
        this.ongoingMonthlyBenefitCreditedAmount = value;
    }

    /**
     * Gets the value of the ongoingMonthlyOverpaymentDeductionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOngoingMonthlyOverpaymentDeductionAmount() {
        return ongoingMonthlyOverpaymentDeductionAmount;
    }

    /**
     * Sets the value of the ongoingMonthlyOverpaymentDeductionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOngoingMonthlyOverpaymentDeductionAmount(BigDecimal value) {
        this.ongoingMonthlyOverpaymentDeductionAmount = value;
    }

    /**
     * Gets the value of the ongoingPaymentInSuspenseIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOngoingPaymentInSuspenseIndicator() {
        return ongoingPaymentInSuspenseIndicator;
    }

    /**
     * Sets the value of the ongoingPaymentInSuspenseIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOngoingPaymentInSuspenseIndicator(Boolean value) {
        this.ongoingPaymentInSuspenseIndicator = value;
    }

    /**
     * Gets the value of the requestedMonthInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public SSATitleIIMonthlyInformationType getRequestedMonthInformation() {
        return requestedMonthInformation;
    }

    /**
     * Sets the value of the requestedMonthInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public void setRequestedMonthInformation(SSATitleIIMonthlyInformationType value) {
        this.requestedMonthInformation = value;
    }

    /**
     * Gets the value of the requestedMonthMinusOneInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public SSATitleIIMonthlyInformationType getRequestedMonthMinusOneInformation() {
        return requestedMonthMinusOneInformation;
    }

    /**
     * Sets the value of the requestedMonthMinusOneInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public void setRequestedMonthMinusOneInformation(SSATitleIIMonthlyInformationType value) {
        this.requestedMonthMinusOneInformation = value;
    }

    /**
     * Gets the value of the requestedMonthMinusTwoInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public SSATitleIIMonthlyInformationType getRequestedMonthMinusTwoInformation() {
        return requestedMonthMinusTwoInformation;
    }

    /**
     * Sets the value of the requestedMonthMinusTwoInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public void setRequestedMonthMinusTwoInformation(SSATitleIIMonthlyInformationType value) {
        this.requestedMonthMinusTwoInformation = value;
    }

    /**
     * Gets the value of the requestedMonthMinusThreeInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public SSATitleIIMonthlyInformationType getRequestedMonthMinusThreeInformation() {
        return requestedMonthMinusThreeInformation;
    }

    /**
     * Sets the value of the requestedMonthMinusThreeInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIMonthlyInformationType }
     *     
     */
    public void setRequestedMonthMinusThreeInformation(SSATitleIIMonthlyInformationType value) {
        this.requestedMonthMinusThreeInformation = value;
    }

}
