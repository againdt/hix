package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.I551DocumentID4Type;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for I551DocumentID4Type
 * 
 * @author Abhai Chaudhary, Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I551DocumentID4Wrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I551DocumentID4Wrapper.class);
	
	private ObjectFactory factory;
	private I551DocumentID4Type i551DocumentID4Type;
	
	private String alienNumber;
	private String receiptNumber;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String RECEIPT_NUMBER_KEY = "ReceiptNumber";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public I551DocumentID4Wrapper(){
		this.factory = new ObjectFactory();
		this.i551DocumentID4Type = factory.createI551DocumentID4Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.i551DocumentID4Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i551DocumentID4Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I551 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	public void setReceiptNumber(String receiptNumber) throws HubServiceException{
		
		if(receiptNumber == null){
			throw new HubServiceException("No receiptNumber provided");
		}
		
		this.receiptNumber = receiptNumber;
		this.i551DocumentID4Type.setReceiptNumber(receiptNumber);
	}
	
	public String getReceiptNumber() {
		return receiptNumber;
	}
	
	public I551DocumentID4Type geti551DocumentID4Type(){
		return this.i551DocumentID4Type;
	}
	
	/**
	 * Converts the JSON String to I551DocumentID4Wrapper object
	 * 
	 * @param jsonString - String representation for I551DocumentID4Wrapper
	 * @return I551DocumentID4Wrapper Object
	 */
	public static I551DocumentID4Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I551DocumentID4Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I551DocumentID4Wrapper wrapper = new I551DocumentID4Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.i551DocumentID4Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(RECEIPT_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.receiptNumber = (String)tmpObj;
			wrapper.i551DocumentID4Type.setReceiptNumber(wrapper.receiptNumber);
		}
		
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i551DocumentID4Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I551DocumentID4Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I551DocumentID4Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I551DocumentID4Wrapper wrapper = new I551DocumentID4Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setReceiptNumber((String) jsonObj.get(RECEIPT_NUMBER_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
