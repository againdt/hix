package com.getinsured.eligibility.verification.rulesets;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Component
public class ResidencyVerificationRule {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResidencyVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) throws Exception {
		ResidencyVerificationRule residencyVerificationRule = new ResidencyVerificationRule();
		residencyVerificationRule.verifyResidency(SSAPUtils.loadSSAPJSON());
	}
	
	/**
	 * This rule will verify Residency Status of Household Members
	 * 
	 * @param ssapJson
	 * @throws Exception 
	 */
	public boolean verifyResidency(String ssapJson) throws Exception {
		LOGGER.info("Verify Residency Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonArray householdMembers;
		JsonObject householdMember;
		String personId;
		boolean livesWithHouseholdContactIndicator;
		JsonObject householdContact;
		String state;
		boolean livesAtOtherAddressIndicator;
		JsonObject otherAddress;
		boolean livingOutsideofStateTemporarilyIndicator;
		List<String> taxFilerDependants = null;
		JsonObject singleStreamlinedApplication;
		String primaryTaxFilerPersonId;
		JsonObject taxFiler;
		long ssapApplicationId;
		
		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			ssapApplicationId = Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication));
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();

				try{
					livesWithHouseholdContactIndicator = householdMember.get(SSAPConstants.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).getAsBoolean();
					
					if (livesWithHouseholdContactIndicator) {
						householdContact = householdMember.get(SSAPConstants.HOUSEHOLD_CONTACT).getAsJsonObject();
						state = householdContact.get(SSAPConstants.HOME_ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
						
						if(SSAPUtils.getStateCode().equals(state)){
							status = SSAPConstants.VERIFIED;
						} else {
							state = householdContact.get(SSAPConstants.MAILING_ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
							
							if(SSAPUtils.getStateCode().equals(state)){
								status = SSAPConstants.VERIFIED;
							} else {
								primaryTaxFilerPersonId = singleStreamlinedApplication.get(SSAPConstants.PRIMARY_TAX_FILER_PERSON_ID).getAsString();
								
								if(!SSAPUtils.isEmpty(primaryTaxFilerPersonId) && !SSAPConstants.ZERO.equals(primaryTaxFilerPersonId)){
									taxFiler = SSAPUtils.getTaxFiler(householdMembers, primaryTaxFilerPersonId);
									
									if(checkTaxFilerVerificationStatus(ssapApplicationId, personId) || checkTaxFilerStateOfResidency(taxFiler)){
										taxFilerDependants = SSAPUtils.getListOfTaxFilerDependants(taxFiler, primaryTaxFilerPersonId);

										if(taxFilerDependants.contains(personId)){
											status = SSAPConstants.VERIFIED;
										} else {
											status = SSAPConstants.NOT_VERIFIED;
										}
									} else {
										status = SSAPConstants.NOT_VERIFIED;
									}
								} else {
									status = SSAPConstants.NOT_VERIFIED;
								}
							}
						}
					} else {
						livesAtOtherAddressIndicator = householdMember.get(SSAPConstants.LIVES_AT_OTHER_ADDRESS_INDICATOR).getAsBoolean();
						
						if(livesAtOtherAddressIndicator){
							otherAddress = householdMember.get(SSAPConstants.OTHER_ADDRESS).getAsJsonObject();
							state = otherAddress.get(SSAPConstants.ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
							
							if(SSAPUtils.getStateCode().equals(state)){
								status = SSAPConstants.VERIFIED;
							} else {
								livingOutsideofStateTemporarilyIndicator = otherAddress.get(SSAPConstants.LIVING_OUTSIDE_OF_STATE_TEMPORARILY_INDICATOR).getAsBoolean();
								
								if(livingOutsideofStateTemporarilyIndicator){
									status = SSAPConstants.VERIFIED;
								} else {
									primaryTaxFilerPersonId = singleStreamlinedApplication.get(SSAPConstants.PRIMARY_TAX_FILER_PERSON_ID).getAsString();
									
									if(!SSAPUtils.isEmpty(primaryTaxFilerPersonId) && !SSAPConstants.ZERO.equals(primaryTaxFilerPersonId)){
										taxFiler = SSAPUtils.getTaxFiler(householdMembers, primaryTaxFilerPersonId);
										
										if(checkTaxFilerVerificationStatus(ssapApplicationId, personId) || checkTaxFilerStateOfResidency(taxFiler)){
											taxFilerDependants = SSAPUtils.getListOfTaxFilerDependants(taxFiler, primaryTaxFilerPersonId);

											if(taxFilerDependants.contains(personId)){
												status = SSAPConstants.VERIFIED;
											} else {
												status = SSAPConstants.NOT_VERIFIED;
											}
										} else {
											status = SSAPConstants.NOT_VERIFIED;
										}
									} else {
										status = SSAPConstants.NOT_VERIFIED;
									}
								}
							}
						} else {
							status = SSAPConstants.NOT_VERIFIED;
						}
					}
					
					verificationStatus = verificationRulesService.updateVerificationStatus(ssapApplicationId, Long.valueOf(personId), status, SSAPConstants.RESIDENCY_VERIFICATION_RULE);
					verificationStatus = true;
				} catch(Exception exception){
					LOGGER.error("Exception occurred while Verifying Residency : Person : "+personId,exception);
					String rule = SSAPConstants.RULES.RESIDENCY_VERIFICATION_RULE.toString();
					giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
							null, null, null, null, rule), rule));
					throw exception;
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Residency : ", exception);
			throw exception;
		}
		
		LOGGER.info("Verify Residency Rule : Ends : Status : "+verificationStatus);
		
		return verificationStatus;
	}
	
	/**
	 * Checks the Tax Filer Verification Status in DB
	 * 
	 * @param taxFiler
	 * @return
	 */
	private boolean checkTaxFilerVerificationStatus(long ssapApplicationId, String personId) throws Exception{
		return verificationRulesService.checkVerificationStatus(ssapApplicationId, Long.valueOf(personId), SSAPConstants.RESIDENCY_VERIFICATION_RULE);
	}
	
	/**
	 * Checks if the Tax Filer lives in the state or not
	 * 
	 * @param taxFiler
	 * @return
	 */
	private boolean checkTaxFilerStateOfResidency(JsonObject taxFiler){
		boolean flag = false;
		JsonObject householdContact;
		String homeAddressState;
		String mailingAddressState;
		JsonObject otherAddress;
		boolean livingOutsideofStateTemporarilyIndicator;
		String otherAddressState;
		
		try {
			householdContact = taxFiler.get(SSAPConstants.HOUSEHOLD_CONTACT).getAsJsonObject();
			homeAddressState = householdContact.get(SSAPConstants.HOME_ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
			
			if(SSAPUtils.getStateCode().equals(homeAddressState)){
				flag = true;
			} else {
				mailingAddressState = householdContact.get(SSAPConstants.MAILING_ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
				
				if(SSAPUtils.getStateCode().equals(mailingAddressState)){
					flag = true;
				} else {
					otherAddress = taxFiler.get(SSAPConstants.OTHER_ADDRESS).getAsJsonObject();
					otherAddressState = otherAddress.get(SSAPConstants.ADDRESS).getAsJsonObject().get(SSAPConstants.STATE).getAsString();
					
					if(SSAPUtils.getStateCode().equals(otherAddressState)){
						flag = true;
					} else {
						livingOutsideofStateTemporarilyIndicator = otherAddress.get(SSAPConstants.LIVING_OUTSIDE_OF_STATE_TEMPORARILY_INDICATOR).getAsBoolean();
						if(livingOutsideofStateTemporarilyIndicator){
							flag = true;
						}
					}
				}
			}
		} catch(Exception exception){
			throw exception;
		}
		
		return flag;
	}
}
