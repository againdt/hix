<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
	<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />

<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>	
<!--[if IE]>
	<style type="text/css">
		form#commentForm + table.table {
			visibility: hidden;
		}
	</style>
<![endif]-->	

<script type="text/javascript">
$(function() {
	$(".newcommentiframe").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.indexOf('#') != 0) {
        	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
		}
	});
});

function closeCommentBox() {
	$("#newcommentiframe").remove();
	window.location.reload(true);
}
function closeIFrame() {
	$("#newcommentiframe").remove();
	window.location.reload(true);
}
</script>

<div class="gutter10-lr">
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
			
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p>
						<c:out value="${errorMsg}"></c:out>
					<p />
				</c:if>
				<br>
			</div>
		</div><!--  end of row-fluid -->
	</div><!--  end l-page-breadcrumb -->
	
	<div class="row-fluid">
		<h1><a name="skip"></a>
			<c:choose>
				<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
					${broker.user.firstName}&nbsp;${broker.user.lastName}
				</c:when>
				<c:otherwise>
					${broker.firstName}&nbsp;${broker.lastName}
				</c:otherwise>
			</c:choose>
		</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">						
				<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				<c:if test="${CA_STATE_CODE}">
					<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
				</c:if>
				<li class="active"><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
				<c:if test="${CA_STATE_CODE}">
					<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
				</c:if>	
				<c:if test="${!CA_STATE_CODE && broker.user != null}">
					<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
					<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	 --%>
				</c:if>
						
			</ul>
			<br>
			<c:if test="${!CA_STATE_CODE}">
				<div class="header">
					<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
				</div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">  
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">  
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
	            </ul>
            </c:if>   
		</div><!-- end of span3 -->
		
		<div id="rightpanel" class="span9">
		<div class="header">
			<h4 class="pull-left"><spring:message code="label.brkComments"/></h4>
		</div>
			<div class="gutter10">
			<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
				<table class="table">
					<tbody>
						<tr>
							<td>					
								<comment:view targetId="${broker.id}" targetName="BROKER">
								</comment:view>
								
								<jsp:include page="../../../views/platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="<encryptor:enc value="BROKER"/>" id="target_name" name="target_name" /> 
								<input type="hidden" value="<encryptor:enc value="${broker.id}"/>" id="target_id" name="target_id" />
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="brokerName" id="brokerName" value="${broker.user.firstName}&nbsp;${broker.user.lastName}" />
			</form>
			</div>
		</div>
	</div>	
	</div>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>
 
  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>