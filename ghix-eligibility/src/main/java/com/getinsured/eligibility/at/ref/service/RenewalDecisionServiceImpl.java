package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("renewalDecisionService")
@Scope("singleton")
public class RenewalDecisionServiceImpl implements RenewalDecisionService {
	private static final Logger LOGGER = Logger.getLogger(RenewalDecisionServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private FinancialApplicationConversionService financialApplicationConversionService;

	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;

	@Override
	@ReferralTransactionAnno
	public boolean execute(long applicationId, boolean cmrAutoLinking, boolean multipleCmr, long enrolledId, long comparedToId) {
		boolean blnComplete = true;
		try {
			LOGGER.info("RenewalDecisionServiceImpl for Referral application " + applicationId);

			final SsapApplication application = ssapApplicationRepository.findOne(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}

			if (enrolledId != ReferralConstants.NONE) {
				LOGGER.info("Found enrolled application in current year");
				handleEnrolledError(application);
			} else if (cmrAutoLinking) {
				handleLinkingCase(application, comparedToId);
			} else {
				LOGGER.info("No/Multiple Cmr found for application");
				blnComplete = handleOtherCase(application, multipleCmr);
			}
			LOGGER.info("RenewalDecisionServiceImpl for Referral application Ends");
		} catch (Exception e) {
			throw e;
		}
		return blnComplete;
	}

	private void handleEnrolledError(SsapApplication ssapApplication) {
		closeSsapApplication(ssapApplication);
		setRenewalStatus(ssapApplication, RenewalStatus.ERROR_REFERRAL_DUPLICATE);
		updateSsapApplication(ssapApplication);
	}

	private void handleLinkingCase(SsapApplication ssapApplication, long comparedToId) {
		final SsapApplication previousApplication = ssapApplicationRepository.findOne(comparedToId);
		if (previousApplication != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(previousApplication.getApplicationStatus())) {
			enrolledCase(ssapApplication, previousApplication);
		} else {
			eRCase(ssapApplication);
		}

	}
	/**
	 * Carry Applicant level info from previously enrolled app to current app
	 * 
	 * @param ssapApplication
	 * @param enrolledApplication
	 */
	@SuppressWarnings("unused")
	private void setApplicantLevelInfo(SsapApplication ssapApplication, SsapApplication enrolledApplication){
		
		List<SsapApplicant> enApplicants = ssapApplicantRepository.findBySsapApplication(enrolledApplication);
		
		List<SsapApplicant> currApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
		
		Map<String, SsapApplicant> enAppMap = new HashMap<>();
		for (SsapApplicant enApplicant : enApplicants) {
			enAppMap.put(enApplicant.getApplicantGuid(), enApplicant);
		}
		
		for (SsapApplicant currApplicant : currApplicants) {
			if (enAppMap.containsKey(currApplicant.getApplicantGuid())){
				SsapApplicant enApp = enAppMap.get(currApplicant.getApplicantGuid());
				if (enApp.getTobaccouser() != null){
					currApplicant.setTobaccouser(enApp.getTobaccouser());
				}
			}
		}
		
		ssapApplicantRepository.save(currApplicants);
	}

	private void enrolledCase(SsapApplication ssapApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Found enrolled application in previous year");
		
		/***
		 * For prev en app, 
		 * 		If denied convert the case to NF
		 * 		else mark as STARTED
		 */
		
		final boolean isDenied = isApplicationDenied(ssapApplication);
		if (isDenied) {
			LOGGER.info("Application - " + ssapApplication.getId() + " Denied");
			convertApplicationToNonFinancial(ssapApplication, enrolledApplication);
		} else {
			//setRenewalStatus(ssapApplication, RenewalStatus.STARTED);
			ssapApplication.setAllowEnrollment(ReferralConstants.Y);
			ssapApplication.setParentSsapApplicationId(BigDecimal.valueOf(enrolledApplication.getId())); // HIX-74238
			updateSsapApplication(ssapApplication);
		}
	}

	private void convertApplicationToNonFinancial(SsapApplication ssapApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Convert Application - " + ssapApplication.getId() + " to non financial application");
		financialApplicationConversionService.convertFinancialAppToNonFinancial(ssapApplication, enrolledApplication);
		if (checkIfEntireHouseholdElgLost(ssapApplication)) {
			setRenewalStatus(ssapApplication, RenewalStatus.NOT_TO_CONSIDER);
			closeSsapApplication(ssapApplication);
		} else {
			ssapApplication.setParentSsapApplicationId(BigDecimal.valueOf(enrolledApplication.getId())); // HIX-74238
			//setRenewalStatus(ssapApplication, RenewalStatus.STARTED);
			erSsapApplication(ssapApplication);
			ssapApplication.setAllowEnrollment(ReferralConstants.Y);
		}
		updateSsapApplication(ssapApplication);
	}

	/**
	 * Not needed, as this would taken care by renewal batches
	 * 
	 */
	/*private boolean checkConfirmedEnrollment(SsapApplication enrolledApplication) {
		boolean isConfirmedEnrollment = false;
		try {
			isConfirmedEnrollment = activeEnrollmentService.isConfirmedEnrollment(enrolledApplication.getId(), ReferralConstants.EXADMIN_USERNAME);
		} catch (GIException e) {
			throw new GIRuntimeException("Error while checking termination status for Application ID " + enrolledApplication.getId());
		}
		return isConfirmedEnrollment;
	}*/

	private void eRCase(SsapApplication ssapApplication) {
		LOGGER.info("Found application in previous year");
		final boolean isDenied = isApplicationDenied(ssapApplication);
		if (isDenied) {
			LOGGER.info("Application - " + ssapApplication.getId() + " Denied");
			closeSsapApplication(ssapApplication);
			setRenewalStatus(ssapApplication, RenewalStatus.NOT_TO_CONSIDER);
		} else {
			ssapApplication.setAllowEnrollment(ReferralConstants.Y);
			setRenewalStatus(ssapApplication, RenewalStatus.NOT_TO_CONSIDER); // As we do not have enrollment so simply mark as do not consider for renewal
		}
		updateSsapApplication(ssapApplication);
	}

	private boolean handleOtherCase(SsapApplication ssapApplication, boolean multipleCmr) {
		boolean blnComplete = true;
		final boolean isDenied = isApplicationDenied(ssapApplication);
		if (isDenied) {
			LOGGER.info("Application - " + ssapApplication.getId() + " Denied");
			closeSsapApplication(ssapApplication);
			setRenewalStatus(ssapApplication, RenewalStatus.NOT_TO_CONSIDER); // orphan AT..
			updateSsapApplication(ssapApplication);
		} else if (multipleCmr) {
			ssapApplication.setAllowEnrollment(ReferralConstants.Y);
			setRenewalStatus(ssapApplication, RenewalStatus.SEND_ACTIVATION_LINK);
			updateSsapApplication(ssapApplication);
		} else {
			blnComplete = false;
		}
		return blnComplete;
	}

	private boolean isApplicationDenied(SsapApplication application) {
		return EligibilityStatus.DE.equals(application.getEligibilityStatus());
	}

	private void setRenewalStatus(SsapApplication ssapApplication, RenewalStatus status) {
		LOGGER.info("Update Renewal Status Application id " + ssapApplication.getId() + " : status -" + status);
		ssapApplication.setRenewalStatus(status);
	}

	private void closeSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("Close Application id " + ssapApplication.getId());
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
	}

	private void erSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("ER Application id " + ssapApplication.getId());
		ssapApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
	}

	private void updateSsapApplication(SsapApplication ssapApplication) {
		ssapApplicationRepository.save(ssapApplication);
	}

	private boolean checkIfEntireHouseholdElgLost(SsapApplication currentApplication) {
		boolean blnFlag = true;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, ssapApplicant.getEligibilityStatus())) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}
}
