<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="viewindModal" class="modal hide fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">&times;</button>
		<h3 id="myModalLabel">View Individual Account</h3>
	</div>
	<div class="modal-body">
		<p>Clicking &#34;Individual View&#34; will take you to the
			Individual&#39;s portal for ${individualUser.firstName}
			${individualUser.lastName}. Through this portal you will be able to
			take actions on behalf of this individual, such as view plans, fill
			out individual eligibility, etc.</p>
		<p>Proceed to Individual view?</p>
	</div>

	<div class="modal-footer">
		<label class="checkbox span5 pull-left"> <input
			type="checkbox" name="indchkshowpopup"
			onclick="window.parent.setPopupValue();" /> Don&#39;t show this
			message again. <input type="hidden" name="showPopupInFuture" value="" />
		</label>
		<button class="btn" data-dismiss="modal">Cancel</button>
		<c:choose>
			<c:when test="${isCACall == true}">
				<a class="btn btn-primary" type="button"
					href="<c:url value="${switchAccUrl}&_pageLabel=individualHomePage&ahbxId=${individualUser.id}&ahbxUserType=1&recordId=${brokerId}&recordType=2&newAppInd=N"/>">Individual View</a>
			</c:when>
			<c:otherwise>
				<a class="btn btn-primary" type="button"
					href="<c:url value="/account/user/switchUserRole/individual/${individualUser.id}"/>">Individual View</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>