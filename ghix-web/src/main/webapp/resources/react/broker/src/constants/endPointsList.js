const PARTICIPATION_DETAILS_ENDPOINT = '/hix/broker/participationDetails';
const AVAILABILITY_ENDPOINT = '/hix/broker/available';
const CONNECT_HOURS_ENDPOINT = '/hix/broker/hours';

export default {
  PARTICIPATION_DETAILS_ENDPOINT,
  AVAILABILITY_ENDPOINT,
  CONNECT_HOURS_ENDPOINT
};
