
package com.getinsured.hix.batch.ssap.integration.task;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

public class VerifiedNonFinancialAppNotificationAppTask implements Tasklet {
    
    private static final String VERIFIED = "VERIFIED";
    
    private static final String APPLICATION_ID  = "Application ID";
	private static final String APPLICATION_TYPE  = "Application Type";
	private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";
	private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
    private static final String APPLICATION_ELIGIBILITY_FULL = "FULL_ELIGIBILITY_RECEIVED";

	private static final Logger LOGGER = Logger.getLogger(VerifiedNonFinancialAppNotificationAppTask.class);
    
    private SsapApplicationRepository ssapApplicationRepository;
    
    private RestTemplate restTemplate;

    private int maxAllowedDaysForVerification;
    
    private SsapJsonBuilder ssapJsonBuilder;
    
    private AppEventService appEventService;
    private LookupService lookupService;

    private final List<String> openApplicationStatuses = Arrays.asList(
            ApplicationStatus.SIGNED.getApplicationStatusCode(),
            ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
            ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
    
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        LOGGER.info("VerifiedNonFinancialAppNotificationAppTask started on " + new Timestamp(System.currentTimeMillis()));
        DateTime expiryDateForCompletingVerifications = DateTime.now().minusDays(maxAllowedDaysForVerification);
        try {
            List<SsapApplication> unverifiedApplications =
                    ssapApplicationRepository.getNonFinancialAppsWithExchangeEligStatusAsCAEAndEsignDateGreaterThan(
                            openApplicationStatuses, expiryDateForCompletingVerifications.toDate());
            for(SsapApplication application : unverifiedApplications) {
            	try {
            		boolean householdVerified = true;
            		for(SsapApplicant applicant : application.getSsapApplicants()) {
            			if(StringUtils.isNotBlank(applicant.getApplyingForCoverage()) && "Y".equals(applicant.getApplyingForCoverage())) {
            				boolean incarcerationStatus = StringUtils.isNotBlank(applicant.getIncarcerationStatus()) && VERIFIED.equals(applicant.getIncarcerationStatus());
            				boolean citizenshipStatus = StringUtils.isNotBlank(applicant.getCitizenshipImmigrationStatus()) && VERIFIED.equals(applicant.getCitizenshipImmigrationStatus());
            				boolean vlpVerificationStatus = StringUtils.isNotBlank(applicant.getVlpVerificationStatus()) && VERIFIED.equals(applicant.getVlpVerificationStatus());
            				boolean residencyVerificationStatus = StringUtils.isNotBlank(applicant.getResidencyStatus()) && VERIFIED.equals(applicant.getResidencyStatus());
            				
            				if(!checkIncarcerationStatus(application.getApplicationData(), applicant.getPersonId(), incarcerationStatus) || 
            						!(vlpVerificationStatus || citizenshipStatus) || !residencyVerificationStatus) {
            					householdVerified = false;
            					break;
            				}
            			}
            		}
            		if(householdVerified && EligibilityStatus.CAE.equals(application.getEligibilityStatus())) {
            			updateApplicationAndSendNotification(application);
            		}
            	} catch(Exception e) {
            		 LOGGER.error("Error in VerifiedNonFinancialAppNotificationAppTask processing application id " + application.getId(), e);
            	}
            }
        } catch (Exception e) {
            LOGGER.error("Error in VerifiedNonFinancialAppNotificationAppTask", e);
        }
        LOGGER.info("VerifiedNonFinancialAppNotificationAppTask finished on " + new Timestamp(System.currentTimeMillis()));
        return RepeatStatus.FINISHED;
    }
    
    private boolean checkIncarcerationStatus(String ssapJsonString, long personId, boolean incarcerationStatus) {
    	boolean determinedIncarcerationStatus = false;
    	if(incarcerationStatus) {
    		SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapJsonString);
    		for (HouseholdMember member : ssapJSON.getTaxHousehold().get(0).getHouseholdMember()) {
				if(member.getPersonId() == personId) {
					determinedIncarcerationStatus = !member.getIncarcerationStatus().getIncarcerationStatusIndicator();
					break;
				}
			}
    	}
		return determinedIncarcerationStatus;
	}

	private void updateApplicationAndSendNotification(SsapApplication application) {
        application.setEligibilityStatus(EligibilityStatus.AE);
        application.setEligibilityReceivedDate(new Date());
        ssapApplicationRepository.save(application);
        logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_FULL, application);
        // Call Eligibility notification service to send notifications.
        try {
            restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_NOTIFICATION_UPDATED_URL + application.getCaseNumber(), String.class);
        } catch (Exception exception) {
            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
        }
        
    }
	
	private void logIndividualAppEvent(String eventName, String eventType,SsapApplication ssapApplication){
	       
		try {

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(APPLICATION_ID,String.valueOf(ssapApplication.getId()));
			mapEventParam.put(APPLICATION_TYPE,NON_FINANCIAL_APPLICATION);
			
			if(null !=ssapApplication.getCmrHouseoldId()){
				eventInfoDto.setModuleId(ssapApplication.getCmrHouseoldId().intValue());	
			}
			
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}

    public SsapApplicationRepository getSsapApplicationRepository() {
        return ssapApplicationRepository;
    }

    public void setSsapApplicationRepository(SsapApplicationRepository ssapApplicationRepository) {
        this.ssapApplicationRepository = ssapApplicationRepository;
    }
    
    public int getMaxAllowedDaysForVerification() {
        return maxAllowedDaysForVerification;
    }

    public void setMaxAllowedDaysForVerification(int maxAllowedDaysForVerification) {
        this.maxAllowedDaysForVerification = maxAllowedDaysForVerification;
    }
    
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	public AppEventService getAppEventService() {
		return appEventService;
	}

	public void setAppEventService(AppEventService appEventService) {
		this.appEventService = appEventService;
	}

	public LookupService getLookupService() {
		return lookupService;
	}

	public void setLookupService(LookupService lookupService) {
		this.lookupService = lookupService;
	}
    
}