package com.getinsured.hix.admin.util;

/**
 * HIX-82138
 * 
 * @author sahoo_s
 */
public class AnnouncementDTO {

	private int id;
	private String announcementName;
	private String announcementText;
	private String effectiveStartDate;
	private String effectiveEndDate;
	private String updatedDate;
	private String status = "Inactive";
	private String moduleName;
	
	public enum ModuleName{
		INDIVIDUAL_PORTAL("Individual Portal");
		
		private final String value;
		public String getValue(){
			return this.value;
		}

		private ModuleName(String value){
	        this.value = value;
	    }
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAnnouncementName() {
		return announcementName;
	}
	public void setAnnouncementName(String announcementName) {
		this.announcementName = announcementName;
	}
	public String getAnnouncementText() {
		return announcementText;
	}
	public void setAnnouncementText(String announcementText) {
		this.announcementText = announcementText;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
}
