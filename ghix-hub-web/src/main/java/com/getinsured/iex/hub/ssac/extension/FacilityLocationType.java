
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;
import com.getinsured.iex.hub.ssac.niem.usps_states._2_0.USStateCodeType;



/**
 * A data type for the Facility Location. 
 *             
 * 
 * <p>Java class for FacilityLocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FacilityLocationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}LocationStreet"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}LocationCityName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStateUSPostalServiceCode"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}LocationPostalCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacilityLocationType", propOrder = {
    "locationStreet",
    "locationCityName",
    "locationStateUSPostalServiceCode",
    "locationPostalCode"
})
public class FacilityLocationType
    extends ComplexObjectType
{

    @XmlElement(name = "LocationStreet", required = true)
    protected String locationStreet;
    @XmlElement(name = "LocationCityName", required = true)
    protected String locationCityName;
    @XmlElement(name = "LocationStateUSPostalServiceCode", namespace = "http://niem.gov/niem/niem-core/2.0", required = true, nillable = true)
    protected USStateCodeType locationStateUSPostalServiceCode;
    @XmlElement(name = "LocationPostalCode", required = true)
    protected String locationPostalCode;

    /**
     * Gets the value of the locationStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationStreet() {
        return locationStreet;
    }

    /**
     * Sets the value of the locationStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationStreet(String value) {
        this.locationStreet = value;
    }

    /**
     * Gets the value of the locationCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCityName() {
        return locationCityName;
    }

    /**
     * Sets the value of the locationCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCityName(String value) {
        this.locationCityName = value;
    }

    /**
     * Gets the value of the locationStateUSPostalServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link USStateCodeType }
     *     
     */
    public USStateCodeType getLocationStateUSPostalServiceCode() {
        return locationStateUSPostalServiceCode;
    }

    /**
     * Sets the value of the locationStateUSPostalServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link USStateCodeType }
     *     
     */
    public void setLocationStateUSPostalServiceCode(USStateCodeType value) {
        this.locationStateUSPostalServiceCode = value;
    }

    /**
     * Gets the value of the locationPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostalCode() {
        return locationPostalCode;
    }

    /**
     * Sets the value of the locationPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostalCode(String value) {
        this.locationPostalCode = value;
    }

}
