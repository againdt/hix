package com.getinsured.ssap.util;

/**
 * Contains all VLP endpoints that are using during SSAP VLP verifications.
 * <p>
 *   Some documentation can be found on following page:
 *   <a href="https://confluence.getinsured.com/display/IE/Verify+Lawful+Presence+37+%28VLP37%29+-+Step+1">VLP endpoint documentation</a>
 *   on confluence
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public enum VlpEndpoint
{
  /**
   * Initial verification.
   * <p>Step 1</p>
   */
  VERIFICATION("/invokeInitialVerifRequest"),

  /**
   * Re-verification.
   * <p>Step 1a</p>
   */
  REVERIFICATION("/invokeReverifyRequest"),

  /**
   * Re-submition with SEVISID.
   * <p>Step 1b</p>
   */
  RESUBMIT_WITH_SAVISID("/invokeResubmitRequest"),

  /**
   * Initiate additional verification
   * <p>Step 2</p>
   */
  SECOND_VERIFICATION("/invokeInitiateAddVerifRequest"),

  /**
   * Initiate third verification.
   * <p>Step 3</p>
   */
  THIRD_VERIFICATION("/invokeInitiateThirdVerifRequest"),

  /**
   * Case status. Add {@code caseNumber} after last /.
   */
  CASE_STATUS("/case/status/"),

  /**
   * Close case endpoint.
   */
  CLOSE_CASE("/invokeCloseCaseRequestV37"),

  /**
   * Endpoint to retrieve step 2 case resolution.
   */
  STEP_TWO_CASE_RESOLUTION("/invokeRetrieveStep2CaseResolution"),

  /**
   * Endpoint to retrieve step 3 case resolution. (GI not doing step 3)
   */
  STEP_THREE_CASE_RESOLUTION("/invokeRetrieveStep3CaseResolution");

  private String path;

  /**
   * Constructs new ENUM with given URL path.
   * @param path URL path.
   */
  VlpEndpoint(String path) {
    this.path = path;
  }

  /**
   * Returns URL path assigned to this endpoint.
   * @return URL path, after domain/server name. Starting with {@code /}
   */
  public String url() {
    return this.path;
  }
}
