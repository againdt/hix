package com.getinsured.eligibility.rrv.service.impl;

import com.getinsured.eligibility.rrv.service.SingleStreamlinedApplicationService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Service("singleStreamlinedApplicationService")
@SuppressWarnings("Duplicates")
public class SingleStreamlinedApplicationServiceImpl implements SingleStreamlinedApplicationService {

    private static final Logger logger = LoggerFactory.getLogger(SingleStreamlinedApplicationServiceImpl.class);

    private final UserService userService;
    private final GhixRestTemplate ghixRestTemplate;
    private final SsapApplicationRepository ssapApplicationRepository;

    @Autowired
    SingleStreamlinedApplicationServiceImpl(UserService userService, GhixRestTemplate ghixRestTemplate, SsapApplicationRepository ssapApplicationRepository){
        this.userService = userService;
        this.ghixRestTemplate = ghixRestTemplate;
        this.ssapApplicationRepository = ssapApplicationRepository;
    }

    private static Gson gsonParser;

    static {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
        gsonBuilder.serializeNulls();
        gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT); // "MMM dd, yyyy hh:mm:ss a"
        gsonBuilder.serializeSpecialFloatingPointValues();
        gsonParser = gsonBuilder.create();
    }

    @Override
    public SingleStreamlinedApplication getSingleStreamlinedApplication(long applicationId) {
        SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);

        SingleStreamlinedApplication application;

        application = parseApplicationDataJson(ssapApplication);

        logger.info("Returning SSAP by id: {}", applicationId);

        return application;
    }

    @Override
    public SingleStreamlinedApplication getSingleStreamlinedApplication(SsapApplication ssapApplication) {
        logger.info("Returning SSAP by id: {}", ssapApplication.getId());
        return parseApplicationDataJson(ssapApplication);
    }

    @Override
    public void saveSingleStreamlinedApplication(SsapApplication ssapApplication, SingleStreamlinedApplication singleStreamlinedApplication){
        Map<String, SingleStreamlinedApplication> jsonMap = new HashMap<>();
        jsonMap.put("singleStreamlinedApplication", singleStreamlinedApplication);
        String appDataJson = gsonParser.toJson(jsonMap);

        ssapApplication.setApplicationData(appDataJson);

        ssapApplicationRepository.save(ssapApplication);
    }

    private static SingleStreamlinedApplication parseApplicationDataJson(final SsapApplication ssapApplication) {
        if (ssapApplication.getApplicationData() != null && !ssapApplication.getApplicationData().equals("")) {
            LinkedHashMap m = gsonParser.fromJson(ssapApplication.getApplicationData(), LinkedHashMap.class);

            return gsonParser.fromJson(gsonParser.toJson(m.get("singleStreamlinedApplication")), SingleStreamlinedApplication.class);
        }

        return null;
    }

}
