export const pageNumberAdjuster = pageNumber => {
    switch (typeof pageNumber) {
    case 'number':
        if (pageNumber < 0) {
            return 'page_00';
        }
        return `page_${pageNumber < 10 ? `0${pageNumber}` : pageNumber}`;
    default:
        return 'page_00';
    }
};