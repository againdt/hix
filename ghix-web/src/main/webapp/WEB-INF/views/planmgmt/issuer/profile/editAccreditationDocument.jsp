<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
		<div class="row-fluid">
		    <div class="gutter10">
		    	<ul class="page-breadcrumb">
		        		<li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>">&lt; <spring:message  code="label.back"/></a></li>
		            	<li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
		            	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
		            	<li><spring:message  code="pgheader.accreditationDocuments"/></li>
		        </ul><!--page-breadcrumb ends-->
				<%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
				<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
		    </div>
		</div>
		
		<div class="row-fluid">
        	<div class="gutter10">
                <div class="span3" id="sidebar">
                    <div class="header">
                        <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                    </div>
                    <!--  beginning of side bar -->
	                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="accreditation"/>
 	 	 	 	    </jsp:include>
                    <!-- end of side bar -->
                </div>
                <!-- end of span3 -->
                <div class="span9" id="rightpanel">
                    <div class="header">
                        <h4><spring:message  code="pgheader.accreditationDocuments"/></h4>
                    </div>
                    <div class="gutter10">
                        <p><spring:message  code="pgheader.uploadDocumentsInfo"/></p>
                
                    <form class="form-horizontal" id="frmIssuerApp" name="frmIssuerApp" action="<c:url value="/planmgmt/issuer/application/documents/save"/>" method="POST">
                    	<df:csrfToken/>
                    	<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
                    	<input type="hidden" name="issuerId" id="issuerId" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
                        <div class="header">
                            <h4><spring:message  code="label.issuerUploadAuthority"/></h4>
							   
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="certificate_auth"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="authority" name="authority"  class="input-file" multiple="multiple"/>
									&nbsp; <button type="submit" id="btn_upload_authority" name="btn_upload_authority" class="btn"><spring:message  code="label.upload"/></button>
									<div id="authority_display"></div>
									<input type="hidden" id="hdnAuthority" name="hdnAuthority" value=""/>
									<div id="authority_error" class=""></div>
								</div>
							</div><!-- end of control-group-->
							
							<div class="control-group">
								<c:forEach var="authFilesListObj" items="${authFilesList}" varStatus="indexvar">
										<div class="control-group clearfix">
											<label for="issuerName" class="control-label required">File<c:out value="${indexvar.index+1}"/></label>
											<div class="controls paddingT5 txt-bold">${authFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
										</div><!-- end of control-group --> 
									</c:forEach>
							</div>
						   </div>
							
							<div class="header">
								<h4><spring:message  code="label.issuerUploadAccreditation"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
							<label class="control-label" for="certificate_auth"><spring:message  code="label.issuerUploadAccreditation"/></label>
							<div class="pull-left marginL10">
								<select name="issuerAccreditation" id="issuerAccreditation">
								<c:choose>
								<c:when test="${selectedIssuerAccreditation == 'YES'}">
								 	<option value="yes" selected="selected"><spring:message  code="label.yes"/></option>
									<option value="no"><spring:message  code="label.no"/></option>
									<option value="excellent"><spring:message  code="label.excellent"/></option>
								</c:when>
								 <c:when test="${selectedIssuerAccreditation == 'NO'}">
								 	<option value="yes"><spring:message  code="label.yes"/></option>
									<option value="no" selected="selected"><spring:message  code="label.no"/></option>
									<option value="excellent"><spring:message  code="label.excellent"/></option>
								</c:when>
								 <c:when test="${selectedIssuerAccreditation == 'EXCELLENT'}">
								 	<option value="yes"><spring:message  code="label.yes"/></option>
									<option value="no"><spring:message  code="label.no"/></option>
									<option value="excellent" selected="selected"><spring:message  code="label.excellent"/></option>
								</c:when>
								<c:otherwise>
									 <option value="" selected="selected">Select</option>
								 	<option value="yes"><spring:message  code="label.yes"/></option>
									<option value="no"><spring:message  code="label.no"/></option>
									<option value="excellent"><spring:message  code="label.excellent"/></option>
								</c:otherwise>
								</c:choose>
									
								</select>
							</div>
						</div><!-- end of control-group-->
														
							<div class="control-group">
								<label class="control-label" for="accrediting-entity"><spring:message  code="label.accreditingEntity"/></label>
								<div class="controls">
									<input type="text" name="accreditingEntity" id="accreditingEntity" class="input-large" value = "${selectedAccreditionEntity}" />
									<!--<div id="accreditingEntity_error" class=""></div>-->
								</div>
							</div><!-- end of control-group-->
							
							<div class="control-group">
								<label class="control-label" for="accreditationExpDate"><spring:message  code="label.expirationDate"/></label>
								<div class="controls">
									<div class="input-append date date-picker" id="date" data-date="${systemDate}" data-date-format="mm/dd/yyyy">
										<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${selectedAccreditationExpDate}" var="expDate" />
										<input type="text" pattern="MM/dd/yyyy" class="span10" name="accreditationExpDate" id="accreditationExpDate" value="${expDate}">
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
									<div id="accreditationExpDate_error" class=""></div>
								</div>
							</div><!-- end of control-group-->
							
							<div class="control-group">
								<label class="control-label" for="accrediting"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="accrediting" name="accrediting"  class="input-file" value="${accreditingForFilePath}" multiple="multiple"/>
									&nbsp; <button id="btn_upload_accrediting" name="btn_upload_accrediting" class="btn"><spring:message  code="label.upload"/></button>
									<div id="accrediting_error" class=""></div>
									<div id="accrediting_display" class=""></div>
									<input type="hidden" id="hdnAccrediting" name="hdnAccrediting" value=""/>
									<input type="hidden" id="firstEdit" name="firstEdit" value="${isFirstEdit }"/>
							 
								</div>
								<c:forEach var="accreditationFilesListObj" items="${accreditationFilesList}" varStatus="indexvar">
								<div class="control-group clearfix">
									<label for="issuerName" class="control-label required"> <spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
									<div class="controls paddingT5 txt-bold">${accreditationFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
								</div><!-- end of control-group --> 
								</c:forEach>
							</div><!-- end of control-group-->
							</div>
							<div class="header">
								<h4><spring:message  code="label.organizationData"/></h4>
							</div>
							<div class="gutter10-t">
							<%-- 
								hiding download template button
								<div class="control-group">
								<label class="control-label"><spring:message  code="label.templateText"/></label>
								<div class="pull-left marginL20">
									<a href="<c:url value="/resources/sampleTemplates/organization_data.csv"/>" class="btn"><spring:message  code="label.download"/></a>
								</div>
							</div><!-- end of control-group--> --%>
							
							<div class="control-group">
								<label class="control-label" for="org-data"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="orgData" name="orgData"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_orgData" name="btn_upload_orgData" class="btn"><spring:message  code="label.upload"/></button>
									<div id="orgData_error" class=""></div>
									<div id="orgData_display"></div>							
									<input type="hidden" id="hdnOrgData" name="hdnOrgData" value=""/>
								</div>
								
							</div><!-- end of control-group-->
							<div class="controls-group">
								<c:forEach var="organizationFilesListObj" items="${organizationFilesList}" varStatus="indexvar">
										<div class="control-group clearfix">
											<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
											<div class="controls paddingT5 txt-bold">${organizationFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
										</div><!-- end of control-group --> 
									</c:forEach>
								</div>
							</div>
							<div class="header">
								<h4><spring:message  code="label.issuerUploadMarketingReq"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="marketing-org"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="marketing" name="marketing"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_marketing" name="btn_upload_marketing" class="btn"><spring:message  code="label.upload"/></button>
									<div id="marketing_display"></div>
									<div id="marketing_error" class=""></div>
									<input type="hidden" id="hdnMarketing" name="hdnMarketing" value=""/>
								</div>
							</div><!-- end of control-group-->
							 <div class="control-group">
								<c:forEach var="marketingFilesListObj" items="${marketingFilesList}" varStatus="indexvar">
										<div class="control-group clearfix">
											<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
											<div class="controls paddingT5 txt-bold">${marketingFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
										</div><!-- end of control-group --> 
									</c:forEach>
							</div>
							</div>
							
							<div class="header">
								<h4><spring:message  code="label.financialDisclosures"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="financial-disclosures"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="finDisclosures" name="finDisclosures"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_finDisclosures" name="btn_upload_finDisclosures" class="btn"><spring:message  code="label.upload"/></button>
									<div id="finDisclosures_error" class=""></div>
									<div id="finDisclosures_display"></div>
									<input type="hidden" id="hdnFinDisclosures" name="hdnFinDisclosures" value=""/>
								</div>                            
							</div><!-- end of control-group-->
							<div class="control-group">
								<c:forEach var="disclosureFilesListObj" items="${disclosureFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${disclosureFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>
							
							<div class="header">
								<h4><spring:message  code="label.claimsPaymentPolPract"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="Claims-Payment-Policies-And-Practices"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="payPolPractices" name="payPolPractices"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_payPolPractices" name="btn_upload_payPolPractices" class="btn"><spring:message  code="label.upload"/></button>
									<div id="payPolPractices_error" class=""></div>
									<div id="payPolPractices_display"></div>
									<input type="hidden" id="hdnPayment_policies_practices" name="hdnPayment_policies_practices" value=""/>
							 
								</div>
							</div><!-- end of control-group-->
							<div class="control-group">
								<c:forEach var="payPolPractFilesListObj" items="${payPolPractFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${payPolPractFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>
							</div>
							<div class="header">
								<h4><spring:message  code="label.enrollmentAndDisenroll"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="Enrollment-And-Disenrollment-Data"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="enrollmentAndDis" name="enrollmentAndDis"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_enrollmentAndDis" name="btn_upload_enrollmentAndDis" class="btn"><spring:message  code="label.upload"/></button>
									<div id="enrollmentAndDis_error" class=""></div>
									<div id="enrollmentAndDis_display"></div>
									<input type="hidden" id="hdnEnrollment_Disenrollment" name="hdnEnrollment_Disenrollment" value=""/>
							 
								</div>
							</div><!-- end of control-group-->
							<div class="control-group">
								<c:forEach var="enrollDisDataFilesListObj" items="${enrollDisDataFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${enrollDisDataFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>
							</div>
							<div class="header">
								<h4><spring:message  code="label.ratingPractices"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="Rating-Practices"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="ratingPractices" name="ratingPractices"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_ratingPractices" name="btn_upload_ratingPractices" class="btn"><spring:message  code="label.upload"/></button>
									<div id="ratingPractices_error" class=""></div>
									<div id="ratingPractices_display"></div>
									<input type="hidden" id="hdnRating_practices" name="hdnRating_practices" value=""/>
							 
								</div>
							</div><!-- end of control-group-->
							<div class="control-group">
								<c:forEach var="ratingPractFilesListObj" items="${ratingPractFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${ratingPractFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>
							</div>
							 <div class="header">
								<h4><spring:message  code="label.costSharAndPayments"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="Cost-Shar-And-Payment"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="costSharAndPayment" name="costSharAndPayment"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_costSharAndPayment" name="btn_upload_costSharAndPayment" class="btn"><spring:message  code="label.upload"/></button>
									<div id="costSharAndPayment_error" class=""></div>
									<div id="costSharAndPayment_display"></div>
									<input type="hidden" id="hdnCost_sharing_payments" name="hdnCost_sharing_payments" value=""/>
							 
								</div>
							</div><!-- end of control-group-->
							<div class="control-group">
								<c:forEach var="costSharPaymentFilesListObj" items="${costSharPaymentFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${costSharPaymentFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>						
							</div>
						
							<div class="header">
								<h4><spring:message  code="label.issuerUploadAdditionalInfo"/></h4>
							</div>
							<div class="gutter10-t">
							<div class="control-group">
								<label class="control-label" for="add-support-doc"><spring:message  code="label.uploadFile"/></label>
								<div class="controls">
									<input type="file" id="addSupportDoc" name="addSupportDoc"  class="input-file" multiple="multiple"/>
									&nbsp; <button id="btn_upload_addSupportDoc" name="btn_upload_addSupportDoc" class="btn"><spring:message  code="label.upload"/></button>
									<div id="addSupportDoc_error" class=""></div>
									<div id="addSupportDoc_display"></div>
									<input type="hidden" id="hdnAddSupportDoc" name="hdnAddSupportDoc" value=""/>
								</div>
							</div><!-- end of control-group--> 
						   <div class="control-group">
								<c:forEach var="addsuppFilesListObj" items="${addsuppFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${addsuppFilesListObj.uploadedFileName}&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="deleteAccDoc"><i class="icon-remove"></i></a></div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</div>
							</div>
							<input type="hidden" id="fileToUpload" name="fileToUpload" value=""/>
								 
							<div>
								<a href='<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/view"/>' class="btn pull-left"><spring:message  code="label.cancel"/></a>
								<button type="submit" class="btn btn-primary pull-right"><spring:message  code="label.save"/></button>
							</div>
							</div>
					</form>
                </div>
            </div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->
		<div class="modal hide confirmModal">
	<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>
	<div class="modal-body">
	<p>Are you sure you want to delete the document?</p>
	<button class="btn btn-primary deleteAccConfirm">Yes</button>
	<a href="javascript:void(0)" data-dismiss="modal" class="btn" style="float:none;">No</a>
	</div>
	</div>
<script type="text/javascript">
	$(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true,
				minDate : 0
			});
		});
	});
</script>
<script type="text/javascript">
var csrValue = $("#csrftoken").val();
$(document).ready(function() {
	$(".deleteAccDoc").click(function(){
		$(".confirmModal").modal();
		$(this).attr("id","deleteAccConfirmYes");
	});
	$('.date-picker').datepicker({
	    startDate: '+'+ '${systemDate}' + 'd',
	    autoclose: true,
	 	format: 'mm/dd/yyyy',
	 	forceParse: false
	});
	$(".deleteAccConfirm").click(function(){
		var ele = $("#deleteAccConfirmYes");
		var docIdString = $("#deleteAccConfirmYes").siblings('a').attr('href').split('=')[1];
		var docId = docIdString.split('&'); 
			$.ajax({ 
				 	type: "POST",
				 	url: "<c:url value='/issuer/application/documents/delete/${issuerObj.id}'/>",								   				  
					data: {
						"csrftoken" : csrValue,
						"documentId" : docId[0]
						},
				 	success: function(response){
						if(response == 'Success'){
							var ele1 = $(ele).parent('div');
							$(".confirmModal,.modal-backdrop").modal("hide");
							$(ele1).html("");
							$(ele1).append('Document deleted successfully!!!');
						} else{
							alert("Error deleting document");
							$(".confirmModal,.modal-backdrop").modal("hide");
						}					
				 	},
					error : function(responseText){
						alert("Error processing the request : " + responseText.statusText);
						$(".confirmModal,.modal-backdrop").modal("hide");
					}
				});
				if($(".deleteAccDoc").attr("id")){
				 $(".deleteAccDoc").removeAttr("id");			 
			 }
	});
	$("a[data-dismiss='modal'],button[data-dismiss='modal']").click(function(){
		if($(".deleteAccDoc").attr("id")){
			 $(".deleteAccDoc").removeAttr("id");			 
		 }
	});
	
	var button_id_names='#btn_upload_authority,'+
						'#btn_upload_accrediting,'+
						'#btn_upload_orgData,'+
						'#btn_upload_marketing,'+
						'#btn_upload_finDisclosures,'+
						'#btn_upload_payPolPractices,'+
						'#btn_upload_enrollmentAndDis,'+
						'#btn_upload_ratingPractices,'+
						'#btn_upload_costSharAndPayment,'+
						'#btn_upload_addSupportDoc';
						
		$(button_id_names).click(function(){
			$("#authority_error").html("");
			$("#accrediting_error").html("");
			$("#orgData_error").html("");
			$("#marketing_error").html("");
			$("#finDisclosures_error").html("");
			$("#payPolPractices_error").html("");
			$("#enrollmentAndDis_error").html("");
			$("#ratingPractices_error").html("");
			$("#costSharAndPayment_error").html("");
			$("#addSupportDoc_error").html("");
		var idName = this.id;
		var upload = idName.split("_");
		$('#fileToUpload').val(upload[2]);
		var v = $("#"+upload[2]).val();
		if(v == ""){
				$("#" + upload[2] + "_error").attr('class','error');
				$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.selectFile' javaScriptEscape='true'/></span></label>");
				return false;
		}
		else{
			var urlregex = /\.(exe|dll)$/ig;
			var condition = urlregex.test(v);
			if(condition){
				$("#" + upload[2] + "_error").attr('class','error');
				$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.inValidFile' javaScriptEscape='true'/></span></label>");
				return false;
			}
		}
		$('#frmIssuerApp').ajaxSubmit({
			url: "<c:url value='/planmgmt/issuer/application/documents/upload'/>",
			data: {"csrftoken":csrValue},
			success: function(responseText){
	        	 
				var mulValArray = responseText.split("^"); 
				var fileName = "";
				for(var i = 0; i < mulValArray.length; i++){
					var singleValArray = mulValArray[i];
					var val = singleValArray.split("|"); 
					
					if( val[0]=="SIZE_FAILURE"){							
						$("#" + upload[2] + "_error").attr('class','error');
						$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.fileSizeExceedsLimit' javaScriptEscape='true'/></span></label>");
		       		 }
					else if( val[0]=="EXT_FAILURE"){							
						$("#" + upload[2] + "_error").attr('class','error');
						$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.fileExtension' javaScriptEscape='true'/></span></label>");
		       		}
					else if(val[0]=='authority'){
		       			$("#authority").val("");
		       			 if(i==0){
		 		        	 $("#authority_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnAuthority").val(fileName);
		 	        	 }else{
		 	        		$("#authority_display").append('<br>');
		 	        		$("#authority_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnAuthority").val(fileName);
		 	        	 }
		        	 }
		        	 else if(val[0]=='accrediting'){
		        		 $("#accrediting").val("");
		        		 if(i==0){
		 		        	 $("#accrediting_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnAccrediting").val(fileName);
		 	        	 }else{
		 	        		$("#accrediting_display").append('<br>');
		 	        		$("#accrediting_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnAccrediting").val(fileName);
		 	        	 }
		        	 }
		        	 else if(val[0]=='orgData'){
		        		 $("#orgData").val("");
		        		 if(i==0){
		 		        	 $("#orgData_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnOrgData").val(fileName);
		 	        	 }else{
		 	        		$("#orgData_display").append('<br>');
		 	        		$("#orgData_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnOrgData").val(fileName);
		 	        	 }
		        	 }
		        	 if(val[0]=='marketing'){
		        		 $("#marketing").val("");
		        		 if(i==0){
		 		        	 $("#marketing_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnMarketing").val(fileName);
		 	        	 }else{
		 	        		$("#marketing_display").append('<br>');
		 	        		$("#marketing_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnMarketing").val(fileName);
		 	        	 }
						 
		        	 }
		        	 else if(val[0]=='finDisclosures'){
		        		 $("#finDisclosures").val("");
		        		 if(i==0){
		 		        	 $("#finDisclosures_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnFinDisclosures").val(fileName);
		 	        	 }else{
		 	        		$("#finDisclosures_display").append('<br>');
		 	        		$("#finDisclosures_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnFinDisclosures").val(fileName);
		 	        	 }
		        	 }
		        	 else if(val[0]=='addSupportDoc'){
		        		 $("#addSupportDoc").val("");
		        		 if(i==0){
		 		        	 $("#addSupportDoc_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnAddSupportDoc").val(fileName);
		 	        	 }else{
		 	        		$("#addSupportDoc_display").append('<br>');
		 	        		$("#addSupportDoc_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnAddSupportDoc").val(fileName);
		 	        	 }
		        	 
		        	 }else if(val[0]=='payPolPractices'){
		        		 $("#payPolPractices").val("");
		        		 if(i==0){
		 		        	 $("#payPolPractices_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnPayment_policies_practices").val(fileName);
		 	        	 }else{
		 	        		$("#payPolPractices_display").append('<br>');
		 	        		$("#payPolPractices_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnPayment_policies_practices").val(fileName);
		 	        	 }
		        	 }
					 else if(val[0]=='enrollmentAndDis'){
						 $("#enrollmentAndDis").val("");
						 if(i==0){
		 		        	 $("#enrollmentAndDis_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnEnrollment_Disenrollment").val(fileName);
		 	        	 }else{
		 	        		$("#enrollmentAndDis_display").append('<br>');
		 	        		$("#enrollmentAndDis_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnEnrollment_Disenrollment").val(fileName);
		 	        	 }
		        	 }
					 else if(val[0]=='ratingPractices'){
						 $("#ratingPractices").val("");
						 if(i==0){
		 		        	 $("#ratingPractices_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnRating_practices").val(fileName);
		 	        	 }else{
		 	        		$("#ratingPractices_display").append('<br>');
		 	        		$("#ratingPractices_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnRating_practices").val(fileName);
		 	        	 }
		        	 }
					 else if(val[0]=='costSharAndPayment'){
						 $("#costSharAndPayment").val("");
						 if(i==0){
		 		        	 $("#costSharAndPayment_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnCost_sharing_payments").val(fileName);
		 	        	 }else{
		 	        		$("#costSharAndPayment_display").append('<br>');
		 	        		$("#costSharAndPayment_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnCost_sharing_payments").val(fileName);
		 	        	 }
		        	 }
				}
			},
			error : function(responseText){ /* For Firefox*/
				var parsedJSON = responseText;
				var  temp= parsedJSON.responseText.split("^"); 
	       		var mulValArray = [];
	       		mulValArray = temp;
	       		var fileName = "";
				for(var i = 0; i < mulValArray.length; i++){
					var singleValArray = mulValArray[i];
					var val = singleValArray.split("|"); 
					
					if( val[0]=="SIZE_FAILURE"){							
						$("#" + upload[2] + "_error").attr('class','error');
						$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.fileSizeExceedsLimit' javaScriptEscape='true'/></span></label>");
		       		}else if( val[0]=="EXT_FAILURE"){							
						$("#" + upload[2] + "_error").attr('class','error');
						$("#" + upload[2] + "_error").html("<label class='error' generated='true'><span><em class='excl'>!</em><spring:message  code='err.fileExtension' javaScriptEscape='true'/></span></label>");
		       		}else if(val[0]=='authority'){
		       			$("#authority").val("");
		       			 if(i==0){
		 		        	 $("#authority_display").text(val[1]);
		 		        	fileName = val[2];
		 					 $("#hdnAuthority").val(fileName);
		 	        	 }else{
		 	        		$("#authority_display").append('<br>');
		 	        		$("#authority_display").append(val[1]);
		 	        		fileName = fileName + '|' + val[2];
		 	        		$("#hdnAuthority").val(fileName);
		 	        	 }
		 	        	 }
		 	        	 else if(val[0]=='accrediting'){
		 	        		$("#accrediting").val("");
		 	        		if(i==0){
			 		        	 $("#accrediting_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnAccrediting").val(fileName);
			 	        	 }else{
			 	        		$("#accrediting_display").append('<br>');
			 	        		$("#accrediting_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnAccrediting").val(fileName);
			 	        	 }
		 	        		
		 	        	 }
		 	        	 else if(val[0]=='orgData'){
		 	        		$("#orgData").val("");
		 	        		if(i==0){
			 		        	 $("#orgData_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnOrgData").val(fileName);
			 	        	 }else{
			 	        		$("#orgData_display").append('<br>');
			 	        		$("#orgData_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnOrgData").val(fileName);
			 	        	 }
		 	        		
		 	        	 }
		 	        	 if(val[0]=='marketing'){
		 	        		$("#marketing").val("");
		 	        		if(i==0){
			 		        	 $("#marketing_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnMarketing").val(fileName);
			 	        	 }else{
			 	        		$("#marketing_display").append('<br>');
			 	        		$("#marketing_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnMarketing").val(fileName);
			 	        	 }
		 	        	 }
		 	        	 else if(val[0]=='finDisclosures'){
		 	        		$("#finDisclosures").val("");
		 	        		if(i==0){
			 		        	 $("#finDisclosures_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnFinDisclosures").val(fileName);
			 	        	 }else{
			 	        		$("#finDisclosures_display").append('<br>');
			 	        		$("#finDisclosures_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnFinDisclosures").val(fileName);
			 	        	 }
		 	        	 }
		 	        	 else if(val[0]=='addSupportDoc'){
		 	        		$("#addSupportDoc").val("");
		 	        		if(i==0){
			 		        	 $("#addSupportDoc_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnAddSupportDoc").val(fileName);
			 	        	 }else{
			 	        		$("#addSupportDoc_display").append('<br>');
			 	        		$("#addSupportDoc_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnAddSupportDoc").val(fileName);
			 	        	 }
		 	        	 
		 	        	 }else if(val[0]=='payPolPractices'){
		 	        		$("#payPolPractices").val("");
		 	        		if(i==0){
			 		        	 $("#payPolPractices_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnPayment_policies_practices").val(fileName);
			 	        	 }else{
			 	        		$("#payPolPractices_display").append('<br>');
			 	        		$("#payPolPractices_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnPayment_policies_practices").val(fileName);
			 	        	 }
		 	        	 }
		 				 else if(val[0]=='enrollmentAndDis'){
		 					$("#enrollmentAndDis").val("");
		 					if(i==0){
			 		        	 $("#enrollmentAndDis_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnEnrollment_Disenrollment").val(fileName);
			 	        	 }else{
			 	        		$("#enrollmentAndDis_display").append('<br>');
			 	        		$("#enrollmentAndDis_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnEnrollment_Disenrollment").val(fileName);
			 	        	 }
		 	        	 }
		 				 else if(val[0]=='ratingPractices'){
		 					$("#ratingPractices").val("");
		 					if(i==0){
			 		        	 $("#ratingPractices_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnRating_practices").val(fileName);
			 	        	 }else{
			 	        		$("#ratingPractices_display").append('<br>');
			 	        		$("#ratingPractices_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnRating_practices").val(fileName);
			 	        	 }
		 	        	 }
		 				 else if(val[0]=='costSharAndPayment'){
		 					$("#costSharAndPayment").val("");
		 					if(i==0){
			 		        	 $("#costSharAndPayment_display").text(val[1]);
			 		        	fileName = val[2];
			 					 $("#hdnCost_sharing_payments").val(fileName);
			 	        	 }else{
			 	        		$("#costSharAndPayment_display").append('<br>');
			 	        		$("#costSharAndPayment_display").append(val[1]);
			 	        		fileName = fileName + '|' + val[2];
			 	        		$("#hdnCost_sharing_payments").val(fileName);
			 	        	 }
		 	        	 }
						
					}
		   		 }
		});
	  return false; 
	});	// button click close
	}); // ready method close
	
	$.validator.addMethod("greaterThanToday", function(value, element) {
		var startdatevalue = '${currDate}';
		if (value == null || value.length <1)
			return true;
		return Date.parse(startdatevalue) <= Date.parse(value);
		});
	
	var validator = $("#frmIssuerApp").validate({ 
		rules : {
			authority : {required : false},
			accrediting : { required : false},
			orgData : { required : false},
			marketing : { required : false},
			finDisclosures : { required : false},		
			addSupportDoc : {required : false},
			accreditationExpDate : {greaterThanToday : true}
		},
		messages : {
			authority : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAuthority' javaScriptEscape='true'/></span>"},
			accrediting : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAccreditation' javaScriptEscape='true'/></span>"},
			orgData : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadOrgData' javaScriptEscape='true'/></span>"},
			marketing: { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadMarketingReq' javaScriptEscape='true'/></span>"},
			finDisclosures : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadFinDisclosures' javaScriptEscape='true'/></span>"},		
			addSupportDoc : {required : "<span><em class='excl'>!</em><spring:message  code='err.validateAddSupportDoc' javaScriptEscape='true'/></span>"},
			accreditationExpDate : { greaterThanToday : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.validateAccreditationExpDate'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

</script>
