package com.getinsured.consumer.service;

import java.util.List;
import java.util.Map;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.hix.dto.consumer.ConsumerRequestDTO;
import com.getinsured.hix.dto.consumer.HouseholdRequestDTO;
import com.getinsured.hix.dto.consumer.MemberPortalRequestDTO;
import com.getinsured.hix.dto.consumer.MemberPortalResponseDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.ConsumerCallLog;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.FFMHouseholdResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.HouseholdEnrollment;
import com.getinsured.hix.model.consumer.Member;
import com.getinsured.hix.model.consumer.Prescription;

public interface ConsumerService {
	

	Member saveMember(Member member) ;
	
	Member findMemberById(int id);
	List<Member> findMembersByHousehold(Household household);
		
	Map<String, Object> searchApplicants(Map<String, Object> searchCriteria);
	Map<String,Object> searchHousehold(Map<String, Object> searchCriteria);
	Map<String,Object> searchEligLead(Map<String, Object> searchCriteria);
	
    Household saveHousehold(Household household) ;
	
	Household findHouseholdById(int id);
	Household findHouseholdByUserId(int userId);
	Household findHouseholdRecordByUserId(int userId);
	Household findHouseholdByEligLeadId(long eligLeadId);
	Household findHouseholdByGIHouseholdId(String giHouseholdId);
	MemberPortalResponseDTO saveHouseholdInformation(MemberPortalRequestDTO memberPortalRequestDTO) throws ConsumerServiceException;
	Prescription findPrescriptionById(long id);
	Prescription findPrescriptionByName(Prescription prescriptionName);
	ConsumerResponse findPrescriptionsByHousehold(Household household);
	ConsumerResponse findPrescriptionsByHouseholdId(int householdId);
	Prescription savePrescription(Prescription prescription) throws ConsumerServiceException;
	ConsumerResponse savePrescriptions(ConsumerResponse consumerResponse) throws ConsumerServiceException;
	void deletePrescription(Prescription prescription);
	void deletePrescriptionById(long id);
	Location saveLocation(Location location);
	HouseholdEnrollment saveHouseholdEnrollment(HouseholdEnrollment householdEnrollment);
	List<HouseholdEnrollment> findHouseholdEnrollmentByHouseholdId(Integer householdId);
	ConsumerCallLog saveCallLogs(ConsumerCallLog callLogs);
		
	//Further methods can be written for Household
	Household saveStateInHousehold(ConsumerRequestDTO consumerRequestDTO);	
	List<Household> getHouseholdList(HouseholdRequestDTO hhRequestDTO);
	
	Map<String,Object> getConsumerList(Map<String, Object> searchCriteria);

	Map<String, Object> getlinkappdata(Map<String, Object> searchCriteria);
	
	boolean checkIfEmailExists(String Email);
	boolean checkExactHouseholdExists(Household household);

	Map<String, Object> searchHouseholdUsingNativeQuery(
			Map<String, Object> searchCriteria);

	List<String> getPrimaryApplicantAccessCodes(int id);

	Household findHouseholdByCaseId(String id);
	
}
