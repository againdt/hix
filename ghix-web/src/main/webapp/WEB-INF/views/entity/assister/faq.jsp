<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#"> Back to FAQ Home</a></li>
                    <li>Information Management for Enrollment Counselors</li>
                </ul><!--page-breadcrumb ends-->
		<h1 class="gutter10"><a name="skip"></a>Information Management for Enrollment Entities</h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	
		<h4 class="margin0"></h4>
		
	             <form class="form-vertical grayBgTable" id="frmbrokerreg" name="frmbrokerreg" action="./Covered California_files/Covered California.html" method="POST">
							<ul class="nav nav-list" id="nav">
								<li><a href="#entity1">Understand the Role of the Enrollment Counselor</a></li>
								<li><a href="#entity2">Apply to Become a Certified Enrollment Counselor</a></li>
								<li><a href="#entity3">Manage Your Profile</a></li>
								<li><a href="#entity4">Manage Individual Accounts</a></li>
								
							</ul>
				 </form>
				 
			</div>
    	
       			<div class="span9 " id="rightpanel">
       				<div class="entityFaq1" id="entity1">
					<h4 class="rule" id="role-top">Understand the Role of the Enrollment Counselor
					
					<!-- <div class="pull-right">
					<button class="btn btn-small btn-back"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question <i class=" icon-chevron-right"></i></button> </div> -->
					
					
					</h4>
						<p>As an Enrollment Counselor, you can affiliate with an enrollment entity registered at the health insurance marketplace to be trained and certified by the marketplace to offer face-to-face, one-on-one consumer assistance. This will allow the enrollment entities to offer services to a wider range of population. To become a certified enrollment counselor, you must:</p>
						
						<ul>
							<li>Become affiliated with and submit an application to an enrollment entity</li>
							<li>Complete the fingerprinting process</li>
							<li>Complete required training</li>
							<li>Not have a conflict of interest with the enrollment entity or the health insurance marketplace</li>
							<li>Comply with all privacy and security standards set by the marketplace</li>
							
						</ul>
						<p>If you are a certified enrollment counselor affiliated with a registered enrollment entity, individuals searching the marketplace for assistance with health plan enrollment can designate you to assist them. You may choose to accept or decline the individual's request. Once you accept an individual's designation request, you may complete the health plan application process for the individual. The individual can then review and confirm the information.</p>

						</div>
						<!--end of .entityFaq1-->
						
						<div class="entityFaq2" id="entity2">
							<h4 class="rule" id="role-top">Apply to Become a Certified Enrollment Counselor 
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-entity2"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity2" href="#entity3">Next Question 2 <i class=" icon-chevron-right"></i></button> </div>
							 -->
							 
							 <a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							<p>To become a certified enrollment counselor, you must first be affiliated with an enrollment entity. After a successful background check, the enrollment entity will add you to the entity roster, either while applying for registration with the health insurance marketplace or after registration has been complete. Once the entity is registered with the marketplace and you are in the entity's roster, you can undergo training and be certified by the marketplace administrator. You will only be able to provide consumer assistance services if you are certified.</p>

							<p>When the enrollment entity has added you to the entity roster, you will receive a notification to create an account at the marketplace.</p>
							
						</div>
						<!--/.entityFaq2-->
						
						<div class="entityFaq3" id="entity3">
							<h4 class="rule" id="role-top">Manage Your Profile
					
							<!-- <div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							<a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>The enrollment entity that you are affiliated with manages your profile information at the health insurance marketplace. Your enrollment entity representative will create your profile at the marketplace when adding you to their roster. The profile information consists of contact information, certification status, primary and secondary work site information, languages handled, and educational level. You can view this information when logged in to your marketplace account. To make any changes to this information, you will need to contact your enrollment entity representative.</p>

							<p>To view your profile information:</p>
							
							<ol>
								<li>Log in to your health insurance marketplace account.</li>
								<li>On the Dashboard, under Quick Links, click My Profile. This opens the Enrollment Counselor Information page.</li>
								<li>To view your account information, public profile, or certification status, click the corresponding link on the left navigation pane.</li>
							</ol>
						</div>
						<!--/.entityFaq3-->
								
										
						<div class="entityFaq4" id="entity4">
							<h4 class="rule" id="role-top">Manage Individual Accounts


					<!-- 
							<div class="pull-right">
							<button class="btn btn-small btn-affiliated-entity"><i class=" icon-chevron-left"></i></button><button class="btn btn-small btn-entity1" href="#">Next Question 3<i class=" icon-chevron-right"></i></button> </div> -->
							
							<a id="role-top" href="#" class="pull-right">Back to Top</a>
							</h4>
							
							<p>You may perform the following actions to manage the accounts of individuals who have sought your assistance.</p>
							
							
							<div class="accordion" id="role-accordian2">
							  <div class="accordion-group">
							    <div class="accordion-heading">
							      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsOne">
							      View list of individuals seeking your assistance
							      </a>
							    </div>
							    <div id="counselorsOne" class="accordion-body collapse">
							      <div class="accordion-inner">
										<p>To view the list of individuals who have sought your assistance:</p>
										
										<ol>
											<li>Log in to your health insurance marketplace Assister account. The Enrollment Highlight chart on the Dashboard shows all of your enrollments over the past 30 days by tier level. These tiers are based on the average percentage of costs covered by the plans.</li>
											<li>To view the list of individuals who have newly enrolled and whose requests are pending your acceptance, click Designation Requests on the left navigation pane. You may access the same page any time by clicking the Individuals drop down tab menu and then Pending Requests.</li>
											<li>To view the list of individuals that are active in your account, from the Individuals drop down tab menu, select Active.</li>
											<li>To view the individuals that you have marked as inactive, from the Individuals drop down tab menu, select InActive.</li>
											
										</ol>
							      </div>
							    </div>
							  </div>
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsTwo">
								        Find an Individual
								      </a>
								    </div>
								    <div id="counselorsTwo" class="accordion-body collapse">
								      <div class="accordion-inner">
											<p>To find an individual who has designated you for assistance:</p>
											
											<ol>
											<li>Log in to your health insurance marketplace Agent account.</li>
											<li>Click the Individuals drop down tab menu.</li>
											<li>Select the status of the individual you want to find; Active, Inactive, or Pending Requests. This returns a corresponding list of individuals.</li>
											<li>To find a specific Active individual, you may use the Refine Results section.</li>
											</ol>
								      </div>
								    </div>
								  </div>
								  
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsThree">
								       Change Individual Status
								      </a>
								    </div>
								    <div id="counselorsThree" class="accordion-body collapse">
								      <div class="accordion-inner">
								       <p>An individual can have a status of Pending Requests, Active, or Inactive. An individual searching the health insurance marketplace for assistance, who designates you as his or her assister will by default have the status of Pending Requests. If you accept the individual's designation request, you can change the individual's status to Active. If you deny the request, the individual's status is changed to Inactive. You may also choose to mark an individual who has not been in business with you over a period of time as Inactive. An individual marked Inactive can choose another assister at the marketplace..</p>
								       
											<p>To change the status of an individual:</p>
											
												<ol>
													<li>Log in to your health insurance marketplace account.</li>
													<li>Find the individual whose status you want to change.
													<p>Note: You will not be able to access the account of and perform actions on behalf of the individual you have marked as Inactive. To be able to access an inactive individual's account again, the individual will have to send you another designation request. You will then have to select the individual from the list of Pending Requests and mark the individual as Active.</p>
													</li>
													<li>In the search results list, click the arrow beside the gear icon corresponding to the individual whose status you want to change and choose the valid option.
													To change the status of multiple individuals at the same time, select the individuals whose status you want to change using the checkboxes. Then click the Bulk Actions tab menu on the top of the list to select the valid action.
													
													<p>
													If the individual has a Pending Request status,</p>
													<ol>
														<li>Click the Contact Name link in the search results list. This opens a window showing the individual's contact information.</li>
														<li>Click Accept to accept the designation request. This changes the individual's status to Active.
														<p>To decline the request, click Decline.</p></li>
													</ol>
												
													</li>
													
												</ol>
								      </div>
								    </div>
								  </div>
								  
								 
								  <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFour">
								        View Individual Summary Information
								      </a>
								    </div>
								    <div id="counselorsFour" class="accordion-body collapse">
								      <div class="accordion-inner">
								      	
								      	<ol>
									        <li>Log in to your health insurance marketplace account.</li>
											<li>Find the individual whose information you want to view.</li>
											<li>In the search results list, click the Contact Name link of the individual whose information you want to view. You may also choose to click the arrow beside the gear icon corresponding to the individual and then click Details.
											<p>Note: You cannot view the summary information of an individual whose status is Inactive. The only information about an inactive individual you will have access to is the Contact Name, Phone Number, and Email Address.</p></li>
										</ol>
								      </div>
								    </div>
								  </div>
								   <div class="accordion-group">
								    <div class="accordion-heading">
								      <a class="accordion-toggle" data-toggle="collapse" data-parent="#role-accordian2" href="#counselorsFive">
								       Add, View, Update, or Delete Comments Related to an Individual
								      </a>
								    </div>
								    <div id="counselorsFive" class="accordion-body collapse">
								      <div class="accordion-inner">
								      <p>To maintain comments about an individual:</p>
								      <ol>
											<li>Log in to your health insurance marketplace account.</li>
											<li>Find the individual for whom you want to view, add, or edit comments.
											<p>Note: You cannot view, add, or edit comments for an individual whose status is Inactive.</p></li>

											<li>In the search results list, click the Contact Name link of the individual. You may also choose to click the arrow beside the gear icon corresponding to the individual and then click Details.
											<li>To add a comment:
												<ol>
													<li>Click New Comment on the left navigation pane. You may also click Comment and then Add Comment.</li>
													<li>Enter your comment.</li>
													<li>Click Save.</li>
												</ol>
											</li>
											<li>To view comments related to the individual, click Comments on the left navigation pane.</li>
											<li>To update a comment:
												<ol>
													<li>Click Comments on the left navigation pane.</li>
													<li>Click the edit icon corresponding to the comment you want to update.</li>
													<li>Make your changes.</li>
													<li>Click Update Comment.</li>
												</ol>
											</li>
											<li>To delete a comment:
												<ol>
													<li>Click Comments on the left navigation pane.</li>
													<li>Click the delete icon corresponding to the comment you want to delete.</li>
													<li>Click Delete to confirm your action.</li>
												</ol>
											</li>
											</ol>
								      </div>
								    </div>
								  </div>
								  
								</div>

						
						</div>
						<!--/.entityFaq4-->						
														
														
																						
						<a id="role-top" href="#" class="pull-right">Back to Top</a>
						
						
				</div>
				
				
				
    </div><!--  end of row-fluid -->
