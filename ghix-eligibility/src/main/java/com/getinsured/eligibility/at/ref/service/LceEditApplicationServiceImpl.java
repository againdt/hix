package com.getinsured.eligibility.at.ref.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.service.BaseDetermination;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TimeshiftContext;
import com.google.gson.Gson;

@Component("lceEditApplicationService")
public class LceEditApplicationServiceImpl implements LceEditApplicationService  {

	private static final Logger LOGGER = Logger.getLogger(LceEditApplicationServiceImpl.class);
	
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private ActiveEnrollmentService activeEnrollmentService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IUserRepository iUserRepository;
	@Autowired private QLEValidationService qleValidationService;
	@Autowired private ReferralCancelService referralCancelService;
	@Autowired private Gson platformGson;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	
	@Value("#{configProp['ghixEligibilityServiceURL']}")
	private String erpSvcUrl;
	
	@Autowired
	@Qualifier("mainDetermination")
	private BaseDetermination mainDetermination;
	
	private static final String PROCESS_LCE_APP = "/processLceAppReferral";

	@Override
	public void editApplicationProcess(Long applicationId) throws Exception{
		LOGGER.info("--------------LceEditApplicationProcess STARTS---------------");
/*		Step 1
		  - Create REST to take cloned case number
		  - Based on case number fetch application and get the previous application id
		  - If prev app is null, come out
		  - else fetch prev app and if prev app is CL then come out
		  - If prev app is not closed then 
		    - Check for active enrollment
		    - If active enrollments are not present then close prev app and come out
		    - if active enrollments are present then put enrollment app and current app and set process to be executed as LCE in account transfer dto and return it
		    
		Step 2
		  Call processLCEAppReferral integration flow*/
		
		
		// TODO Auto-generated method stub
		 try {
			 SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);
				
				
				if (ssapApplication != null) {
					String caseNumber = ssapApplication.getCaseNumber();
					LOGGER.info(ssapApplication.getId() + "is Enrolled Application found by " + caseNumber );
					
					if(ssapApplication.getParentSsapApplicationId() != null){
						LOGGER.info("ParentApplicationId id is " + ssapApplication.getParentSsapApplicationId());
						SsapApplication prevSsapApplication = ssapApplicationRepository.findOne(ssapApplication.getParentSsapApplicationId().longValue());
						
						if (ApplicationStatus.CLOSED.getApplicationStatusCode().equalsIgnoreCase(prevSsapApplication.getApplicationStatus()) ){
							LOGGER.info("Can not edit enrolled application since previous application is in " + prevSsapApplication.getApplicationStatus() + "stat");
							return;
						}else{
							boolean isEnrollmentActive = this.isEnrollmentActive(prevSsapApplication);
							if(isEnrollmentActive == false){
								LOGGER.info("No active enrollment found for application");
								return;
							}else{
								AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
								accountTransferRequestDTO.setCaseNumber(caseNumber);
								
								accountTransferRequestDTO.setEnrolledApplicationId(prevSsapApplication.getId());
								accountTransferRequestDTO.setCompareToApplicationId(ssapApplication.getId());
								accountTransferRequestDTO.setLCE(true);
								accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.LCE.value());
								
								this.callApplicationProcess(accountTransferRequestDTO);
							}
						}
					}else{
						LOGGER.info("ParentApplicationId is null for " + ssapApplication.getId());
						return;
					}
				} else {
					LOGGER.info("Application not found for " + applicationId);
					return;
				}
		} catch (Exception e) {
			throw new Exception("Edit application failed for " + applicationId);
		}
	}
	
	@Override
	public boolean closeApplications(SsapApplication ssapApplication) {
		LOGGER.info(ssapApplication.getId() + " ID - CLOSE " + ssapApplication.getApplicationStatus() + " APP...");
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.discardApp(ssapApplication.getCaseNumber(),  ApplicationStatus.CLOSED, updateUser.getId());
		if (!ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equals(ssapApplication.getApplicationStatus())){
			return true;
		}
		return false;
	}
	
	protected void callApplicationProcess(AccountTransferRequestDTO accountTransferRequestDTO) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("ID - PROCESS To be Executed " + PROCESS_LCE_APP+" "+accountTransferRequestDTO.getEnrolledApplicationId());
		}
		setTimeshiftContext(accountTransferRequestDTO);
		try {
			ghixRestTemplate.exchange(erpSvcUrl + PROCESS_LCE_APP, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		}catch(Exception e) {
			LOGGER.error("Exception in AT process "+e);
		}
	}
	
	private void setTimeshiftContext(AccountTransferRequestDTO requestDTO) {
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			requestDTO.setUser(TimeshiftContext.getCurrent().getUserName());
			requestDTO.setTsOffset(TimeshiftContext.getCurrent().getTimeOffset());
		}
	}
	
	boolean isEnrollmentActive(SsapApplication ssapApplication) {
		try {
			LOGGER.info("Check is any active Enrollment for application " + ssapApplication.getId());
			Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = null;
			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
			String enrollmentResponseStr = this.getEnrollmentsByMember(ssapApplicants, String.valueOf(ssapApplication.getCoverageYear()));
			
			if(StringUtils.isNotBlank(enrollmentResponseStr)) {
				EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
				if(enrollmentResponse != null) {
					enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();							
					ignoreTermHealthEnrollment(enrolleeData, ssapApplicants);
					if(enrolleeData.size() > 0){
						return true;
					}else{
						return false;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.info("Issue while checking is any active enrollment for application " + ssapApplication.getId());
			return false;
		}
		return false;
	}
	
	private String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		enrollmentStatusList.add(EnrollmentStatus.TERM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
	
	private void ignoreTermHealthEnrollment(Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, List<SsapApplicant> ssapApplicants) {
		if(enrolleeData != null) {
			for(SsapApplicant ssapApplicant : ssapApplicants) {
				List<EnrollmentMemberDataDTO> enrollmentMemberDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
				int healthEnrlCount = 0;
				if(CollectionUtils.isNotEmpty(enrollmentMemberDTOList)){
					for(EnrollmentMemberDataDTO enrollmentMemberDTO : enrollmentMemberDTOList){
						if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType())){
							healthEnrlCount++;
						}
					}
					if(healthEnrlCount > 1){
						Iterator<EnrollmentMemberDataDTO> itrEnrlMemberDtoList = enrollmentMemberDTOList.iterator();
						while(itrEnrlMemberDtoList.hasNext()) {
							EnrollmentMemberDataDTO enrollmentMemberDTO = itrEnrlMemberDtoList.next();
							if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType()) && "TERM".equalsIgnoreCase(enrollmentMemberDTO.getEnrollmentStatus())){
								itrEnrlMemberDtoList.remove();
							}
						}						
					}									
				}
			}
		}
	}
	
	private String getUserName() {
		String userName = TimeshiftContext.getCurrent().getUserName();
		if(userName == null) {
			userName = ReferralConstants.EXADMIN_USERNAME;
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Username in AT context:{}");
		}
		return userName;
	}
	
	
}
