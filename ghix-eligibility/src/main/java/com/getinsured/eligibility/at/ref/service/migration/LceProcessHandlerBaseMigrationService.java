package com.getinsured.eligibility.at.ref.service.migration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

public abstract class LceProcessHandlerBaseMigrationService {
	private static final Logger LOGGER = Logger.getLogger(LceProcessHandlerBaseMigrationService.class);
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Autowired private IUserRepository iUserRepository;
	@Autowired
	@Qualifier("qleValidationMigrationService")
	private QLEValidationService qleValidationService;
	
	@Autowired
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	
	@Autowired
	private AptcHistoryService aptcHistoryService;
	
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	
	@Autowired private Gson platformGson;
	
	protected static final String N = "N";

	protected static final String Y = "Y";

	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

	public SsapApplication loadApplication(Long applicationId) {
		return ssapApplicationRepository.findOne(applicationId);
	}

	public void closeSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("Close Application id " + ssapApplication.getId());
		setApplicationStatus(ssapApplication, ApplicationStatus.CLOSED.getApplicationStatusCode());
	}

	public void updateCurrentAppToEN(SsapApplication currentApplication) {
		setApplicationStatus(currentApplication, ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
	}

	public void updateAllowEnrollment(SsapApplication currentApplication, String allowEnrollment) {
		currentApplication.setAllowEnrollment(allowEnrollment);
		ssapApplicationRepository.save(currentApplication);
	}

	public void updateCurrentAppToER(SsapApplication currentApplication) {
		setApplicationStatus(currentApplication, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
	}

	public void copyEHBAmount(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Copy EHB amount from application - " + enrolledApplication.getId() + " to - " + currentApplication.getId());
		currentApplication.setEhbAmount(enrolledApplication.getEhbAmount());
		ssapApplicationRepository.save(currentApplication);
	}

	private void setApplicationStatus(SsapApplication ssapApplication, String applicationStatus) {
		LOGGER.debug("setApplicationStatus - " + applicationStatus);
		ssapApplication.setApplicationStatus(applicationStatus);
		ssapApplicationRepository.save(ssapApplication);
	}

	public void updateSsapApplication(SsapApplication ssapApplication) {
		ssapApplicationRepository.save(ssapApplication);
	}

	public List<String> fetchApplicantGuidsForApplicantsOnCurrentApplication(List<SsapApplicant> currentSsapApplicants) {

		List<String> applicantGuidList = new ArrayList<String>();
		for (SsapApplicant applicant : currentSsapApplicants) {
			if (Y.equals(applicant.getOnApplication())) {
				applicantGuidList.add(applicant.getApplicantGuid());
			}
		}
		return applicantGuidList;
	}

	public void updateSepQepDeniedFlag(SsapApplication currentApplication, String sepQepDenied) {
		currentApplication.setSepQepDenied(sepQepDenied);
		ssapApplicationRepository.save(currentApplication);
	}

	public void changeApplicationType(SsapApplication currentApplication, String applicationType) {
		currentApplication.setApplicationType(applicationType);
		ssapApplicationRepository.save(currentApplication);
	}

	public void updateSsapApplicationEvent(SsapApplicationEvent applicationEvent) {
		ssapApplicationEventRepository.save(applicationEvent);
	}

	public Optional<ApplicationValidationStatus> getValidationStatus(String caseNumber) {
		LOGGER.info("Get validation status of case number " + caseNumber);
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		return qleValidationService.runValidationEngine(caseNumber,  Source.EXTERNAL, updateUser.getId());
	}
	
	public void ignoreGatedNonPrimaryEvents(String caseNumber) {
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.ignoreNonPrimaryEvents(caseNumber, updateUser.getId());
	}
	
	public boolean isApplicationValidated(Optional<ApplicationValidationStatus> validationStatus) {
		boolean isApplicationValidated = false;
		if (validationStatus.isPresent() && (validationStatus.get()==ApplicationValidationStatus.VERIFIED || validationStatus.get()==ApplicationValidationStatus.NOTREQUIRED)){
			isApplicationValidated = true;
		}
		return isApplicationValidated;
	}
	
	void processDenial(String caseNumber) {
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.denySep(caseNumber,updateUser.getId());
	}
	
	public Date updateEffectiveDate(SsapApplication currentApplication,SsapApplicationEvent ssapApplicationEvent) {
		Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEvent);
		aptcHistoryService.updateEffectiveDate(coverageStartDate, new Long(currentApplication.getId()));
		return coverageStartDate;
	}
	
	public String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
}
