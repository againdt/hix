package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.io.IOException;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;

@Component
public class AccountTransferInboundSoapWebClient {

  private static final String AT_ENDPOINT =  "endpoints/AccountTransfer/accountTransfer.wsdl";

  private static Logger lOGGER = Logger.getLogger(AccountTransferInboundSoapWebClient.class);

  @Autowired
  private WebServiceTemplate accountTransferInboundServiceTemplate;


  /**
   * 
   * <soap:Header> <wsse:Security
   * xmlns:wsse="http://schemas.xmlsoap.org/ws/2003/06/secext" xmlns:wsu=
   * "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
   * > <wsu:Timestamp wsu:Id="TS-62">
   * <wsu:Created>2013-11-27T16:08:04.065Z</wsu:Created>
   * <wsu:Expires>2013-11-27T16:33:04.065Z</wsu:Expires> </wsu:Timestamp>
   * <wsse:UsernameToken wsu:Id="UsernameToken-61">
   * <wsse:Username>09.US*.DSH.001.001</wsse:Username> <wsse:Password Type=
   * "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
   * >HU$99shDSTRshs00</wsse:Password>
   * <wsu:Created>2013-11-27T16:08:04.065Z</wsu:Created> </wsse:UsernameToken>
   * </wsse:Security> </soap:Header>
   */
  
  private class WSSESecurityHeaderRequestWebServiceMessageCallback implements
      WebServiceMessageCallback {
	  
	    private static final String WSSE_SECURITY = "wsse";
	    private static final String SOAP_SECURITY = "Security";
	    private static final String SECURITY_NAMESPACE_URL = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    
    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {

      try {
        SaajSoapMessage saajSoapMessage = (SaajSoapMessage) message;
        SOAPMessage soapMessage = saajSoapMessage.getSaajMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        SOAPHeader soapHeader = soapEnvelope.getHeader();
        
        Name headerElementName = soapEnvelope.createName(SOAP_SECURITY, WSSE_SECURITY, SECURITY_NAMESPACE_URL);
        SOAPHeaderElement soapHeaderElement = soapHeader.addHeaderElement(headerElementName);
        soapHeaderElement.setActor(null);
      } catch (SOAPException soapException) {
        throw new RuntimeException("WSSESecurityHeaderRequestWebServiceMessageCallback", soapException);
      }
    }
  }

  public AccountTransferResponsePayloadType send(AccountTransferRequestPayloadType request) {
	String endpoint = new StringBuilder().append(GhixEndPoints.ELIGIBILITY_URL).append(AT_ENDPOINT).toString();
    lOGGER.debug(new StringBuilder().append("Sending account transfer request: ").append(request).append(" to endpoint: ").append(endpoint));
    AccountTransferResponsePayloadType response = null;
    accountTransferInboundServiceTemplate.setDefaultUri(endpoint);
    response = (AccountTransferResponsePayloadType) accountTransferInboundServiceTemplate.marshalSendAndReceive(request, new WSSESecurityHeaderRequestWebServiceMessageCallback());
    return response;
  }
}
