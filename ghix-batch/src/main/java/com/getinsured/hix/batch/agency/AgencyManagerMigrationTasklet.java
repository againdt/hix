package com.getinsured.hix.batch.agency;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.agency.service.AgencyMigrationService;

public class AgencyManagerMigrationTasklet extends StepExecutionListenerSupport implements Tasklet{
	
	AgencyMigrationService agencyMigrationService;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		String sourceFile = null;
		String delimeter = ",";
		int agencyColumnIndex = 6;
		int agencyRowIndex = 5;
		JobParameters jobParameters = chunkContext.getStepContext().getStepExecution().getJobParameters();
		sourceFile = jobParameters.getString("sourceFile",null);
		if(sourceFile == null) {
			throw new Exception("sourceFile parameter not found, Please provide file path for processing.");
		}
		delimeter = jobParameters.getString("delimeter",",");
		try{
			agencyColumnIndex = Integer.parseInt(jobParameters.getString("agencyColumn","9"));
			agencyRowIndex = Integer.parseInt(jobParameters.getString("agencyRow","5"));
		}catch(Exception ex){
			throw new Exception("Please provide valid value for agencyRow,agencyColumn");
		}
		
		agencyMigrationService.process(sourceFile, delimeter, agencyColumnIndex, agencyRowIndex);
		
		return  RepeatStatus.FINISHED;
	}

	public AgencyMigrationService getAgencyMigrationService() {
		return agencyMigrationService;
	}

	public void setAgencyMigrationService(AgencyMigrationService agencyMigrationService) {
		this.agencyMigrationService = agencyMigrationService;
	}
	
	
	
	
	
}
