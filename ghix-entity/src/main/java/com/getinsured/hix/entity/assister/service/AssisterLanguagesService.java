package com.getinsured.hix.entity.assister.service;

import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;

/**
 * Encapsulates service layer method calls for Assister Languages
 */
public interface AssisterLanguagesService {
	/**
	 * Finds the AssisterLanguages by given Assister
	 * 
	 * @param assister
	 *            the given Assister
	 * @return the Assister Languages
	 */
	AssisterLanguages findByAssister(Assister assister);

	/**
	 * Updates the AssisterLanguages table with the given AssisterLanguages
	 * object
	 * 
	 * @param assisterLanguages
	 *            object to update existing record
	 * @return the updated AssisterLanguages object
	 */
	AssisterLanguages update(AssisterLanguages assisterLanguages);

	/**
	 * Saves the given AssisterLanguages object in AssisterLanguages table
	 * 
	 * @param assisterLanguages
	 *            object to save
	 * @return the saved AssisterLanguages object
	 */
	AssisterLanguages saveAssisterLanguages(AssisterLanguages assisterLanguages);
}
