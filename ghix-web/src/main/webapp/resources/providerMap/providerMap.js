(function (app) {
    app.controller('ProviderMapController', ['$scope', 'doctorsService', 'Doctors', function ($scope, doctorsService, Doctors) {
        var pm = this;
        pm.showProviderMap = false;
        // pm.doctors = {};
        pm.zip = '';
        pm.total = 0;
        pm.distance = 10;
        pm.nextStartNum = nextStartNum;
        pm.isMoreThan100 = isMoreThan100;

        $scope.$on('modalShow', function (event, data) {
            $scope.$apply(function () {
                pm.showProviderMap = true;
            });
            getDoctors(data);
        });

        $scope.$on('needOpenInfowindow', function (event, data) {
            $scope.$broadcast('openInfowindow', data);
        });

        function getDoctors (data) {
            pm.zip = data.requestedData.userZip;
            pm.distance = data.requestedData.otherDistance;
            Doctors.currentPage(data.requestedData.currentPage + 1);
            doctorsService
                .getDoctors(data.requestedData, data.url)
                .then(function (data) {
                    // pm.doctors = data.providers;
                    pm.total = Doctors.totalDoctors(data.total_hits);
                    Doctors.setDoctors(data.providers);
                    $scope.$broadcast('createMap');
                });
        }

        function nextStartNum () {
            return Doctors.nextStartNum();
        }

        function isMoreThan100 () {
            return pm.total > 100;
        }

        $scope.$on('doctorsChange', function () {
            $scope.$broadcast('createMap');
        });
    }]);

    app.directive('modalShow', function () {
        return {
            restrict: 'A',
            link: link
        };

        function link (scope, elem, attrs) {
            var zip = $( '#zipcode' ).val(),
                currentPage = 0,
                miles = 10,
                requestedData = {
                            'userZip' : zip,
                            'currentPage' : currentPage,
                            'otherDistance' : miles,
                            "searchKey" : "",
                            "searchType" : 'doctor',
                            "networkId" : ""
                        };
            $(document).on('click', '.map__button', function (event) {
            	triggerGoogleTrackingEvent(['_trackEvent', 'Open Provider Map', 'Provider Map is shown', 'click on view map button']);
                requestedData.networkId = returnPlanID(event.target.id);
                if(event.target.id.indexOf("viewMapDetail") !== -1){
                	requestedData.otherDistance = $("#providerDetailDistance").val();
                }else{
                requestedData.otherDistance = $("#providerDistance").val();
                }
                $('#map-modal').on('shown.bs.modal', function () {
                    scope.$emit('modalShow', {
                        requestedData: requestedData,
                        url: attrs.url || ''
                    });
                });
            });
        }

        function returnPlanID( id ) {
            lastIndex = id.lastIndexOf( '_' ),
            planId = id.substring( lastIndex + 1 );
            return planId;
        }
    });

}(angular.module("PlanView")));