/**
 * 
 */
package com.getinsured.hix.batch.referral.service;

import java.util.List;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;

/**
 * @author Biswakesh
 *
 */
public interface NewFinancialRenewalService {
	
	List<Long> getSsapApplicationIds(RenewalStatus renewalStatus, String applicationType, Long coverageDate, EligibilityStatus eligibilityStatus, String applicationStatus);
	
	void processSsapIds(List<Long> ssapIds);
	
}
