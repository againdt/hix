package com.getinsured.hix.platform.batch.service;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.batch.core.StepExecution;

/**
 */
public class JobProgress {
    private final long jobElapsed;
    private final long stepElapsed;
    @JsonView
    private final String jobName;
    @JsonView
    private final Long jobExecutionId;
    @JsonView
    private final String stepName;
    @JsonView
    private final Long stepExecutionId;

    @JsonView
    private boolean cannotGuess = false;
    private long previousStepExecutionTime = 0;

    @JsonView
    private long progress = 0;

    public JobProgress(StepExecution stepExecution) {
        jobElapsed = System.currentTimeMillis() - stepExecution.getJobExecution().getStartTime().getTime();
        stepElapsed = System.currentTimeMillis() - stepExecution.getStartTime().getTime();
        jobName = stepExecution.getJobExecution().getJobInstance().getJobName();
        jobExecutionId = stepExecution.getJobExecutionId();
        stepName = stepExecution.getStepName();
        stepExecutionId = stepExecution.getId();
    }

    public JobProgress(StepExecution stepExecution, long progressPercentage) {
        this(stepExecution);
        progress = progressPercentage;
    }

    public void cannotGuess() {
        cannotGuess = true;
    }

    public void setPreviousStepExecutionTime(long value) {
        previousStepExecutionTime = value;
    }

    public void calculate() {
        if (!cannotGuess) {
            if (previousStepExecutionTime > stepElapsed) {
                progress = (stepElapsed) * 100 / previousStepExecutionTime;
            }
            else {
                cannotGuess();
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(jobName)
                .append("(")
                .append(jobExecutionId)
                .append(")/")
                .append(stepName)
                .append("(")
                .append(stepExecutionId)
                .append("): ");
        if (cannotGuess) {
            sb.append("Cannot Guess");
        }
        else {
            sb.append(progress).append(" %");
        }
        return sb.toString();
    }
}
