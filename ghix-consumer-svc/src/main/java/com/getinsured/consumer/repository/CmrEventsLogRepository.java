package com.getinsured.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.CmrEventsLog;

public interface CmrEventsLogRepository extends JpaRepository<CmrEventsLog, Integer> {
	
	
	@Query("select c from CmrEventsLog c where c.householdId=:householdId")
	public List<CmrEventsLog> getConsumerEventLogs(@Param("householdId") Integer householdId);
	
	
	
}
