package com.getinsured.eligibility.qlevalidation;

import java.util.List;
import java.util.Optional;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.hix.dto.ssap.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface QLEValidationService {

	void discardApp(String caseNumber, ApplicationStatus cancelled, Integer lastUpdatedBy);

	boolean updateApplicantEventAndRerunValidationEngine(Long applicantEventID, SsapApplicantEvent event);
	
	Optional<ApplicationValidationStatus> runValidationEngine(String caseNumber, Source sourceType, Integer lastUpdatedBy);

	boolean denySep(String caseNumber, Integer lastUpdatedUserId);

	boolean cancelEventsAndTickets(String caseNumber, Integer lastUpdatedUserId);
	
	void ignoreNonPrimaryEvents(String caseNumber, Integer lastUpdatedBy);
	
	boolean cleanUpApplicantEvent(List<Long> applivantEventIds, Integer lastUpdatedBy);
	
	Optional<ApplicationValidationStatus> reRunValidationEngineAfterSEPOverride(SsapApplication ssap,SsapApplicationEvent ssapApplicationEvent, Integer lastUpdatedBy);

}
