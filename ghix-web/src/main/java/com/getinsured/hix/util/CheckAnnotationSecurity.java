package com.getinsured.hix.util;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.parsers.DOMParser;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Checks security of all methods annotated with @RequestMapping.
 * 
 * @author yevgen.golubenko@getinsured.com
 */
public class CheckAnnotationSecurity
{
	private static final Log     log                     = LogFactory.getLog(CheckAnnotationSecurity.class);
	private static final String  MISSING_PLACEHOLDER     = "";
	private static final Pattern pattern                 = Pattern.compile("'(.*?)'");
	private static Desktop       desktop;
	private static String        startPackageName        = "com.getinsured.hix";
	private static final String  ACCESS_LEVEL_PERMIT_ALL = "permitAll";
	private static final String  ANONYMOUS_TEXT          = "ANONONYMOUS";

	/**
	 * Main execution point.
	 * 
	 * @param args command line arguments.
	 */
	public static void main(final String[] args)
	{
		final CheckAnnotationSecurity cas = new CheckAnnotationSecurity();
		final List<List<String[]>> l = CheckAnnotationSecurity.run(args);
		cas.saveReport(l.get(0), l.get(1), l.get(2));
	}

	/**
	 * Checks Annotation Security.
	 * 
	 * @param args command line arguments.
	 * @throws IOException if IOException occurs.
	 * @throws SecurityException if security exception occurs.
	 * @throws ClassNotFoundException if class is not found.
	 */
	public static List<List<String[]>> run(final String[] args)
	{
		log.info("Using class loader: ");
		final ClassLoader cl = ClassLoader.getSystemClassLoader();

		final URL[] urls = ((URLClassLoader) cl).getURLs();

		for (final URL url : urls)
		{
			System.out.println(url.getFile());
		}

		if (Desktop.isDesktopSupported())
		{
			desktop = Desktop.getDesktop();
		}

		if (args != null && args.length > 0)
		{
			startPackageName = args[0];
			System.out.println("Using command line argument for start package: " + startPackageName);
		}
		else
		{
			System.out.println("Using default start package: " + startPackageName);
		}

		final CheckAnnotationSecurity cas = new CheckAnnotationSecurity();
		final Document document = cas.getApplicationSecurityXml(null);
		final Map<String, String> patternAccess = new HashMap<>();

		if (document != null)
		{
			final NodeList root = document.getChildNodes();
			final Node beans = cas.getNode("beans:beans", root);
			final Node http = cas.getNode("http", beans.getChildNodes());

			/*
			 * If we have something like this:
			 * <http auto-config="true" use-expressions="true" disable-url-rewriting="true" access-denied-page="/account/user/denied">
				   <intercept-url pattern="/"  access="permitAll" />
				   ...
			   </http>
			 */
			if (http != null && http.getChildNodes() != null && http.getChildNodes().getLength() > 0)
			{
				final NodeList interceptUrls = http.getChildNodes();

				for (int i = 0; i < interceptUrls.getLength(); i++)
				{
					final Node url = interceptUrls.item(i);

					if (url != null)
					{
						final String pattern = cas.getNodeAttr("pattern", url);
						final String access = cas.getNodeAttr("access", url);

						if (!pattern.trim().equals("") && !access.trim().equals(""))
						{
							if (!patternAccess.containsKey(pattern))
							{
								if (/* !pattern.trim().equals("/") && */!access.trim().equals("isAuthenticated()"))
								{
									patternAccess.put(pattern, access);
								}
							}
							else
							{
								log.debug("! Duplicate key for pattern: " + pattern + " with access: " + access);
							}
						}
					}
				}
			}

			/*
			 * If we have something like this:
			 * <beans>
			 *    ...
			 * 	  <http pattern="/webservice/enrollment/endpoints/**" security="none" />
			 *    ...
			 * </beans>
			 */
			if (root != null && root.getLength() > 0 && root.item(0).getLocalName().equals("beans"))
			{
				final NodeList nl = root.item(0).getChildNodes();

				for (int i = 0; i < nl.getLength(); i++)
				{
					final Node n = nl.item(i);

					if (n != null && "http".equals(n.getLocalName()))
					{
						final String pattern = cas.getNodeAttr("pattern", n);
						final String security = cas.getNodeAttr("security", n);

						if (!pattern.trim().equals("") && !security.trim().equals(""))
						{
							if (!patternAccess.containsKey(pattern))
							{
								if (!security.trim().equals("isAuthenticated()"))
								{
									patternAccess.put(pattern, security);
								}
							}
							else
							{
								log.debug("!! Duplicate key for pattern: " + pattern + " with access: " + security);
							}
						}
					}
				}
			}
		}

		for (final String key : patternAccess.keySet())
		{
			log.debug("access: " + patternAccess.get(key) + " url: " + key);
		}

		final List<List<String[]>> authMissingList = cas.checkAnnotationsInClasspath(startPackageName);

		if (authMissingList != null)
		{
			final List<String[]> withAuth = authMissingList.get(0);
			final List<String[]> missingAuth = authMissingList.get(1);

			// Once we retrive two lists, now compare to definitions of the security xml to see 
			// if we allow anon access to any urls that are in the list with missing @PreAuthorize annotation
			final List<String[]> cleanMissing = new ArrayList<>();
			final List<String[]> anonymousAllowed = new ArrayList<>();

			for (final String[] missingRow : missingAuth)
			{
				if (missingRow == null || missingRow[0] == null)
				{
					continue;
				}

				boolean anon = false;

				String requestMappingPath = missingRow[0];

				for (String accessUrl : patternAccess.keySet())
				{
					final String accessLevel = patternAccess.get(accessUrl);

					if (!accessUrl.startsWith("/"))
					{
						accessUrl = "/" + accessUrl;
					}

					if (!requestMappingPath.startsWith("/"))
					{
						requestMappingPath = "/" + requestMappingPath;
					}

					if (accessLevel.equals(ACCESS_LEVEL_PERMIT_ALL) || "none".equals(accessLevel))
					{
						// wildcard match
						if (accessUrl.contains("**"))
						{
							final String testAccessUrl = accessUrl.substring(0, accessUrl.length() - 3);
							if (requestMappingPath.startsWith(testAccessUrl))
							{
								System.out.println("Wildcard Anonymous access: " + requestMappingPath);
								anon = true;
							}
						}
						// direct match
						else
						{
							if (requestMappingPath.equals(accessUrl))
							{
								System.out.println("Direct Anonymous access: " + requestMappingPath);
								anon = true;
							}
						}
					}
				}

				if (anon)
				{
					missingRow[1] = ANONYMOUS_TEXT;
					anonymousAllowed.add(missingRow);
				}
				else
				{
					cleanMissing.add(missingRow);
				}
			}

			final List<List<String[]>> l = new ArrayList<>();

			l.add(withAuth);
			l.add(anonymousAllowed);
			l.add(cleanMissing);

			return l;
		}
		else
		{
			log.fatal("Unable to read package: " + startPackageName);
			throw new RuntimeException("Unable to read package: " + startPackageName);
		}
	}

	/**
	 * Saves report on the file system.
	 * 
	 * @param withAuth list with authentication urls/method defined
	 * @param anonymousAllowed list that has methos that are allowed to have
	 *            anonymous access (defined in security xml)
	 * @param missingAuth list with missing definitions.
	 */
	private void saveReport(final List<String[]> withAuth, final List<String[]> anonymousAllowed, final List<String[]> missingAuth)
	{
		final String desktopPath = System.getProperty("user.home") + "/Desktop";
		final File desktopFile = new File(desktopPath);

		String fileName = null;

		if (desktopFile.exists() && desktopFile.canWrite())
		{
			fileName = desktopPath + "/annotation-security";
		}
		else
		{
			fileName = System.getProperty("java.io.tmpdir") + "/annotation-security";
		}

		final DateFormat df = new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss");
		final Date now = new TSDate();
		fileName += "-" + df.format(now);

		fileName += ".csv";

		CSVWriter cwriter = null;
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(fileName);
			final OutputStreamWriter osw = new OutputStreamWriter(fos);
			cwriter = new CSVWriter(osw);

			cwriter.writeAll(withAuth);
			cwriter.writeAll(anonymousAllowed);
			cwriter.writeAll(missingAuth);

			System.out.println("finished checking security");

			// After closing, lets open this
			if (desktopFile != null)
			{
				try
				{
					System.out.println("Opening default viewer for *.csv files");

					desktop.open(new File(fileName));
				}
				catch (final Exception e)
				{
					/* ignore */
				}
			}

			System.out.println("Report saved: " + fileName);
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(fos);
			try
			{
				if(cwriter != null)
				{
				cwriter.close();
			}
			}
			catch (final IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private List<List<String[]>> checkAnnotationsInClasspath(final String startPackage)
	{
		final List<String[]> missingAuthList = new ArrayList<String[]>();
		final List<String[]> withAuthList = new ArrayList<String[]>();

		Iterable<Class> c = null;

		try
		{
			c = getClasses(startPackage);
		}
		catch (final IOException e)
		{
			//e.printStackTrace();
			System.out.println("Unable to read package: " + startPackage);
			return null;
		}
		catch (final ClassNotFoundException e)
		{
			//e.printStackTrace();
			System.out.println("Unable to read classes in package: " + startPackage);
			return null;
		}
		catch (final RuntimeException re)
		{
			//re.printStackTrace();
			System.out.println("Runtime Exception: " + re.getMessage());
		}

		for (final Class testClass : c)
		{
			// System.out.println("Processing: " + testClass.getName());
			Method[] methods = new Method[] {};

			try
			{
				methods = testClass.getMethods();
			}
			catch (final Throwable e)
			{
				log.warn("Could not get methods for class: " + testClass.getName(), e);
			}

			for (final Method method : methods)
			{
				if (method.isAnnotationPresent(RequestMapping.class))
				{
					final RequestMapping rma = method.getAnnotation(RequestMapping.class);
					final PreAuthorize paa = method.getAnnotation(PreAuthorize.class);

					final StringBuffer rmURLs = new StringBuffer();
					final StringBuffer paRoles = new StringBuffer();

					if (rma.value() != null)
					{
						for (int i = 0; i < rma.value().length; i++)
						{
							final String s = rma.value()[i];
							rmURLs.append(s);

							if (i < rma.value().length - 1)
							{
								rmURLs.append("|");
							}
						}
					}

					boolean missing = false;

					if (paa != null && paa.value() != null)
					{
						// Capture string between ' and ' otherwise put
						// MISSING_PLACEHOLDER
						final Matcher matcher = pattern.matcher(paa.value());

						if (matcher.find())
						{
							paRoles.append(matcher.group(1));
						}
						else
						{
							missing = true;
							paRoles.append(MISSING_PLACEHOLDER);
						}
					}
					else
					{
						missing = true;
						paRoles.append(MISSING_PLACEHOLDER);
					}

					final String[] csvLine = new String[]
							{
							rmURLs.toString(),
							paRoles.toString(),
							testClass.getName() + '.' + method.getName()
							};

					if (missing)
					{
						missingAuthList.add(csvLine);
					}
					else
					{
						withAuthList.add(csvLine);
					}
				}
			}
		}

		final List<List<String[]>> ret = new ArrayList<>();

		ret.add(withAuthList);
		ret.add(missingAuthList);

		return ret;
	}

	/**
	 * Reads applicationSecurity-[type].xml file and returns XML document.
	 * 
	 * @param type one of [ db | cas | ghix | oam | -blank|empty-]
	 * @return
	 */
	private Document getApplicationSecurityXml(final String type)
	{
		// classpath:ghix-platform-3.0-BUILD-SNAPSHOT.jar!:resources/applicationSecurity[-db].xml
		Document doc = null;

		final String ghixHome = System.getenv("GHIX_HOME");
		String xmlFileName = "applicationSecurity";
		String xmlFilePath = "";

		if (type != null && !type.trim().equals(""))
		{
			xmlFileName += "-" + type;
		}
		xmlFileName += ".xml";

		if (ghixHome != null)
		{
			xmlFilePath = ghixHome + File.separatorChar + "ghix-platform" +
					File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar
					+ xmlFileName;
			final File sysFile = new File(xmlFilePath);

			// If this file doesn't exist or we can't read it, try to find it in ghix-platform-*.jar instead.

			InputSource inputSource = null;

			if (!sysFile.exists() || !sysFile.canRead())
			{
				log.info("Could not find [" + sysFile.getAbsolutePath() + "] trying to find it in classpath.");
				final URL xmlResource = getClass().getClassLoader().getResource(xmlFileName);
				final InputStream is = getClass().getClassLoader().getResourceAsStream(xmlFileName);

				final File xmlResourceFile = new File(xmlResource.getFile());

				try
				{
					if ((is != null && is.available() > 0) || (xmlResourceFile.exists() && xmlResourceFile.canRead()))
					{
						log.info("Found " + xmlFileName + " in classpath: " + xmlResourceFile.getAbsolutePath());
						xmlFilePath = xmlResourceFile.getAbsolutePath();
						inputSource = new InputSource(is);
					}
				}
				catch (final IOException e)
				{
					log.error("Could not read " + xmlFileName + ": " + e.getMessage());
				}finally{
					IOUtils.closeQuietly(is);
				}
			}

			System.out.println("Using GHIX_HOME: " + ghixHome);
			System.out.println("  Security file: " + xmlFileName);
			System.out.println(" Full file path: " + xmlFilePath);

			final DOMParser parser = new DOMParser();

			try
			{
				if(inputSource != null)
				{
					parser.parse(inputSource);
				}
				else
				{
					parser.parse(xmlFilePath);
				}

				doc = parser.getDocument();
			}
			catch (SAXException | IOException e)
			{
				log.error("Could not load " + xmlFilePath, e);
			}

			return doc;
		}

		System.out.println("*** Warning *** GHIX_HOME environment variable is undefined.\n" +
				"Cannot parse " + xmlFileName +
				"\nTo set environment variable, run:\nexport GHIX_HOME=/path/to/ghix - on linux/mac\n" +
				"set GHIX_HOME=C:\\path\\to\\ghix - on windows");
		return null;
	}

	/**
	 * Gets attribute from given node.
	 * 
	 * @param attrName name of the attribute.
	 * @param node node to read attribute from.
	 * @return node value or empty string.
	 */
	protected String getNodeAttr(final String attrName, final Node node)
	{
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			final NamedNodeMap attrs = node.getAttributes();

			for (int y = 0; y < attrs.getLength(); y++)
			{
				final Node attr = attrs.item(y);

				if (attr.getNodeName().equalsIgnoreCase(attrName))
				{
					return attr.getNodeValue();
				}
			}
		}

		return "";
	}

	/**
	 * Gets attribute from given node list.
	 * 
	 * @param tagName name of the tag.
	 * @param attrName attribute name.
	 * @param nodes list of nodes.
	 * @return node value or empty string.
	 */
	protected String getNodeAttr(final String tagName, final String attrName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);

			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				final NodeList childNodes = node.getChildNodes();

				for (int y = 0; y < childNodes.getLength(); y++)
				{
					final Node data = childNodes.item(y);

					if (data.getNodeType() == Node.ATTRIBUTE_NODE)
					{
						if (data.getNodeName().equalsIgnoreCase(attrName))
						{
							return data.getNodeValue();
						}
					}
				}
			}
		}

		return "";
	}

	/**
	 * Returns node from given node list.
	 * 
	 * @param tagName tag name of the node thats needed.
	 * @param nodes list of nodes where this node is placed
	 * @return Node or null.
	 */
	protected Node getNode(final String tagName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				return node;
			}
		}

		return null;
	}

	/**
	 * Returns node text value from given node.
	 * 
	 * @param node node that contains text, e.g. &lt;name&gt;Cousin
	 *            Vinny&lt;/name&gt;
	 * @return Cousin Vinny
	 */
	protected String getNodeValue(final Node node)
	{
		final NodeList childNodes = node.getChildNodes();
		for (int x = 0; x < childNodes.getLength(); x++)
		{
			final Node data = childNodes.item(x);
			if (data.getNodeType() == Node.TEXT_NODE)
			{
				return data.getNodeValue();
			}
		}
		return "";
	}

	/**
	 * Returns text node value.
	 * 
	 * @param tagName tag name of the node.
	 * @param nodes list of nodes.
	 * @return text node value or empty string.
	 */
	protected String getNodeValue(final String tagName, final NodeList nodes)
	{
		for (int x = 0; x < nodes.getLength(); x++)
		{
			final Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName))
			{
				final NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++)
				{
					final Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
					{
						return data.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	private Iterable<Class> getClasses(final String packageName)
			throws ClassNotFoundException, IOException
			{
		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final String path = packageName.replace('.', '/');
		final Enumeration<URL> resources = classLoader.getResources(path);
		final List<File> dirs = new ArrayList<File>();

		while (resources.hasMoreElements())
		{
			final URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}

		final List<Class> classes = new ArrayList<Class>();

		for (final File directory : dirs)
		{
			classes.addAll(findClasses(directory, packageName));
		}

		return classes;
			}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	private List<Class> findClasses(final File directory, final String packageName)
	{
		final List<Class> classes = new ArrayList<Class>();

		if (!directory.exists())
		{
			return classes;
		}

		final File[] files = directory.listFiles();

		try
		{
			for (final File file : files)
			{
				if (file.isDirectory())
				{
					classes.addAll(findClasses(file, packageName + "." + file.getName()));
				}
				else if (file.getName().endsWith(".class"))
				{
					Class cls = null;
					final String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
					log.debug("Parsing class name: " + className);

					try
					{
						cls = Class.forName(className);
						classes.add(cls);
					}
					catch (final Throwable e)
					{
						log.error("Unable to initialize a class to parse @RequestMapping annotation(s), because: " + e.getMessage());
					}
				}
			}
		}
		catch (final Exception e)
		{
			log.error("Exception while parsing classes", e);
		}

		return classes;
	}
}
