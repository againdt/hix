<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link href="<c:url value="/resources/css/jquery.multiselect.css" />"
	rel="stylesheet" type="text/css" />

<link href="<c:url value="/resources/css/planViewStyles-v2.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/variables.css" />"
	rel="stylesheet" type="text/css" />

<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}

.modal {
	width: 760px;
}

.pagination {
	margin: 0 0 0 10px;
	position: relative;
}

#main >.container {
min-height:1080px;
}
#main {margin-top:0px;}

.addedItem,.addedToFav {
	background-color: #74B33C;
	background-image: -moz-linear-gradient(center top, #99cc00, #74B33C);
	background-repeat: repeat-x;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
}

.addedItem:hover,.addedToFav:hover {
	background-color: #99cc00;
}

h3.green {
	margin-top: 50px;
}

.ui-multiselect {
	margin-top: 0;
}

.ui-widget {
	font-family: "Droid Sans", Arial, San-serif;
}

.btn.ui-multiselect {
	font-size: 12px;
}
.navbar {margin-bottom: 0px; }
</style>

<div class="row-fluid" id="main">
	<div class="row-fluid">
		<div class="span12">
			<div id="planSelect" class="tabbedSections">
				<div class="plans-top-wrapper">
					<div class="row-fluid" id="titlebar">
						<div class="gutter10">
							<div class="subnav span1">
								<ul class="nav nav-pills">
									<li class=""><a
										href="<c:url value="/shop/employer/plandisplay/setpreferences" />">Back</a>
									</li>
								</ul>
							</div>
							<div id="plansbar">
								<h3 class="pull-left" id="Nplans">
									<div id="totalPlanCount"></div>
									Plans
								</h3>
								<div class="pagination pull-left">
									<ul id="pagination"></ul>
								</div>
								<span id="sort"></span> &nbsp; <span id="compare"></span> &nbsp;

								<div class="pull-right">
									<select id="addToCart" name="example" multiple="multiple"
										style="display: none">
										<option value="levelbronze" inputClass="addToCart">Bronze</option>
										<option value="levelsilver" inputClass="addToCart">Silver</option>
										<option value="levelgold" inputClass="addToCart">Gold</option>
										<option value="levelplatinum" inputClass="addToCart">Platinum</option>
									</select> <a class="btn-primary btn btn-small" id="cartBtn"
										href="<c:url value="/shop/employer/plandisplay/cart" />">Your
										Cart
										<div id="planCount">(Loading...)</div>
									</a>
									<div id="cartmessageplan"
										style="position: relative; display: none">
										<div class="popover bottom"
											style="display: block; top: 20px; left: -175px;">
											<div class="arrow" style="display: block;"></div>
											<div class="popover-content">
												<p>1 plan added to cart</p>
											</div>
										</div>
									</div>
									<div id="cartmessageplanremove"
										style="position: relative; display: none">
										<div class="popover bottom"
											style="display: block; top: 20px; left: -175px;">
											<div class="arrow" style="display: block;"></div>
											<div class="popover-content">
												<p>1 plan removed from cart</p>
											</div>
										</div>
									</div>
									<div id="cartmessagelevel"
										style="position: relative; display: none">
										<div class="popover bottom"
											style="display: block; top: 20px; left: -175px;">
											<div class="arrow" style="display: block;"></div>
											<div class="popover-content">
												<p>1 level added to cart</p>

											</div>
										</div>
									</div>
									<div id="cartmessagelevelremove"
										style="position: relative; display: none">
										<div class="popover bottom"
											style="display: block; top: 20px; left: -175px;">
											<div class="arrow" style="display: block;"></div>
											<div class="popover-content">
												<p>1 level removed from cart</p>
											</div>
										</div>
									</div>
								</div>
								<!-- pull-right -->
							</div>
							<!--plansbar-->
						</div>
						<!--gutter10-->
					</div>
					<!--titlebar-->

					<div class="scroll-left-arrow-wrapper">
						<div class="scroll-left-arrow">
							<h1 id="prevItem" class="nextprev">&#8249;</h1>
						</div>
					</div>
					<div class="plans-header plans-section row-fluid plans-wrap"
						id="plans-header">

						<div class="plans-header-content-wrapper">
							<div class="plans-section-content">
								<div id="summary" class="��well" �� style="��background: none"��>
									<h3 class="green">Loading...</h3>
									<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
								</div>
								<div id="summary_err" class="��well"
									�� style="�background: none; display: none;">
									<h3 class="box-gutter10 label-info"
										style='vertical-align: center; margin: 20px 10px; width: 95%'>
										No plans are available for the selected employees. Please
										change your preferences and try again.</h3>
								</div>
							</div>
							<!-- plans-section-content-->
						</div>
						<!--plans-header-content-wrapper-->

					</div>
					<!--plans-section plans-header-->
					<div class="scroll-right-arrow-wrapper">
						<div class="scroll-right-arrow">
							<h1 id="nextItem" class="nextprev">&#8250;</h1>
						</div>
					</div>
				</div>
				<!-- End of top bar -->
				<table class="view-plans">
					<tbody>
						<tr>
							<!-- <td class="scroll-left-button">&nbsp;</td> -->
							<td class="center-container row-fluid">
								<div class="plans-container">

									<div class="plans-section  row-fluid" id="plans-cost">
										<div class="plans-section-header">
											<h3>Costs</h3>
										</div>
										<div class="plans-section-info"></div>
										<div class="plans-section-content row-fluid">
											<div id="cost"></div>
										</div>
										<!--plans-section-content-->
									</div>
									<!--plans-section row-fluid-->

									<div class="plans-section dedictible row-fluid">
										<div class="plans-section-header">
											<h3>Benefits</h3>
										</div>
										<div class="plans-section-info"></div>
										<div class="plans-section-content row-fluid">
											<div id="benefit"></div>
										</div>
										<!--plans-section-content-->
									</div>
									<!--plans-section row-fluid-->

								</div> <!--plans-container-->
							</td>
							<!-- <td class="scroll-right-button">&nbsp;</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- container -->
</div>
<!-- main -->

<!--sample template for result item-->
<script type="text/html" id="resultItemTemplateSummary">
			<div class="plan plan-<@=id@>">
				<div class="gutter">
				<img src='<c:url value="/resources/img/logo_<@= issuerText @>.png" />' alt="Logo <@= issuer @>" /> 
				<table class="planNameWrap">
					<tr><td class="planName">
						<@= name @>
					</td></tr>
				</table>
				<h4 class="planType <@=level@>"><small><@=level @></small></h4>
    			<div class="actions">
                	<a class="btn btn-small seeplandetails" href="plandetails/<@=id@>" rel="tooltip" title="View more details for this plan"></a>
					<a class="addToCompare addtofav btn btn-small" href="#" id="compare_<@=id@>" rel="tooltip" title="Add to 'Your Favorites' for comparison. Click again to remove.">
						
					</a>
					<a class="removeCompare addtofav addedToFav btn btn-small" href="#" id="remove_<@=id@>" style="display:none;" rel="tooltip" title="Add to 'Your Favorites' for comparison. Click again to remove.">
					
					</a>
                    <a class="addToCart btn btn-primary btn-small" id="cart_<@=id@>"  rel="tooltip" title="Add this plan to your cart">&nbsp;Add to Cart</a>
                </div>
				</div>
            </div>			

		</script>

<script type="text/html" id="resultItemTemplateCost">
			<div class="plan plan-<@=id@>">
				<div class="gutter">
				<@ if (empCount < employeeTotalCount) { @>
					<p class="label label-warning">Available only to <@=empCount@> of your <@=employeeTotalCount@> employees.</p>
				<@ } else{ @>
					<p class="label label-info">Available to all of your <@=employeeTotalCount@> employees.</p>
				<@ } @>

				<@ if (notAffodableCount >= empCount) { @>
					<p class="label label-important">Not affordable to all of your <@=empCount@> employees.</p>
				<@ } else if (notAffodableCount > 0) { @>
					<p class="label label-warning">Not affordable to <@=notAffodableCount@> of your employees.</p>
				<@ } else if (empCount == employeeTotalCount){ @>
					<p class="label label-info">Affordable to all of your <@=empCount@> employees.</p>
				<@ } else{ @>
					<p class="label label-info">Affordable to <@=empCount@> employees.</p>
				<@ } @>
				<table class="table ">
					<tbody>
						<tr>
                        	<td>Employer's Total Cost</td>
                            <td><h4>$<@=employerTotalCost@><small>per month</small></h4></td>
            			</tr>
						<tr>
                        	<td>Contribution towards employees</td>
                            <td><h4>$<@=employerContrForEmp@><small>per month</small></h4></td>
                        </tr>
						<@ if (taxPenalty > 0) { @>
							<tr>
                        		<td>Estimated Tax Penalty</td>
                            	<td><h4>$<@=taxPenalty@><small>per month</small></h4></td>
							</tr>
						<@ } else if(estimatedTaxCredit > 0){ @>
							<tr>
                        		<td>Estimated Tax Credit (@<@=taxCredit@>%)</td>
                            	<td><h4>$<@=estimatedTaxCredit@><small>per month</small></h4></td>
							</tr>
						<@ } @>
						<tr>
                        	<td>Average Premium per Employee</td>
                            <td><h4>$<@=avgPremium@><small>per month</small></h4></td>
						</tr>
					</tbody>
				</table>

				<p class="contribution">
                                  <em style="padding-bottom:<@=employerContrHeight /2.5@>px">Employer Contributes</em> 
                    <span style="height:<@=employerContrHeight@>px" class="employerC">
                                         <b>$<@=employerContrForEmpInPreimum @></b>
                                  </span>
                           </p>
                                         
                <p class="contribution" style="border-bottom:solid 1px #99cc00">
                                  <em>Employee Pays</em>
                     <span style="height:<@=employeeContrHeight@>px" class="employeeC">
                                         <b>$<@=employeePayInPreimum @></b>
                                  </span>
                           </p>
				</div>
			</div>
		</script>

<script type="text/html" id="resultItemTemplateBenefit">
			<div class="plan plan-<@=id@> ">
				<table class="table ">
                	<tr>
                    	<td>Deductible</td>
                        <td><h4>$<@=deductible@></h4></td>
					</tr>
                    <tr>
						<td>Annual Out-of-Pocket Limit</td>
					    <td><h4>$<@=oopMax@><br/><small>Excl. deductible</small></h4></td>
					</tr>
					<tr>
						<td>Coninsurance</td>
					    <td><h4><@=coInsurance@>%</h4></td>
					</tr>
					<tr>
						<td>Doctor Visits</td>
					    <td><h4>$<@=officeVisit@></h4></td>
					</tr>
				</table>
			</div>
		</script>
<!--sample template for pagination UI-->

<script type="text/html" id="tmpPagination">
		
			<@ if (page != 1) { @>
				<li><a href="#" class="prev" data-original-title="">&#8249;</a></li>
			<@ } @>
			<@ _.each (pageSet, function (p) { @>
				<@ if (page == p) { @>
					<li class="active"><a href="#" class="page" data-original-title=""><@= p @></a></li>
				<@ } else { @>
					<li><a href="#" class="page" data-original-title=""><@= p @></a></li>
				<@ } @>
			<@ }); @>
			<@ if (lastPage != page) { @>
				<li><a href="#" class="next" data-original-title="">&#8250;</a></li>
			<@ } @>
		
</script>

<script type="text/html" id="tmpCompare">
	<span><a href='#' class="backToAll btn btn-small" style="display:none;">Back to all plans(<span id="totalCount" class=""></span>)</a></span>
	<span><a href='#' class="btn btn-small comparePlans"  rel="tooltip" title="Compare plans in your favorites list. Click on the heart icon under a plan name to add the plan to this list."> Your Favorites (<span id="compareCount" class="">0</span>)</a></span>
	<div id="favmessage" style="position:relative; display:none ">
		<div class="popover bottom" style="display:block;top:10px;left:-175px;">
			<div class="arrow" style="display:block;"></div>
            <div class="popover-content">
				<p>1 plan added to favorites</p>
            </div>
		</div>
	</div>
</script>
<script type="text/html" id="tmpSort">
<div class="dropdown">
                 <a href="#" data-target="#" data-toggle="dropdown-menu" role="button" id="sort_link" class="btn btn-small dropdown-toggle" data-original-title="" onclick="toggleSort();">
                           Sort By
	                  <b class="caret"></b>
                 </a>
                                <ul class="dropdown-menu" id="sort_dropdown">
                                  <li><a href="#" class="sort_premium" id="premium">Premium <span id="premiumOrder"><span>(Low To High)</a></li>
                                  <li><a href="#" class="sort_deductible" id="deductible">Deductible <span id="deductibleOrder">(Low To High)<span></a></li>
                                  <li><a href="#" class="sort_issuer" id="issuerText">Insurer<span id="issuerTextOrder"><span></a></li>
                                </ul>
 </div>
</script>


<script src="<c:url value="/resources/js/bootstrap.js" />">     </script>
<script src="<c:url value="/resources/js/modernizr.custom.js" />">      </script>
<script src="<c:url value="/resources/js/waypoints.min.js" />">     </script>
<script
	src="<c:url value="/resources/js/jquery-ui-1.8.22.custom.min.js" />">     </script>
<script src="<c:url value="/resources/js/jquery.multiselect.js" />">     </script>

<script type="text/javascript">
$("#addToCart").multiselect({header: false,select: 'levelsilver'});

</script>

<script src="<c:url value="/resources/js/underscore-min.js" />">     </script>
<script src="<c:url value="/resources/js/backbone-min.js" />">     </script>
<script src="<c:url value="/resources/js/json2.js" />">     </script>
<script src="<c:url value="/resources/js/plandisplay/showplans.js" />">     </script>

<!--Backbone.Paginator-->
<script src="<c:url value="/resources/js/backbone-pagination.js" />">     </script>
<script src="<c:url value="/resources/js/backbone-cart.js" />">     </script>

<!--Models/Collections-->
<script src="<c:url value="/resources/js/plandisplay/models/plan.js" />">     </script>
<script
	src="<c:url value="/resources/js/plandisplay/collections/PaginatedCollection.js" />">     </script>
<script src="<c:url value="/resources/js/plandisplay/models/cart.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/collections/CartCollection.js" />">
	
</script>

<!--Views-->
<script
	src="<c:url value="/resources/js/plandisplay/views/PlanView.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/views/PaginationView.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/views/CompareView.js" />">
	
</script>
<script
	src="<c:url value="/resources/js/plandisplay/views/SortView.js" />">
	
</script>





<script type="text/javascript">
	$(window).load(function() {
		$('.view-plans').animate({
			opacity : 1,
			right : 0
		}, 650);
	});

	$(document).ready(function() {

		$('.navbar .dropdown-toggle').click(function(){
			$('.navbar .dropdown').addClass('open');
		}); 

		$.ajaxSetup({
			cache : false
		});

		$('#planSelect a').tooltip({
			placement : 'top'
		});

		$.waypoints.settings.scrollThrottle = 30;

		$('.plans-top-wrapper').waypoint(function(event, direction) {
			$(this).toggleClass('sticky', direction === "down");
			event.stopPropagation();
		}, {
			offset : '0'
		});

	});

	function toggleSort() {
		$('#planSelect .dropdown').addClass('open');
	}
</script>
