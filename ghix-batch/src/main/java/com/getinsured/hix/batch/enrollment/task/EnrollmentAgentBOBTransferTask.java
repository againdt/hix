package com.getinsured.hix.batch.enrollment.task;

import java.util.List;

import org.springframework.batch.core.StepExecution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.enrollment.repository.IEnrlAgentBobTransferRepository;
import com.getinsured.hix.enrollment.service.EnrlAgentBobTransferService;
import com.getinsured.hix.model.enrollment.EnrlAgentBOBTransfer;
import com.getinsured.hix.platform.batch.service.GhixStoppableTasklet;

public class EnrollmentAgentBOBTransferTask extends GhixStoppableTasklet {
	
	private EnrlAgentBobTransferService enrlAgentBobTransferService;
	private IEnrlAgentBobTransferRepository iEnrlAgentBobTransferRepository;
	private  final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAgentBOBTransferTask.class);
	@Override
	protected RepeatStatus _execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		StepExecution se = chunkContext.getStepContext().getStepExecution();
		Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		// TODO Auto-generated method stub
		//enrlAgentBobTransferService.serveEnrlAgentBOBTransferJob(jobExecutionId);
		List<EnrlAgentBOBTransfer> agentBOBTransferRequests= iEnrlAgentBobTransferRepository.findAgentBOBTransferRequest();
		if(agentBOBTransferRequests!=null && agentBOBTransferRequests.size()>0){
			for(EnrlAgentBOBTransfer bobTransfer: agentBOBTransferRequests){
				
				if(se.isTerminateOnly()){
		            // gracefully stop the job
		            se.getJobExecution().setExitStatus(ExitStatus.STOPPED);
		            break;
		        }
				if(jobExecutionId!=null){
					bobTransfer.setBatchExecId(jobExecutionId.intValue());
					
				}
				enrlAgentBobTransferService.serveEnrlAgentBOBTransferJob(bobTransfer);
				
			}
		}else{
			LOGGER.error("No BOB Transfer Request found in database for job execution id "+jobExecutionId);
		}
	
		
		return RepeatStatus.FINISHED;
	}

	public EnrlAgentBobTransferService getEnrlAgentBobTransferService() {
		return enrlAgentBobTransferService;
	}

	public void setEnrlAgentBobTransferService(EnrlAgentBobTransferService enrlAgentBobTransferService) {
		this.enrlAgentBobTransferService = enrlAgentBobTransferService;
	}

	public IEnrlAgentBobTransferRepository getiEnrlAgentBobTransferRepository() {
		return iEnrlAgentBobTransferRepository;
	}

	public void setiEnrlAgentBobTransferRepository(IEnrlAgentBobTransferRepository iEnrlAgentBobTransferRepository) {
		this.iEnrlAgentBobTransferRepository = iEnrlAgentBobTransferRepository;
	}

	@Override
	public long getStopDelaySeconds() {
		// TODO Auto-generated method stub
		return 5;
	}

}