import { exceptionActionTypes } from '../constants/actionTypes';

export const initialState = {
    exceptionType: null,
    exceptionPages: []
};

export default (state = initialState, action) => {

    if(action.exceptionPages) {
        if(!Array.isArray(action.exceptionPages)) {
            throw new Error('Not an array has been passed in exceptionPages prop in exceptionReducer');
        }

        action.exceptionPages.forEach(e => {
            if(typeof e !== 'number') {
                throw new Error('Exception page has not been provided as a number');
            }
        });
    } else if(action.exceptionPages !== undefined) {
        throw new Error ('One of false types was provided in exceptionPages prop in exceptionReducer');
    }

    switch (action.type) {

    case exceptionActionTypes.SET_EXCEPTION:
        return {
            exceptionType: action.exceptionType,
            exceptionPages: action.exceptionPages
        };

    case exceptionActionTypes.REMOVE_EXCEPTION:
        return initialState;

    case exceptionActionTypes.UPDATE_EXCEPTION:
        return {
            ...state,
            exceptionPages: action.exceptionPages
        };

    case exceptionActionTypes.CLEAR_STATE:
        return initialState;

    default:
        return state;
    }
};
