package com.getinsured.hix.planmgmt.web;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.admin.web.AdminIssuerFinancialController;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerFinancialController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminIssuerFinancialController.class);
	
	@Autowired private IssuerService issuerService;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	
		// issuer account add financial information
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/add", method = {RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize("hasPermission(#model, 'ADD_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO')")
		public String issuerAccountProfileAddFinancialInfo(Model model, HttpServletRequest request) throws InvalidUserException {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			boolean is_Payment_Gateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
			try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setModuleID(issuerObj.getId());
				paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
				paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
				Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
				List<PaymentMethods> paymentMethodList = ((List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY));
				PaymentMethods paymentMethod = null;
				if(paymentMethodList != null && !paymentMethodList.isEmpty()){
					paymentMethod = paymentMethodList.get(0);
				}
				model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, (paymentMethod == null) ? 0 : paymentMethod.getId());
				
				//LOGGER.info("Issuer Account :: Add Financial Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/account/profile/financial/info/list");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, is_Payment_Gateway);
				if (is_Payment_Gateway) {
					Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
					if (map != null) {
						PaymentMethods paymentMethodsInfo = (PaymentMethods) map.get(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ);
						model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsInfo);
					}
				}
				return "planmgmt/issuer/account/profile/financial/add";
			}
			} catch (Exception ex) {
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ACCOUNT_PROFILE_ADD_FINANCIAL_INFO_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
			}
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}

		
		
		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/info/changePaymentStatus")
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		@ResponseBody
		public String changePaymentStatus(@RequestParam Integer id, @RequestParam String status) {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				PaymentMethods paymentMethodsObj = null;
				String paymentMethodsString = null;
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setId(id);
				
				if (status.equalsIgnoreCase(PaymentStatus.InActive.toString())) {
					paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.InActive);		
				}else if (status.equalsIgnoreCase(PaymentStatus.Active.toString())) {
					paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);	
				}
				paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
				paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				return paymentMethodsObj.getStatus().toString();
			} catch (Exception e) {
					GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.CHANGE_PAYMENT_STATUS_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
					GIErrorCode errorCodeObject = gobj.getErrorCode();
				    LOGGER.error("Error_Msg @ changePaymentStatus " + errorCodeObject.getMessage());
				    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			}
		}

		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/info/makePaymentDefault")
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		@ResponseBody
		public String makePaymentDefault(@RequestParam Integer id, @RequestParam Integer existingDefaultId) throws GIException {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				PaymentMethods paymentMethodsObj = null;
				String paymentMethodsString = null;
				boolean is_Payment_Gateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
				if (!is_Payment_Gateway) {
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , existingDefaultId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				} else {
					paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+existingDefaultId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				}
		
				if (paymentMethodsObj != null) {
					PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
					paymentMethodRequestDTO.setId(existingDefaultId);
					paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.N);
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
				}
		
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setId(id);
				paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
				paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
				PaymentMethods paymentMethodsObj2 = getPaymentMethods(paymentMethodsString);
		
				return paymentMethodsObj2.getIsDefault().toString();
			} catch (Exception e) {
				GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.MAKE_PAYMENT_DEFAULT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
				GIErrorCode errorCodeObject = gobj.getErrorCode();
			    LOGGER.error("Error_Msg @ makePaymentDefault " + errorCodeObject.getMessage());
			    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			}
		}

		// validate routing number
		@RequestMapping(value = "/planmgmt/issuer/financial/info/validateroutingnumber", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		@ResponseBody
		public String validatebankinfo(@RequestParam String routingNumber) {
			try {
				String response = "INVALID";
//				BankAccInfo bankaccinfo = bankAccInfoService.getBankAccInfoDetails(routingNumber);
//				if (bankaccinfo != null) {
//					response = "VALID";
//				}
		
				return response;
			} catch (Exception e) {
				giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.VALIDATE_BANK_INFO_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			}
		}

		
		// issuer account financial information dashboard
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/info/list", method = RequestMethod.GET)
		@PreAuthorize("hasPermission(#model, 'VIEW_FINANCIAL_LIST_INFO')")
		public String viewFinancialListInfo(Model model, HttpServletRequest prequest) throws InvalidUserException {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
					Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
		
					// set the default sort column
					prequest.setAttribute(PlanMgmtConstants.DEFAULT_SORT, "financialInfo.bankInfo.updated");
					// get the Pageable object consisting of pagination and sorting
					// enabled properties
					Pageable pageable = issuerService.getPagingAndSorting(model, prequest, null);
					PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
					paymentMethodRequestDTO.setModuleID(issuerObj.getId());
					paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
					paymentMethodRequestDTO.setPageSize(pageable.getPageSize());
					if(pageable.getPageNumber() == 0){
						paymentMethodRequestDTO.setPageNumber(pageable.getPageNumber()+1);
					}else{
						paymentMethodRequestDTO.setPageNumber(pageable.getPageNumber());
					}
					
					String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_MODULEID_MODULENAME_PAGE, paymentMethodRequestDTO, String.class);
					
					XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
				
					PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
					Page<PaymentMethods> paymentMethods = paymentMethodResponse.getPagePaymentMethods();
		
					model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethods.getContent());
					model.addAttribute(PlanMgmtConstants.RESULT_SIZE, paymentMethods.getTotalElements());
					model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
					model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
					return "planmgmt/issuer/account/profile/financial/info/list";
				}
			} catch (Exception ex) {
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_FINANCIAL_INFO_LIST_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
			}
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}

		// issuer account financial information
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/info/{encPaymentId}", method = RequestMethod.GET)
		@PreAuthorize("hasPermission(#model, 'VIEW_FINANCIAL_LIST_INFO')")
		public String issuerAccountProfileFinancialInfo(Model model, @PathVariable("encPaymentId") String encPaymentId) {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			PaymentMethods paymentMethodsObj = null;
			String decryptedPaymentId = null;
			String paymentMethodsString = null;
			try {
				decryptedPaymentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPaymentId);
				Integer paymentId = Integer.parseInt(decryptedPaymentId);
				if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
					Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					boolean is_Payment_Gateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					if (!is_Payment_Gateway) {
						paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , paymentId, String.class);
						paymentMethodsObj = getPaymentMethods(paymentMethodsString);
					} else {
						paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentId, String.class);
						paymentMethodsObj = getPaymentMethods(paymentMethodsString);
					}
					model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, is_Payment_Gateway);
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsObj);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
					return "planmgmt/issuer/account/profile/financial/info";
				}
			} catch (Exception ex) {
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ACCOUNT_PROFILE_FINANCIAL_INFO_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
			}
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}

		// issuer account edit financial information
		/*
		 * @Suneel: Secured the method with permission
		 */

		@RequestMapping(value = "/planmgmt/issuer/account/profile/financial/edit/{encPaymentId}", method = {
				RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize("hasPermission(#model, 'EDIT_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO')")
		public String issuerAccountProfileEditFinancialInfo(Model model, @PathVariable("encPaymentId") String encPaymentId, HttpServletRequest request) throws GIException {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			PaymentMethods paymentMethodsObj = null;
			try {
				String decryptedPaymentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPaymentId);
				Integer paymentId = Integer.parseInt(decryptedPaymentId);
				if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
					Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
					paymentMethodRequestDTO.setModuleID(issuerObj.getId());
					paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
					String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
					Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
					List<PaymentMethods> paymentMethodList = ((List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY));
					PaymentMethods paymentMethod = null;
					if(paymentMethodList != null && !paymentMethodList.isEmpty()){
						paymentMethod = paymentMethodList.get(0);
					}
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, (paymentMethod == null) ? 0 : paymentMethod.getId());
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					boolean is_Payment_Gateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
					if (!is_Payment_Gateway) {
						paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , paymentId, String.class);
						paymentMethodsObj = getPaymentMethods(paymentMethodsString);
					} else {
						paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentId, String.class);
						paymentMethodsObj = getPaymentMethods(paymentMethodsString);
					}
					model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, is_Payment_Gateway);
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.FLOW, PlanMgmtConstants.EDIT);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);

					// Adding condition for for fixing HIX-21190
					if(request.getParameter("update")!=null && request.getParameter("update").toString().trim().equalsIgnoreCase("YES")){
						Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
						if (map != null) {
							PaymentMethods paymentMethodsInfo = (PaymentMethods) map.get(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ);
							paymentMethodsInfo.setId(paymentMethodsObj.getId());
							paymentMethodsInfo.setFinancialInfo(paymentMethodsObj.getFinancialInfo());
							FinancialInfo financialInfo=paymentMethodsObj.getFinancialInfo();
							paymentMethodsInfo.setFinancialInfo(financialInfo);
							paymentMethodsInfo.setStatus(paymentMethodsObj.getStatus());
							paymentMethodsInfo.setIsDefault(paymentMethodsObj.getIsDefault());				
							model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsInfo);
						}
					}else{
						model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsObj);
					}
					model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/account/profile/financial/info/list");
					return "planmgmt/issuer/account/profile/financial/edit";
				}
			} catch (Exception ex) {
				LOGGER.error("Exception in issuerAccountProfile EditFinancialInfo " + ex.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ACCOUNT_PROFILE_EDIT_FINANCIAL_INFO_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
			}
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}

		/*@RequestMapping(value = "/planmgmt/issuer/financial/info", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		public String newIssuerFinancialInfo(Model model) {
			try {
				if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
					Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.STATES_OBJ, new StateHelper().getAllStates());
					model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/financial/info");
					LOGGER.info("New Issuer Financial Page Info");
					return "planmgmt/issuer/financialinfo";
				}
			} catch (Exception e) {
				LOGGER.error("Exception in newIssuerFinancialInfo");
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUER_FINANCIAL_INFO_EXCEPTION.getCode(),
																								null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
			}
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}*/

		// Bug Fix :: HIX-4842 Functional : Duplicacy of Financial Information
		// Account
		@RequestMapping(value = "/planmgmt/issuer/financial/info/exists", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		@ResponseBody
		public String bankAccountNameExists(Model model, @RequestParam(value = "bankAccountName", required = false) String bankAccountName, 
														@RequestParam(value = "bankName", required = false) String bankName) {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			
			try {
				String bankAccountNameExists = "No";
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setPaymentMethodName(bankAccountName);
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
				Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
			 	List<PaymentMethods> paymentsList = ((List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY));
				if (paymentsList != null && !paymentsList.isEmpty()) {
					for (PaymentMethods pm : paymentsList) {
						String bankNameStr = pm.getFinancialInfo().getBankInfo().getBankName().toString();
						if (bankNameStr.equalsIgnoreCase(bankName)) {
							bankAccountNameExists = "Yes";
							break;
						}
					}
				}
				return bankAccountNameExists;
			} catch (Exception e) {
				GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.BANKACCOUNT_EXISTS_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
				GIErrorCode errorCodeObject = gobj.getErrorCode();
			    LOGGER.error("Error_Msg @ bankAccountNameExists " + errorCodeObject.getMessage());
			    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			}
		}

		@RequestMapping(value = "/planmgmt/issuer/financial/info/save", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
		public String saveIssuerFinancialInfo(Model model, @ModelAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ) PaymentMethods paymentMethod, 
				@RequestParam(value = "paymentId", required = false) Integer paymentId, @RequestParam(value = "redirectTo", required = false) String temp_redirectUrl, 
				@RequestParam(value = "chkboxAutoPay", required = false) String chkboxAutoPay, @RequestParam(value = "currentPaymentId", required = false) int currentPaymentId,
				@RequestParam(value = "currentBankAccountNumber", required = false) String currentBankAccountNumber, @RequestParam(value = "accountNumChanged", required = false) String accountNumChanged,
				RedirectAttributes redirectAttrs) {
			String redirectUrl = temp_redirectUrl;
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}
			try {
				if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
					Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
					boolean isFinancialInfoAdded = false;
					BankInfo bankInfo = null;
					FinancialInfo financialInfo = null;
					PaymentMethods paymentMethods = null;
					boolean is_Payment_Gateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
					model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, is_Payment_Gateway);
					String paymentMethodsString = null;
					PaymentMethods paymentMethodsObj = null;
					try {
						// get the PaymentMethods, FinancialInfo, BankInfo which is already exists, if not exists, create one.
						if (!is_Payment_Gateway) {
							if(null != paymentId && paymentId > 0){
								paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , paymentId, String.class);
								paymentMethods = getPaymentMethods(paymentMethodsString);
							}
							
						} else {
							if(null != paymentId && paymentId > 0){
								paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentId, String.class);
								paymentMethods = getPaymentMethods(paymentMethodsString);
								
							}
						}
						if(paymentMethods == null){
							paymentMethods = new PaymentMethods();
						}
						financialInfo = (paymentMethods.getFinancialInfo() != null) ? paymentMethods.getFinancialInfo() : new FinancialInfo();
						bankInfo = (financialInfo.getBankInfo() != null) ? financialInfo.getBankInfo() : new BankInfo();

						// adding the bank related information to the BankInfo object
						if(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber() != null){
						     bankInfo.setAccountNumber(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber());
						}else{
						     bankInfo.setAccountNumber(currentBankAccountNumber); 
						}
						bankInfo.setBankName(paymentMethod.getFinancialInfo().getBankInfo().getBankName());
						bankInfo.setRoutingNumber(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber());
						bankInfo.setAccountType(paymentMethod.getFinancialInfo().getBankInfo().getAccountType());
						bankInfo.setNameOnAccount(paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount());

						//server side validation
						 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
						 Set<ConstraintViolation<BankInfo>> violations = null;
					     if(accountNumChanged.equalsIgnoreCase("true")){
					    	 violations = validator.validate(bankInfo, BankInfo.EditPaymentMethodBankInfo.class);
					     }else{
					    	 violations = validator.validate(bankInfo, BankInfo.EditPaymentMethod.class); 
					     }
						 if(violations != null && !violations.isEmpty()){
							//throw exception in case client side validation breached
							 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
							 }
						 
						// adding the financial information to the FinancialInfo object
						if (financialInfo.getPaymentType() == null) {
							financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.BANK);
						}

						// setting the BankInfo object to the FinancialInfo object
						financialInfo.setBankInfo(bankInfo);

						/*	Information of issuer for Financial Information start here */
						financialInfo.setAddress1(issuerObj.getAddressLine1());
						financialInfo.setAddress2(issuerObj.getAddressLine2());
						financialInfo.setCity(issuerObj.getCity());
						financialInfo.setState(issuerObj.getState());
						financialInfo.setZipcode(issuerObj.getZip());
						financialInfo.setEmail(issuerObj.getEmailAddress());

						Location location=new Location();
						location.setAddress1(issuerObj.getAddressLine1());
						location.setAddress2(issuerObj.getAddressLine2());
						location.setCity(issuerObj.getCity());
						location.setState(issuerObj.getState());
						location.setZip(issuerObj.getZip());
						financialInfo.setLocation(location);
						
						/*IssuerRepresentative issuerRepresentative = issuerService.getIssuerRepresentativeForIssuer(issuerObj);

						if (issuerRepresentative != null) {
							LOGGER.info("issuerRepresentative: " + issuerRepresentative.getId());
							financialInfo.setFirstName(issuerRepresentative.getFirstName());
							financialInfo.setLastName(issuerRepresentative.getLastName());
							financialInfo.setContactNumber(issuerRepresentative.getPhone());
						}else{
							throw new GIException(201,"No primary contact found","HIGH");
						}
*/
						/*	Information of issuer for Financial Information end here */
						
						// setting the FinancialInfo object to the PaymentMethods object
						paymentMethods.setFinancialInfo(financialInfo);
						paymentMethods.setPaymentMethodName(paymentMethod.getPaymentMethodName());

						//server side validation
						 Validator validatorPaymentMethod = Validation.buildDefaultValidatorFactory().getValidator();
						 Set<ConstraintViolation<PaymentMethods>> violationsPaymentMethod = validatorPaymentMethod.validate(paymentMethods, PaymentMethods.EditPaymentMethod.class);
						 if(violationsPaymentMethod != null && !violationsPaymentMethod.isEmpty()){
							//throw exception in case client side validation breached
							 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
							 }
						
						// check whether the paymentType, Status already exists, if not,
						// assign the defaults
						if (paymentMethods.getPaymentType() == null) {
							paymentMethods.setPaymentType(PaymentType.BANK);
						}

						// when issuer add payment method for first time, make that
						// payment method as default one
						if (currentPaymentId == 0) {
							paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
							paymentMethods.setStatus(PaymentStatus.Active);
						} else {
							// when issuer have more than one payment method, if check
							// box checked
							if (chkboxAutoPay != null) {
								PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
								paymentMethodRequestDTO.setModuleID(issuerObj.getId());
								paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
								paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
								String defaultPaymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
								Map<String, Object> defaultPaymentMethods = getPaymentMethodsMap(defaultPaymentMethodsString);
								List<PaymentMethods> defaultPaymentMethodList = (List<PaymentMethods>)defaultPaymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
								if(defaultPaymentMethodList != null && !defaultPaymentMethodList.isEmpty()){
									for (PaymentMethods defaultPaymentMethod: defaultPaymentMethodList){
										paymentMethodRequestDTO = new PaymentMethodRequestDTO();
										paymentMethodRequestDTO.setId(defaultPaymentMethod.getId());
										paymentMethodRequestDTO.setModuleName(defaultPaymentMethod.getModuleName());
										paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.N);
										String resultString  = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
										if(LOGGER.isDebugEnabled()){
											LOGGER.debug("Modified Financial Information "+resultString);
										}	
									}
									
								}
								paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
							} else if (paymentMethods.getIsDefault() == null) {
								paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.N);
							}

							if (paymentMethods.getStatus() == null) {
								paymentMethods.setStatus(PaymentStatus.Active);
							}
						}

						paymentMethods.setModuleId(issuerObj.getId());
						paymentMethods.setModuleName(PaymentMethods.ModuleName.ISSUER);

						// saving the PaymentMethods info into the payment_methods table
						paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SAVE_PAYMENT_METHOD, paymentMethods, String.class);
						XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
					
						PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
						paymentMethodsObj = paymentMethodResponse.getPaymentMethods();
						
						isFinancialInfoAdded = (paymentMethodsObj != null) ? true	: false;
						if(isFinancialInfoAdded){
							// send email notification to Issuer rep only for ID state exchange
							if(stateCode.equalsIgnoreCase("ID")){
								issuerService.sendFinancialNotificationToIssuerRep(issuerObj);
							}	
						}else{
							if (paymentId != null) {
								redirectUrl = "redirect:/planmgmt/issuer/account/profile/financial/edit/" + paymentId+"?update=YES";
								redirectAttrs.addFlashAttribute("payment_error", paymentMethodResponse.getErrMsg());
								redirectAttrs.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
							} else {
								redirectUrl = "redirect:/planmgmt/issuer/account/profile/financial/add";
								redirectAttrs.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
								redirectAttrs.addFlashAttribute("payment_error", paymentMethodResponse.getErrMsg());
							}
												
						}
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Financial Information is saved");
						}	

					} catch (Exception ex) {
						LOGGER.error("Error in financial Info: ", ex);
						if (paymentId != null) {
							redirectUrl = "redirect:/planmgmt/issuer/account/profile/financial/edit/" + paymentId+"?update=YES";
							redirectAttrs.addFlashAttribute(PlanMgmtConstants.ROUTING_NUMBER, paymentMethods.getFinancialInfo().getBankInfo().getRoutingNumber());
							redirectAttrs.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
						} else {
							redirectUrl = "redirect:/planmgmt/issuer/account/profile/financial/add";
							redirectAttrs.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
						}
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.INVALID_ROUTING_NUMBER, ex.getLocalizedMessage());
					}

					redirectAttrs.addFlashAttribute("isFinancialInfoAdded", isFinancialInfoAdded);
					return redirectUrl;
				}
				} catch (Exception e) {
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.FINANCIAL_INFO_EXCEPTION.getCode(),
								null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
				}			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		}

		
		
		private  Map<String, Object> getPaymentMethodsMap(String paymentMethodsString){
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			
			PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
			return paymentMethodResponse.getPaymentMethodMap();
		}
		
		
		private  PaymentMethods getPaymentMethods(String paymentMethodsString){
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		
			PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
			return paymentMethodResponse.getPaymentMethods();
		}
}
