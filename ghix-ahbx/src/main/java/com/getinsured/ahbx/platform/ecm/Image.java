
package com.getinsured.ahbx.platform.ecm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for image complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="image">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ecm.ahbx.accenture.com/}content">
 *       &lt;sequence>
 *         &lt;element name="bitDepth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="imageStream" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="resolution" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "image", propOrder = {
    "bitDepth",
    "height",
    "imageStream",
    "resolution",
    "width"
})
public class Image
    extends Content
{

    protected int bitDepth;
    protected int height;
    protected byte[] imageStream;
    protected int resolution;
    protected int width;

    /**
     * Gets the value of the bitDepth property.
     * 
     */
    public int getBitDepth() {
        return bitDepth;
    }

    /**
     * Sets the value of the bitDepth property.
     * 
     */
    public void setBitDepth(int value) {
        this.bitDepth = value;
    }

    /**
     * Gets the value of the height property.
     * 
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     */
    public void setHeight(int value) {
        this.height = value;
    }

    /**
     * Gets the value of the imageStream property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImageStream() {
        return imageStream;
    }

    /**
     * Sets the value of the imageStream property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImageStream(byte[] value) {
        this.imageStream = ((byte[]) value);
    }

    /**
     * Gets the value of the resolution property.
     * 
     */
    public int getResolution() {
        return resolution;
    }

    /**
     * Sets the value of the resolution property.
     * 
     */
    public void setResolution(int value) {
        this.resolution = value;
    }

    /**
     * Gets the value of the width property.
     * 
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     */
    public void setWidth(int value) {
        this.width = value;
    }

}
