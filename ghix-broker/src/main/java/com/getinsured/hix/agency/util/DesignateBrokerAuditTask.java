package com.getinsured.hix.agency.util;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.agency.repository.IDesignateBrokerAudRepository;
import com.getinsured.hix.agency.repository.IRevisionInfoRepository;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.agency.DesignateBrokerAud;
import com.getinsured.hix.model.agency.RevisionInfo;

@Component
public class DesignateBrokerAuditTask {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(DesignateBrokerAuditTask.class);
	
	@Autowired
	private DesignateService designateSevice;
	@Autowired
	private IRevisionInfoRepository revisionInfoRepository;
	@Autowired
	private IDesignateBrokerAudRepository designateBrokerAudRepository;
	
	@Async
	public void processAudit(int brokerId, AccountUser user){
		int count = 0;
		LOGGER.info("Designate Broker Audit : Starts : target broker ID : "+brokerId);
		long startTime = TimeShifterUtil.currentTimeMillis();
		List<DesignateBroker> designateBrokers = designateSevice.findByBrokerIdAndUpdatedTime(brokerId, getInterval(5));
		
		if(designateBrokers!=null && designateBrokers.size() > 0){
			LOGGER.info("Total no. of records : "+designateBrokers.size());
			for(DesignateBroker designateBroker:designateBrokers){
				try {
					auditDesignateBroker(designateBroker, user);
					count++;
				} catch(Exception exception){
					LOGGER.error("Exception occured while auditing designate broker in bulk transfer.",exception);
				}
			}
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("No. of records successfully executed : "+count+" : Total Execution TIme : "+ (endTime-startTime));
		
		LOGGER.info("Designate Broker Audit : Ends ");
	}
	
	private Date getInterval(int interval){
		Calendar cal = TSCalendar.getInstance();
		cal.add(Calendar.MINUTE, -interval);
		return cal.getTime();
	}
	
	@Transactional
	private void auditDesignateBroker(DesignateBroker designateBroker, AccountUser user){
		DesignateBrokerAud aud = new DesignateBrokerAud();
		aud.setId(Long.valueOf(designateBroker.getId()));
		aud.setRev(getRevision());
		aud.setRevType(1);
		aud.setBrokerId(designateBroker.getBrokerId());
		aud.setStatus(designateBroker.getStatus());
		aud.setCreated(new TSDate());
		aud.setCreatedBy(user.getId());
		aud.setUpdated(designateBroker.getUpdated());
		aud.setUpdatedBy(designateBroker.getUpdatedBy());
		aud.setEsignDate(designateBroker.getEsignDate());
		aud.setEsignBy(designateBroker.getEsignBy());
		aud.setExternalIndividualId(designateBroker.getExternalIndividualId());
		
		designateBrokerAudRepository.save(aud);
	}
	
	private long getRevision(){
		RevisionInfo newRevisionInfo = new RevisionInfo();
		newRevisionInfo.setRevisionTimestamp(TimeShifterUtil.currentTimeMillis());
		RevisionInfo updatedRevisionInfo = revisionInfoRepository.save(newRevisionInfo);
		return updatedRevisionInfo.getId();
	}

}
