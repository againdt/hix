package com.getinsured.eligibility.at.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;

@XmlRootElement(name = "AccountTransferResponseDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountTransferResponseDTO {

	@XmlElement(name = "AccountTransferResponsePayloadType", required = true)
    private AccountTransferResponsePayloadType accountTransferResponsePayloadType;

	@XmlElement(name = "GiWsPayloadId", required = true)
	private Integer giWsPayloadId;
	
	@XmlElement(name = "GiwsPayload", required = true)
	private GIWSPayload giwsPayload;

	@XmlElement(name = "x-user", required = false)
	private String user;
	
	@XmlElement(name = "x-user-offset", required = false)
	private String TSoffset;
	
	@XmlElement(name = "atSpanInfoId", required = false)
	private long atSpanInfoId;
	
	public AccountTransferResponsePayloadType getAccountTransferResponsePayloadType() {
		return accountTransferResponsePayloadType;
	}

	public void setAccountTransferResponsePayloadType(
			AccountTransferResponsePayloadType accountTransferResponsePayloadType) {
		this.accountTransferResponsePayloadType = accountTransferResponsePayloadType;
	}

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public GIWSPayload getGiwsPayload() {
		return giwsPayload;
	}

	public void setGiwsPayload(GIWSPayload giwsPayload) {
		this.giwsPayload = giwsPayload;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTSoffset() {
		return TSoffset;
	}

	public void setTSoffset(String tSoffset) {
		TSoffset = tSoffset;
	}
	
	public long getAtSpanInfoId() {
		return atSpanInfoId;
	}

	public void setAtSpanInfoId(long atSpanInfoId) {
		this.atSpanInfoId = atSpanInfoId;
	}

}
