
package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ridp.fars.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.ridp.fars.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for the Response elements.
 * 
 * <p>Java class for ResponsePayloadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponsePayloadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}ResponseMetadata"/>
 *         &lt;element ref="{http://ridp.dsh.cms.gov/extension/1.0}FinalDecisionCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsePayloadType", propOrder = {
    "responseMetadata",
    "finalDecisionCode"
})
public class ResponsePayloadType
    extends ComplexObjectType
{

    @XmlElement(name = "ResponseMetadata", namespace = "http://hix.cms.gov/0.1/hix-core", required = true)
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "FinalDecisionCode")
    protected FinalDecisionCodeSimpleType finalDecisionCode;

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the finalDecisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link FinalDecisionCodeSimpleType }
     *     
     */
    public FinalDecisionCodeSimpleType getFinalDecisionCode() {
        return finalDecisionCode;
    }

    /**
     * Sets the value of the finalDecisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalDecisionCodeSimpleType }
     *     
     */
    public void setFinalDecisionCode(FinalDecisionCodeSimpleType value) {
        this.finalDecisionCode = value;
    }

}
