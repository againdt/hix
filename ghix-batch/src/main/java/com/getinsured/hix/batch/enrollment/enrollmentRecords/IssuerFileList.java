package com.getinsured.hix.batch.enrollment.enrollmentRecords;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileDeleteStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("issuerFileList")
public class IssuerFileList {
	
	List<String> issuerXmlFiles = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerFileList.class);
	public IssuerFileList(){
		issuerXmlFiles = new ArrayList<String>(); 
	}
	
	public synchronized void add(String outputPath) {
		issuerXmlFiles.add(outputPath);
	}
	public synchronized void clearFiles() {
		issuerXmlFiles= new ArrayList<>();
	}
	
	public synchronized void add(List<String> outputPath) {
		issuerXmlFiles.addAll(outputPath);
	}

	public   void deleteAllFiles(){
	for (String fileName : issuerXmlFiles){
		File tempFile = null;
		try{
			tempFile=new File(fileName);
			if(tempFile != null){
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("ENROLLMENT_834:: Removing Orphan temp file "+fileName);
				}
				FileDeleteStrategy.FORCE.delete(tempFile);
			}
		}catch(Exception e){
			LOGGER.error("Error occurred in Outbound 834 batch job while deleting orphan temp file "+fileName, e);
		}
	}
}
	
	public void renameAllFiles(){
		for (String fileName : issuerXmlFiles){
			File tempFile = new File(fileName);
			if(tempFile != null){
				// Rename back from .tmp to .xml So that files get picked up for EDI.
				String newXmlFileName = tempFile.getAbsolutePath().replaceAll(".tmp", ".xml");
				File newXmlFile = new File(newXmlFileName);
				tempFile.renameTo(newXmlFile);
			}
		}
	}
}
