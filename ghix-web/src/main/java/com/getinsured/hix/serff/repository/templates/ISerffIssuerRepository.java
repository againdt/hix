package com.getinsured.hix.serff.repository.templates;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;

public interface ISerffIssuerRepository extends JpaRepository<Issuer, Integer>, RevisionRepository<Issuer, Integer, Integer> {
	//Issuer findByUser(AccountUser user);

	@Query("SELECT issuer.id, issuer.name FROM Issuer issuer order by UPPER(issuer.name)")
	List<Object[]> getAllIssuerNames();

	@Query("SELECT issuer.id, issuer.name FROM Issuer issuer WHERE (issuer.certificationStatus='CERTIFIED' OR issuer.certificationStatus='RECERTIFIED') order by UPPER(issuer.name)")
	List<Object[]> findAllCertifiedIssuerNames();

	@Query("SELECT i FROM Issuer i WHERE (i.name = :issuerName) AND (i.licenseNumber = :issuerLicenseNumber) AND (i.state = :issuerState)")
	Issuer findIssuer(@Param("issuerName") String issuerName, @Param("issuerLicenseNumber") String issuerLicenseNumber, @Param("issuerState") String issuerState);

	@Query("SELECT i FROM Issuer i WHERE (i.creationTimestamp BETWEEN :startdatetime AND :enddatetime) OR (i.lastUpdateTimestamp between :startdatetime AND :enddatetime) ORDER BY i.id")
	List<Issuer> getAddedOrUpdatedIssuers(@Param("startdatetime") Date startdatetime, @Param("enddatetime") Date enddatetime);

	@Query("SELECT ir.issuer FROM IssuerRepresentative ir WHERE ir.userRecord.id = :userId")
	Issuer getIssuerByRepUserId(@Param("userId") Integer userId);
	
	/*//@Query("SELECT u FROM AccountUser u , Issuer i, ModuleUser mu, UserRole ur, Role r where i.id=:issuerId AND r.name=:roleName AND mu.user.id=u.id AND mu.moduleId=i.id AND mu.user.id=ur.user.id AND ur.role.id=r.id")
	@Query("SELECT U FROM AccountUser U , Issuer I, ModuleUser MU, UserRole UR, Role R, IssuerRepresentative IR " +
			"WHERE IR.issuer.id = :issuerId AND IR.issuer.id = I.id " +
			"AND R.name = :roleName AND R.id = UR.role.id AND UR.user.id = MU.user.id AND MU.user.id = U.id AND U.id = IR.issuer.id")
	List<AccountUser> getRepListForIssuer(@Param("issuerId") Integer issuerId, @Param("roleName") String roleName);*/
	
	@Query("SELECT u FROM AccountUser u , " +
            "Issuer i, " +
            "IssuerRepresentative ir, " +
            "ModuleUser mu, " +
            "UserRole ur, " +
            "Role r " +
           " where " +
	         "mu.user.id=u.id AND " +
	         "mu.moduleId=i.id AND " +
	         "mu.user.id=ur.user.id AND " +
	         "u.id=ir.userRecord.id AND " +
	         "i.id=ir.issuer.id AND " +
	         "ur.role.id=r.id AND " +
	         "mu.moduleName=:moduleName AND " +
	         "r.name=:roleName AND " +
	         "i.id=:issuerId AND " +
	         "((lower(u.firstName) like '%'|| lower(:name) || '%') OR (lower(u.lastName) like '%'|| lower(:name) || '%')) AND " +
	         "(((LOWER(:title) IS NOT NULL) AND (LOWER(u.title) like '%'|| lower(:title) || '%')) OR (((LOWER(:title) IS NULL) AND ((u.title IS NULL) OR (u.title IS NOT NULL))))) AND " +
	         "u.confirmed in (:status)")
	Page<AccountUser> getRepListForIssuer(@Param("moduleName") String moduleName, @Param("roleName") String roleName,  @Param("issuerId") Integer issuerId, @Param("name") String name, @Param("title") String title, @Param("status") List<Integer> status, Pageable pageable);
	
	@Query("SELECT i FROM Issuer i where (((LOWER(:status) IS NOT NULL) AND (LOWER(i.certificationStatus)=LOWER(:status))) OR ((LOWER(:status) IS NULL) AND ((i.certificationStatus IS NULL) OR (i.certificationStatus IS NOT NULL)))) AND (((LOWER(:name) IS NOT NULL) AND (LOWER(i.name) like '%'|| lower(:name) || '%')) OR (((LOWER(:name) IS NULL) AND ((i.name IS NULL) OR (i.name IS NOT NULL)))))")
	Page<Issuer> getAllIssuers(@Param("name") String name, @Param("status") String status, Pageable pageable);
	
	@Query("SELECT i FROM Issuer i WHERE (i.hiosIssuerId = :hiosIssuerID)")
	Issuer getIssuerByHiosID(@Param("hiosIssuerID") String hiosIssuerID);
	
	@Query("SELECT i FROM Issuer i WHERE i.name = :issuerName")
	Issuer getIssuerByName(@Param("issuerName") String issuerName);
	
	@Query("SELECT i FROM Issuer i WHERE (i.hiosIssuerId = :hiosIssuerID)")
	List<Issuer> getIssuerFromHiosID(@Param("hiosIssuerID") String hiosIssuerID);
	
	@Query("SELECT new Issuer(issuer.id, issuer.name, issuer.hiosIssuerId, issuer.shortName) from Issuer as issuer")
	List<Issuer> getIssuerNameList();
}
