package com.getinsured.hix.indportal;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.client.ClientProtocolException;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.getinsured.hix.admin.util.AnnouncementDTO;
import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.cap.util.CapPortalConfigurationUtil;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.endpoints.annotation.EndpointDescription;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.AppGroupResponse.GroupType;
import com.getinsured.hix.indportal.dto.ApplicationEligibilityDetails;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.CustomGroupingResponse;
import com.getinsured.hix.indportal.dto.EnrollmentHistory;
import com.getinsured.hix.indportal.dto.EnrollmentStatusUpdate;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.indportal.dto.IndAdditionalDetails;
import com.getinsured.hix.indportal.dto.IndAppeal;
import com.getinsured.hix.indportal.dto.IndPortalAddlnRegDetails;
import com.getinsured.hix.indportal.dto.IndPortalApplicantDetails;
import com.getinsured.hix.indportal.dto.IndPortalContactUs;
import com.getinsured.hix.indportal.dto.IndPortalEligibilityDateDetails;
import com.getinsured.hix.indportal.dto.IndPortalSpecialEnrollDetails;
import com.getinsured.hix.indportal.dto.IndPortalValidationDTO;
import com.getinsured.hix.indportal.dto.Member;
import com.getinsured.hix.indportal.dto.MyAppealDetails;
import com.getinsured.hix.indportal.dto.MyApplications;
import com.getinsured.hix.indportal.dto.OTR;
import com.getinsured.hix.indportal.dto.OverrideComment;
import com.getinsured.hix.indportal.dto.PendingReferralDetails;
import com.getinsured.hix.indportal.dto.PlanDetails;
import com.getinsured.hix.indportal.dto.PlanSummaryDetails;
import com.getinsured.hix.indportal.dto.PlanTile;
import com.getinsured.hix.indportal.dto.PlanTileRequest;
import com.getinsured.hix.indportal.dto.ReverseSepQepDenial;
import com.getinsured.hix.indportal.dto.SEPOverrideDTO;
import com.getinsured.hix.indportal.dto.SEPOverrideSEPDatesDTO;
import com.getinsured.hix.indportal.dto.SEPOverrideSEPEventsDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardAnnouncementDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardConfigurationDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardEnrollmentDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardPreEligibilityDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardTabDTO;
import com.getinsured.hix.indportal.enums.DisenrollReasonEnum;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.estimator.mini.PreEligibilityResults;
import com.getinsured.hix.model.ssap.MultiHousehold;
import com.getinsured.hix.model.ssap.MultiHouseholdResponse;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.SecurityUtils;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixGateWayService;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.tokens.GiBinaryToken;
import com.getinsured.hix.platform.security.tokens.SecureTokenFactory;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.ssap.SsapConstants;
import com.getinsured.hix.ws_trust.saml.GhixSecureToken;
import com.getinsured.hix.ws_trust.saml.SAMLTokenValidator;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.dto.PortalResponse;
import com.getinsured.iex.dto.SsapApplicantDto;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.household.service.RelationshipTypeService;
import com.getinsured.iex.indportal.dto.IndDisenroll;
import com.getinsured.iex.indportal.dto.ReinstateEnrollmentDTO;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * Controller for all endpoints used in Individual Portal
 * 
 * @author desu_s
 *
 */
@Controller
public class IndividualPortalController extends IndividualPortalUtility{

	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalController.class);
	
	@Autowired
	private CommentTargetService commentTargetService;
	@Autowired
	private CSROverrideUtility csrOverrideUtility;
	@Autowired
	private LookupService lookUpService;
	@Autowired
	private UserService userService;
	@Autowired
	private AppEventService appEventService;
	@Autowired
	private ConsumerEventsUtil consumerEventsUtil;
	@Autowired
	private IndividualPortalEnrollmentUtility portalEnrollmentUtility;
	
	@Autowired
	private DesignateService designateService;
	@Autowired
	private IBrokerRepository brokerRepository;
	@Autowired
	private GhixPlatformConstants platformConfig;
	
	@Autowired private IHouseholdRepository householdRepository;
	@Autowired private Gson platformGson;
    
	@Autowired private CapPortalConfigurationUtil capPortalConfigurationUtil;
	@Autowired private GhixGateWayService ghixGateWayService;
	@Autowired private RoleService roleService;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	@Autowired private ContentManagementService ecmService;
	
	@Autowired private RelationshipTypeService relationshipTypeService;
	
	private static boolean isSSOEnabled = System.getProperty("GHIX_SECNTX_TYPE").equalsIgnoreCase("saml");
	private String tokenValidationKey = null;
	
	@Value("#{configProp['saml.token.validation_key'] != null ? configProp['saml.token.validation_key'] : 'NOT AVAILABLE'}")
	public void setTokenValidationKey(String key){
		tokenValidationKey = key.trim();
	}
	
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	public static final String UNSECURE_CHARS_REGEX = "[\n\r]";
	private static final String ERROR_MSG = "errorMsg";
	private static final String UPLOAD_FAILURE = "Upload_Failure";
	
	@RequestMapping(value = "/indportal/showDashboard", method = RequestMethod.GET)
	public String showDashboard(@RequestParam("refId") String refId,
			HttpServletRequest request) {
		LOGGER.info("Processing request for dashboard");
		 String userDefLandingPage = null;
		if(refId == null || tokenValidationKey.equalsIgnoreCase("NOT AVAILABLE")) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Redirect Reference or the token key is not available using defaults RefId:{}",refId);
				}
			return "redirect:"+platformConfig.getIDPInitiatedSSO(refId);
		}
		AccountUser user = SecurityUtils.getLoggedInUser();
		if(user == null || user.getUsername().equalsIgnoreCase("Anonymous")) {
			String ssoUrl = null;
			if(isSSOEnabled) {
				ssoUrl =platformConfig.getIDPInitiatedSSO(refId);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Redirecting to IAM {}",ssoUrl);
				}
				return "redirect:"+ssoUrl;
			}else {
				return "redirect:/account/user/login?refId="+refId;
			}
		}else {
			// existing authenticated session
			try {
				GiBinaryToken token = SecureTokenFactory.processBinaryToken(refId, this.tokenValidationKey);
				Map<String, String> parameters = token.getAllParameters();
				// Do something with this and forward to the target URL, do not use the loginContext in the user object
				user.setLoginContext(parameters);
				// Get the Target page and use it is as page context
				String targetPage=(parameters!=null && parameters.get(this.getConsumerUtil().TARGET_PAGE)!=null)?(String)parameters.get(this.getConsumerUtil().TARGET_PAGE):"";
				String targetConsumerId=(parameters!=null && parameters.get(this.getConsumerUtil().TARGET_CONSUMER_ID)!=null)?(String)parameters.get(this.getConsumerUtil().TARGET_CONSUMER_ID):"";
				
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info(" target page in the param map {}",targetPage);
					LOGGER.info(" target targetConsumerId in the param map {}",targetConsumerId);
					
				}
				/* Impersonate as INDIVIDUAL if the logged in user is  of role 'EXTERNAL_ASSISTER'
				 * 
				 * Or If the user's default role is EXTERNAL_ASSISTER - This is to handle the subsequent visits with the same session with a different target Consumer
				 *  */
				if(StringUtils.equalsIgnoreCase(user.getDefRole().getName(), "EXTERNAL_ASSISTER") || 
					StringUtils.equalsIgnoreCase(user.getActiveModuleName(), "EXTERNAL_ASSISTER"))
				{
			
					Role indivRole = roleService.findRoleByName(StringUtils.upperCase("INDIVIDUAL"));
					userDefLandingPage = indivRole.getLandingPage();
					
					
					request.getSession().setAttribute("userActiveRoleName", "INDIVIDUAL".intern());
				    request.getSession().setAttribute("switchToModuleName", "INDIVIDUAL".intern());
				    
					this.getConsumerUtil().handleExternalAssister(user,parameters);
					if(user.getLinkedModuleIds()!=null && user.getLinkedModuleIds().size()>1)
					{
						user.setActiveModuleId(0);
					}
				
				}
				
				else
				{
					if(StringUtils.equalsIgnoreCase(
						user.getActiveModuleName(), "INDIVIDUAL".intern()))
					{
					Role role = userService.getDefaultRole(user);
					//userDefLandingPage = indivRole.getLandingPage();
					
					userDefLandingPage=role.getLandingPage();
					
					List<ModuleUser> userDefModules = userService.getUserSelfSignedModules(user.getActiveModuleName() ,user.getId());
					if(userDefModules !=null && userDefModules.size()>0 ){
						
						user.setActiveModuleName(role.getName().toLowerCase());
						
						// Set the Linked active moduleId's to the user.
						List<Integer> linkedModuleIds = 
								userDefModules.stream()
							              .map(ModuleUser::getModuleId)
							              .collect(Collectors.toList());
						user.setActiveModuleId(linkedModuleIds.get(0));
						user.setLinkedModuleIds(linkedModuleIds);
						if(user.getLinkedModuleIds()!=null && user.getLinkedModuleIds().size()>1)
						{
							user.setActiveModuleId(0);
						}
					}
					else
					{
						user.setActiveModuleId(0);
						//TODO Check: What should be done if the user is provisioned but there was not AT- Household in our system ??? 
					}	

					
					}
				}
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Resolved landing page {} for {}",userDefLandingPage,user.getActiveModuleName());
				 }
				
					  if(StringUtils.isNotBlank(targetPage) && StringUtils.isNotBlank(userDefLandingPage)) {
					   userDefLandingPage += userDefLandingPage.indexOf("?") >= 0 ? "&pageContext=" + targetPage
							: "?pageContext=" + targetPage;
					  }	
				LOGGER.info("*************** Complete this implementation, context received *************");
				LOGGER.info("Forward parameters {}",parameters.toString());
				
			} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
					| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
					| IOException e) {
				// redirect to user's default landing page, if none
			}
		}
		 return "redirect:"+ userDefLandingPage; 
	}
    /**
     * @param model
     * @param request
     * @param response
     */
    @RequestMapping(value = "/individual/saml/idalinkredirect", method = RequestMethod.GET)
    //@PreAuthorize(IndividualPortalConstants.PORTAL_VIEW_AND_PRINT_PERMISSION)
    public void secureYhiRedirect(HttpServletRequest request, HttpServletResponse response) {
        boolean useStaticUrl = false;
        String idalinkAppendToken = IndividualPortalConstants.NO;
        StringBuilder redirectUrl = null;
        AccountUser accountUser = null;
        
        try {
            idalinkAppendToken = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IDALINK_APPEND_TOKEN);
            
            if(null == idalinkAppendToken || idalinkAppendToken.isEmpty()) {
                idalinkAppendToken = IndividualPortalConstants.NO;
            }
            
            accountUser = userService.getLoggedInUser();
            
            if(null != accountUser) {
                String defaultRole = accountUser.getDefRole().getName();
                if(null != defaultRole && !StringUtils.isEmpty(defaultRole) && !"INDIVIDUAL".equalsIgnoreCase(defaultRole)) {
                    useStaticUrl = true;
                }
            }
            
            if(null == accountUser || IndividualPortalConstants.NO.equalsIgnoreCase(idalinkAppendToken)) {
                useStaticUrl = true; 
            } 
            
            redirectUrl = new StringBuilder(
                        DynamicPropertiesUtil
                                .getPropertyValue(IEXConfiguration.IEXConfigurationEnum.FINANCIAL_SSAP_URL));
            if(!useStaticUrl) {
                if(redirectUrl.indexOf(IndividualPortalConstants.QUERY_START) > 0) {
                    redirectUrl.append(IndividualPortalConstants.QUERY_PARAM_SEPERATOR);
                } else {
                    redirectUrl.append(IndividualPortalConstants.QUERY_START);
                }
                redirectUrl.append(SAMLTokenValidator.generateToken(
                        GhixPlatformConstants.TOKEN_VALIDATION_KEY,
                        GhixPlatformConstants.SAML_ALIAS).toString());
            }
            request.getSession().invalidate();        // Invalidate the user session as user is going out of YHI    
            response.sendRedirect(redirectUrl.toString());
        } catch (Exception ex) {
            LOGGER.error("ERR: WHILE REDIRECTING TO YHI: ", ex);
        }

    }

    @RequestMapping(value="/setPrefReviewed", method=RequestMethod.GET)
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_VIEW_AND_PRINT')")
    public @ResponseBody String setPrefReviewed(Model model, HttpServletRequest httpServletRequest) throws GIException, InvalidUserException{
    		AccountUser user  = userService.getLoggedInUser();
        Household household = null;
        if(null != user){
            int activeModuleId = user.getActiveModuleId();
            LOGGER.debug("Retrieving information for activeModuleId:"+ activeModuleId);
            household = this.getConsumerUtil().getHouseholdRecord(activeModuleId);
            if(null == household){
                LOGGER.debug("Household retrieved is having a null value");
            }
            else{
            		household.setPreferencesReviewDate(new TSDate());
    	    			try {
    	    				this.getConsumerUtil().saveHouseholdRecord(household);
    	    			} catch (Exception e) {
    	    				LOGGER.error("Error saving household", e);
    	    				return "Error saving household";
    	    			} 
            }
        		return "SUCCESS";
        }
        else{
            return "User is un-authorized";
        }
    }

    @RequestMapping(value="/indportal/multiHouseholdDetails", method = RequestMethod.GET)
    public @ResponseBody String getMultiHouseholdPage(Model model, HttpServletRequest httpServletRequest) throws GIException, InvalidUserException{
        LOGGER.debug("getMultiHouseholdPage::In getMultiHouseholdPage");
        //LOGGER.info("data: " + data);
        AccountUser user  = userService.getLoggedInUser();
        MultiHouseholdResponse response = new MultiHouseholdResponse();

        List<Integer> householdIds = user.getLinkedModuleIds();

        try {
            if(householdIds != null && householdIds.size() > 0) {
                List<MultiHousehold> households = platformGson.fromJson(ghixRestTemplate.postForObject(GhixEndPoints.SsapEndPoints.SSAP_GET_MULTIHOUSEHOLD_MEMBERS_URL, householdIds, String.class), List.class);

                if(households != null && households.size() > 0) {
                    LOGGER.debug("getMultiHouseholdPage::householdMembers = " + households.toString());
                } else {
                    LOGGER.debug("getMultiHouseholdPage::household was null or empty");
                }

                response.setHouseholds(households);
            }
        } catch(Exception ex) {
            LOGGER.error("getMultiHouseholdPage::error getting households", ex);
        }

        return platformGson.toJson(response);
    }

    @RequestMapping(value="/indportal/activeHousehold", method = RequestMethod.POST)
    public @ResponseBody String setActiveHousehold(@RequestBody String householdId, Model model, HttpServletRequest httpServletRequest) throws GIException, InvalidUserException{
        LOGGER.debug("setActiveHousehold::household ID to set = " + householdId);
        if(StringUtils.isNotBlank(householdId)) {
            AccountUser user = userService.getLoggedInUser();
            user.setActiveModuleId(Integer.parseInt(householdId));
            LOGGER.debug("setActiveHousehold::new active module ID = " + user.getActiveModuleId());
            return "Success";
        } else {
            return "Failure";
        }
    }

    @RequestMapping(value="/indportal/multiHousehold", method = RequestMethod.GET)
    public @ResponseBody boolean isMultiHousehold(Model model, HttpServletRequest httpServletRequest) throws GIException, InvalidUserException{
        try {
            AccountUser user  = userService.getLoggedInUser();
            List<Integer> householdIds = user.getLinkedModuleIds();
            if(householdIds != null && householdIds.size() > 1) {
                LOGGER.debug("isMultiHousehold::has more than one household");
                return true;
            }
        } catch(Exception ex) {
            LOGGER.error("getMultiHouseholdPage::error getting households", ex);
        }

        LOGGER.debug("isMultiHousehold::has one household");
        return false;
    }

    @RequestMapping(value = "/indportal/consent", method = RequestMethod.GET)
    @GiAudit(transactionName = "Get Dashboard for Individual", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_VIEW_AND_PRINT') || hasPermission(#model, 'approvedadminstaffl1', 'IND_PORTAL_MY_ENROLLMENTS') || hasPermission(#model, 'approvedadminstaffl2', 'IND_PORTAL_MY_ENROLLMENTS')")
    @EndpointDescription(description="Endpoint to load Individual Portal home page")
    public String showConsent(HttpServletRequest request, Model model) throws GIException {
    	AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			return "redirect:/account/user/login";
		}

		if(null == user){
			model.addAttribute("error", "No User");
			return "iex/indportal/errorPage";
		}
		int individualId = user.getActiveModuleId();
		LOGGER.debug("getIndividualPortalPage::active module id: " + individualId);
		Household household = this.getConsumerUtil().getHouseholdRecord(individualId);
		if(household == null){
			model.addAttribute("error", "No Household");
			return "iex/indportal/errorPage";
		}
		
		model.addAttribute(IndividualPortalConstants.FIRST_NAME,household.getFirstName());
		model.addAttribute(IndividualPortalConstants.LAST_NAME,household.getLastName());
		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findLatestByCmrHouseoldId(new BigDecimal(household.getId()));
		SingleStreamlinedApplication singleStreamlinedApplication = null;
		SsapApplication ssapApplication = null;
		if(ssapApplicationList.size() > 0){
			ssapApplication = ssapApplicationList.get(0);
			singleStreamlinedApplication =  ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			String financialAssistanceFlag = (StringUtils.isNotBlank(ssapApplication.getFinancialAssistanceFlag())) ? ssapApplication.getFinancialAssistanceFlag() : "N";
					
			model.addAttribute("financialAssistanceFlag", financialAssistanceFlag);
			model.addAttribute("incomeConsent",singleStreamlinedApplication.getAgreeToUseIncomeData());
			model.addAttribute("renewalConsent",singleStreamlinedApplication.getConsentAgreement());
			model.addAttribute("incomeConsentYearsDB",singleStreamlinedApplication.getNumberOfYearsAgreed());
			model.addAttribute("renewalConsentChanged",singleStreamlinedApplication.getRenewalConsentChanged());
			model.addAttribute("incomeConsentChanged",singleStreamlinedApplication.getIncomeConsentChanged());
			model.addAttribute("ssapApplicationId", ssapApplication.getId());
		}else {
			model.addAttribute("error", "No Application");
			return "iex/indportal/errorPage";
		}
		
		if(request.getSession().getAttribute("designateBroker") != null){
			singleStreamlinedApplication.setBrokerChanged(true);
			String applicationDataJson =  ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
			ssapApplication.setApplicationData(applicationDataJson);
			ssapApplicationRepository.save(ssapApplication);
		}
		model.addAttribute("brokerChanged",singleStreamlinedApplication.getBrokerChanged());
		DesignateBroker designateBroker = designateService.findBrokerByIndividualId(individualId);
		if (designateBroker != null) {
			Broker broker = brokerRepository.findById(designateBroker.getBrokerId());
			model.addAttribute("dbConsent", designateBroker);
			model.addAttribute("dbProfileConsent", broker);
		}
		 
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setIndividualId(individualId);
		 
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
		if(assisterDesignateResponseDTO!=null){
			DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
			Assister assister = assisterDesignateResponseDTO.getAssister();
			if(designateAssister != null && assister !=null && !designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString())){
				model.addAttribute("daConsent", designateAssister);
				model.addAttribute("daProfileConsent", assister);
			}
		}
		
		return "iex/indportal/consent";
	}
    

    @RequestMapping(value = "/indportal/consent/submit", method = RequestMethod.POST)
	public String postConsent(HttpServletRequest request, Model model) throws GIException {
    	String renewalConsent  = request.getParameter("renewalConsent");
    	String ssapAppId = request.getParameter("ssapApplicationId");
    	
    	try {
    		if(!org.springframework.util.StringUtils.isEmpty(ssapAppId)) {
		    	SsapApplication ssapApplication = ssapApplicationRepository.findOne(Long.valueOf(ssapAppId));
		    	SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		    	String numberOfYearsAgreedStr = singleStreamlinedApplication.getNumberOfYearsAgreed();
		    	
	    		if(renewalConsent!=null && (!renewalConsent.equals(String.valueOf(singleStreamlinedApplication.getConsentAgreement()))) ){
		    		singleStreamlinedApplication.setRenewalConsentChanged(true);
		    		singleStreamlinedApplication.setConsentAgreement(Boolean.valueOf(renewalConsent));
		    	}
	    		
		    	Boolean incomeConsent = null;
		    	if(request.getParameter("incomeConsentYearsVal") != null){
		    		Integer incomeConsentYearsValInt = Integer.valueOf(request.getParameter("incomeConsentYearsVal"));
		    		if(incomeConsentYearsValInt == 5){
		    			incomeConsent = Boolean.TRUE;
		    		}else if(incomeConsentYearsValInt >= 0 && incomeConsentYearsValInt < 5){
		    			incomeConsent = Boolean.FALSE;
		    		}
		    		singleStreamlinedApplication.setNumberOfYearsAgreed(request.getParameter("incomeConsentYearsVal"));
		    	}
		    	
	    		if(incomeConsent != null && incomeConsent != singleStreamlinedApplication.getAgreeToUseIncomeData()){
		    		singleStreamlinedApplication.setIncomeConsentChanged(true);
		    		singleStreamlinedApplication.setAgreeToUseIncomeData(incomeConsent);
		    	}else if(!numberOfYearsAgreedStr.equals(singleStreamlinedApplication.getNumberOfYearsAgreed())){
		    		singleStreamlinedApplication.setIncomeConsentChanged(true);
		    	}
	    		
		    	String applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
		    	ssapApplication.setApplicationData(applicationData);
		    	
		    	ssapApplicationRepository.save(ssapApplication);
    		}
    		else
    		{
    			LOGGER.error("Invalid ssapApplication Id");
    		}
    	}
    	catch(Exception e)
    	{
    		LOGGER.error("Error while saving consent information", e);
    	}
		return "redirect:/indportal/consent";
    }
    
    /**
     * 1. Check if the loggedIn user is valid and is of role - validIndPortalRole
     * 2. Fetch CMR_HOUSEHOLD based on the activeModuleId
     * 3. Load ELIG_LEAD for the household.
     * 4. Check the stage and place it in one of the 4 stages on the UI.
     * 5. Map the values to be displayed to the UI.
     *
     * @param model
     * @param httpServletRequest
     * @return String actionURLs
     * 
     * @throws GIException 
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     * @throws InvalidUserException 
     * @author Sunil Desu
     * @throws ParseException 
     * 
     */
    @RequestMapping(value="/indportal", method=RequestMethod.GET)
    @GiAudit(transactionName = "Get Dashboard for Individual", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_VIEW_AND_PRINT') || hasPermission(#model, 'approvedadminstaffl1', 'IND_PORTAL_MY_ENROLLMENTS') || hasPermission(#model, 'approvedadminstaffl2', 'IND_PORTAL_MY_ENROLLMENTS')")
    @EndpointDescription(description="Endpoint to load Individual Portal home page")
    public String getIndividualPortalPage(Model model, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes) throws GIException, InvalidUserException{
    	    if(LOGGER.isInfoEnabled()){
    	    	LOGGER.info("getIndividualPortalPage function call, pageContext = "+httpServletRequest.getParameter("pageContext")); 
    	    }
    	    
    	    String isConsentPageEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_IS_CONSENT_PAGE_ENABLED);
    	    if(LOGGER.isInfoEnabled()){
    	    	LOGGER.info("isConsentPageEnabled = "+isConsentPageEnabled); 
    	    }
    	    String globalPreventAddConsumer = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.GLOBAL_PREVENT_ADD_CONSUMER);
    	    
    	    if("Y".equalsIgnoreCase(isConsentPageEnabled) || ("N".equalsIgnoreCase(isConsentPageEnabled) && "true".equalsIgnoreCase(globalPreventAddConsumer))) {
    	     	return "redirect:/indportal/consent";
    	    }
    	    
    	    String pageContext = httpServletRequest.getParameter("pageContext");
    		HttpSession session = httpServletRequest.getSession();
    		if(session != null) {
    			session.removeAttribute(SecurityUtils.INTERNAL_REF);
    		}
    		
    		if(LOGGER.isInfoEnabled()){
    			LOGGER.info("Before get User object"); 
    		}
            AccountUser user  = userService.getLoggedInUser();
            if(LOGGER.isInfoEnabled()){
            	LOGGER.info("After get User object = "+user.getActiveModuleId());
            }
            
            Role currUserDefRole = userService.getDefaultRole(user);
            
            if(user !=null && currUserDefRole!=null && "CA".equalsIgnoreCase(stateCode) 
            		&& !currUserDefRole.getName().equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE) && !user.getActiveModuleName().equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE)){
            	 throw new GIRuntimeException("User not authorized.");
            }
            
            if(null != user){
            pageContext=(user.getLoginConext()!=null && user.getLoginConext().get(this.getConsumerUtil().TARGET_PAGE)!=null) ?
            		     user.getLoginConext().get(this.getConsumerUtil().TARGET_PAGE):pageContext;
            if(LOGGER.isInfoEnabled()){
            	LOGGER.info(" User  ({}) has {} Module Id's linked", user.getId(), user.getLinkedModuleIds()!=null?user.getLinkedModuleIds().size():0);
            }

            LOGGER.debug("getIndividualPortalPage::active module id: " + user.getActiveModuleId());
            } else{
	            throw new GIRuntimeException("User is un-authorized");
	        }
            
            Household household = null;
    		if("myEnrollments".equalsIgnoreCase(pageContext) || this.getConsumerUtil().ENROLLMENT_HISTORY.equalsIgnoreCase(pageContext)) {
            	if(user.getActiveModuleId() == 0 && (user.getLinkedModuleIds() != null && user.getLinkedModuleIds().size() > 1)) {
				redirectAttributes.addFlashAttribute("currentPageContext", "multipleHousehold");
				redirectAttributes.addFlashAttribute("hasMultiHousehold", (user.getLinkedModuleIds() != null && user.getLinkedModuleIds().size() > 1));
				redirectAttributes.addFlashAttribute("multiHouseholdContext", "enrollmenthistory");
				}
            	// Login context not being cleared on redirect thus we are manually clearing
            	if(user.getLoginConext() != null) {
				user.getLoginConext().put(this.getConsumerUtil().TARGET_PAGE, "");
            	}
    			return "redirect:/indportal#/enrollmenthistory";
            } else if(user.getActiveModuleId() == 0) {
				model.addAttribute("hasMultiHousehold", (user.getLinkedModuleIds() != null && user.getLinkedModuleIds().size() > 1));
				model.addAttribute("currentPageContext", "multipleHousehold");
                return "iex/indportal/dashboard";
    		}

			model.addAttribute("hasMultiHousehold", (user.getLinkedModuleIds() != null && user.getLinkedModuleIds().size() > 1));
			model.addAttribute("ssapFlowVersion", DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SSAP_FLOW_VERSION));

    		 if(null != user){
    	            int activeModuleId = user.getActiveModuleId();
    	            LOGGER.debug("Retrieving information for activeModuleId:"+ activeModuleId);
    	           String delayPopupConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_DELAY_POPUP);
    	           if("ON".equalsIgnoreCase(delayPopupConfig)){
    	        	   boolean isPopup = isShowPopup(user.getActiveModuleId());
	    	           if(isPopup){
	    	        	   model.addAttribute("popup", true);
	    	           }
    	           }else {
    	        	   model.addAttribute("popup", false);
    	           }
    	            household = this.getConsumerUtil().getHouseholdRecord(activeModuleId);
    	        }
    	        else{
    	            throw new GIRuntimeException("User is un-authorized");
    	        }
    	        
    	        if(null == household){
    	            LOGGER.debug("Household retrieved is having a null value");
    	        }
    	        else{
    	            LOGGER.debug("Household retrieved is: "+ household.getId());
    	            Integer prefContactMethod = household.getPrefContactMethod();
    	            Role defRole = userService.getDefaultRole(user);
    	            if((null == prefContactMethod) && 
    	                    defRole !=null && StringUtils.equalsIgnoreCase(defRole.getName(),GhixRole.INDIVIDUAL.toString())){
    	                return "redirect:/individual/preferences";
    	            }
    	        }
    	        String previewId = httpServletRequest.getParameter("previewId");
    	        if(StringUtils.isBlank(previewId)){
    	        		previewId = (String) httpServletRequest.getSession().getAttribute("previewId");
    	        }
    	        if(StringUtils.isNotBlank(previewId)){
        			httpServletRequest.getSession().setAttribute("previewId", previewId);
        		} 
        showDashboard(model, httpServletRequest, true);
        if(LOGGER.isInfoEnabled()){
			LOGGER.info("getIndividualPortalPage function call end"); 
		}
        return "iex/indportal/dashboard";
    }
    @RequestMapping(value="/indportal/getDashboardModels", method=RequestMethod.GET)
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_VIEW_AND_PRINT')")
    public @ResponseBody String getDashboardModels(Model model, HttpServletRequest httpServletRequest) throws GIException, InvalidUserException{
    	String dashboardDTOJson = showDashboard(model, httpServletRequest, false);
       // return platformGson.toJson(model);
        return dashboardDTOJson;
    }
	private String showDashboard(Model model, HttpServletRequest httpServletRequest, boolean isDashboardCall)
			throws InvalidUserException, GIException {
        AccountUser user  = null;
        Household household = null;
        EligLead eligLead = null;
        DashboardDTO dashboardDTO = new DashboardDTO();
        DashboardConfigurationDTO configuration = new DashboardConfigurationDTO();
        List<DashboardTabDTO> tabs = new ArrayList<>();
        
        configuration.setOepStartDate(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));
        configuration.setOepEndDate(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE));
        configuration.setTobbacoEnabled(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ADDITIONALINFO_TOBACCO));
        configuration.setHardshipEnabled(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ADDITIONALINFO_HARDSHIP));
        configuration.setServerDate(dateToString("MM/dd/yyyy", new TSDate()));
        configuration.setSsapFlowVersion(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SSAP_FLOW_VERSION));
        configuration.setOeEnrollEndDate(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE));
        
        if(isDashboardCall)
        {
        httpServletRequest.getSession().removeAttribute(SsapConstants.CSR_OVER_RIDE);
        //added to remove the value via CSR flow -pravin.
      
        httpServletRequest.getSession().removeAttribute("ActiveAppPresent");
        httpServletRequest.getSession().removeAttribute("enrolledAppSess");
        }
        
        user = userService.getLoggedInUser();
        
        if(null != user){
            int activeModuleId = user.getActiveModuleId();
            LOGGER.debug("Retrieving information for activeModuleId:"+ activeModuleId);
            household = this.getConsumerUtil().getHouseholdRecord(activeModuleId);
        }
        else{
            throw new GIRuntimeException("User is un-authorized");
        }
        
        if(null == household){
            LOGGER.debug("Household retrieved is having a null value");
        }
        else{
            LOGGER.debug("Household retrieved is: "+ household.getId());
            eligLead = household.getEligLead();
            if(eligLead == null) {
            	if(LOGGER.isDebugEnabled()) {
            		LOGGER.debug("No Elig lead available creating new");
            	}
            	eligLead = new EligLead();
            	eligLead.setName(user.getFirstName()+" "+ user.getLastName());
            	eligLead.setEmailAddress(user.getEmail());
            	eligLead.setStage(EligLead.STAGE.WELCOME);
            	eligLead.setCreatedBy(user.getId());                	            	
            	eligLead = this.getConsumerUtil().createEligLeadRecord(eligLead);    
            	household.setEligLead(eligLead);
            
            	try {
					this.getConsumerUtil().saveHouseholdRecord(household);
				} catch (Exception e) {
					LOGGER.error("Error saving household", e);
					throw new GIException(e);
				} 
            }
            String previewId = (String) httpServletRequest.getSession().getAttribute("previewId");
            if("CA".equalsIgnoreCase(stateCode) && StringUtils.isNotBlank(previewId)){
            		PdHouseholdDTO pdHouseholdDTO = findHouseholdByShoppingId(previewId, user.getUsername());
            		if(pdHouseholdDTO != null && (pdHouseholdDTO.getEligLeadId() == null || pdHouseholdDTO.getEligLeadId().longValue() != eligLead.getId())){
            			pdHouseholdDTO.setEligLeadId(eligLead.getId());
            			ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.SET_LEAD_ID_IN_PDHOUSEHOLD, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,pdHouseholdDTO);
            			
            			httpServletRequest.getSession().removeAttribute("previewId");
            		}                	
            }
	        	
            if(isDashboardCall)
            {
            httpServletRequest.getSession().setAttribute(IndividualPortalConstants.LEAD_ID, eligLead.getId());
            }
            GiAuditParameterUtil.add(new GiAuditParameter("INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER", IndividualPortalConstants.INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.get(String.valueOf(eligLead.getStage()))));
        }
        
        //HIX-70579 Multi-year enrollment support
        int year = populateMultiYearTabInfo(tabs, model,httpServletRequest);
        
        populateMultiTabInfo(tabs, eligLead,model,year,household, user.getUsername(), configuration);
       
        DesignateBroker designateBroker = null;
        DesignateAssister designateAssister = null;
        
        if(null != household){
            
           /* //HIX-70579 Multi-year enrollment support
            populateModelValuesForDisplay(configuration, model, preEligResults, household,year, preEligibility, enrollments, user.getUsername());*/
            List<AnnouncementDTO> listOfAnnouncements = getActiveAnnouncements();
            List<DashboardAnnouncementDTO> announcements = new ArrayList<>();
            if(listOfAnnouncements != null && !listOfAnnouncements.isEmpty()){
            	for(AnnouncementDTO announcementDTO : listOfAnnouncements){
            		DashboardAnnouncementDTO announcement = new DashboardAnnouncementDTO();
            		announcement.setAnnouncementStartDate(announcementDTO.getEffectiveStartDate());
            		announcement.setAnnouncementEndDate(announcementDTO.getEffectiveEndDate());
            		announcement.setAnnouncementContent(announcementDTO.getAnnouncementText());
            		announcements.add(announcement);
            	}  
            	configuration.setAnnouncements(announcements);
            }
            
            
            /*Check if this Individual already 
             * designated to CEC or an Agent 
             */
            
            AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
            assisterRequestDTO.setIndividualId(household.getId());
            
            AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
            if(assisterDesignateResponseDTO!=null){
                designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
                Assister assister = assisterDesignateResponseDTO.getAssister();
    
                if(designateAssister != null && assister !=null && 
                        !designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString())){
                    LOGGER.debug("Designate Assister not null. Setting existing designatedAssister in the session.");
                    if(isDashboardCall)
                    {
                    httpServletRequest.getSession().setAttribute(IndividualPortalConstants.DESIGNATED_ASSISTER_PROFILE, assister);
                    httpServletRequest.getSession().setAttribute(IndividualPortalConstants.DESIGNATE_ASSISTER, designateAssister);
                    }
                }
                else
                {
                	if(isDashboardCall)
                    {
                	httpServletRequest.getSession().removeAttribute(IndividualPortalConstants.DESIGNATED_ASSISTER_PROFILE);
                	httpServletRequest.getSession().removeAttribute(IndividualPortalConstants.DESIGNATE_ASSISTER);
                    }
                }
            }  
    
    
            designateBroker = this.getDesignateService().findBrokerByIndividualId(household.getId());
            if(designateBroker!=null) {
            	
                LOGGER.debug("Designate Broker is not null. Setting existing designatedBroker in the session.");
                int brkId = designateBroker.getBrokerId();
                Broker brokerObj = this.getBrokerService().getBrokerDetail(brkId);
                
                if(!designateBroker.getStatus().toString().equals(DesignateBroker.Status.InActive.toString())) {  
                	if(isDashboardCall)
                    {
                    httpServletRequest.getSession().setAttribute(IndividualPortalConstants.DESIGNATED_BROKER_PROFILE, brokerObj);
                    httpServletRequest.getSession().setAttribute("designateBroker", designateBroker);
                    }
                }
                else
                {
                	if(isDashboardCall)
                    {
                	httpServletRequest.getSession().removeAttribute(IndividualPortalConstants.DESIGNATED_BROKER_PROFILE);
                	httpServletRequest.getSession().removeAttribute("designateBroker");
                    }
        }
            }
            else
            {
            	if(isDashboardCall)
                {
            	httpServletRequest.getSession().removeAttribute(IndividualPortalConstants.DESIGNATED_BROKER_PROFILE);
            	httpServletRequest.getSession().removeAttribute("designateBroker");
                }
            }
        }
        
        dashboardDTO.setConfiguration(configuration);
        dashboardDTO.setTabs(tabs);
        //HIX-70815 Show modal for agent promotion
        populateAgentPromoInfo(designateAssister,designateBroker,user,httpServletRequest, isDashboardCall);
        
        
        GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
        String dashboardDTOJson = platformGson.toJson(dashboardDTO);
        return dashboardDTOJson;

    }

    private void populateMultiTabInfo(List<DashboardTabDTO> dashboardTabs, EligLead eligLead, Model model, int year, Household household, String userName, DashboardConfigurationDTO dashboardConfigurationDTO) throws GIException {
    	 for(DashboardTabDTO dashboardTabDTO : dashboardTabs){  
    		 DashboardPreEligibilityDTO dashboardPreEligibilityDTO = new DashboardPreEligibilityDTO();
    		 year = Integer.parseInt(dashboardTabDTO.getApplicationYear());
    		 populateFavoritePlanInfo(eligLead,model,year,household, dashboardPreEligibilityDTO, dashboardTabDTO);
    		 //HIX-72054 - Support for multi year FPS and PERs
    		 PreEligibilityResults preEligResults = null;
    		 if("CA".equalsIgnoreCase(stateCode) && eligLead != null) {
    			 //TODO 1. fetch latest pdhousehold based on lead id and coverage year
    			 preEligResults = new PreEligibilityResults();
        		 preEligResults.setStage(STAGE.WELCOME);
    			 PdHouseholdDTO pdHouseholdDTO = findPdHouseholdByLeadIdAndYear(String.valueOf(eligLead.getId()), String.valueOf(year), userName);
    			 if(pdHouseholdDTO != null){
    			 MiniEstimatorResponse apiOutput = getAPIOutputForLead(pdHouseholdDTO);
    			 preEligResults.setApiOutput(platformGson.toJson(apiOutput));
    			 preEligResults.setCoverageYear(Integer.parseInt(DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy")));
    			 preEligResults.setAptc(pdHouseholdDTO.getMaxSubsidy()!=null ? pdHouseholdDTO.getMaxSubsidy()+"" : null);
    			 preEligResults.setCsr(pdHouseholdDTO.getCostSharing() != null ? pdHouseholdDTO.getCostSharing().name(): null);	
    			 }    			 
    		 }else {
    			 preEligResults = getPreEligResultForCoverageYear(eligLead, year);
    		 }
    		 //Create dummy results till actual pre eligibility results are available
    		 if(preEligResults == null){
    			 preEligResults = new PreEligibilityResults();
    			 preEligResults.setStage(STAGE.WELCOME);
    		 }
    		 if(null != household){
    			 //HIX-70579 Multi-year enrollment support
    			 populateModelValuesForDisplay(dashboardConfigurationDTO, model, preEligResults, household,year, dashboardPreEligibilityDTO, userName, dashboardTabDTO);               
    		 }
    	 }		
	}

    /**
     * @author Sunil Desu
     * @since March 10 2013
     * 
     * Post registration functionality for an INDIVIDUAL.
     * 
     * This will be invoked by UserService after the Individual signup 'submit' is processed and a User has been created.
     * This endpoint is invoked since the POST_REG_URL for INDIVIDUAL role in ROLES table is set to /indportal/postreg.
     * 
     * Following is the sequence of key steps for post registration
     * 
     * 1. Check if Lead is created and ID is in the session. If no Lead is found, create an empty lead.
     * 2. Create a household for the User and link it to the lead
     * 3. Create module user
     *  
     * @param model
     * @param request
     * @param redirectAttrs
     * @return
     * @throws InvalidUserException 
     * @throws URISyntaxException 
     * @throws IOException 
     * @throws ClientProtocolException 
     * @throws Exception 
     * 
     */
    @RequestMapping(value = "/indportal/postreg", method = RequestMethod.GET)
    @GiAudit(transactionName = "Complete post reg for user", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    public String individualPostRegistration( HttpServletRequest request) throws GIException {
        
        try{
            LOGGER.debug("Individual Post Registration process started.");
            
            AccountUser user = null;
            long leadId = -1;
            EligLead eligLead = null;
            Household household = null;
            user = userService.getLoggedInUser();
            
            household = new Household();
            //HIX-41165
            LOGGER.debug("individualPostRegistration - isSkipHouseholdCreation .. ");
            boolean isSkipHouseholdCreation = false;
            String activationId = null;
            Object activationIdFromSession = request.getSession().getAttribute("activationId");       
            if(activationIdFromSession != null){
                activationId = activationIdFromSession.toString();
            }
            
            Household dbHousehold = householdRepository.findByUserId(user.getId());
            if(dbHousehold != null) {
            	//Household already exists
            	household = dbHousehold;
            	isSkipHouseholdCreation=true;
            }
            if(StringUtils.isNotEmpty(activationId) && StringUtils.isNumeric(activationId)){
                AccountActivation accountActivation = getAccountActivationService().findByActivationId(Integer.valueOf(activationId));
                if(GhixRole.INDIVIDUAL.toString().equals(accountActivation.getCreatedObjectType())) // Added as a part of HIX-41626 fix
                {
                    household = this.getConsumerUtil().getHouseholdRecord(accountActivation.getCreatedObjectId());
                    if(household != null)
                    {
                        LOGGER.debug("individualPostRegistration - isSkipHouseholdCreation true .. ");
                        /* HIX-56933 - Account Activation Flow - Starts */
                        /**
                         * If Post Registration is for Account Activation, set user id in household.
                         */
                        Map<String, String> mapEventParam = new HashMap<>();
                        LookupValue lookupValue = lookUpService.getlookupValueByTypeANDLookupValueCode("APPEVENT_ACCOUNT","ACCOUNT_ACTIVATED");
                		EventInfoDto eventInfoDto = new EventInfoDto();
                		eventInfoDto.setEventLookupValue(lookupValue);
                		eventInfoDto.setModuleId(household.getId());
                		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
                		
                		appEventService.record(eventInfoDto, mapEventParam);
                		
                        household.setUser(user);
                        household = this.getConsumerUtil().saveHouseholdRecord(household);
                        /* HIX-56933 - Account Activation Flow - Ends */
                        isSkipHouseholdCreation=true;
                    }
                    //else : this means we do not have a Household object created yet.. in this case, go ahead and create household
                }
            }
            
            LOGGER.debug("individualPostRegistration - isSkipHouseholdCreation value =   " + isSkipHouseholdCreation);
            if(!isSkipHouseholdCreation){
    
                if (request.getSession() != null && 
                        request.getSession().getAttribute(IndividualPortalConstants.LEAD_ID) != null && 
                        Long.parseLong(request.getSession().getAttribute(IndividualPortalConstants.LEAD_ID).toString())>0) {
                    leadId = Long.parseLong(request.getSession().getAttribute(IndividualPortalConstants.LEAD_ID).toString().trim());
                    eligLead = this.getConsumerUtil().fetchEligLead(leadId);
                    if (eligLead != null) {
                        LOGGER.debug("individualPostRegistration - populateHouseholdFromEligLead - isSkipHouseholdCreation value =   " + isSkipHouseholdCreation);
                        populateHouseholdFromEligLead(household, eligLead, user);
                    }
                } else {
                    eligLead = new EligLead();
                    LOGGER.debug("individualPostRegistration - populateHouseholdFromEmptyLead - isSkipHouseholdCreation value =   " + isSkipHouseholdCreation);
                    populateHouseholdFromEmptyLead(household, eligLead, user, request);
                }
                household.setGiHouseholdId(UUID.randomUUID().toString());
                household = this.getConsumerUtil().saveHouseholdRecord(household);
                if (household == null) {
                    LOGGER.error("Error saving Household to CMR_HOUSEHOLD table");
                    throw new GIException("CMR_HOUSEHOLD could not be created.");
                } else {
                	//event type and category
            		LookupValue lookupValue = lookUpService.getlookupValueByTypeANDLookupValueCode("APPEVENT_ACCOUNT","ACCOUNT_CREATED");
            		EventInfoDto eventInfoDto = new EventInfoDto();
            		eventInfoDto.setEventLookupValue(lookupValue);
            		eventInfoDto.setModuleId(household.getId());
            		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
            		
            		appEventService.record(eventInfoDto, null);
                }
            }// if(skipHouseholdCreation) 
            
            if(null!=household && null!=household.getUser()){
                ConsumerEventHistory consumerCreatedEvent = new ConsumerEventHistory(household.getCreated());
                consumerCreatedEvent.setEventSource("User-Management");
                consumerCreatedEvent.setEventLevel("Account Created");
                consumerCreatedEvent.setHouseholdId(household.getId());
                consumerCreatedEvent.setDatetime(household.getCreated());
                consumerCreatedEvent.setCreatedBy(user.getId());
                consumerEventsUtil.postConsumerLoginEvent(consumerCreatedEvent);
            }
            if(null!=household){
                LOGGER.debug("individualPostRegistration - populateHouseholdFromEmptyLead - isSkipHouseholdCreation household.getId()  =   " + household.getId());

                ModuleUser mUser = userService.findModuleUser(household.getId(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE, user);

                if(mUser == null) {
                  mUser = userService.createModuleUser(household.getId(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE, user, true);
                }

                user.setActiveModuleId(mUser.getModuleId());
                user.setActiveModuleName(IndividualPortalConstants.INDIVIDUAL_LOWER_CASE);
                user.setActiveModule(mUser);
            }
            /* HIX-38644 - Changes for State Referral Account Starts*/
            if(request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL) != null && 
                    StringUtils.equals("Y",request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL).toString())){
                return "redirect:/referral/activation/binduser";    
            }    
            /* HIX-38644 - Changes for State Referral Account Ends*/
            if(StringUtils.equalsIgnoreCase("ID",DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE.getValue()))){
                return "redirect:/individual/preferences";
            }
            
        }
        catch(IOException e) {
            LOGGER.error("IO Exception occured",e);
            persistToGiMonitor(e);
        }
        catch(GIException | URISyntaxException e) {
            LOGGER.error("Exception while post registration ",e);
            persistToGiMonitor(e);
        } catch (InvalidUserException e) {
            LOGGER.error("Exception while getting logged in user",e);
            persistToGiMonitor(e);
        }   
        
        return "redirect:/indportal";
    }
    
    /**
     * @param model
     * @param request
     * @param redirectAttrs
     * @return Map
     */
    @RequestMapping(value = "/indportal/getApplications", method = RequestMethod.GET)
    @GiAudit(transactionName = "Show all applications for a household", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_START_APP')")
    @ResponseBody
    public MyApplications viewMyApplications(@RequestParam(value = "coverageYear", required = true) String coverageYear, HttpServletRequest request){
    	MyApplications myApplications = new MyApplications();
    	AccountUser user  = null;    
        boolean isIndividual = false;
        
        try{
            user = userService.getLoggedInUser();
            if (StringUtils.equalsIgnoreCase(user.getDefRole().getName(), IndividualPortalConstants.INDIVIDUAL_LOWER_CASE)) {
                isIndividual = true;
            }
            
            IndPortalValidationDTO indPortalValidationDTO = validateCoverageYrForApplications(coverageYear);
            if(!indPortalValidationDTO.getStatus()){
            	LOGGER.debug(indPortalValidationDTO.getErrors().toString());            	
            }else{
            	myApplications =  getApplicationsForHousehold(user.getActiveModuleId(), isIndividual, coverageYear); 
            	Household household = this.getConsumerUtil().getHouseholdRecord(user.getActiveModuleId());
            	if(household != null && myApplications != null){
            		myApplications.setHouseholdCaseId(household.getHouseholdCaseId());
            	} 
            }            
        }catch(Exception ex){
            LOGGER.error("Error retrieving applications",ex);
            persistToGiMonitor(ex);
        }
        return myApplications;
    }
    
	/**
      * @param model
      * @param request
      * @return
      * @throws GIException
      * @throws InvalidUserException
      */
     @RequestMapping(value = "/indportal/enroll", method = RequestMethod.POST)
     @GiAudit(transactionName = "Enroll user in plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public String enrollIndividual( HttpServletRequest request) throws GIException{
        AccountUser user;
        try {
            user = userService.getLoggedInUser();
        } catch (InvalidUserException e) {
            throw new GIRuntimeException("Unauthorized",e);
        }
        if(user==null){
            throw new GIRuntimeException("Unauthorized");
        }
        //HIX-57896
        int activeModuleId = user.getActiveModuleId();
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        validateActiveModuleId(caseNumber, activeModuleId);
        /*
         * HIX-58645 Send Primary Contact Phone number in IND 19
         * This number will be used from SSAP.
         * If no number is present in SSAP, use the one from CMR_HOUSE_HOLD
         */
        Household household = this.getConsumerUtil().getHouseholdRecord(activeModuleId);
        int year = TSCalendar.getInstance().get(Calendar.YEAR);
        String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
        if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
            year = Integer.parseInt(coverageYear);
        }
        Ind19ClientRequest ind19ClientRequest = new Ind19ClientRequest();
        ind19ClientRequest.setCaseNumber(caseNumber);
        ind19ClientRequest.setHouseHoldId((long)activeModuleId);
        ind19ClientRequest.setAssisterId(null);
        ind19ClientRequest.setBrokerId(null);
        ind19ClientRequest.setPhoneNumber(household.getPhoneNumber());
        ind19ClientRequest.setIsChangePlanFlow(false);
        ind19ClientRequest.setIsSEPPlanFlow(false);
        ind19ClientRequest.setCoverageYear(year);
        return invokeInd19(ind19ClientRequest);
     }
     
    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @return URL
     */
     @GiAudit(transactionName = "View Verification Results", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.SSAP_APP)
    @RequestMapping(value = "/indportal/viewVerificationResults", method = RequestMethod.GET)
    @PreAuthorize(IndividualPortalConstants.PORTAL_VIEW_VER_RESULTS_PERMISSION)
    public String viewVerificationResults( HttpServletRequest request,RedirectAttributes redirectAttributes){
        try{
            //AccountUser user = userService.getLoggedInUser();
            //HIX-57896
            String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
            if(caseNumber!=null && caseNumber.trim().length()>0){
                validateActiveModuleId(caseNumber, -1);
                redirectAttributes.addFlashAttribute(IndividualPortalConstants.CASE_NUMBER, caseNumber.trim());
            }
            request.getSession().setAttribute("SSAP_APPLICATION_ID", getIdFromCaseNumber(caseNumber));
            return "redirect:/ssap/verificationResultDashboard";
        }catch(Exception ex){
            LOGGER.error("Error accessing ssap ",ex);
            persistToGiMonitor(ex);
            return null;
        }
    }
    
     /**
      * @param model
      * @param request
      * @return
      * @throws GIException
      */
     @RequestMapping(value = "/indportal/cancel", method = RequestMethod.POST)
     @GiAudit(transactionName = "Cancel an application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
    @ResponseBody
    public String cancelApplication( HttpServletRequest request) throws GIException{
         try{
             
             AccountUser user  = null;    
             user = userService.getLoggedInUser();
             int activeModuleId = user.getActiveModuleId();
             String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
             validateActiveModuleId(caseNumber, activeModuleId);
             
             //HIX-63511
             verifyCancelAction(caseNumber);
             
             updateApplicationStatus(caseNumber,"CC");
             logIndividualAppEvent(IndividualPortalConstants.APPEVENT_APPLICATION, IndividualPortalConstants.APPLICATION_CANCEL, null, request);
             
             return IndividualPortalConstants.SUCCESS;
             
         }catch(Exception ex){
             persistToGiMonitor(ex);
             return IndividualPortalConstants.FAILURE;
         }
     }
     
    /**
     * @param model
     * @param request
     * @param indPortalContactUs
     * @return SUCCESS or FAILURE
     */
    @RequestMapping(value = "/indportal/submitcontactus", method = RequestMethod.POST)
    @GiAudit(transactionName = "Submit Complaint", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_CONTACT_US_PERMISSION)
    @ResponseBody
    public String submitContactUs(@RequestBody IndPortalContactUs indPortalContactUs){
        LOGGER.debug("Type of Request: "+indPortalContactUs.getTypeOfRequest());
        LOGGER.debug("Reason: "+indPortalContactUs.getReason());
        try{
            submitComplaintToCreateTicket(indPortalContactUs);
        }catch(Exception ex){
            LOGGER.error("Error creating ticket: ",ex);
            persistToGiMonitor(ex);
            return IndividualPortalConstants.FAILURE;
        }
        return IndividualPortalConstants.SUCCESS;
    }
    
    /**
     * @param fileToUpload
     * @param mrequest
     * @return docId
     */
    @RequestMapping(value="/indportal/upload", method = RequestMethod.POST)
    @GiAudit(transactionName = "Upload appeal file", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_CONTACT_US_PERMISSION)
    @ResponseBody
    public String uploadFile(@RequestParam(value = "fileToUpload", required = false) CommonsMultipartFile fileToUpload)
    {
       String docId = null;
        try {
             AccountUser user  = null;    
             user = userService.getLoggedInUser();
             docId = uploadFileToECM(fileToUpload, user.getActiveModuleId(), IndividualPortalConstants.APPEAL);
        } catch (Exception e) {
            LOGGER.error("Error uploading file", e);
            persistToGiMonitor(e);
            docId = IndividualPortalConstants.FAILURE;
        }
        return docId;
    }
    
    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @param caseNumber
     * @return URL
     */
    @GiAudit(transactionName = "Goto SSAP", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.SSAP_APP)
    @RequestMapping(value = "/indportal/gotossap", method = RequestMethod.GET)
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
     public String goToSsap( HttpServletRequest request, RedirectAttributes redirectAttributes) {
        //HIX-57896
        String caseNumberFromRequest = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if(caseNumberFromRequest!=null && caseNumberFromRequest.trim().length()>0){
            validateActiveModuleId(caseNumberFromRequest, -1);
            redirectAttributes.addFlashAttribute(IndividualPortalConstants.CASE_NUMBER, caseNumberFromRequest.trim());
        }
        return "redirect:/ssap";
    }
    
    /**
     * HIX-64416
     * Send application type for start application
     * @param request
     * @param redirectAttributes
     * @param request
     * @param redirectAttributes
     * @return
     */
    @GiAudit(transactionName = "Start QEP", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.SSAP_APP)
    @RequestMapping(value = "/indportal/startQepApp", method = RequestMethod.GET)
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
     public String startQepApp( HttpServletRequest request, RedirectAttributes redirectAttributes) {
    
        redirectAttributes.addFlashAttribute("applicationType", "QEP");
        redirectAttributes.addFlashAttribute(IndividualPortalConstants.COVERAGE_YEAR, request.getParameter(IndividualPortalConstants.COVERAGE_YEAR));
        
        return "redirect:/ssap";
    }
    
    /**
     * HIX-69095
     * Send application type for start application for OE
     * @param request
     * @param redirectAttributes
     * @param request
     * @param redirectAttributes
     * @return
     */
    @GiAudit(transactionName = "Start OE", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.SSAP_APP)
    @RequestMapping(value = "/indportal/startOEApp", method = RequestMethod.GET)
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
     public String startOEApp( HttpServletRequest request, RedirectAttributes redirectAttributes) {
    
        redirectAttributes.addFlashAttribute("applicationType", "OE");
        redirectAttributes.addFlashAttribute(IndividualPortalConstants.COVERAGE_YEAR, request.getParameter(IndividualPortalConstants.COVERAGE_YEAR));
        
        return "redirect:/ssap";
    }
    
    /**
     * @param model
     * @param request
     * @return applicationEligibilityDetails
     * @throws GIException
     */
    @RequestMapping(value = "/indportal/geteligibilitydetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "Gets the eligibility results for an application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_VIEW_ELIG_RESULTS_PERMISSION)
     public ApplicationEligibilityDetails getApplicationEligibilityDetails( HttpServletRequest request) throws GIException {
        ApplicationEligibilityDetails applicationEligibilityDetails = null;
        //HIX-57896
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if (caseNumber != null && caseNumber.trim().length() > 0) {
            validateActiveModuleId(caseNumber, -1);
            applicationEligibilityDetails = getApplicationEligibilityDetails(caseNumber.trim());
        }
        return applicationEligibilityDetails;
    }


    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @param caseNumber
     * @return URL
     */
    @RequestMapping(value = "/indportal/editapplication", method = RequestMethod.GET)
    @GiAudit(transactionName = "CSR over ride for Edit Application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
//    @PreAuthorize(IndividualPortalConstants.PORTAL_EDIT_APP_PERMISSION)
     public String editApplication( HttpServletRequest request, RedirectAttributes redirectAttributes) {
        //HIX-57896
        String caseNumberFromRequest = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if(caseNumberFromRequest!=null && caseNumberFromRequest.trim().length()>0){
            //            validateActiveModuleId(caseNumberFromRequest, -1);
            redirectAttributes.addFlashAttribute(IndividualPortalConstants.CASE_NUMBER, caseNumberFromRequest.trim());
            request.getSession().setAttribute(SsapConstants.CSR_OVER_RIDE, true);
            logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_EDIT_NON_FIN_APP,null,request);
        }

        SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumber(caseNumberFromRequest);
        if(ssapApplication != null){
        	SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
        	singleStreamlinedApplication.setIsEditApplication(true);
        	ssapApplication.setApplicationData(ssapJsonBuilder.transformToJson(singleStreamlinedApplication));
        	ssapApplicationRepository.save(ssapApplication);
        }
        
        String ssapFlowVersion = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SSAP_FLOW_VERSION);
        if(StringUtils.isNotBlank(ssapFlowVersion) && ssapFlowVersion.equalsIgnoreCase("2.0")) {
            return String.format("redirect:/newssap/start?caseNumber=%s&mode=%s", caseNumberFromRequest, "edit");
        } else {
          return "redirect:/editssap";
        }
    }
    
    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @param caseNumber
     * @return integrationStatus
     */
    @RequestMapping(value = "/indportal/initVerifications", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride for initializing eligibility verifications", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
//    @PreAuthorize(IndividualPortalConstants.PORTAL_INIT_VERIFICATIONS_PERMISSION)
     public String initiateVerifications( HttpServletRequest request) {
        String integrationStatus = IndividualPortalConstants.FAILURE;
        //HIX-57896
        String caseNumberFromRequest = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if(caseNumberFromRequest!=null && caseNumberFromRequest.trim().length()>0){
//            validateActiveModuleId(caseNumberFromRequest, -1);
            Object applicationId = getIdFromCaseNumber(caseNumberFromRequest);
            integrationStatus = csrOverrideUtility.invokeSSAPIntegration(Long.parseLong(applicationId.toString()));      
            logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_INITIATE_NON_FIN_VERIF,null,request);
        }
        return integrationStatus;
    }
    
    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @param caseNumber
     * @return String
     */
    @RequestMapping(value = "/indportal/rerunEligibility", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride for re-running non-financial eligibility", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
//    @PreAuthorize(IndividualPortalConstants.PORTAL_RUN_ELIGIBILITY_PERMISSION)
     public String rerunEligibility( HttpServletRequest request) {
        //HIX-57896
        String caseNumberFromRequest = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
//        validateActiveModuleId(caseNumberFromRequest, -1);
        
        logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_RERUN_NON_FIN_ELIG,null,request);
        
        String responseStatus = csrOverrideUtility.reRunEligibility(caseNumberFromRequest);
        
        return responseStatus; 
    }


    /**
     * @param model
     * @param request
     * @param caseNumber
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/updateCarrier", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride for sending demo updates to carrier", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_UPDATE_CARRIER_PERMISSION)
     public String updateCarrier( HttpServletRequest request,@RequestParam(required = true) String caseNumber) {
        String responseVal = IndividualPortalConstants.FAILURE;            
        if(null != caseNumber && !caseNumber.isEmpty()){
            Ind57Mapping ind57Mapping = new Ind57Mapping();
            ind57Mapping.setCaseNumber(caseNumber);
            try {
                AccountUser user = userService.getLoggedInUser();
                //HIX-57896
                int activeModuleId = user.getActiveModuleId();
                validateActiveModuleId(caseNumber, activeModuleId);
                ind57Mapping.setUserName(user.getUserName());
                responseVal = csrOverrideUtility.invokeAdminUpdateEnrollment(ind57Mapping);
                logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_SEND_DEMO_UPDATES,null,request);
            }catch(Exception e) {
                LOGGER.error("Exception occurred while updating carrier");
                persistToGiMonitor(e);
                responseVal = IndividualPortalConstants.FAILURE;
            }     
        }
        return responseVal;
    }

    /**
     * @param model
     * @param request
     * @param redirectAttributes
     * @param overrideComment
     * @return URL
     */
    @RequestMapping(value = "/indportal/addOverrideComment", method = RequestMethod.POST)
    @GiAudit(transactionName = "Saves CSR over ride comments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
     public String submitOverrideComment( HttpServletRequest request, @RequestBody OverrideComment overrideComment)  {
        LOGGER.debug("OverrideComment",overrideComment);
        String caseNumber = overrideComment.getCaseNumber();
        try {
        		capPortalConfigurationUtil.setActiveModuleId(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("encHouseholdid")));
        	} catch (InvalidUserException e) {
	 	 		LOGGER.debug("Invalid User Exception");
	 	 		return IndividualPortalConstants.FAILURE;
	 	 	}
        request.getSession().setAttribute(IndividualPortalConstants.OVERRIDE_COMMENT, overrideComment.getOverrideComment());
        return "forward:/platform/web/comment/controller/savecomment?target_id="+this.getIdFromCaseNumber(caseNumber)+"&target_name=SSAP_APPLICATIONS&comment_text="+overrideComment.getOverrideComment();
    }
    
    /**
     * @param model
     * @param request
     * @return Map
     */
    @RequestMapping(value = "/indportal/getOverrideComments", method = RequestMethod.POST)
    @GiAudit(transactionName = "Gets CSR over ride comments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
     @ResponseBody
    public Map<String, List<OverrideComment>> getOverrideComments( HttpServletRequest request) {
        LOGGER.debug("OverrideComments");
        Map<String, List<OverrideComment>> overrideCommentsMap = new HashMap<String, List<OverrideComment>>();
        List<OverrideComment> overrideComments = new ArrayList<OverrideComment>(0);
        //HIX-57896
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if(caseNumber!=null && caseNumber.trim().length()>0){
            validateActiveModuleId(caseNumber, -1);
            CommentTarget commentTargets = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(getIdFromCaseNumber(caseNumber).toString()), CommentTarget.TargetName.SSAP_APPLICATIONS);
            if(null != commentTargets){
                List<Comment> comments = commentTargets.getComments();
                overrideComments = csrOverrideUtility.transformComments(comments);
            }
        }
        
        
        overrideCommentsMap.put("comments", overrideComments);
        return overrideCommentsMap;
    }

    /**
     * @param model
     * @param request
     * @param indAppeal
     * @return SUCCESS or FAILURE
     */
    @RequestMapping(value = "/indportal/submitappeal", method = RequestMethod.POST)
    @GiAudit(transactionName = "Submit Appeal", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_SUBMIT_APPEAL_PERMISSION)
    @ResponseBody
    public String submitAppeal(@RequestBody IndAppeal indAppeal){
        LOGGER.debug("Submitting appeal");
        try{
            submitAppealRequest(indAppeal);
        }catch(Exception ex){
            LOGGER.error("Error creating ticket: ",ex);
            persistToGiMonitor(ex);
            return IndividualPortalConstants.FAILURE;
        }
        return IndividualPortalConstants.SUCCESS;
    }
    
    /**
     * @param model
     * @param request
     * @param indDisenroll
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/terminatePlan", method = RequestMethod.POST)
    @GiAudit(transactionName = "Terminate health plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String disenroll( HttpServletRequest request, @RequestBody IndDisenroll indDisenroll) {
        String responseVal = IndividualPortalConstants.FAILURE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
        try {
            AccountUser user = userService.getLoggedInUser();
            //HIX-57896
            validateActiveModuleId(indDisenroll.getCaseNumber(), user.getActiveModuleId());
            Object[] idAndType = getIdAndApplicationTypeFromCaseNumber(indDisenroll.getCaseNumber());
            Long applicationIdToDisenroll = Long.valueOf(idAndType[0].toString());
            String applicationType = idAndType[1].toString();
            Date applicationCreationTime = (Date) idAndType[3];
            
            indDisenroll.setApplicationId(applicationIdToDisenroll);
            
            String overrideComment = (String) request.getSession().getAttribute(IndividualPortalConstants.OVERRIDE_COMMENT);
            if (StringUtils.isNotBlank(overrideComment) && StringUtils.isBlank(indDisenroll.getCoverageStartDate())) {
            	Map<String, PlanSummaryDetails> planDetailsMap = getEnrollmentDetails(applicationIdToDisenroll.toString(), indDisenroll.getCaseNumber());
            	if(planDetailsMap != null && !planDetailsMap.isEmpty()){
            		PlanSummaryDetails healthDetails = planDetailsMap.get(IndividualPortalConstants.HEALTH_PLAN);
                	if(healthDetails != null && StringUtils.isNotBlank(healthDetails.getCoverageStartDate())){
    					indDisenroll.setCoverageStartDate(healthDetails.getCoverageStartDate());
    				}else {
    					PlanSummaryDetails dentalDetails = planDetailsMap.get(IndividualPortalConstants.DENTAL_PLAN);
    					if (dentalDetails != null && StringUtils.isNotBlank(dentalDetails.getCoverageStartDate())) {
    						indDisenroll.setCoverageStartDate(dentalDetails.getCoverageStartDate());
    					}
    				} 
            	}
            	
			}
            
            if(null !=  indDisenroll.getTerminationDate()){
                indDisenroll.setTerminationDate(formatter.format(dateFormat.parse(indDisenroll.getTerminationDate())));
            }else{
                indDisenroll.setTerminationDate(formatter.format(getTerminationDateOnDisenrollment(indDisenroll.getTerminationDateChoice(),formatter.parse(indDisenroll.getCoverageStartDate()))));
            }
            indDisenroll.setUserName(user.getUserName());
            
            String reasonCode = indDisenroll.getReasonCode();
            for (DisenrollReasonEnum c : DisenrollReasonEnum.class.getEnumConstants()) {
                if (c.name().equalsIgnoreCase(indDisenroll.getReasonCode())) {
                    indDisenroll.setReasonCode(c.value());
                    break;
                }
            }
            responseVal = csrOverrideUtility.invokeIndDisenrollment(indDisenroll); 
            if(responseVal.equalsIgnoreCase(IndividualPortalConstants.SUCCESS)){
            	
                String saveComments = null;
                if (indDisenroll.getHealthEnrollmentId() != null) {
                    String healthEnrolId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getHealthEnrollmentId());
                    
                    String comment = "HealthDisenrollmentReason:"+reasonCode;
                    if(StringUtils.equalsIgnoreCase(reasonCode, DisenrollReasonEnum.OTHER.name()) && StringUtils.isNotBlank(indDisenroll.getOtherReason())){
                    	comment = comment.concat(":"+indDisenroll.getOtherReason());
                    }
                    saveComments = saveComments(healthEnrolId, IndividualPortalConstants.ENROLLMENT, comment);
                    
                    if (!StringUtils.equalsIgnoreCase(saveComments, IndividualPortalConstants.SUCCESS)) {
                        LOGGER.info("Unable to store the comments on table while disenrolling Health enrollments");
                    }
                }
                if (indDisenroll.getDentalEnrollmentId() != null) {
                    String dentalEnrolId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getDentalEnrollmentId());
                    
                    String comment = "DentalDisenrollmentReason:"+reasonCode;
                    if(StringUtils.equalsIgnoreCase(reasonCode, DisenrollReasonEnum.OTHER.name()) && StringUtils.isNotBlank(indDisenroll.getOtherReason())){
                     comment = comment.concat(":"+indDisenroll.getOtherReason());
                    }
                    saveComments = saveComments(dentalEnrolId, IndividualPortalConstants.ENROLLMENT, comment);
                    
                    if (!StringUtils.equalsIgnoreCase(saveComments, IndividualPortalConstants.SUCCESS)) {
                        LOGGER.info("Unable to store the comments on table while disenrolling Dental enrollments");
                    }
                }
                
                if (StringUtils.isNotEmpty(overrideComment)) {
                    logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_TERMINATE_PLAN,null,request);
                }
                
            }
        }
        catch(GIException | ParseException e) {
            LOGGER.error("Error while dis-enrollment: ",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        } catch (InvalidUserException e) {
            LOGGER.error("Exception while getting user",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        } 
        return responseVal;
    }  
    
    /**
     * @param model
     * @param request
     * @return additionalDetails
     */
    @RequestMapping(value = "/indportal/getApplicantsForAddInfo", method = RequestMethod.POST)
    @GiAudit(transactionName = "Get tobacco usage page", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
     @ResponseBody
     @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public IndAdditionalDetails getApplicantsForAddInfo( HttpServletRequest request) {
        LOGGER.debug("IndPortalApplicantDetails");
        IndAdditionalDetails additionalDetails = null;
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        String isChangePlanFlowFlag = request.getParameter("isChangePlanFlowFlag");
        String changeSEPPlanFlow = request.getParameter("changeSEPPlanFlow");
        if(caseNumber != null && caseNumber.trim().length()>0){
            try {
                //HIX-57896
                validateActiveModuleId(caseNumber, -1);
                /*
                 * HIX-63747 Allow change plan for Native Americans
                 * only once a month
                 */
                if(StringUtils.equalsIgnoreCase(isChangePlanFlowFlag, IndividualPortalConstants.TRUE)){
                    Map<String, PlanSummaryDetails> planDetailsMap = getEnrollmentDetails(getIdFromCaseNumber(caseNumber).toString(), caseNumber);
                    if(!verifyChangePlanAction(planDetailsMap)){
                        LOGGER.error("Change of plan is allowed only for recognized tribes having EN application after 30 days of last enrollment");
                        return null;
                    }
                }
                //HIX-67563 - Change Plan and Enroll for SEP
                /*long year = TSCalendar.getInstance().get(Calendar.YEAR);
                String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
                if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
                    year = Long.parseLong(coverageYear);
                }*/
                additionalDetails = getApplicantDetails(caseNumber,isChangePlanFlowFlag,changeSEPPlanFlow);
            }catch(Exception ex) {
                LOGGER.error("Error while getting the additional details  ",ex);
                persistToGiMonitor(ex);
                return additionalDetails;
            }
        }
        return additionalDetails;
    }
    
    /**
     * @param model
     * @param request
     * @param indAdditionalDetails
     * @return response
     * @throws InvalidUserException
     */
    @RequestMapping(value = "/indportal/saveAdditionalInfo", method = RequestMethod.POST)
    @GiAudit(transactionName = "Save tobacoo usage information", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
     @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public String saveAdditionalInfo( HttpServletRequest request, @RequestBody IndAdditionalDetails indAdditionalDetails ) throws InvalidUserException {
        LOGGER.debug("Saving IndPortalApplicantDetails");
        AccountUser user = userService.getLoggedInUser();
        if(user==null){
            throw new GIRuntimeException("Unauthorized");
        }

        int activeModuleId = user.getActiveModuleId();
        String response=IndividualPortalConstants.FAILURE;
        AppGroupResponse appGroupResponse = new AppGroupResponse();
        createValidationMessage(500, "Error while saving tobacco usage information", appGroupResponse);
        try{
            if(indAdditionalDetails != null){
                
                //HIX-63511 - Check if there are any eligible members
                List<IndPortalApplicantDetails> eligibleMembers = indAdditionalDetails.getApplicantDetails().get("eligiblitydetails");
                if(eligibleMembers == null || eligibleMembers.isEmpty()){
                    LOGGER.error("There are no eligible members for this household to enroll");
                    return IndividualPortalConstants.FAILURE;
                }
                //HIX-57896 - Validation for active module ID
                String caseNumber = eligibleMembers.get(0).getCaseNumber();
                validateActiveModuleId(caseNumber, activeModuleId);                 
                
                //HIX-63511 - Check if user should be allowed to enroll
                int year = getValidCoverageYear(request.getParameter(IndividualPortalConstants.COVERAGE_YEAR));
                
                verifySaveAdditionalInfoAction(caseNumber,indAdditionalDetails.isChangePlanFlow(),indAdditionalDetails.isSEPPlanFlow(),year, activeModuleId);
              				
                String updateStatus = updateSsapApplicantsAdditionalInfo(indAdditionalDetails,caseNumber);

                if(updateStatus.equalsIgnoreCase(IndividualPortalConstants.SUCCESS)){
                	if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CUSTOM_GROUPING))){
                    	String customGrouping = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CUSTOM_GROUPING);
                    	if("ON".equalsIgnoreCase(customGrouping)){
                    		return "redirectToCustomGrouping";                        
                    	}
                    }   
                	return formInd19Request(activeModuleId, caseNumber, indAdditionalDetails, year);
                }
            }
        }catch(Exception ex){
            LOGGER.error("Error proceeding to enroll with additional information ", ex);
            response = IndividualPortalConstants.FAILURE;
            createValidationMessage(500, "Error proceeding to enroll with additional information", appGroupResponse);
            
        }
        // return response;
        return platformGson.toJson(appGroupResponse);
        
    }            
   

	/**
     * @param model
     * @param request
     * @return planSummaryDetails
     * @throws AccessDeniedException
     */
    @RequestMapping(value = "/indportal/getplandetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "Get details of enrolled plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody
    /*@PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)*/
    //FIXME : Removed the permission for 1095 A user , we need to fix this later
     public PlanDetails getPlanSummary( HttpServletRequest request) throws GIException {
    	PlanDetails planDetails = new PlanDetails();
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if (StringUtils.length(StringUtils.trim(caseNumber)) > 0) {
            //HIX-57896
            validateActiveModuleId(caseNumber, -1);
           // planSummaryDetails = getEnrollmentDetails(getIdFromCaseNumber(StringUtils.trim(caseNumber)).toString(), StringUtils.trim(caseNumber));
            SsapApplicationResource applicationObj = getApplicationByCaseNumber(caseNumber);
            List<DashboardEnrollmentDTO> dashboardEnrollments = new ArrayList<>();
            getPlanSummaryDetails((int) applicationObj.getCoverageYear(), caseNumber, dashboardEnrollments, planDetails);
        }
        return planDetails;
    }
    
    
    /**
     * @param model
     * @param request 
     * @return appealsDetails
     */
    @RequestMapping(value = "/indportal/getAppeals", method = RequestMethod.POST)
    @GiAudit(transactionName = "View Appeals", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_APPEALS)
    public Map<String,List<MyAppealDetails>>  getAppealsSummary() {
        Map<String,List<MyAppealDetails>> appealsDetails = null;
        try{
            appealsDetails = getAppealsDetails();
        }catch(Exception ex){
            LOGGER.error("Error while getting the appeals History ",ex);
            persistToGiMonitor(ex);
            return appealsDetails;
        }

        return appealsDetails;
    }
    
    /**
     * @param model
     * @param request
     * @return pendingReferrals
     */
    //HIX-74354 - Removing the @RequestMapping
    //@RequestMapping(value = "/indportal/pendingReferrals", method = RequestMethod.POST)
    @GiAudit(transactionName = "View PendingReferrals", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
       @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_REFERRALS)
       public Map<String,List<PendingReferralDetails>>  getPendingReferrals()  {
        Map<String,List<PendingReferralDetails>> pendingReferrals = null;
        try{
            pendingReferrals = getPendingReferral();
        }catch(Exception ex){
            LOGGER.error("Error While get the pending referral details ",ex);
            persistToGiMonitor(ex);
            return pendingReferrals;
        }
        return pendingReferrals;
    }

    /**
     * @param model
     * @param request
     * @return URL
     * @throws GIException
     */
    @RequestMapping(value = "/individual/preferences", method = RequestMethod.GET)
    @GiAudit(transactionName = "Get comm pref for user", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
       public String  getPreferencePageForIndSignup(Model model) throws GIException {
        AccountUser user  = null;
        
        try{
            user = userService.getLoggedInUser();
            //Pre-population of preferences page
            prepopulateCommPrefPage(model, user.getActiveModuleId());	
        }catch(Exception ex){
            persistToGiMonitor(ex);
        }      
        
        return "account/routing";
    }
    
	@RequestMapping(value = "/indportal/preferences", method = RequestMethod.GET)
	@PreAuthorize(IndividualPortalConstants.PORTAL_MANAGE_PREFERENCES)
	@GiAudit(transactionName = "Get contact preferences for the household", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	@ResponseBody
	public PreferencesDTO getPreferences(@RequestParam(required = false, defaultValue = "false") boolean editMode) {
		AccountUser user = null;
		PreferencesDTO communicationPreferenceDTO = null;

		try {
			user = userService.getLoggedInUser();
			// population of preferences page
			communicationPreferenceDTO = getHouseholdPreferences(user.getActiveModuleId(), editMode);
		} catch (Exception ex) {
			LOGGER.error("Unable to retrieve household preferences", ex);
			persistToGiMonitor(ex);
		}

		return communicationPreferenceDTO;
	}
    
    	/**
     * @param model
     * @param requests
     * @param indPortalAddlnRegDetails
     * @return SUCCESS or FAILURE
     */
    @RequestMapping(value = "/saveContactPref", method = RequestMethod.POST)
    @GiAudit(transactionName = "Save comm pref for user", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody
    public String saveAdditionalContact(@RequestBody IndPortalAddlnRegDetails indPortalAddlnRegDetails ){
        AccountUser user  = null;
        Household household = null;
        
        try{
            user = userService.getLoggedInUser();
            household = this.getConsumerUtil().getHouseholdRecord(user.getActiveModuleId());
            
            PreferencesDTO contactPreferenceDTO = convertToContactPreferenceDTO(indPortalAddlnRegDetails);
            IndPortalValidationDTO indPortalValidationDTO = validateCommunicationPrefData(contactPreferenceDTO);            
            if(!indPortalValidationDTO.getStatus()){
            	LOGGER.debug(indPortalValidationDTO.getErrors().toString());
            	return IndividualPortalConstants.FAILURE;
            }
            
            populateHouseholdFromPreference(contactPreferenceDTO, household, user);            
            household = this.getConsumerUtil().saveHouseholdRecord(household);
            if (household == null) {
                LOGGER.error("Error saving Household to CMR_HOUSEHOLD table");
                throw new GIException("CMR_HOUSEHOLD could not be created.");
            }
            if(!IndividualPortalConstants.VALID_INDIVIDUAL_PORTAL_LOGIN_ROLES.contains(user.getActiveModuleName().toUpperCase())){
                throw new InvalidUserException("User does not have permission to see Individual Portal Dashboard");
            }
        }
        catch(GIException | IOException | URISyntaxException | ParseException e) {
            LOGGER.error("Error while saving contact preferences: ",e);
            persistToGiMonitor(e);
            return IndividualPortalConstants.FAILURE;
        } catch (InvalidUserException e) {
            LOGGER.error("Exception while getting user",e);
            persistToGiMonitor(e);
            return IndividualPortalConstants.FAILURE;
        }
        return IndividualPortalConstants.SUCCESS; 

    }
    
    @RequestMapping(value = "/indportal/preferences", method = RequestMethod.POST)
    @GiAudit(transactionName = "Update Communication Preferences for user", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_MANAGE_PREFERENCES)
    @ResponseBody
    public String savePreferences(HttpServletRequest request, @RequestBody PreferencesDTO communicationPreferenceDTO) {
    		LOGGER.debug("Update Communication Preference: START");
    		String result = IndividualPortalConstants.FAILURE;      
           
    		try{
    			AccountUser user = userService.getLoggedInUser();
    			IndPortalValidationDTO indPortalValidationDTO = validateCommunicationPrefData(communicationPreferenceDTO);
    			if(!indPortalValidationDTO.getStatus()){
    				LOGGER.debug(indPortalValidationDTO.getErrors().toString());
    				return IndividualPortalConstants.FAILURE;
    			}
    			Household household = this.getConsumerUtil().getHouseholdRecord(user.getActiveModuleId());
    			boolean textMeFromDb = false;
    			boolean paperLess1095PreferenceUpdated = false;
    			boolean textMeUpdated = false;
    			if(household != null) {
    				textMeFromDb = null!=household.getTextMe()?household.getTextMe():false;
    				if(null==household.getOptInPaperless1095()||(null!=household.getOptInPaperless1095()&& (household.getOptInPaperless1095() != communicationPreferenceDTO.getOptInPaperless1095()))) {
    					paperLess1095PreferenceUpdated = true;
    				}
    				if((null==household.getTextMe()&&communicationPreferenceDTO.getTextMe())||(null!=household.getTextMe()&& (household.getTextMe() != communicationPreferenceDTO.getTextMe()))) {
    					textMeUpdated = true;
    				}
    				populateHouseholdFromPreference(communicationPreferenceDTO, household, user);
    				household = this.getConsumerUtil().saveHouseholdRecord(household);
    			}
               
    			if(household == null) {
    				LOGGER.error("Error saving Household to CMR_HOUSEHOLD table");
    				throw new GIException("CMR_HOUSEHOLD could not be created.");
    			}
               
    			/* HIX-101055 */
               
    			String commNotificationLiteral = null;
    			if(communicationPreferenceDTO.getPrevPrefCommunication() != null) {
    				Map<String, String> logEventParams = new HashMap<String, String>();
    				commNotificationLiteral = fetchPrefCommunicationLiteral(communicationPreferenceDTO.getPrevPrefCommunication());
    				logEventParams.put(IndividualPortalConstants.PREV_PREF_COMMUNICATION, commNotificationLiteral);
    				logContactMethodPreferenceEvent(communicationPreferenceDTO, logEventParams,request);
    				GiAuditParameterUtil.add(new GiAuditParameter(IndividualPortalConstants.PREFERENCES_PREV_NOTIFICATION, commNotificationLiteral));
    				
    			}
				if (!communicationPreferenceDTO.isEditMode()) {
					Map<String, String> logEventParams = new HashMap<String, String>();
					logContactMethodPreferenceEvent(communicationPreferenceDTO, logEventParams, request);
					
				}
				if (textMeUpdated) {
					Map<String, String> logEventParams = new HashMap<String, String>();
					logTextMePreferenceEvent(request, communicationPreferenceDTO, textMeFromDb, logEventParams);
				}
    			
    			if (communicationPreferenceDTO.isMailingAddressUpdated()) {
    				Map<String, String> logEventParams = new HashMap<String, String>();
					logIndividualAppEvent(IndividualPortalConstants.APPEVENT_PREFERENCES,
							IndividualPortalConstants.CONTACT_INFORMATION_UPDATE, logEventParams, request);
				} 
    			if (paperLess1095PreferenceUpdated) {
    				Map<String, String> logEventParams = new HashMap<String, String>();
					log1095PreferenceEvent(request, communicationPreferenceDTO, user, logEventParams);
				} 
				
    			result = IndividualPortalConstants.SUCCESS;
   
    		} catch (InvalidUserException e) {
    			LOGGER.error("Exception while getting user",e);
    			persistToGiMonitor(e);
    		} catch(Exception e) {
    			LOGGER.error("Error while updating Communication preferences: ",e);
    			persistToGiMonitor(e);
    		}
    		LOGGER.info("Update Communication Preference: END ");
    		return result;
	}

	private void logTextMePreferenceEvent(HttpServletRequest request, PreferencesDTO communicationPreferenceDTO,
			boolean textMeFromDb, Map<String, String> logEventParams) {
		if (communicationPreferenceDTO.getTextMe()) {
			logEventParams.put(IndividualPortalConstants.TEXT_MESSAGING_PREF, "Enabled");
		} else {
			logEventParams.put(IndividualPortalConstants.TEXT_MESSAGING_PREF, "Disabled");
		}
		if (communicationPreferenceDTO.getTextMe() != textMeFromDb) {
			if (textMeFromDb) {
				logEventParams.put(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV, "Enabled");
			} else {
				logEventParams.put(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV, "Disabled");
			}
		}
		
		logIndividualAppEvent(IndividualPortalConstants.APPEVENT_PREFERENCES,
				IndividualPortalConstants.PREFERENCES_DATA_UPDATE, logEventParams, request);
	}

	private void logContactMethodPreferenceEvent(PreferencesDTO communicationPreferenceDTO,
			Map<String, String> logEventParams, HttpServletRequest request) {
		String commNotificationLiteral;
		commNotificationLiteral = fetchPrefCommunicationLiteral(
				communicationPreferenceDTO.getPrefCommunication());
		logEventParams.put(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION,
				fetchPrefCommunicationLiteral(communicationPreferenceDTO.getPrefCommunication()));
		logIndividualAppEvent(IndividualPortalConstants.APPEVENT_PREFERENCES,
				IndividualPortalConstants.PREFERENCES_DATA_UPDATE, logEventParams, request);
		GiAuditParameterUtil.add(new GiAuditParameter(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION,
				commNotificationLiteral));
	}

	private void log1095PreferenceEvent(HttpServletRequest request, PreferencesDTO communicationPreferenceDTO,
			AccountUser user, Map<String, String> logEventParams) {
		if (communicationPreferenceDTO.getOptInPaperless1095()) {
			logEventParams.put(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE,
					IndividualPortalConstants.PREF_COMMUNICATION_SECURE_EMAIL);
		} else {
			logEventParams.put(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE,
					IndividualPortalConstants.PREF_COMMUNICATION_SECURE_EMAIL_AND_MAIL);
		}
		logEventParams.put(IndividualPortalConstants.UPDATED_BY,
				user.getFirstName() + " " + user.getLastName());
		String changeDate = dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, new TSDate());
		logEventParams.put(IndividualPortalConstants.PREFERENCES_CHANGE_DATE, changeDate);
		logIndividualAppEvent(IndividualPortalConstants.APPEVENT_PREFERENCES,
				IndividualPortalConstants.A1095_RECEIPT_PREFERENCE, logEventParams, request);
	}
    
    /**
     * @param model
     * @param request
     * @param planSummaryDetails
     * @return response
     */
    @RequestMapping(value = "/indportal/payforhealth", method = RequestMethod.POST)
    @GiAudit(transactionName = "Pay for health plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
     @ResponseBody
     @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public String payForHealth( HttpServletRequest request, @RequestBody PlanSummaryDetails planSummaryDetails) {
        LOGGER.debug("IndPortalApplicantDetails");
        EnrollmentPaymentDTO enrollmentPaymentDTO = null;
        String response = null;
        if(planSummaryDetails !=null){
            try {
                
                //HIX-57896
                if (planSummaryDetails.getCaseNumber() != null) {
                    validateActiveModuleId(planSummaryDetails.getCaseNumber(), -1);
                }
                
                enrollmentPaymentDTO = getEnrollmentPaymentDto(planSummaryDetails);

                HttpSession session = request.getSession();
                String planType = planSummaryDetails.getTypeOfPlan();
                if(StringUtils.equalsIgnoreCase("health",planType)){
                    session.setAttribute("finEnrlHltDataKey", enrollmentPaymentDTO);
                    response = "finEnrlHltDataKey";
                }else if(StringUtils.equalsIgnoreCase("Dental",planType)){
                    session.setAttribute("finEnrlDntDataKey", enrollmentPaymentDTO);
                    response = "finEnrlDntDataKey";
                }


            }catch(Exception ex) {
                LOGGER.error("Error while getting the additional details  ",ex);
                return response;
            }
        }
        return response;
    }
    
    /**
     * @param model
     * @param request
     * @param indDisenroll
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/terminateDentalPlan", method = RequestMethod.POST)
    @GiAudit(transactionName = "Terminate dental plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String disenrollFromDental(@RequestBody IndDisenroll indDisenroll) {
        String responseVal = IndividualPortalConstants.FAILURE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
        try {
            AccountUser user = userService.getLoggedInUser();
            
            String dentalEnrolId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getDentalEnrollmentId());
            indDisenroll.setDentalEnrollmentId(dentalEnrolId);
            //indDisenroll.setApplicationId(Long.valueOf(getIdFromCaseNumber(indDisenroll.getCaseNumber()).toString()));
            if(null !=  indDisenroll.getTerminationDate()){
                indDisenroll.setTerminationDate(formatter.format(dateFormat.parse(indDisenroll.getTerminationDate())));
            }else{
                indDisenroll.setTerminationDate(formatter.format(getTerminationDateOnDisenrollment(indDisenroll.getTerminationDateChoice(),formatter.parse(indDisenroll.getCoverageStartDate()))));
            }
            indDisenroll.setUserName(user.getUserName());
            String reasonCode = indDisenroll.getReasonCode();
            
            for (DisenrollReasonEnum c : DisenrollReasonEnum.class.getEnumConstants()) {
                if (c.name().equalsIgnoreCase(indDisenroll.getReasonCode())) {
                    indDisenroll.setReasonCode(c.value());
                    break;
                }
            }
            responseVal = csrOverrideUtility.invokeDentalDisenrollment(indDisenroll); 
            
            if (responseVal.equalsIgnoreCase(IndividualPortalConstants.SUCCESS)) {
                String saveComments = null;
                if (indDisenroll.getDentalEnrollmentId() != null) {
                	
                	String comment = "DentalDisenrollmentReason:"+reasonCode;
                    if(StringUtils.equalsIgnoreCase(reasonCode, DisenrollReasonEnum.OTHER.name()) && StringUtils.isNotBlank(indDisenroll.getOtherReason())){
                    	comment = comment.concat(":"+indDisenroll.getOtherReason());
                    }
                    saveComments = saveComments(dentalEnrolId, IndividualPortalConstants.ENROLLMENT, comment);
                	
                    if (!StringUtils.equalsIgnoreCase(saveComments, IndividualPortalConstants.SUCCESS)) {
                        LOGGER.info("Unable to store the comments on table while disenrolling Dental enrollments");
                    }
                }
            }else {
                responseVal = IndividualPortalConstants.FAILURE;
            }
            
        }catch(ParseException e) {
            LOGGER.error("Parse Exception occured",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }
        catch(GIException e) {
            LOGGER.error("Error while dis-enrollment: ",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        } catch (InvalidUserException e) {
            LOGGER.error("Exception while getting logged-in user",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }                 
        return responseVal;
    }  
    
    /**
     * HIX-69276 Provide ability to disenroll from QHP only
     * 
     * @author Nikhil Talreja
     *  
     * @param indDisenroll
     * @return
     */
    @RequestMapping(value = "/indportal/terminateHealthPlan", method = RequestMethod.POST)
    @GiAudit(transactionName = "Terminate health plan", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String disenrollFromHealth(@RequestBody IndDisenroll indDisenroll) {
        String responseVal = IndividualPortalConstants.FAILURE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
        try {
            AccountUser user = userService.getLoggedInUser();
            
            String healthEnrolId = ghixJasyptEncrytorUtil.decryptStringByJasypt(indDisenroll.getHealthEnrollmentId());
            indDisenroll.setHealthEnrollmentId(healthEnrolId);
            
            if(null !=  indDisenroll.getTerminationDate()){
                indDisenroll.setTerminationDate(formatter.format(dateFormat.parse(indDisenroll.getTerminationDate())));
            }else{
                indDisenroll.setTerminationDate(formatter.format(getTerminationDateOnDisenrollment(indDisenroll.getTerminationDateChoice(),formatter.parse(indDisenroll.getCoverageStartDate()))));
            }
            indDisenroll.setUserName(user.getUserName());
            String reasonCode = indDisenroll.getReasonCode();
            
            for (DisenrollReasonEnum c : DisenrollReasonEnum.class.getEnumConstants()) {
                if (c.name().equalsIgnoreCase(indDisenroll.getReasonCode())) {
                    indDisenroll.setReasonCode(c.value());
                    break;
                }
            }
            responseVal = csrOverrideUtility.invokeHealthDisenrollment(indDisenroll); 
            
            if (responseVal.equalsIgnoreCase(IndividualPortalConstants.SUCCESS)) {
                String saveComments = null;
                if (indDisenroll.getHealthEnrollmentId() != null) {
                	
                	String comment = "HealthDisenrollmentReason:"+reasonCode;
                    if(StringUtils.equalsIgnoreCase(reasonCode, DisenrollReasonEnum.OTHER.name()) && StringUtils.isNotBlank(indDisenroll.getOtherReason())){
                    	comment = comment.concat(":"+indDisenroll.getOtherReason());
                    }
                    saveComments = saveComments(healthEnrolId, IndividualPortalConstants.ENROLLMENT, comment);
                	
                    if (!StringUtils.equalsIgnoreCase(saveComments, IndividualPortalConstants.SUCCESS)) {
                        LOGGER.info("Unable to store the comments on table while disenrolling Health enrollments");
                    }
                }
                
                if (NumberUtils.isNumber(indDisenroll.getDentalAptc()) && 
                        Float.parseFloat(indDisenroll.getDentalAptc()) > 0) {
                    AptcUpdate aptcUpdate = new AptcUpdate();
                    aptcUpdate.setCaseNumber(indDisenroll.getCaseNumber());
                    aptcUpdate.setHealthEnrollmentId(null);
                    aptcUpdate.setDentalEnrollmentId(indDisenroll.getDentalEnrollmentId());
                    aptcUpdate.setAptcAmt(0);
                    aptcUpdate.setTerminationDate(formatter.parse(indDisenroll.getTerminationDate()));
                    responseVal = changeElectedAPTC(aptcUpdate, user);
                }
                
                if(StringUtils.isNotBlank(indDisenroll.getCoverageStartDate())){                	
                	int year = Integer.parseInt(indDisenroll.getCoverageStartDate().split("/")[2]);
                	List<EnrollmentDataDTO> enrollmentDataDTOList = getEnrollmentbyhoByHouseholdcaseids(year);
                	List<EnrollmentDataDTO> enrollmentForHealth = new ArrayList<EnrollmentDataDTO>(3);
                	for (EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOList) {
            			if(portalEnrollmentUtility.isEnrollmentActive(enrollmentDataDTO)){
            				if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
            					enrollmentForHealth.add(enrollmentDataDTO);
            				}
            			}            			
            		}              	
                }                
                
            }else {
                responseVal = IndividualPortalConstants.FAILURE;
            }
            
        }catch(ParseException e) {
            LOGGER.error("Parse Exception occured",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }
        catch(GIException e) {
            LOGGER.error("Error while health dis-enrollment: ",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        } catch (InvalidUserException e) {
            LOGGER.error("Exception while getting logged-in user",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }                
        return responseVal;
    }

    
    /**
     * Story HIX-41023 - CSR Overrides: CSR can choose to open a special
     * enrollment period for the household
     * 
     * Sub-Task HIX-54283 - Create getSepDetails API
     * 
     * Gets the SEP details and populates IndPortalSpecialEnrollDetails
     * DTO. This DTO is read on the Ind Portal during CSR over ride for updating
     * SEP details
     * 
     * @author Nikhil Talreja
     * 
     * @return IndPortalSpecialEnrollDetails
     * @param caseNumber - SSAP Case number for which SEP details need to be fetched
     *
     * @param request
     * @return
     * @throws GIException
     */
    @RequestMapping(value = "/indportal/getSepDetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to get SEP details", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
       @ResponseBody
    //@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_COVERAGE_DATE_PERMISSION)
    public IndPortalSpecialEnrollDetails getSepDetails(
            HttpServletRequest request) throws GIException {

        
        try {
            return getSepDetails(request.getParameter(IndividualPortalConstants.CASE_NUMBER));

        } catch (Exception ex) {
            LOGGER.error(
                    "Error while fetching the special enrollment details",
                    ex);
            return null;
        }
    }
    
    /**
     * Story HIX-41023 - CSR Overrides: CSR can choose to open a special
     * enrollment period for the household
     * 
     * Sub-Task HIX-54281 - Create opensep endpoint which accepts the dto and
     * maps it to the sep creation api
     * 
     * Saves the SEP details in SSAP_APPLICATION_EVENTS table
     * 
     * @author Nikhil Talreja
     * 
     * @param indPortalSpecialEnrollDetails
     *            - DTO Object containing details to be saved
     *            
     * @return String - SUCCESS/FAILURE           
     * 
     *
     * @param request
     * @param indPortalSpecialEnrollDetails
     * @return
     * @throws GIException
     */
    @RequestMapping(value = "/indportal/saveSepDetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to open SEP for household", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    //@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_COVERAGE_DATE_PERMISSION)
    public String saveSepDetails(
            HttpServletRequest request,
            @RequestBody IndPortalSpecialEnrollDetails indPortalSpecialEnrollDetails) throws GIException {

        String response = IndividualPortalConstants.SUCCESS;

        try {
            response = saveSEPDTO(indPortalSpecialEnrollDetails);
            if(indPortalSpecialEnrollDetails.isEditSep()){
                logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_OPEN_SEP,null,request);
            }else{
                logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_CHANGE_COVERAGE_START_DATE,null,request);
			}
            return response;
        } 
        catch (Exception ex) {
            LOGGER.error(
                    "Error while saving the special enrollment details",
                    ex);
            response = IndividualPortalConstants.FAILURE;
            persistToGiMonitor(ex);
        }

        return response;
    }
    
    /**
     * @param model
     * @param request
     * @param reinstateEnrollmentDTO
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/reinstateenrollment", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to re-instate cancelled/terminated enrollments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.IND_PORTAL_REINSTATE_ENROLLMENT)
       @ResponseBody
       public String reinstateEnrollment( HttpServletRequest request, @RequestBody ReinstateEnrollmentDTO reinstateEnrollmentDTO) {
        String responseVal = IndividualPortalConstants.FAILURE;
        AccountUser user = null;
        Long ssapOrEnrollmentId = null;
        try {
            user = userService.getLoggedInUser();
            //HIX-57896
            String caseNumber = reinstateEnrollmentDTO.getCaseNumber();
            validateActiveModuleId(caseNumber, user.getActiveModuleId());
            boolean reinstateBothDentalAndHealth = false;
            if(null != reinstateEnrollmentDTO.getReinstateHealth() && reinstateEnrollmentDTO.getReinstateHealth() && 
                    null != reinstateEnrollmentDTO.getReinstateDental() && reinstateEnrollmentDTO.getReinstateDental()) {
                reinstateBothDentalAndHealth = true;
                ssapOrEnrollmentId = Long.valueOf(getIdFromCaseNumber(caseNumber).toString());
            } else {
                //Added null check for getReinstateHealth
                String enrollmentId = null;
                if(BooleanUtils.isTrue(reinstateEnrollmentDTO.getReinstateHealth())){
                    enrollmentId = reinstateEnrollmentDTO.getHealthEnrollmentId();
                }
                else{
                    enrollmentId = reinstateEnrollmentDTO.getDentalEnrollmentId();
                }
                if(enrollmentId != null)
                {
                	ssapOrEnrollmentId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentId));
                }
            }
            if(ssapOrEnrollmentId != null) {
	            responseVal = csrOverrideUtility.reinstateEnrollment(ssapOrEnrollmentId, reinstateBothDentalAndHealth, user.getUserName());
	            logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_REINSTATE_ENROLLMENTS,null,request);
	            
	            //HIX-69290 :: call to update Dental APTC amount
	            if (!"CA".equalsIgnoreCase(stateCode) && StringUtils.equalsIgnoreCase(responseVal, IndividualPortalConstants.SUCCESS) && 
	                    null != reinstateEnrollmentDTO.getReinstateHealth() && reinstateEnrollmentDTO.getReinstateHealth()) {
	                updateAptcOnReinstate(reinstateEnrollmentDTO, user, caseNumber);
	            }
            }
            
        } catch (InvalidUserException e) {
            LOGGER.error("Unable to get logged in user during reinstating enrollment", e);
        } catch(Exception gir){
            LOGGER.error("Reinstatement is not successful. ", gir);
        }
        return responseVal;
    }
    
    /**
     * @param request
     * @param caseNumber
     * @param enrollmentId
     * @param planType
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/reinstate", method = RequestMethod.POST)
    @GiAudit(transactionName = "Re-instate cancelled/terminated enrollments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.IND_PORTAL_REINSTATE_ENROLLMENT)
    @ResponseBody
    public String reinstate(HttpServletRequest request, @RequestParam(value = "caseNumber", required = true) String caseNumber, 
    		@RequestParam(value = "enrollmentId", required = true) String enrollmentId, @RequestParam(value = "planType", required = true) String planType) 
    {
    	
    	ReinstateEnrollmentDTO reinstateEnrollmentDTO = new ReinstateEnrollmentDTO();
    	reinstateEnrollmentDTO.setCaseNumber(caseNumber);
    	if (planType != null && planType.trim().length() > 0) {
    		if(planType.equalsIgnoreCase("Health")) {
    			reinstateEnrollmentDTO.setReinstateHealth(true);
    			reinstateEnrollmentDTO.setHealthEnrollmentId(enrollmentId);
    		} else if(planType.equalsIgnoreCase("Dental")) {
    			reinstateEnrollmentDTO.setReinstateDental(true);
    			reinstateEnrollmentDTO.setDentalEnrollmentId(enrollmentId);
    		}
    	}
    	
    	return reinstateEnrollment(request, reinstateEnrollmentDTO);
    }

 
    /**
     * @param model
     * @param request
     * @return applicationEnrollmentDetails
     */
    @RequestMapping(value = "/indportal/getApplicationEnrollmentDetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to get application enrollment details", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @PreAuthorize(IndividualPortalConstants.IND_PORTAL_REINSTATE_ENROLLMENT)
    @ResponseBody
     public Map<String, String> getApplicationEnrollmentDetails( HttpServletRequest request) {

        Map<String, String> applicationEnrollmentDetails = new HashMap<String, String>();
        try {
            //HIX-57896
            String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
            validateActiveModuleId(caseNumber, -1);
            long applicationId = Long.parseLong(getIdFromCaseNumber(caseNumber).toString());
            applicationEnrollmentDetails = getEnrollmentsForReinstatement(applicationId, caseNumber);
        } catch (Exception e) {
            LOGGER.error("Unable to get application enrollment details : ", e);
        }
        return applicationEnrollmentDetails;
    }
    
    /**
     * @param model
     * @param request
     * @param indDisenroll
     * @return responseVal
     */
    @RequestMapping(value = "/indportal/disenrollforsep", method = RequestMethod.POST)
    @GiAudit(transactionName = "Disenroll from SEP", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String disenrollforsep( HttpServletRequest request, @RequestBody IndDisenroll indDisenroll) {
        String responseVal = IndividualPortalConstants.FAILURE;
        SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
        LOGGER.debug("Sep disenroll flow enabled");
        try {
            AccountUser user = userService.getLoggedInUser();
            int activeModuleId = user.getActiveModuleId();
            String caseNumber = indDisenroll.getCaseNumber();
            
            //HIX-57896
            validateActiveModuleId(caseNumber, activeModuleId);
            
            long year = TSCalendar.getInstance().get(Calendar.YEAR);
            String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
            if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
                year = Long.parseLong(coverageYear);
            }
            //Current Enrolled application in EN state
            SsapApplication application = getEnrolledSsapIdForCoverageYearAndHousehold(new BigDecimal(activeModuleId),year);
            //HIX-86407
            if(null == application) {
        	   throw new GIException("No Current Enrolled appplication found" 
        	   		+ " for household: " + activeModuleId + " and coverage year: "+ year);
           }
            
            //HIX-68967
            Object applicationId = getIdFromCaseNumber(caseNumber);
            LocalDate terminationDate = calculateTerminationDateForSEP(caseNumber,applicationId);
            
            indDisenroll.setUserName(user.getUserName());
            indDisenroll.setApplicationId(Long.valueOf(applicationId.toString()));
            indDisenroll.setCurrentEnrolledApplicationCaseNumber(application.getCaseNumber());
            indDisenroll.setCurrentEnrolledApplicationId(application.getId());
            indDisenroll.setTerminationDate(formatter.format(terminationDate.toDate()));
            //lost coverage value
            indDisenroll.setReasonCode("14");
            responseVal = csrOverrideUtility.invokeDisenrollmentForSep(indDisenroll); 
        }catch(Exception e) {
            LOGGER.error("Error while dis-enrollment: ",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }                 
        return responseVal;
    }  
    
    /**
     * Story HIX-48237
     * As an Exchange, allow consumer (House hold) to 
     * change the Elected APTC Amount
     * 
     * Sub-task HIX-57890 
     * Create api to accept enrollment id, elected aptc 
     * from plan summary page    
     * 
     * Makes the API call to change the elected APTC
     * for the enrollment
     * 
     * @param model
     * @param request
     * @param aptcUpdate
     * @return SUCCESS or FAILURE
     * @author Nikhil Talreja
     */
    @RequestMapping(value = "/indportal/updateaptc", method = RequestMethod.POST)
    @GiAudit(transactionName = "Update elected APTC", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public String updateAptc(@RequestBody AptcUpdate aptcUpdate){
        
        LOGGER.debug("Calling enrollment API to change elected APTC");
        try {
            
            //Retrieve values from SSAP_APPLICATIONS
            Object[] values = getIdHHidMaxAptc(aptcUpdate.getCaseNumber());
            BigDecimal cmrHouseholId = new BigDecimal(values[1].toString());
            BigDecimal maxAptc = new BigDecimal("0.0");
            if(values[IndividualPortalConstants.TWO] != null){
                maxAptc = new BigDecimal(values[IndividualPortalConstants.TWO].toString());
            }
            
            //Compare household id from values with active module id
            AccountUser user = userService.getLoggedInUser();
            if(user.getActiveModuleId() != cmrHouseholId.intValue()){
                throw new GIException("CMR household Id does not match active module Id");
            }
            
            //Check that elected APTC is more than 0 and less than max APTC
            if(aptcUpdate.getAptcAmt() < 0 || 
                    aptcUpdate.getAptcAmt() > maxAptc.floatValue()){
                throw new GIException("Elected APTC amount is not valid. It should be more than 0 and less than max APTC");
            }
            
            return changeElectedAPTC(aptcUpdate, user);
            
        }catch(Exception e) {
            LOGGER.error("Error while changing elected APTC: ",e);
            persistToGiMonitor(e);
            return IndividualPortalConstants.FAILURE;
        }                 
        
    }
    
    /**
     * Story HIX-57777
     * IEX: Individual Portal, CSR Overrides, Pre screener 
     * technical enhancements, optimizations and improvisations
     * 
     * Sub-task HIX-59136 
     * Create API to retrieve favorite plan details or enrolled plan details    
     * 
     * Makes the API call to fetch the favorite plan details 
     * and selected enrollment details
     * 
     * @param httpServletRequest
     * @param planTileRequest
     * @return planTile
     * @author Sunil Sahoo
     */
    @RequestMapping(value = "/indportal/fillPlanTile", method = RequestMethod.POST) 
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_VIEW_AND_PRINT_PERMISSION)
    public PlanTile fetchPlanTileDetails(@RequestBody PlanTileRequest planTileRequest) throws GIException {
        
        boolean isEnrolled = false;
        
        AccountUser user  = null;
        Household household = null;
        EligLead eligLead = null;

        try {
            user = userService.getLoggedInUser();
        } catch (InvalidUserException e) {
           LOGGER.error("Exception while getting logged in user",e);
        }
        if(null != user){
            LOGGER.debug("Retrieving information for activeModuleId:"+ user.getActiveModuleId());
            household = this.getConsumerUtil().getHouseholdRecord(user.getActiveModuleId());
        }
        if(null == household){
            LOGGER.debug("Household retrieved is having a null value");
        }
        else{
            eligLead = household.getEligLead();
        }
                
        if (Integer.parseInt(planTileRequest.getStage()) < IndividualPortalConstants.FIVE) {
            isEnrolled = false;
        }else if (Integer.parseInt(planTileRequest.getStage()) == IndividualPortalConstants.FIVE) {
            isEnrolled = true;
        }
        
        return setSelectedPlanDetails(planTileRequest,eligLead, household, isEnrolled);
        
    }
    
    /**
     * HIX-59752
     * Prevent Disenroll Action on an enrolled application 
     * if there is any another application in 
     * Eligibility Received status
     * 
     * Checks if SSAP exists based on CMR_HOUSEHOLD_ID, Coverage year and
     * application status
     * @param request
     * @return TRUE or FALSE
     * @author Nikhil Talreja
     */
    @RequestMapping(value = "/indportal/checkForPendingApplication", method = RequestMethod.POST)
    @GiAudit(transactionName = "Check for pending application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String checkForPendingApplication(HttpServletRequest request){
        
        try{
            LOGGER.info("Checking for pending application");
            AccountUser user = userService.getLoggedInUser();
            String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
            if(getSsapByStatusAndCoverageYear(user.getActiveModuleId(), coverageYear)){
                return "true";
            }
        }
        catch(Exception e) {
            LOGGER.error("Error while checking for pending application: ",e);
            persistToGiMonitor(e);
            return IndividualPortalConstants.FAILURE;
        }   
        
        return "false";
    }
    
    /**
     * HIX-60300
     * Integrate with enrollment api to check if there is a valid enrollment
     * 
     * Checks if a valid enrollment exists for an SSAP
     * 
     * @author Nikhil Talreja
     * @param request
     * @return TRUE or FALSE
     */
    @RequestMapping(value = "/indportal/sepcheck", method = RequestMethod.POST)
    @GiAudit(transactionName = "Check for active enrollment", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
    public boolean sepCheck( HttpServletRequest request){
        LOGGER.info("Checking for active enrollment");
        //HIX-57896
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        validateActiveModuleId(caseNumber, -1);
        return individualPortalEnrollmentIntegrationService.hasActiveEnrollment(caseNumber);
    }
    
    /**
     * HIX-59164
     * Display Enrollment History for the Household
     * 
     * @return enrollmentHistory
     * @param coverageYear
     * @return
     * 
     * @author sahoo_s
     */
    @RequestMapping(value = "/indportal/enrollmenthistory", method = RequestMethod.GET)
    @GiAudit(transactionName = "Gets the enrollment history", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    //@PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    //FIXME
    public EnrollmentHistory getEnrollmentHistoryDetails(@RequestParam(required = true) int coverageYear, HttpServletRequest request, Model model){
        
        EnrollmentHistory enrollmentHistory = new EnrollmentHistory();

        try{
            if (coverageYear != 0) {
                enrollmentHistory = portalEnrollmentUtility.getEnrollmentHistory(coverageYear);
            }
            
        }
        catch(Exception e){
            LOGGER.error("Unable to get enrollment history",e);
        }
        
        return enrollmentHistory;
        
    }
    
    /**
     * HIX-69292
     * Check if there is an active application in the current coverage year after renewal process.
     * 
     * @return TRUE or FALSE
     * @param request
     * @return
     * 
     * @author Sunil Sahoo
     */
    @RequestMapping(value = "/indportal/activeApplicationCheck", method = RequestMethod.POST)
    @GiAudit(transactionName = "Check for active application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_REPORT_CHANGES_PERMISSION)
    public String activeApplicationCheck(HttpServletRequest request){
        LOGGER.info("Checking for active appliaction");
        
        int prevYear=0;
        int currYear=0;
        
        try {
            String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
            if (StringUtils.isNumeric(coverageYear)) {
                AccountUser user = userService.getLoggedInUser();
                String previousCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
                String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
                
                if(StringUtils.isNumeric(previousCoverageYear)){
                    prevYear = Integer.parseInt(previousCoverageYear);
                }
                if(StringUtils.isNumeric(currentCoverageYear)){
                    currYear = Integer.parseInt(currentCoverageYear);
                }
                
                if( Integer.parseInt(coverageYear) == prevYear){
                    SsapApplicationResource ssapApplicationResource = 
                            getApplicationForCoverageYearForPortalHome(user.getActiveModuleId(),currYear);
                    if(ssapApplicationResource!=null){
                        return "true";
                    }
                }
            }
            
        } catch (Exception e) {
            LOGGER.error("Error while checking for active application: ",e);
            persistToGiMonitor(e);
        }
        return "false";
    }
    
    /**
     * Reverses SEP/QEP denial
     * 
     * @param reverseSepQepDenial
     * @return
     */
    @RequestMapping(value = "/indportal/seps/reversedenial", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to reverse sep/qep denial", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DENIAL_PERMISSION)
    public String reverseSepDenial(@RequestBody ReverseSepQepDenial reverseSepQepDenial, HttpServletRequest request){

        String responseVal = IndividualPortalConstants.FAILURE;
        LOGGER.debug("Reverse Sep denial flow started");
        try {
            AccountUser user = userService.getLoggedInUser();
            int activeModuleId = user.getActiveModuleId();
            if (null != reverseSepQepDenial) {
                String caseNumber = reverseSepQepDenial.getCaseNumber();

                //Commenting below validation against HIX-112897
                //validateActiveModuleId(caseNumber, activeModuleId);

                responseVal = reverseSepQepDenialApp(caseNumber, user, reverseSepQepDenial.getEnrollmentEndDate());
                
                if (StringUtils.equalsIgnoreCase(responseVal, IndividualPortalConstants.SUCCESS)) {
                    //A notice will be created that is sent to the consumer about the reversal of SEP Denial 
                    csrOverrideUtility.sendSepQepGrantedNotice(reverseSepQepDenial);
                }
                logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_SEP_DENIAL,null,request);
            }
            
        }catch(Exception e) {
            LOGGER.error("Error while reverse sep denial flow : ",e);
            persistToGiMonitor(e);
            responseVal = IndividualPortalConstants.FAILURE;
        }                 
        return responseVal;
    }
    
    /**
     * Checks for active application
     * 
     * @param coverageYear
     * @return
     */
    @RequestMapping(value = "/indportal/applications/{coverageYear}/checkactive", method = RequestMethod.POST)
    @GiAudit(transactionName = "Check for active application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DENIAL_PERMISSION)
    public String checkForActiveApplication(@PathVariable(IndividualPortalConstants.COVERAGE_YEAR) String coverageYear){
        
        try{
            LOGGER.info("Checking for active application");
            if (StringUtils.isBlank(coverageYear)) {
                return IndividualPortalConstants.FAILURE;
            }
            AccountUser user = userService.getLoggedInUser();
            long ssapCount = checkForActiveApplication(user.getActiveModuleId(),coverageYear);
            if(ssapCount > 0){
                return IndividualPortalConstants.SUCCESS;
            }
        }
        catch(Exception e) {
            LOGGER.error("Error while checking for active application: ",e);
            persistToGiMonitor(e);
        }   
        return IndividualPortalConstants.FAILURE;
    }
    
    /**
     * HIX-74199
     * As an exchange system, allow consumer to cancel an 
     * application when the enrollment on the application 
     * is either terminated or cancelled
     * 
     * Checks if a valid enrollment exists for an SSAP
     * 
     * @author Nikhil Talreja
     * @param request
     * @return TRUE or FALSE
     * @throws ParseException 
     */
    @RequestMapping(value = "/indportal/checkEnrollmentsForCancellation", method = RequestMethod.POST)
    @GiAudit(transactionName = "Checks for active enrollment during cancellation of Enrolled application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize(IndividualPortalConstants.PORTAL_START_APP_PERMISSION)
    public boolean checkEnrollmentsForCancellation( HttpServletRequest request) throws ParseException{
        LOGGER.info("Checking for active enrollment");
        //HIX-57896
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        validateActiveModuleId(caseNumber, -1);
        long applicationId = Long.parseLong(getIdFromCaseNumber(caseNumber).toString());
        return checkEnrollmentsForCancellation(applicationId,caseNumber);
    }
    
    /**
     * HIX-79572 :: CSR Override Option for 
     * Overriding the Enrollment Status
     * @author Sunil Sahoo
     * @param httpServletRequest, enrollmentStatusUpdate
     * @return SUCCESS or FAILURE
     */
    @RequestMapping(value = "indportal/updateEnrollmentStatus", method = RequestMethod.POST)
    @GiAudit(transactionName = "CSR over ride to update enrollment status", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_1095_OVERRIDE')")
    public String updateEnrollmentStatusDetails(HttpServletRequest request,@RequestBody EnrollmentStatusUpdate enrollmentStatusUpdate){
       
    	LOGGER.info("Update the enrollment status");
    	String response = IndividualPortalConstants.FAILURE;
    	if (null != enrollmentStatusUpdate) {
    		if(enrollmentStatusUpdate.getLastPremiumPaidDate() != null){
    			try {    			
        		    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        		    formatter.parse(enrollmentStatusUpdate.getLastPremiumPaidDate());
        		} catch (ParseException e) { 
        			LOGGER.debug("Invalid Last Premium Paid Date : "+ enrollmentStatusUpdate.getLastPremiumPaidDate());
        			return IndividualPortalConstants.FAILURE;
        		}
    		}    		
    		
    		//String caseNumber = enrollmentStatusUpdate.getCaseNumber();
        	if (/*StringUtils.isNotBlank(caseNumber) &&*/ StringUtils.isNotBlank(enrollmentStatusUpdate.getOverrideCommentText())) {
        		//validateActiveModuleId(caseNumber, -1);
        		response = csrOverrideUtility.invokeEnrollmentStatusUpdate(enrollmentStatusUpdate);
        		
        		request.getSession().setAttribute(IndividualPortalConstants.OVERRIDE_COMMENT, enrollmentStatusUpdate.getOverrideCommentText());
        		logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_ENROLLMENT_STATUS_UPDATE,null,request);
    		}
		}
        return response;
    }
    
    /**
     * @param model
     * @param request
     * @return Map
     */
    @RequestMapping(value = "/indportal/getOverrideCommentsOfEnrollmentHistory", method = RequestMethod.POST)
    @GiAudit(transactionName = "Gets CSR over ride comments for enrollment history", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody
    public Map<String, List<OverrideComment>> getOverrideCommentsOnEnrollmentHistory(HttpServletRequest request,@RequestParam(required = true)String enrollmentId) {
        LOGGER.debug("OverrideComments");
        Map<String, List<OverrideComment>> overrideCommentsMap = new HashMap<String, List<OverrideComment>>();
        List<OverrideComment> overrideComments = new ArrayList<OverrideComment>(0);
        //HIX-57896
        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
        if(caseNumber!=null && caseNumber.trim().length()>0 && enrollmentId!=null){
            validateActiveModuleId(caseNumber, -1);
            String decryptEnrollmentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentId);
            CommentTarget commentTargets = commentTargetService.findByTargetIdAndTargetType(new Long(decryptEnrollmentId), CommentTarget.TargetName.ENROLLMENT_OVERRIDE);
            if(null != commentTargets){
                List<Comment> comments = commentTargets.getComments();
                overrideComments = csrOverrideUtility.transformComments(comments);
            }
        }
        
        
        overrideCommentsMap.put("comments", overrideComments);
        return overrideCommentsMap;
    }
    
    @RequestMapping(value = "indportal/announcements", method = RequestMethod.GET)
    @GiAudit(transactionName = "Gets the announcements list", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @ResponseBody    
    @PreAuthorize("hasPermission(#model, 'individual', 'IND_PORTAL_VIEW_AND_PRINT')")
    public List<AnnouncementDTO> getActiveAnnouncements(){
        
    	List<AnnouncementDTO> listOfAnnouncements = null;
        try{
           listOfAnnouncements = getActiveAnnouncementList();
        }
        catch(Exception e){
            LOGGER.error("Unable to get active announcements list",e);
        }
        return listOfAnnouncements;
    }
    
    
    
    /**
     * Redirects to SSO
     * 
     * @param response
     * @param key
     * @param env
     * @param id
     */
    @RequestMapping(value = "/indportal/ssoTest", method = RequestMethod.GET)
    public void ssoRedirect(HttpServletResponse response, @RequestParam(value="key", required=true) String key, @RequestParam(value="env", required=true) String env,
            @RequestParam(value="id", required=true) String id){
        try{
            String urlSuffix = null;
            if(StringUtils.equalsIgnoreCase("YHISIT",env)){
                urlSuffix = "https://idsit.ghixqa.com/hix/saml/login/alias/YHISIT?";
            }else if(StringUtils.equalsIgnoreCase("YHIE2E",env)){
                urlSuffix = "https://ide2e.ghixqa.com/hix/saml/login/alias/YHIE2E?";
            }else if(StringUtils.equalsIgnoreCase("IDSTAGE",env)){
                urlSuffix = "https://idstage.ghixtest.com/hix/saml/login/alias/IDSTAGE?";
            }else if(StringUtils.equalsIgnoreCase("idssodev",env)){
                urlSuffix = "https://idssodev.ghixqa.com/hix/saml/login/alias/idssodev?";
            }else{
                response.sendRedirect("/");
            }
            GhixSecureToken token = SAMLTokenValidator.generateToken(key, id);
            String targetUrl = urlSuffix+token.toString();
            LOGGER.info("Redirecting to:"+targetUrl);
            response.sendRedirect(targetUrl);
        }catch(Exception e){
            LOGGER.error("Exception occured ",e);
        }
    }
    
    @RequestMapping(value = "/indportal/otr", method = RequestMethod.POST)
    @GiAudit(transactionName = "Mark Application for One Touch Renewal", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ONE_TOUCH_RENEWAL)
    public String markApplicationForOTR(HttpServletRequest request, @RequestBody OTR otr){
        String response = IndividualPortalConstants.FAILURE;
        
        LOGGER.debug("Marking the ssap for One Touch Renewal");
        try {
        	if(otr != null && StringUtils.isNotBlank(otr.getCaseNumber())) {
        		OTR validatedOtr = new OTR(); 
        		portalEnrollmentUtility.determineOtrEligibility(otr.getCoverageYear(), validatedOtr, userService.getLoggedInUser());
        		if(otr.getCaseNumber().equalsIgnoreCase(validatedOtr.getCaseNumber())){
            		response = updateSSAPApplicationForOTR(otr.getCaseNumber());
            		if(IndividualPortalConstants.SUCCESS.equalsIgnoreCase(response)) {
            			logIndividualAppEvent(IndividualPortalConstants.APPEVENT_APPLICATION, IndividualPortalConstants.CSR_OVERRIDES_MARK_OTR, null, request);
            		}
        		}
        	}  
        }catch(Exception e) {
            LOGGER.error("Error while Marking Current Application for One Touch Renewal: ",e);
            persistToGiMonitor(e);
        }
        
        return response;
    }

    @RequestMapping(value = "/indportal/otr/{coverageYear}", method = RequestMethod.GET)
    @GiAudit(transactionName = "Determine Show OTR button for One Touch Renewal", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ONE_TOUCH_RENEWAL)
    public OTR showOTR(HttpServletRequest request, @PathVariable(IndividualPortalConstants.COVERAGE_YEAR) String coverageYear){
        OTR response = new OTR();
        LOGGER.debug("Determine Show OTR value for One Touch Renewal");
        try {
        	
        	portalEnrollmentUtility.determineOtrEligibility(coverageYear, response, userService.getLoggedInUser());
        	  
        }catch(Exception e) {
            LOGGER.error("Error while determining Show OTR value for One Touch Renewal: ",e);
            persistToGiMonitor(e);
        }
        
        return response;
    }
	
	@RequestMapping(value = "/indportal/enrollments/terminationDate", method = RequestMethod.POST)
	@GiAudit(transactionName = "Update termination date for terminated enrollments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_DISENROLL_PERMISSION)
    public String updateTerminationDate(@RequestBody IndDisenroll indDisenroll){
        String response = IndividualPortalConstants.SUCCESS;
        
        LOGGER.debug("Update Termination Date for enrollments");
        try {
        	if(indDisenroll != null) {
                AccountUser user = userService.getLoggedInUser();
                
        		IndPortalValidationDTO indPortalValidationDTO = validateIndDisenrollForTerminationDate(indDisenroll);
        		if(!indPortalValidationDTO.getStatus()){
        			LOGGER.debug(indPortalValidationDTO.getErrors().toString());
        			return IndividualPortalConstants.FAILURE;
        		}
        		//validateActiveModuleId(indDisenroll.getCaseNumber(), user.getActiveModuleId());
                
        		response = updateEnrollmentTerminationDate(indDisenroll, user);
        	}  
        }catch(Exception e) {
            LOGGER.error("Error while Marking Current Application for One Touch Renewal: ",e);
            persistToGiMonitor(e);
            response = IndividualPortalConstants.FAILURE;
        }
        
        return response;
    }
	
	@RequestMapping(value = "/indportal/aptcPremiumEffDate", method = RequestMethod.POST)
    @GiAudit(transactionName = "Get Effective date for APTC Slider", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
    public AptcUpdate getAptcEffectiveDate(@RequestBody AptcUpdate aptcUpdate){
        
        LOGGER.debug("Calling enrollment API to fetch APTC Premium Effective date");
        try {
        	IndPortalValidationDTO indPortalValidationDTO = validateAptcEffectiveDateData(aptcUpdate);         	
        	if(!indPortalValidationDTO.getStatus()){
        		LOGGER.debug(indPortalValidationDTO.getErrors().toString());         		
        	}else{
        		aptcUpdate.setResponse(IndividualPortalConstants.FAILURE);
        		populateAPTCPremiumEffDate(aptcUpdate);
        	}        	
        }catch(Exception e) {
            LOGGER.error("Error occured while calling enrollment API to fetch APTC Premium Effective date: ",e);
            persistToGiMonitor(e);
        }                 
        
        return aptcUpdate;
    }

	
	@RequestMapping(value = "/indportal/overrideSEPDetails", method = RequestMethod.POST)
    @GiAudit(transactionName = "Save override SEP Details", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
    @PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@ResponseBody
    public String updateOverrideSEPDetails(HttpServletRequest request, @RequestBody SEPOverrideDTO sepOverrideDTO){
        
        String response = IndividualPortalConstants.SUCCESS;

        LOGGER.debug("Calling API: save Override SEP details");
        try {
			capPortalConfigurationUtil.setActiveModuleId(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("encHouseholdid")));
        	IndPortalValidationDTO indPortalValidationDTO = validateOverrideSEPDetails(sepOverrideDTO);
        	if(!indPortalValidationDTO.getStatus()){
        		LOGGER.debug(indPortalValidationDTO.getErrors().toString());
        		return IndividualPortalConstants.FAILURE;
        	}
        	response = saveOverrideSEPDetails(sepOverrideDTO);
        	
        	if(IndividualPortalConstants.SUCCESS.equalsIgnoreCase(response)) {
        		logAppEventForOverrideSEPDetails(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_SEP_DETAILS, sepOverrideDTO);
        	}

        }catch(Exception e) {
        	LOGGER.error("Error while saving Override SEP details: ",e);
            persistToGiMonitor(e);
            response = IndividualPortalConstants.FAILURE;
        }                 
        
        return response;
    }

	@RequestMapping(value = "/indportal/getSEPEventsByApplicationType", method = RequestMethod.POST)
    @GiAudit(transactionName = "Fetch SEP Events based on SSAP Application Type", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
    @PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@ResponseBody
    public String getSEPEventsByApplicationType(HttpServletRequest request, @RequestBody SEPOverrideSEPEventsDTO sepOverrideSEPEventsDTO){
        
        String sepEvents = null;

        LOGGER.debug("Calling API to get SEP Events based on Application Type");
        try {
        	IndPortalValidationDTO indPortalValidationDTO = validateFetchSEPEvents(sepOverrideSEPEventsDTO);
        	if(!indPortalValidationDTO.getStatus()){
        		LOGGER.debug(indPortalValidationDTO.getErrors().toString());
        	}else{
        		sepEvents = fetchSEPEventsBasedOnApplicationType(sepOverrideSEPEventsDTO);
        	}        	
        	
        }catch(Exception e) {
        	LOGGER.error("Error while fetching SEP Events based on Application Type: ",e);
            persistToGiMonitor(e);
        }                 
        
        return sepEvents;
    }
	
	@ResponseBody
	@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@RequestMapping(value="/indportal/getSEPPeriodDates", method = RequestMethod.GET)
	@GiAudit(transactionName = "Get SEP Period Start and End date for an application", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	public SEPOverrideSEPDatesDTO getSEPPeriodDates(HttpServletRequest request) {
		
		LOGGER.debug("Calling API to get SEP Period start and End dates for application");
		
		SEPOverrideSEPDatesDTO sepOverrideSEPDatesDTO = null;
		try {
			
			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			if(StringUtils.isNotBlank(caseNumber)) {
			
        	sepOverrideSEPDatesDTO = fetchSEPPeriodDatesForApplication(caseNumber);
			}
        	
        }catch(Exception e) {
        	LOGGER.error("Error while fetching SEP Period start and end dates for on Application: ",e);
            persistToGiMonitor(e);
        }
		return sepOverrideSEPDatesDTO;
	}
	
	@ResponseBody
	@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@RequestMapping(value="/indportal/getEligibilityDateDetails", method = RequestMethod.GET)
	@GiAudit(transactionName = "Fetch Eligibility Start date details for CSR Override", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	public IndPortalEligibilityDateDetails getEligibilityStartDateDetails(HttpServletRequest request) {
		
		LOGGER.debug("Calling API to get existing Enrollment and APTC Effective date details for application");
		
		IndPortalEligibilityDateDetails indPortalEligibilityDateDetails = null;
		try {
			
			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			if(StringUtils.isNotBlank(caseNumber)) {
				validateActiveModuleId(caseNumber, -1);
				
				int year = TSCalendar.getInstance().get(Calendar.YEAR);
		        String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
		        if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
		            year = Integer.parseInt(coverageYear);
		        }
		        
				indPortalEligibilityDateDetails = fetchEligibilityDetailsForAppln(caseNumber, year);
			}
        	
        }catch(Exception e) {
        	LOGGER.error("Error calling API to get existing Enrollment and APTC Effective date details for application", e);
            persistToGiMonitor(e);
        }
		return indPortalEligibilityDateDetails;
	}
	
	@ResponseBody
	@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@RequestMapping(value="/indportal/saveOverrideEligibilityDate", method = RequestMethod.POST)
	@GiAudit(transactionName = "CSR Override for save Eligibility Start date", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
	public String saveOverrideEligibilityDate(HttpServletRequest request, @RequestBody IndPortalEligibilityDateDetails indPortalEligibilityDateDetails) {
		
		LOGGER.debug("Calling API to save ESD details for application");
        String response = IndividualPortalConstants.SUCCESS;

		try {
			
			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			if(StringUtils.isNotBlank(caseNumber)) {
			
	        	validateActiveModuleId(caseNumber, -1);
	        	IndPortalValidationDTO indPortalValidationDTO = validateESDInputParameters(indPortalEligibilityDateDetails);
	        	if(!indPortalValidationDTO.getStatus()){
	        		LOGGER.debug(indPortalValidationDTO.getErrors().toString());
	        		return IndividualPortalConstants.FAILURE;
	        	}
	        	
	        	response = saveESDDetailsForAppln(caseNumber, indPortalEligibilityDateDetails);
			}
        	
        }catch(Exception e) {
        	LOGGER.error("Error while calling API to save ESD details for application: ",e);
            persistToGiMonitor(e);
            response = IndividualPortalConstants.FAILURE;
        }
		return response;
	}
	
	@RequestMapping(value = "/indportal/appNotToConsider", method = RequestMethod.POST)
	@GiAudit(transactionName = "Change an application to not_to_consider", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
	@PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
	@ResponseBody public String applicationNotToConsider( HttpServletRequest request) throws GIException{
		LOGGER.debug("Update Application to not_to_consider - Method started.");
		RenewalStatusNotToConsiderResponseDTO renewalStatusNotToConsiderResponseDTO = null;
		try{
	            
	            String applicationId = request.getParameter("applicationId");
	            String type = request.getParameter("type");
	            renewalStatusNotToConsiderResponseDTO = updateAppNotToConsider(Long.parseLong(applicationId),type);            	
	            //renewalStatusNotToConsiderResponseDTO = platformGson.fromJson(response, RenewalStatusNotToConsiderResponseDTO.class);
	            
	            String updateStatus = renewalStatusNotToConsiderResponseDTO.getStatusCode().toString();
	            if(HttpStatus.OK.toString().equalsIgnoreCase(updateStatus))
                 	 return IndividualPortalConstants.SUCCESS;
                else
	            	 return IndividualPortalConstants.FAILURE;
	             
	        }catch(Exception ex){
	             persistToGiMonitor(ex);
	            return IndividualPortalConstants.FAILURE;
	        }
	     }
	    
	
	
	@RequestMapping(value="/indportal/customGrouping", method=RequestMethod.GET)
	@GiAudit(transactionName = "Get custom group information", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
	@PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
	@ResponseBody public String getCustomGrouping(HttpServletRequest request) throws GIException{
		LOGGER.debug("Individual Get customGrouping Method started.");
    	String appGroupResponseJson = null;
    	CustomGroupingResponse appGroupResponse = new CustomGroupingResponse();
    	//AppGroupResponse appGroupResponse = new AppGroupResponse();
    	try{ 
    		String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
    		validateActiveModuleId(caseNumber, -1);    					
    		SsapApplicationResource applicationObj = getApplicationByCaseNumber(caseNumber);
    		String coverageStartDate = computeCoverageStartDate(applicationObj);
    		boolean isCAProfile = "CA".equalsIgnoreCase(stateCode) ? true : false;	
    		Map<String, String> apiResponse = null;
    		if(isCAProfile){    		
	    		Household household = this.getConsumerUtil().getHouseholdRecord(applicationObj.getCmrHouseoldId().intValue());
	    		if(household == null){
	    			 throw new GIException("cmrHousehod should not be null.");
	    		 }    		
	    		Map<String, String> inputData = new HashMap<String, String>(3);
				inputData.put("externalCaseId", household.getHouseholdCaseId());
				inputData.put("enrollmentYear", String.valueOf(applicationObj.getCoverageYear()));
				inputData.put("coverageStartDate", coverageStartDate);	
				LOGGER.info("Recal API parameters {}",inputData.toString());
				
				try{
					apiResponse = fetchLatestAPTCAmount(inputData);
				}catch(Exception ex){
					LOGGER.error("Exception occured while calling ssapaptcrecalc/fetchLatestAPTCAmount api", ex);
					appGroupResponse.setStatus(400);
		    		appGroupResponse.setMessage(ex.getMessage());
		    		return platformGson.toJson(appGroupResponse);
				}
    		}
			if((isCAProfile && (apiResponse != null && (apiResponse.isEmpty() || apiResponse.containsValue("200")))) || !isCAProfile){
				Long applicationId =  getApplicationId(applicationObj);    
	    		if(applicationId != null){
	            	AppGroupRequest appGroupRequest = new AppGroupRequest();
	            	appGroupRequest.setApplicationId(applicationId);
	            	appGroupRequest.setCoverageStartDate(coverageStartDate);  
	            	appGroupResponseJson = getAppGroups(appGroupRequest);            	
	            	//appGroupResponse = platformGson.fromJson(appGroupResponseJson, AppGroupResponse.class);
	            	appGroupResponse = platformGson.fromJson(appGroupResponseJson, CustomGroupingResponse.class);
	            	appGroupResponse.setServerDate(dateToString("MM/dd/yyyy", new TSDate()));
	            	if(appGroupResponse.getStatus() != null){
	            		return platformGson.toJson(appGroupResponse);
	            	}else{
	            		encryptData(appGroupResponse);
	            		setDisenrollDialogData(appGroupResponse);
	            		appGroupResponseJson = platformGson.toJson(appGroupResponse);       		 	
	            	}               
	    		} 
			}else{
				appGroupResponse.setStatus(400);
	    		appGroupResponse.setMessage("Custom Grouping page is not available.");
	    		return platformGson.toJson(appGroupResponse);
			}
    		
    	}catch(GIException ex){
    		appGroupResponse.setStatus(400);
    		appGroupResponse.setMessage(ex.getMessage());
    		return platformGson.toJson(appGroupResponse);
    	}catch(Exception e){
    		LOGGER.error("Error occoured in getCustomGrouping method.", e);
    		appGroupResponse.setStatus(500);
    		appGroupResponse.setMessage(e.getMessage());
    		return platformGson.toJson(appGroupResponse);
    	}    	
    	return appGroupResponseJson;
    }
	
	 private void encryptData(CustomGroupingResponse appGroupResponse){
		for(Group group : appGroupResponse.getGroupList())
		{
			if(group.getGroupEnrollmentId() != null){
				Map<String,String> groupJson = encryptGroupData(group);
				group.setEncData(GhixAESCipherPool.encryptParameterMap(groupJson));
			}			
			for(Member member : group.getMemberList())
			{
				Map<String,String> memberJson = getMemberDataJson(member);
				String encData = 	GhixAESCipherPool.encryptParameterMap(memberJson);
				member.setEncData(encData);
			}
		}
	 }

	 private Map<String,String> encryptGroupData(Group group){
		 Map<String,String> groupData = new HashMap<String,String>();
		 groupData.put("groupEnrollmentId", group.getGroupEnrollmentId()!=null ? group.getGroupEnrollmentId() : StringUtils.EMPTY);
		 groupData.put("maxAptc", group.getMaxAptc()!=null?group.getMaxAptc().setScale(2, BigDecimal.ROUND_HALF_UP).toString():"0");
		return groupData;
	}

	 private Map<String,String> getMemberDataJson(Member member) {
		Map<String,String> memberData = new HashMap<String,String>();
		memberData.put("age",Integer.toString(member.getAgeAtCoverageDate()));
		 memberData.put("healthEnrollmentId",member.getHealthEnrollmentId()!=null ? member.getHealthEnrollmentId() : StringUtils.EMPTY);
		 memberData.put("memberId", member.getId());
		 memberData.put("aptc", member.getAptc()!=null?member.getAptc().setScale(2, BigDecimal.ROUND_HALF_UP).toString():"0");
		 memberData.put("dentalEnrollmentId", member.getDentalEnrollmentId()!=null ? member.getDentalEnrollmentId() : StringUtils.EMPTY);
			return memberData;
	}

	 @RequestMapping(value = "/indportal/saveCustomGrouping", method = RequestMethod.POST)
	    @GiAudit(transactionName = "Save custom group information", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
	     @ResponseBody
	    @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
	    public String saveCustomGrouping( HttpServletRequest request, @RequestBody IndAdditionalDetails indAdditionalDetails ) throws InvalidUserException {
	        LOGGER.debug("Saving IndPortalApplicantDetails");
	        AccountUser user = userService.getLoggedInUser();
	        String response=IndividualPortalConstants.FAILURE;
	        AppGroupResponse appGroupResponse = new AppGroupResponse();
	        try{ 
	        if(user==null){
	            throw new GIRuntimeException("Unauthorized");
	        }

	        int activeModuleId = user.getActiveModuleId();
		    
	        String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
	        String coverageStartDate = indAdditionalDetails.getGroup().getCoverageDate();
	        validateActiveModuleId(caseNumber, activeModuleId);                 
			 
	        if(!StringUtils.isEmpty(caseNumber)){
	        	AppGroupResponse validationRespone = null;
	        	if(!isValidGroup(indAdditionalDetails.getGroup())){
	        		createValidationMessage(400, "Invalid Group Data.", appGroupResponse);
	        		return platformGson.toJson(appGroupResponse);
	        	}
	        	if(!isValidMember(indAdditionalDetails.getGroup())){
	        		createValidationMessage(400, "Invalid Member Data.", appGroupResponse);
	        		return platformGson.toJson(appGroupResponse);
	        	}
	        	if(!"MN".equalsIgnoreCase(stateCode) && "HEALTH".equalsIgnoreCase(indAdditionalDetails.getGroup().getGroupType().toString())){
	        		validationRespone = validateAptc(indAdditionalDetails.getGroup());
	        		if(validationRespone.getStatus() != null){
	        			return platformGson.toJson(validationRespone);
	        		}
	        	}
				 
	        	String ssapApplicantsJson = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/indportal/getEligibleApplicantsByCaseNumber/"+caseNumber, user.getUsername(), HttpMethod.GET, MediaType.APPLICATION_JSON, String.class,null).getBody();
	        	List<SsapApplicantDto> applicants = platformGson.fromJson(ssapApplicantsJson, new TypeToken<List<SsapApplicantDto>>() {}.getType());
	        	validationRespone = validateMembersData(indAdditionalDetails, applicants, coverageStartDate, caseNumber);
	        	if(validationRespone.getStatus() != null){
	        		return platformGson.toJson(validationRespone);
	        	}
	        }
		    	
	        String year = coverageStartDate.split("/")[2];
	        SsapApplicationResource applicationObj = getApplicationByCaseNumber(caseNumber);
	        int applicaitonCoverageYear = (int) applicationObj.getCoverageYear();
			if(Integer.parseInt(year) != applicaitonCoverageYear && !indAdditionalDetails.getGroup().getIsRenewal()){
				throw new GIException("Enrollment start date spills to next year. Start Date: " + coverageStartDate +", Coverage Year: " + applicaitonCoverageYear);
			}
			
			
			calculateSingleEnrolGrpAptcAndSubsidy(indAdditionalDetails.getGroup(), applicationObj.getApplicationId(), applicationObj.getMaximumAPTC(), applicationObj.getMaximumStateSubsidy());
	        
	        response = formInd19Request(activeModuleId, caseNumber, indAdditionalDetails, Integer.parseInt(year));
	        } catch(GIException ex){
	        	LOGGER.warn(ex.getErrorMsg(), ex);
		    	appGroupResponse.setStatus(400);
		    	appGroupResponse.setMessage(ex.getMessage());
		    	return platformGson.toJson(appGroupResponse);
	        } catch(Exception ex){
	            LOGGER.error("Error proceeding to enroll with saving custom group information ", ex);
	            appGroupResponse.setStatus(500);
		    	appGroupResponse.setMessage(ex.getMessage());
		    	return platformGson.toJson(appGroupResponse);
	        }
	        return response;
	 }
	 
	 private int getAgeFromCoverageDate(Date coverageStartDate, Date dateOfBirth) {
		 Calendar today = Calendar.getInstance();
		 today.setTime(coverageStartDate);
		 Calendar dob = Calendar.getInstance();
		 dob.setTime(dateOfBirth);
		 Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		 if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			 age--;
		 }
		 return age;
	 }

	 private AppGroupResponse validateMembersData(IndAdditionalDetails indAdditionalDetails, List<SsapApplicantDto> eligibleMembers, String coverageStartDate, String caseNumber) throws  GIException{
		 AppGroupResponse appGroupResponse = new AppGroupResponse();
		 boolean isDental = "DENTAL".equalsIgnoreCase(indAdditionalDetails.getGroup().getGroupType().toString());
		 SsapApplicationResource applicationObj = getApplicationByCaseNumber(caseNumber);
		 String applicationType  = applicationObj.getApplicationType();
		 if(isDental && "CA".equalsIgnoreCase(stateCode) && !indAdditionalDetails.getGroup().getIsRenewal())
		 {
			 List<Member> members = indAdditionalDetails.getGroup().getMemberList();
			 List<String> childMemberGuids = new ArrayList<String>();
			 boolean hasAdult = false;
			 List<String> allChildsGuid = new ArrayList<String>();
			 for(SsapApplicantDto ssapApplicant: eligibleMembers)
			 {	
				 for(Member member : members){
					 if(member.getId().equalsIgnoreCase(ssapApplicant.getApplicantGuid())){
								 if (StringUtils.isEmpty(member.getHealthEnrollmentId()) && StringUtils.isEmpty(member.getDentalEnrollmentId()) )
								 {
									 appGroupResponse.setStatus(400);
									 appGroupResponse.setMessage("In order to enroll in a dental plan a member is required to be enrolled in Health plan first. Please make sure that all members that you are planning to enroll in a Dental plan are enrolled in a Health Plan.");
									 return appGroupResponse;
								 }
								 if(member.getAgeAtCoverageDate() >= 19 ) 
								 {
									 hasAdult = true;
								 }
								 if(member.getAgeAtCoverageDate() < 19 )
								 {
									 childMemberGuids.add(member.getId());
								 }
					 }
				 }
				 if(getAgeFromCoverageDate(DateUtil.StringToDate(coverageStartDate, "MM/dd/yyyy"), ssapApplicant.getBirthDate()) < 19){
					 allChildsGuid.add(ssapApplicant.getApplicantGuid());
				 }	
			 }

			 if(!hasAdult)
				{
				 appGroupResponse.setStatus(400);
				 appGroupResponse.setMessage("At least one adult is required to enroll in a family dental plan. Please select an least one adult to proceed with Dental Plan shopping.");
				 return appGroupResponse;
			 }				 
			 
			 if(CollectionUtils.isNotEmpty(childMemberGuids) && !childMemberGuids.containsAll(allChildsGuid)){
				 if((!indAdditionalDetails.isChangePlanFlow() && "OE".equalsIgnoreCase(applicationType) || !"OE".equalsIgnoreCase(applicationType))){
					 appGroupResponse.setStatus(400);
					 appGroupResponse.setMessage("Please enroll all kids for Dental Plan enrollment.");
					 return appGroupResponse;
				 }
			 }
		 }

		 String nonStandardConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_GROUPING_KEEP_NON_STANDARD_RELATIONSHIP_SEPARATE);
			 
			 Boolean hasStandard = false;
			 Boolean hasNonStandard = false;
			 Boolean allUnenrolled = true;
		 int nonStandardCount = 0;
		 if(!isDental && "ID".equalsIgnoreCase(stateCode) && null != nonStandardConfig && "true".equalsIgnoreCase(nonStandardConfig)) {
			 for(Member member : indAdditionalDetails.getGroup().getMemberList()){
					 if(!StringUtils.isEmpty(member.getHealthEnrollmentId())){
						 allUnenrolled = false;
					 }
				 }
				 if(allUnenrolled) {
					 for(SsapApplicantDto ssapApplicant: eligibleMembers){	
					 for(Member member : indAdditionalDetails.getGroup().getMemberList()){
							 if(member.getId().equalsIgnoreCase(ssapApplicant.getApplicantGuid()) && StringUtils.isEmpty(member.getHealthEnrollmentId()) ){
								 if(member.getRelation() == null ) {
									 hasNonStandard = true;
								 nonStandardCount++;
								 }
								 else {
									boolean isStandard = relationshipTypeService.isRelationshipStandardForCode(member.getRelation());
								 	if(isStandard) {
								 		hasStandard = true;
								 	}
								 	else {
								 		hasNonStandard = true;
							 		nonStandardCount++;
								 	}
								 }
							 }
						 }
					 }
				 if((hasStandard && hasNonStandard) || (!hasStandard && hasNonStandard && nonStandardCount > 1)) {
						 appGroupResponse.setStatus(400);
					 appGroupResponse.setMessage("This household member is not allowed to enroll in the same plan as other members of the household because of this member’s relationship with the primary member responsible for this coverage. Please enroll this member in their own plan.");
						 return appGroupResponse;
					 }
				 }
				
			 }
		 return appGroupResponse;
	}

	 private boolean hasUncoveredChild(Date coverageStartDate, Date dateOfBirth) {
		 Calendar today = TSCalendar.getInstance();
			today.setTime(coverageStartDate);
			Calendar dob = TSCalendar.getInstance();
			dob.setTime(dateOfBirth);
			Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
				age--;
			}
		
			
		return age<19;
	}

	private Boolean isValidMember(Group group){
		 for(Member member : group.getMemberList()){
			 if(StringUtils.isNotEmpty(member.getEncData())){
				 Map<String,String> reqMemberData = getMemberDataJson(member);
				 Map<String,String> originalData = GhixAESCipherPool.decryptParameterMap(member.getEncData());
				 if(reqMemberData.get("age").equals(originalData.get("age")) && reqMemberData.get("memberId").equals(originalData.get("memberId")) 
						 && reqMemberData.get("aptc").equals(originalData.get("aptc"))){
					 if(StringUtils.isEmpty(reqMemberData.get("healthEnrollmentId")) || reqMemberData.get("healthEnrollmentId").equals(originalData.get("healthEnrollmentId"))){
						 if(StringUtils.isEmpty(reqMemberData.get("dentalEnrollmentId")) || reqMemberData.get("dentalEnrollmentId").equals(originalData.get("dentalEnrollmentId"))){
					 return true;
				 }
			}
		}
			}
		}
		return false;
	}

	private Boolean isValidGroup(Group group){
		if(group.getGroupEnrollmentId() != null){
			if(StringUtils.isNotEmpty(group.getEncData())){
				Map<String,String> originalGroupData = GhixAESCipherPool.decryptParameterMap(group.getEncData());
				BigDecimal decyptedAptc = new BigDecimal(originalGroupData.get("maxAptc"));
				BigDecimal groupMaxAptc = group.getMaxAptc()!=null?group.getMaxAptc().setScale(2, BigDecimal.ROUND_HALF_UP):new BigDecimal("0");
				if(group.getGroupEnrollmentId().equalsIgnoreCase(originalGroupData.get("groupEnrollmentId"))
						&& groupMaxAptc.compareTo(decyptedAptc) == 0) {	
					return true;
				}
			}						 
		}else{
			return true;
		}
		return false;
	}

	private AppGroupResponse validateAptc(Group group) {
		AppGroupResponse appGroupResponse = new AppGroupResponse();
		Boolean isAtpcNull = false;
		Boolean isAtpcNotNull = false;
		Boolean enableInconsistentGroupAptc = false;

		String renewalInconsistentGroupHealth = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.RENEW_INCONSISTENT_GROUP_HEALTH);
		
		if ("Y".equalsIgnoreCase(renewalInconsistentGroupHealth)) {
			enableInconsistentGroupAptc = true;
		}

		for (Member member : group.getMemberList()) {
			BigDecimal aptc = null;

			if (member.getHealthEnrollmentId() != null) {
				aptc = group.getMaxAptc();
			} else {
				aptc = member.getAptc();
			}

			if (aptc != null) {
				isAtpcNotNull = true;
			} else {
				isAtpcNull = true;
			}
		}
		if (isAtpcNotNull && isAtpcNull && !enableInconsistentGroupAptc) {
			createValidationMessage(400,"Please Shop for APTC eligible members and APTC ineligible members separately.", appGroupResponse);
			return appGroupResponse;
		}
		return appGroupResponse;
	}
	
	
	 @RequestMapping(value = "/indportal/getUserPermissions", method = RequestMethod.GET)
	 @PreAuthorize(IndividualPortalConstants.PORTAL_ENROLL_APP_PERMISSION)
	 @ResponseBody
	 public String getUserPermissions(HttpServletRequest request) throws GIException{
		 LOGGER.debug("getUserPermissions methold started");
		 String response=IndividualPortalConstants.FAILURE;
		 try{
			 AccountUser user = userService.getLoggedInUser();
			 if(user == null){
				 throw new GIRuntimeException("Unauthorized");
			 }
			 Set<String> userPermissions = user.getUserPermissions();
			 response = platformGson.toJson(userPermissions);			 
		 }catch(Exception e){
			 LOGGER.error("Error while finding user permissions from logged in users.");
		 }	        
		 return response;
	 }
	 
	 private String getRequestParameters(HttpServletRequest request) {
			StringBuffer sb = new StringBuffer();
			Enumeration<String> e = request.getParameterNames();
			try {
				while( e.hasMoreElements() ){
					 String key = e.nextElement();
					 String[] val = request.getParameterValues( key );
					 
					 if(!key.equalsIgnoreCase("redirectUri")) {
						sb.append((sb.length()==0)?"?":"&");
						 sb.append(key);
						 
						 
						 sb.append("=");
						 sb.append(val[0]);
					 }
				}

			}
			catch ( Exception ex) {
				LOGGER.error("UserController:getRequestParameters:Exception", ex);
			}
			if(sb.length() > 0) {
				LOGGER.debug("Request headers from user controller:" + sb.toString());
				return sb.toString();
			}
			else {
				LOGGER.debug("Request headers from user controller:" + sb.toString());
				return "";
			}
		}

	 private boolean hasAnsweredSecurityQuestions(AccountUser user) {
			LOGGER.debug("hasAnsweredSecurityQuestions::");

			boolean result=true;

			if(StringUtils.isBlank(user.getSecurityAnswer1())
					|| StringUtils.isBlank(user.getSecurityAnswer2())
					/*|| StringUtils.isBlank(user.getSecurityAnswer3())*/){
				result=false;
			}
			LOGGER.debug("hasAnsweredSecurityQuestions::"+result);
			return result;
		}
	 
	 private String getLandingPage(AccountUser user,String roleName,HttpServletRequest request) throws GIException {
			LOGGER.debug("Get LandingPage ");
			Role activeRole = userService.getDefaultRole(user);
			//String redirectUri = request.getParameter("redirectUri");
			//LOGGER.info("Request param redirectUri ::"+redirectUri);

			String landingPage = "";

			try
			{
				//START :: Below block is added for individual signup flow

				String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
				LOGGER.info("Exchange Type ::" +exchangeType );
				if(StringUtils.isNotBlank(exchangeType)){
					if(exchangeType.equalsIgnoreCase("PHIX")){
						String referrerUrl = (String) request.getSession().getAttribute("referrerUrl");
						//String houseHoldId= (String) request.getSession().getAttribute("HOUSEHOLD_ID");// is it required here?
						//LOGGER.info("userController session.getAttribute(referrerUrl) ::" + referrerUrl);
						if(!StringUtils.isBlank(referrerUrl) ){
							//LOGGER.debug("Redirecting to Url::" + referrerUrl);
							landingPage = referrerUrl;
							return landingPage;//"redirect:"+referrerUrl;
						}

					}/*else if(currUserDefRole!= null && currUserDefRole.getDescription().equals("INDIVIDUAL")
								&& !GhixConfiguration.IS_SECURITY_QUESTIONS_REQUIRED )
						{
							String postRegistrationUrl = userService.getDefaultRole(user).getPostRegistrationUrl();
							return "redirect:"+postRegistrationUrl;
						}*/
				}
				//END :: Below block is added for individual signup flow
				boolean isSecQuestionMandatory=false; //HIX-24504 : Merged security questions page with account signup page. Note:
													  //      if the security questions page (while activation) is different page then
				                                      //      the default isSecQuestionMandatory value should be 'true'



				String isUserReg = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EXTERNAL);
				if(null!=isUserReg&&isUserReg.equalsIgnoreCase("true")){
					isSecQuestionMandatory = false;
				}

				LOGGER.debug("Is User Registration External ? :: "+ isUserReg);
				LOGGER.debug("Is Sec Question Mandatory ? :: "+ isSecQuestionMandatory);

				if(StringUtils.isBlank((roleName))){
					// if the moduleName is blank (empty , whitespace or null) then moduleName=user's default role name (in lowercase)
					roleName=StringUtils.lowerCase(userService.getDefaultRole(user).getName());
				}

				if(isSecQuestionMandatory&&!hasAnsweredSecurityQuestions(user)){
					//NOTE: the following block will executed due to the HIX-24504
					request.getSession().setAttribute("signUpRole",userService.getDefaultRole(user).getName());
					request.getSession().setAttribute("isUserFullyAuthorized", "false");
					landingPage="/account/securityquestions";
				} else {
					// HIX-29686 - Darshan Hardas : To check if it is a Duo-Security authentication
					String duoSecurityFlowFlag = request.getParameter("isDuoSecurityCleared");
					boolean isDuoSecurityFlow = ("true").equalsIgnoreCase(duoSecurityFlowFlag);
					if(isDuoSecurityFlow) { // Remove the param as User has already authenticated
						request.removeAttribute("isDuoSecurityCleared");
					}

					String redirectUri = (isDuoSecurityFlow) ? StringUtils.EMPTY : ghixGateWayService.getConversationUrl(request);

					//LOGGER.debug("redirectUri ::"+redirectUri);
					//if(userService.hasUserRole(user, roleName))
					{
						if(!StringUtils.isBlank(redirectUri)){
							landingPage =redirectUri + getRequestParameters(request);// required for rebuilding

						}else{
							activeRole=roleService.findRoleByName(StringUtils.upperCase(roleName));
							/* HIX-40546 - Changes for State Referral Account Starts*/
							if(request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL) != null && request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL).equals("Y") && roleName.equalsIgnoreCase("INDIVIDUAL"))
							{
								LOGGER.debug("Came Inside the State Referral Account Flow");
								landingPage = "/referral/activation/binduser";
							}
							else
							{
								//activerole is null, which shouldn't be the case HIX-102766
								activeRole  = (null != activeRole) ? activeRole : userService.getDefaultRole(user);
								landingPage = (null != activeRole) ? activeRole.getLandingPage() : "/";
								String pageContext = request.getParameter("pageContext");
								if(StringUtils.isNotBlank(pageContext) && StringUtils.isNotBlank(landingPage)) {
									landingPage += landingPage.indexOf("?") >= 0 ? "&pageContext="+pageContext : "?pageContext="+pageContext;
								}
								
							}
							/* HIX-40546 - Changes for State Referral Account Ends*/
						}

					}
					userService.enableRoleMenu(request,roleName);

				}

				request.getSession().setAttribute("switchToThemeName", roleName);

			}
		    catch(Exception ex){
		    	LOGGER.error("Landing Page Exception ::"+ex.getMessage(),ex);
		    	throw new GIException("Landing Page Exception ::"+ex.getMessage(),ex);
		    }
			//LOGGER.debug("Redirecting to LandingPage after rebuilding :: "+landingPage);
			return landingPage;
		}
	 
	 
	 @RequestMapping(value="/indportal/setIndividualDashboard", method=RequestMethod.GET)
	public String linkHouseholdAndRedirectToIndividualPortal(@RequestParam("householdCaseId") String householdCaseId,
			HttpServletRequest request) {
		 String userDefLandingPage = null;
		 String pageContext = request.getParameter("pageContext");
		 String previewId = request.getParameter("previewId");
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Request for dashboard with page context {}", pageContext);
		}
		 Household household = null;
		 String strExternalCaseId = null;
		 try {
			 AccountUser user  = userService.getLoggedInUser();
			if (user == null) {
				throw new GIRuntimeException(
						"Permission denied, no logged in user available for dashboard request".intern());
			}
			// resetUserState(user, request);
			LOGGER.info("linkHouseholdAndRedirectToIndividualPortal : householdCaseId: "+ householdCaseId);
			  if(householdCaseId != null) {
				  strExternalCaseId =  ghixJasyptEncrytorUtil.decryptStringByJasypt(householdCaseId);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.info("User context for dashboard request {} for case Id household {}", user.getUserName(),
							strExternalCaseId);
				}
				request.getSession(true).setAttribute("externalHouseholdCaseId", householdCaseId);
				  //Get household by external case ID
				  household = consumerEventsUtil.getHouseHoldByCaseId(strExternalCaseId);
				  if(household != null) {
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Internal household {} found for the given external case Id {}", household.getId(),
								strExternalCaseId);
					}
					if (StringUtils.equalsIgnoreCase(user.getActiveModuleName(), "INDIVIDUAL")) {
							  if (household.getUser() == null) {
							if (LOGGER.isInfoEnabled()) {
								LOGGER.info("Updating household {} with user reference {}".intern(), household.getId(),
										user.getId());
							}
								  household.setUser(user);
								  householdRepository.save(household);
							  }
							  user.setActiveModuleId(household.getId());
						  }
					  //Set household in context
					  request.getSession().setAttribute("householdId", household.getId());
						  request.getSession().setAttribute("consumerFirstName", household.getFirstName());
						  request.getSession().setAttribute("consumerlastName", household.getLastName());
				}else {
					request.getSession().setAttribute("householdId", household);
					LOGGER.error("Failed to find the internal household for the provided case id {}",
							strExternalCaseId);
					throw new GIRuntimeException("Houshehold not found, invalid householdCaseId in request");
				  }
				  request.getSession().setAttribute("householdCaseId", strExternalCaseId);
			  }else {
				LOGGER.error("Failed to find the case id, expected encrypted id");
				  throw new GIRuntimeException("Houshehold not found, invalid householdCaseId in request");
			  }
			String userActiveModuleName = user.getActiveModuleName();
			if (StringUtils.equalsIgnoreCase(userActiveModuleName, "AGENCY_MANAGER")
					|| StringUtils.equalsIgnoreCase(userActiveModuleName, "BROKER")
					|| StringUtils.equalsIgnoreCase(userActiveModuleName, "ASSISTER")
						  || (StringUtils.isNotBlank(pageContext) && (pageContext.equalsIgnoreCase("dashboard") || 
								  pageContext.equalsIgnoreCase("myEnrollments"))
							&& (StringUtils.equalsIgnoreCase(userActiveModuleName, "L1_CSR")
									|| StringUtils.equalsIgnoreCase(userActiveModuleName, "L2_CSR")
									|| StringUtils.equalsIgnoreCase(userActiveModuleName, "SUPERVISOR")
									|| StringUtils.equalsIgnoreCase(userActiveModuleName, "L0_READONLY")))) {
					  
					  Role indivRole = roleService.findRoleByName(StringUtils.upperCase("INDIVIDUAL"));
					  userDefLandingPage = indivRole.getLandingPage();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Resolved landing page {} for {}",userDefLandingPage,userActiveModuleName);
				}
					  if(StringUtils.isNotBlank(pageContext) && StringUtils.isNotBlank(userDefLandingPage)) {
					userDefLandingPage += userDefLandingPage.indexOf("?") >= 0 ? "&pageContext=" + pageContext
							: "?pageContext=" + pageContext;
					  }
					 
					  
					  if(household != null) {
						  request.getSession().setAttribute("userActiveRoleName", "INDIVIDUAL".intern());
						  request.getSession().setAttribute("switchToModuleName", "INDIVIDUAL".intern());
						  user.setActiveModuleId(household.getId());
						  user.setActiveModuleName("INDIVIDUAL".intern());
						  user.setPersonatedRoleName("INDIVIDUAL".intern());
					  }
					   ModuleUser moduleUser = user.getActiveModule(); 
					   int moduleId = -1;
					   if(moduleUser != null) {
						   moduleId = moduleUser.getId();
					   }
					  if(LOGGER.isInfoEnabled()) {
					LOGGER.info("household = {}, user active role = {}, active module id = {}, ", household,
							userActiveModuleName, moduleId);
					  }
					  
			} else if((StringUtils.isNotBlank(pageContext) && pageContext.equalsIgnoreCase("myEnrollments")) && (StringUtils.equalsIgnoreCase(userActiveModuleName,"APPROVEDADMINSTAFFL2".intern())
					|| StringUtils.equalsIgnoreCase(userActiveModuleName,"APPROVEDADMINSTAFFL1".intern()))){
				Role indivRole = roleService.findRoleByName(StringUtils.upperCase("INDIVIDUAL"));
				userDefLandingPage = indivRole.getLandingPage();
				if(StringUtils.isNotBlank(pageContext) && StringUtils.isNotBlank(userDefLandingPage)) {
					userDefLandingPage += userDefLandingPage.indexOf("?") >= 0 ? "&pageContext=" + pageContext : "?pageContext=" + pageContext;
				}
				if(household != null) {
					request.getSession().setAttribute("userActiveRoleName", userActiveModuleName.toUpperCase().intern());
					request.getSession().setAttribute("switchToModuleName", userActiveModuleName.toUpperCase().intern());
					user.setActiveModuleId(household.getId());					
				}
			}else {
						  userDefLandingPage = getLandingPage(user,user.getActiveModuleName(),request);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Resolved landing page {} for {}",userDefLandingPage,userActiveModuleName);
				}
						  if(userDefLandingPage == null && user.getDefRole() != null){
								userDefLandingPage = user.getDefRole().getLandingPage();
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Setting user active role name to user's default role name".intern());
					}
								request.getSession().setAttribute("userActiveRoleName",userService.getDefaultRole(user).getName());
						  }
						  if(StringUtils.isNotBlank(pageContext) && StringUtils.isNotBlank(userDefLandingPage)) {
					userDefLandingPage += userDefLandingPage.indexOf("?") >= 0 ? "&pageContext=" + pageContext
							: "?pageContext=" + pageContext;
						  }
				  }
			
			 if(StringUtils.isNotBlank(previewId) && StringUtils.isNotBlank(userDefLandingPage)) {
					userDefLandingPage += userDefLandingPage.indexOf("?") >= 0 ? "&previewId=" + previewId: "?previewId=" + previewId;
			  }
				  if(userDefLandingPage == null) {
					  throw new GIException("Error in getting the landing page for user id" + user.getId());
				  }
				  if(LOGGER.isInfoEnabled()) {
				LOGGER.info("User active role; {}, default role : {}, landing page : {}", user.getActiveModuleName(),
						user.getDefRole(), userDefLandingPage);
			  }
			  
			} catch (GIRuntimeException e) {
				LOGGER.error("Failed to decrypt case Id ["+householdCaseId+"]"+e.getMessage());
				throw new GIRuntimeException("Failed to decrypt case Id ["+householdCaseId+"]"+e.getMessage(),e);
			} catch (InvalidUserException e) {
				LOGGER.error("Failed to get logged in user" + e.getMessage());
				throw new GIRuntimeException("Failed to get logged in user" + e.getMessage(),e);
			} catch (GIException e) {
				LOGGER.error("Error in getting the landing page : "+e.getMessage());
				throw new GIRuntimeException("Error in getting the landing page: "+ e.getMessage(),e);
			}
		 	request.getSession().setAttribute(SecurityUtils.INTERNAL_REF, true);
	        return "redirect:"+ userDefLandingPage; // "redirect:/indportal";
	 }


	   /* Test Endpoint to set the user login context param for external assisters
	    * This will be removed after testing
	    * */
	    @RequestMapping(value="/indportal/test/accountUserContextParams", method=RequestMethod.POST)
	    public @ResponseBody String setActiveHousehold(@RequestBody String request) throws GIException, InvalidUserException{
	        LOGGER.debug("setAccountUserContextParams = " +request);
	        AccountUser user = userService.getLoggedInUser();
	        if(StringUtils.isNotBlank(request)) {
	        	 Type type = new TypeToken<Map<String, String>>(){}.getType();
	            Map<String, String> requestMap = platformGson.fromJson(request, type);
	            user.setLoginContext(requestMap);
	            this.getConsumerUtil().handleExternalAssister(user,requestMap);
	            LOGGER.debug("set login Context with Map = " + request);
	            return platformGson.toJson(requestMap);
	        } else {
	            return platformGson.toJson(user.getLoginConext());
	        }
	    } 
	 
	    @RequestMapping(value = "/indportal/showDelayPopup", method = RequestMethod.GET)
		@ResponseBody
		 public boolean showDelayPopup(Model model, HttpServletRequest request) {
			AccountUser user = null;
			boolean response = false;
			try {
				user = userService.getLoggedInUser();			
				response = isShowPopup(user.getActiveModuleId());
			} catch (Exception ex) {
				LOGGER.error("Exception occurs while calling showDelayPopup method", ex);			
			}
			return response;
		}
	 
	@RequestMapping(value = "/indportal/getapplicationstatusbyid/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public String getApplicationStatus(@PathVariable("ssapApplicationId") Long ssapApplicationId) {
		String applicationStatus = null;
		Map<String, String> status = new HashMap<String, String>();
		try {
			if (ssapApplicationId != null) {
				applicationStatus = ghixRestTemplate.getForObject(
						GhixEndPoints.EligibilityEndPoints.GET_SSAP_APPLICATION_STATUS + "/" + ssapApplicationId,
						String.class);

			}
			if (applicationStatus != null) {
				status.put("applicationStatus", applicationStatus);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in getapplicationstatusbyid API", ex);
		}
		return platformGson.toJson(status);
	}

	@RequestMapping(value = "/indportal/viewDocument", method = RequestMethod.GET)
  @PreAuthorize(IndividualPortalConstants.PORTAL_VIEW_UPLOAD_DOCUMENT_PERMISSION)
	public void getAttachment(@RequestParam String documentId, Model model,
							  HttpServletResponse response) {
		byte[] attachment;
		String errorMsg;
		Content content;
		try {
			String decryptedDocID = ghixJasyptEncrytorUtil.decryptStringByJasypt(documentId);
			attachment = ecmService.getContentDataById(decryptedDocID);
			content = ecmService.getContentById(decryptedDocID);
			response.setContentType(content.getMimeType());
			response.setContentLength(attachment.length);
			String fileName = content.getOriginalFileName();
			fileName = fileName.replaceAll(UNSECURE_CHARS_REGEX,
					StringUtils.EMPTY);
			fileName = XssHelper.stripXSS(fileName);
			response.setHeader("Content-Disposition", "inline;filename="
					+ fileName);
			FileCopyUtils.copy(attachment, response.getOutputStream());
		} catch (ContentManagementServiceException e) {
			errorMsg = e.getMessage();
			model.addAttribute(ERROR_MSG, errorMsg);
			LOGGER.error("getAttachment::error with content management", e);
		} catch (IOException e) {
			errorMsg = e.getMessage();
			model.addAttribute(ERROR_MSG, errorMsg);
			LOGGER.error("getAttachment::IO Error", e);
		}
	}

	@RequestMapping(value = "/indportal/switchCSRToTicket/{ticketNumber}", method = RequestMethod.GET)
  @PreAuthorize(IndividualPortalConstants.PORTAL_TKM_VIEW_TICKET_PERMISSION)
	public String switchCSRToTicket(@PathVariable("ticketNumber") String ticketNumber, Model model, HttpServletRequest request) {
		String userActiveRoleName = "";
		try {
			userActiveRoleName = (String) request.getSession().getAttribute("userActiveRoleName");
			String referrerUrl = (String) request.getSession().getAttribute("adminReferralUrl");
			request.getSession().setAttribute("adminReferralUrl", "/ticketmgmt/ticket/ticketdetail/" + UriUtils.decode(ticketNumber, "UTF-8"));
			LOGGER.debug("switchCSRToTicket::current referrerUrl {} setting to {}", referrerUrl, "");
		} catch (UnsupportedEncodingException uee) {
			LOGGER.error("switchCSRToTicket::error decoding ticket number", uee);
		} catch (Exception ex) {
			LOGGER.error("switchCSRToTicket::error changing session", ex);
		}

		if(StringUtils.isNotBlank(userActiveRoleName)) {
			return "redirect:/account/user/switchNonDefRole/" + userActiveRoleName;
		} else {
			return "redirect:" + request.getRequestURI();
		}
	}
	
	@RequestMapping(value = "/indportal/cloneApplicationByCaseNumber/{caseNumber}", method = RequestMethod.POST)
	@ResponseBody    
	public String cloneApplication(@PathVariable("caseNumber") String caseNumber, HttpServletRequest httpRequest, Model model) throws GIException {
		String caseId = "";
		final Map<String, String> request = new HashMap<String,String>();
		String response = null;
		try {
			LOGGER.info("Processing request cloning application for caseNumber : "+ caseNumber);
			String applicationId = null;

			SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumber(caseNumber);
	        if(ssapApplication != null){
	        	applicationId = String.valueOf(ssapApplication.getId());
	        }
			if (!StringUtils.isEmpty(applicationId)) {
				response = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL+ "/ssapapplication/cloneApplication?applicationId=" + applicationId, String.class,request);
				LOGGER.info("response from eligibility cloning API : "+ response);		
			}
			if (response != null) {
				LOGGER.info("Creating object ");
				PortalResponse responseObj = platformGson.fromJson(response, PortalResponse.class);
				if(responseObj != null)
				{
					caseId = responseObj.getClonedAppCaseNumber();
					SsapApplication clonedSsapApplication = ssapApplicationRepository.findByCaseNumber(caseId);
			        if(clonedSsapApplication != null){
			        	SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(clonedSsapApplication.getApplicationData());
			        	singleStreamlinedApplication.setIsEditApplication(true);
			        	clonedSsapApplication.setApplicationData(ssapJsonBuilder.transformToJson(singleStreamlinedApplication));
			        	ssapApplicationRepository.save(clonedSsapApplication);
			        }
					LOGGER.info("caseId to be returned " + caseId);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in cloneApplication API", ex);
		}
		return caseId;
	}
	
	private void calculateSingleEnrolGrpAptcAndSubsidy(Group group, Long applicationId, BigDecimal maxAptc, BigDecimal maxStateSubsidy) throws GIException {
		if(group != null && GroupType.HEALTH.equals(group.getGroupType()) && StringUtils.isNotBlank(group.getGroupEnrollmentId())){
			if((applicationId != null && ((maxAptc != null && maxAptc.compareTo(BigDecimal.ZERO) > 0) || (maxStateSubsidy != null && maxStateSubsidy.compareTo(BigDecimal.ZERO) > 0)))){
				AppGroupRequest appGroupRequest = new AppGroupRequest();
		    	appGroupRequest.setApplicationId(applicationId);
		    	appGroupRequest.setCoverageStartDate(group.getCoverageDate());  
		    	String appGroupResponseJson = getAppGroups(appGroupRequest);            	
		    	AppGroupResponse appGroupResponse = platformGson.fromJson(appGroupResponseJson, CustomGroupingResponse.class);
		    	if(appGroupResponse.getStatus() == null && appGroupResponse.getGroupList() != null && !appGroupResponse.getGroupList().isEmpty() && !(appGroupResponse.getGroupList().size() > 2)){
		    		setAptcForSingleEnrolGrp(group, maxAptc, appGroupResponse);
		    		setStateSubsidyForSingleEnrolGrp(group, maxStateSubsidy, appGroupResponse);		    	
		    	}
			}
		}
	}
	
	private void setAptcForSingleEnrolGrp(Group group, BigDecimal maxAptc, AppGroupResponse appGroupResponse) {
		if(maxAptc != null && maxAptc.compareTo(BigDecimal.ZERO) > 0){
			int healthGroupCount = 0;
			int enrollDentalGroupCount = 0;
			String enrolledHealthGrpId = null;
			BigDecimal dentalGroupAptc = null;
			
			for(Group grp : appGroupResponse.getGroupList()){
				if(GroupType.HEALTH.equals(grp.getGroupType())){
					healthGroupCount++;
					if(StringUtils.isNotBlank(grp.getGroupEnrollmentId())){
						enrolledHealthGrpId = grp.getGroupEnrollmentId();
					}	
					if(healthGroupCount > 1){
						break;
					}
				}
				if(GroupType.DENTAL.equals(grp.getGroupType()) && StringUtils.isNotBlank(grp.getGroupEnrollmentId())){
					dentalGroupAptc = grp.getMaxAptc();
					enrollDentalGroupCount++;
				}
			}
			if(healthGroupCount == 1 && group.getGroupEnrollmentId().equalsIgnoreCase(enrolledHealthGrpId)){	            		
				if(enrollDentalGroupCount == 1 && (dentalGroupAptc != null && dentalGroupAptc.compareTo(BigDecimal.ZERO) > 0)){
					BigDecimal remainingAptc = maxAptc.subtract(dentalGroupAptc);
					maxAptc = remainingAptc;
				}
				if(maxAptc.compareTo(BigDecimal.ZERO) > 0){
					group.setMaxAptc(maxAptc.setScale(2, BigDecimal.ROUND_HALF_UP));
				}	            		
			}
		}		
	}
	
	private void setStateSubsidyForSingleEnrolGrp(Group group, BigDecimal maxStateSubsidy, AppGroupResponse appGroupResponse) {
		if(maxStateSubsidy != null && maxStateSubsidy.compareTo(BigDecimal.ZERO) > 0){
			int healthGroupCount = 0;
			int enrollDentalGroupCount = 0;
			String enrolledHealthGrpId = null;
			BigDecimal dentalGroupStateSubsidy = null;
			
			for(Group grp : appGroupResponse.getGroupList()){
				if(GroupType.HEALTH.equals(grp.getGroupType())){
					healthGroupCount++;
					if(StringUtils.isNotBlank(grp.getGroupEnrollmentId())){
						enrolledHealthGrpId = grp.getGroupEnrollmentId();
					}	
					if(healthGroupCount > 1){
						break;
					}
				}
				if(GroupType.DENTAL.equals(grp.getGroupType()) && StringUtils.isNotBlank(grp.getGroupEnrollmentId())){
					dentalGroupStateSubsidy = grp.getMaxStateSubsidy();
					enrollDentalGroupCount++;
				}
			}
			if(healthGroupCount == 1 && group.getGroupEnrollmentId().equalsIgnoreCase(enrolledHealthGrpId)){	            		
				if(enrollDentalGroupCount == 1 && (dentalGroupStateSubsidy != null && dentalGroupStateSubsidy.compareTo(BigDecimal.ZERO) > 0)){
					BigDecimal remainingStateSubsidy = maxStateSubsidy.subtract(dentalGroupStateSubsidy);
					maxStateSubsidy = remainingStateSubsidy;
				}
				if(maxStateSubsidy.compareTo(BigDecimal.ZERO) > 0){
					group.setMaxStateSubsidy(maxStateSubsidy.setScale(2, BigDecimal.ROUND_HALF_UP));
				}	            		
			}		
		}		
	}
	
}
