<script type="text/ng-template" id="maritalstatus-incomesummary.jsp">
  <div class="gutter10 jobIncome" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.incomesummary"/></h3>
    <div class="gutter10">
      <table class="table table-striped" role="presentation">
  	   <tbody>
			   <tr>
          <td class="primaryContact header">
            <h5>
              {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}
            </h5>
          </td>
          <td class="primaryContact header">
            <h5>
              <div ng-repeat="relationship in householdMember.bloodRelationship">
                {{getRelation(relationship.relationshipCode)}}
              </div>
            </h5>

          </td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.jobIncome.length > 0 && householdMember.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes > 0">
          <td><spring:message code="label.lce.primaryjob"/></td>
          <td>
          	<span ng-if ="householdMember.detailedIncome.jobIncome.length > 0">{{householdMember.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.jobIncome[0].incomeFrequency}}</span>
          </td>
        </tr>

        <tr ng-show="householdMember.detailedIncome.jobIncome.length > 1 && showAdditionalJobs(householdMember)">
          <td><spring:message code="label.lce.additionaljob"/></td>
          <td>
          	<table role="presentation">
          	    <tr ng-repeat="jobIncome in householdMember.detailedIncome.jobIncome" ng-if="!$first && jobIncome.incomeAmountBeforeTaxes>0" >
          			<td>{{jobIncome.employerName}}  - {{jobIncome.incomeAmountBeforeTaxes | currency}} <spring:message code="label.lce.per"/> {{jobIncome.incomeFrequency}} </td>
          		</tr>
          	</table>
          </td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.selfEmploymentIncome.monthlyNetIncome > 0">
          <td><spring:message code="label.lce.selfemployed"/>Self-Employed</td>
          <td>{{householdMember.detailedIncome.selfEmploymentIncome.monthlyNetIncome | currency}} <spring:message code="label.lce.per"/> <spring:message code="label.lce.month"/></td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.socialSecurityBenefit.benefitAmount > 0">
          <td><spring:message code="label.lce.ssb"/></td>
          <td>{{householdMember.detailedIncome.socialSecurityBenefit.benefitAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.socialSecurityBenefit.incomeFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.unemploymentBenefit.benefitAmount > 0">
          <td><spring:message code="label.lce.unemployment"/></td>
          <td>{{householdMember.detailedIncome.unemploymentBenefit.benefitAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.unemploymentBenefit.incomeFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.retirementOrPension.taxableAmount > 0">
          <td><spring:message code="label.lce.retirepension"/></td>
          <td>{{householdMember.detailedIncome.retirementOrPension.taxableAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.retirementOrPension.incomeFrequency}} </td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.capitalGains.annualNetCapitalGains > 0">
          <td><spring:message code="label.lce.capitalgains"/></td>
          <td>{{householdMember.detailedIncome.capitalGains.annualNetCapitalGains | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.capitalGains.incomeFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.investmentIncome.incomeAmount > 0">
          <td><spring:message code="label.lce.investmentincome"/></td>
          <td>{{householdMember.detailedIncome.investmentIncome.incomeAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.investmentIncome.incomeFrequency}}</td>
        </tr >
        <tr ng-show="householdMember.detailedIncome.rentalRoyaltyIncome.netIncomeAmount > 0">
          <td><spring:message code="label.lce.rentalroyalincome"/></td>
          <td>{{householdMember.detailedIncome.rentalRoyaltyIncome.netIncomeAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.rentalRoyaltyIncome.incomeFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.farmFishingIncome.netIncomeAmount > 0">
          <td><spring:message code="label.lce.farmfishingincome"/></td>
          <td>{{householdMember.detailedIncome.farmFishingIncome.netIncomeAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.farmFishingIncome.incomeFrequency }}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.alimonyReceived.amountReceived > 0">
          <td><spring:message code="label.lce.alimonymoney"/></td>
          <td>{{householdMember.detailedIncome.alimonyReceived.amountReceived | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.alimonyReceived.incomeFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.retirementOrPension.taxableAmount > 0">
          <td><spring:message code="label.lce.other"/></td>
          <td>{{householdMember.detailedIncome.otherIncome.incomeAmount | currency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.deductions.alimonyDeductionAmount > 0">
          <td><spring:message code="label.lce.deduction"/>: <spring:message code="label.lce.alimonypaid"/></td>
          <td>{{householdMember.detailedIncome.deductions.alimonyDeductionAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.deductions.alimonyDeductionFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.deductions.studentLoanDeductionAmount > 0">
          <td><spring:message code="label.lce.deduction"/>: <spring:message code="label.lce.studentloanpaidinterest"/></td>
          <td>{{householdMember.detailedIncome.deductions.studentLoanDeductionAmount | currency}} {{householdMember.detailedIncome.deductions.studentLoanDeductionFrequency}}</td>
        </tr>
        <tr ng-show="householdMember.detailedIncome.deductions.otherDeductionAmount > 0">
          <td><spring:message code="label.lce.deduction"/> : <spring:message code="label.lce.otherdeductions"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong>'s <spring:message code="label.lce.taxreturn"/></td>
          <td>{{householdMember.detailedIncome.deductions.otherDeductionAmount | currency}} <spring:message code="label.lce.per"/> {{householdMember.detailedIncome.deductions.otherDeductionFrequency}}</td>
        </tr>
        <tr>
          <td><spring:message code="label.lce.totaldeductions"/></td>
          <td>--</td>
        </tr>
        <tr>
          <td><spring:message code="label.lce.totalpaid"/></td>
          <td>--</td>
        </tr>
        </tbody>
      </table>
    </div>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>