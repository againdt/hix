package com.getinsured.iex.indportal.dto;


/**
 * HIX-60186
 * 
 * Helper class for Ind Portal operations
 * @author Nikhil Talreja
 *
 */
public class IndExHelper {
	
	private String caseNumber;
	
	private String effectiveStartDate;
	
	private float ehbAmount;

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public float getEhbAmount() {
		return ehbAmount;
	}

	public void setEhbAmount(float ehbAmount) {
		this.ehbAmount = ehbAmount;
	} 
	
}
