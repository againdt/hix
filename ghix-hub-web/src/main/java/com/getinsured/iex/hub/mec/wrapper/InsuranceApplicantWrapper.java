package com.getinsured.iex.hub.mec.wrapper;


import java.text.ParseException;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;

import com.getinsured.iex.hub.mec.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;


/**
 * @author Vishaka Tharani
 * @since February 21, 2014
 *
 */

public class InsuranceApplicantWrapper {

	private static com.getinsured.iex.hub.mec.niem.niem.niem_core._2.ObjectFactory niemCoreFactory;
	private static com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	private static com.getinsured.iex.hub.mec.cms.hix._0_1.hix_ee.ObjectFactory hixCoreFactory;

    private DateType startDateType;
    private DateType endDateType;
    private InsuranceApplicantType insuranceApplicantType;
    private DateRangeType dateRangeType;
    private String insuranceApplicantRequestedCoverageStartDate = null;
    private String insuranceApplicantRequestedCoverageEndDate = null;
    
    public InsuranceApplicantWrapper(){
		niemCoreFactory = new com.getinsured.iex.hub.mec.niem.niem.niem_core._2.ObjectFactory();
		hixCoreFactory = new com.getinsured.iex.hub.mec.cms.hix._0_1.hix_ee.ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory();
		
		this.dateRangeType = niemCoreFactory.createDateRangeType();
		this.insuranceApplicantType = hixCoreFactory.createInsuranceApplicantType();
		insuranceApplicantType.setInsuranceApplicantRequestedCoverage(dateRangeType);
	}
    
    public DateRangeType getSystemRepresentation()
    {
    	return this.dateRangeType;
    }
    
    public void setInsuranceApplicantRequestedCoverageStartDate(String insuranceApplicantRequestedCoverageStartDate) throws DatatypeConfigurationException, ParseException
    {
		if(insuranceApplicantRequestedCoverageStartDate != null && insuranceApplicantRequestedCoverageStartDate.trim().length() > 0){
		
			if(startDateType == null){
				startDateType = niemCoreFactory.createDateType();
			}
			
			Date date = proxyFactory.createDate();
			date.setValue(PlatformServiceUtil.stringToXMLDate(insuranceApplicantRequestedCoverageStartDate, "MM/dd/yyyy"));
	
			
			JAXBElement<Date> startDateJAXBElement = niemCoreFactory.createDate(date);
			this.startDateType.setDateRepresentation(startDateJAXBElement);
			
			dateRangeType.setStartDate(startDateType);
			
			insuranceApplicantType.setInsuranceApplicantRequestedCoverage(dateRangeType);
			
			this.insuranceApplicantRequestedCoverageStartDate = insuranceApplicantRequestedCoverageStartDate;
		}
	}
	
	public String getInsuranceApplicantRequestedCoverageStartDate(){
			return this.insuranceApplicantRequestedCoverageStartDate;
	}
	
	 public void setInsuranceApplicantRequestedCoverageEndDate(String insuranceApplicantRequestedCoverageEndDate) throws DatatypeConfigurationException, ParseException
	    {
			if(insuranceApplicantRequestedCoverageEndDate != null  && insuranceApplicantRequestedCoverageEndDate.trim().length() > 0){
				
				if(endDateType == null)
				{
					endDateType = niemCoreFactory.createDateType();
				}
	
				Date date = proxyFactory.createDate();
				date.setValue(PlatformServiceUtil.stringToXMLDate(insuranceApplicantRequestedCoverageEndDate, "MM/dd/yyyy"));
				
				JAXBElement<Date> endDateJAXBElement = niemCoreFactory.createDate(date);
				this.endDateType.setDateRepresentation(endDateJAXBElement);
				
				dateRangeType.setEndDate(endDateType);
				
				insuranceApplicantType.setInsuranceApplicantRequestedCoverage(dateRangeType);
				
				this.insuranceApplicantRequestedCoverageEndDate = insuranceApplicantRequestedCoverageEndDate;
			}
		}
		
		public String getInsuranceApplicantRequestedCoverageEndDate(){
				return this.insuranceApplicantRequestedCoverageEndDate;
		}

}
