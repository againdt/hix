package com.getinsured.eligibility.at.ref.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.getinsured.eligibility.at.ref.common.AdditionalDocumentToVerifyNotification;
import com.getinsured.eligibility.at.ref.common.VerificationDocumentStatusNotification;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;


/**
 * 
 * @author raguram_p
 *
 */
@Component("referralDocumentTriggerMailService")
@Scope("singleton")
public class ReferralDocumentTriggerMailServiceImpl implements ReferralDocumentTriggerMailService{
	
	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	@Autowired
	private VerificationDocumentStatusNotification documentStatusNotification;
	
	@Autowired 
	private ApplicationContext appContext;
	
	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired 
	private NoticeService noticeService;
	
	
	@Autowired 
	private SsapApplicantRepository ssapApplicantRepository;
	
	
	@Autowired private NoticeRepository noticeRepo;
	
	@Autowired
	private AdditionalDocumentToVerifyNotification additionaldocumenttoverifyNotify;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired private ContentManagementService ecmService;
	
	private static final String OMIT = "omit";

	private static final String UTF_8 = "UTF-8";
	
	
	private static final Logger LOGGER = Logger.getLogger(ReferralDocumentTriggerMailServiceImpl.class);
	
	public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
	public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
	
	/**
	 * Created by pravin for triggering email && || notice to primary application when document 
	 * uploaded for verfication is verified by csr.
	 * @param consumerDocument
	 * @param referralActivation
	 */
	public void triggerDocumentStatusMail(ConsumerDocument consumerDocument,
			ReferralActivation referralActivation) {

		SsapApplicant ssapApplicant = fetchPrimarySsapApplicant(referralActivation);
		//Household household = fetchHouseholdObj(referralActivation);
		Location location = fetchPrimarySsapApplicantAddress(ssapApplicant);

		
		try {
			documentStatusNotification.setConsumerDocument(consumerDocument);
			documentStatusNotification.setSsapApplicant(ssapApplicant);
			documentStatusNotification.setLocation(location);
			documentStatusNotification.setReferralId(toEncrypt(referralActivation.getId()));
			Notice noticeObj=documentStatusNotification.generateEmail();
			LOGGER.info(noticeObj.getEmailBody());
			if(ssapApplicant.getEmailAddress()!=null)
			{
				documentStatusNotification.sendEmail(noticeObj);
			} 
			
			if (location != null){
				String relativePath = "uploadedDocumentVerified/" + "INDIVIDUAL_REFERRAL" +"/" + ssapApplicant.getId() + "/notifications/";
				String ecmFileName = "INDIVIDUAL_REFERRAL" + "_UploadedDocVerified_" + ssapApplicant.getId() + (new TSDate().getTime()) + ".pdf";
				updateEcmPath(noticeObj, location, relativePath, ecmFileName);
			}
			
			
		} catch (NotificationTypeNotFound e) {
			LOGGER.error("Error while sending mail for Document verification status to :"+ssapApplicant.getId() , e);
		}
		
	}



	private SsapApplicant fetchPrimarySsapApplicant(
			ReferralActivation referralActivation) {
		int ssapId = referralActivation.getSsapApplicationId() != null ? referralActivation
				.getSsapApplicationId() : 0;
		if (ssapId == 0) {
			throw new GIRuntimeException(
					"SSAP Id is null.Unable to fetch household id. Email cannot be triggered"
							+ referralActivation.getId());
		}
		return ssapApplicantRepository.findByPersonId(ssapId, 1);
	}
	
	private Location fetchPrimarySsapApplicantAddress(
			SsapApplicant ssapApplicant) {
		
		int locationid=ssapApplicant.getMailiingLocationId()!=null ? ssapApplicant.getMailiingLocationId().intValue():0;
		if(locationid==0)
		{
			locationid= ssapApplicant.getOtherLocationId()!=null ? ssapApplicant.getOtherLocationId().intValue():0;
		}
		return iLocationRepository.findOne(locationid);
	}

	
	private void updateEcmPath(Notice noticeObj,Location location,String path,String fileName)
	{
		if (location != null){
			byte[] pdfBytes = null;
			try {
				pdfBytes = generatePdf(noticeObj.getEmailBody());
			} catch (IOException | NoticeServiceException e) {
				throw new GIRuntimeException("Error creating PDF using flying saucer - " + e);
			}

			String ecmDocId = null;
			try {
				ecmDocId = ecmService.createContent(path, fileName, pdfBytes);
			} catch (ContentManagementServiceException e) {
				throw new GIRuntimeException("Error creating content in ECM - " + e);
			}
			noticeObj.setEcmId(ecmDocId);
			noticeObj.setPrintable("Y");
			noticeObj = noticeRepo.save(noticeObj);
	}
	}
		
		private byte[] generatePdf(String incomingData) throws IOException, NoticeServiceException {

			/** Replace resources path (images, css, etc) with the full actual host name */
			String finalData = new String(StringUtils.replace(formData(incomingData), "{host}", GhixPlatformEndPoints.GHIXWEB_SERVICE_URL));


			ByteArrayOutputStream os = new ByteArrayOutputStream();
			try{
				/** If resources (images, css, etc) are not found,
				 * then ITextRenderer logs "<strong>java.io.IOException: Stream closed</strong>" exception without throwing it
				 * So we can't catch such exception thrown by 3rd party jar
				 *
				 * Also, stack trace gets printed in the PDF on Alfresco
				 *
				 */
				ITextRenderer renderer = new ITextRenderer();
				renderer.setDocumentFromString(finalData);
				renderer.layout();
				renderer.createPDF(os);
			}catch(Exception e){
				throw new NoticeServiceException(e);
			}
			finally{
				os.close();
			}

			return os.toByteArray();
		}
		
		private String formData(String data) throws IOException {

			StringWriter writer = new StringWriter();
			Tidy tidy = new Tidy();
			tidy.setTidyMark(false);
			tidy.setDocType(OMIT);
			tidy.setXHTML(true);
			tidy.setInputEncoding(UTF_8);
			tidy.setOutputEncoding(UTF_8);
			tidy.parse(new StringReader(data), writer);
			writer.close();
			return writer.toString();
		}


	@Override
	public Integer sendNotificationToUploadDoc(
			ReferralActivation referralActivation) {
		SsapApplicant ssapApplicant = fetchPrimarySsapApplicant(referralActivation);
		Location location = fetchPrimarySsapApplicantAddress(ssapApplicant);

		try {
			additionaldocumenttoverifyNotify.setLocation(location);
			additionaldocumenttoverifyNotify.setSsapApplicant(ssapApplicant);
			additionaldocumenttoverifyNotify.setReferralId(toEncrypt(referralActivation.getId())); 
			Notice noticeObj = additionaldocumenttoverifyNotify.generateEmail();

			LOGGER.info(noticeObj.getEmailBody());
			if (ssapApplicant.getEmailAddress() != null) {
				additionaldocumenttoverifyNotify.sendEmail(noticeObj);
			}

			if (location != null) {
				String relativePath = "additionalVerifyDocument/" + "INDIVIDUAL_REFERRAL" +"/" + ssapApplicant.getId() + "/notifications/";
				String ecmFileName = "INDIVIDUAL_REFERRAL" + "_AdditionalDocToVerify_" + ssapApplicant.getId() + (new TSDate().getTime()) + ".pdf";
				updateEcmPath(noticeObj, location, relativePath, ecmFileName);
				return new Integer("1");
			}
		} catch (NotificationTypeNotFound e) {
			LOGGER.error("Error while sending mail for Document verification status to :"+ssapApplicant.getId() , e);
		}

		return null;
		
	}
	
	

	private String toEncrypt(final long idToEncrypt) {
		return ghixJasyptEncrytorUtil.encryptStringByJasypt("" + idToEncrypt);
	}
	

}
