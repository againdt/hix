package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.dm.application.Attestations;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.EnrolledCoverage;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.hix.indportal.dto.dm.application.TaxHousehold;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncarcerationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCCalculationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSRAdvancePaymentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSREligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExchangeEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceMemberType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsurancePolicyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ReferralActivityStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ReferralActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsurancePlanVariantCategoryAlphaCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsuranceSourceCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsuranceSourceCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime;



@Component(value="insuranceApplicantMapperBatch")
public class InsuranceApplicantMapperBatch {
	

    private static Logger lOGGER = Logger.getLogger(InsuranceApplicantMapperBatch.class);
    
    private static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
    private static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
    private static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
    
    @Autowired private LawfulPresenceMapperBatch lawfulPresenceMapper;
    
    private static final Map<String, String> CSR_MAP = new HashMap();
    
    static{
		CSR_MAP.put("1_A","94PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_MAP.put("1_B","87PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_MAP.put("1_C","73PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_MAP.put("1_D","OpenToIndiansBelow300PercentFPL");
		CSR_MAP.put("1_E","OpenToIndiansAbove300PercentFPL");
	}
    
	public InsuranceApplicantType createInsuranceApplicants(AttestationsMember member, String memberId, Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas, Member computedMember, Attestations attestations, Map<String, TaxHousehold> taxHouseholds, boolean isPrimary) throws GIException {
		
		InsuranceApplicantType insuranceApplicantType = null;
		try {
			//if (member.getRequestingCoverageIndicator()) {
				insuranceApplicantType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicantType();
                //Map<String, VerificationMetadataType> personVerificationMetadata = verificationMetadatas.get(Long.valueOf(memberId));
				// medicaidInsurance:requestHelpPayingMedicalBillsLast3MonthsIndicator
		        /*if((JSONObject)((JSONObject)houseHold.get("healthCoverage")).get("medicaidInsurance") != null){
                	insuranceApplicantType.setInsuranceApplicantRecentMedicalBillsIndicator(
                		AccountTransferUtil.addBoolean(
                				((JSONObject)((JSONObject)houseHold.get("healthCoverage"))
                						.get("medicaidInsurance")).get("requestHelpPayingMedicalBillsLast3MonthsIndicator")));
                }*/
				insuranceApplicantType.setInsuranceApplicantTemporarilyLivesOutsideApplicationStateIndicator(
						AccountTransferUtil.addBoolean(member.getDemographic().getLiveOutsideStateTemporarilyIndicator()));
				boolean noHomeAddressIndicator = false;
				if(member.getDemographic().getNoHomeAddressIndicator() != null)
				{
					noHomeAddressIndicator = !member.getDemographic().getNoHomeAddressIndicator();
				}
				insuranceApplicantType.setInsuranceApplicantFixedAddressIndicator(AccountTransferUtil.addBoolean(noHomeAddressIndicator));
				insuranceApplicantType.setInsuranceApplicantParentCaretakerIndicator(AccountTransferUtil.addBoolean(member.getFamily().getParentCaretakerIndicator()));
				if(member.getMedicaid() != null)
				{
				insuranceApplicantType.getInsuranceApplicantNonESICoverageIndicator().add(this.createInsuranceApplicantNonESICoverageIndicator(member.getMedicaid().getEnrolledInHealthCoverageIndicator()));
				}
				
				List<EnrolledCoverage> otherInsurances = null ;
				if(member.getMedicaid() != null)
				{	
					member.getMedicaid().getInsuranceCoverage();
				}
				if(CollectionUtils.isNotEmpty(otherInsurances)) {
					for(EnrolledCoverage otherInsurance : otherInsurances) {
		            	insuranceApplicantType.getInsuranceApplicantNonESIPolicy().add(this.addNonEsiInsurancePolicy(otherInsurance, computedMember));
		            }
				}
				if(member.getOther()!= null && "INCARCERATED".equalsIgnoreCase(member.getOther().getIncarcerationType()))
				{
					insuranceApplicantType.getInsuranceApplicantIncarceration().add(this.createIncarcerationType(true));
				}
				
				if (member.getFamily().getFosterCareIndicator() != null && member.getFamily().getFosterCareIndicator()) {

					insuranceApplicantType.setInsuranceApplicantFosterCareIndicator(AccountTransferUtil.addBoolean(member.getFamily().getFosterCareIndicator()));
					if(member.getFamily().getFosterCareEndAge() != null) {
					insuranceApplicantType.setInsuranceApplicantAgeLeftFosterCare(AccountTransferUtil.createNumericType((java.lang.Integer) member.getFamily().getFosterCareEndAge()));
					}
					if(member.getFamily().getFosterCareState() != null) {
					insuranceApplicantType.setInsuranceApplicantFosterCareState(AccountTransferUtil.createUSStateCodeType((java.lang.String) member.getFamily().getFosterCareState()));
					}
					insuranceApplicantType.setInsuranceApplicantHadMedicaidDuringFosterCareIndicator(AccountTransferUtil.addBoolean(member.getFamily().getMedicaidDuringFosterCareIndicator()));
				}
				if(member.getNonMagi() != null) {
				insuranceApplicantType.setInsuranceApplicantBlindnessOrDisabilityIndicator(AccountTransferUtil.addBoolean(member.getNonMagi().getBlindOrDisabledIndicator()));
				
				insuranceApplicantType.setInsuranceApplicantLongTermCareIndicator(AccountTransferUtil.addBoolean(member.getNonMagi().getLongTermCareIndicator()));
				}
				/*
				insuranceApplicantType.getInsuranceApplicantNonESICoverageIndicator().add(this.createInsuranceApplicantNonESICoverageIndicator(healthCoverage, personVerificationMetadata));
				
				JSONArray currentEmployer = (JSONArray) healthCoverage.get("currentEmployer");
				
				if(currentEmployer.size() > 0){
					insuranceApplicantType.getInsuranceApplicantESIAssociation().add(this.createInsuranceApplicantESIAssociation(currentEmployer, createESIEligibleIndicator(healthCoverage,personVerificationMetadata)));
				}
				 
				insuranceApplicantType.setInsuranceApplicantCoverageDuringPreviousSixMonthsIndicator(AccountTransferUtil.addBoolean(healthCoverage.get("haveBeenUninsuredInLast6MonthsIndicator")));
				String disenrollmentReasonCode = (String) ((JSONObject) healthCoverage.get("chpInsurance")).get("reasonInsuranceEndedOther");
				if (StringUtils.isNotBlank(disenrollmentReasonCode)) {
					insuranceApplicantType.getInsuranceApplicantNonESIPolicy().add(addDisenrollmentActivity(disenrollmentReasonCode));
				}
				insuranceApplicantType.getInsuranceApplicantIncarceration().add(this.createIncarcerationType(houseHold, personVerificationMetadata));

				//immigration
				JSONObject citizenshipImmigrationStatus = (JSONObject) houseHold.get("citizenshipImmigrationStatus");
                if(null != citizenshipImmigrationStatus && !((Boolean)citizenshipImmigrationStatus.get("citizenshipAsAttestedIndicator"))) {
				    LawfulPresenceStatusType lawfulPresenceStatusType = new LawfulPresenceMapper().createLawfulPresenceStatusType(citizenshipImmigrationStatus);
				
    				// Set ELIGIBLE_IMMIGRATION_STATUS verification metadata if available.
    				VerificationMetadataWrapper.setMetadata(personVerificationMetadata, lawfulPresenceStatusType, VerificationCategoryCodeSimpleType.ELIGIBLE_IMMIGRATION_STATUS);
                    insuranceApplicantType.setInsuranceApplicantLawfulPresenceStatus(lawfulPresenceStatusType);
                }
                
				JSONObject specialCircumstances = (JSONObject) houseHold.get("specialCircumstances");
				// everInFosterCareIndicator
				if ((java.lang.Boolean) specialCircumstances.get("everInFosterCareIndicator")) {

					insuranceApplicantType.setInsuranceApplicantFosterCareIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("everInFosterCareIndicator")));
					insuranceApplicantType.setInsuranceApplicantAgeLeftFosterCare(AccountTransferUtil.createNumericType((java.lang.Integer) specialCircumstances.get("ageWhenLeftFosterCare")));
					insuranceApplicantType.setInsuranceApplicantFosterCareState(AccountTransferUtil.createUSStateCodeType((java.lang.String) specialCircumstances.get("fosterCareState")));
				}
				insuranceApplicantType.setInsuranceApplicantHadMedicaidDuringFosterCareIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("gettingHealthCareThroughStateMedicaidIndicator")));

				insuranceApplicantType.setInsuranceApplicantBlindnessOrDisabilityIndicator(AccountTransferUtil.addBoolean(houseHold.get("disabilityIndicator")));
				
				insuranceApplicantType.setInsuranceApplicantLongTermCareIndicator(AccountTransferUtil.addBoolean(houseHold.get("livingArrangementIndicator")));
				*/
				//if(null != citizenshipImmigrationStatus && !((Boolean)citizenshipImmigrationStatus.get("citizenshipAsAttestedIndicator"))) {
				    //LawfulPresenceStatusType lawfulPresenceStatusType = new LawfulPresenceMapper().createLawfulPresenceStatusType(citizenshipImmigrationStatus);
				
    				// Set ELIGIBLE_IMMIGRATION_STATUS verification metadata if available.
    				//VerificationMetadataWrapper.setMetadata(personVerificationMetadata, lawfulPresenceStatusType, VerificationCategoryCodeSimpleType.ELIGIBLE_IMMIGRATION_STATUS);
                    
                //}
				
				insuranceApplicantType.setInsuranceApplicantLawfulPresenceStatus(lawfulPresenceMapper.createLawfulPresenceStatusType(member, computedMember));
				
				insuranceApplicantType.setReferralActivity(this.createReferralActivityType());
				/*if(!"YES".equalsIgnoreCase(computedMember.getCitizenshipStatus()))
				{
					LawfulPresenceStatusType lawfulPresenceStatusType = new LawfulPresenceMapper().createLawfulPresenceStatusType(citizenshipImmigrationStatus);
					insuranceApplicantType.setInsuranceApplicantLawfulPresenceStatus(createLawfulPresenceStatusType(computedMember));
				}*/
				BigDecimal aptcAmount = null;
				if(isPrimary && taxHouseholds != null && !taxHouseholds.isEmpty())
				{
					TaxHousehold taxHousehold = taxHouseholds.entrySet().iterator().next().getValue();
					if(taxHousehold.getMaxAPTC() != null)
					{
						aptcAmount = taxHousehold.getMaxAPTC().getMaxAPTCAmount();
					}
				}
				this.setApctCsrExchEligibility(insuranceApplicantType,member.getRequestingCoverageIndicator(), isPrimary, "YES".equalsIgnoreCase(computedMember.getAptcStatus()),aptcAmount,!"NO".equalsIgnoreCase(computedMember.getQhpStatus()),"YES".equalsIgnoreCase(computedMember.getCsrStatus()), computedMember.getCsrVariant());
			//}
		} catch (Exception e) {
			lOGGER.error("Exeption while creating InsuranceApplicant element",e);
			throw new GIException(e);
		}
		
		return insuranceApplicantType;
	}
	
	private IncarcerationType createIncarcerationType(Boolean nonIncarerationStatusProvidedByApplicant){
			
			IncarcerationType incarcerationType = AccountTransferUtil.hixCoreFactory.createIncarcerationType();
		
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean incarcerationIndicator = AccountTransferUtil.basicFactory.createBoolean();
			
			if(nonIncarerationStatusProvidedByApplicant != null){
				incarcerationIndicator.setValue(!nonIncarerationStatusProvidedByApplicant);	
		    }
			
			if(incarcerationIndicator != null) {
				incarcerationType.setIncarcerationIndicator(incarcerationIndicator);
			}
			return incarcerationType;
		}

	private InsurancePolicyType addNonEsiInsurancePolicy(EnrolledCoverage otherInsurance, Member computedMember){
		InsurancePolicyType nonEsiInsurancePolicyType = AccountTransferUtil.insuranceApplicationObjFactory.createInsurancePolicyType();
        InsuranceSourceCodeType otherInsuranceCodeType = AccountTransferUtil.hixTypeFactory.createInsuranceSourceCodeType();
        //if("Other".equals(currentOtherInsuranceSelected)) {
            otherInsuranceCodeType.setValue(InsuranceSourceCodeSimpleType.fromValue(otherInsurance.getInsuranceMarketType()));  
            InsurancePlanType otherInsurancePlan = AccountTransferUtil.hixPMFactory.createInsurancePlanType();
            ProperNameTextType planName = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
            
            planName.setValue(otherInsurance.getInsurancePlanName());
            otherInsurancePlan.setInsurancePlanName(planName);
            nonEsiInsurancePolicyType.setInsurancePlan(otherInsurancePlan);
        /*} else {
        	otherInsuranceCodeType.setValue(InsuranceSourceCodeSimpleType.fromValue(currentOtherInsuranceSelected));  
        }*/
        nonEsiInsurancePolicyType.setInsurancePolicySourceCode(otherInsuranceCodeType);
        InsuranceMemberType insuranceMemberType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceMemberType();
        IdentificationType CHIPIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();

		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String chipId = AccountTransferUtil.basicFactory.createString();
		chipId.setValue(computedMember.getPersonTrackingNumber());
        CHIPIdentificationType.setIdentificationID(chipId);
        insuranceMemberType.setCHIPIdentification(CHIPIdentificationType);
        
        
        IdentificationType medicaidIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();

		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String medicaidId = AccountTransferUtil.basicFactory.createString();
		medicaidId.setValue(computedMember.getPersonTrackingNumber());
        CHIPIdentificationType.setIdentificationID(medicaidId);
        insuranceMemberType.setMedicaidIdentification(medicaidIdentificationType);
        
        nonEsiInsurancePolicyType.setInsuranceMember(insuranceMemberType);
        IdentificationType policyIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();

		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String policyNumber = AccountTransferUtil.basicFactory.createString();
		policyNumber.setValue(otherInsurance.getInsurancePolicyNumber());
        policyIdentificationType.setIdentificationID(policyNumber);
        nonEsiInsurancePolicyType.setInsurancePolicyIdentification(policyIdentificationType);
        return nonEsiInsurancePolicyType;
	}
	
	private com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean createInsuranceApplicantNonESICoverageIndicator(Boolean enrolledInHealthCoverageIndicator){
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean nonESICoverageFlag = AccountTransferUtil.basicFactory.createBoolean();
		
		// Extract and set the non-ESI coverage details based on otherInsuranceIndicator and currentOtherInsurance tags from Ssap json
		if(null != enrolledInHealthCoverageIndicator && enrolledInHealthCoverageIndicator) {
		    // Set the value of the non-ESI coverage indicator as true because some other insurance type is selected by the user 
		    nonESICoverageFlag.setValue(true);
		} else {
		    nonESICoverageFlag.setValue(false);
		}
		
        return nonESICoverageFlag;
	}
	private InsuranceApplicantType setApctCsrExchEligibility(InsuranceApplicantType insuranceApplicantType, Boolean seekingCoverage, boolean isPrimary, boolean aptcStatus, BigDecimal aptcAmount,boolean qhpStatus, boolean csrStatus, String csrLevel){
		
		List<EligibilityType> eligibilityTypeList = new ArrayList<EligibilityType>();
		//--- APTC eligibility date start here ---
		APTCEligibilityType aptcEligibilityType = createAPTCEligibilityType(isPrimary,aptcStatus,aptcAmount);
		eligibilityTypeList.add(aptcEligibilityType);
		//--- APTC eligibility date ends here ---
		
		//--- CSR eligibility date starts here ---
		CSREligibilityType csrEligibilityType = createCSREligibilityType(csrStatus,csrLevel);
		eligibilityTypeList.add(csrEligibilityType);
		//--- CSR eligibility date ends here ---
		
		//--- EXCHANGE eligibility date starts here ---
		ExchangeEligibilityType exchangeEligibilityType = createExchangeEligibilityType(seekingCoverage,qhpStatus);
		eligibilityTypeList.add(exchangeEligibilityType);
		//--- EXCHANGE eligibility date end here ---
		insuranceApplicantType.setEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility(eligibilityTypeList);
		
		return insuranceApplicantType;
	}

	public APTCEligibilityType createAPTCEligibilityType(boolean isPrimary, boolean aptcStatus, BigDecimal aptcAmount) {
		XMLGregorianCalendar gregCal = null;
		DateType aptcEligibilityStartDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		try 
		{
			gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2019, 1, 1, DatatypeConstants.FIELD_UNDEFINED);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date aptcEligibiltyStartDate = AccountTransferUtil.basicFactory.createDate();
			aptcEligibiltyStartDate.setValue(gregCal);
			aptcEligibilityStartDateType.setDate(aptcEligibiltyStartDate);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		APTCEligibilityType aptcEligibilityType = new APTCEligibilityType();
		DateRangeType aptcEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		DateType aptcEligibilityEndDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		aptcEligibilityDateRange.setStartDate(aptcEligibilityStartDateType);
		XMLGregorianCalendar gregCalEndDate = null;
		try 
		{
			gregCalEndDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2019, 12, 31, DatatypeConstants.FIELD_UNDEFINED);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date aptcEligibiltyEndDate = AccountTransferUtil.basicFactory.createDate();
			aptcEligibiltyEndDate.setValue(gregCalEndDate);
			aptcEligibilityEndDateType.setDate(aptcEligibiltyEndDate);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		aptcEligibilityDateRange.setEndDate(aptcEligibilityEndDateType);
		aptcEligibilityType.setEligibilityDateRange(aptcEligibilityDateRange);
		ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
		
		/*XMLGregorianCalendar gregCalActivityDate = null;
		DateType aptcEligibilityActivityDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		try 
		{
			gregCalActivityDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2020, 1, 1, DatatypeConstants.FIELD_UNDEFINED);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date aptcEligibiltyActivityDate = AccountTransferUtil.basicFactory.createDate();
			aptcEligibiltyActivityDate.setValue(gregCalActivityDate);
			refActivityDate.setValue(AccountTransferUtil.getSystemDateTime());
			aptcEligibilityActivityDateType.setDateTime();(aptcEligibiltyActivityDate);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Date eligibilityActivationDateDb = new Date();

		DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
		DateType aptcEligibilityActivityDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		
		eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
		aptcEligibilityActivityDateType.setDateTime(eligibilityActivationDateDbAtBasic);
		
		eligibilityDetermination.setActivityDate(aptcEligibilityActivityDateType);
		eligibilityDetermination.setActivityDate(aptcEligibilityActivityDateType);
		
		aptcEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue("999");
		aptcEligibilityType.setEligibilityReasonText(textType);
		aptcEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(aptcStatus));
		
		/**
		 * if member having APTCSTATUS YES
		 * 		aptc eligible as TRUE
		 * 		if member is primary and 
		 * 			set APTC from taxHOusehold
		 * 		If member is not primary 
		 * 			set APTC ZERO
		 * else
		 * 		aptc eligibile as false
		 * 
		 * 
		 * if member having CSR YES
		 * 		csr eligible as true
		 */
		if(aptcStatus)
		{
			aptcEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.TRUE));
			AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
			APTCCalculationType aptcCalculationType  = AccountTransferUtil.insuranceApplicationObjFactory.createAPTCCalculationType();
			amountType.setValue(aptcAmount!= null ? aptcAmount :  BigDecimal.ZERO);
			aptcCalculationType.setAPTCMaximumAmount(amountType);
			aptcEligibilityType.setAPTC(aptcCalculationType);
			aptcEligibilityType.getAPTC().setAPTCMaximumAmount(aptcCalculationType.getAPTCMaximumAmount());
		}
		
		return aptcEligibilityType;
	}
	
	
	
	private CSREligibilityType createCSREligibilityType(boolean csrStatus, String csrLevel){
		/**
		 * if member having CSR YES
		 * 		csr eligible as true
		 */
		CSREligibilityType csrEligibilityType = new CSREligibilityType();
		
		
		DateRangeType csrEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType csrEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		
		Date startDate = new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime();
		startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDate));
		csrEligibilityStartdateType.setDate(startDateDbAtBasic);
		csrEligibilityDateRange.setStartDate(csrEligibilityStartdateType);
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType csrEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();

		Date endDate = new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime();
		endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDate));
		csrEligibilityEnddateType.setDate(endDateDbAtBasic);
		csrEligibilityDateRange.setEndDate(csrEligibilityEnddateType);
		
		csrEligibilityType.setEligibilityDateRange(csrEligibilityDateRange);
		
		Date eligibilityActivationDateDb = new Date();
		if(eligibilityActivationDateDb != null){
			DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			csrEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}
		
		TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue("999");
		csrEligibilityType.setEligibilityReasonText(textType);
		csrEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(csrStatus));
		if(csrStatus && CSR_MAP.get(csrLevel)!= null){
			InsurancePlanVariantCategoryAlphaCodeType insurancePlanVariantCategoryAlphaCodeType = AccountTransferUtil.hixTypeFactory.createInsurancePlanVariantCategoryAlphaCodeType();
			insurancePlanVariantCategoryAlphaCodeType.setValue(CSR_MAP.get(csrLevel));
			 
			CSRAdvancePaymentType csrAdvancePaymentType = AccountTransferUtil.insuranceApplicationObjFactory.createCSRAdvancePaymentType();
			csrAdvancePaymentType.setCSRCategoryAlphaCode(insurancePlanVariantCategoryAlphaCodeType);
			csrEligibilityType.setCSRAdvancePayment(csrAdvancePaymentType);
			csrEligibilityType.getCSRAdvancePayment().setCSRCategoryAlphaCode(insurancePlanVariantCategoryAlphaCodeType);
		}
		
		return csrEligibilityType;
	}
	
	private ExchangeEligibilityType createExchangeEligibilityType(Boolean seekingCoverage, boolean qhpStatus){
		ExchangeEligibilityType exchangeEligibilityType = new ExchangeEligibilityType();
		
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		Date startDateDb = new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		
		startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);
		
		Date endDateDb = new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		
		endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
			exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		exchangeEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb  = new Date();
		DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
		DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		
		eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
		eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
		ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
		eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
		exchangeEligibilityType.setEligibilityDetermination(eligibilityDetermination);

		TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue("999");
		exchangeEligibilityType.setEligibilityReasonText(textType);
		exchangeEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(qhpStatus));
		
		
		return exchangeEligibilityType;
	}
	
	private ReferralActivityType createReferralActivityType(){
		//insuranceApplicant/ReferralActivity
		ReferralActivityType referralActivityType = AccountTransferUtil.insuranceApplicationObjFactory.createReferralActivityType();
		
		DateType refActivityDateType= AccountTransferUtil.niemCoreFactory.createDateType();
		DateTime refActivityDate = AccountTransferUtil.basicFactory.createDateTime();
		//TODO source of this date.It is not system Date
		refActivityDate.setValue(AccountTransferUtil.getSystemDateTime());
		//refActivityDate.setValue(value);
		refActivityDateType.setDateTime(refActivityDate);
		referralActivityType.setActivityDate(refActivityDateType);	
		
		IdentificationType referralActivityIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String referralActivityId = AccountTransferUtil.basicFactory.createString();
		referralActivityId.setValue(AccountTransferUtil.getTransferID());
		referralActivityIdentificationType.setIdentificationID(referralActivityId);
		referralActivityType.setActivityIdentification(referralActivityIdentificationType);
		
		ReferralActivityStatusCodeType referralActivityStatusCodeType = AccountTransferUtil.hixTypeFactory.createReferralActivityStatusCodeType();
		ReferralActivityStatusType referralActivityStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createReferralActivityStatusType();
		//TODO per abhinav hardcoded to initied for now. revisit later
		referralActivityStatusCodeType.setValue(ReferralActivityStatusCodeSimpleType.INITIATED);
		referralActivityStatusType.setReferralActivityStatusCode(referralActivityStatusCodeType);
		referralActivityType.setReferralActivityStatus(referralActivityStatusType);
		return referralActivityType;
	}
	
	public LawfulPresenceStatusType createLawfulPresenceStatusType(Member computedMember) {
			
			LawfulPresenceStatusType lawfulPresenceStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createLawfulPresenceStatusType();
					
			lawfulPresenceStatusType.setLawfulPresenceStatusArrivedBefore1996Indicator(AccountTransferUtil.addBoolean(false));
			
			EligibilityType eligibilityIndicator = AccountTransferUtil.insuranceApplicationObjFactory.createEligibilityType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean eligibilityIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();
						//|| ((java.lang.Boolean) citizenshipImmigrationStatus.get("(naturalizedCitizenshipindicator"));
			eligibilityIndicatorBoolean.setValue("YES".equalsIgnoreCase(computedMember.getQhpLawfulPresenceStatus()));
			eligibilityIndicator.setEligibilityIndicator(eligibilityIndicatorBoolean);
			lawfulPresenceStatusType.getLawfulPresenceStatusEligibility().add(eligibilityIndicator);
			return lawfulPresenceStatusType;
		}
	}
