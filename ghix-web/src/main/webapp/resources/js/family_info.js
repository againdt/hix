function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	var elementName = element.name;
  	if( elementName.indexOf('applicant') >= 0){ ssn1 = $("#applicant_ssn1").val(); ssn2 = $("#applicant_ssn2").val(); ssn3 = $("#applicant_ssn3").val(); }
  	if( elementName.indexOf('spouse') >= 0){ ssn1 = $("#spouse_ssn1").val(); ssn2 = $("#spouse_ssn2").val(); ssn3 = $("#spouse_ssn3").val(); }
  	if( elementName.indexOf('child') >= 0){
  		var childName = elementName.substr(0,7);
  		ssn1 = $("#"+childName+"_ssn1").val(); ssn2 = $("#"+childName+"_ssn2").val(); ssn3 = $("#"+childName+"_ssn3").val();
  	}
  	if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3))  ){ return false; }
  	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	var elementName = element.name;
  	if( elementName.indexOf('applicant') >= 0){ dob_mm = $("#applicant_dob_mm").val(); dob_dd = $("#applicant_dob_dd").val(); dob_yy = $("#applicant_dob_yy").val(); }
  	if( elementName.indexOf('spouse') >= 0){ dob_mm = $("#spouse_dob_mm").val(); dob_dd = $("#spouse_dob_dd").val(); dob_yy = $("#spouse_dob_yy").val(); }
  	if( elementName.indexOf('child') >= 0){
  		var childName = elementName.substr(0,7);
  		dob_mm = $("#"+childName+"_dob_mm").val(); dob_dd = $("#"+childName+"_dob_dd").val(); dob_yy = $("#"+childName+"_dob_yy").val();
  	}
  	if( (dob_mm == "" || dob_dd == "" || dob_yy == "")  || (isNaN(dob_mm)) || (isNaN(dob_dd)) || (isNaN(dob_yy))  ){ return false; }
  		
	var today=new Date()
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

var family_info = {
	line_index:    0,
	child_numb:    0,
	applicant_row:
		'<fieldset id="family_info_FID">\n' +
		'<legend id="family_legend_FID" class="open togglelink">Your Profile</legend>\n' +
		'<div class="profile">\n' +
			'<div class="clearfix">\n' +
				'<input type="hidden" id="relation_FID" name="relation[]" value="applicant">\n' +
				'<label for="xlInput3">First Name</label>\n' +  
				'<div class="input">\n'+
					'<input size="30" name="applicant_first_name" id="applicant_first_name" class="xlarge" type="text">\n'+ 
				'</div>\n' +
				'<div id="applicant_first_name_error" class=""></div>\n'+
			 '</div>\n' +
			'<div class="clearfix">\n' +
				'<label for="xlInput3">Last Name</label>\n' +
				'<div class="input">\n' +
					'<input size="30" name="applicant_last_name" id="applicant_last_name"  class="xlarge" type="text">\n' +
					'<div id="applicant_last_name_error" class=""></div>\n'+
				'</div>\n' +
			'</div>\n' +
			'<div class="clearfix">\n' +
				'<label>Date of Birth</label>\n' +
				'<div class="input">\n' +
					'<div class="inline-inputs">\n' +
						'<input placeholder="MM" name="applicant_dob_mm" id="applicant_dob_mm" maxlength="2" onkeyup="shiftbox(this,\'applicant_dob_dd\')" class="micro" type="text">\n' +
						'<input placeholder="DD" name="applicant_dob_dd" id="applicant_dob_dd" maxlength="2" onkeyup="shiftbox(this,\'applicant_dob_yy\')" class="micro" type="text">\n' +
						'<input placeholder="YYYY" name="applicant_dob_yy" id="applicant_dob_yy" maxlength="4" size="4" type="text">\n' +
					'</div>\n' +
				'</div>\n' +
				'<div id="applicant_dob_yy_error" class=""></div>\n'+
			 '</div>\n' +
			'<div class="clearfix">\n' +
				'<label for="normalSelect">Gender</label>\n' +
				'<div class="input">\n' +
					'<select  name="applicant_gender" id="applicant_gender">\n' +
						'<option>Male</option>\n' +
						'<option>Female</option>\n' +
					'</select>\n' +
				'</div>\n' +
			'</div>\n' +
			'<div class="clearfix">\n' +
				'<label>Social Security Number</label>\n' +
				'<div class="input">\n' +
					'<div class="inline-inputs">\n' +
						'<input  name="applicant_ssn1" value="123"  id="applicant_ssn1" size=3  maxlength="3" onkeyup="shiftbox(this,\'applicant_ssn2\')" class="micro" type="text">\n' +
						'<input  name="applicant_ssn2" value="45"   id="applicant_ssn2" size=2  maxlength="2" onkeyup="shiftbox(this,\'applicant_ssn3\')" class="micro" type="text">\n' +
						'<input  name="applicant_ssn3" value="6789" id="applicant_ssn3" size=4  maxlength="4" class="micro" type="text">\n' +
					'</div>\n' +
				'</div>\n' +
				'<div id="applicant_ssn3_error" class=""></div>\n'+
			'</div>\n' +
			'<div class="clearfix">\n' +
				'<label id="optionsRadio">Do you have health insurance through your employer?</label>\n' +
				'<div class="input">\n' +
					'<ul class="inputs-list">\n' +
						'<li>\n' +
							'<label class="stacked">\n' +
								'<input value="Yes" name="applicant_insured" id="applicant_insured_yes" onClick="toggleEmp(this);" type="radio">\n' +
								'<span>Yes</span>\n' +
							'</label>\n' +
							'<label class="stacked">\n' +
							    '<input value="No" name="applicant_insured" id="applicant_insured_no" onClick="toggleEmp(this);" checked="checked"  type="radio">\n' +
							    '<span>No</span>\n' +
							'</label>\n' +
						'</li>\n' +
						'<fieldset id="applicant_employerinfo" style="display:none;">\n'+
							'<ul>\n'+
								'<li class="clearfix">\n' +
								'<label class="question clearfix">Please provide the following information about the employer and its sponsored plan:</label>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3">Name of Employer</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="30" name="applicant_employer" id="applicant_employer" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3">Employer Identification Number</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="30" name="applicant_empId" id="applicant_empId" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3" >Health Plan Name and ID Number</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="5" name="applicant_esiPlan" id="applicant_esiPlan" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3" >How much are the employee monthly premiums for this plan?</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="5" name="applicant_premium" id="applicant_premium" class="mini">\n' +
									'</div>\n' +
								'</div>\n' +
								'</li>\n' +
							'</ul>\n'+
						'</fieldset>\n'+
					'</ul>\n' +
				'</div>\n' +
			'</div>\n' +
			'<div class="clearfix">\n' +
				'<label id="optionsCheckboxes">Please check all those that are applicable to you:</label>\n' +
					'<div class="input">\n' +
						'<ul class="inputs-list">\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes" name="applicant_smoker" id="applicant_smoker" type="checkbox">\n' +
									'<span>Smoker</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_medicalcondition" id="applicant_medicalcondition" type="checkbox">\n' +
									'<span>Emergency Medical Condition</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_pregnant" id="applicant_pregnant" type="checkbox">\n' +
									'<span>Pregnant</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_nativeamerican" id="applicant_nativeamerican" type="checkbox">\n' +
									'<span>Native American Indian</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_disabled" id="applicant_disabled" type="checkbox">\n' +
									'<span>Disabled or Blind</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_employeddisabled" id="applicant_employeddisabled" type="checkbox">\n' +
									'<span>Employed person with disability</span>\n' +
								'</label>\n' +
							'</li>\n' +
							'<li>\n' +
								'<label>\n' +
									'<input value="Yes"  name="applicant_cancer" id="applicant_cancer" type="checkbox">\n' +
									'<span>Person affected by breast or cervical cancer</span>\n' +
								'</label>\n' +
							'</li>\n' +
						'</ul>\n' +
					'</div>\n' +
				'</div>\n' +
			'</div>\n' +
		'</fieldset>\n', 
spouse_row:
'<fieldset id="family_info_FID">\n' +
	'<legend id="family_legend_FID" class="open togglelink">Your Spouse</legend>\n' +
	'<div class="profile" style="display:block">\n' +
		'<div class="clearfix">\n' +
			'<a class="right btn small danger" href="javascript:family_info.delete_line(\'FID\', \'spouse\' )" id="remove">Remove Spouse</a>\n' +
			'<input type="hidden" id="relation_FID" name="relation[]" value="spouse">\n' +
			'<label for="xlInput3">First Name</label>  \n' +
			'<div class="input">\n' +
				'<input size="30" name="spouse_first_name" id="spouse_first_name" class="xlarge" type="text">\n' +
			'</div>\n' +
			'<div id="spouse_first_name_error" class=""></div>\n'+
		'</div>\n' +
		'<div class="clearfix">\n' +
			'<label for="xlInput3">Last Name</label>\n' +
			'<div class="input">\n' +
				'<input size="30"  name="spouse_last_name" id="spouse_last_name" class="xlarge" type="text">\n' +
			'</div>\n' +
			'<div id="spouse_last_name_error" class=""></div>\n'+
		'</div><!-- /clearfix -->\n' +
		'<div class="clearfix">\n' +
			'<label>Date of Birth</label>\n' +
			'<div class="input">\n' +
				'<div class="inline-inputs">\n' +
					'<input placeholder="MM" name="spouse_dob_mm" id="spouse_dob_mm" maxlength="2" onkeyup="shiftbox(this,\'spouse_dob_dd\')" class="micro" type="text">\n' +
					'<input placeholder="DD" name="spouse_dob_dd"   id="spouse_dob_dd" maxlength="2" onkeyup="shiftbox(this,\'spouse_dob_yy\')" class="micro" type="text">\n' +
					'<input placeholder="YYYY" name="spouse_dob_yy"  id="spouse_dob_yy" maxlength="4" size="4" type="text">\n' +
				'</div>\n' +
			'</div>\n' +
			'<div id="spouse_dob_yy_error" class=""></div>\n'+
		'</div>\n' +
		'<div class="clearfix">\n' +
			'<label for="normalSelect">Gender</label>\n' +
			'<div class="input">\n' +
				'<select name="spouse_gender" id="spouse_gender">\n' +
					'<option>Male</option>\n' +
					'<option>Female</option>\n' +
				'</select>\n' +
			'</div>\n' +
		'</div>\n' +
		'<div class="clearfix">\n' +
			'<label>Social Security Number</label>\n' +
			'<div class="input">\n' +
				'<div class="inline-inputs">\n' +
					'<input name="spouse_ssn1" id="spouse_ssn1" value="123"   size=3 maxlength="3" onkeyup="shiftbox(this,\'spouse_ssn2\')" class="micro" type="text">\n' +
					'<input name="spouse_ssn2" id="spouse_ssn2" value="45"  size=2 maxlength="2" onkeyup="shiftbox(this,\'spouse_ssn3\')" class="micro" type="text">\n' +
					'<input name="spouse_ssn3" id="spouse_ssn3" value="6789"  size=4 maxlength="4" class="micro" type="text">\n' +
				'</div>\n' +
			'</div>\n' +
			'<div id="spouse_ssn3_error" class=""></div>\n'+
		'</div>\n' +
		'<div class="clearfix">\n' +
			'<label id="optionsRadio">Do you and your spouse file a joint tax return?</label>\n' +
			'<div class="input">\n' +
				'<ul class="inputs-list">\n' +
					'<li>\n' +
						'<label class="stacked">\n' +
							'<input value="Yes" name="spouse_jointtax" id="spouse_jointtax_yes" type="radio">\n' +
							'<span>Yes</span>\n' +
						'</label>\n' +
						'<label class="stacked">\n' +
							'<input value="No" name="spouse_jointtax" id="spouse_jointtax_no" checked="checked" type="radio">\n' +
							'<span>No</span>\n' +
						'</label>\n' +
					'</li>\n' +
				'</ul>\n' +
			'</div>\n' +
		'</div><!-- clearfix-->\n' +
		'<div class="clearfix">\n' +
			'<label id="optionsRadio">Is your spouse a citizen, national, or non-citizen lawfully present in the United States?</label>\n' +
			'<div class="input">\n' +
				'<ul class="inputs-list">\n' +
					'<li>\n' +
						'<label class="stacked">\n' +
							'<input value="Yes"  name="spouse_citizen" id="spouse_citizen_yes" type="radio">\n' +
							'<span>Yes</span>\n' +
						'</label>\n' +
						'<label class="stacked">\n' +
							'<input value="No"  name="spouse_citizen" id="spouse_citizen_no"  checked="checked"  type="radio">\n' +
							'<span>No</span>\n' +
						'</label>\n' +
					'</li>\n' +
				'</ul>\n' +
			'</div>\n' +
		'</div><!-- clearfix-->\n' +
		'<div class="clearfix">\n' +
			'<label id="optionsRadio">Does he/she reside or intend to reside in Minnesota during coverage?</label>\n' +
			'<div class="input">\n' +
				'<ul class="inputs-list">\n' +
					'<li>\n' +
						'<label class="stacked">\n' +
							'<input value="Yes"  name="spouse_minresident" id="spouse_minresident_yes"  type="radio">\n' +
							'<span>Yes</span>\n' +
						'</label>\n' +
						'<label class="stacked">\n' +
							'<input value="No"  name="spouse_minresident" id="spouse_minresident_no" checked="checked" type="radio">\n' +
							'<span>No</span>\n' +
						'</label>\n' +
					'</li>\n' +
				'</ul>\n' +
			'</div>\n' +
		'</div><!-- clearfix-->\n' +
		'<div class="clearfix">\n' +
			'<label id="optionsRadio">Does your spouse have health insurance through his/her employer?</label>\n' +
			'<div class="input">\n' +
				'<ul class="inputs-list">\n' +
					'<li>\n' +
						'<label class="stacked">\n' +
							'<input value="Yes" name="spouse_insured" id="spouse_insured_yes" onClick="toggleEmp(this);"  type="radio">\n' +
							'<span>Yes</span>\n' +
						'</label>\n' +
						'<label class="stacked">\n' +
							'<input value="No" name="spouse_insured" id="spouse_insured_no" onClick="toggleEmp(this);" checked="checked"  type="radio">\n' +
							'<span>No</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<fieldset id="spouse_employerinfo" style="display:none;">\n'+
						'<ul>\n'+
							'<li class="clearfix">\n' +
								'<label class="question clearfix">Please provide the following information about the employer and its sponsored plan:</label>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3">Name of Employer</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="30" name="spouse_employer" id="spouse_employer" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3">Employer Identification Number</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="30" name="spouse_empId" id="spouse_empId" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3" >Health Plan Name and ID Number</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="5" name="spouse_esiPlan" id="spouse_esiPlan" class="xlarge">\n' +
									'</div>\n' +
								'</div>\n' +
								'<div class="clearfix">\n' +
									'<label for="xlInput3" >How much are the employee monthly premiums for this plan?</label>\n' +
									'<div class="input">\n' +
										'<input type="text" size="5" name="spouse_premium" id="spouse_premium" class="mini">\n' +
									'</div>\n' +
								'</div>\n' +
							'</li>\n' +
						'</ul>\n'+
					'</fieldset>\n'+
				'</ul>\n' +
			'</div>\n' +
		'</div><!-- clearfix-->\n' +
		'<div class="clearfix">\n' +
			'<label id="optionsCheckboxes">Please check all those that are applicable to you:</label>\n' +
			'<div class="input">\n' +
				'<ul class="inputs-list">\n' +
					'<li>\n' +
						'<label>\n' +
							'<input value="Yes" name="spouse_smoker" id="spouse_smoker"  type="checkbox">\n' +
							'<span>Smoker</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
						'<label>\n' +
							'<input value="Yes" name="spouse_pregnant" id="spouse_pregnant"  type="checkbox">\n' +
							'<span>Pregnant</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
						'<label>\n' +
							'<input value="Yes"  name="spouse_medicalcondition" id="spouse_medicalcondition" type="checkbox">\n' +
							'<span>Emergency Medical Condition</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
						'<label>\n' +
							'<input value="Yes"  name="spouse_nativeamerican" id="spouse_nativeamerican" type="checkbox">\n' +
							'<span>Native American Indian</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
						'<label>\n' +
							 '<input value="Yes"  name="spouse_disabled" id="spouse_disabled" type="checkbox">\n' +
							 '<span>Disabled or Blind</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
						'<label>\n' +
							'<input value="Yes"  name="spouse_employeddisabled" id="spouse_employeddisabled" type="checkbox">\n' +
							'<span>Employed person with disability</span>\n' +
						'</label>\n' +
					'</li>\n' +
					'<li>\n' +
					'<label>\n' +
						'<input value="Yes"  name="spouse_cancer" id="spouse_cancer" type="checkbox">\n' +
						'<span>Person affected by breast or cervical cancer</span>\n' +
					'</label>\n' +
					'</li>\n' +
				'</ul>\n' +
			'</div>\n' +
		'</div>\n' +
	'</div><!-- profile -->\n' +
'</fieldset>\n',
children_row:
'<fieldset id="family_info_FID">\n'+
'<legend id="family_legend_FID" class="open togglelink">Child</legend>\n'+
'<div class="profile" style="display:block">\n'+
	'<div class="clearfix">\n'+
		 '<input type="hidden" id="relation_FID" name="relation[]" value="child_CN">\n' +
		 '<a  class="right btn small danger" href="javascript:family_info.delete_line(\'FID\', \'child\' )" id="remove">Remove Child</a>\n' +
		'<label for="xlInput3">First Name</label>\n'+  
		'<div class="input"><input size="30"  name="child_CN_first_name" id="child_CN_first_name"  class="xlarge" type="text"></div>\n'+
		'<div id="child_CN_first_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="xlInput3">Last Name</label>\n'+
		'<div class="input"><input size="30"  name="child_CN_last_name" id="child_CN_last_name"  class="xlarge" type="text"></div>\n'+
		'<div id="child_CN_last_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Date of Birth</label>\n'+
		'<div class="input">\n'+
		      '<div class="inline-inputs">\n'+
			'<input placeholder="MM" name="child_CN_dob_mm" id="child_CN_dob_mm" maxlength="2" onkeyup="shiftbox(this,\'child_CN_dob_dd\')" class="micro" type="text">\n'+
			'<input placeholder="DD" name="child_CN_dob_dd" id="child_CN_dob_dd" maxlength="2" onkeyup="shiftbox(this,\'child_CN_dob_yy\')" class="micro" type="text">\n'+
			'<input placeholder="YYYY" name="child_CN_dob_yy" id="child_CN_dob_yy" maxlength="4" size="4" type="text">\n'+
		      '</div>\n'+
		'</div>\n'+
		'<div id="child_CN_dob_yy_error" class=""></div>\n'+
	'</div>\n'+
        '<div class="clearfix">\n'+
	   '<label for="normalSelect">Gender</label>\n'+
	    '<div class="input">\n'+
	        '<select  name="child_CN_gender" id="child_CN_gender">\n'+
			'<option>Male</option>\n'+
			'<option>Female</option>\n'+
	        '</select>\n'+
	    '</div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Social Security Number <a href="#" data-original-title="SSN# or ATIN#:" data-content="If your child is adopted and does not have a social security number yet, please provide his/her Adoption Taxpayer Identification Number (ATIN)." rel="popover"><span class="label">?</span></a></label>\n'+
		'<div class="input">\n'+
			'<div class="inline-inputs">\n'+
				'<input  name="child_CN_ssn1" id="child_CN_ssn1" value="123"    maxlength="3" onkeyup="shiftbox(this,\'child_CN_ssn2\')" class="micro" type="text">\n'+
				'<input  name="child_CN_ssn2" id="child_CN_ssn2" value="45"     maxlength="2" onkeyup="shiftbox(this,\'child_CN_ssn3\')" class="micro" type="text">\n'+
				'<input  name="child_CN_ssn3" id="child_CN_ssn3" value="6789"   maxlength="4" class="micro" type="text">\n'+
			'</div>\n'+
		'</div>\n'+
		'<div id="child_CN_ssn3_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label id="optionsRadio">Is your child a citizen, national, or non-citizen lawfully present in the United States?</label>\n'+
		'<div class="input">\n'+
			'<ul class="inputs-list">\n'+
				'<li>\n'+
				'<label class="stacked">\n'+
					'<input value="Yes" name="child_CN_citizen" id="child_CN_citizen_yes" type="radio">\n'+
					'<span>Yes</span>\n'+
				'</label>\n'+
				'<label class="stacked">\n'+
					'<input value="No" name="child_CN_citizen" id="child_CN_citizen_no" checked="checked"  type="radio">\n'+
					'<span>No</span>\n'+
				'</label>\n'+
				'</li>\n'+
			 '</ul>\n'+
	    '</div>\n'+
	'</div>\n'+
       '<div class="clearfix">\n'+
		'<label>Does he/she reside or intend to reside in Minnesota during coverage?</label>\n'+
		'<div class="input">\n'+
			'<ul class="inputs-list">\n'+
				'<li>\n'+
				'<label class="stacked">\n'+
					'<input value="Yes"  name="child_CN_minresident" id="child_CN_minresident_yes"  type="radio">\n'+
					'<span>Yes</span>\n'+
				'</label>\n'+
				'<label class="stacked">\n'+
					'<input value="No" name="child_CN_minresident" id="child_CN_minresident_no" checked="checked" type="radio">\n'+
					'<span>No</span>\n'+
				'</label>\n'+
				'</li>\n'+
			'</ul>\n'+
		'</div>\n'+
      '</div>\n'+
      '<div class="clearfix">\n'+
		'<label id="optionsRadio">Does your child have health insurance through an employer?</label>\n'+
		'<div class="input">\n'+
			'<ul class="inputs-list">\n'+
				'<li>\n'+
				'<label class="stacked">\n'+
					'<input value="Yes" name="child_CN_insured" id="child_CN_insured_yes" onClick="toggleEmp(this);"  type="radio">\n'+
					'<span>Yes</span>\n'+
				'</label>\n'+
				'<label class="stacked">\n'+
					'<input value="No"  name="child_CN_insured" id="child_CN_insured_no" onClick="toggleEmp(this);" checked="checked" type="radio">\n'+
					'<span>No</span>\n'+
				'</label>\n'+
				'</li>\n'+
				'<fieldset id="child_CN_employerinfo" style="display:none;">\n'+
				'<ul>\n'+
					'<li class="clearfix">\n' +
						'<label class="question clearfix">Please provide the following information about the employer and its sponsored plan:</label>\n' +
						'<div class="clearfix">\n' +
							'<label for="xlInput3">Name of Employer</label>\n' +
							'<div class="input">\n' +
								'<input type="text" size="30" name="child_CN_employer" id="child_CN_employer" class="xlarge">\n' +
							'</div>\n' +
						'</div>\n' +
						'<div class="clearfix">\n' +
							'<label for="xlInput3">Employer Identification Number</label>\n' +
							'<div class="input">\n' +
								'<input type="text" size="30" name="child_CN_empId" id="child_CN_empId" class="xlarge">\n' +
							'</div>\n' +
						'</div>\n' +
						'<div class="clearfix">\n' +
							'<label for="xlInput3" >Health Plan Name and ID Number</label>\n' +
							'<div class="input">\n' +
								'<input type="text" size="5" name="child_CN_esiPlan" id="child_CN_esiPlan" class="xlarge">\n' +
							'</div>\n' +
						'</div>\n' +
						'<div class="clearfix">\n' +
							'<label for="xlInput3" >How much are the employee monthly premiums for this plan?</label>\n' +
							'<div class="input">\n' +
								'<input type="text" size="5" name="child_CN_premium" id="child_CN_premium" class="mini">\n' +
							'</div>\n' +
						'</div>\n' +
					'</li>\n' +
				'</ul>\n'+
			'</fieldset>\n'+
			'</ul>\n'+
		'</div>\n'+
      '</div>\n'+
      '<div class="clearfix">\n'+
		'<label id="optionsCheckboxes">Please check all those that are applicable to you:</label>\n'+
		'<div class="input">\n'+
			'<ul class="inputs-list">\n'+
				'<li>\n'+
				'<label>\n'+
					'<input value="Yes" name="child_CN_smoker" id="child_CN_smoker" type="checkbox">\n'+
					'<span>Smoker</span>\n'+
				'</label>\n'+
				'</li>\n'+
				'<li>\n'+
				'<label>\n'+
					'<input value="Yes" name="child_CN_medicalcondition" id="child_CN_medicalcondition" type="checkbox">\n'+
					'<span>Emergency Medical Condition</span>\n'+
				'</label>\n'+
				'</li>\n'+
				'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="Yes" name="child_CN_pregnant" id="child_CN_pregnant" >\n'+
					'<span>Pregnant</span>\n'+
				'</label>\n'+
				'</li>\n'+
				'<li>\n'+
				'<label>\n'+
					'<input value="Yes" name="child_CN_nativeamerican" id="child_CN_nativeamerican" type="checkbox">\n'+
					'<span>Native American Indian</span>\n'+
				'</label>\n'+
				'</li>\n' +
				'<li>\n' +
				'<label>\n'+
					'<input value="Yes" name="child_CN_disabled" id="child_CN_disabled" type="checkbox">\n'+
					'<span>Disabled or Blind</span>\n'+
				'</label>\n'+
				'</li>\n'+
				'<li>\n' +
				'<label>\n' +
				    '<input value="Yes"  name="child_CN_employeddisabled" id="child_CN_employeddisabled" type="checkbox">\n' +
				    '<span>Employed person with disability</span>\n' +
				'</label>\n' +
				'</li>\n' +
				'<li>\n' +
				'<label>\n' +
				    '<input value="Yes"  name="child_CN_cancer" id="child_CN_cancer" type="checkbox">\n' +
				    '<span>Person affected by breast or cervical cancer</span>\n' +
				'</label>\n' +
				'</li>\n' +
			'</ul>\n'+
		'</div>\n'+
	'</div>\n'+
'</div>\n'+
'</fieldset>\n',
	add_applicant: function(){
		this.line_index++
		var row = this.applicant_row 
		row = row.replace(/FID/g, this.line_index);
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		
		$('#applicant_dob_mm').mask('99',{placeholder:""});
		$('#applicant_dob_dd').mask('99',{placeholder:""});
		$('#applicant_dob_yy').mask('9999',{placeholder:""});
		
		$('#applicant_ssn1').mask('999',{placeholder:""});
		$('#applicant_ssn2').mask('99',{placeholder:""});
		$('#applicant_ssn3').mask('9999',{placeholder:""});
		
		jQuery("#applicant_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your first name.</span>" } } );
		jQuery("#applicant_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your last name.</span>" } } );
		jQuery("#applicant_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" } });
		jQuery("#applicant_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
},
	
	add_spouse: function() {
		this.line_index++
		var row = this.spouse_row 
		row = row.replace(/FID/g, this.line_index);
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		if($(('spouse_cell_id'))){
			document.getElementById('spouse_cell_id').innerHTML = ""
		}

		$('#spouse_dob_mm').mask('99',{placeholder:""});
		$('#spouse_dob_dd').mask('99',{placeholder:""});
		$('#spouse_dob_yy').mask('9999',{placeholder:""});
		
		$('#spouse_ssn1').mask('999',{placeholder:""});
		$('#spouse_ssn2').mask('99',{placeholder:""});
		$('#spouse_ssn3').mask('9999',{placeholder:""});

		jQuery("#spouse_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your spouse's first name.</span>" } } );
		jQuery("#spouse_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your spouse's last name.</span>" } } );
		jQuery("#spouse_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" }  });
		jQuery("#spouse_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
	},

		   				   
	add_children: function() {
		this.line_index++;
		this.child_numb++;
		var childCN = this.child_numb;
		var row = this.children_row 
		row = row.replace(/FID/g, this.line_index);
		row = row.replace(/CN/g, this.child_numb)
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		
		$("#child_"+childCN+'_dob_mm').mask('99',{placeholder:""});
		$("#child_"+childCN+'_dob_dd').mask('99',{placeholder:""});
		$("#child_"+childCN+'_dob_yy').mask('9999',{placeholder:""});
		
		$("#child_"+childCN+'_ssn1').mask('999',{placeholder:""});
		$("#child_"+childCN+'_ssn2').mask('99',{placeholder:""});
		$("#child_"+childCN+'_ssn3').mask('9999',{placeholder:""});

		jQuery("#child_"+childCN+"_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your child's first name.</span>" } } );
		jQuery("#child_"+childCN+"_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your child's last name.</span>" } } );
		jQuery("#child_"+childCN+"_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" } });
		jQuery("#child_"+childCN+"_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
	},

	delete_line: function(index, member_type){
		if(confirm('Are you sure you want to delete this record ?')){
			$('#family_info_'+index).remove();
			$('#relation_'+index).remove();
			if(member_type == 'spouse'){
				document.getElementById('spouse_cell_id').innerHTML = '<input type="button" class="btn small primary" onclick="javascript:family_info.add_spouse();" id="add_spouse" name="add_spouse" value="Add Spouse">'
			}
		}
	}	 
}

function toggleEmp(element){
	var elementName = element.name;
	if( elementName.indexOf('applicant') >= 0){ 
		if(element.value == 'Yes'){
			$('#applicant_employerinfo').show();
		}else{
			$('#applicant_employerinfo').hide();
		}
	}
	if( elementName.indexOf('spouse') >= 0){ 
		if(element.value == 'Yes'){
			$('#spouse_employerinfo').show();
		}else{
			$('#spouse_employerinfo').hide();
		}
	}
	if( elementName.indexOf('child') >= 0){
		var childName = elementName.substr(0,7);
		if(element.value == 'Yes'){
			$('#'+childName+'_employerinfo').show();
		}else{
			$('#'+childName+'_employerinfo').hide();
		}
	}
}