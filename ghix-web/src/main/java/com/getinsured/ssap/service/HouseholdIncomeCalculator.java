package com.getinsured.ssap.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.financial.Expense;
import com.getinsured.iex.ssap.financial.Income;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.financial.type.IncomeType;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.ssap.model.financial.TaxFilingRequirement;
import com.getinsured.ssap.model.financial.TaxFilingRequirementType;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.SsapHelper;
import com.getinsured.ssap.util.TaxFilingThresholds;

/**
 * Contains methods to calculate household incomes.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public class HouseholdIncomeCalculator {
  private static final Logger log = LoggerFactory.getLogger(HouseholdIncomeCalculator.class);

  /**
   * Takes list of ALL household members (not filtered!) and any particular household member for whom MAGI needs
   * to be calculated.
   *
   * @param members ALL household members across ALL tax households.
   * @param member member for who MAGI needs to be calculated.
   * @param applicationYear year of this SSAP application.
   * @param primaryTaxFilerPersonId primary tax filer's person id.
   * @return MAGI number in cents.
   */
  static long calculateMagi(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    long income = 0L;
    boolean conditionA = conditionA(members, member, applicationYear, primaryTaxFilerPersonId);
    boolean conditionC = conditionC(members, member);
    boolean incomeAboveTaxFilingThreshold = incomeAboveTaxFilingThreshold(member, applicationYear);
    final List<HouseholdMember> magiHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, member, applicationYear, primaryTaxFilerPersonId);
    final List<HouseholdMember> magiHouseholdMembersExceptCurrent = new ArrayList<>();

    for(HouseholdMember m : magiHouseholdMembers) {
      if(!m.getPersonId().equals(member.getPersonId())) {
        magiHouseholdMembersExceptCurrent.add(m);
      }
    }

    if(conditionA) {
      if(incomeAboveTaxFilingThreshold) {
        income += calculateTotalHouseholdIncome(magiHouseholdMembers);
        log.debug("condition A && Income Above TFT, including income of {} HH Members = {}",
            magiHouseholdMembers.size(), income);
      } else {
        income += calculateTotalHouseholdIncome(magiHouseholdMembersExceptCurrent);
        log.debug("condition A && Income Below TFT, including income of only {} HH Members = {}",
            magiHouseholdMembersExceptCurrent.size(), income);
      }
    } else if(conditionC) {
      if(incomeAboveTaxFilingThreshold) {
        income += calculateTotalHouseholdIncome(magiHouseholdMembers);
        log.debug("condition C && Income Above TFT, including income of {} HH Members = {}",
            magiHouseholdMembers.size(), income);
      } else {
        income += calculateTotalHouseholdIncome(magiHouseholdMembersExceptCurrent);
        log.debug("condition C && Income Below TFT, including income of only {} HH Members = {}",
            magiHouseholdMembersExceptCurrent.size(), income);
      }
    }
    else {
      income += calculateTotalHouseholdIncome(magiHouseholdMembers);
      log.debug("condition A/C not met, including income of {} HH Members", magiHouseholdMembers.size());
    }

    return income;
  }

  private static boolean conditionA(List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> parentsLivingWithMember = HouseholdUtil.getParentsLivingWith(members, member, primaryTaxFilerPersonId);

    int age = getApplicantAge(member.getDateOfBirth(), applicationYear);
    boolean livesWithParents = !parentsLivingWithMember.isEmpty();

    boolean claimedByParents = HouseholdSizeCalculator.isClaimedByParent(members, member);

    // If under ADULT AGE and lives with parents, or claimed by parents
    return age < HouseholdSizeCalculator.ADULT_AGE && livesWithParents || claimedByParents;
  }

  private static boolean conditionC(List<HouseholdMember> members, HouseholdMember member) {
    boolean claimedByAnyone = HouseholdSizeCalculator.isClaimedByAnyoneOtherThanParent(members, member);
    final List<HouseholdMember> claimers = HouseholdSizeCalculator.getClaimers(members, member);
    boolean claimerLivesWithMember = HouseholdUtil.isLivingWith(claimers, member);
    return claimedByAnyone && claimerLivesWithMember;
  }

  /**
   * Calculates total earned income for given {@link HouseholdMember}s.
   * @param members List of {@link HouseholdMember}s
   * @return total earned income.
   */
  static long calculateTotalEarnedIncome(final List<HouseholdMember> members) {
    return calculateTotalHouseholdIncome(members,
        Stream.of(IncomeType.values()).filter(it -> !it.isEarned()).toArray(IncomeType[]::new));
  }

  /**
   * Calculates gross income, does NOT include any expenses.
   * @param members List of {@link HouseholdMember}'s.
   * @return gross income amount w/o any expenses subtracted.
   */
  static long calculateGrossIncome(final List<HouseholdMember> members) {
    long totalIncome = calculateTotalHouseholdIncome(members);
    long totalExpenses = calculateExpensesInDollars(members);
    return totalIncome + totalExpenses;
  }

  /**
   * Calculates total unearned income for given {@link HouseholdMember}s,
   * excluding any SSN benefits.
   *
   * @param members List of {@link HouseholdMember}s
   * @return total unearned income.
   */
  static long calculateTotalUnearnedIncomeWithoutSsnBenefits(final List<HouseholdMember> members) {
    return calculateTotalHouseholdIncome(members,
        Stream.of(IncomeType.values()).filter(it -> it.isEarned() ||
            it == IncomeType.SOCIAL_SECURITY_DISABILITY).toArray(IncomeType[]::new)
    );
  }

  /**
   * Calculates total unearned income for given {@link HouseholdMember}s.
   * @param members List of {@link HouseholdMember}s
   * @return total unearned income.
   */
  static long calculateTotalUnearnedIncome(final List<HouseholdMember> members) {
    return calculateTotalHouseholdIncome(members,
        Stream.of(IncomeType.values()).filter(IncomeType::isEarned).toArray(IncomeType[]::new));
  }

  /**
   * Calculates total household income for all given {@link HouseholdMember}'s, and excludes
   * tribal income portion from it.
   *
   * @param members List of {@link HouseholdMember}'s
   * @return total income - tribal income portion.
   * @see #calculateTotalHouseholdIncome(List)
   * @see #calculateTotalHouseholdTribalIncome(List)
   */
  static long calculateTotalIncomeWithoutTribalPortion(final List<HouseholdMember> members) {
    long totalIncome = calculateTotalHouseholdIncome(members);
    long totalTribalIncome = calculateTotalHouseholdTribalIncome(members);

    return totalIncome - totalTribalIncome;
  }
  /**
   * Calculates total tribal income for given household members. It does not
   * take into account associated expenses.
   *
   * @param members List of {@link HouseholdMember} objects.
   * @return total tribal portion of the member's income.
   */
  static long calculateTotalHouseholdTribalIncome(final List<HouseholdMember> members) {
    long totalIncome;
    long income = 0L;

    for (HouseholdMember hhm : members) {
      List<Income> incomes = hhm.getIncomes();

      for (Income inc : incomes) {
        if(inc.getType() == IncomeType.AI_AN) {
          income += getYearlyTotal(inc.getAmount(), inc.getFrequency(), inc.getCyclesPerFrequency());
        } else {
          income += getYearlyTotal(inc.getTribalAmount(), inc.getFrequency(), inc.getCyclesPerFrequency());
        }
      }
    }

    totalIncome = Math.round( income / 100.0);
    log.info("Total tribal income for {} members is: {}", members.size(), totalIncome);
    return totalIncome;
  }

  /**
   * Returns total household income, by taking all income sources and subtracting
   * all expenses.
   *
   * @param members List of {@link HouseholdMember}
   * @return total household income in <b>whole dollars</b>.
   */
  public static long calculateTotalHouseholdIncome(final List<HouseholdMember> members) {
    return calculateTotalHouseholdIncome(members, null);
  }

  /**
   * Returns total household income, by taking all income sources and subtracting
   * all expenses.
   *
   * @param members List of {@link HouseholdMember}
   * @param skipTypes Skip given {@link IncomeType} in calculations.
   * @return total household income in <b>whole dollars</b>.
   */
  private static long calculateTotalHouseholdIncome(final List<HouseholdMember> members, IncomeType... skipTypes) {
    long totalIncome;
    long income = 0L;

    Set<IncomeType> skipIncomeTypes = new HashSet<>();

    if(skipTypes != null) {
      skipIncomeTypes = Stream.of(skipTypes).collect(Collectors.toSet());
    }

    for (HouseholdMember hhm : members) {
      if(hhm.getAnnualTaxIncome() != null && hhm.getAnnualTaxIncome().getProjectedIncome() > 0) {
       log.debug("Household member has projected annual income of: {}", hhm.getAnnualTaxIncome().getProjectedIncome()/100);
       income += hhm.getAnnualTaxIncome().getProjectedIncome();
      } else {
        List<Income> incomes = hhm.getIncomes();

        for (Income inc : incomes) {
          if(skipIncomeTypes.contains(inc.getType())) {
            continue;
          }

          income += getYearlyTotal(inc.getAmount(), inc.getFrequency(), inc.getCyclesPerFrequency());
        }
      }
    }

    long expense = calculateHouseholdExpenses(members, skipTypes);

    totalIncome = Math.round( (income - expense) / 100.0);

    log.debug("Total household income for {} members is: {}", members.size(), totalIncome);
    return totalIncome;
  }

  /**
   * Calculates total household income for Eligibility Purposes.
   * @param application {@link SingleStreamlinedApplication} object.
   * @return calculated income.
   */
  public static long calculateTotalHouseholdIncomeForEligibility(final SingleStreamlinedApplication application) {
    List<HouseholdMember> primaryHouseholdMembers = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);
    int primaryTaxFilerPersonId = application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId();
    long coverageYear = SsapHelper.getApplicationId(application);
    final List<HouseholdMember> filteredMembers = filterDependentsBelowTaxFilingThreshold(primaryHouseholdMembers, coverageYear, primaryTaxFilerPersonId);
    return calculateTotalHouseholdIncome(filteredMembers);
  }

  static List<HouseholdMember> filterDependentsBelowTaxFilingThreshold(final List<HouseholdMember> members, long applicationYear, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> filteredList = new ArrayList<>(members.size());
    members.forEach(hhm -> {
      boolean include = true;

      if(HouseholdUtil.isDependent(hhm) && !HouseholdIncomeCalculator.isDependentOverTaxFilingThreshold(members, hhm, applicationYear, primaryTaxFilerPersonId)) {
        include = false;
      }

      if(HouseholdUtil.isDanglingMember(hhm)) {
        include = false;
      }

      if(include) {
        filteredList.add(hhm);
      }
    });

    return filteredList;
  }

  /**
   * Calculates all tax household expenses.
   *
   * @param members List of {@link HouseholdMember}'s
   * @param skipTypes type of incomes to skip.
   * @return sum of all expenses in <b>cents</b>
   * @see #calculateExpensesInDollars(List, IncomeType...)
   */
  static long calculateHouseholdExpenses(final List<HouseholdMember> members, IncomeType... skipTypes) {
    long expense = 0L;
    Set<IncomeType> skipIncomeTypes = new HashSet<>();

    if(skipTypes != null) {
      skipIncomeTypes = Stream.of(skipTypes).collect(Collectors.toSet());
    }

    for (HouseholdMember hhm : members) {
        List<Income> incomes = hhm.getIncomes();
        List<Expense> expenses = hhm.getExpenses();

        for (Income inc : incomes) {
          if(skipIncomeTypes.contains(inc.getType())) {
            continue;
          }
          if (inc.getRelatedExpense() > 0L) {
            expense += getYearlyTotal(inc.getRelatedExpense(), inc.getFrequency(), inc.getCyclesPerFrequency());
          }
        }

        for (Expense exp : expenses) {
          expense += getYearlyTotal(exp.getAmount(), exp.getFrequency(), 0);
        }
    }

    return expense;
  }

  /**
   * Calculates all expenses in whole dollars.
   * @param members List of {@link HouseholdMember}'s.
   * @param skipTypes What types of {@link IncomeType} to skip for this calculations.
   * @return all expenses rounded to <b>whole dollars</b>.
   * @see #calculateHouseholdExpenses(List, IncomeType...) 
   */
  private static long calculateExpensesInDollars(final List<HouseholdMember> members, IncomeType... skipTypes) {
    long expenses = calculateHouseholdExpenses(members, skipTypes);
    return Math.round(expenses / 100.0);
  }

  /**
   * Returns boolean if given {@link HouseholdMember}'s {@link Income} is above tax filing threshold.
   *
   * @param member {@link HouseholdMember} object.
   * @param applicationYear application year, based on which thresholds are calculated.
   * @return boolean if income was above threshold.
   */
  static boolean incomeAboveTaxFilingThreshold(final HouseholdMember member, final int applicationYear) {
    final List<TaxFilingRequirement> taxFilingRequirements = TaxFilingThresholds.getTaxFilingRequirementsByYear(applicationYear);
    int age = getApplicantAge(member.getDateOfBirth(), applicationYear);

    long earnedIncome = calculateTotalEarnedIncome(Collections.singletonList(member));

    if(earnedIncome < 0) {
      earnedIncome = 0L;
    }

    long unearnedIncome = calculateTotalUnearnedIncomeWithoutSsnBenefits(Collections.singletonList(member));

    if(unearnedIncome < 0) {
      unearnedIncome = 0L;
    }

    long grossIncome = calculateGrossIncome(Collections.singletonList(member));

    long over65EarnedLimit = 0L;
    long over65AndBlindEarnedLimit = 0L;
    long under65EarnedLimit = 0L;

    assert taxFilingRequirements != null : "TaxFilingRequirements should not be null";

    for(TaxFilingRequirement taxFilingRequirement : taxFilingRequirements) {
      if(taxFilingRequirement.getType() == TaxFilingRequirementType.SINGLE_UNDER_65) {
        under65EarnedLimit = taxFilingRequirement.getDollarAmount();
      } else if(taxFilingRequirement.getType() == TaxFilingRequirementType.SINGLE_OVER_65) {
        over65EarnedLimit = taxFilingRequirement.getDollarAmount();
      } else if(taxFilingRequirement.getType() == TaxFilingRequirementType.SINGLE_BLIND_OVER_65) {
        over65AndBlindEarnedLimit = taxFilingRequirement.getDollarAmount();
      }
    }

    if(age >= 65 || member.isBlindOrDisabled()) {
      if(member.isBlindOrDisabled()) {
        if(unearnedIncome > 4_250L) {
          return true;
        } else if(earnedIncome > over65AndBlindEarnedLimit) {
          return true;
        } else if(grossIncome > 4_250L) {
          return true;
        } else if(grossIncome > (earnedIncome + 3_550L)) {
          return true;
        }
      } else {
        if(unearnedIncome > 2_650L) {
          return true;
        } else if(earnedIncome > over65EarnedLimit) {
          return true;
        } else if(grossIncome > 2_650L) {
          return true;
        } else if(grossIncome > (earnedIncome + 1_950L)) {
          return true;
        }
      }
    } else {
      if(unearnedIncome > 1_050L) {
        return true;
      } else if(earnedIncome > under65EarnedLimit) {
        return true;
      } else if(grossIncome > 1_050L) {
        return true;
      } else if(grossIncome > (earnedIncome + 350)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Returns boolean if given {@link HouseholdMember} dependent {@link Income} is above tax filing threshold.
   *
   * <p>
   *   <pre>
   * === Single dependents—Were you either age 65 or older or blind? ===
   * > No. You must file a return if any of the following apply.
   * 1. Your unearned income was more than $1,050.
   * 2. Your earned income was more than $12,000.
   * 3. Your gross income was more than the larger of—
   * a. $1,050, or
   * b. Your earned income (up to $11,650) plus $350.
   *
   * > Yes. You must file a return if any of the following apply.
   * 1. Your unearned income was more than $2,650 ($4,250 if 65 or older and blind).
   * 2. Your earned income was more than $13,600 ($15,200 if 65 or older and blind).
   * 3. Your gross income was more than the larger of—
   * a. $2,650 ($4,250 if 65 or older and blind), or
   * b. Your earned income (up to $11,650) plus $1,950 ($3,550 if 65 or older and blind).
   *
   * === Married dependents—Were you either age 65 or older or blind? ===
   * > No. You must file a return if any of the following apply.
   * 1. Your gross income was at least $5 and your spouse files a separate return and itemizes deductions.
   * 2. Your unearned income was more than $1,050.
   * 3. Your earned income was more than $12,000.
   * 4. Your gross income was more than the larger of—
   * a. $1,050, or
   * b. Your earned income (up to $11,650) plus $350.
   *
   * > Yes. You must file a return if any of the following apply.
   * 1. Your gross income was at least $5 and your spouse files a separate return and itemizes deductions.
   * 2. Your unearned income was more than $2,350 ($3,650 if 65 or older and blind).
   * 3. Your earned income was more than $13,300 ($14,600 if 65 or older and blind).
   * 4. Your gross income was more than the larger of—
   * a. $2,350 ($3,650 if 65 or older and blind), or
   * b. Your earned income (up to $11,650) plus $1,650 ($2,950 if 65 or older and blind).
   *   </pre>
   * </p>
   *
   * For more information see page 4 of https://www.irs.gov/pub/irs-pdf/p501.pdf
   *
   * @param members all household members
   * @param member {@link HouseholdMember} object (who is dependent)
   * @param applicationYear application year, based on which thresholds are calculated.
   * @param primaryTaxFilerPersonId primary tax filer person id.
   * @return boolean if income was above threshold.
   */
  static boolean isDependentOverTaxFilingThreshold(final List<HouseholdMember> members, final HouseholdMember member, final long applicationYear, final int primaryTaxFilerPersonId) {
    final boolean over65 = getApplicantAge(member.getDateOfBirth(), applicationYear) > 65;
    final boolean blind = member.isBlindOrDisabled();
    boolean isMarried = HouseholdUtil.isMarried(members, member, primaryTaxFilerPersonId);
    boolean spouseFilingSeparately = false;
    HouseholdMember spouse = null;

    if(isMarried) {
      spouse = HouseholdSizeCalculator.getSpouse(members, member);
      if(spouse != null) {
        spouseFilingSeparately = spouse.getTaxHouseholdComposition().getTaxFilingStatus() == TaxFilingStatus.FILING_SEPARATELY;
      }
    }

    long earnedIncome = calculateTotalEarnedIncome(Collections.singletonList(member));

    if(earnedIncome < 0) {
      earnedIncome = 0L;
    }

    long unearnedIncome = calculateTotalUnearnedIncomeWithoutSsnBenefits(Collections.singletonList(member));

    if(unearnedIncome < 0) {
      unearnedIncome = 0L;
    }

    long grossIncome = calculateGrossIncome(Collections.singletonList(member));

    if(isMarried) {
      if(grossIncome > 5L && spouseFilingSeparately) {
        return true;
      }

      if(over65 || blind) {
        if(over65 && blind) {
          if(unearnedIncome > 3_650L) {
            return true;
          } else if(earnedIncome > 14_600L) {
            return true;
          } else if(grossIncome > 3_650L || grossIncome > (earnedIncome + 2_950L)) {
            return true;
          }
        } else {
          if(unearnedIncome > 2_350L) {
            return true;
          } else if(earnedIncome > 13_300L) {
            return true;
          } else if(grossIncome > 2_350L || grossIncome > (earnedIncome + 1_650L)) {
            return true;
          }
        }
      } else {
        if(unearnedIncome > 1_050L) {
          return true;
        } else if(earnedIncome > 12_000L) {
          return true;
        } else if(grossIncome > 1_050L || grossIncome > (earnedIncome + 350L)) {
          return true;
        }
      }
    }
    else {
      if(over65 || blind) {
        if(over65 && blind) {
          if(unearnedIncome > 4_250L) {
            return true;
          } else if(earnedIncome > 15_200L) {
            return true;
          } else if(grossIncome > 4_250L || grossIncome > (earnedIncome + 3_550L)) {
            return true;
          }
        }
        else {
          if(unearnedIncome > 2_650L) {
            return true;
          } else if(earnedIncome > 13_600L) {
            return true;
          } else if(grossIncome > 2_650L || grossIncome > (earnedIncome + 1_950L)) {
            return true;
          }
        }
      }
      else {
        if(unearnedIncome > 1_050L) {
          return true;
        } else if(earnedIncome > 12_000L) {
          return true;
        } else if(grossIncome > 1_050L || grossIncome > (earnedIncome + 350L)) {
          return true;
        }
      }
    }

    return false;
  }

  static int getApplicantAge(Date dob, int applicationYear) {
    LocalDate endOfApplicationYear = LocalDate.of(applicationYear, 12, 31);
    LocalDate birthDate = dob.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    Period period = Period.between(birthDate, endOfApplicationYear);
    return period.getYears();
  }

  static int getApplicantAge(Date dob, long applicationYear) {
    LocalDate endOfApplicationYear = LocalDate.of(Math.toIntExact(applicationYear), 12, 31);
    LocalDate birthDate = dob.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    Period period = Period.between(birthDate, endOfApplicationYear);
    return period.getYears();
  }

  private static long getYearlyTotal(long amount, Frequency frequency, int cyclesPerFrequency) {
    long total;
    switch (frequency) {
      case MONTHLY:
        total = amount * 12;
        break;
      case WEEKLY:
        total = amount * 52;
        break;
      case BIMONTHLY:
        total = amount * 24;
        break;
      case BIWEEKLY:
        total = amount * 26;
        break;
      case DAILY:
        // Default to 5 days per week
        if(cyclesPerFrequency <= 0) {
          cyclesPerFrequency = 5;
        }
        total = amount * cyclesPerFrequency * 52;
        break;
      case HOURLY:
        // Default to 40 hours per week
        if(cyclesPerFrequency <= 0) {
          cyclesPerFrequency = 40;
        }
        total = amount * cyclesPerFrequency * 52;
        break;
      case QUARTERLY:
        total = amount * 4;
        break;
      case ONCE:
      case YEARLY:
      default:
        total = amount;
        break;
    }

    return total;
  }
}
