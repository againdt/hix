
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmitAgency3DHSResubRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitAgency3DHSResubRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}CaseNumber"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}SEVISID"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}RequestedCoverageStartDate"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}FiveYearBarApplicabilityIndicator"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}RequesterCommentsForHub" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}CategoryCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitAgency3DHSResubRequestType", propOrder = {
    "caseNumber",
    "sevisid",
    "requestedCoverageStartDate",
    "fiveYearBarApplicabilityIndicator",
    "requesterCommentsForHub",
    "categoryCode"
})
@XmlRootElement(name="SubmitAgency3DHSResubRequest")
public class SubmitAgency3DHSResubRequestType {

    @XmlElement(name = "CaseNumber", required = true)
    protected String caseNumber;
    @XmlElement(name = "SEVISID", required = true)
    protected String sevisid;
    @XmlElement(name = "RequestedCoverageStartDate", required = true)
    protected XMLGregorianCalendar requestedCoverageStartDate;
    @XmlElement(name = "FiveYearBarApplicabilityIndicator")
    protected boolean fiveYearBarApplicabilityIndicator;
    @XmlElement(name = "RequesterCommentsForHub")
    protected String requesterCommentsForHub;
    @XmlElement(name = "CategoryCode")
    protected String categoryCode;

    /**
     * Gets the value of the caseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNumber() {
        return caseNumber;
    }

    /**
     * Sets the value of the caseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNumber(String value) {
        this.caseNumber = value;
    }

    /**
     * Gets the value of the sevisid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEVISID() {
        return sevisid;
    }

    /**
     * Sets the value of the sevisid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEVISID(String value) {
        this.sevisid = value;
    }

    /**
     * Gets the value of the requestedCoverageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedCoverageStartDate() {
        return requestedCoverageStartDate;
    }

    /**
     * Sets the value of the requestedCoverageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedCoverageStartDate(XMLGregorianCalendar value) {
        this.requestedCoverageStartDate = value;
    }

    /**
     * Gets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public boolean isFiveYearBarApplicabilityIndicator() {
        return fiveYearBarApplicabilityIndicator;
    }

    /**
     * Sets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public void setFiveYearBarApplicabilityIndicator(boolean value) {
        this.fiveYearBarApplicabilityIndicator = value;
    }

    /**
     * Gets the value of the requesterCommentsForHub property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequesterCommentsForHub() {
        return requesterCommentsForHub;
    }

    /**
     * Sets the value of the requesterCommentsForHub property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequesterCommentsForHub(String value) {
        this.requesterCommentsForHub = value;
    }

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryCode(String value) {
        this.categoryCode = value;
    }

}
