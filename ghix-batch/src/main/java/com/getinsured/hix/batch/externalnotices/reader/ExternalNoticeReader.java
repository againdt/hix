package com.getinsured.hix.batch.externalnotices.reader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import  com.getinsured.hix.platform.repository.IExternalNoticeRepository;
import com.getinsured.hix.platform.util.DateUtil;

@Component("externalNoticeReader")
@Scope("step")
public class ExternalNoticeReader implements ItemReader<ExternalNotice> , ItemStream{
	
	private static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticeReader.class);
	private String startDateStr;
	private String endDateStr;
	private BatchJobExecutionService batchJobExecutionService;
	private JdbcTemplate jdbcTemplate;
	private ResultSet resultSet;
	private Connection connection;
	private PreparedStatement statement;
	private Date startDate;
	private Date endDate;
	private SimpleJobOperator jobOperator;
	
	private static final String EXTERNAL_NOTICE_QUERY = "SELECT ID,SSAP_ID,NOTICE_ID,EXTERNAL_HOUSEHOLD_CASE_ID,STATUS,REQUEST_PAYLOAD,RESPONSE_PAYLOAD,"
			+ " CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP, RETRY_COUNT"
			+ " FROM NOTICES_EXTERNAL WHERE STATUS ='RESEND' OR (STATUS IN ('INIT','QUEUED') AND CREATION_TIMESTAMP >= ? ) OR  (STATUS ='FAILED' AND RETRY_COUNT <4 )";
	
	private IExternalNoticeRepository iExternalNoticesRepository;
	
	/**
	 * 
	 * @param stepExecution
	 * @throws Exception 
	 */
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) throws Exception{
		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob("externalNoticesBatchJob");
		if (batchExecutionList != null && batchExecutionList.size() > 1) {
			for (BatchJobExecution batchJobExecution : batchExecutionList) {
				jobOperator.stop(batchJobExecution.getId());
			}
		} else {
			LOGGER.info("Started Execution for externalNoticesBatchJob......");
			Calendar currentDate = Calendar.getInstance();
			int daysToConsider = (Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXTERNAL_NOTICE_DAYS_TO_CONSIDER)));
			currentDate.add(Calendar.DATE, -daysToConsider);
			this.startDate = DateUtil.removeTime(currentDate.getTime());
			this.endDate = new Date();
			startDate = (getStartDateStr() != null && DateUtil.isValidDate(getStartDateStr(), BATCH_DATE_FORMAT)) ? DateUtil.StringToDate(getStartDateStr(), BATCH_DATE_FORMAT)	: startDate;
			endDate = (getEndDateStr() != null && DateUtil.isValidDate(getEndDateStr(), BATCH_DATE_FORMAT)) ? DateUtil.StringToDate(getEndDateStr(), BATCH_DATE_FORMAT) : getLastPosibleTimeOfTheDay(startDate);
		}
	}

	@Override
	public ExternalNotice read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		ExternalNoticeMapper externalNoticeMapper = new ExternalNoticeMapper();
		ExternalNotice externalNotice = null;
		if(this.resultSet.next()){
			externalNotice = externalNoticeMapper.mapRow(this.resultSet,0);
		}
		return externalNotice;
	}
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		try {
			this.connection  =  jdbcTemplate.getDataSource().getConnection();
			this.connection.setAutoCommit(Boolean.FALSE);
			this.statement = connection.prepareStatement(EXTERNAL_NOTICE_QUERY);
			this.statement.setFetchSize(Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXTERNAL_NOTICE_COMMIT_INTERVAL)));
			this.statement.setTimestamp(1, new java.sql.Timestamp(this.startDate.getTime()));
			this.resultSet = this.statement.executeQuery();	
		} catch (SQLException e) {
			throw new ItemStreamException(e.getMessage(),e);
		}
	}
	
	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		// TODO Auto-generated method stub	
	}

	@Override
	public void close() throws ItemStreamException {
		if(this.statement != null){
			try{
				this.statement.close();
			}
			catch (SQLException e) {
				LOGGER.warn("ExternalNoticeReader :: Exception "+e.getMessage()+" encountered while closing the statement, Ignoring");
			}
		}
		if(this.connection != null){
			try 
			{
				this.connection.close();
				LOGGER.info("ExternalNoticeReader:: closing connection-------------->");
			} catch (SQLException e) {
				LOGGER.warn("ExternalNoticeReader :: Exception "+e.getMessage()+" encountered while closing the connection, Ignoring");
			}
		}
		LOGGER.info("Closing provider read stream");		
	}
	

	public IExternalNoticeRepository getiExternalNoticesRepository() {
		return iExternalNoticesRepository;
	}

	public void setiExternalNoticesRepository(IExternalNoticeRepository iExternalNoticesRepository) {
		this.iExternalNoticesRepository = iExternalNoticesRepository;
	}

	public SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}
	
	public String getStartDateStr() {
		return startDateStr;
	}
	
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private Date getLastPosibleTimeOfTheDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE,      calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND,      calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
}
