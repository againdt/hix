$(window).load(function() {
	$('.resize-img, .carrierlogo').each(function() {
 		$(this).css('width','auto','height','auto');
 		var maxWidth = 160; // Max width for the image
 		var maxHeight = 60; // Max height for the image
 		var ratio = 0; // Used for aspect ratio
 		var w = $(this).width();
 		var h = $(this).height();
 		// Check if the current width is larger than the max
	 		if(w > maxWidth){
		 		ratio = maxWidth / w; // get ratio for scaling image
		 		$(this).css("width", maxWidth); // Set new width
		 		$(this).css("height", h * ratio); // Scale height based on ratio
		 		h = h * ratio; // Reset height to match scaled image
		 		w = w * ratio; // Reset width to match scaled image
	 		}
	 		// Check if current height is larger than max
	 		if(h > maxHeight){
		 		ratio = maxHeight / h; // get ratio for scaling image
		 		$(this).css("height", maxHeight); // Set new height
		 		$(this).css("width", w * ratio); // Scale width based on ratio
		 		w = w * ratio; // Reset width to match scaled image
	 		}
	 		if (w < maxWidth && h < maxHeight) {
	 			$(this).css('width',w,'height',h);
	 		}
	 		$(this).css('display','inline-block');
	    });
 	/*$('.carrierlogo').each(function() {
 		//$(this).css('width','auto','height','auto');
 		var maxWidth = 160; // Max width for the image
 		var maxHeight = 60; // Max height for the image
 		var ratio = 0; // Used for aspect ratio
 		var width = $(this).width();
 		var height = $(this).height();
 		// Check if the current width is larger than the max
	 		if(width > maxWidth){
		 		ratio = maxWidth / width; // get ratio for scaling image
		 		$(this).css("width", maxWidth); // Set new width
		 		$(this).css("height", height * ratio); // Scale height based on ratio
		 		height = height * ratio; // Reset height to match scaled image
		 		width = width * ratio; // Reset width to match scaled image
	 		}
	 		// Check if current height is larger than max
	 		if(height > maxHeight){
		 		ratio = maxHeight / height; // get ratio for scaling image
		 		$(this).css("height", maxHeight); // Set new height
		 		$(this).css("width", width * ratio); // Scale width based on ratio
		 		width = width * ratio; // Reset width to match scaled image
	 		} 
	    });*/	
});