<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "session_start_ts <= systimestamp and input_page = 'M'";
		
	}
%>

<sql:query dataSource="jdbc/ghixDS" var="result1">
select results_type, count(results_type) as total
from prescreen_data
where <%=whereClause%>
group by results_type
order by results_type
</sql:query> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>PreScreen - Plans Displayed</title>
  

  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});

      function drawVisualization() {
              // Create and populate the data table.
              var data = google.visualization.arrayToDataTable([
                ['CSR', 'Total'],
		<c:forEach var="row" items="${result1.rows}">
		['<c:out value="${row.results_type}"/>',<c:out value="${row.total}"/>],
		</c:forEach>
              ]);
            
              // Create and draw the visualization.
              new google.visualization.PieChart(document.getElementById('visualization')).
                  draw(data, {title:"Plans Displayed", pieSliceText:"percentage"});
      }
    

    google.setOnLoadCallback(drawVisualization);
  </script>
<script type="text/javascript">
	
$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
});

$(function() {
	$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
});

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	
	//Step 1: Get the Dates
	var date1 = document.getElementById('date1').value;
	var date2 = document.getElementById('date2').value;

	if (date1 && date2) {

		if (whereClause) {
			whereClause = whereClause
					+ "and session_start_ts between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
			

		} else {
			whereClause = whereClause
					+ " session_start_ts between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
		}
		
		filterStatement = filterStatement + ' Start Date : ' + date1
		+ ' End date : ' + date2;
		
	} else {
		if (date1 && !date2) {
			if (whereClause) {

				whereClause = whereClause
					+ " and session_start_ts between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				
			} else {
				whereClause = whereClause
						+ " session_start_ts between to_timestamp('"
						+ date1
						+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				+ date1 + "'";
				
			}
			
			filterStatement = filterStatement + ' Start Date : ' + date1;
			
		} else {

			if (!date1 && date2) {
				if (whereClause) {
					whereClause = whereClause
							+ " and session_start_ts <= to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				} else {
					whereClause = whereClause
							+ " session_start_ts <=  to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				}
				
				filterStatement = filterStatement + ' End Date : ' + date2;
			}
			
			
		}

	}
	if (!whereClause) {
		whereClause = "session_start_ts <= systimestamp";
	}
	
	//Step 2: Get the Version Value
	var statuses = document.getElementById('version');
	var status = statuses.options[statuses.selectedIndex].value;
	if (status == 'P' || status == 'M') {
		if (whereClause) {
			whereClause = whereClause + " and input_page = '" + status
					+ "'";
		} else {
			whereClause = whereClause + " input_page = '" + status
					+ "'";
		}

	}
	
	if (status == 'P') {

		filterStatement = filterStatement
				+ ' Version Accessed :  Product';
	}
	if (status == 'M') {

		filterStatement = filterStatement
				+ ' Version Accessed :  Marketing';
	}
	
	//Step 3: Get the Source value
	var statuses = document.getElementById('source');
	var status = statuses.options[statuses.selectedIndex].value;
	if (status == 'phix' || status == 'jh') {
		if (whereClause) {
			whereClause = whereClause + " and source = '" + status
					+ "'";
		} else {
			whereClause = whereClause + " source = '" + status
					+ "'";
		}

	}
	
	if (status == 'phix') {

		filterStatement = filterStatement
				+ ' Source :  GetInsured Private Exchange';
	}
	if (status == 'jh') {

		filterStatement = filterStatement
				+ ' Source :  Jackson Hewitt';
	}
	
	document.getElementById('whereClause').value = whereClause;
	document.getElementById('filterStatement').value = filterStatement;
}

</script>
</head>
<body style="font-family: Arial;border: 0 none;">
<h4>Plans Displayed - Benchmark plan displayed to applicants</h4>
<form method="post" action="pie_plans_displayed">
	<table border="1" style="border-color:#C0C0C0">
		<tbody>
		<tr><td>
				Date Start:	<input type="text" id="date1" name="date1" size="10" maxlength="10"/> 
				End: <input type="text" id="date2" name="date2" size="10" maxlength="10"/>
			</td>
			<td>
				Version Accessed:
				<select size="1" id="version" name="status">
					<option value="both">Both</option>
					<option value="P">Product</option>
					<option value="M" selected="selected">Marketing</option>
				</select>
				Source:
				<select size="1" id="source" name="source">
					<option value="">Other</option>
					<option value="phix" selected="selected">GI PHIX</option>
					<option value="jh">Jackson Hewitt</option>
				</select>
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause();"/>
				<input id="whereClause" name="whereClause" type="hidden"/>
				<input id="filterStatement" name="filterStatement" type="hidden"/>
			</td>
		</tr>
		</tbody>
	</table>
</form>
<br>
<%
	if (request.getParameter("filterStatement") != null) {
		out.println("Search Criteria : " + request.getParameter("filterStatement"));
	}
%>
<table>
<tr>
<td><div id="visualization" style="width: 600px; height: 600px;"></div></td>
<td><br><br><br><br>
<b>Plans Displayed</b>
<br>
<display:table name="${result1.rows}"  requestURI="showDataTop" id="table1" export="true" >
	<display:column property="results_type" title="Plan Displayed" sortable="false" />
	<display:column property="total" title="Total" sortable="false" />
</display:table>
</td>
</tr>
</table>
</body>
</html>

