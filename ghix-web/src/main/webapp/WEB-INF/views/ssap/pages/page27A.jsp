<%@ include file="../include.jsp" %>

<div id="appscr77Part1" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page27A.employerHealthCoverageInformationFor" text="Employer Health Coverage Information for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr77Part1Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="alert alert-info">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27A.needHelpForAnswering" text="Need help answering these questions?" />
					<spring:message code="ssap.page27A.clickTheButtonToDownloadHelpForm" text="Click the button to download a form that can help you collect the information from your employer." />
				</p>
				<a href="resources/employer-coverage-tool.pdf" class="btn btn-primary pull-right"  target="_blank">
					<spring:message code="ssap.page27A.ESIForm" text="ESI Form" />
				</a>
			</div><!-- control-group ends -->
			</div>
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27A.is" text="Is" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27A.currentlyEnrolledInAHealthPlanOfferedBy" text="currently enrolled in a health plan offered by " />
					<strong class="EmployerNameHere"></strong>
					<spring:message code="ssap.page27A." text="?" />
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" id="77P1CurrentlyEnrolledInHealthPlanYes" name=77P1CurrentlyEnrolledInHealthPlan value="yes" required parsley-error-message="<spring:message code='ssap.required.option' text='Please Select a Option.' />"/>
						<spring:message code="ssap.page27A.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" id="77P1CurrentlyEnrolledInHealthPlanNo" name="77P1CurrentlyEnrolledInHealthPlan" value="no" />
						<spring:message code="ssap.page27A.no" text="No" />
					</label>
					<span class="errorMessage"></span>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27A.will" text="Will" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27A.beEnrolledInAHealthPlanOfferedBy" text="be enrolled in a health plan offered by" />
					<strong class="EmployerNameHere"></strong>
					<spring:message code="ssap.page27A.in" text="in" />
					<span class='coverageYear'></span>
					<spring:message code="ssap.page27A.QM" text="?" />
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" id="77P1WillBeEnrolledInHealthPlanYes" name="77P1WillBeEnrolledInHealthPlan" value="yes" required parsley-error-message="<spring:message code='ssap.required.option' text='Please Select a Option.' />" />
						<spring:message code="ssap.page27A.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" id="77P1WillBeEnrolledInHealthPlanNo" name="77P1WillBeEnrolledInHealthPlan" value="no" />
						<spring:message code="ssap.page27A.no" text="No" />
					</label>
					<div class="margin20-l hide" id="DateToCovered">
						<p>
							<spring:message code="ssap.page27A.date" text="Date" />
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page27A.willBeCoveredBy" text="will be covered by" />
							<strong class="EmployerNameHere"></strong>
							<spring:message code="ssap.page27A.plan" text="&apos;s plan:" />
						</p><!-- end of label -->
						<div class="margin5-l controls">
							<div class="input-append date date-picker"  data-date="">
								<input type="text" id="77P1WillBeEnrolledInHealthPlanDate" name="77P1WillBeEnrolledInHealthPlanDate" class="input-medium dob-input" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />"  data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
								<span  class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<span class="errorMessage"></span>
							<%-- <label class="checkbox clearfix">
								<input type="checkbox" name="naturalizedCitizen" value="0"><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
							</label> --%>
						</div>
					</div><!-- margin20-l ends -->
					<span class="errorMessage"></span>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			
			<div class="appscr77InfoDivHideAndShow">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.whatTheLastDay" text="What&apos;s the last day" />
						<strong class="EmployerNameHere"></strong>
						<spring:message code="ssap.page27A.coverageWillBeAvailableTo" text="coverage will be available to" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page27A." text="?" />
					</p><!-- end of label -->
					<div class="margin5-l controls">
						<div class="input-append date date-picker"  data-date="">
							<input type="text" id="77P1NoLongerHealthCoverageDate" name="appscr77P1Date2" class="input-medium dob-input" data-parsley-americandate="true" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#77P1NoLongerHealthCoverageDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span  class="add-on"><i class="icon-calendar"></i></span>
						</div>
						<div id="77P1NoLongerHealthCoverageDateErrorDiv"></div>
						<span class="errorMessage"></span>
						<%-- <label>
							<input type="checkbox" name="naturalizedCitizen" value="0"><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
						</label> --%>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div><!-- appscr77InfoDivHideAndShow ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27A.does" text="Does" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27A.expectAnyChangesTo" text=" expect any changes to" />
					<strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthCoverageIn" text="&apos;s health coverage in" />
					<strong class="coverageYear"></strong>
					<spring:message code="ssap.page27A.QM" text="?" />
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" id="77P1ExpectChangesToHealthCoverageYes" name="77P1ExpectChangesToHealthCoverage" value="yes" data-parsley-errors-container="#77P1ExpectChangesToHealthCoverageErrorDiv"/>
						<spring:message code="ssap.page27A.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" id="77P1ExpectChangesToHealthCoverageNo"  name="77P1ExpectChangesToHealthCoverage" value="no" />
						<spring:message code="ssap.page27A.no" text="No" />
					</label>
					<div id="77P1ExpectChangesToHealthCoverageErrorDiv"></div>
					<span class="errorMessage"></span>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="doesExceptAnyChangeToHealthCoverageInappscr77Part1 hide">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.whatDoes" text="What does" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page27A.expectToChangeIn" text=" expect to change in" />
						<strong class="coverageYear"></strong>
						<spring:message code="ssap.page27A.QM" text="?" />
					</p><!-- end of label -->
					<div class="margin5-l">
						<label>
							<input type="checkbox" id="77P1WillNoLongerHealthCoverage" name="77P1WillNoLongerHealthCoverage" value="0">
							<span class="EmployerNameHere"></span>
							<spring:message code="ssap.page27A.willNoLongerOfferHealthCoverage" text="will no longer offer health coverage" />
						</label>
						<label>
							<input type="checkbox" id="77P1PlanToDropHealthCoverage" name="77P1PlanToDropHealthCoverage" value="0">
							<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page27A.plansToDrop" text="plans to drop" />
							<strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthCoverage" text="&apos;s health coverage" />
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.whatWillBe" text="What will be" />
						<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page27A.lastDay" text="&apos;s last day of coverage through" />
						<strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthPlan" text="&apos;s health plan?" />
					</p><!-- end of label -->
					<div class="margin5-l controls">
						<div data-date=""  class="input-append date date-picker">
							<input type="text" class="input-medium" name="appscr77P1Date3" id="77P1PlanToDropHealthCoverageDate" autocomplete="off"  data-parsley-errors-container="#77P1PlanToDropHealthCoverageDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span class="add-on dateSpan"><i class="icon-calendar"></i></span>
						</div>
						<div id ="77P1PlanToDropHealthCoverageDateErrorDiv"></div>
						<span class="errorMessage"></span>
						<label class="clearfix">
							<input type="checkbox" name="naturalizedCitizen" value="0" /><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
						</label>
						<label>
							<input type="checkbox" id="77P1WillOfferCoverage" name="77P1WillOfferCoverage" value="0" />
							<strong class="EmployerNameHere"></strong>
							<spring:message code="ssap.page27A.willOffer" text=" will offer" />
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page27A.coverageIn" text="coverage in" />
							<strong class="coverageYear"></strong>
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.WhatTheFirstDate" text="What&apos;s the first date" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page27A.wouldBeCoveredBy" text=" would be covered by" />
						<strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthPlan" text="&apos;s health plan?" />
					</p><!-- end of label -->
					<div class="margin5-l controls">
						<div data-date=""  class="input-append date date-picker">
							<input type="text" class="input-medium" name="appscr77P1Date4" id="77P1WillOfferCoverageDate" autocomplete="off" parsley-americandate="true" data-parsley-errors-container="#77P1WillOfferCoverageDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span class="add-on dateSpan"><i class="icon-calendar"></i></span>
						</div>
						<div id='77P1WillOfferCoverageDateErrorDiv'></div>
						<span class="errorMessage"></span>
						<label class="clearfix">
							<input type="checkbox" name="naturalizedCitizen" value="0"><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
						</label>
						<label>
							<input type="checkbox" id="77P1PlaningToEnrollInHC" name="77P1PlaningToEnrollInHC" value="0" />
							<strong class="nameOfHouseHold"></strong>
							<spring:message code="ssap.page27A.isPlanningToEnrollIn" text="is planning to enroll in" />
							<strong class="EmployerNameHere"></strong>
							<spring:message code="ssap.page27A.healthCoverageIn" text="health coverage in" />
							<strong class="coverageYear"></strong>
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.whatTheFirstDay" text="What&apos;s the first day" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page27A.willBeCoveredBy" text="will be covered by" /><strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthPlan" text="&apos;s health plan?" />
					</p><!-- end of label -->
					<div class="margin5-l controls">
						<div data-date=""  class="input-append date date-picker">
							<input type="text" class="input-medium" name="appscr77P1Date5" id="77P1PlaningToEnrollInHCDate" autocomplete="off" data-parsley-americandate="true"  data-parsley-errors-container ="#77P1PlaningToEnrollInHCDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span class="add-on dateSpan"><i class="icon-calendar"></i></span>
						</div>
						<div id ="77P1PlaningToEnrollInHCDateErrorDiv"></div>
						<span class="errorMessage"></span>
						
						<label class="clearfix">
							<input type="checkbox" name="naturalizedCitizen" value="0" /><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
						</label>
						<label>
							<input type="checkbox" id="77P1HealthPlanOptionsGoingToChange" name="77P1HealthPlanOptionsGoingToChange" value="0" />
							<strong class="EmployerNameHere"></strong><spring:message code="ssap.page27A.healthPlanOptionsAreGoingToChangeIn" text="&apos;s health plan options are going to change in" />
							<strong class="coverageYear"></strong>
							<spring:message code="ssap.page27A.comma" text="," />
							<spring:message code="ssap.page27A.includingChangesInTheCostAndMinimumValue" text="including changes in the cost and minimum value" />
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page27A.WhenWillTheChangeHappen" text="When will the change happen?" />
					</p><!-- end of label -->
					<div class="margin5-l controls">
						<div data-date=""  class="input-append date date-picker">
							<input type="text" class="input-medium" name="appscr77P1Date6" id="77P1HealthPlanOptionsGoingToChangeDate" autocomplete="off" parsley-americandate="true" data-parsley-errors-container ="#77P1HealthPlanOptionsGoingToChangeDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span class="add-on dateSpan"><i class="icon-calendar"></i></span>
						</div>
						<div id ="77P1HealthPlanOptionsGoingToChangeDateErrorDiv"></div>
						<span class="errorMessage"></span>
						
						<label class="clearfix">
							<input type="checkbox" name="naturalizedCitizen" value="0" /><spring:message code="ssap.page27A.dontKnow" text="I don&apos;t Know" />
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div><!--doesExceptAnyChangeToHealthCoverageInappscr77Part1 ends  -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.whatTheNameOfTheLowestCostSelfOnlyHealthPlanOfferedTo" text="What&#39;s the name of the lowest cost self-only health plan offered to" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.QM" text="?" />
					<small>
						<spring:message code="ssap.page27B.onlyIncludePlans" text="(Only include plans that meet the &#39;minimum value standard&#39; set by the Affordable Care Act.)" />
					</small>
					<a href="#"><spring:message code="ssap.page27B.employerCoverageForm" text="Employer Coverage Form" /></a>
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<div for="currentLowestCostSelfOnlyPlanName">
						<input placeHolder="<spring:message code="ssap.page27B.healthPlanName" text="Health Plan Name"/> " class="input-medium" type="text" id="currentLowestCostSelfOnlyPlanName" name="appscr77p2HealthPlanName"/>
						<span class="errorMessage"></span>
					</div>
					
					<label class="clearfix">
						<input type="checkbox" name="appscr77p1dontKnow" value="0"><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know" />
					</label>
					
					<label>
						<input type="checkbox" id="currentPlanMeetsMinimumStandard" name="currentPlanMeetsMinimumStandard" value="0"  />
						<span><spring:message code="ssap.page27B.noPlansMeetTheMinimumValuStandard" text="No plans meet the minimum value standard" /></span>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.whatTheNameOfTheLowestCostSelfonlyHealthPlanThatWillBeOfferedTo" text="What&#39;s the name of the lowest cost self-only health plan that will be offered to" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.in" text="in" /> <span class='coverageYear'></span> <spring:message code="ssap.page27B.QM" text="?" />
					<small>
						<spring:message code="ssap.page27B.onlyIncludePlans" text="(Only include plans that meet the &#39;minimum value standard&#39; set by the Affordable Care Act.)" />
					</small>
					<a href="#"><spring:message code="ssap.page27B.employerCoverageForm" text="Employer Coverage Form" /></a>
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<div for="comingLowestCostSelfOnlyPlanName">
						<input placeHolder="<spring:message code="ssap.page27B.healthPlanName" text="Health Plan Name"/> " class="input-medium" type="text" id="comingLowestCostSelfOnlyPlanName" name="appscr77p2HealthPlanNameCoverageYear" />
						<span class="errorMessage"></span>
					</div>
					
					<label class="clearfix">
						<input type="checkbox" name="appscr77p2CoverageForm" value="0"><spring:message code="ssap.page27B.dontKnow" text="I	don&#39;t know" />
					</label>
					
					<label>
						<input type="checkbox" id="comingPlanMeetsMinimumStandard" name="appscr77p2CoverageForm" value="0" />
						<spring:message code="ssap.page27B.noPlansMeetTheMinimumValuStandard" text=" No plans meet the minimum value standard" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.howMuchWould" text="How much would" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.payInPremiumsToEnrollInThisPlan" text=" pay in premiums to enroll in this plan?" />
				</p>
				<!-- end of label -->
				 <div class="input-prepend margin10-b">
  					<span class="add-on">$</span>
  					<input placeHolder="<spring:message code="ssap.page27B.dollerAmount" text=" Dollar Amount" />" type="text" id="lowestCostSelfOnlyPremiumAmount" name="appscr77p2Payment" class="input-medium currency"/>
				</div>
				<span class="errorMessage"></span>
				<label class="clearfix">
					<input type="checkbox" name="appscr77p2PaymentDontKnow" value="0" /><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know" />
				</label>
				
			</div><!-- control-group ends -->
			
			
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page21.howOftenDoes" text="How often does"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.payInPremiumsToEnrollInThisPlan" text="pay in premiums to enroll in this plan?"/>
				</p>
				<div class="margin5-l controls">
					<select class="input-large" id="premiumPlansStatus" name="premiumPlansStatus"
					aria-label="Frequency options">
						<option value="">Select Frequency</option>
						<option value="Hourly">Hourly </option>
						<option value="Daily">Daily </option>
						<option value="Weekly">Weekly </option>
						<option value="EveryTwoWeeks">Every 2 week </option>
						<option value="Monthly">Monthly  </option>
						<option value="Yearly">Yearly  </option>
						<option value="OneTimeOnly">One time only  </option>
					</select>
					<span class="errorMessage"></span>
					<label class="clearfix">
						<input type="checkbox" name="appscr77p2PaymentDontKnow" value="0" /><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know" />
					</label>
				</div>
			</div>
				
		<%-- 	<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.howMuchWould" text="How much would" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.payInPremiumsToEnrollInThisPlan" text="pay in premiums to enroll in this plan?"/>
				</p>
				<label class="control-label">
					<spring:message code="ssap.page27B.amount" text="Amount:" />
					<strong>$</strong>
				</label><!-- end of label -->
				<div class="controls">
					<input placeHolder="<spring:message code="ssap.page27B.fr" text="Frequency:"/>" type="text" id="lowestCostSelfOnlyPremiumFrequency" name="appscr77p2PremiumsPayment" class="input-medium" />
					<label>
						<input type="checkbox" name="naturalizedCitizen" value="0" /><spring:message code="ssap.page27B.dontKnow" text="I don&#39;t know"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends --> --%>
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page27B.does" text="Does" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page27B.thinkThisCoverageIsAffordable" text="think this coverage is affordable?" />
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<label>
						<input type="radio" name="appscr77p2IsAffordable" value="1"/>
						<spring:message code="ssap.page27B.yes" text="Yes"  />
					</label>
					
					<label>
						<input type="radio" name="appscr77p2IsAffordable" value="0" />
						<spring:message code="ssap.page27B.no" text=" No" />
					</label>
					<span class="errorMessage"></span>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
		</form>
	</div>
</div>