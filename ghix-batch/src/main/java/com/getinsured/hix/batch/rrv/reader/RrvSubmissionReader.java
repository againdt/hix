package com.getinsured.hix.batch.rrv.reader;


import com.getinsured.eligibility.model.RrvSubmission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class RrvSubmissionReader implements ItemReader<RrvSubmission>, ItemStream {

    private static final Logger logger = LoggerFactory.getLogger(RrvSubmissionReader.class);

    private static final int FETCH_SIZE = 500;

    @Autowired
    private DataSource batch_dataSource;

    private ResultSet resultSet;
    private Connection con;
    private PreparedStatement preparedStatement;

    private long coverageYear;

    private static final String QUERY =
            "SELECT id, coverage_year, rrv_batch_id, household_id, ifsv_status, ssac_status, eqfx_status, smec_status, creation_date, last_update_date " +
                    "FROM RRV_SUBMISSION rs " +
                    "WHERE rs.coverage_year = ? " +
                    "          AND ( rs.ifsv_status = 'PENDING' " +
                    "          OR rs.ssac_status = 'PENDING' " +
                    "          OR rs.eqfx_status = 'PENDING' " +
                    "          OR rs.smec_status = 'PENDING' ) ";

    @Override
    public RrvSubmission read() {
        logger.info("Reading record from query");
        try{
            if(this.resultSet.next()){
                RrvSubmission rrvSubmission = new RrvSubmission();
                rrvSubmission.setId(resultSet.getLong("id"));
                rrvSubmission.setCoverageYear(resultSet.getLong("coverage_year"));
                rrvSubmission.setRrvBatchId(resultSet.getLong("rrv_batch_id"));
                rrvSubmission.setHouseholdId(resultSet.getLong("household_id"));
                rrvSubmission.setIfsvStatus(resultSet.getString("ifsv_status"));
                rrvSubmission.setSsacStatus(resultSet.getString("ssac_status"));
                rrvSubmission.setEqfxStatus(resultSet.getString("eqfx_status"));
                rrvSubmission.setSmecStatus(resultSet.getString("smec_status"));
                rrvSubmission.setCreationDate(resultSet.getDate("creation_date"));
                rrvSubmission.setLastUpdateDate(resultSet.getDate("last_update_date"));

                return rrvSubmission;
            }
        }
        catch(SQLException e){
            logger.error("Failed to get records from DB",e);
        }
        return null;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        try {
            this.con = this.batch_dataSource.getConnection();
            this.preparedStatement = con.prepareStatement(QUERY);
            this.preparedStatement.setFetchSize(FETCH_SIZE);
            this.preparedStatement.setLong(1, coverageYear);
            this.resultSet = this.preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new ItemStreamException(e.getMessage(),e);
        }
        logger.info("Opened Item stream for reading HUB responses");
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        logger.info("Closing DB connection");
        if(this.con != null){
            try {
                this.preparedStatement.close();
                this.con.close();
            } catch (SQLException e) {
                logger.error("Failed to close DB connection, Ignoring",e);
            }
        }
    }

    public DataSource getBatch_dataSource() {
        return batch_dataSource;
    }

    public void setBatch_dataSource(DataSource batch_dataSource) {
        this.batch_dataSource = batch_dataSource;
    }

    public long getCoverageYear() {
        return coverageYear;
    }

    public void setCoverageYear(long coverageYear) {
        this.coverageYear = coverageYear;
    }
}
