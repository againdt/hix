var app = angular.module('myApp', []);

app.controller("validateCtrl", function($scope) {
	// Declaring members for validations
	$scope.invalidCarrier = true;
	$scope.invalidFileList = true;
	angular.element('#fileUploadPlans').prop('disabled', true);

	// Declaring Suffix constants for SERFF Templates
	$scope.NETWORK_SUFFIX = null;
	$scope.BENEFITS_SUFFIX = null;
	$scope.RATE_SUFFIX = null;
	$scope.SERVICE_AREA_SUFFIX = null;
	$scope.DRUG_SUFFIX = null;
	$scope.BUSINESS_RULE_SUFFIX = null;
	$scope.UNIFIED_RATE_SUFFIX = null;

	// Declaring constants
	$scope.PLAN_INITIAL_INDEX = 0;
	$scope.LEN_HIOS_PLAN_ID_WITH_VARIANT = 17;
	$scope.LEN_HIOS_PLAN_ID = 14;
	$scope.UNDERSCORE = "_";
	$scope.PDF_EXTENSION = ".PDF";
	$scope.DATA_SBC = "SBC";
	$scope.DATA_EOC = "EOC";
	$scope.DATA_BROCHURE = "BROCHURE";

	// Declaring regular expression for HIOS Plan ID
	$scope.PATTERN_HIOS_PLAN_NUM_WITH_VARIANT = new RegExp("(^[A-Za-z0-9]{5})([A-Za-z]{2})([0-9]{7})([\\\\-]{1})([0]{1}[0-6]{1})+$");
	$scope.PATTERN_HIOS_PLAN_NUM = new RegExp("(^[A-Za-z0-9]{5})([A-Za-z]{2})([0-9]{7})$");

	// Declaring Methods
	$scope.resetForm = resetForm;
	$scope.resetSelectedFiles = resetSelectedFiles;
	$scope.hasValidPattern = hasValidPattern;
	$scope.validateSupportDocument = validateSupportDocument;
	$scope.resetFileNode = resetFileNode;

	function resetForm() {
		// debugger;
		$scope.invalidCarrier = true;
		$scope.invalidFileList = true;
		$scope.frmUploadPlans.$dirty = false;
		$scope.frmUploadPlans.$pristine = true;
		$scope.frmUploadPlans.$submitted = false;
		angular.element('#fileUploadPlans').prop('disabled', true);
	}

	function resetFileNode(disabledNode) {
		// debugger;
		angular.element("#fileUploadPlans").prop('disabled', disabledNode);
		angular.element("#fileUploadPlans").val(null);
		$scope.invalidFileList = true;
		$scope.frmUploadPlans.fileUploadPlans.$dirty = false;
	}

	function resetSelectedFiles() {
		// debugger;
		$scope.invalidFileList = true;
		$scope.frmUploadPlans.fileUploadPlans.$dirty = false;
		angular.element("#fileUploadPlans").val(null);
	}

	function hasValidPattern(value, pattern) {
		var validFlag = false;

		if (value && pattern && pattern.test(value)) {
			validFlag = true;
		}
		return validFlag;
	}

	function validateSupportDocument(originalDocumentName, hiosIssuerIdWithState) {
		// debugger;

		var hasValidDocument = false;
		var hasDocumentWithPlanId = false;
		var issuerPlanNumber = null;

		if ($scope.LEN_HIOS_PLAN_ID_WITH_VARIANT < originalDocumentName.length) {
			issuerPlanNumber = originalDocumentName.substring($scope.PLAN_INITIAL_INDEX, $scope.LEN_HIOS_PLAN_ID_WITH_VARIANT);

			// Validate Support Document Name with HIOS Plan ID with variant.
			if (hasValidPattern(issuerPlanNumber, $scope.PATTERN_HIOS_PLAN_NUM_WITH_VARIANT)) {
				hasDocumentWithPlanId = true;
			}
			else {
				issuerPlanNumber = originalDocumentName.substring($scope.PLAN_INITIAL_INDEX, $scope.LEN_HIOS_PLAN_ID);

				// Validate Support Document Name with HIOS Plan ID without variant.
				if (hasValidPattern(issuerPlanNumber, $scope.PATTERN_HIOS_PLAN_NUM)) {
					hasDocumentWithPlanId = true;
				}
			}

			// Validate Support Document Name start with Selected Issuer ID and State Code
			if (hasDocumentWithPlanId && !issuerPlanNumber.startsWith(hiosIssuerIdWithState)) {
				hasDocumentWithPlanId = false;
			}

			// Support Document is start with HIOS Plan ID
			if (hasDocumentWithPlanId) {
				var documentNameToUpperCase = originalDocumentName.toUpperCase();
				var sbcFile = issuerPlanNumber + $scope.UNDERSCORE + $scope.DATA_SBC + $scope.PDF_EXTENSION;
				var eocFile = issuerPlanNumber + $scope.UNDERSCORE + $scope.DATA_EOC + $scope.PDF_EXTENSION;
				var brochureFile = issuerPlanNumber + $scope.UNDERSCORE + $scope.DATA_BROCHURE + $scope.PDF_EXTENSION;

				// SBC document check
				if (documentNameToUpperCase == sbcFile
					// EOC document check
					|| documentNameToUpperCase  == eocFile
					// BROCHURE document check
					|| documentNameToUpperCase == brochureFile) {
					hasValidDocument = true;
				}
			}
		}
		return hasValidDocument;
	}
});

var INTEGER_REGEXP = /^-?\d+$/;
app.directive('carrierValidation', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, mCtrl) {
			function carrierValidation(value) {
				var hasValidValue = false;
				scope.invalidCarrier = true;
				// debugger;
				if (INTEGER_REGEXP.test(value)) {
					hasValidValue = true;
					scope.invalidCarrier = false;
					scope.resetFileNode(false);
				}
				else if (!value) {
					hasValidValue = true;
					scope.resetFileNode(true);
				}
				mCtrl.$setValidity('carrierValidation', hasValidValue);
				return value;
			}
			mCtrl.$parsers.push(carrierValidation);
		}
	};
});

app.directive('filesValidation', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, mCtrl) {

			element.bind('change', function() {
				var laFields = new Array();
				var li = 0;
				mCtrl.$dirty = true;
				$("#fileValid").html("");
				scope.invalidFileList = true;

				var pndCheckValue = false;
				if (scope.exchangeState && (scope.exchangeState == 'WA' || scope.exchangeState == 'CT') && scope.pndCheckbox) {
					pndCheckValue = scope.pndCheckbox.selected;
				}
				var fileList = "";
				// debugger;

				if (pndCheckValue) {

					if (this.files.length < 2 || this.files.length > 2) {
						laFields[li++] = "Please attach only 2 mandatory Template XMLs.";
					}
					else {
						var fileName = null;
						var cntMandatory = 0;
						var errorMsg = "Mandatory templates not attached. ";

						for (var fi = 0; fi < this.files.length; fi++) {

							if (this.files[fi].type != "text/xml") {
								laFields[li++] = "Attachment file must have XML format.";
								break;
							}
							fileName = this.files[fi].name.toLowerCase()

							if (fileName.lastIndexOf(scope.BENEFITS_SUFFIX) > -1) {
								cntMandatory += 1;
								continue;
							}
							else if (fileName.lastIndexOf(scope.DRUG_SUFFIX) > -1) {
								cntMandatory += 10;
								continue;
							}
							else {
								fileList += fileName + ", ";
							}
						}

						if (cntMandatory != 11) {
							laFields[li++] = errorMsg;
						}
					}
				}
				else {

					if (this.files.length < 4) {
						laFields[li++] = "Mandatory templates not attached.";
					}
					else if ("MN" != scope.exchangeState && this.files.length > 7) {
						laFields[li++] = "Template names not correct.";
					}
					else {

						var fileName = null;
						var originalFileName = null;
						var cntMandatory = 0;
						var errorMsg = "Mandatory templates not attached. ";
						var hiosIssuerIdWithState = scope.carrier + scope.exchangeState;

						var allowedFileTypes = null;
						var errorMsgForAllowedFiles = null;
						if ("MN" == scope.exchangeState) {
							allowedFileTypes = ['text/xml', 'application/pdf'];
							errorMsgForAllowedFiles = "Attachment file must have XML and PDF(optional) formats.";
						}
						else {
							allowedFileTypes = ['text/xml'];
							errorMsgForAllowedFiles = "Attachment file must have XML format.";
						}

						for (var fi = 0; fi < this.files.length; fi++) {

							if (!allowedFileTypes.includes(this.files[fi].type)) {
								laFields[li++] = errorMsgForAllowedFiles;
								break;
							}
							originalFileName = this.files[fi].name;
							fileName = originalFileName.toLowerCase();

							if (fileName.lastIndexOf(scope.NETWORK_SUFFIX) > -1) {
								cntMandatory += 1;
								continue;
							}
							else if (fileName.lastIndexOf(scope.BENEFITS_SUFFIX) > -1) {
								cntMandatory += 10;
								continue;
							}
							else if (fileName.lastIndexOf(scope.RATE_SUFFIX) > -1) {
								cntMandatory += 100;
								continue;
							}
							else if (fileName.lastIndexOf(scope.SERVICE_AREA_SUFFIX) > -1) {
								cntMandatory += 1000;
								continue;
							}
							else if (fileName.lastIndexOf(scope.DRUG_SUFFIX) > -1) {
								continue;
							}
							else if (fileName.lastIndexOf(scope.BUSINESS_RULE_SUFFIX) > -1) {
								continue;
							}
							else if (fileName.lastIndexOf(scope.UNIFIED_RATE_SUFFIX) > -1) {
								continue;
							}
							else {
								// Validate Support Document for MN.
								if ("MN" == scope.exchangeState) {

									if (this.files[fi].type != "application/pdf" || !scope.validateSupportDocument(originalFileName, hiosIssuerIdWithState)) {
										fileList += originalFileName + ", ";
									}
								}
								else {
									fileList += originalFileName + ", ";
								}
							}
						}

						if (cntMandatory != 1111) {
							laFields[li++] = errorMsg;
						}
					}
				}

				if (fileList) {
					laFields[li++] = "Template names not correct ("+ fileList.substring(0, fileList.length - 2) +").";
				}

				var hasValidValue = true;
				if (laFields.length > 0) {

					if (laFields.length > 1) {

						for (var fi = 0; fi < laFields.length; fi++) {
							laFields[fi] = "- " + laFields[fi];
						}
					}
					// alert(laFields.join('\n'));
					$("#fileValid").html(laFields.join('<br>'));
					hasValidValue = false;
				}
				scope.invalidFileList = !hasValidValue;
				mCtrl.$valid = hasValidValue;
				mCtrl.$setValidity('filesValidation', hasValidValue);

				scope.$apply(function () {
					mCtrl.$render();
				});
			});
		}
	};
});
