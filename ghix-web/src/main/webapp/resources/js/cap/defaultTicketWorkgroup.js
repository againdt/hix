var App=angular.module("defaultTicketWorkgroupApp",[]);
App.controller("defaultTicketWorkgroupController",["$scope","$http",function($scope,$http){
	$scope.queueList=angular.copy(queueList);
	$scope.result=false;
	$scope.onBtnReset=function()
	{
		$scope.queueList=angular.copy(queueList);
		$scope.result=false;
	};
	$scope.onBtnSubmit=function()
	{
		var defaultIds="";
		angular.forEach($scope.queueList,function(workgroup){
			if(workgroup.isDefault=="Y")
			{
				if(defaultIds=="")
				{
					defaultIds=workgroup.id;
				}else{
					defaultIds +="|"+workgroup.id;
				}
			}
		});
		$("#frmDefaultTicketWorkgroup #btnSubmit").button("loading");
		$("#frmDefaultTicketWorkgroup #btnReset").attr("disabled","disabled");
		$scope.result=false;
		var dataToServer={
			queueIds:defaultIds
		};
		dataToServer[csrfToken.paramName]=csrfToken.paramValue;
		$http({
			method:"POST",
			url:defaultTicketWorkgroupServiceUrl,
			transformResponse:[function(data){return data;}],
			data:$.param(dataToServer),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			$scope.result=data;
			if(data=="\"SUCCESS\"")
			{
				$scope.result="SUCCESS";
				setTimeout(function(){
					window.location.reload();
				},2000)
			}else{
				$("#frmDefaultTicketWorkgroup #btnSubmit").button("reset");
				$("#frmDefaultTicketWorkgroup #btnReset").removeAttr("disabled");
			}
		}).error(function(){
			$scope.result="ERROR";
			$("#frmDefaultTicketWorkgroup #btnSubmit").button("reset");
			$("#frmDefaultTicketWorkgroup #btnReset").removeAttr("disabled");
		});
	};
}]);
angular.bootstrap(document.getElementById("defaultTicketWorkgroupApp"),["defaultTicketWorkgroupApp"]);