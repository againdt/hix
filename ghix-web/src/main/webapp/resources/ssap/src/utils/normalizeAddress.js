export default addressData => {
    const {
        streetAddress1,
        streetAddress2,
        city,
        state,
        postalCode
    } = addressData;

    return (streetAddress1 || city || state || postalCode) ?
        `${streetAddress1}, ${streetAddress2 ? `${streetAddress2}, ` : ''} ${city}, ${state}, ${postalCode}`
        : '--NA-- ';
};
