<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="ghix" uri="/WEB-INF/tld/dropdown-lookup.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
	       <li><a href="<c:url value="/admin/issuer/partner/list/${encIssuerId}"/>">&lt; <spring:message code="label.back"/></a></li>
	       <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
           <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
	       <li><spring:message  code="pgheader.partners"/></li>
        </ul><!--page-breadcrumb ends-->
		<h1><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
    </div>
	<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.partners"/></h4>
                </div>
                <!--  beginning of side bar --> 
			<ul class="nav nav-list">
				<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message code="pgheader.issuerDetails"/></a></li>
				<li><a href="<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>"><spring:message code="pgheader.issuerRepresentative"/></a></li>
				<c:if test="${exchangeState != 'ID' && exchangeState != 'MN'}">
					<li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message code="pgheader.financialInformation"/></a></li>
				</c:if>	
				<li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message code="pgheader.update.companyProfile"/></a></li>
				<li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
				<%-- <c:if test="${exchangeState != 'ID'}">
					<li><a href="<c:url value="/admin/issuer/market/shop/profile/${encIssuerId}"/>"><spring:message code="pgheader.update.shopMarketProfile"/></a></li>
				</c:if> --%>
				<li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}"/>"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
				<li><a href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message code="pgheader.certificationStatus"/></a></li>
				<li class="active"><a href="<c:url value="/admin/issuer/partner/list/${encIssuerId}"/>"><spring:message  code="pgheader.ediTransactions"/></a></li>
				<li><a href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message code="pgheader.issuerHistory"/></a></li>
				<li><a href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
				<c:if test="${exchangeState == 'ID'}">
				<li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
				</c:if>
                 <c:if test="${activeRoleName == 'OPERATIONS'}">
					<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
				</c:if>
				
			</ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.partners"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/partner/list/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
					<a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                </div>
				<div class="gutter10">
			
				<form class="form-horizontal" id="frmIssuerEditPartner" name="frmIssuerEditPartner" action="<c:url value="/admin/issuer/partner/save" />" method="POST">
					<df:csrfToken/>
                    <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                    <input type="hidden" id="partnerId" name="partnerId" value="${listExchgPartnersObj.id}"/>
                    <input type="hidden" id="issuerId" name="issuerId" value="${issuerObj.id}"/>
                    <input type="hidden" id="currentPaymentId" name="currentPaymentId" value="${curent_payment_id}"/>
                    <input type="hidden" id="insurerName" name="insurerName" value="${issuerObj.name}">
					<input type="hidden" id="hiosIssuerId" name="hiosIssuerId" value="${issuerObj.hiosIssuerId}">
					<div class="control-group">
					<label class="control-label" for="st01"><spring:message code="label.st01" /> - <spring:message code="label.st01.description"/></label>
					<div class="controls">
					<ghix:dropdown name="st01" id="st01" cssclass="span12" lookuptype="ST01" value ="${listExchgPartnersObj.st01}" onchange = "changeSt01(this.value)" />
						
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="market"><spring:message code="label.market" /></label>
						<div class="controls">
							<select name="market" id="market">
									<option value= "24" <c:if test="${'24' == listExchgPartnersObj.market}">SELECTED</c:if>><spring:message code ="label.shop"/></option>
									<option value= "FI" <c:if test="${'FI' == listExchgPartnersObj.market}">SELECTED</c:if>><spring:message code ="label.individual"/></option>
							</select>
							<%--<div id="market_error"> </div>  --%>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="direction"><spring:message 

code="label.direction" /></label>
						<div class="controls">
							<select name="direction" id="direction" onchange="changeDirection

(this.value)">
								<option value=<spring:message code ="label.inbound"/> <c:if 

test="${'Inbound' == listExchgPartnersObj.direction}">SELECTED</c:if>> <spring:message code="label.inbound" /> </option>
								<option value=<spring:message code ="label.outbound"/> 

<c:if test="${'Outbound' == listExchgPartnersObj.direction}">SELECTED</c:if>> <spring:message code="label.outbound" /> 

</option>
							</select>
							<%--<div id="direction_error"> </div>  --%>
						</div>
					</div>

					<div class="control-group" id = "isa05">
						<label class="control-label" for="isa05"><spring:message code="label.isa05" 

/> - <spring:message code="label.isa05.description"/></label>
						<div class="controls">
						<select name="isa05" id="isa05">
								<c:forEach var="isa05" items="${isa05Values}">
									<option value="${isa05}" <c:if test="${isa05 == 

listExchgPartnersObj.isa05}">SELECTED</c:if>>${isa05}</option>
								</c:forEach>
						</select> 
							<%--<div id="isa05_error"></div>  --%>
						</div>
					</div>
					
					<div class="control-group" id = "isa06">
						<label class="control-label" for="isa06"><spring:message  

code="label.isa06"/> - <spring:message code="label.isa06.description"/></label>
						<div class="controls">
							<input type="text" name="isa06" id="isa06Text" maxlength="15" 

class="input-large" value="${listExchgPartnersObj.isa06}"/>
                            <div id="isa06Text_error"  class="error"></div>
						</div>
					</div>
					
					<div class="control-group" id = "isa07">
						<label class="control-label" for="isa07"><spring:message code="label.isa07" 

/> - <spring:message code="label.isa07.description"/></label>
						<div class="controls">
							<select name="isa07" id="isa07">
								<c:forEach var="isa07" items="${isa07Values}">
									<option value="${isa07}" <c:if test="${isa07 == 

listExchgPartnersObj.isa07}">SELECTED</c:if>>${isa07}</option>
								</c:forEach>
							</select>
							<%--<div id="isa07_error"></div>  --%>
						</div>
					</div>
					
					<div class="control-group" id ="isa08">
						<label class="control-label" for="isa08"><spring:message  

code="label.isa08"/> - <spring:message code="label.isa08.description"/></label>
						<div class="controls">
							<input type="text" name="isa08" id="isa08Text" maxlength="15" 

class="input-large" value="${listExchgPartnersObj.isa08}"/>
                            <div id="isa08Text_error"  class="error"></div>
						</div>
					</div>
					
					
					<div class="control-group" id="gs02">
						<label class="control-label" for="gs02"><spring:message  

code="label.gs02"/> - <spring:message code="label.gs02.description"/></label>
						<div class="controls">
							<input type="text" name="gs02" id="gs02Text" maxlength="15" 

class="input-large" value="${listExchgPartnersObj.gs02}"/>
                            <div id="gs02Text_error"  class="error"></div>
						</div>
					</div>
					<div class="control-group" id="gs03">
						<label class="control-label" for="gs03"><spring:message  

code="label.gs03"/> - <spring:message code="label.gs03.description"/></label>
						<div class="controls">
							<input type="text" name="gs03" id="gs03" maxlength="15" 

class="input-large" value="${listExchgPartnersObj.gs03}" />
                            <div id="gs03_error"  class="error"></div>
						</div>
					</div>
					
					
					<div class="control-group" id="gs08">
						<label class="control-label" for="gs08"><spring:message code="label.gs08" 

/> - <spring:message code="label.gs08.description"/></label>
						<div class="controls">
							<select name="gs08" id="gs08" >
								<c:forEach items="${gs08Values}" var="entry">
    								<option value="${entry.key}" <c:if test="${fn:contains

(listExchgPartnersObj.gs08, entry.key)}"> SELECTED</c:if>>${entry.value}</option>
								</c:forEach>
							</select>
							<%--<div id="isa07_error"></div>  --%>
						</div>
					</div>
					
					<div class="control-group" id="mapName">
						<label class="control-label" for="mapName"><spring:message 

code="label.mapname" /></label>
						<div class="controls">
						<select name="mapName" id="mapName">
								<c:forEach var="mapName" items="${ediMapNameValues}">
									<option value="${mapName}">${mapName}</option>
								</c:forEach>
						</select> 
							<div id="mapName_error" class="error"></div> 
						</div>
					</div>
					
					<div class="control-group" id="validationStandard">
						<label class="control-label" for="validationStandard"><spring:message 

code="label.validation.standard"/></label>
						<div class="controls">
						<select name="validationStandard" id="validationStandard">
								<c:forEach var="mapName" 

items="${ediValidationStandardValues}">
									<option value="${mapName}">${mapName}</option>
								</c:forEach>
						</select> 
							<div id="validationStandard_error" class="error"></div> 
						</div>
					</div>
					
					<div class="control-group" id="apf">
						<label class="control-label" for="apf"><spring:message code="label.apf" 

/></label>
						<div class="controls">
						<select name="apf" id="apf">
								<c:forEach var="apf" items="${ediApfValues}">
									<option value="${apf}">${apf}</option>
								</c:forEach>
						</select> 
							<div id="apf_error" class="error"></div> 
						</div>
					</div>
					
 					<div class="control-group">
						<label class="control-label" for="roleId"><spring:message code="label.role" 

/></label>
						<div class="controls">
							<select name="roleId" id="roleId" >
								<c:forEach items="${ediRoles}" var="entry">
    								<option value="${entry.key}" <c:if test="${fn:contains
(listExchgPartnersObj.roleId, entry.key)}"> SELECTED</c:if>>${entry.value}</option>
								</c:forEach>
							</select>
							<div id="roleId_error" class="error"></div> 
						</div>
					</div>
					
					<div class="control-group" id = "sourceDir">
						<label class="control-label" for="sourceDirectory">
							<spring:message  code="label.source.directory"/>
						</label>
						<div class="controls">
							<input type="text" name=sourceDir id="sourceDirTxt" maxlength="200" 

class="input-xlarge" value="${listExchgPartnersObj.sourceDir}"/>
                            <div id="sourceDirTxt_error" class="error"></div>
						</div>
					</div>
					
					<div class="control-group" id = "targetDir">
						<label class="control-label" for="targetDir">
							<spring:message  code="label.target.directory"/>
						</label>
						<div class="controls">
							<input type="text" name=targetDir id="targetDirTxt" maxlength="200" 

class="input-xlarge" value="${listExchgPartnersObj.targetDir}"/>
                            <div id="targetDirTxt_error" class="error"></div>
						</div>
					</div>
					
					<div class="control-group" id = "inprogressDir">
						<label class="control-label" for="inprogressDir">
							<spring:message  code="label.inprocess.directory"/>
						</label>
						<div class="controls">
							<input type="text" name=inprogressDir id="inprogressDirTxt" 

maxlength="200" class="input-xlarge" value="${listExchgPartnersObj.inprogressDir}"/>
                            <div id="inprogressDirTxt_error" class="error"></div>
						</div>
					</div>
					<div class="control-group" id = "archiveDir">
						<label class="control-label" for="archiveDir">
							<spring:message  code="label.archive.directory"/>
						</label>
						<div class="controls">
							<input type="text" name=archiveDir id="archiveDirTxt" 

maxlength="200" class="input-xlarge" value="${listExchgPartnersObj.archiveDir}"/>
                            <div id="archiveDirTxt_error" class="error"></div>
						</div>
					</div>
                     <div class="form-actions paddingLR20"></div>
                    
<script type="text/javascript">
$(window).load(function () {
	if($('#st01').val()== 'GRP'){
		document.getElementById("market").value = "24";
		document.getElementById("market").disabled = true;
		hideUiElementsForGroupInstall();
	}
	if($('#st01').val()== 'TA1'){
		document.getElementById("gs08").value = "TA1";
		showUIElements();
		hideUiElementsForTa1();
	}
	});
</script>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#market').bind('keyup focusout', function(event) {
			$('#eSignature').text($('#applicant_esig').val());
		});
		$('.info').tooltip();
		
	});
</script>
<script type="text/javascript">
function submitForm(){
		$("#frmIssuerEditPartner").submit(); 
} 
$('.ttclass').tooltip();

var validator = $("#frmIssuerEditPartner").validate({ 
	rules : {
		isa06 : { required : true, minlength : 3, maxlength : 15},
		isa08 : { required : true, minlength : 3, maxlength : 15},
		gs02 :  { required : true, minlength : 3, maxlength : 15},
		gs03 :  { required : true, minlength : 3, maxlength : 15},
		sourceDir : { required : true, maxlength : 200},
		targetDir : { required : true, maxlength : 200},
		inprogressDir : { required : true, maxlength : 200},
		archiveDir : { required : true, maxlength : 200}
	},
	messages : {
		isa06 : { required : "<span><em class='excl'>!</em><spring:message  code='err.isa06' 

javaScriptEscape='true'/></span>"},		
		isa08 : { required : "<span><em class='excl'>!</em><spring:message  code='err.isa08' 

javaScriptEscape='true'/></span>"},			
		gs02 :  { required : "<span><em class='excl'>!</em><spring:message  code='err.gs02' 

javaScriptEscape='true'/></span>"},		
		gs03 :  { required : "<span><em class='excl'>!</em><spring:message  code='err.gs03' 

javaScriptEscape='true'/></span>"},		
		sourceDir : { required : "<span><em class='excl'>!</em><spring:message  code='err.sourceDir' 

javaScriptEscape='true'/></span>"},
		targetDir : { required : "<span><em class='excl'>!</em><spring:message  code='err.targetDir' 

javaScriptEscape='true'/></span>"},
		inprogressDir : { required : "<span><em class='excl'>!</em><spring:message  code='err.inProcessDir' 

javaScriptEscape='true'/></span>"},
		archiveDir : { required : "<span><em class='excl'>!</em><spring:message  code='err.archiveDir' 

javaScriptEscape='true'/></span>"}	
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

function changeDirection(selVal){
	if(selVal == 'Inbound'){
		$("#targetDir").hide();
		$("#isa06Text").val('');
		$("#isa08Text").val('');
		$("#gs02Text").val('');
		$("#gs03Text").val('${gs03DefaultValue}');
		
	}
	else if(selVal == 'Outbound'){
		$("#targetDir").show();
		$("#isa06Text").val('${isa06DefaultValue}');
		$("#isa08Text").val('${isa08DefaultValue}');
		$("#gs02Text").val('${gs02DefaultValue}');
		$("#gs02Text").val('${gs02DefaultValue}');
		$("#gs03Text").val('');
	}
}


function changeSt01(selVal){
	if( selVal.indexOf("834") != -1){
		document.getElementById("gs08").value = "834";
		showUIElements();
	}
	if( selVal.indexOf("820") != -1){
		document.getElementById("gs08").value = "820";
		showUIElements();
	}
	if( selVal.indexOf("999") != -1){
		document.getElementById("gs08").value = "999";
		showUIElements();
		$("#mapName").hide();
	}
	if( selVal.indexOf("TA1") != -1){
		document.getElementById("gs08").value = "TA1";
		showUIElements();
		hideUiElementsForTa1();
	}
	if( selVal.indexOf("GRP") != -1){
		document.getElementById("market").value = "24";
		document.getElementById("market").disabled = true;
		hideUiElementsForGroupInstall();
	}
}

function showUIElements(){
	$("#gs02").show();
	$("#gs03").show();
	$("#gs08").show();
	$("#mapName").show();
	$("#validationStandard").show();
	$("#apf").show();
	$("#isa05").show();
	$("#isa06").show();
	$("#isa07").show();
	$("#isa08").show();
}

function hideUiElementsForTa1(){
	$("#gs02").hide();
	$("#gs03").hide();
	$("#gs08").hide();
	$("#mapName").hide();
	$("#validationStandard").hide();
	$("#apf").hide();
}
function hideUiElementsForGroupInstall(){
	$("#isa05").hide();
	$("#isa06").hide();
	$("#isa07").hide();
	$("#isa08").hide();
	$("#gs02").hide();
	$("#gs03").hide();
	$("#gs08").hide();
	$("#mapName").hide();
	$("#validationStandard").hide();
	$("#apf").hide();

}
</script>
