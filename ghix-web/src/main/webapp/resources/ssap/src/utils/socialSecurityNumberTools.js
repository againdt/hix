export default {
    getAdjustedSsn: ssn => {
        if(ssn === null) {
            return '';
        }
        const ssnToString = String(ssn);
        if(typeof ssn === 'number' && ssnToString.length === 9) {
            return `${ssnToString.slice(0, 3)}-${ssnToString.slice(3, 5)}-${ssnToString.slice(5)}`;
        } else {
            return ssn;
        }

    },

    getOnFocus: ssn => {
        if(ssn === null) {
            return '';
        } else {
            const ssnToString = String(ssn);
            return `${ssnToString.slice(0, 3)}-${ssnToString.slice(3, 5)}-${ssnToString.slice(5)}`;
        }
    },

    cleanedUpSsn: ssn => typeof ssn === 'string' &&
        ssn.split('').filter(ch => ch.match(/^[0-9]*$/)).join(''),

    maskSsn: ssn => {
        if(ssn === null) {
            return '';
        }
        const ssnToString = String(ssn);
        if(ssnToString.length === 9) {
            return `***-**-${ssnToString.slice(5)}`;
        } else {
            return ssn;
        }
    },

    autoFillSsn: (ssn, previousSSNValue) => {
        let result;
        switch(true) {
        case ssn.length < previousSSNValue.length:
            switch (ssn.length) {
            case 0:
                result = ''; break;
            case 4:
                result = ssn.slice(0, ssn.length - 1); break;
            case 7:
                result = ssn.slice(0, ssn.length - 1); break;
            default:
                result = ssn;
            } break;
        case ssn.length > previousSSNValue.length:
            switch (ssn.length) {
            case 4:
                result = `${ssn.slice(0, 3)}-${ssn.slice(-1)}`; break;
            case 7:
                result = `${ssn.slice(0, 3)}-${ssn.slice(4, 6)}-${ssn.slice(-1)}`; break;
            default:
                result = ssn;
            } break;
        default:
            result = ssn; break;
        }
        return result;
    }
};