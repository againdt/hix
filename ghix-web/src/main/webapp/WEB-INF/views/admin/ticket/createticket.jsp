<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page isELIgnored="false"%>
<%-- <jsp:include page="verifyDocument.jsp">
	<jsp:param value="" name=""/>
</jsp:include> --%>

<!-- this style is added as per HIX-14441, to Make the auto suggest box for requester as scrollable -->
<style type="text/css">
.ui-autocomplete{
height: 125px !important;
overflow-y: scroll;
overflow-x: hidden;width: 220px !important;
font-family:Helvetica,Arial,sans-serif;
font-size:13px;
} 

</style>

<style rel="stylesheet" type="text/css">
	.row-fluid .span70 {
    width: 70%;
}

.head-btn{
	float:right;
	width:auto;
}
</style>
<div class="gutter10">
	<div class="row-fluid" id="titlebar">
			<h1 id="skip">
				<c:choose>
				<c:when test="${flow != null && flow == 'edit'}">
					<c:out value="${ticket.subject}"/>
				</c:when>
				<c:otherwise>
					<spring:message  code="label.createTicket"/>
				</c:otherwise>
				</c:choose>
<%-- 				<c:if test="${stateExchenge != null && stateExchenge != 'SHOP' }"> --%>
<%-- 				 <a href="#" class="btn btn-small btn-primary pull-right"  id='fff' onclick="javascript:showUploadDiv()" data-toggle="modal"><spring:message  code="label.addDocumentType"/></a> --%>
<%-- 				</c:if> --%>
			</h1>
	</div>
	
	<div class="row-fluid">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" id="sidebar">
            <jsp:include page="ticketsidebar.jsp"></jsp:include>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">
            <div style="font-size: 14px; color: red">
                <c:if test="${errorMsg != null && errorMsg != ''}">
                    <p><c:out value="${errorMsg}"></c:out><p/>
                </c:if>
            </div>
            <div class="header">
                <h4 class="pull-left" id="pageTitle"> <spring:message code="${flow != null && flow == 'add' ? 'label.createTicket' : 'label.editTicket'}"/> </h4>
            </div>
            <div class="row-fluid">
                <div class="gutter10">

                <form class="form-horizontal gutter10" id="fromaddticket" name="fromaddticket"
                action='<c:if test="${flow != null && flow == 'add'}"> <c:out value="/hix/ticketmgmt/ticket/createpage/submit"/></c:if>
                <c:if test="${flow != null && flow == 'edit'}"><c:out value="/hix/ticketmgmt/ticket/edit/submit"/></c:if>'
                method="POST">
                <div class="row-fluid">
                <df:csrfToken/>
                <input type="hidden" id="id" name="id" value="${ticket.id}">
                <input type="hidden" name="prevPage" value="${prevPage}">




                    <!-- HIX-36034 -->
                    <div id="subjectDiv" class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.subject"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
                        <a class="ttclass" rel="tooltip" href="#" data-original-title='<spring:message  code="label.ticketSubject_tooltip" />'><i class="icon-question-sign"></i></a></label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <div class="pull-left">
                                 <textarea name="subject" id="subject" class="input-large input-textarea pdf_txtbox" rows="4" cols="20" maxlength="100">${ticket.subject}</textarea>
                                    <div id="subject_error"></div>
                                </div>
                            </div>
                    </div>
                    <!-- end of form-group -->

                    <div class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.ticketType"/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
                        <a class="ttclass" rel="tooltip" href="#" data-original-title='<spring:message  code="label.ticketType_tooltip" />'><i class="icon-question-sign"></i></a></label>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <select class="input-large" name="tkmWorkflows.category" id="ticketType">
                                <c:if test="${'edit' == flow}">
                                    <option
                                        value='${ticket.tkmWorkflows.category}'>${ticket.tkmWorkflows.category}
                                    </option>
                                SELECTED </c:if>
                                <option value="">Select</option>
                                <c:forEach var="category" items="${typeList}">
                                    <option value="<c:out value="${category}" />">
                                        <c:out value="${category}" />
                                </c:forEach>
                            </select>
                            <div id="ticketType_error"></div>
                        </div>
                    </div>

                    <div id="userRoleDiv" class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">Created For Role
                        <a class="ttclass input-large" rel="tooltip" href="#" data-original-title='<spring:message  code="label.requesterRole_tooltip" />'><i class="icon-question-sign"></i></a></label>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <select class="input-large" name="userRole.id"
                                id="userRole" onchange="javascript:checkUserRole();">
                                <c:if test="${flow == 'edit'}">
                                    <option value='${ticket.userRole.id}'>${ticket.userRole.id} </option>
                                    SELECTED
                                </c:if>
                                <option value="">Select</option>
                                <c:forEach var="role" items="${roleList}">
                                    <option value="<c:out value="${role.id}" />">
                                        <c:out value="${role.label}" />
                                    </option>
                                </c:forEach>
                            </select>
                            <div id="userRole_error"></div>
                        </div>
                    </div>
                    <!-- end of form-group -->

                    <div id="companyDiv" class="form-group" style="display: none">
                    <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"> <spring:message  code="label.ct_companyName"/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <div class="pull-left">
                                    <input type="text" class="input-large" name="company" id="company"  >
                                    <div id="company_error"></div>
                                </div>
                                <div class="companyLoader pull-left">
                                    <img style="float:right;" src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="20" height="20" />
                                </div>

                        </div>

                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" style="display:none;">
                                <input type="text" class="input-large" name="moduleId"
                                id="moduleId" value="${moduleId}"  >
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" style="display:none;">
                                <input type="text" class="input-medium" name="moduleName"
                                id="moduleName" value="${moduleName}"  >
                            </div>
                    </div>
                    <!-- end of form-group -->
                    <div id="requesterDiv" class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">Created For  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
                        <a class="ttclass" rel="tooltip" href="#" data-original-title='<spring:message  code="label.requesterName_tooltip" />'><i class="icon-question-sign"></i></a></label>

                        <c:choose>
                            <c:when test="${flow == 'edit'}">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <div class="pull-left">
                                    <input type="text" class="input-large"
                                    id="requester" value="${ticket.role.firstName} ${ticket.role.lastName}" type="text"
                                    onkeyup="javascript:getRequesters();"
                                    >
                                    <div id="requester_error"></div>
                                    </div>
                                </div>
                                <div  style="display: none;">
                                    <div class="pull-left">
                                    <input type="text" class="input-large" name="role.id"
                                        id="role.id" value="${ticket.role.id}">
                                    <div id="role.id_error"></div>
                                    </div>
                                </div>

                            </c:when>
                            <c:otherwise>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <div class="pull-left">
                                        <input type="text" class="input-large" name="requester" id="requester" onkeyup="javascript:getRequesters();">
                                        <div id="requester_error"></div>
                                    </div>
                                        <div class="requesterLoader pull-left">
                                        <img style="float:right;" src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="20" height="20" />
                                        </div>
                                </div>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" style="display:none;">
                                    <input type="text" class="input-large" name="role.id"
                                    id="roleId" >
                                    <div id="roleId_error"></div>
                                </div>
                            </c:otherwise>
                            </c:choose>

                     </div>
                    <!-- end of form-group -->
                    <div id="priorityDiv" class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.priority"/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
                         <a class="ttclass" rel="tooltip"  data-original-title='<spring:message  code="label.priority_tooltip" />'><i class="icon-question-sign"  ></i></a></label>
                           <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <div class="pull-left">
                              <select class="input-large" name="priority" id="priority">
                                    <c:forEach var="priority" items="${priorityList}">
                                        <option
                                            <c:choose>
                                                <c:when	test="${((flow == 'edit')  && (priority.toString() == ticket.priority)) ||
                                                ((flow == 'add')  && (priority.toString() == 'Medium'))}"> SELECTED </c:when>
                                            </c:choose>
                                            value="<c:out value="${priority.toString()}" />">
                                            <c:out value="${priority.toString()}" />
                                    </c:forEach>
                                </select>
                                <div id="priority_error"></div>
                                </div>
                            </div>
                    </div>

                    <!-- end of form-group -->



                    <div id="desc" class="form-group">
                        <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.ticketDetail"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
                        <a class="ttclass input-large" rel="tooltip" href="#" data-original-title='<spring:message  code="label.ticketDesc_tooltip" />'><i class="icon-question-sign"></i></a></label>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <div class="pull-left">
                            <div class="input">
                                <textarea name="description" id="description" class="input-large input-textarea"
                                    rows="4" cols="20" >${ticket.description}</textarea>
                            </div>
                            <div id="description_error"></div>
                            </div>
                        </div>
                    </div>
                    </div>
                </form>
                    </div>
                    <!-- HIX-36034 -->
                    <div class="form-actions form_actions">
                    <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
                        <a href="<c:url value="${prevPage}" />" id="cancelFooterBtn" class="btn btn_cancel"><spring:message  code="button.cancel"/></a>
                        <a class="btn btn-primary btn_save" id="saveTicketFooterBtn" onclick="javascript:saveTicket();"><spring:message  code="button.save"/></a>
                     </div>
                    </div>
                    <!-- end of form-group -->
            </div>
        </div>
	</div>
</div>
	<!--     </div> end of row-fluid -->

<!--  end of gutter10 -->

<script type="text/javascript">
   $('.ttclass').tooltip();
	var availableUsers = new Array();
	var availableUsersEmailId = new Array();
	 var availableCompanyList = new Array(); 
	 function showUploadDiv()
	 {  	
	 	$('#tkmErrorDiv').html("");
	 	$('#uploadDialog').modal('show');
	 	checkDocumentType();
	 	
	 	}
	function checkTicketType() {
		
		$("#ticketSubType").empty();
		$("#ticketSubType_error").val("");

		var validateUrl = '<c:url value="/ticketmgmt/ticket/subtype"></c:url>';
		var ticketType = $("#ticketType").val();	
		
		if(ticketType == "" || ticketType == "Triage") {
			$('#ticketSubType')
	         .append($("<option></option>")
	         .attr("value","Triage")
	         .text("Triage")); 
			
			$('#subtypeDiv').attr('style', 'display:none');
		}
		else {
			if ('${flow}' == 'edit') {
				$('#subtypeDiv').attr('style', 'display:block');
			} else {
				$.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
						ticketType : ticketType
					},
					success: function(response,xhr){
						if(isInvalidCSRFToken(xhr))	    				
							return;
						$('#subtypeDiv').attr('style', 'display:block');
						if(response == "") {
							$("#ticketSubType_error").val("Unable to add the ticket type");
						} else {
	        				populateTicketSubType(response);
						}
	        		},
	       			error: function(e){
	        			alert("Failed to Add subtype");
	        		},
				});
			}
		}
		
	}
	

	function populateTicketSubType(subListJSON){
		subListJSON = JSON.parse(subListJSON);
		
		for(var i=0;i<subListJSON.length;i++){
		    var obj = subListJSON[i];
		    $('#ticketSubType')
	         .append($("<option></option>")
	         .attr("value",obj)
	         .text(obj)); 
		}
	}

	function checkUserRole() {
		availableUsers.length =0;
		 availableCompanyList.length = 0;
		$('#companyDiv').attr('style', 'display:none');
		
		$("#company").empty();
		$("#company").val("");
 		
		
		$("#requester").val("");
		$("#requester").empty();
		$("#roleId_error").val("");
		
		$("#moduleId").val('');
		
		var validateUrl = "";
		var elt = document.getElementById("userRole");
		var userRole = elt.options[elt.selectedIndex].text;
		
		if(userRole == "Employer") {
		 $('#companyDiv').attr('style', 'display:block');
		   $( "#requester" ).attr( "placeholder","");
		   validateUrl = '<c:url value="/ticketmgmt/ticket/employers"></c:url>';
		   
		   getCompanyList(validateUrl, 'EMPLOYER');
		  } else if(userRole == "Issuer") {
		   $('#companyDiv').attr('style', 'display:block');
		   validateUrl = '<c:url value="/ticketmgmt/ticket/issuers" ></c:url>';
		   
		   getCompanyList(validateUrl, 'ISSUER');
		  } else if(userRole == "Employee") {
		   $( "#requester" ).attr( "placeholder","");
		   $('#companyDiv').attr('style', 'display:block');
		   validateUrl = '<c:url value="/ticketmgmt/ticket/employers" ></c:url>';
			
			getCompanyList(validateUrl, "EMPLOYER");
		} /* else { // commented because the requesters will be populated only when user types in three starting letters of the first or last name of the user
			getRequesters();
		} */
		/* if(userRole=="Select")
			{
				$("#requester").prop("disabled",true);
			}else{
				$("#requester").prop('disabled', false);
		} */
	}
	
	function getCompanyList(url, userRole) {
		$('.companyLoader').show();
		$( "#company" ).attr( "placeholder","");
		
		$.ajax({
			url : url,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				roleName : userRole
			},
			success: function(response,xhr){
				if(isInvalidCSRFToken(xhr))	    				
					return;
				if(response == "") {
					$('.companyLoader').hide();
					$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompanyNotPresent"/>");
					$("#company_error").val("Unable to add the users for the selected role");
				} else {
					$('.companyLoader').hide();
       				populateCompany(response);
				}
       		},
      		error: function(e){
       			alert("Failed to Add User Role");
       		},
		});
	}
	
 function populateCompany(companyMapJSON) {
	 companyMapJSON = JSON.parse(companyMapJSON);
		var index=0;
		availableUsers.length =0;
		 availableCompanyList.length = 0; 
		 if(jQuery.isEmptyObject(companyMapJSON)){
				$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompanyNotPresent"/>");
				
			}else{
				$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompany"/>");
		for (var key in companyMapJSON) {
			availableCompanyList[index]={label:companyMapJSON[key], idx:key};
			index++;		
	    }
	   }
		 autoCompleteCompanyMap();  
	}
 function autoCompleteCompanyMap(){
	 $( "#company" ).focus(function() {
	    	$(this).data("autocomplete").search($(this).val());
 	}).autocomplete({
			minLength : 0,
			source: function (request, response) {
		            var term = $.ui.autocomplete.escapeRegex(request.term)
		                , startsWithMatcher = new RegExp("^" + term, "i")
		                , startsWith = $.grep(availableCompanyList, function(value) {
		                    return startsWithMatcher.test(value.label || value.value || value);
		                })
		                , containsMatcher = new RegExp(term, "i")
		                , contains = $.grep(availableCompanyList, function (value) {
		                    return $.inArray(value, startsWith) < 0 &&
		                        containsMatcher.test(value.label || value.value || value);
		                });

         			response(startsWith.concat(contains));
    		 },
	     	 select: function(event, ui) { AutoCompleteSelectHandler(event, ui); },
		  	 change: function( event, ui ) {if(ui.item==null) $("#moduleId").val('');}
	   	});
	 function AutoCompleteSelectHandler(event, ui) {               
		 $("#moduleId").val(ui.item.idx);
		 // getRequesters(); //// commented because the requesters will be populated only when user types in three starting letters of the first or last name of the user
		 $("#company_error").html('');
	  	}
    }
  function getRequesters(){
	  if($("#requester").val().length>=3){
		 //$("#requester").empty();
		$("#roleId_error").val("");
		
		var validateUrl = '<c:url value="/ticketmgmt/ticket/requesters"></c:url>';
		var userRole = $("#userRole").val();
		var companyId = $("#moduleId").val();
		var elt = document.getElementById("userRole");
		var userRoleName = elt.options[elt.selectedIndex].text;
		var userName = encodeURIComponent($("#requester").val());
			
		availableUsers.length = 0;
		$( "#requester" ).attr( "placeholder","");
		$("#roleId").val('');
		//if(userRole!=''){
			$('.requesterLoader').show();
			$.ajax({
				url : validateUrl,
				type : "POST",
				contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
				data : {
					${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
					roleType : userRole,
					moduleId : companyId,
					moduleName : userRoleName,
					userName : userName
				},
				success: function(response,xhr){
					if(isInvalidCSRFToken(xhr))	    				
						return;
						 //$("#requester").empty();
					if(response == "") {
						$('.requesterLoader').hide();
						$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequesterNotPresent"/>");
						$("#roleId_error").val("Unable to add the users for the selected role");
					} else {
						//console.log('response=',response);
						$('.requesterLoader').hide();
	       				populateRequesters(response);
		       				autoCompleteBox();
		       				$("#requester").data("autocomplete").search($("#requester").val());
					}
	       		},
	      		error: function(e){
					$('.requesterLoader').hide();
	       			alert("Failed to Add User Role");
	       		},
			});
		//}
	  }
	}
	

	function populateRequesters(userListJSON){
		userListJSON = JSON.parse(userListJSON);
		//console.log('userListJSON=',userListJSON);
		var index=0;
		
		if(jQuery.isEmptyObject(userListJSON)){
			$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequesterNotPresent"/>");
			
		}else{
			availableUsersEmailId = new Array();
			$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequester"/>");
			$.each(userListJSON, function(key,value) {
				var uiLabel="";
			
				if((typeof value[4] == 'undefined' || value[4] == null) && (typeof value[3] == 'undefined' ||  value[3] == null)) {
					
					uiLabel = value[1];
				}
				
				else if(typeof value[4] == 'undefined' ||  value[4] == null){
					uiLabel = value[1]+" ("+value[3]+")";
				}
				
				else if(typeof value[3] == 'undefined' ||  value[3] == null){
					uiLabel = value[1]+" ("+value[4]+")";
				}
				
				else 
					uiLabel = value[1]+" ("+ value[4]+" : "+ value[3]+")";
					
				availableUsers[index]={label:uiLabel, idx:value[0],desc:value[2]};
				index++;
				});
		    }
		
	}
	
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		  alert($('Session is invalid').text());
		  rv = true;
		 }
		return rv;
	}
	
	jQuery.validator.addMethod("ticketSubTypeCheck", function (value, element, param) {
		
	    var ticketSubType = $("#ticketSubType").val();
	    var ticketType = $("#ticketType").val();
	    
	    if(ticketType != "Triage") {
		    if (ticketSubType == "") {
		        return false;
		    }
	    } else {
	    	$("#ticketSubType").val("Triage");
	    }
	    return true;
	});
	
	jQuery.validator.addMethod("requesterIdCheck", function (value, element, param) {
	    var requesterId = $("#roleId").val();
	    
	    if(requesterId === ''||requesterId===null){
	    	return false;
	    }
	    
	    return true;	    
	});
	 
	jQuery.validator.addMethod("moduleIdCheck", function (value, element, param) {
	    var companyId = $("#moduleId").val();
	    
	    if(companyId === ''||companyId===null){
	    	return false;
	    }
	    
	    return true;	    
	});
	
	
	jQuery.validator.addMethod("specialcharacters", function(value, element) {
		formSubmited=false;
	    return !(this.optional(element) || value.startsWith('=') || value.startsWith('@') || value.startsWith('+') || value.startsWith('-'));
    });
	
	var validator = $("#fromaddticket").validate({ 
		rules : {
			    subject : { required : true, specialcharacters: true },
			    description : { required : true, maxlength : 2000, specialcharacters: true },
			    'tkmWorkflows.category' : { required : true },
			    'tkmWorkflows.type' : { required : true, ticketSubTypeCheck: true},
			    'requester': {required : true ,requesterIdCheck:true},
			    'userRole.id' : {required:true},
			    priority : {required:true},
			    'company': {required : true ,moduleIdCheck:true}
		},
		messages : {
			subject : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateSubject' javaScriptEscape='true'/></span>",
				specialcharacters :"<span> <em class='excl'>!</em><spring:message code='label.validateTicketSpecialChar' javaScriptEscape='true'/></span>"},
			description: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateDescription' javaScriptEscape='true'/></span>",
						   maxlength : "<span> <em class='excl'>!</em><spring:message code='label.descriptionMaxLen' javaScriptEscape='true'/></span>",
						   specialcharacters :"<span> <em class='excl'>!</em><spring:message code='label.validateTicketSpecialChar' javaScriptEscape='true'/></span>"},
			'tkmWorkflows.category': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTicketType' javaScriptEscape='true'/></span>"},
			'tkmWorkflows.type': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTicketSubType' javaScriptEscape='true'/></span>"},
			'userRole.id': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateUserRole' javaScriptEscape='true'/></span>"},
			priority: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriority' javaScriptEscape='true'/></span>"},
			'company': { required : "<span> <em class='excl'>!</em><spring:message code='label.validatecompany' javaScriptEscape='true'/></span>",
		                moduleIdCheck:"<span> <em class='excl'>!</em><spring:message code='label.validateCorrectCompany' javaScriptEscape='true'/></span>"},
		},
		errorClass: "error",
		errorPlacement : function(error, element) {
			formSubmited=false;
			var elementId = element.attr('id');
			if($("#" + elementId + "_error").html() != null)
			{
				$("#" + elementId + "_error").html('');
			}
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		}
	});

	$(document).ready(function() {
		$('#subTypeDiv').attr('style', 'display:none');
		
	
		if ('${flow}' == 'edit') {
			$('#userRoleDiv').attr('style', 'display:none');
			
			$("#ticketType").attr("disabled", true);
			$("#ticketSubType").attr("disabled", true);
			$("#requester").attr("disabled", true);
			$("#userRole").attr("disabled", true);

			$('#subtypeDiv').attr('style', 'display:block');
			
			if('${ticket.tkmWorkflows.type}' == "Triage") {
				$('#subtypeDiv').attr('style', 'display:none');
			}	
			
			//Disable the col-xs-12 col-sm-7 col-md-7 col-lg-7 if the ticket is Resolved, Completed or Canceled
			var ticketStatus = "${ticket.status}";
			if(ticketStatus == 'Completed' || ticketStatus=='Canceled' || ticketStatus=='Resolved') {
				$('#subject').attr("disabled", true);
				$('#priority').attr("disabled", true);
				$('#description').attr("disabled", true);
				$('#saveTicketBtn').attr('style', 'display:none');
				$('#cancelEditBtn').attr('style', "float: right; margin-right:10px;");
			}
			// Disabling col-xs-12 col-sm-7 col-md-7 col-lg-7 code ends here 
		} else {
			$('#userRoleDiv').attr('style', 'display:block');
			
			$("#ticketType").attr("disabled", false);
			$("#ticketSubType").attr("disabled", false);
			//$("#requester").attr("disabled", false);
			$("#userRole").attr("disabled", false);
		}
		
		var defPreFillDate="${prefillUser}";
		var defRoleId="${roleId}";
		var defRequesterName="${requestorName}";
		var defRequesterId="${requestorId}";
		
		
		if(defPreFillDate=='true')
			{
				$("#userRole").val(defRoleId).attr("disabled","disabled");			
				$("#requester").attr("disabled","disabled").val(defRequesterName);
				$("#roleId").val(defRequesterId);
				
			};
		
	});
	
	var formSubmited=false;
	
	function saveTicket() {
		 if(formSubmited==false)
			{ 
				formSubmited=true;
				$("#userRole").removeAttr("disabled").attr("readonly","readonly");
				$("#fromaddticket").submit();
				
			 }
		
	}
var autoCompleteBoxCreate=false;
	function autoCompleteBox() { 
		if(autoCompleteBoxCreate){return;}
		autoCompleteBoxCreate=true;
		$("#requester").on("focus",function(){
			$("#requester").data("autocomplete").search($("#requester").val());			
		});
    	$("#requester").autocomplete({
			minLength : 0,
			source: function (request, response) {
		            var term = $.ui.autocomplete.escapeRegex(request.term)
		                , startsWithMatcher = new RegExp("^" + term, "i")
		                , startsWith = $.grep(availableUsers, function(value) {
		                    return startsWithMatcher.test(value.label || value.value || value);
		                })
		                , containsMatcher = new RegExp(term, "i")
		                , contains = $.grep(availableUsers, function (value) {
		                    return $.inArray(value, startsWith) < 0 &&
		                        containsMatcher.test(value.label || value.value || value);
		                });

            			response(startsWith.concat(contains));
       		 },
	     	 select: function(event, ui) {AutoCompleteSelectHandler(event, ui); },
		  	 change: function( event, ui ) {
		  		 if(ui.item==null) 
		  			 $("#roleId").val('');
		  		else{
		  			 if ($("#userRole").val() == ""){
		  					var requesterStr=$("#requester").val();
		  					var userRoleVal="";
		  					if(requesterStr.indexOf(':')!=-1)
		  						userRoleVal = requesterStr.slice(requesterStr.indexOf("(") + 1, requesterStr.lastIndexOf(":")).trim();
		  					else
		  						userRoleVal = requesterStr.slice( requesterStr.indexOf("(") + 1, requesterStr.lastIndexOf(")")).trim();
		  					
		  	 				$("#userRole option").filter(function(index) { 
			  					return $(this).text().trim() === userRoleVal;
			  				}).attr('selected', 'selected');
			  			}
		  		 }
		  		 
		  	 },
	     	 focus: function( event, ui ) {$(".ui-autocomplete > li").attr("title", ui.item.desc);}
	   	});
	
		function AutoCompleteSelectHandler(event, ui) {   
			//console.log('inside select handler');
	      $("#roleId").val(ui.item.idx);
	      
	       $("#requester_error").html('');
	       $("#roleId_error").html('');
	  	}
	}
</script>
