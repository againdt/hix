package com.getinsured.hix.referral.exception;

/**
 * @author chopra_s
 * 
 */
public class ReferralServiceException extends Exception {
	private String message;
	private Exception baseException;
	private String errorCode;
	private String errorSeverity;
	private static final long serialVersionUID = 1L;
	
	public enum ReferralErrors {
		/**
		 * REFERRAL_10001, "Unable to bind user with the application."
		 */
		USER_CMR_TO_REFERRAL_BINDERROR("REFERRAL_10001"),
		/**
		 * REFERRAL_10002, "Invalid Referral Activation Id."
		 */
		SECURITY_QUESTION_RETRIEVAL_ERROR("REFERRAL_10002"),
		/**
		 * REFERRAL_10003, "Invalid Work Flow Status."
		 */
		INVALID_APPLICATION_STATUS("REFERRAL_10003"),
		/**
		 * REFERRAL_10004, "Security Question validation Exception."
		 */
		SECURITY_QUESTION_EXCEPTION("REFERRAL_10004"),
		/**
		 * REFERRAL_10005, ""Document Upload Exception."
		 */
		DOCUMENT_FETCH_EXCEPTION("REFERRAL_10005"),
		/**
		 * REFERRAL_10006, "Consumer Document Fetch Exception."
		 */
		DOCUMENT_UPLOAD_EXCEPTION("REFERRAL_10006"),
		/**
		 * REFERRAL_10007, "RIDP Check Exception."
		 */
		RIDP_CHECK_EXCEPTION("REFERRAL_10007"),
		/**
		 * REFERRAL_10008, "Unable to process LCE."
		 */
		LCE_CHECK_EXCEPTION("REFERRAL_10008"),
		/**
		 * REFERRAL_10009, "Unable to update LCE events."
		 */
		LCE_UPDATE_EXCEPTION("REFERRAL_10009"),
		/**
		 * REFERRAL_10010, "Unable to navigate to Pending Referral."
		 */
		PENDING_REFERRAL_EXCEPTION("REFERRAL_10010"),
		/**
		 * REFERRAL_10011, "Unable to navigate to CSR Referral Linking."
		 */
		REFERRAL_CSR_LINKING_EXCEPTION("REFERRAL_10011"),
		/**
		 * REFERRAL_10012, "Coverage Year Exception."
		 */
		COVERAGE_YEAR_EXCEPTION("REFERRAL_10012");

		private final String id;
		
		ReferralErrors(String id) {
			this.id = id;

		}

		public String getId() { return id; }

	}
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorSeverity() {
		return errorSeverity;
	}

	public void setErrorSeverity(String errorSeverity) {
		this.errorSeverity = errorSeverity;
	}


	public ReferralServiceException(String message) {
		super();
		this.message = message;
	}

	public ReferralServiceException(String message, Exception baseException) {
		super();
		this.message = message;
		this.baseException = baseException;
	}
	public ReferralServiceException(String errorCode, String errorMsg, String errorSeverity) {
		super(errorMsg);
		this.errorCode = errorCode;
		this.message = errorMsg;
		this.errorSeverity = errorSeverity;
	}
	
	public ReferralServiceException(ReferralErrors referralError) 
	{
		this.errorCode = referralError.getId();
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getBaseException() {
		return baseException;
	}

	public void setBaseException(Exception baseException) {
		this.baseException = baseException;
	}

}
