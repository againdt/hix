package com.getinsured.eligibility.ssap.documentverification.service;


import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.getinsured.eligibility.ssap.documentverification.exceptions.SsapDocumentsServiceException;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.iex.ssap.repository.IConsumerDocumentRepository;

@Service("ssapApplicantDocumentService")
public class SsapApplicantDocumentServiceImpl implements SsapApplicantDocumentService {

	private static final Logger LOGGER = Logger.getLogger(SsapApplicantDocumentServiceImpl.class);
	private static final String SUCCESS_RESPONSE = "{\"responseCode\": \"SUCCESS\"}";

	@Autowired
	private IConsumerDocumentRepository consumerDocumentRepository;
	
	@Override
	public ConsumerDocument saveSsapApplicantDocument(ConsumerDocument consumerDocument) throws SsapDocumentsServiceException {
		
		consumerDocument.setComments("");
		consumerDocument = consumerDocumentRepository.save(consumerDocument);
		
		
		return consumerDocument;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public String handleException(Throwable exception) {
		if(null == exception) {
			return "Unknown";
		}
		JSONObject exceptionJSONObj = new JSONObject();
		exceptionJSONObj.put("exception", exception.getClass().getName());
		exceptionJSONObj.put("message", exception.getMessage());
		exceptionJSONObj.put("cause", handleException(exception.getCause()));
		return exceptionJSONObj.toJSONString();
	}


}
