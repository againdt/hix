

import { bloodRelationshipsTypes } from '../constants/actionTypes';
import CONFIG from '../.env';



let initialState = {};

if(CONFIG.ENV.DEVELOPMENT) {
    initialState = {
        dropdownOptions: [
            {
                text: 'Brother-in-law or Sister-in-law',
                value: '12'
            },
            {
                text: 'Child (son or daughter)',
                type: 'CHILD',
                value: '19',
                mirrorType: 'PARENT'
            },
            {
                text: 'Court Appointed or Live-In Guardian',
                type: 'COURT_APPOINTED_GUARDIAN',
                value: '31',
                mirrorType: 'WARD'
            },
            {
                text: 'Domestic partner',
                type: 'DOMESTIC_PARTNER',
                value: '53',
                mirrorType: 'DOMESTIC_PARTNER'
            },
            {
                text: 'First Cousin',
                type: 'FIRST_COUSIN',
                value: '08',
                mirrorType: 'FIRST_COUSIN'
            },
            {
                text: 'Former Spouse',
                type: 'FORMER_SPOUSE',
                value: '25',
                mirrorType: 'FORMER_SPOUSE'
            },
            {
                text: 'Grandchild (grandson or granddaughter)',
                type: 'GRANDCHILD',
                value: '05',
                mirrorType: 'GRANDPARENT'
            },
            {
                text: 'Grandparent (grandfather or grandmother)',
                type: 'GRANDPARENT',
                value: '04',
                mirrorType: 'GRANDCHILD'
            },
            {
                text: 'Mother-in-law or Father-in-law',
                type: 'MOTHER_FATHER_IN_LAW',
                value: '13',
                mirrorType: 'SON_DAUGHTER_IN_LAW'
            },
            {
                text: 'Nephew or Niece',
                type: 'NEPHEW_NIECE',
                value: '07',
                mirrorType: 'UNCLE_AUNT'
            },
            {
                text: 'Parent (father or mother)',
                type: 'PARENT',
                value: '03',
                mirrorType: 'CHILD'
            },
            {
                text: 'Self',
                type: 'SELF',
                value: '18',
                mirrorType: 'SELF'
            },
            {
                text: 'Sibling (brother or sister)',
                type: 'SIBLING',
                value: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                value: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Spouse',
                type: 'SPOUSE',
                value: '01',
                mirrorType: 'SPOUSE'
            },
            {
                text: 'Stepchild (stepson or stepdaughter)',
                type: 'STEPCHILD',
                value: '17',
                mirrorType: 'STEPPARENT'
            },
            {
                text: 'Stepparent (stepfather or stepmother)',
                type: 'STEPPARENT',
                value: '16',
                mirrorType: 'STEPCHILD'
            },
            {
                text: 'Uncle or Aunt',
                type: 'UNCLE_AUNT',
                value: '06',
                mirrorType: 'NEPHEW_NIECE'
            },
            {
                text: 'Ward',
                type: 'WARD',
                value: '15',
                mirrorType: 'COURT_APPOINTED_GUARDIAN'
            }
        ],
        loadedRelations: [
            {
                text: 'Brother-in-law or Sister-in-law',
                type: 'BROTHER_OR_SISTER_IN_LAW',
                code: '12',
                mirrorType: 'BROTHER_OR_SISTER_IN_LAW'
            },
            {
                text: 'Child (son or daughter)',
                type: 'CHILD',
                code: '19',
                mirrorType: 'PARENT'
            },
            {
                text: 'Counter Appointed Guardian',
                type: 'COURT_APPOINTED_GUARDIAN',
                code: '31',
                mirrorType: 'WARD'
            },
            {
                text: 'Domestic partner',
                type: 'DOMESTIC_PARTNER',
                code: '53',
                mirrorType: 'DOMESTIC_PARTNER'
            },
            {
                text: 'First Cousin',
                type: 'FIRST_COUSIN',
                code: '08',
                mirrorType: 'FIRST_COUSIN'
            },
            {
                text: 'Former Spouse',
                type: 'FORMER_SPOUSE',
                code: '25',
                mirrorType: 'FORMER_SPOUSE'
            },
            {
                text: 'Grandchild (grandson or granddaughter)',
                type: 'GRANDCHILD',
                code: '05',
                mirrorType: 'GRANDPARENT'
            },
            {
                text: 'Grandparent (grandfather or grandmother)',
                type: 'GRANDPARENT',
                code: '04',
                mirrorType: 'GRANDCHILD'
            },
            {
                text: 'Mother-in-law or Father-in-law',
                type: 'MOTHER_FATHER_IN_LAW',
                code: '13',
                mirrorType: 'SON_DAUGHTER_IN_LAW'
            },
            {
                text: 'Nephew or Niece',
                type: 'NEPHEW_NIECE',
                code: '07',
                mirrorType: 'UNCLE_AUNT'
            },
            {
                text: 'Parent (father or mother)',
                type: 'PARENT',
                code: '03',
                mirrorType: 'CHILD'
            },
            {
                text: 'Self',
                type: 'SELF',
                code: '18',
                mirrorType: 'SELF'
            },
            {
                text: 'Sibling (brother or sister)',
                type: 'SIBLING',
                code: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                code: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Sibling (brother or sister',
                type: 'SIBLING',
                code: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                code: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Sibling (brother or sister',
                type: 'SIBLING',
                code: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                code: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Sibling (brother or sister',
                type: 'SIBLING',
                code: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                code: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Spouse',
                type: 'SPOUSE',
                code: '01',
                mirrorType: 'SPOUSE'
            },
            {
                text: 'Stepchild (stepson or stepdaughter)',
                type: 'STEPCHILD',
                code: '17',
                mirrorType: 'STEPPARENT'
            },
            {
                text: 'Stepparent (stepfather or stepmother)',
                type: 'STEPPARENT',
                code: '16',
                mirrorType: 'STEPCHILD'
            },
            {
                text: 'Stepchild (stepson or step daughter)',
                type: 'STEPCHILD',
                code: '17',
                mirrorType: 'STEPPARENT'
            },
            {
                text: 'Child (son or daughter)',
                type: 'CHILD',
                code: '19',
                mirrorType: 'PARENT'
            },
            {
                text: 'Counter Appointed Guardian',
                type: 'COURT_APPOINTED_GUARDIAN',
                code: '31',
                mirrorType: 'WARD'
            },
            {
                text: 'Domestic partner',
                type: 'DOMESTIC_PARTNER',
                code: '53',
                mirrorType: 'DOMESTIC_PARTNER'
            },
            {
                text: 'First Cousin',
                type: 'FIRST_COUSIN',
                code: '08',
                mirrorType: 'FIRST_COUSIN'
            },
            {
                text: 'Former Spouse',
                type: 'FORMER_SPOUSE',
                code: '25',
                mirrorType: 'FORMER_SPOUSE'
            },
            {
                text: 'Grandchild (grandson or granddaughter)',
                type: 'GRANDCHILD',
                code: '05',
                mirrorType: 'GRANDPARENT'
            },
            {
                text: 'Grandparent (grandfather or grandmother)',
                type: '04',
                code: 'GRANDPARENT',
                mirrorType: 'GRANDCHILD'
            },
            {
                text: 'Mother-in-law or Father-in-law',
                type: 'MOTHER_FATHER_IN_LAW',
                code: '13',
                mirrorType: 'SON_DAUGHTER_IN_LAW'
            },
            {
                text: 'Nephew or Niece',
                type: 'NEPHEW_NIECE',
                code: '07',
                mirrorType: 'UNCLE_AUNT'
            },
            {
                text: 'Parent (father or mother)',
                type: 'PARENT',
                code: '03',
                mirrorType: 'CHILD'
            },
            {
                text: 'Self',
                type: 'SELF',
                code: '18',
                mirrorType: 'SELF'
            },
            {
                text: 'Sibling (brother or sister',
                type: 'SIBLING',
                code: '14',
                mirrorType: 'SIBLING'
            },
            {
                text: 'Son-in-law or Daughter-in-law',
                type: 'SON_DAUGHTER_IN_LAW',
                code: '11',
                mirrorType: 'MOTHER_FATHER_IN_LAW'
            },
            {
                text: 'Spouse',
                type: 'SPOUSE',
                code: '01',
                mirrorType: 'SPOUSE'
            },
            {
                text: 'Stepchild (stepson or stepdaughter)',
                type: 'STEPCHILD',
                code: '17',
                mirrorType: 'STEPPARENT'
            },
            {
                text: 'Stepparent (stepfather or stepmother)',
                type: 'STEPPARENT',
                code: '16',
                mirrorType: 'STEPCHILD'
            },
            {
                text: 'Uncle or Aunt',
                type: 'UNCLE_AUNT',
                code: '06',
                mirrorType: 'NEPHEW_NIECE'
            },
            {
                text: 'Ward',
                type: 'WARD',
                code: '15',
                mirrorType: 'COURT_APPOINTED_GUARDIAN'
            }
        ],
        loadedRelationsWholeList: [
            {
                text: 'Adopted son or daughter',
                code: '09'
            },
            {
                text: 'Annuitant',
                code: '60'
            },
            {
                text: 'Brother-in-law or sister-in-law',
                code: '12'
            },
            {
                text: 'Child (son or daughter)',
                code: '19'
            },
            {
                text: 'Collateral dependent',
                code: '38'
            },
            {
                text: 'Court Appointed or Live-In Guardian',
                code: '31'
            },
            {
                text: 'Dependent of a minor dependent',
                code: '24'
            },
            {
                text: 'Domestic partner',
                code: '53'
            },
            {
                text: 'First cousin',
                code: '08'
            },
            {
                text: 'Former spouse',
                code: '25'
            },
            {
                text: 'Foster child (foster son or foster daughter)',
                code: '10'
            },
            {
                text: 'Grandchild (grandson or granddaughter)',
                code: '05'
            },
            {
                text: 'Grandparent (grandfather or grandmother)',
                code: '04'
            },
            {
                text: 'Guardian',
                code: '26'
            },
            {
                text: 'Mother-in-law or father-in-law',
                code: '13'
            },
            {
                text: 'Nephew or niece',
                code: '07'
            },
            {
                text: 'Parent (father or mother)',
                code: '03'
            },
            {
                text: 'Self',
                code: '18'
            },
            {
                text: 'Sibling (brother or sister)',
                code: '14'
            },
            {
                text: 'Son-in-law or daughter-in-law',
                code: '11'
            },
            {
                text: 'Sponsored dependent',
                code: '23'
            },
            {
                text: 'Spouse',
                code: '01'
            },
            {
                text: 'Stepchild (stepson or stepdaughter)',
                code: '17'
            },
            {
                text: 'Stepparent (stepfather or stepmother)',
                code: '16'
            },
            {
                text: 'Trustee',
                code: 'D2'
            },
            {
                text: 'Uncle or aunt',
                code: '06'
            },
            {
                text: 'Unspecified relationship',
                code: 'G8'
            },
            {
                text: 'Unspecified relative',
                code: 'G9'
            },
            {
                text: 'Ward',
                code: '15'
            }
        ]

    };
} else {
    initialState = {
        dropdownOptions: [],
        loadedRelations: [],
        selfOption: {},
        loadedRelationsWholeList: []
    };
}

export default (state = initialState, action) => {
    const newState = {};

    const dropdownOptions = [{
        text: 'Relationship',
        value: 'default'
    }];

    action.bloodRelationships && action.bloodRelationships.map(br => {
        if(br.type === 'SELF') {
            newState.selfOption = br;
        }
        if(br.type !== 'SELF') {
            dropdownOptions.push({
                text: br.text,
                value: br.code
            });
        }
    });

    switch(action.type) {

    case bloodRelationshipsTypes.SET_RELATIONS:
        return {
            ...state,
            ...newState,
            dropdownOptions: dropdownOptions,
            loadedRelations: action.bloodRelationships
        };
    case bloodRelationshipsTypes.SET_RELATIONS_WHOLE_LIST:
        return {
            ...state,
            loadedRelationsWholeList: action.bloodRelationships
        };

    case bloodRelationshipsTypes.CLEAR_STATE:
        return initialState;

    default:
        return state;
    }
};
