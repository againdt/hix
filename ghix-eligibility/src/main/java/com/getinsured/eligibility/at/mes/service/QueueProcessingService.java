package com.getinsured.eligibility.at.mes.service;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATQueueInfoResponseDTO;

public interface QueueProcessingService {

	ATQueueInfoResponseDTO determineATProcessingType(ATSpanInfoRequestDTO atQueueInfoRequestDTO);

	ATSpanInfoRequestDTO populateATQueueInfoRequestDTO(AccountTransferRequestDTO request);

}
