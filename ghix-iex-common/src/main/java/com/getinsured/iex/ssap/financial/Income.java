package com.getinsured.iex.ssap.financial;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.financial.type.IncomeSubType;
import com.getinsured.iex.ssap.financial.type.IncomeType;
import com.google.gson.annotations.Expose;

/**
 * Describes income for {@link com.getinsured.iex.ssap.HouseholdMember}.
 * Used in SSAP Financial Flow.
 *
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public class Income {
  @Expose
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private IncomeType type = IncomeType.UNSPECIFIED;

  @Expose
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private IncomeSubType subType = IncomeSubType.UNSPECIFIED;

  @Expose
  private String sourceName;

  @Expose
  private long amount;

  @Expose
  private long tribalAmount;

  @Expose
  private int cyclesPerFrequency;

  @Expose
  private long relatedExpense;

  @Expose
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Frequency frequency = Frequency.UNSPECIFIED;

  /**
   * Returns type of the income.
   * @return {@link IncomeType}.
   */
  public IncomeType getType() {
    return type;
  }

  /**
   * Sets type of this income.
   * @param type {@link IncomeType}.
   */
  public void setType(IncomeType type) {
    this.type = type;
  }

  /**
   * Gets sub type for this income, only
   * returned if primary income is {@link IncomeType#OTHER}
   * @return {@link IncomeSubType} for this income.
   */
  public IncomeSubType getSubType() {
    return subType;
  }

  /**
   * Sets sub type for this income, only
   * applicable if primary income is {@link IncomeType#OTHER}
   * @param subType {@link IncomeSubType}.
   */
  public void setSubType(IncomeSubType subType) {
    this.subType = subType;
  }

  /**
   * Source name of this income, such as Employer Name
   * if income is {@link IncomeType#JOB}.
   * @return name of the source of income.
   */
  public String getSourceName() {
    return sourceName;
  }

  /**
   * Set source name for this income. For example if
   * income is {@link IncomeType#JOB} then this would be
   * name of the employer.
   * @param sourceName source name.
   */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /**
   * Amount of this income in cents.
   *
   * @return amount in cents.
   */
  public long getAmount() {
    return amount;
  }

  /**
   * Set amount in cents for this income.
   * @param amount amount in cents.
   */
  public void setAmount(long amount) {
    this.amount = amount;
  }

  /**
   * Holds amount that came from tribal source(s). This is
   * to figure out what part of the {@link #amount} came
   * from tribal income.
   * @return tribal income amount.
   */
  public long getTribalAmount() {
    return tribalAmount;
  }

  /**
   * Sets tribal income amount. Tribal income amount
   * cannot be greater than {@link #amount}.
   *
   * @param tribalAmount tribal income amount.
   */
  public void setTribalAmount(long tribalAmount) {
    this.tribalAmount = tribalAmount;
  }

  /**
   * Gets amount in cents for income related expenses.
   * @return expenses related to this income.
   */
  public long getRelatedExpense() {
    return relatedExpense;
  }

  /**
   * Sets amount in cents for income related expenses.
   * @param relatedExpense expenses related to this income.
   */
  public void setRelatedExpense(long relatedExpense) {
    this.relatedExpense = relatedExpense;
  }

  /**
   * Returns frequency for this income.
   * @return {@link Frequency} for this income.
   */
  public Frequency getFrequency() {
    return frequency;
  }

  /**
   * Sets frequency for this income.
   * @param frequency {@link Frequency} for this income
   */
  public void setFrequency(Frequency frequency) {
    this.frequency = frequency;
  }


  /**
   * Returns how many hours per day or days per week
   * person receives this income.
   *
   * Currently applicable for
   * <ul>
   *   <li>{@link Frequency#DAILY}</li>
   *   <li>{@link Frequency#HOURLY}</li>
   * </ul>
   * @return cycles for current frequency.
   */
  public int getCyclesPerFrequency() {
    return cyclesPerFrequency;
  }

  /**
   * Sets cycles per for current frequency for this income.
   * Currently applicable for
   * <ul>
   *   <li>{@link Frequency#DAILY}</li>
   *   <li>{@link Frequency#HOURLY}</li>
   * </ul>
   * @param cyclesPerFrequency cycles for current frequency.
   */
  public void setCyclesPerFrequency(int cyclesPerFrequency) {
    this.cyclesPerFrequency = cyclesPerFrequency;
  }
}
