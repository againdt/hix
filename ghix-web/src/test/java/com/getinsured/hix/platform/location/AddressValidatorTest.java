package com.getinsured.hix.platform.location;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.AddressValidatorResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.location.service.AddressValidatorService;
import com.getinsured.hix.platform.location.service.jpa.AddressValidatorSmartyStreet;
import com.google.gson.Gson;

/**
 * @author polimetla_b
 * @since Oct-31-2012
 */
public class AddressValidatorTest extends BaseTest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AddressValidatorTest.class);

	//ApplicationContext context = null;
	AddressValidatorService av = null;

	public AddressValidatorTest() {
//		System.setProperty("GHIX_HOME", "C:/workspace_ghix/");
//		context = new ClassPathXmlApplicationContext("/applicationContext.xml");
		av = (AddressValidatorService) context.getBean("addressValidator");
	}

	@Test
	public void testValidateSingleAddress() {

		LOGGER.info("============ testValidateSingleAddress Begin =============");
		Location ad = new Location();
		// ad.setAddress1("bayshore road, ca, 94303");
		// ad.setStreetName("second st, hackensack, nj");
		// ad.setAddress1("1 main st, hackensack, nj");

		ad.setAddress1("2595 E bayshore road");
		ad.setCity("palo alto");
		ad.setState("CA");
		ad.setZip("94303");

		AddressValidatorResponse response = av.validateAddress(ad);
		List<Location> validList = response.getList();

		LOGGER.info("status==>" + response.getStatus());
		LOGGER.info("validator==>" + response.getValidatorName());
		LOGGER.info("duration==>" + response.getExecDuration());
		LOGGER.info("1st record==>" + response.getList().get(0).toString());
		Gson gson = new Gson();
		String json = gson.toJson(response);
		LOGGER.info(json);

		assertTrue(validList.size() > 0);

		assertTrue(validList.get(0).getCity().equalsIgnoreCase("palo alto"));

		LOGGER.info("============ testValidateSingleAddress End =============");
	}

	@Test
	public void testValidateSingleAddressEmpty() {

		LOGGER.info("============ testValidateSingleAddress Begin =============");
		Location ad = new Location();
		// ad.setAddress1("bayshore road, ca, 94303");
		// ad.setStreetName("second st, hackensack, nj");
		// ad.setAddress1("1 main st, hackensack, nj");
		
		//Setting address 1 as empty.
		ad.setCity("palo alto");
		ad.setState("CA");
		ad.setZip("94303");

		AddressValidatorResponse response = av.validateAddress(ad);
		List<Location> validList = response.getList();

		LOGGER.info("status==>" + response.getStatus());
		
		Gson gson = new Gson();
		String json = gson.toJson(response);
		LOGGER.info(json);

		
		assertTrue(validList == null);
		
		LOGGER.info("============ testValidateSingleAddress End =============");
	}

	
	//@Test
	public void testValidateMultipleAddresses() {

		Location ad = new Location();
		// ad.setStreetName("20990 valley green drive cupertino ca");
		//ad.setAddress1("second st, hackensack, nj");
		ad.setAddress1(" ");
		ad.setZip("95014");
		AddressValidatorResponse response = av.validateAddress(ad);
		List<Location> validList = response.getList();
		assertTrue(validList.size() > 1);

		LOGGER.info("status==>" + response.getStatus());
		LOGGER.info("validator==>" + response.getValidatorName());
		LOGGER.info("duration==>" + response.getExecDuration());
		LOGGER.info("1st record==>" + response.getList().get(0).toString());
		LOGGER.info("2nd record==>" + response.getList().get(0).toString());

		// Gson gson = new Gson();
		// String json = gson.toJson(response);
		// LOGGER.info(json);
	}

	@Test
	public void testSmartyStreetDataParser() throws Exception {

		AddressValidatorSmartyStreet psp = new AddressValidatorSmartyStreet();

		// Multiple return value test

		String multipleData = "[{\"input_index\":0,\"candidate_index\":0,\"addressee\":\"Driftwood\",\"delivery_line_1\":\"30 2nd St\",\"last_line\":\"Hackensack NJ 07601-2028\",\"delivery_point_barcode\":\"076012028996\",\"components\":{\"primary_number\":\"30\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2028\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"6\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C043\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Residential\",\"latitude\":40.8825,\"longitude\":-74.05425,\"precision\":\"Zip7\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}},{\"input_index\":0,\"candidate_index\":1,\"addressee\":\"Hackensack Medical Center\",\"delivery_line_1\":\"60 2nd St\",\"last_line\":\"Hackensack NJ 07601-1991\",\"delivery_point_barcode\":\"076011991600\",\"components\":{\"primary_number\":\"60\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"1991\",\"delivery_point\":\"60\",\"delivery_point_check_digit\":\"0\"},\"metadata\":{\"record_type\":\"F\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C043\",\"congressional_district\":\"09\",\"rdi\":\"Commercial\",\"latitude\":40.88307,\"longitude\":-74.05497,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"Y\",\"dpv_footnotes\":\"AABB\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false}},{\"input_index\":0,\"candidate_index\":2,\"delivery_line_1\":\"60 2nd St\",\"last_line\":\"Hackensack NJ 07601-2050\",\"delivery_point_barcode\":\"076012050991\",\"components\":{\"primary_number\":\"60\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2050\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"1\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C043\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Commercial\",\"latitude\":40.88307,\"longitude\":-74.05497,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}},{\"input_index\":0,\"candidate_index\":3,\"addressee\":\"Hack Med Cte\",\"delivery_line_1\":\"60 2nd St Ste 1\",\"last_line\":\"Hackensack NJ 07601-2000\",\"delivery_point_barcode\":\"076012000013\",\"components\":{\"primary_number\":\"60\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"secondary_number\":\"1\",\"secondary_designator\":\"Ste\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2000\",\"delivery_point\":\"01\",\"delivery_point_check_digit\":\"3\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C043\",\"congressional_district\":\"09\",\"rdi\":\"Commercial\",\"latitude\":40.88307,\"longitude\":-74.05497,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"Y\",\"dpv_footnotes\":\"AABB\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false}},{\"input_index\":0,\"candidate_index\":4,\"delivery_line_1\":\"100 2nd St\",\"last_line\":\"Hackensack NJ 07601-2155\",\"delivery_point_barcode\":\"076012155995\",\"components\":{\"primary_number\":\"100\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2155\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"5\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C043\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Residential\",\"latitude\":40.88492,\"longitude\":-74.05391,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}},{\"input_index\":0,\"candidate_index\":5,\"addressee\":\"Padovano Adult Education\",\"delivery_line_1\":\"191 2nd St\",\"last_line\":\"Hackensack NJ 07601-2458\",\"delivery_point_barcode\":\"076012458917\",\"components\":{\"primary_number\":\"191\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2458\",\"delivery_point\":\"91\",\"delivery_point_check_digit\":\"7\"},\"metadata\":{\"record_type\":\"F\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C026\",\"congressional_district\":\"09\",\"rdi\":\"Commercial\",\"latitude\":40.88837,\"longitude\":-74.05192,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"Y\",\"dpv_footnotes\":\"AABB\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false}},{\"input_index\":0,\"candidate_index\":6,\"addressee\":\"Hackensack Board of Ed\",\"delivery_line_1\":\"191 2nd St\",\"last_line\":\"Hackensack NJ 07601-5577\",\"delivery_point_barcode\":\"076015577912\",\"components\":{\"primary_number\":\"191\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"5577\",\"delivery_point\":\"91\",\"delivery_point_check_digit\":\"2\"},\"metadata\":{\"record_type\":\"F\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C026\",\"congressional_district\":\"09\",\"rdi\":\"Commercial\",\"latitude\":40.89013,\"longitude\":-74.04116,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"Y\",\"dpv_footnotes\":\"AABB\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false}},{\"input_index\":0,\"candidate_index\":7,\"delivery_line_1\":\"215 2nd St\",\"last_line\":\"Hackensack NJ 07601-2477\",\"delivery_point_barcode\":\"076012477998\",\"components\":{\"primary_number\":\"215\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2477\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"8\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C026\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Residential\",\"latitude\":40.88837,\"longitude\":-74.05192,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}},{\"input_index\":0,\"candidate_index\":8,\"delivery_line_1\":\"222 2nd St\",\"last_line\":\"Hackensack NJ 07601-2483\",\"delivery_point_barcode\":\"076012483991\",\"components\":{\"primary_number\":\"222\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2483\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"1\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C026\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Residential\",\"latitude\":40.88837,\"longitude\":-74.05192,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}},{\"input_index\":0,\"candidate_index\":9,\"delivery_line_1\":\"256 2nd St\",\"last_line\":\"Hackensack NJ 07601-2757\",\"delivery_point_barcode\":\"076012757997\",\"components\":{\"primary_number\":\"256\",\"street_name\":\"2nd\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"2757\",\"delivery_point\":\"99\",\"delivery_point_check_digit\":\"7\"},\"metadata\":{\"record_type\":\"H\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C050\",\"congressional_district\":\"09\",\"building_default_indicator\":\"Y\",\"rdi\":\"Residential\",\"latitude\":40.89003,\"longitude\":-74.05097,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"D\",\"dpv_footnotes\":\"AAN1\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"Y\",\"ews_match\":false,\"footnotes\":\"H#\"}}]";
		List<Location> list = psp.parseSmartyStreetData(multipleData);

		if (null != list) {
			Gson gson = new Gson();
			String json = gson.toJson(list);
			LOGGER.info("Size ==>" + list.size());
			LOGGER.info("Result ==>" + json);
		} else
			fail("Failed to parse the given data");

		// Single return value test
		multipleData = "[{\"input_index\":0,\"candidate_index\":0,\"delivery_line_1\":\"1 Main St\",\"last_line\":\"Hackensack NJ 07601-7011\",\"delivery_point_barcode\":\"076017011016\",\"components\":{\"primary_number\":\"1\",\"street_name\":\"Main\",\"street_suffix\":\"St\",\"city_name\":\"Hackensack\",\"state_abbreviation\":\"NJ\",\"zipcode\":\"07601\",\"plus4_code\":\"7011\",\"delivery_point\":\"01\",\"delivery_point_check_digit\":\"6\"},\"metadata\":{\"record_type\":\"S\",\"county_fips\":\"34003\",\"county_name\":\"Bergen\",\"carrier_route\":\"C004\",\"congressional_district\":\"09\",\"rdi\":\"Residential\",\"latitude\":40.87837,\"longitude\":-74.0447,\"precision\":\"Zip9\"},\"analysis\":{\"dpv_match_code\":\"Y\",\"dpv_footnotes\":\"AABB\",\"dpv_cmra\":\"N\",\"dpv_vacant\":\"N\",\"active\":\"N\",\"ews_match\":false,\"footnotes\":\"N#\"}}]";
		list = psp.parseSmartyStreetData(multipleData);

		if (null != list) {
			Gson gson = new Gson();
			String json = gson.toJson(list);
			LOGGER.info("Size ==>" + list.size());
			LOGGER.info("Result ==>" + json);
		} else
			fail("Failed to parse the given data");

	}

}
