package com.getinsured.iex.hub.hubc.servicehandler;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.HubConnectivityResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.ObjectFactory;
import com.getinsured.iex.hub.hubc.wrapper.HubConnectivityResponseTypeWrapper;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;

public class HubConnectivityServiceHandler extends AbstractServiceHandler {
	
private static final Logger LOGGER = LoggerFactory.getLogger(HubConnectivityServiceHandler.class);
	
	private static ObjectFactory factory;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}	
	
	@Override
	public Object getRequestpayLoad() {
		return factory.createHubConnectivityRequest("");
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = null;
		try{
			@SuppressWarnings("unchecked")
			JAXBElement<HubConnectivityResponseType> resp= (JAXBElement<HubConnectivityResponseType>)responseObject;
			HubConnectivityResponseType responsePayload = resp.getValue();
			HubConnectivityResponseTypeWrapper response = new HubConnectivityResponseTypeWrapper(responsePayload);

			if(response != null){
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				if(response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseMetadataType().getResponseCode());
				}
				this.requestStatus = RequestStatus.SUCCESS;
			}
			
			LOGGER.debug("Response message " + message);
			
		}catch(ClassCastException ce){
			message = "Class in response: " + responseObject.getClass().getName()+" Expecting:"+ HubConnectivityResponseType.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H74";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
