package com.getinsured.hix.entity.assister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.AssisterLanguages;

/**
 * Spring-JPA repository that encapsulates methods that interact with the AssisterLanguages
 * table
 */
public interface IAssisterLanguagesRepository extends JpaRepository<AssisterLanguages, Integer> {
	/**
	 * Fetches the AssisterLanguages record against the given Assister
	 * 
	 * @param assister
	 *            the Assister object
	 * @return the AssisterLanguages record against the Assister
	 */
	@Query("FROM AssisterLanguages s where s.assister.id = :id")
	AssisterLanguages findByAssisterId(@Param("id") int id);
}
