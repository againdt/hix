package com.getinsured.eligibility.ssap.integration.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.timeshift.TimeShifterUtil;

@SuppressWarnings("serial")
public final class AccountTransferUtil {
	
	private AccountTransferUtil(){}
	
	private static Logger lOGGER = Logger.getLogger(AccountTransferUtil.class);
	public static final String AT_VERSION_TXT = "2.4";
	private static final int TRANFER_ID_LENGTH = 17;//because states schematron expects only 20 characters.
	//private static final String DATE_FORMAT_FROM_JSON = "MMM dd, yyyy HH:mm:ss";
	private static final String DATE_FORMAT_FROM_JSON = "MMM dd, yyyy";
	public static final com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.ObjectFactory mainFactory = new com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.ObjectFactory();
	public static final  com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ObjectFactory hixCoreFactory =new com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ObjectFactory insuranceApplicationObjFactory = new com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.ObjectFactory hixPMFactory  = new com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ObjectFactory hixTypeFactory  = new com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ObjectFactory niemCoreFactory = new com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.ObjectFactory basicFactory  = new com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.usps_states._2.ObjectFactory statesObjFactory = new com.getinsured.iex.erp.gov.niem.niem.usps_states._2.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.structures._2.ObjectFactory niemStructFactory= new com.getinsured.iex.erp.gov.niem.niem.structures._2.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.fips_6_4._2.ObjectFactory niemFipsFactory= new com.getinsured.iex.erp.gov.niem.niem.fips_6_4._2.ObjectFactory();
	public static final com.getinsured.iex.erp.gov.niem.niem.iso_3166._2.ObjectFactory iSOFactory = new com.getinsured.iex.erp.gov.niem.niem.iso_3166._2.ObjectFactory();

    private static final Map<String, String> SSAP_TO_STATE_FREQUENCY_MAPPING; 
    private static final Map<String, IncomeCategoryCodeSimpleType> SSAP_TO_STATE_INCOME_TYPE_MAPPING;
    private static final Map<String, String> SSAP_TO_STATE_IMMGR_DOUCMENT_MAPPING; 
    static {
    	SSAP_TO_STATE_FREQUENCY_MAPPING = new HashMap<String, String>() {
            {
                put("Weekly", "Weekly");
                put("Monthly", "Monthly");//BIMONTHLY
                put("Yearly", "Annually");
                put("EveryTwoWeeks", "BiWeekly");
                put("TwiceAMonth", "SemiMonthly");
                put("Hourly", "Hourly");
                put("Daily", "Daily");
                put("OneTimeOnly", "Once");
                put("SemiAnnually", "SemiAnnually");
                put("13xPerYear", "13xPerYear");
                put("11xPerYear", "11xPerYear");
                put("10xPerYear", "10xPerYear");
                put("Quarterly", "Quarterly");
                put("BIMONTHLY", "SemiMonthly");
                put("BIWEEKLY", "BiWeekly");
                put("DAILY", "Daily");
                put("HOURLY", "Hourly");
                put("MONTHLY", "Monthly");
                put("ONCE", "Once");
                put("QUARTERLY", "Quarterly");
                put("WEEKLY", "Weekly");
                put("YEARLY", "Annually");
            }
        };
	    SSAP_TO_STATE_IMMGR_DOUCMENT_MAPPING = new HashMap<String, String>() {
	        {
	            put("I327", "I327");
	            put("I551", "I551");
	            put("I571", "I571");
	            put("I766", "I766");
	            put("I797", "I797");
	            put("MacReadI551", "MachineReadableVisa");
	            put("TempI551", "TemporaryI551Stamp");
	            put("I94", "I94");
	            put("I94UnexpForeignPassport", "UnexpiredForeignPassport");
	            put("UnexpForeignPassport", "13xPerYear");
	            put("I20", "I20");
	            put("DS2019", "DS2019");
	            put("NaturalizationCertificate", "NaturalizationCertificate");
	            put("CitizenshipCertificate", "CitizenshipCertificate");
	        }
	    };
	    SSAP_TO_STATE_INCOME_TYPE_MAPPING = new HashMap<String, IncomeCategoryCodeSimpleType>() {
	        
	        {
	            put("Canceled debts", IncomeCategoryCodeSimpleType.CANCELED_DEBT);
	            put("Court Awards", IncomeCategoryCodeSimpleType.COURT_AWARD);
	            put("Jury duty pay", IncomeCategoryCodeSimpleType.JURY_DUTY);
	           
	        }
	    };

    }
    
    public static String getTransferID() {
		return java.util.UUID.randomUUID().toString().substring(0, TRANFER_ID_LENGTH);
	}

	/**
	 * creates XMLGregorianCalendar from a String
	 * 
	 * @param dateString
	 * @return
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String dateString) {
		XMLGregorianCalendar gregCal=null;
		if (StringUtils.isBlank(dateString)) { 
			return null;
		}else{
			try{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_FROM_JSON);
			Date date = simpleDateFormat.parse(dateString);
			GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
			gregorianCalendar.setTime(date);
			lOGGER.debug(dateString + " date converted to String :"+ gregorianCalendar.getTime().toString());
			gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH)+1, gregorianCalendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
			return gregCal;
			}catch(ParseException | DatatypeConfigurationException e){
				lOGGER.error("error while converting date from value "+ dateString , e);
				return null;
			}
		}
	}
	
	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) {
		XMLGregorianCalendar gregCal=null;
		if (date == null) { 
			return null;
		}else{
			try{
				GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
				gregorianCalendar.setTimeInMillis(date.getTime());
				lOGGER.debug(date + " date converted to String :"+ gregorianCalendar.getTime().toString());
				gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH)+1, gregorianCalendar.get(Calendar.DAY_OF_MONTH), gregorianCalendar.get(Calendar.HOUR), gregorianCalendar.get(Calendar.MINUTE), gregorianCalendar.get(Calendar.SECOND), gregorianCalendar.get(Calendar.MILLISECOND), DatatypeConstants.FIELD_UNDEFINED);
				return gregCal;
			}catch(DatatypeConfigurationException e){
				lOGGER.error("error while converting date from value "+ date , e);
				return null;
			}
		}
	}
	
	public static XMLGregorianCalendar getSystemDateYear() {
		XMLGregorianCalendar gregCal=null;
		GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		//gregorianCalendar.setGregorianChange(new TSDate());
		gregorianCalendar.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		try {
			gregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR), DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			lOGGER.error(
					" Exception while creating XMLGregorianCalendar from new TSDate() ", e);
		}
		return gregCal;
	}
	
	public static XMLGregorianCalendar getSystemDate() {
		XMLGregorianCalendar gregCal=null;
		GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		//gregorianCalendar.setGregorianChange(new TSDate());
		gregorianCalendar.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		try {
			gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH)+1, gregorianCalendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
			//gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		} catch (DatatypeConfigurationException e) {
			lOGGER.error("Exception while creating XMLGregorianCalendar from new TSDate() ", e);
		}
		return gregCal;
	}
	
	public static XMLGregorianCalendar getSystemDateTime() {
		XMLGregorianCalendar gregCal=null;
		GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		//gregorianCalendar.setGregorianChange(new TSDate());
		gregorianCalendar.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		try {
			gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		} catch (DatatypeConfigurationException e) {
			lOGGER.error(
					" Exception while creating XMLGregorianCalendar from new TSDate() ", e);
		}
		return gregCal;
	}
	
	public static boolean checkBoolean(Object value){
		if (!(value instanceof Boolean)){
			return false;
		}
		return (boolean) value;
	}

	
    public static String getFrequencyType(String ssapFrequencyString) {
        return SSAP_TO_STATE_FREQUENCY_MAPPING.get(ssapFrequencyString);
    }
    public static String getImmigrationDocument(String document) {
        return SSAP_TO_STATE_IMMGR_DOUCMENT_MAPPING.get(document);
    }
	
    public static BigDecimal getAmount(Object jsonObj){
    	
		if (jsonObj == null || !NumberUtils.isNumber(jsonObj.toString())) {
			return new BigDecimal("0.0");
		} else {
			return new BigDecimal(jsonObj.toString());
		}
	}
    
    public static com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean addBoolean(Object jo){
    	
    	com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean booleanElem = AccountTransferUtil.basicFactory.createBoolean();
		java.lang.Boolean flag = AccountTransferUtil.checkBoolean( jo);
		booleanElem.setValue(flag);
		return booleanElem;
    }
    
    public static com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String createString(String str){
    	com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String returnString = AccountTransferUtil.basicFactory.createString();
    	returnString.setValue(str);
    	return returnString; 
	}
    
    public static PersonNameTextType createPersonNameTextType(String text){
    	PersonNameTextType personNameTextType = AccountTransferUtil.niemCoreFactory.createPersonNameTextType();
    	personNameTextType.setValue(text);
    	return personNameTextType;
    }
    public static com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType createUSStateCodeType(String stateCode){
    	com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType stateCodeType = AccountTransferUtil.statesObjFactory.createUSStateCodeType();
    	stateCodeType.setValue(USStateCodeSimpleType.fromValue(stateCode));
    	return stateCodeType;
    }
    
    public static com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType createNumericType(Integer number ){ 
    	com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType numeric  =AccountTransferUtil.niemCoreFactory.createNumericType();
    	numeric.setValue(new BigDecimal(number));
    	return numeric;
	}
    
    public static TextType createTextType(String str){
    	TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue(str);
		return textType;
    }
    public static IncomeCategoryCodeSimpleType getIncomeType(String incomeType) {
        return SSAP_TO_STATE_INCOME_TYPE_MAPPING.get(incomeType);
    }
   
}

