
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for Quarters of Coverage, always returned by SSA as exactly 356 alphabetical characters that encode\ coverage information from 1937 to 2025. 
 *                 See Business Service Document for processing details.
 *             
 * 
 * <p>Java class for SSAQuartersOfCoverageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSAQuartersOfCoverageType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}LifeTimeQuarterQuantity"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}QualifyingYearAndQuarter" maxOccurs="89" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSAQuartersOfCoverageType", propOrder = {
    "lifeTimeQuarterQuantity",
    "qualifyingYearAndQuarter"
})
public class SSAQuartersOfCoverageType
    extends ComplexObjectType
{

    @XmlElement(name = "LifeTimeQuarterQuantity", required = true)
    protected BigInteger lifeTimeQuarterQuantity;
    @XmlElement(name = "QualifyingYearAndQuarter")
    protected List<QualifyingYearAndQuarterType> qualifyingYearAndQuarter;

    /**
     * Gets the value of the lifeTimeQuarterQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLifeTimeQuarterQuantity() {
        return lifeTimeQuarterQuantity;
    }

    /**
     * Sets the value of the lifeTimeQuarterQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLifeTimeQuarterQuantity(BigInteger value) {
        this.lifeTimeQuarterQuantity = value;
    }

    /**
     * Gets the value of the qualifyingYearAndQuarter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qualifyingYearAndQuarter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQualifyingYearAndQuarter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QualifyingYearAndQuarterType }
     * 
     * 
     */
    public List<QualifyingYearAndQuarterType> getQualifyingYearAndQuarter() {
        if (qualifyingYearAndQuarter == null) {
            qualifyingYearAndQuarter = new ArrayList<QualifyingYearAndQuarterType>();
        }
        return this.qualifyingYearAndQuarter;
    }

}
