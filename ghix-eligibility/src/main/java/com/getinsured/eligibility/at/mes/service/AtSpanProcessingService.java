package com.getinsured.eligibility.at.mes.service;

import java.util.Map;

import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author deorukhkar_a
 *
 */
public interface AtSpanProcessingService {

	/**
	 * This method will be called from mes processor.
	 * Steps:
	 * 1. If current span == 1, DQ all spans
	 * 2. Run Queuing Algorithm and Find span processing type i.e realtime or queued or closed
	 * 3. Create span in any case and update application status if required.
	 * @param referralResponseMap
	 * @return
	 */
	String processCurrentSpan(Map<String,Object> referralResponseMap);
	
	/**
	 * This method will be called once OE/QE/SEP spring integration flow is completed,
	 * if the process was triggered from batch job. It will update at_span_info with processed_status
	 * as SUCESS or FAILURE
	 * @param status
	 * @param errorMessage
	 * @param ssapApplicationId
	 */
	void updateSpanStatus(String status,String errorMessage,long ssapApplicationId);
	
	void closePreviousERApplications(SsapApplication application);
	
	long createAtSpanInfo(ATSpanInfoRequestDTO atSpanInfoRequestDTO);
	
	AccountTransferSpanInfo updateCurrentAndFetchNextSpan(ATSpanInfoRequestDTO atSpanInfoRequestDTO);
	
}
