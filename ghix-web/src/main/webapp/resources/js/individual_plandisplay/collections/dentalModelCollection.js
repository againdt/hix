(function (collections,pagination,model) {
	collections.dentalModelPlans = Backbone.Collection.extend({
		model : model,
		url : 'getDentalModelPlans?'+(Math.random()*10),
		
		/**
		 * @param resp the response returned by the server
		 * @returns (Array) dental plans
		 */
		parse : function (resp) {
			var dentalPlans = resp;
			return dentalPlans;
		}
	});
	
	_.extend(collections.dentalModelPlans.prototype, pagination);
})(App.collections,App.backbone.Pagination, App.models.dentalModelPlan);

