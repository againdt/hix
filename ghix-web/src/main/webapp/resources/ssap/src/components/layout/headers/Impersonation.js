import React from 'react';

/**
 * Displays impersonation 'lip' that is below the header.
 *
 * @param props impersonation details
 * @returns {*} HTML content.
 */
const Impersonation = props => {
    const { impersonationDetails } = props;

    return (
        <div id="impersonationSubHeader" className="usa-width-one-whole header">
            <div>
                <h4>
                    Viewing Individual Account ({impersonationDetails.name})
                    <span>
                        <a href={impersonationDetails.switchBackLink} className="ds-c-button usa-button btn-primary">My Account</a>
                    </span>
                </h4>
            </div>
        </div>
    );
};

export default Impersonation;
