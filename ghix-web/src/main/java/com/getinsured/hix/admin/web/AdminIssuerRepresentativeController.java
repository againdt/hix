package com.getinsured.hix.admin.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerRepresentativeService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.IssuerService.RepStatus;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class AdminIssuerRepresentativeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminIssuerRepresentativeController.class);
	
	
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private LocationService locationService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerRepresentativeService issuerRepresentativeService;
	

	private static final String ROLE_NAME ="activeRoleName";
	private final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
	private final Boolean isEmailActivation = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION).toLowerCase());

	
		/*  ##### controller for send account activation link   ##### */
		@GiAudit(transactionName = "Send Activation Link For Issuer Representative", eventType = EventTypeEnum.PM_SEND_ACTIVATION_LINK, eventName = EventNameEnum.PII_WRITE)
		@RequestMapping(value = "/admin/issuer/representative/sendActivationlink/{encIssuerId}", method = { RequestMethod.GET })
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String sendActivationLink(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser accountUser, 
				Model model, RedirectAttributes redirectAttr, 
				@RequestParam(value = PlanMgmtConstants.REP_ID, required = false) String repId, 
				@PathVariable("encIssuerId") String encIssuerId) {
			
			String decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(repId);

			IssuerRepresentative issuerRepresentative = iIssuerRepresentativeRepository.findById(Integer.parseInt(decryptedIssuerRepId));
			accountUser.setFirstName(issuerRepresentative.getFirstName());
			accountUser.setLastName(issuerRepresentative.getLastName());
			accountUser.setEmail(issuerRepresentative.getEmail());
			accountUser.setPhone(issuerRepresentative.getPhone());
			
			// if role is null in issur_representative table then set ISSUER_REPRESENTATIVE as default role
			String role = ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString();
			if(StringUtils.isNotBlank(issuerRepresentative.getRole())){
				role = issuerRepresentative.getRole();
			}

			try {
				issuerService.generateActivationLink(Integer.parseInt(decryptedIssuerRepId), accountUser, role);
				redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to " + accountUser.getEmail());
			} catch (Exception e) {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE));
				LOGGER.error("Exception in sendActivationLink" + e.getMessage());
				redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sending process failed... Please take appropriate actions. -  "
						+ e.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SEND_ACTIVATION_LINK_EXCEPTION, e, null, Severity.HIGH);
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encIssuerId;
		}

		
		/*  ##### controller for manage issuer representatives   ##### */
		@RequestMapping(value = "/admin/issuer/representative/manage/{encIssuerId}", method = {RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String manageRepresentative(Model model,  @RequestParam(value = "action", required = false) String action, @RequestParam(value = PlanMgmtConstants.REP_ID, required = false) String encIssuerRepId, 
				@PathVariable("encIssuerId") String encIssuerId, HttpServletRequest manageRepRequest, RedirectAttributes redirectAttrs) {
			String decryptedIssuerId = null;
			Integer issuerId = null;
			boolean updateRepStatus = false;
			boolean checkforSuspension = false;
			try{
				decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
				issuerId = Integer.parseInt(decryptedIssuerId);
				AccountUser currentAccountUser=userService.getLoggedInUser();
				String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				
				if (StringUtils.isNotBlank(action) && (action.equalsIgnoreCase(RepStatus.ACTIVE.toString()) || action.equalsIgnoreCase(RepStatus.INACTIVE.toString()))) {
					// set Activate or Suspend flag for issuer representative
					try {
						Integer issureRepId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerRepId));
						if (null != issureRepId && PlanMgmtConstants.ZERO < issureRepId) {
							checkforSuspension = true;
							updateRepStatus = issuerService.updateRepStatus(issureRepId, action);
						}
					} catch (InvalidUserException e) {
						LOGGER.error("Invalid User : Exception in : " + this.getClass() + ", Exception : " + e.getMessage());
					}
		
					if(updateRepStatus){
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.EMPTY_STRING);
						return PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encIssuerId;
					}else{
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.TRUE_STRING);
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR_MSG, "Unable to "+ (RepStatus.ACTIVE.toString().equalsIgnoreCase(action) ? RepStatus.ACTIVE.toString() : "suspend") +" issuer representative. <br> Please check permission and try again.");
						return PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encIssuerId;
					}
				}
		
				manageRepRequest.setAttribute(PlanMgmtConstants.DEFAULT_SORT, PlanMgmtConstants.UPDATED);
				List<Map<String, String>> repArrList = new ArrayList<Map<String, String>>();
				int repCount = issuerService.getIssuerRepresentataivesCount(issuerId);
				repArrList = issuerService.getIssuerRepresentatives(manageRepRequest, issuerId, false, true);
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerService.getIssuerById(issuerId));
				model.addAttribute("issuerReps", repArrList);
				model.addAttribute(PlanMgmtConstants.RESULT_SIZE, repCount);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute("displayDtFmt", GhixConstants.DISPLAY_DATE_FORMAT);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute("isEmailActivation", isEmailActivation);
				model.addAttribute(PlanMgmtConstants.SORT_ORDER, manageRepRequest.getAttribute(PlanMgmtConstants.SORT_ORDER));
				model.addAttribute(PlanMgmtConstants.SORT_BY, manageRepRequest.getParameter(PlanMgmtConstants.SORT_BY));
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
				redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.EMPTY_STRING);
				
			}catch(Exception ex){
				if(!updateRepStatus && checkforSuspension){
					redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR, PlanMgmtConstants.TRUE_STRING);
					redirectAttrs.addFlashAttribute(PlanMgmtConstants.ERROR_MSG, "Unable to "+ (RepStatus.ACTIVE.toString().equalsIgnoreCase(action) ? RepStatus.ACTIVE.toString() : "suspend") +" issuer representative. <br> Please check permission and try again.");
					return PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encIssuerId;
				}else{
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.MANAGE_REPRESENTATIVE_EXCEPTION.getCode(), ex, null, Severity.LOW);
				}
			}
			return "admin/issuer/representative/manage";
		}

		
		/*  ##### controller for add issuer representative   ##### */
		@RequestMapping(value = "/admin/issuer/representative/add/{encIssuerId}", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String addRepresentative(@PathVariable("encIssuerId") String encIssuerId, Model model) {
			 String decryptedIssuerId = null;
			try{
				decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
				AccountUser currentAccountUser=userService.getLoggedInUser();
				String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
				model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ADD_REPRESENTATIVE_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			return "admin/issuer/representative/add";
		}
		
		
		/*  ##### controller for view issuer representative details   ##### */
		@RequestMapping(value = "/admin/issuer/representative/details/{encRepId}", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String showRepresentativeDetails(Model model, @PathVariable("encRepId") String encRepId, HttpServletRequest viewRepRequest) throws InvalidUserException {
			 String isFromUser=null;
			 String decryptedIssuerRepId = null;
			 Integer repId = null;
			 try{
				decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encRepId);
				repId = Integer.parseInt(decryptedIssuerRepId);
				 AccountUser currentAccountUser=userService.getLoggedInUser();
					String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
					model.addAttribute(ROLE_NAME, activeRoleName);
				
				String userStatus = null, issuerRepStatus = null;

				if (repId != null) {
					IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findById(repId);
					 /*      As per the comment in JIRA HIX-23920 code changes done*/
					if(representativeObj.getUserRecord()!=null){
			 	 	 	isFromUser="USER";
			 	 	 	AccountUser user=userService.findById(representativeObj.getUserRecord().getId());
			 	 	 	userStatus = user.getStatus();
			 	 	 	model.addAttribute("USER_OBJ", user);
			 	 	 	model.addAttribute("repUser", representativeObj);
			 	 	 	model.addAttribute("isFromUser", isFromUser);
			 	 	}else{
			 	 	 		model.addAttribute("repUser", representativeObj);
			 	 	}
					issuerRepStatus = representativeObj.getStatus();
					model.addAttribute("displayStatus", issuerService.getIssuerRepStatus(userStatus, issuerRepStatus));
			 	 	model.addAttribute("repId", repId);
			 	 	/*	Commented while code Merge for HIX-23920	*/
					/*model.addAttribute(PlanMgmtConstants.ISSUER_REP_OBJ, representativeObj);*/
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, representativeObj.getIssuer());
					model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());
				}
				model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			 }catch(Exception ex){
				 throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ex, null, Severity.LOW);
			 }
			return "admin/issuer/representative/details";
		}
		

		/*  ##### controller for edit issuer representative details   ##### */
		@RequestMapping(value = "/admin/issuer/representative/edit/{encRepId}", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String editRepresentative(@PathVariable("encRepId") String encRepId, Model model, HttpServletRequest manageRepRequest) {
			String decryptedIssuerRepId = null;
			try{
				decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encRepId);
				AccountUser currentAccountUser=userService.getLoggedInUser();
				String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findById(Integer.parseInt(decryptedIssuerRepId));
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, representativeObj.getIssuer());
				model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
				model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				if(representativeObj.getUserRecord()!=null) {
					model.addAttribute("repUser", representativeObj.getUserRecord());
				}else {
					model.addAttribute("repUser", representativeObj);
				}
				if (representativeObj.getUserRecord()!=null && representativeObj.getUserRecord().getPhone() != null && representativeObj.getUserRecord().getPhone().length() >= PlanMgmtConstants.TEN) {
					model.addAttribute(PlanMgmtConstants.PHONE1, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE2, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE3, representativeObj.getUserRecord().getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
				} else if(representativeObj.getPhone() != null && representativeObj.getPhone().length() >= PlanMgmtConstants.TEN){
					model.addAttribute(PlanMgmtConstants.PHONE1, representativeObj.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE2, representativeObj.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE3, representativeObj.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
				}
				model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());
				model.addAttribute("representativeObj", representativeObj);
				List<String> primaryContactOptions = new ArrayList<String>();
				primaryContactOptions.add(IssuerRepresentative.PrimaryContact.YES.toString());
				primaryContactOptions.add(IssuerRepresentative.PrimaryContact.NO.toString());
				model.addAttribute("primaryContactList", primaryContactOptions);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute("primaryContact", (representativeObj.getPrimaryContact() != null) ? representativeObj.getPrimaryContact() : PlanMgmtConstants.EMPTY_STRING);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.EDIT_REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ex, null, Severity.LOW);
			}
			return "admin/issuer/representative/edit";
		}
	
		
		/*  ##### controller for save issuer representative details   ##### */
		@RequestMapping(value = "/admin/issuer/representative/save", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String saveRepresentative(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser accountUser, 
				@ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) {
			Issuer issuerObj = null;
			String redirectUrl = null;
			String encryptedIssuerId = null;

			try {
				boolean duplicateEmailFlag = issuerService.isDuplicate(issuerRepresentative.getEmail());
				if (duplicateEmailFlag) {
					model.addAttribute("duplicateError", PlanMgmtConstants.YES);
					model.addAttribute("issuerRepresentative", issuerRepresentative);
					encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerRepresentative.getIssuer().getId()));
						redirectUrl = "redirect:/admin/issuer/representative/add/" + encryptedIssuerId;
					redirectAttrs.addFlashAttribute("repUser", issuerRepresentative);
					//redirectAttrs.addFlashAttribute("duplicateError", PlanMgmtConstants.YES);
					redirectAttrs.addFlashAttribute(PlanMgmtConstants.REP_DUPLICATE_EMAIL, true);
					if(issuerRepresentative.getPhone() != null && issuerRepresentative.getPhone().length() >= PlanMgmtConstants.TEN){
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.PHONE1, issuerRepresentative.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.PHONE2, issuerRepresentative.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.PHONE3, issuerRepresentative.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
					}
				}else{
					//server side validation
					 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
					 Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative, IssuerRepresentative.AddNewIssuerRepresentative.class);
					 if(violations != null && !violations.isEmpty()){
						//throw exception in case client side validation breached
						 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					 }
					issuerObj = issuerService.getIssuerById(issuerRepresentative.getIssuer().getId());
					if (accountUser.getUsername() == null) {
						accountUser.setUserName(accountUser.getEmail());
					}
					if(null != issuerObj && null != issuerRepresentative){
						
						String role = ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString(); // default role is issuer_representative

						// set role as issuer_enrollment_representative when admin add issuer enrollment representative
						if(null != request.getParameter("role") && request.getParameter("role").equalsIgnoreCase("ENROLLMENT_REP")){ 
							role =  ActivationJson.OBJECTTYPE.ISSUER_ENROLLMENT_REPRESENTATIVE.toString();
						}
						addRepresentative(accountUser, issuerObj, PrimaryContact.NO.toString(), redirectAttrs, issuerRepresentative, role);
					}
					model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
					encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerObj.getId()));
					
					redirectUrl = PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encryptedIssuerId;
				}
			} catch (InvalidUserException e) {
				LOGGER.error("Error occured while adding representative", e.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION, e, null, Severity.HIGH);
			}
			return redirectUrl;
		}
		
		
		/*  ##### controller for update issuer representative details   ##### */
		@RequestMapping(value = "/admin/issuer/representative/update", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
		public String editRepresentative(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser repUser, 
				@ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, 
				Model model, HttpServletRequest editRepRequest,  
				@RequestParam(value = "issuerRepId", required = false) Integer issuerRepId, RedirectAttributes redirectAttr) throws InvalidUserException {
			Integer issuerId = 0;
			try{
				//server side validation
				 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative, IssuerRepresentative.AddNewIssuerRepresentative.class);
				 if(violations != null && !violations.isEmpty()){
						//throw exception in case client side validation breached
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
				 String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();				 
				 IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findById(issuerRepId);
				 if(null != issuerRepObj)
				 {
					issuerId = issuerRepObj.getIssuer().getId();
					
					// If state exchange is CA then update location information
					if (PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) {
						Location location = issuerRepObj.getLocation();
						if (issuerRepresentative != null && issuerRepresentative.getLocation() != null) {
							if (location == null) {
								location = new Location();
							}
							location.setAddress1(issuerRepresentative.getLocation().getAddress1());
							location.setAddress2(issuerRepresentative.getLocation().getAddress2());
							location.setCity(issuerRepresentative.getLocation().getCity());
							location.setState(issuerRepresentative.getLocation().getState());
							location.setZip(issuerRepresentative.getLocation().getZip());

							// save issue representative address in location table
							Location newlocation = locationService.save(location);
							issuerRepObj.setLocation(newlocation);
						}
					}else if (issuerRepObj.getUserRecord()==null && stateCode.equalsIgnoreCase("ID") &&
							!issuerRepObj.getEmail().equalsIgnoreCase(editRepRequest.getParameter(PlanMgmtConstants.EMAIL))) {   //HIX-103663
						boolean duplicateEmailFlag = issuerService.isDuplicate(editRepRequest.getParameter(PlanMgmtConstants.EMAIL));
						if(duplicateEmailFlag){
							issuerRepresentative.setId(issuerRepId);
							model.addAttribute("duplicateError", PlanMgmtConstants.YES);
							model.addAttribute("representativeObj", issuerRepresentative);
							model.addAttribute("repUser", issuerRepresentative);
							model.addAttribute(PlanMgmtConstants.REP_DUPLICATE_EMAIL, true);
							if(issuerRepresentative.getPhone() != null && issuerRepresentative.getPhone().length() >= PlanMgmtConstants.TEN){
								model.addAttribute(PlanMgmtConstants.PHONE1, issuerRepresentative.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
								model.addAttribute(PlanMgmtConstants.PHONE2, issuerRepresentative.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
								model.addAttribute(PlanMgmtConstants.PHONE3, issuerRepresentative.getPhone().substring(PlanMgmtConstants.SIX,  PlanMgmtConstants.TEN));
							}
							AccountUser currentAccountUser=userService.getLoggedInUser();
							String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
							model.addAttribute(ROLE_NAME, activeRoleName);
							model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerRepObj.getIssuer());
							model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
							model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
							List<String> primaryContactOptions = new ArrayList<String>();
							primaryContactOptions.add(IssuerRepresentative.PrimaryContact.YES.toString());
							primaryContactOptions.add(IssuerRepresentative.PrimaryContact.NO.toString());
							model.addAttribute("primaryContactList", primaryContactOptions);
							model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
							model.addAttribute("primaryContact", (issuerRepresentative.getPrimaryContact() != null) ? issuerRepresentative.getPrimaryContact() : PlanMgmtConstants.EMPTY_STRING);
							model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
							return "admin/issuer/representative/edit";
						}
						
						
					}
			
					String primaryContactStr = editRepRequest.getParameter(PlanMgmtConstants.PRIMARY_CONTACT);
					if (StringUtils.isNotBlank(primaryContactStr)) {
						if(primaryContactStr.equalsIgnoreCase(IssuerRepresentative.PrimaryContact.YES.toString())){
							// set primary contact from NO for other issuer rep expect current one
							String response = issuerService.updatePrimaryContactToNo(issuerId, issuerRepresentative.getId());
							if(LOGGER.isDebugEnabled()){
								LOGGER.debug("response from updateStatusToNo : " +  SecurityUtil.sanitizeForLogging(response));
							}
							issuerRepObj.setPrimaryContact(IssuerRepresentative.PrimaryContact.YES);
						}else{
							issuerRepObj.setPrimaryContact(IssuerRepresentative.PrimaryContact.NO);
						}
					}
					 
					issuerRepObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
					issuerRepObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
					issuerRepObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
					issuerRepObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
					issuerRepObj.setEmail(editRepRequest.getParameter(PlanMgmtConstants.EMAIL));
					issuerRepObj.setUpdatedBy(userService.getLoggedInUser());
					
					// save issuer representative data
					IssuerRepresentative newIssuerRepresentative = issuerService.saveRepresentative(issuerRepObj);
					
					// update user data
					if(issuerRepObj.getUserRecord()!=null) {
						AccountUser accountUserObj = userService.findById(issuerRepObj.getUserRecord().getId());
						if(null != accountUserObj){
							accountUserObj.setFirstName(editRepRequest.getParameter(PlanMgmtConstants.FIRST_NAME));
							accountUserObj.setLastName(editRepRequest.getParameter(PlanMgmtConstants.LAST_NAME));
							accountUserObj.setTitle(editRepRequest.getParameter(PlanMgmtConstants.TITLE));
							accountUserObj.setPhone(editRepRequest.getParameter(PlanMgmtConstants.PHONE));
							userService.updateUser(accountUserObj);
						}
					}else {
						
						if (newIssuerRepresentative != null && isEmailActivation && !stateCode.equalsIgnoreCase("CA")
								&& !stateCode.equalsIgnoreCase("MS")) {
							try {
								String role = ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString();
								if(StringUtils.isNotBlank(newIssuerRepresentative.getRole())){
									role = newIssuerRepresentative.getRole();
								}
								issuerService.generateActivationLink(newIssuerRepresentative.getId(), repUser, role);
								redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to "
										+ repUser.getEmail());
							} catch (Exception e) {
								LOGGER.error(" " + e);
								redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sending process failed... Please take appropriate actions. -  "
										+ e.getMessage());
							}
						}
					}
				 }
				 
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPDATE_REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			
			String encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerId));
			return  PlanMgmtConstants.MANAGE_REPRESENTATIVE_REDIRECT_URL + encryptedIssuerId;
		}

		
		/*  ##### private method to add issuer representative   ##### */
		private AccountUser addRepresentative(AccountUser accountUser, Issuer issuerObj, String primaryContact, RedirectAttributes redirectAttr, IssuerRepresentative issuerRepresentative, String role) throws InvalidUserException {
			AccountUser representativeUser = null;
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			if (!issuerService.isDuplicate(accountUser.getEmail())) {
				try {
					IssuerRepresentative issuerRep = new IssuerRepresentative();
					issuerRep.setUserRecord(null); // Until user object is created in users table, IssureRep's user_id will be null
					if (!isEmailActivation) {
						representativeUser = userService.createUser(accountUser, GhixRole.ISSUER_REP.getRoleName());
						ModuleUser issuerRepUser = userService.findModuleUser(issuerObj.getId(), ModuleUserService.ISSUER_MODULE, representativeUser);
						if (issuerRepUser == null) {
							userService.createModuleUser(issuerObj.getId(), ModuleUserService.ISSUER_MODULE, representativeUser);
						} else {
							LOGGER.error("Admin Portal: Adding New Issuer Rep: Representative record already exists in ModuleUser.");
						}
						redirectAttr.addAttribute("isIssuerRepAdded", true);
						if(representativeUser != null) {
							issuerRep.setUserRecord(representativeUser);
						}
					} else {
						redirectAttr.addAttribute("isIssuerRepAdded", false);
					}
					
					issuerRep.setIssuer(issuerObj);
					issuerRep.setPrimaryContact((primaryContact.equalsIgnoreCase("YES")) ? PrimaryContact.YES
							: PrimaryContact.NO);
					issuerRep.setLocation(issuerRepresentative != null ? issuerRepresentative.getLocation()
							: null);
					issuerRep.setUpdatedBy(userService.getLoggedInUser());
					issuerRep.setFirstName(accountUser.getFirstName());
					issuerRep.setLastName(accountUser.getLastName());
					issuerRep.setTitle(accountUser.getTitle());
					issuerRep.setEmail(accountUser.getEmail());
					issuerRep.setPhone(accountUser.getPhone());
					issuerRep.setRole(role);

					IssuerRepresentative newIssuerRepresentative = issuerService.saveRepresentative(issuerRep);
					if (newIssuerRepresentative != null
							&& isEmailActivation
							&& !stateCode.equalsIgnoreCase("CA")
							&& !stateCode.equalsIgnoreCase("MS")) {
						try {
							issuerService.generateActivationLink(newIssuerRepresentative.getId(), accountUser, role);
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to "
									+ accountUser.getEmail());
						} catch (Exception e) {
							LOGGER.error(" " + e);
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sending process failed... Please take appropriate actions. -  "
									+ e.getMessage());
						}
					}
					
					// send delegation code
					issuerRepresentativeService.updateDelegationInfo(newIssuerRepresentative, redirectAttr, issuerObj, accountUser);
					
				} catch (GIException gi) {
					LOGGER.error("Exception caught in AHBX call" + gi.getMessage(), gi);
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GI_AHBX_EXCEPTION, gi, null, Severity.LOW);
				} catch (Exception e) {
					LOGGER.error("Admin Portal: Adding New Issuer Rep: Error in saving representative details.", e.getMessage());
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ADD_REPRESENTATIVE_EXCEPTION, e, null, Severity.LOW);
				}

			} else {
				LOGGER.error("Admin Portal: Adding New Issuer Rep: Email address already exists ");
				redirectAttr.addAttribute("isIssuerRepAdded", "Admin Portal: Adding New Issuer Rep: Email address already exists");
			}
			return representativeUser;
		}

	
}
