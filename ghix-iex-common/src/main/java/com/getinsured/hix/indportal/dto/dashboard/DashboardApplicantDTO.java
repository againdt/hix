package com.getinsured.hix.indportal.dto.dashboard;

import java.io.Serializable;
import java.util.List;

public class DashboardApplicantDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public enum RelationEnum {
		SELF, SPOUSE, CHILD, DEPENDENT
	}

	private String memberId;
	private String guid;
	private String firstName;
	private String lastName;
	private String relation;
	private boolean seekingCoverage;
	private boolean seeksQHP;
	private boolean exchangeEligible;
	private boolean aptcEligible;
	private boolean stateSubsidyEligible;
	private boolean csrEligible;
	private boolean nativeAmerican;
	private String csrLevel;
	private String middleName;
	private String nameSuffix;
	private boolean chipEligible;
	private boolean assessedChipEligibile;
	private boolean medicaidMAGIEligibile;
	private boolean medicaidNonMAGIEligibile;
	private boolean assessedMedicaidMAGIEligibile;
	private boolean assessedMedicaidNonMAGIEligibile;

	private List<SepApplcantEventDTO> sepApplicantEvents;
	
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public boolean getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(boolean seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}
	
	public boolean isSeeksQHP() {
		return seeksQHP;
	}

	public void setSeeksQHP(boolean seeksQHP) {
		this.seeksQHP = seeksQHP;
	}

	public boolean getExchangeEligible() {
		return exchangeEligible;
	}

	public void setExchangeEligible(boolean exchangeEligible) {
		this.exchangeEligible = exchangeEligible;
	}

	public boolean getAptcEligible() {
		return aptcEligible;
	}

	public void setAptcEligible(boolean aptcEligible) {
		this.aptcEligible = aptcEligible;
	}

	public boolean getCsrEligible() {
		return csrEligible;
	}

	public void setCsrEligible(boolean csrEligible) {
		this.csrEligible = csrEligible;
	}

	public boolean getNativeAmerican() {
		return nativeAmerican;
	}

	public void setNativeAmerican(boolean nativeAmerican) {
		this.nativeAmerican = nativeAmerican;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}	

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	public boolean getChipEligible() {
		return chipEligible;
	}

	public void setChipEligible(boolean chipEligible) {
		this.chipEligible = chipEligible;
	}

	public boolean getStateSubsidyEligible() {
		return stateSubsidyEligible;
	}

	public void setStateSubsidyEligible(boolean stateSubsidyEligible) {
		this.stateSubsidyEligible = stateSubsidyEligible;
	}
	
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public List<SepApplcantEventDTO> getSepApplicantEvents() {
		return sepApplicantEvents;
	}

	public void setSepApplicantEvents(List<SepApplcantEventDTO> sepApplicantEvents) {
		this.sepApplicantEvents = sepApplicantEvents;
	}

	public boolean isAssessedChipEligibile() {
		return assessedChipEligibile;
	}

	public void setAssessedChipEligibile(boolean assessedChipEligibile) {
		this.assessedChipEligibile = assessedChipEligibile;
	}

	public boolean isMedicaidMAGIEligibile() {
		return medicaidMAGIEligibile;
	}

	public void setMedicaidMAGIEligibile(boolean medicaidMAGIEligibile) {
		this.medicaidMAGIEligibile = medicaidMAGIEligibile;
	}

	public boolean isMedicaidNonMAGIEligibile() {
		return medicaidNonMAGIEligibile;
	}

	public void setMedicaidNonMAGIEligibile(boolean medicaidNonMAGIEligibile) {
		this.medicaidNonMAGIEligibile = medicaidNonMAGIEligibile;
	}

	public boolean isAssessedMedicaidMAGIEligibile() {
		return assessedMedicaidMAGIEligibile;
	}

	public void setAssessedMedicaidMAGIEligibile(boolean assessedMedicaidMAGIEligibile) {
		this.assessedMedicaidMAGIEligibile = assessedMedicaidMAGIEligibile;
	}

	public boolean isAssessedMedicaidNonMAGIEligibile() {
		return assessedMedicaidNonMAGIEligibile;
	}

	public void setAssessedMedicaidNonMAGIEligibile(boolean assessedMedicaidNonMAGIEligibile) {
		this.assessedMedicaidNonMAGIEligibile = assessedMedicaidNonMAGIEligibile;
	}
	
}
