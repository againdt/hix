package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SupervisionFacilityType;

/**
 * Wrapper class for SupervisionFacilityType
 * 
 * @author Nikhil Talreja
 * @since 05-Mar-2014
 */
public class SupervisionFacilityTypeWrapper implements JSONAware {

	private String facilityName;
	private FacilityLocationTypeWrapper facilityLocation;
	private FacilityContactInformationTypeWrapper facilityContactInformation;
	private String facilityCategoryCode;
	
	public SupervisionFacilityTypeWrapper(SupervisionFacilityType supervisionFacilityType){
		
		if(supervisionFacilityType == null){
			return;
		}
		
		this.facilityName = supervisionFacilityType.getFacilityName();
		this.facilityLocation = new FacilityLocationTypeWrapper(supervisionFacilityType.getFacilityLocation());
		this.facilityContactInformation = new FacilityContactInformationTypeWrapper(supervisionFacilityType.getFacilityContactInformation());
		this.facilityCategoryCode = supervisionFacilityType.getFacilityCategoryCode();
	}

	
	public String getFacilityName() {
		return facilityName;
	}


	public FacilityLocationTypeWrapper getFacilityLocation() {
		return facilityLocation;
	}


	public FacilityContactInformationTypeWrapper getFacilityContactInformation() {
		return facilityContactInformation;
	}


	public String getFacilityCategoryCode() {
		return facilityCategoryCode;
	}


	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("FacilityName", this.facilityName);
		if(this.facilityLocation != null){
			obj.put("FacilityLocation", this.facilityLocation);
		}
		if(this.facilityContactInformation != null){
			obj.put("FacilityContactInformation", this.facilityContactInformation);
		}
		obj.put("FacilityCategoryCode", this.facilityCategoryCode);
		return obj.toJSONString();
	}
	
	
}
