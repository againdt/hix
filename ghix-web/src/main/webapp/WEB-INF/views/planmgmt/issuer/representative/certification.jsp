<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="gutter10">
<div class="row-fluid">
	<h1><spring:message code="pgheader.issuerAuthRepr" /></h1>
</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="gutter10">
			<!--  beginning of side bar -->
			<ul class="nav nav-list steplist">
				<li class="nav-header"><spring:message code="label.issuerCertification" /></li>
				<li class="active"><a> <spring:message code="label.authorizeRepresentatives" /></a></li>				
				<li><a><spring:message code="label.submitIssuerApplication" /></a></li>
				<li><a><spring:message code="label.reviewAndConfirm" /></a></li>
			</ul>
			<!-- end of side bar -->
			</div>
		</div>
		<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<div class="gutter10">
				<p class="gutter10"><spring:message code="note.issuerAuthRepr" /></p>
				<form class="form-horizontal gutter10" id="issuerCertAddRep" name="issuerCertAddRep" action="<c:url value="/planmgmt/issuercertaddrepresentative" />" method="POST">
						<df:csrfToken/>
						<div class="control-group">
							<label for="firstName" class="required control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large">
									<c:if test="${isIssuerRepDuplicate == 'true'}">
									    <div class="error help-inline">
	                                        <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
	                                    </div> 
	                                </c:if>
									<div id="firstName_error" class=""></div>
								</div> <!-- end of controls-->
							
							<!-- end of label -->
						</div>
	
						<div class="control-group">
							<label for="lastName" class="required control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large">
									<div id="lastName_error" class=""></div>
								</div> <!-- end of controls-->
							
							<!-- end of label -->
						</div>
						
						<div class="control-group">
							<label for="lastName" class="required control-label"><spring:message  code="label.title"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="title" id="title" value="${repUser.title}" class="input-large" size="30">
									<div id="title_error" class=""></div>
								</div> 
						</div> 
	
						<div class="control-group">
							<label for="email" class="required control-label" type="email"><spring:message  code="label.Email"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="email" name="email" id="email" value="${repUser.email}" class="input-large">
									<div id="email_error" class=""></div>
								</div> <!-- end of controls-->
							<!-- end of label -->
						</div>
	
						<div class="control-group">
							<label for="userName" class="required control-label"><spring:message  code="label.userName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="userName" id="userName" value="${repUser.userName}" class="input-large">
									<c:if test="${isUserNameExists == 'true'}">
									    <div class="error help-inline">
	                                        <label class="error"><span><spring:message  code="err.userNameAlreadyExists"/></span></label>
	                                    </div> 
	                                </c:if>
									<div id="userName_error" class=""></div>
								</div> <!-- end of controls-->
						</div>
	
						<div class="control-group">
							<label for="password" class="required control-label"><spring:message  code="label.password"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="password" name="password" id="password" value="" class="input-large" autocomplete="off">
									<div id="password_error" class=""></div>
								</div> 							
						</div>
						
						<div class="control-group">
							<label for="confirmPassword" class="required control-label"><spring:message  code="label.confirmPassword"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="password" name="confirmPassword" id="confirmPassword" value="" class="input-large" autocomplete="off">
									<div id="confirmPassword_error" class=""></div>
								</div> 							
						</div>
						
						
						<!-- end of control-group -->
					</form>	
					<div class="form-actions">
						<input type="button" name="submit" id="submit" value="<spring:message code='label.saveAndStartApplication'/>" class="btn btn-primary" data-target="myModal" data-toggle="modal" onclick="formSubmit()"  />
					</div>
					
					<!-- end of form-actions -->
	
					</div>
					<!-- end of profile -->

					<c:choose>
						<c:when test="${isIssuerRepAdded == 'true'}">			
							<div class="modal hide fade" id="myModal">
							    <div class="modal-header">
								    <button type="button" id="pnetwrkClose" class="close" data-dismiss="modal"><span><spring:message code='label.close'/></span> &times;</button>
								    <h3>&nbsp;</h3>
								</div>
								<div class="modal-body">
								    <h3><spring:message code="msg.repAdded" javaScriptEscape='true'/></h3>
								</div>
								<div class="modal-footer">
								    <a href="<%=request.getContextPath()%>/planmgmt/issuercertificationauthreps" class="btn" data-dismiss="modal"><spring:message code='link.addAnotherRep' javaScriptEscape='true'/></a>
								     or 
								    <a href="<%=request.getContextPath()%>/planmgmt/issuerapplication" class="btn btn-primary"><spring:message code='link.continue' javaScriptEscape='true'/></a>
							    </div>
						    </div>	
						     <script type="text/javascript">
							    $('#myModal').modal("show");
							</script>					
						</c:when>
						
						<c:when test="${doesIssuerHaveRepr == 'true'}">			
							<div class="modal hide fade" id="myModal">
							    <div class="modal-header">
								    <button type="button" id="pnetwrkClose" class="close" data-dismiss="modal"><span><spring:message code='label.close'/></span> &times;</button>
								    <h3>&nbsp;</h3>
								</div>
								<div class="modal-body">
								    <h3><spring:message code="msg.repExists" javaScriptEscape='true'/></h3>
								</div>
								<div class="modal-footer">
								    <a href="<%=request.getContextPath()%>/planmgmt/issuercertificationauthreps" class="btn" data-dismiss="modal"><spring:message code='link.addAnotherRep' javaScriptEscape='true'/></a>
								     or 
								    <a href="<%=request.getContextPath()%>/planmgmt/issuerapplication" class="btn btn-primary"><spring:message code='link.continue' javaScriptEscape='true'/></a>
							    </div>
						    </div>	
						     <script type="text/javascript">
							    $('#myModal').modal("show");
							</script>					
						</c:when>
						
						<c:otherwise>							
							<div class="modal hide fade" id="myModal">
							    <div class="modal-header">
								    <button type="button" id="pnetwrkClose" class="close" data-dismiss="modal"><span><spring:message code="label.close"/></span> &times;</button>
								    <h3>&nbsp;</h3>
								</div>
								<div class="modal-body">
								    <h3><spring:message code="msg.repAdded" javaScriptEscape='true'/></h3>
								</div>
								<div class="modal-footer">
								    <a href="<%=request.getContextPath()%>/planmgmt/issuercertificationauthreps" class="btn" data-dismiss="modal"><spring:message code='link.addAnotherRep' javaScriptEscape='true'/></a>
								     or 
								    <a href="<%=request.getContextPath()%>/planmgmt/issuerapplication" class="btn btn-primary"><spring:message code='link.continue' javaScriptEscape='true'/></a>
							    </div>
						    </div>		
						    <script type="text/javascript">
							    $('#myModal').modal("hide");
							</script>				
						</c:otherwise>
					</c:choose>
				
				<!-- end of form-actions -->

		</div>
		<!-- end of span9 -->
		<!-- assister registration -->
	</div>
</div>
	<!-- end of row-fluid -->


<script type="text/javascript">
var validator = $("#issuerCertAddRep").validate({ 
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		email : { required : true, email: true},
		userName : { required : true},
		password : { required : true, minlength:8},		
		confirmPassword : {equalTo: "#password"}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>"},
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>"},
		userName: { required : "<span><em class='excl'>!</em><spring:message  code='err.userName' javaScriptEscape='true'/></span>"},
		password : { required : "<span><em class='excl'>!</em><spring:message  code='err.password' javaScriptEscape='true'/></span>"},		
		confirmPassword : {required : "<span><em class='excl'>!</em><spring:message  code='err.confirmPassword' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

function chkUserName() {
	var userName = ${"userName"};
	alert("userName : " + userName);
}

function  formSubmit(){			
	$("#issuerCertAddRep").submit();
	
}
</script>
