<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/issuer/partner/list/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
       		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
       		<li><spring:message  code="pgheader.partners"/></li>
        </ul><!--page-breadcrumb ends-->
		<h1><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
    </div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.partners"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
                    <c:if test="${exchangeState != 'ID' && exchangeState != 'MN'}">
                    	<li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
                    <%-- <c:if test="${exchangeState != 'ID'}">
                    	<li><a href="<c:url value="/admin/issuer/market/shop/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    </c:if> --%>
                    <li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
					<li><a href="<c:url value="/admin/issuer/partner/list/${encIssuerId}"/>"><spring:message  code="pgheader.ediTransactions"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
                    <c:if test="${exchangeState == 'ID'}">
                    <li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
                    </c:if>
           			 <c:if test="${activeRoleName == 'OPERATIONS'}">
						<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
					</c:if>
                    
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.partners"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/partner/edit/${issuerObj.id}?partnerId=${listExchgPartnersObj.id}"/>"><spring:message  code="label.edit"/></a>
                </div>
				<div class="gutter10">

				<form class="form-horizontal" id="frmIssuerPartner" name="frmIssuerPartner" action="#" method="POST">
                    <div class="control-group">
						<label class="control-label" for="insurerName"><spring:message  code="label.insurerName"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.insurerName}</strong></div>
					</div><!-- end of control-group-->

					<div class="control-group">
						<label class="control-label" for="st01"><spring:message  code="label.transactionType"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.st01}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="isa05"><spring:message  code="label.isa05"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa05}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="isa06"><spring:message  code="label.isa06"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa06}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="isa07"><spring:message  code="label.isa07"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa07}</strong></div>
					</div><!-- end of control-group-->                    
                    
                    <div class="control-group">
						<label class="control-label" for="isa08"><spring:message  code="label.isa08"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa08}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="isa13"><spring:message  code="label.isa13"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa13}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="isa15"><spring:message  code="label.isa15"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.isa15}</strong></div>
					</div><!-- end of control-group-->
                                   
                    <div class="control-group">
						<label class="control-label" for="gs02"><spring:message  code="label.gs02"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.gs02}</strong></div>
					</div><!-- end of control-group-->
                                   
                    <div class="control-group">
						<label class="control-label" for="gs03"><spring:message  code="label.gs03"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.gs03}</strong></div>
					</div><!-- end of control-group-->
                                   
                    <div class="control-group">
						<label class="control-label" for="gs08"><spring:message  code="label.gs08"/></label>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.gs08}</strong></div>
					</div><!-- end of control-group-->
                        
                    <div class="control-group">
						<label class="control-label" for="pickupOrDropoffLocation">
						<c:if test="${'Inbound' == listExchgPartnersObj.direction}"><spring:message  code="label.pickupLocation"/></c:if>
						<c:if test="${'Outbound' == listExchgPartnersObj.direction}"><spring:message  code="label.dropoffLocation"/></c:if>
						<div class="controls paddingT5"><strong>${listExchgPartnersObj.pickupOrDropoffLocation}</strong></div>
					</div><!-- end of control-group-->
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<script type="text/javascript">
$('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
});
</script>
