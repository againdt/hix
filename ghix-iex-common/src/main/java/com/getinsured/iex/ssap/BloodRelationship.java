
package com.getinsured.iex.ssap;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 *
 */
public class BloodRelationship {


	@Expose
    private String individualPersonId;
    @Expose
    private String relatedPersonId;
    @Expose
    private String relation;
    @Expose
    private Boolean textDependency;
    
	public String getIndividualPersonId() {
        return individualPersonId;
    }

    public void setIndividualPersonId(String individualPersonId) {
        this.individualPersonId = individualPersonId;
    }

    public String getRelatedPersonId() {
        return relatedPersonId;
    }

    public void setRelatedPersonId(String relatedPersonId) {
        this.relatedPersonId = relatedPersonId;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Boolean getTextDependency() {
        return textDependency;
    }

    public void setTextDependency(Boolean textDependency) {
        this.textDependency = textDependency;
    }

    @Override
	public int hashCode() {
		return new HashCodeBuilder()
		.append(individualPersonId)
		.append(relatedPersonId)
		.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BloodRelationship){

			final BloodRelationship other = (BloodRelationship) obj;
			return new EqualsBuilder().append(individualPersonId, other.individualPersonId)
					.append(relatedPersonId, other.relatedPersonId)
					.isEquals();

		} else{
			return false;
		}
	}

	public static BloodRelationship build(String individualPersonId, String relatedPersonId){
		BloodRelationship br = new BloodRelationship();
		br.setIndividualPersonId(individualPersonId);
		br.setRelatedPersonId(relatedPersonId);
		return br;
	}



	
}
