<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">

</script>

<!--start page-breadcrumb -->
<div class="gutter10">
<div class="row-fluid">	
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employer/searchemployer"/>">Employers</a></li>
			<li>Summary</li>
		</ul> 
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
		<h1>
		${employer.name} <small class="margin20-l">Contact Name:
			${employer.contactFirstName} ${employer.contactLastName}</small>
	</h1>
	</div>
	<!--page-breadcrumb ends-->
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">
		<div class="header">
			<h4>About this Employer</h4>
			</div>
			<ul class="nav nav-list">
				<li class="active navmenu">Summary</li>
				<li class="navmenu"><a href="/hix/crm/crmemployer/employercontactinfo/${employerId}">Contact Info</a></li>
				<!--   <li class="navmenu disabled"><a href="#">Messages</a></li> -->
				 <li class="navmenu"><a href="/hix/crm/crmemployer/employercomments/${employerId}">Comments</a></li>
				<li class="navmenu"><a href="/hix/crm/crmemployer/ticket/history/${employerId}">Ticket History</a></li>
				<li ><a href="/hix/crm/crmemployer/securityQuestions/${employerId}">Security Questions</a></li>
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> Actions</h4>
			</div>
			<ul class="nav nav-list">
			<c:if test="${checkDilog == null}">  
				<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
			</c:if>
			<c:if test="${checkDilog != null}">  
				<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employer&switchToModuleId=${employerId}&switchToResourceName=${employer.name}"      role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
			</c:if>
			<li class="navmenu"><a href="<c:url value="/crm/crmemployer/passwordreset"/>/${employerId}?sourcePage=details" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
			
                <!-- <li class="navmenu disabled"><a href="#" ><i class="icon-envelope"></i>Compose Message</a></li>
                <li class="navmenu disabled"><a href="#"><i class="icon-comment"></i>New Comment</a></li> -->
            </ul>    
		</div>
		
		<div id="rightpanel" class="span9">
			<div class="graydrkaction margin0">
                 <h4 class="span10">Summary</h4>
            </div>
            <div class="gutter10">
			<form class="form-horizontal" id="" name="" action="" method="POST">
			              <df:csrfToken/>
				<table class="table table-border-none">
					<!-- <thead>
						<tr class="graydrkbg">
							<th class="span3"><strong>Summary</strong></th>
							<th></th>
						</tr>
					</thead> -->
					<tbody>
						<tr>
							<td class="txt-right span4">Business Name</td>
							<td><strong>${employer.name}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">EIN</td>
							<td><strong>${employer.federalEIN}</strong></td>
						</tr>
						<c:choose>
							<c:when test="${isCACall == 'TRUE'}">
								<tr>
									<td class="txt-right">Corporate Type</td>
									<td><strong></strong></td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td class="txt-right">Corporate Type</td>
									<td><strong>${employer.orgType.type}</strong></td>
								</tr>
							</c:otherwise>
						</c:choose>
						
						<tr>
							<td class="txt-right">Total Employees</td>
							<td><strong>${totalEmp}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Average Salary</td>
							<td><strong>${avgSal}</strong></td>
						</tr>
						
								<tr>
									<td class="txt-right">Locations</td>
									<td><strong>${fn:length(employer.locations)}</strong></td>
								</tr>
								<tr>
									<td class="txt-right vertical-align-top">Primary Worksite Address</td>
									<td><strong>
											${primaryLocation.location.address1} <br />
											${primaryLocation.location.address2}
											${primaryLocation.location.city}
											${primaryLocation.location.state}
											${primaryLocation.location.zip}
									</strong></td>
								</tr>
							<c:if test="${alternatelocations != null}"> 
							<% 	int index = 0; %>
										<c:forEach items="${alternatelocations}" var="employerLoc">	
										<% index++;%>
										<tr>	
										<td class="txt-right vertical-align-top">Worksite Address <%= index+1 %></td>							
										<td>
											<strong>
													${employerLoc.location.address1} <br />
													${employerLoc.location.address2}
													${employerLoc.location.city}
													${employerLoc.location.state}
													${employerLoc.location.zip}
											</strong>
										</td>
										</tr>							
										</c:forEach>									
							</c:if>										
						<tr>
							<td class="txt-right">Benchmark Plan</td>
							<td><strong>${benchMarkPlan}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Eligibility Status</td>
							<td><strong>${employer.eligibilityStatus}</strong></td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="employerName" id="employerName" 
				value="${employer.name}" />
			</form>
			</div>
		</div>
		

	</div>
	<!-- row-fluid -->
</div>
<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/employer/home" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employer Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="close" type="button">&times;</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employer View" will take you to the Employer's portal for ${employer.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employer, such as view <br/>
					and edit employee list, fill out employer eligibility etc.<br/>
					Proceed to Employer view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployerView" name="checkEmployerView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employerId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employer.name}" />
   <button class="btn btn-primary" type="submit" >Employer View</button >
    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>