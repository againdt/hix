import { DrugDetail } from './../../../models/data.model';
import { DosageInfo, DrugInfo, RxcuiInfo } from '../../../models/data.model';
import { DataKeepService } from '../../../services/data-keep.service';
import { FetchService } from '../../../services/fetch.service';
import { Component, OnInit, OnChanges, ViewChild, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {DrugSearchTypeaheadComponent} from '../../drug-search-typeahead/drug-search-typeahead.component';
import { ElementRef, HostListener} from '@angular/core';


@Component({
    selector: 'gi-drug-which',
    templateUrl: './drug-which.component.html',
    styleUrls: ['./drug-which.component.scss']
})
export class DrugWhichComponent implements OnInit {
    @Input() drugList: DrugDetail[];
    @Input() dosagePane: boolean;
    public dosages$ = new Subject<DosageInfo[]>();
    public dosageSelectionActive$ = new Subject<boolean>(); // dosage selection pane
    public radio: { dosage: DosageInfo } = { dosage: null }; // for ngModel
    public selectedDrug: DrugInfo;
    public drugSays$ = new Subject<string>();
    public drugErrorMessage: string = '';
    public selectDosageAlert: boolean = false;
    public isFocus: boolean = false;
    public isDrugEmpty: boolean = true;
    @ViewChild("drugInfoInput")
    private drugInfoInput;

    @ViewChild(DrugSearchTypeaheadComponent)
    private drugSearch: DrugSearchTypeaheadComponent;

    @HostListener('click', ['$event'])
    @HostListener('keypress', ['$event'])
    clickout(event) {
		this.drugErrorMessage = '';
    }

    constructor(
        public datakeep: DataKeepService,
        private fetch: FetchService,
        private eRef: ElementRef
    ) { }

	ngOnInit() {
		this.drugSays$.subscribe(val => {
            if (val) {
                this.drugErrorMessage = val;
            }

        });
        this.selectDosageAlert = this.dosagePane;
        this.datakeep.selectDosageAlert$.subscribe(
	        data => {
	            this.selectDosageAlert = data;
	        }
        );
	}


    //select drug name from typeahead dropdown list
    public receiveDrug(drug: DrugInfo) {
        //console.log("select drug: " + drug);

        this.dosageSelectionActive$.next(true);
        this.dosages$.next(null);
        this.selectedDrug = drug;
        this.drugInfoInput.nativeElement.value = ""
        this.fetch.searchDrugById(encodeURIComponent(drug.drugID)).subscribe(
            val => {
                if(val.hasOwnProperty("drugDosages")) {
                    this.dosages$.next(val.drugDosages);
                    this.datakeep.dosageCard = true;
                    this.readPrescription("dosage-card");
                    //console.log(document.activeElement);
                }else {
			        this.triggerErrorMessageObservable('DRUG_SEARCH_TYPEAHED.error.no_result');
                    this.dosageSelectionActive$.next(false);
                }
            },
            err => {
                console.error("searchDrugById failed", err);
            }
        )
    }

    public handleDrugDestroy(drugDetail: DrugDetail): void {
        this.datakeep.updatePersonalizedState('DRUG', 'REMOVE', drugDetail);

        const label = 'You removed ' + drugDetail.drugDosage  + ' ' + (drugDetail.genericName === undefined? '' : drugDetail.genericName + '(Generic Name)');
        this.datakeep.updateAriaLabel('drug-search-field', label);
    }

    //select drug from dosage pane
    public addDrug(): void {
        //let rxcuiInfo$ = new Subject<RxcuiInfo>();
        this.drugSearch.drug = "";
        this.dosages$.next(null);
        this.dosageSelectionActive$.next(false);
        this.datakeep.dosageCard = false;
        this.selectDosageAlert = false;

        const drugInfo: DrugInfo = this.selectedDrug;
        const dosageInfo: DosageInfo = this.radio.dosage;
        //console.log("drugInfo: " + drugInfo);

        let rxcuiInfo: RxcuiInfo;
        const drugDetail: DrugDetail =  {
            drugID: drugInfo.drugID,
            drugName: drugInfo.drugName,
            drugType: drugInfo.drugType,
            drugDosage: dosageInfo.drugDosageName, // drugDosageName
            drugRxCode: dosageInfo.drugDosageID, // rx
            genericID: null,
            genericName: null,
            genericDosage: null,
            genericRxCode: null
        };

		if(dosageInfo.genericDosageName !== undefined && dosageInfo.genericDosageName !== null && dosageInfo.genericDosageName !== ''){
			drugDetail.genericID = dosageInfo.genericDosageName;
			drugDetail.genericName = dosageInfo.genericDosageName;
			drugDetail.genericDosage = dosageInfo.genericDosageName;
		}


		if(dosageInfo.genericDosageID !== undefined && dosageInfo.genericDosageID !== null && dosageInfo.genericDosageID !== ''){
			drugDetail.genericRxCode = dosageInfo.genericDosageID;
		}

		if(drugDetail.genericRxCode === null){
			this.fetch.genericRxcui(drugInfo.drugID, dosageInfo.strength).subscribe(
				val => {
					if(val._body !== '' && val._body !== null) {
						rxcuiInfo = val.json();

						drugDetail.drugType = rxcuiInfo.drugType;
                		drugDetail.genericID = rxcuiInfo.genericName;
                		drugDetail.genericName = rxcuiInfo.genericName;
                		drugDetail.genericDosage = rxcuiInfo.genericDosage;
               			drugDetail.genericRxCode = rxcuiInfo.genericRxcui;

					}
				},

				err => {
					console.error("RxcuiInfo search failed", err);
				},

				() => {

					this.updateDrug(drugDetail, drugInfo.drugID);


				}
			)
		}else{

			this.updateDrug(drugDetail, drugInfo.drugID);
		}




    }

	public updateDrug(drugDetail: DrugDetail, drugInfo: string): void {
		const rsp = this.datakeep.updatePersonalizedState('DRUG', 'ADD', drugDetail);
        if(rsp.status === "success") {
        	this.datakeep.updateAriaLabel('drug-search-field', 'You selected ' + drugDetail.drugDosage);
        }
        if (rsp.status === "failure") {
	        this.triggerErrorMessageObservable(rsp.msg);
        }
        this.radio.dosage = null;
	}

    public destroy(): void {
        this.dosageSelectionActive$.next(false);
        this.dosages$.next(null);
        this.datakeep.dosageCard = false;
        this.radio.dosage = null;

        this.selectDosageAlert = false;
        this.datakeep.updateAriaLabel('drug-search-field', 'You remove dosage selection.');

    }

    public readPrescription(dosageId){
          setTimeout(function() {
            document.getElementById(dosageId).focus();
        }, 0);
    }

    getFocus(getFocus: boolean) {
		this.isFocus = getFocus;
		if(!this.isFocus){
			this.drugErrorMessage = '';
		}


		if(this.isFocus) {
			this.selectDosageAlert = false;
			this.getChange('');


			//close dosage list container happends in next loop, so timeout is needed here
			setTimeout(() => {
				const selectDosageShowing = !!document.getElementById("dosage-card");

				if(selectDosageShowing){
					this.selectDosageAlert = true;

					document.getElementById("dosage-card").focus();
				}

			}, 0);


		}
    }


    getDrug(drugFound: boolean) {
		if(!drugFound){
			this.triggerErrorMessageObservable('DRUG_SEARCH_TYPEAHED.error.no_result');
		}
    }


    getChange(drug: string) {
    	if(drug.length > 0){
    		this.isDrugEmpty = false;
    	}else{
    		this.isDrugEmpty = true;
    	}
    }

    triggerErrorMessageObservable(errorMessage: string): void{
    	setTimeout(() => {
        	this.drugSays$.next(errorMessage);
        	document.getElementById('drug-search-field').focus();
        }, 0);
    }

}
