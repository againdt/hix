/**
 * 
 */
package com.getinsured.hix.cdn;

/**
 * CDN provider definitions.
 * 
 * @author Yevgen Golubenko
 */
public class CDNProvider
{
	private Type cdnType;

	/**
	 * Type of CDN provider.
	 * 
	 * @author Yevgen Golubenko
	 */
	public enum Type
	{
		/**
		 * Unknown or unspecified type. Something is not configured or
		 * application
		 * started with errors if this type is returned. Check logs to make sure
		 * everything
		 * is loaded properly.
		 */
		UNKNOWN(-1),

		/**
		 * Local server is used as CDN provider (localhost that is)
		 */
		LOCAL(0),

		/**
		 * Getinsured.com used as CDN provider.
		 */
		GETINSURED(1),

		/**
		 * Google CDN is used.
		 */
		GOOGLE_CDN(2),

		/**
		 * Amazon S3 storage used as CDN provider.
		 */
		AMAZON_S3(3),

		/**
		 * Rackspace CDN provider.
		 */
		RACKSPACE(4);

		private Integer type;

		private Type(final Integer type)
		{
			this.type = type;
		}

		/**
		 * Returns integer type associated with current CDN configuration.
		 * 
		 * @return integer type for current CDN.
		 */
		public Integer getType()
		{
			return type;
		}

		/**
		 * Converts from java.lang.Integer to Enum type.
		 * 
		 * @param i Integer value to convert.
		 * @return {@link Type} for given integer or {@link Type#UNKNOWN}
		 */
		public Type fromInteger(final Integer i)
		{
			for (final Type t : values())
			{
				if (t.getType().equals(i) || t.getType() == i)
				{
					return t;
				}
			}

			return UNKNOWN;
		}
	}

	/**
	 * Returns base domain for CDN.
	 * 
	 * @return
	 */
	public String getBaseDomain()
	{
		/*
		 * TODO: golubenko: figure out good way to test QA boxes...
		 */
		switch (cdnType)
		{
			case UNKNOWN:
				return "127.0.0.1";
			case LOCAL:
				return "localhost";
			case GETINSURED:
				return "www.getinsured.com";
			case GOOGLE_CDN:
				return "cdn.google.com";
			case AMAZON_S3:
				return "getinsured.s3.amazon.com";
			case RACKSPACE:
				return "cdn.getinsured.com";
			default:
				return "www.getinsured.com";
		}
	}

	/**
	 * @return the cdnType
	 */
	public Type getCdnType()
	{
		return cdnType;
	}

	/**
	 * @param cdnType the cdnType to set
	 */
	public void setCdnType(final Type cdnType)
	{
		this.cdnType = cdnType;
	}
}
