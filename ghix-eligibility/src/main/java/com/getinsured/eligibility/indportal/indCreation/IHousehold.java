package com.getinsured.eligibility.indportal.indCreation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.eligibility.indportal.dto.HouseholdMemberDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO;
import com.getinsured.eligibility.indportal.dto.SsapApplicantsDataDTO;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.ContactPreferences;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SocialSecurityCard;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component
public interface IHousehold {
	public static final Logger LOGGER = LoggerFactory.getLogger(IHousehold.class);
	public Object createResponsiblePerson(HouseholdData householdData);
	public Object createHouseHoldContact(HouseholdData householdData);
	public Object createIndividual(HouseholdData householdData, SsapApplication ssapApplication, com.getinsured.iex.dto.ind19.Ind19ClientRequest ind19ClientRequest) throws GIException;
	public Object createMembers(Object membersObject, SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData, String userName, List<String> membersGUIDs ) throws GIException;
	public Object createDisenrollMembers(Ind19ClientRequest ind19ClientRequest, HouseholdData householdData);
	public void setHouseholdData(SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData) throws GIException;
	public default SingleStreamlinedApplication getSingleStreamlinedApplication(String ssapJson){
		SingleStreamlinedApplication applicationData = null;
		if(StringUtils.isNotBlank(ssapJson)){
			SsapJsonBuilder ssapJsonBuilder = (SsapJsonBuilder) HouseholdFactory.getApplicationContext().getBean("ssapJsonBuilder");
			applicationData = ssapJsonBuilder.transformFromJson(ssapJson);	
		}			
		return applicationData;
	}
	
	public default List<MemberDTO> populateMembersDTO(SingleStreamlinedApplication applicationData,  Map<Long, SsapApplicantResource> ssapApplicants){
		List<MemberDTO> memberDTOList = new ArrayList<>();
		if (applicationData != null) {			
						
			Map<Long, Long> ssapApplicantsPersonIdMap = convertToPersonIdPKeyMap(ssapApplicants);
			//List<SsapApplicantsDataDTO> ssapApplicantsDataList = populateSsapApplicantsDataDTO(ssapApplicants);
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {

				MemberDTO memberDTO = new MemberDTO();
				Integer personId = householdMember.getPersonId();
				memberDTO.setMemberId(String.valueOf(personId));
				String applicantGuid = householdMember.getApplicantGuid();	
				memberDTO.setApplicantGuid(applicantGuid);			
				
				memberDTO.setExternalMemberId(householdMember.getExternalId());
				populateHoueholdMemberName(householdMember.getName(), memberDTO);
				populateSocialSecurityNumber(householdMember.getSocialSecurityCard(), memberDTO);
				
				if(householdMember.getDateOfBirth() != null){
					memberDTO.setDob(dateFormatString(householdMember.getDateOfBirth()));
				}
				if(StringUtils.isNotBlank(householdMember.getGender())){
					if ("male".equalsIgnoreCase(householdMember.getGender())) {
						memberDTO.setGenderCode("M");
					} else if ("female".equalsIgnoreCase(householdMember.getGender())) {
						memberDTO.setGenderCode("F");
					}
				}
				
				memberDTO.setMaritalStatusCode("R");
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if ("CA".equalsIgnoreCase(stateCode)) {
					if(householdMember.getMarriedIndicatorCode() != null){
						memberDTO.setMaritalStatusCode(householdMember.getMarriedIndicatorCode());					
					}
				}
				else if(householdMember.getMarriedIndicator() != null && householdMember.getMarriedIndicator()){
					memberDTO.setMaritalStatusCode("M");					
				}
				
				if(householdMember.getCitizenshipImmigrationStatus()!=null) {
					if(householdMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null && householdMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
						memberDTO.setCitizenshipStatusCode("1");
					}else{
						memberDTO.setCitizenshipStatusCode("3");
					}
				}						
				
				if(householdMember.getSocialSecurityCard() != null && StringUtils.isNotBlank(householdMember.getSocialSecurityCard().getSocialSecurityNumber())){
					memberDTO.setSsn(householdMember.getSocialSecurityCard().getSocialSecurityNumber());
				}
				
				if(householdMember.getLivesWithHouseholdContactIndicator() !=null){
					if(householdMember.getLivesWithHouseholdContactIndicator()){
						if( householdMember.getHouseholdContact() != null && householdMember.getHouseholdContact().getHomeAddress() != null) {
							populateHomeAddress(householdMember.getHouseholdContact().getHomeAddress(), memberDTO);
							if(householdMember.getHouseholdContact().getHomeAddress().getCountyCode() != null) {
								memberDTO.setCountyCode(householdMember.getHouseholdContact().getHomeAddress().getCountyCode());
								if(LOGGER.isWarnEnabled()){
									LOGGER.warn("CountyCode from homeAddress = {}", householdMember.getHouseholdContact().getHomeAddress().getCountyCode());
								}
							}else if(householdMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode() != null) {
								memberDTO.setCountyCode(householdMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode());
								if(LOGGER.isWarnEnabled()){
									LOGGER.warn("CountyCode from PrimaryAddressCountyFipsCode = {}", householdMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode());
								}
							}
						}
					}else{
						
						Address otherAddress = new Address();
						if(householdMember.getAddressId() != 0 && applicationData.getTaxHousehold().get(0).getOtherAddresses() != null && applicationData.getTaxHousehold().get(0).getOtherAddresses().size() > 0) {
							for(Address otherAddressObj : applicationData.getTaxHousehold().get(0).getOtherAddresses()) {
								if(otherAddressObj.getAddressId() == householdMember.getAddressId()) {
									otherAddress = otherAddressObj;
								}
							}
						} else if(householdMember.getOtherAddress() != null && householdMember.getOtherAddress().getAddress() != null && householdMember.getOtherAddress().getAddress().getPostalCode() != null){
							otherAddress = householdMember.getOtherAddress().getAddress();	
						}
						
						memberDTO.setHomeAddress1(nullCheckedValue(otherAddress.getStreetAddress1()));
						memberDTO.setHomeAddress2(nullCheckedValue(otherAddress.getStreetAddress2()));
						memberDTO.setHomeCity(nullCheckedValue(otherAddress.getCity()));
						memberDTO.setHomeState(nullCheckedValue(otherAddress.getState()));
						memberDTO.setHomeZip(getFiveDigitZipCode(otherAddress.getPostalCode()));
						memberDTO.setCountyCode(otherAddress.getCountyCode()!=null?otherAddress.getCountyCode():otherAddress.getCounty());
						if(LOGGER.isWarnEnabled()){
							LOGGER.warn("CountyCode form otherAddress = {}", otherAddress.getCountyCode());
						}
					}
				}
								
				if(householdMember.getHouseholdContact() != null){
					populatePhone(householdMember.getHouseholdContact(), memberDTO);
					
					/*if("IND19".equals(objectType)){	
						if(StringUtils.isBlank(memberDTO.getPrimaryPhone())){
							//HIX-58645 Send Primary Contact Phone number in IND 19
							memberDTO.setPrimaryPhone(phoneNumber);
						}						
						populateMailingAddress(householdMember.getHouseholdContact(), memberDTO);
					}	*/				
				}
					
				if (householdMember.isPrimaryTaxFiler() || householdMember.isPrimaryContact()) {
					
					populateRelationShip(householdMember.getBloodRelationship(), memberDTO);						
						
					if(householdMember.getHouseholdContact() != null){
						if(householdMember.getHouseholdContact().getContactPreferences() != null){
							populatePreferredEmail(householdMember.getHouseholdContact().getContactPreferences(), memberDTO);
							populatePreferredPhone(householdMember.getHouseholdContact(), memberDTO);
						}						
						
						/*if("IND57".equalsIgnoreCase(objectType)){
							populateMailingAddress(householdMember.getHouseholdContact(), memberDTO);
						}	*/					
					}											
				}						
				
				if(householdMember.getHouseholdContact() != null){
					if(householdMember.getHouseholdContact().getContactPreferences() != null){
						
						String spokenLanguage = householdMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
						if(StringUtils.isNotBlank(spokenLanguage) && StringUtils.isNotBlank(LanguageEnum.getCode(spokenLanguage))) {
							memberDTO.setSpokenLanguageCode(LanguageEnum.getCode(spokenLanguage));								
						}
						
						String writtenLanguage = householdMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
						if(StringUtils.isNotBlank(writtenLanguage) && StringUtils.isNotBlank(LanguageEnum.getCode(writtenLanguage))) {
							memberDTO.setWrittenLanguageCode(LanguageEnum.getCode(writtenLanguage));								
						}
					}						
				}			
				
				long personIdInLong = personId;
				if((!ssapApplicantsPersonIdMap.isEmpty() && ssapApplicantsPersonIdMap.get(personIdInLong)!=null) 
						&& (ssapApplicants != null && ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong))!=null && ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getTobaccouser()!=null)){
					if(ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getTobaccouser().equalsIgnoreCase("Y")){
						memberDTO.setTobacco("Y");
					}else if(ssapApplicants.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getTobaccouser().equalsIgnoreCase("N")){
						memberDTO.setTobacco("N");
					}
				}
				
				populateRacesAndEthnicities(householdMember.getEthnicityAndRace(), memberDTO);
				
				/*if("IND57".equals(objectType)){
					memberDTO.setMaintenanceReasonCode("25");
				}else if("IND19".equals(objectType)){
					........
				}*/
				memberDTOList.add(memberDTO);			
			}
		}
		
		return memberDTOList;
		
	}
	default Map<Long, Long> convertToPersonIdPKeyMap(Map<Long, SsapApplicantResource> ssapApplicants){
		Map<Long, Long> ssapApplicantsPersonIdMap = new HashMap<Long, Long>();		
		if(ssapApplicants != null && !ssapApplicants.isEmpty()){
			for(Map.Entry<Long, SsapApplicantResource> key:ssapApplicants.entrySet()){
				ssapApplicantsPersonIdMap.put(key.getValue().getPersonId(), key.getKey());				
			}
		}		
		return ssapApplicantsPersonIdMap;
	}
	
	 default List<SsapApplicantsDataDTO> populateSsapApplicantsDataDTO(Map<Long, SsapApplicantResource> ssapApplicants) {
		List<SsapApplicantsDataDTO> ssapApplicantsDataList = new ArrayList<>();
		if(ssapApplicants != null && !ssapApplicants.isEmpty()){
			for(Map.Entry<Long, SsapApplicantResource> key:ssapApplicants.entrySet()){
				SsapApplicantsDataDTO ssapApplicantsDataDTO = new SsapApplicantsDataDTO();
				ssapApplicantsDataDTO.setId(key.getKey());
				ssapApplicantsDataDTO.setPersonId(key.getValue().getPersonId());
				ssapApplicantsDataDTO.setGuid(key.getValue().getApplicantGuid());
				ssapApplicantsDataList.add(ssapApplicantsDataDTO);
			}
		}	
		return ssapApplicantsDataList;
	}
	 
	 default void populateHoueholdMemberName(Name name, HouseholdMemberDTO memberDTO) {
			if(null != name){
				if(StringUtils.isNotBlank(name.getFirstName())){
					memberDTO.setFirstName(name.getFirstName());
				}
				if(StringUtils.isNotBlank(name.getLastName())){
					memberDTO.setLastName(name.getLastName());
				}
				if(StringUtils.isNotBlank(name.getMiddleName())){
					memberDTO.setMiddleName(name.getMiddleName());
				}
				if(StringUtils.isNotBlank(name.getSuffix())){
					memberDTO.setSuffix(name.getSuffix());
				}
			}		
		}
	 
	 default void populateSocialSecurityNumber(SocialSecurityCard socialSecurityCard, HouseholdMemberDTO householdMemberDTO) {
			if(socialSecurityCard != null && StringUtils.isNotBlank(socialSecurityCard.getSocialSecurityNumber())){
				householdMemberDTO.setSsn(socialSecurityCard.getSocialSecurityNumber());
			}		
		}
	 
	 static String dateFormatString(Date date) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			return simpleDateFormat.format(date);
		}
	 
	 default void populateHomeAddress(HomeAddress homeAddress, HouseholdMemberDTO householdMemberDTO) {
			householdMemberDTO.setHomeAddress1(nullCheckedValue(homeAddress.getStreetAddress1()));
			householdMemberDTO.setHomeAddress2(nullCheckedValue(homeAddress.getStreetAddress2()));
			householdMemberDTO.setHomeCity(nullCheckedValue(homeAddress.getCity()));
			householdMemberDTO.setHomeState(nullCheckedValue(homeAddress.getState()));
			householdMemberDTO.setHomeZip(getFiveDigitZipCode(homeAddress.getPostalCode()));		
		}
	 
	 default String nullCheckedValue(String value) {
			if(StringUtils.isBlank(value)) {
				return null;
			}
			return value;
		}
	 
	 default void populatePhone(HouseholdContact householdContact, HouseholdMemberDTO householdMemberDTO) {
		if((householdContact.getPhone() != null && StringUtils.isNotBlank(householdContact.getPhone().getPhoneNumber())) 
				&& (householdContact.getOtherPhone() != null && StringUtils.isNotBlank(householdContact.getOtherPhone().getPhoneNumber()))){
			householdMemberDTO.setPrimaryPhone(householdContact.getPhone().getPhoneNumber());
			householdMemberDTO.setSecondaryPhone(householdContact.getOtherPhone().getPhoneNumber());
		}else if(householdContact.getPhone() != null && StringUtils.isNotBlank(householdContact.getPhone().getPhoneNumber())){
			householdMemberDTO.setPrimaryPhone(householdContact.getPhone().getPhoneNumber());
		} else if(householdContact.getOtherPhone() != null && StringUtils.isNotBlank(householdContact.getOtherPhone().getPhoneNumber())){
			householdMemberDTO.setPrimaryPhone(householdContact.getOtherPhone().getPhoneNumber());
		}
	}
	 
	 	 
	 default void populateRelationShip(List<BloodRelationship> bloodRelationshipList, MemberDTO memberDTO){
			List<MemberDTO.Relationships> relationships = new ArrayList<MemberDTO.Relationships>(1);
			if(bloodRelationshipList != null && !bloodRelationshipList.isEmpty()){
				for (BloodRelationship bloodRelationship : bloodRelationshipList) {
					MemberDTO.Relationships releation = new MemberDTO().new Relationships();
					releation.setIndividualId(bloodRelationship.getIndividualPersonId());
					releation.setMemberId(bloodRelationship.getRelatedPersonId());
					releation.setRelationshipCode(bloodRelationship.getRelation());
					relationships.add(releation);
				}			
			}
			memberDTO.setRelationships(relationships);
			
		}
	
	 
	 default void populatePreferredEmail(ContactPreferences contactPreferences, HouseholdMemberDTO householdMemberDTO) {
			if( contactPreferences != null 
					&& "EMAIL".equalsIgnoreCase(contactPreferences.getPreferredContactMethod())) {
				householdMemberDTO.setPreferredEmail(nullCheckedValue(contactPreferences.getEmailAddress()));
			}
		}
	 
	 default void populatePreferredPhone(HouseholdContact householdContact, HouseholdMemberDTO householdMemberDTO) {
			if( householdContact.getContactPreferences() != null 
					&& "TextMessage".equalsIgnoreCase(householdContact.getContactPreferences().getPreferredContactMethod()) 
					&& householdContact.getPhone() != null) {
				householdMemberDTO.setPreferredPhone(nullCheckedValue(householdContact.getPhone().getPhoneNumber()));
			}
		}
	 
	 default void populateRacesAndEthnicities(EthnicityAndRace ethnicityAndRace, MemberDTO memberDTO) {
			if(ethnicityAndRace != null) {
				//List<Ethnicity> ethnicities = ethnicityAndRace.getEthnicity();
				List<Race> races = ethnicityAndRace.getRace();
				
				if(races != null) {
					for (Race race : races) {
						MemberDTO.Race raceOjb = new MemberDTO().new Race();
						raceOjb.setRaceEthnicityCode(race.getCode());	
						/*if("0000-0".equalsIgnoreCase(race.getCode())){
							continue;
						}*/
						if("2131-1".equalsIgnoreCase(race.getCode())){
							raceOjb.setRaceEthnicityDescription(race.getOtherLabel());
						}
						memberDTO.getRace().add(raceOjb);
					}
				}
				
				/*if(ethnicities != null) {
					for (Ethnicity ethnicity : ethnicities) {
						MemberDTO.Race raceOjb = new MemberDTO().new Race();
						raceOjb.setRaceEthnicityCode(ethnicity.getCode());	
						if("0000-0".equalsIgnoreCase(ethnicity.getCode())){
							continue;
						}
						if("2131-1".equalsIgnoreCase(ethnicity.getCode())){
							raceOjb.setRaceEthnicityDescription(ethnicity.getOtherLabel());
						}
						memberDTO.getRace().add(raceOjb);
					}
				}*/
				
			}
		}	
	
	 public default HouseholdMemberDTO getHouseholdMemberDTO(SingleStreamlinedApplication applicationData, String requiredPersonType) {
			HouseholdMemberDTO householdMemberDTO = new HouseholdMemberDTO();
			int memberPersonId=1; // default as per the existing logic
			if (applicationData != null) {			
				List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
				 memberPersonId  = 
							members.stream()
						              .filter(member->member.getApplicantPersonType() != null
						              && member.getApplicantPersonType().getPersonType() != null 
						              && member.getApplicantPersonType().getPersonType().contains(requiredPersonType))
						              .map(HouseholdMember::getPersonId)
						              .findAny()
						              .orElse(1); // Return the PersonId as 1 if there is no PC/PTF present in the stream
							
				householdMemberDTO= getHouseholdMemberDTO(applicationData,memberPersonId );
			}
			
		return householdMemberDTO;
	 }
	 
	public default HouseholdMemberDTO getHouseholdMemberDTO(SingleStreamlinedApplication applicationData,int memberPersonId) {
		HouseholdMemberDTO householdMemberDTO = new HouseholdMemberDTO();
		if (applicationData != null) {			
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {
				if (householdMember.getPersonId() == memberPersonId) {
					householdMemberDTO.setId(householdMember.getApplicantGuid());
					populateHoueholdMemberName(householdMember.getName(), householdMemberDTO);
					populateSocialSecurityNumber(householdMember.getSocialSecurityCard(), householdMemberDTO);						
					if(householdMember.getHouseholdContact() != null){
						populatePhone(householdMember.getHouseholdContact(), householdMemberDTO);
						populatePreferredEmail(householdMember.getHouseholdContact().getContactPreferences(), householdMemberDTO);
						populatePreferredPhone(householdMember.getHouseholdContact(), householdMemberDTO);		
						if( householdMember.getHouseholdContact().getHomeAddress() != null) {
							populateHomeAddress(householdMember.getHouseholdContact().getHomeAddress(), householdMemberDTO);							
						}
					}					
					householdMemberDTO.setType("QD");
				}
			}
		}
		return householdMemberDTO;
	}
	
	default String getProductType(Group group, long applicationId, String applicationType){
		return null;	
	}
		
	public default HouseholdMember getResponsibleMemberData(SingleStreamlinedApplication applicationData) {
		HouseholdMember responsibleMember = null;
		if (applicationData != null) {			
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			if(members !=null && !members.isEmpty()){
				for (HouseholdMember householdMember : members) {
					if(householdMember.isPrimaryTaxFiler()){
						responsibleMember = householdMember;
						break;
					}
				}	
			}
			
		 }
		
	return responsibleMember;
 }
	
	default String getFiveDigitZipCode(String postalCode) {
		postalCode = nullCheckedValue(postalCode);
		if(postalCode != null && postalCode.length() > 5){
			postalCode = postalCode.substring(0, 5);
		}
		return postalCode;
	}
		
}
