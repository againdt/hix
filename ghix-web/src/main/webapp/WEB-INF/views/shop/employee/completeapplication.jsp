<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="gutter10">
  <div class="row-fluid">
      <div class="row-fluid">
          <h1>Apply for Employee Coverage</h1>
      </div>
      <div class="row-fluid">
        <div class="span3" id="sidebar">
          <h4 class="graydrkbg">Steps</h4>
          <ol class="nav nav-list">
             <li><a href="<c:url value="/shop/employee/planselection/gettingstarted" />"> <i class="icon-ok"></i>Getting Started</a></li>
            <li><a href="<c:url value="/shop/employee/planselection/verifyemployee" />"> <i class="icon-ok"></i>Verify Information</a></li>
            <li><a href="<c:url value="/shop/employee/planselection/verifydependent" />"> <i class="icon-ok"></i>Verify Dependents</a></li>
            <li><a href="<c:url value="/shop/employee/planselection/affordabilitynotice" />"> <i class="icon-ok"></i>Affordability Notice</a></li>
            <li><a href="<c:url value="/shop/employee/planselection/selectplan" />"> <i class="icon-ok"></i>Select a Plan</a>
            <li><a href="<c:url value="/shop/employee/planselection/signapplication" />"> <i class="icon-ok"></i>Sign Application</a></li>
          </ol>
          <div class="gutter10"> <a href="waivecoverage" rel="tooltip" data-placement="top" data-original-title="If you have alternate coverage, you may choose to waive employer coverage" class="info link">Waive Employer Coverage <i class="icon-question-sign"></i></a> </div>
        </div>
        <!-- /.sidebar-->
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
            <h4 class="graydrkbg gutter10">Application Complete</h4>
            <div class="gutter10">
                    <p> Thank You, An email confirmation has been sent to you.
                    Please note, you have until ${EnrollmentEndDate} to come back and make adjustments
                    to your selection if you change your mind. After ${EnrollmentEndDate} your selection
                    will be processed and coverage will begin on ${CoverageDate}</p>
                    <a data-original-title="" href="#" class="btn btn-small" onclick="window.print();"><i class="icon-print"></i>&nbsp;Print for your records </a>
                <hr>

                <h4> ${EmployeeName} Sponsored health Coverage</h4>
                                 <div class="table-border">
                			             <div class="gutter10 cart">
                
                <div class="control-group">
                  <div class="controls">
                  <label>Covered Lives : ${coveredLives}</label> 
                  </div>
                </div>
                <div class="control-group">
                  <div class="controls"><label>Coverage Starts : ${CoverageDate}</label></div>
                </div>
                <div class="control-group">
                  <div class="controls"><label>Plan ID : ${CartPlan.id}</label></div>
                </div>
			             <table class="table table-border cart">
			             <tbody>
			             <tr>
				             <td class="txt-center">
				             		<img src='<c:url value="/resources/img/logo_${CartPlan.issuerText}.png" />' alt="Logo ${CartPlan.issuer}" /><br>
				             		<small><a href="plandetail/${CartPlan.id}">See Plan Information</a></small>
				             </td>
				             <td class="span3">
				             		${CartPlan.name}<br>
				             		<h4 class="planType ${CartPlan.level}"><small>${CartPlan.level}</small></h4>
				             </td>
				             <td class="span4">
				             		Monthly Premium<br>
				             		Employer Contribution</td>
				             <td class="txt-right">
				             		<strong>${CartPlan.totalPremium}<br>
				             		-${CartPlan.employerContribution}</strong>
				             </td>
			             </tr>
			             </tbody></table>
			             </div>
			             <table class="table blue gutter10 margin0">
			             <tbody> 
			             <tr>
			             <td><strong class="gutter10">Total Monthly Payroll Deduction</strong></td>
			             <td class="txt-right total"><h3 class="gutter10 margin0">$${CartPlan.monthlyPayrollDeduction} / month</h3> </td>
			             </tr></tbody></table>
                        </div>

              </div>
              <div class="form-actions margin-top50 form-horizontal">
                <div class="controls"> <a href="<c:url value="/shop/employee/planselection/planselection" />" class="btn"> Edit Selections </a> <a href="#" class="btn btn-primary offset2"> Exit </a> </div>
              </div>
            </div>
            <!--gutter10--> 
          </div>
          <!--row-fluid--> 
        </div>
        <!--rightpanel--> 
      </div>
      <!--row-fluid--> 
  </div>
  <!--gutter10--> 
<script type="text/javascript">
$(document).ready(function(){
$('.info').tooltip();
$('i.icon-ok').parents('li').css('list-style','none');

});

</script>