/* TYPEKIT */
  (function() {
    var config = {
      kitId: 'xer8gub',
      scriptTimeout: 6000
    };
    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
  })();
/* TYPEKIT ENDS */

  $(document).ready(function(){
	  
	//add .header to thead tr
	    $('.table thead tr').addClass('header');
	    
	//clear form after canceling in modal box    
	    $('.clearForm').click(function(){
	        $(this).parents("form").find("input").not(':button,:hidden').val("");
	        $('label.error').hide();
		});
  });


