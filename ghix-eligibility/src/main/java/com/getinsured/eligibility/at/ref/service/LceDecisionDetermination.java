package com.getinsured.eligibility.at.ref.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.SsapUtil;

/**
 * @author chopra_s
 * 
 */
@Component("lceDecisionDetermination")
@Scope("singleton")
public class LceDecisionDetermination {
	private static final Logger LOGGER = Logger.getLogger(LceDecisionDetermination.class);

	public final static String DEMO_HANDLER = "DEMO_HANDLER";

	public final static String DEMO_DOB_HANDLER = "DEMO_DOB_HANDLER";

	public final static String ELG_LOST_HANDLER = "ELG_LOST_HANDLER";

	public final static String APTC_ONLY_HANDLER = "APTC_ONLY_HANDLER";

	public final static String CITIZENSHIP_HANDLER = "CITIZENSHIP_HANDLER";

	public final static String RELATIONSHIP_HANDLER = "RELATIONSHIP_HANDLER";

	public final static String ALL_CHANGES_HANDLER = "ALL_CHANGES_HANDLER";

	public final static String NO_CHANGES_HANDLER = "NO_CHANGES_HANDLER";

	public final static String ERROR_HANDLER = "ERROR_HANDLER";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired private SsapUtil ssapUtil;

	public String determineHandler(long currentApplicationId, EnrollmentAttributesDTO enrolledApplicationAttributes) throws Exception {
		String strHandler = ERROR_HANDLER;

		try {
			LOGGER.info("LceDecisionDetermination starts for current application id - " + currentApplicationId);
			strHandler = executeLogic(currentApplicationId, enrolledApplicationAttributes);
			LOGGER.info("Lce LceDecisionDetermination ends for application id - " + currentApplicationId);
		} catch (Exception e) {
			throw e;
		}
		return strHandler;
	}

	private String executeLogic(long currentApplicationId, EnrollmentAttributesDTO enrolledApplicationAttributes) {
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);

		if (checkIfOnlyDemographic(currentApplication)) {
			return DEMO_HANDLER;
		}
		if (checkIfDobAndDemoChangesOnly(currentApplication)) {
			return DEMO_DOB_HANDLER;
		}
		if (checkIfEntireHouseholdElgLost(currentApplication)) {
			return ELG_LOST_HANDLER;
		}
		if (!checkIfCatastrophicPlan(enrolledApplicationAttributes) && checkChangeOnlyAPTC(currentApplication)) {
			return APTC_ONLY_HANDLER;
		}
		if (checkForCitizenship(currentApplication)) {
			return CITIZENSHIP_HANDLER;
		}
		if (checkForRelationship(currentApplication)) {
			return RELATIONSHIP_HANDLER;
		}
		if (checkAllChangeInReferral(currentApplication)) {
			return ALL_CHANGES_HANDLER;
		} else {
			return NO_CHANGES_HANDLER;
		}
	}

	private boolean checkIfCatastrophicPlan(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes) && Plan.PlanLevel.CATASTROPHIC.toString().equals(enrolledApplicationAttributes.getHealthPlanLevel());
	}

	private boolean checkForRelationship(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (ReferralConstants.RELATIONSHIP_CHANGED.contains(temp)) {
				blnFlag = true;
			} else if (ReferralConstants.NON_RELATIONSHIP_APPLICANT_STATUS.contains(temp)) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}

	private boolean checkForCitizenship(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (ReferralConstants.CITIZEN_SHIP_CHANGED.contains(temp)) {
				blnFlag = true;
			} else if (ReferralConstants.NON_CITIZEN_SHIP_APPLICANT_STATUS.contains(temp)) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}

	private SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

	private boolean checkIfOnlyDemographic(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(temp)) {
				blnFlag = true;
			} else if (ReferralConstants.NON_DEMOGRAPHIC_APPLICANT_STATUS.contains(temp)) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}

	private boolean checkIfDobAndDemoChangesOnly(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (temp == ApplicantStatusEnum.UPDATED_DOB) {
				blnFlag = true;
			} else if (ReferralConstants.NON_DEMO_DOB_APPLICANT_STATUS.contains(temp)) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}

	private boolean checkIfEntireHouseholdElgLost(SsapApplication currentApplication) {
		boolean blnFlag = true;
        final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			blnFlag = false;
		} else {
			for (SsapApplicant ssapApplicant : ssapApplicants) {
				HouseholdMember memberJson = ssapUtil.getApplicantJson(members, ssapApplicant);
				if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, ssapApplicant.getEligibilityStatus()) && memberJson.isSeeksQhp()) {
					blnFlag = false;
					break;
				}
			}
		}
        return blnFlag;
	}

	private boolean checkChangeOnlyAPTC(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (temp == ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT) {
				blnFlag = true;
			} else if (ReferralConstants.NON_APTC_APPLICANT_STATUS.contains(temp)) {
				blnFlag = false;
				break;
			}
		}

		return blnFlag;
	}

	private boolean checkAllChangeInReferral(SsapApplication currentApplication) {
		boolean blnFlag = false;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(temp) || ReferralConstants.NON_DEMOGRAPHIC_APPLICANT_STATUS.contains(temp)) {
				blnFlag = true;
				break;
			}
		}

		return blnFlag;
	}
}
