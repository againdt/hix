package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;

public class Result {
	 private int coverageYear;
	 private BigDecimal insuranceApplicationIdentifier;
	 private int applicationVersionNumber;
	 private String lastModifiedDateTime;
	 private String lastModifiedUserId;
	 private Boolean deletedIndicator;
	 private Boolean currentVersionIndicator;
	 private String versionDateTime;
	 private String applicationStatus;
	 private String comments;
	 private String creatingSystemId;
	 private String creatingSystemRole;
	 private String creatingUserId;
	 private String applicationSubmissionDateTime;
	 private String creationOriginType;
	 private String applicationCreationDateTime;
	 private String linkedSystemUserIdentifier;
	 private String insuranceApplicationType;
	 private String useCase;
	 private String submissionOriginType;
	 private String lastConsumerSubmissionDateTime;
	 private String creatingDePartner;
	 private String submittingDePartner;
	 private Attestations attestations;
	 private Computed computed;


	 // Getter Methods 

	 public int getCoverageYear() {
	  return coverageYear;
	 }

	 public BigDecimal getInsuranceApplicationIdentifier() {
	  return insuranceApplicationIdentifier;
	 }

	 public int getApplicationVersionNumber() {
	  return applicationVersionNumber;
	 }

	 public String getLastModifiedDateTime() {
	  return lastModifiedDateTime;
	 }

	 public String getLastModifiedUserId() {
	  return lastModifiedUserId;
	 }

	 public Boolean getDeletedIndicator() {
	  return deletedIndicator;
	 }

	 public Boolean getCurrentVersionIndicator() {
	  return currentVersionIndicator;
	 }

	 public String getVersionDateTime() {
	  return versionDateTime;
	 }

	 public String getApplicationStatus() {
	  return applicationStatus;
	 }

	 public String getComments() {
	  return comments;
	 }

	 public String getCreatingSystemId() {
	  return creatingSystemId;
	 }

	 public String getCreatingSystemRole() {
	  return creatingSystemRole;
	 }

	 public String getCreatingUserId() {
	  return creatingUserId;
	 }

	 public String getApplicationSubmissionDateTime() {
	  return applicationSubmissionDateTime;
	 }

	 public String getCreationOriginType() {
	  return creationOriginType;
	 }

	 public String getApplicationCreationDateTime() {
	  return applicationCreationDateTime;
	 }

	 public String getLinkedSystemUserIdentifier() {
	  return linkedSystemUserIdentifier;
	 }

	 public String getInsuranceApplicationType() {
	  return insuranceApplicationType;
	 }

	 public String getUseCase() {
	  return useCase;
	 }

	 public String getSubmissionOriginType() {
	  return submissionOriginType;
	 }

	 public String getLastConsumerSubmissionDateTime() {
	  return lastConsumerSubmissionDateTime;
	 }

	 public Attestations getAttestations() {
	  return attestations;
	 }

	 public Computed getComputed() {
	  return computed;
	 }

	 // Setter Methods 

	 public void setCoverageYear(int coverageYear) {
	  this.coverageYear = coverageYear;
	 }

	 public void setInsuranceApplicationIdentifier(BigDecimal insuranceApplicationIdentifier) {
	  this.insuranceApplicationIdentifier = insuranceApplicationIdentifier;
	 }

	 public void setApplicationVersionNumber(int applicationVersionNumber) {
	  this.applicationVersionNumber = applicationVersionNumber;
	 }

	 public void setLastModifiedDateTime(String lastModifiedDateTime) {
	  this.lastModifiedDateTime = lastModifiedDateTime;
	 }

	 public void setLastModifiedUserId(String lastModifiedUserId) {
	  this.lastModifiedUserId = lastModifiedUserId;
	 }

	 public void setDeletedIndicator(Boolean deletedIndicator) {
	  this.deletedIndicator = deletedIndicator;
	 }

	 public void setCurrentVersionIndicator(Boolean currentVersionIndicator) {
	  this.currentVersionIndicator = currentVersionIndicator;
	 }

	 public void setVersionDateTime(String versionDateTime) {
	  this.versionDateTime = versionDateTime;
	 }

	 public void setApplicationStatus(String applicationStatus) {
	  this.applicationStatus = applicationStatus;
	 }

	 public void setComments(String comments) {
	  this.comments = comments;
	 }

	 public void setCreatingSystemId(String creatingSystemId) {
	  this.creatingSystemId = creatingSystemId;
	 }

	 public void setCreatingSystemRole(String creatingSystemRole) {
	  this.creatingSystemRole = creatingSystemRole;
	 }

	 public void setCreatingUserId(String creatingUserId) {
	  this.creatingUserId = creatingUserId;
	 }

	 public void setApplicationSubmissionDateTime(String applicationSubmissionDateTime) {
	  this.applicationSubmissionDateTime = applicationSubmissionDateTime;
	 }

	 public void setCreationOriginType(String creationOriginType) {
	  this.creationOriginType = creationOriginType;
	 }

	 public void setApplicationCreationDateTime(String applicationCreationDateTime) {
	  this.applicationCreationDateTime = applicationCreationDateTime;
	 }

	 public void setLinkedSystemUserIdentifier(String linkedSystemUserIdentifier) {
	  this.linkedSystemUserIdentifier = linkedSystemUserIdentifier;
	 }

	 public void setInsuranceApplicationType(String insuranceApplicationType) {
	  this.insuranceApplicationType = insuranceApplicationType;
	 }

	 public void setUseCase(String useCase) {
	  this.useCase = useCase;
	 }

	 public void setSubmissionOriginType(String submissionOriginType) {
	  this.submissionOriginType = submissionOriginType;
	 }

	 public void setLastConsumerSubmissionDateTime(String lastConsumerSubmissionDateTime) {
	  this.lastConsumerSubmissionDateTime = lastConsumerSubmissionDateTime;
	 }

	 public void setAttestations(Attestations attestations) {
	  this.attestations = attestations;
	 }

	 public void setComputed(Computed computed) {
	  this.computed = computed;
	 }

	public String getCreatingDePartner() {
		return creatingDePartner;
	}

	public void setCreatingDePartner(String creatingDePartner) {
		this.creatingDePartner = creatingDePartner;
	}

	public String getSubmittingDePartner() {
		return submittingDePartner;
	}

	public void setSubmittingDePartner(String submittingDePartner) {
		this.submittingDePartner = submittingDePartner;
	}
	}
