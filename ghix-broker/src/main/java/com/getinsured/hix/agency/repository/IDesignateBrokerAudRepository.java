package com.getinsured.hix.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.agency.DesignateBrokerAud;

public interface IDesignateBrokerAudRepository extends JpaRepository<DesignateBrokerAud, Long> {

}
