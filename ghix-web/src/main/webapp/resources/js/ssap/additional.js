function isPrimaryAddressEmpty() {
		if($("#home_addressLine1").val()==""||$("#home_primary_city").val()==""||$("#home_primary_zip").val()==""||$("#home_primary_state").val()==""||$("#home_primary_county").val()=="")
			{
				return true;
			}
			else return false;
	}

	function addressIsSame() {
		alert("Hello");
		$("#mailingAddressIndicator").prop('value',true);
		var contactHomeAddress = $("#mailingAddressIndicator");
		if (isPrimaryAddressEmpty() && contactHomeAddress.is(":checked")) {
			alert("Primary Address is not filled correctly");
			contactHomeAddress.prop("checked",false) ;

		}
		if (!isPrimaryAddressEmpty()) {
			if (contactHomeAddress.is(":checked")) {
				
				
				
				$("#mailing_addressLine1").val($("#home_addressLine1").val());
				$("#mailing_addressLine2").val($("#home_addressLine2").val());
				$("#mailing_primary_city").val($("#home_primary_city").val());
				$("#mailing_primary_zip").val($("#home_primary_zip").val());
				$("#mailing_primary_state").val($("#home_primary_state").val());
				$("#mailing_primary_county").val($("#home_primary_county").val());
				
				$("#mailing_addressLine1").prop("disabled",true);
				$("#mailing_addressLine2").prop("disabled",true);
				$("#mailing_primary_city").prop("disabled",true);
				$("#mailing_primary_zip").prop("disabled",true);
				$("#mailing_primary_state").prop("disabled",true);
				$("#mailing_primary_county").prop("disabled",true);
				$("#home_addressLine1").keyup(function(event){
					var valueToSet=$("#home_addressLine1").val();
						$("#mailing_addressLine1").val(valueToSet);
						 event.stopPropagation();
					});
				$("#home_addressLine2").keyup(function(event){
					
					var valueToSet=$("#home_addressLine2").val();
					$("#mailing_addressLine2").val(valueToSet);
					event.stopPropagation();
					});
				$("#home_primary_city").keyup(function(event){
					var valueToSet=$("#home_primary_city").val();
					$("#mailing_primary_city").val(valueToSet);
					event.stopPropagation();
					});
				$("#home_primary_zip").keyup(function(event){
					var valueToSet=$("#home_primary_zip").val();
					$("#mailing_primary_zip").val(valueToSet);
					event.stopPropagation();
					});
				$("select[name=home_primary_state]").change(function(event){
					var valueToSet='';
					$("select[name=home_primary_state] option:selected").each(function(){
						 valueToSet=$(this).val();
						 $("#mailing_primary_state").val(valueToSet);
						});
					
					}).change();
				$("select[name=home_primary_county]").change(function(){
					var valueToSet='';
					$("select[name=home_primary_county] option:selected").each(function(){
						 valueToSet=$(this).val();
						 $("#mailing_primary_county").val(valueToSet);
						});
					
					}).change();
				
			} else if (!contactHomeAddress.is(":checked")) {
				$("#mailing_addressLine1").val("");
				$("#mailing_addressLine2").val("");
				$("#mailing_primary_city").val("");
				$("#mailing_primary_zip").val("");
				$("#mailing_primary_state").val("");
				$("#mailing_primary_county").val("");
				$("#mailing_addressLine1").prop("disabled",false);
				$("#mailing_addressLine2").prop("disabled",false);
				$("#mailing_primary_city").prop("disabled",false);
				$("#mailing_primary_zip").prop("disabled",false);
				$("#mailing_primary_state").prop("disabled",false);
				$("#mailing_primary_county").prop("disabled",false);
				}
			}
	}

function changePrimaryStateInMailingAddress(obj) {
	
}	
