<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.LocalDate"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration.IEXConfigurationEnum"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style>
	.datepicker { 
	    z-index: 1050;
	}
</style>

<script type="text/javascript">
var screener =[];
$(document).ready(function(){
	screener['screener.label.errorMessage.dateFormat'] = $('#errorMessageDateFormat').text();
	screener['screener.label.errorMessage.dateRequired'] = $('#errorMessageDateRequired').text();
	screener['screener.label.errorMessage.age18'] = $('#errorMessageAge18').text();
	screener['screener.label.errorMessage.age120'] = $('#errorMessageAge120').text();
	screener['screener.label.errorMessage.age26'] = $('#errorMessageAge26').text();
	screener['screener.label.errorMessage.dateFuture'] = $('#errorMessageDateFuture').text();
})

</script>
<script src="<c:url value="/resources/angular/mask.js"/>"></script>
<script src="<c:url value="/resources/js/ui-bootstrap-tpls-0.12.0.min.js"/>"></script>
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>

<%
	String pregnancyIndicatorConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_PREGNANCY_INDICATOR);
	String selectedCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
	String exchangeStartYear = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR);
	String previousCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
	String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
    String currentCoverageYearOptionStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_SHOW_CURRENT_YEAR);
    String previousCoverageYearOptionEndDate = "11/15/";
    String showNumberOfPreviousYearStr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_SHOW_NUMBER_OF_PREV_YEAR);
    boolean multiYearEnabled = false;
    int showNumberOfPreviousYear = 0;

	  try{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		    if(currentCoverageYearOptionStartDate != null && currentCoverageYearOptionStartDate.trim().length() != 0){
				LocalDate todaysDate = new LocalDate();
				LocalDate cutOffDate = new DateTime(formatter.parseObject(currentCoverageYearOptionStartDate)).toLocalDate();
				if (todaysDate.isAfter(cutOffDate) || todaysDate.isEqual(cutOffDate)) {
					multiYearEnabled = true;
					selectedCoverageYear = currentCoverageYear;
				}
		    }

		    if(previousCoverageYear != null && previousCoverageYear.trim().length() != 0){
		    	previousCoverageYearOptionEndDate = previousCoverageYearOptionEndDate+previousCoverageYear;
				LocalDate todaysDate = new LocalDate();
				LocalDate cutOffDate = new DateTime(formatter.parseObject(previousCoverageYearOptionEndDate)).toLocalDate();
				if (todaysDate.isAfter(cutOffDate)) {
					multiYearEnabled = false;
				}
		    }

		    if (null != showNumberOfPreviousYearStr && 0 < showNumberOfPreviousYearStr.trim().length()) {
		    	showNumberOfPreviousYear = Integer.valueOf(showNumberOfPreviousYearStr);

				if (0 < showNumberOfPreviousYear) {
					multiYearEnabled = true;
				}
		    }
		    
		    if(exchangeStartYear != null && previousCoverageYear != null){
		    	int exchangeStartYearInt = Integer.valueOf(exchangeStartYear);
		    	int previousCoverageYearInt = Integer.valueOf(previousCoverageYear);
		    	if(previousCoverageYearInt < exchangeStartYearInt){
		    		multiYearEnabled = false;
		    	}	
		    }
		}
		catch(Exception ex){
		}
		pageContext.setAttribute("pregnancyIndicatorConfig", pregnancyIndicatorConfig);
	  	pageContext.setAttribute("leadPresent", request.getSession().getAttribute("leadId"));
		pageContext.setAttribute("multiYearEnabled", multiYearEnabled);
		pageContext.setAttribute("selectedCoverageYear", selectedCoverageYear);
		pageContext.setAttribute("showNumberOfPreviousYear", showNumberOfPreviousYear);
%>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<div data-ng-app="benefitsApp" class="preEligibilityPage" data-ng-controller="DisclaimerCtrl" data-ng-init="_init('${stateCode}', '${leadPresent}')" data-ng-cloak>

  <div class="margin20-t gutter10" data-ng-include="'disclaimer'" data-ng-show="showDisclaimer"></div>

  <div class="gutter10" data-ng-show="!showDisclaimer">
    <div id="titlebar">
    	<h1>
    		<c:choose>
	    		<c:when test='${stateCode == "MN" || stateCode == "ID"}'>
	    			<spring:message code="label.iex.sidebar.header" javaScriptEscape="true"/>
	    		</c:when>
	    		<c:otherwise>
	    			<spring:message code="label.iex.sidebar.header" javaScriptEscape="true"/>&nbsp;<span class="coverageYear">${selectedCoverageYear}</span>
	    		</c:otherwise>
    		</c:choose>
    	</h1>
    </div>
    <div class="row-fluid margin20-t eligibility-wrapper">
      <div class="span3" id="sidebar">
        <div class="header">
          <h2><spring:message code="label.iex.sidebar.subHeader" javaScriptEscape="true"/></h2>
        </div>
        <div class="gutter10 border-custom">
            <div data-ng-if="requiredNotice" class="center margin10-b">
            	<small>
            		<spring:message code="label.iex.sidebar.required1" javaScriptEscape="true"/>
            		<img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
            		<spring:message code="label.iex.sidebar.required2" javaScriptEscape="true"/>
            	</small>
            </div>
          <ul>
              <li><spring:message code="label.iex.sidebar.formExplanation1" javaScriptEscape="true"/></li>
			<c:if test='${stateCode != "NV"}'>
              <li><spring:message code="label.iex.sidebar.formExplanation4" javaScriptEscape="true"/>

              	<c:if test='${stateCode != "CT" && stateCode != "MN"}'>
              		<span class="coverageYear">${selectedCoverageYear}</span> <spring:message code="label.iex.sidebar.formExplanation5" javaScriptEscape="true"/>
              	</c:if>
              </li>
			</c:if>
          </ul>
        </div>
      </div>
        <div data-ng-view class="span9" id="rightpanel" role="application">
        </div>
    </div>
  </div>
  <div id="errorMessages" class="hide">
  	<span id="errorMessageDateFormat"><spring:message code="label.iex.screener.errorMessage.dateFormat"/></span>
	<span id="errorMessageDateRequired"><spring:message code="label.iex.screener.errorMessage.dateRequired"/></span>
	<span id="errorMessageAge18"><spring:message code="label.iex.screener.errorMessage.age18"/></span>
	<span id="errorMessageAge120"><spring:message code="label.iex.screener.errorMessage.age120"/></span>
	<span id="errorMessageAge26"><spring:message code="label.iex.screener.errorMessage.age26"/></span>
	<span id="errorMessageDateFuture"><spring:message code="label.iex.screener.errorMessage.dateFuture"/></span>
  </div>



<script type="text/ng-template" id="disclaimer">
	<h1><spring:message code="label.iex.disclaimer.header" /></h1>
	<div class="gutter20-lr">
		<c:if test='${stateCode == "CT"}'>
			<h3>
				<spring:message code="label.iex.disclaimer.subHeader" />
			</h3>
            <p>
		</c:if>
        <c:if test='${stateCode != "CT"}'>
		<p class="gutter20-t">
        </c:if>
			<spring:message code="label.iex.disclaimer.content1" />
		</p>

		<div class="margin20-t">
			<c:if test='${stateCode == "CT"}'>
				<p><spring:message code="label.iex.disclaimer.content2" /></p>
			</c:if>
			<ul>
				<li><spring:message code="label.iex.disclaimer.content3" /></li>
				<li><spring:message code="label.iex.disclaimer.content4" /></li>
				<li><spring:message code="label.iex.disclaimer.content5" /></li>
			</ul>
		</div>

		<div class="margin20-t">
			<p><spring:message code="label.iex.disclaimer.content6" /></p>
			<c:if test='${stateCode != "CT"}'>
				<p><spring:message code="label.iex.disclaimer.content6.1" /></p>
			</c:if>
			<c:if test='${stateCode == "CT"}'>
				<ul>
					<li><spring:message code="label.iex.disclaimer.content7" /></li>
					<li>
						<spring:message code="label.iex.disclaimer.content8" />
						<ul class="margin10-t margin10-b">
							<li><spring:message code="label.iex.disclaimer.content9" /></li>
							<li><spring:message code="label.iex.disclaimer.content10" /></li>
						</ul>
					</li>
					<li><spring:message code="label.iex.disclaimer.content11" /></li>
					<li><spring:message code="label.iex.disclaimer.content12" /></li>
					<li><spring:message code="label.iex.disclaimer.content13" /></li>
				</ul>
			</c:if>
			<c:if test='${stateCode != "CT"}'>
				<ul>
					<li><spring:message code="label.iex.disclaimer.content7" /></li>
					<li><spring:message code="label.iex.disclaimer.content8" /></li>
					<li><spring:message code="label.iex.disclaimer.content9" /></li>
				</ul>
				<p><strong><spring:message code="label.iex.disclaimer.content10" /></strong> <spring:message code="label.iex.disclaimer.content11" /></p>
				<ul>
					<li><spring:message code="label.iex.disclaimer.content12" /></li>
					<li><spring:message code="label.iex.disclaimer.content13" /></li>
					<li><spring:message code="label.iex.disclaimer.content14" /></li>
					<li><spring:message code="label.iex.disclaimer.content15" />
						<ul class="margin10-t margin10-b">
							<li><spring:message code="label.iex.disclaimer.content16" /></li>
							<li><spring:message code="label.iex.disclaimer.content17" /></li>
							<li><spring:message code="label.iex.disclaimer.content18" /></li>
						</ul>
					</li>
					<li><spring:message code="label.iex.disclaimer.content19" /></li>
					<li><spring:message code="label.iex.disclaimer.content20" /></li>
					<li><spring:message code="label.iex.disclaimer.content21" /></li>
				</ul>
			</c:if>
		</div>

		</div>
		<div class="txt-right form-actions">
			<c:if test='${stateCode == "CT"}'>
				<a class="btn btn-primary" href="#" data-ng-click="agree()"><spring:message code="label.iex.disclaimer.agree" /></a>
				<a class="btn" href="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)%>"><spring:message code="label.iex.disclaimer.notAgree" /></a>
			</c:if>
			<c:if test='${stateCode == "MN"}'>
					<a class="btn btn-primary" href="#pp1" data-ng-click="agree()"><spring:message code="label.iex.disclaimer.continue" /></a>
			</c:if>
		</div>
</script>
  <!-- HOUSEHOLD INFO PAGE -->
<script type="text/ng-template" id="pp1">

<!-- Prescreen form starts -->
<form novalidate name="prescreen" class="form-horizontal">

	<sec:authorize access="!isAuthenticated()" var="isUserAuthenticated">
		<div class="question-group" data-ng-init="initPrescreen(false, '${leadPresent}', ${multiYearEnabled}, ${selectedCoverageYear}, ${showNumberOfPreviousYear})">
	</sec:authorize>
	<sec:authorize access="isAuthenticated()" var="isUserAuthenticated">
		<div class="question-group" data-ng-init="initPrescreen(true, '${leadPresent}', ${multiYearEnabled}, ${selectedCoverageYear}, ${showNumberOfPreviousYear})">
	</sec:authorize>

	<!-- Coverage year selection starts-->
	<div id="coverage-year-div" ng-show="multiYearEnabled === true && showIncomeSection === true">
		<div class="header">
    		<h2><spring:message code="label.iex.prescreen.qCoverageYear" javaScriptEscape="true"/></h2>
		</div>
		<div class="control-group gutter10">
			<label class="control-label" for="coverageYear">
				<spring:message code="label.iex.prescreen.selectCoverageYear" javaScriptEscape="true"/>: <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
			</label>
			<div class="controls">
				<select id="coverageYear" class="input-small" data-ng-model="formInfo.coverageYear" data-ng-init="coverageYearChange('${selectedCoverageYear}')" data-ng-change="coverageYearChange('${selectedCoverageYear}')" data-ng-options="coverageYear for coverageYear in coverageYearList"></select>

				<div data-ng-show="'${selectedCoverageYear}' != formInfo.coverageYear" class="input-append date date-picker" data-date-format="mm/dd/yyyy">&nbsp;
					<label class="control-label" for="coverageStartDate">
						<spring:message code="label.effectiveStartDate" javaScriptEscape="true"/>: <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
					</label>
					<input type="text" id="coverageStartDate" data-ng-model="formInfo.coverageStartDate" class="datepick input-small" title="MM/dd/yyyy" pattern="MM/DD/YYYY" ng-readonly="true">
					<span class="datepick add-on" for="coverageStartDate"><i class="icon-calendar"></i></span>
				</div>
			</div> <!-- end of controls-->
		</div>
	</div>
	<!-- Coverage year selection ends-->

	<c:if test="${multiYearEnabled == false}">
		<input id="gCoverageYear" name="gCoverageYear" type="hidden" value="${defaultCoverageYear}">
	</c:if>


	<!-- Zipcode starts-->
	<div class="header" id="zip-anchor-div">
		<h2 id="zipAnchor"><spring:message code="label.iex.prescreen.qAddress" javaScriptEscape="true"/></h2>
	</div>
	<div id="zipEntry" class="control-group gutter10">
		<label class="control-label" id="ariaZipCodeLabel" for="zipCode">
			<spring:message code="label.iex.prescreen.promZip" javaScriptEscape="true"/>: <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
		</label>
		<div class="controls">
			<input id="zipCode" class="input-small" aria-invalid="false" name="zipCode" aria-labelledby="ariaZipCodeLabel ariaZipCodeError" type="text" data-ng-model='formInfo.zipCode' data-ng-blur="submitZip()" data-ng-change='zipChange()' data-container="body" data-toggle="popover" data-placement="bottom" maxlength="5" required tabindex="0">
			<span data-ng-if="formInfo.showOneCounty" aria-live="assertive">
				@@formInfo.countyName@@

			</span>
			<span id="ariaZipCodeError" aria-live="assertive" class="inline-block">
				<span class="popover-content data-ng-binding zip-warning redBorder" data-ng-if="!zipWarning && countyNotSelected" tabindex="0">
					<spring:message code="label.iex.prescreen.errCountyNotSelected" javaScriptEscape="true"/>
				</span>
				<span class="popover-content data-ng-binding zip-warning redBorder" data-ng-if="zipWarning" tabindex="0">
					<spring:message code="label.iex.prescreen.errInvalidZip" javaScriptEscape="true"/>
				</span>
			</span>
		</div>
	</div>
	<!-- Zipcode ends-->

	<!-- Multi-county starts-->
	<div class="control-group gutter10" data-ng-if='formInfo.showMultipleCounties'>
		<label class="control-label" for="countyname">
			<spring:message code="label.iex.prescreen.multiCounty" javaScriptEscape="true"/><img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
		</label>
		<div class="controls">
			<select id="countyname" data-ng-show="formInfo.showMultipleCounties" data-ng-options="item.codeCombined as item.nameCombined for item in formInfo.countyInfo" data-ng-model="formInfo.selectedCounty" data-ng-change="multiCountyChange()">
				<option value="" required><spring:message code="label.iex.prescreen.multiCountyOption" javaScriptEscape="true"/></option>
			</select>
		</div>
	</div>
	<!-- Multi-county ends-->

	<!-- Family member starts-->
	<div class="question-group">
		<div class="header">
			<h2 class="need-coverage">
				<spring:message code="label.iex.prescreen.qFamilyInfo" javaScriptEscape="true"/> <span class="aria-hidden"> <spring:message code="label.iex.prescreen.tableBelow" javaScriptEscape="true"/></span>
			</h2>
		</div>

		<!-- FAMILY MEMEBER MOBILE CONTAINER STARTS -->
		<!-- Family member container starts-->
		<div id="family-members-container" role="grid" class="gutter10-tb">
			<!-- Family member header starts-->
			<div class="row-fluid member-info-header-wrapper hidden-small" role="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 family-members-container-header family-members-container-header_member">
					<spring:message code="label.iex.prescreen.members" javaScriptEscape="true"/>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 dob family-members-container-header">
					<spring:message code="label.iex.prescreen.birthdate" javaScriptEscape="true"/><img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 tobacco family-members-container-header" data-ng-if="'${stateCode}' !== 'CT'">
					<a
						tabindex="0"
						data-toggle="tooltip"
						rel="tooltip"
						data-placement="bottom"
						role="tooltip"
						aria-tooltip
						data-original-title="<strong><spring:message code="label.iex.prescreen.tobaccoUse" javaScriptEscape="true"/></strong><br><spring:message code="label.iex.prescreen.tobaccoUseTooltip" javaScriptEscape="true"/>"
						data-html="true">
						<spring:message code="label.iex.prescreen.tobaccoUse" javaScriptEscape="true"/>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 native family-members-container-header" data-ng-if="'${stateCode}' !== 'CT'">
					<a
						data-ng-if="'${stateCode}' === 'MN'"
						tabindex="0"
						data-toggle="tooltip"
						rel="tooltip"
						data-placement="left"
						role="tooltip"
						aria-tooltip
						data-original-title="<strong><spring:message code="label.iex.prescreen.nativeAmerican" javaScriptEscape="true"/></strong><br><spring:message code="label.iex.prescreen.nativeAmericanTooltip"/>"
						data-html="true">
						<spring:message code="label.iex.prescreen.nativeAmerican" javaScriptEscape="true"/>
					</a>
					<span data-ng-if="'${stateCode}' !== 'MN'">
						<spring:message code="label.iex.prescreen.nativeAmerican" javaScriptEscape="true"/>
					</span>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pregnant family-members-container-header" data-ng-if="'${pregnancyIndicatorConfig}' === 'ON'">
					<spring:message code="label.iex.prescreen.pregnant" javaScriptEscape="true"/>?
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 coverage family-members-container-header">
					<a
						tabindex="0"
						data-toggle="tooltip"
						rel="tooltip"
						data-placement="bottom"
						role="tooltip"
						aria-tooltip
						data-original-title="<spring:message code="label.iex.prescreen.seekingCoverageTooltip"/>"
						data-html="true">
						<spring:message code="label.iex.prescreen.seekingCoverage" javaScriptEscape="true"/>
					</a>
				</div>
				<div></div>
			</div>

			<!-- Family-members-container ends-->
			<div data-ng-repeat="member in formInfo.family" class="row-fluid member-info-block fadeIn" data-ng-class="member.type === 'SELF'? 'member-info-block_self':'member-info-block_others'">
				<ng-form name='indiv'>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="family-members-container-body family-members-container-body_member">
						<span class="member-label">
							<span data-ng-if="member.type == 'SELF'"><spring:message code="label.iex.prescreen.YOU" javaScriptEscape="true"/></span>
							<span data-ng-if="member.type == 'SPOUSE'"><spring:message code="label.iex.prescreen.spouse" /></span>
							<span data-ng-if="member.type == 'CHILD'"><spring:message code="label.iex.prescreen.child" /></span>

							<a href="javascript:void(0);" data-ng-click="remove($index, member.type)" data-ng-if="member.type !== 'SELF' && '${stateCode}' === 'MN'" role="button" class="remove-member">
								<i class="icon-remove-sign">
									<span class="aria-hidden">
										<spring:message code="label.iex.prescreen.remove" javaScriptEscape="true"/>
										<span data-ng-if="member.type === 'SPOUSE'">
											<spring:message code="label.iex.prescreen.spouse" javaScriptEscape="true"/>
										</span>
										<span data-ng-if="member.type === 'CHILD'">
											<spring:message code="label.iex.prescreen.child" javaScriptEscape="true"/>
										</span>
									</span>
								</i>
							</a>
						</span>

					</div>
				</div>
				<div class="col-xs-12">

					<div class="hide details member-info-section__detail-column__header col-xs-5">
						<spring:message code="label.iex.prescreen.birthdate" javaScriptEscape="true"/>
						<img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</div>


					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 family-members-container-body family-members-container-body_dob">
						<div class="aria-hidden" id="birthdate@@$index+1@@-label">
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantBirthdate" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spouseBirthdate" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childBirthdate" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.errorMsg">@@member.errorMsg@@</span>
						</div>
                      	<input
							class="member-birthday input-small"
							data-ng-class="member.preloaded"
							type="text"
							name="birthdate@@$index + 1@@"
							id="birthdate@@$index+1@@"
							placeholder="mm/dd/yyyy"
							ui-mask="99/99/9999"
							ui-mask-use-viewvalue="true"
							data-ng-model="member.birthday"
							date-check mtype="@@member.type@@"
							order="@@$index@@"
							popover="@@member.errorMsg@@"
							popover-trigger="blur"
							popover-placement="top"
							aria-invaid="false"
							aria-labelledby="birthdate@@$index+1@@-label"
							required>

							<span class="aria-hidden" data-ng-if="member.errorMsg" tabindex="0">@@member.errorMsg@@</span>
                    </div>
				</div>
				<div class="col-xs-12">
                    <div class="hide details member-info-section__detail-column__header col-xs-5" data-ng-if="'${stateCode}' !== 'CT'">
						<spring:message code="label.iex.prescreen.tobaccoUse" javaScriptEscape="true"/>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 family-members-container-body" data-ng-if="'${stateCode}' !== 'CT'">
						<div class="aria-hidden" id="tobacco@@member.id@@-label">
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantTobaccoUse" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spouseTobaccoUse" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childTobaccoUse" javaScriptEscape="true"/>
							</span>
						</div>
                      	<input
							class="tobacco-input gtm_tobacco_@@member.type@@"
							type="checkbox"
							data-ng-change="updateGoogleTracking(member, 'tobaccoUse')"
							name="tobacco@@member.id@@"
							id="tobacco@@member.id@@"
							data-ng-disabled="member.disableTobacco"
							data-ng-model="member.tobaccoUse"
							aria-labelledby="tobacco@@member.id@@-label"/>
                    </div>
				</div>
				<div class="col-xs-12">
                    <div class="col-xs-5 hide details member-info-section__detail-column__header" data-ng-if="'${stateCode}' !== 'CT'">
						<spring:message code="label.iex.prescreen.nativeAmerican" javaScriptEscape="true"/>
                    </div>


                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 family-members-container-body" data-ng-if="'${stateCode}' !== 'CT'">
						<div class="aria-hidden" id="nativeAmerican@@member.id@@-label">
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantNativeAmerican" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spouseNativeAmerican" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childNativeAmerican" javaScriptEscape="true"/>
							</span>
						</div>
                      	<input
							class="coverage-input"
							type="checkbox"
							name="nativeAmerican@@member.id@@"
							id="nativeAmerican@@member.id@@"
							data-ng-model="member.nativeAmerican"
							aria-labelledby="nativeAmerican@@member.id@@-label"/>
                    </div>
				</div>
				<div class="col-xs-12">
                    <div class="hide details member-info-section__detail-column__header col-xs-5" data-ng-if="'${pregnancyIndicatorConfig}' === 'ON'">
						<spring:message code="label.iex.prescreen.pregnant" javaScriptEscape="true"/>?
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantPregnant" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spousePregnant" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childPregnant" javaScriptEscape="true"/>
							</span>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 family-members-container-body" data-ng-if="'${pregnancyIndicatorConfig}' === 'ON'">
						<div class="aria-hidden" id="pregnant@@member.id@@-label">
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantPregnant" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spousePregnant" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childPregnant" javaScriptEscape="true"/>
							</span>
						</div>
                      	<input
							class="pregnant-input gtm_pregnant"
							type="checkbox"
							data-ng-change="updateGoogleTracking(member, 'isPregnant')"
							name="pregnant@@member.id@@"
							id="pregnant@@member.id@@"
							data-ng-model="member.isPregnant"
							aria-labelledby="pregnant@@member.id@@-label"/>
                    </div>
				</div>
				<div class="col-xs-12">
                    <div class="hide details member-info-section__detail-column__header col-xs-5">
						<spring:message code="label.iex.prescreen.seekingCoverage" javaScriptEscape="true"/>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 family-members-container-body">
						<div class="aria-hidden" id="coverage@@member.id@@-label">
							<span class="aria-hidden" data-ng-if="member.type == 'SELF'">
								<spring:message code="label.iex.prescreen.applicantNeedCoverage" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spouseNeedCoverage" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.childNeedCoverage" javaScriptEscape="true"/>
							</span>
						</div>
						<input
							class="coverage-input"
							type="checkbox"
							name="coverage@@member.id@@"
							id="coverage@@member.id@@"
							data-ng-model="member.seekingCoverage"
							data-ng-click="seekCoverage($index)"
							aria-labelledby="coverage@@member.id@@-label"/>
                    </div>
				</div>
					<div class="col-md-1 col-sm-1 col-xs-2" data-ng-if="'${stateCode}' !== 'MN'">
                    	<a href="javascript:void(0);" data-ng-show="member.removable" data-ng-click="remove($index, member.type)" class="btn btn-mini">
							<spring:message code="label.iex.prescreen.remove" javaScriptEscape="true"/>

							<span class="aria-hidden" data-ng-if="member.type == 'SPOUSE'">
								<spring:message code="label.iex.prescreen.spouse" javaScriptEscape="true"/>
							</span>
							<span class="aria-hidden" data-ng-if="member.type == 'CHILD'">
								<spring:message code="label.iex.prescreen.child" javaScriptEscape="true"/>
							</span>
						</a>
					</div>
				</ng-form>
			</div>


			<div class="form-actions" id="addMemberAction">
				<div class="pull-right">
					<button class="btn margin5-r gtm_add_spouse" type="button" id="addSpouse" data-ng-if="formInfo.spouse == 0" member-birthday-focus data-ng-click="addSpouse()">
						<span class="aria-hidden">
							<spring:message code="label.iex.prescreen.addA" javaScriptEscape="true"/>
						</span>
						<spring:message code="label.iex.prescreen.spouse" javaScriptEscape="true"/>
					</button>

					<button class="btn addChild addChild2 gtm_add_dependant" type="button" id="addChild" member-birthday-focus data-ng-click="addChild()">
						<span class="aria-hidden">
							<spring:message code="label.iex.prescreen.addA" javaScriptEscape="true"/>
						</span>
						<spring:message code="label.iex.prescreen.dependent" javaScriptEscape="true"/>
					</button>
				</div>
			</div>
		</div>
		<!-- FAMILY MEMBER MOBILE CONTAINER ENDS-->
	</div>
	<!-- Family member ends-->

	<!-- Income starts-->
	<div ng-if="showIncomeSection === true" class="question-group">
		<div class="header">
			<h2 class="qualify">
				<spring:message code="label.iex.prescreen.pIncome" javaScriptEscape="true"/><span class="aria-hidden"><spring:message code="label.iex.prescreen.ariaIncomeAlert" javaScriptEscape="true"/></span>
			</h2>
		</div>
		<div id="incomeEntry" class="control-group gutter10">
			<label class="pull-left margin5-t margin10-r" for="incomeValue">
				<spring:message code="label.iex.prescreen.annual" javaScriptEscape="true"/>
				<a tabindex="0" rel="tooltip" data-placement="bottom" role="tooltip" aria-tooltip  data-original-title="<spring:message code="label.iex.prescreen.taxHouseholdIncomeTooltip"/>" data-html="true"><spring:message code="label.iex.prescreen.taxHouseholdIncome" javaScriptEscape="true"/></a>:
				<span class="aria-hidden">dollar</span>
			</label>
			<span class="add-on">$</span>
      		<input
			id="incomeValue"
			type="text"
			name="income"
			class="input-small gtm_income"
			data-ng-model="formInfo.income" maxlength="7" value ="" num-only num-mask="number" required tabindex="0"
			onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': 'Enter Household Income','eventLabel': 'Household Income'});">
			<button type="button" id="check-for-savings" class="btn btn-primary pull-right col-xs-12 gtm_check_savings" data-ng-click="submitPrescreen('check')" onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.checkSavings"/> Button Click', 'eventLabel': '<spring:message code="label.iex.prescreen.checkSavings"/>'});" data-ng-disabled="!prescreen.$valid || !formInfo.zipValid"><spring:message code="label.iex.prescreen.checkSavings" javaScriptEscape="true"/></button>
		</div>
	</div>
	<!-- Income starts-->

	<p class="alert alert-info" id="incomeAlertText">
		<spring:message code="label.iex.prescreen.incomeAlert" javaScriptEscape="true"/>
	</p>

        <!-- CSR starts-->
        <div ng-if="showIncomeSection === false" id="csrEntry" class="control-group gutter10">
            <label class="control-label" id="ariaCsrLevelLabel" for="csrLevel">
                <spring:message code="label.iex.prescreen.selectACsrLevel" javaScriptEscape="true"/>
            </label>
            <div class="controls">
                <select id="csrLevel" name="csrLevel" data-ng-model='formInfo.csrLevel' ng-init="formInfo.csrLevel = 'CS1'">
                    <option value="CS1">CS1</option>
                    <option value="CS2">CS2</option>
                    <option value="CS3">CS3</option>
                    <option value="CS4">CS4</option>
                    <option value="CS5">CS5</option>
                    <option value="CS6">CS6</option>
                </select>
            </div>
        </div>
        <!-- CSR ends-->


	<div class="form-actions form-action-sub txt-right">
		<c:set var="individualRoleName"><encryptor:enc value="individual" isurl="true"/> </c:set>
		<sec:authorize access="!isAuthenticated()" var="isUserAuthenticated">
			<c:choose>
				<c:when test='${stateCode == "CT"}'>
					<a id="skip-sign-up"
					onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.skipSignup"/> Button Click','eventLabel': '<spring:message code="label.iex.prescreen.skipSignup"/>'});"
					class="btn actionBtn col-xs-5 gtm_skip" href="https://www.accesshealthct.com/AHCT/DisplayCreateUserAccount.action"><spring:message code="label.iex.prescreen.skipSignup" javaScriptEscape="true"/></button></a>
				</c:when>
				<c:when test='${stateCode == "MN"}'>
					<button
						id="skip-sign-up"
						onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.skipSignup"/> Button Click', 'eventLabel': '<spring:message code="label.iex.prescreen.skipSignup"/>'});
						location.href='https://auth.mnsure.org/login/Authredirector.jsp';"
						class="btn actionBtn col-xs-12 gtm_skip">
						<spring:message code="label.iex.prescreen.skipSignup" javaScriptEscape="true"/>
					</button>
				</c:when>
				<c:otherwise>
					<a
					id="skip-sign-up"
					onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.skipSignup"/> Button Click', 'eventLabel': '<spring:message code="label.iex.prescreen.skipSignup"/>'});"
					class="btn actionBtn col-xs-12 gtm_skip" redirectOnNoUrl="/account/signup/${individualRoleName}"
					href="<c:url value="/account/signup/${individualRoleName}" />"><spring:message code="label.iex.prescreen.skipSignup" javaScriptEscape="true"/></button></a>
				</c:otherwise>
			</c:choose>
		</sec:authorize>
		<c:choose>
			<c:when test='${stateCode == "CT" || stateCode == "MN"}'>
				<button
				type="button"
				id="check-for-plans"
				class="btn btn-primary margin10-l col-xs-12 gtm_browse_plans"
				onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.browsePlans"/> Button Click', 'eventLabel': '<spring:message code="label.iex.prescreen.browsePlans"/>'});"
				data-ng-disabled="formInfo.income !== undefined || (prescreen.indiv2 && !prescreen.indiv2.$valid) || !formInfo.zipValid || !checkDateInput()"
				data-ng-click="submitPrescreen('browse')">
				<spring:message code="label.iex.prescreen.browsePlans" javaScriptEscape="true"/>
			</button>
			</c:when>
			<c:otherwise>
				<button
				type="button"
				id="check-for-plans"
				class="btn btn-primary col-xs-12 gtm_browse_plans"
				onclick="window.dataLayer.push ({'event': 'preeligibilityEvent', 'eventCategory': 'Pre - Eligibility Events', 'eventAction': '<spring:message code="label.iex.prescreen.browsePlans"/> Button Click', 'eventLabel': '<spring:message code="label.iex.prescreen.browsePlans"/>'});"
				data-ng-disabled="(prescreen.indiv2 && !prescreen.indiv2.$valid) || !formInfo.zipValid || !checkDateInput()"
				data-ng-click="submitPrescreen('browse')">
					<spring:message code="label.iex.prescreen.browsePlans" javaScriptEscape="true"/>
			</button>
			</c:otherwise>
		</c:choose>
	</div>
</form>
<!-- Prescreen form ends -->

<!-- Modal for failling submission of pre-eligibility-->
<div data-ng-show="openSubmitFail">
      <div modal-show="openSubmitFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="openSubmitFail" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="openSubmitFail"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="indportal.disenrollSEP.submissionFailureMsg" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="label.iex.prescreen.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for failling submission of pre-eligibility-->

<!-- Modal for dependent limit-->
<div data-ng-show="openDependLimit">
      <div modal-show="openDependLimit" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="openDependLimit" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="openDependLimit"><spring:message code="label.iex.prescreen.limitDependentsTitle" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="label.iex.prescreen.limitDependentsContent" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="label.iex.prescreen.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for dependent limit-->

<!-- Modal for Seeking Coverage-->
<div data-ng-show= "seekingCoverageM">
      <div modal-show="seekingCoverageM" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="seekingCoverageM" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="seekingCoverageM"><spring:message code="label.iex.prescreen.seekingCoverage" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="label.iex.prescreen.seekingCoverageMContent" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="label.iex.prescreen.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for Seeking Coverage-->

<!-- Modal for all member medicare-->
<div data-ng-show="allMemberMedicareM">
      <div modal-show="allMemberMedicareM" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="allMemberMedicareM" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="allMemberMedicareM"><spring:message code="label.iex.prescreen.allMemberMedicareMHeader" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="label.iex.prescreen.allMemberMedicareMContent"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="label.iex.prescreen.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for all member medicare-->

</script>



  <!-- RESULTS PAGE  -->
<script type="text/ng-template" id="pp2">
<div data-ng-controller="resultsControl" data-ng-init="handleSavingsData()">
  <div id="resultsSection" class="panel span12">
    <div id="resultsMain">
	<span tabindex="0" class="aria-hidden" aria-labelledby="resultsTitles" id="ariaResultsTitles"></span>
<div id="resultsTitles">
      <div class="resultsTitle results header" data-ng-if="eligiRes===1"><h2><spring:message code="label.nmiex.resultsSection.header7" htmlEscape="false" /></h2></div>
      <div class="resultsTitle results header" data-ng-if="eligiRes===2"><h2><spring:message code="label.nmiex.resultsSection.header8" htmlEscape="false" /></h2></div>
      <div class="resultsTitle results header" data-ng-if="eligiRes===3"><h2><spring:message code="label.nmiex.resultsSection.header1" htmlEscape="false" /></h2></div>
      <div class="resultsTitle results header" data-ng-if="eligiRes===4"><h2><spring:message code="label.nmiex.resultsSection.header2" htmlEscape="false" /></h2></div>
</div>
      <div class="row-fluid gutter10" id="resultDivs">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<!-- APTC with sufficient data -->
          <div id="container" data-ng-if="aptcEligible" class="row-fluid">
            <h3 class="title">
              <spring:message code="label.nmiex.aptcAllPlans.header" htmlEscape="false" />
            </h3>

      <div class="span2 span-eligibility">
            <img alt="Tax Credit" src="<c:url value="/resources/images/i-taxcredit-ID.png"/>" class="nmhide mshide mnhide">
			<img alt="Cost Share" src="<c:url value="/resources/images/i-costshare-ID.png"/>" class="nmhide mshide idhide nvhide">
            <img alt="Tax Credit" src="<c:url value="/resources/images/i-taxcredit.png"/>" class="mnhide idhide nvhide">
      </div>

      <div class="span9 span-eligibility">
            <p class="subtitle"><spring:message code="label.nmiex.aptcAllPlans.title" htmlEscape="false" />&nbsp;<span class="maxAptc">@@maxAptc@@</span><spring:message code="label.nmiex.aptcAllPlans.maxAptc" htmlEscape="false" /></p>


	            <div class="contents txt-justified">
					<c:if test='${stateCode != "MN"}'>
		              <spring:message code="label.nmiex.aptcAllPlans.content1" htmlEscape="false" />
		              <!-- <spring:message code="label.nmiex.tooltip.taxCredits" htmlEscape="false" />-->
		              <spring:message code="label.nmiex.aptcAllPlans.content2" htmlEscape="false" />
		              <spring:message code="label.nmiex.aptcAllPlans.content3" htmlEscape="false" />
		              $<strong class="maxAptc">@@formInfo.lspPremiumAfterAptc@@</strong><spring:message code="label.nmiex.aptcAllPlans.content6" htmlEscape="false" />
		              <!-- <spring:message code="label.nmiex.aptcAllPlans.content4" htmlEscape="false" /><strong id="maxAptcYearly">@@maxAptcYearly@@</strong>
		              <spring:message code="label.nmiex.aptcAllPlans.content5" htmlEscape="false" />
		              $<strong id="lspPremium">@@formInfo.lspPremiumAfterAptc@@</strong><spring:message code="label.nmiex.aptcAllPlans.content6" htmlEscape="false" />-->
					</c:if>
					<c:if test='${stateCode == "MN"}'>
						<span data-ng-if="chipResults"><spring:message code="label.nmiex.aptcAllPlans.content1" htmlEscape="false" /></span>
					</c:if>
	            </div>
          </div>
          </div>


<!-- APTC eligibile, but APTC equals to zero. -->
  <div id="container" data-ng-if="showZeroAptc && formInfo.lspPremiumAfterAptc" class="row-fluid">
    <h3 class="title">
      <spring:message code="label.nmiex.aptcAllPlansNoAptc.header" htmlEscape="false" />
    </h3>

  <div class="span2 span-eligibility">
<img alt="Savings" src="<c:url value="/resources/images/i-nosave-ID.png"/>" class="nmhide mshide">
<img alt="Savings" src="<c:url value="/resources/images/i-nosave.png"/>" class="mnhide idhide nvhide">
  </div>

  <div class="span9 span-eligibility">

<div class="contents">
      <spring:message code="label.nmiex.aptcAllPlansNoAptc.content1" htmlEscape="false" />

	  <div class="margin10-t" data-ng-if="formInfo.stateCode == 'MN' && noOfMedicare > 0">
			 <spring:message code="label.iex.mnMedicare.1" />
	  </div>
      <c:if test='${stateCode != "MN"}'>
	  	<span>@@formInfo.aptcAllPlansNoAptcCountyName@@ <spring:message code="label.nmiex.aptcAllPlansNoAptc.content4" htmlEscape="false" /></span>
      	<span>@@formInfo.aptcAllPlansNoAptcStateName@@<spring:message code="label.nmiex.aptcAllPlansNoAptc.content2" htmlEscape="false" /></span>
      	<span>$@@formInfo.lspPremiumAfterAptc@@<spring:message code="label.nmiex.aptcAllPlansNoAptc.content3" htmlEscape="false" /></span>
	  </c:if>
    </div>
  </div>
  </div>
<!--NO PLANS-->
  <div id="container" data-ng-if="showZeroAptc && formInfo.expectedPremium && showexppremium && !hideNextAndAptcCsr" class="row-fluid">
    <h3 class="title">
      <spring:message code="label.nmiex.aptcNoPlans.header" htmlEscape="false" />
    </h3>
  <div class="span2 span-eligibility">
<img alt="Savings" src="<c:url value="/resources/images/i-nosave-ID.png"/>" class="nmhide mshide">
<img alt="Savings" src="<c:url value="/resources/images/i-nosave.png"/>" class="mnhide idhide nvhide">
  </div>

  <div class="span9 span-eligibility">
    <div class="contents gtm_eligibility_aptc">
      <spring:message code="label.nmiex.aptcNoPlans.content1" htmlEscape="false" />
      <!-- <span><spring:message code="label.nmiex.aptcNoPlans.content2" htmlEscape="false" />
      <spring:message code="label.nmiex.aptcNoPlans.content3" htmlEscape="false" /> -->
      $<span>@@formInfo.expectedPremium@@</span><spring:message code="label.nmiex.aptcNoPlans.content4" htmlEscape="false" />
    </div>
  </div>
  </div>
<!--NO PLANS-->

<!--HIGH-->
	<c:if test='${stateCode != "MN"}'>
	  	<div id="container" data-ng-if="highincome && !hideNextAndAptcCsr" class="row-fluid ">
		    <h3 class="title">
		      <spring:message code="label.nmiex.aptcAllPlansNoAptc.header" htmlEscape="false" />
		    </h3>
		  	<div class="span2 span-eligibility">
				<img alt="Savings" src="<c:url value="/resources/images/i-nosave-ID.png"/>" class="nmhide mshide">
				<img alt="Savings" src="<c:url value="/resources/images/i-nosave.png"/>" class="mnhide idhide nvhide">
		  	</div>
		  	<div class="span9 span-eligibility">
				<div class="contents">
		      		<spring:message code="label.nmiex.noSubsidy.title" htmlEscape="false" />
		    	</div>
		  	</div>
	  	</div>
	</c:if>
<!--HIGH-->
<!-- CSR -->
  <div id="container" data-ng-if="csrResults && !hideNextAndAptcCsr" class="row-fluid">
    <h3 class="title">
      <spring:message code="label.nmiex.CSR.header" htmlEscape="false" />
    </h3>
  <div class="span2 span-eligibility">
	  <img alt="Tax Credit" src="<c:url value="/resources/images/i-taxcredit-ID.png"/>" class="mshide idhide nvhide">
	  <img alt="Cost Share" src="<c:url value="/resources/images/i-costshare-ID.png"/>" class="nmhide mshide mnhide">
	  <img alt="Cost Share" src="<c:url value="/resources/images/i-costshare.png"/>" class="idhide mnhide nmhide mshide nvhide">
  </div>
  <div class="span9 span-eligibility">

    <div class="contents">
      <spring:message code="label.nmiex.CSR.content1" htmlEscape="false" />
	    <a class="gtm_coinsurance_hover" tabindex="0" data-toggle="tooltip" rel="tooltip" data-placement="top" role="tooltip" aria-tooltip onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Eligibility Estimate', 'eventAction': '<spring:message code="label.nmiex.CSR.content1.1"/> Hover', 'eventLabel': '<spring:message code="label.nmiex.CSR.content1.1"/>'});" data-original-title="<spring:message code="label.nmiex.tooltip.coinsurance" htmlEscape="false" />">
	     <spring:message code="label.nmiex.CSR.content1.1" htmlEscape="false" />
	    </a>,
        <a data-class="gtm_copays_hover" tabindex="0" data-toggle="tooltip" rel="tooltip" data-placement="top" role="tooltip" aria-tooltip onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Eligibility Estimate', 'eventAction': '<spring:message code="label.nmiex.CSR.content2"/> Hover', 'eventLabel': '<spring:message code="label.nmiex.CSR.content2"/>'});" data-original-title="<spring:message code="label.nmiex.tooltip.coPay" htmlEscape="false" />">
         <spring:message code="label.nmiex.CSR.content2" htmlEscape="false" />
		</a>
		<spring:message code="label.nmiex.CSR.content3" htmlEscape="false" />
        <a class="gtm_deductibles_hover" tabindex="0" data-toggle="tooltip" rel="tooltip" data-placement="top" role="tooltip" aria-tooltip onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Eligibility Estimate', 'eventAction': '<spring:message code="label.nmiex.CSR.content4"/> Hover', 'eventLabel': '<spring:message code="label.nmiex.CSR.content4"/>'});" data-original-title="<spring:message code="label.nmiex.tooltip.deductible" htmlEscape="false" />">
         <spring:message code="label.nmiex.CSR.content4" htmlEscape="false" />
        </a>
    <spring:message code="label.nmiex.CSR.content5" htmlEscape="false" />
    </div>
  </div>
  </div>


<!-- CHIP -->
<div id="container" data-ng-if="chipResults || mnMedicalAssistance" class="row-fluid">
	<h3 class="title">
		<c:if test='${stateCode != "MN"}'>
			<span data-ng-if="chipHeaderOne" class="results"><spring:message code="label.nmiex.CHIP.header1" htmlEscape="false" /></span>
			<span data-ng-if="chipHeaderTwo" class="results"><spring:message code="label.nmiex.CHIP.header2" htmlEscape="false" /></span>
		</c:if>
		<c:if test='${stateCode == "MN"}'>
			<span class="results"><spring:message code="label.nmiex.medicaid.header" htmlEscape="false" /></span>
		</c:if>
	</h3>

	<div class="span2 span-eligibility">
		<c:if test='${stateCode != "MN"}'>
			<img alt="CHIP" src="<c:url value="/resources/images/i-chip-ID.png"/>" class="nmhide mshide">
			<img alt="CHIP" src="<c:url value="/resources/images/i-chip.png"/>" class="mnhide idhide nvhide">
		</c:if>
		<c:if test='${stateCode == "MN"}'>
			<img alt="Medical" src="<c:url value="/resources/images/i-medicaid-ID.png"/>">
		</c:if>
	</div>
	<div class="span9 span-eligibility">
		<div class="contents">
			<c:if test='${stateCode != "MN"}'>
				<span data-ng-show="@@chipTextOne@@" class="results"><spring:message code="label.nmiex.CHIP.content1" htmlEscape="false" /></span>
				<span data-ng-show="@@chipTextTwo@@" class="results"><spring:message code="label.nmiex.CHIP.content2" htmlEscape="false" /></span>
			</c:if>
			<c:if test='${stateCode == "MN"}'>
				<p data-ng-show="@@!isAllMemberChip@@" class="results"><spring:message code="label.nmiex.CHIP.content1" htmlEscape="false" /></p>
				<p data-ng-show="@@!isAllMemberChip@@" class="results"><spring:message code="label.nmiex.CHIP.content1.1" htmlEscape="false" /></p>
				<p data-ng-show="@@isAllMemberChip@@"><spring:message code="label.nmiex.medicaid.content" htmlEscape="false" /></p>
				<p data-ng-show="@@isAllMemberChip@@"><strong><spring:message code="label.nmiex.medicaid.content2" htmlEscape="false" /></strong> <spring:message code="label.nmiex.medicaid.content3" htmlEscape="false" /> <a href="https://mn.gov/dhs/people-we-serve/adults/health-care/health-care-programs/" target="_blank" class="border-b"><spring:message code="label.nmiex.medicaid.content4" htmlEscape="false"/></a> <spring:message code="label.nmiex.medicaid.content5" htmlEscape="false"/></p>
				<p data-ng-show="@@isAllMemberChip@@"><spring:message code="label.nmiex.medicaid.content6" htmlEscape="false" /></p>
			</c:if>
		</div>
	</div>
</div>


<!-- Medicare -->
  <div id="container" data-ng-if="medicareResults" class="row-fluid">
    <h3 class="title">
      <spring:message code="label.nmiex.medicare.title" htmlEscape="false" />
    </h3>
  <div class="span2 span-eligibility">
<img alt="Medicare" src="<c:url value="/resources/images/i-medicare-ID.png"/>" class="nmhide mshide">
<img alt="Medicare" src="<c:url value="/resources/images/i-medicare.png"/>" class="mnhide idhide nvhide">
  </div>
  <div class="span9 span-eligibility">

    <div class="contents">
	  <span data-ng-show="@@medicareTextOne@@" class="results"><spring:message code="label.nmiex.medicare.content1" htmlEscape="false" /> <spring:message code="label.nmiex.medicare.content1.1" htmlEscape="false" /> <spring:message code="label.nmiex.medicare.content3" htmlEscape="false"/> <spring:message code="label.nmiex.medicare.content4" htmlEscape="false" /></span>
      <span data-ng-show="@@medicareTextTwo@@" class="results"><spring:message code="label.nmiex.medicare.content1" htmlEscape="false" /> <spring:message code="label.nmiex.medicare.content2.1" htmlEscape="false" /> <spring:message code="label.nmiex.medicare.content3" htmlEscape="false"/> <spring:message code="label.nmiex.medicare.content4" htmlEscape="false" /></span>
      	<div class="margin10-t" data-ng-if="showZeroAptc && formInfo.lspPremiumAfterAptc && formInfo.stateCode == 'MN'">
			<spring:message code="label.iex.mnMedicare.2" />
		</div>
	</div>
  </div>
  </div>


<!-- Medicaid -->
  <div id="container" data-ng-if="!(!isAllMemberMedicaid && formInfo.stateCode == 'MN') && (medicaidResults || noOfMedicaid > 0)" class="row-fluid">
    <h3 class="title">
      <spring:message code="label.nmiex.medicaid.header" htmlEscape="false" />
    </h3>
  <div class="span2 span-eligibility">
<img alt="Medicaid" src="<c:url value="/resources/images/i-medicaid-ID.png"/>" class="nmhide mshide">
  <img alt="Medicaid" src="<c:url value="/resources/images/i-medicaid.png"/>" class="mnhide idhide nvhide">
  </div>
  <div class="span9 span-eligibility">
<c:if test='${stateCode != "ID" && stateCode != "CT" &&  stateCode != "MN"}'>
  <p class="subtitle"><spring:message code="label.nmiex.medicaid.header" htmlEscape="false" /></p>
</c:if>
    <div class="contents">
		<c:if test='${stateCode != "MN"}'>
			<p data-ng-show="@@medicaidResults@@">
		</c:if>
		<c:if test='${stateCode == "MN"}'>
			<p data-ng-show="@@medicaidResults@@">
		</c:if>
		<c:if test='${stateCode != "NV"}'>
				<spring:message code="label.nmiex.medicaid.content" htmlEscape="false" />
		</c:if>
		<c:if test='${stateCode == "MN"}'>
			</p>
		</c:if>
		<c:if test='${stateCode != "MN"}'>
			</p>
		</c:if>
		<c:if test='${stateCode != "MN"}'>
			<p data-ng-show="@@medicaidResults@@"><spring:message code="label.nmiex.medicaid.content2" htmlEscape="false" /></p>
		</c:if>
		<c:if test='${stateCode == "MN"}'>
			<p data-ng-show="@@medicaidResults@@"><strong><spring:message code="label.nmiex.medicaid.content2" htmlEscape="false" /></strong> <spring:message code="label.nmiex.medicaid.content3" htmlEscape="false" /> <a href="https://mn.gov/dhs/people-we-serve/adults/health-care/health-care-programs/" target="_blank" class="border-b"><spring:message code="label.nmiex.medicaid.content4" htmlEscape="false"/></a> <spring:message code="label.nmiex.medicaid.content5" htmlEscape="false"/></p>
			<p><spring:message code="label.nmiex.medicaid.content6" htmlEscape="false" /></p>
		</c:if>
		<c:if test='${stateCode != "MN"}'>
      		<span data-ng-show="@@!medicaidResults && noOfMedicaid > 0@@"><spring:message code="label.nmiex.medicaid.content3" htmlEscape="false" /></span>
		</c:if>
    </div>
  </div>
  </div>



<!-- Error Result-->
  <div id="errorResult" class="alert resultwrap span9 col-xs-9 results" style="display: none;">
    <div class="gutter10">
      <p>
        <spring:message code="label.nmiex.errorResult1" htmlEscape="false" />
        <spring:message code="label.nmiex.errorResult2" htmlEscape="false" />
      </p>
    </div>
  </div>
	<div class="alert alert-info col-xs-10">
		<spring:message code="label.iex.result.estimateAlert" htmlEscape="false"/>
	</div>

</div>
</div>


	<div class="form-actions">
		<button type="button" class="btn gtm_estimate_back col-xs-12" data-ng-click="back()"><spring:message code="label.iex.result.previous" htmlEscape="false"/></button>
		<button type="button" class="btn btn-primary pull-right gtm_estimate_next col-xs-12" data-ng-if="!hideNextAndAptcCsr" data-ng-click="next()"><spring:message code="label.iex.result.next" htmlEscape="false"/></button>

      	<a role="button" class="btn btn-primary pull-right col-xs-12" data-ng-href="@@formInfo.sbeWebsite@@" data-ng-if="hideNextAndAptcCsr && !isAllMemberMedicare && formInfo.stateCode == 'MN'" target="_blank">
			<spring:message code="label.iex.startApplication" htmlEscape="false"/>
		</a>
	</div>
  </div>

</div>
</div>
   </script>

   <script>
/*     $(document).ready(function() {
      $('body').tooltip({
        selector : '[rel=tooltip]',
        placement : 'right',
      });

    }); */

  </script>
   <input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
   <input id="orEffDate" name="orEffDate" type="hidden" value="${orEffDate}"/>

   <script src="<c:url value="/resources/js/eligibility/prescreen_module.js"/>?v=R2" type="text/javascript"></script>

</div>
