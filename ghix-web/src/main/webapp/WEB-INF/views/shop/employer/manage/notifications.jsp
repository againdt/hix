<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
		<li><a href="<c:url value="/shop/employer/manage/list" />" > &lt; Back</a> <span class="divider">|</span></li>
		<li><a href="<c:url value="/shop/employer/" />">Employees</a> <span class="divider">/</span></li>
		<li><a href="<c:url value="/shop/employer/manage/list" />">Manage List</a> <span class="divider">/</span></li>
		<li class="active">${employee.name}</li>
	</ul>
	<h3 id="skip">${employee.name}
		<small>
				<c:set var="status" value="${fn:toUpperCase(fn:substring(employee.status, 0, 1))}${fn:toLowerCase(fn:substring(employee.status, 1, -1))}" />
				${fn:replace(status,'_',' ')}
		</small>
	</h3>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<div class="accordion-group">
					<div class="accordion-heading graydrkbg">
						<a class="accordion-toggle" data-toggle="collapse"
							data-parent="#accordion2" href="#collapseOne"> <spring:message  code='label.aboutthisemp'/></a>
					</div>
					<div id="collapseOne" class="accordion-body collapse in">
						<div class="accordion-inner">
							<ul class="nav nav-list">
								<li><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}" />">Employee Information</a></li>
								<li><a	href="<c:url value="/shop/employer/manage/dependents/${employee.id}" /> ">Dependent Information	(${employee.dependentCount})</a></li>
								<li><a href="<c:url value="/shop/employer/manage/enrollment/${employee.id}" />">Employee Coverage</a></li>
								<li class="active"><a href="<c:url value="/shop/employer/manage/notifications/${employee.id}" />">Notification History</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> <spring:message  code='label.actions'/></h4>
			</div>
			<ul class="nav nav-list">
			<%-- <c:if test="${(employee.status != 'ENROLLED') || (employee.status != 'DELETED')}">
				<li><a
					href="<c:url value="/shop/employer/manage/editemployee?id=${employee.id}" />">
						<i class="icon-pencil"></i> <spring:message  code='label.edit'/>
				</a></li>
			</c:if> --%>
			<c:if test="${(employee.status != 'DELETED')}">
				<li><a	href="<c:url value="/shop/employer/manage/editemployee?id=${employee.id}" />">
						<i class="icon-pencil"></i> <spring:message  code='label.edit'/>
				</a></li>
			</c:if>
				<!-- <li><a href="#"> <i class="icon-folder-open"></i> Enroll
						For
				</a></li> -->
				<%-- <li><a href="#waive-coverage" data-toggle="modal"><i
						class="icon-lock"></i> <spring:message  code='label.waivedcov'/></a></li> --%>
				<!-- modal -->
				<%-- <div id="waive-coverage" class="modal hide fade" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">�</button>
						<h3 id="myModalLabel"><spring:message  code='label.waivedcov'/></h3>
					</div>
					<div class="modal-body">
					<fieldset>
						<legend><spring:message  code='label.reasonempwaiving'/></legend>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="<spring:message  code='label.covanitherempplan'/>"> <spring:message  code='label.covanitherempplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="<spring:message  code='label.covbyindvplan'/>">
								<spring:message  code='label.covbyindvplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="<spring:message  code='label.covbygovtplan'/>">
								<spring:message  code='label.covbygovtplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="<spring:message  code='label.covasdependent'/>">
								<spring:message  code='label.covasdependent'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio" for="waive_reason_other"> <input type="radio"
								id="waive_reason" name="waive_reason" value="<spring:message  code='label.reason4'/>">
								<spring:message  code='label.reason4'/>
							</label>
							<textarea name="waive_reason_other" id="waive_reason_other"
								class="span5" rows="3" maxlength="255"></textarea>
						</div>
						</fieldset>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.cancelButton'/></button>
						<button class="btn btn-primary" id="waivecoverageButton"
							name="waivecoverageButton"
							onclick="updateWaiveCoverage('${employee.id}')"><spring:message  code='label.ok'/></button>
					</div>
				</div> --%>
				<!-- /modal-->
			</ul>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
          <div class="row-fluid">
          <div class="header">
           	<h4><spring:message  code='label.empnotification'/></h4>
          </div>
            <div class="gutter10">
              <p>&nbsp;</p>

            </div>
            <!--gutter10--> 
          </div>
          <!--row-fluid--> 
        </div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
</div> <!--gutter10--> 
<script type="text/javascript">
function updateWaiveCoverage(elem){
	var waiveOption = "";
	 $(':radio[name="waive_reason"]').each(function() {
		if(this.checked)	 
	 		waiveOption = this.value;
	 });
	
	if(waiveOption !=""){
		
		if(waiveOption == 'other'){
			waiveOption = $('#waive_reason_other').val() + "."; }
		window.location.href ='../waivecoverage/'+elem + '&SN='+waiveOption;
	}
}

$(document).ready( function () {
	 
	maxLength = $("textarea#waive_reason_other").attr("maxlength");
        $("textarea#waive_reason_other").after("<div><span id='remainingLengthTempId'>" 
                  + maxLength + "</span> remaining</div>");
 
        $("textarea#waive_reason_other").bind("keyup change", function(){checkMaxLength(this.id,  maxLength); } )
 
    });
 
    function checkMaxLength(textareaID, maxLength){
 
        currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 
		if (currentLengthInTextarea > (maxLength)) { 
 
			// Trim the field current length over the maxlength.
			$("textarea#waive_reason_other").val($("textarea#waive_reason_other").val().slice(0, maxLength));
			$(remainingLengthTempId).text(0);
 
		}
    }
</script>