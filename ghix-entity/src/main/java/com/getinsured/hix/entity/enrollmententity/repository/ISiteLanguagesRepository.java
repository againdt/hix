package com.getinsured.hix.entity.enrollmententity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;



public interface ISiteLanguagesRepository extends JpaRepository<SiteLanguages, Long> {

	@Query("FROM SiteLanguages as an where an.id = :siteLanguageId")
	SiteLanguages findBySiteLanguagesId(@Param("siteLanguageId") long siteLanguageId);
	SiteLanguages findSiteLanguagesBySite(Site site);
}
