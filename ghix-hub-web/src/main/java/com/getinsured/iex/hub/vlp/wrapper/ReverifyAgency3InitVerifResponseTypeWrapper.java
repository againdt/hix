package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifResponseType;

/**
 * Wrapper Class for ReverifyAgencyResponse
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 */

public class ReverifyAgency3InitVerifResponseTypeWrapper implements JSONAware{

	private ResponseMetadataWrapper responseMetadata;
	private ReverifyAgency3InitVerifResponseSetTypeWrapper reverifyAgency3InitVerifResponseSet;
	
	public ReverifyAgency3InitVerifResponseTypeWrapper(ReverifyAgency3InitVerifResponseType response){
		
		if (response == null){
    		return;
    	}
		
		this.responseMetadata = new ResponseMetadataWrapper(response.getResponseMetadata());
		this.reverifyAgency3InitVerifResponseSet = new ReverifyAgency3InitVerifResponseSetTypeWrapper(response.getReverifyAgency3InitVerifResponseSet());
	}
	
	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public ReverifyAgency3InitVerifResponseSetTypeWrapper getAgency3InitVerifResponse() {
		return reverifyAgency3InitVerifResponseSet;
	}

	public void setAgency3InitVerifResponse(
			ReverifyAgency3InitVerifResponseSetTypeWrapper agency3VerifResponse) {
		this.reverifyAgency3InitVerifResponseSet = agency3VerifResponse;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("ReverifyAgency3InitVerifResponse",this.reverifyAgency3InitVerifResponseSet);
		return obj.toJSONString();
	}
}
