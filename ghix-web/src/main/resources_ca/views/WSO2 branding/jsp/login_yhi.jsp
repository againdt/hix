<!DOCTYPE html>
<!--
~ Copyright (c) 2005-2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
~
~ WSO2 Inc. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied.  See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<%@ page import="org.wso2.carbon.identity.application.common.util.IdentityApplicationConstants"%>

<fmt:bundle basename="org.wso2.carbon.identity.application.authentication.endpoint.i18n.Resources">

    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Your Health Idaho</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
         <link href="/authenticationendpoint/assets/css/bootstrap-responsive.css" rel="stylesheet"  type="text/css" media="screen,print" />
		 <link href='//fonts.googleapis.com/css?family=Droid+Serif:400,700|Merriweather|Raleway:400,600' rel='stylesheet' type='text/css' media="screen,print" />
		 <link href="/authenticationendpoint/assets/css//bootstrap_ID.css" media="screen,print" rel="stylesheet" type="text/css" />
		 <link href="/authenticationendpoint/assets/css/ghixcustom.css" rel="stylesheet"  type="text/css" media="screen,print" />
		 <link href="/authenticationendpoint/assets/css/quickfixes_id.css" media="screen,print" rel="stylesheet" type="text/css" /> 
        
        
        <!--[if lt IE 8]>
        <link href="css/localstyles-ie7.css" rel="stylesheet">
        <![endif]-->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5.js"></script>
        <![endif]-->
        <script src="/authenticationendpoint/assets/js/jquery-1.7.1.min.js"></script>
        <script src="/authenticationendpoint/js/scripts.js"></script>
        <script src="/authenticationendpoint/assets/js/bootstrap.min.js"></script>
	<style>
		div.different-login-container a.truncate {
			width: 148px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		
		.dl-horizontal dt {
			font-weight: normal;
		}
		
		.dl-horizontal dd {
			font-weight: bold;
		}
		
		#account-settings dt,#account-settings dd {
			margin-bottom: 15px;
		}
		
		#acc-settings-info dt {
			width: 160px;
		}
		
		#acc-settings-info dd {
			margin-left: 180px;
			font-weight: bold;
		}
		
		#change-email label,#change-email input,#change-password label,#change-password input
			{
			margin: 10px 0;
		}
		
		#change-questions .form-group {
			float: left;
			margin: 5px 0;
		}
	</style>

    </head>

    <body id="user-login">
    
	<!-- Header -->
	<div id="masthead" class="topbar">
		<div class="navbar navbar-fixed-top navbar-inverse" id="masthead"
			role="banner">
			<input type="hidden" id="exchangeName" value="Your Health Idaho">

			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<ul class="nav">
						<li><a href="http://www.yourhealthidaho.org/"><img class="brand"
								src="/authenticationendpoint/assets/img/logo_ca.png" alt="Your Health Idaho" /></a><span
								style="display: none;">Your Health Idaho</span></li>
					</ul>
					<div class="nav-collapse collapse pull-right">  
						<ul class="nav pull-right clearfix" id="second-menu-nm">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
								<ul class="dropdown-menu">
								  <li><a href="#" style="cursor:default">1-855-944-3246</a></li>		 	
								  <li><a href="http://www.yourhealthidaho.org/enrollment-help/browse-frequently-asked-questions/" target="_blank">Help Center</a></li>
								</ul>
							</li> 
						</ul>
					</div>	
					
				</div>
			</div>
		</div>
	</div>
	<!-- End Header -->
<!-- 
	<div class="header-text">
        Login
    </div>
    
    <div class="container">
		<div class="row">
			<div class="span12">
				<h1>Login to continue</h1>
			</div>
		</div>
    </div> -->
    <!-- container -->
    <%@ page import="java.util.Map" %>
    <%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder" %>
    <%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.Constants" %>

    <%
        String queryString = request.getQueryString();
        Map<String, String> idpAuthenticatorMapping = null;
        if (request.getAttribute("idpAuthenticatorMap") != null) {
            idpAuthenticatorMapping = (Map<String, String>)request.getAttribute("idpAuthenticatorMap");
        }
        
        String errorMessage = "Authentication Failed! Please Retry";
        String loginFailed = "false";
        
        if (request.getParameter(Constants.AUTH_FAILURE) != null &&
                "true".equals(request.getParameter(Constants.AUTH_FAILURE))) {
            loginFailed = "true";
            
            if(request.getParameter(Constants.AUTH_FAILURE_MSG) != null){
                errorMessage = (String) request.getParameter(Constants.AUTH_FAILURE_MSG);
                
                if (errorMessage.equalsIgnoreCase("login.fail.message")) {
                    errorMessage = "Authentication Failed! Please Retry";
                }
            }
        }
        
        String newUser = "false";
        String user_created = request.getParameter("RelayState");
        String userCreatedMessage = "";
        if(user_created != null && user_created.equalsIgnoreCase("true")){
        	 newUser = "true";
        	 userCreatedMessage = "Your account was successfully created. Please login to continue.";
        }
        
        //To show date on footer
        DateFormat formatter = new SimpleDateFormat("yyyy");
        String now = formatter.format(new Date());
    %>

    <script type="text/javascript">
        function doLogin() {
            var loginForm = document.getElementById('loginForm');
            loginForm.submit();
        }
    </script>
    
<% 

boolean hasLocalLoginOptions = false; 
List<String> localAuthenticatorNames = new ArrayList<String>();

if (idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME) != null){
	String authList = idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME);
	if (authList!=null){
		localAuthenticatorNames = Arrays.asList(authList.split(","));
	}
}


%>
<div id="container-wrap" class="row-fluid">
<div class="container">
<div class="row-fluid" id="titlebar">
		<h1 class="custom-margin">Login</h1>	
		
		<% if ("true".equals(newUser)) { %>
			<div class="errorblock alert alert-info">
				<%=userCreatedMessage%>
			</div>
		<% } %>
</div>
<%if(localAuthenticatorNames.contains("BasicAuthenticator")){ %>
    <div id="local_auth_div">
<%} %>

		
		<div class="row-fluid">
		<div class="span10 offset1" id="rightpanel">
        <form action="../../commonauth" method="post" id="loginForm" class="form-horizontal box-loose" autocomplete="off" >
            <%
                if(localAuthenticatorNames.size()>0) {

                    if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("OpenIDAuthenticator")){
                    	hasLocalLoginOptions = true;
            %>

            <div class="row-fluid">
                <div class="span7">
                
                        <%@ include file="openid.jsp" %>

                </div>
                
            </div>

            <%
            } else if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("BasicAuthenticator")) {
            	hasLocalLoginOptions = true;
            %>
			
            <div class="row-fluid">
		            <% if ("true".equals(loginFailed)) { %>
		            <div class="errorblock alert alert-info">
		                <%=errorMessage%>
		            </div>
			    <% } %>
			  
                <div class="span7">
                        <%@ include file="basicauth.jsp" %>
                </div>
                <div class="span5 nmhide mshide idhide">
					<div class="signinWithExistingAccount">
						<img src="/authenticationendpoint/assets/img/idaLink_logo.png"
							alt="Welcome to idalink! Your online portal for managing benefits from Idaho's Department of Health and Welfare. idaLink"
							title="idalink" /> <small>You can also login with your idalink account</small>
					</div>
				</div>
            </div>

            <%
            } 
            } 
            %>

    <%if(idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME) != null){ %>        
	</div>
	<%} %>
	<%
        if ((hasLocalLoginOptions && localAuthenticatorNames.size() > 1) || (!hasLocalLoginOptions)
        		|| (hasLocalLoginOptions && idpAuthenticatorMapping.size() > 1)) {
    	%>
  <div class="container">
	<div class="row">
		<div class="span12">
		    <% if(hasLocalLoginOptions) { %>
			<h2>Other login options:</h2>
			<%} else { %>
			<script type="text/javascript">
			    document.getElementById('local_auth_div').style.display = 'block';
			</script>
			<%} %>
		</div>
	</div>
    </div>
    </div>	
	<div class="container different-login-container">
            <div class="row">
                                               
                    <%
                        for (Map.Entry<String, String> idpEntry : idpAuthenticatorMapping.entrySet())  {
                            if(!idpEntry.getKey().equals(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME)) {
                            	String idpName = idpEntry.getKey();
                            	boolean isHubIdp = false;
                            	if (idpName.endsWith(".hub")){
                            		isHubIdp = true;
                            		idpName = idpName.substring(0, idpName.length()-4);
                            	}
                    %>
                              <div class="span3">
                                    <% if (isHubIdp) { %>
                                    <a href="#"  class="main-link"><%=idpName%></a>
                                    <div class="slidePopper" style="display:none">
				                        <input type="text" id="domainName" name="domainName"/>
				                        <input type="button" class="btn btn-primary go-btn" onClick="javascript: myFunction('<%=idpName%>','<%=idpEntry.getValue()%>','domainName')" value="Go" />
			                        </div>
			                        <%}else{ %>
			                              <a onclick="javascript: handleNoDomain('<%=idpName%>','<%=idpEntry.getValue()%>')"  class="main-link truncate" style="cursor:pointer" title="<%=idpName%>"><%=idpName%></a>			                        
			                        <%} %>
		                      </div>
                            <%}else if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("IWAAuthenticator")) {
                            %>
                            	<div class="span3">
                                <a onclick="javascript: handleNoDomain('<%=idpEntry.getKey()%>','IWAAuthenticator')"  class="main-link" style="cursor:pointer">IWA</a>
	                            </div>
	                       <% 
                            }

                         }%>
                        
                    
               
            </div>
	    <% } %>
        </form>
        </div>
    </div>

    <script>
 	$(document).ready(function(){
		$('.main-link').click(function(){
			$('.main-link').next().hide();
			$(this).next().toggle('fast');
			var w = $(document).width();
			var h = $(document).height();
			$('.overlay').css("width",w+"px").css("height",h+"px").show();
		});
		$('.overlay').click(function(){$(this).hide();$('.main-link').next().hide();});
	
	});
        function myFunction(key, value, name)
        {
	    var object = document.getElementById(name);	
	    var domain = object.value;


            if (domain != "")
            {
                document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=request.getParameter("sessionDataKey")%>&domain=" + domain;
            } else {
                document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=request.getParameter("sessionDataKey")%>";
            }
        }
        
        function handleNoDomain(key, value)
        {


          document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=request.getParameter("sessionDataKey")%>";
            
        }
        
    </script>
    <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-43319506-1', 'auto', {
		  'allowLinker': true
		});
		ga('send', 'pageview');
	</script>
</div>
</div>
	<div id="footer">
		<footer class="container" role="contentinfo">
			<div class="row-fluid">
				<div class="pull-left">
					<ul class="nav nav-pills">
						<li id="copyrights">&copy;<%=now%> Your Health Idaho</li>
						<li><a
							href="http://www.yourhealthidaho.org/privacy-policy/"
							class="margin20-l">Privacy Policy</a></li>
					</ul>
				</div>
	
	
			</div>
		</footer>
	</div>	
</body>
    </html>

</fmt:bundle>

