
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class SpecialEnrollmentPeriod {

    @Expose
    private Boolean birthOrAdoptionEventIndicator=false;

    public Boolean getBirthOrAdoptionEventIndicator() {
        return birthOrAdoptionEventIndicator;
    }

    public void setBirthOrAdoptionEventIndicator(Boolean birthOrAdoptionEventIndicator) {
        this.birthOrAdoptionEventIndicator = birthOrAdoptionEventIndicator;
    }

}
