package com.getinsured.hix.platform.audit.service.jpa;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;

public class DisplayAuditServiceImplTest extends BaseTest{

	private static final String IBROKER_REPOSITORY = "iBrokerRepository";
	private static final String CONTACT_NUMBER2 = "contactNumber";
	private static final String CONTACT_NUMBER = "Contact Number";
	private static final String MODEL_BROKER = "com.getinsured.hix.model.Broker";
	private static final String APPLICATION_DATE2 = "applicationDate";
	private static final String APPLICATION_DATE = "Application Date";
	@Autowired private DisplayAuditService displayAuditService;

	@Test
	public void testFindRevisions() {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(APPLICATION_DATE2, APPLICATION_DATE);
		requiredFieldsMap.put(CONTACT_NUMBER2, CONTACT_NUMBER);

		List<Map<String, String>> brokerData = displayAuditService.findRevisions(IBROKER_REPOSITORY, MODEL_BROKER,requiredFieldsMap, 2);


		/*StringBuilder sb = new StringBuilder();
		sb.append("<div><table><tr>");
		Set<String> keys = requiredFieldsMap.keySet();
		for (String key : keys) {
			sb.append("<td>");
			String value = requiredFieldsMap.get(key);
			sb.append(value);
			sb.append("</td>");
		}
		sb.append("</tr>");

		for (Map<String, String> map : brkData) {
			sb.append("<tr>");
			for (Map.Entry entry : map.entrySet()) {
				sb.append("<td>");
				sb.append(entry.getValue());
				sb.append("</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</table></div>");


		String reqString = sb.toString();*/

		assertNotNull(brokerData);
	}

	@Test
	public void testFindRevisionsWithEmptyResult() {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(APPLICATION_DATE2, APPLICATION_DATE);
		requiredFieldsMap.put(CONTACT_NUMBER2, CONTACT_NUMBER);

		List<Map<String, String>> brkData = displayAuditService.findRevisions(IBROKER_REPOSITORY, MODEL_BROKER,requiredFieldsMap, 1);

		assertTrue(brkData.isEmpty());
	}



	@Test(expected = AssertionError.class)
	public void testNullRepositoryObject() {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(APPLICATION_DATE2, APPLICATION_DATE);
		requiredFieldsMap.put(CONTACT_NUMBER2, CONTACT_NUMBER);

		String repositoryObject = null;
		displayAuditService.findRevisions(repositoryObject, MODEL_BROKER,requiredFieldsMap, 2);
	}

	@Test(expected = AssertionError.class)
	public void testNullDomainName() {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(APPLICATION_DATE2, APPLICATION_DATE);
		requiredFieldsMap.put(CONTACT_NUMBER2, CONTACT_NUMBER);

		String repositoryObject = IBROKER_REPOSITORY;
		String domainName = null;
		displayAuditService.findRevisions(repositoryObject, domainName,requiredFieldsMap, 2);
	}

	@Test(expected = AssertionError.class)
	public void testNullRequiredFieldsMap() {
		Map<String, String> requiredFieldsMap = null;
		displayAuditService.findRevisions(IBROKER_REPOSITORY, MODEL_BROKER,requiredFieldsMap, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullId() {
		Map<String, String> requiredFieldsMap = null;
		Integer id = null;
		displayAuditService.findRevisions(IBROKER_REPOSITORY, MODEL_BROKER,requiredFieldsMap, id);
	}

	@Test(expected = AssertionError.class)
	public void testInvalidRequiredFieldsMap() {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put("applicationDate1", APPLICATION_DATE);
		requiredFieldsMap.put("contactNumber1", CONTACT_NUMBER);

		String repositoryObject = IBROKER_REPOSITORY;
		displayAuditService.findRevisions(repositoryObject, MODEL_BROKER,requiredFieldsMap, 2);
	}

}
