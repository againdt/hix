package com.getinsured.hix.platform.security.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.util.exception.GIException;


public class AccountActivationServiceTest extends BaseTest {

	@Autowired private AccountActivationService accountActivationService;

	@Test
	public void testInitiateActivationForCreatedRecord() {
		List<String> phoneNumbers = new ArrayList<String>();
		phoneNumbers.add("+123456789");
		phoneNumbers.add("(605) 456 789");

		String phoneNumber = null;
		CreatedObject createdObject = new CreatedObject();
		createdObject.setEmailId("ekramali.kazi@Xoriant.com");
		createdObject.setObjectId(1);
		createdObject.setRoleName(GhixRole.EMPLOYEE.toString());
		createdObject.setPhoneNumbers(Arrays.asList("+123456789", "(605) 456 789"));
		//createdObject.setPhoneNumbers(Arrays.asList(phoneNumber));
		createdObject.setFullName("Salman Khan");

		CreatorObject creator = new CreatorObject();
		creator.setObjectId(1);
		creator.setFullName("Ekram Ali Kazi");
		creator.setRoleName("EMPLOYER");

		try {
			accountActivationService.initiateActivationForCreatedRecord(createdObject, creator);
		} catch (GIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
