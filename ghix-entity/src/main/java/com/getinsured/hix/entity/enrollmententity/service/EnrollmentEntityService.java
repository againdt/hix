package com.getinsured.hix.entity.enrollmententity.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.entity.EnrollmentEntityAssisterSearchResult;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

/**
 * Encapsulates service layer method calls for Enrollment Entity
 */
public interface EnrollmentEntityService {

	/**
	 * Retrieves AEE based on user id
	 * 
	 * @param id
	 *            AEE user Id
	 * @return {@link EnrollmentEntity}
	 */
	EnrollmentEntity findEnrollmentEntityByUserId(int id);

	
	/**
	 * Retrieves AEE based on record Id in session i.e. AEE priamry key id
	 * 
	 * @param id
	 *            AEE id
	 * @return {@link EnrollmentEntity}
	 */
	EnrollmentEntity getEnrollmentEntityByRecordID(int id);

	/**
	 * Finds Payment method of AEE
	 * 
	 * @param id
	 *            AEE id
	 * @param entityModule
	 *            module name
	 * @return {@link PaymentMethods}
	 */
	PaymentMethods findPaymentMethodDetailsForEntity(int id, String entityModule);
	
	/**
	 * Loads AEE status history
	 * 
	 * @param id
	 *            AEE id
	 * @return {@link List} of {@link Map} of String and Object
	 */
	List<Map<String, Object>> loadEntityrStatusHistory(Integer id);

	/**
	 * Retrieves {@link EnrollmentEntity} based on entity Id
	 * 
	 * @param entityId
	 * @return {@link EnrollmentEntity}
	 */
	EnrollmentEntity findEnrollmentEntityById(int entityId);

	/**
	 * Retrieves Enrollment Entity based on search criteria
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param startRecord
	 *            Start of record on page
	 * @param pageSize
	 *            Page size for pagination
	 * @return {@link Map} of {@link Broker}
	 */
	Map<String, Object> findEnrollmentEntity(EnrollmentEntity enrollmentEntity, Integer startRecord, Integer pageSize);

	/**
	 * Searches for AEE
	 * 
	 * @param entityRequestDTO
	 *            {@link EnrollmentEntityRequestDTO}
	 * @param totalFlag
	 *            Flag
	 * @return {@link Map} of String and Object
	 */
	Map<String, Object> searchEnrollmentEntity(EnrollmentEntityRequestDTO entityRequestDTO, boolean totalFlag);
	
	/**
	 * Searches for AEE
	 * 
	 * @param entityRequestDTO
	 *            {@link EnrollmentEntityRequestDTO}
	 * @return count of records
	 */
	int searchEnrollmentEntity(EnrollmentEntityRequestDTO entityRequestDTO);
	
	/**
	 * Finds AEE by Registration renewal date
	 * 
	 * @return count of records
	 */
	int findEnrollmentEntityByRegistrationRenewalDate();
	
	/**
	 * Finds AEE by Registration Status
	 * 
	 * @param registrationStatus
	 *            Registration status of AEE
	 * @return count of records
	 */
	int findEnrollmentEntityByRegistrationStatus(String registrationStatus);
	
	/**
	 * Counts number of AEE
	 * 
	 * @return count of records
	 */
	int findTotalNoOfEnrollmentEntities();
	
	/**
	 * Finds {@link Assister} for AEE
	 * 
	 * @param enrollmentEntityId
	 *            AEE Id
	 * @return count of records
	 */
	int findAssistersForEnrollmentEntity(int enrollmentEntityId);
	
	/**
	 * Loads AEE Document History
	 * 
	 * @param entityId
	 *            AEE Id
	 * @return {@link List} of {@link Map} String and Object
	 */
	List<Map<String, Object>> loadEntityrDocumentHistory(Integer entityId);

	/**
	 * Deletes AEE document
	 * 
	 * @param id
	 *            document Id
	 * @throws Exception
	 */
	void deleteDocument(Integer id) throws ContentManagementServiceException;

	/**
	 * Saves AEE
	 * 
	 * @param enrollEntity
	 *            {@link EnrollmentEntity}
	 * @return {@link EnrollmentEntity}
	 */
	EnrollmentEntity saveEnrollmentEntity(EnrollmentEntity enrollEntity);
	
	/**
	 * Finds AEE by AEE name
	 * 
	 * @param entityName
	 *            AEE name
	 * @return {@link EnrollmentEntity}
	 */
	EnrollmentEntity findEnrollmentEntityByEntityName(String entityName);
	
	/**
	 * Searches AEE for Assisters
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            {@link EnrollmentEntityRequestDTO}
	 * @return {@link List} of {@link EnrollmentEntityAssisterSearchResult}
	 */
	EnrollmentEntityResponseDTO searchEnrollmentEntityForAssisters(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO);

	EnrollmentEntityAssisterSearchResult getEnrollmentEntityDetails(Integer enrollmentEntityId, Integer siteId);
	
	/**
	 * Searches individual records for AEE
	 * 
	 * @param searchCriteria
	 *            {@link Map} of Search criteria
	 * @param desigStatus
	 *            designation status
	 * @param startRecord
	 *            start record from
	 * @param pageSize
	 *            total records on page
	 * @return {@link EnrollmentEntityResponseDTO}
	 */
	EnrollmentEntityResponseDTO searchIndividualForEnrollmentEntity(Map<String, Object> searchCriteria, String desigStatus,Integer startRecord,Integer pageSize);
	
	Map<String,Object> searchCECToReassign(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO);
}