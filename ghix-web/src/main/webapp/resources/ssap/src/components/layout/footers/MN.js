import React from 'react';

function FooterMN () {

    const footerStyle = {
        background: '#f1eded',
        color: 'black',
        borderTop: '3px solid #03a2a2',
        fontSize: '13px'
    };
    
    return(
        <>
            <footer id="footer" className="usa-footer" role="contentinfo" style={footerStyle}>
                <div className="footer__container usa-footer-primary-section">
                    <div className="usa-grid footer__container_wrapper" style={{display: 'flex'}}>
                        <div>&copy;{(new Date().getFullYear())} MNsure</div>
                        <div style={{textIndent: '10px'}}><a href='https://www.mnsure.com'>Privacy Policy</a></div>
                    </div>
                </div>
            </footer>
        </>
    );
}

export default FooterMN;
