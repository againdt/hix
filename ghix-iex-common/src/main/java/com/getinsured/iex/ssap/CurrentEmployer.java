
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class CurrentEmployer {

    @Expose
    private Employer employer=new Employer();
    @Expose
    private CurrentEmployerInsurance currentEmployerInsurance=new CurrentEmployerInsurance();

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public CurrentEmployerInsurance getCurrentEmployerInsurance() {
        return currentEmployerInsurance;
    }

    public void setCurrentEmployerInsurance(CurrentEmployerInsurance currentEmployerInsurance) {
        this.currentEmployerInsurance = currentEmployerInsurance;
    }

}
