/**
 * 
 */
package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.enrollment._1095.cy_2018.manifest.cms.dsh.bdshfferesp.extension._1.BatchRequestType;
import com.getinsured.enrollment._1095.cy_2018.manifest.cms.dsh.bdshfferesp.extension._1.RequestDocumentType;
import com.getinsured.enrollment._1095.cy_2018.manifest.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.Form1095AResponseGrpType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.msg.form1095atransmissionexchrespmessage.Form1095ATransmissionExchRespType;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.EnrollmentIn1095;
import com.getinsured.hix.model.enrollment.EnrollmentInDtl1095;



/**
 * Coverage year 2018 and above schema service implementation of inbound 1095 XML
 * @author negi_s
 *
 */
@Service
public class Enrollment1095InboundCy2019 extends Enrollment1095Inbound {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095InboundCy2019.class);

	@SuppressWarnings("unchecked")
	@Override
	public void updateEnrollmentIn1095ForAckAndNack(File file, String fileType, Long jobExecutionId) {
		JAXBContext jaxbContext = null;
		Unmarshaller jaxbUnmarshaller = null;
		try {
			jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment._1095.cy_2018.manifest.cms.dsh.bdshfferesp.exchange._1.ObjectFactory.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<BatchRequestType> batchRequestTypeList = null;
			batchRequestTypeList = (JAXBElement<BatchRequestType>) jaxbUnmarshaller.unmarshal(file);
			BatchRequestType batchRequestType = batchRequestTypeList.getValue();
			List<EnrollmentIn1095> enrollmentIn1095List = new ArrayList<>();
			enrollmentIn1095List.add(updateEnrollmentIn1095(batchRequestType, fileType, file.getName(), jobExecutionId));
			for (EnrollmentIn1095 enrIn1095 : enrollmentIn1095List) {
				enrollmentIn1095Repository.save(enrIn1095);
			}
		}catch(JAXBException e) {
			LOGGER.error("Error while unmarshalling inbound file", e);
		}
		
	}
	
	private EnrollmentIn1095 updateEnrollmentIn1095(BatchRequestType batchRequestType, String fileName,String responseFileName, Long jobExecutionId) {
		EnrollmentIn1095 enrollmentIn1095 = new EnrollmentIn1095();
		
		fillEnrollmentIn1095(batchRequestType,enrollmentIn1095);
		
		// .OUT file name
		if(null != responseFileName && responseFileName.length()>4 && responseFileName.endsWith(".zip")){
			enrollmentIn1095.setInPackageFileName(responseFileName.substring(0, responseFileName.length()-4));
		}else{
			enrollmentIn1095.setInPackageFileName(responseFileName);
		} 
		enrollmentIn1095.setResponseFileName(fileName); // ack or nac or eoy XML file after unzip
		enrollmentIn1095.setJobExecutionId(String.valueOf(jobExecutionId));
		
		List<ResponseMetadataType> responseDataList = null;
		
		if(batchRequestType.getServiceSpecificData() != null){
			responseDataList = batchRequestType.getServiceSpecificData().getResponseMetadata();
			
			if(responseDataList != null && responseDataList.size() > 0){
				ResponseMetadataType responseMetadataType = responseDataList.get(0);
				
				enrollmentIn1095.setErrorCodeLkp(
						lookupService.getlookupValueByTypeANDLookupValueCode(ENROLLMENT_1095_ERROR_CODE,batchRequestType.getBatchMetadata().getBatchCategoryCode().value()) );
				enrollmentIn1095.setErrorMessage(responseMetadataType.getResponseDescriptionText().getValue());
				enrollmentIn1095.setErrorCode(responseMetadataType.getResponseCode().getValue());
			}
		}
		return enrollmentIn1095;
	}
	
	private void fillEnrollmentIn1095(BatchRequestType batchRequestType, EnrollmentIn1095 enrollmentIn1095 ){
		String documentSeqId = null;
		if(batchRequestType.getBatchMetadata() != null){
			String batchId = batchRequestType.getBatchMetadata().getBatchID().getValue();
			if(StringUtils.isNotEmpty(batchId)){
				if(batchId.contains("|")){
					enrollmentIn1095.setBatchId(batchId.split("\\|")[0]);
					documentSeqId = batchId.substring(batchId.indexOf("|")+EnrollmentConstants.ONE, batchId.length());
					enrollmentIn1095.setDocumentSeqId(documentSeqId);
				}else{
					enrollmentIn1095.setBatchId(batchId);
				}
			}
		}
		// Add MissingDocumentSequenceID in DocumentSeqId coloum
				if(batchRequestType.getServiceSpecificData() != null){
					List<BigInteger> missingDocumentSequenceIDList = batchRequestType.getServiceSpecificData().getMissingDocumentSequenceID();
					if(missingDocumentSequenceIDList != null && missingDocumentSequenceIDList.size() > 0){
						StringBuilder missingDocIds = new StringBuilder();
						DecimalFormat df = new DecimalFormat(String.valueOf("00000"));
						for (BigInteger bigInteger : missingDocumentSequenceIDList) {
							String temp = df.format(bigInteger);
							missingDocIds.append(temp).append("|");
						}
						if(missingDocIds.length() > 5){
							documentSeqId = missingDocIds.deleteCharAt(missingDocIds.length()-1).toString();
							enrollmentIn1095.setDocumentSeqId(documentSeqId);
						}
					}
		}
				
		if(documentSeqId == null){
			List<RequestDocumentType> requestDocList = batchRequestType.getAttachment();
			if(requestDocList != null && requestDocList.size() > 0){
				StringBuilder missingDocIds = new StringBuilder();
				for (RequestDocumentType requestDocumentType : requestDocList) {
					String temp = requestDocumentType.getDocumentSequenceID().getValue();
					missingDocIds.append(temp).append("|");
				}
				if(missingDocIds.length() > 5){
					documentSeqId = missingDocIds.deleteCharAt(missingDocIds.length()-1).toString();
					enrollmentIn1095.setDocumentSeqId(documentSeqId);
				}
			}
		}
				
		if(batchRequestType.getServiceSpecificData() != null && batchRequestType.getServiceSpecificData().getOriginalBatchID() != null){
			enrollmentIn1095.setPreviousBatchId(batchRequestType.getServiceSpecificData().getOriginalBatchID().toString());
		}
		if(batchRequestType.getBatchMetadata() != null && batchRequestType.getBatchMetadata().getBatchCategoryCode() != null){
			enrollmentIn1095.setBatchCategoryCode(batchRequestType.getBatchMetadata().getBatchCategoryCode().toString());
		}
		
		enrollmentIn1095.setIsProcessedFlag(EnrollmentConstants.N);
		
		if(batchRequestType.getServiceSpecificData() != null && batchRequestType.getServiceSpecificData().getEFTFileName() != null){
		enrollmentIn1095.setEftFileName(batchRequestType.getServiceSpecificData().getEFTFileName().getValue());
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processExchangeResponseMessage(File[] nonManifestResponseFiles, BatchRequestType manifestBatchRequestType, String responseFileName, Long jobExecutionId) {
		Map<String,Form1095ATransmissionExchRespType> attachmentMap = new HashMap<>();

		if(nonManifestResponseFiles != null && nonManifestResponseFiles.length == 1){
			for (File file : nonManifestResponseFiles) {
				try{
					JAXBContext jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment._1095.cy_2018.treasury.irs.msg.form1095atransmissionexchrespmessage.ObjectFactory.class);
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					JAXBElement<Form1095ATransmissionExchRespType> form1095ATransmissionExchRespType = null;
					form1095ATransmissionExchRespType = (JAXBElement<Form1095ATransmissionExchRespType>) jaxbUnmarshaller.unmarshal(file);
					attachmentMap.put(file.getName(), form1095ATransmissionExchRespType.getValue());
				}catch (JAXBException e) {
					LOGGER.error("JAXBException while processing exchange response File");
				}
			}
		}

		updateEnrollmentIn1095ForManifest(manifestBatchRequestType, attachmentMap, responseFileName, jobExecutionId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processEnrollmentIn1095ForOutResponse(File file, String responseFileName,  String outResponsePath, Long jobExecutionId) {
		JAXBContext jaxbContext = null;
		Unmarshaller jaxbUnmarshaller = null;
		BatchRequestType batchRequestType = null;
		try {
			jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment._1095.cy_2018.manifest.cms.dsh.bdshfferesp.exchange._1.ObjectFactory.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<BatchRequestType> batchRequestTypeList = null;
			batchRequestTypeList = (JAXBElement<BatchRequestType>) jaxbUnmarshaller.unmarshal(file);
			batchRequestType = batchRequestTypeList.getValue();
		}catch(JAXBException e) {
			LOGGER.error("Error while unmarshalling manifest", e);
		}
		
		File[] nonManifestResponseFiles = getNonManifestResponseFile(new File(outResponsePath).getAbsolutePath());
		if(null != batchRequestType && null != batchRequestType.getBatchMetadata()) {
			String batchId = batchRequestType.getBatchMetadata().getBatchID().getValue();
			if(StringUtils.isNotEmpty(batchId) &&  batchId.contains("|")){
				batchId = batchId.split("\\|")[0];
			}
			processExchangeResponseMessage(nonManifestResponseFiles, batchRequestType, responseFileName, jobExecutionId);
		}
	}
	
	private void updateEnrollmentIn1095ForManifest(BatchRequestType manifestBatchRequestType, Map<String,Form1095ATransmissionExchRespType> attachmentMap, String responseFileName, Long jobExecutionId) {

		List<EnrollmentIn1095> enrollmentIn1095List = new ArrayList<>();

		EnrollmentIn1095 enrollmentIn1095 = updateEnrollmentIn1095(manifestBatchRequestType, MANIFEST, responseFileName, jobExecutionId);

		if(attachmentMap != null && attachmentMap.size() > 0){
			List<EnrollmentInDtl1095> enrollmentInDtl1095List = new ArrayList<>();
			for(Entry<String, Form1095ATransmissionExchRespType> entry : attachmentMap.entrySet()){

				Form1095ATransmissionExchRespType attachmentType = entry.getValue();
				List<Form1095AResponseGrpType> attachmentList = attachmentType.getForm1095AResponseGrp();
				for (Form1095AResponseGrpType form1095aResponseGrpType : attachmentList) {

					EnrollmentInDtl1095 enrollmentInDtl1095 = new EnrollmentInDtl1095();

					enrollmentInDtl1095.setEnrollmentIn1095(enrollmentIn1095);
					enrollmentInDtl1095.setIsProcessedFlag(EnrollmentConstants.N);
					enrollmentInDtl1095.setErrorMessage(form1095aResponseGrpType.getErrorMessageTxt());
					enrollmentInDtl1095.setErrorCode(form1095aResponseGrpType.getErrorMessageCd());
					enrollmentInDtl1095.setXpathContent(form1095aResponseGrpType.getXpathContent());
					
					if (form1095aResponseGrpType.getSchemaErrorInfo() != null) {
						enrollmentInDtl1095.setSchemaErrorInfo(form1095aResponseGrpType.getSchemaErrorInfo());
					}
					
					if (form1095aResponseGrpType.getUniqueRecordSequenceNum() != null) {
						List<Integer> enrollmentIdList = enrollment1095AudRepository.getEnrollmentId(form1095aResponseGrpType.getUniqueRecordSequenceNum());
						if (enrollmentIdList != null && enrollmentIdList.size() > 0) {
							enrollmentInDtl1095.setEnrollmentId(enrollmentIdList.get(0));
						} else {
							LOGGER.error("Did not get enrollment id for UniqueRecordSequenceNum : " + form1095aResponseGrpType.getUniqueRecordSequenceNum());
						}
					}
					enrollmentInDtl1095List.add(enrollmentInDtl1095);
				}
			}
			enrollmentIn1095.setEnrollmentInDtl1095List(enrollmentInDtl1095List);
		}

		enrollmentIn1095List.add(enrollmentIn1095);

		for (EnrollmentIn1095 enrIn1095 : enrollmentIn1095List) {
			enrollmentIn1095Repository.save(enrIn1095);
		}
	}


}
