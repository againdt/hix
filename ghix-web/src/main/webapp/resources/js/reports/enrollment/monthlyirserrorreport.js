var app = angular.module('monthlyIRSErrorReportApp', ['ui.grid','ui.grid.exporter', 'ui.bootstrap','ui.router', 'ui.grid.resizeColumns', 'ui.grid.pagination']);


app.controller('MonthlyIRSErrorReportCtrl', ['$scope', '$filter', '$timeout', 'MonthlyIRSErrorReportService', 'uiGridConstants','uiGridExporterConstants','uiGridExporterService', function ($scope,$filter,$timeout,MonthlyIRSErrorReportService, uiGridConstants,uiGridExporterConstants,uiGridExporterService) {
	var paginationOptions = {
			pageNumber: 1,
			pageSize: 25,
			sort: null
	};
	
	$scope.colDef = [];
	$scope.externalConfig = {};
	$scope.dropDownColumns = [];
	$scope.data = null;
	
	var displayName = {month : 'Month', year: 'Year', householdCaseId : 'Household Case ID', 
			errorCode : 'Error Code', errorMessage : 'Error Message',
			errorFieldName : 'Error Field Name', batchId : 'Batch ID', xpathContent : 'XPath Content'};
	
	var monthandYearFilter = function(start,end) {
		var selectOptions = [];
		for (var i=start; i<=end; i++) {
			selectOptions.push({ value: i, label : i});
		}
		return selectOptions;
	};
	
	var setPropertiesFn = function (elem, key) {
		var filter = { type: uiGridConstants.filter.SELECT, condition : uiGridConstants.filter.EXACT };
		
        switch (key) {
            case 'month':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(1, 12);
            	$scope.dropDownColumns.push(key);
            	break;
            case 'year':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(2015, new Date().getFullYear()+1);
            	$scope.dropDownColumns.push(key);
                break;
            case 'xpathContent':
            	elem.enableFiltering = false;
            	elem.enableSorting = false;
            case 'errorMessage':
            	elem.enableFiltering = false;
            	elem.enableSorting = false;
            default:
            	//elem.enableSorting = true;
            	//elem.enableFiltering = false;
//            	if(key != 'sbmsSbmrRcvd' && key != 'missingPolicyIds' && key != 'skipRecords' && key != 'fileName')
//            		elem.enableFiltering = false;
//            	if(key != 'hoisIssuerId')
//            		elem.enableSorting = false;

        }
    };
	
	MonthlyIRSErrorReportService.getReport(paginationOptions.pageNumber,
			paginationOptions.pageSize, null).success(function(data){
				$scope.gridOptions.data = data.content;
				$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
				
				if(data.size != 0) {
					angular.forEach(data.content[0], function(value, key) {
						var elem = {name:key};
						setPropertiesFn(elem,key);
						$scope.colDef.push(elem);
					});
				}
			});
	$scope.gridOptions = {
			paginationPageSizes: [25, 50, 75, 100],
			paginationPageSize: paginationOptions.pageSize,
			useExternalPagination: true,
			columnDefs:  $scope.colDef,
			enableFiltering: true,
			enableGridMenu: true, 
			useExternalFiltering: true,
			useExternalSorting: true,
			exporterMenuPdf: false,
			exporterMenuCsv: false,
			exporterPdfFilename: 'download.pdf',
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
			onRegisterApi: function(gridApi) {
				$scope.gridApi = gridApi;
				
				$scope.gridApi.core.on.filterChanged( $scope, function() {
                    var grid = this.grid;
                    var filterExact = {}, filterLike = {};
                    angular.forEach(grid.columns, function (column, index) {
                            if (column.filters && column.filters[0].term) {
                                    if($scope.dropDownColumns.indexOf(column.field) !== -1)
                                            filterExact[column.field] = column.filters[0].term;
                                    else
                                            filterLike[column.field] = column.filters[0].term;
                            }
                    });
                    $scope.externalConfig.filterExact = filterExact;
                    $scope.externalConfig.filterLike = filterLike;
                    MonthlyIRSErrorReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                          $scope.data = $scope.gridOptions.data = data.content;
                                          $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                                  });
                   });
                  
                  $scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){
                          var grid = this.grid;
                    var asc = [], desc = [];
                    angular.forEach(grid.columns, function (column, index) {
                            console.log(column);
                            if (column.sort && column.sort.direction) {
                                    if(column.sort.direction == uiGridConstants.ASC)
                                            asc.push(column.field);
                                    else if(column.sort.direction == uiGridConstants.DESC)
                                            desc.push(column.field);
                            }
                        });
                    $scope.externalConfig[uiGridConstants.ASC] = asc;
                    $scope.externalConfig[uiGridConstants.DESC] = desc;
                    MonthlyIRSErrorReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                          $scope.data = $scope.gridOptions.data = data.content;
                                          $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                                  });
                  });
                  
				gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
					paginationOptions.pageNumber = newPage;
					paginationOptions.pageSize = pageSize;
					MonthlyIRSErrorReportService.getReport(newPage,pageSize, $scope.externalConfig).success(function(data){
						$scope.data = $scope.gridOptions.data = data.content;
						$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
					});
				});
			}
	};
	
}]);

app.service('MonthlyIRSErrorReportService',['$http', function ($http) {

	function getReport(pageNumber,size,requestdata) {
		return  $http({
			method: 'POST',
			url: '/hix/admin/reports/monthlyirserrorreport?pageNumber='+pageNumber+'&pageSize='+size,
            dataType: "json",
            headers: {
                'Content-Type': "application/json",
                "csrftoken": $('#tokid').val()
            },
            data: JSON.stringify(requestdata)
		});
	}
	return {
		getReport:getReport
	};

}]);