<%@ include file="../include.jsp" %>
 
<div id="appscr59B" style="display:none;">
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page9.familyAndHousehold" text="Family & Household" /></h5>
		</div>
		
		<div class="container-fluid">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page9B.weNeedToKnow" text="We need to know more details about everyone that is applying for health insurance." />
				</p>
				<p>
					<em>
						<spring:message code="ssap.page9B.allFields" text="All fields on this Family & Household section are required unless otherwise indicated." /> 
					</em>
				</p>
				
				<hr>
				
				<div class="control-group">
					<strong><spring:message code="ssap.page2.youMayNeed" text="You may need:" /></strong> 
				</div>
				
				<div class="control-group">
					<p>
						<img alt="Time Estimate" src="<c:url value="/resources/images/icons/arow.png" />" class="arrow_ui_design" />
						<spring:message code="ssap.page9B.youMayNeedText1" text="Social Security numbers" />
					</p>
				</div>
				
				<img alt="Time Estimate" src="<c:url value="/resources/images/icons/2button.png" />" class="call-us-img pull-right">
				
				
				<div class="control-group margin30-t">
					<p>
						<strong>
							<img alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
							<spring:message code="ssap.page9.estimatedTime" text="Estimated time for this section:" />&nbsp;
							<spring:message code="ssap.page9.estimatedTimeMinutes" text="10 Minutes &ndash; 15 Minutes" />
						</strong>
					</p>
				</div>
			</div>
		</div>
		<!-- content above -->
	</div>
</div>
