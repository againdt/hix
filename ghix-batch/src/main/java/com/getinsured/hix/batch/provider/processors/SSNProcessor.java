package com.getinsured.hix.batch.provider.processors;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class SSNProcessor implements ProviderDataFieldProcessor {

    private ValidationContext context;
    private int index;
    
    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated)
            throws ProviderValidationException, InvalidOperationException {
        String s;
        int intSSN;
        try {
            s = (String) objToBeValidated;
            String strSSN = s.trim();
            if (strSSN.length() != 9) {
                throw new ProviderValidationException("Validation of SSN failed. Length of SSN is not 9.");
            }
            String left = strSSN.substring(0, 4);
            String mid = strSSN.substring(4, 6);
            String right = strSSN.substring(6);
            if (left.equals("000") || mid.equals("00") || right.equals("0000")) {
                throw new ProviderValidationException("Validation of SSN failed. It looks like an invalid SSN number.");
            }
            intSSN = Integer.valueOf(s);
        } catch (NumberFormatException e) {
            throw new ProviderValidationException("Validation of SSN failed. It seems to contain non-digit characters.");
        }
        return intSSN;
    }

}
