package com.getinsured.hix.batch.enrollment.xmlEnrollments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

public class EnrollmentFileResource {
	
	private int recordsRead;
	private int recordsSkipped;
	private URI filepathURL;
	private int index;
	private Resource resource = null;
	private boolean badResource = false;
	private Logger logger = LoggerFactory.getLogger(EnrollmentFileResource.class);
	
	public EnrollmentFileResource(URI uri){
		this.filepathURL = uri;
	}
	public int getRecordsRead() {
		return recordsRead;
	}
	public void setRecordsRead(int recordsRead) {
		this.recordsRead = recordsRead;
	}
	public int getRecordsSkipped() {
		return recordsSkipped;
	}
	public void setRecordsSkipped(int recordsSkipped) {
		this.recordsSkipped = recordsSkipped;
	}
	public URI getFilepathURL() {
		return filepathURL;
	}
	public void setFilepathURL(URI uri) {
		this.filepathURL = uri;
	}
	@Override
	public String toString() {
		return "EnrollmentFileResource ["+filepathURL+"] Records Read=" + recordsRead
				+ ", Records Skipped=" + recordsSkipped+ "]";
	}
	public void setindex(int i) {
		this.index = i;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public Resource getResource() throws FileNotFoundException {
		if(this.filepathURL != null) {
			resource = new InputStreamResource(new FileInputStream(new File(this.filepathURL)));
		}
		return this.resource;
	}
	public void setBadResource(boolean b) {
		if(!b) {
			logger.error("Marking "+this.filepathURL+ " a bad resource");
		}
		this.badResource = b;
	}
	
	public boolean isBadResource() {
		return this.badResource;
	}
	public void incrementReadCount() {
		this.recordsRead++;
	}
	
	public void incrementSkipCount() {
		this.recordsSkipped++;
	}
}
