<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<jsp:useBean id="now" class="java.util.Date" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript"> 
function validateForm(){
	if( $("#frmsignature").validate().form() ) {
		 if( $(".terms:checked").length < 3){
			 alert('Please accept all Attestations.');
			 return false;
		 }else{
			 $("#frmsignature").submit();
		 }
	 }
}

</script>
<div class="gutter10">
	<div class="page-header">
		<h1>Employer Application: <small>Signature</small></h1>
	</div>

	<div class="row-fluid">		
		<div class="span3">
			<div class="gutter10"></div>
		</div>
		<div class="span9">
			<form class="form-horizontal" id="frmsignature" name="frmsignature" action="signature" method="POST">
				<df:csrfToken/>
					<div class="control-group">
						<div class="control-group">
							 <h3>Attestations</h3>
	                         <label class="checkbox">
	                           	 <input type="checkbox" class="terms" name="terms1" id="terms1" value="1">
	                           	 I authorize the California Exchange to verify the information contained in this application.
	                         </label>
	                         <label class="checkbox">
	                          	 <input type="checkbox" class="terms" name="terms2" id="terms2" value="2">
	                           	 I agree to offer health coverage to, at minimum, all full-time employees. 
	                         </label>
	                         <label class="checkbox">
	                          	 <input type="checkbox" class="terms" name="terms3" id="terms3" value="3">
	                           	 I attest that the information I have provided herein is accurate. 
	                         </label>
                 		</div>
                 		
                 		<div class="control">
                 			<h3>Signature</h3> 
                 			
                            <div class="control-group">
                            		<label class="control-label">Corporate Representative:</label>
                            		<div class="controls">
                            			<label class="data">${corpRepresentative}</label>
                            		</div>
							</div>
						</div>
                 		<div class="control">
                            <div class="control-group">
                            		<label class="control-label">Applicant E-Signature <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            		<div class="controls">
                            			<input type="text" class="input-xlarge" id="esignBy" name="esignBy" size="30">
                            			<p><small>Type your full name here as your electronic signature.</small></p>
                            			<div id="esignBy_error"></div>
                            		</div>
                            		
							</div>
						</div>
						<div class="control">
							<div class="control-group">
                            		<label class="control-label">Today&#39;s Date</label>
                            		<div class="controls">
                            			<input type="text" readonly class="input-mini" maxlength="2" id="today_mm" name="today_mm" value="<fmt:formatDate value="${now}" type="both" pattern="MM" />">
		                                <input type="text" readonly class="input-mini" maxlength="2" id="today_dd" name="today_dd" value="<fmt:formatDate value="${now}" type="both" pattern="dd" />">
		                                <input type="text" readonly class="input-mini" size="4" maxlength="4" id="today_yy" name="today_yy" value="<fmt:formatDate value="${now}" type="both" pattern="yyyy" />">
                            		</div>
                             </div>
                 		</div>
					</div>
				<div class="form-actions">
					<input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" class="btn btn-primary pull-right" value="Next" />
					</br></br>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
var validator = $("#frmsignature").validate({ 
	rules : {
		esignBy : {required: true},
	},
	messages : {
		esignBy: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>"},
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});
</script>