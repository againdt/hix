{
"applicationId": "50",
"clientIp": "192.168.56.10",
"operation": "",
"payload":{
		"householdMember": [
			{
				"personId": "1",
				"name": {
					"firstName": "Jessica",
					"middleName": "K",
					"lastName": "Miller"
				},
				"dateOfBirth": "Jun 28, 1959 0:00:00 PM",
				"socialSecurityCard": {
					"socialSecurityNumber": "222-22-2401"
				},
				"citizenshipImmigrationStatus": {
					"citizenshipAsAttestedIndicator": false
				},
				"incarcerationStatus": {
					"incarcerationAsAttestedIndicator": false
				}
			}
		]
	}
}