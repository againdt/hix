package com.getinsured.hix.enrollment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.Enrollment1095DTO;
import com.getinsured.hix.dto.enrollment.Enrollment1095SearchFilter;
import com.getinsured.hix.dto.enrollment.EnrollmentMember1095DTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremium1095DTO;
import com.getinsured.hix.enrollment.repository.Enrollment1095EditRepository;
import com.getinsured.hix.enrollment.util.CreateDTOBean;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentPremium1095;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

/**
 * Enrollment1095EditServiceImpl is the implementation of {@link Enrollment1095EditService}
 * 
 */

@Service("enrollment1095EditService")
@Transactional
public class Enrollment1095EditServiceImpl implements Enrollment1095EditService {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095EditServiceImpl.class);
	@Autowired private Enrollment1095EditRepository enrollment1095EditRepository;
	@Autowired private CreateDTOBean createDTOBean;
	@Autowired private UserService userService;
	@PersistenceUnit private EntityManagerFactory emf;
	private static final String DATE_FORMAT = "MM/dd/yyyy hh.mm aa";
	private static final String IRS_HEALTH_EXCHANGE_ID = "enrollment.irs.HealthExchangeId";
	private static final String EDIT_TOOL = "EDIT_TOOL";
	private static final String YES_FLAG = "Y";
	private static final String NO_FLAG = "N";
	private static final String DATA = "data";
	private static final String TOTAL_RECORDS = "totalRecords";
	private static final String SSN_DISPLAY_FORMAT = "%s-%s-%s";
	private static final String ZERO_STRING = "0";
	private static final String HIPHEN = "-";
	private static final String EMPTY_STRING = "";
	
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int ELEVEN = 11;
	public static final int TWELVE = 12;
	public static final int TWENTY_FIVE = 25;
	
	private static final String searchSelectClause = "SELECT recepient.FIRST_NAME AS recepient_FName, recepient.LAST_NAME AS recepient_LName, recepient.SSN AS recepient_SSN, spouse.FIRST_NAME AS spouse_FName, spouse.LAST_NAME AS spouse_LName, spouse.SSN AS spouse_SSN, en.EXCHG_ASSIGNED_POLICY_ID, en.COVERAGE_YEAR, en.PDF_GENERATION_TIMESTAMP, en.ID AS ENROLLMENT_1095_ID";
	private static final String searchWhereClause = " WHERE ";
	private static final String countSelectClause = "SELECT count(*) as totalRecords";
	private static final String recepientSelect = "( SELECT enMemRec.FIRST_NAME, enMemRec.LAST_NAME, enMemRec.SSN, enMemRec.ENROLLMENT_1095_ID FROM ENROLLMENT_MEMBER_1095 enMemRec WHERE  enMemRec.MEMBER_TYPE = 'RECEPIENT' ) recepient ON recepient.ENROLLMENT_1095_ID = en.ID";
	private static final String spouseSelect = "( SELECT enMemSp.FIRST_NAME, enMemSp.LAST_NAME, enMemSp.SSN, enMemSp.ENROLLMENT_1095_ID FROM ENROLLMENT_MEMBER_1095 enMemSp WHERE enMemSp.MEMBER_TYPE = 'SPOUSE') spouse ON spouse.ENROLLMENT_1095_ID=en.ID";
	
	public enum QueryType {SEARCH_SELECT, COUNT_SELECT};
	private static Map<Integer , String> monthMap = new HashMap<Integer , String>();
	
	static
	{
		monthMap.put(ONE,"JANUARY");
		monthMap.put(TWO,"FEBRUARY");
		monthMap.put(THREE,"MARCH");
		monthMap.put(FOUR,"APRIL");
		monthMap.put(FIVE,"MAY");
		monthMap.put(SIX,"JUNE");
		monthMap.put(SEVEN,"JULY");
		monthMap.put(EIGHT,"AUGUST");
		monthMap.put(NINE,"SEPTEMBER");
		monthMap.put(TEN,"OCTOBER");
		monthMap.put(ELEVEN,"NOVEMBER");
		monthMap.put(TWELVE,"DECEMBER");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Map<String , Object> getEnrollment1095SearchResult(Enrollment1095SearchFilter enrollment1095SearchFilter) 
	{
		Map<String , Object> dataMap = new HashMap<String , Object>();
		String firstName =  enrollment1095SearchFilter.getFirstName();
		String lastName = enrollment1095SearchFilter.getLastName(); 
		Long policyNumber = enrollment1095SearchFilter.getPolicyNumber();
		String ssn = enrollment1095SearchFilter.getSsn(); 
		Integer coverageYear =  enrollment1095SearchFilter.getCoverageYear();
		String sortOrder =  enrollment1095SearchFilter.getSortOrder();
		Integer pageSize =  enrollment1095SearchFilter.getPageSize() == null ? TWENTY_FIVE :enrollment1095SearchFilter.getPageSize();
		Integer start =  enrollment1095SearchFilter.getPageNumber()  == null ? ONE : enrollment1095SearchFilter.getPageNumber(); 
		Integer totalRecords = 0;
		List<Enrollment1095DTO> data = new ArrayList<Enrollment1095DTO>();
		EntityManager entityManager = emf.createEntityManager();
		try 
		{
			Query searchQuery = entityManager.createNativeQuery(getQueryString(QueryType.SEARCH_SELECT, firstName, lastName, policyNumber, ssn, coverageYear, sortOrder));
			Query countQuery = entityManager.createNativeQuery(getQueryString(QueryType.COUNT_SELECT, firstName, lastName, policyNumber, ssn, coverageYear, sortOrder));
			
			start = (start-1)*pageSize;
			searchQuery.setFirstResult(start);
			searchQuery.setMaxResults(pageSize);
			setQueryParameters(searchQuery, firstName, lastName, policyNumber, ssn, coverageYear);
			setQueryParameters(countQuery, firstName, lastName, policyNumber, ssn, coverageYear);
			
			
			List<Object[]> resultList = searchQuery.getResultList();
			for(int i=0;i<resultList.size();i++)
			{
				Object[] objArray = resultList.get(i);
				data.add(setEnrollment1095Dto(objArray));
			}
			
			totalRecords = Integer.valueOf(countQuery.getSingleResult().toString());
		} 
		catch (Exception e) {
			LOGGER.error("Exception while searching for Enrollment1095 : "+ e);
		}
		finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
			}
		}
		
		dataMap.put(DATA, data);
		dataMap.put(TOTAL_RECORDS, totalRecords);
		return dataMap;
	}
	
	private void setQueryParameters (Query query,String firstName,String lastName,Long policyNumber,String ssn,Integer coverageYear)
	{
		if( firstName != null && StringUtils.isNotBlank(firstName))
		{
			query.setParameter("firstName", "%"+firstName+"%");
		}
		if( lastName != null && StringUtils.isNotBlank(lastName))
		{
			query.setParameter("lastName", "%"+lastName+"%");
		}
		if( policyNumber != null && StringUtils.isNotBlank(policyNumber.toString()))
		{
			query.setParameter("policyNumber", policyNumber);
		}
		if( ssn != null && StringUtils.isNotBlank(ssn))
		{
			query.setParameter("ssn", "%"+ssn);
		}
		if( coverageYear != null && StringUtils.isNotBlank(coverageYear.toString()))
		{
			query.setParameter("coverageYear", coverageYear);
		}
	}

	private String getQueryString(QueryType queryType, String firstName,String lastName,Long policyNumber,String ssn,Integer coverageYear , String sortOrder)
	{
		StringBuilder query = new StringBuilder();
		if(queryType.equals(QueryType.SEARCH_SELECT)){
			query.append(searchSelectClause);
		}
		else if(queryType.equals(QueryType.COUNT_SELECT)){
			query.append(countSelectClause);
		}
		query.append(" FROM ENROLLMENT_1095 en LEFT JOIN ");
		
		query.append(recepientSelect).append(" LEFT JOIN ").append(spouseSelect);
		boolean addAnd=false;
		StringBuilder whereClause = new StringBuilder(searchWhereClause);
		if( coverageYear != null && StringUtils.isNotBlank(coverageYear.toString()))
		{
			whereClause.append(" en.COVERAGE_YEAR =:coverageYear ");
			addAnd=true;
		}
		
		
		StringBuilder andQuery =  new StringBuilder();
		List<String> andQueryParams = new ArrayList<String>();
		if( ssn != null && StringUtils.isNotBlank(ssn))
		{
			//sb.append(" lower(enMem.SSN) like lower('%"+ssn+"') ");
			andQueryParams.add("(lower(recepient.SSN) like lower(:ssn) OR lower(spouse.SSN) like lower(:ssn))");
		}
		if( firstName != null && StringUtils.isNotBlank(firstName))
		{
			//orQueryParams.add("enMem.FIRST_NAME =:firstName");
			andQueryParams.add("(lower(recepient.FIRST_NAME) like lower(:firstName) OR lower(spouse.FIRST_NAME) like lower(:firstName))");
		}
		if( lastName != null && StringUtils.isNotBlank(lastName))
		{
			andQueryParams.add("(lower(recepient.LAST_NAME) like lower(:lastName) OR lower(spouse.LAST_NAME) like lower(:lastName))");
		}
		if( policyNumber != null && StringUtils.isNotBlank(policyNumber.toString()))
		{
			andQueryParams.add("en.EXCHG_ASSIGNED_POLICY_ID =:policyNumber");
		}
		
		if( !andQueryParams.isEmpty())
		{
			andQuery.append("(") ;
			Iterator<String> iterator = andQueryParams.iterator();
			while(iterator.hasNext()) 
			{
				if(andQuery.length() > 1)
				{
					andQuery.append(" AND ") ;
				}
			    andQuery.append(iterator.next());
			}
			andQuery.append(")") ;
		}
		
		if( !andQueryParams.isEmpty())
		{
			if(addAnd){
				whereClause.append(" AND ");
			}
			whereClause.append(andQuery.toString());
		}
		
		query.append(whereClause.toString());
		
		if(queryType.equals(QueryType.SEARCH_SELECT)){
		if( sortOrder == null || StringUtils.isBlank(sortOrder))
		{
			sortOrder = "ASC";
		}
		query.append(" ORDER BY recepient.FIRST_NAME ").append(sortOrder);
		}
		
		LOGGER.info("Query --> " + query.toString());
		return query.toString();
	}
	
	private Enrollment1095DTO setEnrollment1095Dto(Object[] objArray)
	{
		Enrollment1095DTO enDto = new Enrollment1095DTO();
		enDto.setRecepientFirstName(getNotNullString(objArray[ZERO]));
		enDto.setRecepientLastName(getNotNullString(objArray[ONE]));
		enDto.setRecepientSsn(getSsnString(objArray[TWO] != null ? objArray[TWO].toString() : null));
		enDto.setSpouseFirstName(getNotNullString(objArray[THREE]));
		enDto.setSpouseLastName(getNotNullString(objArray[FOUR]));
		enDto.setSpouseSsn(getSsnString(objArray[FIVE] != null ? objArray[FIVE].toString() : null));
		enDto.setPolicyNumber(objArray[SIX] != null ? getFormattedPolicyNumber(objArray[SIX]) : null);
		enDto.setCoverageYear(objArray[SEVEN] != null ? Integer.valueOf(objArray[SEVEN].toString()) : null);
		enDto.setPdfGeneratedOn(objArray[EIGHT] != null ? DateUtil.dateToString(TSDate.getNoOffsetTSDate(Timestamp.valueOf(objArray[EIGHT].toString()).getTime()), GhixConstants.REQUIRED_DATE_FORMAT) : null);
		enDto.setId(objArray[NINE] != null ? Integer.valueOf(objArray[NINE].toString()) : null);
		
		return enDto;
	}
	
	private String getSsnString( String ssn)
	{
		if( ssn != null)
		{
			String substr = ssn.substring(ssn.length() - FOUR);
			return "***-**-" + substr;
		}
		
		return "";
	}
	
	private  String getFormattedSsnString(String ssn , boolean update)
	{
		String formattedSsnString = null;
		if(ssn != null )
		{
			if( update )
			{
				formattedSsnString = ssn.replace(HIPHEN,EMPTY_STRING);
			}
			else
			{
				if(ssn.length() == NINE)
				{
					formattedSsnString = String.format(SSN_DISPLAY_FORMAT, ssn.substring(ZERO,THREE), ssn.substring(THREE,FIVE), ssn.substring(FIVE,NINE));
				}
			}
		}
		return formattedSsnString;
	}
	
	private  String getFormattedPolicyNumber(Object enrollmentId )
	{
		String formattedPolicyNumber = null;
		if(enrollmentId != null )
		{
			if(enrollmentId.toString().length() < SIX){
			formattedPolicyNumber = StringUtils.leftPad(enrollmentId.toString(), SIX , ZERO_STRING);
		}
			else{
				formattedPolicyNumber = enrollmentId.toString();
			}
		}
		
		return formattedPolicyNumber;
	}
	
	private  Integer getIntegerPolicyNumber(String enrollmentId )
	{
		Integer formattedPolicyNumber = null;
		if(enrollmentId != null && StringUtils.isNotBlank(enrollmentId))
		{
			formattedPolicyNumber = Integer.valueOf(enrollmentId);
		}
		
		return formattedPolicyNumber;
	}
	
	
	private String getNotNullString(Object obj)
	{
		String substr = EMPTY_STRING;
		if( obj != null){
			substr = obj.toString();
		}
		return substr;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Enrollment1095DTO getDetailsById(Integer id) 
	{
		Enrollment1095 enrollment1095 = enrollment1095EditRepository.findEnrollment1095ById(id);
		Enrollment1095DTO enDto = new Enrollment1095DTO();
		try 
		{
			if( enrollment1095 != null)
			{
				createDTOBean.copyProperties(enDto, enrollment1095);
				enDto.setExchgAsignedPolicyId(getFormattedPolicyNumber(enrollment1095.getExchgAsignedPolicyId()));
				enDto.setPdfGeneratedOn(enrollment1095.getPdfGeneratedOn() != null ? DateUtil.dateToString( enrollment1095.getPdfGeneratedOn(), DATE_FORMAT) : null );
				enDto.setCmsXmlGeneratedOn(enrollment1095.getCmsXmlGeneratedOn() != null ? DateUtil.dateToString( enrollment1095.getCmsXmlGeneratedOn(), DATE_FORMAT) : null );
				enDto.setCreatedOn(enrollment1095.getCreatedOn() != null ? DateUtil.dateToString( enrollment1095.getCreatedOn(), DATE_FORMAT) : null );
				enDto.setUpdatedOn(enrollment1095.getUpdatedOn() != null ? DateUtil.dateToString( enrollment1095.getUpdatedOn(), DATE_FORMAT) : null );
				enDto.setPolicyStartDate(enrollment1095.getPolicyStartDate() != null ? DateUtil.dateToString( enrollment1095.getPolicyStartDate(), GhixConstants.REQUIRED_DATE_FORMAT): null );
				enDto.setPolicyEndDate(enrollment1095.getPolicyEndDate() != null ? DateUtil.dateToString( enrollment1095.getPolicyEndDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null );
				enDto.setMarketPlaceIdentifier(DynamicPropertiesUtil.getPropertyValue(IRS_HEALTH_EXCHANGE_ID));
				setMemberAndPremiumData(enrollment1095,enDto);
				//EnrollmentMember1095 result = getMember1095DetailsByEnrollment1095IdAndMemberType(id,"RECEPIENT");
			}
		} 
		catch (Exception e)
		{
			LOGGER.error("Exception in getDetailsById() : "+ e.getMessage());
		}
		return enDto;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Enrollment1095 getById(Integer id) 
	{
		return enrollment1095EditRepository.findEnrollment1095ById(id);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setMemberAndPremiumData(Enrollment1095 enrollment1095 , Enrollment1095DTO enDto) throws Exception
	{
		List<EnrollmentMember1095>  members = enrollment1095.getEnrollmentMembers();
		List<EnrollmentPremium1095> premiums = enrollment1095.getEnrollmentPremiums();
		List<EnrollmentMember1095DTO>  newMemberList = new ArrayList<EnrollmentMember1095DTO>();
		List<EnrollmentPremium1095DTO>  newPremiumsList = new ArrayList<EnrollmentPremium1095DTO>();
		if( members != null)
		{
			for(EnrollmentMember1095 member : members)
			{
				EnrollmentMember1095DTO enrollmentMember1095DTO = new EnrollmentMember1095DTO();
				createDTOBean.copyProperties(enrollmentMember1095DTO, member);
				member.setEnrollment1095(null);
				enrollmentMember1095DTO.setEnrollment1095(null);
				enrollmentMember1095DTO.setCoverageEndDate(member.getCoverageEndDate() != null ? DateUtil.dateToString( member.getCoverageEndDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
				enrollmentMember1095DTO.setCoverageStartDate(member.getCoverageStartDate() != null ? DateUtil.dateToString( member.getCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null );
				enrollmentMember1095DTO.setCreatedOn(member.getCreatedOn() != null ? DateUtil.dateToString( member.getCreatedOn(), DATE_FORMAT) : null);
				enrollmentMember1095DTO.setUpdatedOn(member.getUpdatedOn() != null ? DateUtil.dateToString( member.getUpdatedOn(), DATE_FORMAT) : null);
				enrollmentMember1095DTO.setBirthDate(member.getBirthDate() != null ? DateUtil.dateToString( member.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null );
				enrollmentMember1095DTO.setSsn(getFormattedSsnString(member.getSsn(),false));
				newMemberList.add(enrollmentMember1095DTO);
			}
		}
		
		if( premiums != null)
		{
			Collections.sort(premiums, new Comparator() {
		        @Override
				public int compare(Object o1, Object o2) {

		            int x1 = ((EnrollmentPremium1095) o1).getMonthNumber();
		            int x2 = ((EnrollmentPremium1095) o2).getMonthNumber();
		            return x1 - x2;
		        }
		    });
			
			Integer startMonth = premiums.get(0).getMonthNumber();
			Integer endMonth = premiums.get(premiums.size()-1).getMonthNumber();
			Float defaultEhb = getDefaultEhb(premiums); 
			if( startMonth == ONE && endMonth == TWELVE)
			{
				for(EnrollmentPremium1095 premium : premiums)
				{
					EnrollmentPremium1095DTO premiumDto = new EnrollmentPremium1095DTO();
					createDTOBean.copyProperties(premiumDto, premium);
					if( !YES_FLAG.equals(premium.getIsActive()))
					{
						premiumDto.setEhbPercent(defaultEhb);
						premiumDto.setGrossPremium(0.0f);
						premiumDto.setSlcspAmount(0.0f);
						premiumDto.setAptcAmount(null);
					}
					newPremiumsList.add(premiumDto);
				}
			}
			else
			{
				Map<Integer , String> missingMonths = new HashMap<Integer , String>();
				missingMonths.putAll(monthMap);
				
				for(EnrollmentPremium1095 premium : premiums)
				{
					if(monthMap.containsKey(premium.getMonthNumber()))
					{
						EnrollmentPremium1095DTO enPremiumDto = new EnrollmentPremium1095DTO();
						createDTOBean.copyProperties(enPremiumDto, premium);
						if( !YES_FLAG.equals(premium.getIsActive()))
						{
							enPremiumDto.setEhbPercent(defaultEhb);
							enPremiumDto.setGrossPremium(0.0f);
							enPremiumDto.setSlcspAmount(0.0f);
							enPremiumDto.setAptcAmount(null);
						}
						newPremiumsList.add(enPremiumDto);
						missingMonths.remove(premium.getMonthNumber());
					}
				}
				
				for( Integer i : missingMonths.keySet())
				{
					EnrollmentPremium1095DTO premium1095Dto = new EnrollmentPremium1095DTO();
					premium1095Dto.setMonthName(missingMonths.get(i));
					premium1095Dto.setMonthNumber(i);
					premium1095Dto.setEhbPercent(defaultEhb);
					premium1095Dto.setGrossPremium(0.0f);
					premium1095Dto.setSlcspAmount(0.0f);
					premium1095Dto.setAptcAmount(null);
					newPremiumsList.add(premium1095Dto);
				}
			}
		}

		Collections.sort(newPremiumsList, new Comparator() {
	        @Override
			public int compare(Object o1, Object o2) {

	            int x1 = ((EnrollmentPremium1095DTO) o1).getMonthNumber();
	            int x2 = ((EnrollmentPremium1095DTO) o2).getMonthNumber();
	            return x1 - x2;
	        }
	    });
		enDto.setEnrollmentPremiums(newPremiumsList);
		enDto.setEnrollmentMembers(newMemberList);
	}
	
	@Override
	public boolean updateResendFlag(Integer id) 
	{
		Enrollment1095 enrollment1095 = enrollment1095EditRepository.findEnrollment1095ById(id);
		AccountUser user = getUser();
		enrollment1095.setResendPdfFlag(YES_FLAG);
		enrollment1095.setCorrectionSource(EDIT_TOOL);
		enrollment1095.setUpdatedBy(user.getId());
		enrollment1095 = enrollment1095EditRepository.saveAndFlush(enrollment1095);
		if(enrollment1095!= null){
			return true;
		}
		else {
			return false;
		}
		/*int noOfRecordsUpdated = enrollment1095EditRepository.updateResendFlag(id, "Y");
		if(noOfRecordsUpdated > 0 ){
			return true;
		}
		return false;*/
	}
	
	@Override
	public boolean updateEnrollment1095( Enrollment1095DTO enrollment1095Dto) 
	{
		Enrollment1095 enrollment1095Edited = enrollment1095EditRepository.findEnrollment1095ById(enrollment1095Dto.getId());
		AccountUser user = getUser();
		try 
		{
			setMemberAndPremiumDataForUpdate(enrollment1095Edited, enrollment1095Dto ,user);
			String updateNotes = enrollment1095Dto.getUpdateNotes();
			if( updateNotes != null && StringUtils.isNotBlank(updateNotes))
			{
				enrollment1095Edited.setCorrectionSource(EDIT_TOOL);
				enrollment1095Edited.setCorrectionIndicatorPdf(YES_FLAG);
				enrollment1095Edited.setCorrectionIndicatorXml(YES_FLAG);
				enrollment1095Edited.setCorrectedCheckBoxIndicator(YES_FLAG);
				enrollment1095Edited.setEditToolOverwritten(null);
				enrollment1095Edited.setUpdatedBy(user.getId());
				enrollment1095Edited = enrollment1095EditRepository.saveAndFlush(enrollment1095Edited);
			}
			
		} 
		catch (Exception e) 
		{
			LOGGER.error("Exception in updateEnrollment1095() : "+ e.getMessage());
			return false;
		}
		if( enrollment1095Edited != null){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	private void setMemberAndPremiumDataForUpdate(Enrollment1095 enrollment1095 , Enrollment1095DTO enDto , AccountUser user) throws Exception
	{
		List<EnrollmentMember1095DTO>  membersDto = enDto.getEnrollmentMembers();
		List<EnrollmentPremium1095DTO> premiumDtoList = enDto.getEnrollmentPremiums();
		List<EnrollmentPremium1095> premiumList = enrollment1095.getEnrollmentPremiums();
		List<EnrollmentPremium1095> newPremiumList = new ArrayList<EnrollmentPremium1095>();
		enrollment1095.setCoverageYear(enDto.getCoverageYear());
		enrollment1095.setPolicyEndDate(DateUtil.StringToDate( enDto.getPolicyEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		enrollment1095.setPolicyStartDate(DateUtil.StringToDate( enDto.getPolicyStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		enrollment1095.setPolicyIssuerName(enDto.getPolicyIssuerName());
		enrollment1095.setUpdateNotes(enDto.getUpdateNotes());
		enrollment1095.setExchgAsignedPolicyId(getIntegerPolicyNumber(enDto.getExchgAsignedPolicyId()));
		List<EnrollmentMember1095>  membersList = enrollment1095.getEnrollmentMembers();
		Integer startMonth = getMonthnumber(enrollment1095.getPolicyStartDate());
		Integer endMonth = getMonthnumber(enrollment1095.getPolicyEndDate());
		
		if( membersDto != null)
		{
			for(EnrollmentMember1095DTO memberDTO : membersDto)
			{
				for(EnrollmentMember1095 member : membersList)
				{
					if( member.getId() == memberDTO.getId())
					{
						member.setFirstName(memberDTO.getFirstName());
						member.setMiddleName(memberDTO.getMiddleName());
						member.setLastName(memberDTO.getLastName());
						member.setAddress1(memberDTO.getAddress1());
						member.setAddress2(memberDTO.getAddress2());
						member.setCity(memberDTO.getCity());
						member.setState(memberDTO.getState());
						member.setZip(memberDTO.getZip());
						member.setCoverageEndDate( memberDTO.getCoverageEndDate() != null ? DateUtil.StringToDate( memberDTO.getCoverageEndDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
						member.setCoverageStartDate(memberDTO.getCoverageStartDate() != null ? DateUtil.StringToDate( memberDTO.getCoverageStartDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
						member.setBirthDate(memberDTO.getBirthDate() != null ? DateUtil.StringToDate( memberDTO.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
						member.setSsn(getFormattedSsnString(memberDTO.getSsn(),true));
						member.setUpdatedBy(user.getId());
						//member.setCreatedOn(memberDTO.getCreatedOn() != null ? DateUtil.StringToDate( memberDTO.getCreatedOn(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
						//member.setUpdatedOn(memberDTO.getUpdatedOn() != null ? DateUtil.StringToDate( memberDTO.getUpdatedOn(), GhixConstants.REQUIRED_DATE_FORMAT) : null);
						member.setEnrollment1095(enrollment1095);
					}
				}
			}
		}
		if( premiumDtoList != null)
		{
			for(EnrollmentPremium1095DTO premiumDto : premiumDtoList)
			{
				if( premiumDto.getId() == null)
				{
					EnrollmentPremium1095 premium = new EnrollmentPremium1095();
					premium.setMonthName(premiumDto.getMonthName());
					premium.setMonthNumber(premiumDto.getMonthNumber());
					premium.setAptcAmount(premiumDto.getAptcAmount());
					premium.setGrossPremium(premiumDto.getGrossPremium());
					premium.setSlcspAmount(premiumDto.getSlcspAmount());
					premium.setEhbPercent(premiumDto.getEhbPercent());
					premium.setEnrollment1095(enrollment1095);
					premium.setCreatedBy(user.getId());
					premium.setUpdatedBy(user.getId());
					setPremiumStatus(startMonth, endMonth, premium);
					newPremiumList.add(premium);
				}
				else
				{
					for(EnrollmentPremium1095 premium : premiumList)
					{
						if( premium.getId() == premiumDto.getId() && premium.getMonthNumber().equals(premiumDto.getMonthNumber()))
						{
							premium.setAptcAmount(premiumDto.getAptcAmount());
							premium.setGrossPremium(premiumDto.getGrossPremium());
							premium.setSlcspAmount(premiumDto.getSlcspAmount());
							premium.setUpdatedBy(user.getId());
							setPremiumStatus(startMonth, endMonth, premium);
							premium.setEnrollment1095(enrollment1095);
						}	
					}
				}
			}
		}
		premiumList.addAll(newPremiumList);
		enrollment1095.setEnrollmentMembers(membersList);
		enrollment1095.setEnrollmentPremiums(premiumList);
	}
	
	private AccountUser getUser() {
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if(null == user){
				user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
			}
		} catch (InvalidUserException e) {
			LOGGER.debug("Error fetching logged in user", e);
		}
		return user;
	}
	
	
	private void setPremiumStatus(Integer startMonth , Integer endMonth , EnrollmentPremium1095 premium)
	{
		// make status of premium as active for all premiums within coverage period
		// and make status of premium as In-Active for all premiums outside coverage period
		Integer premiumMonthNumber = premium.getMonthNumber();
		if( startMonth <= premiumMonthNumber && premiumMonthNumber <= endMonth){
			premium.setIsActive(YES_FLAG);
		}
		else{
			premium.setIsActive(NO_FLAG);
		}
	}
	
	private Integer getMonthnumber(Date date)
	{
		Calendar cal = TSCalendar.getInstance();
	    cal.setTime(date);
	    return (cal.get(Calendar.MONTH)+1);// month index is from 0 to 11 , we need to add 1 in months in order to get months from 1to12 
	}
	
	private Float getDefaultEhb(List<EnrollmentPremium1095> premiumList)
	{
		Float defaultEhb = null;
		for(EnrollmentPremium1095 premium : premiumList)
		{
			if( YES_FLAG.equals(premium.getIsActive()))
			{
				defaultEhb = premium.getEhbPercent();
				break;
			}
		}
		return defaultEhb;
	}
}
