package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.MacReadI551DocumentID22Type;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for MacReadI551DocumentID22Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class MacReadI551DocumentID22Wrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MacReadI551DocumentID22Wrapper.class);
	
	private ObjectFactory factory;
	private MacReadI551DocumentID22Type macReadI551DocumentID22Type;
	
	private String alienNumber;
	private String visaNumber;
	private String passportNumber;
	private String countryOfIssuance;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String VISA_NUMBER_KEY = "VisaNumber";
	private static final String PASSPORT_NUMBER_KEY = "PassportNumber";
	private static final String COUNTRY_KEY = "CountryOfIssuance";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public MacReadI551DocumentID22Wrapper(){
		this.factory = new ObjectFactory();
		this.macReadI551DocumentID22Type = factory.createMacReadI551DocumentID22Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.macReadI551DocumentID22Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setVisaNumber(String visaNumber) throws HubServiceException{
		
		if(visaNumber == null){
			throw new HubServiceException("No Visa number provided");
		}
		
		this.visaNumber = visaNumber;
		this.macReadI551DocumentID22Type.setVisaNumber(visaNumber);
	}
	
	public String getVisaNumber() {
		return visaNumber;
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.macReadI551DocumentID22Type.setPassportNumber(passportNumber);
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.macReadI551DocumentID22Type.setCountryOfIssuance(countryOfIssuance);
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public void setExpiryDate(String expiryDate) {
		
		try{
			this.macReadI551DocumentID22Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting Mac Read I551 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	
	public MacReadI551DocumentID22Type getMacReadI551DocumentID22Type(){
		return this.macReadI551DocumentID22Type;
	}
	
	/**
	 * Converts the JSON String to MacReadI551DocumentID22Wrapper object
	 * 
	 * @param jsonString - String representation for MacReadI551DocumentID22Wrapper
	 * @return MacReadI551DocumentID22Wrapper Object
	 */
	public static MacReadI551DocumentID22Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create MacReadI551DocumentID22Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		MacReadI551DocumentID22Wrapper wrapper = new MacReadI551DocumentID22Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.macReadI551DocumentID22Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(VISA_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.visaNumber = (String)tmpObj;
			wrapper.macReadI551DocumentID22Type.setVisaNumber(wrapper.visaNumber);
		}
		
		tmpObj = obj.get(PASSPORT_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.passportNumber = (String)tmpObj;
			wrapper.macReadI551DocumentID22Type.setPassportNumber(wrapper.passportNumber);
		}
		
		tmpObj = obj.get(COUNTRY_KEY);
		if(tmpObj != null){
			wrapper.countryOfIssuance = (String)tmpObj;
			wrapper.macReadI551DocumentID22Type.setCountryOfIssuance(wrapper.countryOfIssuance);
		}
	
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.macReadI551DocumentID22Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating MacReadI551DocumentID22Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static MacReadI551DocumentID22Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		MacReadI551DocumentID22Wrapper wrapper = new MacReadI551DocumentID22Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setVisaNumber((String) jsonObj.get(VISA_NUMBER_KEY));
		wrapper.setPassportNumber((String) jsonObj.get(PASSPORT_NUMBER_KEY));
		wrapper.setCountryOfIssuance((String) jsonObj.get(COUNTRY_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
