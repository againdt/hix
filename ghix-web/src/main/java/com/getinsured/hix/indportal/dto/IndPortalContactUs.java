package com.getinsured.hix.indportal.dto;

import java.util.List;

/**
 * @author Sunil Desu
 * 
 */
public class IndPortalContactUs {

	private String typeOfRequest;
	private String reason;
	private String requestDescription;
	private List<String> fileList;

	public String getTypeOfRequest() {
		return typeOfRequest;
	}

	public void setTypeOfRequest(String typeOfRequest) {
		this.typeOfRequest = typeOfRequest;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRequestDescription() {
		return requestDescription;
	}

	public void setRequestDescription(String requestDescription) {
		this.requestDescription = requestDescription;
	}

	public List<String> getFileList() {
		return fileList;
	}

	public void setFileList(List<String> fileList) {
		this.fileList = fileList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndPortalContactUs [typeOfRequest=");
		builder.append(typeOfRequest);
		builder.append(", reason=");
		builder.append(reason);
		builder.append(", requestDescription=");
		builder.append(requestDescription);
		builder.append(", fileList=");
		builder.append(fileList);
		builder.append("]");
		return builder.toString();
	}

}
