package com.getinsured.eligibility.indportal;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.indportal.dto.AgeAptcRatio;
import com.getinsured.hix.indportal.dto.AptcRatioRequest;
import com.getinsured.hix.indportal.dto.AptcRatioResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.timeshift.TSCalendar;

@Component
public class CustomGroupingUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomGroupingUtil.class);
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	public static List<String> ACTIVE_ENROLLMENT_STATUS = Arrays.asList("CONFIRM", "PENDING", "TERM");
	@Autowired private ApplicationContext appContext;
	
	public Object getAptcDistribution(AptcRatioRequest aptcRatioRequest) {
		AptcRatioResponse aptcRatioResponse = new AptcRatioResponse();
		if(aptcRatioRequest != null) {
			try {
				String message = validateAptcRatioRequest(aptcRatioRequest);
				if("SUCCESS".equalsIgnoreCase(message)) {
					Map<String,Float> memberAgeAptcRatio = new HashMap<String,Float>();
					BigDecimal sumOfAllMemberRatio = BigDecimal.ZERO;					
					Resource resource = appContext.getResource("classpath:aptcRatio/"+stateCode+"/age-aptc-ratio-"+aptcRatioRequest.getCoverageYear()+".xml");
					if(!resource.exists()) 
					{
						resource = appContext.getResource("classpath:aptcRatio/age-aptc-ratio-"+aptcRatioRequest.getCoverageYear()+".xml");
						if(!resource.exists()) {
							resource = appContext.getResource("classpath:aptcRatio/age-aptc-ratio-default.xml");
						}
					}
					
					if(resource.exists()) {
						InputStream inputStreamUrl = resource.getInputStream();
						JAXBContext context = JAXBContext.newInstance(AgeAptcRatio.class);
				        Unmarshaller unMarshaller = context.createUnmarshaller();
				        AgeAptcRatio ageAptcRatio = AgeAptcRatio.class.cast(unMarshaller.unmarshal(inputStreamUrl));
				        for(AptcRatioRequest.Member member : aptcRatioRequest.getMemberList()) {
				        		for(AgeAptcRatio.Ratio ratio : ageAptcRatio.getRatio()) {
				        			if(ratio.getMinAge() <= member.getAge() && ratio.getMaxAge() >= member.getAge()) {
				        				memberAgeAptcRatio.put(member.getId(), ratio.getPremium());
				        				sumOfAllMemberRatio = sumOfAllMemberRatio.add(new BigDecimal(Float.toString(ratio.getPremium())));
				        				break;
				        			}
				        		}
				        }
					}
					
					if(sumOfAllMemberRatio.compareTo(BigDecimal.ZERO) < 0) {
						return "Invalid Aptc Ratio";
					}
					
					List<AptcRatioResponse.Member> memberList= new ArrayList<AptcRatioResponse.Member>();
					BigDecimal sumOfAptc = BigDecimal.ZERO;
					Integer memberCounter = 0;
					for(AptcRatioRequest.Member member : aptcRatioRequest.getMemberList()) {
						memberCounter++;
						Float aptcRatio = memberAgeAptcRatio.get(member.getId());
						BigDecimal memberAptc = null;
						if(memberCounter < aptcRatioRequest.getMemberList().size()) {
							memberAptc = aptcRatio != null ? aptcRatioRequest.getMaxAptc().multiply(BigDecimal.valueOf(aptcRatio/sumOfAllMemberRatio.floatValue())) : BigDecimal.ZERO;
						}else {
							memberAptc = aptcRatioRequest.getMaxAptc().subtract(sumOfAptc);
						}
						memberAptc = memberAptc.setScale(2, BigDecimal.ROUND_HALF_UP);
						sumOfAptc = sumOfAptc.add(memberAptc);
						
						AptcRatioResponse.Member responseMember = aptcRatioResponse.new Member();
						responseMember.setId(member.getId());
						responseMember.setAptc(memberAptc);
						
						memberList.add(responseMember);
					}
					
					aptcRatioResponse.setMemberList(memberList);
					
				}else {
					return message;
				}
				
			}catch(Exception e){
				return e;
			}
		}
		return aptcRatioResponse;
	}
	
	private String validateAptcRatioRequest(AptcRatioRequest aptcRatioRequest) {
		if(aptcRatioRequest.getCoverageYear() == null) {
			return "Invalid Coverage Year";
		}
		if(aptcRatioRequest.getMaxAptc() == null) {
			return "Invalid Max Aptc";
		}
		List<String> memberIds = new ArrayList<String>();
		for(AptcRatioRequest.Member member : aptcRatioRequest.getMemberList()) {
			if(StringUtils.isBlank(member.getId())) {
				return "Invalid Member Id";
			} 
			
			if(memberIds.contains(member.getId())) {
				return "Duplicate Member Id : "+member.getId();
			}else {
				memberIds.add(member.getId());
			}
			
			if(member.getAge() == null) {
				return "Invalid Member Age";
			} else if(member.getAge() < 0 || member.getAge() > 200) {
				return "Invalid Member Age";
			}
		}
		return "SUCCESS";
	}
	
	public int getAgeFromCoverageDate(Date coverageStartDate, Date dateOfBirth) {
		Calendar today = TSCalendar.getInstance();
		today.setTime(coverageStartDate);
		Calendar dob = TSCalendar.getInstance();
		dob.setTime(dateOfBirth);
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			age--;
		}
	
		return age;
	}
	
	public boolean isEnrollmentActive(EnrollmentMemberDataDTO enrollmentMemberDataDTO){
		boolean isActive = false;		
		if(ACTIVE_ENROLLMENT_STATUS.contains(enrollmentMemberDataDTO.getEnrollmentStatus())){
			isActive = true;
		}		
		return isActive;		
	}
	
	public boolean isEnrolleeActive(EnrollmentMemberDataDTO enrollmentMemberDataDTO) {
		if("TERM".equalsIgnoreCase(enrollmentMemberDataDTO.getEnrolleeStatus()) 
		     && enrollmentMemberDataDTO.getMemberCoverageEndDate() != null && enrollmentMemberDataDTO.getBenefitEndDate() != null
			  && enrollmentMemberDataDTO.getMemberCoverageEndDate().before(enrollmentMemberDataDTO.getBenefitEndDate())) {
				return false;			
		}
		return true;	
	}

}
