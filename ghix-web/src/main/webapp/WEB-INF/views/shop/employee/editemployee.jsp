<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
	#titlebar:after {
		border-bottom: none;
	}
	#frmeditemployee.form-horizontal .form-actions {
		padding-left: 0;
	}
</style>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ajaxfileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<div class="gutter10">
	<div class="topnav">
  		<ul class="inline">
    		<li class="active"><a href="#">Verify Information</a></li>
    		<li><a href="#">Update Dependents</a></li>
    		<li><a href="#">Select Plans</a></li>
    		<li><a href="#" class="last">Enroll</a></li>
  		</ul>
	</div> 
  	<div class="row-fluid" id="titlebar">
          <h1><spring:message  code="label.editEmployeeInfo"/></h1>
    </div>
      <div class="row-fluid">
        <div class="span3" id="sidebar"></div>
        <!-- /.sidebar-->
        
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
              <p><spring:message  code="label.updateyourinfo"/></p>
              <form class="form-horizontal addressValidator" id="frmeditemployee" name="frmeditemployee" action="<c:url value="/shop/employee/editemployeesubmit" />" method="post">
              <df:csrfToken/>
              		
					<div class="addressBlock">
						<input type="hidden" name="contactNumber" id="contactNumber"/>
		                <input type="hidden" name="id" id="id" value="${empDetailsApplication.id}" />
		                <input type="hidden" name="location.id" id="location_id" value="${empDetailsApplication.location.id}" />
		                <input type="hidden" name="location.lat" id="lat" />
		                <input type="hidden" name="location.lon" id="lon" />
		                <input type="hidden" name="frtDocument" id="frtDocument"/>
						<input type="hidden" name="lat" id="address1lat" value="0.0"  />
						<input type="hidden" name="lon" id="address1lon" value="0.0"  />	
						<input type="hidden" name="rdi" id="rdi" value="" />
						<input type='hidden' name='applicationId' value='${employeeApplication.id}'/>
						
						<div class="control-group"> 
						    <label for="firstname" class="control-label"><spring:message  code="label.emplFirstName"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>                    
							<div class="controls">
								<input type="text" class="input-medium"  name="firstName" id="firstname" value="${empDetailsApplication.firstName}" size="20"/>
								<span id="firstname_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						    <label for="middlename" class="control-label"><spring:message  code="label.emplMiddleInitial"/></label>                    
							<div class="controls">
								<input type="text" class="input-mini" name="middleInitial" id="middlename" value="${empDetailsApplication.middleInitial}" maxlength="1"/>
								<span id="middlename_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="lastname" class="control-label"><spring:message  code="label.emplLastName"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>                 
							<div class="controls">
								<input type="text" class="input-medium" name="lastName" id="lastname" value="${empDetailsApplication.lastName}" size="20"/>
								<span id="lastname_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="suffix" class="control-label"><spring:message  code="label.suffix"/></label>                 
							<div class="controls">
								<select class="input-medium" name="suffix" id="suffix">
									<option value=""></option>
									<c:forEach var="suffix" items="${suffixlist}">
										<option id="${suffix}" <c:if test="${suffix == empDetailsApplication.suffix}"> SELECTED </c:if>
										value="${suffix}">${suffix}</option>
									</c:forEach>
								</select>
								 <span id="suffix_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="taxid" class="control-label"><spring:message  code="label.taxId"/></label>                 
							<div class="controls">
                      			<c:if test="${empDetailsApplication.ssn != null && empDetailsApplication.ssn != ''}">
									<c:set var="ssnParts" value="${fn:split(empDetailsApplication.ssn,'-')}" />
									<strong>	***-**-${ssnParts[2]}</strong>
								</c:if>
							</div>
						</div>
						
					  <div class="control-group"> 
						   <label for="dob" class="control-label"><spring:message  code="label.dob"/> 
						   <c:if test="${isInitialEnrollment==true}">
						   		<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
						   	</c:if>	
						   	</label>                 
							<div class="controls">
							<c:if test="${isInitialEnrollment==true}">
								<fmt:formatDate pattern="MM/dd/yyyy" value="${empDetailsApplication.dob}" var="dob" />
								<div class="input-append date date-picker" data-date="${dob}" data-date-format="mm/dd/yyyy">
									<input class="span10" type="text" name="dob" value="${dob}" id="dob"> 
									<span class="add-on"><i	class="icon-calendar"></i></span>
								</div>
								<span id="dob_error"></span>
							</c:if>
							
                      		<c:if test="${isInitialEnrollment==false}">
                      			<fmt:formatDate pattern="MM/dd/yyyy" value="${empDetailsApplication.dob}" var="dob" />
                      			<strong> ${dob} </strong>
                      		</c:if>	
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="email" class="control-label"><spring:message  code="label.emplEmail"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>             
							<div class="controls">
                      			 <input type="text" name="email" value="${empDetailsApplication.email}" id="email">
                      			 <span id="email_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label  for="address1" class="control-label"><spring:message  code="label.homeAddress1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>             
							<div class="controls">
                      			 <input type="text" name="location.address1" id="address1" value="${empDetailsApplication.location.address1}">
		                         <span id="address1_error"></span>
		                         <input type="hidden" id="address1_hidden" name="address1_hidden" >
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="address2" class="control-label"><spring:message  code="label.homeAddress2"/></label>             
							<div class="controls">
                      			 <input type="text" name="location.address2" id="address2" value="${empDetailsApplication.location.address2}">
		                      	 <span id="address2_error"></span>
		                      	 <input type="hidden" id="address2_hidden" name="address2_hidden" >
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="city" class="control-label"><spring:message  code='label.emplCity'/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>            
							<div class="controls">
                      			 	<input type="text" value="${empDetailsApplication.location.city}" name="location.city" id="city">
			                        <span id="city_error"></span>
			                        <input type="hidden" id="city_hidden" name="city_hidden" >
							</div>
						</div>
						
						<div class="control-group"> 
						  <label for="state" class="control-label"><spring:message  code='label.emplState'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>            
							<div class="controls">
                      			 <select class="input-medium" name="location.state" id="state">
									<option value="">Select</option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == empDetailsApplication.location.state}"> SELECTED </c:if>
										value="${state.code}">${state.name}</option>
									</c:forEach>
								</select>
			                    <span id="state_error"></span>
			                    <input type="hidden" id="state_hidden" name="state_hidden" >
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="zip" class="control-label"><spring:message  code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>            
							<div class="controls">
                      			 	<input type="text" class="input-small zipCode" name="location.zip" value="${empDetailsApplication.location.zip}" id="zip" maxlength="5"> 
                      	      		<span id="zip_error"></span> <br>
                      	     		<input type="hidden" value="0.0" id="zip_hidden" name="zip_hidden" >
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="county" class="control-label"><spring:message  code='label.county'/> <img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/></label>            
							<div class="controls">
                      			<select class="input-large" name="location.county"  id="county" >
		                      		<option value="">Select County...</option>
		                      	</select>
                      			<span id="county_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="homephone" class="control-label"><spring:message  code="label.homephone"/><img  src="<c:url value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/></label>            
							<c:choose>
								<c:when
									test="${empDetailsApplication.contactNumber != null && empDetailsApplication.contactNumber != ''}">
									<c:set var="phone" value="${empDetailsApplication.contactNumber}" />
									<c:set var="phoneParts" value="${fn:split(phone,'-')}" />
	
									<c:set var="phone1" value="${phoneParts[0]}" />
									<c:set var="phone2" value="${phoneParts[1]}" />
									<c:set var="phone3" value="${phoneParts[2]}" />
								</c:when>
							</c:choose>
							<div class="controls">
                      			<label for="phone1" class="aria-hidden" ><spring:message  code="label.homephone"/> <spring:message  code='label.firstPhone'/></label>
		                      	<input type="text" aria-required="true" class="span2 inline" value="${phone1}" name="phone1" id="phone1" maxlength="3" >
		                      	<label for="phone2" class="aria-hidden"><spring:message  code="label.homephone"/> <spring:message  code='label.secondPhone'/></label>
		                        <input type="text" aria-required="true" class="span2 inline" value="${phone2}" name="phone2" id="phone2" maxlength="3" >
		                        <label for="phone3" class="aria-hidden"><spring:message  code="label.homephone"/> <spring:message  code='label.thirdPhone'/></label>
		                        <input type="text" aria-required="true" class="span2 inline" value="${phone3}" name="phone3" id="phone3" maxlength="4">
		                        <span id="phone3_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						    <label for="gender" class="control-label"><spring:message  code="label.gender"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>           
							<div class="controls">
                      			<label class="radio inline"><input aria-required="true" type="radio" id="genderMale" name="gender" ${fn:contains(empDetailsApplication.gender, 'MALE') ? 'checked="checked"' : ''} value="MALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.male"/> </label>
			                    <label class="radio inline"><input aria-required="true" type="radio" id="genderFemale" name="gender" ${fn:contains(empDetailsApplication.gender, 'FEMALE') ? 'checked="checked"' : ''}  value="FEMALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.female"/> </label>
			                    <span id="genderMale_error"></span> 
			                    <span id="genderFemale_error"></span><br>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="maritalStatus" class="control-label"><spring:message  code="label.martialstatus"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>            
							<div class="controls">
                      			 <select class="input-xlarge" name="maritalStatus" id="maritalStatus">
									<option value="">Select</option>
									<c:forEach var="martialstatus" items="${martialstatuslist}">
										<option id="${martialstatus.code}" <c:if test="${martialstatus.code == empDetailsApplication.maritalStatus}"> SELECTED </c:if>
										value="${martialstatus.code}">${martialstatus.name}</option>
									</c:forEach>
								</select>
			                    <span id="maritalStatus_error"></span>
							</div>
						</div>
						
						<div class="control-group"> 
						   <label for="tobaccouser" class="control-label"><spring:message  code="label.tobacoUser"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
						   	 <a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.tobaccousertooltip'/>" 
								class="info"><i class="icon-question-sign"></i>
							 </a>
						   </label>            
							<div class="controls">
                      				<label class="radio inline"><input aria-required="true" type="radio" id="smokerYes" name="smoker" ${fn:contains(empDetailsApplication.smoker, 'YES') ? 'checked="checked"' : ''} value="YES"><span class="aria-hidden"><spring:message  code="label.tobacoUser"/></span> <spring:message  code="label.yes"/> </label>
			                        <label class="radio inline"><input aria-required="true" type="radio" id="smokerNo" name="smoker" ${fn:contains(empDetailsApplication.smoker, 'NO') ? 'checked="checked"' : ''}  value="NO"><span class="aria-hidden"><spring:message  code="label.tobacoUser"/></span> <spring:message  code="label.no"/> </label>
			                        <span id="smokerYes_error"></span>
			                        <span id="smokerNo_error"></span> 
							</div>
						</div>
						
						<div class="control-group"> 
						   <div id = "showTcpLblDiv" style="display:none;" class="control-label"><spring:message code="label.tobaccoCessationPrg"/></div>           
						   <div class="controls" id = "showTcpRowDiv" style="display:none;">
                      			<input aria-required="true" type="radio" id="tobaccoCessPrgYes" name="tobaccoCessPrg" ${fn:contains(empDetailsApplication.tobaccoCessPrg, 'YES') ? 'checked="checked"' : ''} value="YES"><span class="aria-hidden"><spring:message  code="label.tobaccoCessationPrg"/></span> <spring:message  code="label.yes"/> 
		                        <input aria-required="true" type="radio" id="tobaccoCessPrgNo" name="tobaccoCessPrg" ${fn:contains(empDetailsApplication.tobaccoCessPrg, 'NO') ? 'checked="checked"' : ''}  value="NO"><span class="aria-hidden"><spring:message  code="label.tobaccoCessationPrg"/></span> <spring:message  code="label.no"/>
		                        <span id="tobaccoCessPrgYes_error"></span>
		                        <span id="tobaccoCessPrgNo_error"></span> <br>
							</div>
						</div>
						
					   <div class="control-group"> 
						   <label for="nativeAmerican" class="control-label"><spring:message  code="label.nativeAmerican"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>            
							<div class="controls">
                      			<label class="radio inline"><input aria-required="true" type="radio" id="nativeAmrYes" name="nativeAmr" ${fn:contains(empDetailsApplication.nativeAmr, 'YES') ? 'checked="checked"' : ''} value="YES"><span class="aria-hidden"><spring:message  code="label.nativeAmerican"/></span> <spring:message  code="label.yes"/> </label>
		                        <label class="radio inline"><input aria-required="true" type="radio" id="nativeAmrNo" name="nativeAmr" ${fn:contains(empDetailsApplication.nativeAmr, 'NO') ? 'checked="checked"' : ''} value="NO"><span class="aria-hidden"><spring:message  code="label.nativeAmerican"/></span> <spring:message  code="label.no"/> </label>
		                        <span id="nativeAmrYes_error"></span>
		                        <span id="nativeAmrNo_error"></span> <br>
							</div>
						</div>
						
						
					   <div class="control-group" style="display: none;"> 
						   <label for="worksite" class="control-label"><spring:message  code="label.worksite"/></label>            
							<div class="controls">
                      			<strong>
				                 ${employee.employerLocation.location.address1} <br>
				                 ${employee.employerLocation.location.city} <br> 
				                 ${employee.employerLocation.location.state}  <br>
				                 ${employee.employerLocation.location.zip}<br>
				                 ${fn:toUpperCase(fn:substring(employee.employerLocation.location.county, 0, 1))}${fn:toLowerCase(fn:substring(employee.employerLocation.location.county, 1, -1))}
				                 </strong>
							</div>
						</div>
						
						<div class="control-group"> 
						  <h4 id="file_upload_container_msg" style="display:none;"><spring:message  code="label.uploadsupportingdocument"/></h4>
					  <%-- <img id = "imgDiv" style="display:none;" src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%>           
							<div id="file_upload_container_box" style="display:none;">
                   			   <div id="file_upload_container">
									<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;	
								</div>
						 
								<div id="file_upload_error" class="error" style="display:none;">File could not be uploaded. Selected file name already exists on the server.</div>
								<div id="uploaddiv" style="display:none;">Uploading..... Please wait</div>
		       					<div id="deletediv" style="display:none;">Deleting..... Please wait</div>
		       					<span id="frtDocument_error" style="display:none;"></span>		
				                <!-- The table listing the files available for attaching/removing -->
     							<div id="" class="files" >
     								<table  id="upload_files_container"></table>
     							</div>
							</div>
						</div>
						
					</div>
    
               <div class="form-actions margin-top50">
                		<a href="<c:url value='/shop/employee/home/${employeeApplication.id}'/>" class="btn pull-left"> <spring:message  code="label.newhireback"/> </a>
                		<input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" class="btn btn-primary pull-right" value="<spring:message  code="label.next"/>" />
               </div>
             
              </form>
              
	
          </div>
        <!--gutter10--> 
      </div>
      <!--row-fluid--> 
    </div>
    <!--rightpanel--> 
  </div>
  <!--row-fluid--> 

<jsp:include page="waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>

<script type="text/javascript">

var uploadedDocId;
var input = $('#fileupload');
var fileDetails;
var rowno=0;
var responseText="";

function setAjaxPlugin(){
	$('#fileupload').ajaxfileupload({
		
	    'action': "<c:url value='/shop/employee/uploadFRTDocument'/>",
	    'onComplete': function(response) {
	    	responseText=response;
	    	 if(response.status == false){       
	    	      alert(response.message);
    	      }
	    	 else if(responseText.indexOf("Error") != -1){
	    		 errorDetails=responseText.split('#');
	    		 alert(errorDetails[1]);
	    	 }
	    	 else if(responseText){
	    		fileDetails=responseText.split('#');
	    		if(fileDetails.length == 2){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	$('#frtDocument').val(uploadedDocId);
	            	rowno++;
	            	appendFileDetails();	
	            	$( "#fileupload" ).hide();
	            	$("#frtDocument_error").hide();
	            	$("#file_upload_error").hide();
	        	}
	        	else{
	        		$('#file_upload_error').show();
	        	}
	    	}
	    	else{
	    		$('#file_upload_error').show();
	    	}
	    	
	    	$('#uploaddiv').hide();
	    },
	    'onStart': function() {
	    	$('#uploaddiv').show();
	    	$('#file_upload_error').hide();
	    },
	    'onCancel': function() {
	      
	    },
	    'valid_extensions' : ['gif','png','jpg','jpeg','pdf']
	  });
}

	function showFrtDocument(){
		var frtdocumentdetails = '${frtdocumentdetails}';
		
		fileDetails=frtdocumentdetails.split('#');
		if(fileDetails.length == 2){
    		uploadedDocId=fileDetails[0];
        	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
        	$('#frtDocument').val(uploadedDocId);
        	rowno++;
        	appendFileDetails();		
        	$( "#fileupload" ).hide();
    	}
    	 
	}


	function deleteFile(fileId,rowid){
		$('#file_upload_error').hide();
		$('#deletediv').show();
		var subUrl = '<c:url value="/shop/employee/removeFRTDocument">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {uploadedDocId : fileId}, 
			dataType:'text',
			success : function(response,xhr) {
				if(isInvalidCSRFToken(xhr))	    				
					return;
				var newText = '<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;';
				$('#file_upload_container').html('');
				$('#file_upload_container').html(newText);
				 //$('#fileupload').replaceWith($('#fileupload').clone(true).val(''));
				 setAjaxPlugin();
				$('#frtDocument').val(null);
				$('#deletediv').hide();
				$('#upload_files_container').html("");
			},error : function(e) {
				alert('error in deleting document.');
				$('#deletediv').hide();
			}
		});
	}
	
	function appendFileDetails(){
		orgHtml=$('#upload_files_container').html();
		//strHtml=orgHtml+'<tr id="' +rowno  +'">'+
		strHtml='<tr id="' +rowno  +'">'+
		'<td colspan="1">'+fileDetails[1]+'</td>'+
		'<td colspan="1"></td>'+
		'<td colspan="1"><a class="btn" href="javascript:downloadFile(\''+uploadedDocId+'\')">download</a></td>'+
		'<td colspan="1"><a class="btn btn-danger" href="javascript:deleteFile(\''+uploadedDocId+'\','+rowno+')">delete</a></td>'
		+'</tr>';
		$('#upload_files_container').html(strHtml);
		 
	}
	
	function downloadFile(fileId){
		var  contextPath =  "<%=request.getContextPath()%>";

		var url = contextPath+"/shop/employee/downloadDocument?udId="+fileId;
		var subUrl =  '<c:url value="'+url+'"/>';
		window.open(subUrl,"Supported Document");
	}
	
	function shiftbox(element, nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if (element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}
	
	jQuery.validator.addMethod("phonecheck",function(value, element, param) {
		phone1 = $("#phone1").val();
		phone2 = $("#phone2").val();
		phone3 = $("#phone3").val();
	  	var intPhone1= parseInt( phone1);
	  	if( 
	  			(phone1 == "" || phone2 == "" || phone3 == "")  || 
	  			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
	  			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
	  		return false; 
	  	}else if(intPhone1<100){
	  		return false;
	  	} 
	  	
		var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
			
		var phoneno = /^\d{10}$/;
		var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
		
		$("#contactnumber").val(phone);
		
		  if(phonenumber.match(phoneno))      {			  
	        return true;
	      }
		return false;		
	});
	
	jQuery.validator.addMethod("zipcheck", function(value, element, param) {
	  	elementId = element.id; 
		if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) ){
	  		return false;	
	  	}return true;
	});
	jQuery.validator.addMethod("address1Check", function(value, element, param) {
		var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
	  	elementId = element.id; 
		if(!regex.test($("#"+elementId).val()) ){
	  		return false;	
	  	}
		return true;
	});
	jQuery.validator.addMethod("address2Check", function(value, element, param) {
		var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
	  	elementId = element.id; 
	  	if($("#"+elementId).val()!=""){
			if(!regex.test($("#"+elementId).val()) ){
		  		return false;	
		  	}
		}
		return true;
	});
	jQuery.validator.addMethod("address2Match", function(value, element, param) {
	  	elementId = element.id;	  	
	  	if($.trim($("#"+elementId).val())!="" && $.trim($("#address1").val())!=""){	  		  	
	  		if($("#"+elementId).val() == $("#address1").val()){
		  		return false;	
			}
	  	}
		return true;
	});	
	
	jQuery.validator.addMethod("namecheck", function(value, element, param) {
	  	if(namecheck(value) == false){
		  	return false;	
		}
		return true;
	});
	
	jQuery.validator.addMethod("frtdoccheck", function(value, element, param) {
		$('#frtDocument_error').html("");
		var fileName = $('#frtDocument').val();
		var frtDocumentRequired = '${frtDocumentRequired}';
		if($('#nativeAmrYes').is(':checked') && frtDocumentRequired == 'YES' && (fileName === undefined || fileName  == '' ) ) {
			$('#frtDocument_error').show();
			return false; 
		}
		return true;
	});
	//"[0-9a-zA-Z .'-]+"

	/* function validateHomeAddress() {
		var enteredAddress = $("#address1").val() + "," + $("#address2").val() + "," + $("#city").val() + 
							 "," + $("#state").val() + "," + $("#zip").val() + ", " +  $("#lat").val() + "," + $("#lon").val();
		var idsText = 'address1~address2~city~state~zip~lat~lon';
		$.ajax({
					url : "<c:url value='/platform/validateaddress'/>",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = "<c:url value='/platform/address/viewvalidaddress'/>";
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame" tabindex="-1" role="dialog"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click",
									submitFrmEditEmployee);
						} else {
							submitFrmEditEmployee();
						}
					}
				});
	} */
	
	function submitFrmEditEmployee() {
		$("#frmeditemployee").submit();
	}
	
	function validateForm() {
		if ($("#frmeditemployee").validate().form()) {
			var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
			$("#contactNumber").val(phone);
			//validateHomeAddress();
			submitFrmEditEmployee();
		}
	}
	
	jQuery.validator.addMethod("dobcheck", function(value, element, param) {
		var today=new Date();
		var dob = $("#dob").val();
	    dob_arr = dob.split(/\//);
		dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
		var birthDate=new Date();
		birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
		if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
		if(today.getTime() < birthDate.getTime()){ return false; }
		return true;
	});
	
	function postPopulateIndex(zipId, zipCodeValue){
		//console.log(zipCodeValue, zipId);
		//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
		jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
		getCountyList( zipCodeValue, '' );
	}

	function getCountyList(zip, county) {
		var subUrl = '<c:url value="/shop/employee/addCounties">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
				if(isInvalidCSRFToken(xhr))	    				
					return;
				populateCounties(response, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, county) {
        var optionsstring = '<option value="">Select County...</option>';
			var i =0;
			$.each(response, function(key, value) {
				var selected = (county == key) ? 'selected' : '';
				var optionVal = key+'#'+value;
				var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
				optionsstring = optionsstring + options;
				i++;
			});
			$('#county').html(optionsstring);
	} // end populateCounties
		
$(document).ready(function(){
	$('.info').tooltip();
	setAjaxPlugin();
	showFrtDocument();
	showTobaccoCessPrg();
	var zipval = '${empDetailsApplication.location.zip}';
	if(zipval != ''){
		var zip = '${empDetailsApplication.location.zip}';
		var county = '${empDetailsApplication.location.county}';
		getCountyList(zip, county);
	}
	
	var frtDocumentRequired = '${frtDocumentRequired}';
	$("#nativeAmrNo").click(function(){
		  $("#file_upload_container_msg").hide(); 
		  $("#file_upload_container_box").hide();
		 /*  if(frtDocumentRequired != 'YES'){
			$("#imgDiv").hide();
		  }  */ 
	});
	$("#nativeAmrYes").click(function(){
	  $("#file_upload_container_msg").show(); 
	  $("#file_upload_container_box").show();
	  /* if(frtDocumentRequired == 'YES'){
		$("#imgDiv").show();
	  } */
	});	
	
	var isfrt = '${empDetailsApplication.nativeAmr}';  
	if(isfrt == 'YES')
		show_file_upload_container(frtDocumentRequired);
});

	function show_file_upload_container(frtDocumentRequired){
		  $("#file_upload_container_msg").show(); 
		  $("#file_upload_container_box").show();		
		 /*  if(frtDocumentRequired == 'YES'){
			  $("#imgDiv").show();
		  } else{
			  $("#imgDiv").hide();
		  } */
	}
	
var validator = $("#frmeditemployee")
.validate(
		{
			ignore: ':hidden:not("#frtDocument")',
			onkeyup : false,
			onclick : false,
			onfocusout : false,
			onSubmit : true,
			rules : {
				dob : {required : true, date : true, dobcheck : true},
				email : { required : true, email : true },
				phone3 : { phonecheck : true },
				'firstName' : {required : true , namecheck : true },
				'middleInitial' : { namecheck : true },
				'lastName' : {required : true , namecheck : true },
				'location.address1' : { required : true , address1Check:true},
				'location.address2' :{address2Check:true, address2Match:true},
				'location.city' : { required : true },
				'location.state' : { required : true },
				'location.zip' : { required : true, zipcheck : true },
				'location.county' : { required : true },				
				gender : {required: true},
				nativeAmr : {required:true},
				smoker : {required:true},
				maritalStatus : {required:true},
				frtDocument : {frtdoccheck:true}
			},
			messages : {
				dob : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>",
					date : "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
					dobcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>"
				},
				email : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>",
					email : "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"
				},
				phone3 : {
					phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
				},
				'firstName' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>",
					namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>"
				},
				'middleInitial' : {
					namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatemiddlenamechar' javaScriptEscape='true'/></span>"
				},
				'lastName' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>",
					namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>"
				},
				'location.address1' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>",
					address1Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>"
				},
				'location.address2' :  {
					address2Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>",
					address2Match: "<span> <em class='excl'>!</em><spring:message code='label.validateaddressline12' javaScriptEscape='true'/></span>"
				},
				'location.city' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"
				},
				'location.state' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"
				},
				'location.zip' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
					zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
				},
				'location.county' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
				},
				gender : {
					required : "<span><em class='excl'>!</em><spring:message code='label.validategender' javaScriptEscape='true'/></span>"
				},
				nativeAmr : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatenativeamerican' javaScriptEscape='true'/></span>"
				},
				smoker : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatetobaccouser' javaScriptEscape='true'/></span>"
				},
				maritalStatus : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatemaritalstatus' javaScriptEscape='true'/></span>"
				},
				frtDocument :{
					frtdoccheck: "<span> <em class='excl'>!</em><spring:message code='label.validatefile' javaScriptEscape='true'/></span>"
				}
			},
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				error.appendTo($("#" + elementId + "_error"));
				$("#" + elementId + "_error").attr('class', 'error span10');
				$('#dob').unbind('focus');
			}
		});

/* $('#address1').focusin(function(e) {
	 if($('#zip').val().length >= 5){
	  getCountyList(0,$('#zip').val(), '');
	 }   
}); */

$(document).ready(function(){
	$('.date-picker').datepicker({
		autoclose:true,
		forceParse: false
	});
});

$('#smokerNo').click(function() {
	  $('input:radio[name=tobaccoCessPrg]').attr('checked',false);
	  
	   if($('#smokerNo').is(':checked')) {
		  $('#showTcpLblDiv').hide();
		  $('#showTcpRowDiv').hide();
	   }
});

$('#smokerYes').click(function() {
	if(!$('input:radio[name="tobaccoCessPrg"]').is(':checked')) {
    	  $('input:radio[name=tobaccoCessPrg]').attr('checked',false);
      }
	   if($('#smokerYes').is(':checked')) {
		  $('input:radio[name=tobaccoCessPrg]').attr('checked',false); 
		  $('#showTcpLblDiv').show();
		  $('#showTcpRowDiv').show();
	   }
});

function showTobaccoCessPrg(){
	var isSmoker = '${empDetailsApplication.smoker}';
	if(isSmoker.toLowerCase() == 'yes' ){
		$('#showTcpLblDiv').show();
		$('#showTcpRowDiv').show();
	} 
}


</script>

