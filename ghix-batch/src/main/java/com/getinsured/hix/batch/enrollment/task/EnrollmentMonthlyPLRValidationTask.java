package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPLRBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentPLROut;

/**
 * 
 * @since 26/03/2016 
 * Class to validate generated monthlyPlr XML against XSD
 *
 */
public class EnrollmentMonthlyPLRValidationTask implements Tasklet
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLRValidationTask.class);
	private EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService;
	private EnrollmentPLROut enrollmentPLROut;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("MonthlyPLRValidation Started.....");
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
		if(!enrollmentMonthlyPLRBatchService.validateGeneratedMonthlyPLRXML(jobId)){
			enrollmentPLROut.setGeneratedXMLsValid(Boolean.FALSE);
		}
			
		return RepeatStatus.FINISHED;
	}

	/**
	 * @return the enrollmentMonthlyPLRBatchService
	 */
	public EnrollmentMonthlyPLRBatchService getEnrollmentMonthlyPLRBatchService() {
		return enrollmentMonthlyPLRBatchService;
	}

	/**
	 * @param enrollmentMonthlyPLRBatchService the enrollmentMonthlyPLRBatchService to set
	 */
	public void setEnrollmentMonthlyPLRBatchService(EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService) {
		this.enrollmentMonthlyPLRBatchService = enrollmentMonthlyPLRBatchService;
	}
	
	public EnrollmentPLROut getEnrollmentPLROut() {
		return enrollmentPLROut;
	}
	public void setEnrollmentPLROut(EnrollmentPLROut enrollmentPLROut) {
		this.enrollmentPLROut = enrollmentPLROut;
	}
}
