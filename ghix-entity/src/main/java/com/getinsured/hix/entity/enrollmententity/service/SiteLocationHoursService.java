package com.getinsured.hix.entity.enrollmententity.service;

/**
 * Encapsulates service layer method call for Enrollment Entity SubSite SiteLocationHours
 */
import java.util.List;

import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLocationHours;

public interface SiteLocationHoursService {
	/**
	 * Retrieves SiteLocationHours based on id
	 * 
	 * @param siteLocationHoursId
	 *         
	 */
	SiteLocationHours findBySiteLocationHoursId(long siteLocationHoursId);
	/**
	 * Retrieves List of SiteLocationHours based on site
	 * 
	 * @param site
	 *         
	 */
	List<SiteLocationHours> findSiteLocationHoursBySite(Site site);

	/**
	 * Saves siteLocationHours for a given site
	 * @param site
	 * @param siteLocationHours
	 */
	void saveSiteLocationHours(Site site, List<SiteLocationHours> siteLocationHours);
}
