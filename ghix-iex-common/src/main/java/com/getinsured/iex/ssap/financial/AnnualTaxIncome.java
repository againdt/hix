package com.getinsured.iex.ssap.financial;

/**
 * Describes user's annual income.
 *
 * @author Yevgen Golubenko
 * @since 5/6/19
 */
public class AnnualTaxIncome {
  private long reportedIncome;
  private long projectedIncome;

  /**
   * Returns income that is reported by the user.
   * @return reported user income.
   */
  public long getReportedIncome() {
    return reportedIncome;
  }

  /**
   * Sets user's reported income.
   * @param reportedIncome reported income to set.
   */
  public void setReportedIncome(long reportedIncome) {
    this.reportedIncome = reportedIncome;
  }

  /**
   * Returns user's projected income.
   * @return returns user projected income.
   */
  public long getProjectedIncome() {
    return projectedIncome;
  }

  /**
   * Sets user's projected income.
   * @param projectedIncome projected users income to set.
   */
  public void setProjectedIncome(long projectedIncome) {
    this.projectedIncome = projectedIncome;
  }
}
