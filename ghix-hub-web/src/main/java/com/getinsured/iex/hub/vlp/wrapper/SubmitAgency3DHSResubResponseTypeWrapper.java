package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.SubmitAgency3DHSResubResponseType;

/**
 * Wrapper Class for SubmitAgency3DHSResubResponseType
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 */

public class SubmitAgency3DHSResubResponseTypeWrapper implements JSONAware{

	private ResponseMetadataWrapper responseMetadata;
	private SubmitAgency3DHSResubResponseSetTypeWrapper submitAgency3DHSResubResponseSetTypeWrapper;
	
	public SubmitAgency3DHSResubResponseTypeWrapper(SubmitAgency3DHSResubResponseType response){
		
		if (response == null){
    		return;
    	}
		
		this.responseMetadata = new ResponseMetadataWrapper(response.getResponseMetadata());
		this.submitAgency3DHSResubResponseSetTypeWrapper = new SubmitAgency3DHSResubResponseSetTypeWrapper(response.getSubmitAgency3DHSResubResponseSet());
	}
	
	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public SubmitAgency3DHSResubResponseSetTypeWrapper getAgency3DHSResubResponse() {
		return submitAgency3DHSResubResponseSetTypeWrapper;
	}

	public void setAgency3DHSResubResponse(
			SubmitAgency3DHSResubResponseSetTypeWrapper agency3DHSResubResponseWrapper) {
		this.submitAgency3DHSResubResponseSetTypeWrapper = agency3DHSResubResponseWrapper;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("SubmitAgency3DHSResubResponseSetType",this.submitAgency3DHSResubResponseSetTypeWrapper);
		return obj.toJSONString();
	}
}
