package com.getinsured.iex.hub.rest.controller;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.iex.hub.platform.MessageProcessor;


@Controller
public class IFSVController extends MessageProcessor {

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/invokeIFSV", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeIFSV(@RequestBody String ifsvRequestJSON){
		JSONObject obj = new JSONObject();
		obj.put("message", "IFSV is not appplicable for this build");
		return new ResponseEntity<String>(obj.toJSONString(), null, HttpStatus.METHOD_NOT_ALLOWED);
	}
}
