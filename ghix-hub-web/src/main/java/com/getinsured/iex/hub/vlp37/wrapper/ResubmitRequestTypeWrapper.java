package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ResubmitRequestType;
import com.getinsured.iex.hub.vlp37.wrapper.util.ResubmitRequestTypeWrapperUtil;


public class ResubmitRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	
	private ResubmitRequestType request = null;
	
	    private String caseNumber;
	    private String sevisid;
	    private boolean fiveYearBarApplicabilityIndicator;
	    private String requesterCommentsForHub;
    
	    private static final String CASE_NUMBER = "CaseNumber";                          
		private static final String SEVIS_ID = "SEVISID";                             
		private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
		private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	

	
	
	public ResubmitRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createResubmitRequestType();
	}
	
	public void setRequest(ResubmitRequestType request) {
		this.request = request;
	}
	
	/**
	 * This method will also do the request specific validations
	 */
	public ResubmitRequestType getRequest() throws HubServiceException {
		
		return this.request;
	}
	
	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}

	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
		this.request.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.requesterCommentsForHub = requesterCommentsForHub;
		this.request.setRequesterCommentsForHub(requesterCommentsForHub);
	}


	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
		this.request.setCaseNumber(caseNumber);
	}

	
	public String getSevisid() {
		return sevisid;
	}

	public void setSevisid(String sevisid) {
		this.sevisid = sevisid;
		this.request.setSEVISID(sevisid);
	}

	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static ResubmitRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
		
		ResubmitRequestTypeWrapper wrapper = new ResubmitRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASE_NUMBER);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
	
		param = obj.get(SEVIS_ID);
		if(param != null){
			wrapper.setSevisid((String)param);
		}
				
		ResubmitRequestTypeWrapperUtil.addIndicatorsToWrapper(obj, wrapper);
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASE_NUMBER, this.caseNumber);	         
		obj.put(SEVIS_ID, this.sevisid);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		return obj.toJSONString();

	}
	
	public ResubmitRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		ResubmitRequestTypeWrapper wrapper = new ResubmitRequestTypeWrapper();
		
		wrapper.setCaseNumber((String)jsonObj.get(CASE_NUMBER));
		wrapper.setSevisid((String)jsonObj.get(SEVIS_ID));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASE_NUMBER, this.caseNumber);
		obj.put(SEVIS_ID, this.sevisid);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		return obj;
	}
}
