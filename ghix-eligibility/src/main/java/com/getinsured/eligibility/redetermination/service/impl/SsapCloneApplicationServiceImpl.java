package com.getinsured.eligibility.redetermination.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.benchmark.service.EligibilityBenchMarkServiceResponse;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicantRepository;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanResponseDTO;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

@Service
public class SsapCloneApplicationServiceImpl implements
 	SsapCloneApplicationService {

	private static final String _131 = "131";
	private static final String _129 = "129";
	private static final String RENEWAL_FAILURE_REASON = " Renewal Failure Reason";
	private static final String RENEWAL_STATUS = " Renewal Status";
	private static final String FAILURE_DUE_TO_PLAN_UNAVAILABILITY = "Failure due to plan unavailability";
	private static final String ERROR_CODE = "Error Code - ";
	private static final String FAILURE = "Failure";
	private static final String SUCCESS = "Success";
	private static final String RENEWAL_INFORMATION = "RENEWAL_INFORMATION";
	private static final String MEMBERS = " Members";
	private static final String INDIVIDUAL = "INDIVIDUAL";
	private static final String HEALTH = "Health";
	private static final String _200 = "200";
	private static final String COVERAGE_YEAR = "Coverage Year";
	private static final String CASE_NUMBER = "Case Number";
	private static final String ELIGIBLE_ENROLLEE = "eligibleEnrolle";
	private static final String DENTAL_ENROLLMENT = "dentalEnrollment";
	private static final String DENTAL = "Dental";
	private static final String PENDING = "PENDING";
	private static final String CONFIRM = "CONFIRM";
	private static final String PLAN_YEAR = "planYear";
	private static final String HIOS_PLAN_ID = "hiosPlanId";
	private static final String JSON_RESULT = "result";
	private static final String JSON_SUMMARY = "summary";
	private static final String PROCESSING_RESULT = "processingResult";
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapCloneApplicationServiceImpl.class);
	private final String SSAPAPPLICATION_GUID_KEY = "SSAP_APPLICATION_DISPLAY_ID";
	private final String notApplicable = "N/A";
	
	@Value("#{configProp['nativeAmerican.default.cslevel'] != null ? configProp['nativeAmerican.default.cslevel'] : 'CS3'}")
	private String nativeAmericanHouseholdCSLevel;
	@Autowired	
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private OutboundATApplicantRepository outboundATApplicantRepository;
	@Autowired
	private SsapUtil ssapUtil;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
    private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;
	@Autowired
	IntegrationLogService integrationLogService;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	@Autowired
	SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	ActiveEnrollmentService activeEnrollmentService;
	@Autowired
	CoverageStartDateService coverageStartDateService;
	@Autowired
	BenchmarkPlanService benchmarkPlanService;
	@Autowired private Gson platformGson;	
	@Autowired private LookupService lookupService;
	@Autowired private AppEventService appEventService;
	@Autowired RenewalApplicationRepository renewalApplicationRepository;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	private static final String CA_STATE_CODE = "CA";
	private static final String NON_FINANCIAL_APPLICATION = "N";
		

	/**
	 * This method returns true if application status is SG, SU, ER or EN.
	 * Uses SSAP_APPLICATIONS.APPLICATION_STATUS
	 * @author sahay_b 
	 */
	@Override
	public Boolean isValidLCEApplication(SsapApplication ssapApplication) {
		if (ssapApplication != null && !isValidLCEApplication(ssapApplication.getApplicationStatus())) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the Enrolled Application for LCE
	 */
	@Override
	public SsapApplication getEnrolledSsapApplicationByMaxIdAndCmrHouseoldId(
			BigDecimal cmrHouseoldId) {
		return ssapApplicationRepository.findByMaxIdAndCmrHouseoldId(cmrHouseoldId);
	}
	

	@Override
	public List<SsapApplication> getRenewedSsapApplicationsByCoverageYear(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getRenewedSsapApplicationsByCoverageYear(renewalYear,new PageRequest(0,batchSize.intValue() )  );
	}

	@Override
	public List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYear(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getAutoRenewalApplicationsForNextCoverageYear(renewalYear,new PageRequest(0,batchSize.intValue()));
	}
	
	@Override
	public List<Long> getSsapApplicationIdsForRenewal(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getSsapApplicationIdsForRenewal(renewalYear,new PageRequest(0,batchSize.intValue()));
	}
	
	@Override
	public List<Long> getSsapApplicationIdsByServerName(Long renewalYear, int serverCount, int serverName, Long batchSize) {
		return ssapApplicationRepository.getSsapApplicationIdsByServerName(renewalYear, serverCount, serverName, new PageRequest(0,batchSize.intValue()));
	}
	
	@Override
	public List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYearWithErrors(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getAutoRenewalApplicationsForNextCoverageYearWithErrors(renewalYear,new PageRequest(0,batchSize.intValue()));
	}
	
	@Override
	public List<Long> getSsapApplicationIdsForErrorProcessing(Long renewalYear, int serverCount, int serverName) {
		return ssapApplicationRepository.getSsapApplicationIdsForErrorProcessing(renewalYear, serverCount, serverName);
	}
	
	@Override
	public List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList){
		return ssapApplicationRepository.getSsapApplicationListByIds(applicationIdList);
	}
	
	@Override
	public Long findOpenApplicationByCoverageYearAndCmrHouseoldId(
			BigDecimal cmrHouseoldId, long coverageYear) {
		return ssapApplicationRepository.findOpenApplicationByCoverageYearAndCmrHouseoldId(cmrHouseoldId, coverageYear);
	}

	@Override
	public void closeSsapApplication(SsapApplication ssapApplication) {
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());		
		ssapApplicationRepository.saveAndFlush(ssapApplication);
	}
	
	@Override
	public List<RenewalApplication> getAutoRenewalApplicationsByCoverageYear(List<RenewalStatus> statusList, Long coverageYear, long batchSize) {
		return renewalApplicationRepository.getAutoRenewalApplicationsByCoverageYear(statusList, coverageYear,new PageRequest(0,(int) batchSize));
	}

	@Override
	public List<Long> getAutoRenewalApplIdListByCoverageYear(List<RenewalStatus> statusList, Long coverageYear, long batchSize) {
		List<Object> list = renewalApplicationRepository.getAutoRenewalApplIdListByCoverageYear(statusList, coverageYear, new PageRequest(0, (int) batchSize));
		return list.stream().map(objId -> Long.parseLong(objId.toString())).collect(Collectors.toList());
	}
	
	@Override
	public List<Long> getAutoRenewalApplIdListByServerName(List<RenewalStatus> statusList, Long coverageYear, int serverCount, int serverName, long batchSize) {
		List<Object> list = renewalApplicationRepository.getAutoRenewalApplIdListByServerName(statusList, coverageYear, serverCount, serverName, new PageRequest(0, (int) batchSize));
		return list.stream().map(objId -> Long.parseLong(objId.toString())).collect(Collectors.toList());
	}

	@Override
	public List<RenewalApplication> getAutoRenewalApplListByIds(List<Long> renewalApplIdList) {
		return renewalApplicationRepository.getAutoRenewalApplListByIds(renewalApplIdList);
	}
	
	@Override
	public SsapApplication saveSsapSepApplication(
			SsapApplication currentApplication, boolean updateJson) throws Exception {
		String applicationData=null;
		currentApplication = ssapApplicationRepository.saveAndFlush(currentApplication);
		if(updateJson) {
			applicationData = LifeChangeEventUtil.setApplicationIdAndGuid(currentApplication.getId(), currentApplication.getApplicationData(), currentApplication.getCaseNumber());
			applicationData=ssapJsonBuilder.transformToJson(ssapJsonBuilder.transformFromJson(applicationData));
			currentApplication.setApplicationData(applicationData);
			currentApplication = ssapApplicationRepository.saveAndFlush(currentApplication);
		}
		return currentApplication;
	}
	
	
	@Override
	public SsapApplication cloneSsapApplicationFromParent(String ssapJson, SsapApplication currentApplication, 
			SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, 
			String applicationSource, String applicationStatus, Long renewalYear) {
		if(currentApplication == null) {
			currentApplication = new SsapApplication();
		}
		
		currentApplication.setApplicationData(ssapJson);
		currentApplication.setParentSsapApplicationId(new BigDecimal(parentApplication.getId()));
		setSSAPApplication(currentApplication, parentApplication, sepRequestParamDTO, applicationType, applicationSource, applicationStatus, renewalYear);
		return currentApplication; 
	}
	
	private SsapApplication setSSAPApplication(SsapApplication ssapApplication, SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, 
			String applicationSource, String applicationStatus, Long renewalYear) {
		Date date = new TSDate();
		String esignFirstName;
		String esignMiddleName;
		String esignLastName;
		
		esignFirstName = sepRequestParamDTO.getEsignFirstName();
		esignMiddleName = sepRequestParamDTO.getEsignMiddleName();
		esignLastName = sepRequestParamDTO.getEsignLastName();
		ssapApplication.setEsignDate(new Timestamp(new TSDate().getTime()));
		ssapApplication.setEsignFirstName(esignFirstName);
		ssapApplication.setEsignMiddleName(esignMiddleName);
		ssapApplication.setEsignLastName(esignLastName);
		ssapApplication.setApplicationType(applicationType);
		ssapApplication.setCaseNumber(ssapUtil.getNextSequenceFromDB(SSAPAPPLICATION_GUID_KEY));			
		ssapApplication.setLastUpdatedBy(null);
		ssapApplication.setLastUpdateTimestamp(null); // Abhai (Why is it null)
		ssapApplication.setSource(applicationSource);
		ssapApplication.setEligibilityStatus(EligibilityStatus.PE);
		if(StringUtils.isNotEmpty(parentApplication.getFinancialAssistanceFlag())){
			ssapApplication.setFinancialAssistanceFlag(parentApplication.getFinancialAssistanceFlag());
		} else {
			ssapApplication.setFinancialAssistanceFlag(NON_FINANCIAL_APPLICATION);
		}
		ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
		ssapApplication.setParentSsapApplicationId(new BigDecimal(parentApplication.getId()));
		ssapApplication.setApplicationStatus(applicationStatus);
		//ssapApplication.setAllowEnrollment(LifeChangeEventConstant.NO);
		ssapApplication.setCmrHouseoldId(parentApplication.getCmrHouseoldId());			
		ssapApplication.setCoverageYear(renewalYear);
		ssapApplication.setCurrentPageId("0");
		ssapApplication.setSsapApplicationSectionId(parentApplication.getSsapApplicationSectionId());
		ssapApplication.setLastNoticeId(parentApplication.getLastNoticeId());
		ssapApplication.setEnrollmentStatus(parentApplication.getEnrollmentStatus());
		if(StringUtils.isBlank(parentApplication.getCsrLevel()) || parentApplication.getCsrLevel().equalsIgnoreCase(notApplicable)){
			ssapApplication.setCsrLevel(null);
		}else{
			ssapApplication.setCsrLevel(parentApplication.getCsrLevel());
		}
		ssapApplication.setMaximumAPTC(null);
		ssapApplication.setElectedAPTC(null);
		ssapApplication.setAllowEnrollment(parentApplication.getAllowEnrollment());
		ssapApplication.setState(parentApplication.getState());
		ssapApplication.setZipCode(parentApplication.getZipCode());
		ssapApplication.setCountyCode(parentApplication.getCountyCode());
		ssapApplication.setExternalApplicationId(parentApplication.getExternalApplicationId());
		ssapApplication.setAvailableAPTC(null);
		ssapApplication.setExchangeType(parentApplication.getExchangeType());
		ssapApplication.setNativeAmerican(parentApplication.getNativeAmerican());
		//ssapApplication.setEhbAmount(parentApplication.getEhbAmount());
		ssapApplication.setExemptHousehold(parentApplication.getExemptHousehold());
				
		
		if(sepRequestParamDTO.getUserId() != null) {
			ssapApplication.setCreatedBy(new BigDecimal(sepRequestParamDTO.getUserId()));
		}
		ssapApplication.setCreationTimestamp(new Timestamp(date.getTime()));
		try {
			ssapApplication.setStartDate(new Timestamp(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse("01/01/2020 00:00:00").getTime()));
		} catch (ParseException e) {
		}
		

		return ssapApplication;
	}
	
	/**
	 * Checks for valid LCE Application
	 * @param applicationStatus
	 * @return
	 */
	private boolean isValidLCEApplication(String applicationStatus) {
		return applicationStatus.equals(ApplicationStatus.SIGNED
				.getApplicationStatusCode())
				|| applicationStatus.equals(ApplicationStatus.SUBMITTED
						.getApplicationStatusCode())
				|| applicationStatus
						.equals(ApplicationStatus.ELIGIBILITY_RECEIVED
								.getApplicationStatusCode()) || applicationStatus
					.equals(ApplicationStatus.ENROLLED_OR_ACTIVE
							.getApplicationStatusCode());
	}
	
	@Override
	public void updateCSRLevel(SsapApplication ssapApplication) throws Exception{
		boolean isNativeAmerican =  nativeAmericanEligibilityHelper.setNativeamericanStatus(ssapApplication);
		String isNativeAmericanStatus = isNativeAmerican ? "Y":"N";
		ssapApplication.setNativeAmerican(isNativeAmericanStatus);
		if(isNativeAmerican){
			ssapApplication.setCsrLevel(nativeAmericanHouseholdCSLevel);
		}else{
			ssapApplication.setCsrLevel(null);
		}
	}
	
	
	
	@Override
	public void logProcessData(Set<Future<Map<String,String>>> tasks,String batchName){
		List<Map<String,String>> jsonResultFailure = new ArrayList<Map<String,String>>();
		Map<String, String> result =null;
		Map<String,Long> summary = new HashMap<String,Long>();
		Long count = null;
		String processingResult = null;
		JsonObject jsonObject = null;
		try {
			for (Future<Map<String,String>> future : tasks) {
				if(future.isDone()) {
					try{
						result = future.get();
					}catch(Exception e){
						 result =  new HashMap<String,String>();
						 result.put(PROCESSING_RESULT,RenewalStatus.ERROR.toString());
						 result.put("exception",ExceptionUtils.getFullStackTrace(e));
					}
					if (result != null) {
						processingResult = result.get(PROCESSING_RESULT);
					}
					if (processingResult == null || processingResult.equals(RenewalStatus.ERROR.toString())) {
						jsonResultFailure.add(result);
					}
					
					if (processingResult != null) {
						count = summary.get(processingResult);
						if (count == null) {
							count = 1L;
							summary.put(processingResult, count);
						}else{
							count++;
							summary.put(processingResult, count);
						}
					}
				}
			}
			
			if(!jsonResultFailure.isEmpty() || !summary.isEmpty()){
				jsonObject = new JsonObject();
				jsonObject.add(JSON_SUMMARY,platformGson.toJsonTree(summary, new TypeToken<Map<String,Long>>() {}.getType()).getAsJsonObject());
				jsonObject.add(JSON_RESULT,platformGson.toJsonTree(jsonResultFailure, new TypeToken<List<Map<String,String>>>() {}.getType()).getAsJsonArray());
				LOGGER.info(batchName+" execution summary :"+ jsonObject.toString());
				integrationLogService.save(jsonObject.toString(), batchName, GhixConstants.RESPONSE_SUCCESS, null, null);
			}
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured while genereate "+batchName+" execution details",ex);
			try{
				giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), this.getClass().getName(), ExceptionUtils.getStackTrace(ex), null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
			}catch(Exception e){
				LOGGER.error("Exception occured while genereate "+batchName+" execution details",e);
			}
		}
	}
	
	
	@Override
	public void logData(Set<Future<Map<String,String>>> tasks,String batchName){
		List<Map<String,String>> jsonResultSuccess = new ArrayList<Map<String,String>>();
		List<Map<String,String>> jsonResultFailure = new ArrayList<Map<String,String>>();
		Map<String, String> result =null;
		String jsonString = null;
		Map<String,Long> summary = new HashMap<String,Long>();
		Long count = null;
		String processingResult = null;
		JsonObject jsonObject = null;
		try {
			for (Future<Map<String,String>> future : tasks) {
				if(future.isDone()) {
					try{
						result = future.get();
					}catch(Exception e){
						 result =  new HashMap<String,String>();
						 result.put(PROCESSING_RESULT,RenewalStatus.ERROR.toString());
						 result.put("exception",ExceptionUtils.getFullStackTrace(e));
					}
					if (result != null) {
						processingResult = result.get(PROCESSING_RESULT);
					}
					if (processingResult != null && !processingResult.equals(RenewalStatus.ERROR.toString())) {
						jsonResultSuccess.add(result);
					} else {
						jsonResultFailure.add(result);
					}
						
					if (processingResult != null) {
						count = summary.get(processingResult);
						if (count == null) {
							count = 1L;
							summary.put(processingResult, count);
						}else{
							count++;
							summary.put(processingResult, count);
						}
					}
				}
			}
			
						
			if(!jsonResultSuccess.isEmpty() || !summary.isEmpty()){
				jsonObject = new JsonObject();
				jsonObject.add(JSON_SUMMARY,platformGson.toJsonTree(summary, new TypeToken<Map<String,Long>>() {}.getType()).getAsJsonObject());
				jsonObject.add(JSON_RESULT,platformGson.toJsonTree(jsonResultSuccess, new TypeToken<List<Map<String,String>>>() {}.getType()).getAsJsonArray());
				LOGGER.info(batchName+" execution summary :"+ jsonObject.toString());
				integrationLogService.save(jsonObject.toString(), batchName, GhixConstants.RESPONSE_SUCCESS, null, null);
			}
			if(!jsonResultFailure.isEmpty()){
				jsonString = (platformGson.toJsonTree(jsonResultFailure, new TypeToken<List<Map<String,String>>>() {}.getType()).getAsJsonArray().toString());
				LOGGER.info(batchName+" execution summary :"+ jsonObject.toString());
				integrationLogService.save(jsonString, batchName, GhixConstants.RESPONSE_FAILURE, null, null);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while genereate "+batchName+" execution details",ex);
			try{
				giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), this.getClass().getName(), ExceptionUtils.getStackTrace(ex), null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
			}catch(Exception e){
				LOGGER.error("Exception occured while genereate "+batchName+" execution details",e);
			}
		}
	}

	/**
	 * Method is used to Log Batch data.
	 */
	@Override
	public void logData(List<Map<String, String>> jsonResultSuccess, List<Map<String, String>> jsonResultFailure, Map<String, Long> summary,
			String batchName) {

		try {

			if (!jsonResultSuccess.isEmpty() || !summary.isEmpty()) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.add(JSON_SUMMARY, platformGson.toJsonTree(summary, new TypeToken<Map<String, Long>>() {}.getType()).getAsJsonObject());
				jsonObject.add(JSON_RESULT, platformGson.toJsonTree(jsonResultSuccess, new TypeToken<List<Map<String, String>>>() {}.getType()).getAsJsonArray());
				LOGGER.info(batchName + " execution summary :" + jsonObject.toString());
				integrationLogService.save(jsonObject.toString(), batchName, GhixConstants.RESPONSE_SUCCESS, null, null);
			}

			if (!jsonResultFailure.isEmpty()) {
				String jsonString = (platformGson.toJsonTree(jsonResultFailure, new TypeToken<List<Map<String, String>>>() {}.getType()).getAsJsonArray().toString());
				LOGGER.info(batchName + " execution summary :" + jsonString);
				integrationLogService.save(jsonString, batchName, GhixConstants.RESPONSE_FAILURE, null, null);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception occured while genereate " + batchName + " execution details", ex);

			try {
				giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), this.getClass().getName(),
						ExceptionUtils.getStackTrace(ex), null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
			}
			catch (Exception e) {
				LOGGER.error("Exception occured while genereate " + batchName + " execution details", e);
			}
		}
	}

	public EnrolleeResponse getEnrollmentDetails(Long previousYearEnrolledApplication) throws GIException{
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(previousYearEnrolledApplication);
		//EnrolleeResponse enrolleeResponse = enrolleeService.findEnrolleeByApplicationid(enrolleeRequest);
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrolleeRequest));
		EnrolleeResponse enrolleeResponse = platformGson.fromJson(response.getBody(), EnrolleeResponse.class);
		
		if (null == enrolleeResponse || !enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
			
		}
		return enrolleeResponse;
	}
	
	
	
	@Override
	public long findPreivousYearEnrolledApplication(BigDecimal cmrHouseholdId) throws GIException{
		Long previousCoverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		Long ssapApplicationId =  ssapApplicationRepository.getEnrolledSsapApplicationIdByCoverageYear(previousCoverageYear,cmrHouseholdId);
		if (null == ssapApplicationId) {
			throw new GIException("Failed to find enrolled application for previous coverage year :"+cmrHouseholdId);
			
		}
		return ssapApplicationId;
		
	}
	
	
	@Override
	public SsapApplication findPreivousYearApplicationAndApplicants(Long previousYearApplicationId) throws GIException{
		List<SsapApplication> ssapApplications =  ssapApplicationRepository.getApplicationsById(previousYearApplicationId);
		if (ssapApplications != null) {
			return ssapApplications.get(0);
		}
		else{
			throw new GIException("Failed to find application for previous year applicationId : " + previousYearApplicationId);
		}
	}
	
	
	@Override
	public Map<String,Object> dependentDentalAgeCheck(SsapApplication ssapApplication,Long renewalYear) throws GIException{
		Map<String,Object> result = new HashMap<String,Object>();
		Long previousCoverageYearEnrolledApp =  findPreivousYearEnrolledApplication(ssapApplication.getCmrHouseoldId());
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
		EnrollmentShopDTO dentalEnrollment = fetchDentalEnrollment(previousCoverageYearEnrolledApp);
		if(dentalEnrollment == null){
			return null;
		}
		result.put(DENTAL_ENROLLMENT, dentalEnrollment);
		//get active health enrollee
		Set<String>  eligibleEnrolle = getEligibleEnrollee(dentalEnrollment, ssapApplication, ssapApplicants, renewalYear);
		result.put(ELIGIBLE_ENROLLEE, eligibleEnrolle); 
		return result;
	}
	
	@Override
	public Set<String> dependentDentalAgeCheck(SsapApplication ssapApplication,List<SsapApplicant> ssapApplicants,Long previousYearEnrolledApplication,Long renewalYear) throws GIException{
		EnrollmentShopDTO dentalEnrollment = fetchDentalEnrollment(previousYearEnrolledApplication);
		if(dentalEnrollment == null){
			return null;
		}
		//get active health enrollee
		return getEligibleEnrollee(dentalEnrollment, ssapApplication, ssapApplicants, renewalYear);
		
	}
	
	
	private Set<String> getEligibleEnrollee(EnrollmentShopDTO dentalEnrollment,SsapApplication ssapApplication,List<SsapApplicant> ssapApplicants,Long renewalYear) throws GIException{
		Set<String> activeDentalEnrollee  = null;
		Set<String> eligibleDentalEnrollee = new HashSet<String>();
		Set<String> ageOutEnrollee  = new HashSet<String>();
		Set<String> ageOut26Enrollee  = new HashSet<String>();
		String childRelations = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_CHILDRELATIONCODE);
		Long previousCoverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		
		activeDentalEnrollee = getActiveEnrollee(dentalEnrollment);
		if(activeDentalEnrollee ==null || activeDentalEnrollee.isEmpty()){
			return activeDentalEnrollee;
		}
		
		String[] childRelationArr = childRelations.split(",");
		Set<String> bloodRelations = new HashSet<String>();
		for (String childRelation : childRelationArr) {
			bloodRelations.add(childRelation);
		}
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		List<BloodRelationship> relationShip = getBloodRelationship(members);
		if(relationShip == null){
			throw new GIRuntimeException("Can't find relationship for application :"+ssapApplication.getId());
		}
		
		Date coverageStartDate = getCoverageStartDate(ssapApplication);
		
		//check if child relationship build age out set 
		for (BloodRelationship bloodRelationship : relationShip) {
			if (bloodRelationship.getRelatedPersonId() != null && Integer.parseInt(bloodRelationship.getRelatedPersonId()) == 1) {
				if(bloodRelations.contains(bloodRelationship.getRelation())){	
					for (SsapApplicant ssapApplicant : ssapApplicants) {
						if (ssapApplicant.getPersonId() == Integer.parseInt(bloodRelationship.getIndividualPersonId())) {
							if (checkForAge19(ssapApplicant.getBirthDate(),coverageStartDate) && ssapApplicant.getApplyingForCoverage().equals("Y")  && activeDentalEnrollee.contains(ssapApplicant.getApplicantGuid())) {
								ageOutEnrollee.add(ssapApplicant.getApplicantGuid());
								//build set for age more 26 
								if(checkForAge26(ssapApplicant.getBirthDate(),coverageStartDate)){
									ageOut26Enrollee.add(ssapApplicant.getApplicantGuid());
								}
							}
						}
					}
				}
			}
		}
		
		//no age out member
		if(!ageOutEnrollee.isEmpty()){
			//is child only plan
			boolean isChildOnly = isChildOnlyPlan(dentalEnrollment.getCmsPlanId(),previousCoverageYear);
			//if adult dental plan
			if(!isChildOnly){
				//Adult Dental Plan only members more than 26 will age out
				ageOutEnrollee = ageOut26Enrollee;
			}
		}
		//filter non seeking and age out members
		eligibleDentalEnrollee = filterNonSeekingMembers(ssapApplicants,ageOutEnrollee,activeDentalEnrollee);
		return eligibleDentalEnrollee;
	}

	private EnrollmentShopDTO fetchDentalEnrollment(Long previousYearEnrolledApplication) throws GIException {
		
		Set <String> validEnrollmentStatus = validEnrollmentStatus();
		//get enrollee response
		EnrolleeResponse enrolleeResponse = getEnrollmentDetails(previousYearEnrolledApplication);
		//get eligible health enrollee
		return getDentalEnrollment(enrolleeResponse,validEnrollmentStatus);
		
	}

	
	private Set<String> validEnrollmentStatus() {
		String activeEnrollmentStatusList = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_ACTIVEENROLLMENTSTATUSLIST);
		Set<String> validActiveEnrollmentStatus = new HashSet<String>();
		String[] activeEnrollmentStatusArr = activeEnrollmentStatusList.split(",");
		
		for (String activeEnrollmentStatus : activeEnrollmentStatusArr) {
			validActiveEnrollmentStatus.add(activeEnrollmentStatus);
		}
	
		if(validActiveEnrollmentStatus.size() == 0) {
			validActiveEnrollmentStatus.add("CONFIRM");
			validActiveEnrollmentStatus.add("PENDING");
		}
		return validActiveEnrollmentStatus;
	}
	
	
	private Set<String> filterNonSeekingMembers(List<SsapApplicant> ssapApplicants,Set<String> ageOutEnrollee,Set<String> activeDentalEnrollee) {
		Set<String> eligibleDentalEnrollee = new HashSet<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants){
			if(ssapApplicant.getApplyingForCoverage().equals("Y") && !ageOutEnrollee.contains(ssapApplicant.getApplicantGuid())
				&& activeDentalEnrollee.contains(ssapApplicant.getApplicantGuid())){
				eligibleDentalEnrollee.add(ssapApplicant.getApplicantGuid());
			}
		}
		return eligibleDentalEnrollee;
	}

	private List<BloodRelationship> getBloodRelationship(List<HouseholdMember> members) {
		for(HouseholdMember householdMember : members){
			if(householdMember.getPersonId() == 1){
				return householdMember.getBloodRelationship();
			}
		}
		return null;
	}
	
	private boolean isChildOnlyPlan(String cmsPlanId,Long coverageYear) {
		Map<String,Object> requestMap = new HashMap<>();
		requestMap.put(HIOS_PLAN_ID, cmsPlanId);
		requestMap.put(PLAN_YEAR, coverageYear);
		String request = platformGson.toJson(requestMap);
		PediatricPlanResponseDTO pediatricPlanResponseDTO=ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PEDIATRIC_PLAN_INFO, request,PediatricPlanResponseDTO.class);
			return pediatricPlanResponseDTO.isChildOnly();
	}
	
	
	private Date getCoverageStartDate(Long renewalYear) {
		Calendar dob = TSCalendar.getInstance(); 
		dob.set(Calendar.DAY_OF_MONTH, 1);
		dob.set(Calendar.MONTH, 0);
		dob.set(Calendar.YEAR, renewalYear.intValue());
		return dob.getTime();
	}


	private boolean checkForAge19(Date birthDate, Date coverageStartDate) {
		Calendar dob = TSCalendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar today = TSCalendar.getInstance();  
		today.setTime(coverageStartDate);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		  age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
		  age--;  
		}
		
		return (age >= 19);
	}
	private boolean checkForAge26(Date birthDate, Date coverageStartDate) {
		Calendar dob = TSCalendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar today = TSCalendar.getInstance();  
		today.setTime(coverageStartDate);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			age--;  
		}
		
		return (age >= 26);
	}
	
	
	
	
	
	private EnrollmentShopDTO getDentalEnrollment(EnrolleeResponse enrolleeResponse,Set<String> validEnrollmentStatus){
		List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
		List<EnrollmentShopDTO> dentalEnrollment = new ArrayList<EnrollmentShopDTO>();
		EnrollmentShopDTO dentalEnrollmentDTO = null;
		
		//sort to consider latest enrollments for age out.
		Comparator<EnrollmentShopDTO> cmp = (enrollment1, enrollment2) -> enrollment2.getEnrollmentCreationTimestamp().compareTo(enrollment1.getEnrollmentCreationTimestamp());
		
		//filter health and dental enrollment
		for(EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs){
			if(validEnrollmentStatus.contains(enrollmentShopDTO.getEnrollmentStatusValue()) && enrollmentShopDTO.getPlanType().equals(DENTAL) ){
					dentalEnrollment.add(enrollmentShopDTO);
			}
		}
		
		if(!dentalEnrollment.isEmpty() ){
			if(dentalEnrollment.size()>1){
				Collections.sort(dentalEnrollment,cmp);
			}
			
			if(dentalEnrollment.get(0).getCoverageEndDate().after(new TSDate())){
				dentalEnrollmentDTO = dentalEnrollment.get(0);
			}
		}
		
		return dentalEnrollmentDTO; 
		
		
	}
	
	
	private Set<String> getActiveEnrollee(EnrollmentShopDTO enrollmentShopDTO){
		Set<String> activeDentalEnrollee = new HashSet<String>();
		if(enrollmentShopDTO != null){
			populateEnrollee(enrollmentShopDTO.getEnrolleeShopDTOList(), activeDentalEnrollee);
		}
		return activeDentalEnrollee;
		
	}
	
	private void populateEnrollee(List<EnrolleeShopDTO> enrolleeShopDTOList,Set<String> activeEnrolleeSet){
		for(EnrolleeShopDTO enrolleeShopDTO:enrolleeShopDTOList){
			if(enrolleeShopDTO.getEnrolleeLookUpValue().equals(CONFIRM) || enrolleeShopDTO.getEnrolleeLookUpValue().equals(PENDING) ){
				activeEnrolleeSet.add(enrolleeShopDTO.getExchgIndivIdentifier());
			}
			
		}
	}
	
	@Override
	public Float getRemainingAPTCForDental(SsapApplication ssapApplication) throws GIException{
		BigDecimal totalAptc = new BigDecimal("0.0");
		
		if(ssapApplication.getMaximumAPTC() == null || ssapApplication.getMaximumAPTC().floatValue() == 0){
			return null;
		}
		//fetch APTC for health enrollment
		List<EnrollmentDataDTO> enrollmentList = activeEnrollmentService.getEnrollmentDataBySsapApplicationId(ssapApplication.getId());
		
		for (EnrollmentDataDTO enrollmentDataDTO : enrollmentList) {
			if(enrollmentDataDTO.getAptcAmount() != null) {
				totalAptc = totalAptc.add(new BigDecimal(enrollmentDataDTO.getAptcAmount()));
			}
		}
		
		//set MAX_APTC - APPLIED APTC for HEALTH
		return ssapApplication.getMaximumAPTC().subtract(totalAptc).floatValue();
	}
	
	@Override
	public Date getCoverageStartDate(SsapApplication ssapApplication) throws GIException {
		Timestamp timestamp = coverageStartDateService.computeCoverageStartDate(new BigDecimal(ssapApplication.getId()), new HashMap<String,EnrollmentShopDTO>(), false, false);
		Calendar startDate = TSCalendar.getInstance();
		startDate.setTimeInMillis(timestamp.getTime());
		return startDate.getTime();
		
	}
	
	@Override
	public float getEhbAmount(String caseNumber,Date coverageStartDate){
		float ehbAmount = 0.0f;
		EligibilityBenchMarkServiceResponse eligibilityBenchMarkServiceResponse = null;
		try {
			eligibilityBenchMarkServiceResponse = benchmarkPlanService.process(caseNumber, coverageStartDate);
			if (EligibilityBenchMarkServiceResponse.ResponseCode.SUCCESS.equals(eligibilityBenchMarkServiceResponse.getResponseCode())) {
				ehbAmount = eligibilityBenchMarkServiceResponse.getBenchMarkPremiumAmount().floatValue();
			}
		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while getting benchmark premium", e);
		}
		return ehbAmount;
	}

	@Override
	public void createApplicationEvents(SsapApplication ssapApplication, List<SsapApplicant> applicantList,
			String reasonCode, Long enrollmentId, String planId, String planType) {

		LOGGER.debug("Start createApplicationEvents()");
		try {
			//event type and category
			String eventName = null;
			EventInfoDto eventInfoDto = new EventInfoDto();
			Map<String, String> mapEventParam = new LinkedHashMap<>();
			
			if (checkStateCode(CA_STATE_CODE)) {
				String houseHoldCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(Integer.valueOf(ssapApplication.getCmrHouseoldId().toString()));
				if (houseHoldCaseId != null) {
					mapEventParam.put(CASE_NUMBER, houseHoldCaseId);
				}
			} else {
				mapEventParam.put(CASE_NUMBER, ssapApplication.getCaseNumber());
			}
			
			mapEventParam.put(COVERAGE_YEAR,String.valueOf(ssapApplication.getCoverageYear()));
			mapEventParam.put("planType", planType);
			mapEventParam.put("enrollmentId", String.valueOf(enrollmentId));
			mapEventParam.put("planId", planId);

			if (reasonCode != null) {

				if (HEALTH.equalsIgnoreCase(planType)) {
					updateResponseDetails(reasonCode, mapEventParam, HEALTH);
				}
				else if (DENTAL.equalsIgnoreCase(planType)) {
					updateResponseDetails(reasonCode, mapEventParam, DENTAL);
				}
			}

			if (reasonCode.equals(_200)) {
				mapEventParam.put(planType + MEMBERS, getMemberNames(applicantList));
			}
			eventInfoDto.setModuleId(ssapApplication.getCmrHouseoldId().intValue());
			eventInfoDto.setModuleName(INDIVIDUAL);

			if (reasonCode != null && reasonCode.equals(_200)) {
				eventName = "RENEWAL_SUCCESS";
			}
			else {
				eventName = "RENEWAL_FAILED";
			}

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(RENEWAL_INFORMATION, eventName);
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while log Application event" + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("End createApplicationEvents()");
		}
	}

	private String getMemberNames(List<SsapApplicant> dentalApplicants) {
		StringBuilder comments = new StringBuilder();
		boolean isFirst = true;
		for(SsapApplicant ssapApplicant : dentalApplicants) {
			if(isFirst) {
				comments.append(getName(ssapApplicant));
				isFirst = false;
			}else {
				comments.append(", ").append(getName(ssapApplicant));
			}
		}
		return comments.toString();
	}
	
	private void updateResponseDetails(String responseCode,Map<String, String> mapEventParam ,String enrollmentType) {
		String renewalStatus = null;
		String renewalDecscription = null;
		switch (responseCode) {
		case _200:
			renewalStatus=SUCCESS;
			break;
		case _129:
		case _131:
			renewalStatus = FAILURE;
			renewalDecscription = FAILURE_DUE_TO_PLAN_UNAVAILABILITY;
			break;
		default:
			renewalStatus = FAILURE;
			renewalDecscription = ERROR_CODE+responseCode;
			break;
		}
		mapEventParam.put(enrollmentType+ RENEWAL_STATUS, renewalStatus);
		if(renewalDecscription !=null) {
			mapEventParam.put(enrollmentType+ RENEWAL_FAILURE_REASON, renewalDecscription);
		}
		
		
		
		
	}

	private String getName(SsapApplicant ssapApplicant) {
		StringBuilder name = new StringBuilder();
		name.append(ssapApplicant.getFirstName()).append(" ");
		if(!StringUtils.isBlank(ssapApplicant.getMiddleName())) {
			name.append(ssapApplicant.getMiddleName()).append(" ");;
		}
		name.append(ssapApplicant.getLastName());
		return name.toString();
	}

	@Override
	public List<Long> getEnrolledSsapApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize) {
		return ssapApplicationRepository.getEnrolledSsapApplicationsByCoverageYearWithPageable(coverageYear, applicationStatusList, new PageRequest(0, batchSize.intValue()));
	}
	
	@Override
	public List<Long> getEnrolledSsapApplicationsIdByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize, String renewalStatus) {
		StringBuilder queryString = new StringBuilder();
		EntityManager em =null;
		List<Long> applicationIds = new ArrayList<>();
		Query query = null;
		try{
			Integer startRecord = 1;
			Integer endRecord = batchSize.intValue();
			
			em = entityManagerFactory.createEntityManager(); 
		
			if(StringUtils.isBlank(renewalStatus)){
				queryString.append("select id from ( "
						+ " select id,ROW_NUMBER() OVER (ORDER BY ID ASC) LINE_NUMBER from ("
						+ " select id,cmr_houseold_id, coverage_year,application_status,renewal_status,"
						+ " row_number() over (partition by cmr_houseold_id, coverage_year order by CREATION_TIMESTAMP desc) rn "
						+ " from ssap_applications where application_status in (:applicationStatusList) and coverage_year=:coverageYear "
						+ " and renewal_status is null) temp  "
						+ " where temp.rn=1) sub "
						+ " where LINE_NUMBER between :startRecord and :endRecord");
				query = em.createNativeQuery(queryString.toString());
			} else {
				queryString.append("select id from ( "
						+ " select id,ROW_NUMBER() OVER (ORDER BY ID ASC) LINE_NUMBER from ("
						+ " select id,cmr_houseold_id, coverage_year,application_status,renewal_status,"
						+ " row_number() over (partition by cmr_houseold_id, coverage_year order by CREATION_TIMESTAMP desc) rn "
						+ " from ssap_applications where application_status in (:applicationStatusList) and coverage_year=:coverageYear "
						+ " and renewal_status = :renewalStatus) temp  "
						+ " where temp.rn=1) sub "
						+ " where LINE_NUMBER between :startRecord and :endRecord");
				query = em.createNativeQuery(queryString.toString());
				query.setParameter("renewalStatus", renewalStatus);
			}
			
			query.setParameter("applicationStatusList", applicationStatusList);
			query.setParameter("coverageYear", coverageYear);
			query.setParameter("startRecord", startRecord);
			query.setParameter("endRecord", endRecord);
			
			List<BigDecimal> objList = query.getResultList();	
			Iterator<BigDecimal> iterator = objList.iterator();

			while (iterator.hasNext()) {
				BigDecimal objArray = iterator.next();
				applicationIds.add(objArray.longValue());
			}
		} catch (Exception e) {
			LOGGER.error("Error while fetching application id :", e);
			throw new GIRuntimeException("Excpetion occured while fetching application id : ", e);
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		return applicationIds;
	}

	@Override
	public List<Long> getSsapApplicationsByCoverageYearAndApplicationStatusList(long coverageYear, List<String> applicationStatusList, Long batchSize) {
		return ssapApplicationRepository.getSsapApplicationsByCoverageYearAndApplicationStatusList(coverageYear, applicationStatusList, new PageRequest(0, batchSize.intValue()));
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public SsapUtil getSsapUtil() {
		return ssapUtil;
	}

	public void setSsapUtil(SsapUtil ssapUtil) {
		this.ssapUtil = ssapUtil;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	@Override
	public List<Long> getOTRApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize) {
		return ssapApplicationRepository.getOTRApplicationsByCoverageYearWithPageable(coverageYear, applicationStatusList, new PageRequest(0, batchSize.intValue()));
	}

	private static boolean checkStateCode(String code){
 	 	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
 	 	return stateCode.equalsIgnoreCase(code);
 	}
	
	@Override
	public List<Long> getSsapAppIdsForRenewalWithSgStatus(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getSsapAppIdsForRenewalWithSgStatus(renewalYear,new PageRequest(0,batchSize.intValue()));
	}

	@Override
	public List<Long> getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(long coverageYear,
			List<String> applicationStatusList, Long batchSize) {
		return ssapApplicationRepository.getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(coverageYear,
				applicationStatusList, new PageRequest(0, batchSize.intValue()));
	}
}
