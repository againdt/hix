package com.getinsured.eligibility.slcsp.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.google.gson.Gson;

@Component
public class BenchMarkPremiumRestUtil {
	
	private static final Logger LOGGER = Logger.getLogger(BenchMarkPremiumRestUtil.class);
	
	private GhixRestTemplate ghixRestTemplate;
 
	private Gson platformGson;
	
	@Autowired
	public BenchMarkPremiumRestUtil(GhixRestTemplate ghixRestTemplate, Gson platformGson) {
		this.ghixRestTemplate = ghixRestTemplate;
		this.platformGson = platformGson;
	}

	public PlanRateBenefitResponse getBenchmarkPlanBenefitRate(final PlanRateBenefitRequest planRateBenefitRequest) {
		PlanRateBenefitResponse planRateBenefitResponse = null;
		try {
			LOGGER.info("BP Request :: "  + platformGson.toJson(planRateBenefitRequest));
			String responseStr = ghixRestTemplate.postForObject(PlanMgmtEndPoints.GET_BENCHMARK_PLAN_RATE_URL,
					planRateBenefitRequest, String.class);
			if (StringUtils.isNotBlank(responseStr)) {
				planRateBenefitResponse = platformGson.fromJson(responseStr, PlanRateBenefitResponse.class);
			}
		}catch(Exception e){
			throw e;
		}
		return planRateBenefitResponse;
	}

}
