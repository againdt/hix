//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.26 at 10:06:56 AM PST 
//


package com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.PersonType;


/**
 * A data type for a human being.
 * 
 * <p>Java class for RequestPersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestPersonType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://niem.gov/niem/niem-core/2.0}PersonType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonBirthDate"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonSexCode" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonSSNIdentification"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestPersonType")
public class RequestPersonType
    extends PersonType
{


}
