package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.util.StringUtils;

import com.getinsured.hix.batch.ssap.renewal.dto.GenerateNoticeEnum;
import com.getinsured.hix.batch.ssap.renewal.service.RenewalNotificationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;

public class NotificationTask implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTask.class);

	private RenewalNotificationService renewalNotificationService;
	private static volatile boolean isBatchRunning = false;
	private static final String STATE_CODE = "MN";
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		synchronized (this) {
			if(isBatchRunning) {
				throw new GIException("Batch is already running");
			}
			else {
				isBatchRunning = true;
			}
		}
		
		try {
			
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			
			Long batchSize = getBatchSize(chunkContext);
			
			GenerateNoticeEnum generateNoticeFor;
			if (!STATE_CODE.equalsIgnoreCase(stateCode)) {
				generateNoticeFor = GenerateNoticeEnum.ALL;
			} else {
				generateNoticeFor = getGenerateNoticeForParamValue(chunkContext);
			}
			
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("GENERATE_NOTICE_FOR parameter value : "+generateNoticeFor.toString());
			}
			
			renewalNotificationService.processRenewalApplicationData(generateNoticeFor, batchSize, stateCode);
			
			LOGGER.info("Finish Job");
		} finally {
			isBatchRunning = false;
		}
		return RepeatStatus.FINISHED;
	}

	public RenewalNotificationService getRenewalNotificationService() {
		return renewalNotificationService;
	}

	public void setRenewalNotificationService(RenewalNotificationService renewalNotificationService) {
		this.renewalNotificationService = renewalNotificationService;
	}
	
	private Long getBatchSize(ChunkContext chunkContext) throws GIException {
		String batchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);

		batchSize = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("BATCH_SIZE", batchSize);

		Long batchSizeValue = Long.valueOf(batchSize);

		if (batchSizeValue == 0) {
			throw new GIException("Invalid Batch Size");
		}

		return batchSizeValue;
	}

	private GenerateNoticeEnum getGenerateNoticeForParamValue(ChunkContext chunkContext) throws GIException {
		String generateNoticeFor = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("GENERATE_NOTICE_FOR");
		
		if (StringUtils.isEmpty(generateNoticeFor)) {
				throw new GIException("\"GENERATE_NOTICE_FOR\" is required and valid values are SUCCESS/FAILURE/BOTH");
		}
		
		return getGenerateNoticeForByName(generateNoticeFor);
	}
	
	private GenerateNoticeEnum getGenerateNoticeForByName(String generateNoticeParam) throws GIException {
		try {
			GenerateNoticeEnum generateNoticeFor = GenerateNoticeEnum.valueOf(generateNoticeParam);
			return generateNoticeFor;
		} catch (IllegalArgumentException e) {
			throw new GIException("Invalid GenerateNoticeFor param value, valid values are SUCCESS/FAILURE/BOTH");
		}
	}
}