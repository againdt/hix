package com.getinsured.hix.batch.hub.exceptions;

public class HubServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HubServiceException() {
		super();
	}

	public HubServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public HubServiceException(String message) {
		super(message);
	}

	public HubServiceException(Throwable cause) {
		super(cause);
	}
}
