<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
	<div class="gutter10">
	<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
		<h1 id="skip"><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.planAbout'/></h4>       			
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/issuer/qdp/plandetails/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/viewbenefits/${encPlanId}"/>"><spring:message  code='label.planBenefits'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/viewrates/${encPlanId}"/>"><spring:message  code='label.planRates'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/qhphistory/${encPlanId}"/>"><spring:message  code='label.planStatusHistory'/></a></li>
			   </ul>		
        </div><!-- end of span3 -->			
            <div class="span9" id="rightpanel">
                <form class="form-horizontal" id="frmCreateTicket" name="frmCreateTicket" action="<c:url value="/planmgmt/issuer/qdp/save" />" method="POST">
                	 <input type="hidden" id="planId" name="planId" value="${encPlanId}"/>
                	 <input type="hidden" id="planName" name="planName" value="${plan.name}"/>
                	 <input type="hidden" id="issuerId" name="issuerId" value="${encIssuerId}"/>
                    	<df:csrfToken/>
                        <div class="header">
                            <h4 class="pull-left"><spring:message  code="label.createTicket"/></h4>
                            <a href='<c:url value="/issuer/qdp/plandetails/${encPlanId}"/>' class="btn btn-small pull-right"><spring:message  code="label.cancel"/></a>
                            <input type="submit" name="CreateTicketSubmitBtn" id="CreateTicketSubmitBtn" class="btn btn-primary btn-small pull-right" value="<spring:message  code="label.save"/>" title="<spring:message  code="label.save"/>">
                        </div>
                        <div class="gutter10">
        
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.issuerName"/></label>
                                    <div class="controls">
                                       <div class="margin5-t"><strong>${issuerObj.name}</strong></div>                                       
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.planName"/></label>
                                    <div class="controls">
                                    	<div class="margin5-t"><strong>${plan.name}</strong></div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.planNumber"/> </label>
                                    <div class="controls">
                                    	<div class="margin5-t"><strong>${plan.issuerPlanNumber}</strong></div>
                                    </div>
                                </div>
                                
                                 <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.createdBy"/> </label>
                                    <div class="controls">
			                           <div class="margin5-t"><strong>${issuerRepObj.firstName} &nbsp; ${issuerRepObj.lastName}</strong></div>
			                        </div> 
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.priority"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
                                    	<select id="priority" name="priority">
                                    		<option value="Critical">Critical</option>
                                    		<option value="High">High</option>
                                    		<option value="Medium" SELECTED>Medium</option>
                                    		<option value="Low">Low</option>
                                    	</select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.ticketDetails"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
                                 		<div class="pull-left">
											<div class="input">
		                                    	<textarea name="description" id="description" rows="2" cols="20" ></textarea>                                  
		                                    </div>
	                                    </div>
										<div id="description_error"></div>
                                    </div>
                                </div>
                                </div>
                       </div>
                </form>
        	</div><!--  end of span9 -->
		</div>
	</div>
	</div><!-- end row-fluid -->
	<script type="text/javascript">
	var validator = $("#frmCreateTicket").validate({ 
		rules : {			   
			    description : { required : true, maxlength : 2000 },			    
			    priority : {required:true}
			    
		},
		messages : {
			
			description: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateDescription' javaScriptEscape='true'/></span>",
						   maxlength : "<span> <em class='excl'>!</em><spring:message code='label.descriptionMaxLen' javaScriptEscape='true'/></span>"},			
			priority: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriority' javaScriptEscape='true'/></span>"}
			
		},
		errorClass: "error",
		errorPlacement : function(error, element) {
			formSubmited=false;
			var elementId = element.attr('id');
			if($("#" + elementId + "_error").html() != null)
			{
				$("#" + elementId + "_error").html('');
			}
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		}
	});
   </script>