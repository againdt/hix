package com.getinsured.iex.hub.nmec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.IndividualResponseSetType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ResponseMetadataType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.VerifyNonESIMECResponseType;

public class VerifyNonESIMECResponseTypeWrapper implements JSONAware {

	private ResponseMetadataWrapper responseMetadata;
	private IndividualResponseSetTypeWrapper individualResponseSet;
	private String responseCode = null;

	public VerifyNonESIMECResponseTypeWrapper(VerifyNonESIMECResponseType verifyNonESIMECResponseType)
	{
		if(verifyNonESIMECResponseType != null)
		{
			ResponseMetadataType metaData = verifyNonESIMECResponseType.getResponseMetadata();
			
			if(metaData != null)
			{
				responseMetadata = new ResponseMetadataWrapper(metaData);
				this.responseCode = responseMetadata.getResponseCode();
			}
			IndividualResponseSetType individualResponseSetType = verifyNonESIMECResponseType.getIndividualResponseSet();
			
			if(individualResponseSetType != null)
			{
				individualResponseSet = new IndividualResponseSetTypeWrapper(individualResponseSetType);
			}
		}
	}
	
	public String getResponseCode() {
		return responseCode;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata", this.responseMetadata);
		obj.put("IndividualResponseSet", this.individualResponseSet);
		return obj.toJSONString();
	}
}
