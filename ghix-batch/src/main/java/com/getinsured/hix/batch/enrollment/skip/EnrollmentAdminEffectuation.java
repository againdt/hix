package com.getinsured.hix.batch.enrollment.skip;

import org.springframework.stereotype.Component;

@Component("enrollmentAdminEffectuation")
public class EnrollmentAdminEffectuation {
	
	private String wipFolderPath;
	private String wipFolderName;

	public String getWipFolderPath() {
		return wipFolderPath;
	}

	public void setWipFolderPath(String wipFolderPath) {
		this.wipFolderPath = wipFolderPath;
	}

	public String getWipFolderName() {
		return wipFolderName;
	}

	public void setWipFolderName(String wipFolderName) {
		this.wipFolderName = wipFolderName;
	}

}
