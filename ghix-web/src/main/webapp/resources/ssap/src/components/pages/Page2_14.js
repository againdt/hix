import React, { Fragment, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


// Components

import PageHeader from '../headers/PageHeader';
import FamilyAndHouseholdSummary_NV from '../summary/FamilyAndHouseholdSummary_NV';
import FamilyAndHouseholdSummary_MN from '../summary/FamilyAndHouseholdSummary_MN';

// ActionCreators

import { fetchTribes } from '../../actions/actionCreators';

// Constants

import CONFIG from '../../.env';
import ENDPOINTS from '../../constants/endPointsList';





/**
 *
 * @param {Object} props
 * @returns {React.Component} - Summary page for 'Family and Household' Section
 *
 */
const Page2_14  = props => {

    const { householdMembers } = props;

    const {
        buttonContainer,
        flow: {
            flowType
        },
        PAGES,
        stateCode,
        tribes
    } = props;
    const isMN = stateCode === 'MN';
    const isNV = stateCode !== 'MN';

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            tribes.length === 0 && props.actions.fetchTribes(`${window.location.origin}${ENDPOINTS.TRIBES_ENDPOINT}`);
        }
    }, []);

    return (
        <Fragment>
            <div id="fhSummary" className="fade-in ">
                <div className="subsection">
                    <PageHeader
                        text="Review and Confirm"
                        headerClassName={'usa-heading-alt'}
                    />
                    <div className="usa-width-one-whole">
                        <div className="usa-alert usa-alert-info alert--info alert__review">
                            <div className="usa-alert-body" >
                                <p className="usa-alert-text">
                                    {
                                        `Here is the information you provided about everyone who is part of your household.
                                            Please take a moment to review and double-check the information.
                                            If you see any mistakes, please edit them now.`
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                    <div  className="usa-width-one-whole">
                        {isNV &&
                        <FamilyAndHouseholdSummary_NV
                            {...{
                                buttonContainer,
                                householdMembers,
                                flowType,
                                stateCode,
                                PAGES,
                                tribes
                            }}
                        />
                        }
                        {isMN &&
                        <FamilyAndHouseholdSummary_MN
                            {...{
                                buttonContainer,
                                householdMembers,
                                flowType,
                                stateCode,
                                PAGES
                            }}
                        />
                        }
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    flow: state.flow,
    householdMembers: state.data.singleStreamlinedApplication.taxHousehold[0].householdMember,
    stateCode: state.headerLinks.stateCode,
    tribes: state.tribes
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        fetchTribes
    }, dispatch)
});

Page2_14.defaultProps = {
    name: 'Page2_14'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_14);
