package com.getinsured.iex.hub.mec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.hix._0_1.hix_core.ResponseMetadataType;

public class ResponseMetadataWrapper implements JSONAware{
	private String responseDescription;
	private String responseCode;

	public ResponseMetadataWrapper(ResponseMetadataType responseType){
		this.responseCode = responseType.getResponseCode().getValue();
		this.responseDescription = responseType.getResponseDescriptionText().getValue();
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public String getResponseCode() {
		return responseCode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescription", this.responseDescription);
		return obj.toJSONString();		
	}

}
