<#assign copyright><@spring.messageText code="copyright" text=copyright!"Getinsured Health Benefit Exchange."/></#assign>
<#assign company_url><@spring.messageText code="company.contact.url" text=companyContactUrl!"http://www.springsource.com/web/guest/contact"/></#assign>
<#assign company_contact><@spring.messageText code="company.contact" text=companyContact!"Contact SpringSource"/></#assign>
<div id="footer-wrapper">
	<div id="footer-left">
		&copy; ${copyright}
	</div>
</div> <!-- /footer-wrapper -->
