package com.getinsured.iex.dto;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SSAP_VERIFICATION_GIWSPAYLOAD database table.
 * 
 */
public class SsapVerificationGiwspayload implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private java.math.BigDecimal giWsPayloadId;

	private SsapVerification ssapVerification;

	public SsapVerificationGiwspayload() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.math.BigDecimal getGiWsPayloadId() {
		return this.giWsPayloadId;
	}

	public void setGiWsPayloadId(java.math.BigDecimal giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public SsapVerification getSsapVerification() {
		return this.ssapVerification;
	}

	public void setSsapVerification(SsapVerification ssapVerification) {
		this.ssapVerification = ssapVerification;
	}

}