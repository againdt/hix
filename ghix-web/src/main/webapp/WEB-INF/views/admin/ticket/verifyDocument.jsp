<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style type="text/css">
#documentVerifyForm{
margin:0px !important;

}
.modal-body {
	overflow-y: inherit !important;
} 
.form-horizontal {
	margin-left: 0px !important;
} 
textarea {
    width: 300px;
}
#documentType{
    width:200px
}

</style>
   <form class="form-horizontal" id="documentVerifyForm" name="documentVerifyForm" enctype="multipart/form-data" action="<c:url value='/ticketmgmt/ticket/verifydocument'/>" method="POST">
   <df:csrfToken/>	 
	   <div id="uploadDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="uploadDialog" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
		 <div class="uploadDialogHeader modal-header">
			<div class="header">
				<h4 class="pull-left">
					<spring:message code='header.documentUpload' />
				</h4>
				<button aria-hidden="true" data-dismiss="modal" id="crossClose" 
					class="dialogClose clearForm" title="x" type="button">
					<spring:message code="btn.crossClose" />
				</button>
			</div>
		 </div>
         <div class="modal-body">
			<div  class="control-group">
				<label class='control-label'><spring:message code="label.file_upload" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/><span aria-label="Required!"></span></label>
				  <div class="controls">
					 <input type="file" name="files[]" id="fileupload" class="input-file" aria-label="Press Tab and then Space bar to open the choose dialog box." role="">&nbsp;
						<!-- The table listing the files available for attaching/removing -->
						<div class="files" id="upload_files_container"></div>
						<!-- File upload plugin ends -->
					  <div id="fileupload_error"></div>
				  </div>
			</div>
			<div class="control-group">
				<label class='control-label'><spring:message code="doc_Type.label" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /><a><span aria-label="Required!"></span></a></label>
				 <div class="controls">
						<select class="input-medium" name="documentType" id="documentType">	</select>
						 <div id="documentType_error"></div>		
				 </div>
			</div>
			<div class="control-group">
			    <label class='control-label'><spring:message code="label.file_description" /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /><a><span aria-label="Required!"></span></a></label>
					<div class="controls">						
						<textarea maxlength="100" cols="20" rows="5" class="xlarge" id="comments" name="comments"></textarea>	
						<div id="comments_error"></div>				
					</div>
			</div>

			</div>
		    <div id="documentUploadloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		       <img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
            </div>
		     <div class="modal-footer clearfix">
				<button class="btn pull-left clearForm" data-dismiss="modal" aria-hidden="true" onclick="javascript:cancelDocument();" id="cancelUpload"> <spring:message code='button.cancel' />
				</button>
				<a class="btn btn-primary pull-right" id="saveTicketBtn" onclick="javascript:submitDocument();"><spring:message code='button_tkt.submit' /></a>
				</button>
			</div> 	
			
	   </div>		
  </form>	

<script  type="text/javascript" >


function submitDocument() {
	var fup = document.getElementById('fileupload');
	var fileName = fup.value;
	var comments = document.getElementById('comments');
	var comment = comments.value;
	 $("#documentUploadloader").show(); 
	if (fileName == null || fileName == "" ) {
		 $("#documentUploadloader").hide(); 
		alert("Please select file to upload");
	} else {
		$("#crossClose").attr("disabled",true); 	
		$("#cancelUpload").attr("disabled",true); 	
		$("#saveTicketBtn").attr("disabled",true); 	
		$('#documentVerifyForm').append($('#uploadDialog'));
		$('#documentVerifyForm').submit();
	}
	if(comment == null || comment == "")
		{
		 $("#documentUploadloader").hide(); 
		 $("#crossClose").attr("disabled",false); 	
		 $("#cancelUpload").attr("disabled",false); 	
		 $("#saveTicketBtn").attr("disabled",false); 	
		}
}

function checkDocumentType() {

	$("#documentType").empty();
	$("#documentType_error").val("");
	
	var validateUrl = '<c:url value="/ticketmgmt/ticket/type" ></c:url>';
		var ticketType = "Document Verification";

	$.ajax({
		${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
		url : validateUrl,
		type : "POST",
		data : {
			ticketType : ticketType
		},
		success : function(response, xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			if (response == "") {

			} else {
				populateDocumentType(response);
			}
		},
		error : function(e) {
			alert("Failed to Add subtype");
		},
	});
}

function populateDocumentType(subListJSON) {
	subListJSON = JSON.parse(subListJSON);
	for ( var i = 0; i < subListJSON.length; i++) {
		var obj = subListJSON[i];
		$('#documentType').append(
				$("<option></option>").attr("value", obj).text(obj));
	}
}

var validator = $("#documentVerifyForm")
		.validate(
				{
					rules : {
						comments : {
							required : true,
							maxlength : 2000
						}

					},
					messages : {
						comments : {
							required : "<span> <em class='excl'>!</em><spring:message code='label.validateDescription' javaScriptEscape='true'/></span>",
							maxlength : "<span> <em class='excl'>!</em><spring:message code='label.descriptionMaxLen' javaScriptEscape='true'/></span>"
						},
					},
					errorClass : "error",
					errorPlacement : function(error, element) {
						var elementId = element.attr('id');
						if ($("#" + elementId + "_error").html() != null) {
							$("#" + elementId + "_error").html('');
						}
						error.appendTo($("#" + elementId + "_error"));
						$("#" + elementId + "_error")
								.attr('class', 'error');
					}
				});
</script>