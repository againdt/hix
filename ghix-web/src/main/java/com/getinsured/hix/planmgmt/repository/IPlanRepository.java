package com.getinsured.hix.planmgmt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Plan;

public interface IPlanRepository extends JpaRepository<Plan, Integer>, RevisionRepository<Plan, Integer, Integer> {
	@Query("SELECT p.id, p.name FROM Plan p WHERE LOWER(p.insuranceType) = LOWER(:planType) AND isDeleted='N' ORDER BY p.name")
    List<Object[]> findPlanNamesByType(@Param("planType") String planType);
    
    @Query("SELECT DISTINCT p.insuranceType FROM Plan p WHERE isDeleted='N' ORDER BY p.insuranceType")
    List<String> distinctPlanTypes();
    
    @Query("SELECT p FROM Plan p WHERE (p.creationTimestamp BETWEEN :startdatetime AND :enddatetime) OR (p.lastUpdateTimestamp between :startdatetime AND :enddatetime) AND isDeleted='N' ORDER BY p.id")
    List<Plan> getAddedOrUpdatedPlans(@Param("startdatetime") Date startdatetime, @Param("enddatetime") Date enddatetime );

    @Query("SELECT count(p.id) FROM Plan p WHERE p.issuer.id=:issuerId and LOWER(p.name)=LOWER(:name) AND isDeleted='N'")
    Long getMatchingPlanCount(@Param("issuerId") int issuerId, @Param("name") String name);
    
    @Modifying
    @Transactional
    @Query("UPDATE Plan as p SET status='DECERTIFIED' WHERE p.issuer.id=:issuerId AND isDeleted='N'")
    void decertifyPlans(@Param("issuerId") int issuerId);
    
    @Query("SELECT insuranceType FROM Plan p WHERE p.id=:planId AND isDeleted='N'")
    String getInsuranceType(@Param("planId") int planId);

    @Query("SELECT p FROM Plan p WHERE p.issuer.id=:issuerID and p.issuerPlanNumber=:issuerPlanNumber AND applicableYear = :applicableYear AND isDeleted='N'")
    Plan getRequiredPlan(@Param("issuerID") int issuerID,@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);

    @Query("SELECT p.name FROM Plan p where p.issuer.id=:issuerId AND isDeleted='N' order by name")
    List<String> getAllPlanName(@Param("issuerId") int issuerId);

    @Query("SELECT p FROM Plan p WHERE p.market=:market AND isDeleted='N' AND p.issuer.id=:issuerId AND p.id IN :planIdList ")
    List<Plan> getPlanByIDMarket(@Param("planIdList") List<Integer> planIdList, @Param("issuerId") int issuerId, @Param("market") String market);

    @Query("SELECT p FROM Plan p WHERE p.market=:market AND isDeleted='N' AND p.id IN :planIdList ")
    List<Plan> getPlanByIDMarket(@Param("planIdList") List<Integer> planIdList, @Param("market") String market);

    @Query("SELECT p FROM Plan p WHERE p.id=:planid and p.market=:market AND isDeleted='N'")
    Plan getPlanByIDMarket(@Param("planid") int planid,@Param("market") String market);

    @Query("SELECT p FROM Plan p WHERE p.issuerPlanNumber=:HIOSID AND isDeleted='N'")
    List<Plan> getPlanByHIOSID(@Param("HIOSID") String HIOSID);
    
    @Query("SELECT p FROM Plan p where p.issuer.id=:issuerId AND isDeleted='N' order by issuerPlanNumber")
    List<Plan> getAllPlansByIssuer(@Param("issuerId") int issuerId);
    
    @Query("SELECT distinct(p.insuranceType) FROM Plan p where p.id IN (:planIdList) AND isDeleted='N'")
    String getInsuranceTypeForPlanList(@Param("planIdList") List<Integer> planIdList);
    
    @Query("SELECT p FROM Plan p where p.id IN (:planIdList)")
    List<Plan> getPlans(@Param("planIdList") List<Integer> planIdList);
    
    @Query("SELECT p FROM Plan p WHERE p.issuer.id=:issuerId and p.id=:planId AND isDeleted='N'")
    Plan findPlanByIdAndIssuerId(@Param("planId") int planId, @Param("issuerId") int issuerId);
    
    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.status =:status, p.lastUpdatedBy =:lastUpdatedBy, certifiedby =:lastUpdatedBy, certifiedOn =CURRENT_TIMESTAMP(), lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
			+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateCertificationStatusQHPPlans(@Param("status") String status, 
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, 
    		@Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
            
    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.status =:status, p.lastUpdatedBy =:lastUpdatedBy, certifiedby =:lastUpdatedBy, certifiedOn =CURRENT_TIMESTAMP(), p.issuerVerificationStatus ='PENDING' , lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateCertificationStatusAndVerificationStatusQHPPlans(@Param("status") String status, 
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, 
    		@Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
    
    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.enrollmentAvail =:enrollmentAvail, p.enrollmentAvailEffDate =:erlAvailEffDate,  "
    		+ " p.futureEnrollmentAvail = '', p.futureErlAvailEffDate = null, p.lastUpdatedBy =:lastUpdatedBy , lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
    		+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateEnrollmentAvblQHPPlans(@Param("enrollmentAvail") String enrollmentAvail,
    		@Param("erlAvailEffDate") Date erlAvailEffDate, @Param("lastUpdatedBy") int lastUpdatedBy, 
    		@Param("insuranceType") String insuranceType, @Param("issuerPlanNumber") String issuerPlanNumber, 
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);

    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.futureEnrollmentAvail =:futureEnrollmentAvail, p.futureErlAvailEffDate =:futureErlAvailEffDate, p.lastUpdatedBy =:lastUpdatedBy, lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
    		+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateFutureEnrollmentAvblQHPPlans(@Param("futureEnrollmentAvail") String futureEnrollmentAvail,
    		@Param("futureErlAvailEffDate") Date futureErlAvailEffDate, @Param("lastUpdatedBy") int lastUpdatedBy, 
    		@Param("insuranceType") String insuranceType, @Param("issuerPlanNumber") String issuerPlanNumber, 
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
    
    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.status =:status, p.lastUpdatedBy =:lastUpdatedBy, certifiedby =:lastUpdatedBy, certifiedOn =CURRENT_TIMESTAMP(), lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "		    		
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
    		+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateCertificationStatusQDPPlans(@Param("status") String status, 
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus,
    		@Param("verified") String verified, @Param("planYear") int applicableYear,  
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
    
    @Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.status =:status, p.lastUpdatedBy =:lastUpdatedBy, certifiedby =:lastUpdatedBy, certifiedOn =CURRENT_TIMESTAMP(), p.issuerVerificationStatus ='PENDING', lastUpdateTimestamp =CURRENT_TIMESTAMP()"
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "		    		
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
			+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateCertificationStatusAndVerificationStatusQDPPlans(@Param("status") String status, 
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus,
    		@Param("verified") String verified, @Param("planYear") int applicableYear,  
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
    
	@Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.enrollmentAvail =:enrollmentAvail, p.enrollmentAvailEffDate =:erlAvailEffDate,  "
    		+ " p.futureEnrollmentAvail = '', p.futureErlAvailEffDate = null, p.lastUpdatedBy =:lastUpdatedBy, lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "		    		
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
    		+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateEnrollmentAvblQDPPlans(@Param("enrollmentAvail") String enrollmentAvail,
    		@Param("erlAvailEffDate") Date erlAvailEffDate, @Param("lastUpdatedBy") int lastUpdatedBy, 
    		@Param("insuranceType") String insuranceType,@Param("issuerPlanNumber") String issuerPlanNumber, 
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus,
    		@Param("verified") String verified, @Param("planYear") int applicableYear,  
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
	
	@Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.futureEnrollmentAvail =:futureEnrollmentAvail, p.futureErlAvailEffDate =:futureErlAvailEffDate, p.lastUpdatedBy =:lastUpdatedBy, lastUpdateTimestamp =CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "		    		
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
    		+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
    		+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null)")
    int bulkUpdateFutureEnrollmentAvblQDPPlans(@Param("futureEnrollmentAvail") String futureEnrollmentAvail,
    		@Param("futureErlAvailEffDate") Date futureErlAvailEffDate, @Param("lastUpdatedBy") int lastUpdatedBy, 
    		@Param("insuranceType") String insuranceType,@Param("issuerPlanNumber") String issuerPlanNumber, 
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus,
    		@Param("verified") String verified, @Param("planYear") int applicableYear,  
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
	
	
	@Modifying
    @Transactional
    @Query("SELECT count(p.id), p.issuer.hiosIssuerId, p.id, ph.planLevel, p.market, p.name, p.networkType, p.creationTimestamp, p.lastUpdateTimestamp, "
    		+ " p.insuranceType, p.startDate, p.endDate, p.issuerPlanNumber, p.applicableYear, p.issuer.id "
    		+ " FROM "
    		+ " Plan p, PlanHealth ph "
    		+ " WHERE ph.plan.id = p.id AND ( p.isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"    		
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
			+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null) "
			+ " GROUP BY (p.issuer.hiosIssuerId, p.id, ph.planLevel, p.market, p.name, p.networkType, p.creationTimestamp, p.lastUpdateTimestamp, p.insuranceType, p.startDate, p.endDate, p.issuerPlanNumber, p.applicableYear, p.issuer.id ) ")
    List<String[]> getBulkUpdatedQHPPlanList(@Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, 
    		@Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
	
	@Modifying
    @Transactional
    @Query("SELECT count(p.id), p.issuer.hiosIssuerId, p.id, pd.planLevel, p.market, p.name, p.networkType, p.creationTimestamp, p.lastUpdateTimestamp, "
    		+ " p.insuranceType, p.startDate, p.endDate, p.issuerPlanNumber, p.applicableYear, p.issuer.id "
    		+ " FROM "
    		+ " Plan p, PlanDental pd "
    		+ " WHERE pd.plan.id = p.id AND ( p.isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE '%' || UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"    		
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
			+ " AND p.status <> 'DECERTIFIED' AND (p.deCertificationEffDate is null) "
			+ " GROUP BY (p.issuer.hiosIssuerId, p.id, pd.planLevel, p.market, p.name, p.networkType, p.creationTimestamp, p.lastUpdateTimestamp, p.insuranceType, p.startDate, p.endDate, p.issuerPlanNumber, p.applicableYear, p.issuer.id ) ")
	List<String[]> getBulkUpdatedQDPPlanList(@Param("insuranceType") String insuranceType, 
    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, 
    		@Param("filterStatus") String filterStatus, 
    		@Param("verified") String verified, @Param("planYear") int applicableYear, 
    		@Param("planLevel") String planLevel, @Param("issuerId") int issuerId);
	
	@Query("SELECT p FROM Plan p where p.id IN (:planIdList)")
    List<Plan> getPlansList(@Param("planIdList") List<Integer> planIdList);
	
	@Query("SELECT p FROM Plan p WHERE p.issuer.id=:issuerId AND p.issuerPlanNumber=:HIOSID AND isDeleted='N'")
	List<Plan> getPlanByIssuerIdandHIOSID(@Param("issuerId") int issuerId, @Param("HIOSID") String HIOSID);
}


