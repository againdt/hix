package com.getinsured.eligibility.redetermination.service;

import java.util.Map;
import java.util.Set;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderRequestDTO;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderResponseDTO;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface RenewalApplicationService {
	
	RenewalApplication saveRenewalApplication(RenewalApplication renewalApplication);
	
	Map<String,Object> getRenewalApplicationStatus(Long ssapApplicationId) throws GIException, Exception;
	
	Set<String> dependentDentalAgeCheck(SsapApplication ssapApplication,SsapApplicant ssapApplicant,Set<String> dentalEnrolledMembers,Long renewalYear,String planId) throws GIException;

	RenewalStatusNotToConsiderResponseDTO setNotToConsiderRenewalStatusByApplIdAndPlanType(RenewalStatusNotToConsiderRequestDTO requestDTO);
	
	void markRenewalApplicationClose(long ssapApplicationId);
	
	void updateRenewalStatus(String plantType,Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException;
	
	RenewalApplication findBySsapApplicationId(Long ssapApplicationId);
	
	RenewalStatus getRenewalStatus (String enrlRenewalFlag,RenewalStatus renewalStatus);
}
