package com.getinsured.hix.batch.enrollment.terminator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.partitioner.EnrollmentUpdateEHBPremiumPartitioner;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentEHBUpdation;
import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class TerminateEnrollmentUpdateEHBPremiumJob implements Tasklet{
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private EnrollmentEHBUpdation enrollmentEHBUpdation;
	
	private String skipFilePath;
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentUpdateEHBPremiumJob.class);
	/**
	 * @return the enrollmentEmailUtils
	 */
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	

	/**
	 * @param enrollmentEmailUtils the enrollmentEmailUtils to set
	 */
	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}


	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		BufferedWriter bufferedWriter=null;
		FileWriter writer=null;
		try{
				if( enrollmentEHBUpdation!=null && enrollmentEHBUpdation.getSkippedEnrollmentIdList()!=null && enrollmentEHBUpdation.getSkippedEnrollmentIdList().size()>0 && skipFilePath!=null && !skipFilePath.isEmpty()){
					
					//write code to write to the file path
					String skipFile= skipFilePath.trim()+"EnrollmentEHBUpdateSkippedRecords_"+chunkContext.getStepContext().getStepExecution().getJobExecution().getCreateTime().getTime()+".txt";
					File skipFolder = new File(skipFilePath.trim());
					if(skipFolder.exists()){
						 writer = new FileWriter(skipFile);
						 bufferedWriter= new BufferedWriter(writer,EnrollmentConstants.BUFFER_SIZE);
						bufferedWriter.write(
								StringUtils.join(enrollmentEHBUpdation.getSkippedEnrollmentIdList(), ",")
								);
						bufferedWriter.flush();
						
					}
				}
				
				
		}catch(Exception e){
			LOGGER.error("updateEHBPremiumJob failed for enrollment ids :: "+ enrollmentEHBUpdation.getSkippedEnrollmentIdList());
		}
		finally{
			if(enrollmentEHBUpdation!=null ){
			enrollmentEHBUpdation.clearSkippedEnrollmentIdList();
			enrollmentEHBUpdation.clearEnrollmentIdList();
			}
			if(bufferedWriter!=null){
				bufferedWriter.close();
			}
			if(writer!=null){
				writer.close();
			}
		}
		
		
		return RepeatStatus.FINISHED;
	}



	public EnrollmentEHBUpdation getEnrollmentEHBUpdation() {
		return enrollmentEHBUpdation;
	}



	public void setEnrollmentEHBUpdation(EnrollmentEHBUpdation enrollmentEHBUpdation) {
		this.enrollmentEHBUpdation = enrollmentEHBUpdation;
	}



	public String getSkipFilePath() {
		return skipFilePath;
	}



	public void setSkipFilePath(String skipFilePath) {
		this.skipFilePath = skipFilePath;
	}

	
	
}
