<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

<style type="text/css">
	.table tr th{
		background: none !important;
		height: 14px;
		font-weight: normal;
		color: #5A5A5A;
	}
</style>

<div class="gutter10">


	<div class="l-page-breadcrumb hide">
	<!--start page-breadcrumb -->
	<div class="row-fluid">
		<ul class="page-breadcrumb">
		<c:choose>          
          	<c:when test="${loggedUser =='entityAdmin' }">  
			<li><a href="#">&lt; <spring:message code="label.entity.back"/></a></li>
			<li><a href="<c:url value="/entity/entityadmin/managelist"/>"><spring:message code="label.entity.entities"/></a></li>
			<li>${enrollmentEntity.entityName}</li>
			 </c:when>
            <c:otherwise>
            </c:otherwise>
           </c:choose>
		</ul>
		<!--page-breadcrumb ends-->
	</div><!-- end of .row-fluid -->
	</div><!--l-page-breadcrumb ends-->
	
<div class="row-fluid">
	<c:if test="${loggedUser =='entityAdmin'}">
	<div id="titlebar">
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>
</div>
<div class="row-fluid">
	 <!-- #sidebar -->      
<jsp:include page="../leftNavigationMenu.jsp" />
<!-- .span3 --> 
<!-- #sidebar ENDS -->   
<div class="span9" id="rightpanel">
	<div class="header">
			<h4 class="pull-left"><spring:message code="label.entity.entityInformation"/></h4>			
			<c:choose>          
	          	<c:when test="${loggedUser == 'entityAdmin' && enrollmentEntity.registrationStatus != 'Incomplete'}">  
	          			   <c:set var="enrollmentEntityUserId" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/> </c:set>      
	                      <a class="btn btn-small pull-right" href="<c:url value="/entity/entityadmin/entityinformation/${enrollmentEntityUserId}"/>"><spring:message  code="label.edit"/></a>
	            </c:when>
	       		<c:when test="${CA_STATE_CODE}">
	       			<c:if test="${loggedUser != 'entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
	       				<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entityinformation"/>"><spring:message  code="label.edit"/></a>
	       			</c:if>
				</c:when>
				<c:otherwise>
					<c:choose> 
						<c:when test="${loggedUser != 'entityAdmin' && (enrollmentEntity.registrationStatus != 'Incomplete' && enrollmentEntity.registrationStatus != 'Pending')}">
			            	<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entityinformation?editEntity=true"/>"><spring:message  code="label.edit"/></a>
			            </c:when>
			            <c:otherwise>
			            	<c:if test="${loggedUser != 'entityAdmin' && enrollmentEntity.registrationStatus == 'Incomplete'}">
			            		<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entityinformation"/>"><spring:message  code="label.edit"/></a>
			            	</c:if>
			            </c:otherwise>
		            </c:choose>   
				</c:otherwise>	            		
         	</c:choose>
 	</div>
<form class="form-vertical" id="frmviewentityinformation" name="frmviewentityinformation" action="/entity/enrollmententity/getPopulationServed" method="GET">
  
 <input type="hidden" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/> "/>
<div class="gutter10">
	<table class="table table-border-none">
		<tbody>
			<tr>
               <td class="span4 txt-right" scope="row"><spring:message code="label.entity.entityNumber"/></td>
               <td><strong>${enrollmentEntity.entityNumber}</strong></td>
 			</tr>
 			
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.entityType"/></td>
			   <td>
				   <strong>
				       <c:choose>
				         <c:when test="${enrollmentEntity.entityType=='In-Person Assistance'}">
				           <spring:message  code="label.entity.inpersonassistance"/>
				         </c:when>
				         <c:when test="${enrollmentEntity.entityType=='Navigation Organization'}">
				           <spring:message  code="label.entity.navigatorOrganization"/>
				         </c:when>
				         <c:otherwise>
				            <spring:message  code="label.entity.certifiedApplicationCounselor"/>
				         </c:otherwise>
				      </c:choose>
				   </strong>
			   </td>
		 </tr>
		 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.enrollmentEntityName"/></td>
		   <td><strong>${enrollmentEntity.entityName}</strong></td>
		 </tr>
		 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.adminBusinessLegalName"/></td>
		   <td><strong>${enrollmentEntity.businessLegalName}</strong></td>
		 </tr>
 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.primaryEmailAddress"/></td>
		   <td><strong>${enrollmentEntity.primaryEmailAddress}</strong></td>
		 </tr>
 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.primaryPhoneNumber"/></td>
		   <td><strong>
		     <c:choose>
		       <c:when test="${enrollmentEntity.primaryPhoneNumber!=null}">
		         <c:set var="primaryPhoneNumber" value="${enrollmentEntity.primaryPhoneNumber}"></c:set>
		         <c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)})${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
		          ${formattedPrimaryPhoneNumber}
		       </c:when>
		       <c:otherwise>
		         ${enrollmentEntity.primaryPhoneNumber}
		       </c:otherwise>
		     </c:choose>
		   <%-- ${enrollmentEntity.primaryPhoneNumber} --%></strong></td>
		 </tr>
 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.secondaryPhoneNumber"/></td>
		   <td>
		   <strong>
		       <c:choose>
			       <c:when test="${enrollmentEntity.secondaryPhoneNumber!=null && not empty enrollmentEntity.secondaryPhoneNumber}">
			         <c:set var="secondaryPhoneNumber" value="${enrollmentEntity.secondaryPhoneNumber}"></c:set>
			         <c:set var="formattedSecondaryPhoneNumber" value="(${fn:substring(secondaryPhoneNumber,0,3)})${fn:substring(secondaryPhoneNumber,3,6)}-${fn:substring(secondaryPhoneNumber,6,10)} "></c:set>
			          ${formattedSecondaryPhoneNumber}
			       </c:when>
			       <c:otherwise>
			         ${enrollmentEntity.secondaryPhoneNumber}
			       </c:otherwise>
		       </c:choose>
		       </strong></td>
		 </tr>
		 <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.finaceFaxNumber"/></td>
		   <td>
		   	<strong>
		   		<c:choose>
		   			<c:when test="${enrollmentEntity.faxNumber!=null && not empty enrollmentEntity.secondaryPhoneNumber}">
		   				<c:set var="faxNumber" value="${enrollmentEntity.faxNumber}"></c:set>
		   				<c:set var="formattedFaxNumber" value="(${fn:substring(faxNumber,0,3)})${fn:substring(faxNumber,3,6)}-${fn:substring(faxNumber,6,10)} "></c:set>
		   			${formattedFaxNumber}
		   			</c:when>
		   			<c:otherwise>
		   				${enrollmentEntity.faxNumber}
		   			</c:otherwise>
		   		</c:choose>
		   	</strong>
		   </td>
		 </tr>
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.howWouldthisPersonLiketobeContacted"/></td>
		   <td><strong>${enrollmentEntity.communicationPreference}</strong></td>
		 </tr>
		 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.federalTaxID"/></td>
		   <td><strong>${enrollmentEntity.federalTaxID}</strong></td>
		 </tr>
		 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.stateTaxID"/></td>
		   <td><strong>${enrollmentEntity.stateTaxID}</strong></td>
		 </tr>
 
		  <tr>
		   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.organizationType"/></td>
		   <td><strong>${enrollmentEntity.orgType}</strong></td>
		 </tr>    
		            
		  <tr>
		   <td class="span4 txt-right comma" scope="row"><spring:message code="label.entity.countiesYouServe"/></td>
		   <td><strong class="comma">${enrollmentEntity.countiesServed}</strong></td>
		 </tr>
		 
		
		 <tr>
		<td class="span4 txt-right"><spring:message code="label.entity.didYourOrganizationReceiveAnOutReachndEducationGrant"/></td>                    
		<td>
		<strong>
		<c:choose>
		<c:when test="${enrollmentEntity.grantContractNo != null || enrollmentEntity.grantAwardAmount != null}">											
		<spring:message code="label.entity.yes"/>
		</c:when>
		<c:otherwise>
		<spring:message code="label.entity.no"/>
		</c:otherwise>
		</c:choose>  
		</strong> 
		</td>
		</tr>

<c:choose>
<c:when test="${enrollmentEntity.grantContractNo != null || enrollmentEntity.grantAwardAmount != null}">
<tr>
   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.grantContractNumber"/></td>
   <td><strong>${enrollmentEntity.grantContractNo}</strong></td>
 </tr>
 
 <tr>
   <td class="span4 txt-right" scope="row"><spring:message code="label.entity.grantAwardAmount"/></td>
   <td><strong>${enrollmentEntity.grantAwardAmount}</strong></td>
 </tr>
 </c:when>
<c:otherwise>
</c:otherwise>
</c:choose>   
</tbody>
</table>
</div><!-- end of .gutter10 -->	
			<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus=='Incomplete'}">
				<div class="form-actions">
					<a class="btn btn-primary offset8" id="label.entity.next" onClick="window.location.href='/hix/entity/enrollmententity/getPopulationServed'"><spring:message code="label.entity.next"/></a>
				</div>
			</c:if>			
</form>
		</div>
	</div>
</div>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	
<script type="text/javascript">


$(document).ready(function() {
	$(".comma").text(function(i, val) {
	    return val.replace(/,/g, ", ");
	});
	
	$("#entityInformation").removeClass("link");
	$("#entityInformation").addClass("active");
	
	//$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
	

});
</script>

<script>
$(".comma").text(function(i, val) {
    return val.replace(/,/g, ", ");
});
</script>
