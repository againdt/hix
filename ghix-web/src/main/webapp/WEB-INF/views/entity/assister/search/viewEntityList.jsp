<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<%-- <link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />


<script type="text/javascript"	src="/hix/resources/js/jquery-ui.min.js"></script>
<script type="text/javascript"	src="/hix/resources/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/js/preserveImgAspectRatio.js'/>"></script>
<script type="text/javascript" src="/hix/resources/js/gi.responsive-table.js" ></script>

<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:if test='${stateCode == "CA"}'>
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>


<style>
	body {
		background-image: none;
		padding-top: 0;
	}
	.break-word {
    word-wrap: break-word;
}
table#entitySearchResult {
    table-layout: fixed;
    width: 100%;
}
</style>
	<div class="row-fluid" id="search_results">
		<div class="gutter10">
			<div class="row-fluid result-count-header">
				<h1>
				<c:choose>
							<c:when test="${totalResultCount > 1}">
								${totalResultCount} <spring:message code="label.assister.organizationsfound"/>
							</c:when>
							<c:otherwise>
								${totalResultCount} <spring:message code="label.assister.organizationsfoundsingular"/>
							</c:otherwise>
				</c:choose>

					<span class="search pull-right">
						<a class="btn btn-primary btn-small" id="aid-backBtn" href="/hix/entity/locateassister/searchentities?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}"><spring:message code="label.assister.backbtn"/></a>
					</span>
				</h1>
			</div>
			<div>
				<form id="entityListfrm" name="entityListfrm" action="searchentities" method="post">
					<input type="hidden" name="currentpage" id="currentpage" value="">
					<c:choose>
						<c:when test="${fn:length(entitySearchResult) > 0}">
							<table id="entitySearchResult" class="table table-condense">
								<thead>
									<tr class="header list-header">
										<th><spring:message code="label.assister.assistername"/></th>
										<th><spring:message code="label.assister.contactinfo"/></th>
										<c:if test="${zipCode!=null && zipCode!= ''}">
											<th><spring:message code="label.entity.assister.distance"/></th>
										</c:if>
										<th><spring:message code="label.assister.languages"/></th>
									</tr>
								</thead>
								<c:forEach items="${entitySearchResult}" var="eachentity">
								<c:set var="encrytedEnrollmentEntityid" ><encryptor:enc value="${eachentity.enrollmentEntity.id}" isurl="true"/> </c:set>
								 <c:set var="encrytedSiteId" ><encryptor:enc value="${eachentity.site.id}" isurl="true"/> </c:set>
									<tr>
										<td>
											<a class="aid-entityName" href="<c:url value="/entity/locateassister/entitydetail/${encrytedEnrollmentEntityid}/${encrytedSiteId}?entityName=${entityName}&language=${language}&zipCode=${zipCode}&distance=${distance}"/>">${eachentity.enrollmentEntity.entityName}</a>
										</td>
										<td>
											<c:if test="${eachentity.entityLocation!=null}">
												<c:if test="${eachentity.entityLocation.address1 != null && eachentity.entityLocation.address1 != \"\"}">
													${eachentity.entityLocation.address1}
												</c:if>
												<c:if test="${eachentity.entityLocation.address2 != null && eachentity.entityLocation.address2 != \"\"}">
													<br>${eachentity.entityLocation.address2}
												</c:if>
													<br>${eachentity.entityLocation.city}&nbsp;&nbsp;&nbsp;${eachentity.entityLocation.state}
													<br><c:out value="${eachentity.entityLocation.zip}" />
											</c:if>
											<c:choose>
												<c:when test="${eachentity.enrollmentEntity.primaryPhoneNumber != null && not empty eachentity.enrollmentEntity.primaryPhoneNumber}">
													<c:set var="unformattedPhoneNbr" value="${eachentity.enrollmentEntity.primaryPhoneNumber}"/>
												<c:set var="formattedPhoneNbr" value="${fn:substring(unformattedPhoneNbr, 0, 3)}-${fn:substring(unformattedPhoneNbr, 3, 6)}-${fn:substring(unformattedPhoneNbr, 6, 10)}" />
												<br><c:out value="${formattedPhoneNbr}" />
												</c:when>
												<c:otherwise>
													<br><c:out value="${eachentity.enrollmentEntity.primaryPhoneNumber}" />
												</c:otherwise>
											</c:choose>
											<%-- <br><c:out value="${eachentity.enrollmentEntity.primaryPhoneNumber}" /> --%>
											<br><c:out value="${eachentity.enrollmentEntity.primaryEmailAddress}" />
										</td>
										<c:if test="${zipCode!=null && zipCode!= ''}">
											<td class=""><fmt:formatNumber type="number" minFractionDigits = "1" maxFractionDigits="1" value="${eachentity.distance}" /><spring:message code="label.entity.assister.miles"/> </td>
										</c:if>
										<td class="break-word comma">${eachentity.siteLanguages.spokenLanguages}</td>
									</tr>
								</c:forEach>
							</table>
							<div class="center">
								<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							</div>
						</c:when>
						<c:otherwise>
							<div class="no-records">
									<b><spring:message code="label.assister.recordsnotfound"/></b>
							</div>
						</c:otherwise>
					</c:choose>
				</form>
			</div>
		</div>
	</div>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->
<script type="text/javascript">
$(".comma").text(function(i, val) {
    return val.replace(/,/g, ", ");
});
window.onload = ResponsiveCellHeaders("entitySearchResult");
</script>
