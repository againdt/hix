var haveData = false; /* change to false when the user is logged in */
var firstNameDB = "";
var middleNameDB = "";
var lastNameDB = "";
var emailDB = "";
var dobDB = "";
var phoneDB = "";
var validationCheck = true;
var useJSON = true;
var webJson = "";
var submitForAutoContinue = false;
var mode = "";
var totalApplicants = 0;

var ssapApplicationJson = $('#ssapApplicationJson').val();
var ssapCurrentPageId = $('#ssapCurrentPageId').val();
var ssapApplicationStatus = $('#ssapApplicationStatus').val();//$('#ssapApplicationType').val()
var ssapApplicationType = '';

var dateValidation = true;
var signatureValidation = true;

var applicationStatus = "";


var currentPage = 'appscr51';

var currentPersonIndex = -1;
var brokerCertificationNumber = "";
var brokerName="";
var brokerFirstName = "";
var brokerLastName = "";

var noneOftheseDisabilityIndicator = false;
var noneOftheseAssistanceServices = false;
var noneOftheseAmericanIndianAlaskaNativeIndicator=false;
var noneOftheseEverInFosterCareIndicator=false;
var noneoftheseDeceased = false;
var nonoftheseIsAvailableForHealthCoverage= false;
var noneofthesePregnancy = false;
var applyingForCoverageIndicatorArrCopy = [];
var exchangeName = $('#exchangeName').val();

var pageArr = new Array('appscr51', 'appscr52', 'appscr53', 'appscr54',
		'appscr55', 'appscr57', 'appscr58', 'appscr59A', 'appscr60',
		'appscr61', 'appscr62Part1',  'appscr63', 'appscr64',
		'appscr65', 'appscr66', 'appscrBloodRel','appscr67', 'appscr68', 'appscr69', 'appscr70',
		'appscr71', 'appscr72', 'appscr73', 'appscr74', 'appscr75A',
		'appscr76P1', 'appscr77Part1', 'appscr76P2', 'appscr81B', 'appscr83',
		'appscr84','appscr85', 'appscr87');

var f = new Object();
f.appscr51 = 'Before We Begin';
f.appscr52 = 'Get Ready to Start Your Application';
f.appscr53 = 'Primary Contact Information';
f.appscr54 = 'Application Assistance';
f.appscr55 = 'Applicants';
f.appscr56 = 'How Many are Applying for Health Insurance';
f.appscr57 = 'About Your Household';
f.appscr58 = 'Household Summary';
f.appscr59A = 'Get Ready for Family and Household';
/* f.appscr59B = ' Family & Household'; */

f.appscr60 = 'Tell Us About your Household';

var householdFName = $('#firstName').val();
var householdLName= $('#lastName').val();
var householdName = householdFName + " " + householdLName;

f.appscr61 = 'Personal Information';
f.appscr62Part1 = 'Citizenship/Immigration Status';
f.appscr62Part2 = 'Citizenship/Immigration Status';
f.appscr63 = 'Parent/Caretaker Relatives';
f.appscr64 = 'Ethnicity and Race';
f.appscr65 = 'Other Addresses';
f.appscr66 = 'Additional Information';
f.appscr67 = 'Family and Household Summary';
f.appscr68 = 'Income';
f.appscr69 = 'Expected Annual Income';
f.appscr70 = 'Current Income';
f.appscr71 = 'Current Income Details';
f.appscr72 = 'Current Income Details';
f.appscr73 = 'Income Discrepancies';
f.appscr74 = 'Income Summary';

f.appscr75A = 'Additional Question';

f.appscr76P1 = 'Health Insurance Information';
f.appscr76P2 = 'Health Insurance Information';
f.appscr77Part1 = 'Employer Health Coverage Information';
f.appscr77Part2 = 'Employer Health Coverage Information';
f.appscr81B = 'Medicaid & CHP + Specific Questions';
f.appscr82 = 'Additional Questions Summary';
f.appscr83 = 'Review and Sign';
f.appscr84A = 'Part1 - Review Application';
f.appscr84B = 'Part2 - Review Application';
f.appscr84 = 'Final Application Review';
f.appscr85 = 'Sign and Submit';
f.appscr86 = ' Required Documents';
f.appscrBloodRel = 'Relationships';
f.appscr87 = 'Application Complete';

var tabClass = new Object();
tabClass.appscr51 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr52 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr53 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr54 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr55 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr56 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr57 = "active#NONE#NONE#NONE#NONE";
tabClass.appscr58 = "active#NONE#NONE#NONE#NONE";

tabClass.appscr59A = "visited#active#NONE#NONE#NONE";
tabClass.appscr60 = "visited#active#NONE#NONE#NONE";

tabClass.appscr61 = "visited#active#NONE#NONE#NONE";
tabClass.appscr62Part1 = "visited#active#NONE#NONE#NONE";
tabClass.appscr62Part2 = "visited#active#NONE#NONE#NONE";
tabClass.appscr63 = "visited#active#NONE#NONE#NONE";
tabClass.appscr64 = "visited#active#NONE#NONE#NONE";
tabClass.appscr65 = "visited#active#NONE#NONE#NONE";
tabClass.appscr66 = "visited#active#NONE#NONE#NONE";
tabClass.appscr67 = "visited#active#NONE#NONE#NONE";
tabClass.appscrBloodRel = "visited#active#NONE#NONE#NONE";

tabClass.appscr68 = "visited#visited#active#NONE#NONE";
tabClass.appscr69 = "visited#visited#active#NONE#NONE";
tabClass.appscr70 = "visited#visited#active#NONE#NONE";
tabClass.appscr71 = "visited#visited#active#NONE#NONE";

tabClass.appscr72 = "visited#visited#active#NONE#NONE";
tabClass.appscr73 = "visited#visited#active#NONE#NONE";
tabClass.appscr74 = "visited#visited#active#NONE#NONE";

tabClass.appscr75A = "visited#visited#visited#active#NONE";
tabClass.appscr76P1 = "visited#visited#visited#active#NONE";
tabClass.appscr76P2 = "visited#visited#visited#active#NONE";
tabClass.appscr77Part1 = "visited#visited#visited#active#NONE";
tabClass.appscr77Part2 = "visited#visited#visited#active#NONE";
tabClass.appscr81B = "visited#visited#visited#active#NONE";
tabClass.appscr82 = "visited#visited#visited#active#NONE";
tabClass.appscr83 = "visited#visited#visited#visited#active";
tabClass.appscr84A = "visited#visited#visited#visited#active";
tabClass.appscr84B = "visited#visited#visited#visited#active";
tabClass.appscr84 = "visited#visited#visited#visited#active";
tabClass.appscr85 = "visited#visited#visited#visited#active";
tabClass.appscr86 = "visited#visited#visited#visited#active";
tabClass.appscr87 = "visited#visited#visited#visited#active";

var pageMethod = new Object();
pageMethod.appscr51 = "next();";
pageMethod.appscr52 = "next();";
pageMethod.appscr53 = "saveData53();loadData54();";
pageMethod.appscr54 = "saveData54();loadData55();";
pageMethod.appscr55 = "saveData55();createHousehold();";
pageMethod.appscr56 = "createHousehold()";
pageMethod.appscr57 = "saveData57();";
pageMethod.appscr58 = "next();";
pageMethod.appscr59A = "getSpouse(1);setNonFinancialPageProperty(1);next();";

pageMethod.appscr60 = "continueData60();addHouseHold()";

pageMethod.appscr61 = "continueData61();addNonFinancialHouseHold()";
pageMethod.appscr62Part1 = "continueData62Part1();addNonFinancialHouseHold()";
pageMethod.appscr62Part2 = "continueData62Part2();addNonFinancialHouseHold()";
pageMethod.appscr63 = "continueData63();addNonFinancialHouseHold()";
pageMethod.appscr64 = "continueData64();addNonFinancialHouseHold()";

pageMethod.appscr65 = "continueData65();addSpecialCircumstances();next();";
pageMethod.appscr66 = "continueData66();saveSpecialCircumstances();";
pageMethod.appscr67 = "next();";
pageMethod.appscr68 = "setupPage69();";

pageMethod.appscr69 = "continueData69();addFinancialHouseHold()";
pageMethod.appscr70 = "continueData70();addFinancialHouseHold()";
pageMethod.appscr71 = "continueData71();addFinancialHouseHold()";
pageMethod.appscr72 = "continueData72();addFinancialHouseHold()";
pageMethod.appscr73 = "continueData73();addFinancialHouseHold()";
pageMethod.appscr74 = "next()";
pageMethod.appscr75A = "next(); setNonFinancialPageProperty(1); setHouseHoldData()";
pageMethod.appscr76P1 = "continueData76P1();next()";

pageMethod.appscr77Part1="next()";
pageMethod.appscr76P2 = "continueData76P2();next()";
pageMethod.appscr82 = " next();";
pageMethod.appscr81B = "continueData77Part2();addAdditionalQuetionDataToTable();continueData78(); continueData79();continueData81A();addDataAppscr81ToTable();addQuestionSummery(); ";

pageMethod.appscr83 = "addReviewSummary();";
//pageMethod.appscr84 = "addIncarceratedContent(); caller();setNonFinancialPageProperty(1);";
pageMethod.appscr84 = "caller();";
pageMethod.appscr85 = "saveIncarceratedDetail();";
pageMethod.appscr86 = "setButtons();";
pageMethod.appscrBloodRel = "saveBloodRelationData();";

pageMethod.appscr87 = "goToDashboard();";

function goToDashboard() {
	location.href="indportal";
}

function changeTabCss(strCol) {	
	var classArr = strCol.split('#');
	$('#tab1').removeClass('visited').removeClass('active').removeClass(
			'active').removeClass('active').removeClass('NONE').addClass(
			classArr[0]);
	$('#tab2').removeClass('visited').removeClass('active').removeClass(
			'active').removeClass('active').removeClass('NONE').addClass(
			classArr[1]);
	$('#tab3').removeClass('visited').removeClass('active').removeClass(
			'active').removeClass('active').removeClass('NONE').addClass(
			classArr[2]);
	$('#tab4').removeClass('visited').removeClass('active').removeClass(
			'active').removeClass('active').removeClass('NONE').addClass(
			classArr[3]);
	$('#tab5').removeClass('visited').removeClass('active').removeClass(
			'active').removeClass('active').removeClass('NONE').addClass(
			classArr[4]);
}

function goToPageById(pageId) {

	$('#' + currentPage).hide();
	$('#' + pageId).show();

	currentPage = pageId;
	
	updateProgressBar(currentPage);
	//focus on first element for accessibility
	document.activeElement.blur();
	
	getCurrentPageMethod(currentPage);

	changeTitle(currentPage);
	if ((currentPage == 'appscr73' && ($(
			'input[name=aboutIncomeSpped]:radio:checked').val() == 'yes' || ($(
			'input[name=aboutIncomeSpped]:radio:checked').val() == 'no' && $(
			'input[name=incomeHighOrLow]:radio:checked').val() == 'higher')))) {
		$('#prevBtn').click();
	}
	
	//scroll to top if error message shows 
	$("html, body").animate({ scrollTop: 0 }, "fast");
}

function getCurrentPageMethod(page) {

	var v = 0;
	for (; v < pageArr.length; v++) {
		if (page == pageArr[v]) {
			changeContinue(pageMethod[pageArr[v]]);
			return;
		}
	}
	changeContinue('next();');
}

function getCurrentPageTabCss(page) {
	var v = 0;
	for (; v < pageArr.length; v++) {
		if (page == pageArr[v]) {
			changeTabCss(tabClass[pageArr[v]]);
			return;
		}
	}
}

function getMethod(page) {

	var v = 0;
	for (; v < pageArr.length; v++) {
		if (page == pageArr[v]) {

			changeContinue(pageMethod[pageArr[v]]);
			return;
		}
	}

	changeContinue('next();');
}

function changeContinue(methodName) {
	$('#contBtn').attr("onClick", methodName);

}

function getNextPage() {

	var v = 0;
	for (; v < pageArr.length; v++) {
		if (currentPage == pageArr[v]) {
			return pageArr[v + 1];
		}
	}

	return false;
}

function getPrevPage() {

	var v = 0;
	for (; v < pageArr.length; v++) {		
		if (currentPage == pageArr[v]) {
			return pageArr[v - 1];
		}
	}

	return false;
}

function next() {	
	pageValidation();
	
/*	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}*/
	
	// At last page if user enters wrong name at esignature testbox, the signatureValidation variable will be false. for all other pages it will be true.
	if(!signatureValidation){
		return;
	}
	
	if(!dateValidation){
		
		$('html, body').animate({
			scrollTop : $("#"+currentPage).find('.dobCustomError span').first().offset().top - 250
		}, 'fast');
		return;
	}
	
	//Atleast 1 person should be seeking coverage to continue the application
	if(currentPage == 'appscr58'){
		if(coverageMinimumCheck() == false){
			$('#coverageMinimumAlert').modal();
			return;
		}
	}
	
	/*if(currentPage == 'appscr67' ){
		assignjointTaxFilerSpousePersonId();
	}
	if(currentPage == 'appscr74' ){
		derivehouseHoldIncome();
	}*/
	
	if (currentPage == 'appscr67' || currentPage == 'appscr74' || currentPage == 'appscr82') {	
		editNonFinacial = false;
		saveData();
	}

	if (currentPage == 'appscr84') {
		$("#waiting").hide();		
	}
	
	/* added by shovan */
	if (currentPage == "appscr51") {
		termsCheckboxIsCheck();
	}	
	/* code ends here by @shovan */

	if (currentPage == "appscr51" && validation(currentPage) == false) {
		return false;
	}
	
	if (currentPage == "appscr55" && haveData == false) {
		setValuesToappscr57JSON();
	}
	
	if(currentPage == "appscr65" && haveData == false )
	{		
		addSpecialCircumstances();
		appscr66FromMongodbJson();
		//appscr66FromMongodb();
		//appscr66NoneOfThese();
	}

	if (currentPage == "appscr59B") {
		validationCheck = true;
	}
	
	if (validationCheck == false) {
		return false;
	}
	
	if ((currentPage == "appscr68" || currentPage == "appscr69" || currentPage == "appscr70")  && haveData == false) {
		fillValuesToFields(currentPage);
	}

	if (haveData == true) {
		fillValuesToFields(currentPage);
	}

	var pageId;
	var ssapApplicationObj = webJson.singleStreamlinedApplication;
	if(currentPage == 'appscr67' && ssapApplicationObj.applyingForFinancialAssistanceIndicator==false){
		addReviewSummary();// suneel 05092014
		pageId = 'appscr83';
		//skip ParentOrCaretaker page if under 19 or NFA
	}else if(currentPage == 'appscr62Part1' && (hideParentOrCaretaker() || ssapApplicationType == "NON_FINANCIAL")){
		pageId = 'appscr64';
	}else if(currentPage == 'appscr59A' && ssapApplicationType == "NON_FINANCIAL"){
		pageId = 'appscr61';
		appscr67EditController(nonFinancialHouseHoldDone);
	}else if(currentPage == 'appscr76P1' && !ssapApplicationObj.taxHousehold[0].householdMember[addQuetionsHouseHoldDone-1].healthCoverage.employerWillOfferInsuranceIndicator){
		pageId = 'appscr76P2';
	}else if(currentPage == 'appscr64' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
		//skip other address page if there is only 1 person
		addSpecialCircumstances();
		appscr66FromMongodbJson();
		pageId = 'appscr66';
	}else if(currentPage == 'appscr66' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
		//skip relationship page if there is only 1 person
		updateSingleMemRel();
		showHouseHoldContactSummary();
		pageId = 'appscr67'; //summary page
	}else{
		pageId = getNextPage();
	}

	if (!pageId) {
		return false;
	}



	$('#' + currentPage).hide();
	$('#' + pageId).show();

	getMethod(pageId);
	currentPage = pageId;
		
	updateProgressBar(currentPage);
	
	//focus on first element for accessibility
	document.activeElement.blur();
	
	if (haveData == false && currentPage == "appscr75A" && useJSON == true) {
		setNonFinancialPageProperty(1);
		appscr76FromMongodb();
	}
	backButton(currentPage);
	saveButton(currentPage);
	changeTitle(currentPage);
	getCurrentPageTabCss(currentPage);

	if (currentPage == "appscr54") {
		setFederalAmount400(); 
	}
	
	if (currentPage == "appscrBloodRel") {
		populateDataToRelations();
		//populateRelationship(); 
	}
	
	if (currentPage == "appscr60" && haveData == false) {
		updateAppscr60FromJSON(HouseHoldRepeat);
	}
	
	if (currentPage == "appscr62Part1") {
		appscr61FromJSON(nonFinancialHouseHoldDone);
	}
	
	if (currentPage == "appscr63" && haveData == false) {
		appscr63FromJSON(nonFinancialHouseHoldDone);
	}	
	
	if (currentPage == "appscr64") {
		fillAppscr64(nonFinancialHouseHoldDone);
	}
	
	if (currentPage == "appscr65" && haveData == false) {
		appscr65FromMongodbJSON();
	}
	
	if (currentPage == "appscr67") {
		nonFinancialHouseHoldDone = getNoOfHouseHolds();
	}
	
	if (currentPage == 'appscr68') {
		setNonFinancialPageProperty(1);
		$('#prevBtn').attr('onclick', 'prev();');
	}
	
	if (currentPage == "appscr85"){
		appscr85FromMongodbJSON();
	}
	
	if (currentPage == 'appscr59A') {
		$('.button_ui_design_rote').css('margin-top', '-100px');
	} else {
		$('.button_ui_design_rote').css('margin-top', '');
	}

	//scroll to top if error message shows 
	$("html, body").animate({ scrollTop: 0 }, "fast");


	if (currentPage == "appscr73"
			&& ($('input[name=aboutIncomeSpped]:radio:checked').val() == 'yes' || ($(
					'input[name=aboutIncomeSpped]:radio:checked').val() == 'no' && $(
					'input[name=incomeHighOrLow]:radio:checked').val() == 'higher'))) {
		$('#contBtn').click();
	}

	if (haveData == false && currentPage == "appscr64" && useJSON == true) {
		appscr65FromMongodbJSON();
	}


	if (currentPage == "appscr85"){
		$("#contBtn").text("Submit Application");
		$("#waiting").hide();
	}else if(currentPage == "appscr87"){
		$("#contBtn").text("Go to Dashboard");
		//$("#sidebar").hide();		
		$( "#sidebar" ).children(".header, .nav-list").hide();
		
	}else{
		$("#contBtn").text("Continue");
	}

}

function changeTitle(page) {

	document.title = f[page];

	/*Added by Kunal for adding Dynamic page title*/

	if (currentPage == "appscr60" || currentPage == "appscr61" || currentPage == "appscr62Part1" || currentPage == "appscr62Part2" || 
			currentPage == "appscr63" || currentPage == "appscr64" || currentPage == "appscr69" ||
			currentPage == "appscr70" || currentPage == "appscr71" || currentPage == "appscr72" || currentPage == "appscr86" )
	{
		$('#headerHouseHoldName').show();
		}
	else{
		$('#headerHouseHoldName').hide();
	}

	$('.SSAPpgtitle').text('');
	$('.SSAPpgtitle').append("&nbsp;" + f[page]);
	/*Added by Kunal for adding Dynamic page title ends here*/
}

function prev() {
	
	validationCheck = true;
	dateValidation = true;
	signatureValidation = true;
	$('#'+$("#"+currentPage).find("form").attr("id")).find("div.errorsWrapper").hide();
	if (currentPage == "appscr52" || currentPage == "appscr53")
		validationCheck = true;

	if (currentPage == "appscr54")
		backData53();

	if (currentPage == "appscr55"){
		backData54();
		checkFinacialAssistance();
	}

	if (currentPage == "appscr57") {
		backData55();
		deleteHouseholdForm();
	}

	if (currentPage == "appscr58"){
		backData57();
		setValuesToappscr57JSON();
	}
	
	if (currentPage == "appscr59A"){		
		Appscr58DetailJson();
	}
	
	if (currentPage == "appscr60")
		backData60();
	
	if(currentPage == "appscr61")	{			
		updateAppscr60FromJSON(HouseHoldRepeat);
	}
	
	if (currentPage == "appscr62Part1")
		backData61();

	if (currentPage == "appscr62Part2")
		backData62Part1();

	if (currentPage == "appscr63") {		
		backData62Part1();
	}

	if (currentPage == "appscr64")
		backData63();

	if (currentPage == "appscr66"){
		continueData66();
		backData65();
	}
	if (currentPage == "appscrBloodRel"){
		addSpecialCircumstances();
		appscr66FromMongodbJson();
	//appscr66FromMongodb();
	//appscr66NoneOfThese();
	}
	if (currentPage == "appscr67")
		backData66();

	if (currentPage == "appscr70")
		backData69();

	if (currentPage == "appscr71")
		backData70();

	if (currentPage == "appscr72")
		backData71();

	if (currentPage == "appscr73")
		backData72();
	
	if (currentPage == "appscr76P2")
		backData76P2();

	if (currentPage == "appscr77Part1")
		backData76P2();

	if (currentPage == "appscr77Part2")
		backData77Part1();

	if (currentPage == "appscr78")
		backData77Part2();

	if (currentPage == "appscr79")
		backData78();

	if (currentPage == "appscr81A")
		backData79();

	if (currentPage == "appscr81B")
		backData81A();
		
	if (currentPage == "appscr83"){
		showHouseHoldContactSummary();
	}
	
	if (currentPage == "appscr85")
		$('#saveBtn').show();
		
	if (baseFunctionForBackButton() == false) {
		return false;
	}

	removeValidationClass(currentPage);

	if (currentPage == "appscr75A") {
		totalIncomeShowMethod();
	}

	var pageId;
	var ssapApplicationObj = webJson.singleStreamlinedApplication;
	if(currentPage == 'appscr64' && (hideParentOrCaretaker() || ssapApplicationType == "NON_FINANCIAL")){
		backData62Part1();
		pageId = 'appscr62Part1';
	}else if(currentPage == 'appscr83' && ssapApplicationObj.applyingForFinancialAssistanceIndicator == false){
		pageId = 'appscr67';
	}else if(currentPage == 'appscr76P2' && !ssapApplicationObj.taxHousehold[0].householdMember[addQuetionsHouseHoldDone-1].healthCoverage.employerWillOfferInsuranceIndicator){
		pageId = 'appscr76P1';
	}else if(currentPage == 'appscr66' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
		//skip address other page if there is only 1 person
		pageId = 'appscr64';
		fillAppscr64(nonFinancialHouseHoldDone);
	}else if(currentPage == 'appscr67' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
		//skip relationship page if there is only 1 person
		pageId = 'appscr66';
		addSpecialCircumstances();
		appscr66FromMongodbJson();		
		//appscr66NoneOfThese();
	}else{
		pageId = getPrevPage();
	}
	
	if (!pageId) {
		alert('No Previous page!');
		return false;
	}

	$('#' + currentPage).hide();
	$('#' + pageId).show();

	currentPage = pageId;
	
	updateProgressBar(currentPage);
	//focus on first element for accessibility
	document.activeElement.blur();
	
	
	if (currentPage == 'appscr59A') {
		$('.button_ui_design_rote').css('margin-top', '-100px');
	} else {
		$('.button_ui_design_rote').css('margin-top', '');
	}

	backButton(currentPage);

	saveButton(currentPage);
	changeTitle(currentPage);
	getCurrentPageMethod(currentPage);
	getCurrentPageTabCss(currentPage);

	//scroll to top if error message shows 
	$("html, body").animate({ scrollTop: 0 }, "fast");

	if (currentPage == "appscr56") {
		goToPageById("appscr55");
	}

}

function deleteHouseholdForm() {	
	var strouter = "";
	$('#appscr57HouseholdForm').html(strouter);
}

function backButton(pageID) {
	//if (pageID == "appscr51") { // suneel 05092014
	if (pageID == "appscr51" || pageID == "appscr87") {
		$('#prevBtn').hide();
	} else {
		$('#prevBtn').show();
	}
	if (pageID == "appscr57") {
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();		
	}
}

function saveButton(pageID) {	
	if (pageID == "appscr51" || pageID == "appscr52" || pageID == "appscr87") {
		$('#save_button_div').hide();
	} else {
		$('#save_button_div').show();
	}
}

function setCurrentFinancialYear() {
	var today = new Date();
	var month = today.getMonth();
	var year = 1900 + today.getYear();
	var appYear = year + " - ";
	if (month < 3) {
		year--;
	} else {
		year++;
	}
	appYear += year;
	return appYear;
}

function setFederalAmount400() {

	var noOfPeople = $('#noOfPeople53').val();
	var amount = 0;
	if (noOfPeople <= 0) {
		$('#noOfPeople53').val('');
		$('#noOfPeople53').addClass('validateColor');
		setTimeout("$('#noOfPeople53').removeClass('validateColor');", 2000);
		$('#amount400').html(0);
		return;
	}
	amount = (11490 + (4020 * (noOfPeople - 1)));
	var amount400 = amount * 4;
	$('#amount400').html(amount400);
}

function noIrsData() {
	$('#irsIncome2013').val(0);
	$('#irsIncome').val(0);

	if ($('#noIrsData').is(":checked") == true) {
		$('#irsTable').hide();
		$('.irsIncome2013').text('[ Not Provided ]');
	} else {
		$('#irsTable').show();
		$('.irsIncome2013').text('$0');
	}

}

function irsIncomePrevYear() {
	var amount = $('#irsIncome2013').val();
	if (amount != undefined)
		amount = (amount.length == 0) ? 0 : amount;
	else
		amount = 0;
	$('#irsIncome').val(amount);
	$('.irsIncome2013').text('$' + amount);
}

// Keys
function irsIncomeResetValues() {

	$('#irsIncomeLabel').text(jQuery.i18n.prop('ssap.amountFromIRS'));
	$('#irsIncome2013').val('');
	/* document.getElementById('noIrsData').checked = false; */
	$('#irsTable').show();
}

function irsIncome2014(obj) {
	var val = obj.value;
	/* $('#expectedIncomeByHouseHoldMember').val(val); */
}

function addRemoveClass(id, bool) {

	if (bool)
		$('#' + id).addClass('validateColor');
	else
		$('#' + id).removeClass('validateColor');

}

function editFinalPage() {

	var editType = $('input[name=editRadioGroup]:radio:checked').val();

	$('#appscr57HouseHoldPersonalInformation tr').each(function(i) {
		$('#' + editType + (i + 1)).attr("checked", false);
	});

	$('.show_divtext').hide();
	$('.ad_alin_ui1').removeClass('visited_list');
	$('#' + editType + "_head").addClass('visited_list');
	$('#' + editType + "_div").slideDown('slow');

}

function populateChechBox() {

	var strToApp = '';
	$('#appscr57HouseHoldPersonalInformation tr').each(
			function(i) {
				var pureNameOfUser = '';
				$("td", this).each(function(j) {
					if (j < 3)
						pureNameOfUser += $(this).text();
					if (j < 2)
						pureNameOfUser += ' ';
				});

				strToApp += "<div class='checkbox_alinment'>"
						+ "<input id='_TYPE" + (i + 1)
						+ "' type='checkbox' name='CheckboxGroup" + (i + 1)
						+ "' value='" + pureNameOfUser
						+ "' class='list_selected_check'>"
						+ "<div class='text_style_change'>" + pureNameOfUser
						+ "</div>" + "</div>";

			});

	$('#HouseHoldDetailsUpdate_div').html(
			replaceAllCustom(strToApp, '_TYPE', 'HouseHoldDetailsUpdate'));
	$('#nonFinancialUpdate_div').html(
			replaceAllCustom(strToApp, '_TYPE', 'nonFinancialUpdate'));
	$('#financialDetailsUpdate_div').html(
			replaceAllCustom(strToApp, '_TYPE', 'financialDetailsUpdate'));

}

function replaceAllCustom(target, replace, replacement) {

	while (target.indexOf(replace) != -1) {
		target = target.replace(replace, replacement);
	}
	return target;
}

var irsIncomeLabelToolTip;
function setupPage69() {	
	//$('#houseHoldTRNo').val("HouseHold1");
	saveCurrentApplicantID(1);
	next();
}

function setKeyManagerForInput() {
	//$('#currentDate').val('18-Apr-2013'); // this format is not working in validating birthdate days count function checkBirthDate(fDob)
	$('#currentDate').val('05/05/2014');

	jQuery.fn.ForceCharacterOnly = function() {
		return this.each(function() {
			$(this).keydown(function(e) {
				var key = e.charCode || e.keyCode || 0;
				// allow backspace, tab, delete, arrows,
				// Alfabetics
				// home, end, and Esc.
				return (key == 27 || key == 46 || key == 8 || key == 9 || (key >= 35 && key <= 40) || (key >= 65 && key <= 90));
			});
		});
	};
	jQuery.fn.ForceNumericOnly = function() {
		return this.each(function() {
			$(this).keydown(function(e) {
				var key = e.charCode || e.keyCode || 0;

				return (key == 8 || key == 9 || key == 46 || key == 110 || key == 190 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
			});
		});
	};

	$('#Financial_Job_HoursOrDays_id, #noOfPeople53,'
	+ '#irsIncome, #irsIncome2013,'
	+ '#Financial_Job_id, #Financial_Self_Employment_id, #Financial_SocialSecuritybenefits_id, #Financial_Unemployment_id'
	+ '#Financial_Retirement_pension_id, #Financial_Capitalgains_id, #Financial_InvestmentIncome_id, #Financial_RentalOrRoyaltyIncome_id'
	+ '#Financial_FarmingOrFishingIncome_id, #Financial_AlimonyReceived_id, #Financial_OtherIncome_id, .numberStroke').ForceNumericOnly();

	$('#accordingToFederal_incomeTax').ForceNumericOnly();
	$('#basedOnAmountInput').ForceNumericOnly();

	$('#dateOfBirth, .dob-input').mask("00/00/0000");
	//$('#ssn').mask("999-99-9999");
	$('#passportExpDate').mask("00/00/0000");

	$('#unemploymentIncomeDate').mask("00/00/0000");

	$('#77P1WillBeEnrolledInHealthPlanDate').mask("12/31/9999");

	$('#ssn1').mask("999");
	$('#ssn2').mask("99");
	$('#ssn3').mask("9999");

	$('#spouseDOB').mask("00/00/0000");
	$('#taxFilerDob').mask("00/00/0000");

	$('#ssn1_80').mask("999");
	$('#ssn2_80').mask("99");
	$('#ssn3_80').mask("9999");

	$('#ssn1_81b').mask("999");
	$('#ssn2_81b').mask("99");
	$('#ssn3_81b').mask("9999");

	/*masking for amount*/

	//$(".amountMask").mask("00000000000000000");
	setControls();

	seleniumFlag = true;
}

function setFunctions() {

	$("input[name='docType']:radio").on('click', function() {
		if ($("#docType_other").is(":checked")) {
			$("#otherDocStatusOnPage62P1").show();

			$('#DocAlignNumber').removeClass("validateColor");
			$('#DocI-94Number').removeClass("validateColor");
			$('#passportDocNumber').removeClass("validateColor");
			$('#appscr62P1County').removeClass("validateColor");
			$('#passportExpDate').removeClass("validateColor");
			$('#sevisIDNumber').removeClass("validateColor");
		} else {
			$("#otherDocStatusOnPage62P1").hide();
		}
	});

	$(":input[name='whatHealthInsuranceDoesHaveNow']").on('click', function() {
		if ($("#otherHealthPlanRadioOnPage81P1").is(":checked")) {
			$("#otherHealthPlanOnPage80P1").show();
		} else
			$("#otherHealthPlanOnPage80P1").hide();
	});

	$(":input[name='doesHaveAPrimaryCarePhysicianP81A']").on(
			'click',
			function() {

				if ($(":input[name='doesHaveAPrimaryCarePhysicianP81A']").eq(1)
						.is(":checked")) {
					$("#primaryPhysicianInfoOnPage81P1").hide();
				} else
					$("#primaryPhysicianInfoOnPage81P1").show();
			});

	$(":input[name='whyDidThatInsuranceEnd']").on('click', function() {
		if ($(":input[name='whyDidThatInsuranceEnd']").last().is(":checked")) {
			$("#descriptionTextArea").show();
		} else
			$("#descriptionTextArea").hide();

	});

	var $appscr77Part1RG2 = $(":input[name='appscr77Part1RG2']");
	var $appscr77Part1RG3 = $(":input[name='appscr77Part1RG3']");
	var $hideandDisplayDiv = $(".appscr77P1HideOrDisplay");
	$appscr77Part1RG2.on('click', function() {
		if ($appscr77Part1RG2.first().is(":checked")) {
			$hideandDisplayDiv.first().show();
		} else {
			$hideandDisplayDiv.first().hide();
		}
	});
	$appscr77Part1RG3.on('click', function() {
		if ($appscr77Part1RG3.first().is(":checked")) {
			$hideandDisplayDiv.eq(1).show();
		} else {
			$hideandDisplayDiv.eq(1).hide();
		}
	});

	var $UScitizen77pp1RadioButton = $(":input[name='77P1ExpectChangesToHealthCoverage']");
	$UScitizen77pp1RadioButton.click(function() {
		if ($UScitizen77pp1RadioButton.first().is(":checked")) {
			$(".doesExceptAnyChangeToHealthCoverageInappscr77Part1").show();
		} else
			$(".doesExceptAnyChangeToHealthCoverageInappscr77Part1").hide();
	});

	var $UScitizen77Radios = $(":input[name='77P1WillBeEnrolledInHealthPlan']");
	$UScitizen77Radios.click(function() {
		if ($UScitizen77Radios.first().is(":checked")) {
			$("#DateToCovered").show();
		} else
			$("#DateToCovered").hide();
	});

	var $willHealthCoverageThroughJob = $(":input[name='willHealthCoverageThroughJob']");
	$willHealthCoverageThroughJob.click(function() {
		if ($willHealthCoverageThroughJob.first().is(":checked")) {
			$("#appscr76P1FirstHideAndShowDiv").show();
		} else
			$("#appscr76P1FirstHideAndShowDiv").hide();
	});

	$("#appscr76P1CheckBoxForEmployeeName").click(function() {
		if ($(this).is(":checked")) {
			$("#memberNameDataDivPart1").show();
		} else
			$("#memberNameDataDivPart1").hide();
	});

	$("#appscr76P1CheckBoxForOtherEmployee").click(function() {
		if ($(this).is(":checked")) {
			$("#memberNameDataDivPart2").show();
		} else
			$("#memberNameDataDivPart2").hide();
	});

	var $haveApPrimaryCarePhysician = $(":input[name='haveApPrimaryCarePhysician']");
	$haveApPrimaryCarePhysician.click(function() {
		if ($haveApPrimaryCarePhysician.first().is(":checked")) {
			$("#haveApPrimaryCarePhysicianOnPageAppscr76Part2").show();
		} else
			$("#haveApPrimaryCarePhysicianOnPageAppscr76Part2").hide();
	});

	$("#nameOfHouseholdOnPage76P2").click(function() {
		if ($(this).is("checked")) {
			$("#tellUsaboutUrFormerEmployerAapscr76P2").show();
		} else {
			$("#tellUsaboutUrFormerEmployerAapscr76P2").hide();
		}
	});

	$("#otherEmployeeOnPage76P2").click(function() {
		if ($(this).is(":checked")) {
			$("#secondDivToBeHideAndShowOnAppsce76P2").show();

		} else {
			$("#secondDivToBeHideAndShowOnAppsce76P2").hide();
		}
	});

	$("#naturalizedCitizenNaturalizedIndicator").click(function() {
		if ($(this).is(":checked")) {
			$(".appscr62Part1HideandShowFillingDetails").eq(0).show();
			$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();

		} else {
			$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
		}

	});

	$("#naturalizedCitizenNaturalizedIndicator2").click(function() {
		if ($(this).is(":checked")) {
			$(".appscr62Part1HideandShowFillingDetails").eq(1).show();
			$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();

		} else {
			$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();
		}
	});	

	$('input[name="docType"]').bind('click', function() {
		$(".immigrationDocType").hide();
		var immigrationDocType = $(this).attr('id');
		$('#' + immigrationDocType + 'div').show();
		$("#otherDocStatusOnPage62P1").hide();
		if(immigrationDocType == 'docType_other'){
			$("#otherDocStatusOnPage62P1").show();
		}	
	});	
}

function isPrimaryAddressEmpty() {
	var addLine1 = document.getElementById("home_addressLine1").value;
	var addLine2 = document.getElementById("home_addressLine2").value;
	var city = document.getElementById("home_primary_city").value;
	var zip = document.getElementById("home_primary_zip").value;
	var state = document.getElementById("home_primary_state").value;
	var county = document.getElementById("home_primary_county").value;
	if (addLine1 == "" || city == "" || zip == "" || state == ""
			|| county == "") {
		return true;
	} else
		return false;

}

/*
 * This function is used for filling the adress as the home address and mailing
 * address is same
 *
 */

function addressIsSame() {
	var contactHomeAddress = document.getElementById("mailingAddressIndicator");
	if (isPrimaryAddressEmpty() && contactHomeAddress.checked) {
		/*
		 * alert("Primary Address is not filled correctly");
		 */
		$('#primary-contact-information-form').parsley('validate');

		/*if ($('#dateOfBirth').next().hasClass('parsley-error-list')) {
			$('#dateSpan').addClass('dateSpanWithError');
		} else {
			$('#dateSpan').removeClass('dateSpanWithError');
		}*/
		contactHomeAddress.checked = false;

	}
	if (!$("#mailingAddressIndicator").is(":checked")) {
		$('#mailingAddressIndicator').attr('value', false);

	}
	if (!isPrimaryAddressEmpty()) {
		if (contactHomeAddress.checked == true) {			
			$('#mailing_primary_state').removeClass("validateColor");
			$('#mailingAddressIndicator').attr('value', true);
			$('.physicalmailingAddress').hide();
			$('#mailing_addressLine1').val($('#home_addressLine1').val());
			$('#mailing_addressLine2').val($('#home_addressLine2').val());
			$('#mailing_primary_city').val($('#home_primary_city').val());
			$('#mailing_primary_zip').val($('#home_primary_zip').val());
			$('#mailing_primary_state').val($('#home_primary_state').val());

			var home_primary_address = $('#home_primary_state').val();/*.split("#")[0];*/
			$('#mailing_primary_county option[value!=""]').remove();
			$('#mailing_primary_county').append('<option value="'+home_primary_address+'" selected="selected">'+home_primary_address+'</option>');

		} else if (contactHomeAddress.checked == false) {
			$('.physicalmailingAddress').show();
			$("#mailing_addressLine1, #mailing_addressLine2, #mailing_primary_city, #mailing_primary_zip").val('');
			$('#mailing_primary_county option[value!=""]').remove();
			setStateValue();
		}
	}
}

$(document).ready(function() {
	setKeyManagerForInput();
	
	/*var url = window.location.href;
	var lang = url.split("=")[1];*/
	
	/*HIX-46231*/
	var lang = 'en';
	
	loadBundles(lang);
	if ($("#mailingAddressIndicator").is(":checked")) {
		$('.physicalmailingAddress').hide();
	}
	getCountyList(' ');
	setStateValue();
	/*Added by Kunal*/
	$('.SSAPpgtitle').append('Before We Begin'); //need to put to properties file
	
	$('.dob').on('focusout',function(){
		var strDate = $(this).val();
		var mon = parseInt(strDate.substring(0, 2), 10);
		var dt = parseInt(strDate.substring(3, 5), 10);
		var yr = parseInt(strDate.substring(6, 10), 10);
		
		var validDate =/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/;

		var Joindate = new Date(yr, mon-1, dt);
		var today = new Date();
		
		var today_mon = today.getMonth() + 1;
		var today_dt = today.getDate();
		var today_yr = today.getFullYear();
		
		
		if($(this).closest('div.controls').find('.dobCustomError').length == 0){
			$(this).closest('div.controls').append('<div class ="dobCustomError"></div>');
		};
		
		var dobCustomError = $(this).closest('div.controls').find('.dobCustomError');
		if(validDate.test(strDate)){
		if (yr > today_yr) {
		
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
			dateValidation = false;
		} else if ((yr == today_yr) && mon > today_mon) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
			dateValidation = false;
		} else if ((yr == today_yr) && (mon == today_mon) && (dt > today_dt)) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
			dateValidation = false;		
		} else if ((today_yr - yr) >= 104) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date, Age Is More Than 104.</span>');
			dateValidation = false;		
		}else{
			dobCustomError.html('');
			dateValidation = true;
		}
		} else{
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
			dateValidation = false;		
			
		}
		
	});

});

function loadBundles(lang) {
	jQuery.i18n.properties({
		name : 'ssap',
		path : 'resources/js/ssap/',
		mode : 'both',
		language : lang,
		callback : function() {
			readPropertyFile();
		}
	});
}

function readPropertyFile() {

}

$(function() {
	$(".validateDedesignate")
			.click(
					function(e) {
						e.preventDefault();
						var href = $(this).attr('href');
						if (href.indexOf('#') != 0) {
							$(
									'<div id="modal" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="modalClose close" aria-hidden="true">×</button></div><div class="modal-body"><iframe id="dedesignatePopup" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:140px;"></iframe></div></div>')
									.modal({
										backdrop : "static",
										keyboard : "false"
									});
						}
					});
});

function closeLightbox() {
	$("#modal, .modal-backdrop").remove();
	window.location.reload();
}

function closeLightboxOnCancel() {
	$("#modal, .modal-backdrop").remove();
}

var registrationUrls = new Array("/securityquestions",
		"/account/phoneverification/sendcode", "/user/activation");
var currDocUrl = document.URL;
var urlIdx = 0;
var noOfRegUrls = registrationUrls.length;

while (urlIdx < noOfRegUrls) {
	if (currDocUrl.indexOf(registrationUrls[urlIdx]) > 0) {
		$('#menu').hide();
	}
	if (currDocUrl.indexOf('admin/broker/securityquestions') > 0) {
		$('#menu').show();
	}
	if (currDocUrl.indexOf('crm/consumer/securityquestions') > 0) {
		$('#menu').show();
	}
	urlIdx++;
}

$(document).ready(
		function() {
			$('#ssn').mask('999-99-9999');
			$('.date.date-picker').each(function() {
				$(this).datepicker({
					autoclose : true,
					dateFormat : 'MM/DD/YYYY'
				});
			});

			getDetailsFromJSONFile();
			
			// add "Any" option into plan level
			$("#planLevel").prepend("<option value=''>Any</option>").val('');
			var selectedPlanLevel = '';

			if (selectedPlanLevel == '') {
				$("#planLevel option[value='']").attr('selected', 'selected');
			} else {
				$("#planLevel option[value='" + selectedPlanLevel + "']").attr(
						'selected', 'selected');
			}
			populateCountriesOfIssuance();
			
			
			if($("#helpingEntity").val() == "NONE"){
				
				$("#authorizedRepresentativeHelpYes").prop("checked",false);
				$("#authorizedRepresentativeHelpNo").prop("checked",true);
			}else{
				$("#authorizedRepresentativeHelpYes").prop("checked",true);
				$("#authorizedRepresentativeHelpNo").prop("checked",false);
				displayAuthorizedRepresenatitive();
			}
		});

// /// DATA READ FROM JSON FILE /////

function getDetailsFromJSONFile() {
		applicationStatus = $("#ssapApplicationStatus").val();
		mode = $("#applicationMode").val();
		haveData = true;
		webJsonString = $("#ssapApplicationJson").val();
		webJson = $.parseJSON(webJsonString);
		var orgJson = $.parseJSON(webJsonString);
		
		$("#ssapApplicationJson").val('');
		
		noOfHouseHold = getNoOfHouseHolds();
		var householdMemberObj = "";
		for ( var outerCnt = 1; outerCnt <= noOfHouseHold; outerCnt++) {			
			for ( var innerCnt = 1; innerCnt <= noOfHouseHold; innerCnt++) {
				householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[innerCnt-1];
				if(householdMemberObj.personId == outerCnt){										
					break;
				}
			}			
			orgJson.singleStreamlinedApplication.taxHousehold[0].householdMember[outerCnt-1] = householdMemberObj;			
		}		
		webJson = orgJson;
		
		//var arrlastSavedPage = $("#ssapCurrentPageId").val();
		//arrlastSavedPage = arrlastSavedPage.split('#');
		
		var lastSavedPage = $("#ssapCurrentPageId").val();
		var CurrentApplicantID = 1;				
		var helpType = $("#helpingEntity").val();		
		var brokerCertificationNumber = $("#designatedBrokerLicenseNumber").val();
		var brokerName = $("#designatedBrokerName").val();
		var brokerFirstName =  $("#brokerFirstName").val();
		var brokerLastName =   $("#brokerLastName").val();
		var internalBrokerId =  parseInt($("#internalBrokerId").val());
		var internalAssisterId = parseInt($("#internalAssisterId").val());
		var assisterLicenseNumber = $("#designatedAssisterLicenseNumber").val();
		var assisterName = $("#designatedAssisterName").val();
		var assisterFirstName =  $("#assisterFirstName").val();
		var assisterLastName =   $("#assisterLastName").val();
		
		if(webJson == null || webJson == "") {
			$('#appscr51').show();
			webJson = getDefatulJson();
			
			webJson.singleStreamlinedApplication.helpType = helpType;
			var cmrHouseHoldInfo = $.parseJSON($("#cmrHouseHold").val());	
			
			
			
			if(helpType == "BROKER"){
				webJson.singleStreamlinedApplication.broker.brokerName = brokerName;
				webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = brokerCertificationNumber;
				webJson.singleStreamlinedApplication.broker.brokerFirstName = brokerFirstName;
				webJson.singleStreamlinedApplication.broker.brokerLastName = brokerLastName;
				webJson.singleStreamlinedApplication.broker.internalBrokerId = internalBrokerId;
			}else if(helpType == "ASSISTER"){
				if(webJson.singleStreamlinedApplication.assister == undefined){
					var defaultJson = getDefatulJson();
					var assister = defaultJson.singleStreamlinedApplication.assister;
					webJson.singleStreamlinedApplication.assister = assister;
				}
				if(webJson.singleStreamlinedApplication.assister != undefined){
					webJson.singleStreamlinedApplication.assister.assisterName = assisterName;
					webJson.singleStreamlinedApplication.assister.assisterID = assisterLicenseNumber;
					webJson.singleStreamlinedApplication.assister.internalAssisterId = internalAssisterId;
					webJson.singleStreamlinedApplication.assister.assisterFirstName = assisterFirstName;
					webJson.singleStreamlinedApplication.assister.assisterLastName = assisterLastName;
				}
				
			}else{
				webJson.singleStreamlinedApplication.broker.brokerName = "";
				webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = "";
				webJson.singleStreamlinedApplication.broker.brokerFirstName = "";
				webJson.singleStreamlinedApplication.broker.brokerLastName = "";
				webJson.singleStreamlinedApplication.broker.internalBrokerId = 0;
				if(webJson.singleStreamlinedApplication.assister != undefined){
					webJson.singleStreamlinedApplication.assister.assisterName = "";
					webJson.singleStreamlinedApplication.assister.assisterID = null;
					webJson.singleStreamlinedApplication.assister.assisterFirstName = "";
					webJson.singleStreamlinedApplication.assister.assisterLastName = "";
					webJson.singleStreamlinedApplication.assister.internalAssisterId = 0;	
				}
			}
			
			haveData = false;
			mode="";
			
			$('#firstName').val(cmrHouseHoldInfo.firstName);
			$('#middleName').val(cmrHouseHoldInfo.middleName);
			$('#lastName').val(cmrHouseHoldInfo.lastName);
			$('#emailAddress').val(cmrHouseHoldInfo.emailAddress);
			$('#first_homePhoneNo').val(cmrHouseHoldInfo.phoneNumber);
			
			
			$('#acceptanceCB').prop("checked", false);
			return;
		}
		else {
			
			webJson.singleStreamlinedApplication.helpType = helpType;
			
			if(helpType == "BROKER"){
				webJson.singleStreamlinedApplication.broker.brokerName = brokerName;
				webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = brokerCertificationNumber;
				webJson.singleStreamlinedApplication.broker.brokerFirstName = brokerFirstName;
				webJson.singleStreamlinedApplication.broker.brokerLastName = brokerLastName;
				webJson.singleStreamlinedApplication.broker.internalBrokerId = internalBrokerId;
			}else if(helpType == "ASSISTER"){
				webJson.singleStreamlinedApplication.assister.assisterName = assisterName;
				webJson.singleStreamlinedApplication.assister.internalAssisterId = internalAssisterId;					
				webJson.singleStreamlinedApplication.assister.assisterID = assisterLicenseNumber ;
				webJson.singleStreamlinedApplication.assister.assisterFirstName = assisterFirstName;
				webJson.singleStreamlinedApplication.assister.assisterLastName = assisterLastName;
			}else{
				webJson.singleStreamlinedApplication.broker.brokerName = "";
				webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = "";
				webJson.singleStreamlinedApplication.broker.brokerFirstName = "";
				webJson.singleStreamlinedApplication.broker.brokerLastName = "";
				webJson.singleStreamlinedApplication.broker.internalBrokerId = 0 ;
				if(webJson.singleStreamlinedApplication.assister != undefined){

					webJson.singleStreamlinedApplication.assister.assisterName = "";
					webJson.singleStreamlinedApplication.assister.assisterID = null;
					webJson.singleStreamlinedApplication.assister.assisterFirstName = "";
					webJson.singleStreamlinedApplication.assister.assisterLastName = "";
					webJson.singleStreamlinedApplication.assister.internalAssisterId = 0;
				}
				

			}
			
			if (webJson.singleStreamlinedApplication.applicationSignatureDate !=""){
				//$('.SSAPpgtitle').append('View Your Application');
				f.appscr51 = 'Before We Begin';
				f.appscr52 = 'Get Ready to Start Your Application';
			}
			$('#acceptanceCB').prop("checked", true);
			if (typeof webJson.singleStreamlinedApplication.currentApplicantId !== 'undefined' && webJson.singleStreamlinedApplication.currentApplicantId !== null){				 
				 if(lastSavedPage == "appscr67"){
					webJson.singleStreamlinedApplication.currentApplicantId =  webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;					
				 } 
				 CurrentApplicantID = webJson.singleStreamlinedApplication.currentApplicantId;
			}
			if(lastSavedPage != ""){
			if ("Y" != $("#csroverride").val()) {
				currentPage = lastSavedPage;
				currentPage = getPrevPage();
				$('#appscr51').hide();
				noOfHouseHoldmember=getNoOfHouseHolds();
				$('#numberOfHouseHold').text(noOfHouseHoldmember);
				$('#noOfPeople53').val(noOfHouseHoldmember);				
				saveCurrentApplicantID(CurrentApplicantID);
				nonFinancialHouseHoldDone = CurrentApplicantID;	
				saveCurrentApplicantID(CurrentApplicantID);
				
				/*HIX-68449*/
				if(currentPage == "appscr65")
				{		
					addSpecialCircumstances();
					appscr66FromMongodbJson();
				}
				next();
				}
			}else{
				$('#appscr51').show();
			}
			
			if ("Y"== $("#csroverride").val()) {
				$('#contBtn').show();
				$('#prevBtn').show();
				$('#saveBtn').show();

				var totalApplicants = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
				$('#appscr57HouseHoldPersonalInformation').html('');
				houseHoldNumberValue = 0;
				mode=""; // reset so that the data cna be saved
				
				$('#contBtn').click();
				$('#contBtn').click();
				
				return;
				
			}			
			if (applicationStatus == "CC" || applicationStatus == "CL" || applicationStatus == "SG" || applicationStatus == "SU" || applicationStatus == "ER" || applicationStatus == "EN") {
				$( "#sidebar" ).children(".header, .nav-list").hide();
				$('#appscr51').hide();
				$('#' + currentPage).hide();
				$('.SSAPpgtitle').text("View Your Application");
				$('#tab1').removeClass('active');
				currentPage = "appscr84";
				
				$('#progressDiv').hide();
				
				$('#appscr84').show();
				$('#contBtn').hide();
				$('#prevBtn').hide();
				$('#saveBtn').hide();
				
				//update26Indicator();
				addReviewSummaryJson();
				
				hideNonFinacial();
				
				submitForAutoContinue = true;

				return;
			}

			$('#contBtn').show();
			$('#prevBtn').show();
			$('#saveBtn').show();

			var totalApplicants = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
			$('#appscr57HouseHoldPersonalInformation').html('');
			houseHoldNumberValue = 0;
			mode=""; 
			
			if("appscr74" == lastSavedPage || "appscr75A" == lastSavedPage || "appscr76P1" == lastSavedPage 
					|| "appscr77Part1" == lastSavedPage || "appscr76P2" == lastSavedPage || "appscr81B" == lastSavedPage  ) {
				
				$('#appscr53' ).hide();
				addReviewSummary();
				addIncarceratedContent(); 
				//caller();
				//setNonFinancialPageProperty(2);
				setNonFinancialPageProperty(1);
				currentPage = "appscr85";
				getMethod("appscr85");
				currentPage = "appscr85";
				$('#appscr53' ).hide();
				$('#appscr85').show();
				$('#contBtn').text('Submit');
				
			}
			
			else if("appscr67"==lastSavedPage){
				showHouseHoldContactSummary();
			}
	}
}

function setButtons() {
	$('#contBtn').text('Submit');
	next();
}
function caller() {	
	$('#waiting').fadeIn();
	$.ajax({
		type : "GET",
		url : theUrlForisRidpVerified,
		dataType : 'json',
		success : function(response) {

			if(isInvalidCSRFToken(response))	    				
				return;	
			
			if(response.toString()=='true'){
				webJson.singleStreamlinedApplication.isRidpVerified = true;
				setNonFinancialPageProperty(1);
				addIncarceratedContent();
				//next();
			}else{
				//currentPage = 'appscr85';
				//saveDataSubmit();
				window.location.assign("http://"+window.location.host + "/hix/ridp/verification?caseNumber=" + webJson.singleStreamlinedApplication.applicationGuid);
			}
		},
		error : function(e) {
			alert("Failed to Check RIDP Response");
		}
	});
	$('#waiting').hide();
	$('#saveBtn').hide();
}

function getDefatulJson() {
	var jsonData = {
		"singleStreamlinedApplication" : {
			"ssapVersion":1.0,
			"ssapApplicationId" : "",
			"applicationDate" : "Mar 13, 2014 1:25:47 PM",
			"applicationStartDate" : getCurrentDateTime(),
			"applicationSignatureDate" : null,
			"authorizedRepresentativeIndicator" : null,
			"acceptPrivacyIndicator" : true,
			"isRidpVerified":false,
			"ApplicationType" : "OE",
			"clientIp" : "",
			"IP" : null,
			"applicationGuid" : "",
			"currentApplicantId" : null,
			"applyingForFinancialAssistanceIndicator" : null,
			"mecCheckStatus" : "N",
			"getHelpIndicator": null,
			"helpType" : "",			
			"applyingForhouseHold" : null,
			"consentAgreement" : null,
			"numberOfYearsAgreed" : "",
			"incarcerationAsAttestedIndicator":null,
			"authorizedRepresentative" : {
				"name" : {
					"firstName" : "",
					"middleName" : "",
					"lastName" : "",
					"suffix" : ""
				},
				"address" : {
					"streetAddress1" : "",
					"streetAddress2" : "",
					"city" : "",
					"state" : "",
					"postalCode" : ""
				},
				"phone" : [ 
				    {
						"phoneNumber" : "",
						"phoneExtension" : "",
						"phoneType":"cell"
					},
					{
						"phoneNumber" : "",
						"phoneExtension" : "",
						"phoneType":"home"
					},
					{
						"phoneNumber" : "",
						"phoneExtension" : "",
						"phoneType":"work"
					}
				],
				"emailAddress" : "",
				"partOfOrganizationIndicator" : null,
				"companyName" : "",
				"organizationId" : null,
				"signatureisProofIndicator" : null,
				"signature":""
			},
			"broker" : {
				"brokerName" : "",
				"brokerFirstName" : "",
				"brokerLastName" : "",
				"brokerFederalTaxIdNumber" : "",
				"internalBrokerId" : 0
			},
			"assister" : {
				"assisterName" : "",
				"assisterFirstName" : "",
				"assisterLastName" : "",
				"assisterID" : null,
				"internalAssisterId" : 0
			},			
			"primaryTaxFilerPersonId" : 0,
			"taxFilerDependants" : [],
			"taxHousehold" : [ {
				"houseHoldIncome": 0,
				"householdMember" : [
						{
							"personId" : 1,
							"applicantGuid" : "",
							"name" : {
								"firstName" : "",
								"middleName" : "",
								"lastName" : "",
								"suffix" : ""
							},
							"dateOfBirth" : null,
							"householdContactIndicator" : true,
							"applyingForCoverageIndicator" : false,
							"under26Indicator" : true,
							"householdContact" : {
								"homeAddressIndicator" : false,
								"homeAddress" : {
									"streetAddress1" : "",
									"streetAddress2" : "",
									"city" : "",
									"state" : "",
									"postalCode" : "",
									"county" : "",
									"primaryAddressCountyFipsCode" : ""
								},
								"mailingAddressSameAsHomeAddressIndicator" : true,
								"mailingAddress" : {
									"streetAddress1" : "",
									"streetAddress2" : "",
									"city" : "",
									"state" : "",
									"postalCode" : "",
									"county" : ""
								},
								"phone" : {
									"phoneNumber" : "",
									"phoneExtension" : "",
									"phoneType" : "CELL"
								},
								"otherPhone" : {
									"phoneNumber" : "",
									"phoneExtension" : "",
									"phoneType" : "HOME"
								},
								"contactPreferences" : {
									"preferredSpokenLanguage" : "",
									"preferredWrittenLanguage" : "",
									"emailAddress" : "",
									"preferredContactMethod":""
								},
								"stateResidencyProof" : ""
							},
							"livesWithHouseholdContactIndicator" : true,
							"taxFiler" : {
								"liveWithSpouseIndicator" : null,
								"spouseHouseholdMemberId" : 0,
								"claimingDependantsOnFTRIndicator" : null,
								"taxFilerDependants": [
						                                {
						                                    "dependantHouseholdMemeberId": 0
						                                }
						                            ]
							},
							"taxFilerDependant" : {
								"taxFilerDependantIndicator" : null
							},
							"planToFileFTRIndicator" : null,
							"planToFileFTRJontlyIndicator" : null,
							"marriedIndicator" : null,
							"gender" : "",
							"tobaccoUserIndicator" : false,
							"livesAtOtherAddressIndicator" : null,
							"otherAddress" : {
								"address" : {
									"streetAddress1" : "",
									"streetAddress2" : "",
									"city" : "",
									"state" : "",
									"postalCode" : "",
									"county" : ""
								},
								"livingOutsideofStateTemporarilyIndicator" : false,
								"livingOutsideofStateTemporarilyAddress" :{
									"streetAddress1" : "",
									"streetAddress2" : "",
									"city" : "",
									"state" : "",
									"postalCode" : "",
									"county" : ""
								}
							},
							"specialEnrollmentPeriod" : {
								"birthOrAdoptionEventIndicator" : false
							},
							"socialSecurityCard" : {
								"socialSecurityCardHolderIndicator" : null,
								"socialSecurityNumber" : "",
								"ssnManualVerificationIndicator" : true,
								"nameSameOnSSNCardIndicator" : null,
								"firstNameOnSSNCard" : "",
								"middleNameOnSSNCard" : "",
								"lastNameOnSSNCard" : "",
								"suffixOnSSNCard": "",
								"deathIndicatorAsAttested" : false,
								"deathIndicator" : false,
								"deathManualVerificationIndicator" : false,
								"useSelfAttestedDeathIndicator" : true,
								"individualMandateExceptionIndicator": null,
	                            "individualManadateExceptionID": null
							},
							"specialCircumstances" : {
								"americanIndianAlaskaNativeIndicator" : false,
								"tribalManualVerificationIndicator" : true,
								"pregnantIndicator" : false,
								"numberBabiesExpectedInPregnancy" : 0,
								"fosterChild" : false,
								"everInFosterCareIndicator" : false,
								"fosterCareState": "",
	                            "ageWhenLeftFosterCare": 0,
	                            "gettingHealthCareThroughStateMedicaidIndicator": true
							},
							"americanIndianAlaskaNative" : {
								"memberOfFederallyRecognizedTribeIndicator" : false,
								"state": "",
	                            "tribeName": "",
	                            "tribeFullName":"",
	                            "stateFullName":""
							},

							"citizenshipImmigrationStatus" : {
								"citizenshipAsAttestedIndicator" : true,
								"citizenshipStatusIndicator" : null,
								"citizenshipManualVerificationStatus" : null,
								"naturalizedCitizenshipIndicator" : null,
								"naturalizationCertificateIndicator" : null,
								"naturalizationCertificateAlienNumber" : "",
								"naturalizationCertificateNaturalizationNumber" : "",
								"eligibleImmigrationStatusIndicator" : null,
								"livedIntheUSSince1996Indicator" : null,
								"lawfulPresenceIndicator" : true,
								"honorablyDischargedOrActiveDutyMilitaryMemberIndicator" : null,
								"eligibleImmigrationDocumentSelected":"",
								"eligibleImmigrationDocumentType" : [ {
									"I551Indicator" : false,
									"TemporaryI551StampIndicator" : false,
									"MachineReadableVisaIndicator" : false,
									"I766Indicator" : false,
									"I94Indicator" : false,
									"I94InPassportIndicator" : false,
									"I797Indicator" : false,
									"UnexpiredForeignPassportIndicator" : false,
									"I327Indicator" : false,
									"I571Indicator" : false,
									"I20Indicator" : false,
									"DS2019Indicator" : false,
									"OtherDocumentTypeIndicator" : false,
								} ],
								"otherImmigrationDocumentType" : {
									"ORRCertificationIndicator" : false,
									"ORREligibilityLetterIndicator" : false,
									"CubanHaitianEntrantIndicator" : false,
									"WithholdingOfRemovalIndicator" : false,
									"AmericanSamoanIndicator" : false,
									"StayOfRemovalIndicator" : false
								},
								"citizenshipDocument" : {
									"alienNumber" : "",
									"i94Number" : "",
									"cardNumber" : "",
									"documentExpirationDate" : null,
									"visaNumber" : "",
									"SEVISId" : "",
									"foreignPassportOrDocumentNumber" : "",
									"foreignPassportCountryOfIssuance" : "0",
									"sevisId" : "",
									"documentDescription" : "",
									"nameSameOnDocumentIndicator" : null,
									"nameOnDocument" : {
										"firstName" : "",
										"middleName" : "",
										"lastName" : "",
										"suffix" : ""
									}
								},
							},
							"parentCaretakerRelatives": {
	                            "mainCaretakerOfChildIndicator": null,
	                            "personId": [],
	                            "anotherChildIndicator": false,
	                            "name": {
	                                "firstName": "",
	                                "middleName": "",
	                                "lastName": "",
	                                "suffix": ""
	                            },
	                            "dateOfBirth": null,
	                            "relationship": "",
	                            "liveWithAdoptiveParentsIndicator": true
	                        },
							"ethnicityAndRace" : {
								"hispanicLatinoSpanishOriginIndicator" : null,
								"ethnicity" : [],
								"race" : []
							},
							"expeditedIncome" : {
								"irsReportedAnnualIncome" : null,
								"irsIncomeManualVerificationIndicator" : false,
								"expectedAnnualIncome" : null,
								"expectedAnnualIncomeUnknownIndicator" : null,
								"expectedIncomeVaration" : null
							},
							"healthCoverage" : {
								"haveBeenUninsuredInLast6MonthsIndicator" : null,
								"isOfferedHealthCoverageThroughJobIndicator" : null,
								"employerWillOfferInsuranceIndicator" : null,
								"employerWillOfferInsuranceCoverageStartDate": null,
								"currentEmployer" : [ {
									"employer" : {
										"phone" : {
											"phoneNumber" : "",
											"phoneExtension" : "",
											"phoneType" : "HOME"
										},
										"employerContactPersonName" : "",
										"employerContactEmailAddress" : ""
									},
									"currentEmployerInsurance" : {
										"isCurrentlyEnrolledInEmployerPlanIndicator" : false,
										"willBeEnrolledInEmployerPlanIndicator" : false,
										"willBeEnrolledInEmployerPlanDate": null,
										"expectedChangesToEmployerCoverageIndicator": true,
                                        "employerWillNotOfferCoverageIndicator": false,
                                        "employerCoverageEndingDate": null,
                                        "memberPlansToDropEmployerPlanIndicator": true,
                                        "memberEmployerPlanEndingDate": null,
                                        "employerWillOfferPlanIndicator": true,
                                        "employerPlanStartDate": null,
                                        "memberPlansToEnrollInEmployerPlanIndicator": true,
                                        "memberEmployerPlanStartDate": null,
										"currentLowestCostSelfOnlyPlanName" : "",
										"currentPlanMeetsMinimumStandardIndicator" : false,
										"comingLowestCostSelfOnlyPlanName" : "",
										"comingPlanMeetsMinimumStandardIndicator" : false
									}
								} ],
								"currentlyEnrolledInCobraIndicator" : null,
								"currentlyEnrolledInRetireePlanIndicator" : null,
								"currentlyEnrolledInVeteransProgramIndicator" : null,
								"currentlyEnrolledInNoneIndicator" : null,
								"willBeEnrolledInCobraIndicator" : null,
								"willBeEnrolledInRetireePlanIndicator" : null,
								"willBeEnrolledInVeteransProgramIndicator" : null,
								"willBeEnrolledInNoneIndicator" : null,
								"hasPrimaryCarePhysicianIndicator" : false,
								"primaryCarePhysicianName" : "",
								"formerEmployer" : [ {
									"employerName" : "",
									"address" : {
										"streetAddress1" : "",
										"streetAddress2" : "",
										"city" : "",
										"state" : "",
										"postalCode": ""
									},
									"phone" : {
										"phoneNumber" : "",
										"phoneExtension" : "",
										"phoneType" : "CELL"
									},
									"employerIdentificationNumber" : "",
									"employerContactPersonName" : "",
									"employerContactPhone" : {
										"phoneNumber" : "",
										"phoneExtension" : "",
										"phoneType" : "HOME"
									},
									"employerContactEmailAddress" : ""
								} ],
								"currentOtherInsurance" : {
									"currentOtherInsuranceSelected":"",									
									"otherStateOrFederalProgramType" : "",
									"otherStateOrFederalProgramName" : "",
								},
								"currentlyHasHealthInsuranceIndicator" : false,
								"otherInsuranceIndicator" : null,
								"medicaidInsurance" : {
									"requestHelpPayingMedicalBillsLast3MonthsIndicator" : false
								},
								"chpInsurance" : {
									"insuranceEndedDuringWaitingPeriodIndicator" : false,
									"reasonInsuranceEndedOther" : ""
								}
							},
							"incarcerationStatus" : {
								"incarcerationAsAttestedIndicator" : false,
								"incarcerationStatusIndicator" : false,
								"incarcerationPendingDispositionIndicator" : false,
								"incarcerationManualVerificationIndicator" : false,
								"useSelfAttestedIncarceration" : false
							},
							"detailedIncome" : {
								"jobIncomeIndicator":null,
								"jobIncome" : [ {
									"employerName" : "",
									"incomeAmountBeforeTaxes" : null,
									"incomeFrequency" : "",
									"workHours" : null
								} ],
								"otherIncomeIndicator":null,
								"otherIncome" : [{
									"otherIncomeTypeDescription" : "Canceled debts",
									"incomeAmount" : null,
									"incomeFrequency" : ""
								},
								{
									"otherIncomeTypeDescription" : "Court Awards",
									"incomeAmount" : null,
									"incomeFrequency" : ""
								},
								{
									"otherIncomeTypeDescription" : "Jury duty pay",
									"incomeAmount" : null,
									"incomeFrequency" : ""
								}],
								"capitalGainsIndicator":null,
								"capitalGains" : {
									"annualNetCapitalGains" : null
								},
								"rentalRoyaltyIncomeIndicator":null,
								"rentalRoyaltyIncome" : {
									"netIncomeAmount" : null,
									"incomeFrequency" : ""
								},
								"farmFishingIncomeIndictor":null,
								"farmFishingIncome" : {
									"netIncomeAmount" : null,
									"incomeFrequency" : ""
								},
								"alimonyReceivedIndicator":null,
								"alimonyReceived" : {
									"amountReceived" : null,
									"incomeFrequency" : ""
								},
								"selfEmploymentIncomeIndicator":null,
								"selfEmploymentIncome" : {
									"typeOfWork" : "",
									"monthlyNetIncome" : null									
								},
								"deductions" : {
									"deductionType" : [ "ALIMONY",
											"STUDENT_LOAN_INTEREST", "OTHER" ],
									"deductionAmount" : null,
									"deductionFrequency" : "MONTHLY",
									"alimonyDeductionAmount" : null,
									"alimonyDeductionFrequency" : "",
									"studentLoanDeductionAmount" : null,
									"studentLoanDeductionFrequency" : "",
									"otherDeductionAmount" : null,
									"otherDeductionFrequency" : ""
								},
								"unemploymentBenefitIndicator":null,
								"unemploymentBenefit" : {
									"stateGovernmentName" : "",
									"benefitAmount" : null,
									"incomeFrequency" : "",
									"unemploymentDate" : null
								},
								"retirementOrPensionIndicator":null,
								"retirementOrPension" : {
									"taxableAmount" : null,
									"incomeFrequency" : ""
								},
								"socialSecurityBenefitIndicator":null,
								"socialSecurityBenefit" : {
									"benefitAmount" : null,
									"incomeFrequency" : ""
								},
								"investmentIncomeIndicator":null,
								"investmentIncome" : {
									"incomeAmount" : null,
									"incomeFrequency" : ""
								},
								"discrepancies" : {
									"hasChangedJobsIndicator" : false,
									"stopWorkingAtEmployerIndicator" : null,
									"didPersonEverWorkForEmployerIndicator" : null,
									"hasHoursDecreasedWithEmployerIndicator" : null,
									"hasWageOrSalaryBeenCutIndicator" : null,
									"explanationForJobIncomeDiscrepancy" : "",
									"seasonalWorkerIndicator" : null,
									"explanationForDependantDiscrepancy" : "",
									"otherAboveIncomeIncludingJointIncomeIndicator" : null
								},
								"wageIncomeProof" : "",
								"scholarshipIncomeProof" : "",
								"dividendsIncomeProof" : "",
								"taxableInterestIncomeProof" : "",
								"annuityIncomeProof" : "",
								"pensionIncomeProof" : "",
								"royaltiesIncomeProof" : "",
								"unemploymentCompensationIncomeProof" : "",
								"foreignEarnedIncomeProof" : "",
								"rentalRealEstateIncomeProof" : "",
								"sellingBusinessPropertyIncomeProof" : "",
								"farmIncomeProof" : "",
								"partnershipAndSwarpIncomeIncomeProof" : "",
								"businessIncomeProof" : "",
								"childNaTribe" : "",
								"taxExemptedIncomeProof" : "",
								"socialSecurityBenefitProof" : "",
								"selfEmploymentTaxProof" : "",
								"studentLoanInterestProof" : "",
								"receiveMAthrough1619" : false,
								"receiveMAthroughSSI" : false,
								"foreignEarnedIncome" : {
									"incomeAmount" : null
								},
								"rentalRealEstateIncome" : {
									"incomeAmount" : null
								},
								"sellingBusinessProperty" : {
									"incomeAmount" : null
								},
								"farmIncom" : {
									"incomeAmount" : null
								},
								"partnershipsCorporationsIncome" : {
									"incomeAmount" : null
								},
								"businessIncome" : {
									"incomeAmount" : null
								},
								"selfEmploymentTax" : {
									"incomeAmount" : null
								},
								"studentLoanInterest" : {
									"incomeAmount" : null
								}
							},
							"bloodRelationship" : [{
									"individualPersonId" : null,
									"relatedPersonId" : null,
									"relation" : "",
									"textDependency" : null
									}],
							"migrantFarmWorker" : true,
							"birthCertificateType" : "NO_CRETIFICATE",
							"infantIndicator" : false,
							"PIDIndicator" : false,
							"PIDVerificationIndicator" : false,
							"livingArrangementIndicator" : false,
							"IPVIndicator" : false,
							"strikerIndicator" : false,
							"drugFellowIndicator" : false,
							"SSIIndicator" : false,
							"residencyVerificationIndicator" : false,
							"disabilityIndicator" : false,
							"shelterAndUtilityExpense" : null,
							"dependentCareExpense" : null,
							"childSupportExpense" : null,
							"medicalExpense" : null,
							"heatingCoolingindicator" : false
							
							
						},

				],
				"applyingForCashBenefitsIndicator" : false
			} ]
		}
	};
	return jsonData;

}


function callLastPage() {

}

function testRidp() {
	location.href = "ssap/ssapverification";
}

/*
 * Populate Counties Based on the State i.e the exchange Type
 */

function getCountyList(stateCode) {
	
	$.ajax({
		type : "POST",
		url : theUrlForAddCounties,
		data : {
			stateCode : stateCode
		},
		dataType : 'json',
		success : function(response) {

			// populateCounties(response,''); FIX ME
		},
		error : function(e) {
			alert("Failed to Populate County Details");
		}
	});
}

function populateCounties(response, county) {
	// console.log(response, eIndex,county,"populate counties")
	$('#mailing_primary_county').html('');
	$('#home_primary_county').html('');

	var optionsstring = '<option value="">County</option>';
	var i = 0;
	$.each(response, function(key, value) {
		var selected = (county == key) ? 'selected' : '';
		var options = '<option value="' + key + '" ' + selected + '>' + key
				+ '</option>';
		optionsstring = optionsstring + options;
		i++;
	});

	$('#mailing_primary_county').html(optionsstring);
	$('#home_primary_county').html(optionsstring);
}

/*
 * Populating Counties Ends here
 *
 */

function guidGenerator() {
	var IEBrowser = isIEBrowser();
	if(IEBrowser && IEBrowser < 11){
		var buf = new Array(8);
		for(var i=0; i < buf.length; i++){
			buf[i] = Math.floor((Math.random() * 9999) + 1000).toString();
		}
		
		return (buf[0] + buf[1] + "-" + buf[2] + "-4"
				+ buf[3].substring(1) + "-y" + buf[4].substring(1) + "-"
				+ buf[5] + buf[6] + buf[7]);
	}
	
	var buf = new Uint16Array(8);
	window.crypto.getRandomValues(buf);
	var S4 = function(num) {
		var ret = num.toString(16);
		while (ret.length < 4) {
			ret = "0" + ret;
		}
		;
		return ret;
	};
	return (S4(buf[0]) + S4(buf[1]) + "-" + S4(buf[2]) + "-4"
			+ S4(buf[3]).substring(1) + "-y" + S4(buf[4]).substring(1) + "-"
			+ S4(buf[5]) + S4(buf[6]) + S4(buf[7]));
}

function isIEBrowser() { //return false or browser version
  var BrowserDetail = navigator.userAgent.toLowerCase();
  return (BrowserDetail.indexOf('msie') != -1) ? parseInt(BrowserDetail.split('msie')[1]) : false;
}


function setStateValue(){
	var exchangeName = $('#exchangeName').val();

	if(exchangeName === "NMHIX"){
		$('#home_primary_state option[value="NM"], #mailing_primary_state option[value="NM"]').attr("selected","selected");
	}else if(exchangeName === "One, Mississippi"){
		$('#home_primary_state option[value="MS"], #mailing_primary_state option[value="MS"]').attr("selected","selected");
	}else if(exchangeName === "Your Health Idaho"){
		$('#home_primary_state option[value="ID"], #mailing_primary_state option[value="ID"]').attr("selected","selected");
	}
}

function startFinancialSectionPages() {



	if (currentPage == "appscr68") {
		if(currentPersonIndex == -1) {
			currentPersonIndex ++;
		}
		var pageId = getNextPage();

		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);

		$('#' + currentPage).hide();
		$('#' + pageId).show();
		currentPage = pageId;

		$('#prevBtn').show();
	}

	else if (currentPage == "appscr69") {
		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var pageId = getNextPage();
		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;

		appscr69FromMongodbJSON(currentPersonIndex + 1);
		selectIncomesCheckBoxes();
		appscr69FromMongodbJSON(currentPersonIndex + 1);
		$('#prevBtn').show();
	}

	else if (currentPage == "appscr70") {
		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var pageId = getNextPage();
		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;

		var otherIncomeIndicator = personObject.detailedIncome.otherIncomeIndicator;
		if(otherIncomeIndicator != null && otherIncomeIndicator == true) {
			$('#Financial_OtherIncome').prop("checked", true);
		}
		else {
			$('#Financial_OtherIncome').prop("checked", false);
		}

		var financialAlimonyReceivedIndicator = personObject.detailedIncome.alimonyReceivedIndicator;
		if(financialAlimonyReceivedIndicator != null && financialAlimonyReceivedIndicator == true) {
			$('#Financial_AlimonyReceived').prop("checked", true);
		}
		else {
			$('#Financial_AlimonyReceived').prop("checked", false);
		}

		var farmFishingIncomeIndictor = personObject.detailedIncome.farmFishingIncomeIndictor;
		if(farmFishingIncomeIndictor != null && farmFishingIncomeIndictor == true) {
			$('#Financial_FarmingOrFishingIncome').prop("checked", true);
		}
		else {
			$('#Financial_FarmingOrFishingIncome').prop("checked", false);
		}

		var rentalRoyaltyIncomeIndicator = personObject.detailedIncome.rentalRoyaltyIncomeIndicator;
		if(rentalRoyaltyIncomeIndicator != null && rentalRoyaltyIncomeIndicator == true) {
			$('#Financial_RentalOrRoyaltyIncome').prop("checked", true);
		}
		else {
			$('#Financial_RentalOrRoyaltyIncome').prop("checked", false);
		}

		var investmentIncomeIndicator = personObject.detailedIncome.investmentIncomeIndicator;
		if(investmentIncomeIndicator != null && investmentIncomeIndicator == true) {
			$('#Financial_InvestmentIncome').prop("checked", true);
		}
		else {
			$('#Financial_InvestmentIncome').prop("checked", false);
		}

		var capitalGainsIndicator = personObject.detailedIncome.capitalGainsIndicator;
		if(capitalGainsIndicator != null && capitalGainsIndicator == true) {
			$('#Financial_Capitalgains').prop("checked", true);
		}
		else {
			$('#Financial_Capitalgains').prop("checked", false);
		}

		var retirementOrPensionIndicator = personObject.detailedIncome.retirementOrPensionIndicator;
		if(retirementOrPensionIndicator != null && retirementOrPensionIndicator == true) {
			$('#Financial_Retirement_pension').prop("checked", true);
		}
		else {
			$('#Financial_Retirement_pension').prop("checked", false);
		}

		var unemploymentBenefitIndicator = personObject.detailedIncome.unemploymentBenefitIndicator;
		if(unemploymentBenefitIndicator != null && unemploymentBenefitIndicator == true) {
			$('#Financial_Unemployment').prop("checked", true);
		}
		else {
			$('#Financial_Unemployment').prop("checked", false);
		}

		var socialSecurityBenefitIndicator = personObject.detailedIncome.socialSecurityBenefitIndicator;
		if(socialSecurityBenefitIndicator != null && socialSecurityBenefitIndicator == true) {
			$('#Financial_SocialSecuritybenefits').prop("checked", true);
		}
		else {
			$('#Financial_SocialSecuritybenefits').prop("checked", false);
		}

		var selfEmploymentIncomeIndicator = personObject.detailedIncome.selfEmploymentIncomeIndicator;
		if(selfEmploymentIncomeIndicator != null && selfEmploymentIncomeIndicator == true) {
			$('#Financial_Self_Employment').prop("checked", true);
		}
		else {
			$('#Financial_Self_Employment').prop("checked", false);
		}


		var selfEmploymentIncomeIndicator = personObject.detailedIncome.selfEmploymentIncomeIndicator;
		if(selfEmploymentIncomeIndicator != null && selfEmploymentIncomeIndicator == true) {
			$('#Financial_Job').prop("checked", true);
		}
		else {
			$('#Financial_Job').prop("checked", false);
		}

		if ($('#Financial_Job').prop("checked")== true) {
			$('#Financial_Job_Div').show();
			$('#Financial_Job_EmployerName_id').val(personObject.detailedIncome.jobIncome[0].employerName);
			$('#Financial_Job_id').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes)));
			$('#Financial_Job_select_id').val(personObject.detailedIncome.jobIncome[0].incomeFrequency);
			$('#Financial_Job_HoursOrDays_id').val(personObject.detailedIncome.jobIncome[0].workHours);

			showDiductionsFields();
		}
		else {
			$('#Financial_Job_Div').hide();
			$('#Financial_Job_EmployerName_id').val("");
			$('#Financial_Job_HoursOrDays_id').val("");
			$('#Financial_Job_id').val("");
			$('#Financial_Job_select_id').val("");

		}


		if ($('#Financial_Self_Employment').prop("checked")== true) {
			$('#Financial_Self_Employment_Div').show();
			$('#Financial_Self_Employment_TypeOfWork').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.selfEmploymentIncome)));

			//showDiductionsFields();
		}
		else {
			$('#Financial_Self_Employment_Div').hide();
			$('#Financial_Self_Employment_TypeOfWork').val("");

		}
	}

	else if (currentPage == "appscr71") {
		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var pageId = getNextPage();
		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;

		personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes = $('#Financial_Job_id').val();
		personObject.detailedIncome.jobIncome[0].incomeFrequency = $('#Financial_Job_select_id').val();

		continueData71Json(currentPersonIndex+1)

		totalIncomeAt71(currentPersonIndex+1);

		var alimonyIncome = personObject.detailedIncome.deductions.alimonyDeductionAmount;
		if(alimonyIncome != null && alimonyIncome != "") {
			$('#appscr72alimonyPaid').prop("checked", true);
			appscr72CheckBoxAction($('#appscr72alimonyPaid'));
			$('#alimonyAmountInput').val(alimonyIncome);
			$('#alimonyFrequencySelect').val(householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency);

		}
		else {
			$('#appscr72alimonyPaid').prop("checked", false);
			appscr72CheckBoxAction($('#appscr72alimonyPaid'));
			$('#alimonyAmountInput').val(null);
			$('#alimonyFrequencySelect').val("SelectFrequency");
		}


		var studentLoanDeductionAmount = personObject.detailedIncome.deductions.studentLoanDeductionAmount;
		if(studentLoanDeductionAmount != null && studentLoanDeductionAmount != "") {
			$('#appscr72studentLoanInterest').prop("checked", true);
			appscr72CheckBoxAction($('#appscr72studentLoanInterest'));
			$('#studentLoanInterestAmountInput').val(studentLoanDeductionAmount);
			$('#studentLoanInterestFrequencySelect').val(householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency);
		}
		else {
			$('#appscr72studentLoanInterest').prop("checked", false);
			appscr72CheckBoxAction($('#appscr72alimonyPaid'));
			$('#studentLoanInterestAmountInput').val(null);
			$('#studentLoanInterestFrequencySelect').val("SelectFrequency");
		}

		var otherDeductionAmount = personObject.detailedIncome.deductions.otherDeductionAmoun;
		if(otherDeductionAmount != null && otherDeductionAmount != "") {
			$('#appscr72otherDeduction').prop("checked", true);
			appscr72CheckBoxAction($('#appscr72otherDeduction'));
			$('#deductionAmountInput').val(otherDeductionAmount);
			$('#deductionFrequencySelect').val(householdMemberObj.detailedIncome.deductions.otherDeductionFrequency);

			$('#deductionTypeInput').val("");
		}
		else {
			$('#appscr72otherDeduction').prop("checked", false);
			appscr72CheckBoxAction($('#appscr72otherDeduction'));
			$('#deductionAmountInput').val(null);
			$('#deductionFrequencySelect').val("SelectFrequency");
			$('#deductionTypeInput').val("");
		}
	}

	else if (currentPage == "appscr72") {
		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var pageId = getNextPage();
		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;
	}

	else if (currentPage == "appscr73") {
		var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		if(editData == true) { // code repeated. TBD
			$('#' + currentPage).hide();
			editData = false;
			$('#prevBtn').show();
			currentPage = "appscr74";
			$('#' + currentPage).show();
			showFinancialDetailsSummaryJSON();
		}

		else if(currentPersonIndex < householdSize -1  ) {
			currentPersonIndex ++;
			$('#' + currentPage).hide();
			currentPage = "appscr69";

			var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
			$('.nameOfHouseHold').text(lastName);

			$('#' + currentPage).show();

			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);
		}


		else {
			$('#' + currentPage).hide();
			currentPage = "appscr74";
			$('#' + currentPage).show();
			showFinancialDetailsSummaryJSON();
		}

		editData = false; // resert this to show the back button
		$('#prevBtn').show();

	}


}


/********************/

function reverseFinancialSectionPages() {



	if(currentPage == "appscr74") {

		var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		currentPersonIndex = householdSize -1;

		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var pageId = getPrevPage();
		$('#' + currentPage).hide();
		currentPage = pageId;
		var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
		$('.nameOfHouseHold').text(lastName);
		$('#' + currentPage).show();
	}
	else if(currentPage == "appscr73") {
		var pageId = getPrevPage();
		$('#' + currentPage).hide();
		currentPage = pageId;
		$('#' + currentPage).show();
		totalIncomeAt71(currentPersonIndex+1);
	}

	else if(currentPage == "appscr72") {
		var pageId = getPrevPage();
		$('#' + currentPage).hide();
		currentPage = pageId;
		$('#' + currentPage).show();

		var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
		var employerName = personObject.detailedIncome.jobIncome[0].employerName;
		if(employerName != null && employerName != null) {
			$('#Financial_Job').prop("checked", true);
			$('#Financial_Job_Div').show();

			$('#Financial_Job_EmployerName_id').val(personObject.detailedIncome.jobIncome[0].employerName);
			$('#Financial_Job_id').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes)));
			$('#Financial_Job_select_id').val(personObject.detailedIncome.jobIncome[0].incomeFrequency);
			$('#Financial_Job_HoursOrDays_id').val(personObject.detailedIncome.jobIncome[0].workHours);
		}

		else {
			$('#Financial_Job').prop("checked", false);
			$('#Financial_Job_Div').show();
		}

	}

	else if(currentPage == "appscr71") {
		var pageId = getPrevPage();
		$('#' + currentPage).hide();
		currentPage = pageId;
		$('#' + currentPage).show();
	}

	else if(currentPage == "appscr70") {
		var pageId = getPrevPage();
		$('#' + currentPage).hide();
		currentPage = pageId;
		$('#' + currentPage).show();

		if(editData ==  true) {
			$('#prevBtn').hide();
		}
		else {
			$('#prevBtn').show();
		}
	}

	else if(currentPage == "appscr69") {
		var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		if(householdSize == 1 || currentPersonIndex == 0) {
			$('#' + currentPage).hide();
			currentPage = "appscr68";
			$('#' + currentPage).show();
		}

		else if(currentPersonIndex != 0) {
			currentPersonIndex --;

			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = "appscr73";
			$('#' + currentPage).hide();
			currentPage = pageId;
			var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
			$('.nameOfHouseHold').text(lastName);
			$('#' + currentPage).show();

		}

	}

}



function checkFinacialAssistance(){
	if($("#wantToGetHelpPayingHealthInsurance_id1").is(":checked") || $('#page55radio9').is(":checked")){
		webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = true;
			ssapApplicationType = "FINANCIAL";
			$(".non_financial_hide").css("display","");
			$(".financial_hide").css("display","none");
		}else{
			webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = false;
			ssapApplicationType = "NON_FINANCIAL";
			$(".non_financial_hide").css("display","none");
			$(".financial_hide").css("display","");

		}
}

$(document).ready(function(){
	hideNonFinacial();
	
	$("#sidebar ul").css("visibility","visible");
	
	$("input, select").attr("data-parsley-trigger","change");
	//removeRequiredValidation();
	$('.currency').mask('000,000,000,000.00', {reverse: true});
	$('.phoneNumber').mask('(000) 000-0000');
	$('.phoneNumberExt').mask('0000');	
	
	updateCoverageYear();
	
	$('.ssapTooltip').tooltip();
	/*$('#progressDiv').waypoint('sticky');*/
	calculateProgressBarHeight();
	if(applicationStatus == 'OP'){
		updateProgressBar();
	}
});


function removeRequiredValidation(){
	if(haveData == true || true){
		$('input, select').each(function(){
			$(this).removeAttr('required');
			$(this).removeAttr('parsley-required');
		});

		$('input[type=checkbox]').each(function(){
			$(this).removeAttr('parsley-mincheck');
		});

		$('input[name=householdContactAddress]').removeAttr('parsley-mincheck');
	}
}

function getNameByPersonId(personId){
	if(personId == null || personId == "") {
		return "";
	}
	var houseHoldname = "";
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[personId-1];
	if(householdMemberObj != undefined){
		houseHoldname = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
	}
	return houseHoldname;
}

function getCurrentDateTime(){
	return moment().format('MMMM D, YYYY h:mm:ss A');

}

function hideNonFinacial(){
	ssapApplicationType = $('#ssapApplicationType').val();
	if(ssapApplicationType == "NON_FINANCIAL" || webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator === false){

		//check no need for financial assistance
		$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
		$('#page55radio10').prop("checked", true);
		
		$(".non_financial_hide").css("display","none");
		
		if($("#exchangeName ").val() != "NMHIX"){
			//hide in id only
			$(".non_financial_hide_id").css("display","none");
			/*$('#progressDiv').css('top','78px');*/
		}
		
	}else{
		$(".financial_hide").css("display","none");
	}
	
	//only hide for nm
	if($("#exchangeName ").val() == "NMHIX"){
		$(".hide_nm").css("display","none");
	}else{
		$(".hide_nm").css("display","block");
	}
}

function getNoOfHouseHolds(){
	if(webJson == null || webJson == undefined || webJson == 'undefined'){
		return 0;
	}
	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	return noOfHouseHoldmember;
}

function updatePrimaryContactName(){
	
	var primaryContactObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	$('.primaryContactName').text(primaryContactObject.name.firstName+" "+primaryContactObject.name.middleName+" "+primaryContactObject.name.lastName);
}

window.ParsleyConfig = {
  errorsWrapper: '<div class="errorsWrapper"></div>',
  errorTemplate: '<span class="validation-error-text-container"></span>'
};

function pageValidation(){
	var formValidationID = $("#"+currentPage).find("form").attr("id");
	if(formValidationID !== undefined && parsleyPageValidation(formValidationID) == false){
			return;
	}
}

function parsleyPageValidation(parsleyPageID){
	validationCheck = $('#'+parsleyPageID).parsley().validate();
	var errorsWrapper = $('#'+parsleyPageID).find("div.errorsWrapper");
	if(validationCheck == true){
		errorsWrapper.hide();
	}else{
		$('#'+parsleyPageID).find("div.errorsWrapper").each(function(){
			if($(this).closest("div.controls").is(":visible") || $(this).closest("div.control-group").is(":visible")){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	}
	return validationCheck;
		
	
};


function trimSpaces(paraString){
	return $.trim(paraString);
}


/*function resetError(){
	if($("#"+currentPage).find("form").attr("id") !== undefined){
		$('#' + $("#" + currentPage).find("form").attr("id")).parsley().reset();
	}
}*/
function updateProgressBar(){
	if(currentPage == 'appscr51'){
		$('#progressDiv').hide();
		$('.triangle-isosceles').css('left','0px');
		return;
	}else{
		$('#progressDiv').show();
	}
	$('.triangle-isosceles').css('visibility','hidden');
	
	$('.triangle-isosceles').animate({opacity: "0"},'fast');

	
	var progressPercentage = calculatePagePercentage(); 
	$('.ssapProgressBar .bar').css('width', progressPercentage*100 + '%');
	
	
	$('.triangle-isosceles').animate({left:$('.ssapProgressBar').width()*progressPercentage - 23 + 'px'},300,function(){
		$('.triangle-isosceles').css('visibility','visible');
		$('.triangle-isosceles').text(Math.ceil(progressPercentage*100) + '%').animate({opacity: "1"},'fast');
	});
}


function calculateProgressBarHeight(){
	//height of masthead of nm and id are different.
	if(exchangeName === "NMHIX"){
		//new mexico
		if($('#navtopview').length > 0){
			$('#progressDiv').css("padding-top","62px");
		}else{
			$('#progressDiv').css("padding-top","32px");
		}	
	}else{
		//idaho
		if($('#navtopview').length > 0){
			$('#progressDiv').css("padding-top","77px");
		}else{
			$('#progressDiv').css("padding-top","48px");
		}
	}
}

function calculatePagePercentage(){
	var houseHoldMembercount = getNoOfHouseHolds();
	//var HouseHoldRepeat = webJson.singleStreamlinedApplication
	
	//4 repeated pages in household, 5 in income, 4 in additional
	var totalPages = pageArr.length + (houseHoldMembercount - 1) * 13;
	
	var currentPageIndex = pageArr.indexOf(currentPage) + 1;
	
	var familyRepeatCount = (nonFinancialHouseHoldDone - 1) * 4;
	var incomeRepeatCount = (financialHouseHoldDone - 1) * 5;
	var additionalRepeatCount = (addQuetionsHouseHoldDone - 1) * 4;
	
	var familyAdditionCount = (houseHoldMembercount - 1) * 4;
	var incomeAdditionCount = (houseHoldMembercount - 1) * 5;
	var additionalAdditionCount = (houseHoldMembercount - 1) * 4;
	
	//start application:'appscr51', 'appscr52', 'appscr53', 'appscr54', 'appscr55', 'appscr57', 'appscr58', 'appscr59A', 'appscr60'
	if(currentPageIndex < 10){
		return currentPageIndex/totalPages;
	//family and household repeat pages: 'appscr61', 'appscr62Part1',  'appscr63', 'appscr64'
	}else if(10 <= currentPageIndex && currentPageIndex < 14){
		return (currentPageIndex + familyRepeatCount) / totalPages;
	//non-repeat: 'appscr65', 'appscr66', 'appscrBloodRel','appscr67', 'appscr68' 	
	}else if(14 <= currentPageIndex && currentPageIndex < 19){
		return (currentPageIndex + familyAdditionCount) / totalPages;
	//income repeat pages: 'appscr69', 'appscr70', 'appscr71', 'appscr72', 'appscr73'	
	}else if(19 <= currentPageIndex && currentPageIndex < 24){
		return (currentPageIndex + incomeRepeatCount + familyAdditionCount) / totalPages;
	//non-repeat: 'appscr74', 'appscr75A'
	}else if(24 <= currentPageIndex && currentPageIndex < 26){
		return (currentPageIndex + familyAdditionCount + incomeAdditionCount) / totalPages;
	//additional repeat pages: 'appscr76P1', 'appscr77Part1', 'appscr76P2', 'appscr81B'	
	}else if(26 <= currentPageIndex && currentPageIndex < 30){
		return (currentPageIndex + additionalRepeatCount + familyAdditionCount + incomeAdditionCount) / totalPages;
	//'appscr83', 'appscr84','appscr85', 'appscr87'
	}else{
		return (currentPageIndex + familyAdditionCount + incomeAdditionCount + additionalAdditionCount) / totalPages;
	}
	

}


function getAmericanAlaskaTribeList(stateCode,dropdownFocused) {	
	$.ajax({
		type : "POST",
		url : theUrlForAmericanAlaskaTribe,
		data : {stateCode : stateCode}, 
		dataType:'json',
		success : function(response) {
			populateTribes(response, dropdownFocused);
		},error : function(e) {
			 var optionsstring = '<option value="">Tribe Name</option>';
				var i =0;
				$(dropdownFocused).html('');
				$(dropdownFocused).append(optionsstring);
		}
	});
}

function populateTribes(response, dropdownFocused) {	
    var optionsstring = '<option value="">Tribe Name</option>';
		var i =0;
		$.each(response, function(key, value) {
			var optionVal = key;/*+'#'+value;*/
			var selectedoption = (tribeCode == optionVal)? "selected" : "";			
			var options = '<option value="'+optionVal+'" '+selectedoption+'>'+ value +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$(dropdownFocused).html('');
		$(dropdownFocused).append(optionsstring);		
		noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];			
			if(householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator == true){
				var tribeCode = householdMemberObj.americanIndianAlaskaNative.tribeName;
				$('#tribeName'+i+ ' option[value='+tribeCode+']').attr('selected','selected');
			}
		}
		
}


function populateCountriesOfIssuance(){
	var response = $("#alCountryOfIssuances").val();
	response = $.parseJSON(response)
	var optionsstring = '<option value="">Country Name</option>';
	$.each(response , function(key, value) {
		var optionVal = key;/*+'#'+value;*/
		var options = '<option value="'+optionVal+'">'+ value +'</option>';
		optionsstring = optionsstring + options;
		
	});

	$('#tempcarddetailsCounty').html('');
	$('#tempcarddetailsCounty').append(optionsstring);
	
	$('#machinecarddetailsCounty').html('');
	$('#machinecarddetailsCounty').append(optionsstring);
	
	$('#arrivaldeprecordForeignCounty').html('');
	$('#arrivaldeprecordForeignCounty').append(optionsstring);
	
	$('#ForeignppI94County').html('');
	$('#ForeignppI94County').append(optionsstring);
	
	$('#certificatenonimmigrantF1-Student-I20County').html('');
	$('#certificatenonimmigrantF1-Student-I20County').append(optionsstring);	
		
}

function saveCurrentApplicantID(i){
	$('#houseHoldTRNo').val(parseInt(i));
}

function getCurrentApplicantID(){
	return parseInt($('#houseHoldTRNo').val());
}


function updateCoverageYear(){
	$('.coverageYear').text(getCoverageYear());
}

function getCoverageYear(){
	var coverageDate = new Date();
	var coverageMonth = coverageDate.getMonth()+1;
	
	var coverageYear = coverageDate.getFullYear();
	
	if(coverageMonth >= 11 || coverageYear == 2014){
		coverageYear++;
	}
	
	return coverageYear;
}


function validateSignature(){
	
	var esignature = $("#appscr85ESignature").val();
	var name = "";
	
	if($("#appscr85ESignature").closest('div.control-group').find('#esignErrorDiv').length == 0){
		$("#appscr85ESignature").closest('div.control-group').append('<div id="esignErrorDiv"></div>');
	};
	var esignCustomError = $("#appscr85ESignature").closest('div.control-group').find('#esignErrorDiv');
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	
	if(householdMemberObj != undefined){
		if(getFormattedname(householdMemberObj.name.middleName)!= ""){
			name = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		}
		else{
			name = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.lastName);
		}
	}
	
	if(trimSpaces(esignature.toLowerCase()) == trimSpaces(name.toLowerCase())){
		signatureValidation = true;
		esignCustomError.html('');		
	}
	else{
		esignCustomError.html('');
		esignCustomError.html('<span class="validation-error-text-container">Please Enter  valid Signature</span>');
		signatureValidation = false;
	}
}

function update26Indicator(){
	if($("#exchangeName").val().trim() == "Your Health Idaho"){
		
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
		
		var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
		for(var j=0;j<parimaryContactRelationArr.length;j++){
			
			var relatedMemberObj = householdMemberObj[parimaryContactRelationArr[j].relatedPersonId - 1];

			//only care relationship to PC
			if(parimaryContactRelationArr[j].individualPersonId == '1'){
				
				//No age check for PC, spouse and ward, so under26Indicator should be always true for them
				if(parimaryContactRelationArr[j].relation == '18' || parimaryContactRelationArr[j].relation == '01' || parimaryContactRelationArr[j].relation == '31' ){
					relatedMemberObj.under26Indicator = true;
				}else{
					var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);
					// if they are 26 years or over, mark under26Indicator as false
					if(ageGreaterThan26(dateOfBirth)){
												
						relatedMemberObj.under26Indicator = false;
					}
					else{
						relatedMemberObj.under26Indicator = true;
					}
				}
			}
		}
		
	}
}
