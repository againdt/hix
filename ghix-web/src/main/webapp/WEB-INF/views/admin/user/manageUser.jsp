<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>


<!--[if lt IE 9]>
	<link href="/hix/resources/css/ie8fixes.css" media="screen" rel="stylesheet" type="text/css" />
<![endif]-->
<%

String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>


	<div class="gutter10">
		<div class="row-fluid">
        <div class="span10">
            <h1 id="skip">Manage User <small>${resultSize} Total Users </small></h1>
			<br>
			<c:if test="${errorMsg != null && errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out></p>
			</c:if>
        </div>
		<div class="span2">
            <a href="<c:url value="/admin/user/add"/>" class="btn btn-primary span12" id="addUser" name="addUser">Add User</a>
        </div>
        
		</div><!-- end of .row-fluid -->
		
		<div class="row-fluid">
			<div id="sidebar" class="span3 gray">
				<div class="header">
					<h4><span id="rrb">Refine Results</span></h4>
				</div>
					<form method="POST" id="manageUserForm" name="manageUserForm" action="<c:url value="/admin/manage/users" />" class="form-vertical gutter10 lightgray" novalidate="novalidate" aria-describedby="rrb">
						<df:csrfToken/>
						
						<input type="hidden" id="sortBy" name="sortBy" value="id">
						<input type="hidden" id="sortOrder" name="sortOrder" value="DESC" >
						<input type="hidden" id="pageNumber" name="pageNumber" value="1">
						<input type="hidden" id="changeOrder" name="changeOrder" >
						<input type="hidden" id="pageSize" name="pageSize" value="${pageSize}">
						
						<div class="control-group">
							<label for="userName" class="control-label">User Name</label>
							<div class="controls">
								<input type="text" name="userName" id="userName" value="${searchCriteria.userName}"  class="input-medium" />
							</div><!-- end of controls -->
						</div><!-- end of control-group -->
			
			            <div class="control-group">
			                <label for="userType" class="control-label">User Type</label>
							<select name="userType" id="userType"  class="span12 ie8-select">
							<option value="" <c:if test="${searchCriteria.userType == ''}"> selected="selected" </c:if>>Any</option>
							<c:forEach var="roleDesc" items="${USER_TYPE_LIST}">
								<option value="<encryptor:enc value="${roleDesc.name}"/>" <c:if test="${searchCriteria.userType == roleDesc.name}"> selected="selected" </c:if>>${roleDesc.label}</option>
							</c:forEach>
							</select>
						</div>
						
						
						<div class="control-group">
			                <label for="status" class="control-label">Status</label>
							<select name="userStatus" id="userStatus"  class="span12">
							<option value="" selected="selected">Any</option>
								<option value="<encryptor:enc value="Active"/>" <c:if test="${searchCriteria.userStatus == 'Active'}"> selected="selected" </c:if>>Active</option>							
								<option value="<encryptor:enc value="Inactive"/>" <c:if test="${searchCriteria.userStatus == 'Inactive'}"> selected="selected" </c:if>>Inactive</option>
								<option value="<encryptor:enc value="Inactive-dormant"/>" <c:if test="${searchCriteria.userStatus=='Inactive-dormant'}"> selected="selected" </c:if>>Inactive-dormant</option>
								<option value="<encryptor:enc value="Pending"/>" <c:if test="${searchCriteria.userStatus=='Pending'}"> selected="selected" </c:if>>Pending</option>
							</select>
						</div>
						
						<div class="control-group">
			                <label for="lastUpdated" class="control-label">Last Updated On / Before</label>
							<input type="text" title="MM/DD/YYYY" value="${searchCriteria.lastUpdated}" 
                    		class="datepick input-small" name="lastUpdated" id="lastUpdated" onChange="setdate();" placeholder="MM/DD/YYYY" />
						</div>
			            
			            <div class="gutter10">
			                <input type="submit" value="Go" class="btn input-small offset3" id="submit" name="submit">
			            </div>
					</form>
				
				
			</div><!-- end of .span3 -->
			
			
			
			<div class="span9" id="rightpanel">
							
				<table id="userlistTbl" name="userlistTbl" class="table table-striped">
					<thead>
						<tr class="header">	
							<th class="sortable" scope="col" style="width: 150px;"><dl:sort customFunctionName="traverse" title="User Name" sortBy="userName" sortOrder="${sort_order}"></dl:sort></th>
						    
							<th scope="col" style="width:70px;">Role</th>									
							<th class="sortable" scope="col" style="width:150px;"><dl:sort customFunctionName="traverse" title="Email Address" sortBy="email" sortOrder="${sort_order}"></dl:sort></th>
							<th class="sortable" scope="col" style="width:100px;"><dl:sort customFunctionName="traverse" title="Last Update" sortBy="updated" sortOrder="${sort_order}"></dl:sort></th>
							<th class="sortable" scope="col" style="width:55px;"><dl:sort customFunctionName="traverse" title="Status" sortBy="status" sortOrder="${sort_order}"></dl:sort></th>
							<th class="sortable gear-wheel" scope="col" style="width:20px; text-align: right;"><i class="icon-cog"></i> <b class="caret"></b></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${userList}" var="user" varStatus="vs">
						<c:set var="userId" ><encryptor:enc value="${user.id}" isurl="true"/> </c:set>
						<tr>
							<c:choose>
							<c:when test="${user.isSuperUser==1}">
								<td class="breakword">${user.userName}</td>
							</c:when>
							
							<c:otherwise>
								<td class="breakword"><a href="<c:url value="/admin/manage/user/details?userId=${userId}" />">${user.userName}</a></td>
							</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${not empty user.userRole}">
									<c:forEach items="${user.userRole}" var="userRole">
										<c:if test="${userRole.isDefaultRole()==true}">
											<td class="breakword">${userRole.role.label}</td>
										</c:if>
									</c:forEach>
								</c:when> 
								<c:otherwise>
									<td class="breakword">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						<td class="breakword">${user.email}</td>
						<%-- <td class="breakword">${user.updated}</td> --%>
						
						<td class="breakword"><fmt:formatDate	value="${user.updated}" pattern="MM-dd-yyyy hh:mm aa" timeZone="<%=timeZone%>" /></td>
						<td class="" id="user_Status">
							<c:if test="${user.status == 'Active'}">
							Active
							</c:if>
							<c:if test="${user.status != 'Active'}">
							${user.status}		
							</c:if>
						
						</td>
                        <td>
							<c:if test="${user.isSuperUser!=1}">
								<span class="dropdown pull-right">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown"><i
										class="icon-cog"></i><i class="caret"></i><span class="hide">Action</span></a>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
										<li><a href="<c:url value="/admin/manage/user/details?userId=${userId}"/>" class="offset1"><i class="icon-pencil"></i> Edit</a></li>
										<%-- HIX-48036 <c:if test="${user.confirmed==1}"> 
											<li><a href="#" onclick="javascript:changeStatus('Deactivate','${userId}');" class="offset1 status">Deactivate</a></li>
										</c:if>
										<c:if test="${user.confirmed==0}">
											<li><a href="#" onclick="javascript:changeStatus('Activate','${userId}');" class="offset1">Activate</a></li>
										</c:if>	 --%>
										
									</ul>
								</span>
							</c:if>
						</td>
					</tr>
					</c:forEach>
					</tbody>
				</table>
                <div class="pagination txt-center">
                    <dl:paginate customFunctionName="traverse" resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" showDynamicPageSize="true"/>
                </div>
				<!-- pagination here -->
				<div class="center"></div>
				
				<!-- If no record -->
				<!-- <div class="alert alert-info">No matching records found.</div> -->
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
		</div> <!-- end of .gutter10-lr -->
		
<div id="dialog-modal" style="display: none;">
  <p>
	<c:if test="${not empty accountActivationLink}">
    	${accountActivationLink}
	</c:if>	
 <%--  Representative Name: ${sessionRepresentativeName} <br/>
  		Delegation Code is: ${sessionDelegationCode} <br/>
  		${generateactivationlinkInfo} --%>
  </p>
</div>

<script>
/*  $(document).ready(function(){
	$( ".dropdown-menu li a.status" ).click(function() {
		alert("Hi");
		 var change=confirm("Are you sure to change the status?");
		 var $this=$(this);
		 if(change){
			 $this.closest("td").prev().text($this.text());
		 }
	});
});  */



	/* $(document).ready(function() { 
		
		$('table[name="userlistTbl"]').tablesorter({ 
			// pass the headers argument and assing a object 
			headers: { 
				// assign the secound column (we start counting zero) 
				6: { 
					// disable it by setting the property sorter to false 
					sorter: false 
				}
			}

		}); 
	}); */
			
   if('${accountActivationLink}' != null && '${accountActivationLink}' != '')
		{
			 $( "#dialog-modal" ).dialog({
			  		modal: true,
			    	title: "Account activation email",
			    	buttons: {
			    	Ok: function() {
			    		$( this ).dialog( "close" );
			    	}
			    	}
			    });
		}
$('#submittedBefore').keypress(function(e){
  	if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }
    if (charCode > 31 && (charCode > 48 || charCode < 57)) { return false; }
    return true;
  
});

$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});	

function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
    	alert($('Session is invalid').text());
           rv = true;
    }
    return rv;
}

function traverse(url){
	var queryString = {};
	url.replace(
		    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
		    function($0, $1, $2, $3) { queryString[$1] = $3; }
		);
	 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
	 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
	 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
	 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
	 if( queryString["pageSize"] ) $("#pageSize").val(queryString["pageSize"]);
	
	$("#manageUserForm input[type=submit]").click();
}

function changeStatus(status,userId){
	 var change=confirm("Are you sure to change the status?");
	 if(change){
		var validateUrl = "<c:url value='/admin/manage/user/updateStatus'/>";
		var csrftoken = '${sessionScope.csrftoken}';
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				status:status,
				id : userId,
				csrftoken : csrftoken
			},
			success: function(response)
			{
				if(isInvalidCSRFToken(response)){
					return; 
				}         
				var url = "<c:url value='/admin/manage/users'/>";
			    window.location.assign(url);
			},
			error: function(e)
			{
				alert("Failed to Fetch Data");
			}
		});
	 }
}
function changeStatusText(status){
		$("#user_Status").val(status);
}
function setdate(){
	$("#submittedBefore").val($("#submittedBefore").val());
}
</script>
