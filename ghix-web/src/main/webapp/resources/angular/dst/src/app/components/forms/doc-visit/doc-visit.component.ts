import { DataKeepService } from '../../../services/data-keep.service';
import { Component, OnInit, Input } from '@angular/core';
import { InputSet } from "../../input-group/input-group.component";

@Component({
    selector: 'gi-doc-visit',
    template: `
        <fieldset name="doctorVisits">
            <legend><h2  class="question-header">{{"DOC_VISIT.question" | translate}}</h2></legend>

            <div class="col-12">
                <input-group [inputSet]="docvisits" (select)="receiveFrequency($event)" [picked]="picked_doctor_visitFrequency"></input-group>
            </div>
        </fieldset>
    `,
    styleUrls: ['./doc-visit.component.scss']
})
export class DocVisitComponent implements OnInit {
    @Input() picked_doctor_visitFrequency : string;
    public docvisits: InputSet[] = [
        {
            name: "docVisitFrequency",
            value: "LEVEL1",
            text: "INPUT_GROUP.visit.level1"
        },
        {
            name: "docVisitFrequency",
            value: "LEVEL2",
            text: "INPUT_GROUP.visit.level2",
        },
        {
            name: "docVisitFrequency",
            value: "LEVEL3",
            text: "INPUT_GROUP.visit.level3",
        },
        {
            name: "docVisitFrequency",
            value: "LEVEL4",
            text: "INPUT_GROUP.visit.level4",
        },
    ]

    //public picked_doctor_visitFrequency: string = this.dataKeep.personalization.doctor.visitFrequency;

    constructor(
        private dataKeep: DataKeepService,
    ) { }

    ngOnInit() {
    }

    public receiveFrequency(value: string) {
        // console.log(value)
        this.dataKeep.updatePersonalizedState("DOC_VISIT_FREQUENCY", "ADD", value)
    }


}
