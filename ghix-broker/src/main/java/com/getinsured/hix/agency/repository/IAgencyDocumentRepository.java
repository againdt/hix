package com.getinsured.hix.agency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.agency.AgencyDocument;

public interface IAgencyDocumentRepository extends RevisionRepository<AgencyDocument, Integer, Integer>, JpaRepository<AgencyDocument, Integer> {
	
	public AgencyDocument findById(Long id);
	
	@Query("FROM AgencyDocument d where d.agency.id = :agencyId and (d.activeFlag != 'N' or d.activeFlag is null) order by createdDate desc")
	List<AgencyDocument> findByAgencyId(@Param("agencyId") Long agencyId);
	
}