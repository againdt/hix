import { FURTHER_ACTION_LINKS } from '../../constants/dashboardModelTypes';

const states = {

    // ** STATES WITHOUT TRANSITIONS (FINAL STATES)
    's_stub': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_start_new_application',
                paragraphText: 'further_action.further_text.OE1',
                link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
            })
        }
    },

    // START APPLICATION GROUP
    's_startApplicationOE1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_start_new_application',
                paragraphText: 'further_action.further_text.OE1',
                link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
            })
        }
    },

    's_startApplicationOE5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_start_new_application',
                paragraphText: 'further_action.further_text.OE5',
                link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
            })
        }
    },

    // RESUME APPLICATION GROUP
    's_resumeApplicationOE2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_resume_application',
                paragraphText: 'further_action.further_text.OE2',
                link: FURTHER_ACTION_LINKS.RESUME_APPLICATION
            })
        }
    },

    // NO BUTTON GROUP
    's_noButtonOE3': {
        onEnter(sm, model) {
            sm.stop({
                btnText: '',
                paragraphText: 'further_action.further_text.OE3',
                link: ''
            })
        }
    },
    's_noButtonOE8': {
        onEnter(sm, model) {
            sm.stop({
                btnText: '',
                paragraphText: 'further_action.further_text.OE8',
                link: ''
            })
        }
    },
    's_noButtonOE10': {
        onEnter(sm, model) {
            sm.stop({
                btnText: '',
                paragraphText: 'further_action.further_text.OE10',
                link: ''
            })
        }
    },
    's_noButtonREN6': {
        onEnter(sm, model) {
            sm.stop({
                btnText: '',
                paragraphText: 'further_action.further_text.REN6',
                link: ''
            })
        }
    },

    // CONTINUE SHOPPING GROUP
    's_continueShoppingOE2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.OE2',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingOE3': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.OE3',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingOE4': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.OE4',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingOE11': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.OE11',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingOE13': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.OE13',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingQLE2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.QLE2',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingQLE5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.QLE5',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingQLE10': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.QLE10',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingSEP3': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.SEP3',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingSEP5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.SEP5',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingREN2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.REN2',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingREN3': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.REN3',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_continueShoppingREN5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue_shopping',
                paragraphText: 'further_action.further_text.REN5',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },

    // SHOP FOR PLANS GROUP
    's_shopForPlansOE1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_shop_for_plans',
                paragraphText: 'further_action.further_text.OE1',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_shopForPlansOE12': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_shop_for_plans',
                paragraphText: 'further_action.further_text.OE12',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_shopForPlansQLE6': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_shop_for_plans',
                paragraphText: 'further_action.further_text.QLE6',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_shopForPlansREN1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_shop_for_plans',
                paragraphText: 'further_action.further_text.REN1',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_shopForPlansREN6': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_shop_for_plans',
                paragraphText: 'further_action.further_text.REN6',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },

    // CHANGE PLANS GROUP
    's_changePlansOE15': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_change_plans',
                paragraphText: 'further_action.further_text.OE15',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_changePlansQLE8': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_change_plans',
                paragraphText: 'further_action.further_text.QLE8',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
            })
        }
    },
    's_changePlansQLE9': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_change_plans',
                paragraphText: 'further_action.further_text.QLE9',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
            })
        }
    },
    's_changePlansSEP6': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_change_plans',
                paragraphText: 'further_action.further_text.SEP6',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_changePlansREN4': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_change_plans',
                paragraphText: 'further_action.further_text.REN4',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },

    // REPORT A CHANGE GROUP
    's_reportAChangeQLE4': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_a_change',
                paragraphText: 'further_action.further_text.QLE4',
                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
            })
        }
    },
    's_reportAChangeQLE7': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_a_change',
                paragraphText: 'further_action.further_text.QLE7',
                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
            })
        }
    },
    's_reportAChangeQLE11': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_a_change',
                paragraphText: 'further_action.further_text.QLE11',
                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
            })
        }
    },
    's_reportAChangeSEP4': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_a_change',
                paragraphText: 'further_action.further_text.SEP4',
                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
            })
        }
    },
    's_reportAChangeOE9': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_a_change',
                paragraphText: 'further_action.further_text.OE9',
                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
            })
        }
    },

    // OTHER GROUP
    's_reportLifeEventAndShopOE5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_report_life_event_and_shop',
                paragraphText: 'further_action.further_text.OE5',
                link: FURTHER_ACTION_LINKS.REPORT_LIFE_EVENT_SHOP
            })
        }
    },
    's_confirmEventAndShopQLE1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_confirm_event_and_shop',
                paragraphText: 'further_action.further_text.QLE1',
                link: FURTHER_ACTION_LINKS.CONFIRM_EVENT
            })
        }
    },
    's_uploadDocumentsQLE3': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_upload_documents',
                paragraphText: 'further_action.further_text.QLE3',
                link: FURTHER_ACTION_LINKS.UPLOAD_DOCUMENTS
            })
        }
    },
    's_continueQLE3_2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_continue',
                paragraphText: 'further_action.further_text.QLE3_2',
                link: FURTHER_ACTION_LINKS.CONTINUE
            })
        }
    },
    's_provideAdditionalInfoQLE15': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_provide_additional_info',
                paragraphText: 'further_action.further_text.QLE15',
                link: FURTHER_ACTION_LINKS.VERIFICATION
            })
        }
    },
    's_confirmEventAndFinalizePlansSEP1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_confirm_event_and_finalize_plans',
                paragraphText: 'further_action.further_text.SEP1',
                link: FURTHER_ACTION_LINKS.CONFIRM_EVENT
            })
        }
    },

    // CONTACT US GROUP
    's_contactUsOE1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_contact_us',
                paragraphText: 'further_action.further_text.OE1',
                link: FURTHER_ACTION_LINKS.CONTACT_US
            })
        }
    },
    's_contactUsOE5': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_contact_us',
                paragraphText: 'further_action.further_text.OE5',
                link: FURTHER_ACTION_LINKS.CONTACT_US
            })
        }
    },
    's_contactUsOE9': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_contact_us',
                paragraphText: 'further_action.further_text.OE9',
                link: FURTHER_ACTION_LINKS.CONTACT_US
            })
        }
    },
    's_contactUsQLE11': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_contact_us',
                paragraphText: 'further_action.further_text.QLE11',
                link: FURTHER_ACTION_LINKS.CONTACT_US
            })
        }
    },

    // FINALIZE PLANS GROUP
    's_finalizePlansSEP2': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_confirm_finalize_plans',
                paragraphText: 'further_action.further_text.SEP2',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_finalizePlansREN1': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_confirm_finalize_plans',
                paragraphText: 'further_action.further_text.REN1',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },
    's_finalizePlansREN6': {
        onEnter(sm, model) {
            sm.stop({
                btnText: 'further_action.cta.btn_confirm_finalize_plans',
                paragraphText: 'further_action.further_text.REN6',
                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
            })
        }
    },

    // ** STATES WITH TRANSITIONS
    's_tabIsInsideOEEnrollmentWindow': {
        transitions: [
            't_tabIsInsideOE',
            't_tabIsOutsideOE'
        ]
    },

    // >> High level Flow OE
    's_periodTypeOE': {
        transitions: [
            't_OE_AppType',
            't_periodTypeOE_2',
            't_periodTypeOE_3',
            't_periodTypeOE_4',
            't_periodTypeOE_5'

        ]
    },
    's_OEAppType': {
        transitions: [
            't_OE_OEP',
            't_OE_QEP',
            't_OE_SEP',
        ]
    },

    // OE OEP
    's_isRenewalAppSet': {
        transitions: [
            't_renewalAppSet',
            't_renewalAppNotSet',
        ]
    },
    's_OE_OEP_isQHPEligible': {
        transitions: [
            't_OE_OEP_QHPEligible',
            't_OE_OEP_QHPNotEligible'
        ]
    },

    's_OE_OEP_RenewalsAppStatus': {
        transitions: [
            't_OE_OEP_RenewalsAppStatusER',
            't_OE_OEP_RenewalsAppStatusPN',
            't_OE_OEP_RenewalsAppStatusEN'
        ]
    },
    's_OE_OEP_RenewalsAppStatusER_isDentalAppStatusEN': {
        transitions: [
            't_OE_OEP_RenewalsAppStatusER_dentalAppStatusEN',
            't_OE_OEP_RenewalsAppStatusER_dentalAppStatusNotEN'
        ]
    },

    's_OE_OEP_RenewalsAppStatusEN_isDentalAppStatusEN': {
        transitions: [
            't_OE_OEP_RenewalsAppStatusEN_dentalAppStatusEN',
            't_OE_OEP_RenewalsAppStatusEN_dentalAppStatusNotEN'
        ]
    },
    's_OE_OEP_NotRenewedAppStatus': {
        transitions: [
            't_OE_OEP_NotRenewedAppStatusER',
            't_OE_OEP_NotRenewedAppStatusPN',
            't_OE_OEP_NotRenewedAppStatusEN'
        ]
    },
    's_OE_OEP_NotRenewedER': {
        transitions: [
            't_OE_OEP_NotRenewed_ER_dentalAppStatusEN',
            't_OE_OEP_NotRenewed_ER_dentalAppStatusNotEN'
        ]
    },
    's_OE_OEP_NotRenewedEN': {
        transitions: [
            't_OE_OEP_NotRenewed_EN_dentalAppStatusEN',
            't_OE_OEP_NotRenewed_EN_dentalAppStatusNotEN'
        ]
    },

    // OE QEP
    's_OE_QEP_isEventVerified': {
        transitions: [
            't_OE_QEP_eventVerified',
            't_OE_QEP_eventNotVerified'
        ]
    },

    // OE SEP
    's_OE_SEP_AppStatus': {
        transitions: [
            't_OE_SEP_AppStatusER',
            't_OE_SEP_AppStatusPN',
            't_OE_SEP_AppStatusEN'
        ]
    },
    's_OE_SEP_EN': {
        transitions: [
            't_OE_SEP_EN_dentalAppStatusEN',
            't_OE_SEP_EN_dentalAppStatusNotEN'
        ]
    },

    // >> High level Flow Outside OE
    's_periodTypeOutsideOE': {
        transitions: [
            't_OutsideOE_AppType',
            't_OutsideOE_2',
            't_OutsideOE_3',
            't_OutsideOE_4'
        ]
    },
    's_outsideOEAppType': {
        transitions: [
            't_OutsideOE_QEP',
            't_OutsideOE_SEP',
            't_OutsideOE_OEP'
        ]
    },

    // outsideOE QEP
    's_outsideOE_QEP': {
        transitions: [
            't_outsideOE_QEP_ER',
            't_outsideOE_QEP_PN',
            't_outsideOE_QEP_EN'
        ]
    },

    // outsideOE QEP >> ER
    's_outsideOE_QEP_ER': {
        transitions: [
            't_outsideOE_QEP_ER_eventIdentified',
            't_outsideOE_QEP_ER_eventNotIdentified',
        ]
    },
    's_outsideOE_QEP_ER_isSepOpen': {
        transitions: [
            't_outsideOE_QEP_ER_SEPOpen',
            't_outsideOE_QEP_ER_SEPNotOpen',
        ]
    },
    's_outsideOE_QEP_ER_isEventVerified': {
        transitions: [
            't_outsideOE_QEP_ER_eventVerified',
            't_outsideOE_QEP_ER_eventNotVerified',
        ]
    },
    's_outsideOE_QEP_ER_verificationStatus': {
        transitions: [
            't_outsideOE_QEP_ER_verificationStatusPending',
            't_outsideOE_QEP_ER_verificationStatusPendingInfo',
        ]
    },
    's_outsideOE_QEP_ER_isSupervisor': {
        transitions: [
            't_outsideOE_QEP_ER_supervisor',
            't_outsideOE_QEP_ER_notSupervisor',
        ]
    },
    's_outsideOE_QEP_ER_isEnrolledInDental': {
        transitions: [
            't_outsideOE_QEP_ER_enrolledInDental',
            't_outsideOE_QEP_ER_notEnrolledInDental',
        ]
    },
    's_outsideOE_QEP_ER_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_QEP_ER_dentalAppStatusEN',
            't_outsideOE_QEP_ER_dentalAppStatusNotEN'
        ]
    },

    // outsideOE QEP >> PN
    's_outsideOE_QEP_PN': {
        transitions: [
            't_outsideOE_QEP_PN_SEPOpen',
            't_outsideOE_QEP_PN_SEPNotOpen'
        ]
    },
    's_outsideOE_QEP_PN_isAIAN': {
        transitions: [
            't_outsideOE_QEP_PN_AIAN',
            't_outsideOE_QEP_PN_NotAIAN'
        ]
    },

    // outsideOE QEP >> EN
    's_outsideOE_QEP_EN': {
        transitions: [
            't_outsideOE_QEP_EN_SEPOpen',
            't_outsideOE_QEP_EN_SEPNotOpen'
        ]
    },
    's_outsideOE_QEP_EN_isEnrolledInDental': {
        transitions: [
            't_outsideOE_QEP_EN_enrolledInDental',
            't_outsideOE_QEP_EN_notEnrolledInDental'
        ]
    },
    's_outsideOE_QEP_EN_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_QEP_EN_dentalAppStatusEN',
            't_outsideOE_QEP_EN_dentalAppStatusNotEN'
        ]
    },

    // outsideOE OEP
    's_outsideOE_OEP': {
        transitions: [
            't_outsideOE_OEP_periodWithinDates',
            't_outsideOE_OEP_periodNotWithinDates',
        ]
    },
    's_outsideOE_OEP_isPriorOEStartDate': {
        transitions: [
            't_outsideOE_OEP_priorOEStartDate',
            't_outsideOE_OEP_notPriorOEStartDate',
        ]
    },
    's_outsideOE_OEP_AppStatus': {
        transitions: [
            't_outsideOE_OEP_ER',
            't_outsideOE_OEP_PN',
            't_outsideOE_OEP_EN',
        ]
    },
    's_outsideOE_OEP_notPriorOEStartDateAppStatus': {
        transitions: [
            't_outsideOE_OEP_notPriorOEStartDate_ER',
            't_outsideOE_OEP_notPriorOEStartDate_EN_PN',
        ]
    },
    's_outsideOE_OEP_notPriorOEStartDate_isAIAN': {
        transitions: [
            't_outsideOE_OEP_notPriorOEStartDate_AIAN',
            't_outsideOE_OEP_notPriorOEStartDate_NotAIAN'
        ]
},

    // outsideOE OEP >> ER
    's_outsideOE_OEP_ER': {
        transitions: [
            't_outsideOE_OEP_ER_dentalAppStatusEN',
            't_outsideOE_OEP_ER_dentalAppStatusNotEN'
        ]
    },

    // outsideOE OEP >> EN
    's_outsideOE_OEP_EN': {
        transitions: [
            't_outsideOE_OEP_EN_dentalAppStatusEN',
            't_outsideOE_OEP_EN_dentalAppStatusNotEN'
        ]
    },
    's_outsideOE_OEP_EN_isAIAN': {
        transitions: [
            't_outsideOE_OEP_EN_AIAN',
            't_outsideOE_OEP_EN_NotAIAN'
        ]
    },

    // outside SEP
    's_outsideOE_SEP': {
        transitions: [
            't_outsideOE_SEP_ER',
            't_outsideOE_SEP_PN',
            't_outsideOE_SEP_EN'
        ]
    },

    // outside SEP >> ER
    's_outsideOE_SEP_ER': {
        transitions: [
            't_outsideOE_SEP_ER_eventIdentified',
            't_outsideOE_SEP_ER_eventNotIdentified',
        ]
    },
    's_outsideOE_SEP_ER_isSepOpen': {
        transitions: [
            't_outsideOE_SEP_ER_SEPOpen',
            't_outsideOE_SEP_ER_SEPNotOpen',
        ]
    },
    's_outsideOE_SEP_ER_isInsideOEEnrollmentWindow': {
        transitions: [
            't_outsideOE_SEP_ER_insideOEEnrollmentWindow',
            't_outsideOE_SEP_ER_outsideOEEnrollmentWindow',
        ]
    },
    's_outsideOE_SEP_ER_insideOEEnrollmentWindow_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_SEP_ER_insideOEEnrollmentWindow_dentalAppStatusEN',
            't_outsideOE_SEP_ER_insideOEEnrollmentWindow_dentalAppStatusNotEN',
        ]
    },
    's_outsideOE_SEP_ER_isEventVerified': {
        transitions: [
            't_outsideOE_SEP_ER_eventVerified',
            't_outsideOE_SEP_ER_eventNotVerified',
        ]
    },
    's_outsideOE_SEP_ER_verificationStatus': {
        transitions: [
            't_outsideOE_SEP_ER_verificationStatusPending',
            't_outsideOE_SEP_ER_verificationStatusPendingInfo',
        ]
    },
    's_outsideOE_SEP_ER_isEnrolledInDental': {
        transitions: [
            't_outsideOE_SEP_ER_enrolledInDental',
            't_outsideOE_SEP_ER_notEnrolledInDental',
        ]
    },
    's_outsideOE_SEP_ER_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_SEP_ER_dentalAppStatusEN',
            't_outsideOE_SEP_ER_dentalAppStatusNotEN'
        ]
    },

    // outside SEP >> PN
    's_outsideOE_SEP_PN': {
        transitions: [
            't_outsideOE_SEP_PN_SEPOpen',
            't_outsideOE_SEP_PN_SEPNotOpen',
        ]
    },
    's_outsideOE_SEP_PN_isAIAN': {
        transitions: [
            't_outsideOE_SEP_PN_AIAN',
            't_outsideOE_SEP_PN_NotAIAN'
        ]
    },
    's_outsideOE_SEP_PN_notAIAN_isInsideOEEnrollmentWindow': {
        transitions: [
            't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow',
            't_outsideOE_SEP_PN_notAIAN_outsideOEEnrollmentWindow'
        ]
    },
    's_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_dentalAppStatusEN',
            't_outsideOE_SEP_PN_notAIAN_insideOEEnrollmentWindow_dentalAppStatusNotEN'
        ]
    },

    // outside SEP >> EN
    's_outsideOE_SEP_EN': {
        transitions: [
            't_outsideOE_SEP_EN_SEPOpen',
            't_outsideOE_SEP_EN_SEPNotOpen'
        ]
    },
    's_outsideOE_SEP_EN_isEnrolledInDental': {
        transitions: [
            't_outsideOE_SEP_EN_enrolledInDental',
            't_outsideOE_SEP_EN_notEnrolledInDental'
        ]
    },
    's_outsideOE_SEP_EN_isDentalAppStatusEN': {
        transitions: [
            't_outsideOE_SEP_EN_dentalAppStatusEN',
            't_outsideOE_SEP_EN_dentalAppStatusNotEN'
        ]
    },
    's_outsideOE_SEP_EN_isChangePlan2': {
        transitions: [
            't_outsideOE_SEP_EN_changePlan2',
            't_outsideOE_SEP_EN_notChangePlan2'
        ]
    },
};

export default states;