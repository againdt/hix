package com.getinsured.hix.entity.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEntityEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Invokes the REST calls for EE to ghix-entity
 */
@Component
public class EERestCallInvoker {

	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	
	/**
	 * retrieves site details by id
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return response DTO String containing site details
	 */
	public String retieveSiteById(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_SITE_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves siteDetails by enrollmentEntityRequestDTO
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return response DTO String containing site detail
	 */

	public String retrieveSiteDetails(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_SITE_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves siteDetails by enrollmentEntityRequestDTO
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return response DTO String containing enrollmentEntity detail
	 */
	public String retrieveEnrollmentEntityObject(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENROLLMENTENTITY_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Saves sitedetail
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return response DTO string with response as Success or failure
	 */
	public String saveSiteDetails(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_SITE_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	public String retrievePaymentDetails(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_PAYMENT_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
 	* Retrieves PaymentMethod object by moduleId and enitityType
 	*
 	* @param enrollmentEntityRequestDTO
 	* @return String response DTO String with PaymentMethod object
 	*/
 	public String retrieveBrokerPaymentDetails(PaymentMethodRequestDTO paymentMethodRequestDTO) {
	 	return ghixRestServiceInvoker.invokeRestService(paymentMethodRequestDTO,
	 	FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
 	}
	
	/**
	 * Retrieves PaymentMethod object by paymentId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	public String retrievePaymentDetailsbyId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_PAYMENT_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * save PaymentMethod object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	public String savePaymentDetails(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_PAYMENT_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
 	* save PaymentMethod object
 	*
 	* @param enrollmentEntityRequestDTO
 	* @return String response DTO String with PaymentMethod object
 	*/
 	public String saveBrokerPaymentDetails(PaymentMethods paymentMethods) {
	 	return ghixRestServiceInvoker.invokeRestService(paymentMethods,
	 	FinanceServiceEndPoints.SAVE_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
 	}
	
	/**
	 * save EnrollmentEntity object with changed Status
	 * 
	 * @param requestXML
	 * @return String response DTO String with EnrollmentEntity object
	 */
	public String saveEnrollmentEntityStatus(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_ENROLLMENTENTITY_STATUS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Retrieves EnrollmentEntity object by UserId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with EnrollmentEntity object
	 */
	public String retrieveEnrollmentEntityObjectByUserId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENTITY_ENROLLMENT_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves enrollmententity data in db
	 * 
	 * @param requestXML
	 * @return String response DTO String
	 */
	public String saveEnrollmentEntityInfo(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_ENROLLMENTENTITY_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Retrieves EnrollmentEntity History object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with List<Map<String,object>> object
	 */
	public String retrieveEnrollmentEntityHistoryByUserId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENTITY_ENROLLMENT_HISTORY_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve population served information
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            request DTO String
	 * @return String response DTO String
	 */
	public String getPopulationServedDetails(String enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_POPULATION_SERVED_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Saves population served object
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            request DTO String
	 * @return String response DTO String with saved population served object
	 */
	public String savePopulationServedDetails(String enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_POPULATION_SERVED_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves a list of enrollmententity sites
	 * 
	 * @param requestXML
	 * @return
	 */
	public String getListOfEnrollmentEntitySites(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_LIST_OF_ENROLLMENT_ENTITY_SITES, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves list of enrollment entity sites by site type
	 * 
	 * @param requestXML
	 * @return String EnrollmentEntityResponseDTO enrollmentEntityResponseDTO
	 */
	public String getListOfEnrollmentEntitySitesBySiteType(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_LIST_OF_ENROLLMENT_ENTITY_SITES_BY_SITE_TYPE, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to save Assister Detail
	 * 
	 * @param request
	 *            contains assister request DTO
	 * @return assister response DTO
	 */
	public String saveAssisterDetails(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Assister Detail
	 * 
	 * @param assisterRequestDTO
	 *            contains assister id
	 * @return assister response DTO
	 */
	public String getAssisterDetail(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DETAIL, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve list of Assisters for given Enrollment
	 * Entity
	 * 
	 * @param request
	 *            contains Enrollment Entity id
	 * @return assister response DTO
	 */
	public String getListOfAssistersByEnrollmentEntity(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.GET_LIST_OF_ASSISTERS_BY_EE, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * populates LeftNavigation Map
	 * 
	 * @param requestXML
	 * @return String EnrollmentEntityResponseDTO responseDTO
	 */
	public String populateNavigationMenuMap(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.POPULATE_NAVIGATION_MENU_MAP, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves site for an entity by type:primary or subsite
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String EnrollmentEntityResponseDTO responseDTO
	 */
	public String retrieveSiteByType(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_SITE_BY_TYPE, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves site by Id
	 * 
	 * @param requestXML
	 * @return String EnrollmentEntityResponseDTO enrollmentEntityResponseDTO
	 */
	public String getSiteById(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_SITE_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves enrollmentEntityContactDetails
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String EnrollmentEntityResponseDTO enrollmentEntityResponseDTO
	 */
	public String getEnrollmentEntityContactDetails(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENROLLMENT_ENTITY_CONTACT_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * save enrollmentEntity Contact data
	 * 
	 * @param String
	 *            requestXML
	 * @return EnrollmentEntityResponseDTO enrollmentEntityResponseDTO
	 */
	public String saveEnrollmentEntityContactDetails(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_ENROLLMENT_ENTITY_CONTACT_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves enrollmentEntityObject by Id
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequest
	 * @return String enrollmentEntityresponseDTo
	 */
	public String retrieveEnrollmentEntityObjectById(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENROLLMENTENTITY_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves Document data for an enrollmentEntity
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequest
	 * @return String enrollmentEntityResponseDTO
	 */
	public String retrieveEntityDocumentsObjectById(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENTITY_DOCUMENTS_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves document from ECM for EnrollmentEntity
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequest
	 * @return String enrollmentEntityResponseDTO
	 */
	public String retrieveDocumentsfromECMbyId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_DOCUMENTS_FROM_ECM_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves Document on ECM
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String enrollmentEntityResponseDTO
	 */
	public String saveDocumentsOnECM(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_DOCUMENTS_ON_ECM, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves Document for an enrollmentEntity
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String enrollmentEntityResponseDTO
	 */
	public String saveDocument(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_DOCUMENTS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves enrollmentEntity History for admin view
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String enrollmentEntityResponseDTO
	 */
	public String retrieveEnrollmentEntityHistoryForAdminByUserId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENTITY_ENROLLMENT_HISTORY_FOR_ADMIN_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves enrollmentEntityMap
	 * 
	 * @param requestXML
	 * @return String enrollmentEntityResponseDTO
	 */
	public String retrieveEnrollmentEntityMap(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ENROLLMENTENTITY_MAP, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * populates left navigation map
	 * 
	 * @param requestXML
	 * @return String assisterResponseDTO
	 */
	public String assisterPopulateNavigationMenuMap(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.ASSISTER_POPULATE_NAVIGATION_MENU_MAP, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Assister detail by given user id
	 * 
	 * @param assisterRequestDTO
	 *            contains user id of Assister
	 * @return assister response DTO
	 */
	public String getAssisterDetailByUserId(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DETAIN_BY_USER_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve all the Assisters in DB
	 * 
	 * @param requestXML
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssisterList(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ASSISTER_LIST, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to update Assister status
	 * 
	 * @param requestXML
	 *            contains status update related information
	 * 
	 * @return assister response DTO
	 */
	public String saveAssisterStatus(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_STATUS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to update Assister Detail
	 * 
	 * @param requestXML
	 *            contains Assister Information
	 * 
	 * @return assister response DTO
	 */
	public String updateAssisterDetail(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.UPDATE_ASSISTER_DETAIL, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Assister Status History
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssisterHistoryForAdminByUserId(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENTITY_ENROLLMENT_HISTORY_FOR_ADMIN_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assister details for admin view
	 * 
	 * @param assisterRequestDTO
	 * @return String enrollmentResponseDTO
	 */
	public String retrieveAssisterAdminDetailsById(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_ADMIN_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assister list for admin
	 * 
	 * @param String
	 *            requestXML
	 * @return String enrollmentEntityResponseDTO
	 */
	public String retrieveAssistersForExchangeAdmin(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ASSISTER_LIST_FOR_EXCHANGEADMIN, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assisterDetails for Exchange Admin
	 * 
	 * @param assisterRequestDTO
	 * @return String assisterResponseDTO
	 */
	public String retrieveAssistersDetailsForExchangeAdmin(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DETAIL_FOR_EXCHANGEADMIN, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves enrollmentEntityStatus for Exchange Admin
	 * 
	 * @param AssisterRequestDTO
	 *            assisterRequestDTO
	 * @return String assisterResponseDTO
	 */
	public String saveAssisterStatusForExchangeAdmin(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_STATUS_FOR_EXCHANGEADMIN, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * updates assister data
	 * 
	 * @param String
	 *            requestXML
	 * @return String assisterResponseDTO
	 */
	public String updateAssisterInformation(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.UPDATE_ASSISTER_INFORMATION, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assisterlist for search
	 * 
	 * @param String
	 *            requestXML
	 * @return String assisterResponseDTO
	 */
	public String retrieveSearchAssisterList(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_SEARCH_ASSISTER_LIST_FOR_EXCHANGEADMIN, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves AssisterDocument related data by Id
	 * 
	 * @param AssisterRequestDTO
	 *            enrollmentEntityRequest
	 * @return String assisterResponseDTO
	 */
	public String retrieveAssisterDocumentsObjectById(AssisterRequestDTO enrollmentEntityRequest) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequest, 
				EnrollmentEntityEndPoints.GET_ASSISTER_ADMIN_DOCUMENTS_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assisterDocuments from ECM by Id
	 * 
	 * @param AssisterRequestDTO
	 *            enrollmentEntityRequest
	 * @return String assisterResponseDTO
	 */
	public String retrieveAssisterDocumentsfromECMbyId(AssisterRequestDTO requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DOCUMENTS_FROM_ECM_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves assister Object by Id
	 * 
	 * @param enrollmentEntityRequest
	 * @return
	 */
	public String retrieveAssisterObjectById(AssisterRequestDTO requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves assisterDocument on ECM
	 * 
	 * @param AssisterRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String assisterResponseDTO
	 */
	public String saveAssisterDocumentsOnECM(AssisterRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_DOCUMENTS_ON_ECM, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * saves AssisterDocument
	 * 
	 * @param AssisterRequestDTO
	 *            enrollmentEntityRequestDTO
	 * @return String assisterResponseDTO
	 */
	public String saveAssisterDocument(AssisterRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_DOCUMENTS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * retrieves a list of Assisters by enrollmentEntity
	 * 
	 * @param String
	 *            requestXML
	 * @return String assisterResponseDTO
	 */
	public String getSearchListOfAssistersByEnrollmentEntity(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_SEARCH_LIST_OF_ASSISTERS_BY_EE, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Gets the list of sites,siteLanguages,siteLocationHours.
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String response DTO String containing list of
	 *         sites,siteLanguages,siteLocationHours.
	 */
	public String getAllSites(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ALL_SITE_DETAILS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Assister Activity Status History
	 * 
	 * @param assisterRequestDTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssisterActivityDetailsById(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_ACTIVITY_DETAILS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * save enrollment entity with document id
	 * 
	 * @param requestXML
	 * 
	 * @return String response DTO
	 */
	public String saveEnrollmentEntityWithDocument(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SAVE_ENROLLMENTENTITY_WITH_DOCUMENT, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * get document history for enrollment entity
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String response DTO
	 */
	public String retrieveEnrollmentEntityDocumentHistoryById(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_ENROLLMENT_DOCUMENT_HISTORY, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * delete document from broker document
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String response DTO
	 */
	public String deleteDocument(EnrollmentEntityRequestDTO enrollmentEntityRequest) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequest, 
				EnrollmentEntityEndPoints.DELETE_DOCUMENT, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Entities based on Search Criteria
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return enrollment entity response DTO
	 */
	public String searchEntities(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.SEARCH_ENTITIES, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Assisters based on Search Criteria
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return assister response DTO
	 */
	public String searchAssisters(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.SEARCH_ASSISTERS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Enrollment Entity Detail by given entity
	 * ID
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return enrollment entity response DTO
	 */
	public String retrieveEnrollmentEntityDetailByID(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.GET_ENROLLMENTENTITY_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve list of Assisters for given EE
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssistersByEnrollmentEntityId(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.GET_ASSISTERS_BY_ENROLLMENTENTITY, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * delete payment method by module id
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String response DTO
	 */
	public String deletePaymentMethod(EnrollmentEntityRequestDTO enrollmentEntityRequest) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequest, 
				EnrollmentEntityEndPoints.DELETE_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * updates enrollment entity for receive payment
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String response DTO
	 */
	public String updateEnrollmentEntity(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.UPDATE_RECEIVED_PAYMENT, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve list of Assisters for given EE
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveNoOfAssistersByEnrollmentEntityAndSite(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.GET_NO_OF_ASSISTERS_BY_ENROLLMENTENTITY, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve list of Assisters for given EE
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssisterDetailById(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DETAIL_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to designate Assister for given Individual
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String designateAssister(String requestXML) {
		return ghixRestServiceInvoker.invokeRestService(requestXML, 
				EnrollmentEntityEndPoints.DESIGNATE_ASSISTER, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve assister designation details for
	 * individual
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return <AssisterDesignateResponseDTO> instance.
	 */
	public AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DESIGNATION_DETAIL_BY_INDIVIDUAL_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString(), AssisterDesignateResponseDTO.class);
	}

	/**
	 * Invokes ghix-entity to retrieve assisters based on enrollment entity and
	 * site id
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveListOfAssistersByEnrollmentEntityAndSiteId(String request) {
		return ghixRestServiceInvoker.invokeRestService(request, 
				EnrollmentEntityEndPoints.GET_ASSISTERS_BY_ENROLLMENTENTITY_AND_SITE_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Individuals list for given
	 * EnrollmentEntity
	 * 
	 * @param enrollmentEntityRequestDTO
	 * 
	 * @return String
	 */
	public String getIndividualListForEE(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_INDIVIDUAL_LIST_FOR_ENROLLMENTENTITY, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve Individual Information for given id.
	 * 
	 * @param assisterRequest
	 * @return String
	 */
	public String retrieveExternalIndividualInfoById(String assisterRequest) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequest, 
				EnrollmentEntityEndPoints.GET_INDIVIDUAL_INFO_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve a list of individuals associated with
	 * assister
	 * 
	 * @param assisterRequestDTO
	 * @return assister response DTO
	 */
	public String getIndividualListForAssister(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_INDIVIDUAL_LIST_FOR_ASSISTER, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve assister designation details for
	 * individual
	 * 
	 * @param assister
	 *            request DTO
	 * 
	 * @return assister response DTO
	 */
	public String retrieveAssisterDesignationDetailsForRequest(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_DESIGNATION_DETAIL_FOR_REQUEST, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to save designated assister.
	 * 
	 * @param assisterRequestDTO
	 * @return String
	 */
	public String saveAssisterDesignation(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.SAVE_ASSISTER_DESIGNATION, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve assister based on UserId
	 * 
	 * @param assisterRequestDTO
	 * @return assister response DTO
	 */
	public String retrieveAssisterObjectByUserId(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_BY_USER_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
	public String retrieveAssisterRecordByUserId(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.GET_ASSISTER_RECORD_BY_USER_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}

	/**
	 * Invokes ghix-entity to retrieve list of Assister
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return
	 */
	public String retrieveListOfCEC(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(enrollmentEntityRequestDTO, 
				EnrollmentEntityEndPoints.GET_LIST_OF_CEC, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
	/**
	 * Invokes ghix-entity to re-assign Individuals
	 * 
	 * @param assisterRequestDTO
	 * @return
	 */
	public String reassignIndividuals(AssisterRequestDTO assisterRequestDTO) {
		return ghixRestServiceInvoker.invokeRestService(assisterRequestDTO, 
				EnrollmentEntityEndPoints.REASSIGN_INDIVIDUALS, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
	/**
	 * Migrate PaymentMethod object
	 *
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	public String migrateEntityPaymentDetails(PaymentMethodRequestDTO paymentMethodRequestDTO) {
			return ghixRestServiceInvoker.invokeRestService(paymentMethodRequestDTO,
							FinanceServiceEndPoints.MIGRATE_PAYMENT_METHOD_PCI, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
	public String AssisterForEmail(String email) {
		return ghixRestServiceInvoker.invokeRestService(email, 
				EnrollmentEntityEndPoints.GET_ASSISTER_FOR_EMAIL, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
	public String retrieveAssisterExportList() {
		return ghixRestServiceInvoker.invokeRestService(null, 
				EnrollmentEntityEndPoints.GET_ASSISTER_EXPORT_LIST, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
	}
	
}
