package com.getinsured.eligibility.qlevalidation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CancelTicketRequest;
import com.getinsured.hix.model.CancelTicketRequest.SubModuleName;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Service("tkmServiceUtils")
public class TkmServiceUtils {
	
	private static final String INITIATE_A_CALL_TO_CANCEL_TICKET = "Initiate a call to cancel ticket...";


	private static final Logger LOGGER = Logger.getLogger(TkmServiceUtils.class);
	
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	@Async
	public void cancelTickets(List<Integer> ticketEventIds, AccountUser lastUpdatedAccountUser, String reason){
		
		if (LOGGER.isInfoEnabled()){
			LOGGER.info(INITIATE_A_CALL_TO_CANCEL_TICKET);
		}
		
		CancelTicketRequest ctr = new CancelTicketRequest();
		ctr.setComment(reason);
		ctr.setLastUpdatedBy(lastUpdatedAccountUser.getId());
		ctr.setSubModuleIds(ticketEventIds);
		ctr.setSubModuleName(SubModuleName.APPLICANTEVENT);
		
		ghixRestTemplate.exchange(GhixEndPoints.TicketMgmtEndPoints.CANCEL_QLE_TICKET, 
				lastUpdatedAccountUser.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, ctr);
	}

}
