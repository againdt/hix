<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />

<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>

<%
Broker mybroker = (Broker)request.getAttribute("broker");
Date currentdate = new Date();
//if certification date is null, then renew button need not be shown, thus, initializing diffDays with 40 any number which is greater than 30
long diffDays = 40;
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();
}
Calendar calUpdated = Calendar.getInstance();
if(mybroker.getReCertificationDate()!=null){
calUpdated.set(mybroker.getReCertificationDate().getYear()+1900, mybroker.getReCertificationDate().getMonth(),  mybroker.getReCertificationDate().getDate());
long currentDateMilliSec = currentdate.getTime();
long updateDateMilliSec = calUpdated.getTimeInMillis();
diffDays = (updateDateMilliSec - currentDateMilliSec) / (24 * 60 * 60 * 1000);
System.out.println("test differ days "+diffDays);
}
%>

<script>
function getComment(comments)
{
	$('#commentlbl').html("<p> Loading Comment...</p>");
	comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
	comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
	$('#commentlbl').html("<p> "+ comments + "</p>");

}

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault();  // prevent the default action, e.g., following a link
		});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes *
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val;
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
</script>


<div class="gutter10">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
		</div><!--  end of row-fluid -->
	</div><!--  end l-page-breadcrumb -->

	<div class="row-fluid">
		<h1><a name="skip"></a>
			<c:choose>
				<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
					${broker.user.firstName}&nbsp;${broker.user.lastName}
				</c:when>
				<c:otherwise>
					${broker.firstName}&nbsp;${broker.lastName}
				</c:otherwise>
			</c:choose>
		</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">
				<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				<li class="active"><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				<c:if test="${CA_STATE_CODE}">
					<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
				</c:if>
				<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>
				<c:if test="${CA_STATE_CODE}">
					<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
				</c:if>
				<c:if test="${!CA_STATE_CODE && broker.user != null}">
					<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>
				<%-- 	<li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
				</c:if>
			</ul>
			<br>
			<c:if test="${!CA_STATE_CODE}">
				<div class="header">
					<h4><i class="icon-cog icon-white"></i><spring:message code="label.brkactions"/></h4>
				</div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
	            </ul>
	         </c:if>
		</div><!-- end of span3 -->

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.brkCertificationStatus"/></h4>
				<c:choose>
					<c:when test="${fn:containsIgnoreCase(broker.certificationStatus,'Incomplete')}">
					</c:when>
						<c:otherwise>
								<a class="btn btn-small pull-right"  title="<spring:message code="label.brkEdit"/>" href="<c:url value="/admin/broker/editcertificationstatus/${encBrokerId}"/>"><spring:message code="label.brkEdit"/></a>

						</c:otherwise>
					</c:choose>

			</div>

			<form class="form-horizontal" id="frmcertificationstatus" name="frmcertificationstatus" action="certificationstatus"  method="POST">

			<input type="hidden" id="documentId1" name="documentId1" value=""/>
				<div class="gutter10-tb">
					<table class="table table-border-none table-condensed verticalThead">
						<tbody>
						    <tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.brkNumber"/></td>
							    <td><strong>${broker.brokerNumber}</strong></td>
						    </tr>
						    <tr>
								<td class="span4 txt-right"><spring:message  code="label.brkApplicationSubmissionDate"/></td>
								<td><strong><fmt:formatDate value="${broker.applicationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<tr>
								<td class="span3 txt-right"><spring:message code="label.brkCertificationStatus"/></td>
								<td class="span6"><strong>${broker.certificationStatus}</strong></td>
							</tr>
							<tr>
								<td class="span3 txt-right"><spring:message code="label.brkCertificationNumber"/></td>
								<td class="span6"><strong>${broker.certificationNumber}</strong></td>
							</tr>
							<tr>
								<td class="span3 txt-right"><spring:message code="label.brkStartDate"/></td>
								<td class="span6"><strong><fmt:formatDate value= "${broker.certificationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<tr>
								<td class="span3 txt-right"><spring:message code="label.brkEndDate"/></td>
								<td class="span6"><strong><fmt:formatDate value= "${broker.reCertificationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<c:if test="${CA_STATE_CODE}">
								<tr>
									<td class="span3 txt-right"><spring:message code="label.brkDelegationCode"/></td>
									<td><strong>${broker.delegationCode}</strong></td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div><!-- end of .gutter10 -->
					<p><spring:message code="label.bkr.statusofcertificationapplication"/></p>
                    <div class="header">
                    <h4><spring:message code="label.bkr.certificationHistory"/></h4>
					</div>
					<div class="gutter10-tb">
					<display:table id="brokerStatusHistory" name="brokerStatusHistory" list="brokerStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
				           <display:column property="updated" titleKey="label.brkdate"  format="{0,date,MMM dd, yyyy}" sortable="false" />
				           <display:column property="previousCertificationStatus" titleKey="label.bkr.previousStatus" sortable="false" />
				           <display:column property="newCertificationStatus" titleKey="label.brkNewStatus" sortable="false" />
				           <display:column property="comments" titleKey="label.bkr.viewComment" sortable="false" />
				           <display:column titleKey="label.brkViewAttachment" >
				                 <c:choose>
				                 	<c:when test="${brokerStatusHistory.eoDeclarationDocumentId == null && brokerStatusHistory.contractDocumentId == null && brokerStatusHistory.supportingDocumentId == null}">
				                		  <spring:message code="label.brkNoAttachment"/>
				                	</c:when>
			                		<c:otherwise>
					                      <c:if test="${brokerStatusHistory.eoDeclarationDocumentId!=null}">
					                      		<c:set var="eoDeclarationDocumentId" ><encryptor:enc value="${brokerStatusHistory.eoDeclarationDocumentId}" isurl="true"/> </c:set>
				                                <a href="#" onClick="showdetail('${eoDeclarationDocumentId}');" id="edit_${brokerStatusHistory.eoDeclarationDocumentId}"><spring:message code="label.brkViewAttachmentEODeclPage"/></a>
				                                <br/>
				                          </c:if>
					                      <c:if test="${brokerStatusHistory.contractDocumentId!=null}">
					                      		<c:set var="contractDocumentId" ><encryptor:enc value="${brokerStatusHistory.contractDocumentId}" isurl="true"/> </c:set>
				                                <a href="#" onClick="showdetail('${contractDocumentId}');" id="edit_${brokerStatusHistory.contractDocumentId}"><spring:message code="label.brkViewAttachmentContract"/></a>
				                                <br/>
				                          </c:if>
					                      <c:if test="${brokerStatusHistory.supportingDocumentId!=null}">
					                      		<c:set var="supportingDocumentId" ><encryptor:enc value="${brokerStatusHistory.supportingDocumentId}" isurl="true"/> </c:set>
				                                <a href="#" onClick="showdetail('${supportingDocumentId}');" id="edit_${brokerStatusHistory.supportingDocumentId}"><spring:message code="label.brkViewAttachmentSupporting"/></a>
				                          </c:if>
				                    </c:otherwise>
			                     </c:choose>
						   </display:column>
					</display:table>
				</div><!-- end of .gutter10 -->

			</form>
		</div><!-- end of span9 -->
		</div>
	</div><!--  end of row-fluid -->

	<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.bkr.viewComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		 <label id="commentlbl" ></label>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.button.close"/></a>
	</div>
</div>
<script type="text/javascript">


		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm-dd-yyyy'
		});

 function showdetail(documentId) {
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="graydrkaction"><h4 class="margin0 pull-left"><spring:message  code='label.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/admin/broker/viewAttachment?encDocumentId="+documentId;
		      // window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		      window.location.assign(documentUrl);
		}
 }
 $(document).ready(function(){

		 	                $('#feedback-badge-right').click(function() {
	 	                        $dialog.dialog('open');
		 	                        return false;
	  	                });
	  });

</script>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>

  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>
