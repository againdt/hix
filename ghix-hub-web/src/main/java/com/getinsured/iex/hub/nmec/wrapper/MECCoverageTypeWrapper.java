package com.getinsured.iex.hub.nmec.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.MECCoverageType;
import com.getinsured.iex.hub.nmec.niem.niem.niem_core._2.InsuranceType;

public class MECCoverageTypeWrapper implements JSONAware{

	private String locationStateUSPostalServiceCode;
	private String mecVerificationCode;
	private List<InsuranceTypeWrapper> insuranceWrappers;
	
	public MECCoverageTypeWrapper(MECCoverageType mecCoverageType)
	{
		if(mecCoverageType != null)
		{
			if(mecCoverageType.getLocationStateUSPostalServiceCode() != null)
			{
				locationStateUSPostalServiceCode = mecCoverageType.getLocationStateUSPostalServiceCode().getValue().value();
			}
			
			if(mecCoverageType.getMECVerificationCode() != null)
			{
				mecVerificationCode = mecCoverageType.getMECVerificationCode().getValue().value();
			}
			
			List<InsuranceType> listInsuranceType = mecCoverageType.getInsurance();
			
			int responseCount = listInsuranceType.size();
			
			if(responseCount > 0)
			{
				insuranceWrappers = new ArrayList<InsuranceTypeWrapper>();
			}
			for(int i = 0; i < responseCount; i++)
			{
				InsuranceTypeWrapper insuranceTypeWrapper = new InsuranceTypeWrapper(listInsuranceType.get(i));
				insuranceWrappers.add(insuranceTypeWrapper);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("LocationStateUSPostalServiceCode", this.locationStateUSPostalServiceCode);
		obj.put("MECVerificationCode", this.mecVerificationCode);
		
		JSONArray insuranceTypeResponse = null;
		
		if(this.insuranceWrappers != null && this.insuranceWrappers.size() > 0){
			insuranceTypeResponse = new JSONArray();
			for(InsuranceTypeWrapper data : this.insuranceWrappers){
				insuranceTypeResponse.add(data);
			}
		}
		
		obj.put("InsuranceType", insuranceTypeResponse);
		return obj.toJSONString();
	}
}
