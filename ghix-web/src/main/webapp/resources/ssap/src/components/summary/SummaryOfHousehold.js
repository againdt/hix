import React from 'react';
import SummaryItem from './SummaryItem';

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

const SummaryOfHousehold = props => {
    return (
        <div className="usa-width-one-whole usa-grid bordered">
            <p style={{fontWeight: 'bold'}}>
                Members of a federally recognized tribe
            </p>
            {/*<div className="usa-grid usa-width-one-whole divider"/>*/}
            <div className="usa-width-one-whole">
                {
                    props.householdMembers.map((member, i) => {
                        const {
                            name,
                            americanIndianAlaskaNative: {
                                memberOfFederallyRecognizedTribeIndicator
                            }
                        } = member;
                        return (
                            <SummaryItem
                                key={`${name.firstName}_${i}`}
                                category={normalizeNameOfPerson(name)}
                                value={memberOfFederallyRecognizedTribeIndicator ? 'Yes' : 'No'}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />

                        );
                    })
                }
            </div>
        </div>
    );
};

export default SummaryOfHousehold;
