(function(){
    'use strict';

    angular.module('fileUploadServiceModule', []).service('fileUpload', fileUpload);

	fileUpload.$inject = ['$http'];
	
	function fileUpload($http){
		return {
			uploadFileToUrl: function(fileName, file, fields, uploadUrl){
				var fd = new FormData();
				fd.append(fileName, file);
				fd.append('csrftoken', $('#tokid').val());
			    if(fields != undefined){
			    	for(var i = 0; i < fields.length; i++){
			            fd.append(fields[i].name, fields[i].data);
			        };
			    }

			   return  $http.post(uploadUrl, fd, {
			        transformRequest: angular.identity,
			        headers: {
		        		'Content-Type': undefined
			        }
			    });
			}
		};
	}

	
})();