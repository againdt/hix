package com.getinsured.hix.planmgmt.service.jpa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.dto.plandisplay.DrugDataRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.plandisplay.util.PlanDisplayConstants;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.history.IssuerHistoryRendererImpl;
import com.getinsured.hix.planmgmt.repository.IExchgPartnerLookupRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerDocumentRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerServiceAreaReposiroty;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.email.IssuerFinancialInformationChangeEmail;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyPersist;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyReader;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyVO;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyValidator;
import com.getinsured.hix.util.DelegationResponseUtils;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Service("issuerService")
@Repository
@DependsOn("dynamicPropertiesUtil")
@Transactional
public class IssuerServiceImpl implements IssuerService, ApplicationContextAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerServiceImpl.class);
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String SORTING_DATE_FORMAT = "yyyy-MM-dd";

	private static final String STATUS_STRING = "status";
	private static final String REPNAME = "repname";
	private static final String REPTITLE = "reptitle";
	private static final String SORT_ORDER = "sortOrder";
	//private static final String MODEL_SORT_ORDER = "sort_order";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String SORT_BY = "sortBy";
	private static final String DEFAULT_SORT = "defaultSort";
	private static final String CHANGE_ORDER = "changeOrder";
	private static final int LISTING_NUMBER = 10;

	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;

	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private IExchgPartnerLookupRepository iExchgPartnerLookupRepository;
	@Autowired private IIssuerServiceAreaReposiroty iIssuerServiceAreaReposiroty;
	@Autowired private UserService userService;
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private ObjectFactory<DisplayAuditService> objectFactory;
	@Autowired private HistoryRendererService issuerHistoryRendererImpl;
	@Autowired private CommentService commentService;
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private DelegationResponseUtils delegateResponseUtils;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private AccountActivationService accountActivationService;
	@Autowired private IUserRepository iUserRepository;
	@Autowired private IIssuerDocumentRepository issuerDocumentRepository;
	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeService noticeService;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private SerffService serffService;
	@Autowired private NetworkTransparencyReader networkTransparencyReader;
	@Autowired private NetworkTransparencyPersist networkTransparencyPersist;
	@Autowired private NetworkTransparencyValidator networkTransparencyValidator;
	@Autowired private IZipCodeRepository iZipCodeRepository;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerFinancialInformationChangeEmail issuerFinancialInformationChangeEmail;
	@Autowired private PlanDisplayRestClient planDisplayRestClient;

	private String[] fileExtetionList = new String[]{"txt", "css", "xml", "csv", "png", "jpg", "jpeg", "bmp", "gif", "doc", "docx", "dot", "dotx", "xls", "xlsx", "ppt", "pptx", "pdf", "zip", "tar", "gz"};
	private ApplicationContext appContext;
		
	@Override
	public Issuer getIssuer(AccountUser user) throws InvalidUserException {

		String userName = user.getUserName();

		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("Get Issuer Object for the User ::" + SecurityUtil.sanitizeForLogging(String.valueOf(userName)));
		}

		if (!userService.hasUserRole(user, RoleService.ISSUER_REP_ROLE)) {
			LOGGER.error("Not an Issuer Representative ::" + SecurityUtil.sanitizeForLogging(String.valueOf(userName)));
			throw new InvalidUserException("Not an Issuer Representative ::"+ SecurityUtil.sanitizeForLogging(String.valueOf(userName)));
		}

		IssuerRepresentative issuerRep = iIssuerRepresentativeRepository.findByUserRecord(user);

		if (issuerRep == null) {
			LOGGER.error("Issuer Representative Not found for the user :: " + SecurityUtil.sanitizeForLogging(String.valueOf(userName)));
			throw new InvalidUserException("Issuer Representative Not found for the user :: "
					+ userName);
		}
		Issuer issuerOfloggedInRep = issuerRep.getIssuer();

		if (issuerOfloggedInRep == null) {
			LOGGER.error("Issuer Not found for the user :: " + userName);
			throw new InvalidUserException("Issuer Not found for the user :: " + SecurityUtil.sanitizeForLogging(String.valueOf(userName)));
		}

		return issuerOfloggedInRep;
	}

	/*
	 * to be deprecated the Integer getIssuerId(); instad use Issuer getIssuerId(AccountUser user)
	 * @see com.getinsured.hix.planmgmt.service.IssuerService#getIssuerId()
	 */
	@Override
	public Integer getIssuerId(){
		AccountUser user = null;
		int issuerId = 0;
		try{
			user = userService.getLoggedInUser();
			// bcoz. now onwards there will not be any login for issuer, so
			// ISSUER_ROLE changed to ISSUER_REP_ROLE
			if (userService.hasUserRole(user, ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString()) ||
					userService.hasUserRole(user, ActivationJson.OBJECTTYPE.ISSUER_ENROLLMENT_REPRESENTATIVE.toString())) { // if
																				// user
																				// is
																				// issuer
																				// representative
				IssuerRepresentative issuerReps = iIssuerRepresentativeRepository.findByUserRecord(user);
				if(issuerReps != null){
					Issuer issuerOfloggedInRep = issuerReps.getIssuer();
					issuerId = issuerOfloggedInRep.getId();
				}
			}
			// following code is commented, bcoz. now onwards there will not be
			// any login for issuer
			/*else {																	// if user is issuer representative
				List<ModuleUser> repUser = userService.getUserModules(user.getId(), ModuleUserService.ISSUER_MODULE);
				if(!repUser.isEmpty()){
					issuerId = repUser.get(0).getModuleId();
				}
				
			}*/
		}catch(Exception ex){
			LOGGER.error("Error occuers inside getIssuerId ", ex);
		}
		
		return issuerId;
	}

	/*@Override
	public boolean isIssuerLoggedIn() {
		AccountUser user = null;
		try{
			user = userService.getLoggedInUser();
			if(userService.hasUserRole(user, RoleService.ISSUER_REP_ROLE)){
				return true;
			}
		}catch(InvalidUserException ex){
			LOGGER.error("Issuer not logged in");
		}
		return false;
	}*/

	@Override
	public boolean isIssuerRepLoggedIn()  throws InvalidUserException{
		AccountUser user = null;
		user = userService.getLoggedInUser();
		
		if (userService.isIssuerRep(user))
		{
			user = null;
			return true;
		}
			
		user = null;
		return false;
	}

	// check duplicate issuer/representative on the basis of email id
	/*@Override
	public boolean isDuplicate(String userEmailId) {
		boolean duplicateUser = false;

		AccountUser useObj = userService.findByEmail(userEmailId);
		IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByEmail(userEmailId);

		if ((useObj != null) || (issuerRepObj != null)) {
			duplicateUser = true;
		}
		return duplicateUser;
	}*/
	
	@Override
	public boolean isDuplicate(String userEmailId) {
		boolean duplicateUser = true;
		int duplicateCount = 0;
		EntityManager em = null;
		try{
			em = emf.createEntityManager();
			StringBuilder duplicateQuery = new StringBuilder("");
			duplicateQuery.append("SELECT COUNT(CHECKED) FROM (");

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				duplicateQuery.append("SELECT CASE WHEN (COUNT(*)=0) THEN 1 ELSE 2 END as CHECKED");
				duplicateQuery.append(" FROM ISSUER_REPRESENTATIVE WHERE EMAIL = ");
				duplicateQuery.append(":userEmailId");
				duplicateQuery.append(" UNION SELECT CASE WHEN (COUNT(*)=0) THEN 1 ELSE 3 END as CHECKED");
				duplicateQuery.append(" FROM USERS WHERE EMAIL = ");
				duplicateQuery.append(":userEmailId");
				duplicateQuery.append(" ) AS issuerReprData");
			}
			else {
			duplicateQuery.append("SELECT DECODE(COUNT(*),0,1,2) CHECKED FROM ISSUER_REPRESENTATIVE WHERE EMAIL = ");
			duplicateQuery.append(":userEmailId");
			duplicateQuery.append(" UNION SELECT DECODE(COUNT(*),0,1,3) CHECKED FROM USERS WHERE EMAIL = ");
			duplicateQuery.append(":userEmailId");
			duplicateQuery.append(")");
			}
			Query query = em.createNativeQuery(duplicateQuery.toString());
			query.setParameter("userEmailId", userEmailId);
			Iterator<?> rsIterator = query.getResultList().iterator();
			while (rsIterator.hasNext()) {
				duplicateCount = Integer.parseInt(rsIterator.next().toString());
			}
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("duplicateQueryCount: " + duplicateCount);
			}
			
			if(duplicateCount == PlanMgmtConstants.ONE){
				duplicateUser = false;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in isDuplicate >>>>>>>>>>>>>>>>"+ex.getMessage());
			throw new GIRuntimeException("Exception occured in isDuplicate >>>>>>>>>>>>>>>>"+ex.getMessage());
	    }finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return duplicateUser;
	}


	@Override
	public Issuer getIssuerById(Integer issuerId) {
		if (iIssuerRepository.exists(issuerId)) {
			return iIssuerRepository.findOne(issuerId);
		}
		return null;
	}

	@Override
	public boolean isIssuerExists(Issuer givenIssuer) {
		// check whether issuer info match with any existing issuer info in DB
			Issuer existingIssuer = iIssuerRepository.findIssuer(givenIssuer.getName(), givenIssuer.getLicenseNumber(), givenIssuer.getState());
			if (existingIssuer != null) {
				return true;
			}
		
		return false;
	}

	@Override
	public List<Issuer> findAllIssuers() {
		return iIssuerRepository.findAll();
	}

	@Override
	public List<String> getAllIssuerNames() {
		List<Object[]> issuerNamesList = iIssuerRepository.getAllIssuerNames();
		List<String> issuerNames = new ArrayList<String>();

		if (!(issuerNamesList.isEmpty())) {
			for (int i = 0; i < issuerNamesList.size(); i++) {
				if (issuerNamesList.get(i)[1] != null) {
					issuerNames.add(issuerNamesList.get(i)[1].toString());
				}
			}
		}
		return issuerNames;
	}

	@Override
	public List<Issuer> getAllIssuerNamesByInsuranceType(String insuranceType) {
		return iIssuerRepository.getAllIssuerNamesByInsuranceType(insuranceType);
//		List<String> issuerNames = new ArrayList<String>();
//
//		if (!(issuerNamesList.isEmpty())) {
//			for (int i = 0; i < issuerNamesList.size(); i++) {
//				if (issuerNamesList.get(i)[0] != null) {
//					issuerNames.add(issuerNamesList.get(i)[0].toString());
//				}
//			}
//		}
//		return issuerList;
	}

	
	@Override
	public String saveFile(String fileLocation, MultipartFile mfile, String suggestedFileName) throws IOException {
		LOGGER.info("saveFile()");
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String actualFileName = null;
		try{
			boolean isValidPath = planMgmtUtils.isValidPath(fileLocation);
			if(isValidPath){
				if (mfile.getSize() > 0) {
					if(LOGGER.isDebugEnabled()){
					   LOGGER.debug("Location of the file : " + SecurityUtil.sanitizeForLogging(String.valueOf(fileLocation)));
					}
					
					File fileDir = new File(fileLocation);
					// create Directory if not exists.
					if (!fileDir.exists()) {
						fileDir.mkdir();
					}
					String fileName = null;
					// fileName would be a.txt then it will be come a_123456789.txt i.e. a_TIMESTAMP.EXTENSION
					if (suggestedFileName == null) {
						actualFileName = FilenameUtils.getBaseName(mfile.getOriginalFilename()) + UNDERSCORE + TimeShifterUtil.currentTimeMillis() + DOT
								+ FilenameUtils.getExtension(mfile.getOriginalFilename());
					} else {
						actualFileName = suggestedFileName;
					}
					fileName = fileLocation + GhixConstants.FRONT_SLASH + actualFileName;
					inputStream = mfile.getInputStream();
					outputStream = new FileOutputStream(fileName);
					int readBytes = 0;
					byte[] buffer = new byte[10000];
					while ((readBytes = inputStream.read(buffer, 0, 10000)) != -1) {
						outputStream.write(buffer, 0, readBytes);
					}
					outputStream.close();
					inputStream.close();
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(IOException e){
			LOGGER.error("Error :"+e.getMessage());
		}finally{
			IOUtils.closeQuietly(outputStream);
			IOUtils.closeQuietly(inputStream);
		}	
		
		return actualFileName;
	}

	@Override
	public Issuer saveIssuer(Issuer issuer) {
		return iIssuerRepository.save(issuer);
	}

	@Override
	public String getAllCertifiedIssuerNames() {
		String selectStr = "";
		selectStr += "<Select name=\"issuer.id\" id=\"issuerId\">";
		List<Object[]> issuerNames = iIssuerRepository.findAllCertifiedIssuerNames();
		if (!(issuerNames.isEmpty())) {
			for (int i = 0; i < issuerNames.size(); i++) {
				selectStr += "<option value=\"" + issuerNames.get(i)[0] + "\">"
						+ issuerNames.get(i)[1] + "</option>";
			}

		}
		selectStr += "</select>";
		return selectStr;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Issuer> getAddedOrUpdatedIssuers() throws ParseException {

			String startdatetime;
			String enddatetime;
			Calendar cal = TSCalendar.getInstance();
			cal.add(Calendar.DATE, -1);

			SimpleDateFormat startformatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			SimpleDateFormat endformatter = new SimpleDateFormat("yyyy-MM-dd 23:59:59");

			startdatetime = startformatter.format(cal.getTime());
			enddatetime = endformatter.format(cal.getTime());

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date convertedStartDate = dateFormat.parse(startdatetime);
			Date convertedEndDate = dateFormat.parse(enddatetime);

			return iIssuerRepository.getAddedOrUpdatedIssuers(convertedStartDate, convertedEndDate);

		

	}

	@Override
	public List<Issuer> getIssuerList(HttpServletRequest request) {
		Integer pageSize = (Integer) request.getAttribute("pageSize");
		Integer startRecord = (Integer) request.getAttribute("startRecord");
		List<Issuer> issuerList = null;
		String status = request.getParameter(STATUS_STRING);
		String issuername = request.getParameter("issuerName");
		String sortBy = (request.getAttribute("sortBy") != null) ? request.getAttribute("sortBy").toString()
				: "";

		QueryBuilder<Issuer> queryObj = delegateFactory.getObject();
		queryObj.buildObjectQuery(Issuer.class);

		if (status != null && !status.equalsIgnoreCase("")) {
			queryObj.applyWhere("certificationStatus", status, DataType.STRING, ComparisonType.EQUALS);
		}
		if (issuername != null && !issuername.equalsIgnoreCase("")) {
			queryObj.applyWhere("name", issuername.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		queryObj.applySort("id", SortOrder.DESC);
		if (sortBy != null && !sortBy.isEmpty()) {
			queryObj.applySort(sortBy, SortOrder.DESC);
		}

		issuerList = queryObj.getRecords(startRecord, pageSize);
		request.setAttribute("resultSize", queryObj.getRecordCount().intValue());

		return issuerList;
	}

	@Override
	public Page<Issuer> getIssuerList(String issuerName, String status, Pageable pageable) {
		if(issuerName.equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
			issuerName = PlanMgmtConstants.ALL;
		}
		if(status.equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
			status = PlanMgmtConstants.ALL;
		}
		return iIssuerRepository.getAllIssuers(issuerName, status, pageable);
	}

	@Override
	public List<Plan> getPlanList(HttpServletRequest request, Integer issuerId) {

		Integer pageSize = (Integer) request.getAttribute("pageSize");
		Integer startRecord = (Integer) request.getAttribute("startRecord");
		List<Plan> planList = null;

		String status = request.getParameter(STATUS_STRING);
		String planName = request.getParameter("planName");
		String networkName = request.getParameter("networkName");

		QueryBuilder<Plan> queryObj = delegateFactory.getObject();
		queryObj.buildObjectQuery(Plan.class);

		if (issuerId != null) {
			queryObj.applyWhere("issuer.id", issuerId, DataType.NUMERIC, ComparisonType.EQ);
		}
		if (planName != null && !planName.equalsIgnoreCase("")) {
			queryObj.applyWhere("name", planName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		if (status != null && !status.equalsIgnoreCase("")) {
			queryObj.applyWhere(STATUS_STRING, status, DataType.STRING, ComparisonType.EQUALS);
		}
		if (networkName != null && !networkName.equalsIgnoreCase("")) {
			queryObj.applyWhere("network.name", networkName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		queryObj.applySort("id", SortOrder.DESC);

		planList = queryObj.getRecords(startRecord, pageSize);
		request.setAttribute("resultSize", queryObj.getRecordCount().intValue());

		return planList;
	}

	@Override
	public List<AccountUser> getRepListForIssuer(HttpServletRequest request, Integer issuerIdForRep) {
		List<AccountUser> authRepsOfIssuer = new ArrayList<AccountUser>();
		//EntityManager em = emf.createEntityManager();
		EntityManager em = null;

		// preparing SELECT query for fetching up representative of the given
		// issuer
		String getRepListForIssuerQuery = "SELECT u.* FROM Users u , Issuers i, Module_Users mu, User_Roles ur, Roles r "
				+ "WHERE (mu.user_id=u.id) AND (mu.module_id=i.id) AND (mu.user_id=ur.user_id) AND (ur.role_id=r.id) "
				+ " AND mu.module_id = i.id AND mu.user_id = u.id ";

		// for only given issuer
		getRepListForIssuerQuery += " AND i.id = " + issuerIdForRep;
		getRepListForIssuerQuery += " AND mu.module_name = '"
				+ ModuleUserService.ISSUER_MODULE + "'";

		// for only representative role
		getRepListForIssuerQuery += " AND r.name = '"
				+ RoleService.ISSUER_REP_ROLE + "'";

		// if the representative id is given, then just pick up his account info
		if ((request.getAttribute("repId") != null)
				&& !request.getAttribute("repId").toString().trim().isEmpty()) {
			getRepListForIssuerQuery += " AND u.id = "
					+ request.getAttribute("repId").toString().trim();
		}

		// when search for particular representative on the basis of name
		if ((request.getAttribute(REPNAME) != null)
				&& !request.getAttribute(REPNAME).toString().trim().isEmpty()) {
			getRepListForIssuerQuery += " AND ( UPPER(u.first_name) LIKE '%"
					+ request.getAttribute(REPNAME).toString().trim().toUpperCase()
					+ "%' OR ";
			getRepListForIssuerQuery += " UPPER(u.last_name) LIKE '%"
					+ request.getAttribute(REPNAME).toString().trim().toUpperCase()
					+ "%') ";
		}
		// when search for particular representative on the basis of title
		if ((request.getAttribute(REPTITLE) != null)
				&& !request.getAttribute(REPTITLE).toString().trim().isEmpty()) {
			getRepListForIssuerQuery += " AND UPPER(u.title) LIKE '%"
					+ request.getAttribute(REPTITLE).toString().trim().toUpperCase()
					+ "%'";
		}

		// apply sort/order
		if ((request.getAttribute("sortby") != null)
				&& !request.getAttribute("sortby").toString().trim().isEmpty()) {
			String sortby = request.getAttribute("sortby").toString().trim();
			if (sortby.equalsIgnoreCase(REPNAME)) {
				getRepListForIssuerQuery += " ORDER BY u.first_name, u.last_name "
						+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
			} else if (sortby.equalsIgnoreCase(REPTITLE)) {
				getRepListForIssuerQuery += " ORDER BY u.title "
						+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
			} else if (sortby.equalsIgnoreCase("updated")) {
				getRepListForIssuerQuery += " ORDER BY u.updated "
						+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
			} else if (sortby.equalsIgnoreCase(STATUS_STRING)) {
				getRepListForIssuerQuery += " ORDER BY u.confirmed "
						+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
			}
		} else if ((request.getAttribute(SORT_ORDER) != null)
				&& !request.getAttribute(SORT_ORDER).toString().trim().isEmpty()) {
			getRepListForIssuerQuery += " ORDER BY u.updated "
					+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
		}

		// actually firing the query
		try{
			em = emf.createEntityManager();
			List rsList = em.createNativeQuery(getRepListForIssuerQuery).getResultList();
			Iterator rsIterator = rsList.iterator();
			// iterating thru representative records
			while (rsIterator.hasNext()) {
				AccountUser authRepUser = new AccountUser();
				Object[] objArray = (Object[]) rsIterator.next();
	
				authRepUser.setId(Integer.parseInt(objArray[0].toString()));
				authRepUser.setCommunicationPref((objArray[1] == null) ? ""
						: objArray[1].toString());
				authRepUser.setConfirmed(Integer.parseInt(objArray[2].toString()));
				authRepUser.setCreated(DateUtil.StringToDate(objArray[3].toString(), TIMESTAMP_PATTERN));
				authRepUser.setEmail(objArray[4].toString());
				authRepUser.setFirstName(objArray[5].toString());
				authRepUser.setLastName((objArray[6] == null) ? null
						: objArray[6].toString());
				authRepUser.setRecovery(objArray[8].toString());
				authRepUser.setTitle((objArray[9] == null) ? null
						: objArray[9].toString());
				authRepUser.setUpdated(DateUtil.StringToDate(objArray[10].toString(), TIMESTAMP_PATTERN));
				authRepUser.setUserName((objArray[11] == null) ? null
						: objArray[11].toString());
				authRepUser.setPhone((objArray[12] == null) ? null
						: objArray[12].toString());
	
				authRepsOfIssuer.add(authRepUser);
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getRepListForIssuer >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getRepListForIssuer >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		request.setAttribute("resultSize", authRepsOfIssuer.size());
		return authRepsOfIssuer;
	}

	/* @author Raja
	* Fix of HIX-1709 Deleting a Authorized representative throws Application Error.
	* NOTE:  THE method deleteRepForIssuer(Integer repId) has invalid implementation
	*        and not referenced any where in the project..
	* 
	*
	@Override
	@Transactional
	public boolean deleteRepForIssuer(Integer repId) {


	try{
	
		
		ModuleUser moduleUser = iModuleUserRepository.findByUserId(repId);
		iModuleUserRepository.delete(moduleUser);
			
		UserRole userRole = iUserRoleRepository.findByUserId(repId);
		iUserRoleRepository.delete(userRole);
		
	}catch(Exception e){
		
		LOGGER.info("Invalid User Id ========" +e.getMessage());
	}

	return false;
	}*/

	@Override
	@Transactional
	public boolean updateRepStatus(Integer issuerRepId, String newIssuerRepStatus){
		boolean updateStatusFlag = false;
		try{
			IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findById(issuerRepId);
			issuerRepObj.setStatus(newIssuerRepStatus.toUpperCase());
			iIssuerRepresentativeRepository.save(issuerRepObj);
			updateStatusFlag = true;
		}catch(Exception ex){
			LOGGER.error("Exception occured in updatessuerRepStatus : ", ex);
		}
		return updateStatusFlag;
	}

	@GiFeature("ahbx.planmgmt.ind05.enabled")
	@Override
	@Transactional
	public void syncIssuerWithAHBX(int issuerId) {
			LOGGER.info("CALL AHBX issuer SYNC REQUEST ========");

			// call financial API to get default payment method
			PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
			paymentMethodRequestDTO.setModuleID(issuerId);
			paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
			paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);

			String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
			Map<String, Object> paymentMethodsMap = getPaymentMethodsMap(paymentMethodsString);
			List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethodsMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			PaymentMethods paymentMethod = null;
			if(paymentMethodList != null && !paymentMethodList.isEmpty()){
				paymentMethod = paymentMethodList.get(0);
			}
			
			String accountNumber = "";
			String accountName = "" ;
			String routingNumber = "" ;
			String bankName = "";
			if(paymentMethod != null){
				accountNumber = paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber();
				routingNumber = paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber();
				accountName = paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount();
				bankName = paymentMethod.getFinancialInfo().getBankInfo().getBankName();
			}
			// hence we don't have UI to set issuer financial data, so those are hard coded
			Issuer issuerdata = iIssuerRepository.findOne(issuerId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			
			
			Map<String, Object> requestData = new HashMap<String, Object>();
			requestData.put("accountName", accountName);
			requestData.put("accountNumber", accountNumber);	
			requestData.put("addressLine1", (issuerdata.getAddressLine1() == null) ? "" :  issuerdata.getAddressLine1());	
			requestData.put("addressLine2", (issuerdata.getAddressLine2() == null) ? "" :  issuerdata.getAddressLine2());	
			requestData.put("bankName", bankName);	
			requestData.put("city", (issuerdata.getCity() == null) ? "" : issuerdata.getCity());	
			requestData.put("effectiveEndDate", (issuerdata.getEffectiveEndDate() != null ) ? dateFormat.format(issuerdata.getEffectiveEndDate()) : ""); // issuer effective end  date could be null	
			requestData.put("effectiveStartDate", dateFormat.format(issuerdata.getEffectiveStartDate()));	
			requestData.put("issuerId", issuerdata.getHiosIssuerId()); // HIX-83646 -set replace internal Carrier ID with Issuer HIOS ID. 	
			requestData.put("issuerName", issuerdata.getName());	
			requestData.put("issuerRecordIndicator", (issuerdata.getLastUpdateTimestamp().equals( issuerdata.getCreationTimestamp())) ? "N" : "U");	
			requestData.put("paymentType", "B");	
			requestData.put("routingNumber", routingNumber);
			requestData.put("state", (issuerdata.getState() == null) ? "" : issuerdata.getState());
			requestData.put("zipCode", (issuerdata.getZip() == null) ? "" : issuerdata.getZip());
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Call issuer sync REST URL =========" + AHBXEndPoints.SYNC_ISSUER_DATA);
			}	
			ghixRestTemplate.postForObject(AHBXEndPoints.SYNC_ISSUER_DATA, requestData, String.class);
	}

	@Override
	public IssuerRepresentative saveRepresentative(IssuerRepresentative issuerRep) {
		return iIssuerRepresentativeRepository.save(issuerRep);
	}

	/* @author Raja
	 * @UserStory HIX:1811
	 * @HIX-3121
	 * the below methods which implements common pagination and sorting.
	 */

	// sets the pagination code and return the Pageable object
	@Override
	public Pageable getPaging(Model model, HttpServletRequest request) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Paging Begin");
		}
		// get the page number if exists else set default page number to 1 for
		// fetching the data.
		int pageNumber = 0;
		if (StringUtils.isNumeric(request.getParameter(PAGE_NUMBER))) {
			pageNumber = Integer.parseInt(request.getParameter(PAGE_NUMBER));
		}
		if (pageNumber < 1) {
			pageNumber = 1;
		}
		// create a PageRequest object for the specific page number and
		// no.of.records to display on the page (page size).
		Pageable pageable = new PageRequest(pageNumber - 1, GhixConstants.PAGE_SIZE);
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Paging End");
		}
		return pageable;
	}

	@Override
	public Sort getSorting(Model model, HttpServletRequest request, List<String> sortableColumnsList) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Sorting Begin");
		}
		Sort sortObject = null;
		try {
			String sortBy = (StringUtils.isEmpty(request.getParameter(SORT_BY))) ? request.getAttribute(DEFAULT_SORT).toString() : request.getParameter(SORT_BY);
			if(!CollectionUtils.isEmpty(sortableColumnsList) && StringUtils.isNotBlank(sortBy)) {
				if(!sortableColumnsList.contains(sortBy)) {
					LOGGER.warn("Ignoring {} for sorting, not registered",sortBy);
					sortBy = null;
				}
			} 
			//HIX-85763 Display latest record on top
			if(StringUtils.isNotBlank(sortBy)) {
				String sortOrder = (StringUtils.isEmpty(request.getParameter(PlanMgmtConstants.SORT_ORDER))) ? PlanMgmtConstants.SORT_ORDER_DESC : ((request.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
				
				String changeOrder = (StringUtils.isEmpty(request.getParameter(PlanMgmtConstants.CHANGE_ORDER))) ? PlanMgmtConstants.FALSE_STRING : ((request.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
				sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC) : sortOrder;
				request.removeAttribute(CHANGE_ORDER);
	
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("sortBy: " + SecurityUtil.sanitizeForLogging(String.valueOf(sortBy)));
				}
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("changeOrder: " + SecurityUtil.sanitizeForLogging(String.valueOf(changeOrder)));
				}
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("sortOrder: " + SecurityUtil.sanitizeForLogging(String.valueOf(sortOrder)));
				}
				
				model.addAttribute(SORT_ORDER, sortOrder);
	
				Sort.Direction direction = Sort.Direction.ASC;
	
				if (sortOrder.equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_DESC)) {
					direction = Sort.Direction.DESC;
				}
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("direction: " + SecurityUtil.sanitizeForLogging(String.valueOf(direction)));
				}
				sortObject = new Sort(new Sort.Order(direction, sortBy));
			}
	
		} catch(Exception ex){
			LOGGER.error("Excepetion occured in getSorting : ", ex);
		}
			
		 if(LOGGER.isDebugEnabled()){
			 LOGGER.debug("Sorting End");
		 }
		return sortObject;
	}

	// pagination and sorting code for the listing.
	@Override
	public Pageable getPagingAndSorting(Model model, HttpServletRequest request, List<String> sortableColumnsList) {

//		LOGGER.info("Paging Begin");
		// get the page number if exists else set default page number to 1 for
		// fetching the data.
		int pageNumber = 0;
		if (StringUtils.isNumeric(request.getParameter(PAGE_NUMBER))) {
			pageNumber = Integer.parseInt(request.getParameter(PAGE_NUMBER));
		}
		if (pageNumber < 1) {
			pageNumber = 1;
		}
		// create a PageRequest object for the specific page number and
		// no.of.records to display on the page (page size).
		Pageable pageable = new PageRequest(pageNumber - 1, GhixConstants.PAGE_SIZE, this.getSorting(model, request, sortableColumnsList));
	//	LOGGER.info("Paging End");
		return pageable;
	}

	@Override
	public Page<AccountUser> getRepListForIssuer(String moduleName, String roleName, Integer issuerId, String name, String title, List<Integer> status, Pageable pageable) {
		String firstName = name;
		String lastName = name;
		if (name != null && !name.isEmpty()) {
			if (name.contains(" ")) {
				firstName = name.substring(0, name.indexOf(' '));
				lastName = name.substring(name.indexOf(' '));
			}
			firstName = firstName.trim();
			lastName = lastName.trim();
		}
		return iIssuerRepository.getRepListForIssuer(moduleName, roleName, issuerId, firstName, lastName, title, status, pageable);
	}

	@Override
	public String getIssuerNameByRepUserId(Integer userId) {
		return iIssuerRepository.getIssuerByRepUserId(userId).getName();
	}

	@Override
	public IssuerRepresentative findIssuerRepresentative(AccountUser user, Issuer issuer) {
		return iIssuerRepresentativeRepository.findByUserRecordAndIssuer(user, issuer);
	}

	@Override
	public IssuerRepresentative findIssuerRepresentativeById(Integer repId) {
		return iIssuerRepresentativeRepository.findById(repId);
	}

	@Override
	public IssuerRepresentative findPrimaryContact(Issuer issuer, PrimaryContact primarycontact) {
		return iIssuerRepresentativeRepository.findByIssuerAndPrimaryContact(issuer, primarycontact);
	}

	@Override
	public List<Map<String, Object>> loadIssuerStatusHistory(Integer issuerId) {

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CREATED_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.UPDATED_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.NAME_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.COMMENT_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_FILE_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.UPDATEDBY_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS);
		displayCols.add(IssuerHistoryRendererImpl.CREATED);
		displayCols.add(IssuerHistoryRendererImpl.UPDATED);
		displayCols.add(IssuerHistoryRendererImpl.NAME);
		displayCols.add(IssuerHistoryRendererImpl.COMMENT);
		displayCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_FILE);
		displayCols.add(IssuerHistoryRendererImpl.UPDATEDBY);

		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> issuerData = service.findRevisions(IssuerHistoryRendererImpl.REPOSITORY_NAME, IssuerHistoryRendererImpl.MODEL_NAME, requiredFieldsMap, issuerId);
			data = issuerHistoryRendererImpl.processData(issuerData, compareCols, displayCols);
		return data;
	}

	@Override
	public List<Map<String, Object>> loadIssuerHistory(Integer issuerId) {

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CREATED_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.UPDATED_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.UPDATEDBY_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.NAME_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.FEDERAL_EMPLOYER_ID_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.NAIC_COMPANY_CODE_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.NAIC_GROUP_CODE_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.HIOS_ISSUER_ID_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.ADDRESS_LINE1_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.ADDRESS_LINE2_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.CITY_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.STATE_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.ZIP_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.ISSUER_SHORTNAME_COL, "");
		requiredFieldsMap.put(IssuerHistoryRendererImpl.COMMENT_COL, "");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.ACCREDITING_ENTITY_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.PHONE_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.EMAIL_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.ISSUER_ACCREDITATION_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.LICENSE_NUMBER_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.LICENSE_STATUS_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.ENROLLMENT_URL_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.CONTACT_PERSON_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.INITIAL_PAYMENT_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.RECURRING_PAYMENT_COL,"");
		// requiredFieldsMap.put(IssuerHistoryRendererImpl.SITE_URL_COL,"");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_COL);
		compareCols.add(IssuerHistoryRendererImpl.NAME_COL);
		compareCols.add(IssuerHistoryRendererImpl.FEDERAL_EMPLOYER_ID_COL);
		compareCols.add(IssuerHistoryRendererImpl.NAIC_COMPANY_CODE_COL);
		compareCols.add(IssuerHistoryRendererImpl.NAIC_GROUP_CODE_COL);
		compareCols.add(IssuerHistoryRendererImpl.HIOS_ISSUER_ID_COL);
		compareCols.add(IssuerHistoryRendererImpl.ADDRESS_LINE1_COL);
		compareCols.add(IssuerHistoryRendererImpl.ADDRESS_LINE2_COL);
		compareCols.add(IssuerHistoryRendererImpl.CITY_COL);
		compareCols.add(IssuerHistoryRendererImpl.STATE_COL);
		compareCols.add(IssuerHistoryRendererImpl.ZIP_COL);
		compareCols.add(IssuerHistoryRendererImpl.ISSUER_SHORTNAME_COL);
		// compareCols.add(IssuerHistoryRendererImpl.ISSUER_ACCREDITATION_COL);
		// compareCols.add(IssuerHistoryRendererImpl.ACCREDITING_ENTITY_COL);
		// compareCols.add(IssuerHistoryRendererImpl.PHONE_COL);
		// compareCols.add(IssuerHistoryRendererImpl.EMAIL_COL);
		// compareCols.add(IssuerHistoryRendererImpl.LICENSE_NUMBER_COL);
		// compareCols.add(IssuerHistoryRendererImpl.LICENSE_STATUS_COL);
		// compareCols.add(IssuerHistoryRendererImpl.ENROLLMENT_URL_COL);
		// compareCols.add(IssuerHistoryRendererImpl.CONTACT_PERSON_COL);
		// compareCols.add(IssuerHistoryRendererImpl.INITIAL_PAYMENT_COL);
		// compareCols.add(IssuerHistoryRendererImpl.RECURRING_PAYMENT_COL);
		// compareCols.add(IssuerHistoryRendererImpl.SITE_URL_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS);
		displayCols.add(IssuerHistoryRendererImpl.CREATED);
		displayCols.add(IssuerHistoryRendererImpl.UPDATED);
		displayCols.add(IssuerHistoryRendererImpl.UPDATEDBY);
		displayCols.add(IssuerHistoryRendererImpl.NAME);
		displayCols.add(IssuerHistoryRendererImpl.USER_NAME);
		displayCols.add(IssuerHistoryRendererImpl.CERTIFICATION_STATUS_VAL);
		displayCols.add(IssuerHistoryRendererImpl.NAME_VAL);
		displayCols.add(IssuerHistoryRendererImpl.DISPLAY_VAL);
		displayCols.add(IssuerHistoryRendererImpl.FEDERAL_EMPLOYER_ID_VAL);
		displayCols.add(IssuerHistoryRendererImpl.NAIC_COMPANY_CODE_VAL);
		displayCols.add(IssuerHistoryRendererImpl.NAIC_GROUP_CODE_VAL);
		displayCols.add(IssuerHistoryRendererImpl.HIOS_ISSUER_ID_VAL);
		displayCols.add(IssuerHistoryRendererImpl.ADDRESS_LINE1_VAL);
		displayCols.add(IssuerHistoryRendererImpl.ADDRESS_LINE2_VAL);
		displayCols.add(IssuerHistoryRendererImpl.CITY_VAL);
		displayCols.add(IssuerHistoryRendererImpl.STATE_VAL);
		displayCols.add(IssuerHistoryRendererImpl.ZIP_VAL);
		displayCols.add(IssuerHistoryRendererImpl.COMMENT);
		displayCols.add(IssuerHistoryRendererImpl.ISSUER_SHORTNAME_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.ACCREDITING_ENTITY_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.ISSUER_ACCREDITATION_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.LICENSE_NUMBER_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.LICENSE_STATUS_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.PHONE_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.EMAIL_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.ENROLLMENT_URL_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.CONTACT_PERSON_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.INITIAL_PAYMENT_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.RECURRING_PAYMENT_VAL);
		// displayCols.add(IssuerHistoryRendererImpl.SITE_URL_VAL);

		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> issuerData = service.findRevisions(IssuerHistoryRendererImpl.REPOSITORY_NAME, IssuerHistoryRendererImpl.MODEL_NAME, requiredFieldsMap, issuerId);
			data = issuerHistoryRendererImpl.processData(issuerData, compareCols, displayCols);
		return data;
	}

	@Override
	public ArrayList<Map<String, String>> loadRateBenefitReportList(HttpServletRequest manageRepRequest, Integer issuerId, String type) {

		SimpleDateFormat sdf = new SimpleDateFormat(GhixConstants.DISPLAY_DATE_FORMAT);
		SimpleDateFormat sdf2 = new SimpleDateFormat(SORTING_DATE_FORMAT);
		String sortColumn = manageRepRequest.getParameter(SORT_BY);
		String param = manageRepRequest.getParameter("pageNumber");
		int startRecord = 0;
		int endRecords = 1;
		EntityManager em = null;
		
		if (param != null) {
			endRecords = Integer.parseInt(param) * GhixConstants.PAGE_SIZE;
			startRecord = (Integer.parseInt(param) - 1) * GhixConstants.PAGE_SIZE;
		} else {
			endRecords = endRecords * GhixConstants.PAGE_SIZE;
		}

		String sortOrder = (manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER) == null) ? PlanMgmtConstants.SORT_ORDER_DESC : ((manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
		sortOrder = (sortOrder.equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.SORT_ORDER_ASC : sortOrder;
		
		String changeOrder = (manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER) == null) ? PlanMgmtConstants.FALSE_STRING : ((manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
		sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.ASC)) ? PlanMgmtConstants.SORT_ORDER_DESC : PlanMgmtConstants.ASC) : sortOrder;
		
		manageRepRequest.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
		manageRepRequest.setAttribute(SORT_ORDER, sortOrder);

	//	EntityManager em = emf.createEntityManager();
		/*StringBuilder buildquery = new StringBuilder("SELECT t.* FROM (SELECT ROWNUM AS rn , t.* FROM (SELECT pcr.id, pcr.creation_timestamp, (select FIRST_NAME ||' '|| LAST_NAME from users where id=pcr.created_by) AS createdBy, " + 
				"pcr.ecm_doc_id, pcr.ecm_doc_name, pcr.status, pcr.report_type FROM PM_PLAN_COMP_REPORT pcr LEFT JOIN users u ON pcr.created_by = u.id WHERE (pcr.issuer_id=" + issuerId + ") ");*/
		
		StringBuilder buildquery = new StringBuilder("SELECT pcr.id, pcr.creation_timestamp, (select FIRST_NAME ||' '|| LAST_NAME from users where id=pcr.created_by) AS createdBy, " + 
				"pcr.ecm_doc_id, pcr.ecm_doc_name, pcr.status, pcr.report_type FROM PM_PLAN_COMP_REPORT pcr LEFT JOIN users u ON pcr.created_by = u.id WHERE (pcr.issuer_id=" + issuerId + ") ");

		if(type.equalsIgnoreCase(PlanMgmtConstants.QHP)){
			buildquery.append(" AND pcr.report_type ='Q' ");
		} else{
			buildquery.append(" AND pcr.report_type ='S' ");
		}
		
		if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("id"))) {
			buildquery.append(" ORDER BY pcr.id ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("creation_timestamp"))) {
			buildquery.append(" ORDER BY pcr.creation_timestamp ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("createdBy"))) {
			buildquery.append(" ORDER BY pcr.createdBy ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("status"))) {
			buildquery.append(" ORDER BY pcr.status ");
		} else if (StringUtils.isBlank(sortColumn)) {
			buildquery.append(" ORDER BY pcr.creation_timestamp ");
		} else {
			buildquery.append(" ORDER BY pcr.creation_timestamp ");
			LOGGER.warn("Plan Benefits list sorting ignored, as sorting not supported for column ".intern() + sortColumn);
		}
		
		if (StringUtils.isNotBlank(sortOrder) && (sortOrder.equalsIgnoreCase(SortOrder.ASC.toString()))) {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("creation_timestamp")) {
					buildquery.append(" ASC NULLS first ");
				} else {
					buildquery.append(" ASC ");
				}
			} else {
				buildquery.append(" ASC ");
			}		
		} else {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("creation_timestamp")) {
					buildquery.append(" DESC NULLS last ");
				} else {
					buildquery.append(" DESC ");
				}
			} else {
				buildquery.append(" DESC ");
			}
		}
		//buildquery.append(" ) t where rownum <= " + endRecords + " ) t ");
		//buildquery.append(" WHERE rn > " + startRecord);
		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("Final build query " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
		}
		
		ArrayList<Map<String, String>> repArrList = new ArrayList<Map<String, String>>();
		
		try{
		    em = emf.createEntityManager();
			List rsList = em.createNativeQuery(buildquery.toString()).getResultList();
			Iterator rsIterator = rsList.iterator();
	
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Result list", SecurityUtil.sanitizeForLogging(String.valueOf(rsList)));
			}
			
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				Map<String, String> issuerRepData = new HashMap<String, String>();
				issuerRepData.put("id", objArray[0].toString());
				issuerRepData.put("creationTimestamp", (objArray[1] != null) ? sdf.format(DateUtil.StringToDate((objArray[1].toString()), "yyyy-MM-dd")): "");
				//sorting order failed previously due to format
				issuerRepData.put("creationTimestampToSort", (objArray[1] != null) ? sdf2.format(DateUtil.StringToDate((objArray[1].toString()), "yyyy-MM-dd")): "");
				issuerRepData.put("createdBy", (objArray[2] != null) ? objArray[2].toString() : "");
				if(objArray[3] != null && objArray[5].equals(PlanMgmtConstants.COMPLETE)){
					issuerRepData.put("ecmDocId", "<a href='" + GhixConstants.APP_URL + "ecm/filedownloadbyid?uploadedFileId=" + objArray[3].toString() + "'>Download File</a>");
				} else if(objArray[5].equals(PlanMgmtConstants.ERROR)){
				    issuerRepData.put("ecmDocId", PlanMgmtConstants.ERROR_MSG);
				} else{
					issuerRepData.put("ecmDocId", PlanMgmtConstants.IN_PROGRESS);
				}
						
				issuerRepData.put("ecmDocName", (objArray[4] != null) ? objArray[4].toString() : "0");
				issuerRepData.put("status", (objArray[5] != null) ? (objArray[5].toString().substring(0, 1).toUpperCase() + objArray[5].toString().substring(1).toLowerCase()) : "");			
				repArrList.add(issuerRepData);
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in loadRateBenefitReportList >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in loadRateBenefitReportList >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return repArrList;
	}
	
	@Override
	public List<IssuerRepresentative> findIssuerRepresentative(Integer issuerId) {
		return iIssuerRepresentativeRepository.findByIssuerId(issuerId);
	}

	@Override
	public Page<IssuerRepresentative> findIssuerRepresentative(Integer issuerId, Pageable pageable) {
		return iIssuerRepresentativeRepository.findByIssuerId(issuerId, pageable);
	}

	@Override
	public Page<IssuerRepresentative> findIssuerRepresentative(String repFirstName, String repLastName, String repTitle, String status, Integer issuerId, Pageable pageable) {
		Page<IssuerRepresentative> representatives;
		if (StringUtils.isNotBlank(status) && StringUtils.isNotBlank(repTitle)) {
			representatives = iIssuerRepresentativeRepository.findIssuerRepresentative(repFirstName, repLastName, repTitle, issuerId, pageable);
		} else if (StringUtils.isNotBlank(status)) {
			representatives = iIssuerRepresentativeRepository.findIssuerRepresentative(repFirstName, repLastName, issuerId, pageable);
		} else if (StringUtils.isNotBlank(repTitle)) {
			representatives = iIssuerRepresentativeRepository.findIssuerRepresentative(repFirstName, repLastName, repTitle, issuerId, pageable);
		} else {
			representatives = iIssuerRepresentativeRepository.findIssuerRepresentative(repFirstName, repLastName, issuerId, pageable);
		}
		return representatives;
	}

	@Override
	public Integer saveComment(Integer issuerId, String comment, TargetName targetName) throws InvalidUserException {

		Integer userId = null;
		String userName = null;
			userId = userService.getLoggedInUser().getId();
			userName = userService.getLoggedInUser().getUserName();
		CommentTarget target = commentTargetService.findByTargetIdAndTargetType(issuerId.longValue(), targetName);
		if (target == null) {
			target = new CommentTarget();
			target.setTargetId(issuerId.longValue());
			target.setTargetName(targetName);
			List<Comment> commentsList = new ArrayList<Comment>();
			target.setComments(commentsList);
		}

		Comment commentObj = new Comment();
		commentObj.setComentedBy(userId);
		commentObj.setComment(comment);
		commentObj.setCommentTarget(target);
		commentObj.setCommenterName(userName);
		target.getComments().add(commentObj);

		commentObj = commentService.saveComment(commentObj);
		return commentObj.getId();
	}

	@Override
	public IssuerRepresentative updateDelegateInfo(int representativeId, String delegationCode, int responseCode, String responseDescription) {
		IssuerRepresentative issuerRepresentative = null;
		issuerRepresentative = iIssuerRepresentativeRepository.findOne(representativeId);
		if (issuerRepresentative != null) {
			issuerRepresentative.setDelegationCode(delegationCode);
			issuerRepresentative.setResponseCode(responseCode);
			issuerRepresentative.setResponseDescription(responseDescription);

			return iIssuerRepresentativeRepository.save(issuerRepresentative);
		}
		return null;
	}

	@Override
	public DelegationResponse getAhbxDelegationResp(DelegationRequest delegationRequest) throws GIException {
		return delegateResponseUtils.validateIssuerResponse(delegationRequest);
	}

	@Override
	public List<HashMap<String, String>> removeDuplicateFileName(List<HashMap<String, String>> duplicateFileNames) {
		Set<HashMap<String, String>> uniqueSet = new HashSet<HashMap<String, String>>();
		uniqueSet.addAll(duplicateFileNames);
		duplicateFileNames.clear();
		duplicateFileNames.addAll(uniqueSet);
		return duplicateFileNames;
	}

	@Override
	public Issuer getIssuerByHiosID(String hiosIssuerID) {
		Issuer issuer = iIssuerRepository.getIssuerByHiosID(hiosIssuerID);
		if (issuer != null) {
			return issuer;
		} else {
			return null;
		}
	}

	@Override
	public void delete(Issuer entity) {
		iIssuerRepository.delete(entity);
	}

	@Override
	@Transactional
	public IssuerRepresentative findIssuerRepresentativeById(int id) {
		IssuerRepresentative issuerRepresentative = null;
		if (iIssuerRepresentativeRepository.exists(id)) {
			issuerRepresentative = iIssuerRepresentativeRepository.findOne(id);
		}
		return issuerRepresentative;
	}

	@Override
	@Transactional
	public ExchgPartnerLookup saveExchgPartnerLookup(ExchgPartnerLookup partner) {
		return iExchgPartnerLookupRepository.saveAndFlush(partner);
	}

	@Override
	@Transactional
	public ExchgPartnerLookup saveNewExchgPartnerLookup(ExchgPartnerLookup partner) {
		return iExchgPartnerLookupRepository.save(partner);
	}

	@Override
	@Transactional
	public Page<ExchgPartnerLookup> findListPartnersByIssuer(String hiosIssuerId, Pageable pageable) {
		return iExchgPartnerLookupRepository.findByHiosIssuerId(hiosIssuerId, pageable);
	}

	@Override
	public String getIssuerLogoByName(String issuerName) {
		return (iIssuerRepository.getIssuerByName(issuerName).getCompanyLogo());
	}

	@Override
	public byte[] getIssuerLogoById(String issuerIdStr) {
		return getIssuerLogo(getIssuerById(getDecryptedIssuerId(issuerIdStr)));
	}
	
	private int getDecryptedIssuerId(String encIssuerId) {
		// if IssuerID is encrypted format, decryprt IssuerID before using
		String decryptedIssuerID = null;
		int issuerId = 0;
		try {
			if(encIssuerId != null && !StringUtils.isNumeric(encIssuerId)) {
				decryptedIssuerID = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			} else {
				decryptedIssuerID = encIssuerId;
			}
			issuerId = Integer.parseInt(decryptedIssuerID);
		} catch (Exception e) {
			LOGGER.error("Got invalid issuerId param for getting logo");
		}
		return issuerId;
	}
	
	@Override
	public byte[] getIssuerLogoByHiosId(String hiosIssuerId) {
		return getIssuerLogo(getIssuerByHiosID(hiosIssuerId));
	}

	private byte[] getIssuerLogo(Issuer issuerObj) {
		byte[] bytes;
		if(null != issuerObj && null != issuerObj.getLogo()) {
			bytes = issuerObj.getLogo();
		} else {
			LOGGER.debug("Returning blank image as LOGO  is not found");
			//byte code for transparent blank image
			bytes = new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x21, (byte) 0xF9, 0x04,
					0x01, 0x00, 0x00, 0x00, 0x00, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x02};
		}	
		return bytes;
	}

	/*@Override
	public String getIssuerLogoPath(String encIssuerId, String basePath) {
		int issuerId = getDecryptedIssuerId(encIssuerId);
		Issuer issuerObj = getIssuerById(issuerId);
		String logoUrl = null;
		if(null != issuerObj) { 
			logoUrl = issuerObj.getLogoURL();
		}
		return PlanMgmtUtil.getIssuerLogoURLForAdmin(logoUrl, encIssuerId, basePath);
	}*/
	
	/**
	 * HIX-10640
	 */
	@Override
	public void generateActivationLink(Integer repId, AccountUser accountUser, String issuerRepRole) throws GIException {

		IssuerRepresentative issuerRepresentative = iIssuerRepresentativeRepository.findById(repId);
		Issuer issuer = issuerRepresentative.getIssuer();
		// AccountUser accountUser = issuerRepresentative.getUserRecord();
		if (accountUser == null) {
			throw new IllegalArgumentException("No user info found for Id- "
					+ repId);
		}

		Map<String, String> issuerEnrollmentDetails = new HashMap<String, String>();
		issuerEnrollmentDetails.put("name", accountUser.getFirstName() + " "
				+ accountUser.getLastName());
		issuerEnrollmentDetails.put("exchangename", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		issuerEnrollmentDetails.put("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		issuerEnrollmentDetails.put(PlanMgmtConstants.ISSUER_MARKETING_NAME, issuer.getName() == null ? "": issuer.getName().trim());
		String address1 = issuer.getAddressLine1() == null ? "" : issuer.getAddressLine1().trim();
		String address2 = issuer.getAddressLine2() == null ? "" : issuer.getAddressLine2().trim();
		
		String address ="";
		if(StringUtils.isNotBlank(address2)){
			address = address1 + PlanMgmtConstants.BREAK_LINE + address2;
		}else{
			address = address1;
		}
		issuerEnrollmentDetails.put(PlanMgmtConstants.ISSUER_ADDRESS, address);
		issuerEnrollmentDetails.put(PlanMgmtConstants.CITY, issuer.getCity());
		issuerEnrollmentDetails.put(PlanMgmtConstants.STATE, issuer.getState());
		issuerEnrollmentDetails.put(PlanMgmtConstants.ZIPCODE, issuer.getZip());
		
		String firstName = issuerRepresentative.getFirstName() == null ? "" : issuerRepresentative.getFirstName().trim();
		String lastName = issuerRepresentative.getLastName() == null ? "" : issuerRepresentative.getLastName().trim();
		issuerEnrollmentDetails.put(PlanMgmtConstants.ISSUER_REPRESENTATIVE, firstName + " " + lastName);
		
		issuerEnrollmentDetails.put("emailType", "issuerAccountActivationEmail");

		CreatedObject createdObject = new CreatedObject();
		createdObject.setObjectId(repId);
		createdObject.setEmailId(accountUser.getEmail());
		createdObject.setRoleName(issuerRepRole);
		createdObject.setPhoneNumbers(Arrays.asList(accountUser.getPhone()));
		createdObject.setFullName(accountUser.getFirstName() + " "
				+ accountUser.getLastName());
		createdObject.setCustomeFields(issuerEnrollmentDetails);
		createdObject.setFirstName(accountUser.getFirstName());
		createdObject.setLastName(accountUser.getLastName());

		/**
		 * Login from which this process flow occurs is of Issuer
		 * Representative, but Issuer is kept as Creator for the flow. As the
		 * representative whose login used is a Primary Contact and can be
		 * changed.
		 */
		CreatorObject creatorObject = new CreatorObject();
		creatorObject.setObjectId(issuerRepresentative.getUpdatedBy().getId());
		creatorObject.setFullName(issuerRepresentative.getUpdatedBy().getFullName());
		creatorObject.setRoleName(GhixRole.ADMIN.toString());
		int noOfExpirationsDays = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ISSUER)) ;
		AccountActivation accountActivation = accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, noOfExpirationsDays);

		if (accountActivation != null) {
			LOGGER.info("Account activation link sent to the representative...");
		}
	}

	@Override
	public Integer getDefaultServieAreaId(String servicerAreaName, Integer applicableYear) {
		try {
			return iIssuerServiceAreaReposiroty.getServieAreaIdByName(servicerAreaName, applicableYear);
		} catch (Exception ex) {
			LOGGER.error("Invalid service area name '" + SecurityUtil.sanitizeForLogging(String.valueOf(servicerAreaName)) + "'. Ex: ", ex);
		}

		return 0;
	}

	@Override
	public IssuerRepresentative getIssuerRepresentativeForIssuer(Issuer issuerObj) {
		IssuerRepresentative representative = iIssuerRepresentativeRepository.findByIssuerAndPrimaryContact(issuerObj, PrimaryContact.YES);
		if (representative != null) {
			return representative;
		} else {
			return null;
		}
	}

	@Override
	public AccountUser getAccountUserFromID(Integer id) {
		AccountUser accountUser = iUserRepository.findOne(id);
		if (accountUser != null) {
			return accountUser;
		} else {
			return null;
		}
	}

	@Override
	public int getIssuerRepresentataivesCount(Integer issuerId) {
		EntityManager em = null;
		StringBuilder countQuery = new StringBuilder("SELECT count(ir.id) FROM issuer_representative ir LEFT JOIN users u ON ir.user_id = u.id WHERE (ir.issuer_id="
				+ issuerId + ")");

		int resultCount = 0;
		try{
			em = emf.createEntityManager();
			Object objCount = em.createNativeQuery(countQuery.toString()).getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getIssuerRepresentataivesCount >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getIssuerRepresentataivesCount >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return resultCount;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Issuer> findAllIssuerForReport() {
		List<Issuer> issuerList = new ArrayList<Issuer>();
			StringBuilder buildquery = new StringBuilder();
			buildquery = buildquery.append("SELECT i.id,i.name FROM issuers i order by i.id");
			EntityManager em = null;

			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug(" SQL === " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
			}

			try{
			    em = emf.createEntityManager();
			//	LOGGER.debug(" Is_Connection_Open === " + SecurityUtil.sanitizeForLogging(String.valueOf(em.isOpen())));
				List rsList = em.createNativeQuery(buildquery.toString()).getResultList();
	
				if (rsList != null && rsList.size() > 0) {
					Iterator rsIterator = rsList.iterator();
					while (rsIterator.hasNext()) {
						Object[] objArray = (Object[]) rsIterator.next();
						Issuer issuer = new Issuer();
						issuer.setId(Integer.parseInt(objArray[0].toString()));
						if (objArray[1].toString() != null) {
							issuer.setName(objArray[1].toString());
						}
						issuerList.add(issuer);
					}
				}
			}catch (Exception e) {
				LOGGER.error("ERROR in findAllIssuerForReport >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
				throw new GIRuntimeException("ERROR in findAllIssuerForReport >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			}finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
			}
		
		return issuerList;
	}
	
	@Override
	public String getSerfServiceAreaIdById(Integer id){
		return iIssuerServiceAreaReposiroty.getSerfServieAreaIdById(id);
	}
	
	/*
	 * Method for sending email to notification to Issuer Representative for Issuer Financial Information change.
	 * Changes for HIX-38300
	 */
	@Override
	public void sendFinancialNotificationToIssuerRep(Issuer issuerObj) {
	//	Notice noticeObj = null;
		try {
			List<IssuerRepresentative> listIssuerRep = iIssuerRepresentativeRepository.findByIssuerId(issuerObj.getId());
			Map<String, Object> data = new HashMap<>();
			for (int i = 0; i < listIssuerRep.size(); i++) {
				if (listIssuerRep.get(i).getUserRecord() != null) {
					AccountUser accountUser = listIssuerRep.get(i).getUserRecord();
					if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail()) && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(accountUser.getStatus())) {
						issuerFinancialInformationChangeEmail.setIssuerObj(issuerObj);
						issuerFinancialInformationChangeEmail.setIssuerRepObj(listIssuerRep.get(i));
						issuerFinancialInformationChangeEmail.setUser(accountUser);

						/*noticeObj = issuerFinancialInformationChangeEmail.generateEmail();
//						noticeObj = issuerFinancialInformationChangeEmail.sendEmail(noticeObj);

						InboxMsg msg = new InboxMsg();
						InboxMsgClob inboxMsgClob = new InboxMsgClob();

						msg.setType(TYPE.M);
						msg.setContentType(CONTENT_TYPE.H);
						msg.setToUserNameList(accountUser.getFullName());

						msg.setMsgSub(noticeObj.getSubject());
						msg.setFromUserId(userService.getLoggedInUser().getId());
						msg.setFromUserName(userService.getLoggedInUser().getFullName());
						msg.setPriority(InboxMsg.PRIORITY.M);
						if(noticeObj.getStatus() != null){
							if (Notice.STATUS.EMAIL_SENT.toString().equalsIgnoreCase(noticeObj.getStatus().toString())) {
								msg.setIsEmailSent(InboxMsg.EMAIL_SENT_STATUS.Y);
							}
							if (Notice.STATUS.FAILED.toString().equalsIgnoreCase(noticeObj.getStatus().toString())) {
								msg.setIsEmailSent(InboxMsg.EMAIL_SENT_STATUS.N);
							}
						}
						msg.setOwnerUserId(accountUser.getId());
						msg.setOwnerUserName(issuerObj.getName());
						msg.setIsClobUsed(InboxMsg.CLOB_USED.Y);
						msg.setEmailSentDate(new TSDate());
						msg.setDateExpire(DateUtils.addDays(new TSDate(), MONTH_DAY));
						msg.setLocale("EN");
						msg.setStatus(InboxMsg.STATUS.N);
						msg = iInboxMsgRepository.save(msg);
						iInboxMsgRepository.flush();

						inboxMsgClob.setMsgClob(noticeObj.getEmailBody());
						inboxMsgClob.setId(msg.getId());
						iInboxMsgClobRepository.save(inboxMsgClob);*/
						
						String fileName = "IssuerFinancialInformationChange"+TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
		 	 	 	 	data = issuerFinancialInformationChangeEmail.getSingleData();
		 	 	 	 	noticeService.createNotice(PlanMgmtConstants.ISSUER_FINANCIAL_INFORMATION_UPDATE_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.ISSUER_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);

						LOGGER.info("Message stored in inbox successfully");

					} else {
						LOGGER.info("Issuer Representative email address is blank or null");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Notification type \"Issuer financial information change Email\" not found. Email cannot be sent.");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SEND_NOTIFICATION_EAMIL_EXCEPTION, e, null, Severity.HIGH);
		}
	}

	@Override
	public String getIssuerStatus(Integer issuerId) {
		return iIssuerRepository.getIssuerStatus(issuerId);
	}
	
	@Override
	public String deleteAccreditationDocument(String documentId, int issuerId) throws Exception{
	
		LOGGER.info("Deleting logo entry from ECM");
		String response = "";
		response = deleteContent(documentId);
		if(!response.equals("")){
			LOGGER.info("Deleting logo entry from database");
			int resp = issuerDocumentRepository.deleteIssuerDocument(issuerId, documentId);
			
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Logo entry deleted from database " + SecurityUtil.sanitizeForLogging(String.valueOf(resp)));
			}
		} 
		return response;
	}
	
	@Override
	public String deleteContent(String documentID) throws Exception{
		String response = "";
		if (documentID != null) {
			try{
				response = ecmService.deleteContent(documentID);
				if(LOGGER.isDebugEnabled()){
				   LOGGER.debug("Deleted content from Alfresco, response : " + SecurityUtil.sanitizeForLogging(String.valueOf(response)));
				}   
			} catch(ContentManagementServiceException ex){
				LOGGER.error("Error deleting the content from Alfresco, response : " + SecurityUtil.sanitizeForLogging(String.valueOf(response)));
				return PlanMgmtConstants.SUCCESS;
			}
		}	
		return response;
	}
	
	@Override
	public String updatePrimaryContactToNo(Integer issuerId){
		String response = "FAILURE";
		try{
			iIssuerRepresentativeRepository.updatePrimaryContactToNo(issuerId);
			response = "SUCCESS";
		}catch(Exception ex){
			response = "EXCEPTION";
			LOGGER.error("Exception occured in updateStatusToNo", ex);
		}
		return response;
	}
	
	
	@Override
	public String updatePrimaryContactToNo(Integer issuerId, Integer issuerRepId){
		String response = "FAILURE";
		try{
			iIssuerRepresentativeRepository.updatePrimaryContactToNo(issuerId, issuerRepId);
			response = "SUCCESS";
		}catch(Exception ex){
			response = "EXCEPTION";
			LOGGER.error("Exception occured in updateStatusToNo", ex);
		}
		return response;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, String>> getIssuerRepresentatives(HttpServletRequest manageRepRequest, Integer issuerId, boolean escapePagination, boolean showIssuerEnrollmentRep) {
		List<Map<String, String>> repArrList = new ArrayList<Map<String, String>>();
		SimpleDateFormat sdf = new SimpleDateFormat(GhixConstants.DISPLAY_DATE_FORMAT);
		String sortColumn = manageRepRequest.getParameter(SORT_BY);
		String param = manageRepRequest.getParameter("pageNumber");
		int startRecord = 0;
		int endRecords = 1;
		EntityManager em = null;
		
		if (param != null) {
			endRecords = Integer.parseInt(param) * GhixConstants.PAGE_SIZE;
			startRecord = (Integer.parseInt(param) - 1) * GhixConstants.PAGE_SIZE;
		} else {
			endRecords = endRecords * GhixConstants.PAGE_SIZE;
		}

		String sortOrder = (manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER) == null) ? PlanMgmtConstants.SORT_ORDER_ASC : ((manageRepRequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
		sortOrder = (sortOrder.equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.SORT_ORDER_ASC : sortOrder;
		String changeOrder = (manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER) == null) ? PlanMgmtConstants.FALSE_STRING : ((manageRepRequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
		 sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_ASC)) ? PlanMgmtConstants.SORT_ORDER_DESC : PlanMgmtConstants.SORT_ORDER_ASC) : sortOrder;
		manageRepRequest.removeAttribute(CHANGE_ORDER);
		manageRepRequest.setAttribute(SORT_ORDER, sortOrder);
		
		StringBuilder buildquery = new StringBuilder();

		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
			buildquery.append("SELECT t.* FROM (SELECT t.* FROM (SELECT ir.id as tempId, ir.id as id, ir.first_name,ir.last_name, ir.title, ir.last_update_timestamp, u.confirmed, ");
		}
		else {
			buildquery.append("SELECT t.* FROM (SELECT ROWNUM AS rn , t.* FROM (SELECT ir.id, ir.first_name,ir.last_name, ir.title, ir.last_update_timestamp, u.confirmed, ");
		}
		buildquery.append("(select FIRST_NAME ||' '|| LAST_NAME from users where id=ir.updatedBy) AS updatedBy, ");
		buildquery.append("ir.user_id, ir.primary_contact, ir.status, ir.role, u.status AS userStatus, u.first_name AS ufirstName, u.last_name AS ulastName ");
		buildquery.append("FROM ");
		buildquery.append("issuer_representative ir LEFT JOIN users u ON ir.user_id = u.id WHERE (ir.issuer_id= :issuerId)");

		if ((manageRepRequest.getAttribute(REPNAME) != null) && !manageRepRequest.getAttribute(REPNAME).toString().trim().isEmpty()) {
			//buildquery.append(" AND ( UPPER(ir.first_name) LIKE '%'||:repname||'%' OR UPPER(ir.last_name) LIKE '%'||:repname||'%' )");
			buildquery.append(" AND ( ( UPPER(ir.first_name) LIKE '%'||:repname||'%' OR UPPER(ir.last_name) LIKE '%'||:repname||'%' ) OR ( UPPER(u.first_name) LIKE '%'||:repname||'%' OR UPPER(u.last_name) LIKE '%'||:repname||'%' ) ) ");
		}
		if ((manageRepRequest.getAttribute(REPTITLE) != null) && !manageRepRequest.getAttribute(REPTITLE).toString().trim().isEmpty()) {
			buildquery.append(" AND UPPER(ir.title) LIKE '%'||:reptitle||'%'");
		}
		
		if ((manageRepRequest.getAttribute(STATUS_STRING) != null) && !manageRepRequest.getAttribute(STATUS_STRING).toString().trim().isEmpty()) {
			if(manageRepRequest.getAttribute(STATUS_STRING).toString().equalsIgnoreCase("1")){
				buildquery.append(" AND ( UPPER(u.status) = 'ACTIVE' AND (UPPER(ir.status) = 'ACTIVE' OR ir.status IS NULL )) ");
			}else if(manageRepRequest.getAttribute(STATUS_STRING).toString().equalsIgnoreCase("0")){
				buildquery.append(" AND ( (UPPER(u.status) != 'ACTIVE' AND ir.user_id IS NOT NULL) OR (ir.user_id IS NULL) ) ");
			}
			else if(manageRepRequest.getAttribute(STATUS_STRING).toString().equalsIgnoreCase("2")){
				buildquery.append(" AND ( UPPER(u.status) = 'ACTIVE' AND UPPER(ir.status) = 'INACTIVE' ) ");
			}
		}
		
		// if showIssuerEnrollmentRep == false then don't pull records having role 'ISSUER_ENROLLMENT_REPRESENTATIVE' 
		if(!showIssuerEnrollmentRep){
			buildquery.append(" AND (ir.role != '" + ActivationJson.OBJECTTYPE.ISSUER_ENROLLMENT_REPRESENTATIVE.toString() + "' OR ir.role IS NULL) ");
		}
		
		if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals(PlanMgmtConstants.FIRST_NAME))) {
			buildquery.append(" ORDER BY lower(ir.first_name) ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals(PlanMgmtConstants.TITLE))) {
			buildquery.append(" ORDER BY lower(ir.title) ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("updated"))) {
			buildquery.append(" ORDER BY ir.last_update_timestamp ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("status"))) {
			buildquery.append(" ORDER BY u.status ");
		} else if (StringUtils.isNotBlank(sortColumn) && (sortColumn.equals("role"))) {
			buildquery.append(" ORDER BY ir.role ");
		} else if (StringUtils.isNotBlank(sortColumn) && sortColumn.equals("updatedBy.firstName")) {
			buildquery.append(" ORDER BY lower(updatedBy) ");
		}else if (StringUtils.isNotBlank(sortColumn) && sortColumn.equals("primarycontact")) {
			buildquery.append(" ORDER BY lower(ir.primary_contact) ");
		} else if (StringUtils.isBlank(sortColumn)) {
			buildquery.append(" ORDER BY ir.last_update_timestamp ");
		} else {
			buildquery.append(" ORDER BY ir.last_update_timestamp ");
			LOGGER.warn("Issuer Representative list sorting ignored, as sorting not supported for column ".intern() + sortColumn);
		}
		
		if (StringUtils.isNotBlank(sortOrder) && (sortOrder.equalsIgnoreCase(SortOrder.DESC.toString()))) {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("userRecord.confirmed")) {
					buildquery.append(" DESC NULLS last ");
				} else {
					buildquery.append(" DESC ");
				}
			} else {
				buildquery.append(" DESC ");
			}
		} else {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("userRecord.confirmed")) {
					buildquery.append(" ASC NULLS first ");
				} else {
					buildquery.append(" ASC ");
				}
			} else {
				buildquery.append(" DESC ");
			}
		}

		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
			buildquery.append(" ) t ) t ");
			// if escapePagination != true then use pagination filter
			if (!escapePagination) {
				buildquery.append(" offset " + startRecord);
				buildquery.append(" limit " + GhixConstants.PAGE_SIZE);
			}
		}
		else {
			// if escapePagination != true then use pagination filter
			if (!escapePagination) {
				buildquery.append(" ) t where rownum <= :endRecords ) t ");
				buildquery.append(" WHERE rn > :startRecord");
			}
			else {
				buildquery.append(" ) t ) t ");
			}
		}

		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("Final build query " + buildquery);
		}
		
		try{
			em = emf.createEntityManager();
			Query getRepresentativeQuery = em.createNativeQuery(buildquery.toString());
	
			if ((manageRepRequest.getAttribute(REPNAME) != null) && !manageRepRequest.getAttribute(REPNAME).toString().trim().isEmpty()) {
				String repName = manageRepRequest.getAttribute(REPNAME).toString().trim().toUpperCase();
				getRepresentativeQuery.setParameter("repname", repName);
			}
			
			if ((manageRepRequest.getAttribute(REPTITLE) != null) && !manageRepRequest.getAttribute(REPTITLE).toString().trim().isEmpty()) {
				String repTitle = manageRepRequest.getAttribute(REPTITLE).toString().trim().toUpperCase();
				getRepresentativeQuery.setParameter("reptitle", repTitle);
			}
			
			/*if ((manageRepRequest.getAttribute(STATUS_STRING) != null) && !manageRepRequest.getAttribute(STATUS_STRING).toString().trim().isEmpty()) {
				String statusString = manageRepRequest.getAttribute(STATUS_STRING).toString().trim();
				getRepresentativeQuery.setParameter("status", statusString);
			}*/
	
			getRepresentativeQuery.setParameter("issuerId", issuerId);
			
			// if escapePagination != true then use pagination filter
			if(!escapePagination && null != DB_TYPE && !DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)){
				getRepresentativeQuery.setParameter("endRecords", endRecords);
				getRepresentativeQuery.setParameter("startRecord", startRecord);
			}
			
			List<?> rsList = getRepresentativeQuery.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
	
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Result list size", rsList.size());
			}
	
			Map<String, String> issuerRepData = null;
			while (rsIterator.hasNext()) {
	
				Object[] objArray = (Object[]) rsIterator.next();
	
				issuerRepData = new HashMap<String, String>();
	
				issuerRepData.put("id", objArray[PlanMgmtConstants.ONE].toString());
				/*issuerRepData.put(PlanMgmtConstants.FIRST_NAME, (objArray[PlanMgmtConstants.TWO] != null) ? objArray[PlanMgmtConstants.TWO].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put(PlanMgmtConstants.LAST_NAME, (objArray[PlanMgmtConstants.THREE] != null) ? objArray[PlanMgmtConstants.THREE].toString()
						: PlanMgmtConstants.EMPTY_STRING);*/
				issuerRepData.put(PlanMgmtConstants.FIRST_NAME, (objArray[PlanMgmtConstants.THIRTEEN] != null) ? objArray[PlanMgmtConstants.THIRTEEN].toString()
						:  objArray[PlanMgmtConstants.TWO].toString());
				issuerRepData.put(PlanMgmtConstants.LAST_NAME, (objArray[PlanMgmtConstants.FOURTEEN] != null) ? objArray[PlanMgmtConstants.FOURTEEN].toString()
						: objArray[PlanMgmtConstants.THREE].toString());
				issuerRepData.put(PlanMgmtConstants.TITLE, (objArray[PlanMgmtConstants.FOUR] != null) ? objArray[PlanMgmtConstants.FOUR].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("updated", (objArray[PlanMgmtConstants.FIVE] != null) ? sdf.format(DateUtil.StringToDate((objArray[PlanMgmtConstants.FIVE].toString()), "yyyy-MM-dd"))
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("confirmed", (objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString()
						: PlanMgmtConstants.ZERO_STR);
				issuerRepData.put("updatedby", (objArray[PlanMgmtConstants.SEVEN] != null) ? objArray[PlanMgmtConstants.SEVEN].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("repUserId", (objArray[PlanMgmtConstants.EIGHT] != null) ? objArray[PlanMgmtConstants.EIGHT].toString()
						: PlanMgmtConstants.ZERO_STR);
				issuerRepData.put("haveUserId", (objArray[PlanMgmtConstants.EIGHT] != null) ? PlanMgmtConstants.Y : PlanMgmtConstants.N);
				issuerRepData.put("primarycontact", (objArray[PlanMgmtConstants.NINE] != null) ? objArray[PlanMgmtConstants.NINE].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("status", (objArray[PlanMgmtConstants.TEN] != null) ? objArray[PlanMgmtConstants.TEN].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("role", (objArray[PlanMgmtConstants.ELEVEN] != null) ? objArray[PlanMgmtConstants.ELEVEN].toString()
						: PlanMgmtConstants.EMPTY_STRING);
				issuerRepData.put("displayStatus", getIssuerRepStatus(objArray[PlanMgmtConstants.TWELVE], objArray[PlanMgmtConstants.TEN]));
	
				repArrList.add(issuerRepData);
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getIssuerRepresentatives >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getIssuerRepresentatives >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return repArrList;
	}
	
	public String getIssuerRepStatus(Object userStatus, Object issuerRepStatus){
		String displayStatus = PlanMgmtConstants.DISP_INACTIVE;
		
		if(null == userStatus && null == issuerRepStatus) 	// case1: new issuer rep/issuer enrollment rep is added.
		{		
			displayStatus = PlanMgmtConstants.DISP_INACTIVE;
		}
		else if(null !=userStatus && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(userStatus.toString()) &&
				(issuerRepStatus == null || (null != issuerRepStatus && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(issuerRepStatus.toString()))) ) // case2: user status is Active in users table but issuer rep/enrollment rep status is null / Active in issuer_representative table 
		{  
			displayStatus = PlanMgmtConstants.DISP_ACTIVE;
		}
		else if(null !=userStatus && (PlanMgmtConstants.INACTIVE_DORMAT.equalsIgnoreCase(userStatus.toString()) 
				|| PlanMgmtConstants.INACTIVE.equalsIgnoreCase(userStatus.toString())))	//  case3: user status is Inactive-dormant / Inactive in users table
		{ 
			displayStatus = PlanMgmtConstants.DISP_INACTIVE;
		}
		
		else if(null !=userStatus && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(userStatus.toString()) &&
				(null != issuerRepStatus && PlanMgmtConstants.INACTIVE.equalsIgnoreCase(issuerRepStatus.toString())) ) //  case4: user status is Active and issuer rep/enrollment rep status is Inactive in issuer_representative table
		{  
			displayStatus = PlanMgmtConstants.DISP_SUSPENDED;
		}
	
		return displayStatus;
		
	}
	
	public Issuer getIssuerId(String issuerName){
		return iIssuerRepository.getIssuerByName(issuerName);
	}
	
	public Map<String, Object> createEnrolleeSearchCriteria(HttpServletRequest request) {
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();

		String sortBy = (request.getParameter("sortBy") == null) ? "id"
				: request.getParameter("sortBy");
		searchCriteria.put("sortBy", ((sortBy.equals("")) ? "id" : sortBy));

		String sortOrder = (request.getParameter("sortOrder") == null) ? "DESC"
				: request.getParameter("sortOrder");
		sortOrder = (sortOrder.equals("")) ? "DESC" : sortOrder;
		String changeOrder = (request.getParameter("changeOrder") == null) ? "false"
				: request.getParameter("changeOrder");
		sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals("DESC")) ? "ASC"
				: "DESC")
				: sortOrder;
		request.removeAttribute("changeOrder");
		searchCriteria.put("sortOrder", sortOrder);

		int pageNumber = (request.getParameter("pageNumber") != null) ? (Integer.parseInt(request.getParameter("pageNumber")))
				: 0;
		searchCriteria.put("pageNumber", pageNumber);
		searchCriteria.put("pageSize", LISTING_NUMBER);

		searchCriteria.put("policynumber", StringUtils.isNotBlank(request.getParameter("policynumber")) ? request.getParameter("policynumber") : null);
		
		searchCriteria.put("status", StringUtils.isNotBlank(request.getParameter("status")) ? request.getParameter("status"): null);
		
		searchCriteria.put("plantype", StringUtils.isNotBlank(request.getParameter("plantype")) ? request.getParameter("plantype"): null);
		
		searchCriteria.put("issuer", StringUtils.isNotBlank(request.getParameter("issuer")) ? request.getParameter("issuer"): null);
		
		searchCriteria.put("plannumber", StringUtils.isNotBlank(request.getParameter("plannumber")) ? request.getParameter("plannumber"): null);
		
		String name = StringUtils.isNotBlank(request.getParameter("name")) ? request.getParameter("name") : null;
		
		searchCriteria.put("name", name);
		
		if(request.getParameter("employerName")!=null && !(request.getParameter("employerName").toString().isEmpty())){
			String employerName = request.getParameter("employerName").toString().trim();
			searchCriteria.put("employerName", employerName);
		}

		String firstName = name;
		String middlename = "";
		String lastName = "";
		if(StringUtils.isNotBlank(name)) {
			name = name.trim();
			String[] splitName = name.split(" ");
			if (splitName.length == 1) {
				firstName = splitName[0].trim();
				middlename = splitName[0].trim();
				lastName = splitName[0].trim();

			} else if (splitName.length == 2) {
				firstName = splitName[0].trim();
				middlename = splitName[1].trim();
				lastName = splitName[1].trim();
			} else if (splitName.length >= 3) {
				firstName = splitName[0].trim();
				middlename = splitName[1].trim();
				lastName = splitName[2].trim();
			}
		}
		searchCriteria.put("firstname", StringUtils.isNotBlank(firstName) ? firstName.trim() : null);
		searchCriteria.put("middlename", StringUtils.isNotBlank(middlename) ? middlename.trim(): null);
		searchCriteria.put("lastname", StringUtils.isNotBlank(lastName) ? lastName.trim(): null);

		return searchCriteria;
	}
	
	@Override
	public String getIssuerLogoByIssuerId(Integer issuerId) 
	{
		return iIssuerRepository.getIssuerLogoByIssuerId(issuerId);
	}
	
	@Cacheable(value="downloadDoc", key="{#root.methodName, #documentId}")
	public Map<String, Object> getDownloadDocumentInfo(String documentId){
		Map<String, Object> documentInfoMap = new HashMap<String, Object>();
		byte[] bytes = null;
		try{
		    if(StringUtils.isNotBlank(documentId)){
				bytes = ecmService.getContentDataById(documentId);
				LOGGER.info("requested documentId = " + documentId);
				Content content = ecmService.getContentById(documentId);
				if(null != bytes && null != content) {
					LOGGER.info("OriginalFileName() = " + content.getOriginalFileName());
					LOGGER.info("getMimeType() = " + content.getMimeType());
					LOGGER.info("bytes.length = "+ bytes.length);
					documentInfoMap.put(PlanMgmtConstants.DOCUMENT, bytes);
					documentInfoMap.put(PlanMgmtConstants.DOCUMENT_NAME, content.getOriginalFileName().replace(PlanMgmtConstants.SPACE, PlanMgmtConstants.UNDERSCORE));
					documentInfoMap.put(PlanMgmtConstants.MIME_CONTENT_TYPE, content.getMimeType());
				} else {
					LOGGER.info("Failed to get requested documentId = " + documentId);
				}
		    }
		}catch(Exception ex){
		    LOGGER.error("Exception occured in getDownloadDocumentInfo : "+ ex.getMessage(), ex);
		}
		return documentInfoMap;
	}

	private  Map<String, Object> getPaymentMethodsMap(String paymentMethodsString){
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	
		PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
		return paymentMethodResponse.getPaymentMethodMap();
	}
	
	@Override
	public boolean isFileSizeExceedsLimit(List<MultipartFile> authority, List<MultipartFile> accrediting, List<MultipartFile> orgData, List<MultipartFile> marketing, List<MultipartFile> finDisclosures, 
			List<MultipartFile> addSupportDoc, List<MultipartFile> payPolPractices, List<MultipartFile> enrollmentAndDis, List<MultipartFile> ratingPractices, List<MultipartFile> costSharAndPayment){
		Integer idealFileSize = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CERTSUPPDOCFILESIZE));
		boolean fileSizeFlag = false;
		try{
			if(!authority.isEmpty()){
				for(MultipartFile file: authority){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!accrediting.isEmpty()){
				for(MultipartFile file: accrediting){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!orgData.isEmpty()){
				for(MultipartFile file: orgData){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!marketing.isEmpty()){
				for(MultipartFile file: marketing){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!finDisclosures.isEmpty()){
				for(MultipartFile file: finDisclosures){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!addSupportDoc.isEmpty()){
				for(MultipartFile file: addSupportDoc){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!payPolPractices.isEmpty()){
				for(MultipartFile file: payPolPractices){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!enrollmentAndDis.isEmpty()){
				for(MultipartFile file: enrollmentAndDis){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!ratingPractices.isEmpty()){
				for(MultipartFile file: ratingPractices){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}else if(!costSharAndPayment.isEmpty()){
				for(MultipartFile file: costSharAndPayment){
					if (file != null	&& (file.getSize() > idealFileSize)) {
						fileSizeFlag = true;
					}
				}
			}
		
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.FILE_SIZE_EXCEEDS_LIMIT, ex, null, Severity.HIGH);
		}
		return fileSizeFlag;
	}
	
	
	@Override
	public boolean isValidExtension(List<MultipartFile> authority, List<MultipartFile> accrediting, List<MultipartFile> orgData, List<MultipartFile> marketing, List<MultipartFile> finDisclosures, 
			List<MultipartFile> addSupportDoc, List<MultipartFile> payPolPractices, List<MultipartFile> enrollmentAndDis, List<MultipartFile> ratingPractices, List<MultipartFile> costSharAndPayment){
		boolean fileExtensionFlag = false;
		try{
			if(!authority.isEmpty()){
				for(MultipartFile file: authority){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!accrediting.isEmpty()){
				for(MultipartFile file: accrediting){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!orgData.isEmpty()){
				for(MultipartFile file: orgData){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!marketing.isEmpty()){
				for(MultipartFile file: marketing){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!finDisclosures.isEmpty()){
				for(MultipartFile file: finDisclosures){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!addSupportDoc.isEmpty()){
				for(MultipartFile file: addSupportDoc){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!payPolPractices.isEmpty()){
				for(MultipartFile file: payPolPractices){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!enrollmentAndDis.isEmpty()){
				for(MultipartFile file: enrollmentAndDis){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!ratingPractices.isEmpty()){
				for(MultipartFile file: ratingPractices){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}else if(!costSharAndPayment.isEmpty()){
				for(MultipartFile file: costSharAndPayment){
					if (file != null && (Arrays.asList(fileExtetionList).contains(FilenameUtils.getExtension(file.getOriginalFilename().toLowerCase()))) ) {
						fileExtensionFlag = true;
					}
				}
			}
		
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.FILE_SIZE_EXCEEDS_LIMIT, ex, null, Severity.HIGH);
		}
		return fileExtensionFlag;
	}
	
	
	@Override
	public void removeDataFromSession(HttpServletRequest request) {
		try{
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES);
			}
			if (request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING);
			}
			
			if (request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP);
			}
	
			if (request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP);
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.REMOVE_SESSION_DATA_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
	}

	/**
	 * Method is used process and persist Network Transparency Data File.
	 */
	@Override
	public String processAndPersistNetworkTransparencyDataFile(MultipartFile[] fileList, String applicableYear,
			Issuer currentIssuer, String stateCode, int userId) {

		LOGGER.info("processAndPersistNetworkTransparencyDataFile() Start");
		String statusMessage = "Failed to upload Network Transparency file.";
		SerffPlanMgmt trackingRecord = null;

		try {
			String fileName = null;
			InputStream inputStream = null;

			for (MultipartFile multipartFile : fileList) {
				// Creates tracking record in SERFF_PLAN_MGMT table.
				trackingRecord = planMgmtUtils.createSerffPlanMgmtRecord(PlanMgmtConstants.TRANSFER_NW_TRANS, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.P);
				trackingRecord.setIssuerId(String.valueOf(currentIssuer.getId()));
				// HIX-89724: Display Plan Year for Network Breath list view
				trackingRecord.setSerffTrackNum(applicableYear);

				if (null == multipartFile) {
					trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
					trackingRecord.setRequestStateDesc("Failed to upload Network Transparency file because input file content is not valid.");
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
					trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
					LOGGER.error(trackingRecord.getRequestStateDesc());
					continue;
				}
				fileName = multipartFile.getOriginalFilename();
				// Update tracking record from SERFF_PLAN_MGMT table.ss
				trackingRecord.setAttachmentsList(fileName);
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.W);
				trackingRecord.setRequestStateDesc("Network Transparency upload In progress.");
				trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);

				// call method to persist file
				inputStream = multipartFile.getInputStream();
				uploadPopulateAndPersistNetworkTransparency(inputStream, fileName, applicableYear, currentIssuer, stateCode, userId, trackingRecord);

				if (null != inputStream) {
					inputStream.close();
					inputStream = null;
				}
			}
		}
		catch (IOException ex) {

			statusMessage += ex.getMessage();
			if (null != trackingRecord) {
				trackingRecord.setRequestStateDesc(statusMessage);
				trackingRecord.setPmResponseXml(statusMessage);
			}
			LOGGER.error(statusMessage, ex);
		}
		finally {

			if (null != trackingRecord) {

				if (StringUtils.isNotBlank(trackingRecord.getPmResponseXml())) {
					statusMessage = trackingRecord.getPmResponseXml();
				}
				trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
			}
			LOGGER.info("processAndPersistNetworkTransparencyDataFile() End");
		}
		return statusMessage;
	}

	/**
	 * Method is used to Upload, Populate and Persist Network Transparency Excel data.
	 */
	private boolean uploadPopulateAndPersistNetworkTransparency(InputStream inputStream, String fileName, String applicableYear,
			Issuer currentIssuer, String stateCode, int userId, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("uploadPopulateAndPersistNetworkTransparency() Start");
		boolean status = false;
		NetworkTransparencyVO networkTransMainVO = null;

		try {
			networkTransMainVO = networkTransparencyReader.loadExcel(inputStream, fileName, trackingRecord);
			if (null == networkTransMainVO || networkTransMainVO.isNotValid()) {
				return status;
			}

			setCountyFipsMap(networkTransMainVO, stateCode);	// Set all County Name with County Fips in Map
			LOGGER.debug("Network Transparency Excel Data: " + networkTransMainVO.toString());
			networkTransparencyValidator.validateNetworkTransparencyVO(networkTransMainVO, applicableYear, stateCode, currentIssuer.getHiosIssuerId());

			if (!networkTransMainVO.isNotValid()) {
				LOGGER.debug("Persisting Network Transparency Excel data");
				networkTransparencyPersist.persistNetworkTransparencyData(networkTransMainVO, trackingRecord, userId);
			}
		}
		catch (GIException ex) {
			LOGGER.error("Error: " + ex.getMessage(), ex);
			trackingRecord.setPmResponseXml(ex.getMessage());
		}
		finally {

			if (null != networkTransMainVO && networkTransMainVO.isNotValid()) {
				trackingRecord.setRequestStateDesc("Error occurred while saving Network Transparency Data. Check PM Response for details.");
				trackingRecord.setPmResponseXml(networkTransMainVO.getErrorMessages());
			}
			LOGGER.debug("uploadPopulateAndPersistNetworkTransparency() End");
		}
		return status;
	}

	/**
	 * Method is used to retrieve all County Name with County Fips in Map from Zipcodes table.
	 */
	private void setCountyFipsMap(NetworkTransparencyVO networkTransMainVO, String stateCode) {

		LOGGER.debug("setCountyFipsMap() Start");

		try {
			List<ZipCode> zipCodeList = iZipCodeRepository.findCountyNameStateFipsAndCountyFipsByState(stateCode);

			if (!CollectionUtils.isEmpty(zipCodeList)) {
				LOGGER.debug("Total List of County Fips: " + zipCodeList.size());

				for (ZipCode zipCode : zipCodeList) {
					networkTransMainVO.setCountyFipsInMap(zipCode.getCounty().trim(), zipCode.getStateFips().trim() + zipCode.getCountyFips().trim());
				}
			}
		}
		finally {
			LOGGER.debug("setCountyFipsMap() End");
		}
	}
	
	@Override
	public boolean isIssuerRepSuspended() {

		boolean hasIssuerRepSuspended = false;
		AccountUser accountUserObj = null;

		try {
			accountUserObj = userService.getLoggedInUser();

			if (null != accountUserObj) {
				IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByUserRecord(accountUserObj);

				if (null != issuerRepObj && issuerRepObj.getStatus() != null && issuerRepObj.getStatus().equalsIgnoreCase("Inactive")) {
					hasIssuerRepSuspended = true;
				}
			}
		}
		catch (Exception ex) {
			LOGGER.info("Error occurs inside isIssuerRepSuspended() ", ex);
		}
		finally {
			accountUserObj = null;
		}
		return hasIssuerRepSuspended;
	}

	/**
	 * Method is used to invoke get Drug Data by RXCUI-List API by RXCUI List.
	 */
	@Override
	public DrugDataResponseDTO getDrugDataByRxcuiSearchList(String rxcuiData, StringBuffer errorMessage) {

		LOGGER.debug("getDrugDataByRxcuiSearchList() Start");
		DrugDataResponseDTO responseDTO = null;
		final String RXCUI_EMPTY_ERROR = "RXCUI should not be empty";
		final String RXCUI_INVALID_ERROR = "Invalid RXCUI data: ";
		final int PAGE_SIZE_LIMIT = 20;

		try {

			boolean hasValid = false;
			if (org.apache.commons.lang3.StringUtils.isNotBlank(rxcuiData)) {

				List<String> invalidRxcuiList = Stream.of(rxcuiData.split(",")).map(String::trim).filter(str -> !org.apache.commons.lang3.StringUtils.isNumeric(str)).collect(Collectors.toList());

				if (CollectionUtils.isEmpty(invalidRxcuiList)) {
					hasValid = true;
				}
				else {
					errorMessage.append(RXCUI_INVALID_ERROR).append(invalidRxcuiList);
				}
			}
			else {
				errorMessage.append(RXCUI_EMPTY_ERROR);
			}

			DrugDataRequestDTO requestDTO = new DrugDataRequestDTO();

			if (hasValid) {

				Stream.of(rxcuiData.split(",")).map(String::trim).limit(PAGE_SIZE_LIMIT).forEach(rxcui -> requestDTO.addRxcuiToList(rxcui));
	
				if (CollectionUtils.isEmpty(requestDTO.getRxcuiList())) {
					errorMessage.append(RXCUI_EMPTY_ERROR);
					hasValid = false;
				}
			}

			if (!hasValid) {
				return responseDTO;
			}
			requestDTO.setPageNumber(0);
			requestDTO.setPageSize(PAGE_SIZE_LIMIT);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Request RXCUI: " + requestDTO.toString());
			}
			responseDTO = planDisplayRestClient.getDrugDataByRxcuiSearchList(requestDTO);
		}
		catch (Exception e) {
			errorMessage.append("Internal Server Error");
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Exception Occured in get Drug Data By RXCUI Search List ", e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME + PlanDisplayConstants.FAILED_TO_EXECUTE + PlandisplayEndpoints.GET_DRUG_DATA_BY_RXCUI_SEARCH_LIST, e, e.getMessage(), Severity.HIGH);
		}
		finally {

			if (LOGGER.isDebugEnabled() && null != responseDTO) {
				LOGGER.debug("Response DTO: " + responseDTO.toString());
			}
			LOGGER.debug("getDrugDataByRxcuiSearchList() End");
		}
		return responseDTO;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}
}
