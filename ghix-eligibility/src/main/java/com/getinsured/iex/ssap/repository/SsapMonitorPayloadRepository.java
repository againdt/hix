package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.getinsured.iex.ssap.model.SsapMonitorPayload;

@RestResource(path="SsapMonitorPayload")
public interface SsapMonitorPayloadRepository  extends JpaRepository<SsapMonitorPayload, Long> {


	@Query("select g from SsapMonitorPayload g where g.ssapApplicationId = :ssapApplicationId order by id desc")
	List<SsapMonitorPayload> findBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);

	
	SsapMonitorPayload findById(Integer id);
}
