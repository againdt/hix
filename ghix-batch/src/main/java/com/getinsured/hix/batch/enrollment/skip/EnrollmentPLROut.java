package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("enrollmentPLROut")
public class EnrollmentPLROut {

	Map<String,String> skippedHouseholdMap=null;
	boolean isGeneratedXMLsValid = Boolean.TRUE;
	
	List<String> householdCaseIds=null;
	public EnrollmentPLROut() {
		skippedHouseholdMap= new HashMap<String, String>();
		householdCaseIds= new ArrayList<String>();
		
	}
	
	public synchronized void putToSkippedHouseholdMap(String key, String value){
		skippedHouseholdMap.put(key, value);
	}
	public synchronized void putAllToSkippedHouseholdMap(Map<String,String> failedHouseholdMap){
		this.skippedHouseholdMap.putAll(failedHouseholdMap);
	}	
	
	public Map<String,String> getSkippedHouseholdMap(){
		return skippedHouseholdMap;
	}
	
	public synchronized void clearSkippedHouseholdMap(){
		this.skippedHouseholdMap.clear();
	}
	
	public synchronized void addAllToHouseholdCaseIds(List<String> householdCaseIds){
		this.householdCaseIds.addAll(householdCaseIds);
	}
	
	public synchronized void addToHouseholdCaseIds(String householdCaseId){
		this.householdCaseIds.add(householdCaseId);
	}
	
	public List<String> getHouseholdCaseIds(){
		return householdCaseIds;
	}
	public synchronized void clearHouseholdCaseIds(){
		this.householdCaseIds.clear();
	}

	/**
	 * @return the isGeneratedXMLsValid
	 */
	public boolean isGeneratedXMLsValid() {
		return isGeneratedXMLsValid;
	}

	/**
	 * @param isGeneratedXMLsValid the isGeneratedXMLsValid to set
	 */
	public synchronized void setGeneratedXMLsValid(boolean isGeneratedXMLsValid) {
		this.isGeneratedXMLsValid = isGeneratedXMLsValid;
	}

	public synchronized void resetAllFields(){
		this.skippedHouseholdMap.clear();
		this.householdCaseIds.clear();
		this.isGeneratedXMLsValid = Boolean.TRUE;
	}
}
