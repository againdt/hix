package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfBenefitsListArrayType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.BenefitsListArrayType;
import com.getinsured.iex.hub.platform.HubServiceException;


/**
 * Wrapper Class for BenefitsLstArrayType
 * 
 * @author Vishaka Tharani
 * @since  18-Feb-2014 
 * 
 */
public class ArrayOfBenefitsListArrayTypeWrapper implements JSONAware{
	
	private List<BenefitsListArrayTypeWrapper> benefitsListArrayTypeWrapperList;
	
	private static final String BENEFITS_DATA_KEY = "BenefitsData";
	
	public ArrayOfBenefitsListArrayTypeWrapper( ArrayOfBenefitsListArrayType arrayOfBenefitsListArrayType) throws HubServiceException{
		if(arrayOfBenefitsListArrayType == null){
			throw new HubServiceException("No sponsorshipData provided");
		}
		
		List<BenefitsListArrayType> benefitsList = arrayOfBenefitsListArrayType.getBenefitsListArray();
		if(benefitsList != null && !benefitsList.isEmpty())
		{
			benefitsListArrayTypeWrapperList = new ArrayList<BenefitsListArrayTypeWrapper>();
		}
		for(BenefitsListArrayType benefitsListArrayType : benefitsList){
			BenefitsListArrayTypeWrapper benefitsListArrayTypeWrapper = new BenefitsListArrayTypeWrapper(benefitsListArrayType); 
			benefitsListArrayTypeWrapperList.add(benefitsListArrayTypeWrapper);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray benefitsListArrayType = null;
		
		if(benefitsListArrayTypeWrapperList != null && !benefitsListArrayTypeWrapperList.isEmpty()){
			benefitsListArrayType = new JSONArray();
			for(BenefitsListArrayTypeWrapper data : this.benefitsListArrayTypeWrapperList){
				benefitsListArrayType.add(data.toJSONString());
			}
		}
		
		obj.put(BENEFITS_DATA_KEY,benefitsListArrayType);
		return obj.toJSONString();
	}
}
