package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class ValidateEnrollmentCmsOutTask   implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidateEnrollmentCmsOutTask.class);
	private EnrollmentCmsOutService enrollmentCmsOutService;
	private JobService jobService;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("Enrollment CMS OUT Validation Started.....");
		jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		String batchJobStatus=null;
		
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			enrollmentCmsOutService.deleteFileFromWip();
			enrollmentCmsOutService.deleteFromEnrollmentCmsOut(jobExecutionId);
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		if(!enrollmentCmsOutService.validateGeneratedEnrollmentCMSOutXML(jobId)){
			LOGGER.info(" Enrollment CMS OUT Validation Failed ");
		}
		
		return RepeatStatus.FINISHED;
	}

	public EnrollmentCmsOutService getEnrollmentCmsOutService() {
		return enrollmentCmsOutService;
	}

	public void setEnrollmentCmsOutService(EnrollmentCmsOutService enrollmentCmsOutService) {
		this.enrollmentCmsOutService = enrollmentCmsOutService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
