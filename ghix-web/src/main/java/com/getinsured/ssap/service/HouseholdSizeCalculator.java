package com.getinsured.ssap.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SpecialCircumstances;
import com.getinsured.iex.ssap.financial.TaxHouseholdComposition;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.financial.type.TaxRelationship;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.HouseholdUtil;

/**
 * Provides functionality to calculate household size.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-13
 */
public class HouseholdSizeCalculator {

  static final int ADULT_AGE = 19;
  // TODO: Test case H: Dependent B is: Not a full time student, 20 y.o., so for dependent A non-filer rule for siblings is < 19 y.o. or < 21 y.o. if FULL TIME student.

  /**
   * Calculates household size for non-financial application for eligibility engine.
   * @param members all household members.
   * @return size of the household.
   */
  static int calculateNonFinancialSize(final List<HouseholdMember> members) {
    return members.size();
  }

  /**
   * Calculates total household size irrespective if any of the members are seeking coverage or not.
   *
   * @param members List of PRIMARY Tax Household Members {@link HouseholdMember}'s.
   * @param primaryTaxFilerPersonId id of the primary tax filer.
   * @return total household size.
   * @see HouseholdUtil#getPrimaryTaxHouseholdMembers(SingleStreamlinedApplication) Method to get the list of primary tax household members.
   */
  static int calculateSize(final List<HouseholdMember> members, final int primaryTaxFilerPersonId) {
    int total = 0;

    final boolean hasTaxFiler = members.stream().anyMatch(m ->
      m.getTaxHouseholdComposition().getTaxFilingStatus() != TaxFilingStatus.UNSPECIFIED &&
        m.getTaxHouseholdComposition().getTaxRelationship() != TaxRelationship.UNSPECIFIED
    );

    // TODO: Simplify once logic is finalized
    if(primaryTaxFilerPersonId > 0 && hasTaxFiler) {
      for (HouseholdMember member : members) {
        TaxHouseholdComposition taxHouseholdComposition = member.getTaxHouseholdComposition();
        TaxRelationship taxRelationship = taxHouseholdComposition.getTaxRelationship();

        if (taxRelationship == TaxRelationship.FILER || taxRelationship == TaxRelationship.DEPENDENT || taxRelationship == TaxRelationship.FILER_DEPENDENT) {
          total++;
        }
      }
    } else {
      // TODO: If there is no tax filer on the application, size of all primary tax household members?
      total = members.size();
    }

    return total;
  }

  /**
   * Calculates MAGI individual household size.
   *
   * @param members      all household members.
   * @param member       member to calculate size for.
   * @param coverageYear application coverage year.
   * @param primaryTaxFilerPersonId primary tax filer's person id.
   * @return size of the household for given member.
   */
  static int calculateSizeMagi(final List<HouseholdMember> members, HouseholdMember member, int coverageYear, int primaryTaxFilerPersonId) {
    return getMagiSize(members, member, coverageYear, primaryTaxFilerPersonId);
  }

  static List<HouseholdMember> getMagiHousehold(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> magiHousehold = new ArrayList<>();

    if (isTaxFiler(member)) {
      if (isClaimedByAnyTaxFiler(members, member, primaryTaxFilerPersonId)) {
        if (isTaxDependentException(members, member, applicationYear, primaryTaxFilerPersonId)) {
          magiHousehold.addAll(membersNonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId));
        } else {
          magiHousehold.addAll(membersTaxDependentRules(members, member, primaryTaxFilerPersonId));
        }
      } else {
        magiHousehold.addAll(membersTaxFilerRules(members, member, primaryTaxFilerPersonId));
      }
    } else if (isClaimedByPrimaryTaxFiler(members, member, primaryTaxFilerPersonId)) {
      if (isTaxDependentException(members, member, applicationYear, primaryTaxFilerPersonId)) {
        magiHousehold.addAll(membersNonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId));
      } else {
        magiHousehold.addAll(membersTaxDependentRules(members, member, primaryTaxFilerPersonId));
      }
    } else {
      magiHousehold.addAll(membersNonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId));
    }

    return magiHousehold;
  }

  static int getMagiSize(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    int householdSize = 0;

    if (isTaxFiler(member)) {
      if (isClaimedByAnyTaxFiler(members, member, primaryTaxFilerPersonId)) {
        if (isTaxDependentException(members, member, applicationYear, primaryTaxFilerPersonId)) {
          householdSize += nonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId);
        } else {
          householdSize += taxDependentRules(members, member, primaryTaxFilerPersonId);
        }
      } else {
        householdSize += taxFilerRules(members, member, primaryTaxFilerPersonId);
      }
    } else if (isClaimedByPrimaryTaxFiler(members, member, primaryTaxFilerPersonId)) {
      if (isTaxDependentException(members, member, applicationYear, primaryTaxFilerPersonId)) {
        householdSize += nonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId);
      } else {
        householdSize += taxDependentRules(members, member, primaryTaxFilerPersonId);
      }
    } else {
      householdSize += nonTaxFilerRules(members, member, applicationYear, primaryTaxFilerPersonId);
    }

    return householdSize;
  }

  public static boolean isTaxFiler(final HouseholdMember member) {
    final TaxHouseholdComposition taxComposition = member.getTaxHouseholdComposition();
    return taxComposition.getTaxRelationship() == TaxRelationship.FILER || taxComposition.getTaxRelationship() == TaxRelationship.FILER_DEPENDENT;
  }

  private static boolean isClaimedByAnyTaxFiler(final List<HouseholdMember> members, final HouseholdMember member, final int primaryTaxFilerPersonId) {
    return isClaimedByPrimaryTaxFiler(members, member, primaryTaxFilerPersonId) || isClaimedByNonPrimaryTaxFiler(members, member, primaryTaxFilerPersonId);
  }

  private static boolean isClaimedByPrimaryTaxFiler(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> claimants = getClaimers(members, member);
    return claimants.stream().anyMatch(c -> c.getPersonId() == primaryTaxFilerPersonId);
  }

  private static boolean isClaimedByNonPrimaryTaxFiler(final List<HouseholdMember> members, final HouseholdMember member, final int primaryTaxFilerPersonId) {
    final TaxHouseholdComposition taxComposition = member.getTaxHouseholdComposition();
    final Set<Integer> claimantIds = taxComposition.getClaimerIds();
    final List<HouseholdMember> claimantsNoPrimary =
        getClaimers(members, member)
            .stream()
            .filter(c -> c.getPersonId() != primaryTaxFilerPersonId)
            .filter(hhm -> hhm.getTaxHouseholdComposition().getTaxRelationship() == TaxRelationship.FILER ||
              hhm.getTaxHouseholdComposition().getTaxRelationship() == TaxRelationship.FILER_DEPENDENT)
            .collect(Collectors.toList());

    return claimantsNoPrimary.stream().anyMatch(c -> claimantIds.contains(c.getPersonId()));
  }

  private static int taxFilerRules(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    int householdSize = 1; // Individual
    householdSize += getDependents(members, member).size(); // And all of individual's dependents

    if (HouseholdUtil.isMarried(members, member, primaryTaxFilerPersonId) && !isSpouseLivingApartAndFilingSeparately(members, member, primaryTaxFilerPersonId)) {
      householdSize += 1; // Add spouse living separately and filing separately.
    }

    return householdSize;
  }

  private static List<HouseholdMember> membersTaxFilerRules(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> list = new ArrayList<>();
    list.add(member); // Individual
    list.addAll(getDependents(members, member)); // And all of individual's dependents

    if (HouseholdUtil.isMarried(members, member, primaryTaxFilerPersonId) && !isSpouseLivingApartAndFilingSeparately(members, member, primaryTaxFilerPersonId)) {
      HouseholdMember spouse = getSpouse(members, member);
      list.add(spouse); // Add spouse living separately and filing separately.
    }
    return list;
  }

  /**
   * Returns spouse for given member or null.
   * @param members household members.
   * @param member member for whom to return spouse
   * @return spouse or null.
   */
  public static HouseholdMember getSpouse(List<HouseholdMember> members, HouseholdMember member) {
    for(HouseholdMember m : members) {
      if(HouseholdUtil.getRelationOf(members, m, member) == BloodRelationshipCode.SPOUSE) {
        return m;
      }
    }

    return null;
  }

  /**
   * Returns domestic partner for given member or null.
   * @param members list of all household members
   * @param member who's partner you want to get.
   * @return partner of given member or null.
   */
  public static HouseholdMember getDomesticPartner(List<HouseholdMember> members, HouseholdMember member) {
    return members.stream().filter(m->HouseholdUtil.getRelationOf(members, m, member) == BloodRelationshipCode.DOMESTIC_PARTNER).findFirst().orElse(null);
  }

  /**
   * Rules:
   * 1. Household size is the household size of the tax filer claiming given individual as dependent,
   * 2. Spouse if living with individual
   * 3. Number of children individual is expecting.
   *
   * @param members all household members.
   * @param member member for whom we need to calculate medicaid household size.
   * @param primaryTaxFilerPersonId primary tax filer person id.
   * @return household size.
   */
  private static int taxDependentRules(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    List<HouseholdMember> claimers = getClaimers(members, member);
    int householdSize = 0;

    if(!claimers.isEmpty()) {
      // Execute tax filer rules on claimer (multiple claimers only if MFJ).
      householdSize = taxFilerRules(members, claimers.get(0), primaryTaxFilerPersonId); // Individual + Tax Payer (Primary Applicant/Tax Filer)
    } else {
      // This should not happen actually as UI locks down this behaviour
      householdSize = 1;
    }

    //householdSize += getDependentsExcept(members, primaryTaxFiler, member.getPersonId()).size();
    if (HouseholdUtil.isMarried(members, member, primaryTaxFilerPersonId) && !isSpouseLivingApartAndFilingSeparately(members, member, primaryTaxFilerPersonId)) {
      householdSize += 1; // Add spouse living separately and filing jointly.
    }

    // Not adding # of babies expected, I'm passing numberOfBabies to Eligibility as separate property, as Supreet mentioned it's calculated on EE side.
    return householdSize;
  }

  private static List<HouseholdMember> membersTaxDependentRules(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> list = new ArrayList<>();
    final List<HouseholdMember> claimers = getClaimers(members, member);

    List<HouseholdMember> claimersHousehold = new ArrayList<>();

    if(!claimers.isEmpty()) {
      claimersHousehold = membersTaxFilerRules(members, claimers.get(0), primaryTaxFilerPersonId);
    }
    else {
      // This should not happen
      list.add(member);
    }

    list.addAll(claimersHousehold);

    // If member is married, add spouse
    if (HouseholdUtil.isMarried(members, member, primaryTaxFilerPersonId) && !isSpouseLivingApartAndFilingSeparately(members, member, primaryTaxFilerPersonId)) {
      HouseholdMember spouse = getSpouse(members, member);
      list.add(spouse);
    }

    return list;
  }

  private static int nonTaxFilerRules(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    int memberAge = HouseholdIncomeCalculator.getApplicantAge(member.getDateOfBirth(), applicationYear);

    int householdSize = 1; // Individual

    if (memberAge < ADULT_AGE) {
      List<HouseholdMember> parents = HouseholdUtil.getAllParents(members, member, primaryTaxFilerPersonId);
      List<HouseholdMember> siblings = HouseholdUtil.getAllSiblings(members, member, primaryTaxFilerPersonId);

      // All parents
      householdSize += parents.size();

      // All siblings under age of ADULT_AGE
      householdSize += siblings.stream().filter(sibling ->
          HouseholdIncomeCalculator.getApplicantAge(sibling.getDateOfBirth(), applicationYear) < ADULT_AGE).count();
    } else {
      final List<HouseholdMember> peopleLivingWith = HouseholdUtil.membersLivingWith(members, member);
      // TODO: Optimize with streams after finalization
      for (HouseholdMember hhm : peopleLivingWith) {
        BloodRelationshipCode code = HouseholdUtil.getRelationOf(members, member, hhm);
        if (code == BloodRelationshipCode.SPOUSE) {
          householdSize++; // Add spouse that person lives with
        }
      }
    }

    householdSize += HouseholdUtil.getAllChildren(members, member, primaryTaxFilerPersonId).size();

    // TODO: Invoke pregnancy rules?
    return householdSize;
  }

  private static List<HouseholdMember> membersNonTaxFilerRules(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    final List<HouseholdMember> list = new ArrayList<>();

    int memberAge = HouseholdIncomeCalculator.getApplicantAge(member.getDateOfBirth(), applicationYear);

    list.add(member); // Individual

    if (memberAge < ADULT_AGE) {
      List<HouseholdMember> parents = HouseholdUtil.getAllParents(members, member, primaryTaxFilerPersonId);
      List<HouseholdMember> siblings = HouseholdUtil.getAllSiblings(members, member, primaryTaxFilerPersonId);

      // All parents
      list.addAll(parents);

      // All siblings under age of ADULT_AGE
      List<HouseholdMember> brothersSisters =
          siblings.stream().filter(sibling -> HouseholdIncomeCalculator.getApplicantAge(sibling.getDateOfBirth(), applicationYear) < ADULT_AGE).collect(Collectors.toList());

      list.addAll(brothersSisters);

    } else {
      final List<HouseholdMember> peopleLivingWith = HouseholdUtil.membersLivingWith(members, member);

      for (HouseholdMember hhm : peopleLivingWith) {
        BloodRelationshipCode code = HouseholdUtil.getRelationOf(members, hhm, member);
        if (code == BloodRelationshipCode.SPOUSE) {
         list.add(hhm); // Add spouse that person lives with
        } else {
          List<HouseholdMember> children = HouseholdUtil.getAllChildren(members, member, primaryTaxFilerPersonId);
          list.addAll(children);
        }
      }
    }

    return list;
  }

  private static boolean isTaxDependentException(final List<HouseholdMember> members, HouseholdMember member, int applicationYear, int primaryTaxFilerPersonId) {
    boolean exception1 = false;
    boolean exception2 = false;
    boolean exception3 = false;

    /*
     * Check exception 1
     */
    final HouseholdMember primaryTaxFiler = HouseholdUtil.getPrimaryTaxFiler(members, primaryTaxFilerPersonId);
    final BloodRelationshipCode relationshipCode = HouseholdUtil.getRelationOf(members, primaryTaxFiler, member);
    exception1 = relationshipCode != BloodRelationshipCode.SPOUSE && relationshipCode != BloodRelationshipCode.CHILD && relationshipCode != BloodRelationshipCode.STEPCHILD;

    int memberAge = HouseholdIncomeCalculator.getApplicantAge(member.getDateOfBirth(), applicationYear);

    List<HouseholdMember> parents = HouseholdUtil.getAllParents(members, member, primaryTaxFilerPersonId);
    List<HouseholdMember> livingWith = HouseholdUtil.getParentsLivingWith(members, member, primaryTaxFilerPersonId);

    boolean livesWithAllParents  = parents.containsAll(livingWith);

    boolean parentsFilingJointly = HouseholdUtil.isParentsLivingWithFilingStatus(members, member, TaxFilingStatus.FILING_JOINTLY, primaryTaxFilerPersonId);

    /*
     * Check exception 2
     */
    exception2 = memberAge < ADULT_AGE && (livesWithAllParents && !parentsFilingJointly);

    boolean custodialParent = false;
    BloodRelationshipCode primaryRelationToMember = HouseholdUtil.getRelationOf(members, member, primaryTaxFiler);

    if (!HouseholdUtil.isCustodialParentRelationship(primaryRelationToMember)) {
      custodialParent = true;
    }

    /*
     * Check exception 3
     */
    exception3 = memberAge < ADULT_AGE && custodialParent;

    /*
     * Return true exceptions: 1, 2 are true, or 3 is true
     */
    return exception1 || exception2 || exception3;
  }

  private static boolean isSpouseLivingApartAndFilingSeparately(final List<HouseholdMember> members, HouseholdMember member, int primaryTaxFilerPersonId) {
    List<HouseholdMember> relatedMembers = HouseholdUtil.getRelatedPeople(members, member.getPersonId(), primaryTaxFilerPersonId);
    for (HouseholdMember hhm : relatedMembers) {
      if (HouseholdUtil.getRelationOf(members, hhm, member) == BloodRelationshipCode.SPOUSE &&
          !HouseholdUtil.isLivingWith(members, hhm, BloodRelationshipCode.SPOUSE, primaryTaxFilerPersonId)) {
        TaxHouseholdComposition composition = hhm.getTaxHouseholdComposition();
        return composition.getTaxFilingStatus() == TaxFilingStatus.FILING_SEPARATELY;
      }
    }
    return false;
  }

  public static boolean isPregnant(final HouseholdMember member) {
    final SpecialCircumstances specialCircumstances = member.getSpecialCircumstances();

    return specialCircumstances != null && specialCircumstances.getPregnantIndicator() == Boolean.TRUE;
  }

  static int getNumberOfExpectedChildren(final HouseholdMember member) {
    if (isPregnant(member)) {
      return member.getSpecialCircumstances().getNumberBabiesExpectedInPregnancy() != null ?
          member.getSpecialCircumstances().getNumberBabiesExpectedInPregnancy() : 0;
    }

    return 0;
  }

  static List<HouseholdMember> getClaimers(final List<HouseholdMember> members, HouseholdMember member) {
    final TaxHouseholdComposition taxComposition = member.getTaxHouseholdComposition();

    final List<HouseholdMember> claimants = members.stream()
        .filter(
            hhm -> taxComposition.getClaimerIds().contains(hhm.getPersonId()))
        .collect(Collectors.toList());

    return claimants;
  }

  static boolean isClaimedByAnyoneOtherThanParent(final List<HouseholdMember> members, HouseholdMember member) {
    final List<HouseholdMember> claimers = getClaimers(members, member);
    return claimers.stream().anyMatch(claimer -> HouseholdUtil.getRelationOf(members, member, claimer) !=  BloodRelationshipCode.CHILD);
  }

  static boolean isClaimedByParent(final List<HouseholdMember> members, HouseholdMember member) {
    final List<HouseholdMember> claimers = getClaimers(members, member);
    return claimers.stream().anyMatch(claimer -> HouseholdUtil.getRelationOf(members, member, claimer) ==  BloodRelationshipCode.CHILD);
  }

  private static List<HouseholdMember> getDependents(final List<HouseholdMember> members, HouseholdMember member) {
    final int personId = member.getPersonId();
    final List<HouseholdMember> dependents = new ArrayList<>();

    for (HouseholdMember hhm : members) {
      if (hhm.getPersonId() == personId) {
        continue;
      }

      final TaxHouseholdComposition taxComposition = hhm.getTaxHouseholdComposition();
      final Set<Integer> claimerIds = taxComposition.getClaimerIds();

      if (claimerIds.contains(personId)) {
        dependents.add(hhm);
      }
    }

    return dependents;
  }

  private static List<HouseholdMember> getDependentsExcept(final List<HouseholdMember> members, HouseholdMember member, int excludePersonId) {
    List<HouseholdMember> dependents = getDependents(members, member);
    return dependents.stream().filter(dep -> dep.getPersonId() != excludePersonId).collect(Collectors.toList());
  }
}
