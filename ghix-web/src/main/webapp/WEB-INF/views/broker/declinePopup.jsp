<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<body>
		<div id="declinebox gutter10-lr">
		
			<form class="form-vertical margin0" id="validatedecline"
				name="validatedecline" action="decline" method="POST"
				target="_parent">
				<df:csrfToken/>
				<input type="hidden" name="desigId" id="desigId" value="" /> 
				<input type="hidden" name="desigStatus" id="desigStatus" value="" />
				<input type="hidden" name="fromModule" id="fromModule" value="" />


				<div class="profile">
					<div id="confirmationMessage">
						<p><spring:message code="label.agent.employers.confirmationMessage1"/> 
							<c:choose>
									<c:when test="${CA_STATE_CODE}">
									${firstName} ${lastName}	 <spring:message code="label.agent.employers.confirmationMessage1.a"/> 
									</c:when>
							</c:choose>
						</p>
						<p><spring:message code="label.agent.employers.confirmationMessage2"/></p>
					</div>
					<div class="marginTop20">						
						<button name="submitRequest" id="submitRequest" type="submit" onClick="declineRequest();" title="Confirm" class="btn btn-primary pull-right" ><spring:message code="label.brkConfirm"/></button>
						<button class="btn pull-left" title="Cancel" type="button"	data-dismiss="modal"  onClick="closeIFrame();" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
					</div>
				</div>
			</form>
		</div>
		
</body>

		
<script type="text/javascript">
	$(document).ready(function() {
		var desigId = getParameterByName('desigId');
		var desigName = getParameterByName('desigName');
		var desigStatus = getParameterByName('prevStatus');
		var fromModule = getParameterByName('fromModule');
		if(desigId.indexOf(',') !== -1) {
			desigId = '${desigId}';
		}

		$('#desigId').val(desigId);
		$('#desigStatus').val(desigStatus);
		$('#fromModule').val(fromModule);
		//$('#header').html("<h4>" + desigName + "</h4>");
	});

	function declineRequest() {
		var formAction = "decline/" + $('#desigId').val() + "/"
		+ $('#fromModule').val() + "?prevStatus="
		+ $('#desigStatus').val();
		
		if($('#desigId').val().indexOf(',') !== -1) {
			formAction = "declineRequests/" + $('#fromModule').val()+ "?ids=" + $('#desigId').val() + "&prevStatus="
					+ $('#desigStatus').val();
		}
		$("#validatedecline").attr('action', formAction);
		$("#validatedecline").submit();
	}

	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null)
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function closeIFrame() {
		$("#imodal").remove();
		parent.location.reload();
		window.parent.closeIFrame();
	}
</script>
