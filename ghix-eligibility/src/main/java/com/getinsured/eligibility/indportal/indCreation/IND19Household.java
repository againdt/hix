package com.getinsured.eligibility.indportal.indCreation;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.resp.service.EligibilityService;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.ind19.client.Ind19ClientConstants;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.eligibility.indportal.dto.HouseholdMemberDTO;
import com.getinsured.eligibility.indportal.dto.IndividualDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO.Relationships;
import com.getinsured.eligibility.indportal.util.IndportalUtil;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.repository.Ind19SsapApplicationEventRepository;
import com.getinsured.eligibility.ui.dto.EligibilityProgramDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.indportal.dto.AppGroupResponse.GroupType;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.plandisplay.individualinformation.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Race;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ObjectFactory;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesVal;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.SsapApplicantDto;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TSCalendar;

@Service("IND19Household")
public class IND19Household implements IHousehold {
	@Autowired private ResourceCreator resourceCreator;
	@Autowired private EligibilityService eligibilityService;
	@Autowired private Ind19SsapApplicationEventRepository ind19SsapApplicationEventRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private EnrollmentsExtractionService enrollmentsExtractionService;
	@Autowired private IndportalUtil indportalUtil;
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	
	private	final ObjectFactory objectFactory = new ObjectFactory();
	private static final Logger LOGGER = Logger.getLogger(IND19Household.class);	
	
	public static final int TYPE_0_EVENT = 0;
	public static final int TYPE_1_EVENT = 1;
	public static final int TYPE_2_EVENT = 2;
	public static final int TYPE_3_EVENT = 3;
	public static final int TYPE_4_EVENT = 4;
	
	private static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
			
	@Override
	public Object createResponsiblePerson(HouseholdData householdData) {
		HouseholdMemberDTO householdMemberDTO = getHouseholdMemberDTO(householdData.getApplicationData(),SsapApplicantPersonType.PTF.getPersonType());
		ResponsiblePerson responsiblePerson = objectFactory.createIndividualInformationRequestHouseholdResponsiblePerson();
		responsiblePerson.setResponsiblePersonId(householdMemberDTO.getId());		
		responsiblePerson.setResponsiblePersonFirstName(householdMemberDTO.getFirstName());
		responsiblePerson.setResponsiblePersonLastName(householdMemberDTO.getLastName());
		responsiblePerson.setResponsiblePersonMiddleName(householdMemberDTO.getMiddleName());
		responsiblePerson.setResponsiblePersonSuffix(householdMemberDTO.getSuffix());
		responsiblePerson.setResponsiblePersonSsn(householdMemberDTO.getSsn());		
		responsiblePerson.setResponsiblePersonPrimaryPhone(householdMemberDTO.getPrimaryPhone());		
		responsiblePerson.setResponsiblePersonSecondaryPhone(householdMemberDTO.getSecondaryPhone());		
		responsiblePerson.setResponsiblePersonPreferredEmail(householdMemberDTO.getPreferredEmail());	
		responsiblePerson.setResponsiblePersonPreferredPhone(householdMemberDTO.getPreferredPhone());
		responsiblePerson.setResponsiblePersonHomeAddress1(householdMemberDTO.getHomeAddress1());
		responsiblePerson.setResponsiblePersonHomeAddress2(householdMemberDTO.getHomeAddress2());
		responsiblePerson.setResponsiblePersonHomeCity(householdMemberDTO.getHomeCity());
		responsiblePerson.setResponsiblePersonHomeState(householdMemberDTO.getHomeState());
		responsiblePerson.setResponsiblePersonHomeZip(householdMemberDTO.getHomeZip());
		responsiblePerson.setResponsiblePersonType(householdMemberDTO.getType());
		return responsiblePerson;
	}

	@Override
	public Object createHouseHoldContact(HouseholdData householdData) {
		HouseholdMemberDTO householdMemberDTO = getHouseholdMemberDTO(householdData.getApplicationData(),SsapApplicantPersonType.PC.getPersonType());
		HouseHoldContact houseHoldContact = objectFactory.createIndividualInformationRequestHouseholdHouseHoldContact();
		houseHoldContact.setHouseHoldContactId(householdMemberDTO.getId());
		houseHoldContact.setHouseHoldContactFirstName(householdMemberDTO.getFirstName());
		houseHoldContact.setHouseHoldContactMiddleName(householdMemberDTO.getMiddleName());
		houseHoldContact.setHouseHoldContactLastName(householdMemberDTO.getLastName());
		houseHoldContact.setHouseHoldContactSuffix(householdMemberDTO.getSuffix());
		houseHoldContact.setHouseHoldContactFederalTaxIdNumber(householdMemberDTO.getSsn());
		houseHoldContact.setHouseHoldContactPrimaryPhone(householdMemberDTO.getPrimaryPhone());
		houseHoldContact.setHouseHoldContactSecondaryPhone(householdMemberDTO.getSecondaryPhone());
		houseHoldContact.setHouseHoldContactPreferredEmail(householdMemberDTO.getPreferredEmail());
		houseHoldContact.setHouseHoldContactPreferredPhone(householdMemberDTO.getPreferredPhone());
		houseHoldContact.setHouseHoldContactHomeAddress1(householdMemberDTO.getHomeAddress1());
		houseHoldContact.setHouseHoldContactHomeAddress2(householdMemberDTO.getHomeAddress2());
		houseHoldContact.setHouseHoldContactHomeCity(householdMemberDTO.getHomeCity());
		houseHoldContact.setHouseHoldContactHomeState(householdMemberDTO.getHomeState());
		houseHoldContact.setHouseHoldContactHomeZip(householdMemberDTO.getHomeZip());
		return houseHoldContact;
	}
	
	@Override
	public Object createIndividual(HouseholdData householdData, SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest) throws GIException {
		LocalDate coverageDate = new LocalDate(householdData.getCoverageDate().getTime());
		int applicaitonCoverageYear = (int) ssapApplication.getCoverageYear();
		if(coverageDate.getYear() != applicaitonCoverageYear){
			throw new GIException("Enrollment start date spills to next year. Start Date: " + coverageDate +", Coverage Year: " + applicaitonCoverageYear);
		}
		
		Group group = ind19ClientRequest.getGroup();
		if(group != null && StringUtils.isNotBlank(group.getEnrollmentEndDate()) && !group.getIsRenewal()){
			Date enrlEndDate = DateUtil.StringToDate(group.getEnrollmentEndDate(), "MM/dd/yyyy");
        	if(enrlEndDate.before(householdData.getCoverageDate())){
        		throw new GIException(Ind19ClientConstants.ENRL_END_DATE_IS_BEFORE_COVG_DATE);        		
        	}
		}       
        
		SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = getApplicationData(householdData.getApplicationData());
		IndividualDTO individualDTO = populateIndividualDTO(ssapApplication, householdData.getCoverageDate(), ind19ClientRequest, ssapApplicationEligibilityDTO);
		Individual individual = objectFactory.createIndividualInformationRequestHouseholdIndividual();
		individual.setHouseholdCaseId(individualDTO.getHouseholdCaseId());
		individual.setCoverageStartDate(individualDTO.getCoverageStartDate());
		individual.setEhbAmount(individualDTO.getEhbAmount());
		
		if("S".equalsIgnoreCase(householdData.getEnrollmentType())){
			individual.setEnrollmentType(EnrollmentVal.S);
		} else {
			individual.setEnrollmentType(EnrollmentVal.I);
		}
		if(group != null && group.getIsRenewal()) {
			individual.setEnrollmentType(EnrollmentVal.A);
		}
		individual.setFinancialEffectiveDate(individualDTO.getFinancialEffectiveDate());
		individual.setSerc(individualDTO.getSerc());
		if(group != null){
			String minCsrInGroup = getMinMemberCsr(group.getMemberList());	

			group.setGroupCsr(minCsrInGroup);
			individual.setCsr(group.getGroupCsr());

			BigDecimal totalAptc = sumGroupMembersAptc(group.getMemberList(), group.getMaxAptc(), group.getGroupType());
			BigDecimal totalStateSubsidy = sumGroupMembersStateSubsidy(group.getMemberList(), group.getMaxStateSubsidy());
			if(totalAptc != null){
				group.setMaxAptc(totalAptc.setScale(2,BigDecimal.ROUND_HALF_UP));
				individual.setAptc(group.getMaxAptc().floatValue());
			}
			if(totalStateSubsidy != null){
				group.setMaxStateSubsidy(totalStateSubsidy.setScale(2,BigDecimal.ROUND_HALF_UP));
				individual.setStateSubsidy(group.getMaxStateSubsidy());
			}
		}else {
			individual.setAptc(individualDTO.getAptc());
			individual.setStateSubsidy(individualDTO.getStateSubsidy());
			individual.setCsr(individualDTO.getCsr());
		}
		return individual;
	}
	
	private BigDecimal sumGroupMembersAptc(List<com.getinsured.hix.indportal.dto.Member> memberList, BigDecimal maxAptc, GroupType groupType){
		BigDecimal totalAptc = null;
		int memberWithNullAPTCPresentCount = 0;
		if(memberList != null){
			for(com.getinsured.hix.indportal.dto.Member member : memberList){
				if(member.getAptc() == null) {
					memberWithNullAPTCPresentCount++;
				}
			}
		
			if(memberWithNullAPTCPresentCount>0) {
				totalAptc = maxAptc;
				if(memberWithNullAPTCPresentCount != memberList.size()) {
					for(com.getinsured.hix.indportal.dto.Member member : memberList){
						if(member.getAptc() != null && ((GroupType.HEALTH.equals(groupType) && StringUtils.isBlank(member.getHealthEnrollmentId())) || 
								(GroupType.DENTAL.equals(groupType) && StringUtils.isBlank(member.getDentalEnrollmentId())))) {
							totalAptc = totalAptc != null ? totalAptc.add(member.getAptc()) : member.getAptc();
						}
					}
				}
			}else {
				for(com.getinsured.hix.indportal.dto.Member member : memberList){
					if(member.getAptc() != null) {
						totalAptc = totalAptc != null ? totalAptc.add(member.getAptc()) : member.getAptc();
					}
				}
			}
		}
		
		return totalAptc;	
	}

	private BigDecimal sumGroupMembersStateSubsidy(List<com.getinsured.hix.indportal.dto.Member> memberList, BigDecimal maxStateSubsidy){
		BigDecimal totalStateSubsidy = null;
		int memberWithNullStateSubsidyPresentCount = 0;
		if(memberList != null){
			for(com.getinsured.hix.indportal.dto.Member member : memberList){
				if(member.getStateSubsidy() == null) {
					memberWithNullStateSubsidyPresentCount++;
				}
			}

			if(memberWithNullStateSubsidyPresentCount>0) {
				totalStateSubsidy = maxStateSubsidy;
				if(memberWithNullStateSubsidyPresentCount != memberList.size()) {
					for(com.getinsured.hix.indportal.dto.Member member : memberList){
						if(member.getStateSubsidy() != null && StringUtils.isBlank(member.getHealthEnrollmentId())) {
							totalStateSubsidy = totalStateSubsidy != null ? totalStateSubsidy.add(member.getStateSubsidy()) : member.getStateSubsidy();
						}
					}
				}
			}else {
				for(com.getinsured.hix.indportal.dto.Member member : memberList){
					totalStateSubsidy = totalStateSubsidy != null ? totalStateSubsidy.add(member.getStateSubsidy()) : member.getStateSubsidy();
				}
			}
		}

		return totalStateSubsidy;
	}
	
	// Non AI/AN members can not enroll into CS2/CS3 plans so when a mixed group of AI/AN and non AI/AN members shop together, the group get non AI/AN member CS
	private String getMinMemberCsr(List<com.getinsured.hix.indportal.dto.Member> memberList) {
		String groupCsr = null;
		if(memberList != null){
			for(com.getinsured.hix.indportal.dto.Member member : memberList){
				if(member != null){
					String memberCsr = StringUtils.isNotBlank(member.getCsr()) ? member.getCsr() : "CS1";
					if("CS1".equalsIgnoreCase(memberCsr)) {
						return memberCsr;
					}
					if(groupCsr == null){
						groupCsr = memberCsr;
					}
					if(!"CS2".equalsIgnoreCase(memberCsr) && !"CS3".equalsIgnoreCase(memberCsr)) {
						groupCsr = memberCsr;
					}
				}
			}					
		}
		return groupCsr;
   }
	
	@Override
	public Object createMembers(Object membersObject, SsapApplication ssapForInd19, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData, String userName, List<String> memberGUIDs) throws GIException {
		
		boolean issep = householdData.isIssep();
		String exemptHousehold = ssapForInd19.getExemptHousehold();
		Long ssapApplicationId = ssapForInd19.getId();
		Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap = householdData.getSsapApplicantEventPrimaryKeyMap();
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = householdData.getEnrollmentsByTypeMap();
		Map<Long, SsapApplicantResource> ssapApplicants = null;
		try {
			ssapApplicants = resourceCreator.getApplicantsByApplicationId(ssapForInd19.getId());
		} catch (Exception e) {
			throw new GIException("Exception while creating ResourceCreator.");
		}								
		
		Members members = (Members) membersObject;
		
		//List<SsapApplicantsDataDTO> ssapApplicantsDataList = populateSsapApplicantsDataDTO(ssapApplicants);
		List<MemberDTO> memberDTOList = populateMembersDTO(householdData.getApplicationData(), ssapApplicants);
		
		List<MemberDTO.Relationships> relationshipsList = new ArrayList<>();
		for(MemberDTO memberDTO : memberDTOList){
			if(memberDTO.getRelationships() != null && !memberDTO.getRelationships().isEmpty()){
				relationshipsList = memberDTO.getRelationships();
			}
		}	

		if(ind19ClientRequest.getGroup() != null && ind19ClientRequest.getGroup().getMemberList() != null && ind19ClientRequest.getGroup().getMemberList().size() > 0) {
			memberDTOList = filterGroupMembers(ind19ClientRequest.getGroup(), memberDTOList);
		}
		populatePrimaryPhone(memberDTOList, ind19ClientRequest.getPhoneNumber());
		populateMailingAddress(householdData.getApplicationData(), memberDTOList);
		populateOtherMembersData(ssapApplicants, ssapApplicantEventPrimaryKeyMap, memberDTOList, issep, enrollmentsByTypeMap, exemptHousehold, ind19ClientRequest,householdData.getProductType(), householdData.getEnrollmentType());
		
		HouseholdMember primaryTaxFilerDTO = getResponsibleMemberData(householdData.getApplicationData());
		
		for(MemberDTO memberDTO : memberDTOList){
			Member member = (Member) populateMember(memberDTO);
			populateRelationship(member,  relationshipsList, memberDTOList,primaryTaxFilerDTO);
			members.getMember().add(member);
		}
		
		Set<String> setOfApplicants = createSetOfApplicantIds(householdData.getApplicationData());
		SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = getApplicationData(householdData.getApplicationData());
		
		Set<String> eligibleMembersIds = new HashSet<String>();
		assignEligibilityDetailsAndEligibleMembers(ssapApplicationEligibilityDTO, eligibleMembersIds, setOfApplicants, memberDTOList);		
		// coverageStartDate = coverageStartDateService.computeCoverageStartDate(ssapApplicationId!=null?BigDecimal.valueOf(ssapApplicationId):BigDecimal.ZERO, enrollmentsByTypeMap, ind19ClientRequest.isChangePlanFlow(), ind19ClientRequest.isSEPPlanFlow());
				
		assignGuidsToMembers(members, memberDTOList);
		setCatastrophicEligible(exemptHousehold, eligibleMembersIds, householdData.getCoverageDate(), members);
		Members eligibleMembers = objectFactory.createIndividualInformationRequestHouseholdMembers();
		if(members.getMember() != null && !members.getMember().isEmpty()){
			for(IndividualInformationRequest.Household.Members.Member member:members.getMember()){
				if(eligibleMembersIds.contains(member.getMemberId()) && setOfApplicants!=null && setOfApplicants.contains(member.getMemberId())){
					eligibleMembers.getMember().add(member);
				}
				List<com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship> memberRelationships = member.getRelationship();
				List<com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship> memberRelationshipsTemp = new ArrayList<IndividualInformationRequest.Household.Members.Member.Relationship>();
				for(com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members.Member.Relationship relationship:memberRelationships){
					if(eligibleMembersIds.contains(relationship.getMemberId()) && setOfApplicants!=null && setOfApplicants.contains(relationship.getMemberId())){
						memberRelationshipsTemp.add(relationship);
					}
				}
				member.getRelationship().clear();
				member.getRelationship().addAll(memberRelationshipsTemp);
			}
		}
		
		if(ind19ClientRequest.getIsChangePlanFlow() || ind19ClientRequest.getIsSEPPlanFlow()){				
			setMemberOtherData(enrollmentsByTypeMap, eligibleMembers.getMember(), householdData.getProductType());				
		}
		return eligibleMembers;
	}
	
	private List<MemberDTO> filterGroupMembers(Group group, List<MemberDTO> memberDTOList) {
		List<MemberDTO> groupMemberDTOList = new ArrayList<>();
		if(group != null && group.getMemberList() != null){
			for(MemberDTO memberDTO : memberDTOList){
				for(com.getinsured.hix.indportal.dto.Member member : group.getMemberList()){
					if(member != null && StringUtils.isNotBlank(member.getId()) && member.getId().equalsIgnoreCase(memberDTO.getApplicantGuid())){
						groupMemberDTOList.add(memberDTO);
					}
				}
			}		
		}else{
			groupMemberDTOList = memberDTOList;
		}
		return groupMemberDTOList;
	}

	@Override
	public void setHouseholdData(SsapApplication ssapForInd19, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData) throws GIException {
		SingleStreamlinedApplication applicationData = getSingleStreamlinedApplication(ssapForInd19.getApplicationData());
		householdData.setApplicationData(applicationData);
		
		boolean issep = false;
		SsapApplicationEvent ssapApplicationEvent = null;
		int coverageYear = ind19ClientRequest.getCoverageYear();
		Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap = new HashMap<Long, SsapApplicantEvent>(0);
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		
		boolean hasExistingMembers = hasExistingMembers(ind19ClientRequest, householdData);
		if(!hasExistingMembers){
			householdData.setEnrollmentType(EnrollmentVal.I.toString());
		} else {
			householdData.setEnrollmentType(EnrollmentVal.S.toString());
		}
		
		if(hasExistingMembers){
			//This should be done only for SEP
			if(StringUtils.equalsIgnoreCase(ssapForInd19.getApplicationType(),"SEP") && !ind19ClientRequest.getIsChangePlanFlow()){
				issep = true;
				ssapApplicationEvent = getSsapApplicationEventById(ssapForInd19.getId());
				if(ssapApplicationEvent != null){
					householdData.setSsapApplicationEvent(ssapApplicationEvent);
					ssapApplicantEventPrimaryKeyMap = createSsapApplicantEventPrimaryKeyMap(ssapApplicationEvent.filteredPreferredSsapApplicantEvents());
					setKeepOnlyFlag(ssapApplicationEvent, householdData);
				}					
			}
			/*
			 * Fetch the existing enrollment which is in EN state
			 */
			SsapApplication currentSsap  = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(ssapForInd19.getCmrHouseoldId(), coverageYear);
			if(currentSsap == null){
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				boolean isNotCAState = "CA".equalsIgnoreCase(stateCode) ? false : true;	
				if(isNotCAState || (!isNotCAState && ind19ClientRequest.getGroup().getGroupType() == GroupType.DENTAL)){
					List<SsapApplication> ssapApplicationsList = ssapApplicationRepository.findLatestSsapApplicationByHouseholdId(ssapForInd19.getCmrHouseoldId());
					if(ssapApplicationsList != null && ssapApplicationsList.size() > 0 ){
						for(SsapApplication ssapApplication : ssapApplicationsList){
							if(ssapApplication != null && "ER".equalsIgnoreCase(ssapApplication.getApplicationStatus()) && (isNotCAState || (!isNotCAState && "EN".equalsIgnoreCase(ssapApplication.getApplicationDentalStatus()) && "OE".equalsIgnoreCase(ssapApplication.getApplicationType())))){
								currentSsap = ssapApplication;
								break;
							}
						}						
					}
					if(currentSsap == null){
						throw new GIException("There are no enrolled SSAPs for this household for coverage year "+ coverageYear);
					}
				}else{
					throw new GIException("There are no enrolled SSAPs for this household for coverage year "+ coverageYear);
				}				
			}
			enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(currentSsap.getId(), getGroupMemberGUIDs(ind19ClientRequest.getGroup()));
			if(enrollmentsByTypeMap==null || enrollmentsByTypeMap.isEmpty()){
				SsapApplication currentErSsap  = ssapApplicationRepository.findErAfterLatestEnPnSsapApplicationForCoverageYear(ssapForInd19.getCmrHouseoldId(), coverageYear);
				if(currentErSsap == null){
					throw new GIException("There are no ER SsapApplication for this household for coverage year "+ coverageYear);
				}
				enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(currentErSsap.getId(), getGroupMemberGUIDs(ind19ClientRequest.getGroup()));
				if(enrollmentsByTypeMap==null || enrollmentsByTypeMap.isEmpty()){
					throw new GIException("Unable to get Enrollment Plan Details for the SEP application with casenumber "+currentErSsap.getCaseNumber());
				}
			}
			
			
			/*if(enrollmentsByTypeMap==null || enrollmentsByTypeMap.isEmpty()){
				throw new GIException("Existing enrollments could not be retrieved for the SEP application with casenumber "+currentSsap.getCaseNumber());
			}*/
			/*
			 * HIX-56781 Map dental enrollment ID if the member is enrolled in dental plan
			 * 
			 * HIX-59744 - Map existing enrollments for native Americans who are trying
			 * to change plan
			 */
			householdData.setPriorSsapApplicationId(currentSsap.getId());
			
			setTerminationFlag(enrollmentsByTypeMap, householdData);
			setDentalTerminationFlag(enrollmentsByTypeMap, householdData);
				
			
		}

		householdData.setIssep(issep);
		householdData.setEnrollmentsByTypeMap(enrollmentsByTypeMap);
		householdData.setSsapApplicantEventPrimaryKeyMap(ssapApplicantEventPrimaryKeyMap);
	}
	


	public boolean hasExistingMembers(Ind19ClientRequest ind19ClientRequest, HouseholdData householdData) {
		boolean hasExistingMembers = false;
		if(ind19ClientRequest.getGroup() != null && CollectionUtils.isNotEmpty(ind19ClientRequest.getGroup().getMemberList()))
		{
			for(com.getinsured.hix.indportal.dto.Member member : ind19ClientRequest.getGroup().getMemberList())
			{
				if(ProductType.A.value().equalsIgnoreCase(householdData.getProductType()) && (StringUtils.isNotBlank(member.getHealthEnrollmentId()) || StringUtils.isNotBlank(member.getDentalEnrollmentId()))) {
 					hasExistingMembers = true;
 					break;
				}else if(ProductType.H.value().equalsIgnoreCase(householdData.getProductType()) && StringUtils.isNotBlank(member.getHealthEnrollmentId())) {
					hasExistingMembers = true;
					break;
				}else if(ProductType.D.value().equalsIgnoreCase(householdData.getProductType()) && StringUtils.isNotBlank(member.getDentalEnrollmentId())) {
					hasExistingMembers = true;
					break;
				}
			}
		}
		else
		{
			// ---  for non grouping scenarios the hasExistingMembers should be ignored and flow should only work on SEP applicationType
			if(householdData.isIssep()) {
				hasExistingMembers = true;
			} else {
				hasExistingMembers = false;
			}
		}
		return hasExistingMembers;
	}
	
	private List<String> getGroupMemberGUIDs(Group group) {
		List<String> memberGUIDs = null;
		if(group != null && CollectionUtils.isNotEmpty(group.getMemberList()))
		{
			memberGUIDs = new ArrayList<String>(0);
			for(com.getinsured.hix.indportal.dto.Member member : group.getMemberList())
			{
				memberGUIDs.add(member.getId());
			}
		}
		return memberGUIDs;
	}

	private void setDentalTerminationFlag(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, HouseholdData householdData) {
		EnrollmentShopDTO dentalEnrollments = enrollmentsByTypeMap.get("dental");
		if(dentalEnrollments != null){
			String dentalEnrollmentStatus = dentalEnrollments.getEnrollmentStatusLabel();
			if(StringUtils.equalsIgnoreCase(dentalEnrollmentStatus, "terminated")){
				householdData.setDentalTerminationFlag(YesVal.Y.toString());
			}	
		}		
	}

	private void setTerminationFlag(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, HouseholdData householdData) {
		EnrollmentShopDTO healthEnrollments = enrollmentsByTypeMap.get("health");
		if(healthEnrollments != null){	
			String healthEnrollmentStatus = healthEnrollments.getEnrollmentStatusLabel();
			if(StringUtils.equalsIgnoreCase(healthEnrollmentStatus, "terminated")){
				householdData.setTerminationFlag(YesVal.Y.toString());
			}
		}		
	}

	private void setKeepOnlyFlag(SsapApplicationEvent ssapApplicationEvent, HouseholdData householdData) {
		if(StringUtils.isNotBlank(ssapApplicationEvent.getKeepOnly()) && "Y".equalsIgnoreCase(ssapApplicationEvent.getKeepOnly())){
			householdData.setKeepOnlyFlag(YesNoVal.Y.toString());
		}else if(StringUtils.isNotBlank(ssapApplicationEvent.getKeepOnly()) && "N".equalsIgnoreCase(ssapApplicationEvent.getKeepOnly())){
			householdData.setKeepOnlyFlag(YesNoVal.N.toString());
		}	
	}

	private SsapApplicationEvent getSsapApplicationEventById(Long applicationId) throws GIException{
		return ind19SsapApplicationEventRepository.findBySsapApplicationId(applicationId);
	}

	private Map<Long, SsapApplicantEvent> createSsapApplicantEventPrimaryKeyMap(List<SsapApplicantEvent> ssapApplicantEvents) {
		Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap = new HashMap<Long, SsapApplicantEvent>(0);
		if(ssapApplicantEvents != null && !ssapApplicantEvents.isEmpty()){
			for(SsapApplicantEvent ssapApplicantEvent:ssapApplicantEvents){
				ssapApplicantEventPrimaryKeyMap.put(ssapApplicantEvent.getSsapApplicant().getId(), ssapApplicantEvent);
			}
		}		
		return ssapApplicantEventPrimaryKeyMap;
	}
	
	
	@Override
	public Object createDisenrollMembers(Ind19ClientRequest ind19ClientRequest, HouseholdData householdData) {
		
		if(householdData.isIssep() && !ind19ClientRequest.getIsChangePlanFlow() && !ind19ClientRequest.getIsSEPPlanFlow()){
			Date coverageStartDate = householdData.getCoverageDate();
			if(householdData.getSsapApplicationEvent() != null){
				Calendar termianteDate = getTerminationDate(householdData.getSsapApplicationEvent().filteredPreferredSsapApplicantEvents(), householdData.getEnrollmentsByTypeMap(), coverageStartDate);
				return populateDisenrolledMembers(householdData.getSsapApplicationEvent().filteredPreferredSsapApplicantEvents(), householdData.getEnrollmentsByTypeMap(),termianteDate, householdData.getProductType());	
			}			
		}			
		return null;
	}
	
	private void setMemberOtherData(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, List<Member> memberList, String productType) {
		if(memberList != null){
			Integer existingMedicalEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "health");
			Integer existingSADPEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "dental");
			Set<String> healthPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "health");
			Set<String> dentalPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "dental");
			String healthEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "health");
			String dentalEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "dental");
			
			for(IndividualInformationRequest.Household.Members.Member member:memberList){
				//Add MRC code of AI - no reason given
				member.setMaintenanceReasonCode("AI");
				//Existing enrollment Ids
				if(existingMedicalEnrollmentId != null && existingMedicalEnrollmentId != 0 && 
				        !StringUtils.equalsIgnoreCase(healthEnrollmentStatus, "cancelled")){
					if(healthPlanEnrollees.contains(member.getMemberId())){
						if(ProductType.A.value().equalsIgnoreCase(productType) || ProductType.H.value().equalsIgnoreCase(productType)){
							member.setExistingMedicalEnrollmentID(String.valueOf(existingMedicalEnrollmentId));
							//New person flag
							member.setNewPersonFLAG(YesNoVal.N);
						}						
					}
				}
				if(existingSADPEnrollmentId != null && existingSADPEnrollmentId != 0 
				        && !StringUtils.equalsIgnoreCase(dentalEnrollmentStatus, "cancelled")){
					if(dentalPlanEnrollees.contains(member.getMemberId())){
						if(ProductType.A.value().equalsIgnoreCase(productType) || ProductType.D.value().equalsIgnoreCase(productType)){
							member.setExistingSADPEnrollmentID(String.valueOf(existingSADPEnrollmentId));
							//New person flag
							member.setNewPersonFLAG(YesNoVal.N);
						}
					}
				}
			}
		}	
	}
	
	private Calendar getTerminationDate(List<SsapApplicantEvent> applicantEvents, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, Date coverageStartDate) {
		if(applicantEvents == null || enrollmentsByTypeMap == null){
			return null;
		}
		
		Map<Integer, List<Timestamp>> eventTimestamps = getSEPRemoveEventTimestamps(applicantEvents);
		List<Timestamp> type1Timestamps = eventTimestamps.get(TYPE_1_EVENT);
		Calendar terminationDate = TSCalendar.getInstance();
		
		//Check for type 1 event
		if(type1Timestamps != null && !type1Timestamps.isEmpty()){
			/*coverage date already includes the logic for type1 terminations in coverageStartDateService*/
			terminationDate.setTime(coverageStartDate);
			return terminationDate;
		}else{
			//Termination date is 1 day before computed coverage start date
			terminationDate.setTime(coverageStartDate);
			terminationDate.add(Calendar.DATE, -1);
			return terminationDate;
		}
	}
	
	private Map<Integer, List<Timestamp>> getSEPRemoveEventTimestamps(List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.debug("Getting Event times for SSAP Applicant events");
		Map<Integer, List<Timestamp>> eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		
		if(applicantEvents == null || applicantEvents.isEmpty()){
			return eventTimestamps;
		}
		
		List<Timestamp> type1EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type2EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type3EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type4EventDates = new ArrayList<Timestamp>(0);
		
		for (SsapApplicantEvent event : applicantEvents) {

			SepEvents sepEvent = event.getSepEvents();
			if (sepEvent != null && StringUtils.equalsIgnoreCase(sepEvent.getChangeType(), "REMOVE")) {
				switch (sepEvent.getType()) {
				case TYPE_1_EVENT:
					type1EventDates.add(event.getEventDate());
					break;
				case TYPE_2_EVENT:
					type2EventDates.add(event.getEventDate());
					break;
				case TYPE_3_EVENT:
					type3EventDates.add(event.getEventDate());
					break;
				case TYPE_4_EVENT:
					type4EventDates.add(event.getEventDate());
					break;
				default:
					LOGGER.warn("Invalid Event Type");
				}
			}
		}
		eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		eventTimestamps.put(TYPE_1_EVENT, type1EventDates);
		eventTimestamps.put(TYPE_2_EVENT, type2EventDates);
		eventTimestamps.put(TYPE_3_EVENT, type3EventDates);
		eventTimestamps.put(TYPE_4_EVENT, type4EventDates);
		
		LOGGER.debug("Event times: " + eventTimestamps);
		return eventTimestamps;
	}

	private DisenrollMembers populateDisenrolledMembers(List<SsapApplicantEvent> applicantEvents,
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap,Calendar terminationDate, String productType) {
		
		IndividualInformationRequest.Household.DisenrollMembers disenrollMembers = 	objectFactory.createIndividualInformationRequestHouseholdDisenrollMembers();
	
		Map<String, List<String>> memberIdEnrollmentListMap = new HashMap<String, List<String>>();

		//Create the list of enrollments for disenrolled members
		for (Map.Entry<String, EnrollmentShopDTO> entry : enrollmentsByTypeMap.entrySet()){
			boolean addMember=true;
			if("H".equalsIgnoreCase(productType) && !"Health".equalsIgnoreCase(entry.getKey())) {
				addMember = false;
			} else if ("D".equalsIgnoreCase(productType) && !"Dental".equalsIgnoreCase(entry.getKey())) {
				addMember = false;
			}
			if(addMember) {
				EnrollmentShopDTO enrollmentShopDTO = entry.getValue();
				
				List<EnrolleeShopDTO> enrollees = enrollmentShopDTO.getEnrolleeShopDTOList();
				if(enrollees != null && !enrollees.isEmpty()){
					for(EnrolleeShopDTO enrollee:enrollees){
						if(enrollee.getExchgIndivIdentifier() != null){
							if(memberIdEnrollmentListMap.get(enrollee.getExchgIndivIdentifier()) == null){
								memberIdEnrollmentListMap.put(enrollee.getExchgIndivIdentifier(),new ArrayList<String>());
							}
							memberIdEnrollmentListMap.get(enrollee.getExchgIndivIdentifier()).add(String.valueOf(enrollmentShopDTO.getEnrollmentId()));
						}					
					}
				}
			}
		}
		
		
		List<IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember> disenrolledMembers = 
				new ArrayList<IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember>();
		
		/*
		 * HIX-59232
		 * Multiple Terminations with different dates is causing 
		 * inconsistent behavior in Enrollment + 
		 * Enrollment Overlap when consumers change plans during SEP
		 */
		/*Map<Integer, List<Timestamp>> eventTimestamps = getSEPRemoveEventTimestamps(applicantEvents);
		List<Timestamp> type1Timestamps = eventTimestamps.get(Ind19ClientConstants.TYPE_1_EVENT);
		Calendar terminationDate = TSCalendar.getInstance();
		
		//Check for type 1 event
		if(type1Timestamps != null && !type1Timestamps.isEmpty()){
			List<Timestamp> allEventTimestamps = new ArrayList<Timestamp>();
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_1_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_2_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_3_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_4_EVENT));
			allEventTimestamps.add(new Timestamp(coverageStartDate.getTime()));

			Collections.sort(allEventTimestamps,Collections.reverseOrder());
			terminationDate.setTimeInMillis(allEventTimestamps.get(0).getTime());		
		}
		//There is no type 1 event
		else{
			//Termination date is 1 day before computed coverage start date
			terminationDate.setTime(coverageStartDate);
			terminationDate.add(Calendar.DATE, -1);
		}*/
		
		//Check for Removal of applicants during SEP
		for(SsapApplicantEvent applicantEvent : applicantEvents){
			SepEvents sepEvent = applicantEvent.getSepEvents();
			if(sepEvent != null){
				String changeType = sepEvent.getChangeType();
				/*
				 * HIX-70832 IND 19 Improvement to Support Dental CR, 
				 * SEPs in OE Overlap Period, and future dated LCE
				 * 
				 * If the individual is not enrolled either in health or dental, 
				 * but is marked as delete by the change reporting modules (both Financial and Non-Financial) 
				 * then do not populate that person in the dis-enroll loop. 
				 */
				String applicantGuId = applicantEvent.getSsapApplicant().getApplicantGuid();

				List<String> memberEnrollments;

				//Get member enrollments here for SEP remove person. memberIdEnrollmentListMap might not actually contain all enrollments for applicant.
				List<String> applicantGuIdList = new ArrayList<>();
				applicantGuIdList.add(applicantGuId);
				Map<String, EnrollmentShopDTO> disenrolledEnrollmentsByTypeMap = new HashMap<>();

				try {
					SsapApplication ssapApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(
							applicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId(),
							applicantEvent.getSsapAplicationEvent().getSsapApplication().getCoverageYear());

					disenrolledEnrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(
							ssapApplication.getId(),
							applicantGuIdList);

					memberEnrollments = disenrolledEnrollmentsByTypeMap.values().stream()
							.map(EnrollmentShopDTO::getEnrollmentId).collect(Collectors.toList()).stream()
							.map(Objects::toString).collect(Collectors.toList());

				}catch(Exception e){
					LOGGER.error(e);
					memberEnrollments = memberIdEnrollmentListMap.get(applicantGuId);
				}

				if(StringUtils.equalsIgnoreCase(changeType, "REMOVE")
						&& memberEnrollments != null && !memberEnrollments.isEmpty()){
					
					IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember disenrolledMember = 
							objectFactory.createIndividualInformationRequestHouseholdDisenrollMembersDisenrollMember();
					
					disenrolledMember.setMemberId(applicantGuId);
					//Event type is Death
					String mrcCode = sepEvent.getMrcCode();
					if(StringUtils.equalsIgnoreCase(mrcCode,"03")){
						disenrolledMember.setDeathDate(dateToString(SHORT_DATE_FORMAT,applicantEvent.getEventDate()));
					}
					if(terminationDate != null) {
					disenrolledMember.setTerminationDate(dateToString(SHORT_DATE_FORMAT,new Timestamp(terminationDate.getTimeInMillis())));
					disenrolledMember.setTerminationReasonCode(mrcCode);
					}
					
					IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember.Enrollments enrollments = 
							objectFactory.createIndividualInformationRequestHouseholdDisenrollMembersDisenrollMemberEnrollments();
					enrollments.getEnrollmentId().addAll(memberEnrollments);
					
					disenrolledMember.setEnrollments(enrollments);
					//Add memeber to list of disenrolledMembers list
					disenrolledMembers.add(disenrolledMember);
				}
			}
		}
		
		//Add all disenrolled members to the disenrollMembers list
		disenrollMembers.getDisenrollMember().addAll(disenrolledMembers);
		
		return disenrollMembers;
	}
	
	private String dateToString(String format, Timestamp date){		
		if(date == null || StringUtils.isBlank(format)){
			return null;
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		return formatter.format(date);
	}
	
	private void assignGuidsToMembers(Members members,	List<MemberDTO> filteredMemberList) {
		if(members.getMember() != null && !members.getMember().isEmpty()){
			List<Member>  memberList = members.getMember();			
			for(Member member : memberList){
				if(member!=null && member.getMemberId() != null){
					if(filteredMemberList != null && !filteredMemberList.isEmpty()){
						for(MemberDTO memberDTO : filteredMemberList){
							if(memberDTO.getMemberId()!=null && member.getMemberId().equalsIgnoreCase(memberDTO.getMemberId())){
								member.setMemberId(memberDTO.getApplicantGuid());
							}
						}	
					}									
				}
			}	
		}				
	}
	
	private void setCatastrophicEligible(String exemptHousehold, Set<String> eligibleMembersIds, Date coverageStartDate, Members members) {
		if(StringUtils.isBlank(exemptHousehold) || !"Y".equalsIgnoreCase(exemptHousehold)){
			LOGGER.debug("Not hardship exempt. Checking if every member is less than 30");
			boolean isEveryoneUnder30 = true;
			/*
			 * HIX-59481 - Catastrophic plans are not showing up for a 
			 * household where one individual is eligible and 
			 * other not eligible. 
			 */
			//List<String> eligibleMemberIds = eligibleMembersIds.;
			if(members.getMember() != null && !members.getMember().isEmpty()){
				for(IndividualInformationRequest.Household.Members.Member member:members.getMember()){
					if(eligibleMembersIds.contains(member.getMemberId())
							&& getAgeFromDob(member.getDob(),coverageStartDate) >= Ind19ClientConstants.HARDSHIP_EXEMPTION_AGE){
						isEveryoneUnder30 = false;
						break;
					}
				}
			}				
			if(isEveryoneUnder30){
				LOGGER.debug("Not hardship exempt. But everyone under 30");
				if(members.getMember() != null && !members.getMember().isEmpty()){
					for(int i=0;i<members.getMember().size();i++){
						members.getMember().get(i).setCatastrophicEligible(YesNoVal.Y);
					}
				}					
			}
		}		
	}
	
	private int getAgeFromDob(String dob, Date coverageStartDate){
		if(StringUtils.isBlank(dob)){
			return 0;
		}
		//Implementation change as per Joda Time
		LocalDate now = new LocalDate(coverageStartDate.getTime());
		
		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());
				
		Years years = Years.yearsBetween(birthdate, now);
		int age =  years.getYears();

		return age;
		
	}

	
	private void populatePrimaryPhone(List<MemberDTO> memberDTOList, String phoneNumber) {
		for(MemberDTO memberDTO : memberDTOList){
			if(memberDTO != null){
				if(StringUtils.isBlank(memberDTO.getPrimaryPhone())){
					memberDTO.setPrimaryPhone(phoneNumber);
				}	
			}
		}
	}

	private void populateMailingAddress(SingleStreamlinedApplication applicationData, List<MemberDTO> memberDTOList) {
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		for (HouseholdMember householdMember : members) {
			Integer personId = householdMember.getPersonId();
			for(MemberDTO memberDTO : memberDTOList){
				if(memberDTO != null && StringUtils.isNotBlank(memberDTO.getMemberId()) && memberDTO.getMemberId().equalsIgnoreCase(String.valueOf(personId))){
					if(householdMember.getHouseholdContact() != null){
						if(householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator() != null){
							if(householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()){
								if( householdMember.getHouseholdContact().getHomeAddress() != null) { 
									memberDTO.setMailingAddress1(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
									memberDTO.setMailingAddress2(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
									memberDTO.setMailingCity(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getCity()));
									memberDTO.setMailingState(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getState()));
									memberDTO.setMailingZip(getFiveDigitZipCode(householdMember.getHouseholdContact().getHomeAddress().getPostalCode()));
								}
							}else if(!householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()){
								if(householdMember.getHouseholdContact().getMailingAddress() != null){
									memberDTO.setMailingAddress1(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getStreetAddress1()));
									memberDTO.setMailingAddress2(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getStreetAddress2()));
									memberDTO.setMailingCity(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getCity()));
									memberDTO.setMailingState(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getState()));
									memberDTO.setMailingZip(getFiveDigitZipCode(householdMember.getHouseholdContact().getMailingAddress().getPostalCode()));
								}
							}							
						}	
					}
				}
			}
		}	
	}

	private void populateOtherMembersData(Map<Long, SsapApplicantResource> ssapApplicants,
			Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap, List<MemberDTO> memberDTOList,
			boolean issep, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String exemptHousehold, Ind19ClientRequest ind19ClientRequest, String productType, String enrollmentType) {
		
		Map<Long, Long> ssapApplicantsPersonIdMap = convertToPersonIdPKeyMap(ssapApplicants);
		for(MemberDTO memberDTO : memberDTOList){
			String personId = memberDTO.getMemberId();
			Long personIdInLong = Long.valueOf(personId);
			memberDTO.setNewPersonFLAG("N");
			if("S".equalsIgnoreCase(enrollmentType)){
				memberDTO.setMaintenanceReasonCode("AI");
				if(!ssapApplicantEventPrimaryKeyMap.isEmpty() && !ssapApplicantsPersonIdMap.isEmpty() && ssapApplicantsPersonIdMap.get(personIdInLong) != null 
						&& null!=ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)) 
						&& ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents()!=null){
					
					if(StringUtils.isNotBlank(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getChangeType()) 
							&& ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getChangeType().equalsIgnoreCase("ADD")){
						memberDTO.setNewPersonFLAG("Y");
					}
					if(StringUtils.isNotBlank(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getMrcCode())){
						memberDTO.setMaintenanceReasonCode(ssapApplicantEventPrimaryKeyMap.get(ssapApplicantsPersonIdMap.get(personIdInLong)).getSepEvents().getMrcCode().trim());
					}
				}
				
				Integer existingMedicalEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "health");
				Integer existingSADPEnrollmentId = getEnrollmentId(enrollmentsByTypeMap, "dental");
				Set<String> healthPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "health");
				Set<String> dentalPlanEnrollees  = getEnrollees(enrollmentsByTypeMap, "dental");
				String healthEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "health");
				String dentalEnrollmentStatus = getEnrollmentStatus(enrollmentsByTypeMap, "dental");
				
				if(!"Y".equalsIgnoreCase(memberDTO.getNewPersonFLAG())){
					if(existingMedicalEnrollmentId != null && healthPlanEnrollees != null && healthPlanEnrollees.contains(memberDTO.getApplicantGuid()) &&
					        !StringUtils.equalsIgnoreCase(healthEnrollmentStatus, "cancelled")){
						if(ind19ClientRequest.getGroup() != null) { 
							if(existingMedicalEnrollmentId != null && (ProductType.A.value().equalsIgnoreCase(productType) || ProductType.H.value().equalsIgnoreCase(productType))) {
								memberDTO.setExistingMedicalEnrollmentID(String.valueOf(existingMedicalEnrollmentId));
							}
						}else {
							memberDTO.setExistingMedicalEnrollmentID(String.valueOf(existingMedicalEnrollmentId));
						}
					}
					if(existingSADPEnrollmentId != null && dentalPlanEnrollees != null && dentalPlanEnrollees.contains(memberDTO.getApplicantGuid()) &&
					        !StringUtils.equalsIgnoreCase(dentalEnrollmentStatus, "cancelled")){
						if(ind19ClientRequest.getGroup() != null) { 
							if(existingSADPEnrollmentId != null && (ProductType.A.value().equalsIgnoreCase(productType) || ProductType.D.value().equalsIgnoreCase(productType))) {
								memberDTO.setExistingSADPEnrollmentID(String.valueOf(existingSADPEnrollmentId));
							}
						}else {
							memberDTO.setExistingSADPEnrollmentID(String.valueOf(existingSADPEnrollmentId));
						}
					}
				}
			}
			memberDTO.setFinancialHardshipExemption("N");
			if(StringUtils.isNotBlank(exemptHousehold) && exemptHousehold.equalsIgnoreCase("Y")){
				memberDTO.setFinancialHardshipExemption("Y");
			}
			
			memberDTO.setCatastrophicEligible("N");
			if(StringUtils.isNotBlank(exemptHousehold) && exemptHousehold.equalsIgnoreCase("Y")){
				memberDTO.setCatastrophicEligible("Y");
			}			
			memberDTO.setChildOnlyPlanEligibile("N");
		}				
	}
	
	private String getEnrollmentStatus(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		return enrollmentShopDTO != null ? enrollmentShopDTO.getEnrollmentStatusLabel() : null;
	}

	private Set<String> getEnrollees(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		Set<String> ernollees  = new HashSet<String>();
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		if(enrollmentShopDTO != null){
			for(EnrolleeShopDTO enrollee: enrollmentShopDTO.getEnrolleeShopDTOList()){
				ernollees.add(enrollee.getExchgIndivIdentifier());
			}					
		}
		return ernollees;
	}

	
	private Integer getEnrollmentId(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String enrollmentType) {
		EnrollmentShopDTO enrollmentShopDTO = enrollmentsByTypeMap.get(enrollmentType);
		return enrollmentShopDTO != null ? enrollmentShopDTO.getEnrollmentId() : null;
	}

	private Set<String> createSetOfApplicantIds(SingleStreamlinedApplication applicationData) {
		Set<String> applicantSet = new HashSet<String>();
		if (applicationData != null) {
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			for (HouseholdMember householdMember : members) {
				if(householdMember.getApplyingForCoverageIndicator()){
					applicantSet.add(householdMember.getApplicantGuid());
				}
			}
		}
		return applicantSet;
	}
	
	private SsapApplicationEligibilityDTO getApplicationData(SingleStreamlinedApplication singleStreamlinedApplication) {
		SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO = null;
		if (singleStreamlinedApplication != null) {
			String applicationGuid = singleStreamlinedApplication.getApplicationGuid();
			ssapApplicationEligibilityDTO = eligibilityService.getApplicationData(applicationGuid);
		}
		/*if(singleStreamlinedApplication!= null){
			String applicationGuid = (String) singleStreamlinedApplication.get("applicationGuid");
			ssapApplicationEligibilityDTO = eligibilityService.getApplicationData(applicationGuid);
		}*/		
		return ssapApplicationEligibilityDTO;
	}
	
	private void assignEligibilityDetailsAndEligibleMembers(SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO,  Set<String> eligibleMembersIds, 
			Set<String> setOfApplicants, List<MemberDTO> filteredMemberList){		
		if(ssapApplicationEligibilityDTO != null){			
			if(ssapApplicationEligibilityDTO.getSsapApplicantList() != null && !ssapApplicationEligibilityDTO.getSsapApplicantList().isEmpty()){
				for(SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO :ssapApplicationEligibilityDTO.getSsapApplicantList()){
					if(ssapApplicantEligibilityDTO.getEligibilities() != null && !ssapApplicantEligibilityDTO.getEligibilities().isEmpty()){
						if(!filteredMemberList.isEmpty()){
							for(MemberDTO memberDTO : filteredMemberList){
								if(memberDTO!= null && memberDTO.getMemberId() != null && memberDTO.getApplicantGuid() != null){
									Long personId = Long.valueOf(memberDTO.getMemberId());
									String guid = memberDTO.getApplicantGuid();							
									for(EligibilityProgramDTO eligibilityProgramDTO: ssapApplicantEligibilityDTO.getEligibilities()){
										if(personId.equals(ssapApplicantEligibilityDTO.getPersonId())){
											if((eligibilityProgramDTO!=null && Ind19ClientConstants.ELIGIBLE_STATUS_SET.contains(eligibilityProgramDTO.getEligibilityStatus())) && 
											setOfApplicants!=null && setOfApplicants.contains(guid)){
												eligibleMembersIds.add(guid);
												break;
											}
										}
									}									
								}
							}														
						}
					}					
				}
			}
		}		
	}
	
	
	
	private IndividualDTO populateIndividualDTO(SsapApplication ssapApplication, Date coverageStartDate, com.getinsured.iex.dto.ind19.Ind19ClientRequest ind19ClientRequest, SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {
		IndividualDTO individualDTO = new IndividualDTO();
		if(ssapApplication.getCmrHouseoldId() != null){
			individualDTO.setHouseholdCaseId(ssapApplication.getCmrHouseoldId().longValue());
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");			
		individualDTO.setCoverageStartDate(dateFormat.format(coverageStartDate));
		
		if(ind19ClientRequest.getFinancialEffectiveDate() != null){
			individualDTO.setFinancialEffectiveDate(dateFormat.format(ind19ClientRequest.getFinancialEffectiveDate()));
		}		
		
		String applicationType = ssapApplication.getApplicationType();
		if((ind19ClientRequest.getIsChangePlanFlow() || ind19ClientRequest.getIsSEPPlanFlow()) || (StringUtils.equalsIgnoreCase(applicationType,"SEP"))){
			individualDTO.setEnrollmentType("S");
		}
		else if(StringUtils.isEmpty(applicationType) || 
				StringUtils.equalsIgnoreCase(applicationType,"OE") ||
				StringUtils.equalsIgnoreCase(applicationType,"QEP")){
			individualDTO.setEnrollmentType("I");
		}			
		
		
		if(ssapApplicationEligibilityDTO != null){
			if(ssapApplicationEligibilityDTO.getMaxAPTCAmount()!=null){
				individualDTO.setAptc((ssapApplicationEligibilityDTO.getMaxAPTCAmount().floatValue()));
			}
			if(ssapApplicationEligibilityDTO.getMaxStateSubsidy()!=null){
				individualDTO.setStateSubsidy(ssapApplicationEligibilityDTO.getMaxStateSubsidy());
			}
			if(ssapApplicationEligibilityDTO.getCsrLevel()!=null && ssapApplicationEligibilityDTO.getCsrLevel().trim().length()>0){
				individualDTO.setCsr(ssapApplicationEligibilityDTO.getCsrLevel());
			}
		}
		
		AptcHistory aptcHistory = aptcHistoryRepository.findLatestSlcspAmount(ssapApplication.getId()); 
		if(aptcHistory != null && aptcHistory.getSlcspAmount() != null) {
			individualDTO.setEhbAmount(new Float(aptcHistory.getSlcspAmount().toString()));
			}
		
			
		return individualDTO;
	}
	
	
	private Object populateMember(HouseholdMemberDTO householdMemberDTO) {		
		Member member = objectFactory.createIndividualInformationRequestHouseholdMembersMember();
		MemberDTO memberDTO = (MemberDTO) householdMemberDTO;
		member.setMemberId(memberDTO.getMemberId());
		member.setPreferredEmail(memberDTO.getPreferredEmail());
		member.setPreferredPhone(memberDTO.getPreferredPhone());
		member.setSpokenLanguageCode(memberDTO.getSpokenLanguageCode());
		member.setWrittenLanguageCode(memberDTO.getWrittenLanguageCode());
		member.setMailingAddress1(memberDTO.getMailingAddress1());
		member.setMailingAddress2(memberDTO.getMailingAddress2());		
		member.setMailingCity(memberDTO.getMailingCity());
		member.setMailingState(memberDTO.getMailingState());
		member.setMailingZip(memberDTO.getMailingZip());
		member.setNewPersonFLAG(memberDTO.getNewPersonFLAG()!=null?YesNoVal.fromValue(memberDTO.getNewPersonFLAG()):null);
		member.setMaintenanceReasonCode(memberDTO.getMaintenanceReasonCode());
		member.setExistingMedicalEnrollmentID(memberDTO.getExistingMedicalEnrollmentID());
		member.setExistingSADPEnrollmentID(memberDTO.getExistingSADPEnrollmentID());
		member.setFirstName(memberDTO.getFirstName());
		member.setMiddleName(memberDTO.getMiddleName());		
		member.setLastName(memberDTO.getLastName());
		member.setSuffix(memberDTO.getSuffix());		
		member.setDob(memberDTO.getDob());		
		member.setTobacco(memberDTO.getTobacco()!=null?YesNoVal.fromValue(memberDTO.getTobacco()):null);		
		member.setSsn(memberDTO.getSsn());		
		member.setPrimaryPhone(memberDTO.getPrimaryPhone());		
		member.setSecondaryPhone(memberDTO.getSecondaryPhone());		
		member.setHomeAddress1(memberDTO.getHomeAddress1());				
		member.setHomeAddress2(memberDTO.getHomeAddress2());			
		member.setHomeCity(memberDTO.getHomeCity());
		member.setHomeState(memberDTO.getHomeState());
		member.setHomeZip(memberDTO.getHomeZip());
		member.setCountyCode(memberDTO.getCountyCode());           
		member.setGenderCode(memberDTO.getGenderCode());			
		member.setMaritalStatusCode(memberDTO.getMaritalStatusCode());
		member.setExternalMemberId(memberDTO.getExternalMemberId());
		member.setCitizenshipStatusCode(memberDTO.getCitizenshipStatusCode());
		if(memberDTO.getRace() != null && !memberDTO.getRace().isEmpty()){
			for(MemberDTO.Race raceOrEthnicity : memberDTO.getRace()){
				Race race = new Race();
				race.setRaceEthnicityCode(raceOrEthnicity.getRaceEthnicityCode());
				race.setDescription(raceOrEthnicity.getRaceEthnicityDescription());
				member.getRace().add(race);
			}
		}		
		member.setFinancialHardshipExemption(memberDTO.getFinancialHardshipExemption()!=null?YesNoVal.fromValue(memberDTO.getFinancialHardshipExemption()):null);
		member.setCatastrophicEligible(memberDTO.getCatastrophicEligible()!=null?YesNoVal.fromValue(memberDTO.getCatastrophicEligible()):null);
		member.setChildOnlyPlanEligibile(memberDTO.getChildOnlyPlanEligibile()!=null?YesNoVal.fromValue(memberDTO.getChildOnlyPlanEligibile()):null);
		
		return member;
	}

	private void populateRelationship(Object membersObj, List<Relationships> relationshipsList, List<MemberDTO> filteredMemberList,HouseholdMember responsibleMember) {
		Member member = (Member) membersObj;	
		if(relationshipsList != null && !relationshipsList.isEmpty()){
			for(Relationships relationships : relationshipsList){
				if(member.getMemberId().equalsIgnoreCase(relationships.getIndividualId())){
					Relationship relationship = objectFactory.createIndividualInformationRequestHouseholdMembersMemberRelationship();
					if(filteredMemberList!=null && !filteredMemberList.isEmpty()){
						for(MemberDTO memberDTO : filteredMemberList){
							if(memberDTO.getMemberId() != null && relationships.getMemberId().equalsIgnoreCase(memberDTO.getMemberId())){
								relationship.setMemberId(memberDTO.getApplicantGuid());
							}
						}											
					}
					relationship.setRelationshipCode(relationships.getRelationshipCode());
					member.getRelationship().add(relationship);
					if(relationships.getMemberId().equalsIgnoreCase(String.valueOf(responsibleMember.getPersonId()))){
						member.setResponsiblePersonRelationship(relationships.getRelationshipCode());
					}
				}
			}
		}				
	}
	
	public String getProductType(Group group, long applicationId, String applicationType){
		String customGrouping = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CUSTOM_GROUPING);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		boolean isNotCAState = "CA".equalsIgnoreCase(stateCode) ? false : true;	
		
		// Logic for default produtType: if custom grouping is ON/OFF and state code is CA/isNotCA.
		ProductType productType;
    	if("ON".equalsIgnoreCase(customGrouping)){
    		if(isNotCAState){
    			productType = ProductType.A;
    		}else{
    			productType = ProductType.H;
    		}    		
    	}else{
    		productType = ProductType.A;
    	}
		
		if(group != null && group.getMemberList() != null){
			if(GroupType.HEALTH.equals(group.getGroupType())){	
				List<SsapApplicantDto> ssapApplicants = null;
				try {
					ssapApplicants = indportalUtil.getEligibleApplicantDetails(applicationId);
				} catch (GIException e) {
				}
				if(isNotCAState && ssapApplicants != null && ssapApplicants.size() == group.getMemberList().size()){
					productType = ProductType.A;
					if(StringUtils.equalsIgnoreCase(applicationType,"OE") || StringUtils.equalsIgnoreCase(applicationType,"QEP")){
						for(com.getinsured.hix.indportal.dto.Member member : group.getMemberList()){
							if(StringUtils.isNotBlank(member.getDentalEnrollmentId())){
								productType = ProductType.H;
								break;
							}
						}
					}
				}else{
					productType = ProductType.H;
				}
			}else if(GroupType.DENTAL.equals(group.getGroupType())){
				productType = ProductType.D;
			}
		}		
		return productType.value();
	}
	
}
