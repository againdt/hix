package com.getinsured.hix.planmgmt.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.dto.planmgmt.template.csr.hix.pm.InsurancePlanType;
import com.getinsured.hix.dto.planmgmt.template.csr.hix.pm.InsurancePlanVariantType;
import com.getinsured.hix.dto.planmgmt.template.csr.hix.pm.IssuerType;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanHealth;


public class CSRAdvancePaymentDataMappingTest extends BaseTest {
	
	@Autowired private IssuerService issuerService;
	@Autowired private PlanMgmtService planMgmtService;

	@Test
	@SuppressWarnings("unused")
	public void testPlanHealthUpdate(){
		
		//IssuerType issuerType=null;
		//InsurancePlanType insurancePlanType=null;
		//InsurancePlanVariantType insurancePlanVariantType=null;
		
		String hiosIssuerID=null;
		String planNameForIssuer=null;
		String costSharingTypeID=null;
		String csrAdvPayAmt="20";
		Float payAmout=Float.parseFloat(csrAdvPayAmt);

		/*IssuerIdentification*/
		List<String> issuerList=new ArrayList<String>();
		/*InsurancePlanStandardComponentIdentification*/
		List<String> issuerPlanNameList=new ArrayList<String>();
		/*InsurancePlanVariantIdentification*/
		List<String> costSharingList=new ArrayList<String>();

		issuerList.add("2131");
		issuerPlanNameList.add("54152385");
		costSharingList.add("01");
		costSharingList.add("02");
		costSharingList.add("03");
		costSharingList.add("04");
		
		Iterator<String> issuerListItr=issuerList.iterator();
		while (issuerListItr.hasNext()) {
			hiosIssuerID = (String) issuerListItr.next();
			if(hiosIssuerID!=null){
				Issuer issuer=issuerService.getIssuerByHiosID(hiosIssuerID);
				System.out.println("issuerID from HOISID: " + issuer.getId());
				
				if(issuer!=null){
					Iterator<String> issuerPlanNameListItr=issuerPlanNameList.iterator();
					while (issuerPlanNameListItr.hasNext()) {
						planNameForIssuer = (String) issuerPlanNameListItr.next();
						System.out.println("planNameForIssuer: " + planNameForIssuer);
						
						if(planNameForIssuer!=null){
							Plan plan=planMgmtService.getRequiredPlan(issuer.getId(), planNameForIssuer);
							
							if(plan!=null){
								System.out.println("PlanID and IssuerPlanNumber: " +plan.getId()+":::::::::"+plan.getIssuerPlanNumber());
								Iterator<String> costSharingListItr=costSharingList.iterator();
								while (costSharingListItr.hasNext()) {
									costSharingTypeID = (String) costSharingListItr.next();
									String csType=getCSType(costSharingTypeID);
									if(csType!=null){
										PlanHealth planHealth=planMgmtService.getRequiredPlanHealthObject(issuer.getId(),plan.getId(),csType);
										if(planHealth!=null){
											System.out.println("Ready To Update As:");
											System.out.println("HIOSIssuerID: "+hiosIssuerID +",planNameForIssuer: "+planNameForIssuer+",costSharingTypeID: "+ planHealth.getCostSharing().toString() +",csrAdvPayAmt: "+(payAmout+=10.0f));
											String updateResult=planMgmtService.updateCsrAmount(payAmout,issuer.getId(),plan.getId(), csType);;
											System.out.println("updateResult: " + updateResult);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	private String getCSType(String costSharingTypeID){
		String csType=null;
		if(costSharingTypeID.equals("01")){
			csType="CS1";
		}else if(costSharingTypeID.equals("02")){
			csType="CS2";
		}else if(costSharingTypeID.equals("03")){
			csType="CS3";
		}else if(costSharingTypeID.equals("04")){
			csType="CS4";
		}else if(costSharingTypeID.equals("05")){
			csType="CS5";
		}else if(costSharingTypeID.equals("06")){
			csType="CS6";
		}
		return csType;
	}

}
