package com.getinsured.eligibility.active.enrollment.service;

import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentByMemberListRequestDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.platform.util.exception.GIException;

public interface ActiveEnrollmentService {

	boolean isEnrollmentActive(Long applicationId, String username);
	
	boolean isConfirmedEnrollment(Long applicationId, String username) throws GIException;
	
	boolean isConfirmedEnrollment(Long applicationId, List<String> validEnrollmentStatus, String username) throws GIException;
	
	EnrollmentDataDTO getEnrollmentForApplication(Long applicationId, String username) throws GIException;
	
	List<EnrollmentDataDTO> getEnrollmentDataBySsapApplicationId(Long ssapApplicationId);

	EnrollmentShopDTO getLatestEnrollmentApplication(Long applicationId, String username) throws GIException;
	
	boolean isEnrollmentActiveCheck(Long applicationId, String username);
	
	List<EnrollmentWithMemberDataDTO> getEnrollmentByMembers(EnrollmentByMemberListRequestDTO requestDto,Long ssapApplicationId) throws GIException;
}
