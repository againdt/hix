package com.getinsured.eligibility.at.ref.service;

/**
 * @author chopra_s
 * 
 */
public interface ReferralBatchDisenrollService {
	public String csrLevelChange(String[] arguments);
	
	public String geoLocaleChange(String[] arguments);
}
