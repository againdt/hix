package com.getinsured.hix.entity.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.entity.repository.IDesignateAssisterRepository;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Implements {@link DesignateAssisterService} to designate / de-designate
 * employers / individuals
 */
@Service("designateAssisterService")
@Transactional
public class DesignateAssisterServiceImpl implements DesignateAssisterService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignateAssisterServiceImpl.class);

	@Autowired
	private IDesignateAssisterRepository designateAssisterRepository;
	@Autowired
	private AssisterService assisterService;
	@Autowired
	private BrokerUtils brokerUtils;

	/**
	 * @see DesignateAssisterService#deDesignateIndividualForAssister(int, int)
	 */
	@Override
	@Transactional
	public DesignateAssister deDesignateAssisterForIndividual(int assisterId, int extIndividualId) throws GIException {
		
		LOGGER.info("deDesignateIndividualForAssister: START");
		
		LOGGER.debug("De-designating external individual : " + extIndividualId);
		DesignateAssister dedesignatedAssister = null;
		DesignateAssister designateAssister = null;

		try{
			
		// Finds record before de-designate
		dedesignatedAssister = designateAssisterRepository.findDesigAssisterByIndividualId(extIndividualId);

		// If record is found, in-activates the individual. Else returns null
		// object.
		if (dedesignatedAssister != null) {
			dedesignatedAssister.setAssisterId(assisterId);
			dedesignatedAssister.setStatus(DesignateAssister.Status.InActive);
			designateAssister = designateAssisterRepository.save(dedesignatedAssister);
			
			brokerUtils.updateEnrollmentDetails(new EnrollmentBrokerUpdateDTO(), extIndividualId, assisterId, BrokerConstants.ASSISTER_ROLE, BrokerConstants.REMOVEACTIONFLAG);
		}
		
		} catch(Exception exception){
			LOGGER.error("Exception occurred while de-designating Assister : ", exception);
			throw new GIException("Exception occurred while de-designating Assister", exception);
		}
		
		LOGGER.info("deDesignateIndividualForAssister: END");
		
		return designateAssister;
	}
	
	/**
	 * @throws Exception 
	 * @see DesignateAssisterService#designateIndividualForAssister(int, int)
	 */
	@Override
	@Transactional
	public DesignateAssister designateAssisterForIndividual(int assisterId, int extIndividualId) throws GIException {
		
		LOGGER.info("designateIndividualForAssister: START");
		
		LOGGER.debug("Designating external individual : " + extIndividualId);
		DesignateAssister designatedAssister = null;
		DesignateAssister designateAssister = null;

		try {
			Assister assister = assisterService.findById(assisterId);

			if (assister != null && assister.getUser() != null) {
				designateAssister = designateAssisterRepository.findDesigAssisterByIndividualAndAssisterId(assisterId, extIndividualId);
				
				if(designateAssister==null){
					designateAssister = new DesignateAssister();
				}
				designateAssister.setIndividualId(extIndividualId);
				designateAssister.setAssisterId(assister.getId());
				designateAssister.setEntityId(assister.getEntity().getId());
				designateAssister.setStatus(DesignateAssister.Status.Active);
				designateAssister.setEsignBy(assister.getUser().getFirstName() + ' ' + assister.getUser().getLastName());
				designateAssister.setEsignDate(new TSDate());
				designateAssister.setShow_switch_role_popup('Y');
				designatedAssister =  designateAssisterRepository.save(designateAssister);
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while designating Assister : ", exception);
			throw new GIException("Exception occurred while designating Assister", exception);
		}
		
		LOGGER.info("designateIndividualForAssister: END");
		
		return designatedAssister;
	}

	/**
	 * @see DesignateAssisterService#findByIndividualAndAssisterId(int, int)
	 */
	@Override
	public DesignateAssister findByIndividualAndAssisterId(int assisterId, int extIndividualId) {
		
		LOGGER.info("findByIndividualAndAssisterId: START");
		LOGGER.info("findByIndividualAndAssisterId: END");
		
		return designateAssisterRepository.findDesigAssisterByIndividualAndAssisterId(assisterId, extIndividualId);
	}
	
	/**
	 * @see DesignateAssisterService#findByIndividualAndAssisterIdAndStatus(int, int, Status)
	 */
	@Override
	public DesignateAssister findByIndividualAndAssisterIdAndStatus(int assisterId, int extIndividualId, Status status) {
		
		LOGGER.info("findByIndividualAndAssisterIdAndStatus: START");
		LOGGER.info("findByIndividualAndAssisterIdAndStatus: END");
		
		return designateAssisterRepository.findDesigAssisterByIndividualAndAssisterIdAndStatus(assisterId, extIndividualId, status);
	}
}
