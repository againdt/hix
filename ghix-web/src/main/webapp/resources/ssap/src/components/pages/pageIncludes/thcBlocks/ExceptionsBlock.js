import React from 'react';

// Components

import UniversalInput from '../../../../lib/UniversalInput';
import ValidationErrorMessage from '../../../notifications/ValidationErrorMessage';

// Constants

import INPUT_TYPES from '../../../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../constants/validationTypes';


const ExceptionsBlock = props => {
    const {
        ButtonContainer,
        checkTopLevelValidation,
        isFormSubmitted,
        moreInformation,
        noExceptions,
        onFieldChange,
        uniqueMatched,
        ...restProps
    } = props;

    return !noExceptions ?

        <div className="subsection">
            <ValidationErrorMessage
                isVisible={isFormSubmitted && !checkTopLevelValidation()}
                messageText='Please update information'
                errorClasses={['margin-l-0']}
            />
            <UniversalInput
                legend={{
                    legendText:
                        <p>
                            Do you want to provide more information about the family members who live with &nbsp;
                            <strong>
                                { uniqueMatched.join(', ') }
                            </strong>
                            ?
                        </p>
                }}
                label={{
                    text: '',
                    labelClass: ['usa-width-one-whole', 'layout--02'],
                    ulClass: ['usa-width-one-whole margin-b-20'],
                    showRequiredIcon: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.RADIO_BUTTONS,
                    classes:['disp-styl-none'],
                    name: 'moreInformation',
                    value: moreInformation,
                    options: DROPDOWN_OPTIONS.YES_NO,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: {},
                    fields: []
                }}
                inputActions={{
                    onFieldChange
                }}
                indexInArray={0}
                {...restProps}
            />
            {
                moreInformation &&

                <ButtonContainer
                    type="updateInformationButton"
                    args={[]}
                />
            }

            {
                moreInformation === false &&

                <div className="subsection">
                    <p className="usa-alert usa-alert-info alert--info">
                        Your decision to not provide more information about family members who live with &nbsp;
                        <strong>
                            { uniqueMatched.join(', ') }
                        </strong> &nbsp;
                        will impact whether an appropriate eligibility result is given. We will provide eligibility results based on the information you have provided on the application
                    </p>
                </div>
            }

        </div> : null;

};

export default ExceptionsBlock;
