package com.getinsured.iex.hub.consumers.hub;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants.VLP_DHS_ID_TYPES;
import com.rabbitmq.client.Channel;

public class VLPQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired private WebServiceTemplate vlpServiceTemplate;
	@Autowired private WebServiceTemplate reverifyServiceTemplate;
	@Autowired private WebServiceTemplate resubServiceTemplate;
	@Autowired private WebServiceTemplate closeCaseServiceTemplate;
	@Autowired private WebServiceTemplate getCaseDetailsServiceTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(VLPQueueConsumer.class);

	private static final String SERVICE_NAME = "VLP";
	private static final String INITIAL_STATUS = "PENDING";
	private static final String TRUE = "true";
	
	public void invokeVerifiedLawfulPresence() {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("VERIFY_LAWFUL_PRESENCE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);

		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VerifylawfulPresence");
			vlpServiceBridge.processInputParameter("alienNumber", "991122334");
			vlpServiceBridge.processInputParameter("expiryDate", "2012-01-29");
			vlpServiceBridge.processInputParameter("firstName", "Steve");
			vlpServiceBridge.processInputParameter("lastName", "Bolhman");
			vlpServiceBridge.processInputParameter("dateOfBirth", "1987-02-02");
			vlpServiceBridge.processInputParameter("comments", "Initial verification request");
			vlpServiceBridge.processInputParameter("requesterCommentsForHub", "Initial verification request");
			vlpServiceBridge.processInputParameter("requestedCoverageStartDate","2014-01-03");
			vlpServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			vlpServiceBridge.processInputParameter("requestSponsorDataIndicator", TRUE);
			vlpServiceBridge.processInputParameter("requestGrantDateIndicator", TRUE);
			vlpServiceBridge.processInputParameter("categoryCode", "C40");
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",VLP_DHS_ID_TYPES.I327);
			handler.setContext(context);
			
			this.executeRequest(request, handler, this.vlpServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
		}
	}

	public void invokeCloseCaseRequest() {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("CLOSE_CASE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);

		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("CloseCaseRequest");
			vlpServiceBridge.processInputParameter("caseNumber", "6700000000156AX");
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			this.executeRequest(request, handler, this.closeCaseServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
		}	
	}

	public void invokeGetCaseDetailsRequest() {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("GET_CASE_DETAILS");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);

		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("GetCaseDetailsRequest");
			vlpServiceBridge.processInputParameter("caseNbr", "6700000000156AX");
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			this.executeRequest(request, handler, this.getCaseDetailsServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
		}		
	}

	private void invokeReverifyAgencyRequest() throws HubServiceException {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("REVERIFY_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);

		try{
			HubServiceBridge reverifyServiceBridge = HubServiceBridge.getHubServiceBridge("ReverifyAgencyRequest");
			reverifyServiceBridge.processInputParameter("caseNumber","9901009990000CC");
			reverifyServiceBridge.processInputParameter("receiptNumber","AAA1200850001");
			reverifyServiceBridge.processInputParameter("alienNumber", "321654987");
			reverifyServiceBridge.processInputParameter("sevisid", "0000110429");
			reverifyServiceBridge.processInputParameter("passportNumber","59N199754");
			reverifyServiceBridge.processInputParameter("countryOfIssuance","MEXIC");
			reverifyServiceBridge.processInputParameter("citizenshipNumber","552559412");
			reverifyServiceBridge.processInputParameter("visaNumber","76254237");
			reverifyServiceBridge.processInputParameter("firstName", "Rosa");
			reverifyServiceBridge.processInputParameter("lastName", "Villa");
			reverifyServiceBridge.processInputParameter("dateOfBirth","1976-05-05");
			reverifyServiceBridge.processInputParameter("requestedCoverageStartDate", "2012-01-29");
			reverifyServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			reverifyServiceBridge.processInputParameter("requesterCommentsForHub", "Resubmitting the application");
			reverifyServiceBridge.processInputParameter("categoryCode", "C49");
			
			ServiceHandler handler = reverifyServiceBridge.getServiceHandler();
			this.executeRequest(request, handler, this.reverifyServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			throw new  HubServiceException(e);
		}	
	}

	private void invokeResubAgencyRequest() throws HubServiceException {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("RESUB_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);

		try{
			HubServiceBridge resubServiceBridge = HubServiceBridge.getHubServiceBridge("ResubAgencyRequest");
			resubServiceBridge.processInputParameter("caseNumber","1101099980000CC");
			resubServiceBridge.processInputParameter("sevisId", "9885989925");
			resubServiceBridge.processInputParameter("requestedCoverageStartDate","2014-01-03");
			resubServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			resubServiceBridge.processInputParameter("requesterCommentsForHub", "submit agency3 request");
			resubServiceBridge.processInputParameter("categoryCode", "C40");
			
			ServiceHandler handler = resubServiceBridge.getServiceHandler();
			this.executeRequest(request, handler, this.resubServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			throw new  HubServiceException(e);
		}	
	}

	

	@Override
	public void onMessage(Message arg0, Channel arg1) throws HubMappingException, BridgeException, HubServiceException, InputDataValidationException  {
		String threadId = Thread.currentThread().getName();
		
		String routingKey = arg0.getMessageProperties().getReceivedRoutingKey();
		
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming RIDP message, don't know how to process it");
		}
		
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ arg0.getMessageProperties().getReceivedRoutingKey()
				+ " Received Message:" + new String(arg0.getBody())
				+ " from channel:" + arg1.getChannelNumber());

		try {
			invokeVerifiedLawfulPresence();
			invokeCloseCaseRequest();
			invokeReverifyAgencyRequest();
			invokeResubAgencyRequest();
			invokeGetCaseDetailsRequest();
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " And fault code:"
					+ e.getFaultCode(),e);
		}
	}
}
