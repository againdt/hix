<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@page import="com.getinsured.hix.model.Broker"%>

<%
String ShowDesignate = (String)request.getAttribute("ShowDesignate");
String designatedBrokerName = (String)request.getAttribute("designatedBrokerName"); 
String isSameBroker = (String) request.getAttribute("isSameBroker");
%> 

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/accessibility.css" />" media="screen"/>

<script type="text/javascript">

function alreadyDesignated(designatedBroker) {
	$('<div id="alreadyDesignated" class="modal"><div class="modal-body"><iframe id="success" src="#" style="overflow-x:hidden;width:30%;border:0;margin:0;padding:0;height:20px;"></iframe> You are already designated to ' + designatedBroker + '. <br/> <br/> <button class="btn offset2" data-dismiss="modal" aria-hidden="true">Ok</button></div></div>').modal({backdrop:false});
}

</script>

	<form class="form-horizontal" id="frmdetail" name="frmdetail" action="<c:url value="../broker/designate" />"  method="POST">
		<input type="hidden" name="id" id="id" value="">
		<!-- start of secondary navbar - TO DO - move to topnav -->
		<!-- <ul class="nav nav-tabs">
			<li><a href="#">Home</a></li>
			<li><a href="#">Find Insurance</a></li>
			<li><a href="#">Learn More</a></li>   
			<li><a href="#">Get Assistance</a></li>   
		</ul> -->
		<!-- end of secondary navbar -->
		
		<div class="gutter10">
			<div class="row-fluid">
				<div class="alert alert-info span">
					<div class="span10 pull-left">
						<% if("N".equals(ShowDesignate) && "true".equals(isSameBroker)) { %>
							<strong>Broker De-designation</strong><br />
							De-designate currently assigned broker.
						<% } else { %>
							<strong>Broker Designation</strong><br />
							Designating a broker as your representative allows them to&#58; Access your account, see your information, and make changes on your behalf.
						<% } %>
					</div>
					<c:choose>
						<c:when test="${ShowDesignate == 'Y'}">
<%-- 								<a class="btn btn-primary pull-right" href="../broker/designate/${broker.id}">Designate</a>  --%>
								<a class="btn btn-primary pull-right" href="../broker/esignature/${broker.id}">Designate</a>
						</c:when>
						<c:otherwise>
								<% if("N".equals(ShowDesignate) && "true".equals(isSameBroker)) { %>
									<a class="btn btn-primary pull-right" href="../broker/dedesignate/${broker.id}?fromEmployer=true">De-Designate Broker</a> 
								<% } else if("N".equals(ShowDesignate))  { %>
<%-- 									<a class="btn btn-primary pull-right" onclick="alert('You are already designated to ${designatedBrokerName}');">Designate</a>  --%>
									<a class="btn btn-primary pull-right" onClick="alreadyDesignated('${designatedBrokerName}')">Designate</a>
								<% } %>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
			<div class="gutter10">
				<div class="row-fluid">
				
					<c:choose>
						<c:when test="${!empty broker}">
							<spring:url value="/broker/photo/${broker.id}"  var="photoUrl"/>
							<div class="span2">
								<img src="${photoUrl}" alt="Broker photo thumbnail" class="profilephoto thumbnail pull-right" />
							</div>

							<div class="span10">
								<h1 class="span7 margin0">
									<a name="skip" class="skip">${broker.user.firstName}&nbsp;${broker.user.lastName}</a>
								</h1>
								<div class="span5">
									<div class="pull-right">
										<a class="btn" href="../broker/searchlist?usesession=yes&pageNumber=${brkpage}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Search by name', 1]);">Back to Results</a>
										<a class="btn btn-primary" href="../broker/search">Search Again</a>
										
									</div>
								</div>
							</div>
							<div class="span10">
							<p>${broker.location.address1}, 
							<c:if test="${broker.location.address2!=null && broker.location.address2!=\"\"}">
								${broker.location.address2}, 
							</c:if>
								${broker.location.city}, ${broker.location.state}, ${broker.location.zip}<br />
								${broker.contactNumber}<br/>
								${broker.user.email}
							</p>
							</div>
							<div class="row-fluid">
								<div class="span"><hr /></div>
							</div>
							<div class="row-fluid">
								<table class="table table-border-none span7">
									<tbody>
										<tr>
											<td class="txt-right"><spring:message  code="label.brkProductExpertises"/></td>
											<td><strong>${broker.productExpertise}</strong></td>
										</tr>
										<tr>
											<td class="txt-right"><spring:message  code="label.brkLanguageSpoken"/></td>
											<td><strong>${broker.languagesSpoken}</strong></td>
										</tr>
										<tr>
											<td class="txt-right"><spring:message  code="label.brkStateLicenseNumber"/></td>
											<td><strong>${broker.licenseNumber}</strong></td>
										</tr>
											<tr>
											<td class="txt-right"><spring:message  code="label.brkClientServed"/></td>
											<td><strong>${broker.clientsServed}</strong></td>
										</tr>
									</tbody>
								</table>

								<div class="pull-right">
									<iframe width="225" height="225" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;ll=${broker.location.lat},${broker.location.lon}&amp;z=13&amp;iwloc=A&amp;output=embed"></iframe>
								</div>
							</div>

						</c:when>
						<c:otherwise>
							<b>No matching record found.</b>
						</c:otherwise>
					</c:choose>
				</div>
			</div><!-- .gutter10 -->

