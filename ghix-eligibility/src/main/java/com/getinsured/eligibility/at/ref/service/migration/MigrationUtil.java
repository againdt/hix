package com.getinsured.eligibility.at.ref.service.migration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.repository.IAccountTransferMigrationRepository;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.ssap.model.AccountTransferMigration;
import com.getinsured.iex.ssap.repository.EnrollmentReferralRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

@Component
public class MigrationUtil {
	
	private static final Logger LOGGER = Logger.getLogger(MigrationUtil.class);
	
	private static final String COMPONENT = "AT_DATA_MIGRATION";
	public static final String TRANSFER_ID_ALREADY_PROCESSED = "Transfer Id already processed";
	public static final String EMPTY_TRANSFER_ID_OR_ACTIVITY_DATE = "Transfer Id/Activity Date is empty";
	public static final String INVALID_COVERAGE_YEAR = "Invalid Coverage Year";
	public static final String ENABLE = "enable";
	public static final String ENROLLMENT_LIST = "ENROLLMENT_LIST";
	public static final String DATA_MIGRATION = "DATA_MIGRATION";
	public static final String ENROLLMENT_END_DATE = "ENROLLMENT_END_DATE";
	private static final String ENROLLMENT_ID_DOES_NOT_EXIST = "Enrollment Id does not exist";
	private static final String COVERAGE_YEAR_MISMATCH = "Invalid Coverage Year for given Enrollment Id";
	private static final String EXTERNAL_HOUSEHOLD_CASE_ID_MISMATCH = "External Household Case Id is not matching";
	private static final String ERRORS_FOUND_IN_THE_ACCOUNT_TRANSFER_REQUEST_PAYLOAD_TYPE_OBJECT = "errors found in the AccountTransferRequestPayloadType object...";
	private static final String FAILURE = "Failure";
	private static final String SUCCESS = "Success";
	private static final String AT_VERSION = "2.4";
	private static final String ONE_OR_MORE_RULES_FAILED_VALIDATION = "One or more rules failed validation";
	
	@Autowired
	private IAccountTransferMigrationRepository iAccountTransferMigrationRepository;
	@Autowired 
	private GIMonitorService giMonitorService;
	@Autowired
	private EnrollmentReferralRepository enrollmentReferralRepository;
	
	public String getTransferId(AccountTransferRequestDTO accountTransferRequestDTO){
		String transferId;
		try {
			transferId = accountTransferRequestDTO.getAccountTransferRequestPayloadType().getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue();
		} catch(Exception exception){
			LOGGER.error("Error occurred while retrieving Transfer Id from request : ", exception);
			throw exception;
		}
		return transferId;
	}
	
	public void createAccountTransferMigration(AccountTransferRequestPayloadType request, String enrollmentList) {
		try {
			String transferId = request.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue();
			XMLGregorianCalendar activityDate = request.getTransferHeader().getTransferActivity().getActivityDate().getDateTime().getValue();
			
			if(StringUtils.isNotEmpty(enrollmentList)){
				String[] array = enrollmentList.split(",");
				
				for(String enrollmentId:array){
					AccountTransferMigration accountTransferMigration = new AccountTransferMigration();
					accountTransferMigration.setTransferId(transferId);
					accountTransferMigration.setEnrollmentId(Long.valueOf(enrollmentId.trim()));
					accountTransferMigration.setActivityDate(new Timestamp(activityDate.toGregorianCalendar().getTimeInMillis()));
					accountTransferMigration.setCreationDate(new Timestamp(new TSDate().getTime()));
					iAccountTransferMigrationRepository.save(accountTransferMigration);
				}
			} else {
				AccountTransferMigration accountTransferMigration = new AccountTransferMigration();
				accountTransferMigration.setTransferId(transferId);
				accountTransferMigration.setActivityDate(new Timestamp(activityDate.toGregorianCalendar().getTimeInMillis()));
				accountTransferMigration.setCreationDate(new Timestamp(new TSDate().getTime()));
				iAccountTransferMigrationRepository.save(accountTransferMigration);
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while creating account transfer migration record : ", exception);
			throw exception;
		}
	}
	
	private boolean checkTransferIdAndActivityDate(AccountTransferRequestPayloadType request) throws Exception{
		try {
			String transferId = request.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue();
			XMLGregorianCalendar activityDate = request.getTransferHeader().getTransferActivity().getActivityDate().getDateTime().getValue();
			
			if(StringUtils.isEmpty(transferId) || activityDate==null){
				return false;
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return true;
	}
	
	private boolean checkExistingTransferId(AccountTransferRequestPayloadType request) throws Exception{
		try {
			String transferId = request.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue();
			
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByTransferId(transferId);
			if(accountTransferMigrations!=null && accountTransferMigrations.size()>0){
				return false;
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return true;
	}
	
	public AccountTransferResponsePayloadType validate(AccountTransferRequestPayloadType request, String enrollmentIds) throws Exception{
		AccountTransferResponsePayloadType response = null;
		try {
			if(!checkExistingTransferId(request)){
				response = formFailureResponse(Arrays.asList(MigrationUtil.TRANSFER_ID_ALREADY_PROCESSED));
			} else if(!checkTransferIdAndActivityDate(request)){
				response = formFailureResponse(Arrays.asList(MigrationUtil.EMPTY_TRANSFER_ID_OR_ACTIVITY_DATE));
			} else if(!checkCoverageYear(request)){
				response = formFailureResponse(Arrays.asList(MigrationUtil.INVALID_COVERAGE_YEAR));
			} 
			
			if(response==null){
				response = validateEnrollments(request, enrollmentIds);
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return response;
	}
	
	public AccountTransferResponsePayloadType formFailureResponse(List<String> errorList) {
		LOGGER.info(ERRORS_FOUND_IN_THE_ACCOUNT_TRANSFER_REQUEST_PAYLOAD_TYPE_OBJECT + errorList.size());
		if (LOGGER.isDebugEnabled()){
			for (String string : errorList) {
				LOGGER.info(string);
			}
		}
		return formResponse(EligibilityConstants.HE001111, errorList);	// FAILURE
	}
	
	private AccountTransferResponsePayloadType formResponse(String responseValue, List<String> errorList) {

		AccountTransferResponsePayloadType response = new AccountTransferResponsePayloadType();
		response.setAtVersionText(AT_VERSION);

		ResponseMetadataType value = new ResponseMetadataType();
		TextType tt = new TextType();
		tt.setValue(responseValue);
		value.setResponseCode(tt);
		TextType responseDescription = new TextType();
		responseDescription.setValue(SUCCESS);

		if (errorList != null && errorList.size() > 0) {
			TextType responseErrorDescrip = new TextType();
			responseErrorDescrip.setValue(ONE_OR_MORE_RULES_FAILED_VALIDATION);
			value.setResponseDescriptionText(responseErrorDescrip);


			List<TextType> tdsResponseDescriptionTextList = new ArrayList<>();

			for (String error : errorList) {
				TextType errorDescrip = new TextType();
				errorDescrip.setValue(error);
				tdsResponseDescriptionTextList.add(errorDescrip);
			}

			value.getTDSResponseDescriptionText().addAll(tdsResponseDescriptionTextList);

			responseDescription.setValue(FAILURE);
		}

		value.setResponseDescriptionText(responseDescription);
		response.setResponseMetadata(value);
		return response;
	}
	
	private AccountTransferResponsePayloadType validateEnrollments(AccountTransferRequestPayloadType request, String enrollmentIds){
		AccountTransferResponsePayloadType response = null;
		
		try {
			if(StringUtils.isNotEmpty(enrollmentIds)){
				String[] array = enrollmentIds.split(",");
				
				request.getInsuranceApplication().getApplicationIdentification();
				
				for(String enrollmentId:array){
					Enrollment enrollment = enrollmentReferralRepository.findById(Integer.valueOf(enrollmentId));
					if(enrollment!=null){
						if(isInvalidCoverageYear(request, enrollment.getBenefitEffectiveDate())){
							response = formFailureResponse(Arrays.asList(MigrationUtil.COVERAGE_YEAR_MISMATCH));
							break;
						}
						if(!checkExternalHouseholdCaseId(request, enrollment)){
							response = formFailureResponse(Arrays.asList(MigrationUtil.EXTERNAL_HOUSEHOLD_CASE_ID_MISMATCH));
							break;
						}
					} else {
						response = formFailureResponse(Arrays.asList(MigrationUtil.ENROLLMENT_ID_DOES_NOT_EXIST));
						break;
					}
				}
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return response;
	}
	
	private boolean checkExternalHouseholdCaseId(AccountTransferRequestPayloadType request, Enrollment enrollment){
		if(enrollment.getExternalHouseHoldCaseId().equalsIgnoreCase(getHouseholdCaseId(request))){
			return true;
		}
		return false;
	}
	
	private String getHouseholdCaseId(AccountTransferRequestPayloadType request){
		List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =request.getInsuranceApplication().getApplicationIdentification();
		for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
			if(  ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
				return tIdentificationType.getIdentificationID().getValue();
			}
		}
		
		return null;
	}
	
	private boolean isInvalidCoverageYear(AccountTransferRequestPayloadType request, Date effectiveDate){
		try {
			ApplicationExtensionType requestApplicationExtension = request.getInsuranceApplication().getApplicationExtension();
			int coverageYear = requestApplicationExtension.getCoverageYear().getValue().getYear();
			
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(effectiveDate);
			int year = calendar.get(Calendar.YEAR);
			
			if(coverageYear != year){
				return true;
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return false;
	}
	
	private boolean checkCoverageYear(AccountTransferRequestPayloadType request){
		try {
			ApplicationExtensionType requestApplicationExtension = request.getInsuranceApplication().getApplicationExtension();
			int coverageYear = requestApplicationExtension.getCoverageYear().getValue().getYear();
			if(coverageYear <= 2017){
				return false;
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while checking account transfer migration record : ", exception);
			throw exception;
		}
		
		return true;
	}
	
	public void updateATMigrationByTransferId(AccountTransferRequestDTO accountTransferRequestDTO, long applicationId, boolean oldAppId, String householdCaseId, Long cmrHouseholdId, String status){
		try {
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByTransferId(getTransferId(accountTransferRequestDTO));
			
			if(accountTransferMigrations!=null){
				for(AccountTransferMigration accountTransferMigration:accountTransferMigrations){
					if(oldAppId){
						accountTransferMigration.setOldSsapApplicationId(applicationId);
					} else {
						accountTransferMigration.setNewSsapApplicationId(applicationId);
					}
					
					if(StringUtils.isNotEmpty(householdCaseId)){
						accountTransferMigration.setHouseholdCaseId(householdCaseId);
					}
					
					if(cmrHouseholdId!=null && cmrHouseholdId!=0){
						accountTransferMigration.setCmrHouseholdId(cmrHouseholdId);
					}
					
					if(StringUtils.isNotEmpty(status)){
						accountTransferMigration.setStatus(status);
					}
					
					iAccountTransferMigrationRepository.save(accountTransferMigration);
				}
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while updating account transfer migration table : ", exception);
			throw exception;
		}
	}
	
	public void updateCmrHouseholdId(long applicationId,  Long cmrHouseholdId, String status){
		try {
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByNewSsapApplicationId(applicationId);
			
			if(accountTransferMigrations!=null){
				for(AccountTransferMigration accountTransferMigration:accountTransferMigrations){
					if(cmrHouseholdId!=null && cmrHouseholdId!=0){
						accountTransferMigration.setCmrHouseholdId(cmrHouseholdId);
					}
					
					if(StringUtils.isNotEmpty(status)){
						accountTransferMigration.setStatus(status);
					}
					
					iAccountTransferMigrationRepository.save(accountTransferMigration);
				}
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while updating account transfer migration table : ", exception);
			throw exception;
		}
	}
	
	public void updateStatus(AccountTransferMigration accountTransferMigration, String status){
		try {
			accountTransferMigration.setStatus(status);
			iAccountTransferMigrationRepository.save(accountTransferMigration);
		} catch(Exception exception){
			LOGGER.error("Error occurred while updating account transfer migration table : ", exception);
			throw exception;
		}
	}
	
	public void updateStatusById(long applicationId, String status){
		try {
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByNewSsapApplicationId(applicationId);
			
			if(accountTransferMigrations!=null){
				for(AccountTransferMigration accountTransferMigration:accountTransferMigrations){
					if(StringUtils.isNotEmpty(status)){
						accountTransferMigration.setStatus(status);
					}
					
					iAccountTransferMigrationRepository.save(accountTransferMigration);
				}
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while updating account transfer migration table : ", exception);
			throw exception;
		}
	}
	
	public void persistGiMonitorId(long applicationId, Throwable throwable){
		try {
			if(applicationId>0){
				List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByNewSsapApplicationId(applicationId);
				
				if(accountTransferMigrations!=null){
					for(AccountTransferMigration accountTransferMigration:accountTransferMigrations){
						accountTransferMigration.setGiMonitorId(persistToGiMonitor(throwable));
						iAccountTransferMigrationRepository.save(accountTransferMigration);
					}
				}
			}
		} catch(Exception exception){
			LOGGER.error("Error occurred while updating account transfer migration table : ", exception);
			throw exception;
		}
	}
	
	private Integer persistToGiMonitor(Throwable throwable){
		Integer giMonitorId=null;
		try {
			GIMonitor giMonitor = new GIMonitor();
			giMonitor.setComponent(COMPONENT);
			giMonitor.setEventTime(new TSDate());
			giMonitor.setException(throwable.getClass().getName());
			giMonitor.setExceptionStackTrace(ExceptionUtils.getStackTrace(throwable));
			giMonitorId = giMonitorService.saveOrUpdateErrorLog(GIRuntimeException.ERROR_CODE_UNKNOWN, new TSDate(), throwable.getClass().getName(), 
					ExceptionUtils.getStackTrace(throwable), null, null, null, null).getId();
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
		}
		return giMonitorId;
	}
	
	public Timestamp getActivityDate(long applicationId){
		try {
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByNewSsapApplicationId(applicationId);
			if(!accountTransferMigrations.isEmpty()){
				return accountTransferMigrations.get(0).getActivityDate();
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
		}
		return null;
	}
}
