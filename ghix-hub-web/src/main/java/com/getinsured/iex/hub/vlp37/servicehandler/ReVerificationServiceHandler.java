package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ResponseMetadataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationResponseType;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.service.VLP_PostProcessor;
import com.getinsured.iex.hub.vlp37.wrapper.ReVerificationRequestTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.ReVerificationResponseSetTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.ReVerificationResponseTypeWrapper;



public class ReVerificationServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReVerificationServiceHandler.class);
	
	private Jaxb2Marshaller marshaller;
	private String caseNumber = null;
	
	
	public Object getRequestpayLoad() throws HubServiceException{
		
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				
				
				processJSONObject(payloadObj);
				
				ReVerificationRequestTypeWrapper obj = (ReVerificationRequestTypeWrapper)getNamespaceObjects().get(ReVerificationRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("required VLP Request parameters not available");
				}
				
				ReverificationRequestType reqObj = obj.getRequest();
				this.caseNumber = reqObj.getCaseNumber();
				if(this.caseNumber == null) {
					throw new InputDataValidationException("Case Number is mandatory");
				}
				setJsonInput(obj.toJSONString());
				return reqObj;
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
			
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		LOGGER.info("Handling response for Reverification request");
		String message = "";
		try{
			
			JAXBElement<ReverificationResponseType> resp= (JAXBElement<ReverificationResponseType>)responseObject;
			ReverificationResponseType payload = resp.getValue();
			ReVerificationResponseTypeWrapper response = new ReVerificationResponseTypeWrapper(payload);
			ReVerificationResponseSetTypeWrapper individualResponse = null; 
			VLP_PostProcessor responseHandler = (VLP_PostProcessor) this.getContext().get(ServiceHandler.RESPONSE_HANDLER);
			this.getContext().put("CaseNumber", this.caseNumber);
			
			responseHandler.submit(payload, this.getContext());
			if(response != null && response.getReVerificationResponseSet() != null){	
				message = response.toJSONString();
				/*
				 * To set Response code for VLP, we will use the first response in the set of individual responses
				 * Since VLP service will always be called separately for each applicant, we will receive at most
				 * once response 
				 */
				individualResponse = response.getReVerificationResponseSet();
				if(response != null && response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
			}
			ResponseMetadataType metadata = payload.getResponseMetadata();
			if(metadata.getResponseCode().equals("HS000000")) {
				this.requestStatus = RequestStatus.SUCCESS;
			}else {
				this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			}
			getResponseData(individualResponse);
			
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+ ReVerificationResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	/**
	 * Method to get Case Number from HUB response
	 * 
	 * author - Nikhil Talreja
	 * since - 26-May-2014
	 */
	private void getResponseData(ReVerificationResponseSetTypeWrapper individualResponse){
		
		//Case number
		if(individualResponse != null ){
			this.getResponseContext().put("HUB_CASE_NUMBER", individualResponse.getCaseNumber());
			
			this.getResponseContext().put("NonCitLastName", individualResponse.getNonCitLastName());
			this.getResponseContext().put("NonCitFirstName", individualResponse.getNonCitFirstName());
			this.getResponseContext().put("NonCitMiddleName", individualResponse.getNonCitMiddleName());
			this.getResponseContext().put("NonCitBirthDate", individualResponse.getNonCitBirthDate());
			this.getResponseContext().put("NonCitEntryDate", individualResponse.getNonCitEntryDate());
			this.getResponseContext().put("NonCitAdmittedToDate", individualResponse.getNonCitAdmittedToDate());
			this.getResponseContext().put("NonCitAdmittedToText", individualResponse.getNonCitAdmittedToText());
			this.getResponseContext().put("NonCitCountryBirthCd", individualResponse.getNonCitCountryBirthCd());
			this.getResponseContext().put("NonCitCountryCitCd", individualResponse.getNonCitCountryCitCd());
			this.getResponseContext().put("NonCitCoaCode", individualResponse.getNonCitCoaCode());
			this.getResponseContext().put("NonCitEadsExpireDate", individualResponse.getNonCitEadsExpireDate());
			this.getResponseContext().put("EligStatementCd", individualResponse.getEligStatementCd());
			this.getResponseContext().put("EligStatementTxt", individualResponse.getEligStatementTxt());
			this.getResponseContext().put("WebServSftwrVer", individualResponse.getWebServSftwrVer());
			this.getResponseContext().put("GrantDate", individualResponse.getGrantDate());
			this.getResponseContext().put("GrantDateReasonCd", individualResponse.getGrantDateReasonCd());
			this.getResponseContext().put("ArrayOfSponsorshipData", individualResponse.getArrayOfSponsorshipData());
			this.getResponseContext().put("SponsorshipReasonCd", individualResponse.getSponsorshipReasonCd());
			this.getResponseContext().put("PhotoBinaryAttachment", individualResponse.getPhotoBinaryAttachment());
			this.getResponseContext().put("FiveYearBarApplyCode", individualResponse.getFiveYearBarApplyCode());
			this.getResponseContext().put("QualifiedNonCitizenCode", individualResponse.getQualifiedNonCitizenCode());
			this.getResponseContext().put("FiveYearBarMetCode", individualResponse.getFiveYearBarMetCode());
			this.getResponseContext().put("USCitizenCode", individualResponse.getUsCitizenCode());
		}
		
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H92-A";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
