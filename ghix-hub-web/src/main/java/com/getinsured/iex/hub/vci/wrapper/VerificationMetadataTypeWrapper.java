package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.VerificationMetadataType;

/**
 * Wrapper class for com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.
 * CurrentHouseholdIncomeType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class VerificationMetadataTypeWrapper implements JSONAware {

	private String verificationID;

	private static final String VERIFICATION_ID_KEY = "VerificationID";

	public VerificationMetadataTypeWrapper(
			VerificationMetadataType verificationMetadataType) {

		if (verificationMetadataType == null) {
			return;
		}

		this.verificationID = verificationMetadataType.getVerificationID() != null ? verificationMetadataType
				.getVerificationID().getValue() : null;

	}

	public String getVerificationID() {
		return verificationID;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(VERIFICATION_ID_KEY, this.verificationID);
		return obj.toJSONString();
	}
}
