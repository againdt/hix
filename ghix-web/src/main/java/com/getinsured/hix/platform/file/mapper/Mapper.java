package com.getinsured.hix.platform.file.mapper;

import java.util.List;

/**
 * Abstract class to be used for preparing Model object from data and columns specified
 * and return the corresponding object.
 */
public abstract class Mapper<T> 
{
	public abstract T mapData(List<String> lineData, List<String> columns);
}
