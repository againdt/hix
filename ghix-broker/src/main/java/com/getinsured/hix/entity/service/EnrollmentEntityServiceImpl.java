package com.getinsured.hix.entity.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.repository.IEnrollmentEntityRepository;
import com.getinsured.hix.model.entity.EnrollmentEntity;

/**
 * Implements {@link EnrollmentEntityService} to perform Enrollment Entity
 * related operations like save,update etc.
 * 
 */
@Service("enrollmentEntityService")
@Transactional
public class EnrollmentEntityServiceImpl implements EnrollmentEntityService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityServiceImpl.class);
	
	@Autowired
	private IEnrollmentEntityRepository enrollmentEntityRepository;

	/**
	 * @see EnrollmentEntityService#generateEnrollmentEntityRecordId(EnrollmentEntity)
	 */
	@Override
	@Transactional
	public Integer generateEnrollmentEntityRecordId(EnrollmentEntity enrollmentEntity) {
		
		LOGGER.info("generateEnrollmentEntityRecordId: START");
		
		EnrollmentEntity newEnrollmentEntity = null;
		Integer enrollmentEntityId = null;
		if (enrollmentEntity != null) {
			newEnrollmentEntity = enrollmentEntityRepository.save(enrollmentEntity);
			enrollmentEntityId = newEnrollmentEntity.getId();
		}
		
		LOGGER.info("generateEnrollmentEntityRecordId: END");
		
		return enrollmentEntityId;
	}

	/**
	 * @see EnrollmentEntityService#findByEmailAddress(String)
	 */
	@Override
	@Transactional
	public EnrollmentEntity findByEmailAddress(String emailAddress) {
		
		LOGGER.info("findByEmailAddress: START");
		LOGGER.info("findByEmailAddress: END");
		
		return enrollmentEntityRepository.findByEmailAddress(emailAddress);
	}

	/**
	 * @see EnrollmentEntityService#findByStateEIN(String)
	 */
	@Override
	@Transactional
	public EnrollmentEntity findByStateEIN(String stateEIN) {
		
		LOGGER.info("findByStateEIN: START");
		LOGGER.info("findByStateEIN: END");
		
		return enrollmentEntityRepository.findByStateTaxID(stateEIN);
	}
}