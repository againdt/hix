package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.util.EligibilityConstants;

public final class MatchHandler {

	private static final String LOGGER_ENDS = "logger - Ends - #####################################################";
	private static final String LOGGER_STARTS = "logger - Starts - #####################################################";
	private static final String LIST_OF_SS_AP_APPLICANTS_IN_OUR_SS_AP_DB = "List of SSAp Applicants in our SSAp db - ";
	private static final String AT_TAX_PERSON_GET_PERSON_INFO = "atTaxPerson.getPersonInfo() - ";
	private static final String DO_NOT_PROCEED_WITH_MATCHING_LOGIC_REPORT_THIS_HOUSEHOLD_AS_MISMATCH = "DO NOT proceed with matching logic... report this household as mismatch";
	private static final String SSN_FOUND_IN_AT_RESPONSE_AND_BASIC_SSN_CHECK_FAILED = "SSN found in AT Response... and basic SSN check failed...";
	private static final String MEDICAID_FOUND_IN_AT_RESPONSE_AND_BASIC_MEDICAID_CHECK_FAILED = "Medicaid found in AT Response... and basic Medicaid check failed...";
	private static final String TAX_PERSON_INFO_RECEIVED_FROM_AT_RESPONSE_NOT_FOUND_IN_OUR_SS_AP_DB = "taxPersonInfo received from AT Response, not found in our SSAp db ....";

	private static final Logger LOGGER = Logger.getLogger(MatchHandler.class);

	private MatchHandler() { }

	public static Map<String, Object> matchMembers(List<TaxHouseholdMember> atHHList, List<TaxHouseholdMember> ssapHHList) {

		List<TaxHouseholdMember> unmatchedMemberList = new ArrayList<>();
		String reason = EligibilityConstants.OK;
		if (atHHList.size() != ssapHHList.size()){
			// count does not match
			if (atHHList.size() < ssapHHList.size()) {
				// missing individuals in the received response...
				for (TaxHouseholdMember atTaxPerson : atHHList) {
					findMatchingMember(atTaxPerson, ssapHHList);
				}

				populateUnmatchedMemberList(ssapHHList, unmatchedMemberList);

				reason = EligibilityConstants.MISSING_MEMBERS;
			} else {
				// new individuals in the received response...
				for (TaxHouseholdMember atTaxPerson : atHHList) {
					findMatchingMember(atTaxPerson, ssapHHList);
				}

				populateUnmatchedMemberList(atHHList, unmatchedMemberList);

				reason = EligibilityConstants.NEW_MEMBER;
			}
		} else {

			for (TaxHouseholdMember atTaxPerson : atHHList) {

				TaxHouseholdMember ssapApplicant = findMatchingMember(atTaxPerson, ssapHHList);

				if (ssapApplicant == null){
					addToUnmatchedMemberList(unmatchedMemberList, atTaxPerson);
					reason = EligibilityConstants.MISMATCH_MEMBER;
				}
			}

		}

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("reason", reason);
		result.put(EligibilityConstants.UNMATCHED_MEMBER_LIST, unmatchedMemberList);

		return result;
	}

	private static void populateUnmatchedMemberList( List<TaxHouseholdMember> hhList, List<TaxHouseholdMember> unmatchedMemberList) {

		for (TaxHouseholdMember person : hhList) {
			if (!person.getPersonInfo().isMatched()){
				addToUnmatchedMemberList(unmatchedMemberList, person);
			}
		}
	}

	private static void addToUnmatchedMemberList( List<TaxHouseholdMember> unmatchedMemberList,	TaxHouseholdMember person) {
		unmatchedMemberList.add(person);
	}

	/**
	 * Matches AT Response member with the SSAP member list
	 * @param atTaxPerson
	 * @param ssapHHList
	 * @return
	 */
	public static TaxHouseholdMember findMatchingMember(TaxHouseholdMember atTaxPerson, List<TaxHouseholdMember> ssapHHList){

		TaxHouseholdMember ssapApplicant;

		if (!StringUtils.isEmpty(atTaxPerson.getPersonInfo().getApplicantIdentificationId())){
			// Medicaid found in AT Response...
			ssapApplicant = matchMedicaidId(atTaxPerson, ssapHHList);
		} else if (!StringUtils.isEmpty(atTaxPerson.getPersonInfo().getSsn())){
			// SSN found in AT Response...
			ssapApplicant = matchSSN(atTaxPerson, ssapHHList);
		} else {
			// SSN NOT found in AT Response...
			ssapApplicant = matchFnLnDoB(atTaxPerson, ssapHHList);
		}

		return ssapApplicant;
	}

	private static TaxHouseholdMember matchFnLnDoB( TaxHouseholdMember atTaxPerson, List<TaxHouseholdMember> ssapHHList) {
		boolean foundApplicant = false;
		TaxHouseholdMember ssapApplicant = null;

		for (TaxHouseholdMember ssapPerson : ssapHHList) {
			if (StringUtils.equalsIgnoreCase(atTaxPerson.getPersonInfo().getFirstName(), ssapPerson.getPersonInfo().getFirstName())
					&& StringUtils.equalsIgnoreCase(atTaxPerson.getPersonInfo().getLastName(), ssapPerson.getPersonInfo().getLastName())
					&& DateUtils.isSameDay(atTaxPerson.getPersonInfo().getDob(), ssapPerson.getPersonInfo().getDob())
					){
				foundApplicant = true;
				atTaxPerson.getPersonInfo().setMatched(true);
				ssapPerson.getPersonInfo().setMatched(true);
				ssapApplicant = ssapPerson;
				break;
			}
		}

		if (!foundApplicant){
			if (LOGGER.isDebugEnabled()){

				LOGGER.debug(TAX_PERSON_INFO_RECEIVED_FROM_AT_RESPONSE_NOT_FOUND_IN_OUR_SS_AP_DB);
				LOGGER.debug(LOGGER_STARTS);
				LOGGER.debug(AT_TAX_PERSON_GET_PERSON_INFO + atTaxPerson.getPersonInfo());
				LOGGER.debug(LIST_OF_SS_AP_APPLICANTS_IN_OUR_SS_AP_DB + ssapHHList);
				LOGGER.debug(LOGGER_ENDS);
			}
			ssapApplicant = null;
		}

		return ssapApplicant;
	}

	private static TaxHouseholdMember matchSSN(TaxHouseholdMember atTaxPerson, List<TaxHouseholdMember> ssapHHList) {

		boolean foundApplicant = false;
		TaxHouseholdMember ssapApplicant = null;

		for (TaxHouseholdMember ssapPerson : ssapHHList) {
			String atSSN = StringUtils.replace(atTaxPerson.getPersonInfo().getSsn(), "-", "");
			String ssapSSN = StringUtils.replace(ssapPerson.getPersonInfo().getSsn(), "-", "");
			if (StringUtils.equals(atSSN, ssapSSN)){
				foundApplicant = true;
				atTaxPerson.getPersonInfo().setMatched(true);
				ssapPerson.getPersonInfo().setMatched(true);
				ssapApplicant = ssapPerson;
				break;
			}
		}

		if (!foundApplicant){
			if (LOGGER.isDebugEnabled()){

				LOGGER.debug(TAX_PERSON_INFO_RECEIVED_FROM_AT_RESPONSE_NOT_FOUND_IN_OUR_SS_AP_DB);
				LOGGER.debug(SSN_FOUND_IN_AT_RESPONSE_AND_BASIC_SSN_CHECK_FAILED);
				LOGGER.debug(DO_NOT_PROCEED_WITH_MATCHING_LOGIC_REPORT_THIS_HOUSEHOLD_AS_MISMATCH);

				LOGGER.debug(LOGGER_STARTS);
				LOGGER.debug(AT_TAX_PERSON_GET_PERSON_INFO + atTaxPerson.getPersonInfo());
				LOGGER.debug(LIST_OF_SS_AP_APPLICANTS_IN_OUR_SS_AP_DB + ssapHHList);
				LOGGER.debug(LOGGER_ENDS);
			}
			ssapApplicant = null;
		}

		return ssapApplicant;
	}


	private static TaxHouseholdMember matchMedicaidId(TaxHouseholdMember atTaxPerson, List<TaxHouseholdMember> ssapHHList) {

		boolean foundApplicant = false;
		TaxHouseholdMember ssapApplicant = null;

		for (TaxHouseholdMember ssapPerson : ssapHHList) {
			String atMedicaidId = atTaxPerson.getPersonInfo().getApplicantIdentificationId();
			String ssapMedicaidId = ssapPerson.getPersonInfo().getApplicantIdentificationId();
			if (StringUtils.equals(atMedicaidId, ssapMedicaidId)){
				foundApplicant = true;
				atTaxPerson.getPersonInfo().setMatched(true);
				ssapPerson.getPersonInfo().setMatched(true);
				ssapApplicant = ssapPerson;
				break;
			}
		}

		if (!foundApplicant){
			if (LOGGER.isDebugEnabled()){

				LOGGER.debug(TAX_PERSON_INFO_RECEIVED_FROM_AT_RESPONSE_NOT_FOUND_IN_OUR_SS_AP_DB);
				LOGGER.debug(MEDICAID_FOUND_IN_AT_RESPONSE_AND_BASIC_MEDICAID_CHECK_FAILED);
				LOGGER.debug(DO_NOT_PROCEED_WITH_MATCHING_LOGIC_REPORT_THIS_HOUSEHOLD_AS_MISMATCH);

				LOGGER.debug(LOGGER_STARTS);
				LOGGER.debug(AT_TAX_PERSON_GET_PERSON_INFO + atTaxPerson.getPersonInfo());
				LOGGER.debug(LIST_OF_SS_AP_APPLICANTS_IN_OUR_SS_AP_DB + ssapHHList);
				LOGGER.debug(LOGGER_ENDS);
			}
			ssapApplicant = null;
		}

		return ssapApplicant;
	}



}
