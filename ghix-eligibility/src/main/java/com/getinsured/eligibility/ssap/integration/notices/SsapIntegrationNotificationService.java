package com.getinsured.eligibility.ssap.integration.notices;


public interface SsapIntegrationNotificationService {


	String generateAdditionalInformationDocument(String caseNumber) throws Exception;
	
}
