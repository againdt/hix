package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPLRInboundBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrollmentMonthlyPLRInboundTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLRInboundTask.class);
	private EnrollmentMonthlyPLRInboundBatchService enrollmentMonthlyPLRInboundBatchService;
	private JobService jobService;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		if(null != enrollmentMonthlyPLRInboundBatchService){
			LOGGER.trace(" EnrollmentMonthlyPLRInboundTask calling EnrollmentMonthlyPLRInboundBatchService ");
			
			Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
			
			Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			enrollmentMonthlyPLRInboundBatchService.processPlrInFile(jobId);
			
		}else{
			LOGGER.error("enrollmentMonthlyPLRInboundBatchService is null ");
			throw new RuntimeException("enrollmentMonthlyPLRInboundBatchService is null") ;
		}
		return RepeatStatus.FINISHED;
	}
	
	/**
	 * @return the enrollmentMonthlyPLRInboundBatchService
	 */
	public EnrollmentMonthlyPLRInboundBatchService getEnrollmentMonthlyPLRInboundBatchService() {
		return enrollmentMonthlyPLRInboundBatchService;
	}

	/**
	 * @param enrollmentMonthlyPLRInboundBatchService the enrollmentMonthlyPLRInboundBatchService to set
	 */
	public void setEnrollmentMonthlyPLRInboundBatchService(
			EnrollmentMonthlyPLRInboundBatchService enrollmentMonthlyPLRInboundBatchService) {
		this.enrollmentMonthlyPLRInboundBatchService = enrollmentMonthlyPLRInboundBatchService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
