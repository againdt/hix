<SCRIPT TYPE="text/javascript">
$('.checknone').click(function() {
  if ( $('.checknone').is(':checked') ) {
    $('.checkboxes').removeAttr('checked');
  }
})
</SCRIPT>

<script type="text/ng-template" id="maritalstatus-special.jsp">

  <div class="gutter10" ng-controller="maritalStatusSC" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.additionalquestions"/></h3>
    <form class="form-horizontal" name="additionalQuestionsForm">
    <p class="alert alert-info"><spring:message code="label.lce.plsAnsAddQ"/></p>
      <!-- Tribe member? -->
      <div class="tribeMember">
        	<div id="native">
          		<label for="hidFMLNameLabel1"><spring:message code="label.lce.is"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> an American Indian or Alaska Native? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
					<input type="hidden" id="hidFMLNameLabel1" />
				</label>
          		<div class="margin10-l">
            		<label class="radio-inline" for="indiannativeyes">
              			<input type="radio" id="indiannativeyes" name="indiannative" required value="Yes" ng-value="true" ng-model="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator"> <spring:message code="label.lce.yes"/>
           			 </label>
            		<label class="radio-inline" for="indiannativeno">
              			<input type="radio" id="indiannativeno" name="indiannative" required value="No" ng-value="false" ng-model="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator"> <spring:message code="label.lce.no"/>
            		</label>
          		</div>
				      <!-- Error -->
          			<div class="controls">
            				<span class="validation-error-text-container" ng-if="submitted && additionalQuestionsForm.indiannative.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
          			</div>
        	</div>

  <div class="margin30-t" ng-if="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" >
      <!-- Tribe member? -->
      <div class="tribeMember">
        <div id="tribe">
          	<label for="hidFMLNameLabel2"><spring:message code="label.lce.is"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> <spring:message code="label.lce.amemberofarecognizedtribe"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
				<input type="hidden" id="hidFMLNameLabel2" />
			</label>
          	<div class="margin10-l">
            	<label class="radio-inline" for="tribeYes">
              		<input type="radio" id="tribeYes" name="tribeRadio" value="Yes" ng-value="true" ng-required="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" ng-model="householdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator"> <spring:message code="label.lce.yes"/>
           		 </label>
           		 <label class="radio-inline" for="tribeNo">
              		<input type="radio" id="tribeNo" name="tribeRadio" value="No" ng-value="false" ng-required="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" ng-click="householdMember.americanIndianAlaskaNative.tribeName='';householdMember.americanIndianAlaskaNative.tribeFullName='';householdMember.americanIndianAlaskaNative.state='';householdMember.americanIndianAlaskaNative.stateFullName='';" ng-model="householdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator"> <spring:message code="label.lce.no"/>
            	</label>
          	</div>
			<!-- Error -->
          			<!--<div class="controls margin5-t">
            				<spring:message code="label.lce.thisvalueisrequired"/>
          			</div>-->
        </div>

      <!-- What state and tribe? -->
      <div ng-if="householdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true">
          <div class="control-group">
            <label class="control-label" for="ddlState"><spring:message code="label.lce.state"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
               <select id="ddlState" name="state" ng-model="householdMember.americanIndianAlaskaNative.state" ng-change="loadTribes(householdMember.americanIndianAlaskaNative.state);" select-label-model="householdMember.americanIndianAlaskaNative.stateFullName" ng-required="householdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true">
               		<option ng-repeat="state in states" value="{{state.stateCode}}"   ng-selected="{{state.stateCode == householdMember.americanIndianAlaskaNative.state}}" label="{{state.stateName | capitalize:true}}">{{state.stateName | capitalize:true}}</option>
			  </select>
            </div>
			      <div class="controls validation-error-text-container" ng-if="submitted && additionalQuestionsForm.state.$error.required" >
              <spring:message code="label.lce.thisvalueisrequired"/>
            </div>
          </div>


          <div class="control-group">
            <label class="control-label" for="ddlTribeName"><spring:message code="label.lce.tribename"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls" >
              <select id="ddlTribeName" name="tribes" ng-model="householdMember.americanIndianAlaskaNative.tribeName" select-label-model="householdMember.americanIndianAlaskaNative.tribeFullName" ng-required="householdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true">
                <option value="">Select Tribe</option>
				<option ng-repeat="(key,value) in tribes" value="{{key}}" ng-selected="{{key == householdMember.americanIndianAlaskaNative.tribeName }}" label="{{value}}">{{value}}</option>
              </select>
            </div>
			      <div class="controls validation-error-text-container" ng-if="submitted && additionalQuestionsForm.tribes.$error.required" >
              <spring:message code="label.lce.thisvalueisrequired"/>
            </div>
          </div>

        </div><!-- What state and tribe? -->
      </div><!-- tribeMember -->

      <!-- SPOUSE HAVE A DISABILITY? -->
      <!--<spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> <spring:message code="label.haveadisability"/>
      <div class="control-group">
        <label class="control-label" for="hidSpouseDisabilityField"><span class="aria-hidden">Spouse have a disability?</span> <input type="hidden" id="hidSpouseDisabilityField" /></label>

        <div class="control-group">
          <div class="controls">
            <label class="radio inline">
				<span class="aria-hidden">disabilityIndicatorT</span>
              <input type="radio" value="true" ng-value="true"  id="disabilityIndicatorT" name="disabilityIndicator" ng-model="householdMember.disabilityIndicator"> <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio inline">
				<span class="aria-hidden">disabilityIndicatorF</span>
              <input type="radio" value="false" ng-value="false" id="disabilityIndicatorF" name="disabilityIndicator" ng-model="householdMember.disabilityIndicator"> <spring:message code="label.lce.no"/>
            </label>
          </div>
        </div>
      </div>-->

      <!-- MEDICAL FACILITY -->
      <!--<spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> <spring:message code="label.lce.needhelp"/>
      <div class="control-group">
        <label class="control-label" for="hidMedicalFacilityField"><span class="aria-hidden">Medical Facility</span> <input type="hidden" id="hidMedicalFacilityField" /></label>
        <div class="control-group">
          <div class="controls">
            <label class="radio inline">
				<span class="aria-hidden">livingArrangementIndicatorT</span>
              <input type="radio" value="true" ng-value="true" id="livingArrangementIndicatorT" name="livingArrangementIndicator" ng-model="householdMember.livingArrangementIndicator"> <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio inline">
				<span class="aria-hidden">livingArrangementIndicatorF</span>
              <input type="radio" value="false" ng-value="false" id="livingArrangementIndicatorF" name="livingArrangementIndicator" ng-model="householdMember.livingArrangementIndicator"> <spring:message code="label.lce.no"/>
            </label>
          </div>
        </div>
      </div>-->

      <!-- MEDICAID -->
      <!--<spring:message code="label.lce.is"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> <spring:message code="label.lce.eligible"/>
      <div class="control-group">
        <label class="control-label" for="hidMedicadField"><span class="aria-hidden">Medicad</span> <input type="hidden" id="hidMedicadField" /></label>
        <div class="control-group">
          <div class="controls">
            <label class="checkbox">
				<span class="aria-hidden">medicareEligibleIndicator</span>
              <input type="checkbox" class="checkboxes" id="currentOtherInsurance.medicareEligibleIndicator" ng-model="householdMember.currentOtherInsurance.medicareEligibleIndicator"> <spring:message code="label.lce.medicare"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox">
				<span class="aria-hidden">tricareEligibleIndicator</span>
              <input type="checkbox" class="checkboxes" id="currentOtherInsurance.tricareEligibleIndicator" ng-model="householdMember.currentOtherInsurance.tricareEligibleIndicator"> <spring:message code="label.lce.tricare"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox">
				<span class="aria-hidden">peaceCorpsIndicator</span>
              <input type="checkbox" class="checkboxes" id="currentOtherInsurance.peaceCorpsIndicator" ng-model="householdMember.currentOtherInsurance.peaceCorpsIndicator" > <spring:message code="label.lce.peacecorps"/>
            </label>
          </div>
          <div class="controls">
            <label class="checkbox">
				<span class="aria-hidden">otherStateOrFederalProgramIndicator</span>
              <input type="checkbox" class="checkboxes" id="otherStateOrFederalProgramIndicator" ng-model="householdMember.currentOtherInsurance.otherStateOrFederalProgramIndicator" ng-change="otherStateOrFederalProgramIndicator()"> <spring:message code="label.lce.otherstateprogram"/>
            </label>
          </div>

       <div id="otherprogram" class="margin20-t" ng-if="householdMember.currentOtherInsurance.otherStateOrFederalProgramIndicator">
          <label class="control-label" for="hidLCEType"><spring:message code="label.lce.type"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
			<input type="hidden" id="hidLCEType" />	
		</label>
          <div class="control-group">
            <div class="controls">
              <input type="text" class="span7" id="currentOtherInsurance.otherStateOrFederalProgramType" ng-model="householdMember.currentOtherInsurance.otherStateOrFederalProgramType" placeholder='<spring:message code="label.lce.type"/>'/>
            </div>
          </div>

          <label class="control-label"><spring:message code="label.lce.nameofprogram"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
          <div class="control-group">
            <div class="controls">
              <input type="text" class="span7" id="currentOtherInsurance.otherStateOrFederalProgramName" ng-model="householdMember.currentOtherInsurance.otherStateOrFederalProgramName" placeholder='<spring:message code="label.lce.nameofprogram"/>'/>
            </div>
          </div>
        </div>

          <div class="controls">
            <label class="checkbox">
				<span class="aria-hidden">noneOfTheAbove</span>
              <input type="checkbox" name="otherhealth" id="noneOfTheAbove" ng-click="noneOfTheAbove()"> <spring:message code="label.lce.noneoftheabove"/>
            </label>
          </div>
        </div>
      </div>
    </form>-->



  </div>
 <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
		 <a  role="button" tabindex="0"ng-if="additionalQuestionsForm.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
  	 <a  role="button" tabindex="0"ng-if="!additionalQuestionsForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true" ><spring:message code="label.lce.next"/></a>

    </dl>

</script>