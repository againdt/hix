package com.getinsured.eligibility.at.ref.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

/**
 * @author chopra_s
 * 
 */
@Component("lceNoChangesHandlerService")
@Scope("singleton")
public class LceNoChangesHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceNoChangesHandlerService.class);

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceNoChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		createNoChangeEvent(currentApplication, enrolledApplication);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		final String status = moveEnrollmentToNewApplication(currentApplication, enrolledApplication);
		if (!status.equals(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIRuntimeException(ERROR_MOVE_ENROLLMENT + currentApplication.getId());
		}
		
		//HIX-109703
		String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		String previousDentalApplicationStatus = enrolledApplication.getApplicationDentalStatus();
		closeSsapApplication(enrolledApplication);
		currentApplication.setApplicationStatus(previousApplicationStatus);
		currentApplication.setApplicationDentalStatus(previousDentalApplicationStatus);
		updateSsapApplication(currentApplication); 
		//updateCurrentAppToEN(currentApplication);
		
		copyEHBAmount(currentApplication, enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		
		//triggerNoChangeEmail(currentApplication);
		LOGGER.info("LceNoChangesHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createNoChangeEvent(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Create No change SEP event - " + currentApplication.getId());
		ssapApplicationEventService.createDuplicateOnlyApplicationEvent(currentApplication.getId(), enrolledApplication.getId());
	}

	/*private void triggerNoChangeEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger No change Email - Notice LCE5");

		try {
			referralLceNotificationService.generateNoChangeNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE5").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());

		}
	}*/
}
