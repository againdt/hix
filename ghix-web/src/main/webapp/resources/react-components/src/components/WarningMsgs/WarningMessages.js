import React from 'react';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';

import WarningMessage from './WarningMessage';
import { APP_STATUSES, APP_TYPES, WARNING_TYPES } from '../../constants/dashboardModelTypes';

const WarningMessages = ({ tabData, config, stateCode, prevTabYear }) => {
    const {
        applicationStatus,
        applicationDentalStatus,
        applicationType,
        applicants,
        aptc,
        stateSubsidy,
        isInsideOEWindow,
        isInsideOEEnrollmentWindow,
        isSepOpen,
        caseNumber,
        isEligibilityConditional
    } = tabData;
    const { oeApproachingdays, oepStartDate, oepEndDate, sepStartDate, sepEndDate, oeEnrollEndDate } = config;
    const serverDate = config.serverDate ? moment(config.serverDate, 'MM-DD-YYYY') : moment();

    /**
     * OE Approaching message is shown {amount} days before OE Start Date.
     * @param oepStartDate
     * @param amount - default 30
     * @returns {boolean}
     */
    const isOEApproaching = (oepStartDate, amount = 30) => {
        if (moment(serverDate.format('L')).isSame(moment(oepStartDate, 'MM-DD-YYYY'))) {
            return false;
        }

        var tmpOEPStartDate = moment(oepStartDate, 'MM-DD-YYYY').subtract(amount, "days").format('MM-DD-YYYY');
        return (serverDate.isBetween(moment(tmpOEPStartDate, 'MM-DD-YYYY'),
            moment(oepStartDate, 'MM-DD-YYYY'), 'days', true));
    };

    const isOEEndDateApproaching = (OEPStartDate, OEPEndDate) => {
        if (!OEPStartDate && !OEPEndDate)
            return false;

        return isBetween(OEPStartDate, OEPEndDate);
    };

    const isBetween = (startDate, endDate) => {
        if (moment(serverDate.format('L')).isSame(moment(startDate, 'MM-DD-YYYY'))) {
            return true;
        }

        if (moment(serverDate.format('L')).isSame(moment(endDate, 'MM-DD-YYYY'))) {
            return true;
        }

        return (serverDate.isBetween(moment(startDate, 'MM-DD-YYYY'), moment(endDate, 'MM-DD-YYYY'), 'days', true));
    };

    const isAppStatusesEnrolled = () => {
        return applicationStatus === APP_STATUSES.EN && applicationDentalStatus === APP_STATUSES.EN;
    };

    const getOEApproachingMessage = (OEApproachingdays, OEPStartDate, OEPEndDate) => {
        return <FormattedMessage
            id="process_warnings.oe_approaching"
            values={{
                nextyear: serverDate.year() + 1,
                OEStartDate: OEPStartDate,
                OEEndDate: OEPEndDate,
                currentyear: serverDate.year(),
                OEYear: moment(OEPStartDate, 'MM-DD-YYYY').year()
            }}/>
    };

    const getOEEndDateApproachingMessage = (OEPStartDate, OEPEndDate) => {
        let endDate = serverDate;
        let startDate = moment(OEPEndDate);
        let days = startDate.diff(endDate, 'days');
        return <FormattedMessage
            id="process_warnings.oe_end_date_approaching_message"
            values={{
                OEPEndDate: OEPEndDate,
                oedays: days,
                oedaysPlusOne: days + 1,
                oeEnrollEndDate: oeEnrollEndDate,
                prevTabYear: !!prevTabYear ? <FormattedMessage id="process_warnings.oe_end_date_approaching_message_prev_tab" values={{year: prevTabYear}}/> : ''
            }}/>
    };

    const getSEPDateMessage = (sepStartDate, sepEndDate) => {
        const endData = serverDate;
        const startDate = moment(sepEndDate);
        const days = startDate.diff(endData, 'days');
        return <FormattedMessage
            id="process_warnings.sep_ending"
            values={{ sepdays: days }}/>
    };

    const getDMIFailureMessage = (caseNumber) => {
        const link = `/hix/ssap/verificationResultDashboard?caseNumber=${caseNumber}`;
        return <FormattedMessage id="process_warnings.DMI_failure"
                                 values={{
                                     uploadDocuments: <a style={{ textTransform: 'lowercase' }} href={link}
                                                         target="_blank">
                                         <FormattedMessage id="further_action.cta.btn_upload_documents"/>
                                     </a>
                                 }}
        />
    };

    const getAPTCChangeMessage = (totalAPTC, totalSPTC = 0, applicants) => {
        const isAPTCEligible = applicants.some(applicant => applicant.aptcEligible);
        const isSPTCEligible = applicants.some(applicant => applicant.stateSubsidyEligible);
        const isCSREligible = applicants.some(applicant => applicant.csrEligible);
        const andOrMessage = isAPTCEligible ? <FormattedMessage id="process_warnings.and_or"/> : '';

        switch(stateCode) {
            case 'CA':
                return <FormattedMessage id="process_warnings.aptc_change"/>;
            case 'MN':
                return <FormattedMessage
                    id="process_warnings.aptc_change"
                    values={{
                        isAPTCEligible: isAPTCEligible ? <FormattedMessage id="process_warnings.aptc_eligible" values={{ totalAPTC: totalAPTC}}/> : '',
                        isCSREligible: isCSREligible ? <FormattedMessage id="process_warnings.csr_eligible" values={{andOr: andOrMessage}}/> : ''
                    }}/>;
            default:
                return <FormattedMessage
                    id="process_warnings.aptc_change"
                    values={{
                        isAPTCEligible: isAPTCEligible || isSPTCEligible ?
                            <FormattedMessage id="process_warnings.aptc_eligible" values={{ totalSubsidy: (totalAPTC + totalSPTC).toFixed(2)}}/> : '',
                        isCSREligible: isCSREligible ? <FormattedMessage id="process_warnings.csr_eligible" values={{andOr: andOrMessage}}/> : ''
                    }}/>
        }
    };

    const checkChangeType = (applicants, changeType) => {
        let result = false;

        applicants.forEach(applicant => {
            if (applicant.sepApplicantEvents && applicant.sepApplicantEvents.length > 0) {
                let isSepApplicantEvent = applicant.sepApplicantEvents.some(sepApplicantEvent => {
                    return sepApplicantEvent.changeType === changeType;
                });

                if (isSepApplicantEvent) {
                    result = isSepApplicantEvent;
                }
            }
        });

        return result;
    };

    const getCountChangeType = (applicants, changeType) => {
        let cnt = 0;

        applicants.map(applicant => {
            if (applicant.sepApplicantEvents && applicant.sepApplicantEvents.length > 0) {
                applicant.sepApplicantEvents.map(sepApplicantEvent => {
                    if (sepApplicantEvent.changeType === changeType) {
                        cnt++;
                    }
                });
            }
        });

        return cnt;
    }

    /**
     * Checks if at least one applicant in the household has sepApplicantEvents with eventName like 'INCOME_CHANGE';
     * @param applicants
     * @returns {boolean}
     */
    const checkChangeIncomeChangeEventName = (applicants) => {
        const INCOME_CHANGE_VALUES = ['INCOME_CHANGE', 'CHANGE_IN_INCOME', 'INCOME_CHANGE_CSR', 'INCOME_CHANGE_FUTURE', 'INCOME_CHANGE_CSR_FUTURE'];
        let result = false;

        applicants.forEach(applicant => {
            if (applicant.sepApplicantEvents && applicant.sepApplicantEvents.length > 0) {
               let isSepApplicantEvent = applicant.sepApplicantEvents.some(sepApplicantEvent => {
                    return INCOME_CHANGE_VALUES.includes(sepApplicantEvent.eventName);
                });

                if (isSepApplicantEvent) {
                    result = isSepApplicantEvent;
                }
            }
        });

        return result;
    };

    /**
     * Retuns true if at least one member in the HH has seeksQHP=false.
     */

    const anyMemberNotSeekingQHP = applicants => applicants && applicants.some((applicant) => applicant.seeksQHP === false);

    /**
     * Returns true if serverDate is between oepEndDate - 3 and oeEnrollEndDate. Inclusively.
     *
     * @param serverDate
     * @param oepEndDate
     * @param oeEnrollEndDate
     * @returns {boolean}
     */
    const isExtendedOEPDates = (serverDate, oepEndDate, oeEnrollEndDate) => {
        // oepEndDate - 3 days
        const oepEndDateRange = moment(oepEndDate).subtract(3, "days").format('MM-DD-YYYY');
        return (serverDate.isBetween(moment(oepEndDateRange, 'MM-DD-YYYY'),
            moment(oeEnrollEndDate, 'MM-DD-YYYY'), 'days', true));
    };

    const getAlertsWarnings = (tabData) => {
        let warnings = [];

        // OE Approaching
        if (isOEApproaching(oepStartDate)) {
            const OEApproachingMessage = {
                type: WARNING_TYPES.ALERT,
                noticeText: getOEApproachingMessage(oeApproachingdays, oepStartDate, oepEndDate),
                showIcon: true,
            };

            warnings.push(OEApproachingMessage);
        }

        // OE End Date Approaching
        if (isInsideOEWindow &&
            (applicationType === APP_TYPES.OE || !applicationType) && !isAppStatusesEnrolled()) {
            if (isOEEndDateApproaching(oepStartDate, oepEndDate)) {
                const OEEndDateApproachingMessage = {
                    type: WARNING_TYPES.ALERT,
                    noticeText: getOEEndDateApproachingMessage(oepStartDate, oepEndDate),
                    showIcon: true,
                };

                warnings.push(OEEndDateApproachingMessage);
            }
        }

        //SEP date
        if (isSepOpen && !isAppStatusesEnrolled()) {
            const SEPDateMessage = {
                type: WARNING_TYPES.WARNING,
                noticeText: getSEPDateMessage(sepStartDate, sepEndDate),
                showIcon: true,
            };

            if (stateCode === 'NV' ) {
                !isInsideOEEnrollmentWindow && warnings.push(SEPDateMessage);

            } else {
                warnings.push(SEPDateMessage);
            }

        }

        // DMI failure
        if (isEligibilityConditional) {
            const DMIFailureMessage = {
                type: WARNING_TYPES.WARNING,
                noticeText: getDMIFailureMessage(caseNumber),
                showIcon: false,
            };

            warnings.push(DMIFailureMessage);
        }

        // New member added
        if (isSepOpen && applicationStatus === APP_STATUSES.ER && applicationType === APP_TYPES.SEP && checkChangeType(applicants, 'ADD')) {
            const cntNewMembers = getCountChangeType(applicants, 'ADD');
            const newMemberAddedMessage = {
                type: WARNING_TYPES.ALERT,
                noticeText: <FormattedMessage id="process_warnings.new_member_added" values={{cntNewMembers}} />,
                showIcon: false,
            };

            warnings.push(newMemberAddedMessage);
        }

        // Member Removed
        if (isSepOpen && applicationStatus === APP_STATUSES.ER && applicationType === APP_TYPES.SEP && checkChangeType(applicants, 'REMOVE')) {
            const newMemberAddedMessage = {
                type: WARNING_TYPES.ALERT,
                noticeText: <FormattedMessage id="process_warnings.member_removed"/>,
                showIcon: false,
            };

            warnings.push(newMemberAddedMessage);
        }

        // APTC Change
        if (isSepOpen && applicationStatus === APP_STATUSES.ER && applicationType === APP_TYPES.SEP && checkChangeIncomeChangeEventName(applicants)) {
            const APTCChangeMessage = {
                type: WARNING_TYPES.ALERT,
                noticeText: getAPTCChangeMessage(aptc, stateSubsidy, applicants),
                showIcon: false,
            };

            warnings.push(APTCChangeMessage);
        }

        // Not seeking QHP
        if (anyMemberNotSeekingQHP(applicants)) {
            const notSeekingQHP = {
                type: WARNING_TYPES.WARNING,
                noticeText: <FormattedMessage id="process_warnings.not_seeking_QHP"/>,
                showIcon: false,
            };

            warnings.push(notSeekingQHP);
        }

        // Extended OEP (NV only!)
        if (stateCode === 'NV' && applicationType === APP_TYPES.OE &&
            isExtendedOEPDates(serverDate, oepEndDate, oeEnrollEndDate) && !isAppStatusesEnrolled()) {
            const extendedOEP = {
                type: WARNING_TYPES.WARNING,
                noticeText: <FormattedMessage id="process_warnings.extended_OEP" values={{oepEndDate: oepEndDate, oeEnrollEndDate: oeEnrollEndDate}}/>,
                showIcon: false,
            };

            warnings.push(extendedOEP);
        }

        return warnings;
    };

    let warnings = getAlertsWarnings(tabData);

    return (warnings && warnings.length > 0 &&
        <div id="auto-warnings">
            {
                warnings.map((warning, index) => {
                    return <WarningMessage key={index}
                                           content={warning.noticeText}
                                           type={warning.type}
                                           showIcon={warning.showIcon}
                    />
                })
            }
        </div>
    )
};

export default WarningMessages;