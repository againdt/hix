package com.getinsured.hix.batch.provider.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.provider.repository.IProviderUploadRepository;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

/**
 * Class is used to implement services for Provider Service Batch.
 * @since Mar 14, 2018
 */
@Service("batchProviderService")
public class ProviderServiceImpl implements ProviderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderServiceImpl.class);
	private static final int LOGS_MAX_LEN = 4000;

	@Autowired private IProviderUploadRepository iProviderUploadRepository;
	@Autowired private ContentManagementService ecmService;

	/**
	 * Method is used to get Document ECM-ID by Status from Tracking Table.
	 */
	@Override
	public String getDocumentIdByWaitingStatusFromTrackingTable(String providerTypeIndicator) {

		LOGGER.debug("getDocumentIdByWaitingStatusFromTrackingTable() Start");
		String sourceFileEcmId = null;

		try {

			ProviderUpload providerUpload = iProviderUploadRepository.findFirstByStatusAndProviderTypeIndicatorOrderByIdDesc(ProviderUpload.STATUS.WAITING.name(), providerTypeIndicator);

			if (null != providerUpload) {
				sourceFileEcmId = providerUpload.getSourceFileEcmId();
			}
			else {
				LOGGER.warn("There is no waiting tracking record found in database.");
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getDocumentIdByWaitingStatusFromTrackingTable() End");
		}
		return sourceFileEcmId;
	}

	/**
	 * Method is used to get Tracking record from file name.
	 */
	private ProviderUpload getTrackingRecordFromFileName(String fileName) {

		LOGGER.debug("getTrackingRecordFromFileName() Start");
		ProviderUpload providerUpload = null;

		try {

			if (StringUtils.isNotBlank(fileName) &&
					-1 < fileName.lastIndexOf("_") && -1 < fileName.lastIndexOf(".txt")) {

				String providerID = fileName.substring(fileName.lastIndexOf("_") + 1, fileName.lastIndexOf(".txt"));

				if (StringUtils.isNumeric(providerID)) {
					providerUpload = iProviderUploadRepository.findOne(Integer.valueOf(providerID));
				}
			}

			if (null == providerUpload && LOGGER.isWarnEnabled()) {
				LOGGER.warn("Failed to get Tracking record from File Name: " + fileName);
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getTrackingRecordFromFileName() End");
		}
		return providerUpload;
	}

	/**
	 * Method is used to update status of Tracking record from file name.
	 */
	@Override
	public void updateTrackingStatusFromFileName(String fileName, ProviderUpload.STATUS status) {

		LOGGER.debug("updateTrackingStatusFromFileName() Start");

		try {

			if (null == status) {
				LOGGER.error("Failed to update status in Tracking Table, Status should not be blank.");
				return;
			}
			ProviderUpload providerUpload = getTrackingRecordFromFileName(fileName);

			if (null != providerUpload) {
				providerUpload.setStatus(status.name());
				providerUpload = iProviderUploadRepository.save(providerUpload);

				if (null == providerUpload && LOGGER.isWarnEnabled()) {
					LOGGER.warn("Failed to update status of Tracking record from File Name: " + fileName);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("updateTrackingStatusFromFileName() End");
		}
	}

	/**
	 * Method is used to update Logs in Tracking Table using File Name.
	 */
	@Override
	public void updateLogsInTrackingTableUsingFileName(String logs, String fileName, int validRecordCount) {

		LOGGER.debug("updateLogsInTrackingTableUsingFileName() Start");

		try {
			ProviderUpload providerUpload = getTrackingRecordFromFileName(fileName);

			if (StringUtils.isBlank(logs)) {
				LOGGER.warn("Failed to update logs in Tracking Table, Logs should not be blank.");
				return;
			}

			if (null != providerUpload) {

				if (logs.length() > LOGS_MAX_LEN) {
					providerUpload.setLogs(logs.substring(0, LOGS_MAX_LEN));
				}
				else {
					providerUpload.setLogs(logs);
				}

				if (0 == validRecordCount) {
					providerUpload.setStatus(ProviderUpload.STATUS.FAILED.name());
				}
				providerUpload = iProviderUploadRepository.save(providerUpload);

				if (null == providerUpload && LOGGER.isErrorEnabled()) {
					LOGGER.error("Failed to update logs in Tracking Table for File Name: " + fileName);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("updateLogsInTrackingTableUsingFileName() End");
		}
	}

	/**
	 * Method is used to update Tracking status From IN-PROGRESS to COMPLETED/FAILED.
	 */
	@Override
	public boolean updateTrackingStatusFromInProgressToCompleteOrFail(boolean hasCompleted, String providerTypeIndicator) {

		LOGGER.debug("updateTrackingStatusFromInProgressToCompleteOrFail() Start");
		boolean hasCompletedStatus = false;

		try {
			ProviderUpload providerUpload = iProviderUploadRepository.findFirstByStatusAndProviderTypeIndicatorOrderByIdDesc(ProviderUpload.STATUS.IN_PROGRESS.name(), providerTypeIndicator);

			if (null != providerUpload) {
				providerUpload.setStatus(hasCompleted ? ProviderUpload.STATUS.COMPLETED.name() : ProviderUpload.STATUS.FAILED.name());
				providerUpload = iProviderUploadRepository.save(providerUpload);

				if (null != providerUpload) {
					hasCompletedStatus = true;
				}
				else {
					LOGGER.error("Failed to update status with COMPLETED to IN-PROGRESS Tracking Record.");
				}
			}
			else {
				LOGGER.warn("There is no IN-PROGRESS Tracking record found in database.");
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("updateTrackingStatusFromInProgressToCompleteOrFail() End");
		}
		return hasCompletedStatus;
	}

	/**
	 * Method is used to get Content Data by ID from ECM using Content-ID
	 */
	@Override
	public byte[] getContentDataById(String contentId) throws ContentManagementServiceException {
		return ecmService.getContentDataById(contentId);
	}

	/**
	 * Method is used to get Content from ECM using Content-ID
	 */
	@Override
	public Content getContentById(String contentId) throws ContentManagementServiceException {
		return ecmService.getContentById(contentId);
	}
}
