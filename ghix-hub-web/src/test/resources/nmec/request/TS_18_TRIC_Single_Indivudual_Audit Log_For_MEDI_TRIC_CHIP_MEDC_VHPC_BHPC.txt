{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{
  "taxHousehold": [
    {
      "householdMember": [ {
        
          "dateOfBirth": "09/17/1960",
          "name": {
            "firstName": "Peter",
            "middleName": "C",
            "lastName": "England"
          },
          "socialSecurityCard": {
            "socialSecurityNumber": "222224761"
          },
          "gender": "M",
        "householdContact": {
          "homeAddress": {
            "state": "MD"
          }
        },
        "insurance": {
          "insurancePolicyEffectiveDate": "01/01/2014",
          "insurancePolicyExpirationDate": "12/31/2014"
        },
        "organization": [
          {
            "organizationCode": "MEDI"
          },
          {
            "organizationCode": "BHPC"
          },
          {
            "organizationCode": "MEDC"
          },
          {
            "organizationCode": "VHPC"
          },
          {
            "organizationCode": "TRIC"
          }
        ]
      }  ]
    }
  ]
}}