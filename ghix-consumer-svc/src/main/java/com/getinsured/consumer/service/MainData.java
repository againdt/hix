package com.getinsured.consumer.service;

public class MainData
{
	String desc;
	
	public MainData(){
		
	}
	
	public MainData(String desc)
	{
		setDesc(desc);
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}