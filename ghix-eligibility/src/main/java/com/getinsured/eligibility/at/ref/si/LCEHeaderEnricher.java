package com.getinsured.eligibility.at.ref.si;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.service.LceDecisionDetermination;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 *
 */
@Component("lceHeaderEnricher")
@Scope("singleton")
public class LCEHeaderEnricher {
	private static final Logger LOGGER = Logger.getLogger(LCEHeaderEnricher.class);

	@Autowired
	@Qualifier("lceDecisionDetermination")
	private LceDecisionDetermination lceDecisionDetermination;

	@Autowired
	private ExceptionUtil exceptionUtil;
	
	private String LCE_DECISION_HEADER = "lceDecisionFlow";

	public Message<String> computeValue(Message<String> message) throws Exception {
		String headerValue = LceDecisionDetermination.ERROR_HANDLER;
		ReferralResponse referralResponse = new ReferralResponse();
		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);
		try {
			LOGGER.info("LCEHeaderEnricher computeValue starts for " + erpResponse.getSsapApplicationPrimaryKey());
			// System.out.println("erpResponse.getApplicationID() - " + erpResponse.getApplicationID());
			// System.out.println("erpResponse.getGiWsPayloadId() - " + erpResponse.getGiWsPayloadId());
			// System.out.println("erpResponse.getSsapApplicationPrimaryKey() - " + erpResponse.getSsapApplicationPrimaryKey());
			// System.out.println("erpResponse.getApplicationExtension() - " + erpResponse.getApplicationExtension());
			headerValue = lceDecisionDetermination.determineHandler(erpResponse.getSsapApplicationPrimaryKey(), erpResponse.getEnrolledApplicationAttributes());
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_REFERRAL_DESCISION + erpResponse.getApplicationID());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_REFERRAL_DESCISION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_REFERRAL_DECISION);
			exceptionUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, erpResponse.getSsapApplicationPrimaryKey());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, erpResponse.getGiWsPayloadId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID, erpResponse.getCompareEnrolledApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, erpResponse.getCompareToApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, erpResponse.getApplicationsWithSameId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION, erpResponse.getApplicationExtension());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES, erpResponse.getEnrolledApplicationAttributes());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, erpResponse.getAccountTransferCategory());
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, erpResponse.isCmrAutoLinking());
			referralResponse.getData().put(ReferralProcessingConstants.PROCESSOR_REQUESTER, erpResponse.getRequester());
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LCEHeaderEnricher computeValue ends - Response is - " + response + ", Header Value is - " + headerValue);

		return ReferralUtil.buildMessageWithHeader(response, LCE_DECISION_HEADER, headerValue, message);

	}

}
