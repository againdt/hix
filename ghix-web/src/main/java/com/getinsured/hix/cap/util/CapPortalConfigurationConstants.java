package com.getinsured.hix.cap.util;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unused")
public class CapPortalConfigurationConstants {
	private Set<String> navMenuPermissions;
	private Set<String> navMemberInfoPermissions;
	private Set<String> navEnrollmentPermissions;
	private Set<String> navHistoryPermissions;
	
	private Set<String> navMyAppPermissions;
	private Set<String> navMyAppEnrollmentPermissions;
	
	
	public CapPortalConfigurationConstants(){
		setNavMenuPermissions();
		setMemberInfoPermissions();
		setEnrollmentPermissions();
		setHistoryPermissions();
	}
	
	public void setMemberInfoPermissions(){
		Set<String> navMemberInfoPermissions = new HashSet<String>();
		for (MemberInfo memberInfo : MemberInfo.values()) {
			navMemberInfoPermissions.add(memberInfo.name());
		}
		this.navMemberInfoPermissions = navMemberInfoPermissions;
	}
	
	public void setEnrollmentPermissions(){
		Set<String> navEnrollmentPermissions = new HashSet<String>();
		for (EnrollmentInfo enrollmentInfo  : EnrollmentInfo.values()) {
			navMemberInfoPermissions.add(enrollmentInfo.name());
		}
		this.navEnrollmentPermissions = navEnrollmentPermissions;
	}
	
	public void setHistoryPermissions(){
		Set<String> navHistoryPermissions = new HashSet<String>();
		for (MemberHistory memberHistory : MemberHistory.values()) {
			navHistoryPermissions.add(memberHistory.name());
		}
		this.navHistoryPermissions = navMenuPermissions;
	}
	
	
	private static final String MEMBER_ABT_LOGCALL="memberAbtLogcall";
	private static final String MEMBER_ABT_SHOWCOMMENTS="memberAbtShowcomments";
	
	public void setNavMenuPermissions(){
		Set<String> navMenuPermissions = new HashSet<String>();
		for (NAVMenu navMenu : NAVMenu.values()) {
			navMenuPermissions.add(navMenu.name());
		}
		this.navMenuPermissions = navMenuPermissions;
	}
	
	public enum EnrollmentInfo {
		MEMBER_ENRLMNT_PRMHIST("memberEnrlmntPrmHist"), 
		MEMBER_ENRLMNT_834HIST("memberEnrlmnt834hist"), 
		MEMBER_ENRLMNT_LTST834("memberEnrlmntLtst834"),
		MEMBER_ENRLMNT_ADDNLINFO("memberEnrlmntAddnlinfo"), 
		MEMBER_PRMHIST_ACTIONS("memberPrmHistActions"), 
		MEMBER_PRMHIST_CHANGE("memberPrmHistChange"),
		MEMBER_PRMHIST_CANCEL("memberPrmHistCancel"), 
		MEMBER_ENRLMNT_STRTENDDT("memberEnrlmntStrtenddt"), 
		MEMBER_ENRLMNT_MEMBRNM("memberEnrlmntMembrName"),
		MEMBER_ENRLMNT_MEMBRGNDR("memberEnrlmntMembrGender"), 
		MEMBER_ENRLMNT_MEMBRSSN("memberEnrlmntMembrSsn"), 
		MEMBER_ENRLMNT_DPNDTSTRTENDDT("memberEnrlmntDpndtStrtEndDt"),
		MEMBER_ENRLMNT_APTC("memberEnrlmntAptc"), 
		MEMBER_ENRLMNT_NETPRMUM("memberEnrlmntNetprmum"), 
		MEMBER_ENRLMNT_EHBPRMUM("memberEnrlmntEhbprmum"),
		MEMBER_ENRLMNT_GRPMAXPRMUM("memberEnrlmntGrpMaxPrmum"), 
		MEMBER_ENRLMNT_MEMBRNM_EDITABLE("memberEnrlmntMembrNmEditable"), 
		MEMBER_ENRLMNT_MEMBRGNDR_EDITABLE("memberEnrlmntMembrGndrEditable"),
		MEMBER_ENRLMNT_MEMBRSSN_EDITABLE("memberEnrlmntMembrSsnEditable"),
		MEMBER_ENRLMNT_APTC_EDITABLE("memberEnrlmntAptcEditable"),
		MEMBER_ENRLMNT_NETPRMUM_EDITABLE("memberEnrlmntNetPrmumEditable"),
		MEMBER_ENRLMNT_EHBPRMUM_EDITABLE("memberEnrlmntEhbPrmumEditable"),
		MEMBER_ENRLMNT_GRPMAXPRMUM_EDITABLE("memberEnrlmntGrpmaxPrmumEditable"),
		MEMBER_ENRLMNT_MEMBRDPNDNTNM("memberEnrlmntMembrDpndntNm"),
		MEMBER_ENRLMNT_MEMBRDPNDNTGNDR("memberEnrlmntMembrDpndntGndr"),
		MEMBER_ENRLMNT_MEMBRDPNDNTSSN("memberEnrlmntMembrDpndntSsn"),
		MEMBER_ENRLMNT_MEMBRDPNDNTNM_EDITABLE("memberEnrlmntMembrDpndntNmEditable"),
		MEMBER_ENRLMNT_MEMBRDPNDNTGNDR_EDITABLE("memberEnrlmntMembrDpndntGndrEditable"),
		MEMBER_ENRLMNT_MEMBRDPNDNTSSN_EDITABLE("memberEnrlmntMembrDpndntSsnEditable"),
		MEMBER_ENRLMNT_STRTENDDT_EDITABLE("memberEnrlmntStrtenddtEditable"),
		MEMBER_ENRLMNT_DPNDTSTRTENDDT_EDITABLE("memberEnrlmntDpndtStrtEndDtEditable"),
		MEMBER_ENRLMNT_APP_TYPE("memberEnrlmntAppType"),
		MEMBER_ENRLMNT_GROUP_MAX_APTC("memberEnrlmntGroupMaxAptc"),
		MEMBER_ENRLMNT_GROUP_MAX_APTC_EDITABLE("memberEnrlmntGroupMaxAptcEditable");


	    public String permissionLabel;

	    private EnrollmentInfo (String permissionLabel) {
	            this. permissionLabel = permissionLabel;
	    }

	}

	public enum MemberInfo {
		MEMBER_EDIT("memberEdit"), 
		MEMBER_EDIT_RIVS("memberEditRivs"), 
		MEMBER_VIEW_ACCESSCODE("memberViewAccessCode"),
		MEMBER_VIEW_CASEID("memberViewCaseid"),
		MEMBER_VIEW_MNSUREID("memberViewMnsureId");
	    public String permissionLabel;

	    private MemberInfo (String permissionLabel) {
	            this. permissionLabel = permissionLabel;
	    }

	}

	public enum MemberHistory {
		MEMBER_ABT_LOGCALL("memberAbtLogcall"), 
		MEMBER_ABT_SHOWCOMMENTS("memberAbtShowcomments"); 
	    public String permissionLabel;

	    private MemberHistory (String permissionLabel) {
	            this. permissionLabel = permissionLabel;
	    }

	}


	public enum NAVMenu {
		MEMBER_VIEW_BASICINFO("memberViewBasicInfo"), 
		MEMBER_VIEW_ENROLLMENTS("memberViewEnrollments"), 
		MEMBER_VIEW_COMMENTS("memberViewComments"), 
		MEMBER_VIEW_HISTORY("memberViewHistory"),
		MEMBER_VIEW_APPEALHISTORY("memberViewAppealHistory"), 
		MEMBER_VIEW_TICKETHISTORY("memberViewTicketHistory"), 
		MEMBER_VIEW_RESETPASSWORD("memberViewResetPassword"), 
		MEMBER_VIEW_VIEWMEMACCT("memberViewViewMemAcct"),
		MEMBER_VIEW_CREATETKT("memberViewCreateTkt"),
		MEMBER_VIEW_ADMIN_SEARCH("memberViewAdminSearch"),
		MEMBER_VIEW_VIEWCASE("memberViewViewCase"),
		MEMBER_VIEW_SEND_ACCOUNT_ACTIVATION_EMAIL("memberViewSendAccountActivationEmail"),
		MEMBER_VIEW_BACK_TO_CONSUMER_HOME("memberViewBackToConsumerHome"),
		VIEW_APPLICATIONS_PAGE("memberViewApplications");
	    
	    public String permissionLabel;

	    private NAVMenu (String permissionLabel) {
	            this. permissionLabel = permissionLabel;
	    }

	}

}


