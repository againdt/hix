//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.05.29 at 07:29:27 PM PDT 
//


package com.getinsured.iex.shop.mec.enrollment.verifyshopmec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NonESIMECResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NonESIMECResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseCode" type="{http://webservice.hix.getinsured.com/enrollment/VerifyShopMec}ErrorCodeType"/>
 *         &lt;element name="ResponseDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NonESIMECIndividualResponse" type="{http://webservice.hix.getinsured.com/enrollment/VerifyShopMec}NonESIMECIndividualResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NonESIMECResponseType", propOrder = {
    "responseCode",
    "responseDescription",
    "nonESIMECIndividualResponse"
})
public class NonESIMECResponseType {

    @XmlElement(name = "ResponseCode", required = true)
    protected String responseCode;
    @XmlElement(name = "ResponseDescription", required = true)
    protected String responseDescription;
    @XmlElement(name = "NonESIMECIndividualResponse", required = true)
    protected NonESIMECIndividualResponseType nonESIMECIndividualResponse;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDescription() {
        return responseDescription;
    }

    /**
     * Sets the value of the responseDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDescription(String value) {
        this.responseDescription = value;
    }

    /**
     * Gets the value of the nonESIMECIndividualResponse property.
     * 
     * @return
     *     possible object is
     *     {@link NonESIMECIndividualResponseType }
     *     
     */
    public NonESIMECIndividualResponseType getNonESIMECIndividualResponse() {
        return nonESIMECIndividualResponse;
    }

    /**
     * Sets the value of the nonESIMECIndividualResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonESIMECIndividualResponseType }
     *     
     */
    public void setNonESIMECIndividualResponse(NonESIMECIndividualResponseType value) {
        this.nonESIMECIndividualResponse = value;
    }

}
